--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorTableName TableSQL
--WFSScriptProcessorTableDrop
IF OBJECT_ID('SessionMaintenance.TableSQL') IS NOT NULL
       DROP TABLE SessionMaintenance.TableSQL
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/03/2015
*
* Purpose: Stores table create, drop, move SQL scripts.
*		   
*
* Modification History
* 03/03/2015 WI 197439 JPB	Created
* 01/02/2017 PT 127613943	JBS	Add Completed and RecoverCommand columns
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE SessionMaintenance.TableSQL
(
	TableID				INT NOT NULL,
	Completed			BIT, 
	SourceSchemaName	SYSNAME,
	SourceTableName		SYSNAME,
	TargetSchemaName	SYSNAME,
	TargetTableName		SYSNAME,
	FullyQualifiedSourceTableName NVARCHAR(256) NOT NULL,
	FullyQualifiedTargetTableName NVARCHAR(256) NOT NULL,
	CreateCommand		VARCHAR(MAX) NOT NULL,
	DropCommand			VARCHAR(MAX) NOT NULL,
	MoveCommand			VARCHAR(MAX) NOT NULL,
	RecoverCommand		VARCHAR(MAX) NOT NULL
);
--WFSScriptProcessorTableProperties
