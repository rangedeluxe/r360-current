--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorTableName IndexSQL
--WFSScriptProcessorTableDrop
IF OBJECT_ID('SessionMaintenance.IndexSQL') IS NOT NULL
       DROP TABLE SessionMaintenance.IndexSQL
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/03/2015
*
* Purpose: Stores index SQL scripts.
*		   
*
* Modification History
* 03/03/2015 WI 197443 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE SessionMaintenance.IndexSQL
(
	IndexID			INT IDENTITY(1,1) NOT NULL,
	TableID			INT NOT NULL,
	IndexName		NVARCHAR(128) NOT NULL,
	CreateCommand	VARCHAR(MAX) NOT NULL,
	DropCommand		VARCHAR(MAX) NOT NULL
);
--WFSScriptProcessorTableProperties
