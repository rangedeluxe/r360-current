--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorTableName TableColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('SessionMaintenance.TableColumns') IS NOT NULL
       DROP TABLE SessionMaintenance.TableColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Stores the table columns for Session Maintenance package. 
*
*
* Modification History
* 03/02/2015 WI 197437 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE SessionMaintenance.TableColumns
(
	TableID				INT,
	ColumnName			SYSNAME,
	OrdinalPosition		INT,
	IsIdentity			BIT
);
--WFSScriptProcessorTableProperties