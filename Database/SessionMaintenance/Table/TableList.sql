--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorTableName TableList
--WFSScriptProcessorTableDrop
IF OBJECT_ID('SessionMaintenance.TableList') IS NOT NULL
       DROP TABLE SessionMaintenance.TableList
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/03/2015
*
* Purpose: Stores list of tables to create SQL scripts for.
*		   
*
* Modification History
* 03/03/2015 WI 197438 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE SessionMaintenance.TableList
(
	TableID	INT IDENTITY(1,1) NOT NULL,
	SchemaName SYSNAME,
	TableName SYSNAME,
	QualifiedSchemaName SYSNAME,
	QualifiedTableName SYSNAME,
	FullyQualifiedTableName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
