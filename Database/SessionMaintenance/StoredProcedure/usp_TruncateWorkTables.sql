--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_TruncateWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_TruncateWorkTables') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_TruncateWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_TruncateWorkTables 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Truncate the SSIS package working tables.
*
* Modification History
* 03/02/2015 WI 197467 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	TRUNCATE TABLE SessionMaintenance.DefaultConstraintSQL;
	TRUNCATE TABLE SessionMaintenance.ExtendedPropertySQL;
	TRUNCATE TABLE SessionMaintenance.ForeignKeySQL;
	TRUNCATE TABLE SessionMaintenance.IndexSQL;
	TRUNCATE TABLE SessionMaintenance.TableColumns;
	TRUNCATE TABLE SessionMaintenance.TableList;
	TRUNCATE TABLE SessionMaintenance.TableSQL;
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
