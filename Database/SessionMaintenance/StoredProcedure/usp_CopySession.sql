--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_CopySession
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_CopySession') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_CopySession
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_CopySession
(
	@parmSessionRetentionDays INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Copy data session records that will be kept.
*
* Modification History
* 03/08/2015 WI 197450 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@RetentionDays VARCHAR(10);

BEGIN TRY

	SELECT @RetentionDays = CAST(@parmSessionRetentionDays AS VARCHAR(10));

	EXEC SessionMaintenance.usp_GetTableColumns 
		@parmTableName='Session',
		@parmIncludeIdentity=0,
		@parmIncludeQualifiedTableName=0,
		@parmColumnList=@InsertColumnList OUT;

	EXEC SessionMaintenance.usp_GetTableColumns 
		@parmTableName='Session',
		@parmIncludeIdentity=0,
		@parmIncludeQualifiedTableName=1,
		@parmColumnList=@SelectColumnList OUT;
		
	SELECT 
		@SQL = 'INSERT INTO '+FullyQualifiedTargetTableName+' ('+@InsertColumnList+') '+
		'SELECT '+@SelectColumnList+' FROM '+FullyQualifiedSourceTableName+' WHERE '+
		'CAST(CONVERT(VARCHAR,LogonDateTime,112) AS INT) >= (YEAR(DATEADD(dd,-'+@RetentionDays+
		',GETDATE()))*10000 + MONTH(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*100+DAY(DATEADD(d,-'+@RetentionDays +
		',GETDATE())));'
	FROM
		SessionMaintenance.TableList
		INNER JOIN SessionMaintenance.TableSQL ON SessionMaintenance.TableSQL.TableID = SessionMaintenance.TableList.TableID
	WHERE 
		TableName = 'Session';

	EXEC(@SQL);

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
