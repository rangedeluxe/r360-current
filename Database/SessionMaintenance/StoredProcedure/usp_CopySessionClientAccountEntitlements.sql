--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_CopySessionClientAccountEntitlements
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_CopySessionClientAccountEntitlements') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_CopySessionClientAccountEntitlements
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_CopySessionClientAccountEntitlements
(
	@parmEntitlementsRetentionDays INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Copy SessionClientAccountEntitlements records to be kept.
*
* Modification History
* 03/09/2015 WI 197452 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@ColumnList VARCHAR(MAX) = '',
		@RetentionDays VARCHAR(10);

BEGIN TRY

	SELECT @RetentionDays = CAST(@parmEntitlementsRetentionDays AS VARCHAR(10));

	EXEC SessionMaintenance.usp_GetTableColumns 
		@parmTableName='SessionClientAccountEntitlements',
		@parmIncludeIdentity=0,
		@parmIncludeQualifiedTableName=0,
		@parmColumnList=@ColumnList OUT;

	SELECT 
		@SQL = 'INSERT INTO '+SessionMaintenance.TableSQL.FullyQualifiedTargetTableName+' ('+@ColumnList+') '+
		'SELECT '+@ColumnList+' FROM '+SessionMaintenance.TableSQL.FullyQualifiedSourceTableName+' WHERE '+
		'CAST(CONVERT(VARCHAR,CreationDate,112) AS INT) >= (YEAR(DATEADD(dd,-'+@RetentionDays+
		',GETDATE()))*10000 + MONTH(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*100+DAY(DATEADD(d,-'+@RetentionDays +
		',GETDATE())));'
	FROM
		SessionMaintenance.TableList
		INNER JOIN SessionMaintenance.TableSQL ON SessionMaintenance.TableSQL.TableID = SessionMaintenance.TableList.TableID
	WHERE 
		TableName = 'SessionClientAccountEntitlements';

	EXEC(@SQL);

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
