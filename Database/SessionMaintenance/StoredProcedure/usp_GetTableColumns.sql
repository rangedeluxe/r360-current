--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_GetTableColumns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_GetTableColumns') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_GetTableColumns
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_GetTableColumns
(
	@parmTableName					SYSNAME,
	@parmIncludeIdentity			BIT,
	@parmIncludeQualifiedTableName	BIT,
	@parmColumnList					VARCHAR (MAX) OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Generate all the SQL scripts for a given table.
*
* Modification History
* 03/08/2015 WI 197444 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @ColumnList VARCHAR(MAX) = '';

BEGIN TRY

	SELECT
		@ColumnList = @ColumnList+
			CASE @parmIncludeQualifiedTableName
				WHEN 1 THEN QualifiedTableName+'.'
				ELSE ''
			END +
			'['+ColumnName+'],'
	FROM
		SessionMaintenance.TableList
		INNER JOIN SessionMaintenance.TableColumns ON SessionMaintenance.TableColumns.TableID = SessionMaintenance.TableList.TableID
	WHERE 
		SessionMaintenance.TableList.TableName = @parmTableName
		AND SessionMaintenance.TableColumns.IsIdentity = CASE @parmIncludeIdentity WHEN 0 THEN 0 ELSE SessionMaintenance.TableColumns.IsIdentity END
	ORDER BY
		SessionMaintenance.TableColumns.OrdinalPosition;
		
	SET @parmColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
