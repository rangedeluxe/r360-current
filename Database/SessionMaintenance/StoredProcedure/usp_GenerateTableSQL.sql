--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateTableSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_GenerateTableSQL') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_GenerateTableSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_GenerateTableSQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Generate table SQL scripts.
*
* Modification History
* 03/02/2015 WI 197464 JPB	Created
* 01/02/2017 PT #127613943 JBS	Add in code for new columns (Completed and RecoverCommand)
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY

	/* Create temp tables for the SP */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

	CREATE TABLE #tmpTableSQL
	(
		TableID					INT,
		SchemaName				SYSNAME,
		TableName				SYSNAME,
		QualifiedSchemaName		SYSNAME,
		QualifiedTableName		SYSNAME,
		FullyQualifiedTableName NVARCHAR(261),
		CreateCommand			VARCHAR(MAX) NOT NULL,
		DropCommand				VARCHAR(MAX) NOT NULL,
	);

	/* Setup the shared data type for the SP call */
	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName 
	FROM 
		SessionMaintenance.TableList;

	/* Call the shared SP to get the basic table information */
	INSERT INTO #tmpTableSQL
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateTableSQL 
		@parmScriptTableList=@TableList;

	INSERT INTO SessionMaintenance.TableSQL
	(
		TableID,
		Completed,
		SourceSchemaName,
		SourceTableName,
		TargetSchemaName,
		TargetTableName,
		FullyQualifiedSourceTableName,
		FullyQualifiedTargetTableName,
		CreateCommand,
		DropCommand,
		MoveCommand,
		RecoverCommand
	)
	SELECT
		SessionMaintenance.TableList.TableID,
		0,
		'SessionMaintenance',
		SessionMaintenance.TableList.QualifiedTableName,
		SessionMaintenance.TableList.QualifiedSchemaName,
		SessionMaintenance.TableList.QualifiedTableName,
		'[SessionMaintenance].'+SessionMaintenance.TableList.QualifiedTableName,
		SessionMaintenance.TableList.QualifiedSchemaName+'.'+SessionMaintenance.TableList.QualifiedTableName,
		CreateCommand,
		'IF OBJECT_ID('+CHAR(39)+'SessionMaintenance.'+SessionMaintenance.TableList.TableName+CHAR(39)+') IS NOT NULL DROP TABLE [SessionMaintenance].'+SessionMaintenance.TableList.QualifiedTableName+';',
		'ALTER SCHEMA SessionMaintenance TRANSFER '+SessionMaintenance.TableList.FullyQualifiedTableName+';',
		'ALTER SCHEMA RechubUser TRANSFER [SessionMaintenance].'+SessionMaintenance.TableList.QualifiedTableName+';'
	FROM
		SessionMaintenance.TableList
		INNER JOIN #tmpTableSQL ON #tmpTableSQL.TableID = SessionMaintenance.TableList.TableID

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

	EXEC SharedCommon.usp_RethrowException;
END CATCH
