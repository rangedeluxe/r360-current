--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_CopySessionLog
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_CopySessionLog') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_CopySessionLog
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_CopySessionLog
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Copy SessionLog records to be kept.
*
* Modification History
* 03/08/2015 WI 197453 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @FullyQualifiedJoinSourceTableName VARCHAR(256),
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@SQL VARCHAR(MAX) = '',
		@RetentionDays VARCHAR(10);

BEGIN TRY

	EXEC SessionMaintenance.usp_GetTableColumns 
		@parmTableName='SessionLog',
		@parmIncludeIdentity=0,
		@parmIncludeQualifiedTableName=0,
		@parmColumnList=@InsertColumnList OUT;

	EXEC SessionMaintenance.usp_GetTableColumns 
		@parmTableName='SessionLog',
		@parmIncludeIdentity=0,
		@parmIncludeQualifiedTableName=1,
		@parmColumnList=@SelectColumnList OUT;

	SELECT
		@FullyQualifiedJoinSourceTableName = FullyQualifiedTargetTableName
	FROM
		SessionMaintenance.TableList
		INNER JOIN SessionMaintenance.TableSQL ON SessionMaintenance.TableSQL.TableID = SessionMaintenance.TableList.TableID
	WHERE 
		SessionMaintenance.TableList.TableName = 'Session';

	SELECT 
		@SQL = 'INSERT INTO '+SessionMaintenance.TableSQL.FullyQualifiedTargetTableName+' ('+@InsertColumnList+') '+
		'SELECT '+@SelectColumnList+' FROM '+SessionMaintenance.TableSQL.FullyQualifiedSourceTableName+
		' INNER JOIN '+@FullyQualifiedJoinSourceTableName+' ON '+@FullyQualifiedJoinSourceTableName+'.[SessionID]='+
		SessionMaintenance.TableSQL.FullyQualifiedSourceTableName+'.[SessionID];'
	FROM
		SessionMaintenance.TableList
		INNER JOIN SessionMaintenance.TableSQL ON SessionMaintenance.TableSQL.TableID = SessionMaintenance.TableList.TableID
	WHERE 
		SessionMaintenance.TableList.TableName = 'SessionLog';

	EXEC(@SQL);

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
