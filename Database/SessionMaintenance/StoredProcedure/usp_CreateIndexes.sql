--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_CreateIndexes
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_CreateIndexes') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_CreateIndexes
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_CreateIndexes 
(
	@parmEntitlements BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Create indexes.
*
* Modification History
* 03/08/2015 WI 197448 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@Loop INT=1;

BEGIN TRY

	WHILE( @Loop <= (SELECT MAX(IndexID) FROM SessionMaintenance.IndexSQL) )
	BEGIN
		SET @SQL='';
		SELECT 
			@SQL = CreateCommand
		FROM
			SessionMaintenance.IndexSQL
			INNER JOIN SessionMaintenance.TableList ON SessionMaintenance.TableList.TableID = SessionMaintenance.IndexSQL.TableID
		WHERE 
			IndexID = @Loop
			AND ((@parmEntitlements=1 AND TableName = 'SessionClientAccountEntitlements')
				OR (@parmEntitlements=0 AND TableName <> 'SessionClientAccountEntitlements'))
		ORDER BY 
			SessionMaintenance.IndexSQL.TableID;

		IF( LEN(@SQL) > 0 )
			EXEC(@SQL);

		SET @Loop=@Loop+1;
	END

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
