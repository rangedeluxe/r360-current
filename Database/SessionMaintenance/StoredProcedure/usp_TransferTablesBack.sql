--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_TransferTablesBack
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_TransferTablesBack') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_TransferTablesBack
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_TransferTablesBack 
(
	@parmEntitlements BIT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 12/12/2016
*
* Purpose: Transfer tables from Working schema to RechubUser schema.
*
* Modification History
* 12/12/2016 PT #127613943 JBS	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@Loop INT=1;

BEGIN TRY

	WHILE( @Loop <= (SELECT MAX(TableID) FROM SessionMaintenance.TableSQL) )
	BEGIN
		SET @SQL='';
		SELECT 
			@SQL = RecoverCommand
		FROM
			SessionMaintenance.TableSQL
			INNER JOIN SessionMaintenance.TableList ON SessionMaintenance.TableList.TableID = SessionMaintenance.TableSQL.TableID
		WHERE 
			SessionMaintenance.TableSQL.TableID = @Loop
			AND ((@parmEntitlements=1 AND TableName = 'SessionClientAccountEntitlements')
				OR (@parmEntitlements=0 AND TableName <> 'SessionClientAccountEntitlements'))
		ORDER BY 
			SessionMaintenance.TableSQL.TableID;

		IF( LEN(@SQL) > 0 )
			EXEC(@SQL);

		SET @Loop=@Loop+1;
	END
	
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
