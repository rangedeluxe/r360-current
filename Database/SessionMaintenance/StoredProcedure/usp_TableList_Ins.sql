--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TableList_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_TableList_Ins') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_TableList_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_TableList_Ins 
(
	@parmTables VARCHAR(MAX)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Insert the list of tables passed in via a XML string.

* NOTE: The config database does not have a way to store XML data, so it is stored
*	as a string. The string is then casted to a XML so it can be parsed.
*
* Modification History
* 03/02/2015 WI 197465 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @TableXML XML;

BEGIN TRY

	SET @TableXML = CAST(@parmTables AS XML);
	INSERT INTO SessionMaintenance.TableList
	(
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		SessionMaintenance.att.value('@Schema','NVARCHAR(128)'),
		SessionMaintenance.att.value('@Table','NVARCHAR(128)'),
		'[' + SessionMaintenance.att.value('@Schema','NVARCHAR(128)') + ']',
		'[' + SessionMaintenance.att.value('@Table','NVARCHAR(128)') + ']',
		'[' + SessionMaintenance.att.value('@Schema','NVARCHAR(128)') + '].[' + SessionMaintenance.att.value('@Table','NVARCHAR(128)') + ']'
	FROM
		@TableXML.nodes('/SessionMaintenance/Table') AS SessionMaintenance(att);
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
