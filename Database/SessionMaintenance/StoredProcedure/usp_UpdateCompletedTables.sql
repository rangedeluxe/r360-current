--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_UpdateCompletedTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_UpdateCompletedTables') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_UpdateCompletedTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_UpdateCompletedTables 
(
	@parmEntitlements BIT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 12/12/2016
*
* Purpose: Update Rows to Completed in the SessionMaintenance.TableSQL
*
* Modification History
* 12/12/2016 PT #127613943 JBS	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	UPDATE SessionMaintenance.TableSQL
		SET Completed = 1
	FROM
		SessionMaintenance.TableSQL T2
		INNER JOIN SessionMaintenance.TableList ON SessionMaintenance.TableList.TableID = T2.TableID
	WHERE
		(@parmEntitlements=1 AND TableName = 'SessionClientAccountEntitlements')
		OR (@parmEntitlements=0 AND TableName <> 'SessionClientAccountEntitlements');

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
