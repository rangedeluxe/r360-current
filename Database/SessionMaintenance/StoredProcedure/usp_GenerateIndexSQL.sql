--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateIndexSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_GenerateIndexSQL') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_GenerateIndexSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_GenerateIndexSQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Generate index SQL scripts.
*
* Modification History
* 03/02/2015 WI 197462 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 
DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY

	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName 
	FROM 
		SessionMaintenance.TableList;

	INSERT INTO SessionMaintenance.IndexSQL
	(
		TableID,
		IndexName,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateIndexSQL
		@parmScriptTableList=@TableList;

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
