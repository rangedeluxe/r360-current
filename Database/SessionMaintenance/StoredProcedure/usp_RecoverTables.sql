--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_RecoverTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_RecoverTables') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_RecoverTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_RecoverTables 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 12/12/2016
*
* Purpose: Recover Session Tables if Session Maintenance job fails during its processing.
*
* Modification History
* 12/12/2016 PT #127613943 JBS	Created
* 01/12/2017 PT #127613943 MGE	Corrected a comparator in the SessionClientAccountEntitlements section
*				There is a flaw in treating the other 3 tables as a set, 
*					SessionMaintenance.usp_DropTables worked before this fix.  We 
*					really need to treat each separately - just in case.  But that's a lot of work 
*					I'm leaving until we make the whole process more robust.  
*					It may no longer be a need at that point.  
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@Loop INT=1,
		@NeedToProcess INT;

		SELECT ' Restore Process STARTED' 

BEGIN TRY
IF OBJECT_ID('SessionMaintenance.[SessionActivityLog]') IS NOT NULL AND OBJECT_ID('SessionMaintenance.[SessionLog]') IS NOT NULL AND OBJECT_ID('SessionMaintenance.[Session]') IS NOT NULL
BEGIN
	IF EXISTS ( SELECT TOP 1 1 FROM SessionMaintenance.TableSQL WHERE SessionMaintenance.TableSQL.Completed = 0 AND TargetTableName <> '[SessionClientAccountEntitlements]')
	BEGIN
	--	WHILE( @Loop <= (SELECT MAX(TableID) FROM SessionMaintenance.TableSQL WHERE SessionMaintenance.TableSQL.Completed = 0) )
	--BEGIN
		-- Confirm if Table condition exists to  swap
		--   I dont hink i need to find these    SELECT TOP 1  1 FROM RecHubUser.[Session] INNER JOIN RecHubUser.[SessionLog] ON RecHubUser.[Session].SessionID = RecHubUser.[SessionLog].SessionID
		--											INNER JOIN RecHubUser.[SessionActivityLog] ON RecHubUser.[Session].SessionID = RecHubUser.[SessionActivityLog].SessionID
		
		--SET @NeedToProcess = ( SELECT TOP 1  1 FROM SessionMaintenance.[Session] INNER JOIN SessionMaintenance.[SessionLog] ON SessionMaintenance.[Session].SessionID = SessionMaintenance.[SessionLog].SessionID
		--									INNER JOIN SessionMaintenance.[SessionActivityLog] ON SessionMaintenance.[Session].SessionID = SessionMaintenance.[SessionActivityLog].SessionID)
		--SELECT @NeedToProcess

		-- Remove old
		SELECT ' Restore Process began' 

		EXEC SessionMaintenance.usp_DropExtendedProperties @parmEntitlements=0;
		EXEC SessionMaintenance.usp_DropDefaultConstraints @parmEntitlements=0;
		EXEC SessionMaintenance.usp_DropIndexes @parmEntitlements=0;
		IF OBJECT_ID('RecHubUser.[SessionActivityLog]') IS NOT NULL
				DROP TABLE RecHubUser.[SessionActivityLog];
		IF OBJECT_ID('RecHubUser.[SessionLog]') IS NOT NULL
				DROP TABLE RecHubUser.SessionLog;
		IF OBJECT_ID('RecHubUser.[Session]') IS NOT NULL
				DROP TABLE RecHubUser.[Session];

		EXEC SessionMaintenance.usp_TransferTablesBack @parmEntitlements=0;    --  Return tables to RecHubUser schema
		-- Rebuild Objects
		EXEC SessionMaintenance.usp_CreateIndexes @parmEntitlements=0;  --  first
		EXEC SessionMaintenance.usp_CreateExtendedProperties @parmEntitlements=0;
		EXEC SessionMaintenance.usp_CreateForeignKeys @parmEntitlements=0;
		EXEC SessionMaintenance.usp_CreateDefaultConstraints @parmEntitlements=0;

		SELECT ' Restore Process END' 

--	END

	END
END

IF OBJECT_ID('SessionMaintenance.SessionClientAccountEntitlements') IS NOT NULL
BEGIN
	IF EXISTS ( SELECT TOP 1 1 FROM SessionMaintenance.TableSQL WHERE SessionMaintenance.TableSQL.Completed = 0 AND TargetTableName = '[SessionClientAccountEntitlements]')
	BEGIN
 
		SELECT ' Restore Process began SessionClientAccountEntitlements' 

		EXEC SessionMaintenance.usp_DropExtendedProperties @parmEntitlements=1;
		EXEC SessionMaintenance.usp_DropDefaultConstraints @parmEntitlements=1;
		EXEC SessionMaintenance.usp_DropIndexes @parmEntitlements=1;
		IF OBJECT_ID('RecHubUser.SessionClientAccountEntitlements') IS NOT NULL
				DROP TABLE RecHubUser.SessionClientAccountEntitlements; 

		EXEC SessionMaintenance.usp_TransferTablesBack @parmEntitlements=1;    --  Return tables to RecHubUser schema
		-- Rebuild Objects
		EXEC SessionMaintenance.usp_CreateIndexes @parmEntitlements=1;  --  first
		EXEC SessionMaintenance.usp_CreateExtendedProperties @parmEntitlements=1;
		EXEC SessionMaintenance.usp_CreateForeignKeys @parmEntitlements=1;
		EXEC SessionMaintenance.usp_CreateDefaultConstraints @parmEntitlements=1;

		SELECT ' Restore Process END SessionClientAccountEntitlements' 

 	END
END


END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
