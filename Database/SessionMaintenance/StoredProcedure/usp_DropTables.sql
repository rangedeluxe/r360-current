--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema SessionMaintenance
--WFSScriptProcessorStoredProcedureName usp_DropTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SessionMaintenance.usp_DropTables') IS NOT NULL
       DROP PROCEDURE SessionMaintenance.usp_DropTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE SessionMaintenance.usp_DropTables 
(
	@parmEntitlements BIT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/02/2015
*
* Purpose: Drop tables.
*
* Modification History
* 03/08/2015 WI 197458 JPB	Created
* 01/12/2017 PT 127613943 MGE Made sure all drops were executed as intended  
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '';
DECLARE @TableCount INT;
DECLARE @CurrentRow INT;

BEGIN TRY

	IF @parmEntitlements = 1
	BEGIN
	
		SELECT  @CurrentRow = SessionMaintenance.TableSQL.TableID 
		FROM SessionMaintenance.TableSQL
		WHERE SessionMaintenance.TableSQL.SourceTableName = '[SessionClientAccountEntitlements]'
		SET @TableCount = @CurrentRow 
	
	END;

IF	@parmEntitlements = 0
	BEGIN
	
	SELECT	@TableCount = Count(*) 
	FROM SessionMaintenance.TableSQL
	WHERE SessionMaintenance.TableSQL.SourceTableName <> '[SessionClientAccountEntitlements]'

	SELECT		@CurrentRow = MIN(SessionMaintenance.TableSQL.TableID) 
	FROM SessionMaintenance.TableSQL
	WHERE SessionMaintenance.TableSQL.SourceTableName <> '[SessionClientAccountEntitlements]'

	END;

	WHILE (@CurrentRow <= @TableCount)
		BEGIN
		SELECT 
			@SQL = DropCommand
		FROM
			SessionMaintenance.TableSQL
			INNER JOIN SessionMaintenance.TableList ON SessionMaintenance.TableList.TableID = SessionMaintenance.TableSQL.TableID
		WHERE
			(@parmEntitlements=1 AND SourceTableName = '[SessionClientAccountEntitlements]')
			OR (@parmEntitlements=0 AND SourceTableName <> '[SessionClientAccountEntitlements]')
			AND SessionMaintenance.TableSQL.TableID = @CurrentRow
	
		EXEC(@SQL);
		SET @CurrentRow  = @CurrentRow + 1
	END
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
