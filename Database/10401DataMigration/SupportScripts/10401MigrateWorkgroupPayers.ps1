<#
	.SYNOPSIS
	PowerShell script used to exec the 1.04.01 MigrateWorkgroupPayers SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for 1.04.01 Data Migration that will Add WorkgroupPayer records in the 2.03 table.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER BankIDs
	A Bank ID or (comma separated) list of Bank IDs that will be copied from the 1.04.01 to the 2.03 database.

	.PARAMETER LockboxIDs
	A LockBox ID or (comma separated) list of LockBox IDs that will be copied from the 1.04.01 to the 2.03 database. The Value ALL will 
	Bring over ALL Lockboxes for the BankID(s) listed.  ALL is the default value used if nothing is passed in. 

	.PARAMETER StartDate
	The Beginning date for Deposit date key range for Batches to migrate from the 1.04.01 to the 2.03 database.

#>
param
(
	[parameter(Mandatory = $true)][string] $ConfigFile,
	[parameter(Mandatory = $true)][string] $BankID,
	[parameter(Mandatory = $true)][datetime] $StartDate,
	[parameter(Mandatory = $true)][string] $LockboxIDs = "ALL"
)

$ScriptVerison = "2.03";
$ScriptName = "10401MigrateWorkgroupPayers";

################################################################################
## Deluxe Corporation (DLX)
## Copyright © 2019 Deluxe Corp. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2019 Deluxe Corporation. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of DLX and contain DLX trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# DLX (see the DLX license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of DLX.
################################################################################
#
# 04/29/2019 MGE	2.03	R360-15977	Created.
#
################################################################################

cls
if( [System.IO.Path]::GetDirectoryName($ConfigFile).Length -eq 0 )
{
	$ConfigFile = Join-Path -Path (Get-Location -PSProvider FileSystem) -ChildPath $ConfigFile;
}
if( !(Test-Path $ConfigFile) )
{
	Write-Host "Cannot find config file $ConfigFile. Aborting..." -ForegroundColor Red;
	return;
}

#BuildXML document to be sent to execute PowerShellScript
$ConfigXML = New-Object "System.Xml.XmlDocument";
$ConfigXML.Load("$ConfigFile");

$SourceServerName = $ConfigXML.Config.Connections.SourceConnection.Server;
$SourceDatabase = $ConfigXML.Config.Connections.SourceConnection.Database;
$TargetServerName = $ConfigXML.Config.Connections.TargetConnection.Server;
$TargetDatabase = $ConfigXML.Config.Connections.TargetConnection.Database;

$SSISFolder = $ConfigXML.Config.SSISPath;
if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

#Write-Host "ServerName: " $SourceServerName
#Write-Host $SourceDatabase
Write-Host "SSIS Folder: " $SSISFolder;
#
Write-Host "BankID:" $BankID;

$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISFolder</PackageFolder>
	<PackageName>10401MigrateWorkgroupPayers</PackageName>
	<Parameters>
		<Parameter Name=""SourceServerName"" Value=""$SourceServerName""/>
		<Parameter Name=""SourceInitialCatalog"" Value=""$SourceDatabase""/>
		<Parameter Name=""TargetServerName"" Value=""$TargetServerName""/>
		<Parameter Name=""TargetInitialCatalog"" Value=""$TargetDatabase""/>
		<Parameter Name=""SiteBankIDList"" Value=""$BankID""/>
		<Parameter Name=""LockboxIDList"" Value=""$LockboxIDs""/>
		<Parameter Name=""StartDate"" Value=""$StartDate""/>
	</Parameters>
</SSISPackage>"
);


$CurrentFolder = Get-Location;
Set-Location "$SSISFolder";
.\ExecuteSSISPackage $xml;
Set-Location "$CurrentFolder";
