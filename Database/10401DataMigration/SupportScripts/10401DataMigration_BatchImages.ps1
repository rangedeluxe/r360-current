﻿<#
	.SYNOPSIS
	PowerShell script used to exec the 1.04.01 Client Setup Data Migration SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for 1.04.01 Data Migration that will create Client Setup records in the 2.02 DataImportQueue table.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER BankIDs
	A Bank ID or (comma separated) list of Bank IDs that will be copied from the 1.04.01 to the 2.02 database.

	.PARAMETER LockboxIDs
	A LockBox ID or (comma separated) list of LockBox IDs that will be copied from the 1.04.01 to the 2.02 database. The Value ALL will 
	Bring over ALL Lockboxes for the BankID(s) listed.  ALL is the default value used if nothing is passed in. 

	.PARAMETER StartDate
	The Beginning date for Deposit date key range for Batches to migrate from the 1.04.01 to the 2.02 database.

	.PARAMETER EndDate
	The End date for Deposit date key range for Batches to migrate from the 1.04.01 to the 2.02 database.

#>
param
(
	[parameter(Mandatory = $true)][string] $ConfigFile,
	[parameter(Mandatory = $true)][int] $BankID,
	[parameter(Mandatory = $true)][datetime] $StartDate,
	[parameter(Mandatory = $true)][datetime] $EndDate,
	[parameter(Mandatory = $true)][string] $LockboxIDs = "ALL"
)

$ScriptVerison = "2.02";
$ScriptName = "10401DataMigration_BatchImages";

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 12/06/2016 JPB	2.02	PT #132029479  	Created.
#
################################################################################


################################################################################
# Write-Log
# Write a message to the log file and console
################################################################################
function Write-Log([String] $local:LogFileName, [String] $local:LogInformation)
{
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($local:LogFileName) -eq $null )
		{
			$local:LogFileName = Get-Location + "\" + $local:LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date -format "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			Write-Host $_;
		}
	}
	end
	{
		(Get-Date -format "yyyy-MM-dd HH:mm:ss") + " " + $local:LogInformation | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		Write-Host $local:LogInformation;
	}
}

################################################################################
# Copy-BankImages
# Copy images by bank
################################################################################
function Copy-BankImages([string]$local:SourcePath,[string]$local:TargetPath,[string]$local:LogFileName)
{
	if( !(Test-Path -path $local:SourcePath) )
	{
		Write-Log $local:LogFileName ([string]::Format("Folder {0} not found, skipping.",$local:SourcePath));
	}
	else
	{
		$local:SourceFileCount = (Get-ChildItem $local:SourcePath -Recurse -Name).Count;
		Write-Log $local:LogFileName ([string]::Format("Copying {0} folders/files from {1} to {2}",$local:SourceFileCount,$local:SourcePath,$local:TargetPath));
		Write-Host "Please wait....." -NoNewline;
		try
		{
			Copy-Item -Path $local:SourcePath -Destination $local:TargetPath -Recurse -Force;
		}
		catch
		{
			Write-Log $local:LogFileName ([string]::Format("Error: {0} copying files.",$_));
		}
		finally
		{
			$local:TargetFileCount = (Get-ChildItem $local:TargetPath -Recurse -Name).Count - 1;
			Write-Log $local:LogFileName ([string]::Format("Copied {0} folders/files",$local:TargetFileCount));
			if( $local:SourceFileCount -ne $local:TargetFileCount )
			{
				Write-Log $local:LogFileName ([string]::Format("ERROR: There were {0} folders/files in the source folder and {1} folders/files in the target folder",$local:SourceFileCount,$local:TargetFileCount));
			}
		}
		Write-Host "Complete";
	}
}

################################################################################
# Copy-LockboxImages
# Copy images by lockbox list
################################################################################
function Copy-LockboxImages([string]$local:SourcePath,[string]$local:TargetPath,[string]$local:LockboxIDs,[string]$local:LogFileName)
{
	if( !(Test-Path -path $local:SourcePath) )
	{
		Write-Log $local:LogFileName ([string]::Format("Folder {0} not found, skipping.",$local:SourcePath));
	}
	else
	{
		#Convert the lockbox IDs to an array
		$local:LockboxList = $local:LockboxIDs.split(",");
		
		$local:LockboxImageFolders = Get-ChildItem -Path  $local:SourcePath -Directory
		#Get only the folders that match the lockbox list
		$local:ValidLockboxImageFolders = $local:LockboxImageFolders | Where-Object {$local:LockboxList -eq $_.PSChildName};
		foreach( $local:LockboxImageFolder in $local:ValidLockboxImageFolders)
		{
			$local:LockboxTargetPath = Join-Path -Path $local:TargetPath -ChildPath $local:LockboxImageFolder.PSChildName;
			if( !(Test-Path -path $local:LockboxTargetPath) )
			{
				New-Item $local:LockboxTargetPath -Type directory | Out-Null;
			}
			$local:SourceFileCount = (Get-ChildItem $local:LockboxImageFolder.FullName -Name).Count;
			Write-Log $local:LogFileName ([string]::Format("Copying {0} files from {1} to {2}",$local:SourceFileCount,$local:LockboxImageFolder.FullName,$local:LockboxTargetPath));
			Write-Host "Please wait....." -NoNewline;
			try
			{
				Copy-Item -Path (Join-Path -Path $local:LockboxImageFolder.FullName -ChildPath "*.*") -Destination $local:LockboxTargetPath -Force;		
			}
			catch
			{
				Write-Log $local:LogFileName ([string]::Format("Error: {0} copying files.",$_));
			}
			finally
			{
				$local:TargetFileCount = (Get-ChildItem $local:LockboxTargetPath -Name | where {!$_.PSIsContainer}).Count;
				Write-Log $local:LogFileName ([string]::Format("Copied {0} files",$local:TargetFileCount));
				if( $local:SourceFileCount -ne $local:TargetFileCount )
				{
					Write-Log $local:LogFileName ([string]::Format("ERROR: There were {0} files in the source folder and {1} in the target folder",$local:SourceFileCount,$local:TargetFileCount));
				}
			}
			Write-Host "Complete";
		}
	}
}

################################################################################
# Copy-Images
# Copy images, calls either copy by bank or copy by lockbox list based on $local:LockboxIDs
################################################################################
function Copy-Images([string]$local:SourcePath,[string]$local:TargetPath,[int]$local:BankID,[string]$local:LockboxIDs,[string]$local:LogFileName,[int]$local:StartDate,[int]$local:EndDate)
{
	#Create the target folder if it does not exist
	if( !(Test-Path -path $local:TargetPath) )
	{
		New-Item $local:TargetPath -Type directory | Out-Null;
	}
	
	#Get a list of the folders in the source path
	$local:ImageFolders = Get-ChildItem -Path  $local:SourcePath -Directory

	foreach( $local:ImageFolder in $local:ImageFolders)
	{
		#Make sure the folder name is a processing date
		if( $local:ImageFolder.PSChildName -match '^\d{8}$' )
		{
			$local:ImageFolderName = [int] $local:ImageFolder.PSChildName;
			
			#Process only the folders that are within the date range entered
			if( $local:ImageFolderName -ge $local:StartDate -and $local:ImageFolderName -le $local:EndDate )
			{
				$local:CurrentTargetPath = Join-path -path $local:TargetPath -ChildPath $local:ImageFolder.PSChildName;
				
				#Create the target folder if it does not exist
				if( !(Test-Path -path $local:CurrentTargetPath) )
				{
					New-Item $local:CurrentTargetPath -Type directory | Out-Null;
				}
				
				#Copy files based on all lockbox or lockbox by lockbox
				if( $local:LockboxIDs.ToUpper() -eq "ALL" )
				{
					Copy-BankImages (Join-Path -Path (Join-Path -Path $local:SourcePath -ChildPath $local:ImageFolder.PSChildName) -ChildPath $local:BankID) $local:CurrentTargetPath $local:LogFileName;
				}
				else
				{
					Copy-LockboxImages (Join-Path -Path (Join-Path -Path $local:SourcePath -ChildPath $local:ImageFolder.PSChildName) -ChildPath $local:BankID) (Join-Path -Path $local:CurrentTargetPath -ChildPath $local:BankID) $local:LockboxIDs $local:LogFileName;
				}
			}
		}
		else
		{
			Write-Log $local:LogFileName ([string]::Format("{0} is not a valid processing date image folder name, skipping.",$local:ImageFolder.PSChildName));
		}
	}	
}

################################################################################
# Get-DatabaseDateRange
# Get the processing date range from the deposit date range given by the user
################################################################################
function Get-DatabaseDateRange([string]$local:DBServer,[string]$local:DBName,[int]$local:BankID,[string]$local:LockboxIDs,[string]$local:LogFileName,[datetime]$local:StartDate,[datetime]$local:EndDate,[ref]$local:StartDepositDateKey,[ref]$local:EndDepositDateKey)
{

	$local:Results = $true;
	
	#Setup the SQL connection and statement
	$local:dbConnectionOptions = [string]::Format("Data Source={0}; Initial Catalog={1};Integrated Security=SSPI",$local:DBServer,$local:DBName);
	$local:dbConnection = New-Object System.Data.SqlClient.SqlConnection($local:dbConnectionOptions);
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandText = [string]::Format("SELECT COALESCE(MIN(ProcessingDateKey),0) AS MinProcessingDate, COALESCE(MAX(ProcessingDateKey),0) AS MaxProcessingDate
FROM OLTA.factBatchSummary
INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.factBatchSummary.LockboxKey
WHERE SiteBankID = {0}",$local:BankID);
    if( $local:LockboxIDs -ne "All" )
    {
        $local:dbCommand.CommandText = [string]::Format("{0} AND SiteLockboxID IN ({1})",$local:dbCommand.CommandText,$local:LockboxIDs);
    }
	$local:dbCommand.CommandText = ([string]::Format("{0} AND DepositDateKey BETWEEN {1:yyyyMMdd} AND {2:yyyyMMdd}",$local:dbCommand.CommandText,$local:StartDate,$local:EndDate));
    $local:dbCommand.CommandText += ";";

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$local:handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		$event.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$local:SQLError = $true;
		}
	};
	
	$local:dbConnection.add_InfoMessage($local:handler);
	$local:dbConnection.FireInfoMessageEventOnUserErrors = $true;

	try
	{
		$local:dbConnection.Open();
	}
	catch
	{
			Write-Log $local:LogFileName ([string]::Format("Error Opening Database Connection: {0}",$_.Exception.Message));
			return $false;
	}

	try
	{
		#Execute the SQL statement
		$local:reader = $local:dbCommand.ExecuteReader();
	}
	catch
	{
			$local:Results = $false;
			Write-Log $local:LogFileName ([string]::Format("Error executing SQL Command: {0}",$_.Exception.Message));
		    $local:Results = $local:dbConnection.Close();
  	}

	#If there are rows returned, read the data
    if( $local:reader.HasRows )
    {
        $local:table = New-Object System.Data.DataTable;
        $local:table.Load($local:reader);
	    $local:StartDepositDateKey.value = $local:table[0].MinProcessingDate;
	    $local:EndDepositDateKey.value = $local:table[0].MaxProcessingDate;
    }
    else
    {
		#No rows returned, set to 0 so nothing happens
	    $local:StartDepositDateKey.value = 0;
	    $local:EndDepositDateKey.value = 0;
    }
    $local:dbConnection.Close();
	return $local:Results;
}

################################################################################
#
# Main
#
################################################################################
cls
Write-Host ([string]::Format("{0} {1}",$ScriptName,$ScriptVerison));
if( [System.IO.Path]::GetDirectoryName($ConfigFile).Length -eq 0 )
{
	$ConfigFile = Join-Path -Path (Get-Location -PSProvider FileSystem) -ChildPath $ConfigFile;
}
if( !(Test-Path $ConfigFile) )
{
	Write-Host "Cannot find config file $ConfigFile. Aborting..." -ForegroundColor Red;
	return;
}

#Load config file
$ConfigXML = New-Object "System.Xml.XmlDocument";
$ConfigXML.Load("$ConfigFile");

#Get the current folder and create a log file name
$LogFileName = Join-Path -Path ([System.IO.Path]::GetDirectoryName($ConfigFile)) -ChildPath ($ScriptName + "_" + (Get-Date -Format yyyyMMddHHmmss) + ".txt");

#Write the header to the log file
(Get-Date -Format "yyyy-MM-dd HH:mm:ss") + " " + ([string]::Format("{0} Ver. {1}",$ScriptName,$ScriptVerison)) | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;

#Write the input params to the log file
Write-Log $LogFileName ([string]::Format("BankID                           : {0}",$BankID));
Write-Log $LogFileName ([string]::Format("LockboxIDs                       : {0}",$LockboxIDs));
Write-Log $LogFileName ([string]::Format("Input Date Range                 : {0:MM/dd/yyyy}-{1:MM/dd/yyyy}",$StartDate,$EndDate));

[int] $StartDepositDateKey = 0;
[int] $EndDepositDateKey = 0;
if( Get-DatabaseDateRange $ConfigXML.Config.Connections.SourceConnection.Server $ConfigXML.Config.Connections.SourceConnection.Database $BankID $LockboxIDs $LogFileName $StartDate $EndDate ([ref] $StartDepositDateKey) ([ref]$EndDepositDateKey) )
{
	#Write the actual date range that will be used to copy the images
	Write-Log $LogFileName ([string]::Format("Database ProcessingDateKey Range : {0} - {1}",$StartDepositDateKey,$EndDepositDateKey));

	#Copy the images, funciton determines if copying by bank or lockbox list
	Copy-Images $ConfigXML.Config.ImageCopy.SourcePath $ConfigXML.Config.ImageCopy.TargetPath $BankID $LockboxIDs $LogFileName $StartDepositDateKey $EndDepositDateKey;
}
else
{
	Write-Log $LogFileName "Error executing Get-DatabaseDateRange.";
}
