﻿<#
	.SYNOPSIS
	PowerShell script used to exec the 1.04.01 Client Setup Data Migration SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for 1.04.01 Data Migration that will create Client Setup records in the 2.02 DataImportQueue table.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER BankIDs
	A Bank ID or (comma separated) list of Bank IDs that will be copied from the 1.04.01 to the 2.02 database.

	.PARAMETER LockboxIDs
	A LockBox ID or (comma separated) list of LockBox IDs that will be copied from the 1.04.01 to the 2.02 database. The Value ALL will 
	Bring over ALL Lockboxes for the BankID(s) listed.  ALL is the default value used if nothing is passed in. 

	.PARAMETER StartDate
	The Beginning date for Deposit date key range for Batches to migrate from the 1.04.01 to the 2.02 database.

	.PARAMETER EndDate
	The End date for Deposit date key range for Batches to migrate from the 1.04.01 to the 2.02 database.

#>
param
(
	[parameter(Mandatory = $true)][string] $ConfigFile,
	[parameter(Mandatory = $true)][int] $BankID,
	[parameter(Mandatory = $true)][datetime] $StartDate,
	[parameter(Mandatory = $true)][datetime] $EndDate,
	[parameter(Mandatory = $true)][string] $LockboxIDs = "ALL"
)

$ScriptVerison = "2.02";
$ScriptName = "10401DataMigration_BatchData";

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 11/10/2016 JPB	2.02	PT 	  	Created.
# 11/21/2016 JBS	2.02	PT#132009883 	Made LockboxIDs parm mandatory and default = ALL
#
################################################################################

cls
if( [System.IO.Path]::GetDirectoryName($ConfigFile).Length -eq 0 )
{
	$ConfigFile = Join-Path -Path (Get-Location -PSProvider FileSystem) -ChildPath $ConfigFile;
}
if( !(Test-Path $ConfigFile) )
{
	Write-Host "Cannot find config file $ConfigFile. Aborting..." -ForegroundColor Red;
	return;
}

#BuildXML document to be sent to execute PowerShellScript
$ConfigXML = New-Object "System.Xml.XmlDocument";
$ConfigXML.Load("$ConfigFile");

$SourceServerName = $ConfigXML.Config.Connections.SourceConnection.Server;
$SourceDatabase = $ConfigXML.Config.Connections.SourceConnection.Database;
$TargetServerName = $ConfigXML.Config.Connections.TargetConnection.Server;
$TargetDatabase = $ConfigXML.Config.Connections.TargetConnection.Database;

$SSISFolder = $ConfigXML.Config.SSISPath;
if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

#Write-Host $SourceServerName
#Write-Host $SourceDatabase
#Write-Host $SSISFolder;
#

$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISFolder</PackageFolder>
	<PackageName>10401DataMigration_BatchData</PackageName>
	<Parameters>
		<Parameter Name=""SourceServerName"" Value=""$SourceServerName""/>
		<Parameter Name=""SourceInitialCatalog"" Value=""$SourceDatabase""/>
		<Parameter Name=""TargetServerName"" Value=""$TargetServerName""/>
		<Parameter Name=""TargetInitialCatalog"" Value=""$TargetDatabase""/>
		<Parameter Name=""BankID"" Value=""$BankID""/>
		<Parameter Name=""LockboxIDList"" Value=""$LockboxIDs""/>
		<Parameter Name=""StartDate"" Value=""$StartDate""/>
		<Parameter Name=""EndDate"" Value=""$EndDate""/>
	</Parameters>
</SSISPackage>"
);


$CurrentFolder = Get-Location;
Set-Location "$SSISFolder";
.\ExecuteSSISPackage $xml;
Set-Location "$CurrentFolder";
