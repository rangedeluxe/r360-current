--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName ExtractBatches
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.ExtractBatches') IS NOT NULL
       DROP TABLE RecHubMigration.ExtractBatches
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Staging table for batches to extract/migration.
*		   
*
* Modification History
* 11/10/2016 PT  JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.ExtractBatches
(
	Batch_Id BIGINT NOT NULL
		CONSTRAINT PK_ExtractBatches PRIMARY KEY CLUSTERED,
	BatchID BIGINT NOT NULL, /* Source Batch ID, not R360 Batch ID */
	DDA	VARCHAR(40)	NULL,
	ABA VARCHAR(10) NULL,
	DepositDateKey INT NOT NULL,
	BatchDateKey INT NOT NULL,
	ProcessingDateKey INT NOT NULL,
	BatchSourceKey TINYINT NOT NULL,
	LockboxKey BIGINT NOT NULL
);
--WFSScriptProcessorTableProperties
