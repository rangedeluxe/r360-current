--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName DocumentTypeNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.DocumentTypeNodes') IS NOT NULL
       DROP TABLE RecHubMigration.DocumentTypeNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/27/2016
*
* Purpose: Staging table for document types to extract/migration.
*		   
*
* Modification History
* 10/27/2016 PT 133205983 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.DocumentTypeNodes
(
	FileDescriptor VARCHAR(30) NOT NULL,
	DocumentTypeDescription VARCHAR(16) NOT NULL
);
--WFSScriptProcessorTableProperties
