--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName DocumentRemittanceDataNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.DocumentRemittanceDataNodes') IS NOT NULL
       DROP TABLE RecHubMigration.DocumentRemittanceDataNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Staging table for document data to extract/migration.
*		   
*
* Modification History
* 11/10/2016 PT  JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.DocumentRemittanceDataNodes
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Document_Id BIGINT NOT NULL,
	RemittanceDataRecord_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	FieldName VARCHAR(32) NOT NULL,
	FieldValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
