--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName ClientSetupsXML
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.ClientSetupsXML') IS NOT NULL
       DROP TABLE RecHubMigration.ClientSetupsXML
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 11/02/2016
*
* Purpose: Staging table for Client setup documets for migration of 1.04 to 2.02.
*		   
*
* Modification History
* 11/02/2016 PT 129630037 MGE	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.ClientSetupsXML
(
	SiteBankID INT NOT NULL,
	EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
	ClientSetupXML XML
);
--WFSScriptProcessorTableProperties
