--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName PaymentNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.PaymentNodes') IS NOT NULL
       DROP TABLE RecHubMigration.PaymentNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Staging table for transaction data to extract/migration.
*		   
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.PaymentNodes
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Payment_Id BIGINT NOT NULL,
	BatchSequence INT NOT NULL,
	Amount MONEY NOT NULL,
	RT VARCHAR(30) NOT NULL,
	Account VARCHAR(30) NOT NULL,
	Serial VARCHAR(30) NULL,
	TransactionCode VARCHAR(30) NULL,
	RemitterName VARCHAR(60) NULL,
	ABA		VARCHAR(10) NULL,
	DDA		VARCHAR(40)	NULL,
	CheckSequence INT NULL
);
--WFSScriptProcessorTableProperties
