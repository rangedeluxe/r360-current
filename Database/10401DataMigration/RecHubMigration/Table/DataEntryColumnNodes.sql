--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName DataEntryColumnNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.DataEntryColumnNodes') IS NOT NULL
       DROP TABLE RecHubMigration.DataEntryColumnNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/27/2016
*
* Purpose: Staging table for dataentry setups to extract/migration.
*		   
*
* Modification History
* 10/27/2016 PT 133205983 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.DataEntryColumnNodes
(
	DataEntryColumns_Id BIGINT NOT NULL,
	DataEntryColumnID INT NOT NULL,
	DataType SMALLINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	MarkSense TINYINT NOT NULL,
	DisplayGroup VARCHAR(36) NOT NULL,
	FieldName VARCHAR(32) NOT NULL,
	DisplayName VARCHAR(32) NOT NULL,
	BatchSource VARCHAR(64) NOT NULL,
	FieldLength TINYINT NOT NULL
);
--WFSScriptProcessorTableProperties
