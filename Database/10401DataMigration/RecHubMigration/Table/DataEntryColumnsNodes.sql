--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName DataEntryColumnsNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.DataEntryColumnsNodes') IS NOT NULL
       DROP TABLE RecHubMigration.DataEntryColumnsNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/27/2016
*
* Purpose: Staging table for dataentry setups to extract/migration.
*		   
*
* Modification History
* 10/27/2016 PT 133205983 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.DataEntryColumnsNodes
(
	DataEntryColumns_Id BIGINT NOT NULL IDENTITY(1,1),
	Lockbox_Id BIGINT NOT NULL,
	DataEntryColumnsID INT NOT NULL
);
--WFSScriptProcessorTableProperties
