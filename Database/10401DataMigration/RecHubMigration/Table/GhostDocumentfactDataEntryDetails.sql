--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName GhostDocumentfactDataEntryDetails
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.GhostDocumentfactDataEntryDetails') IS NOT NULL
       DROP TABLE RecHubMigration.GhostDocumentfactDataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/19/2017
*
* Purpose: Staging table for Ghost Document processing.
*		   
*
* Modification History
* 06/19/2017 PT 145212351 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.GhostDocumentfactDataEntryDetails
(
		Batch_Id BIGINT,
		Transaction_Id BIGINT,
		BatchSequence INT
);
--WFSScriptProcessorTableProperties
