--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName BatchNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.BatchNodes') IS NOT NULL
       DROP TABLE RecHubMigration.BatchNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/27/2016
*
* Purpose: Staging table for batch data to extract/migration.
*		   
*
* Modification History
* 11/10/2016 PT  JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.BatchNodes
(
	Batch_Id BIGINT NOT NULL,
	DepositDate DATETIME NOT NULL,
	BatchDate DATETIME NOT NULL,
	ProcessingDate DATETIME NOT NULL,
	BatchID BIGINT NOT NULL, /* Source Batch ID, not R360 Batch ID */
	BankID INT NOT NULL,
	ClientID INT NOT NULL,
	BatchNumber BIGINT NOT NULL,
	BatchSiteCode INT NULL,
	BatchSource VARCHAR(64) NOT NULL,
	PaymentType VARCHAR(30) NOT NULL,
	BatchCueID INT NOT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	DDA	VARCHAR(40)	NULL,
	ABA VARCHAR(10) NULL
);
--WFSScriptProcessorTableProperties
