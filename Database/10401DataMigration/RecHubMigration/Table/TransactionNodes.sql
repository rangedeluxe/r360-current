--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorTableName TransactionNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubMigration.TransactionNodes') IS NOT NULL
       DROP TABLE RecHubMigration.TransactionNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Staging table for transaction data to extract/migration.
*		   
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubMigration.TransactionNodes
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TransactionSequence INT NOT NULL
);
--WFSScriptProcessorTableProperties
