--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DocumentRemittanceData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_DocumentRemittanceData_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_DocumentRemittanceData_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_DocumentRemittanceData_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/14/2016
*
* Purpose: Retrieve Document data entry to extract.
*
* Modification History
* 11/14/2016 PT 132009883 JPB	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		RecHubMigration.DocumentNodes.Document_Id,
		DENSE_RANK() OVER
		(
			PARTITION BY RecHubMigration.ExtractBatches.Batch_Id
			ORDER BY RecHubMigration.ExtractBatches.Batch_Id,RecHubMigration.TransactionNodes.Transaction_Id,RecHubMigration.DocumentNodes.Document_Id,RecHubMigration.DocumentNodes.BatchSequence
		) AS RemittanceDataRecord_Id,
		RecHubMigration.TransactionNodes.TransactionID,
		RecHubMigration.DocumentNodes.BatchSequence,
		OLTA.dimDataEntryColumns.FldName AS FieldName,
		CASE OLTA.dimDataEntryColumns.DataType
			WHEN 6 THEN CAST(OLTA.factDataEntryDetails.DataEntryValueFloat AS VARCHAR(256))
			WHEN 7 THEN CAST(OLTA.factDataEntryDetails.DataEntryValueMoney AS VARCHAR(256))
			WHEN 11 THEN CONVERT(VARCHAR(256), OLTA.factDataEntryDetails.DataEntryValueDateTime, 21)
			ELSE OLTA.factDataEntryDetails.DataEntryValue
		END AS FieldValue
	FROM
		OLTA.factStubs
		INNER JOIN RecHubMigration.ExtractBatches ON RecHubMigration.ExtractBatches.DepositDateKey = OLTA.factStubs.DepositDateKey
			AND RecHubMigration.ExtractBatches.BatchDateKey = OLTA.factStubs.ProcessingDateKey
			AND RecHubMigration.ExtractBatches.ProcessingDateKey = OLTA.factStubs.SourceProcessingDateKey
			AND RecHubMigration.ExtractBatches.LockboxKey = OLTA.factStubs.LockboxKey
			AND RecHubMigration.ExtractBatches.BatchID = OLTA.factStubs.BatchID
			AND RecHubMigration.ExtractBatches.BatchSourceKey = OLTA.factStubs.BatchSourceKey
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
			AND RecHubMigration.TransactionNodes.TransactionID = OLTA.factStubs.TransactionID
		INNER JOIN RecHubMigration.DocumentNodes ON RecHubMigration.DocumentNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
			AND RecHubMigration.DocumentNodes.Transaction_Id = RecHubMigration.TransactionNodes.Transaction_Id
			AND RecHubMigration.DocumentNodes.BatchSequence = OLTA.factStubs.DocumentBatchSequence
		INNER JOIN OLTA.factDataEntryDetails ON OLTA.factDataEntryDetails.DepositDateKey = OLTA.factStubs.DepositDateKey
			AND OLTA.factDataEntryDetails.ProcessingDateKey = OLTA.factStubs.ProcessingDateKey
			AND OLTA.factDataEntryDetails.ProcessingDateKey = OLTA.factStubs.SourceProcessingDateKey
			AND OLTA.factDataEntryDetails.LockboxKey = OLTA.factStubs.LockboxKey
			AND OLTA.factDataEntryDetails.BatchID = OLTA.factStubs.BatchID
			AND OLTA.factDataEntryDetails.BatchSourceKey = OLTA.factStubs.BatchSourceKey
			AND OLTA.factDataEntryDetails.BatchSequence = OLTA.factStubs.BatchSequence
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
	WHERE
		OLTA.dimDataEntryColumns.TableType BETWEEN 2 AND 3;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
