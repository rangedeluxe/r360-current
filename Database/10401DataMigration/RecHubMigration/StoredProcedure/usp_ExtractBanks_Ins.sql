--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ExtractBanks_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_ExtractBanks_Ins') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_ExtractBanks_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_ExtractBanks_Ins
(
	@SiteBankIDList VARCHAR(MAX)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/28/2016
*
* Purpose: Retrieve bank keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 10/28/2016 PT #129630037 JBS	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @SiteBanktable TABLE (SiteBankIDList VARCHAR(MAX) )

--/*
	INSERT INTO @SiteBanktable
		(SiteBankIDList)
	VALUES 
		(@SiteBankIDList);

	INSERT INTO RecHubMigration.ExtractBanks
		(SiteBankID)
	SELECT 
		LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)'))) AS SiteBankID
	FROM
		(
			SELECT CAST('<XMLRoot><RowData>' + REPLACE(SiteBankIDList,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
			FROM   @SiteBanktable
		)t
		CROSS APPLY x.nodes('/XMLRoot/RowData')m(n);

--*/

/*
	---- I am curious if we think this method is clearer or easier to maintain
	 
	DECLARE @x INT = 0
	DECLARE @firstcomma INT = 0
	DECLARE @nextcomma INT = 0

	SET @x = LEN(@SiteBankIDList) - LEN(REPLACE(@SiteBankIDList, ',', '')) + 1 -- number of ids in id_list

	WHILE @x > 0
		BEGIN
			SET @nextcomma = CASE WHEN CHARINDEX(',', @SiteBankIDList, @firstcomma + 1) = 0
								  THEN LEN(@SiteBankIDList) + 1
								  ELSE CHARINDEX(',', @SiteBankIDList, @firstcomma + 1)
							 END
			IF ISNumeric(SUBSTRING(@SiteBankIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) ) = 1   -- To make sure we have a number
			BEGIN
				INSERT  INTO RecHubMigration.ExtractBanks
					(SiteBankID)
				VALUES  
					(SUBSTRING(@SiteBankIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) )
			END
			SET @firstcomma = CHARINDEX(',', @SiteBankIDList, @firstcomma + 1)
			SET @x = @x - 1
		END
*/


/*	 SQL 2016  just wanted to show how it easy this will be in 2016  just parse the variable directly
	
	INSERT INTO RecHubMigration.ExtractBanks
		(SiteBank_Id)
	select value 
	from STRING_SPLIT(@SiteBankIDList, ',')
	WHERE RTRIM(value) <> '';
*/

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		