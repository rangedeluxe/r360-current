--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_BatchesXML_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_BatchesXML_Ins') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_BatchesXML_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_BatchesXML_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 11/02/2016
*
* Purpose: Create XML document for each batch migrating from 1.04.01.xx to 2.02.00.
*
* Modification History
* 11/14/2016 PT #132009883 MGE	Created 
* 12/05/2016 PT #135281861 JPB	Added ISNULL to payment attributes
* 12/06/2016 PT #135280727 JPB	Added document remittance data node
* 06/12/2017 PT #145212351 JPB	Remove empty Document/GhostDocument elements, 
*					fixed attribute names.
* 06/12/2018 PT #158031090 MGE	Fixed the PaymentRemittanceData node	
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	DECLARE		@BatchCount INT,
						@Iteration INT = 1,
						@CurrentSiteBankID INT = 0;

	DECLARE		@XMLDocument XML ='';

	SELECT @BatchCount = Count(*) FROM RecHubMigration.BatchNodes;

	WHILE (@Iteration <= @BatchCount)
	BEGIN

		SELECT @XMLDocument =
			(
				SELECT
					CONVERT(VARCHAR(10), DepositDate, 21) AS '@DepositDate',
					CONVERT(VARCHAR(10), BatchDate, 21) AS '@BatchDate',
					CONVERT(VARCHAR(10), ProcessingDate, 21) AS '@ProcessingDate',
					BankID AS '@BankID',
					ClientID AS '@ClientID',
					RecHubMigration.BatchNodes.BatchID AS '@BatchID',
					BatchNumber AS '@BatchNumber',
					BatchSiteCode AS '@BatchSiteCode',
					BatchSource AS '@BatchSource',
					PaymentType AS '@PaymentType',
					BatchCueID AS '@BatchCueID',
					BatchTrackingID AS '@BatchTrackingID',
					RecHubMigration.BatchNodes.ABA AS '@ABA',
					RecHubMigration.BatchNodes.DDA AS '@DDA',
					(
						SELECT
							TransactionNodes.Transaction_Id AS '@Transaction_Id',
							TransactionNodes.TransactionID AS '@TransactionID',
							TransactionNodes.TransactionSequence AS '@TransactionSequence',
					(
								SELECT
									PaymentNodes.Transaction_Id AS '@Transaction_Id',
									Payment_Id AS '@Payment_Id',
									BatchSequence AS '@BatchSequence',
									PaymentNodes.Amount AS '@Amount',
									PaymentNodes.RT AS '@RT',					
									PaymentNodes.Account AS '@Account',						
									ISNULL(PaymentNodes.Serial,'') AS '@Serial',
									ISNULL(PaymentNodes.TransactionCode,'') AS '@TransactionCode',
									PaymentNodes.RemitterName AS '@RemitterName',
									PaymentNodes.ABA AS '@ABA',							
									PaymentNodes.DDA AS '@DDA',						
									(
										(
													SELECT
														PaymentRemittanceDataNodes.Payment_Id AS '@Payment_Id',
														PaymentRemittanceDataNodes.FieldName AS '@FieldName',
														PaymentRemittanceDataNodes.FieldValue AS '@FieldValue'
													FROM
														RecHubMigration.PaymentRemittanceDataNodes AS PaymentRemittanceDataNodes
													WHERE
														PaymentRemittanceDataNodes.Batch_Id = PaymentNodes.Batch_Id  
														AND PaymentRemittanceDataNodes.Transaction_Id = PaymentNodes.Transaction_Id
														AND PaymentRemittanceDataNodes.Payment_Id = PaymentNodes.Payment_Id
														AND TransactionNodes.Batch_Id = @Iteration
													ORDER BY
														PaymentRemittanceDataNodes.Transaction_Id,
														PaymentRemittanceDataNodes.Payment_Id
													FOR XML PATH('PaymentRemittanceData'), TYPE, ELEMENTS
										)
									)
									FROM
										RecHubMigration.PaymentNodes AS PaymentNodes
									WHERE
										PaymentNodes.Batch_Id = TransactionNodes.Batch_Id  
										AND PaymentNodes.Transaction_Id = TransactionNodes.Transaction_Id
										AND TransactionNodes.Batch_Id = @Iteration
									ORDER BY
										PaymentNodes.CheckSequence
									FOR XML PATH('Payment'), TYPE
							),
							(
								SELECT
									DocumentNodes.Transaction_Id AS '@Transaction_Id',
									DocumentNodes.Document_Id AS '@Document_Id',
									DocumentNodes.BatchSequence AS '@BatchSequence',
									DocumentNodes.DocumentSequence AS '@DocumentSequence',
									DocumentNodes.SequenceWithinTransaction AS '@SequenceWithinTransaction',
									DocumentNodes.DocumentDescriptor AS '@DocumentDescriptor',
									DocumentNodes.IsCorrespondence AS '@IsCorrespondence',
											(
												SELECT
													DocumentRemittanceDataNodes.Document_Id AS '@Document_Id',
													DocumentRemittanceDataNodes.RemittanceDataRecord_Id AS '@RemittanceDataRecord_Id',
													DocumentRemittanceDataNodes.BatchSequence AS '@BatchSequence',
													DocumentRemittanceDataNodes.FieldName AS '@FieldName',
													DocumentRemittanceDataNodes.FieldValue AS '@FieldValue'
												FROM
													RecHubMigration.DocumentRemittanceDataNodes AS DocumentRemittanceDataNodes
												WHERE
													DocumentRemittanceDataNodes.Batch_Id = DocumentNodes.Batch_Id
													AND DocumentRemittanceDataNodes.Transaction_Id = DocumentNodes.Transaction_Id
													AND DocumentRemittanceDataNodes.BatchSequence = DocumentNodes.BatchSequence
												ORDER BY
													DocumentRemittanceDataNodes.TransactionID,
													DocumentRemittanceDataNodes.BatchSequence
												FOR XML PATH('DocumentRemittanceData'), TYPE, ELEMENTS
									)
								FROM
									RecHubMigration.DocumentNodes AS DocumentNodes
								WHERE
									DocumentNodes.Batch_Id = @Iteration
									AND DocumentNodes.Transaction_Id = TransactionNodes.Transaction_Id
								ORDER BY
									DocumentNodes.DocumentSequence
								FOR XML PATH('Document'), TYPE
							),
							(
								SELECT
									GhostDocumentNodes.Transaction_Id AS '@Transaction_Id',
									GhostDocumentNodes.GhostDocument_Id AS '@GhostDocument_Id',
									GhostDocumentNodes.BatchSequence AS '@BatchSequence',
									'0' AS '@IsCorrespondence',
										(
											SELECT
												GhostDocumentRemittanceDataNodes.GhostDocument_Id AS '@GhostDocument_Id',
												GhostDocumentRemittanceDataNodes.RemittanceDataRecord_Id AS '@RemittanceDataRecord_Id',
												GhostDocumentRemittanceDataNodes.BatchSequence AS '@BatchSequence',
												GhostDocumentRemittanceDataNodes.FieldName AS '@FieldName',
												GhostDocumentRemittanceDataNodes.FieldValue AS '@FieldValue'
											FROM
													RecHubMigration.GhostDocumentRemittanceDataNodes AS GhostDocumentRemittanceDataNodes
											WHERE
													GhostDocumentRemittanceDataNodes.Batch_Id = GhostDocumentNodes.Batch_Id
													AND GhostDocumentRemittanceDataNodes.Transaction_Id = GhostDocumentNodes.Transaction_Id
													AND GhostDocumentRemittanceDataNodes.BatchSequence = GhostDocumentNodes.BatchSequence
											ORDER BY
														GhostDocumentRemittanceDataNodes.TransactionID,
														GhostDocumentRemittanceDataNodes.BatchSequence
											FOR XML PATH('GhostDocumentRemittanceData'), TYPE, ELEMENTS
										)
								FROM
									RecHubMigration.GhostDocumentNodes AS GhostDocumentNodes
								WHERE
									GhostDocumentNodes.Batch_Id = @Iteration
									AND GhostDocumentNodes.Transaction_Id = TransactionNodes.Transaction_Id
								ORDER BY
									GhostDocumentNodes.GhostDocument_Id
								FOR XML PATH('GhostDocument'), TYPE
							)
						FROM
								RecHubMigration.TransactionNodes AS TransactionNodes
								INNER JOIN RecHubMigration.BatchNodes ON RecHubMigration.BatchNodes.Batch_Id = TransactionNodes.Batch_Id
						WHERE
								TransactionNodes.Batch_Id = BatchNodes.Batch_Id
								AND RecHubMigration.BatchNodes.Batch_Id = @Iteration
						ORDER BY 
								Transaction_Id,
								TransactionSequence
								FOR XML PATH('Transaction'), TYPE
					)
				FROM
					RecHubMigration.ExtractBatches
					INNER JOIN RecHubMigration.BatchNodes ON RecHubMigration.BatchNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
				WHERE
					RecHubMigration.BatchNodes.Batch_Id = @Iteration
				FOR XML PATH('Batch'), TYPE
			)

			INSERT INTO RecHubMigration.BatchesXML
			(SiteLockboxID, ProcessingDate, BatchID, BatchTrackingID, BatchXML)
				(SELECT ClientID AS SiteLockboxID, ProcessingDate, BatchID, BatchTrackingID, @XMLDocument
				FROM RecHubMigration.BatchNodes
				WHERE Batch_Id = @Iteration);
	
		SET @Iteration = @Iteration +1;
		SET @XMLDocument ='';
	END

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH