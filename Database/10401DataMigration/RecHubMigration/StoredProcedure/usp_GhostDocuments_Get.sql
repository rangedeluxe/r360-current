--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_GhostDocuments_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_GhostDocuments_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_GhostDocuments_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_GhostDocuments_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/14/2016
*
* Purpose: Retrieve transctions that have data entry, but not a document. This will 
*	create a ghost document for data entry to extract.
*
* Modification History
* 11/14/2016 PT 132009883 JPB	Created 
* 06/12/2017 PT 145212351 JPB	Update for performance and to get the correct stub records
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubMigration.GhostDocumentfactDataEntryDetails.Batch_Id,
		RecHubMigration.GhostDocumentfactDataEntryDetails.Transaction_Id,
		ROW_NUMBER() OVER 
			(
				PARTITION BY RecHubMigration.GhostDocumentfactDataEntryDetails.Batch_Id
				ORDER BY RecHubMigration.GhostDocumentfactDataEntryDetails.Batch_Id,RecHubMigration.GhostDocumentfactDataEntryDetails.Transaction_Id
			) AS GhostDocument_Id,
		RecHubMigration.GhostDocumentfactDataEntryDetails.BatchSequence
	FROM
		RecHubMigration.GhostDocumentfactStubs
		INNER JOIN RecHubMigration.GhostDocumentfactDataEntryDetails ON RecHubMigration.GhostDocumentfactDataEntryDetails.Batch_Id = RecHubMigration.GhostDocumentfactStubs.Batch_Id
			AND RecHubMigration.GhostDocumentfactDataEntryDetails.Transaction_Id = RecHubMigration.GhostDocumentfactStubs.Transaction_Id
			AND RecHubMigration.GhostDocumentfactDataEntryDetails.BatchSequence = RecHubMigration.GhostDocumentfactStubs.BatchSequence
	GROUP BY
		RecHubMigration.GhostDocumentfactDataEntryDetails.Batch_Id,
		RecHubMigration.GhostDocumentfactDataEntryDetails.Transaction_Id,
		RecHubMigration.GhostDocumentfactDataEntryDetails.BatchSequence;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		



