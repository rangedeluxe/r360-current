--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ExtractLockboxBatches_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_ExtractLockboxBatches_Ins') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_ExtractLockboxBatches_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_ExtractLockboxBatches_Ins
(
	@parmBankID  INT,
	@parmLockboxIDs VARCHAR(MAX) 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/28/2016
*
* Purpose: Retrieve bank keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 11/10/2016 PT #132009883 JBS	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	--/*
	IF @parmLockboxIDs = 'ALL'
	BEGIN
		INSERT INTO RecHubMigration.ExtractLockboxBatches
			(SiteBankID,
			SiteLockboxID)
		SELECT 
			SiteBankID,
			SiteLockBoxID
		FROM
			OLTA.dimLockboxes
		WHERE 
			SiteBankID = @parmBankID
			AND IsActive = 1
			AND MostRecent = 1;
	END
	ELSE
	BEGIN
		DECLARE @Lockboxtable TABLE (LockboxList VARCHAR(MAX) )

		INSERT INTO @Lockboxtable
			(LockboxList)
		VALUES 
			(@parmLockboxIDs);

		INSERT INTO RecHubMigration.ExtractLockboxBatches
			(SiteBankID,
			SiteLockboxID)
		SELECT 
			@parmBankID,
			--LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)')))  -- the raw value if needed
			CASE 
				WHEN ISNUMERIC(LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)')))) = 1 THEN LTRIM(RTRIM(m.n.value('.[1]','varchar(8000)')))
				ELSE -1
			END
			 AS SiteLockBoxID
		FROM
			(
				SELECT CAST('<XMLRoot><RowData>' + REPLACE(LockboxList,',','</RowData><RowData>') + '</RowData></XMLRoot>' AS XML) AS x
				FROM   @Lockboxtable
			)t
			CROSS APPLY x.nodes('/XMLRoot/RowData')m(n);
	END

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		