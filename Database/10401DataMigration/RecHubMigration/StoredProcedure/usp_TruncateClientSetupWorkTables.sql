--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateClientSetupWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_TruncateClientSetupWorkTables') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_TruncateClientSetupWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_TruncateClientSetupWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/28/2016
*
* Purpose: Truncate tables used by SSIS for importing Client Setup.
*			This is for Conversion process from 1.04-2.02
*
* Modification History
* 10/28/2016 PT #129630037 JBS	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	TRUNCATE TABLE RecHubMigration.ExtractBanks;
	TRUNCATE TABLE RecHubMigration.BankNodes;
	TRUNCATE TABLE RecHubMigration.LockboxNodes; 
	TRUNCATE TABLE RecHubMigration.DataEntryColumnsNodes; 
	TRUNCATE TABLE RecHubMigration.DataEntryColumnNodes; 
	TRUNCATE TABLE RecHubMigration.DocumentTypeNodes; 
	TRUNCATE TABLE RecHubMigration.SiteCodeNodes; 
	TRUNCATE TABLE RecHubMigration.ClientSetupsXML;
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH
