--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Batches_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_Batches_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_Batches_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_Batches_Get
(
	@parmStartDate DATETIME,
	@parmEndDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Retrieve batches to extract.
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created 
* 11/23/2016 PT 132009883 JBS	Added -1 default for BatchSiteCode if it is NULL 
*								OR no matching row in dimSiteCodes
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

DECLARE @StartDateKey INT,
		@EndDateKey INT;

SELECT 
	@StartDateKey = CAST(CONVERT(VARCHAR, @parmStartDate, 112) AS INT),
	@EndDateKey = CAST(CONVERT(VARCHAR, @parmEndDate, 112) AS INT);


SELECT
	ROW_NUMBER() OVER (ORDER BY OLTA.factBatchSummary.DepositDateKey,OLTA.factBatchSummary.LockboxKey,OLTA.factBatchSummary.BatchID) AS Batch_Id,
	CONVERT(DATETIME,CONVERT(CHAR(8),OLTA.factBatchSummary.DepositDateKey)) AS DepositDate,
	CONVERT(DATETIME,CONVERT(CHAR(8),OLTA.factBatchSummary.ProcessingDateKey)) AS BatchDate,
	CONVERT(DATETIME,CONVERT(CHAR(8),OLTA.factBatchSummary.SourceProcessingDateKey)) AS ProcessingDate,
	OLTA.factBatchSummary.BatchID,
	OLTA.dimLockboxes.SiteBankID AS BankID,
	OLTA.dimLockboxes.SiteLockboxID AS ClientID,
	OLTA.factBatchSummary.BatchID AS BatchNumber,
	COALESCE(OLTA.dimSiteCodes.SiteCodeID, -1) AS BatchSiteCode,
	OLTA.dimBatchSources.ShortName + '-I' + CAST(OLTA.dimLockboxes.SiteBankID AS VARCHAR(10)) AS BatchSource,
	OLTA.dimBatchPaymentTypes.ShortName AS PaymentType,
	OLTA.factBatchSummary.BatchCueID,
	NEWID() AS BatchTrackingID,
	CASE 
		WHEN LEN(OLTA.factBatchSummary.DepositDDA) > 0 THEN OLTA.factBatchSummary.DepositDDA
		ELSE OLTA.dimLockboxes.DDA
	END AS DDA,
	OLTA.dimBanks.ABA,
	OLTA.factBatchSummary.DepositDateKey,
	OLTA.factBatchSummary.ProcessingDateKey AS BatchDateKey,
	OLTA.factBatchSummary.SourceProcessingDateKey AS ProcessingDateKey,
	OLTA.factBatchSummary.LockboxKey,
	OLTA.factBatchSummary.BatchSourceKey
FROM 
	OLTA.factBatchSummary
	INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.factBatchSummary.LockboxKey
	INNER JOIN RecHubMigration.ExtractLockboxBatches ON RecHubMigration.ExtractLockboxBatches.SiteBankID = OLTA.dimLockboxes.SiteBankID
		AND RecHubMigration.ExtractLockboxBatches.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
	INNER JOIN OLTA.dimBatchSources ON OLTA.dimBatchSources.BatchSourceKey = OLTA.factBatchSummary.BatchSourceKey
	INNER JOIN OLTA.dimBatchPaymentTypes ON OLTA.dimBatchPaymentTypes.BatchPaymentTypeKey = OLTA.factBatchSummary.BatchPaymentTypeKey
	INNER JOIN OLTA.dimBanks ON OLTA.dimBanks.BankKey = OLTA.factBatchSummary.BankKey
	LEFT OUTER JOIN OLTA.dimSiteCodes ON OLTA.factBatchSummary.BatchSiteCode = OLTA.dimSiteCodes.SiteCodeID
	WHERE
		DepositDateKey BETWEEN @StartDateKey AND @EndDateKey

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		