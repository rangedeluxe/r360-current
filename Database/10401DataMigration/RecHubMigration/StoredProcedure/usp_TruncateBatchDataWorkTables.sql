--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateBatchDataWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_TruncateBatchDataWorkTables') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_TruncateBatchDataWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_TruncateBatchDataWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 11/10/2016
*
* Purpose: Truncate tables used by SSIS for importing Batch Data.
*			This is for Conversion process from 1.04-2.02
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created
* 06/19/2017 PT 145212351 JPB	Added GhostDocument staging tables
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	TRUNCATE TABLE RecHubMigration.ExtractLockboxBatches;
	TRUNCATE TABLE RecHubMigration.ExtractBatches;
	TRUNCATE TABLE RecHubMigration.BatchNodes;
	TRUNCATE TABLE RecHubMigration.TransactionNodes;
	TRUNCATE TABLE RecHubMigration.PaymentNodes;
	TRUNCATE TABLE RecHubMigration.BatchesXML;
	TRUNCATE TABLE RecHubMigration.DocumentNodes;
	TRUNCATE TABLE RecHubMigration.DocumentRemittanceDataNodes;
	TRUNCATE TABLE RecHubMigration.GhostDocumentfactDataEntryDetails;
	TRUNCATE TABLE RecHubMigration.GhostDocumentfactStubs;
	TRUNCATE TABLE RecHubMigration.GhostDocumentNodes;
	TRUNCATE TABLE RecHubMigration.GhostDocumentRemittanceDataNodes;
	TRUNCATE TABLE RecHubMigration.PaymentRemittanceDataNodes;
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH
