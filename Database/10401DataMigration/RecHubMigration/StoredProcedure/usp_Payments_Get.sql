--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Payments_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_Payments_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_Payments_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_Payments_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Retrieve Payments (i.e. factChecks) to extract.
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		ROW_NUMBER() OVER 
			(
				PARTITION BY RecHubMigration.ExtractBatches.Batch_Id
				ORDER BY RecHubMigration.ExtractBatches.Batch_Id,RecHubMigration.TransactionNodes.Transaction_Id
			) AS Payment_Id,
		OLTA.factChecks.BatchSequence,
		OLTA.factChecks.Amount,
		OLTA.dimRemitters.RoutingNumber AS RT,
		OLTA.dimRemitters.Account,
		OLTA.factChecks.Serial,
		OLTA.factChecks.TransactionCode,
		OLTA.dimRemitters.RemitterName,
		RecHubMigration.ExtractBatches.ABA,
		RecHubMigration.ExtractBatches.DDA,
		OLTA.factChecks.CheckSequence
	FROM
		OLTA.factChecks
		INNER JOIN RecHubMigration.ExtractBatches ON RecHubMigration.ExtractBatches.DepositDateKey = OLTA.factChecks.DepositDateKey
			AND RecHubMigration.ExtractBatches.BatchDateKey = OLTA.factChecks.ProcessingDateKey
			AND RecHubMigration.ExtractBatches.ProcessingDateKey = OLTA.factChecks.SourceProcessingDateKey
			AND RecHubMigration.ExtractBatches.LockboxKey = OLTA.factChecks.LockboxKey
			AND RecHubMigration.ExtractBatches.BatchID = OLTA.factChecks.BatchID
			AND RecHubMigration.ExtractBatches.BatchSourceKey = OLTA.factChecks.BatchSourceKey
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
			AND RecHubMigration.TransactionNodes.TransactionID = OLTA.factChecks.TransactionID
		INNER JOIN OLTA.dimRemitters ON OLTA.dimRemitters.RemitterKey = OLTA.factChecks.RemitterKey

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		