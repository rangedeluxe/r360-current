--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_PaymentsRemittanceData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_PaymentsRemittanceData_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_PaymentsRemittanceData_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_PaymentsRemittanceData_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/14/2016
*
* Purpose: Retrieve check data entry to extract.
*
* Modification History
* 11/14/2016 PT 132009883 JPB	Created 
* 06/11/2018 PT 158031090 MGE	Change to select FldName instead of DisplayName
*********************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		RecHubMigration.PaymentNodes.Payment_Id,
		OLTA.dimDataEntryColumns.FldName AS FieldName,
		CASE OLTA.dimDataEntryColumns.DataType
			WHEN 6 THEN CAST(OLTA.factDataEntryDetails.DataEntryValueFloat AS VARCHAR(256))
			WHEN 7 THEN CAST(OLTA.factDataEntryDetails.DataEntryValueMoney AS VARCHAR(256))
			WHEN 11 THEN CONVERT(VARCHAR(256), OLTA.factDataEntryDetails.DataEntryValueDateTime, 21)
			ELSE OLTA.factDataEntryDetails.DataEntryValue
		END AS FieldValue
	FROM
		OLTA.factDataEntryDetails
		INNER JOIN RecHubMigration.ExtractBatches ON RecHubMigration.ExtractBatches.DepositDateKey = OLTA.factDataEntryDetails.DepositDateKey
			AND RecHubMigration.ExtractBatches.BatchDateKey = OLTA.factDataEntryDetails.ProcessingDateKey
			AND RecHubMigration.ExtractBatches.ProcessingDateKey = OLTA.factDataEntryDetails.SourceProcessingDateKey
			AND RecHubMigration.ExtractBatches.LockboxKey = OLTA.factDataEntryDetails.LockboxKey
			AND RecHubMigration.ExtractBatches.BatchID = OLTA.factDataEntryDetails.BatchID
			AND RecHubMigration.ExtractBatches.BatchSourceKey = OLTA.factDataEntryDetails.BatchSourceKey
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
			AND RecHubMigration.TransactionNodes.TransactionID = OLTA.factDataEntryDetails.TransactionID
		INNER JOIN RecHubMigration.PaymentNodes ON RecHubMigration.PaymentNodes.Batch_Id = RecHubMigration.TransactionNodes.Batch_Id
			AND RecHubMigration.PaymentNodes.Transaction_Id = RecHubMigration.TransactionNodes.Transaction_Id
			AND RecHubMigration.PaymentNodes.BatchSequence = OLTA.factDataEntryDetails.BatchSequence
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
	WHERE
		OLTA.dimDataEntryColumns.TableType BETWEEN 0 AND 1;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		