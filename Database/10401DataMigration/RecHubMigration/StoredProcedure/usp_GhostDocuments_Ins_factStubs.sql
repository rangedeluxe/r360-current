--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_GhostDocuments_Ins_factStubs
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_GhostDocuments_Ins_factStubs') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_GhostDocuments_Ins_factStubs
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_GhostDocuments_Ins_factStubs
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/19/2017
*
* Purpose: Retrieve transctions that have data entry, but not a document. This will 
*	create a ghost document for data entry to extract.
*
* Modification History
* 06/19/2017 PT 145212351 JPB	Created
* 05/16/2018 PT 157565095 MGE	Updated to catch ghost documents for OLTA stubs not present 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	INSERT INTO GhostDocumentfactStubs(Batch_Id,Transaction_Id,BatchSequence)
	SELECT 
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		OLTA.factStubs.BatchSequence
	FROM 
		RecHubMigration.ExtractBatches
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
		INNER JOIN OLTA.factStubs ON OLTA.factStubs.DepositDateKey = RecHubMigration.ExtractBatches.DepositDateKey
				AND OLTA.factStubs.ProcessingDateKey = RecHubMigration.ExtractBatches.BatchDateKey
				AND OLTA.factStubs.SourceProcessingDateKey = RecHubMigration.ExtractBatches.ProcessingDateKey
				AND OLTA.factStubs.LockboxKey = RecHubMigration.ExtractBatches.LockboxKey
				AND OLTA.factStubs.BatchID = RecHubMigration.ExtractBatches.BatchID
				AND OLTA.factStubs.BatchSourceKey = RecHubMigration.ExtractBatches.BatchSourceKey
				AND OLTA.factStubs.TransactionID = RecHubMigration.TransactionNodes.TransactionID
	WHERE
		OLTA.factStubs.DocumentBatchSequence IS NULL
	UNION
	SELECT 
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		factDataEntryDetails.BatchSequence
	FROM 
		RecHubMigration.ExtractBatches
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
		INNER JOIN 
			(SELECT DISTINCT DepositDateKey, ProcessingDateKey, SourceProcessingDateKey, LockboxKey, BatchID, BatchSourceKey, TransactionID, BatchSequence, DataEntryColumnKey
				 FROM OLTA.factDataEntryDetails) AS factDataEntryDetails
				ON	factDataEntryDetails.DepositDateKey = RecHubMigration.ExtractBatches.DepositDateKey
				AND factDataEntryDetails.ProcessingDateKey = RecHubMigration.ExtractBatches.BatchDateKey
				AND factDataEntryDetails.SourceProcessingDateKey = RecHubMigration.ExtractBatches.ProcessingDateKey
				AND factDataEntryDetails.LockboxKey = RecHubMigration.ExtractBatches.LockboxKey
				AND factDataEntryDetails.BatchID = RecHubMigration.ExtractBatches.BatchID
				AND factDataEntryDetails.BatchSourceKey = RecHubMigration.ExtractBatches.BatchSourceKey
				AND factDataEntryDetails.TransactionID = RecHubMigration.TransactionNodes.TransactionID
		INNER JOIN OLTA.dimDataEntryColumns ON factDataEntryDetails.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
		LEFT JOIN OLTA.factStubs ON OLTA.factStubs.DepositDateKey = RecHubMigration.ExtractBatches.DepositDateKey
				AND OLTA.factStubs.ProcessingDateKey = RecHubMigration.ExtractBatches.BatchDateKey
				AND OLTA.factStubs.SourceProcessingDateKey = RecHubMigration.ExtractBatches.ProcessingDateKey
				AND OLTA.factStubs.LockboxKey = RecHubMigration.ExtractBatches.LockboxKey
				AND OLTA.factStubs.BatchID = RecHubMigration.ExtractBatches.BatchID
				AND OLTA.factStubs.BatchSourceKey = RecHubMigration.ExtractBatches.BatchSourceKey
				AND OLTA.factStubs.TransactionID = RecHubMigration.TransactionNodes.TransactionID
	WHERE
		OLTA.factStubs.DocumentBatchSequence IS NULL
		AND OLTA.factStubs.BatchSequence IS NULL
		AND OLTA.dimDataEntryColumns.TableType > 1

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		



