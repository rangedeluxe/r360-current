--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Documents_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_Documents_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_Documents_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_Documents_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 11/16/2016
*
* Purpose: Retrieve Documents to extract.
*
* Modification History
* 11/16/2016 PT 132009883 MGE	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		ROW_NUMBER() OVER 
			(
				PARTITION BY RecHubMigration.ExtractBatches.Batch_Id
				ORDER BY RecHubMigration.ExtractBatches.Batch_Id,RecHubMigration.TransactionNodes.Transaction_Id
			) AS Document_Id,
		OLTA.factDocuments.BatchSequence,
		OLTA.factDocuments.DocumentSequence,
		OLTA.factDocuments.SequenceWithinTransaction,
		OLTA.dimDocumentTypes.FileDescriptor AS DocumentDescriptor,
		0 AS IsCorrespondence
	FROM
		OLTA.factDocuments
		INNER JOIN RecHubMigration.ExtractBatches ON RecHubMigration.ExtractBatches.DepositDateKey = OLTA.factDocuments.DepositDateKey
			AND RecHubMigration.ExtractBatches.BatchDateKey = OLTA.factDocuments.ProcessingDateKey
			AND RecHubMigration.ExtractBatches.ProcessingDateKey = OLTA.factDocuments.SourceProcessingDateKey
			AND RecHubMigration.ExtractBatches.LockboxKey = OLTA.factDocuments.LockboxKey
			AND RecHubMigration.ExtractBatches.BatchID = OLTA.factDocuments.BatchID
			AND RecHubMigration.ExtractBatches.BatchSourceKey = OLTA.factDocuments.BatchSourceKey
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
			AND RecHubMigration.TransactionNodes.TransactionID = OLTA.factDocuments.TransactionID
		INNER JOIN OLTA.dimDocumentTypes ON OLTA.dimDocumentTypes.DocumentTypeKey = OLTA.factDocuments.DocumentTypeKey
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH