--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataEntryColumns_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_DataEntryColumns_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_DataEntryColumns_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_DataEntryColumns_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/14/2016
*
* Purpose: Retrieve data entry columns for migration.
*
* Modification History
* 11/16/2016 PT 133205983  JPB	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	;WITH DEDataTypes AS
	(
		SELECT
			RecHubMigration.DataEntryColumnsNodes.DataEntryColumns_Id,
			OLTA.dimDataEntryColumns.DataType,
			OLTA.dimDataEntryColumns.TableName AS DisplayGroup,
			OLTA.dimDataEntryColumns.FldName AS FieldName,
			OLTA.dimDataEntryColumns.DisplayName
		FROM
			OLTA.dimDataEntryColumns
			INNER JOIN OLTA.LockboxesDataEntryColumns ON OLTA.LockboxesDataEntryColumns.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
			INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.LockboxesDataEntryColumns.LockboxKey
			INNER JOIN RecHubMigration.LockboxNodes ON RecHubMigration.LockboxNodes.SiteBankID = OLTA.dimLockboxes.SiteBankID
				AND RecHubMigration.LockboxNodes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN RecHubMigration.DataEntryColumnsNodes ON RecHubMigration.DataEntryColumnsNodes.Lockbox_Id = RecHubMigration.LockboxNodes.Lockbox_Id
		GROUP BY
			RecHubMigration.DataEntryColumnsNodes.DataEntryColumns_Id,
			OLTA.dimDataEntryColumns.DataType,
			OLTA.dimDataEntryColumns.TableName,
			OLTA.dimDataEntryColumns.FldName,
			OLTA.dimDataEntryColumns.DisplayName
	),
	DEColumns AS 
	(	
		SELECT
			RecHubMigration.DataEntryColumnsNodes.DataEntryColumns_Id,
			MIN(OLTA.dimDataEntryColumns.ScreenOrder) AS ScreenOrder,
			MAX(OLTA.dimDataEntryColumns.MarkSense) AS MarkSense,
			OLTA.dimDataEntryColumns.TableName AS DisplayGroup,
			OLTA.dimDataEntryColumns.FldName AS FieldName,
			OLTA.dimDataEntryColumns.DisplayName,
			MAX(OLTA.dimDataEntryColumns.FldLength) AS FieldLength
		FROM
			OLTA.dimDataEntryColumns
			INNER JOIN OLTA.LockboxesDataEntryColumns ON OLTA.LockboxesDataEntryColumns.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
			INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.LockboxesDataEntryColumns.LockboxKey
			INNER JOIN RecHubMigration.LockboxNodes ON RecHubMigration.LockboxNodes.SiteBankID = OLTA.dimLockboxes.SiteBankID
				AND RecHubMigration.LockboxNodes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN RecHubMigration.DataEntryColumnsNodes ON RecHubMigration.DataEntryColumnsNodes.Lockbox_Id = RecHubMigration.LockboxNodes.Lockbox_Id
		GROUP BY
			RecHubMigration.DataEntryColumnsNodes.DataEntryColumns_Id,
			OLTA.dimDataEntryColumns.TableName,
			OLTA.dimDataEntryColumns.FldName,
			OLTA.dimDataEntryColumns.DisplayName
	)
	SELECT
		DEColumns.DataEntryColumns_Id,
		ROW_NUMBER() OVER (PARTITION BY DEColumns.DataEntryColumns_Id ORDER BY DEColumns.DataEntryColumns_Id) AS DataEntryColumnID,
		DataType,
		ScreenOrder,
		MarkSense,
		DEColumns.DisplayGroup,
		DEColumns.FieldName,
		DEColumns.DisplayName,
		CAST(BatchSource.ShortName+'-I'+CAST(RecHubMigration.LockboxNodes.SiteBankID AS VARCHAR(10)) AS VARCHAR(64)) AS BatchSource,
		BatchSource.ShortName,
		FieldLength
	FROM
		DEColumns
		INNER JOIN DEDataTypes ON DEDataTypes.DataEntryColumns_Id = DEColumns.DataEntryColumns_Id
			AND DEDataTypes.DisplayGroup = DEColumns.DisplayGroup
			AND DEDataTypes.FieldName = DEColumns.FieldName
			AND DEDataTypes.DisplayName = DEColumns.DisplayName
		INNER JOIN RecHubMigration.DataEntryColumnsNodes ON RecHubMigration.DataEntryColumnsNodes.DataEntryColumns_Id = DEColumns.DataEntryColumns_Id
		INNER JOIN RecHubMigration.LockboxNodes ON RecHubMigration.LockboxNodes.Lockbox_Id = RecHubMigration.DataEntryColumnsNodes.Lockbox_Id
		CROSS APPLY (SELECT ShortName FROM OLTA.dimBatchSources WHERE BatchSourceKey BETWEEN 1 AND 3) AS BatchSource;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		