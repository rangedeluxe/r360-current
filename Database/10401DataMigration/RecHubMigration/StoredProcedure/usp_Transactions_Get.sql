--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Transactions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_Transactions_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_Transactions_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_Transactions_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/10/2016
*
* Purpose: Retrieve batches to extract.
*
* Modification History
* 11/10/2016 PT 133207169 JPB	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubMigration.ExtractBatches.Batch_Id,
		ROW_NUMBER() OVER 
			(
				PARTITION BY RecHubMigration.ExtractBatches.Batch_Id
				ORDER BY RecHubMigration.ExtractBatches.Batch_Id,OLTA.factTransactionSummary.TransactionID,OLTA.factTransactionSummary.TxnSequence
			) AS Transaction_Id,
		OLTA.factTransactionSummary.TransactionID,
		OLTA.factTransactionSummary.TxnSequence AS TransactionSequence
	FROM
		OLTA.factTransactionSummary
		INNER JOIN RecHubMigration.ExtractBatches ON RecHubMigration.ExtractBatches.DepositDateKey = OLTA.factTransactionSummary.DepositDateKey
			AND RecHubMigration.ExtractBatches.BatchDateKey = OLTA.factTransactionSummary.ProcessingDateKey
			AND RecHubMigration.ExtractBatches.ProcessingDateKey = OLTA.factTransactionSummary.SourceProcessingDateKey
			AND RecHubMigration.ExtractBatches.LockboxKey = OLTA.factTransactionSummary.LockboxKey
			AND RecHubMigration.ExtractBatches.BatchID = OLTA.factTransactionSummary.BatchID
			AND RecHubMigration.ExtractBatches.BatchSourceKey = OLTA.factTransactionSummary.BatchSourceKey;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		