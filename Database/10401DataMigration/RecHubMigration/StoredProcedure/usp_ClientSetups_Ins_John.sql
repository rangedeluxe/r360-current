--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ClientSetups_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_ClientSetups_Ins') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_ClientSetups_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_ClientSetups_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 11/02/2016
*
* Purpose: Create XML document for migrating from 1.04.01.xx to 2.02.00.
*
* Modification History
* 11/02/2016 PT #129630037 MGE	Created 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
DECLARE		@BankCount INT,
			@EntityTrackingID UNIQUEIDENTIFIER,
			@Iteration INT = 0,
			@CurrentSiteBankID INT = 0,
			@BankXML XML = '',
			@DocumentTypesXML XML = '',
			@SiteXML XML = '',
			@XMLDocument XML ='';


DECLARE @CurrentSiteLockboxID INT;

IF EXISTS (SELECT 1 FROM RecHubMigration.SiteCodeNodes )
BEGIN
	SELECT @SiteXML = 
		(
			SELECT
				SiteCodeID AS '@SiteCode',
				ShortName AS '@ShortName',
				LongName AS '@LongName',
				LocalTimeZoneBias AS '@LocalTimeZoneBias',
				AutoUpdateCurrentProcessingDate AS '@AutoUpdateCurrentProcessingDate'
			FROM
				RecHubMigration.SiteCodeNodes
			FOR XML PATH('Site')
		)
END

IF EXISTS( SELECT 1 FROM RecHubMigration.DocumentTypeNodes )
BEGIN
	SELECT @DocumentTypesXML = 
		(
			SELECT 
				FileDescriptor AS '@FileDescriptor',
				CASE
					WHEN DocumentTypeDescription='' THEN 'Undefined'
					ELSE DocumentTypeDescription
				END AS '@DocumentTypeDescription'
			FROM
				RecHubMigration.DocumentTypeNodes
			FOR XML PATH('DocumentTypes')
		)
END

SELECT @BankCount = Count(*) FROM RecHubMigration.LockboxNodes;

WHILE (@Iteration < @BankCount)
	BEGIN
	SELECT @CurrentSiteBankID = SiteBankID,@CurrentSiteLockboxID = SiteLockboxID FROM RecHubMigration.LockboxNodes WHERE Lockbox_Id = @Iteration + 1	

	SELECT @EntityTrackingID = NEWID();

	SELECT @BankXML = 
			(
			SELECT
			LockboxNodes.SiteBankID AS '@BankID',
			BankName AS '@BankName',
			ABA AS '@ABA',
			(SELECT
			(
				SELECT
					Lockbox_Id AS '@Client_Id',
					SiteLockboxID AS '@ClientID',
					SiteCode AS '@SiteCode',
					ShortName AS '@ShortName',
					LongName AS '@LongName',   
					OnlineColorMode AS '@OnlineColorMode',
					DataRetentionDays AS '@DataRetentionDays',
					ImageRetentionDays AS '@ImageRetentionDays',
					POBox AS '@POBox',
					DDA AS '@DDA',
					( 
					SELECT
						RecHubMigration.DataEntryColumnsNodes.Lockbox_Id AS '@Client_Id',							
						DataEntryColumns_Id AS '@DataEntryColumns_Id',
						DataEntryColumnsID AS '@DataEntryColumnsID',
						(
						SELECT
							RecHubMigration.DataEntryColumnNodes.DataEntryColumns_Id AS '@DataEntryColumns_Id',
							DataEntryColumnID AS '@DataEntryColumnID',
							DataType AS '@DataType',
							ScreenOrder AS '@ScreenOrder',
							MarkSense AS '@MarkSense',
							DisplayGroup AS '@DisplayGroup',
							FieldName AS '@FieldName',
							DisplayName AS '@DisplayName',
							BatchSource AS '@BatchSource',
							FieldLength AS '@FieldLength'
						FROM
							RecHubMigration.DataEntryColumnNodes
						WHERE
							DataEntryColumnsNodes.DataEntryColumns_Id = RecHubMigration.DataEntryColumnNodes.DataEntryColumns_Id
						FOR XML PATH('DataEntryColumn'), TYPE
						)
					FROM 
						RecHubMigration.DataEntryColumnsNodes
					WHERE 
						LockboxNodes.Lockbox_Id = RecHubMigration.DataEntryColumnsNodes.Lockbox_Id
					FOR XML PATH('DataEntryColumns'), TYPE
			)
			FROM 
				RecHubMigration.LockboxNodes AS LockboxNodes
			WHERE 
				BankNodes.SiteBankID =  LockboxNodes.SiteBankID
				AND LockboxNodes.SiteLockboxID = @CurrentSiteLockboxID
			FOR XML PATH('Client'), TYPE
			))
		FROM 
		RecHubMigration.BankNodes AS BankNodes
			INNER JOIN RecHubMigration.LockboxNodes ON RecHubMigration.LockboxNodes.SiteBankID = BankNodes.SiteBankID
		WHERE BankNodes.SiteBankID = @CurrentSiteBankID
			AND RecHubMigration.LockboxNodes.SiteLockboxID = @CurrentSiteLockboxID
		FOR XML PATH('Bank'), TYPE
		)
	
	SELECT @XMLDocument = 
		(
		SELECT
		@EntityTrackingID AS '@ClientTrackingID',
		'0' AS '@ClientGroupID',
		@BankXML,
		@DocumentTypesXML,
		@SiteXML
	FOR XML PATH('ClientGroup'), TYPE);

	INSERT INTO RecHubMigration.ClientSetupsXML(SiteBankID,EntityTrackingID, ClientSetupXML)
	VALUES (@CurrentSiteBankID, @EntityTrackingID, @XMLDocument);
	SET @SiteXML = '';
	SET @DocumentTypesXML = '';
	SET @Iteration = @Iteration +1;
END

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH