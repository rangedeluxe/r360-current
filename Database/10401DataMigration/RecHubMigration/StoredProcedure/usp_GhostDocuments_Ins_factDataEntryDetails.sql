--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_GhostDocuments_Ins_factDataEntryDetails
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_GhostDocuments_Ins_factDataEntryDetails') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_GhostDocuments_Ins_factDataEntryDetails
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_GhostDocuments_Ins_factDataEntryDetails
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/19/2017
*
* Purpose: Retrieve transctions that have data entry, but not a document. This will 
*	create a ghost document for data entry to extract.
*
* Modification History
* 06/19/2017 PT 145212351 JPB	Created
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY

	INSERT INTO GhostDocumentfactDataEntryDetails(Batch_Id,Transaction_Id,BatchSequence)
	SELECT 
		RecHubMigration.ExtractBatches.Batch_Id,
		RecHubMigration.TransactionNodes.Transaction_Id,
		OLTA.factDataEntryDetails.BatchSequence
	FROM 
		RecHubMigration.ExtractBatches
		INNER JOIN RecHubMigration.TransactionNodes ON RecHubMigration.TransactionNodes.Batch_Id = RecHubMigration.ExtractBatches.Batch_Id
		INNER JOIN OLTA.factDataEntryDetails ON OLTA.factDataEntryDetails.DepositDateKey = RecHubMigration.ExtractBatches.DepositDateKey
				AND OLTA.factDataEntryDetails.ProcessingDateKey = RecHubMigration.ExtractBatches.BatchDateKey
				AND OLTA.factDataEntryDetails.SourceProcessingDateKey = RecHubMigration.ExtractBatches.ProcessingDateKey
				AND OLTA.factDataEntryDetails.LockboxKey = RecHubMigration.ExtractBatches.LockboxKey
				AND OLTA.factDataEntryDetails.BatchID = RecHubMigration.ExtractBatches.BatchID
				AND OLTA.factDataEntryDetails.BatchSourceKey = RecHubMigration.ExtractBatches.BatchSourceKey
				AND OLTA.factDataEntryDetails.TransactionID = RecHubMigration.TransactionNodes.TransactionID
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
	WHERE
		OLTA.dimDataEntryColumns.TableType BETWEEN 2 AND 3

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		



