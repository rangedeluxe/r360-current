--WFSScriptProcessorSchema RecHubMigration
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_LockboxRemitters_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMigration.usp_LockboxRemitters_Get') IS NOT NULL
       DROP PROCEDURE RecHubMigration.usp_LockboxRemitters_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMigration.usp_LockboxRemitters_Get
(
	@parmStartDate				DATETIME,
	@parmSiteBankIDList			NVARCHAR(MAX),
	@parmSiteLockboxIDList		NVARCHAR(MAX)
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright © 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 04/25/2019
*
* Purpose: Select the WorkgroupRemitters for a given RT/Account that belong 
*				to a given Bank and set of workgroups
*
* Modification History
* 04/25/2019 R360-15311 MGE	Created
* 07/16/2019 R360-15311 MGE Fixed a duplicate key on insert issue
*
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	-- Set StartDateKey to correct length
	DECLARE @StartDateKey INT;
	SELECT 	@StartDateKey = CAST(CONVERT(VARCHAR, @parmStartDate, 112) AS INT);

	-- Make a table holding the Banks passed in @parmSiteBankIDList
	DECLARE @SiteBankTable TABLE (SiteBankID VARCHAR(10) );

	DECLARE @x INT = 0
	DECLARE @firstcomma INT = 0
	DECLARE @nextcomma INT = 0
	
	BEGIN
		

		SET @x = LEN(@parmSiteBankIDList) - LEN(REPLACE(@parmSiteBankIDList, ',', '')) + 1 -- number of ids in id_list

		WHILE @x > 0
			BEGIN
				SET @nextcomma = CASE WHEN CHARINDEX(',', @parmSiteBankIDList, @firstcomma + 1) = 0
									  THEN LEN(@parmSiteBankIDList) + 1
									  ELSE CHARINDEX(',', @parmSiteBankIDList, @firstcomma + 1)
								 END
				IF ISNumeric(SUBSTRING(@parmSiteBankIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) ) = 1   -- To make sure we have a number
				BEGIN
					INSERT  INTO @SiteBankTable 
						(SiteBankID)
					VALUES  
						(SUBSTRING(@parmSiteBankIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) )
				END
				SET @firstcomma = CHARINDEX(',', @parmSiteBankIDList, @firstcomma + 1)
				SET @x = @x - 1
			END
		END

	-- Make a table holding the Lockboxes passed in @parmSiteLockboxIDList
	DECLARE @SiteLockboxTable TABLE (SiteLockboxID VARCHAR(10) );
	IF @parmSiteLockboxIDList = 'ALL'
		BEGIN
			INSERT INTO @SiteLockboxTable
			SELECT DISTINCT SiteLockboxID 
			FROM OLTA.dimLockboxes
			INNER JOIN @SiteBankTable SBT ON SBT.SiteBankID = OLTA.dimLockboxes.SiteBankID
		END
	ELSE
		BEGIN 

			SET @x = 0 
			SET @firstcomma = 0
			SET @nextcomma = 0
			
			SET @x = LEN(@parmSiteLockboxIDList) - LEN(REPLACE(@parmSiteLockboxIDList, ',', '')) + 1 -- number of ids in id_list

			WHILE @x > 0
			BEGIN
				SET @nextcomma = CASE WHEN CHARINDEX(',', @parmSiteLockboxIDList, @firstcomma + 1) = 0
									  THEN LEN(@parmSiteLockboxIDList) + 1
									  ELSE CHARINDEX(',', @parmSiteLockboxIDList, @firstcomma + 1)
								 END
				IF ISNumeric(SUBSTRING(@parmSiteLockboxIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) ) = 1   -- To make sure we have a number
				BEGIN
					INSERT  INTO @SiteLockboxTable 
						(SiteLockboxID)
					VALUES  
						(SUBSTRING(@parmSiteLockboxIDList, @firstcomma + 1, (@nextcomma - @firstcomma) - 1) )
				END
				SET @firstcomma = CHARINDEX(',', @parmSiteLockboxIDList, @firstcomma + 1)
				SET @x = @x - 1
			END
		END

;with LatestRemitters AS 
(
	SELECT
		SiteBankID,
		SiteLockboxID,
		RoutingNumber,
		Account,
		MAX(dimRemitters.LoadDate) AS LoadDate,
		MAX(dimRemitters.RemitterKey) AS RemitterKey
	FROM OLTA.factChecks
	INNER JOIN OLTA.dimRemitters ON OLTA.factChecks.RemitterKey = OLTA.dimRemitters.RemitterKey
	INNER JOIN OLTA.dimLockboxes ON OLTA.factChecks.LockboxKey = OLTA.dimLockboxes.LockboxKey
	WHERE RoutingNumber Not LIKE '%!%'
		AND Account Not LIKE '%!%'
		AND IsNumeric(RoutingNumber) = 1
		AND Account <> ''
		AND RemitterName IS NOT NULL
		AND RemitterName <> ''
		AND DepositDateKey > @StartDateKey
	GROUP BY SiteBankID, SiteLockboxID, RoutingNumber, Account
)
	SELECT 
		LatestRemitters.SiteBankID, 
		LatestRemitters.SiteLockboxID, 
		LatestRemitters.RoutingNumber, 
		LatestRemitters.Account, 
		RemitterName,
		LatestRemitters.LoadDate
	FROM LatestRemitters
	INNER JOIN OLTA.dimRemitters ON LatestRemitters.RemitterKey = dimRemitters.RemitterKey
	INNER JOIN @SiteBanKTable SBT ON SBT.SiteBankID = LatestRemitters.SiteBankID
	INNER JOIN @SiteLockboxTable LBXT ON LBXT.SiteLockboxID = LatestRemitters.SiteLockboxID
	WHERE LatestRemitters.RoutingNumber Not LIKE '%!%'
	AND LatestRemitters.Account Not LIKE '%!%'
	AND IsNumeric(LatestRemitters.RoutingNumber) = 1
	AND LatestRemitters.Account <> ''
	AND RemitterName IS NOT NULL
	AND RemitterName <> ''
	ORDER BY SiteBankID, SiteLockboxID, RoutingNumber, Account

END TRY
BEGIN CATCH
    EXEC dbo.usp_WfsRethrowException;
END CATCH
