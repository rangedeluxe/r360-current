﻿--WFSScriptProcessorSchema RecHubAPI
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubAPI_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAPI.usp_factBatchSummary_Get') IS NOT NULL
       DROP PROCEDURE RecHubAPI.usp_factBatchSummary_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAPI.usp_factBatchSummary_Get
(
	@parmBankId INT,
	@parmWorkgroupId INT,
	@parmStartDepositDate INT,
	@parmEndDepositDate INT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Chris Colombo
* Date: 07/09/2018
*
* Purpose: Gets factBatchSummary records 
*	
*
* Modification History
* 07/09/2018 PT 155569216 CMC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		CONVERT(DATETIME, CONVERT(VARCHAR(10), RechubData.factBatchSummary.DepositDateKey), 112) AS DepositDate,
		CONVERT(DATETIME, CONVERT(VARCHAR(10), RecHubData.factBatchSummary.SourceProcessingDateKey), 112) AS ProcessingDate,
		RecHubData.factBatchSummary.BatchID AS BatchId,
		RecHubData.factBatchSummary.SourceBatchID AS SourceBatchId,
		RecHubData.dimClientAccounts.SiteBankID AS BankId,
		RecHubData.dimBanks.BankName AS BankName,
		RecHubData.dimClientAccounts.SiteClientAccountID AS ClientAccountId,
		RecHubData.dimClientAccounts.LongName AS ClientLongName,
		RecHubData.dimClientAccounts.ShortName AS ClientShortName,
		RecHubData.factBatchSummary.BatchNumber AS BatchNumber,
		RecHubData.factBatchSummary.CheckCount AS CheckCount,
		RecHubData.factBatchSummary.StubCount AS StubCount,
		RecHubData.factBatchSummary.BatchSourceKey AS BatchSourceKey,
		RecHubData.factBatchSummary.DocumentCount AS DocumentCount,
		RecHubData.factBatchSummary.ScannedCheckCount AS ScannedCheckCount,
		RecHubData.dimBatchSources.LongName AS BatchPaymentSource,
		RecHubData.dimBatchPaymentTypes.LongName AS BatchPaymentType
	FROM 
		RecHubData.factBatchSummary 
		INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.factBatchSummary.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.factBatchSummary.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
	WHERE 
		RecHubData.dimClientAccounts.SiteBankID = @parmBankId
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmWorkgroupId
		AND RecHubData.factBatchSummary.DepositDateKey BETWEEN @parmStartDepositDate AND @parmEndDepositDate
    	AND RecHubData.factBatchSummary.DepositStatus >= 850
    	AND RecHubData.factBatchSummary.IsDeleted = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
