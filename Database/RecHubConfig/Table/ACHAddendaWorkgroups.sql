--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ACHAddendaWorkgroups
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corp. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/29/2018
*
* Purpose: Store the association between ACHAddenda and Workgroups.
*
*
* Modification History
* 05/29/2018 PT 157790924	JPB/MGE	Created for 2.03
* 08/02/2018 PT 157790924	MGE Added unique Index to prevent multiple identical setups
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ACHAddendaWorkgroups
(
	ACHAddendaWorkgroupKey BIGINT IDENTITY
		CONSTRAINT PK_dimWorkgroupACHAddenda PRIMARY KEY CLUSTERED,
	ACHAddendaKey BIGINT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaWorkgroups_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaWorkgroups_ModificationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubConfig.ACHAddendaWorkgroups ADD 
	CONSTRAINT FK_ACHAddendaWorkgroups_ACHAddenda FOREIGN KEY(ACHAddendaKey) REFERENCES RecHubConfig.ACHAddenda(ACHAddendaKey);
--WFSScriptProcessorIndex RecHubConfig.ACHAddenda.IDX_ACHAddendaWorkgroups_SiteBankIDSiteClientAccountID
CREATE UNIQUE NONCLUSTERED INDEX IDX_ACHAddendaWorkgroups_SiteBankIDSiteClientAccountID ON RecHubConfig.ACHAddendaWorkgroups
(
	SiteBankID ASC,
	SiteClientAccountID ASC
)
INCLUDE
(
	ACHAddendaKey
);