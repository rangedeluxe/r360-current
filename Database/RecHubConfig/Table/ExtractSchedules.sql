--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ExtractSchedules
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 05/15/2013
*
* Purpose: Store the Extract Schedule.
*
*
* Modification History
* 05/15/2013 WI 97699 DRP	Created for 2.0
* 05/29/2013 WI 103459 DRP  Added ExtractRunArguments
* 05/06/2014 WI 139962 JBS	Add columns ScheduleType and DayInMonth
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ExtractSchedules
(
	ExtractScheduleID     BIGINT IDENTITY(1,1) NOT NULL 
	      CONSTRAINT PK_ExtractSchedules PRIMARY KEY,
	ExtractDefinitionID   BIGINT NOT NULL,
	[Description]         VARCHAR(255) NOT NULL,
	IsActive              BIT NOT NULL,
	DaysOfWeek            VARCHAR(7) NOT NULL,
	DayInMonth			  INT,		
	ScheduleTime          TIME (7) NOT NULL,
	ScheduleType		  INT,
	ExtractRunArguments   VARCHAR(255) NOT NULL,
	CreationDate          DATETIME NOT NULL,
	ModificationDate      DATETIME NOT NULL,
	CreatedBy             VARCHAR(128) NOT NULL,
	ModifiedBy            VARCHAR(128) NOT NULL
)
--WFSScriptProcessorTableProperties
