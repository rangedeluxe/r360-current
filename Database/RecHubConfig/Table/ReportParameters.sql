--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorTableName ReportParameters
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/24/2013
*
* Purpose:	Contains the report parameter configuration information.
*
* Modification History
* 06/24/2013 WI 107180 KLC	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ReportParameters
(
	ReportID			INT				NOT NULL,
	OrderValue			INT				NOT NULL,
	DisplayName			VARCHAR(40)		NOT NULL,
	ParameterName		VARCHAR(10)		NOT NULL,
	ParameterType		INT				NOT NULL,
	IsRequired			BIT				NOT NULL,
	AllowMultiple		BIT				NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorPK
ALTER TABLE RecHubConfig.ReportParameters ADD CONSTRAINT PK_ReportParameters PRIMARY KEY CLUSTERED 
(
	ReportID ASC,
	OrderValue ASC
);
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubConfig.ReportParameters ADD 
    CONSTRAINT FK_ReportParameters_ReportConfigurations FOREIGN KEY (ReportID) REFERENCES RecHubConfig.ReportConfigurations(ReportID);
