--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ACHAddenda
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018-2019 Deluxe Corp. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/29/2018
*
* Purpose: Store the Addendas that will be defined in associated tables.
*
*
* Modification History
* 05/29/2018 PT 157790924	JPB/MGE	Created for 2.03
* 02/06/2019 R360-15418		JPB		Changed SegmentDelimiter to VARCHAR(5)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ACHAddenda
(
	ACHAddendaKey BIGINT IDENTITY
		CONSTRAINT PK_ACHAddenda PRIMARY KEY CLUSTERED,
	AddendaName   VARCHAR(64) NOT NULL,
	SegmentDelimiter VARCHAR(5) NOT NULL
		CONSTRAINT DF_ACHAddendaSegments_SegmentDelimiter DEFAULT('\'),
	FieldDelimiter CHAR(1) NOT NULL 
		CONSTRAINT DF_ACHAddendaSegments_FieldDelimiter DEFAULT('*'),
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaSegment_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaSegment_ModificationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties