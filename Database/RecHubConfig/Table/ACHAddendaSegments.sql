--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ACHAddendaSegments
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corp. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/29/2018
*
* Purpose: Store the AddendaSegments that will be defined in associated tables.
*
*
* Modification History
* 05/29/2018 PT 157790924	JPB/MGE	Created for 2.03
* 07/17/2018 PT 157790924	MGE Added CreateNewRecord column
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ACHAddendaSegments
(
	ACHAddendaSegmentKey BIGINT IDENTITY
		CONSTRAINT PK_ACHAddendaSegments PRIMARY KEY CLUSTERED,
	ACHAddendaKey BIGINT,
	AddendaSegmentName VARCHAR(64) NOT NULL,
	SegmentIdentifier VARCHAR(3) NOT NULL,
	CreateNewRecord BIT NOT NULL 
		CONSTRAINT DF_ACHAddendaSegments_CreateNewRecord DEFAULT 0,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaSegments_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaSegments_ModificationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubConfig.ACHAddendaSegments ADD 
	CONSTRAINT FK_ACHAddendaSegments_ACHAddenda FOREIGN KEY(ACHAddendaKey) REFERENCES RecHubConfig.ACHAddenda(ACHAddendaKey);
