--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ExtractDefinitions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 08/23/2013
*
* Purpose: Store the Extract Definition.
*
*
* Modification History
* 05/15/2013 WI 97649 JBS	Created for 2.0 
* 05/06/2014 WI 139961 JBS	Add ExtractDefinitionType and ExtractFilename
* 07/06/2017 PT 146413829 MGE Change Data Type of ExtractDefinition to VARBINARY
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ExtractDefinitions
(
	ExtractDefinitionID		BIGINT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_ExtractDefinitions PRIMARY KEY CLUSTERED,				
	ExtractDefinition		VARBINARY(MAX) NOT NULL,
	ExtractName				VARCHAR(128) NOT NULL,
	[Description]			VARCHAR(255) NOT NULL,
	ExtractDefinitionSizeKb INT NOT NULL,
	ExtractDefinitionType	INT,
	IsActive				BIT NOT NULL,
	ExtractFilename			VARCHAR(128),
	CreationDate			DATETIME NOT NULL,
	ModificationDate		DATETIME NOT NULL,
	CreatedBy				VARCHAR(128) NOT NULL,
	ModifiedBy				VARCHAR(128) NOT NULL
)
--WFSScriptProcessorTableProperties