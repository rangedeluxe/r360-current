--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorTableName ReportConfigurations
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/24/2013
*
* Purpose:	Contains the report configuration information.
*
* Modification History
* 06/24/2013 WI 107179 KLC	Created
* 06/24/2014 WI 149725 JPB	RAAM updates
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ReportConfigurations
(
	ReportID			INT				NOT NULL
		CONSTRAINT PK_ReportConfigurations PRIMARY KEY,
	GroupID				INT				NULL,
	OrderValue			INT				NOT NULL,
	RAAMResourceName	NVARCHAR(50)	NOT NULL,
	ReportName			VARCHAR(70)		NOT NULL,
	XmlParameterName	VARCHAR(40)		NOT NULL,	
	ReportFileName		VARCHAR(70)		NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubConfig.ReportConfigurations ADD 
    CONSTRAINT FK_ReportConfigurations_ReportGroups FOREIGN KEY (GroupID) REFERENCES RecHubConfig.ReportGroups(GroupID);
