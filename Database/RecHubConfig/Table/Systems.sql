--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName Systems
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: XXX
* Date: XX/XX/XXXX
*
* Purpose:	Contains Database version installed.
*
*
* Modification History
* XX/XX/XXXX CR XXXXX XXX	Created
* 03/13/2013 WI 91914 JBS	Update to 2.0 release.  Change Schema Name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.Systems
(
	SystemName VARCHAR(128) NOT NULL
		CONSTRAINT PK_Systems PRIMARY KEY NONCLUSTERED,
	DateInstalled DATETIME NULL,
	SystemVersion VARCHAR(20) NULL
)
--WFSScriptProcessorTableProperties
