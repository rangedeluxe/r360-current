--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ACHAddendaDataEntryMaps
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corp. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/29/2018
*
* Purpose: Store the AddendaSegments map to DataEntry columns.
*
*
* Modification History
* 05/29/2018 PT 157790924	JPB/MGE	Created for 2.03
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ACHAddendaDataEntryMaps
(
	ACHAddendaDataEntryMapKey BIGINT IDENTITY
		CONSTRAINT PK_ACHAddendaDataEntryMaps PRIMARY KEY CLUSTERED,
	ACHAddendaSegmentKey BIGINT NOT NULL,
	IsCheck BIT NOT NULL
		CONSTRAINT DF_ACHAddendaDataEntryMaps_IsCheck DEFAULT(0),
	SegmentQualifier VARCHAR(5),
	FieldNumber TINYINT NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaDataEntryMaps_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ACHAddendaDataEntryMaps_ModificationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubConfig.ACHAddendaDataEntryMaps ADD 
	CONSTRAINT FK_ACHAddendaDataEntryMaps_ACHAddendaSegments FOREIGN KEY(ACHAddendaSegmentKey) REFERENCES RecHubConfig.ACHAddendaSegments(ACHAddendaSegmentKey);
