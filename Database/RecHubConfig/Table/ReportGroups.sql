--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorTableName ReportGroups
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/24/2013
*
* Purpose:	Contains the report group to categorize the reports.
*				Includes name and ordering or the groups.
*
* Modification History	
* 06/24/2013 WI 107178 KLC	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.ReportGroups
(
	GroupID				INT				NOT NULL
		CONSTRAINT PK_ReportGroups PRIMARY KEY,
	GroupName			VARCHAR(40)		NOT NULL,
	OrderValue			INT				NOT NULL
);
--WFSScriptProcessorTableProperties