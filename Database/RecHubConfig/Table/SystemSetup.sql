--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName SystemSetup
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: XXX
* Date: XX/XX/XXXX
*
* Purpose:	Stores Application Configuration information.
*
*
* Modification History
* XX/XX/XXXX CR XXXXX XXX	Created
* 03/13/2013 CR 91921 JBS	Update to 2.0 release.  Change Schema Name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubConfig.SystemSetup
(
	Section VARCHAR(30) NOT NULL,
	SetupKey VARCHAR(50) NOT NULL,
	Value VARCHAR(255) NULL,
	[Description] VARCHAR(255) NULL,
	DataType VARCHAR(30) NOT NULL,
	ValidValues VARCHAR(255) NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_SystemSetup_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_SystemSetup_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_SystemSetup_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_SystemSetup_ModifiedBy DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorPK
ALTER TABLE RecHubConfig.SystemSetup ADD CONSTRAINT PK_SystemSetup PRIMARY KEY CLUSTERED 
(
	[Section] ASC,
	[SetupKey] ASC
);
