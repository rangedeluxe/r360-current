--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorFunctionName udf_ParseSystemVersion
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubConfig.udf_ParseSystemVersion') IS NOT NULL
       DROP Function RecHubConfig.udf_ParseSystemVersion
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubConfig.udf_ParseSystemVersion(@parmVersion VARCHAR(32) = NULL)
RETURNS @Version TABLE(Major INT, Minor INT, Revision INT, Patch INT, FullVersion BIGINT)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/07/2011
*
* Purpose: Parse a version number string into the individual parts.
* Example: 1.01.08.06 becomes
*	Major = 1
*	Minor = 1
*	Revison = 8
*	Patch = 6
*	FullVersion = 1001008006
*
* Modification History
* 10/07/2011 CR 47254 JPB	Created
* 03/13/2013 WI 91873 JBS	Update to 2.0 release
******************************************************************************/
BEGIN
DECLARE @Major 			INT,
		@Minor 			INT,
		@Revision 		INT,
		@Patch 			INT,
		@Pos 			INT,
		@VersionPiece	VARCHAR(25),
		@StringVersion	VARCHAR(32)

SELECT @Major = NULL, @Minor = NULL, @Patch = NULL, @Revision = NULL, @StringVersion = ''

IF @parmVersion IS NOT NULL
BEGIN
	-- Need to tack a delimiter onto the end of the input string if one doesn't exist
	IF RIGHT(RTRIM(@parmVersion),1) <> '0'
		SET @parmVersion = @parmVersion  + '.'

	SET @Pos =  PATINDEX('%.%' , @parmVersion)
	WHILE( @Pos <> 0 )
	BEGIN
		SET @VersionPiece = LEFT(@parmVersion, @Pos - 1)

		SET @StringVersion = @StringVersion + STUFF(@VersionPiece,1,0,REPLICATE('0', 3-LEN(@VersionPiece)))
		IF @Major IS NULL
			SET @Major = CAST(@VersionPiece AS INT)
		ELSE IF @Minor IS NULL
			SET @Minor = CAST(@VersionPiece AS INT)
		ELSE IF @Revision IS NULL
			SET @Revision = CAST(@VersionPiece AS INT)
		ELSE IF @Patch IS NULL
			SET @Patch = CAST(@VersionPiece AS INT)
			
		SET @parmVersion = STUFF(@parmVersion, 1, @Pos, '')
		SET @Pos =  PATINDEX('%.%' , @parmVersion)
	END

	--Cleanup the remaining piece
	SET @StringVersion = @StringVersion + STUFF(@VersionPiece,1,0,REPLICATE('0', 3-LEN(@VersionPiece)))
	IF @Major IS NULL
		SET @Major = CAST(@parmVersion AS INT)
	ELSE IF @Minor IS NULL
		SET @Minor = CAST(@parmVersion AS INT)
	ELSE IF @Revision IS NULL
		SET @Revision = CAST(@parmVersion AS INT)
	ELSE IF @Patch IS NULL
		SET @Patch = CAST(@parmVersion AS INT)
END

	INSERT INTO @Version(Major,Minor,Revision,Patch,FullVersion)
	SELECT @Major AS Major, @Minor AS Minor, @Revision AS Revision, @Patch AS Patch, CAST(@StringVersion AS BIGINT)
	RETURN
END