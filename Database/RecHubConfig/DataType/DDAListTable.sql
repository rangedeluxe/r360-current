--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorDataTypeName DDAListTable
--WFSScriptProcessorDataTypeCreate
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Chris Colombo
* Date: 07/16/2018
*
* Purpose: Table value to used for looking up ABA/DDA Rules 
*	
*
* Modification History
* 07/16/2018 PT 157791221 CMC	Created
******************************************************************************/
CREATE TYPE RecHubConfig.DDAListTable AS TABLE
(
    ABA varchar(10) NOT NULL,
    DDA varchar(35) NOT NULL
);
