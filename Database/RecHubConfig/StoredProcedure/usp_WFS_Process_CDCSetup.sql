--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_WFS_Process_CDCSetup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_WFS_Process_CDCSetup') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_WFS_Process_CDCSetup
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_WFS_Process_CDCSetup
(
	@parmCDCDefinition XML,
	@parmSystemVersion VARCHAR(32) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/15/2013
*
* Purpose: Configure a table for CDC.
*
* Modification History
* 05/15/2013 WI 99832 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE	@SystemVersion 		VARCHAR(32),
		@CDCVersion			VARCHAR(32),
		@Loop				INT,
		@SystemVer			BIGINT,
		@CaptureInstance	NVARCHAR(128),
		@SchemaName			NVARCHAR(256),
		@TableName			NVARCHAR(256),
		@ColumnList			NVARCHAR(MAX);


BEGIN TRY
	/* Get the schema and table CDC is to be enabled on */
	SELECT 	@SchemaName = LTRIM(RTRIM(TableInfo.att.value('@Schema','VARCHAR(256)'))),
			@TableName = LTRIM(RTRIM(TableInfo.att.value('@Name','VARCHAR(256)')))
	FROM	@parmCDCDefinition.nodes('/CDCDefinition/Table') TableInfo(att);

	/* Set the cpature_instance value for easy retrieval by SSIS packages */
	SELECT @CaptureInstance = @SchemaName + '_' + @TableName;

	/* Set the column list to watch */
	SET @ColumnList = '';
	SELECT 	@ColumnList = @ColumnList + ColumnInfo.att.value('@Name','VARCHAR(128)') + ','
	FROM	@parmCDCDefinition.nodes('/CDCDefinition/Columns/Column') ColumnInfo(att);

	IF( LEN(@ColumnList) > 0 )
		SET @ColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);

	/* Check to see if CDC is enabled at the database level, if not enable it. */
	IF( (SELECT is_cdc_enabled FROM sys.databases WHERE [name] = DB_NAME()) = 0 )
		EXEC sys.sp_cdc_enable_db;

	/* Make sure CDC is not enabled on the table first */
	IF( NOT EXISTS(SELECT 1 FROM cdc.change_tables WHERE capture_instance = @CaptureInstance) )
	BEGIN
		/* need to work out what file group to use */
		/* Check to see if the file group exists */

		EXEC sp_cdc_enable_table @source_schema = @SchemaName, 
			@source_name = @TableName,
			@role_name = N'CDCRole',
			@supports_net_changes = 0,
			@capture_instance = @CaptureInstance,
			@captured_column_list = @ColumnList;
			--@filegroup_name = '';
	END
	ELSE
		RAISERROR('CDC is already enabled on %s.%s',10,1,@SchemaName,@TableName);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH