--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Upd_ExtractDefFile
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Upd_ExtractDefFile') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Upd_ExtractDefFile
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Upd_ExtractDefFile 
(
       @parmUserID					INT,
	   @parmExtractDefID			INT,
	   @parmExtractDefSizeKb		INT,
	   @parmExtractDefinitionType	INT,
	   @parmExtractFileName			VARCHAR(128),
	   @parmExtractDefinition		VARCHAR(MAX)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Update Extract XML file
*
* Modification History
* 05/13/2013 WI	105079 EAS	Created
* 05/07/2014 WI 140383 JBS	Adding Columns ExtractDefinitionType, ExtractFilename
* 06/18/2014 WI 148596 DJW  Added logging for audit purposes. Adjusted XML Audit message
* 10/21/2014 WI 173361 BLR  Changed param 'TableName' to the TableName, not the Stored Proc name. 
* 07/06/2017 PT 2677   CEJ	Convert ExtractDefinition from varchar max to varbinary max
******************************************************************************/

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END


	DECLARE @auditMessage			VARCHAR(1024),
			@auditColumns			RecHubCommon.AuditValueChangeTable;
	DECLARE @curTime DATETIME = GETDATE();

	--DECLARE prev values placeholder
	DECLARE 
	   @prevExtractDefSizeKb		INT,
	   @prevExtractDefinitionType	INT,
	   @prevExtractFileName			VARCHAR(128),
	   @prevExtractDefinition		VARBINARY(MAX);


	UPDATE	
		RecHubConfig.ExtractDefinitions
	SET	
	   @prevExtractDefinition = RecHubConfig.ExtractDefinitions.ExtractDefinition,
	   @prevExtractDefSizeKb = RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb,
	   @prevExtractDefinitionType = RecHubConfig.ExtractDefinitions.ExtractDefinitionType,
	   @prevExtractFileName = RecHubConfig.ExtractDefinitions.ExtractFilename,
	   		
	   RecHubConfig.ExtractDefinitions.ExtractDefinition = CONVERT(VARBINARY(MAX), @parmExtractDefinition),
	   RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb = @parmExtractDefSizeKb,
	   RecHubConfig.ExtractDefinitions.ExtractDefinitionType = @parmExtractDefinitionType,
	   RecHubConfig.ExtractDefinitions.ExtractFilename = @parmExtractFileName,
	   RecHubConfig.ExtractDefinitions.ModificationDate = GETDATE(),
	   RecHubConfig.ExtractDefinitions.ModifiedBy = SUSER_SNAME()
	WHERE
	  	RecHubConfig.ExtractDefinitions.ExtractDefinitionID = @parmExtractDefID;

	-- track changes
	-- The message constructed here for Extract definition column because the Audit column is VARCHAR(256) and to match the message required from Specs.
	IF @prevExtractDefinition <> CAST(@parmExtractDefinition AS VARBINARY(MAX))
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) 
	VALUES('ExtractDefinition', CAST('Extract Definition '+@prevExtractFileName+' has changed. The full definition information is stored in ExtractDefinitions table. The previous definition file may be found on disk.' AS VARCHAR(256)), CAST('Extract Definition '+@parmExtractFileName+' has changed. The full definition information is stored in ExtractDefinitions table. The previous definition file may be found on disk.' AS VARCHAR(256)));
	
	IF @prevExtractDefSizeKb <> @parmExtractDefSizeKb
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) 
	VALUES('ExtractDefinitionSizeKb', CAST(@prevExtractDefSizeKb AS VARCHAR(10)), CAST(@parmExtractDefSizeKb AS VARCHAR(10)));

	IF ISNULL(@prevExtractDefinitionType,-1) <> @parmExtractDefinitionType
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) 
	VALUES('ExtractDefinitionType', CAST(@prevExtractDefinitionType AS VARCHAR(10)), CAST(@parmExtractDefinitionType AS VARCHAR(10)));

	IF CAST(ISNULL(@prevExtractFileName,'') AS VARBINARY(128)) <> CAST(@parmExtractFileName AS VARBINARY(128))
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) 
	VALUES('ExtractDefinition', @prevExtractFileName, @parmExtractFileName);

	SET @auditMessage = 'Modified ExtractDefinitions for Definition Id: ' + CAST(@parmExtractDefID AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubConfig.usp_ExtractDefinitions_Upd_ExtractDefFile',
			@parmSchemaName			= 'RecHubConfig',
			@parmTableName			= 'ExtractDefinitions',
			@parmColumnName			= 'ExtractDefinitionID',
			@parmAuditValue			= @parmExtractDefID,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;


	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
