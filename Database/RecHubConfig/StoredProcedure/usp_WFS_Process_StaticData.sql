--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_WFS_Process_StaticData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_WFS_Process_StaticData') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_WFS_Process_StaticData
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_WFS_Process_StaticData
(
	@parmStaticData XML,
	@parmSystemVersion VARCHAR(32) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.L
*
* Author: JPB
* Date: 09/29/2011
*
* Purpose: Insert, update or delete static data records passed in as XML. This is not
*	the most efficent SP every written. :( But speed is not important, readablility
*	and maintenance are. This SP is only executed at install/upgrade time.
*
* Modification History
* 09/29/2011 CR 46052 JPB	Created
* 03/13/2013 WI 91937 JBS	Update to 2.0 release. Change schema name. 
*							Added Identity column support
*							Added CreationDate/CreatedBy/ModificationDate/ModifiedBy
* 06/20/2014 WI 149225 JPB	Added NVARCHAR, DATETIME
*							Added lookup from static data and from xml data.
*							Added version range.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE	@SystemVersion 		VARCHAR(32),
		@StaticDataVersion	VARCHAR(32),
		@StaticDataVersionStart	VARCHAR(32),
		@StaticDataVersionEnd	VARCHAR(32),
		@StaticData			XML,
		@Loop				INT,
		@StaticDataLoop		INT,
		@SystemVer			BIGINT,
		@DataVer			BIGINT,
		@ColumnVerStart		BIGINT,
		@ColumnVerEnd		BIGINT,
		@ActionCode			CHAR(1),
		@ColumnName			VARCHAR(256),
		@ColumnList			VARCHAR(MAX),
		@SQLCommand			VARCHAR(MAX),
		@NSQLCommand		NVARCHAR(MAX),
		@AttributeName		VARCHAR(256),
		@AttributeValue		VARCHAR(1024),		
		@FullTableName		VARCHAR(512),
		@SchemaName			VARCHAR(256),
		@TableName			VARCHAR(256),
		@ProcessRow			BIT,
		@CreationDate		BIT,
		@CreatedBy			BIT,
		@ModificationDate	BIT,
		@ModifiedBy			BIT,
		@IdentityColumn		BIT = 0;

BEGIN TRY
	/* Remove the temp tables if they exist */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupTable')) 
		DROP TABLE #LookupTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupWhere')) 
		DROP TABLE #LookupWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Lookup')) 
		DROP TABLE #Lookup;

	/* Table is altered to look like the static data that is being handled. */
	CREATE TABLE #StaticDataTable 
	(
		RowID INT IDENTITY(1,1),
		[Version] VARCHAR(64),
		ActionCode CHAR(1),
	);

	/* Stored the individual XML nodes */
	CREATE TABLE #StaticDataValues
	(
		RowID INT IDENTITY(1,1),
		StartVersion VARCHAR(32),
		EndVersion VARCHAR(32),
		StaticData XML
	);

	/* Store any lookup tables */
	CREATE TABLE #LookupTable 
	(
		TableID INT IDENTITY(1,1),
		ColumnName VARCHAR(256),
		TableName VARCHAR(128),
		LookupColumnName VARCHAR(256)
	);

	/* Store any lookup columns (WHERE) */
	CREATE TABLE #LookupWhere 
	(
		ColumnID INT IDENTITY(1,1),
		TableID INT,
		ColumnName VARCHAR(256),
		DataType VARCHAR(15),
		Value VARCHAR(128),
		Size INT,
		XMLLookup VARCHAR(256)
	);

	/* #Loopup table needed for xml lookup loop */
	CREATE TABLE #Lookup
	(
		RowID INT IDENTITY(1,1),
		TableID INT,
		ColumnName VARCHAR(256),
		TableName VARCHAR(128),
		LookupColumnName VARCHAR(256)
	)

	/* Store the column definations */
	DECLARE @ColumnMapping TABLE
	(
		RowID INT IDENTITY(1,1),
		StartVersion VARCHAR(64),
		EndVersion VARCHAR(64),
		ColumnName VARCHAR(256),
		KeyValue BIT,
		Updateable BIT,
		DataType VARCHAR(15),
		Size INT,
		IdentityColumn BIT
	);

	/* Get the schema/table name from the XML document. */
	SELECT 	@SchemaName = TableInfo.att.value('@Schema','VARCHAR(256)'),
			@TableName = TableInfo.att.value('@Name','VARCHAR(256)')
	FROM	@parmStaticData.nodes('/StaticData/Table') TableInfo(att);
	
	/* See if Creation Date exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'CreationDate' )
		SET @CreationDate = 1;
	ELSE SET @CreationDate = 0	;

	/* See if Modification Date exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'ModificationDate' )
		SET @ModificationDate = 1;
	ELSE SET @ModificationDate = 0;

	/* See if CreatedBy exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'CreatedBy' )
		SET @CreatedBy = 1;
	ELSE SET @CreatedBy = 0;

	/* See if ModifiedBy exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'ModifiedBy' )
		SET @ModifiedBy = 1;
	ELSE SET @ModifiedBy = 0;

	/* Create a var with the fully qualified schema and table name as [scheme].[table] */
	SELECT @FullTableName = '[' + @SchemaName + '].[' + @TableName + ']';

	RAISERROR('Processing static data for %s.%s',10,1,@SchemaName,@TableName) WITH NOWAIT;

	/* Get the current database level from the systems table based on the system defined in the XML doc. */
	IF( @parmSystemVersion IS NULL )
	BEGIN /* only go to the Systems table if the system version was not passed in */
		SELECT 	@SystemVersion = SystemVersion
		FROM	RecHubConfig.Systems
				INNER JOIN 
				(
					SELECT	StaticData.att.value('@SystemName','VARCHAR(64)') AS SystemName
					FROM	@parmStaticData.nodes('/StaticData') StaticData(att)
				) SD ON RecHubConfig.Systems.SystemName = SD.SystemName;
	END
	ELSE SET @SystemVersion = @parmSystemVersion;
	
	/* Break the system version down into it's parts using a udf */
	SELECT	@SystemVer = FullVersion
	FROM	RecHubConfig.udf_ParseSystemVersion(@SystemVersion);

	/* Get the column definitions from the XML document and store them for use. */
	;WITH StaticData_CTE AS
	(
		SELECT	COALESCE(ColumnMap.att.value('@Version', 'VARCHAR(64)'),'1.00.00.00') AS VersionInfo,
				ColumnMap.att.query('.').value('.', 'VARCHAR(256)') AS ColumnName,
				CASE ColumnMap.att.value('@Key', 'VARCHAR(5)')
					WHEN 'TRUE' THEN 1
					ELSE 0
				END AS KeyValue,
				CASE ColumnMap.att.value('@Update', 'VARCHAR(5)')
					WHEN 'TRUE' THEN 1
					ELSE 0
				END AS Updateable,
				ColumnMap.att.value('@DataType','VARCHAR(15)') AS DataType,
				CASE UPPER(ColumnMap.att.value('@DataType','VARCHAR(15)'))
					WHEN 'VARCHAR' THEN COALESCE(ColumnMap.att.value('@Size', 'VARCHAR(32)'),64)
					WHEN 'NVARCHAR' THEN COALESCE(ColumnMap.att.value('@Size', 'VARCHAR(32)'),64)
					WHEN 'CHAR' THEN COALESCE(ColumnMap.att.value('@Size', 'VARCHAR(32)'),64)
					ELSE 0
				END AS Size,
				COALESCE(ColumnMap.att.value('@Identity','BIT'),0) AS IdentityColumn
		FROM @parmStaticData.nodes('/StaticData/Table/ColumnMap/Column') ColumnMap(att)
	)
	INSERT INTO @ColumnMapping(StartVersion,EndVersion,ColumnName,KeyValue,Updateable,DataType,Size,IdentityColumn)
	SELECT 
		CASE
			WHEN CHARINDEX(',',VersionInfo) > 0 THEN SUBSTRING(VersionInfo,1,CHARINDEX(',',VersionInfo)-1)
			ELSE VersionInfo
		END AS StartVersion,
		CASE
			WHEN CHARINDEX(',',VersionInfo) > 0 THEN SUBSTRING(VersionInfo,CHARINDEX(',',VersionInfo)+1,LEN(VersionInfo)-CHARINDEX(',',VersionInfo))
			ELSE NULL
		END AS EndVersion,
		ColumnName,
		KeyValue,
		Updateable,
		DataType,
		Size,
		IdentityColumn
	FROM 
		StaticData_CTE;

	/* Check for identity column */
	IF EXISTS(SELECT 1 FROM @ColumnMapping WHERE IdentityColumn = 1) 
		SET @IdentityColumn = 1;

	/* See if there is a lookup in needed */
	INSERT INTO #LookupTable(ColumnName,TableName,LookupColumnName)
	SELECT
		LookupColumn.att.value('@Column','VARCHAR(256)'),
		LookupColumn.att.value('./LookupTable[1]/@Schema','VARCHAR(128)')  + '.' + LookupColumn.att.value('./LookupTable[1]/@Name','VARCHAR(128)'),
		LookupColumn.att.value('./LookupColumn[1]/@Column','VARCHAR(256)')
	FROM				
		@parmStaticData.nodes('/StaticData/Table/LookupColumns/LookupColumn') LookupColumn(att)

	/* See if there is a lookup in needed */
	INSERT INTO #LookupWhere(TableID,ColumnName,DataType,Value,XMLLookup,Size)
	SELECT
		#LookupTable.TableID,
		LookupColumn.LookupColumn,
		LookupColumn.LookupDataType,
		LookupColumn.LookupValue,
		LookupColumn.XMLColumn,
		LookupColumn.LookupColumnSize
	FROM
		(
			SELECT
				LookupColumn.att.value('../@Column','VARCHAR(256)') AS ColumnName,
				LookupColumn.att.value('@Column','VARCHAR(256)') AS LookupColumn,
				LookupColumn.att.value('@DataType','VARCHAR(15)') AS LookupDataType,
				CASE 
					WHEN CHARINDEX('xml:',LookupColumn.att.value('@Value','VARCHAR(1024)'),1) = 0 THEN LookupColumn.att.value('@Value','VARCHAR(1024)')
					ELSE NULL
				END AS LookupValue,
				--REPLACE(LookupColumn.att.value('@Value','VARCHAR(1024)'),'xml:','') AS LookupValue,
				CASE
					WHEN CHARINDEX('xml:',LookupColumn.att.value('@Value','VARCHAR(1024)'),1) > 0 THEN REPLACE(LookupColumn.att.value('@Value','VARCHAR(1024)'),'xml:','')
					ELSE NULL
				END AS XMLColumn,
				COALESCE(LookupColumn.att.value('@Size','INT'),1024) AS LookupColumnSize
			FROM
				@parmStaticData.nodes('/StaticData/Table/LookupColumns/LookupColumn/LookupWhere') LookupColumn(att)
		) LookupColumn
		INNER JOIN #LookupTable ON #LookupTable.ColumnName = LookupColumn.ColumnName

	/* Remove columns that not within the the current system verison */
	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ColumnMapping) )
	BEGIN
		SELECT 	
			@StaticDataVersionStart = StartVersion, 
			@StaticDataVersionEnd = EndVersion
		FROM 	
			@ColumnMapping
		WHERE	
			RowID = @Loop;
		/* Break the static data start version down into it's parts */
		SELECT 	
			@ColumnVerStart = FullVersion
		FROM	
			RecHubConfig.udf_ParseSystemVersion(@StaticDataVersionStart);

		IF( @StaticDataVersionEnd IS NULL )
		BEGIN
			IF(	@ColumnVerStart > @SystemVer )
				DELETE FROM @ColumnMapping WHERE RowID = @Loop;
		END
		ELSE
		BEGIN
			SELECT 	
				@ColumnVerEnd = FullVersion
			FROM	
				RecHubConfig.udf_ParseSystemVersion(@StaticDataVersionEnd);

			IF( @ColumnVerEnd < @SystemVer )
				DELETE FROM @ColumnMapping WHERE RowID = @Loop;
		END
		SET @Loop = @Loop + 1;
	END

	/* Get the individual xml nodes (/StaticData/Data) from document passed in. */
	;WITH StaticDataValues_CTE AS
	(
		SELECT 
			COALESCE(StaticData.att.value('@Version', 'VARCHAR(64)'),'1.00.00.00') AS VersionInfo,
			StaticData.att.query('.') AS StaticData
		FROM   	
			@parmStaticData.nodes('/StaticData/Data') StaticData(att)
		
	),
	StaticDataValueVersions_CTE AS
	(
		SELECT 
			CASE
				WHEN CHARINDEX(',',VersionInfo) > 0 THEN SUBSTRING(VersionInfo,1,CHARINDEX(',',VersionInfo)-1)
				ELSE VersionInfo
			END AS StartVersion,
			CASE
				WHEN CHARINDEX(',',VersionInfo) > 0 THEN SUBSTRING(VersionInfo,CHARINDEX(',',VersionInfo)+1,LEN(VersionInfo)-CHARINDEX(',',VersionInfo))
				ELSE NULL
			END AS EndVersion,
			StaticData
		FROM 
			StaticDataValues_CTE
	)
	INSERT INTO #StaticDataValues(StartVersion,EndVersion,StaticData)
	SELECT 
		StartVersion,
		EndVersion,
		StaticData
	FROM 
		StaticDataValueVersions_CTE

	/* Remove static data rows that not within the the current system verison */
	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(RowID) FROM #StaticDataValues) )
	BEGIN
		SELECT 	
			@StaticDataVersionStart = StartVersion, 
			@StaticDataVersionEnd = EndVersion
		FROM 	
			#StaticDataValues
		WHERE	
			RowID = @Loop;

		/* Break the static data start version down into it's parts */
		SELECT 	
			@ColumnVerStart = FullVersion
		FROM	
			RecHubConfig.udf_ParseSystemVersion(@StaticDataVersionStart);

		IF( @StaticDataVersionEnd IS NULL )
		BEGIN
			IF(	@ColumnVerStart > @SystemVer )
				DELETE FROM #StaticDataValues WHERE RowID = @Loop;
		END
		ELSE
		BEGIN
			SELECT 	
				@ColumnVerEnd = FullVersion
			FROM	
				RecHubConfig.udf_ParseSystemVersion(@StaticDataVersionEnd);

			IF( @ColumnVerEnd < @SystemVer )
				DELETE FROM #StaticDataValues WHERE RowID = @Loop;
		END
		SET @Loop = @Loop + 1;
	END

	/* Add any lookup values */
	/* Get the rows with a "global" value */
	INSERT INTO #Lookup(TableID,ColumnName,TableName,LookupColumnName)
	SELECT 
		#LookupTable.TableID,
		#LookupTable.ColumnName,
		#LookupTable.TableName,
		#LookupTable.LookupColumnName 
	FROM 
		#LookupTable
		INNER JOIN #LookupWhere ON #LookupTable.TableID = #LookupWhere.TableID
	WHERE 
		#LookupWhere.Value IS NOT NULL

	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(TableID) FROM #LookupTable) )
	BEGIN
		/* Build the select statement to get the new attribute value from the lookup table */
		SELECT @NSQLCommand = N'SELECT @AttributeValue = CAST(' + LookupColumnName + N' AS VARCHAR(1024)) FROM ' + TableName + N' WHERE '
		FROM
			#Lookup
		WHERE
			TableID = @Loop;

		;WITH ColumnLookup AS
		(
			SELECT
				ColumnName,
				DataType,
				Value
			FROM
				#LookupWhere
			WHERE
				TableID = @Loop
		)
		SELECT 
			@NSQLCommand = @NSQLCommand + ColumnName + ' = ' + 
				CASE UPPER(DataType) 
					WHEN 'NVARCHAR' THEN CHAR(39) + Value + CHAR(39)
					WHEN 'VARCHAR' THEN CHAR(39) + Value + CHAR(39)
					ELSE Value
				END
			+ ' AND '
		FROM 
			ColumnLookup

		SET @NSQLCommand = SUBSTRING(@NSQLCommand,1,LEN(@NSQLCommand)-3);
 
		/* Get the new value to add to the XML */
		EXECUTE sp_ExecuteSQL @NSQLCommand,N'@AttributeValue VARCHAR(1024) OUTPUT',@AttributeValue = @AttributeValue OUT

		/* Build the statement to update the XML with the new attribute */
		SELECT @NSQLCommand = N'UPDATE #StaticDataValues SET StaticData.modify(' + CHAR(39) + N'insert attribute ' + ColumnName + N' {sql:variable("@AttributeValue")} as last into (/Data)[1]' + CHAR(39) + N')'
		FROM
			#Lookup
		WHERE
			TableID = @Loop;

		SET @Loop = @Loop + 1;
		/* Execute the update statement */
		EXECUTE sp_ExecuteSQL @NSQLCommand,N'@AttributeValue VARCHAR(1024)',@AttributeValue = @AttributeValue
	END

	/* Clear out the working lookup table */
	DELETE FROM #Lookup
	DBCC CHECKIDENT('#Lookup',RESEED) WITH NO_INFOMSGS

	/* Get the rows with a "row" value */
	INSERT INTO #Lookup(TableID,ColumnName,TableName,LookupColumnName)
	SELECT 
		#LookupTable.TableID,
		#LookupTable.ColumnName,
		#LookupTable.TableName,
		#LookupTable.LookupColumnName 
	FROM 
		#LookupTable
		INNER JOIN #LookupWhere ON #LookupTable.TableID = #LookupWhere.TableID
	WHERE 
		#LookupWhere.XMLLookup IS NOT NULL

	/* Add any lookup values, row by row */
	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(TableID) FROM #Lookup) )
	BEGIN
		SET @StaticDataLoop = 1;
		WHILE( @StaticDataLoop <= (SELECT MAX(RowID) FROM #StaticDataValues) )
		BEGIN
			SELECT 
				@StaticData = StaticData
			FROM
				#StaticDataValues
			WHERE 
				RowID = @StaticDataLoop;
			
			UPDATE 
				#LookupWhere
			SET 
				Value = NULL;

			SELECT @NSQLCommand = N'', @AttributeValue = NULL;

			;WITH ColumnLookup AS
			(
				SELECT
					ColumnName,
					DataType,
					Size,
					XMLLookup
				FROM
					#LookupWhere
				WHERE
					TableID = @Loop
			)
			SELECT @NSQLCommand = @NSQLCommand + 'UPDATE #LookupWhere SET Value = (SELECT StaticData.value(' + CHAR(39) + '/Data[1]/@' + XMLLookup + CHAR(39) + ',' + CHAR(39) + DataType +
				CASE DataType
					WHEN 'VARCHAR' THEN '(' + CAST(Size AS VARCHAR(10)) + ')'
					WHEN 'NVARCHAR' THEN '(' + CAST(Size AS VARCHAR(10)) + ')'
				END
				+ CHAR(39) + ') FROM #StaticDataValues WHERE RowID = ' + CAST(@StaticDataLoop AS VARCHAR(10)) + ') FROM #LookupWhere WHERE ColumnName = ' + CHAR(39) + ColumnName + CHAR(39) + ';'
			FROM
				ColumnLookup

			EXEC(@NSQLCommand);

			/* Build the select statement to get the new attribute value from the lookup table */
			SELECT @NSQLCommand = N'SELECT @AttributeValue = CAST(' + LookupColumnName + N' AS VARCHAR(1024)) FROM ' + TableName + N' WHERE '
			FROM
				#Lookup
			WHERE
				TableID = @Loop;

			;WITH ColumnLookup AS
			(
				SELECT
					ColumnName,
					DataType,
					Value
				FROM
					#LookupWhere
				WHERE
					TableID = @Loop
			)
			SELECT 
				@NSQLCommand = @NSQLCommand + ColumnName + ' = ' + 
					CASE UPPER(DataType) 
						WHEN 'NVARCHAR' THEN CHAR(39) + Value + CHAR(39)
						WHEN 'VARCHAR' THEN CHAR(39) + Value + CHAR(39)
						ELSE Value
					END
				+ ' AND '
			FROM 
				ColumnLookup

			SET @NSQLCommand = SUBSTRING(@NSQLCommand,1,LEN(@NSQLCommand)-3);

			/* Get the new value to add to the XML */
			EXECUTE sp_ExecuteSQL @NSQLCommand,N'@AttributeValue VARCHAR(1024) OUTPUT',@AttributeValue = @AttributeValue OUT

			IF( @AttributeValue IS NOT NULL )
			BEGIN
				/* Build the statement to update the XML with the new attribute */
				SELECT @NSQLCommand = N'UPDATE #StaticDataValues SET StaticData.modify(' + CHAR(39) + N'insert attribute ' + ColumnName + N' {sql:variable("@AttributeValue")} as last into (/Data)[1]' + CHAR(39) + N') WHERE RowID = @StaticDataLoop'
				FROM
					#LookupTable
				WHERE
					TableID = @Loop;
				/* Execute the update statement */
				EXECUTE sp_ExecuteSQL @NSQLCommand,N'@AttributeValue VARCHAR(1024),@StaticDataLoop INT',@AttributeValue = @AttributeValue, @StaticDataLoop = @StaticDataLoop;

				/* Build the statement to update the XML with the new attribute */
				SET @NSQLCommand = N'';
				;WITH ColumnLookup AS
				(
					SELECT
						XMLLookup
					FROM
						#LookupWhere
					WHERE
						TableID = @Loop
				)
				SELECT @NSQLCommand = @NSQLCommand + N'UPDATE #StaticDataValues SET StaticData.modify(' + CHAR(39) + N'delete /Data/@' + XMLLookup + CHAR(39) + N') WHERE RowID = @StaticDataLoop;'
				FROM
					ColumnLookup

				/* Execute the update statement */
				EXECUTE sp_ExecuteSQL @NSQLCommand,N'@StaticDataLoop INT', @StaticDataLoop = @StaticDataLoop;
			END
			SET @StaticDataLoop = @StaticDataLoop + 1;
		END
		SET @Loop = @Loop + 1;
	END

	/* Build out the StaticDataTable by adding the columns from the column map. */ 
	SET @SQLCommand = 'ALTER TABLE #StaticDataTable ADD ';
	SELECT 	@SQLCommand = @SQLCommand + ColumnName
			+CASE DataType
				WHEN 'VARCHAR' THEN ' VARCHAR(' + CAST(Size AS VARCHAR) + ') NULL'
				WHEN 'NVARCHAR' THEN ' NVARCHAR(' + CAST(Size AS VARCHAR) + ') NULL'
				WHEN 'INTEGER' THEN ' INT NULL'
				WHEN 'INT' THEN ' INT NULL'
				WHEN 'BIT' THEN ' BIT NULL'
				WHEN 'SMALLINT' THEN ' SMALLINT NULL'
				WHEN 'TINYINT' THEN ' TINYINT NULL'
				WHEN 'DATETIME' THEN ' DATETIME NULL'
				ELSE ' VARCHAR(64) NULL'
			END
			+ ','
	FROM 	@ColumnMapping;

	IF( @@ROWCOUNT > 0 )
	BEGIN /* IF @@ROWCOUNT */
		/* Clean up the SQL command I.E. remove the last , in the string. */
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
		/* Execute the Alter that was build */
		EXEC (@SQLCommand);

		/* Insert the parsed XML data into the temp table, each column/data type exists so it is 'real' at this point. */
		SET @SQLCommand = 'INSERT INTO #StaticDataTable([Version],ActionCode,';
		SELECT 	@SQLCommand = @SQLCommand + ColumnName + ','
		FROM	@ColumnMapping;
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
		SET @SQLCommand = @SQLCommand + ') ';
		SET @SQLCommand = @SQLCommand + 'SELECT COALESCE(StaticData.att.value(' + QUOTENAME('@Version',CHAR(39)) + ',' + QUOTENAME('VARCHAR(64)',CHAR(39)) + '),'+QUOTENAME('1.00.00.00',CHAR(39)) + '),COALESCE(StaticData.att.value('+QUOTENAME('@ActionCode',CHAR(39))+ ',' + QUOTENAME('CHAR(1)',CHAR(39)) + '),' + QUOTENAME('I',CHAR(39))+'),';
		SELECT 	@SQLCommand = @SQLCommand 
				+CASE DataType
					WHEN 'VARCHAR' THEN 'REPLACE('
					WHEN 'NVARCHAR' THEN 'REPLACE('
					ELSE ''
				END
				+'StaticData.att.value(' + CHAR(39) + '@' + ColumnName +  CHAR(39) + ','
				+CASE DataType
					WHEN 'VARCHAR' THEN CHAR(39) + 'VARCHAR(' + CAST(Size AS VARCHAR) + ')' + CHAR(39)
					WHEN 'NVARCHAR' THEN CHAR(39) + 'NVARCHAR(' + CAST(Size AS VARCHAR) + ')' + CHAR(39)
					WHEN 'INTEGER' THEN QUOTENAME('INT',CHAR(39))
					WHEN 'INT' THEN QUOTENAME('INT',CHAR(39))
					WHEN 'BIT' THEN QUOTENAME('BIT',CHAR(39))
					WHEN 'SMALLINT' THEN QUOTENAME('SMALLINT',CHAR(39))
					WHEN 'TINYINT' THEN QUOTENAME('TINYINT',CHAR(39))
					WHEN 'DATETIME' THEN QUOTENAME('DATETIME', CHAR(39))
					ELSE QUOTENAME('VARCHAR(512) NULL',CHAR(39))
				END
				+ ')'
				+CASE DataType
					WHEN 'VARCHAR' THEN ',' + QUOTENAME('&amp;',CHAR(39)) + ',' + QUOTENAME('&',CHAR(39)) + ')'
					WHEN 'NVARCHAR' THEN ',' + QUOTENAME('&amp;',CHAR(39)) + ',' + QUOTENAME('&',CHAR(39)) + ')'
					ELSE ''
				END
				+','
		FROM	
			@ColumnMapping;

		/* Clean up the SQL command I.E. remove the last , in the string. */
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
		/* Add in the name of the table and cross apply so we get the individual parts. */
		SET @SQLCommand = @SQLCommand + ' FROM #StaticDataValues CROSS APPLY StaticData.nodes('+QUOTENAME('/Data',CHAR(39))+') StaticData(att)';
		/* Execute the insert command */
		EXEC (@SQLCommand);
		/* Build out the column list for later use. */
		SET @ColumnList = '';
		SELECT 	@ColumnList = @ColumnList + ColumnName + ', '
		FROM 	@ColumnMapping
		SET @ColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);
		
		/* If LoadDate is on the table, add it to the column list */
		IF( @CreationDate = 1 )
			SET @ColumnList = @ColumnList + ', CreationDate';
		IF( @CreatedBy = 1 )
			SET @ColumnList = @ColumnList + ', CreatedBy';
		IF( @ModificationDate = 1 )
			SET @ColumnList = @ColumnList + ', ModificationDate';
		IF( @ModifiedBy = 1 )
			SET @ColumnList = @ColumnList + ', ModifiedBy';

		/* Wish there was a better way, but each row has to be checked against the current DB version 
			and updates/deletes have to be account for as well */
		SET @Loop = 1;
		WHILE( @Loop <= (SELECT MAX(RowID) FROM #StaticDataTable) )
		BEGIN /* WHILE @Loop */
			SELECT 	@ActionCode = ActionCode,
					@StaticDataVersion = [Version]
			FROM 	#StaticDataTable
			WHERE	RowID = @Loop;
			/* Break the static data version down into it's parts */
			SELECT 	@DataVer = FullVersion
			FROM	RecHubConfig.udf_ParseSystemVersion(@StaticDataVersion);

			/* If the static data version is the same or lower in value, then work the row, else skip it. */
			IF( @DataVer <= @SystemVer )
			BEGIN /* IF @DataVer <= @SystemVer */
				IF( @ActionCode = 'I' )
				BEGIN /* Insert or update the record. */
					/* First build an insert command that will only be execute if the row does not exist in the static data table. */
					SET @SQLCommand = 'INSERT INTO ' + @FullTableName + ' (' + @ColumnList + ') SELECT '
					SELECT 	@SQLCommand = @SQLCommand + '#StaticDataTable.' + ColumnName + ','
					FROM 	@ColumnMapping
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)

					IF( @CreationDate = 1 )
						SET @SQLCommand = @SQLCommand + ',CreationDate = GETDATE()';
					IF( @CreatedBy = 1 )
						SET @SQLCommand = @SQLCommand + ',CreatedBy = SUSER_SNAME()';
					IF( @ModificationDate = 1 )
						SET @SQLCommand = @SQLCommand + ',ModificationDate = GETDATE()';
					IF( @ModifiedBy = 1 )
						SET @SQLCommand = @SQLCommand + ',ModifiedBy = SUSER_SNAME()';

					SET @SQLCommand = @SQLCommand + ' FROM #StaticDataTable  LEFT JOIN ' + @FullTableName +' ON';
					SELECT 	@SQLCommand = @SQLCommand + ' #StaticDataTable.' + ColumnName + ' = ' + @FullTableName + '.' + ColumnName + ' AND'
					FROM 	@ColumnMapping
					WHERE	KeyValue = 1;
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
					SET @SQLCommand = @SQLCommand + ' WHERE '
					SELECT 	@SQLCommand = @SQLCommand + @FullTableName + '.' + ColumnName + ' IS NULL AND '
					FROM	@ColumnMapping
					WHERE 	KeyValue = 1;
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4);
					SET @SQLCommand = @SQLCommand + ' AND RowID = ' + CAST(@Loop AS VARCHAR);

					/* If there is an identity column, wrap the INSERT statement with identity column controls */
					IF( @IdentityColumn = 1 )
						SET @SQLCommand = 'SET IDENTITY_INSERT ' + @FullTableName + ' ON ' + @SQLCommand + 'SET IDENTITY_INSERT ' + @FullTableName + ' OFF';

					EXEC(@SQLCommand);
					IF( @@ROWCOUNT = 0 )
					BEGIN /* 0 rows where returned, so the record existed. Do an update if allowed. */
						/* Check to see if any fields are different before doing the update. */
						SELECT @SQLCommand = 'IF NOT EXISTS(SELECT 1 FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON';
						SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
						FROM 	@ColumnMapping;
						/* remove the extra space + AND */
						SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4);
						SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR);

						/* Add the update to the IF NOT EXISTS statement so the update will only be done if something is different. */
						SELECT @SQLCommand = @SQLCommand + ') UPDATE SD SET ';
						SELECT 	@SQLCommand = @SQLCommand + 'SD.' + ColumnName + ' = TSD.' + ColumnName + ','
						FROM	@ColumnMapping
						WHERE	Updateable = 1;
						IF( @@ROWCOUNT > 0 )
						BEGIN /* Updateable columns exist, if 0 was returned no updateable columns so nothing happens. */
							SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1); /* remove the trailing , */
							IF @CreationDate = 1
								SET @SQLCommand = @SQLCommand + ', CreationDate = GETDATE()';
							IF @CreatedBy = 1
								SET @SQLCommand = @SQLCommand + ', CreatedBy = SUSER_SNAME()';
							IF @ModificationDate = 1
								SET @SQLCommand = @SQLCommand + ', ModificationDate = GETDATE()';
							IF @ModifiedBy = 1
								SET @SQLCommand = @SQLCommand + ', ModifiedBy = SUSER_SNAME()';
							SET @SQLCommand = @SQLCommand + ' FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON'
							SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
							FROM 	@ColumnMapping
							WHERE	KeyValue = 1;
							/* remove the extra space + AND */
							SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4);
							SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR);
							EXEC(@SQLCommand);
						END
					END
				END /* Insert or update the record. */
				ELSE IF( @ActionCode = 'D' )
				BEGIN /* Delete the record */
					SET @SQLCommand = 'DELETE '+ @FullTableName + ' FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON';
					SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
					FROM 	@ColumnMapping
					WHERE	KeyValue = 1;
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4);
					SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR);
					EXEC(@SQLCommand);
				END
			END /* IF @DataVer <= @SystemVer */
			SET @Loop = @Loop + 1;
		END	/* WHILE @Loop */
	END /* IF @@ROWCOUNT */

	/* Remove the temp tables that were used. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupTable')) 
		DROP TABLE #LookupTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupWhere')) 
		DROP TABLE #LookupWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Lookup')) 
		DROP TABLE #Lookup;
	PRINT 'The database update succeeded';
END TRY
BEGIN CATCH
	/* Remove the temp tables that were used. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupTable')) 
		DROP TABLE #LookupTable;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#LookupWhere')) 
		DROP TABLE #LookupWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Lookup')) 
		DROP TABLE #Lookup;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
