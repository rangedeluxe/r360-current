--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractSchedules_GetActiveList
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractSchedules_GetActiveList') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractSchedules_GetActiveList
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractSchedules_GetActiveList 
	
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Retreive list of active extract schedule records.
*
*
* Modification History
* 03/13/2013 WI 97761 DRP  Created
* 03/29/2013 WI 103463 DRP Added ExtractRunArguments
* 05/07/2014 WI 140385 JBS Adding Columns DayInMonth, ScheduleType 
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT RecHubConfig.ExtractSchedules.ExtractScheduleId,
		   RecHubConfig.ExtractSchedules.ExtractDefinitionId,
           RecHubConfig.ExtractSchedules.ModificationDate,
           RecHubConfig.ExtractSchedules.DaysOfWeek,
		   RecHubConfig.ExtractSchedules.DayInMonth,
           RecHubConfig.ExtractSchedules.ScheduleTime,
		   RecHubConfig.ExtractSchedules.ScheduleType,
           RecHubConfig.ExtractSchedules.ExtractRunArguments,
           RecHubConfig.ExtractSchedules.[Description]
    FROM   RecHubConfig.ExtractSchedules INNER JOIN RecHubConfig.ExtractDefinitions 
	    ON RecHubConfig.ExtractDefinitions.ExtractDefinitionId = RecHubConfig.ExtractSchedules.ExtractDefinitionId
    WHERE 
           RecHubConfig.ExtractSchedules.IsActive = 1 AND RecHubConfig.ExtractDefinitions.IsActive = 1;
	
END TRY

BEGIN CATCH
		EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH

