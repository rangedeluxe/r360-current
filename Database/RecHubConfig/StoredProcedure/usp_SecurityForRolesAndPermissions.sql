--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_SecurityForRolesAndPermissions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_SecurityForRolesAndPermissions') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_SecurityForRolesAndPermissions
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_SecurityForRolesAndPermissions
(
    @parmSchemaName	  VARCHAR(128)	
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/15/2013
*
* Purpose:	Build the database role and assign permissions to exec all 
*			procedures and functions within the Schema name used as input parameter. 
*
* Modification History
* 03/15/2013 WI 92364 JBS	Created
* 08/25/2014 WI 161464 RDS	Updated to work with scalar functions 
******************************************************************************/
SET NOCOUNT ON;

DECLARE @SQLCommand VARCHAR(2000);
DECLARE @parmRoleName VARCHAR(40);
DECLARE @parmLoopCount INT;

SET @parmRoleName = 'dbRole_'+@parmSchemaName
SET @parmLoopCount = 0

-- Informational display
RAISERROR('Running procedure for %s',10,1,@parmSchemaName) WITH NOWAIT;

BEGIN TRY

	IF @parmSchemaName IS NULL								-- If the variable is not supplied quit run, display error
		RAISERROR ('Expecting Schema Name as input',16,1);
	IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE [Name] = @parmRoleName)
		BEGIN
			SET @SQLCommand = 'CREATE ROLE '+@parmRoleName
			EXEC(@SQLCommand)
		END;
 
	IF OBJECT_ID('tempdb.dbo.#tempTable') IS NOT NULL
		BEGIN 
			DROP TABLE dbo.#tempTable;
		END;

	CREATE TABLE #tempTable 
	(
		ID INT IDENTITY (1,1),
		ObjectType TINYINT,
		ObjectName VARCHAR(128)
	);
		
	INSERT INTO #tempTable									-- Load table with all stored procs and functions in schema
	(	
		ObjectType,
		ObjectName
	) 
	(	
	SELECT	CASE
				WHEN ROUTINE_TYPE = 'PROCEDURE' THEN 0
				WHEN OBJECTPROPERTY(sys.sql_modules.object_id, 'IsScalarFunction') = 1 THEN 0
				ELSE 1
			END,
			ROUTINE_NAME
	FROM	INFORMATION_SCHEMA.ROUTINES
		LEFT JOIN sys.sql_modules
			on INFORMATION_SCHEMA.ROUTINES.ROUTINE_NAME = OBJECT_NAME(sys.sql_modules.object_id)
	WHERE	ROUTINE_SCHEMA = @parmSchemaName
			AND (ROUTINE_TYPE = 'PROCEDURE' OR ROUTINE_TYPE = 'FUNCTION')
	);

	SET @parmLoopCount = (Select MAX(ID) from #tempTable);  -- set counter

	WHILE @parmLoopCount > 0								--  Work through list building Grant command for each object	
	BEGIN
		Select @SQLCommand = 'GRANT ' +
			CASE ObjectType	
				WHEN 0 THEN 'EXECUTE'
				ELSE 'SELECT'
			END + ' ON '
		+ @parmSchemaName + '.'	+ ObjectName
		+ ' TO ' + @parmRoleName
		FROM #tempTable 
		WHERE @parmLoopCount = ID;

		/* for logging during install */
		RAISERROR(@SQLCommand,10,1) WITH NOWAIT;
		EXEC(@SQLCommand);

		SET @parmLoopCount = @parmLoopCount - 1
		CONTINUE
	END;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
