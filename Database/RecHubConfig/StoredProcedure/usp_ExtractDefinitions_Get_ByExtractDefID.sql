--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Get_ByExtractDefID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Get_ByExtractDefID') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Get_ByExtractDefID
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Get_ByExtractDefID
(
	@parmExtractDefID	INT,
	@parmUserID		INT,
	@parmExtractName	VARCHAR(128) OUT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Get Extract file (xml) by its ID
*
* Modification History
* 05/13/2013  WI  105078	EAS	Created
* 08/15/2014  WI  158890    BLR Added download audit.
* 10/20/2014  WI  173160    BLR Updated event audit names.
* 10/22/2014  WI  173694    BLR Missed a comma, causing failures on install.
* 11/13/2014  WI  175767    BLR Added FileName to the return clause.
* 07/06/2017  PT  2677      CEJ Convert ExtractDefinition from varchar max to varbinary max
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @extractName	VARCHAR(128);
	DECLARE @extractFileName VARCHAR(128);
	DECLARE @auditMessage	VARCHAR(1024);
	DECLARE @extractXML		VARCHAR(MAX);

	-- Set OUT Extract Name parameter to name file.
	SELECT
		@parmExtractName = RecHubConfig.ExtractDefinitions.ExtractName,
		@extractXML = Convert(VARCHAR(MAX), RecHubConfig.ExtractDefinitions.ExtractDefinition),
		@extractFileName = RecHubConfig.ExtractDefinitions.ExtractFilename
	FROM 
		RecHubConfig.ExtractDefinitions
	WHERE 
		RecHubConfig.ExtractDefinitions.ExtractDefinitionID = @parmExtractDefID
	
	-- Need to audit the Download.
	SET @auditMessage = 'User Downloaded Extract Definition:'
		+ ' Extract Name = ' + @parmExtractName
		+ ', Extract ID = ' + CAST(@parmExtractDefID AS VARCHAR(10)) 
		+ '.';
	EXEC RecHubCommon.usp_WFS_EventAudit_ins
		@parmUserID				= @parmUserID,
		@parmApplicationName	= 'RecHubConfig.usp_ExtractDefinitions_Get_ByExtractDefID',
		@parmEventName		= 'Extract Definition Downloaded',
		@parmEventType		= 'Download Data',
		@parmAuditMessage	= @auditMessage;

	-- Finally return our results.
	SELECT @parmExtractName as ExtractName, 
		   @extractXML as ExtractDefinition,
		   @extractFileName as ExtractFileName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH