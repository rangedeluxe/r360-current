--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SystemSetup_Get_BySection
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_SystemSetup_Get_BySection') IS NOT NULL
	DROP PROCEDURE RecHubConfig.usp_SystemSetup_Get_BySection
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_SystemSetup_Get_BySection 
(
	@parmSection VARCHAR(30)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 05/16/2013
*
* Purpose: Query System Setup by Section 
*
* Modification History
* 05/16/2013 WI 102720 CRG Purge Development - Admin Data Access layer for System Setup DAL
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		RecHubConfig.SystemSetup.Section,
		RecHubConfig.SystemSetup.SetupKey,
		RecHubConfig.SystemSetup.Value,
		RecHubConfig.SystemSetup.[Description],
		RecHubConfig.SystemSetup.DataType,
		RecHubConfig.SystemSetup.ValidValues
	FROM	
		RecHubConfig.SystemSetup
	WHERE
		RecHubConfig.SystemSetup.Section = @parmSection;		
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;

