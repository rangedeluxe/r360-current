--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ReportConfiguration_Get_ByReportID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ReportConfiguration_Get_ByReportID') IS NOT NULL
	DROP PROCEDURE RecHubConfig.usp_ReportConfiguration_Get_ByReportID;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ReportConfiguration_Get_ByReportID
(
      @parmReportID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the Report Configuration
*
* Modification History
* Created
* 05/06/2013 WI 107231 KLC	Created.
* 07/02/2014 WI 151525 KLC	Updated to support RAAM integration
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		RecHubConfig.ReportConfigurations.ReportID,
		RecHubConfig.ReportConfigurations.GroupID,
		RecHubConfig.ReportConfigurations.OrderValue,
		RecHubConfig.ReportConfigurations.RAAMResourceName,
		RecHubConfig.ReportConfigurations.ReportName,
		RecHubConfig.ReportConfigurations.XmlParameterName,
		RecHubConfig.ReportConfigurations.ReportFileName
	FROM
		RecHubConfig.ReportConfigurations
	WHERE
		RecHubConfig.ReportConfigurations.ReportID = @parmReportID;

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH