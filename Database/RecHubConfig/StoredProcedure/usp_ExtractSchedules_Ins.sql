--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractSchedules_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractSchedules_Ins') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractSchedules_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractSchedules_Ins
( 
	@ExtractDefinitionId  BIGINT,
	@Description          VARCHAR(255), 
    @IsActive             BIT,
    @DaysOfWeek           VARCHAR(7),
	@DayInMonth           INT,
    @ScheduleTime         TIME,
	@ScheduleType         INT,
    @ExtractRunArguments  VARCHAR(255),
    @UserID               BIGINT,
    @ExtractScheduleId    BIGINT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Insert one extract schedule record.
*
*
* Modification History
* 03/13/2013 WI 97717 DRP  Created
* 03/29/2013 WI 103460 DRP Added ExtractRunArguments
* 03/31/2013 WI 103458 DRP Added Data Audits
* 05/07/2014 WI 140394 JBS Adding Columns DayInMonth, ScheduleType 
* 09/05/2014 WI 163547 BLR Added schedule data in audit message
******************************************************************************/
SET NOCOUNT ON;
DECLARE
    @Key            VARCHAR(255),
	@AuditMessage	VARCHAR(1024);

BEGIN TRY
	BEGIN TRANSACTION ExtractSchedules_Add
	
	INSERT INTO RecHubConfig.ExtractSchedules 
	(
		ExtractDefinitionId,
		[Description],
		IsActive,
		DaysOfWeek,
		DayInMonth,
		ScheduleTime,
		ScheduleType,
		ExtractRunArguments,
		CreationDate,
		ModificationDate,
		CreatedBy,
		ModifiedBy
    )
    VALUES 
	(
		@ExtractDefinitionId,
		@Description,
		@IsActive,
		@DaysOfWeek,
		@DayInMonth,
		@ScheduleTime,
		@ScheduleType,
		@ExtractRunArguments,
		GETDATE(), --CreationDate
		GETDATE(), --ModificationDate
		SUSER_SNAME(), --CreatedBy
		SUSER_SNAME() --ModifiedBy
    );
    SELECT @ExtractScheduleId = SCOPE_IDENTITY() 
    SET @AuditMessage = 'Added Extract schedule id: ' + CAST(@ExtractScheduleId AS VARCHAR(10)) + ' for ExtractDefinition: ' 
		+ CAST(@ExtractDefinitionId AS VARCHAR(10)) + '. Description:' + @Description
		+ ', IsActive:' + CAST(@IsActive AS VARCHAR(1))
		+ ', DaysOfWeek:' + @DaysOfWeek
		+ ', ScheduleTime:' + CAST(@ScheduleTime AS VARCHAR(5))
		+ ', ScheduleType:' + CAST(@ScheduleType AS VARCHAR(1))
		+ ', ExtractRunArguments:' + @ExtractRunArguments;

	SELECT @Key = CAST(@ExtractScheduleId AS VARCHAR);
		
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 	
		@parmUserID					= @UserID,
		@parmApplicationName		= 'RecHubConfig.usp_ExtractSchedules_Ins',
		@parmSchemaName				= 'RecHubConfig',
		@parmTableName				= 'ExtractSchedules',
		@parmColumnName				= 'ExtractScheduleID',
		@parmAuditValue				= @Key,
		@parmAuditType				= 'INS',
		@parmAuditMessage			= @AuditMessage
		
    COMMIT TRANSACTION ExtractSchedules_Add;
END TRY

BEGIN CATCH
        IF XACT_STATE() != 0 ROLLBACK TRANSACTION ExtractSchedules_Add;
		EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH



