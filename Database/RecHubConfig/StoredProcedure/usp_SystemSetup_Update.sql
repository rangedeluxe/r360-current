--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_SystemSetup_Update
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_SystemSetup_Update') IS NOT NULL
	DROP PROCEDURE RecHubConfig.usp_SystemSetup_Update
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_SystemSetup_Update 
(
	@parmSection	VARCHAR(30),
	@parmSetupKey	VARCHAR(50),
	@parmValue		VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/16/15
*
* Purpose: Update system setup record. 
*
* Modification History
* 04/16/15 WI 202345 JPB Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( @parmSection IS NULL )
		RAISERROR('A value for @parmSection must be included',16,1);

	IF( @parmSetupKey IS NULL )
		RAISERROR('A value for @parmSetupKey must be included',16,1);
	
	IF( EXISTS (SELECT 1 FROM RecHubConfig.SystemSetup WHERE Section = @parmSection AND SetupKey = @parmSetupKey) )
	BEGIN
		UPDATE 
			RecHubConfig.SystemSetup
		SET
			Value = @parmValue,
			ModificationDate =  GETDATE(),
			ModifiedBy = SUSER_SNAME()
		WHERE 
			Section = @parmSection
			AND SetupKey = @parmSetupKey;
	END
	ELSE
		RAISERROR('Section %s, SetupKey %s could not be found.',16,1,@parmSection,@parmSetupKey);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;

