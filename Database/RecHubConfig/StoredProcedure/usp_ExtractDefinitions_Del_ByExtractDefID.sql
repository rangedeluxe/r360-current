--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Del_ByExtractDefID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Del_ByExtractDefID') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Del_ByExtractDefID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Del_ByExtractDefID 
(
       @parmUserID            INT,
	   @parmExtractDefID      INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Delete Extract Definition
*
* Modification History
* 05/13/2013 WI  97653	EAS	Created
* 06/18/2014 WI  148552 DJW
*    Update to handle logging
*    Verify no schedules exist for the defintion, before it is deleted.
* 07/06/2017 PT  2677   CEJ Convert ExtractDefinition from varchar max to varbinary max
******************************************************************************/

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF EXISTS(SELECT 1 FROM RecHubConfig.ExtractSchedules WHERE RecHubConfig.ExtractSchedules.ExtractDefinitionId = @parmExtractDefID)
	BEGIN
		DECLARE @errorDescription VARCHAR(1024);
		SET @errorDescription = 'Unable to delete ExtractDefinition ID (' + ISNULL(CAST(@parmExtractDefID AS VARCHAR(10)), 'NULL') + ') because it has associated schedules. All schedules for this Extract Definition must be deleted before the definition can be deleted.';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Auditing Info: Get the row info, before it is deleted.

	DECLARE @parmExtractName	VARCHAR(128),
	@parmExtractDefSizeKb		INT,
	@parmExtractDefinition		VARBINARY(MAX),
	@parmExtractDefinitionType  INT,
	@parmExtractDescription		VARCHAR(255),
	@parmExtractIsActive		BIT,
	@parmExtractFilename		VARCHAR(128);

   SELECT 
	    @parmExtractDescription = RecHubConfig.ExtractDefinitions.[Description],
		@parmExtractIsActive = RecHubConfig.ExtractDefinitions.IsActive,
		@parmExtractDefinition = RecHubConfig.ExtractDefinitions.ExtractDefinition,
		@parmExtractName = RecHubConfig.ExtractDefinitions.ExtractName,
		@parmExtractDefSizeKb = RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb,
		@parmExtractDefinitionType = RecHubConfig.ExtractDefinitions.ExtractDefinitionType,
		@parmExtractFilename = RecHubConfig.ExtractDefinitions.ExtractFilename
	FROM	
		RecHubConfig.ExtractDefinitions
	WHERE
	  	RecHubConfig.ExtractDefinitions.ExtractDefinitionID = @parmExtractDefID

	-- see if we actually have a record to delete, before we do any calls for auditing
	IF @parmExtractDescription IS NOT NULL 
	BEGIN


	DECLARE @auditMessage	VARCHAR(1024);
	SET @auditMessage = 'DELETED ExtractDefinition:'
		+ ' Extract Name = ' + @parmExtractName
		+ ', Extract Description = ' + @parmExtractDescription
		+ ', Extract IsActive = ' + CAST(@parmExtractIsActive as VARCHAR(1))
		+ ', Extract Def Size(kb) = ' + CAST(@parmExtractDefSizeKb AS VARCHAR(10)) 
		+ ', Extract Definition Type = ' + CAST(ISNULL(@parmExtractDefinitionType,-1) AS VARCHAR(10))
		+ ', Extract Filename = ' + ISNULL(@parmExtractFilename,'')
		+ ', Definition Contents = ' + CAST(@parmExtractDefinition AS NVARCHAR(max))
		+ '.';

		-- End Getting Audit Info

	DELETE
	FROM	
		RecHubConfig.ExtractDefinitions
	WHERE
	  	RecHubConfig.ExtractDefinitions.ExtractDefinitionID = @parmExtractDefID;


	EXEC RecHubCommon.usp_WFS_DataAudit_ins
		@parmUserID				= @parmUserID,
		@parmApplicationName	= 'RecHubConfig.usp_ExtractDefinitions_Del_ByExtractDefID',
		@parmSchemaName			= 'RecHubConfig',
		@parmTableName			= 'ExtractDefinitions',
		@parmColumnName			= 'ExtractDefinitionID',
		@parmAuditValue			= @parmExtractDefID,
		@parmAuditType			= 'DEL',
		@parmAuditMessage		= @auditMessage;

	END  -- 	IF @parmExtractDescription IS NOT NULL 
	

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
