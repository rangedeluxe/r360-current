--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_SystemSetup_Get_BySetupKey
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubConfig.usp_SystemSetup_Get_BySetupKey') IS NOT NULL
	DROP PROCEDURE RecHubConfig.usp_SystemSetup_Get_BySetupKey
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_SystemSetup_Get_BySetupKey
(
	@parmSection	VARCHAR(30),
	@parmSetupKey	VARCHAR(50)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103843 JMC   Initial Version
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT RecHubConfig.SystemSetup.Value 
	FROM RecHubConfig.SystemSetup 
	WHERE 
		RecHubConfig.SystemSetup.Section = @parmSection 
		AND RecHubConfig.SystemSetup.SetupKey = @parmSetupKey;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
