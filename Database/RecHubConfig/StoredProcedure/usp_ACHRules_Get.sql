--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_ACHRules_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ACHRules_Get') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ACHRules_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ACHRules_Get 
(
	@parmDDARuleListTable RecHubConfig.DDAListTable READONLY
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Chris Colombo
* Date: 07/16/2018
*
* Purpose: Looking up ACH Rules 
*	
*
* Modification History
* 07/16/2018 PT 157791221  CMC	Created
* 08/02/2018 PT 157791221  MGE	Strip leading zeroes from DDA # so join is effective
* 06/25/2019 PT R360-15470 MGE	Fix the logic that strip leading zeroes and also now strip spaces
******************************************************************************/
SET ARITHABORT ON;

BEGIN TRY
	
		
	SELECT
		RecHubConfig.ACHAddendaSegments.AddendaSegmentName, 
		RecHubConfig.ACHAddendaSegments.SegmentIdentifier, 
		RecHubConfig.ACHAddendaDataEntryMaps.FieldName, 
		RecHubConfig.ACHAddendaDataEntryMaps.FieldNumber, 
		RecHubConfig.ACHAddendaDataEntryMaps.SegmentQualifier, 
		RecHubConfig.ACHAddendaDataEntryMaps.IsCheck, 
		RecHubConfig.ACHAddenda.SegmentDelimiter, 
		RecHubConfig.ACHAddenda.FieldDelimiter, 
		RecHubConfig.ACHAddenda.AddendaName,
		RecHubConfig.ACHAddendaSegments.CreateNewRecord,
		RecHubData.dimDDAs.DDA,
		RecHubData.dimDDAs.ABA
	FROM 
		RecHubData.dimDDAs  
		INNER JOIN @parmDDARuleListTable DDAList ON RecHubData.dimDDAs.ABA = DDAList.ABA 
			AND SUBSTRING(LTRIM(RecHubData.dimDDAs.DDA),PATINDEX('%[^0]%', LTRIM(RecHubData.dimDDAs.DDA)),LEN(RecHubData.dimDDAs.DDA)) = SUBSTRING(LTRIM(DDAList.DDA),PATINDEX('%[^0]%', LTRIM(DDAList.DDA)),LEN(DDAList.DDA))
		INNER JOIN RecHubData.ElectronicAccounts ON RecHubData.ElectronicAccounts.DDAKey = RecHubData.dimDDAs.DDAKey
		INNER JOIN RecHubConfig.ACHAddendaWorkgroups ON RecHubConfig.ACHAddendaWorkgroups.SiteBankID = RecHubData.ElectronicAccounts.SiteBankID
			AND RecHubConfig.ACHAddendaWorkgroups.SiteClientAccountID = RecHubData.ElectronicAccounts.SiteClientAccountID
		INNER JOIN RecHubConfig.ACHAddenda ON RecHubConfig.ACHAddenda.ACHAddendaKey = RecHubConfig.ACHAddendaWorkgroups.ACHAddendaKey
		INNER JOIN RecHubConfig.ACHAddendaSegments ON RecHubConfig.ACHAddendaSegments.ACHAddendaKey = RecHubConfig.ACHAddenda.ACHAddendaKey
		INNER JOIN RecHubConfig.ACHAddendaDataEntryMaps ON RecHubConfig.ACHAddendaDataEntryMaps.ACHAddendaSegmentKey = RecHubConfig.ACHAddendaSegments.ACHAddendaSegmentKey
	ORDER BY 
		RecHubConfig.ACHAddendaDataEntryMaps.FieldNumber ASC;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH