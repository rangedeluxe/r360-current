--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractSchedules_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractSchedules_Del') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractSchedules_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractSchedules_Del
( 
	@ExtractScheduleId    BIGINT = NULL,
	@ExtractDefinitionId  BIGINT = NULL,
	@UserID               BIGINT,
	@NumberOfRecords      INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Delete one extract schedule record.
*
*
* Modification History
* 03/13/2013 WI 97718 DRP  Created
* 03/31/2013 WI 103456 DRP Added Data Audits
******************************************************************************/
SET NOCOUNT ON;

DECLARE
    @Key            VARCHAR(255),
	@AuditMessage	VARCHAR(1024);
	
BEGIN TRY

	BEGIN TRANSACTION ExtractSchedules_Del
	
	DELETE 
    FROM RecHubConfig.ExtractSchedules
    WHERE (@ExtractScheduleId IS NULL OR @ExtractScheduleId = RecHubConfig.ExtractSchedules.ExtractScheduleId) AND
          (@ExtractDefinitionId IS NULL OR @ExtractDefinitionId = RecHubConfig.ExtractSchedules.ExtractDefinitionId)

    SELECT @NumberOfRecords = @@ROWCOUNT	 
   
    SET @AuditMessage = 'Deleted Extract schedule id: ' + ISNULL(CAST(@ExtractScheduleId AS VARCHAR(10)), 'null') + 
                        ' for ExtractDefinition: ' + ISNULL(CAST(@ExtractDefinitionId AS VARCHAR(10)), 'null') + ' and deleted '
                        + CAST(@NumberOfRecords AS VARCHAR(10)) + ' records.';
	SELECT @Key = CAST(@ExtractScheduleId AS VARCHAR);
	
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 	
		@parmUserID					= @UserID,
		@parmApplicationName		= 'RecHubConfig.usp_ExtractSchedules_Del',
		@parmAuditType				= 'DEL',
	    @parmSchemaName				='RecHubConfig',
	    @parmTableName				='ExtractSchedules',
	    @parmColumnName				='ExtractScheduleID',
	    @parmAuditValue				= @Key,
		@parmAuditMessage			= @AuditMessage
		
    COMMIT TRANSACTION ExtractSchedules_Add;
    	
END TRY

BEGIN CATCH
        IF XACT_STATE() != 0 ROLLBACK TRANSACTION ExtractSchedules_Del;
		EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH


