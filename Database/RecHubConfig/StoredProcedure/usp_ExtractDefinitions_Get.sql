--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Get') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Get
(
	@parmStartRecord			INT,
	@parmRecordsPerPage			INT = 25,
	@parmActiveOnly             BIT = 1,
	@parmTotalRecords			INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Get all Extracts with paging
*
* Modification History
* 05/13/2013  WI 97650	EAS	Created
* 06/26/2013  WI 107410 DRP Added optional param to get All or get All Active definitions.
* 05/07/2014  WI 140378 JBS Adding Columns ExtractDefinitionType, ExtractFilename
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;

	CREATE TABLE #PagedResults
	(
		RecID			INT,
		ExtractID		INT,
		TotalRecords	INT,
		Name			VARCHAR(128),
		SizeKB			INT,
		ExtractType		INT,
		ExtractFileName VARCHAR(128)
	)

	;WITH PageRecords AS
	(
		SELECT	ROW_NUMBER() OVER 
						(
							ORDER BY RecHubConfig.ExtractDefinitions.ExtractDefinitionID ASC
						) AS RecID,
				RecHubConfig.ExtractDefinitions.ExtractDefinitionID AS ExtractID,
				RecHubConfig.ExtractDefinitions.ExtractName AS Name,
				RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb AS SizeKB,
				RecHubConfig.ExtractDefinitions.ExtractDefinitionType AS ExtractType,
				RecHubConfig.ExtractDefinitions.ExtractFileName AS ExtractFileName
		FROM 	RecHubConfig.ExtractDefinitions
		WHERE	(@parmActiveOnly = 1 AND RecHubConfig.ExtractDefinitions.IsActive = 1) OR (@parmActiveOnly = 0)
	)
	INSERT INTO #PagedResults
		(	
			RecID,
			ExtractID,
			TotalRecords,
			Name,
			SizeKB,
			ExtractType,
			ExtractFileName
		)
	SELECT	RecID,
			ExtractID,
			TotalRecords = (SELECT ISNULL(MAX(RecID), 0) FROM PageRecords),
			Name,
			SizeKB,
			ExtractType,
			ExtractFileName
	FROM	PageRecords
	WHERE	(@parmRecordsPerPage > 0 AND (RecID BETWEEN @parmStartRecord and @parmStartRecord+@parmRecordsPerPage-1))
			OR (@parmRecordsPerPage <= 0 AND (RecID >= @parmStartRecord))
	ORDER BY RecID ASC;

	SELECT TOP(1) @parmTotalRecords = TotalRecords FROM #PagedResults
	
	IF @parmTotalRecords > 0
		SELECT	RecID,
				ExtractID,
				Name,
				SizeKB,
				ExtractType,
				ExtractFileName
		FROM	#PagedResults
		ORDER BY RecID ASC
	ELSE
		SET @parmTotalRecords = 0;

	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH