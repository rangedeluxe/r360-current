--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ReportParameter_Get_ByReportID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ReportParameter_Get_ByReportID') IS NOT NULL
	DROP PROCEDURE RecHubConfig.usp_ReportParameter_Get_ByReportID;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ReportParameter_Get_ByReportID
(
      @parmReportID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the Report Parameters for a given Report ID
*
* Modification History
* Created
* 05/06/2013 WI 107235 KLC	Created.
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		RecHubConfig.ReportParameters.ReportID,
		RecHubConfig.ReportParameters.OrderValue,
		RecHubConfig.ReportParameters.DisplayName,
		RecHubConfig.ReportParameters.ParameterName,
		RecHubConfig.ReportParameters.ParameterType,
		RecHubConfig.ReportParameters.IsRequired,
		RecHubConfig.ReportParameters.AllowMultiple
	FROM
		RecHubConfig.ReportParameters
	WHERE
		RecHubConfig.ReportParameters.ReportID = @parmReportID;

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH