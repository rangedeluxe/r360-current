--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractSchedules_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractSchedules_Get') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractSchedules_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractSchedules_Get
( 
	@ExtractDefinitionId   BIGINT = NULL,
	@ExtractScheduleId     BIGINT = NULL,
	@FirstRec              BIGINT = NULL,
	@LastRec               BIGINT = NULL,
	@NumberOfRecords       INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Retreive one or more extract schedule records, either by the schedule id, or by the extract definition id
*
*
* Modification History
* 03/13/2013 WI 97714 DRP  Created
* 03/13/2013 WI 103465 DRP Added ExtractRunArguments
* 05/07/2014 WI 140384 JBS Adding Columns DayInMonth, ScheduleType 
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

    ;WITH cte AS 
	(
		SELECT RecHubConfig.ExtractSchedules.ExtractScheduleId,
			   RecHubConfig.ExtractSchedules.ExtractDefinitionId,
			   RecHubConfig.ExtractSchedules.[Description],
			   RecHubConfig.ExtractSchedules.IsActive,
			   RecHubConfig.ExtractSchedules.DaysOfWeek,
			   RecHubConfig.ExtractSchedules.DayInMonth,
			   RecHubConfig.ExtractSchedules.ScheduleTime,
			   RecHubConfig.ExtractSchedules.ScheduleType,
			   RecHubConfig.ExtractSchedules.ExtractRunArguments,
			   RecHubConfig.ExtractSchedules.CreationDate,
			   RecHubConfig.ExtractSchedules.ModificationDate,
			   RecHubConfig.ExtractSchedules.CreatedBy,
			   RecHubConfig.ExtractSchedules.ModifiedBy,
			   ROW_NUMBER() OVER(ORDER BY RecHubConfig.ExtractSchedules.ExtractScheduleId) AS RowNumber
		 FROM RecHubConfig.ExtractSchedules 
		 WHERE (@ExtractScheduleId IS NULL OR @ExtractScheduleId = RecHubConfig.ExtractSchedules.ExtractScheduleId) AND
		   (@ExtractDefinitionId IS NULL OR @ExtractDefinitionId = RecHubConfig.ExtractSchedules.ExtractDefinitionId)
     ) 
    
    SELECT * FROM cte    
    WHERE RowNumber BETWEEN ISNULL(@FirstRec, RowNumber) AND ISNULL(@LastRec, RowNumber);

    SELECT @NumberOfRecords = COUNT(*) FROM RecHubConfig.ExtractSchedules 
    WHERE (@ExtractScheduleId IS NULL OR @ExtractScheduleId = RecHubConfig.ExtractSchedules.ExtractScheduleId) AND
          (@ExtractDefinitionId IS NULL OR @ExtractDefinitionId = RecHubConfig.ExtractSchedules.ExtractDefinitionId);

END TRY

BEGIN CATCH
		EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH


