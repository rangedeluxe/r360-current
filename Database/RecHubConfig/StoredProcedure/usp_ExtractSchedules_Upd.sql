--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractSchedules_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractSchedules_Upd') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractSchedules_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractSchedules_Upd
( 
	@ExtractDefinitionId    BIGINT,
	@Description            VARCHAR(255), 
    @IsActive               BIT,
    @DaysOfWeek             VARCHAR(7),
	@DayInMonth				INT,
    @ScheduleTime           TIME,
	@ScheduleType			INT,
    @ExtractScheduleId      BIGINT,
    @ExtractRunArguments    VARCHAR(255),
    @UserID                 BIGINT,
    @NumberOfRecords        INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Update one extract schedule record.
*
*
* Modification History
* 03/13/2013 WI 97716 DRP  Created
* 03/30/2013 WI 103461 DRP Added ExtractRunArguments
* 03/31/2013 WI 103457 DRP Added Data Audits
* 05/07/2014 WI 140396 JBS Adding Columns DayInMonth, ScheduleType.  Added CAST to VARCHAR for AUDIT
******************************************************************************/
SET NOCOUNT ON;

DECLARE
	@AuditMessage	VARCHAR(1024),
	@Key            VARCHAR(255),
	@AuditColumns	RecHubCommon.AuditValueChangeTable;
	
BEGIN TRY

	BEGIN TRANSACTION ExtractSchedules_Upd

    DECLARE @AuditInformation TABLE
	(
		OldExtractDefinitionId	BIGINT NOT NULL,
		OldDescription			VARCHAR(255) NULL,
		OldIsActive				BIT NOT NULL,
		OldDaysOfWeek			VARCHAR(7) NOT NULL,
		OldDayInMonth			INT NOT NULL,
		OldScheduleTime			TIME NOT NULL,
		OldScheduleType			INT NOT NULL,
		OldExtractRunArguments	VARCHAR(255) NOT NULL,	
		NewExtractDefinitionId	BIGINT NOT NULL,
		NewDescription			VARCHAR(255) NULL,
		NewIsActive				BIT NOT NULL,
		NewDaysOfWeek			VARCHAR(7) NOT NULL,
		NewDayInMonth			INT NOT NULL,
		NewScheduleTime			TIME NOT NULL,
		NewScheduleType			INT NOT NULL,
		NewExtractRunArguments	VARCHAR(255) NOT NULL
	)
			
	UPDATE RecHubConfig.ExtractSchedules 
	SET 
			   ExtractDefinitionId = @ExtractDefinitionId,
			   [Description] =  @Description,
			   IsActive = @IsActive,
			   DaysOfWeek = @DaysOfWeek,
			   DayInMonth = @DayInMonth,
			   ScheduleTime = @ScheduleTime,
			   ScheduleType = @ScheduleType,
			   ExtractRunArguments = @ExtractRunArguments,
			   ModificationDate = GETDATE(),
			   ModifiedBy = SUSER_SNAME()
	OUTPUT 
		deleted.ExtractDefinitionId,
		deleted.[Description],
		deleted.IsActive,
		deleted.DaysOfWeek,
		deleted.DayInMonth,
		deleted.ScheduleTime,
		deleted.ScheduleType,
		deleted.ExtractRunArguments,
		inserted.ExtractDefinitionId,
		inserted.[Description],
		inserted.IsActive,
		inserted.DaysOfWeek,
		inserted.DayInMonth,
		inserted.ScheduleTime,
		inserted.ScheduleType,
		inserted.ExtractRunArguments
	INTO 
		@AuditInformation
    WHERE 
              @ExtractScheduleId = RecHubConfig.ExtractSchedules.ExtractScheduleId
     
    SELECT @NumberOfRecords = @@ROWCOUNT
	
	SELECT @AuditMessage = 'Modified ExtractSchedules table entry for ID: ' + CAST(@ExtractScheduleId AS VARCHAR(10)) + '.'
	SELECT @Key = CAST(@ExtractScheduleId AS VARCHAR);

/* Fill the audit column table type to pass to audit SP */
	INSERT INTO @AuditColumns(ColumnName,OldValue,NewValue)
	SELECT 'ExtractDefinitionId',CAST(OldExtractDefinitionId AS VARCHAR), CAST(NewExtractDefinitionId AS VARCHAR) FROM @AuditInformation WHERE OldExtractDefinitionId <> NewExtractDefinitionId
	UNION ALL
	SELECT 'Description',OldDescription,NewDescription FROM @AuditInformation WHERE OldDescription <> NewDescription
	UNION ALL
	SELECT 'IsActive',CAST(OldIsActive AS VARCHAR), CAST(NewIsActive AS VARCHAR) FROM @AuditInformation WHERE OldIsActive <> NewIsActive
	UNION ALL
	SELECT 'DaysOfWeek',OldDaysOfWeek, NewDaysOfWeek FROM @AuditInformation WHERE OldDaysOfWeek <> NewDaysOfWeek
	UNION ALL
	SELECT 'DayInMonth',CAST(OldDayInMonth AS VARCHAR), CAST(NewDayInMonth AS VARCHAR) FROM @AuditInformation WHERE OldDayInMonth <> NewDayInMonth
	UNION ALL
	SELECT 'ScheduleTime',CAST(OldScheduleTime AS VARCHAR), CAST(NewScheduleTime AS VARCHAR) FROM @AuditInformation WHERE OldScheduleTime <> NewScheduleTime
	UNION ALL
	SELECT 'ScheduleType',CAST(OldScheduleType AS VARCHAR), CAST(NewScheduleType AS VARCHAR) FROM @AuditInformation WHERE OldScheduleType <> NewScheduleType
	UNION ALL
	SELECT 'ExtractRunArguments',OldExtractRunArguments, NewExtractRunArguments FROM @AuditInformation WHERE OldExtractRunArguments <> NewExtractRunArguments

	/* Now call the SP to insert the audit records */
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails 	
		@parmUserID					= @UserID,
		@parmApplicationName		= 'RecHubConfig.usp_ExtractSchedules_Upd',
		@parmSchemaName				= 'RecHubConfig',
		@parmTableName				= 'ExtractSchedules',
		@parmAuditType				= 'UPD',
		@parmColumnName				= 'ExtractScheduleID',
		@parmAuditValue				= @Key,		
		@parmAuditColumns			= @AuditColumns,
		@parmAuditMessage			= @AuditMessage;
		
	COMMIT TRANSACTION ExtractSchedules_Upd;
END TRY

BEGIN CATCH
        IF XACT_STATE() != 0 ROLLBACK TRANSACTION ExtractSchedules_Upd;
		EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH



