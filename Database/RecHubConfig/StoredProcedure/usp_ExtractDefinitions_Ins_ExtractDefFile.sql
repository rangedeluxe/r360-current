--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Ins_ExtractDefFile
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Ins_ExtractDefFile') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Ins_ExtractDefFile
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Ins_ExtractDefFile 
(
       @parmUserID					INT,
	   @parmExtractName				VARCHAR(128),
	   @parmExtractDefSizeKb		INT,
	   @parmExtractDefinition		VARCHAR(MAX),
	   @parmExtractDefinitionType	INT,
	   @parmExtractFilename			VARCHAR(128),
	   @parmExtractInsertedID		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Insert new Extract XML file
*
* Modification History
* 05/13/2013 WI	97651  EAS	Created
* 05/07/2014 WI 140382 JBS	Adding Columns ExtractDefinitionType, ExtractFilename
* 06/18/2014 WI 148584 DJW  Added logging for audit purposes 
* 08/15/2014 WI 158039 BLR  Edited audit message to VarChar(MAX).
* 07/06/2017 PT 2677   CEJ	Convert ExtractDefinition from varchar max to varbinary max
******************************************************************************/

DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

--defaults
DECLARE		   
	   @parmExtractDescription VARCHAR(255)='',
	   @parmExtractIsActive bit =1;

	INSERT INTO RecHubConfig.ExtractDefinitions
	(
		RecHubConfig.ExtractDefinitions.ExtractDefinition,
		RecHubConfig.ExtractDefinitions.ExtractName,
		RecHubConfig.ExtractDefinitions.[Description],
		RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb,
		RecHubConfig.ExtractDefinitions.ExtractDefinitionType,
		RecHubConfig.ExtractDefinitions.IsActive,
		RecHubConfig.ExtractDefinitions.ExtractFilename,
		RecHubConfig.ExtractDefinitions.CreationDate,
		RecHubConfig.ExtractDefinitions.ModificationDate,
		RecHubConfig.ExtractDefinitions.CreatedBy,
		RecHubConfig.ExtractDefinitions.ModifiedBy
	)
	VALUES
	(
		CONVERT(VARBINARY(MAX), @parmExtractDefinition),
		@parmExtractName,
		@parmExtractDescription,
		@parmExtractDefSizeKb,
		@parmExtractDefinitionType,
		@parmExtractIsActive,
		@parmExtractFilename,
		GETDATE(),
		GETDATE(),
		SUSER_SNAME(),
		SUSER_SNAME()	
	);
	SET @parmExtractInsertedID = SCOPE_IDENTITY();
	DECLARE @auditMessage	VARCHAR(MAX);
	SET @auditMessage = 'Added ExtractDefinition:'
		+ ' Extract Name = ' + @parmExtractName
		+ ', Extract Description = ' + @parmExtractDescription
		+ ', Extract IsActive = ' + CAST(@parmExtractIsActive as VARCHAR(10))
		+ ', Extract Def Size(kb) = ' + CAST(@parmExtractDefSizeKb AS VARCHAR(10)) 
		+ ', Extract Definition Type = ' + CAST(ISNULL(@parmExtractDefinitionType,-1) AS VARCHAR(10))
		+ ', Extract Filename = ' + ISNULL(@parmExtractFilename,'')
		+ ', Definition Contents = ' + @parmExtractDefinition
		+ '.';
	EXEC RecHubCommon.usp_WFS_DataAudit_ins
		@parmUserID				= @parmUserID,
		@parmApplicationName	= 'RecHubConfig.usp_ExtractDefinitions_Ins_ExtractDefFile',
		@parmSchemaName			= 'RecHubConfig',
		@parmTableName			= 'ExtractDefinitions',
		@parmColumnName			= 'ExtractDefinitionID',
		@parmAuditValue			= @parmExtractInsertedID,
		@parmAuditType			= 'INS',
		@parmAuditMessage		= @auditMessage;



	IF @LocalTransaction = 1 COMMIT TRANSACTION;
END TRY
BEGIN CATCH
       IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
