--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_GetAfterModDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_GetAfterModDate') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_GetAfterModDate
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_GetAfterModDate
(
       @modifiedAfter DATETIME = '1/1/1900'
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 03/07/2013
*
* Purpose: Retrieve list of active extract schedule records.
*
*
* Modification History
* 03/13/2013 WI 97761  DRP  Created
* 05/07/2014 WI 140379 JBS	Adding Columns ExtractDefinitionType, ExtractFilename
* 08/18/2014 WI 159309 BLR	Removed the Definition, added the Definition Size.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT RecHubConfig.ExtractDefinitions.ExtractDefinitionId,
           RecHubConfig.ExtractDefinitions.ExtractName,
		   RecHubConfig.ExtractDefinitions.ExtractDefinitionType,
		   RecHubConfig.ExtractDefinitions.ExtractFilename,
		   RecHubConfig.ExtractDefinitions.ExtractDefinitionSizeKb
    FROM   RecHubConfig.ExtractDefinitions 
    WHERE (RecHubConfig.ExtractDefinitions.IsActive = 1) AND 
          (RecHubConfig.ExtractDefinitions.ModificationDate >= @modifiedAfter)
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_wfsRethrowException;
END CATCH


