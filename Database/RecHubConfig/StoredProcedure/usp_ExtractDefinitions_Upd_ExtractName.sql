--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStoredProcedureName usp_ExtractDefinitions_Upd_ExtractName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubConfig.usp_ExtractDefinitions_Upd_ExtractName') IS NOT NULL
       DROP PROCEDURE RecHubConfig.usp_ExtractDefinitions_Upd_ExtractName
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubConfig.usp_ExtractDefinitions_Upd_ExtractName 
(
       @parmUserID			INT,
	   @parmExtractDefID	INT,
	   @parmExtractName		VARCHAR(128)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright @ 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright @ 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 05/13/2013
*
* Purpose: Update Extract Name
*
* Modification History
* 05/13/2013 WI 97652  EAS	Created
* 06/18/2014 WI 148598 DJW  Added logging for audit purposes 
* 10/21/2014 WI 173362 BLR  Changed param 'TableName' to the TableName, not the Stored Proc name. 
******************************************************************************/


-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	--DECLARE prev values placeholder
	DECLARE @prevExtractName		  VARCHAR(128);

	UPDATE	
		RecHubConfig.ExtractDefinitions
	SET	
		@prevExtractName = RecHubConfig.ExtractDefinitions.ExtractName,

		RecHubConfig.ExtractDefinitions.ExtractName = @parmExtractName,
		RecHubConfig.ExtractDefinitions.ModificationDate = GETDATE(),
		RecHubConfig.ExtractDefinitions.ModifiedBy = SUSER_SNAME()
	WHERE
	  	RecHubConfig.ExtractDefinitions.ExtractDefinitionID = @parmExtractDefID;

	--see if we updated
	IF CAST(@parmExtractName AS VARBINARY(128)) <> CAST(@prevExtractName AS VARBINARY(128) ) 
	BEGIN
		DECLARE @auditMessage			VARCHAR(1024),
				@auditColumns			RecHubCommon.AuditValueChangeTable;
		DECLARE @curTime DATETIME = GETDATE();

		--AUDIT
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) 
		VALUES('ExtractDefinition', @prevExtractName, @parmExtractName);

		SET @auditMessage = 'Modified ExtractDefinitions for Definition Id: ' + CAST(@parmExtractDefID AS VARCHAR(10)) + '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubConfig.usp_ExtractDefinitions_Upd_ExtractName',
				@parmSchemaName			= 'RecHubConfig',
				@parmTableName			= 'ExtractDefinitions',
				@parmColumnName			= 'ExtractDefinitionID',
				@parmAuditValue			= @parmExtractDefID,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;

	END --IF CAST(@parmExtractName AS VARBINARY(128)) <> CAST(@prevExtractName AS VARBINARY(128) ) 

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
