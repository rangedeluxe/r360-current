--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStaticDataName Create User Accounts
--WFSScriptPRocessorStaticDataCreateBegin
-- 03/18/2013 WI92884 JBS Created   This will execute procedure below and create db role for each schema listed
-- 02/07/2014 WI128221 JBS	Updated for 2.01 release	
-- 04/14/2014 WI136858 RDS	RecHubBilling
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubAlert';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubAudit';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubBilling';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubCommon';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubConfig';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubData';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubException';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubSystem';
GO
EXEC RecHubConfig.usp_SecurityForRolesAndPermissions 'RecHubUser';
GO
--WFSScriptPRocessorStaticDataCreateEnd