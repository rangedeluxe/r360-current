--  This script will create all the USER accounts associated with the application.  It will also assign the user account to the
--  database role needed for its functional scope.
--  03/15/2013	WI92418	 JBS Created for 2.0 release.  File name query_BuildUserLoginAccounts.sql
--  11/04/2013	WI121233 JBS Update for 2.00.01 release.
--  11/19/2013  WI122921 EAS Add New Unpruned Procedure
--  12/17/2013  WI125498 EAS Add Audit Stored Procedure to Admin Grants
--	02/07/2014  WI128218 JBS Build for 2.01 release.  Added permissions for DIT process, Added permissions for ExtractWizardUser
--										GRANT SELECT ON [RecHubData].[dimDDAs] TO [dbRole_RecHubSearch] for Search functions
--	04/14/2014	WI136856 RDS RecHubBilling
--	04/10/2015  WI200413 JBS Removed procedures and schemas no longer needed or used
--WFSScriptProcessorSchema RecHubConfig
--WFSScriptProcessorStaticDataName Create User Accounts
--WFSScriptPRocessorStaticDataCreateBegin
USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubConfig_User')
	CREATE LOGIN [RecHubConfig_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubConfig_User')
	BEGIN
		CREATE USER [RecHubConfig_User] FOR LOGIN [RecHubConfig_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubConfig_User')
	BEGIN
		ALTER USER [RecHubConfig_User] WITH DEFAULT_SCHEMA=[RecHubConfig];
		EXEC sp_addrolemember N'dbRole_RecHubConfig', N'RecHubConfig_User';
	END
GO

USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubConfig_Admin')
	CREATE LOGIN [RecHubConfig_Admin] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubConfig_Admin')
	BEGIN
		CREATE USER [RecHubConfig_Admin] FOR LOGIN [RecHubConfig_Admin];

	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubConfig_Admin')
	BEGIN
		ALTER USER [RecHubConfig_Admin] WITH DEFAULT_SCHEMA=[RecHubConfig];
		EXEC sp_addrolemember N'dbRole_RecHubConfig', N'RecHubConfig_Admin';
		EXEC sp_addrolemember N'dbRole_RecHubAlert', N'RecHubConfig_Admin';
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_Get TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_GetSystem TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_Get_WithDescriptions TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_Upd TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLClientAccountUsers_Del_ByUserID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLUserQuestions_Del_ByUserID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_OLUserMachines_Del_ByUserID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_BrandingSchemes_GetByBrandingID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_BrandingSchemes_Upd TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_BrandingSchemes_Ins TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_BrandingSchemes_Del TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_Session_Upd_EndSession TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_Session_UpdPageCounter TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubUser.usp_Users_Ins_OlClientAccountUser TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimDocumentTypes_Get TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimNotificationFileTypes_Get TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimNotificationFileTypes_Ins TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimSiteCodes_Get TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimBanks_Get_BySiteCodeID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimClientAccounts_Get_SiteClientAccountIDs_BySiteBankID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubData.usp_dimClientAccountsView_Get_ByBankIDOrganizationID TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubCommon.usp_WFS_EventAudit_Ins TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubCommon.usp_GetCurrentDatetime TO RecHubConfig_Admin;
		GRANT EXECUTE ON RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails TO RecHubConfig_Admin;
	END


USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubSystem_User')
	CREATE LOGIN [RecHubSystem_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubSystem_User')
	BEGIN
		CREATE USER [RecHubSystem_User] FOR LOGIN [RecHubSystem_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubSystem_User')
	BEGIN
		ALTER USER [RecHubSystem_User] WITH DEFAULT_SCHEMA=[RecHubSystem];
		EXEC sp_addrolemember N'dbRole_RecHubSystem', N'RecHubSystem_User';
		EXEC sp_addrolemember N'dbRole_RecHubConfig', N'RecHubConfig_User';
		-- Purge process is using this User account
		GRANT EXECUTE ON RecHubConfig.usp_SystemSetup_Get_BySection TO RecHubSystem_User;
	END

USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubExtractWizard_User')
	CREATE LOGIN [RecHubExtractWizard_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubExtractWizard_User')
	BEGIN
		CREATE USER [RecHubExtractWizard_User] FOR LOGIN [RecHubExtractWizard_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubExtractWizard_User')
	BEGIN
		ALTER USER [RecHubExtractWizard_User] WITH DEFAULT_SCHEMA=[RecHubCommon];
		GRANT EXECUTE ON RecHubConfig.usp_SystemSetup_Get_BySetupKey TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubConfig.usp_ExtractSchedules_GetActiveList TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubConfig.usp_ExtractDefinitions_GetAfterModDate TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubCommon.usp_Get_TableDefinition TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubCommon.usp_WFS_EventAudit_Ins TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubCommon.usp_WFS_EventAudit_Ins TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubAudit.usp_WFS_factExtractAudits_Ins TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_dimExtractDescriptor_Get TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_dimExtractDescriptor_Ins TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_factChecks_Get_ByImmutableDate TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_factDocuments_Get_ByImmutableDate TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_factExtractTraces_Get TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_factExtractTraces_UpSert TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_dimExtractDescriptor_Get TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubData.usp_factExtractTraces_Get TO RecHubExtractWizard_User;
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_Get TO RecHubExtractWizard_User
		GRANT EXECUTE ON RecHubUser.usp_OLPreferences_Get_WithDescriptions TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimBanks TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimBatchPaymentTypes TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimBatchSources TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimClientAccounts TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimDocumentTypes TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimExtractDescriptor TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimOrganizations TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.dimWorkgroupDataEntryColumns TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factBatchSummary TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factChecks TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factDataEntryDetails TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factDocuments TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factExtractTraces TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factStubs TO RecHubExtractWizard_User;
		GRANT SELECT ON RecHubData.factTransactionSummary TO RecHubExtractWizard_User;
	END

USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubUser_User')
	CREATE LOGIN [RecHubUser_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubUser_User')
	BEGIN
		CREATE USER [RecHubUser_User] FOR LOGIN [RecHubUser_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubUser_User')
	BEGIN
		ALTER USER [RecHubUser_User] WITH DEFAULT_SCHEMA=[RecHubUser];
		EXEC sp_addrolemember N'dbRole_RecHubUser', N'RecHubUser_User';
		EXEC sp_addrolemember N'dbRole_RecHubData', N'RecHubUser_User';
		EXEC sp_addrolemember N'dbRole_RecHubCommon', N'RecHubUser_User';
		EXEC sp_addrolemember N'dbRole_RecHubConfig', N'RecHubUser_User';
		GRANT EXECUTE ON RecHubAlert.usp_EventLog_Ins TO RecHubUser_User;
		GRANT EXECUTE ON RecHubException.usp_Report_ReceivablesExceptionsByType TO RecHubUser_User;
		GRANT EXECUTE ON RecHubException.usp_Report_ReceivablesExceptions TO RecHubUser_User;
		GRANT EXECUTE ON RecHubAudit.usp_Report_ConfigurationAudit TO RecHubUser_User;
		GRANT EXECUTE ON RecHubAudit.usp_Report_ExtractAudit TO RecHubUser_User;
		GRANT EXECUTE ON RecHubAudit.usp_Report_UserActivityAudit TO RecHubUser_User;
		--  For the FIT services process
		GRANT EXECUTE ON RecHubSystem.usp_DataImportIntegrationServices_GetResponseNotification TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_DataImportIntegrationServices_InsertNotification TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_ImportSchemas_Get_BySchemaType TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_DataImportQueue_Upd_QueueStatus TO RecHubUser_User;
		--  For the DIT services process
		GRANT EXECUTE ON RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_DataImportQueue_Get_BatchResponse TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_dimClientAccounts_Get_ClientSetup TO RecHubUser_User;
		GRANT EXECUTE ON RecHubSystem.usp_DataImportQueue_Ins_Batch TO RecHubUser_User;
	END

USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubData_User')
	CREATE LOGIN [RecHubData_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubData_User')
	BEGIN
		CREATE USER [RecHubData_User] FOR LOGIN [RecHubData_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubData_User')
	BEGIN
		ALTER USER [RecHubData_User] WITH DEFAULT_SCHEMA=[RecHubData];
		EXEC sp_addrolemember N'dbRole_RecHubData', N'RecHubData_User';
	END

--USE [master]
--GO
--IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubException_User')
--	CREATE LOGIN [RecHubException_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

--USE [$(DBName)]
--GO
--IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubException_User')
--	BEGIN
--		CREATE USER [RecHubException_User] FOR LOGIN [RecHubException_User];
--	END
--IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubException_User')
--	BEGIN
--		ALTER USER [RecHubException_User] WITH DEFAULT_SCHEMA=[RecHubException];
--		EXEC sp_addrolemember N'dbRole_RecHubException', N'RecHubException_User';
--		GRANT EXECUTE ON RecHubUser.usp_Users_UserID_Get_BySID TO RecHubException_User;
--		GRANT EXECUTE ON RecHubUser.usp_Users_Session_Get_BySID TO RecHubException_User;
--		GRANT EXECUTE ON RecHubCommon.usp_WFS_EventAudit_Ins TO RecHubException_User;
--	END


USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubAlert_User')
	CREATE LOGIN [RecHubAlert_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubAlert_User')
	BEGIN
		CREATE USER [RecHubAlert_User] FOR LOGIN [RecHubAlert_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubAlert_User')
	BEGIN
		ALTER USER [RecHubAlert_User] WITH DEFAULT_SCHEMA=[RecHubAlert];
		EXEC sp_addrolemember N'dbRole_RecHubAlert', N'RecHubAlert_User';
	END

USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubAudit_User')
	CREATE LOGIN [RecHubAudit_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubAudit_User')
	BEGIN
		CREATE USER [RecHubAudit_User] FOR LOGIN [RecHubAudit_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubAudit_User')
	BEGIN
		ALTER USER [RecHubAudit_User] WITH DEFAULT_SCHEMA=[RecHubAudit];
		EXEC sp_addrolemember N'dbRole_RecHubAudit', N'RecHubAudit_User';
	END

--  Special role for the Searching functions
USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.database_principals WHERE [Name] = 'dbRole_RecHubSearch')
	BEGIN
		CREATE ROLE [dbRole_RecHubSearch] AUTHORIZATION [dbo];
	END;
IF EXISTS (SELECT * FROM sys.database_principals WHERE [Name] = 'dbRole_RecHubSearch')
	BEGIN
		EXEC sp_addrolemember N'dbRole_RecHubSearch', N'RecHubUser_User';
		GRANT SELECT ON [RecHubData].[factChecks] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[factDataEntryDetails] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[factTransactionSummary] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[factStubs] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[dimBatchSources] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[dimBatchPaymentTypes] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[dimSiteCodes] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[dimClientAccounts] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubData].[dimDDAs] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubUser].[OLClientAccounts] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubUser].[OLOrganizations] TO [dbRole_RecHubSearch];
		GRANT SELECT ON [RecHubUser].[OLClientAccountUsers] TO [dbRole_RecHubSearch];
	END;

--  Adding generic role to the DBO role for troubleshooting and data access
USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'RecHubDBO_User')
	CREATE LOGIN [RecHubDBO_User] WITH PASSWORD=N'RecHubDBO', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubDBO_User')
	BEGIN
		CREATE USER [RecHubDBO_User] FOR LOGIN [RecHubDBO_User];
	END
IF EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'RecHubDBO_User')
	BEGIN
		ALTER USER [RecHubDBO_User] WITH DEFAULT_SCHEMA=[dbo];
		EXEC sp_addrolemember N'db_owner', N'RecHubDBO_User';
	END

--WFSScriptPRocessorStaticDataCreateEnd
