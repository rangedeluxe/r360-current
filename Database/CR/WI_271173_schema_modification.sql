--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271173
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factItemData.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factItemData')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factItemData'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factItemData_DepositDatefactItemDataKey')
			DROP INDEX IDX_factItemData_DepositDatefactItemDataKey ON RecHubData.factItemData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence')
			DROP INDEX IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factItemData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factItemData_ItemDataSetupFieldKey')
			DROP INDEX IDX_factItemData_ItemDataSetupFieldKey ON RecHubData.factItemData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factItemData_SourceBatchIDDepositDateKey')
			DROP INDEX IDX_factItemData_SourceBatchIDDepositDateKey ON RecHubData.factItemData;


		/****** Object:  Index PK_factItemData    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factItemData')
			ALTER TABLE RecHubData.factItemData DROP CONSTRAINT PK_factItemData;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factItemData ADD
			CONSTRAINT PK_factItemData PRIMARY KEY NONCLUSTERED (factItemDataKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factItemData_DepositDatefactItemDataKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factItemData_DepositDatefactItemDataKey ON RecHubData.factItemData 
		(
			DepositDateKey,
			factItemDataKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factItemData
		(
			BankKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			DepositDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			BatchSequence ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factItemData_ItemDataSetupFieldKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factItemData_ItemDataSetupFieldKey ON RecHubData.factItemData 
		(
			ItemDataSetupFieldKey
		) 
		INCLUDE 
		(
			ClientAccountKey, 
			ImmutableDateKey, 
			BatchID, 
			BatchSequence, 
			ModificationDate, 
			DataValue
		);

		RAISERROR('Rebuilding index IDX_factItemData_SourceBatchIDDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factItemData_SourceBatchIDDepositDateKey ON RecHubData.factItemData 
		(
			SourceBatchID,
			DepositDateKey
		)
		INCLUDE 
		(
			factItemDataKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			ImmutableDateKey, 
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			TransactionID, 
			BatchSequence, 
			ItemDataSetupFieldKey, 
			CreationDate, 
			ModificationDate, 
			DataValueDateTime, 
			DataValueMoney, 
			DataValueInteger, 
			DataValueBit, 
			ModifiedBy, 
			DataValue, 
			BatchSourceKey
		) $(OnDataPartition);

	END
	ELSE
		RAISERROR('WI 271173 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271173 RecHubData.factItemData table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd