--WFSScriptProcessorPrint CR 50526
--WFSScriptProcessorPrint Move usp_UpdateDodumentTypesDescription from to to OLTA if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('dbo.usp_UpdateDocumentTypesDescription') IS NOT NULL
	DROP PROCEDURE dbo.usp_UpdateDocumentTypesDescription
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_UpdateDocumentTypesDescription') IS NOT NULL
	DROP PROCEDURE OLTA.usp_UpdateDocumentTypesDescription
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRBegin
CREATE PROCEDURE [OLTA].[usp_UpdateDocumentTypesDescription]
	@parmFileDescriptor	 varchar(255),
	@parmDescription	 varchar(255)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/4/2011
*
* Purpose: Update document Types
*
* Modification History
* 10/04/2011 CR 47152 WJS	Created
* 02/23/2012 CR 50526 WJS 	Moved to OLTA schema.  For FileDescriptors that
*                           exist in dbo, we update dbo only and allow OTIS to
*                           replicate the change into OLTA.  For FileDescriptors
*                           that exist only in OLTA, we update them directly.
******************************************************************************/
--SET NOCOUNT ON 
DECLARE @ErrorMessage	NVARCHAR(4000),
		@ErrorProcedure	NVARCHAR(200),
		@ErrorSeverity	INT,
		@ErrorState		INT,
		@ErrorLine		INT


BEGIN TRY


	BEGIN TRY
		UPDATE [dbo].[DocumentTypes]
			SET [Description] = @parmDescription
		WHERE [FileDescriptor] = @parmFileDescriptor
		
		IF @@ROWCOUNT = 0
		BEGIN
			UPDATE [olta].dimDocumentTypes
				SET [DocumentTypeDescription] = @parmDescription
			WHERE [FileDescriptor] = @parmFileDescriptor
		END

	END TRY
	BEGIN CATCH
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-')

		
			SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END CATCH
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
--WFSScriptProcessorCREnd