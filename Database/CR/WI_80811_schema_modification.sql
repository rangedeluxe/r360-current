--WFSScriptProcessorPrint WI 80811
--WFSScriptProcessorPrint Update factItemData DocumentTypes data values
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factItemData
SET		DataValueInteger = NULL
FROM	OLTA.factItemData
		INNER JOIN OLTA.dimItemDataSetupFields ON OLTA.factItemData.ItemDataSetupFieldKey = OLTA.dimItemDataSetupFields.ItemDataSetupFieldKey
WHERE	Keyword = 'Document Type'
		AND BatchSourceKey = 101
--WFSScriptProcessorCREnd