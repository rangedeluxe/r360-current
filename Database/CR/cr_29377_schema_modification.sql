--WFSScriptProcessorPrint CR 29377
--WFSScriptProcessorPrint Adding Checks Batch Seqeunce to OLTA.factTransactionDetails
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factTransactionDetails' AND COLUMN_NAME='CheckBatchSequence')
	RETURN
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT DF_factTransactionDetails_ModificationDate
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_dimBanks_factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_dimCustomers_factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_dimLockboxes_factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_ProcessingDate_factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_DepositDate_factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
DROP INDEX IDX_factTransactionDetails_DepositDateKey ON OLTA.factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
DROP INDEX IDX_factTransactionDetails_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID ON OLTA.factTransactionDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Renameing OLTA.factTransactionDetails to OLTA.tempfactTransactionDetails
--WFSScriptProcessorCRBegin
sp_rename 'OLTA.factTransactionDetails', 'tempfactTransactionDetails';
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorTable factTransactionDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/31/2010
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/22/2010 CR 29285 JPB	Created
* 04/15/2010 CR 29377 JPB	Added CheckBatchSequence
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE OLTA.factTransactionDetails
(
	BankKey int NOT NULL,
	CustomerKey int NOT NULL,
	LockboxKey int NOT NULL,
	ProcessingDateKey int NOT NULL,
	DepositDateKey int NOT NULL,
	BatchID int NOT NULL,
	DESetupID int NULL,
	SystemType tinyint NOT NULL,
	DepositStatus int NOT NULL,
	TransactionID int NOT NULL,
	TxnSequence int NOT NULL,
	CheckCount int NOT NULL,
	DocumentCount int NOT NULL,
	ScannedCheckCount int NOT NULL,
	StubCount int NOT NULL,
	OMRCount int NOT NULL,
	CheckTotal money NOT NULL,
	CheckBatchSequence int NULL,
	CheckSequence int NULL,
	RemitterKey int NULL,
	Serial varchar(30) NULL,
	NumericSerial bigint NULL,
	Amount money NULL,
	DataEntryBatchSequence int NULL,
	LoadDate datetime NOT NULL,
	ModificationDate datetime NOT NULL 
		CONSTRAINT DF_factTransactionDetails_ModificationDate DEFAULT GETDATE()
) ON OLTAFacts_Partition_Scheme(DepositDateKey)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE OLTA.factTransactionDetails ADD 
	   CONSTRAINT FK_dimBanks_factTransactionDetails FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
	   CONSTRAINT FK_dimCustomers_factTransactionDetails FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
	   CONSTRAINT FK_dimLockboxes_factTransactionDetails FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
	   CONSTRAINT FK_ProcessingDate_factTransactionDetails FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
	   CONSTRAINT FK_DepositDate_factTransactionDetails FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey)
--WFSScriptProcessorIndex OLTA.factTransactionDetails.IDX_factTransactionDetails_DepositDateKey
CREATE CLUSTERED INDEX IDX_factTransactionDetails_DepositDateKey ON OLTA.factTransactionDetails (DepositDateKey) ON OLTAFacts_Partition_Scheme(DepositDateKey)
--WFSScriptProcessorIndex OLTA.factTransactionDetails.IDX_factTransactionDetails_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID
CREATE NONCLUSTERED INDEX IDX_factTransactionDetails_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID ON OLTA.factTransactionDetails
(
	[LockboxKey] ASC,
	[DepositDateKey] ASC,
	[BatchID] ASC,
	[DepositStatus] ASC,
	[BankKey] ASC,
	[CustomerKey] ASC,
	[ProcessingDateKey] ASC,
	[TransactionID] ASC
) ON OLTAFacts_Partition_Scheme(DepositDateKey)
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Copying data from old table format to new table format
--WFSScriptProcessorCRBegin
INSERT INTO OLTA.factTransactionDetails
(
	BankKey,
	CustomerKey,
	LockboxKey,
	ProcessingDateKey,
	DepositDateKey,
	BatchID,
	DESetupID,
	SystemType,
	DepositStatus,
	TransactionID,
	TxnSequence,
	CheckCount,
	DocumentCount,
	ScannedCheckCount,
	StubCount,
	OMRCount,
	CheckTotal,
	CheckBatchSequence,
	CheckSequence,
	RemitterKey,
	Serial,
	NumericSerial,
	Amount,
	DataEntryBatchSequence,
	LoadDate,
	ModificationDate
)
SELECT 	OLTA.tempfactTransactionDetails.BankKey,
		OLTA.tempfactTransactionDetails.CustomerKey,
		OLTA.tempfactTransactionDetails.LockboxKey,
		OLTA.tempfactTransactionDetails.ProcessingDateKey,
		OLTA.tempfactTransactionDetails.DepositDateKey,
		OLTA.tempfactTransactionDetails.BatchID,
		OLTA.tempfactTransactionDetails.DESetupID,
		OLTA.tempfactTransactionDetails.SystemType,
		OLTA.tempfactTransactionDetails.DepositStatus,
		OLTA.tempfactTransactionDetails.TransactionID,
		OLTA.tempfactTransactionDetails.TxnSequence,
		OLTA.tempfactTransactionDetails.CheckCount,
		OLTA.tempfactTransactionDetails.DocumentCount,
		OLTA.tempfactTransactionDetails.ScannedCheckCount,
		OLTA.tempfactTransactionDetails.StubCount,
		OLTA.tempfactTransactionDetails.OMRCount,
		OLTA.tempfactTransactionDetails.CheckTotal,
		OLTA.factChecks.BatchSequence,
		OLTA.tempfactTransactionDetails.CheckSequence,
		OLTA.tempfactTransactionDetails.RemitterKey,
		OLTA.tempfactTransactionDetails.Serial,
		OLTA.tempfactTransactionDetails.NumericSerial,
		OLTA.tempfactTransactionDetails.Amount,
		OLTA.tempfactTransactionDetails.DataEntryBatchSequence,
		OLTA.tempfactTransactionDetails.LoadDate,
		OLTA.tempfactTransactionDetails.ModificationDate
FROM	OLTA.tempfactTransactionDetails
		INNER JOIN OLTA.factChecks ON OLTA.factChecks.BankKey = OLTA.tempfactTransactionDetails.BankKey
			AND OLTA.factChecks.CustomerKey = OLTA.tempfactTransactionDetails.CustomerKey
			AND OLTA.factChecks.LockboxKey = OLTA.tempfactTransactionDetails.LockboxKey
			AND OLTA.factChecks.ProcessingDateKey = OLTA.tempfactTransactionDetails.ProcessingDateKey
			AND OLTA.factChecks.DepositDateKey = OLTA.tempfactTransactionDetails.DepositDateKey
			AND OLTA.factChecks.BatchID = OLTA.tempfactTransactionDetails.BatchID
			AND OLTA.factChecks.CheckSequence = OLTA.tempfactTransactionDetails.CheckSequence
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = object_id(N'[dbo].[tempfactTransactionDetails]') AND OBJECTPROPERTY(id, N'IsTable') = 1)
	DROP TABLE [dbo].[tempfactTransactionDetails]
--WFSScriptProcessorCREnd
