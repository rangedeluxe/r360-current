--WFSScriptProcessorPrint CR 32839
--WFSScriptProcessorPrint Dropping stored procedure usp_IMSCheckDelete if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_IMSCheckDelete') IS NOT NULL
	DROP PROCEDURE OLTA.usp_IMSCheckDelete
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Dropping stored procedure usp_IMSDocumentDelete if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_IMSDocumentDelete') IS NOT NULL
	DROP PROCEDURE OLTA.usp_IMSDocumentDelete
--WFSScriptProcessorCREnd