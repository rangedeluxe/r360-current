--WFSScriptProcessorPrint CR 32342
--WFSScriptProcessorPrint Adding index dbo.Batch.IDX_Batch_OTIS_BankID_LockboxID_ProcessingDate_BatchID_GlobalBatchID if necessary.
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[Batch]') AND name = N'IDX_Batch_OTIS_BankID_LockboxID_ProcessingDate_BatchID_GlobalBatchID')
	CREATE NONCLUSTERED INDEX [IDX_Batch_OTIS_BankID_LockboxID_ProcessingDate_BatchID_GlobalBatchID] ON [dbo].[Batch] 
	(
		BankID, 
		LockboxID, 
		ProcessingDate, 
		BatchID, 
		GlobalBatchID
	)
--WFSScriptProcessorCREnd