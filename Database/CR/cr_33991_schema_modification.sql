--WFSScriptProcessorPrint CR 33991
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.dimDataEntryColumns if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='dimDataEntryColumns' AND COLUMN_NAME='ModificationDate')
	AND EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='dimDataEntryColumns' AND COLUMN_NAME='MarkSense')
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_factDataEntryDetails' )
		ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimDataEntryColumns_factDataEntryDetails

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='LockboxesDataEntryColumns' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_LockboxesDataEntryColumns' )
		ALTER TABLE OLTA.LockboxesDataEntryColumns DROP CONSTRAINT FK_dimDataEntryColumns_LockboxesDataEntryColumns

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='LockboxesDataEntrySetups' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_LockboxesDataEntrySetups' )
		ALTER TABLE OLTA.LockboxesDataEntrySetups DROP CONSTRAINT FK_dimDataEntryColumns_LockboxesDataEntrySetups

	RAISERROR('Dropping dimLockbox contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimDataEntryColumns' AND CONSTRAINT_NAME='PK_dimDataEntryColumns' )
		ALTER TABLE OLTA.dimDataEntryColumns DROP CONSTRAINT PK_dimDataEntryColumns

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_dimDataEntryColumns_TableType')
		ALTER TABLE OLTA.dimDataEntryColumns DROP CONSTRAINT CK_dimDataEntryColumns_TableType

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimDataEntryColumns_MarkSense')
		ALTER TABLE OLTA.dimDataEntryColumns DROP CONSTRAINT DF_dimDataEntryColumns_MarkSense

	RAISERROR('Rebuilding OLTA.dimDataEntryColumns',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.dimDataEntryColumns', 'OLDdimDataEntryColumns'

	CREATE TABLE OLTA.dimDataEntryColumns
	(
		DataEntryColumnKey int IDENTITY(1,1) NOT NULL 
			CONSTRAINT PK_dimDataEntryColumns PRIMARY KEY CLUSTERED,
		LoadDate datetime NOT NULL,
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_dimDataEntryColumns_ModificationDate DEFAULT GETDATE(),
		TableType tinyint NOT NULL
			CONSTRAINT CK_dimDataEntryColumns_TableType CHECK (TableType IN (0,1,2,3)),
		DataType smallint NOT NULL,
		FldLength tinyint NOT NULL,
		ScreenOrder tinyint NOT NULL,
		MarkSense TINYINT NOT NULL
			CONSTRAINT DF_dimDataEntryColumns_MarkSense DEFAULT 0,
		TableName varchar(36) NOT NULL,
		FldName varchar(32) NOT NULL,
		DisplayName varchar(32) NULL
	)
	
	RAISERROR('Creating index OLTA.dimDataEntryColumns.IDX_dimDataEntryColumns_FldName',10,1) WITH NOWAIT
	CREATE INDEX IDX_dimDataEntryColumns_FldName ON OLTA.dimDataEntryColumns(FldName)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimDataEntryColumns', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'dimDataEntryColumns';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Data Entry Columns dimension is a type 1 SCD holding an entry for 
*	each unique data entry column name. 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/10/2011 CR 33213 JPB	Added Marked Sense column. Also reorged columns.
* 04/22/2011 CR 33991 JPB	Added ModificationDate.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = dimDataEntryColumns

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
	SET IDENTITY_INSERT OLTA.dimDataEntryColumns ON
	INSERT INTO OLTA.dimDataEntryColumns 
	(
		DataEntryColumnKey,
		LoadDate,
		ModificationDate,
		TableType,
		DataType,
		FldLength,
		ScreenOrder,
		MarkSense,
		TableName,
		FldName,
		DisplayName
	) 
	SELECT	OLTA.OLDdimDataEntryColumns.DataEntryColumnKey,
			OLTA.OLDdimDataEntryColumns.LoadDate,
			OLTA.OLDdimDataEntryColumns.LoadDate AS ModificationDate,
			OLTA.OLDdimDataEntryColumns.TableType,
			OLTA.OLDdimDataEntryColumns.DataType,
			OLTA.OLDdimDataEntryColumns.FldLength,
			OLTA.OLDdimDataEntryColumns.ScreenOrder,
			OLTA.OLDdimDataEntryColumns.MarkSense,
			OLTA.OLDdimDataEntryColumns.TableName,
			OLTA.OLDdimDataEntryColumns.FldName,
			OLTA.OLDdimDataEntryColumns.DisplayName
	FROM 	OLTA.OLDdimDataEntryColumns
	SET IDENTITY_INSERT OLTA.dimDataEntryColumns OFF

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.factDataEntryDetails ADD
		CONSTRAINT FK_dimDataEntryColumns_factDataEntryDetails FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey)

	ALTER TABLE OLTA.factDataEntrySummary ADD
		CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey)

	ALTER TABLE OLTA.LockboxesDataEntryColumns ADD
		CONSTRAINT FK_dimDataEntryColumns_LockboxesDataEntryColumns FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey)

	ALTER TABLE OLTA.LockboxesDataEntrySetups ADD
		CONSTRAINT FK_dimDataEntryColumns_LockboxesDataEntrySetups FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey)

	RAISERROR('Granting SELECT ON OLTA.dimDataEntryColumns',10,1) WITH NOWAIT
	GRANT SELECT ON [OLTA].[dimDataEntryColumns] TO [OLTAUser]

	IF OBJECT_ID('OLTA.OLDdimDataEntryColumns') IS NOT NULL
		DROP TABLE OLTA.OLDdimDataEntryColumns
END