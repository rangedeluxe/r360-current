--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271168
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factDataEntryDetails.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntryDetails')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factDataEntryDetails'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey')
			DROP INDEX IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey ON RecHubData.factDataEntryDetails;
 		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_BankKey')
			DROP INDEX IDX_factDataEntryDetails_BankKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_OrganizationKey')
			DROP INDEX IDX_factDataEntryDetails_OrganizationKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_ClientAccountKey')
			DROP INDEX IDX_factDataEntryDetails_ClientAccountKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey')
			DROP INDEX IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_ImmutableDateKey')
			DROP INDEX IDX_factDataEntryDetails_ImmutableDateKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID')
			DROP INDEX IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence')
			DROP INDEX IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID')
			DROP INDEX IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey')
			DROP INDEX IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey ON RecHubData.factDataEntryDetails;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDataEntryDetails_SourceBatchIDDepositDateKey')
			DROP INDEX IDX_factDataEntryDetails_SourceBatchIDDepositDateKey ON RecHubData.factDataEntryDetails; 

		/****** Object:  Index PK_factDataEntryDetails    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factDataEntryDetails')
			ALTER TABLE RecHubData.factDataEntryDetails DROP CONSTRAINT PK_factDataEntryDetails;


		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factDataEntryDetails ADD
			CONSTRAINT PK_factDataEntryDetails PRIMARY KEY NONCLUSTERED (factDataEntryDetailKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey ON RecHubData.factDataEntryDetails
		(
			DepositDateKey,
			factDataEntryDetailKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_BankKey ON RecHubData.factDataEntryDetails (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_OrganizationKey ON RecHubData.factDataEntryDetails (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_ClientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_ClientAccountKey ON RecHubData.factDataEntryDetails (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey ON RecHubData.factDataEntryDetails (WorkgroupDataEntryColumnKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_ImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKey ON RecHubData.factDataEntryDetails (ImmutableDateKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID ON RecHubData.factDataEntryDetails
		(
			WorkgroupDataEntryColumnKey ASC,
			DepositDateKey ASC,
			BankKey ASC,
			OrganizationKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			TransactionID ASC
		)
		INCLUDE 
		( 
			BatchSequence,
			DataEntryValue,
			DataEntryValueDateTime,
			DataEntryValueMoney
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence ON RecHubData.factDataEntryDetails
		(
			DepositDateKey ASC,
			BatchID ASC,
			TransactionID ASC,
			BatchSequence ASC
		)
		INCLUDE
		(          
			OrganizationKey,
			WorkgroupDataEntryColumnKey,
			DataEntryValue
		)  $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID ON RecHubData.factDataEntryDetails
		(
			DepositDateKey ASC,
			OrganizationKey ASC,
			BatchID ASC,
			TransactionID ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey ON RecHubData.factDataEntryDetails 
		(
			ImmutableDateKey, 
			SourceBatchID, 
			TransactionID, 
			BatchSequence,
			DepositDateKey
		) 
		INCLUDE 
		(
			factDataEntryDetailKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			BatchSourceKey, 
			BatchPaymentTypeKey, 
			DepositStatus, 
			WorkgroupDataEntryColumnKey, 
			CreationDate, 
			ModificationDate, 
			DataEntryValueDateTime, 
			DataEntryValueFloat, 
			DataEntryValueMoney, 
			DataEntryValue
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factDataEntryDetails_SourceBatchIDDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_SourceBatchIDDepositDateKey ON RecHubData.factDataEntryDetails 
		(
			SourceBatchID,
			DepositDateKey
		) 
		INCLUDE 
		(
			factDataEntryDetailKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			ImmutableDateKey,
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			BatchSourceKey, 
			BatchPaymentTypeKey, 
			DepositStatus, 
			WorkgroupDataEntryColumnKey,
			TransactionID, 
			BatchSequence, 
			CreationDate, 
			ModificationDate, 
			DataEntryValueDateTime, 
			DataEntryValueFloat, 
			DataEntryValueMoney,
			DataEntryValue
		) $(OnDataPartition);

	END
	ELSE
		RAISERROR('WI 271168 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271168 RecHubData.factDataEntryDetails table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd

