--WFSScriptProcessorPrint CR 53363
--WFSScriptProcessorPrint Add IDX_LoadStatDetails_LoadStatID to LoadStatDetails if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[LoadStatDetails]') AND name = N'IDX_LoadStatDetails_LoadStatID' )
BEGIN

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'LoadStatDetails', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'LoadStatDetails';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/18/2009
*
* Purpose: 
*		   
*
* Modification History
* 08/18/2009 CR 25817 JPB	Created
* 06/11/2012 CR 53363 JPB	Added index IDX_LoadStatDetails_LoadStatID.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = LoadStatDetails
	
	RAISERROR('Applying new index',10,1) WITH NOWAIT
	CREATE INDEX IDX_LoadStatDetails_LoadStatID ON OLTA.LoadStatDetails(LoadStatID);	
	
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd