--WFSScriptProcessorPrint WI 125182
--WFSScriptProcessorPrint Adding additional columns to RecHubSystem.CDSQueue if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubSystem' AND TABLE_NAME = 'CDSQueue' AND COLUMN_NAME = 'SiteBankID' )
BEGIN
	RAISERROR('Dropping CDSQueue contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubSystem' AND TABLE_NAME='CDSQueue' AND CONSTRAINT_NAME='PK_CDSQueue' )
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT PK_CDSQueue;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubSystem' AND CONSTRAINT_NAME = 'CK_CDSQueue_QueueType')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT CK_CDSQueue_QueueType;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubSystem' AND CONSTRAINT_NAME = 'CK_CDSQueue_QueueStatus')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT CK_CDSQueue_QueueStatus;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ResponseStatus')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_ResponseStatus;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubSystem' AND CONSTRAINT_NAME = 'CK_CDSQueue_ActionCode')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT CK_CDSQueue_ActionCode;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_RetryCount')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_RetryCount;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_CreationDate')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_CreationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_CreatedBy')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_CreatedBy

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ModificationDate')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_ModificationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ModifiedBy')
		ALTER TABLE RecHubSystem.CDSQueue DROP CONSTRAINT DF_CDSQueue_ModifiedBy;
			
	RAISERROR('Dropping CDSQueue indexes',10,1) WITH NOWAIT
	IF EXISTS (SELECT 1 FROM sys.indexes where name = 'IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID')
		DROP INDEX IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID ON RecHubSystem.CDSQueue;

	IF EXISTS (SELECT 1 FROM sys.indexes where name = 'IDX_CDSQueue_QueueStatusModificationDateRetryCount')
		DROP INDEX IDX_CDSQueue_QueueStatusModificationDateRetryCount ON RecHubSystem.CDSQueue;

	RAISERROR('Rebuilding RecHubSystem.CDSQueue',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubSystem.CDSQueue', 'OLDCDSQueue';

	CREATE TABLE RecHubSystem.CDSQueue
	(
		CDSQueueID BIGINT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_CDSQueue PRIMARY KEY CLUSTERED,
		QueueType TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_QueueType CHECK(QueueType IN (10,20,70)), 
		QueueStatus TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99)), 
		ResponseStatus TINYINT NOT NULL
			CONSTRAINT DF_CDSQueue_ResponseStatus DEFAULT 255,
		ActionCode TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_ActionCode CHECK(ActionCode IN (1,2)),
		RetryCount TINYINT NOT NULL
			CONSTRAINT DF_CDSQueue_RetryCount DEFAULT 0,
		CreationDate DATETIME NOT NULL
			CONSTRAINT DF_CDSQueue_CreationDate DEFAULT GETDATE(),
		CreatedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_CDSQueue_CreatedBy DEFAULT SUSER_NAME(),
		ModificationDate DATETIME NOT NULL
			CONSTRAINT DF_CDSQueue_ModificationDate DEFAULT GETDATE(),
		ModifiedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_CDSQueue_ModifiedBy DEFAULT SUSER_NAME(),
		OLOrganizationID UNIQUEIDENTIFIER NULL,
		OrganizationCode VARCHAR(20) NULL,
		OLClientAccountID UNIQUEIDENTIFIER NULL,
		SiteBankID INT NULL,
		SiteClientAccountID INT NULL,
		FileDescriptor VARCHAR(30) NULL,
		FileDescription VARCHAR(64) NULL,
		XMLResponseDocument XML NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubSystem', 'TABLE', 'CDSQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubSystem',
			@level1type = N'TABLE',
			@level1name = N'CDSQueue';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/15/2013
*
* Purpose: Queue items to be processed by the intergraPAY import SSIS packages.
*		   
*
* ActionCode:
*	1 - Insert/Update
*	2 - Delete

* QueueStatus:
*	10 - Ready to sent to integraPAY Consolidated Database
*	15 - Failed processing on integraPAY Consolidated Database - but can resend
*	20 - Sent to integraPAY Consolidated Database, waiting for a response
*	30 - Failed processing on integraPAY Consolidated Database - but cannot resend
*	99 - Successfully processed on integraPAY Consolidated Database
*
*
* QueueType:
*	10 - OLOrganizationID
*	20 - OLClientAccountID
*	70 - Document Type
*
* 
* Modification History
* 06/15/2013 WI 103288 JPB	Created
* 12/11/2013 WI 125182 JPB	Added SiteBankID and SiteClientAccountID.
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubSystem,
	@level1type = N'TABLE',@level1name = CDSQueue;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
			
	SET IDENTITY_INSERT RecHubSystem.CDSQueue ON

	INSERT INTO RecHubSystem.CDSQueue
	(
		CDSQueueID,
		QueueType, 
		QueueStatus, 
		ResponseStatus,
		ActionCode,
		RetryCount,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		OLOrganizationID,
		OrganizationCode,
		OLClientAccountID,
		FileDescriptor,
		FileDescription,
		XMLResponseDocument
	) 
	SELECT	
		CDSQueueID,
		QueueType, 
		QueueStatus, 
		ResponseStatus,
		ActionCode,
		RetryCount,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		OLOrganizationID,
		OrganizationCode,
		OLClientAccountID,
		FileDescriptor,
		FileDescription,
		XMLResponseDocument
	FROM 	
		RecHubSystem.OLDCDSQueue;

	SET IDENTITY_INSERT RecHubSystem.CDSQueue OFF

	RAISERROR('Creating index IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID ON RecHubSystem.CDSQueue 
	(
		QueueType,
		QueueStatus,
		CDSQueueID
	);

	RAISERROR('Creating index IDX_CDSQueue_QueueStatusModificationDateRetryCount',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_CDSQueue_QueueStatusModificationDateRetryCount ON RecHubSystem.CDSQueue
	(
		QueueStatus,
		ModificationDate,
		RetryCount
	);

	IF OBJECT_ID('RecHubSystem.OLDCDSQueue') IS NOT NULL
		DROP TABLE RecHubSystem.OLDCDSQueue
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
