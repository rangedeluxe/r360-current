--WFSScriptProcessorPrint CR 29012
--WFSScriptProcessorForeignKey
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'OLTA' AND CONSTRAINT_NAME = 'FK_ProcessingDate_factDataEntryDetails')
	ALTER TABLE OLTA.factDataEntryDetails ADD
			   CONSTRAINT FK_ProcessingDate_factDataEntryDetails FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey)
