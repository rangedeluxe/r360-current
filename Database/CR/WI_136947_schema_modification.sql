--WFSScriptProcessorPrint WI 136947
--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint Update RecHubData.ClientAccountsDataEntryColumns if necessary
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='ClientAccountsDataEntryColumns' AND COLUMN_NAME='HubCreated' )
	BEGIN
		RAISERROR('WI 136947 has already been applied to the database.',10,1) WITH NOWAIT;
	END
ELSE
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='ClientAccountsDataEntryColumns')
		BEGIN
			RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='ClientAccountsDataEntryColumns' AND CONSTRAINT_NAME='FK_ClientAccountsDataEntryColumns_dimDataEntryColumns' )
				ALTER TABLE RecHubData.ClientAccountsDataEntryColumns DROP CONSTRAINT FK_ClientAccountsDataEntryColumns_dimDataEntryColumns;

			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='ClientAccountsDataEntryColumns' AND CONSTRAINT_NAME='FK_ClientAccountsDataEntryColumns_dimClientAccounts' )
				ALTER TABLE RecHubData.ClientAccountsDataEntryColumns DROP CONSTRAINT FK_ClientAccountsDataEntryColumns_dimClientAccounts;

			RAISERROR('Dropping ClientAccountsDataEntryColumns contraints',10,1) WITH NOWAIT;
			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='ClientAccountsDataEntryColumns' AND CONSTRAINT_NAME='PK_ClientAccountsDataEntryColumns' )
				ALTER TABLE RecHubData.ClientAccountsDataEntryColumns DROP CONSTRAINT PK_ClientAccountsDataEntryColumns;

			RAISERROR('Rebuilding RecHubData.ClientAccountsDataEntryColumns',10,1) WITH NOWAIT;
			EXEC sp_rename 'RecHubData.ClientAccountsDataEntryColumns', 'OLDClientAccountsDataEntryColumns';

			CREATE TABLE RecHubData.ClientAccountsDataEntryColumns
			(
				   ClientAccountKey		INT NOT NULL,
				   DataEntryColumnKey	INT NOT NULL,
				   HubCreated			BIT NOT NULL DEFAULT 0
			);

			IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'ClientAccountsDataEntryColumns', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'ClientAccountsDataEntryColumns';		

			EXEC sys.sp_addextendedproperty 
			@name = N'Table_Description',
			@value = N'/******************************************************************************
		** WAUSAU Financial Systems (WFS)
		** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
		*******************************************************************************
		*******************************************************************************
		** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
		*******************************************************************************
		* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
		* other trademarks cited herein are property of their respective owners.
		* These materials are unpublished confidential and proprietary information 
		* of WFS and contain WFS trade secrets.  These materials may not be used, 
		* copied, modified or disclosed except as expressly permitted in writing by 
		* WFS (see the WFS license agreement for details).  All copies, modifications 
		* and derivative works of these materials are property of WFS.
		*
		* Author: JPB
		* Date: 03/26/2009
		*
		* Purpose: Associative table 
		*		   
		*
		* Modification History
		* 03/26/2009 CR 25817 JPB	Created
		* 02/28/2013 WI 89904 JBS	Update Table to 2.0 release.  Rename Schema Name
		*							Rename table from LockboxesDataEntryColumns to 
		*							ClientAccountsDataEntryColumns
		*							Renamed all Constraints, Renamed Columns:
		*							LockboxKey to ClientAccountKey
		* 08/22/2014 WI 136947 KLC	Added HubCreated column
		******************************************************************************/',
			@level0type = N'SCHEMA',@level0name = RecHubData,
			@level1type = N'TABLE',@level1name = ClientAccountsDataEntryColumns;

			RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
			INSERT INTO RecHubData.ClientAccountsDataEntryColumns 
			(
				ClientAccountKey,
				DataEntryColumnKey,
				HubCreated	
			) 
			SELECT	
				ClientAccountKey,
				DataEntryColumnKey,
				0
			FROM 	
				RecHubData.OLDClientAccountsDataEntryColumns;

			RAISERROR('Creating PK and FKs for ClientAccountsDataEntryColumns',10,1) WITH NOWAIT;
			ALTER TABLE RecHubData.ClientAccountsDataEntryColumns ADD
			   CONSTRAINT PK_ClientAccountsDataEntryColumns PRIMARY KEY CLUSTERED (ClientAccountKey, DataEntryColumnKey),
			   CONSTRAINT FK_ClientAccountsDataEntryColumns_dimDataEntryColumns FOREIGN KEY(DataEntryColumnKey) REFERENCES RecHubData.dimDataEntryColumns(DataEntryColumnKey),
			   CONSTRAINT FK_ClientAccountsDataEntryColumns_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);

			RAISERROR('Dropping old table',10,1) WITH NOWAIT;
			IF OBJECT_ID('RecHubData.OLDClientAccountsDataEntryColumns') IS NOT NULL
				DROP TABLE RecHubData.OLDClientAccountsDataEntryColumns;

		END
	ELSE 
		BEGIN
			RAISERROR('WI 89904 Must be added to Database before 136947 can be applied.',10,1) WITH NOWAIT;
		END

--WFSScriptProcessorCREnd
