--WFSScriptProcessorPrint CR 54115
--WFSScriptProcessorPrint Removing obsoleted ICON objects if necessary
--WFSScriptProcessorCRBegin
SET NOCOUNT ON;

DECLARE @SQLCommand VARCHAR(MAX),
		@Message VARCHAR(MAX),
		@Loop INT,
		@CRLF CHAR(2),
		@TAB CHAR(1);

SET @CRLF = CHAR(13)+ CHAR(10);
SET @TAB = CHAR(9);

/* stored procedures */
DECLARE @StoredProcedures TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	ProcedureName VARCHAR(128)
);

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_ICON%'
ORDER BY ROUTINE_NAME;

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 'OLTA','usp_ICONProcessClientSetup'
UNION ALL SELECT 'OLTA','usp_OLTAICONClientInsertQueueReader';

SET @Loop = 1;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @StoredProcedures) )
BEGIN
	SELECT @SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + ProcedureName  + CHAR(39) + ') IS NOT NULL' + @CRLF + @TAB + 'DROP PROCEDURE ' + SchemaName + '.' + ProcedureName,
			@Message = 'Removing stored procedure ' + SchemaName + '.' + ProcedureName
	FROM @StoredProcedures
	WHERE RowID = @Loop;

	RAISERROR(@Message,10,1) WITH NOWAIT;
	EXEC(@SQLCommand);
	SET @Loop = @Loop + 1;
END

/* SSB */
DECLARE @SSB TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	SSBName VARCHAR(128),
	SSBObjectType VARCHAR(128)
);

INSERT INTO @SSB(SchemaName,SSBName,SSBObjectType)
SELECT 'dbo','OLTAICONClientInsertSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTAICONClientInsertMsg','MESSAGE TYPE';

SET @Loop = 1;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @SSB) )
BEGIN
	SELECT @SQLCommand = 'IF EXISTS(SELECT * FROM '
		+CASE
			WHEN SSBObjectType = 'MESSAGE TYPE' THEN 'sys.service_message_types'
			WHEN SSBObjectType = 'CONTRACT' THEN 'sys.service_contracts'
			WHEN SSBObjectType = 'QUEUE' THEN 'sys.service_queues'
			WHEN SSBObjectType = 'SERVICE' THEN 'sys.services'
			WHEN SSBObjectType = 'ROUTE' THEN 'sys.routes'
			WHEN SSBObjectType = 'CERTIFICATE' THEN 'sys.certificates'
			WHEN SSBObjectType = 'REMOTE SERVICE BINDING' THEN 'sys.remote_service_bindings'
			WHEN SSBObjectType = 'USER' THEN 'sys.database_principals'
		END
		+  ' WHERE Name = ' + CHAR(39) + SSBName  + CHAR(39) + ')' + @CRLF + @TAB + 'DROP ' + SSBObjectType + ' ' + SSBName,
		@Message = 'Removing ' + SSBObjectType + ' ' + SSBName
	FROM @SSB
	WHERE RowID = @Loop;

	RAISERROR(@Message,10,1) WITH NOWAIT;
	EXEC(@SQLCommand);
	SET @Loop = @Loop + 1;
END
--WFSScriptProcessorCREnd