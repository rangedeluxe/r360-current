--WFSScriptProcessorPrint CR 30456
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_SCHEMA = 'OLTA' AND  TABLE_NAME = 'dimLockboxesView')
	EXEC sp_refreshview 'OLTA.dimLockboxesView'
--WFSScriptProcessorCREnd