--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 237951
--WFSScriptProcessorPrint Adding index IDX_dimBatchSources_ShortNameBatchSourceImportTypeKeyIsActive if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_dimBatchSources_ShortNameBatchSourceImportTypeKeyIsActive') 
	BEGIN
		RAISERROR('WI 237951 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_dimBatchSources_ShortNameBatchSourceImportTypeKeyIsActive',10,1) WITH NOWAIT;
		CREATE INDEX IDX_dimBatchSources_ShortNameBatchSourceImportTypeKeyIsActive ON RecHubData.dimBatchSources 
		(
			ShortName,
			BatchSourceKey,
			ImportTypeKey,
			IsActive
		);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimBatchSources', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'dimBatchSources';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = dimBatchSources,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/10/2011
*
* Purpose: Batch Sources dimension is a static data dimension for batch sources. 
*
* Modification History
* 01/10/2011 CR 32310 JPB	Created
* 03/01/2011 CR 33123 JPB	Removed IDENTITY property from BatchSourceKey.
* 01/24/2012 CR 49629 JPB	Added standard Creation/Modification columns,
*							removed LoadDate.
* 03/01/2013 WI 89976 JBS	Update table to 2.0 release.
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
* 06/13/2013 WI 105327 JPB	Added IsActive.
* 03/11/2014 WI 132464 JBS	Adding permission tags for table
* 05/22/2014 WI 139928 JPB	Changes to support RAAM.
*							-BatchSourceKey to SMALLINT IDENTITY
*							-Added EntityID
*							-Added ImportTypeKey
* 08/27/2015 WI 237951 JPB	Added new index for SSIS.
******************************************************************************/';
	END
--WFSScriptProcessorCREnd