--WFSScriptProcessorPrint CR 50105
--WFSScriptProcessorPrint Modifing OLTA.ImportSchemaTypes if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'ImportSchemaTypes' AND COLUMN_NAME = 'ImportSchemaType' AND CHARACTER_MAXIMUM_LENGTH = 128 )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='ImportSchemas' AND CONSTRAINT_NAME='FK_ImportSchemaTypes_ImportSchemas' )
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT FK_ImportSchemaTypes_ImportSchemas

	RAISERROR('Dropping ImportSchemaTypes contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='ImportSchemaTypes' AND CONSTRAINT_NAME='PK_ImportSchemaTypes' )
		ALTER TABLE OLTA.ImportSchemaTypes DROP CONSTRAINT PK_ImportSchemaTypes

	RAISERROR('Rebuilding OLTA.ImportSchemaTypes',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.ImportSchemaTypes', 'OLDImportSchemaTypes'

	CREATE TABLE OLTA.ImportSchemaTypes
	(
		ImportSchemaTypeID INT NOT NULL 
			CONSTRAINT PK_ImportSchemaTypes PRIMARY KEY CLUSTERED,
		ImportSchemaType VARCHAR(128) NOT NULL,
		CreationDate datetime NOT NULL 
			CONSTRAINT DF_ImportSchemaTypes_CreationDate DEFAULT(GETDATE()),
		CreatedBy varchar(32) NOT NULL 
			CONSTRAINT DF_ImportSchemaTypes_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_ImportSchemaTypes_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy varchar(32) NOT NULL 
			CONSTRAINT DF_ImportSchemaTypes_ModifiedBy DEFAULT(SUSER_SNAME())
	)

	RAISERROR('Creating index OLTA.ImportSchemaTypes.IDX_ImportSchemaTypes_ImportSchemaType',10,1) WITH NOWAIT
	CREATE UNIQUE INDEX IDX_ImportSchemaTypes_ImportSchemaType ON OLTA.ImportSchemaTypes(ImportSchemaType)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'ImportSchemaTypes', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'ImportSchemaTypes';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table is used by the dynamic partitioning routine it will hold 
*	static data associated with the fact table partitioning.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/09/2012 CR 50105 JPB	Removed IDENTITY from ImportSchemaTypeID, changed
*							length of ImportSchemaType to 128, added standard 
*							creation/modification columns.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = ImportSchemaTypes

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	INSERT INTO OLTA.ImportSchemaTypes
	(
		ImportSchemaTypeID,
		ImportSchemaType,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy	
	)
	SELECT	ImportSchemaTypeID,
			ImportSchemaType,
			GETDATE(),
			'sa',
			GETDATE(),
			'sa'	
	FROM	OLTA.OLDImportSchemaTypes

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.ImportSchemas ADD        
		   CONSTRAINT FK_ImportSchemaTypes_ImportSchemas FOREIGN KEY(ImportSchemaTypeID) REFERENCES OLTA.ImportSchemaTypes (ImportSchemaTypeID)

	IF OBJECT_ID('OLTA.OLDImportSchemaTypes') IS NOT NULL
		DROP TABLE OLTA.OLDImportSchemaTypes

END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
