--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 165627
--WFSScriptProcessorPrint Adding LogonEntityID and LogonEntityName to RecHubUser.Users table name if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT TOP (1) 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubUser' AND TABLE_NAME = 'Users')
BEGIN
	IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubUser' AND TABLE_NAME = 'Users' AND COLUMN_NAME = 'LogonEntityID')
	BEGIN
		RAISERROR('Adding LogonEntityID and LogonEntityName to RecHubUser.Users.',10,1) WITH NOWAIT;
		
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND CONSTRAINT_NAME='FK_Alerts_Users' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT FK_Alerts_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factDataAuditDetails' AND CONSTRAINT_NAME='FK_factDataAuditDetails_Users' )
			ALTER TABLE RecHubAudit.factDataAuditDetails DROP CONSTRAINT FK_factDataAuditDetails_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factDataAuditMessages' AND CONSTRAINT_NAME='FK_factDataAuditMessages_Users' )
			ALTER TABLE RecHubAudit.factDataAuditMessages DROP CONSTRAINT FK_factDataAuditMessages_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factDataAuditSummary' AND CONSTRAINT_NAME='FK_factDataAuditSummary_Users' )
			ALTER TABLE RecHubAudit.factDataAuditSummary DROP CONSTRAINT FK_factDataAuditSummary_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factEventAuditMessages' AND CONSTRAINT_NAME='FK_factEventAuditMessages_Users' )
			ALTER TABLE RecHubAudit.factEventAuditMessages DROP CONSTRAINT FK_factEventAuditMessages_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factEventAuditSummary' AND CONSTRAINT_NAME='FK_factEventAuditSummary_Users' )
			ALTER TABLE RecHubAudit.factEventAuditSummary DROP CONSTRAINT FK_factEventAuditSummary_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotificationFiles' AND CONSTRAINT_NAME='FK_factNotificationFiles_Users' )
			ALTER TABLE RecHubData.factNotificationFiles DROP CONSTRAINT FK_factNotificationFiles_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotifications' AND CONSTRAINT_NAME='FK_factNotifications_Users' )
			ALTER TABLE RecHubData.factNotifications DROP CONSTRAINT FK_factNotifications_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='DecisioningTransactions' AND CONSTRAINT_NAME='FK_DecisioningTransactions_Users' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT FK_DecisioningTransactions_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='OLClientAccountUsers' AND CONSTRAINT_NAME='FK_OLClientAccountUsers_Users' )
			ALTER TABLE RecHubUser.OLClientAccountUsers DROP CONSTRAINT FK_OLClientAccountUsers_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='OLUserMachines' AND CONSTRAINT_NAME='FK_Users_OLUserMachines' )
			ALTER TABLE RecHubUser.OLUserMachines DROP CONSTRAINT FK_Users_OLUserMachines;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='OLUserPreferences' AND CONSTRAINT_NAME='FK_OLUserPreferences_Users' )
			ALTER TABLE RecHubUser.OLUserPreferences DROP CONSTRAINT FK_OLUserPreferences_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='OLUserQuestions' AND CONSTRAINT_NAME='FK_OLUserQuestions_Users' )
			ALTER TABLE RecHubUser.OLUserQuestions DROP CONSTRAINT FK_OLUserQuestions_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='ReportInstance' AND CONSTRAINT_NAME='FK_ReportInstance_Users' )
			ALTER TABLE RecHubUser.ReportInstance DROP CONSTRAINT FK_ReportInstance_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='Session' AND CONSTRAINT_NAME='FK_Session_Users' )
			ALTER TABLE RecHubUser.[Session] DROP CONSTRAINT FK_Session_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionEmulation' AND CONSTRAINT_NAME='FK_SessionEmulation_Users' )
			ALTER TABLE RecHubUser.SessionEmulation DROP CONSTRAINT FK_SessionEmulation_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='UserPasswordHistory' AND CONSTRAINT_NAME='FK_Users_UserPasswordHistory' )
			ALTER TABLE RecHubUser.UserPasswordHistory DROP CONSTRAINT FK_Users_UserPasswordHistory;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='UserPermissions' AND CONSTRAINT_NAME='FK_UserPermissions_Users' )
			ALTER TABLE RecHubUser.UserPermissions DROP CONSTRAINT FK_UserPermissions_Users;


		RAISERROR('Dropping RecHubUser.Users contraints',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='Users' AND CONSTRAINT_NAME='PK_Users' )
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT PK_Users;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_UserType')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_UserType;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_SuperUser')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_SuperUser;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_FailedLogonAttempts')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_FailedLogonAttempts;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_FailedSecurityQuestionAttempts')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_FailedSecurityQuestionAttempts;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_CreationDate')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_CreationDate;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_CreatedBy')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_CreatedBy;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_ModificationDate')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_ModificationDate;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Users_ModifiedBy')
			ALTER TABLE RecHubUser.Users DROP CONSTRAINT DF_Users_ModifiedBy;			

		RAISERROR('Dropping Indexes.',10,1) WITH NOWAIT
		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_Users_ExternalIDHash')
			DROP INDEX IDX_Users_ExternalIDHash 
				ON RecHubUser.Users;					
		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_Users_LogonName')
			DROP INDEX IDX_Users_LogonName 
				ON RecHubUser.Users;	
		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_Users_OLOrganizationIDLogonName')
			DROP INDEX IDX_Users_OLOrganizationIDLogonName 
				ON RecHubUser.Users;	
		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_Users_RA3MSID')
			DROP INDEX IDX_Users_RA3MSID 
				ON RecHubUser.Users;	


		RAISERROR('Rebuilding RecHubUser.Users',10,1) WITH NOWAIT;
		EXEC sp_rename 'RecHubUser.Users', 'OLDUsers';


		CREATE TABLE RecHubUser.Users 
		(
			UserID INT IDENTITY(1,1) NOT NULL 
				CONSTRAINT PK_Users PRIMARY KEY CLUSTERED,
			RA3MSID UNIQUEIDENTIFIER NOT NULL,
			OLOrganizationID UNIQUEIDENTIFIER NULL,
			UserType TINYINT NOT NULL 
				CONSTRAINT DF_Users_UserType DEFAULT(0),
			LogonName VARCHAR(24) NOT NULL,
			LogonEntityID INT NULL,
			LogonEntityName VARCHAR(50) NULL,
			FirstName VARCHAR(16) NOT NULL,
			MiddleInitial VARCHAR(3) NULL,
			LastName VARCHAR(31) NOT NULL,
			SuperUser BIT NOT NULL 
				CONSTRAINT DF_Users_SuperUser DEFAULT(0),
			[Password] VARCHAR(44) NOT NULL,
			PasswordSet DATETIME NOT NULL,
			PasswordExpirationTime DATETIME NULL,
			IsFirstTime BIT NOT NULL,
			FailedLogonAttempts TINYINT NOT NULL 
				CONSTRAINT DF_Users_FailedLogonAttempts DEFAULT(0),
			FailedSecurityQuestionAttempts TINYINT NOT NULL 
				CONSTRAINT DF_Users_FailedSecurityQuestionAttempts DEFAULT(0),
			EmailAddress VARCHAR(50) NULL,
			IsActive BIT NOT NULL,
			ExternalID1 VARCHAR(32) NULL,
			ExternalID2 VARCHAR(32) NULL,
			ExternalIDHash VARCHAR(32) NULL,
			SetupToken VARCHAR(36) NULL,
			CreationDate DATETIME NOT NULL 
				CONSTRAINT DF_Users_CreationDate DEFAULT(GETDATE()),
			CreatedBy VARCHAR(128) NOT NULL 
				CONSTRAINT DF_Users_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate DATETIME NOT NULL 
				CONSTRAINT DF_Users_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy VARCHAR(128) NOT NULL 
				CONSTRAINT DF_Users_ModifiedBy DEFAULT(SUSER_SNAME())
		);
			
		RAISERROR('Updating RecHubUser.Users table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'Users', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubUser',
				@level1type = N'TABLE',
				@level1name = N'Users';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 07/25/2011 CR 45437 JPB	Changed password to 44.
* 09/26/2011 CR 46988 JPB	Added PasswordExpirationTime.
* 10/12/2011 CR 47241 JPB	Added FailedSecurityQuestionAttempts.
* 07/23/2012 CR 54246 JPB	Added ExternalIDHash.
* 02/16/2013 WI 89534 JBS	Update table to 2.0 release. Change Schema Name
*							Remove ContactID and the FK to dbo.Contact
*							LogonName Increase to VARCHAR(24)
*							FP: WI 83964, WI 85099
*							Adding RA3MSID and index for RA3M Mapping
* 09/12/2014 WI 165627 RDS	Add LogonEntityID and LogonEntityName columns
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubUser,
		@level1type = N'TABLE',@level1name = Users;

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
		SET IDENTITY_INSERT RecHubUser.Users ON;
	
		INSERT INTO RecHubUser.Users 
		(
			UserID,
			RA3MSID,
			OLOrganizationID,
			UserType,
			LogonName,
			FirstName,
			MiddleInitial,
			LastName ,
			SuperUser,
			[Password] ,
			PasswordSet,
			PasswordExpirationTime,
			IsFirstTime,
			FailedLogonAttempts,
			FailedSecurityQuestionAttempts,
			EmailAddress,
			IsActive,
			ExternalID1 ,
			ExternalID2,
			ExternalIDHash ,
			SetupToken,
			CreationDate,
			CreatedBy ,
			ModificationDate ,
			ModifiedBy 
		) 
		SELECT	
			UserID,
			RA3MSID,
			OLOrganizationID,
			UserType,
			LogonName,
			FirstName,
			MiddleInitial,
			LastName ,
			SuperUser,
			[Password] ,
			PasswordSet,
			PasswordExpirationTime,
			IsFirstTime,
			FailedLogonAttempts,
			FailedSecurityQuestionAttempts,
			EmailAddress,
			IsActive,
			ExternalID1 ,
			ExternalID2,
			ExternalIDHash ,
			SetupToken,
			CreationDate,
			CreatedBy ,
			ModificationDate ,
			ModifiedBy 
		FROM 	
			RecHubUser.OLDUsers;

		SET IDENTITY_INSERT RecHubUser.Users OFF;

		RAISERROR('Creating indexes ',10,1) WITH NOWAIT;
		CREATE INDEX IDX_Users_ExternalIDHash ON RecHubUser.Users (ExternalIDHash);

		CREATE NONCLUSTERED INDEX IDX_Users_LogonName ON RecHubUser.Users (LogonName);

		CREATE NONCLUSTERED INDEX IDX_Users_OLOrganizationIDLogonName ON RecHubUser.Users 
		(
			OLOrganizationID ASC,
			UserID ASC,
			LogonName ASC
		);

		CREATE NONCLUSTERED INDEX IDX_Users_RA3MSID ON RecHubUser.Users 
		(
			RA3MSID
		)
		INCLUDE
		(
			UserID, 
			OLOrganizationID
		);

		RAISERROR('Adding Foreign Keys to Users table',10,1) WITH NOWAIT;
		ALTER TABLE RecHubAlert.Alerts ADD
			CONSTRAINT FK_Alerts_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubAudit.factDataAuditDetails ADD 
			CONSTRAINT FK_factDataAuditDetails_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubAudit.factDataAuditMessages ADD 
			CONSTRAINT FK_factDataAuditMessages_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubAudit.factDataAuditSummary ADD 
			CONSTRAINT FK_factDataAuditSummary_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubAudit.factEventAuditMessages ADD 
			CONSTRAINT FK_factEventAuditMessages_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubAudit.factEventAuditSummary ADD 
			CONSTRAINT FK_factEventAuditSummary_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubData.factNotifications ADD
			CONSTRAINT FK_factNotifications_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubData.factNotificationFiles ADD
			CONSTRAINT FK_factNotificationFiles_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubException.DecisioningTransactions ADD 
			CONSTRAINT FK_DecisioningTransactions_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.OLClientAccountUsers ADD 
			CONSTRAINT FK_OLClientAccountUsers_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.OLUserMachines ADD CONSTRAINT FK_Users_OLUserMachines FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.OLUserPreferences ADD 
			CONSTRAINT FK_OLUserPreferences_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.OLUserQuestions 
			ADD CONSTRAINT FK_OLUserQuestions_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.ReportInstance ADD 
			CONSTRAINT FK_ReportInstance_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.[Session] ADD 
			CONSTRAINT FK_Session_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.SessionEmulation ADD 
			CONSTRAINT FK_SessionEmulation_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.UserPasswordHistory WITH NOCHECK
			ADD CONSTRAINT FK_Users_UserPasswordHistory FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubUser.UserPermissions ADD 
			CONSTRAINT FK_UserPermissions_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users (UserID);
  
		RAISERROR('Dropping old table',10,1) WITH NOWAIT;
		IF OBJECT_ID('RecHubData.OLDUsers') IS NOT NULL
			DROP TABLE RecHubData.OLDUsers;

	END
	ELSE
		BEGIN
			RAISERROR('WI 165627 has already been applied to the database.',10,1) WITH NOWAIT;
		END
END
ELSE
	BEGIN
		RAISERROR('WI 89534 is required before 165627.',10,1) WITH NOWAIT;
	END
--WFSScriptProcessorCREnd

