--WFSScriptProcessorPrint CR 47513
--WFSScriptProcessorPrint Update factTransactionSummary IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence if necessary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]') AND name = N'IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID')
BEGIN

	DROP INDEX [IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID] ON [OLTA].[factTransactionSummary]
	
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factTransactionSummary', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factTransactionSummary';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28224 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 03/11/2010 CR 29181 JPB	Added new index for Lockbox Search.
* 04/12/2010 CR 29358 JPB	Added ModificationDate.
* 01/12/2011 CR 32285 JPB	Added BatchSourceKey.
* 10/27/2011 CR 47513 JPB	Added TxnSequence to index.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = factTransactionSummary

	CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence ON OLTA.factTransactionSummary
	(
		[LockboxKey] ASC,
		[DepositDateKey] ASC,
		[BatchID] ASC,
		[DepositStatus] ASC,
		[BankKey] ASC,
		[CustomerKey] ASC,
		[ProcessingDateKey] ASC,
		[TransactionID] ASC,
		[TxnSequence] ASC
	) $(OnPartition)
END
--WFSScriptProcessorCREnd
