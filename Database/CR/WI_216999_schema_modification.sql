--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 216999
--WFSScriptProcessorPrint Adding index IDX_EventLog_AlertID
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_EventLog_AlertID') 
	BEGIN
		RAISERROR('WI 216999. Rebuilding index',10,1) WITH NOWAIT;  -- however we will rebuild index
		DROP INDEX IDX_EventLog_AlertID ON RecHubAlert.EventLog;

		CREATE INDEX IDX_EventLog_AlertID ON RecHubAlert.EventLog 
		(
			AlertID
		) 
		INCLUDE 
		(
			EventLogID, 
			EventRuleID, 
			NotificationMessageGroup, 
			EventStatus,
			BankKey,
			OrganizationKey, 
			ClientAccountKey, 
			EventSourceKey, 
			CreationDate, 
			ModificationDate, 
			EventMessage
		);
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_EventLog_AlertID',10,1) WITH NOWAIT;
		CREATE INDEX IDX_EventLog_AlertID ON RecHubAlert.EventLog 
		(
			AlertID
		) 
		INCLUDE 
		(
			EventLogID, 
			EventRuleID, 
			NotificationMessageGroup, 
			EventStatus,
			BankKey,
			OrganizationKey, 
			ClientAccountKey, 
			EventSourceKey, 
			CreationDate, 
			ModificationDate, 
			EventMessage
		);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAlert', 'TABLE', 'EventLog', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubAlert',
					@level1type = N'TABLE',
					@level1name = N'EventLog';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubAlert,
		@level1type = N'TABLE',@level1name = EventLog,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores Event Logs for R360 Alerts.
*
*
* Check Constraint definitions:
* EventStatus
*	10 - Ready to Process
*	15 - Error, can be reprocessed
*	20 - In Process
*	30 - Error, cannot be reprocessed
*	99 - Complete
*	
*
* Modification Date = Date/Time the record was changed
* Modification History
* 05/03/2013 WI 99840 JBS	Created
* 06/03/2014 WI 143688 JPB	Changed EventSourceKey to SMALLINT.
* 06/04/2015 WI 216999 JBS	Add index IDX_EventLog_AlertID
******************************************************************************/';

	END
--WFSScriptProcessorCREnd