--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 141435715, 142595657
--WFSScriptProcessorPrint Adding CreationDateKey to SessionActivityLog Table and partition it
--WFSScriptProcessorCRBegin

DECLARE @SessionActivityLogRetentionDays	INT = 365,
		@BeginDate				DATETIME,
		@BeginDateKey			INT,
		@Message				VARCHAR(90)

IF NOT EXISTS  (SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'SessionActivityLog'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubUser') = sys.tables.schema_id 
	AND sys.partition_schemes.name = 'SessionActivity')
BEGIN
	RAISERROR('Beginning Process to Add CreationDateKey to the SessionActivityLog table',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionActivityLogRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionActivityLogRetentionDays'
	SELECT @BeginDate = GETDATE() - @SessionActivityLogRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @BeginDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate <= @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest date of SessionActivityLog rows to keep is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionActivityLog' AND CONSTRAINT_NAME='FK_SessionActivityLog_ActivityCodes' )
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT FK_SessionActivityLog_ActivityCodes;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionActivityLog' AND CONSTRAINT_NAME='FK_SessionActivityLog_Session' )
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT FK_SessionActivityLog_Session;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionActivityLog' AND CONSTRAINT_NAME='FK_SessionActivityLog_OnlineImageQueue' )
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT FK_SessionActivityLog_OnlineImageQueue;
	
	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	--PK

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionActivityLog' AND CONSTRAINT_NAME='PK_SessionActivityLog' )
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT PK_SessionActivityLog;

	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_ActivityCode')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_ActivityCode;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_BankID')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_BankID;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_DeliveredToWeb')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_DeliveredToWeb;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_ItemCount')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_ItemCount;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_LockboxID')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_LockboxID;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_ModuleName')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_ModuleName;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_ProcessingDate')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_ProcessingDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionActivityLog_CreationDateKey')
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT DF_SessionActivityLog_CreationDateKey;

	RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionActivityLog_ActivityCode') 
		DROP INDEX RecHubUser.SessionActivityLog.IDX_SessionActivityLog_ActivityCode;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode') 
		DROP INDEX RecHubUser.SessionActivityLog.IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionActivityLog_OnlineImageQueueID') 
		DROP INDEX RecHubUser.SessionActivityLog.IDX_SessionActivityLog_OnlineImageQueueID;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionActivityLog_SessionID') 
		DROP INDEX RecHubUser.SessionActivityLog.IDX_SessionActivityLog_SessionID;

	RAISERROR('Rebuilding Table RecHubUser.SessionActivityLog.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubUser.SessionActivityLog', 'OLD_SessionActivityLog';

	CREATE TABLE RecHubUser.[SessionActivityLog]
	(
	SessionActivityLogID BIGINT IDENTITY(1,1) NOT NULL,
	ActivityCode INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ActivityCode DEFAULT(0),
	SessionID UNIQUEIDENTIFIER NOT NULL,
	CreationDateKey	INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	OnlineImageQueueID UNIQUEIDENTIFIER NULL,
	DeliveredToWeb BIT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_DeliveredToWeb DEFAULT(0),
	ModuleName VARCHAR(50) NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ModuleName DEFAULT('Unknown'),
	ItemCount int NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ItemCount DEFAULT(0),
	DateTimeEntered DATETIME NOT NULL,
	BankID INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_BankID DEFAULT((-1)), 
	ClientAccountID INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_LockboxID DEFAULT((-1)),
	ProcessingDate DATETIME NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ProcessingDate DEFAULT(CONVERT(datetime,CONVERT(varchar(10),GETDATE(),101)))  
	) ON SessionActivity (CreationDateKey);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT RecHubUser.SessionActivityLog ON;
	INSERT INTO RecHubUser.SessionActivityLog
	(
		SessionActivityLogID,
		ActivityCode,
		SessionID,
		CreationDateKey,
		OnlineImageQueueID,
		DeliveredToWeb,
		ModuleName,
		ItemCount,
		DateTimeEntered,
		BankID,
		ClientAccountID,
		ProcessingDate
	)
	SELECT
		SessionActivityLogID,
		ActivityCode,
		SessionID,
		CAST(CONVERT(VARCHAR(10), DateTimeEntered, 112) AS INT) AS CreationDateKey,
		OnlineImageQueueID,
		DeliveredToWeb,
		ModuleName,
		ItemCount,
		DateTimeEntered,
		BankID,
		ClientAccountID,
		ProcessingDate
	FROM
		RecHubUser.OLD_SessionActivityLog
	WHERE
		DateTimeEntered > @BeginDate - 1;

	SET IDENTITY_INSERT RecHubUser.SessionActivityLog OFF;

	RAISERROR('Adding Primary/Foreign Keys',10,1) WITH NOWAIT;

	ALTER TABLE RecHubUser.SessionActivityLog ADD 
		CONSTRAINT PK_SessionActivityLog PRIMARY KEY NONCLUSTERED (SessionActivityLogID, CreationDateKey)
	ALTER TABLE RecHubUser.SessionActivityLog ADD 
		CONSTRAINT FK_SessionActivityLog_ActivityCodes FOREIGN KEY(ActivityCode) REFERENCES RecHubUser.ActivityCodes(ActivityCode);
	ALTER TABLE RecHubUser.SessionActivityLog WITH NOCHECK ADD 
		CONSTRAINT FK_SessionActivityLog_Session FOREIGN KEY(SessionID,CreationDateKey) REFERENCES RecHubUser.Session(SessionID,CreationDateKey);

	RAISERROR('Adding IDX_SessionActivityLog_CreationDateKeySessionActivityLogID',10,1) WITH NOWAIT;
	CREATE CLUSTERED INDEX IDX_SessionActivityLog_CreationDateKeySessionActivityLogID ON RecHubUser.SessionActivityLog(CreationDateKey,SessionActivityLogID)
		ON SessionActivity (CreationDateKey);

	RAISERROR('Adding IDX_SessionActivityLog_ActivityCode',10,1) WITH NOWAIT;
	CREATE INDEX IDX_SessionActivityLog_ActivityCode ON RecHubUser.SessionActivityLog(ActivityCode) ON SessionActivity (CreationDateKey);
		
	RAISERROR('Adding IDX_SessionActivityLog_SessionID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_SessionActivityLog_SessionID ON RecHubUser.SessionActivityLog(SessionID) INCLUDE (CreationDateKey, ActivityCode) ON SessionActivity (CreationDateKey);

	RAISERROR('Adding IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode ON RecHubUser.SessionActivityLog
	(
		BankID,
		ClientAccountID,
		ProcessingDate,
		DeliveredToWeb,
		ActivityCode
	) ON SessionActivity (CreationDateKey);

	RAISERROR('Adding IDX_SessionActivityLog_OnlineImageQueueID',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionActivityLog_OnlineImageQueueID ON RecHubUser.SessionActivityLog (OnlineImageQueueID) INCLUDE (DeliveredToWeb) 
		ON SessionActivity (CreationDateKey);

	RAISERROR('Updating RecHubUser.SessionActivityLog.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'SessionActivityLog', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubUser',
			@level1type = N'TABLE',
			@level1name = N'SessionActivityLog';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubUser,
	@level1type = N'TABLE',@level1name = SessionActivityLog,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817	JJR		Created
* 12/04/2012 WI 70807	JPB		Added index for billing (FP:CR 56577)
* 01/02/2013 WI 77759	JPB		Added missing index (FP:77753)
* 02/17/2013 WI 88324	JBS		Update for 2.0 release.  Added Included column to
*								IDX_SessionActivityLog_OnlineImageQueueID
* 03/29/2017 PT 141435715 MGE	Add CreationDateKey & as part of clustered index to prepare for patitioning.
* 04/03/2017 PT 142595657 MGE	Add parition keys
*************************************************************************************************************/';
		
		
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubUser.OLD_SessionActivityLog') IS NOT NULL
		DROP TABLE RecHubUser.OLD_SessionActivityLog;

		
END
ELSE
	RAISERROR('PT 142595657 SessionActivityLog has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd