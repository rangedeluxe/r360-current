--WFSScriptProcessorPrint WI 135311
--WFSScriptProcessorPrint Updating RecHubData.factBatchExtracts.BatchID if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factBatchExtracts' AND COLUMN_NAME = 'BatchID' AND DATA_TYPE = 'bigint' )
BEGIN
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factBatchExtracts' AND COLUMN_NAME = 'IsDeleted' )
	BEGIN /* verify that WI 90207 has been applied before continuing. */
		RAISERROR('WI 90207 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Updating RecHubData.factBatchExtracts.BatchID',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factBatchExtracts ALTER COLUMN BatchID BIGINT;

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factBatchExtracts', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'factBatchExtracts';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103915 JMC  Initial Version
*							Added ModificationDate for  purge process
* 04/09/2014 WI 135311 JPB	Changed BatchID from INT to BIGINT.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factBatchExtracts;
	END
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
