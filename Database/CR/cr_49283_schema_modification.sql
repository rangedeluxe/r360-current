--WFSScriptProcessorPrint CR 49283
--WFSScriptProcessorPrint Update SourceProcessingDateKey from factBatchData
--WFSScriptProcessorCRBegin

DECLARE @BatchDataSetupFieldKey INT

SELECT	@BatchDataSetupFieldKey = BatchDataSetupFieldKey 
FROM	OLTA.dimBatchDataSetupFields
WHERE	Keyword = 'Process Date'

RAISERROR('Updating OLTA.factBatchSummary.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factBatchSummary
SET		OLTA.factBatchSummary.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factBatchSummary
		INNER JOIN OLTA.factBatchData ON OLTA.factBatchSummary.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factBatchSummary.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factBatchSummary.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factBatchSummary.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factBatchSummary.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factBatchSummary.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factTransactionSummary.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factTransactionSummary
SET		OLTA.factTransactionSummary.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factTransactionSummary
		INNER JOIN OLTA.factBatchData ON OLTA.factTransactionSummary.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factTransactionSummary.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factTransactionSummary.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factTransactionSummary.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factTransactionSummary.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factTransactionSummary.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factChecks.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factChecks
SET		OLTA.factChecks.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factChecks
		INNER JOIN OLTA.factBatchData ON OLTA.factChecks.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factChecks.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factChecks.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factChecks.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factChecks.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factChecks.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factDocuments.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factDocuments
SET		OLTA.factDocuments.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factDocuments
		INNER JOIN OLTA.factBatchData ON OLTA.factDocuments.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factDocuments.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factDocuments.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factDocuments.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factDocuments.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factDocuments.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factStubs.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factStubs
SET		OLTA.factStubs.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factStubs
		INNER JOIN OLTA.factBatchData ON OLTA.factStubs.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factStubs.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factStubs.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factStubs.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factStubs.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factStubs.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factDataEntrySummary.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factDataEntrySummary
SET		OLTA.factDataEntrySummary.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factDataEntrySummary
		INNER JOIN OLTA.factBatchData ON OLTA.factDataEntrySummary.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factDataEntrySummary.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factDataEntrySummary.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factDataEntrySummary.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factDataEntrySummary.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factDataEntrySummary.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factDataEntryDetails.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factDataEntryDetails
SET		OLTA.factDataEntryDetails.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factDataEntryDetails
		INNER JOIN OLTA.factBatchData ON OLTA.factDataEntryDetails.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factDataEntryDetails.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factDataEntryDetails.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factDataEntryDetails.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factDataEntryDetails.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factDataEntryDetails.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factItemData.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factItemData
SET		OLTA.factItemData.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,OLTA.factBatchData.DataValueDateTime,112) AS INT)
FROM	OLTA.factItemData
		INNER JOIN OLTA.factBatchData ON OLTA.factItemData.DepositDateKey = OLTA.factBatchData.DepositDateKey
			AND OLTA.factItemData.ProcessingDateKey = OLTA.factBatchData.ProcessingDateKey
			AND OLTA.factItemData.BankKey = OLTA.factBatchData.BankKey
			AND OLTA.factItemData.CustomerKey = OLTA.factBatchData.CustomerKey
			AND OLTA.factItemData.LockboxKey = OLTA.factBatchData.LockboxKey
			AND OLTA.factItemData.BatchID = OLTA.factBatchData.BatchID
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Updating OLTA.factBatchData.SourceProcessingDateKey',10,1) WITH NOWAIT
UPDATE	OLTA.factBatchData
SET		OLTA.factBatchData.SourceProcessingDateKey = CAST(CONVERT(VARCHAR,DataValueDateTime,112) AS INT)
FROM	OLTA.factBatchData
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

RAISERROR('Removing Process Date records from OLTA.factBatchData',10,1) WITH NOWAIT
DELETE	
FROM	OLTA.factBatchData
WHERE	OLTA.factBatchData.BatchDataSetupFieldKey = @BatchDataSetupFieldKey

--WFSScriptProcessorCREnd