--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 217876
--WFSScriptProcessorPrint Adding index IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey') 
	BEGIN
		RAISERROR('WI 217876 Already applied. Rebuilding index',10,1) WITH NOWAIT;  -- we will rebuild index
		DROP INDEX IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey ON RecHubData.ClientAccountsDataEntryColumns;

		CREATE NONCLUSTERED INDEX IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey ON RecHubData.ClientAccountsDataEntryColumns 
		(
			DataEntryColumnKey
		) 
		INCLUDE 
		(
			ClientAccountKey,
			HubCreated
		);
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey ON RecHubData.ClientAccountsDataEntryColumns 
		(
			DataEntryColumnKey
		) 
		INCLUDE 
		(
			ClientAccountKey,
			HubCreated
		);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'ClientAccountsDataEntryColumns', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'ClientAccountsDataEntryColumns';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = ClientAccountsDataEntryColumns,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/26/2009
*
* Purpose: Associative table 
*		   
*
* Modification History
* 03/26/2009 CR 25817 JPB	Created
* 02/28/2013 WI 89904 JBS	Update Table to 2.0 release.  Rename Schema Name
*							Rename table from LockboxesDataEntryColumns to 
*							ClientAccountsDataEntryColumns
*							Renamed all Constraints, Renamed Columns:
*							LockboxKey to ClientAccountKey
* 08/22/2014 WI 136947 KLC	Added HubCreated column
* 06/10/2015 WI 217876 JBS	Adding index based on Regression Analysis.
******************************************************************************/';

	END
--WFSScriptProcessorCREnd