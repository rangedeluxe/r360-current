--WFSSCRIPTPROCESSORSCRNAME Updating OLTA.factDataEntryDetails.GlobalBatchID
--WFSSCRIPTPROCESSORSCRBEGIN
IF (SELECT syscolumns.isnullable FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factDataEntryDetails' AND syscolumns.name = 'GlobalBatchID' AND sysobjects.xtype='U') = 0
	ALTER TABLE OLTA.factDataEntryDetails ALTER COLUMN GlobalBatchID INT NULL
--WFSSCRIPTPROCESSORSCRCREND