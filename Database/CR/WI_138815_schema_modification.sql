--WFSScriptProcessorPrint WI 138815
--WFSScriptProcessorPrint Changing CommonExceptionErrors table name if necessary.
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorCRBegin
SET ARITHABORT ON
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'IntegraPAYExceptionErrors')
BEGIN
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'CommonExceptionErrors') 
	BEGIN
		RAISERROR('WI 113309 must be applied before this WI.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Renaming Table RecHubException.CommonExceptionErrors to RecHubException.IntegraPAYExceptionErrors.',10,1) WITH NOWAIT
			
		RAISERROR('Building RecHubException.IntegraPAYExceptionErrors.',10,1) WITH NOWAIT
		EXEC sp_rename 'RecHubException.CommonExceptionErrors', 'OLD_CommonExceptionErrors'

		CREATE TABLE RecHubException.IntegraPAYExceptionErrors 
		(
			IntegraPAYExceptionErrorKey BIGINT IDENTITY(1,1) NOT NULL
				CONSTRAINT PK_IntegraPAYExceptionErrors PRIMARY KEY CLUSTERED,
			IsDeleted			BIT NOT NULL,
			DepositDateKey		INT NULL,
			DecisioningStatus	TINYINT NULL,
			InProcessException	BIT NULL,
			BatchSourceKey		SMALLINT NULL,
			BatchPaymentTypeKey	TINYINT NULL,
			Deadline			DATETIME NULL,
			CheckAmount			MONEY NULL,
			GlobalBatchID		INT NULL,
			SiteBankID			INT NULL,
			SiteClientAccountID	INT NULL,
			ImmutableDateKey	INT NULL,
			BatchID				BIGINT NULL,
			TransactionID		INT NULL,
			CreationDate		DATETIME NULL,
			ModificationDate	DATETIME NULL,
			CombinedError		NVARCHAR(250) NULL
		)
		
		RAISERROR('Updating RecHubException.IntegraPAYExceptionErrors table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubException', 'TABLE', 'IntegraPAYExceptionErrors', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubException',
				@level1type = N'TABLE',
				@level1name = N'IntegraPAYExceptionErrors';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: PS
* Date: 09/05/2013
*
* Purpose:  Used in the CommonException SSIS package
*               
*
* Modification History
* 09/05/2013 WI 113309 PS   Created 
* 05/05/2014 WI 138815 JBS	Change table name to IntegraPAYExceptionErrors and name of Primary Key
* 11/25/2014 WI 138815 JBS	Change BatchSourceKey to SMALLINT and BatchID to BIGINT
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubException,
		@level1type = N'TABLE',@level1name = IntegraPAYExceptionErrors

		RAISERROR('Copying data from old table to New table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT RecHubException.IntegraPAYExceptionErrors ON

		INSERT INTO RecHubException.IntegraPAYExceptionErrors 
		(
			IntegraPAYExceptionErrorKey,
			IsDeleted,
			DepositDateKey,
			DecisioningStatus,
			InProcessException,
			BatchSourceKey,
			BatchPaymentTypeKey,
			Deadline,
			CheckAmount,
			GlobalBatchID,
			SiteBankID,
			SiteClientAccountID,
			ImmutableDateKey,
			BatchID,
			TransactionID,
			CreationDate,
			ModificationDate,
			CombinedError
		)
		SELECT	
			CommonExceptionErrorID,
			IsDeleted,
			DepositDateKey,
			DecisioningStatus,
			InProcessException,
			BatchSourceKey,
			BatchPaymentTypeKey,
			Deadline,
			CheckAmount,
			GlobalBatchID,
			SiteBankID,
			SiteClientAccountID,
			ImmutableDateKey,
			BatchID,
			TransactionID,
			CreationDate,
			ModificationDate,
			CombinedError
		FROM	
			RecHubException.OLD_CommonExceptionErrors;

		SET IDENTITY_INSERT RecHubException.IntegraPAYExceptionErrors OFF

		IF OBJECT_ID('RecHubException.OLD_CommonExceptionErrors') IS NOT NULL
			DROP TABLE RecHubException.OLD_CommonExceptionErrors;
	END
END
ELSE
	RAISERROR('WI 138815 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
