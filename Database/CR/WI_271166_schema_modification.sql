--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271166
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factCheckImages.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factCheckImages')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factCheckImages'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factCheckImages_DepositDatefactCheckImageKey')
			DROP INDEX IDX_factCheckImages_DepositDatefactCheckImageKey ON RecHubData.factCheckImages;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factCheckImages_BankKey')
			DROP INDEX IDX_factCheckImages_BankKey ON RecHubData.factCheckImages;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factCheckImages_OrganizationKey')
			DROP INDEX IDX_factCheckImages_OrganizationKey ON RecHubData.factCheckImages;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factCheckImages_CLientAccountKey')
			DROP INDEX IDX_factCheckImages_CLientAccountKey ON RecHubData.factCheckImages;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factCheckImages_ImmutableDateKey')
			DROP INDEX IDX_factCheckImages_ImmutableDateKey ON RecHubData.factCheckImages;

		/****** Object:  Index PK_factCheckImages    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factCheckImages')
			ALTER TABLE RecHubData.factCheckImages DROP CONSTRAINT PK_factCheckImages;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factCheckImages ADD
			CONSTRAINT PK_factCheckImages PRIMARY KEY NONCLUSTERED (factCheckImageKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factCheckImages_DepositDatefactCheckImageKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factCheckImages_DepositDatefactCheckImageKey ON RecHubData.factCheckImages 
		(	
			DepositDateKey,
			factCheckImageKey
		) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factCheckImages_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factCheckImages_BankKey ON RecHubData.factCheckImages (BankKey) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factCheckImages_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factCheckImages_OrganizationKey ON RecHubData.factCheckImages (OrganizationKey) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factCheckImages_CLientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factCheckImages_CLientAccountKey ON RecHubData.factCheckImages (ClientAccountKey) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factCheckImages_ImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factCheckImages_ImmutableDateKey ON RecHubData.factCheckImages (ImmutableDateKey) $(OnDataPartition);

	END
	ELSE
		RAISERROR('WI 271166 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271166 RecHubData.factCheckImages table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd

