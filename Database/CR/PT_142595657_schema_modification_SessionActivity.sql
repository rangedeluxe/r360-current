--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 04/03/2017
*
* Purpose: Build the partitions for the Sessions tables.
* 
* Prerequisites:	1) PartitionManager and PartitionDisks row must exist for SessionActivity 
*					2) PartitionDisk row(s) must point to an existing folder on disk.
*
* Modification History
* 04/04/2017 WI 142595657	MGE	Created - use this for partitioning the Session Activity table
***********************************************************************************/
DECLARE @SessionActivityRetentionDays INT = 365,		--If the SetupKey for SessionsRetentionDays does not exist, 49 will be used as the default.
		@BeginDate	DATETIME,
		@EndDate	DATETIME,
		@Message	VARCHAR(90),
		@TotalNewCount	INT,
		@TotalOldCount	INT;

IF NOT EXISTS 
	(
	SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'Session'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubUser') = sys.tables.schema_id
	AND sys.partition_schemes.name = 'SessionActivity'
	)
BEGIN
	RAISERROR('Beginning process to create partitions for SessionActivity',10,1) WITH NOWAIT;
	RAISERROR('Building Partitions for SessionActivity',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionActivityRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionActivityLogRetentionDays'
	SELECT @BeginDate = GETDATE() - @SessionActivityRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest SessionActivity Partition to build is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	SELECT @EndDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Last Future Partition to build for SessionActivity is: ' + CONVERT(CHAR(10),@EndDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Build partitions by calling usp_CreatePartitionsForRange >>>
	RAISERROR('Creating Partitions for SessionActivity',10,1) WITH NOWAIT;
	EXEC RecHubSystem.usp_CreatePartitionsForRange 'SessionActivity', @BeginDate, @EndDate

	RAISERROR('SessionActivity partitions have been successfully created.',10,1) WITH NOWAIT;
END
ELSE
	RAISERROR('PT 142595657_SessionActivity has already been applied to the database.',10,1) WITH NOWAIT;

