--WFSScriptProcessorPrint CR 31416
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM SYS.Service_Queues WHERE [name] = 'CDSDocumentTypeReceiverQueue')
	ALTER QUEUE CDSDocumentTypeReceiverQueue WITH ACTIVATION (MAX_QUEUE_READERS = 1)
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM SYS.Service_Queues WHERE [name] = 'CDSSetupReceiverQueue')
	ALTER QUEUE CDSSetupReceiverQueue WITH ACTIVATION (MAX_QUEUE_READERS = 1)
--WFSScriptProcessorCREnd
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM SYS.Service_Queues WHERE [name] = 'CDSSiteCodeReceiverQueue')
	ALTER QUEUE CDSSiteCodeReceiverQueue WITH ACTIVATION (MAX_QUEUE_READERS = 1)
--WFSScriptProcessorCREnd
