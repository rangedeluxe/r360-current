--WFSScriptProcessorPrint Altering OLTA.OLCustomers changing ViewingDays to NULLable -- CR 29277
--WFSScriptPRocessorStaticDataCreateBegin
IF EXISTS(SELECT 1 FROM sys.objects INNER JOIN sys.columns on sys.objects.object_id = sys.columns.object_id INNER JOIN sys.schemas ON sys.objects.schema_id = sys.schemas.schema_id WHERE sys.schemas.name = 'OLTA' AND sys.objects.name = 'OLCustomers' AND sys.columns.name = 'ViewingDays' AND sys.objects.type = 'U')
BEGIN
      ALTER TABLE OLTA.OLCustomers ALTER COLUMN ViewingDays int NULL
END
--WFSScriptPRocessorStaticDataCreateEnd