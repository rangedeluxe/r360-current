--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 161314
--WFSScriptProcessorPrint Adding EventType to RecHubAudit.dimAuditEvents table name if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubAudit' AND TABLE_NAME = 'dimAuditEvents' AND COLUMN_NAME = 'EventType')
BEGIN
	RAISERROR('ReBuilding Table RecHubAudit.dimAuditEvents.',10,1) WITH NOWAIT
	
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factEventAuditMessages' AND CONSTRAINT_NAME='FK_factEventAuditMessages_dimAuditEvents' )
		ALTER TABLE RecHubAudit.factEventAuditMessages DROP CONSTRAINT FK_factEventAuditMessages_dimAuditEvents;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND TABLE_NAME='factEventAuditSummary' AND CONSTRAINT_NAME='FK_factEventAuditSummary_dimAuditEvents' )
		ALTER TABLE RecHubAudit.factEventAuditSummary DROP CONSTRAINT FK_factEventAuditSummary_dimAuditEvents;		
 
 	RAISERROR('Dropping Events contraints.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAudit' AND CONSTRAINT_NAME='PK_dimAuditEvents' )
		ALTER TABLE RecHubAudit.dimAuditEvents DROP CONSTRAINT PK_dimAuditEvents;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE NAME = 'DF_dimAuditEvents_CreationDate')
		ALTER TABLE RecHubAudit.dimAuditEvents DROP CONSTRAINT DF_dimAuditEvents_CreationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE NAME = 'DF_dimAuditEvents_CreatedBy')
		ALTER TABLE RecHubAudit.dimAuditEvents DROP CONSTRAINT DF_dimAuditEvents_CreatedBy;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE NAME = 'DF_dimAuditEvents_ModificationDate')
		ALTER TABLE RecHubAudit.dimAuditEvents DROP CONSTRAINT DF_dimAuditEvents_ModificationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE NAME = 'DF_dimAuditEvents_ModifiedBy')
		ALTER TABLE RecHubAudit.dimAuditEvents DROP CONSTRAINT DF_dimAuditEvents_ModifiedBy;

  	RAISERROR('Dropping Indexes.',10,1) WITH NOWAIT
	IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_dimAuditEvents_EventName')
		DROP INDEX IDX_dimAuditEvents_EventName 
			ON RecHubAudit.dimAuditEvents;
		
	RAISERROR('Building RecHubAudit.dimAuditEvents.',10,1) WITH NOWAIT
	EXEC sp_rename 'RecHubAudit.dimAuditEvents', 'OLD_dimAuditEvents'



	CREATE TABLE RecHubAudit.dimAuditEvents
	(
		AuditEventKey INT IDENTITY NOT NULL 
			CONSTRAINT PK_dimAuditEvents PRIMARY KEY CLUSTERED,
		CreationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimAuditEvents_CreationDate DEFAULT(GETDATE()),
		CreatedBy VARCHAR(128) NOT NULL 
			CONSTRAINT DF_dimAuditEvents_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimAuditEvents_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy VARCHAR(128) NOT NULL 
			CONSTRAINT DF_dimAuditEvents_ModifiedBy DEFAULT(SUSER_SNAME()),
		EventName VARCHAR(64) NOT NULL,
		EventType VARCHAR(64) NOT NULL
	);	

	RAISERROR('Updating RecHubAudit.dimAuditEvents table properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAudit', 'TABLE', 'dimAuditEvents', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubAudit',
			@level1type = N'TABLE',
			@level1name = N'dimAuditEvents';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Events that are being audited. 
*
* Modification History
* 05/30/2013 WI 103696 JPB	Created
* 08/25/2014 WI 161314 JBS	Add Column EventType
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubAudit,
	@level1type = N'TABLE',@level1name = dimAuditEvents;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

	SET IDENTITY_INSERT RecHubAudit.dimAuditEvents ON;

	INSERT INTO RecHubAudit.dimAuditEvents
	(
		AuditEventKey,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		EventName,
		EventType
	)
	SELECT	
		AuditEventKey,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		EventName,
		'Uncategorized'
	FROM	
		RecHubAudit.OLD_dimAuditEvents;

	SET IDENTITY_INSERT RecHubAudit.dimAuditEvents OFF;

	CREATE UNIQUE INDEX IDX_dimAuditEvents_EventName ON RecHubAudit.dimAuditEvents (EventName);
	
	RAISERROR('Applying Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubAudit.factEventAuditMessages ADD 
		CONSTRAINT FK_factEventAuditMessages_dimAuditEvents FOREIGN KEY(AuditEventKey) REFERENCES RecHubAudit.dimAuditEvents(AuditEventKey);

	ALTER TABLE RecHubAudit.factEventAuditSummary ADD 
		CONSTRAINT FK_factEventAuditSummary_dimAuditEvents FOREIGN KEY(AuditEventKey) REFERENCES RecHubAudit.dimAuditEvents(AuditEventKey);

	IF OBJECT_ID('RecHubAudit.OLD_dimAuditEvents') IS NOT NULL
		DROP TABLE RecHubAudit.OLD_dimAuditEvents;
END
ELSE
	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubAudit' AND TABLE_NAME = 'dimAuditEvents' ) 
		BEGIN
			RAISERROR('WI 161314 has already been applied to the database.',10,1) WITH NOWAIT;
		END
	ELSE
		BEGIN
			RAISERROR('WI 103696 must be applied before this WI.',16,1) WITH NOWAIT;
		END
--WFSScriptProcessorCREnd

