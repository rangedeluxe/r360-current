--WFSScriptProcessorPrint CR 32340
--WFSScriptProcessorPrint Adding index OLTA.dimRemitters.IDX_dimRemitters_RemitterName_Account_RoutingNumber if necessary.
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[dimRemitters]') AND name = N'IDX_dimRemitters_RemitterName_Account_RoutingNumber')
	CREATE NONCLUSTERED INDEX [IDX_dimRemitters_RemitterName_Account_RoutingNumber] ON [OLTA].[dimRemitters] 
	(
		[RemitterName] ASC,
		[Account] ASC,
		[RoutingNumber] ASC
	)
--WFSScriptProcessorCREnd