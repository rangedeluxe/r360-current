--WFSScriptProcessorPrint CR 49282
--WFSScriptProcessorPrint Rebuilding OLTA.factItemData if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factItemData' AND COLUMN_NAME = 'SourceProcessingDateKey' )
BEGIN 	

	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_dimBanks_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_dimBanks_factItemData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_dimCustomers_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_dimCustomers_factItemData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_dimLockboxes_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_dimLockboxes_factItemData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_ProcessingDate_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_ProcessingDate_factItemData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_DepositDate_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_DepositDate_factItemData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_dimItemDataSetupFields_factItemData' )
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT FK_dimItemDataSetupFields_factItemData

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factItemData_ModifiedBy')
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT DF_factItemData_ModifiedBy
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factItemData_ModificationDate')
		ALTER TABLE OLTA.factItemData DROP CONSTRAINT DF_factItemData_ModificationDate

	RAISERROR('Rebuilding OLTA.factItemData adding new columns',10,1) WITH NOWAIT 
	EXEC sp_rename 'OLTA.factItemData', 'OLDfactItemData'

	CREATE TABLE OLTA.factItemData
	(
		BankKey INT NOT NULL,
		CustomerKey INT NOT NULL,
		LockboxKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ProcessingDateKey INT NOT NULL,
		SourceProcessingDateKey int NOT NULL, --CR 49282 JPB 01/12/2012
		ItemDataSetupFieldKey INT NOT NULL,
		BatchID int NOT NULL,
		TransactionID INT NOT NULL,
		BatchSequence INT NOT NULL,
		LoadDate datetime NOT NULL,
		ModifiedBy VARCHAR(32) NOT NULL
			CONSTRAINT DF_factItemData_ModifiedBy DEFAULT SUSER_SNAME(),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_factItemData_ModificationDate DEFAULT GETDATE(),
		DataValueDateTime DATETIME NULL,
		DataValueMoney MONEY NULL,
		DataValueInteger INT NULL,
		DataValueBit BIT NULL,
		DataValue VARCHAR(256) NOT NULL
	) $(OnPartition)
		
	RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
	ALTER TABLE OLTA.factItemData ADD 
		CONSTRAINT FK_dimBanks_factItemData FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
		CONSTRAINT FK_dimCustomers_factItemData FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
		CONSTRAINT FK_dimLockboxes_factItemData FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
		CONSTRAINT FK_ProcessingDate_factItemData FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_DepositDate_factItemData FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_SourceProcessingDate_factItemData FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_dimItemDataSetupFields_factItemData FOREIGN KEY(ItemDataSetupFieldKey) REFERENCES OLTA.dimItemDataSetupFields(ItemDataSetupFieldKey)
	RAISERROR('Creating Index OLTA.factItemData.IDX_factItemData_DepositDateKey',10,1) WITH NOWAIT 
	CREATE CLUSTERED INDEX IDX_factItemData_DepositDateKey ON OLTA.factItemData (DepositDateKey)
	
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factItemData', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factItemData';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*		   
*
* Modification History
* 03/07/2011 CR 33210 JPB	Created
* 01/12/2012 CR 49282 JPB	Added SourceProcessingDateKey.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = factItemData
	
	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	
	INSERT INTO OLTA.factItemData
	(
		BankKey,
		CustomerKey,
		LockboxKey,
		DepositDateKey,
		ProcessingDateKey,
		SourceProcessingDateKey, --CR 49282 JPB 01/12/2012
		ItemDataSetupFieldKey,
		BatchID,
		TransactionID,
		BatchSequence,
		LoadDate,
		ModifiedBy,
		ModificationDate,
		DataValueDateTime,
		DataValueMoney,
		DataValueInteger,
		DataValueBit,
		DataValue
	)
	SELECT 	BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			ProcessingDateKey, --rebuild setting the SourceProcessingDateKey date to ProcessingDateKey
			ItemDataSetupFieldKey,
			BatchID,
			TransactionID,
			BatchSequence,
			LoadDate,
			ModifiedBy,
			ModificationDate,
			DataValueDateTime,
			DataValueMoney,
			DataValueInteger,
			DataValueBit,
			DataValue
	FROM 	OLTA.OLDfactItemData

	IF OBJECT_ID('OLTA.OLDfactItemData') IS NOT NULL
	BEGIN
		RAISERROR('Removing old factItemData table.',10,1) WITH NOWAIT 
		DROP TABLE OLTA.OLDfactItemData
	END
END
--WFSScriptProcessorCREnd

