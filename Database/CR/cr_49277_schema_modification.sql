--WFSScriptProcessorPrint CR 49277
--WFSScriptProcessorPrint Adding SourceProcessingDateKey to OLTA.factDocuments if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factDocuments' AND COLUMN_NAME = 'SourceProcessingDateKey' )
BEGIN /* verify that CR 34259 has been applied before continuing. */
	IF NOT EXISTS(	SELECT	1
					FROM	sys.indexes 
							INNER JOIN sys.index_columns ON sys.indexes.object_id = sys.index_columns.object_id AND sys.indexes.index_id = sys.index_columns.index_id
							INNER JOIN sys.columns ON sys.index_columns.object_id = sys.columns.object_id AND sys.index_columns.column_id = sys.columns.column_id 
							INNER JOIN sys.tables ON sys.indexes.object_id = sys.tables.object_id
					WHERE	sys.tables.name = 'factDocuments'
							AND sys.indexes.name = 'IDX_factDocuments_DepositDateKey'
							AND sys.columns.name = 'BatchSequence' 
				)
	BEGIN
		RAISERROR('CR 34259 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimBanks_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimBanks_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimCustomers_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimCustomers_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimLockboxes_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimLockboxes_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_ProcessingDate_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_ProcessingDate_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_DepositDate_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_DepositDate_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimDocumentTypes_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimDocumentTypes_factDocuments
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimBatchSources_factDocuments' )
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimBatchSources_factDocuments

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factDocuments_ModificationDate ')
			ALTER TABLE OLTA.factDocuments DROP CONSTRAINT DF_factDocuments_ModificationDate

		RAISERROR('Rebuilding OLTA.factDocuments adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factDocuments', 'OLDfactDocuments'

		CREATE TABLE OLTA.factDocuments
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL,
			ProcessingDateKey int NOT NULL,
			SourceProcessingDateKey int NOT NULL, --CR 49277 JPB 01/12/2012
			DocumentTypeKey int NOT NULL,
			GlobalBatchID int NULL, --28193 JPB 11/13/2009
			BatchID int NOT NULL,
			DepositStatus int NOT NULL,
			TransactionID int NOT NULL,
			TxnSequence int NOT NULL,
			SequenceWithinTransaction int NOT NULL,
			BatchSequence int NOT NULL,
			DocumentSequence int NOT NULL,
			GlobalDocumentID int NULL,
			ImageInfoXML xml NULL,
			BatchSourceKey tinyint NOT NULL, --CR 32300 JPB 01/13/2011
			LoadDate datetime NOT NULL,
			ModificationDate datetime NOT NULL --CR 32598 JPB 02/07/2011
				CONSTRAINT [DF_factDocuments_ModificationDate] DEFAULT GETDATE()
		)  $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factDocuments ADD
			CONSTRAINT FK_dimBanks_factDocuments FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factDocuments FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factDocuments FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_ProcessingDate_factDocuments FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_DepositDate_factDocuments FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factDocuments FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimDocumentTypes_factDocuments FOREIGN KEY(DocumentTypeKey) REFERENCES OLTA.dimDocumentTypes(DocumentTypeKey),
			CONSTRAINT FK_dimBatchSources_factDocuments FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
			
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factDocuments_DepositDateKey ON OLTA.factDocuments (DepositDateKey,LockboxKey,ProcessingDateKey,BatchID,BatchSequence) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDocuments_BankKey ON OLTA.factDocuments (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDocuments_CustomerKey ON OLTA.factDocuments (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDocuments_LockboxKey ON OLTA.factDocuments (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_ProcessingDateKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDocuments_ProcessingDateKey ON OLTA.factDocuments (ProcessingDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_DocumentTypeKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDocuments_DocumentTypeKey ON OLTA.factDocuments (DocumentTypeKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_BankCustomerLockboxProcessingDateDepositDateKeyBatchID',10,1) WITH NOWAIT 
		CREATE NONCLUSTERED INDEX IDX_factDocuments_BankCustomerLockboxProcessingDateDepositDateKeyBatchID ON OLTA.factDocuments 
		(
			[BankKey] ASC,
			[CustomerKey] ASC,
			[LockboxKey] ASC,
			[ProcessingDateKey] ASC,
			[DepositDateKey] ASC,
			[BatchID] ASC,
			[BatchSequence] ASC
		) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDocuments.IDX_factDocuments_DepositDateProcessingDateBankLockboxKeyBatchID',10,1) WITH NOWAIT 
		CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateProcessingDateBankLockboxKeyBatchID ON OLTA.factDocuments
		(
			[DepositDateKey] ASC,
			[ProcessingDateKey] ASC,
			[BankKey] ASC,
			[LockboxKey] ASC,
			[BatchID] ASC
		) $(OnPartition)
	
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDocuments', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factDocuments';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28193 JPB	GlobalBatchID is now nullable.
* 03/11/2010 CR 29183 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32300 JPB	Added BatchSourceKey.
* 02/07/2011 CR 32598 JPB 	Added ModificationDate.
* 03/15/2011 CR 33351 JPB	Added new index.
* 05/13/2011 CR 34259 JPB	Updated clustered index for performance.
* 01/12/2012 CR 49277 JPB	Added SourceProcessingDateKey.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factDocuments
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factDocuments
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			DocumentTypeKey,
			GlobalBatchID,
			BatchID,
			DepositStatus,
			TransactionID,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			DocumentSequence,
			GlobalDocumentID,
			ImageInfoXML,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey, 
				ProcessingDateKey, --rebuild setting the SourceProcessingDateKey date to ProcessingDateKey
				DocumentTypeKey,
				GlobalBatchID,
				BatchID,
				DepositStatus,
				TransactionID,
				TxnSequence,
				SequenceWithinTransaction,
				BatchSequence,
				DocumentSequence,
				GlobalDocumentID,
				ImageInfoXML,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactDocuments

		IF OBJECT_ID('OLTA.OLDfactDocuments') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factDocuments table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactDocuments
		END
	END
END
--WFSScriptProcessorCREnd

