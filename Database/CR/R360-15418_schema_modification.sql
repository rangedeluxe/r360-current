--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint R360-15418
--WFSScriptProcessorPrint Updating RecHubConfig.ACHAddenda.SegmentDelimiter to VARCHAR(5) if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubConfig' AND TABLE_NAME = 'ACHAddenda' AND COLUMN_NAME = 'SegmentDelimiter' AND DATA_TYPE = 'varchar')
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ACHAddendaSegments' AND CONSTRAINT_NAME='FK_ACHAddendaSegments_ACHAddenda' )
		ALTER TABLE RecHubConfig.ACHAddendaSegments DROP CONSTRAINT FK_ACHAddendaSegments_ACHAddenda;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ACHAddendaWorkgroups' AND CONSTRAINT_NAME='FK_ACHAddendaWorkgroups_ACHAddenda' )
		ALTER TABLE RecHubConfig.ACHAddendaWorkgroups DROP CONSTRAINT FK_ACHAddendaWorkgroups_ACHAddenda;

	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	--PK
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ACHAddenda' AND CONSTRAINT_NAME='PK_ACHAddenda' )
		ALTER TABLE RecHubConfig.ACHAddenda DROP CONSTRAINT PK_ACHAddenda;

	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ACHAddendaSegments_SegmentDelimiter')
		ALTER TABLE RecHubConfig.ACHAddenda DROP CONSTRAINT DF_ACHAddendaSegments_SegmentDelimiter;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ACHAddendaSegments_FieldDelimiter')
		ALTER TABLE RecHubConfig.ACHAddenda DROP CONSTRAINT DF_ACHAddendaSegments_FieldDelimiter;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ACHAddendaSegment_CreationDate')
		ALTER TABLE RecHubConfig.ACHAddenda DROP CONSTRAINT DF_ACHAddendaSegment_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ACHAddendaSegment_ModificationDate')
		ALTER TABLE RecHubConfig.ACHAddenda DROP CONSTRAINT DF_ACHAddendaSegment_ModificationDate;

	RAISERROR('Rebuilding Table RecHubConfig.ACHAddenda.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubConfig.ACHAddenda', 'OLD_ACHAddenda';

	CREATE TABLE RecHubConfig.ACHAddenda
	(
		ACHAddendaKey BIGINT IDENTITY
			CONSTRAINT PK_ACHAddenda PRIMARY KEY CLUSTERED,
		AddendaName   VARCHAR(64) NOT NULL,
		SegmentDelimiter VARCHAR(5) NOT NULL
			CONSTRAINT DF_ACHAddendaSegments_SegmentDelimiter DEFAULT('\'),
		FieldDelimiter CHAR(1) NOT NULL 
			CONSTRAINT DF_ACHAddendaSegments_FieldDelimiter DEFAULT('*'),
		CreationDate DATETIME NOT NULL 
			CONSTRAINT DF_ACHAddendaSegment_CreationDate DEFAULT(GETDATE()),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_ACHAddendaSegment_ModificationDate DEFAULT(GETDATE())
	);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT RecHubConfig.ACHAddenda ON;

	INSERT INTO RecHubConfig.ACHAddenda
	(
		ACHAddendaKey,
		AddendaName,
		SegmentDelimiter,
		FieldDelimiter,
		CreationDate,
		ModificationDate
	)
	SELECT
		ACHAddendaKey,
		AddendaName,
		SegmentDelimiter,
		FieldDelimiter,
		CreationDate,
		ModificationDate
	FROM
		RecHubConfig.OLD_ACHAddenda;

	SET IDENTITY_INSERT RecHubConfig.ACHAddenda OFF;

	RAISERROR('Updating RecHubConfig.ACHAddenda Properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubConfig', 'TABLE', 'ACHAddenda', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubConfig',
			@level1type = N'TABLE',
			@level1name = N'ACHAddenda';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubConfig,
	@level1type = N'TABLE',@level1name = ACHAddenda,
	@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018-2019 Deluxe Corp. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/29/2018
*
* Purpose: Store the Addendas that will be defined in associated tables.
*
*
* Modification History
* 05/29/2018 PT 157790924	JPB/MGE	Created for 2.03
* 02/06/2019 R360-15418		JPB		Changed SegmentDelimiter to VARCHAR(5)
******************************************************************************/';
		
	RAISERROR('Adding Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubConfig.ACHAddendaSegments ADD 
		CONSTRAINT FK_ACHAddendaSegments_ACHAddenda FOREIGN KEY(ACHAddendaKey) REFERENCES RecHubConfig.ACHAddenda(ACHAddendaKey);
	ALTER TABLE RecHubConfig.ACHAddendaWorkgroups ADD 
		CONSTRAINT FK_ACHAddendaWorkgroups_ACHAddenda FOREIGN KEY(ACHAddendaKey) REFERENCES RecHubConfig.ACHAddenda(ACHAddendaKey);
		
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubConfig.OLD_ACHAddenda') IS NOT NULL AND EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='RecHubConfig' AND TABLE_NAME='ACHAddenda')  -- Proceed
		DROP TABLE RecHubConfig.OLD_ACHAddenda;


END
ELSE
	RAISERROR('R360-15418 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
