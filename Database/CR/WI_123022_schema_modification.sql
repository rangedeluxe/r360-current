--WFSScriptProcessorPrint WI 123022
--WFSScriptProcessorPrint Remove RecHubUser.usp_OLClientAccounts_Get_ByUserID_Reporting if necessary
--WFSScriptProcessorCRBegin
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_ByUserID_Reporting') IS NOT NULL
BEGIN
	RAISERROR('Removing stored procedure RecHubUser.usp_OLClientAccounts_Get_ByUserID_Reporting',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_ByUserID_Reporting
END
--WFSScriptProcessorCREnd