--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 217785
--WFSScriptProcessorPrint Adding indexes IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey and IDX_factDataEntryDetails_SourceBatchIDDepositDateKey
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey') 
	BEGIN
		RAISERROR('WI 217785 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey ON RecHubData.factDataEntryDetails 
		(
			ImmutableDateKey, 
			SourceBatchID, 
			TransactionID, 
			BatchSequence,
			DepositDateKey
		) 
		INCLUDE 
		(
			factDataEntryDetailKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			BatchSourceKey, 
			BatchPaymentTypeKey, 
			DepositStatus, 
			DataEntryColumnKey, 
			CreationDate, 
			ModificationDate, 
			DataEntryValueDateTime, 
			DataEntryValueFloat, 
			DataEntryValueMoney, 
			DataEntryValue
		) $(OnDataPartition);

		RAISERROR('Creating index IDX_factDataEntryDetails_SourceBatchIDDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDataEntryDetails_SourceBatchIDDepositDateKey ON RecHubData.factDataEntryDetails 
		(
			SourceBatchID,
			DepositDateKey
		) 
		INCLUDE 
		(
			factDataEntryDetailKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			ImmutableDateKey,
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			BatchSourceKey, 
			BatchPaymentTypeKey, 
			DepositStatus, 
			DataEntryColumnKey,
			TransactionID, 
			BatchSequence, 
			CreationDate, 
			ModificationDate, 
			DataEntryValueDateTime, 
			DataEntryValueFloat, 
			DataEntryValueMoney,
			DataEntryValue
		) $(OnDataPartition);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factDataEntryDetails', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factDataEntryDetails';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factDataEntryDetails,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every data entry field.
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28223 JPB	ProcessingDateKey now NOT NULL.
* 12/23/2009 CR 28238  CS	Add index on ProcessingDateKey
* 02/10/2010 CR 28977 JPB	Added ModificationDate.
* 02/12/2010 CR 29012 JPB	Added missing foreign key on processing date.
* 01/06/2011 CR 32233 JPB	Added native data types.
* 01/11/2011 CR 32302 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49280 JPB	Added SourceProcessingDateKey.
* 03/20/2012 CR 51368 JPB	Created new index.
* 03/26/2012 CR 51542 JPB	Added BatchPaymentTypeKey
* 07/19/2012 CR 54125 JPB	Added BatchNumber.
* 07/19/2012 CR 54134 JPB	Renamed and updated index with BatchNumber.
* 03/05/2013 WI 90483 JBS	Update table to 2.0 release.  Change Schema Name.
*							Added factDataEntryDetailKey to Clustered Index.
*							Changed Indexes to match schema and column renaming.
*							Added Columns: factDataEntryDetailKey, IsDeleted
*							Rename Columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey
*							LoadDate to CreationDate.
*							Remove: constraint on ModificationDate, column GlobalBatchID
*							Forward patch:  WI 83270, WI 87217
* 05/30/2014 WI 144025 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 144898 JPB	Added SourceBatchID.
* 05/30/2014 WI 144899 JPB	Changed SourceBatchKey to SMALLINT.
* 06/10/2015 WI 217785 JBS  Adding indexes from Regression Analysis.
******************************************************************************/';
	END
--WFSScriptProcessorCREnd