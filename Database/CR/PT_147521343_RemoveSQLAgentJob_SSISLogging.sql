--WFSScriptProcessorPrint PT147521343 
--WFSScriptProcessorSQLAgentJobName SSISLoggingDatabaseMaintenance
--WFSScriptProcessorSQLAgentJobCreate
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT,
		@StartDate INT;

SELECT @ReturnCode = 0, @JobID = NULL;

SELECT @StartDate = CAST(CONVERT(varchar, GetDate(), 112) AS INT);


IF EXISTS(SELECT @JobID FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Deleting R360 SSIS Logging Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode =  msdb.dbo.sp_delete_job @job_name=N'$(SSISJobName)', 
		@delete_unused_schedule=1;
END	

