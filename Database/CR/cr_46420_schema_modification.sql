--WFSScriptProcessorPrint CR 46420
--WFSScriptProcessorPrint Removing stored procedure usp_Research_GetImageDisplayModeForBatch if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS	(
				SELECT	* 
				FROM	INFORMATION_SCHEMA.ROUTINES 
				WHERE	SPECIFIC_SCHEMA = N'OLTA' 
						AND SPECIFIC_NAME = N'usp_Research_GetImageDisplayModeForBatch' 
			)
   DROP PROCEDURE OLTA.usp_Research_GetImageDisplayModeForBatch
--WFSScriptProcessorCREnd
