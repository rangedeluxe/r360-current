--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 189641
--WFSScriptProcessorPrint Delete RecHubUser.OLUserPreferences rows where PreferenceName=ReceivablesSummaryRecordsPerPage.
--WFSScriptProcessorCRBegin
IF EXISTS 
        (SELECT TOP (1) UserID FROM RecHubUser.OLUserPreferences INNER JOIN RecHubUser.OLPreferences 
                       ON RecHubUser.OLUserPreferences.OLPreferenceId = RecHubUser.OLPreferences.OLPreferenceId
                       WHERE 
                              PreferenceName='ReceivablesSummaryRecordsPerPage')
	BEGIN
		RAISERROR('WI 189641 Removing OLUserPreferences related to PreferenceName=ReceivablesSummaryRecordsPerPage.',10,1) WITH NOWAIT;
        DELETE 
			RecHubUser.OLUserPreferences 
		FROM 
            RecHubUser.OLUserPreferences INNER JOIN RecHubUser.OLPreferences 
              ON RecHubUser.OLUserPreferences.OLPreferenceId = RecHubUser.OLPreferences.OLPreferenceId
        WHERE 
            PreferenceName='ReceivablesSummaryRecordsPerPage'
		END
ELSE
    RAISERROR('WI 189641 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
