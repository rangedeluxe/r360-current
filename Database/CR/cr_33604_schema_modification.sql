--WFSScriptProcessorPrint CR 33604
--WFSScriptProcessorPrint Removing usp_ICONClientNotifyOLTAServiceBroker if necessary
--WFSScriptProcessorCRBegin
IF OBJECT_ID('dbo.usp_ICONClientNotifyOLTAServiceBroker') IS NOT NULL
       DROP PROCEDURE dbo.usp_ICONClientNotifyOLTAServiceBroker
--WFSScriptProcessorCREnd