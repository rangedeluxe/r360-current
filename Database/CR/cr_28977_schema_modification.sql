--WFSScriptProcessorPrint CR 28977 
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.factDataEntryDetails
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factDataEntryDetails ADD ModificationDate DATETIME NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Updating existing OLTA.factDataEntryDetails.ModificationDate with LoadDate
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factDataEntryDetails 
SET		ModificationDate = LoadDate
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factDataEntryDetails.ModificationDate to NOT NULL
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factDataEntryDetails ALTER COLUMN ModificationDate DATETIME NOT NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factDataEntryDetails.ModificationDate adding default constraint
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM	sysobjects WHERE name = 'DF_factDataEntryDetails_ModificationDate')
	AND EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factDataEntryDetails ADD
		CONSTRAINT DF_factDataEntryDetails_ModificationDate DEFAULT GETDATE() FOR ModificationDate
--WFSScriptProcessorCREnd