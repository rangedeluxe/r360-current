RAISERROR('CR 33996',10,1) WITH NOWAIT
RAISERROR('Adding Partitions',10,1) WITH NOWAIT
SET NOCOUNT ON

DECLARE @SQLCommand VARCHAR(MAX),
		@Loop  INT,
		@TableName VARCHAR(36),
		@PartitionIdentifier varchar(20),
		@BeginDate datetime,
		@EndDate datetime,
		@value int,
		@YearsAdded INT,
		@PartitionMethod TINYINT,
		@Msg VARCHAR(MAX)

/* by default, add 6 years. Change only if needed. */
SET @YearsAdded = 6
/* find the max partition number */
SELECT @value = CONVERT(int,MAX(value)) FROM sys.partition_range_values
/* set the partition identifier and lookup the partition method */
SELECT	@PartitionIdentifier='OLTAFacts'
SELECT @PartitionMethod = RangeSetting --0:none/1:weekly/2:monthly
FROM OLTA.PartitionManager
WHERE PartitionIdentifier=@PartitionIdentifier

IF @PartitionMethod <> 0 
BEGIN /* only attempt if the database is partitioned */
	/* Convert the max partition number to a date/time */
	SELECT @BeginDate=CONVERT(datetime, CONVERT(varchar, @value), 112)
	/* add the correct value (based on the partition method) to the current max partition number to get the new beginning partition date */
	SELECT @BeginDate = 
			CASE
				WHEN @PartitionMethod = 1 THEN DATEADD(DAY,7,@BeginDate)
				WHEN @PartitionMethod = 2 THEN DATEADD(MONTH,1,@BeginDate)
			END
		
	/* Build a list of the fact tables we need to deal with */
	DECLARE @Tables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName VARCHAR(36)
	)

	INSERT INTO @Tables(TableName)
	SELECT 'factBatchSummary'
	UNION ALL SELECT 'factChecks'
	UNION ALL SELECT 'factDataEntryDetails'
	UNION ALL SELECT 'factDataEntrySummary'
	UNION ALL SELECT 'factDocuments'
	UNION ALL SELECT 'factStubs'
	UNION ALL SEleCT 'factTransactionSummary'


	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factTransactionDetails')
		INSERT INTO @Tables(TableName) VALUES('factTransactionDetails')

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factBatchData')
		INSERT INTO @Tables(TableName) VALUES('factBatchData')

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factItemData')
		INSERT INTO @Tables(TableName) VALUES('factItemData')

	SET @Loop = 1
	/* first create the temp tables so we can store the existing 'out of partition' data */
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Tables) )
	BEGIN

		SELECT @TableName = TableName
		FROM @Tables
		WHERE RowID = @Loop
		
		SELECT @SQLCommand = 'CREATE TABLE OLTA.' + @TableName + '_Temp('

		SELECT @SQLCommand = @SQLCommand + COLUMN_NAME + ' ' + CAST(DATA_TYPE AS VARCHAR) + CASE WHEN DATA_TYPE = 'VARCHAR' OR DATA_TYPE = 'CHAR' THEN '(' + CAST(CHARACTER_MAXIMUM_LENGTH AS VARCHAR) + ') ' ELSE ' ' END + CASE WHEN IS_NULLABLE = 'NO' THEN 'NOT NULL,' ELSE ',' END
		FROM 	INFORMATION_SCHEMA.Columns
		WHERE 	TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = @TableName
		ORDER BY ORDINAL_POSITION
		IF @@ROWCOUNT > 0 
		BEGIN
			SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)
			SET @SQLCommand = @SQLCommand + ')'
			EXEC(@SQLCommand)
		END
		SET @Loop = @Loop + 1
	END

	/* Calculate the last date that is in the parititions */
	SELECT @value = CONVERT(int,MAX(value)) FROM sys.partition_range_values
	PRINT @value
	/* Now copy the 'out of partition' data to the temp tables */
	SET @Loop = 1
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Tables) )
	BEGIN

		SELECT @TableName = TableName
		FROM @Tables
		WHERE RowID = @Loop
		
		SELECT @SQLCommand = 'INSERT INTO OLTA.' + @TableName + '_Temp'
		SELECT @SQLCommand = @SQLCommand + ' SELECT * FROM OLTA.' + @TableName + ' WHERE DepositDateKey > ' + CAST(@value AS VARCHAR)
		RAISERROR(@SQLCommand,10,1) WITH NOWAIT
		EXEC(@SQLCommand)
		SELECT @SQLCommand = 'DELETE FROM OLTA.' + @TableName + ' WHERE DepositDateKey > ' + CAST(@value AS VARCHAR)
		RAISERROR(@SQLCommand,10,1) WITH NOWAIT
		EXEC(@SQLCommand)
		SET @Loop = @Loop + 1
	END	
	/* now set the new end date to the new begin date plus @YearsAdded years */
	SET @EndDate = DATEADD(YEAR,@YearsAdded,@BeginDate)
	SELECT @Msg = 'Creating Partitions for date range: ' + CONVERT(varchar, @BeginDate, 101) + ' to ' + CONVERT(varchar, @EndDate, 101)
	RAISERROR(@Msg,10,1) WITH NOWAIT
	/* call the SP to add the partitions */
	EXEC OLTA.usp_CreatePartitionsForRange @PartitionIdentifier, @BeginDate, @EndDate
	
	/* now copy the data back */
	SET @Loop = 1
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Tables) )
	BEGIN

		SELECT @TableName = TableName
		FROM @Tables
		WHERE RowID = @Loop
		
		SELECT @SQLCommand = 'INSERT INTO OLTA.' + @TableName + ''
		SELECT @SQLCommand = @SQLCommand + ' SELECT * FROM OLTA.' + @TableName + '_Temp'
		RAISERROR(@SQLCommand,10,1) WITH NOWAIT
		EXEC(@SQLCommand)
		SET @Loop = @Loop + 1
	END	
	/* Everything has been copied, delete the temp tables */
	SET @Loop = 1
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Tables) )
	BEGIN

		SELECT @TableName = TableName
		FROM @Tables
		WHERE RowID = @Loop
		
		SELECT @SQLCommand = 'DROP TABLE OLTA.' + @TableName + '_Temp'
		RAISERROR(@SQLCommand,10,1) WITH NOWAIT
		EXEC(@SQLCommand)
		SET @Loop = @Loop + 1
	END	
END
