--WFSScriptProcessorPrint CR 46869
--WFSScriptProcessorPrint Adding preference columns to OLTA.OLPreferences if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='AppType')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='PreferenceGroup')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldDataTypeEnum')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldSize')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldMaxLength')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldExp')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='EncodeOnSerialization')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldIntMinValue')
	AND NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND COLUMN_NAME='fldIntMaxValue')
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLUserPreferences' AND CONSTRAINT_NAME='FK_OLPreferences_OLUserPreferences' )
		ALTER TABLE OLTA.OLUserPreferences DROP CONSTRAINT FK_OLPreferences_OLUserPreferences

	RAISERROR('Dropping OLPreferences contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLPreferences' AND CONSTRAINT_NAME='PK_OLPreferences' )
		ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT PK_OLPreferences

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_OLPreferenceID' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_OLPreferenceID

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_IsOnline' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_IsOnline

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_IsSystem' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_IsSystem

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_CreationDate' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_CreationDate

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_CreatedBy' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_CreatedBy

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_ModificationDate' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_ModificationDate

	IF EXISTS(	SELECT	1 
				FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
				WHERE sys.columns.object_id = object_id(N'OLTA.OLPreferences') AND sys.objects.name = 'DF_OLPreferences_ModifiedBy' )
	ALTER TABLE OLTA.OLPreferences DROP CONSTRAINT DF_OLPreferences_ModifiedBy

	RAISERROR('Rebuilding OLTA.OLPreferences',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.OLPreferences', 'OLD_OLPreferences'

	/* NOTE: The two new columns are added as NULL in the create table. Later the in script,
		the columns are updated with the correct values and then the columns are altered to 
		NOT NULL.
	*/

	CREATE TABLE OLTA.OLPreferences 
	(
		OLPreferenceID uniqueidentifier NOT NULL 
			CONSTRAINT PK_OLPreferences PRIMARY KEY CLUSTERED
			CONSTRAINT DF_OLPreferences_OLPreferenceID DEFAULT(NEWID()),
		PreferenceName varchar(64) NOT NULL,
		[Description] varchar(255) NULL,
		IsOnline bit NOT NULL CONSTRAINT DF_OLPreferences_IsOnline DEFAULT(0),
		IsSystem bit NOT NULL CONSTRAINT DF_OLPreferences_IsSystem DEFAULT(0),
		DefaultSetting varchar(255) NULL,
		AppType tinyint NOT NULL,
			CONSTRAINT CK_OLPreferences_AppType CHECK(AppType IN (0,1,2,99)),
		PreferenceGroup varchar(64) NOT NULL,
		fldDataTypeEnum int NOT NULL
			CONSTRAINT CK_OLPreferences_fldDataTypeEnum CHECK(fldDataTypeEnum IN (1,4,5,6,101,102,103)),
		fldSize int NOT NULL,
		fldMaxLength int NOT NULL,
		fldExp varchar(64) NULL,
		EncodeOnSerialization bit NOT NULL,
		fldIntMinValue int NULL,
		fldIntMaxValue int NULL,
		CreationDate datetime NOT NULL 
			CONSTRAINT DF_OLPreferences_CreationDate DEFAULT(GETDATE()),
		CreatedBy varchar(32) NOT NULL 
			CONSTRAINT DF_OLPreferences_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_OLPreferences_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy varchar(32) NOT NULL 
			CONSTRAINT DF_OLPreferences_ModifiedBy DEFAULT(SUSER_SNAME())
	)
	
	RAISERROR('Creating index OLTA.OLPreferences.IDX_OLPreferences_PreferenceName',10,1) WITH NOWAIT
	CREATE UNIQUE INDEX IDX_OLPreferences_PreferenceName ON OLTA.OLPreferences(OLPreferenceID)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'OLPreferences', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'OLPreferences';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Customers dimension is a SCD type 2 holding Customer info.  
*	Most recent flag of 1 indicates the current Customer row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 09/29/2011 CR 46869 JPB	Added Preference columns
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = OLPreferences

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
	INSERT INTO OLTA.OLPreferences 
	(
		OLPreferenceID,
		PreferenceName,
		[Description],
		IsOnline,
		IsSystem,
		DefaultSetting,
		AppType, 
		PreferenceGroup, 
		fldDataTypeEnum,   
		fldSize,     
		fldMaxLength,
		fldExp,  
		EncodeOnSerialization,   
		fldIntMinValue,  
		fldIntMaxValue,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy
	) 
	SELECT	OLPreferenceID,
			PreferenceName,
			[Description],
			IsOnline,
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 0
				WHEN 'CheckImageDisplayMode' THEN 1
				WHEN 'DisplayRemitterNameInPDF' THEN 0
				WHEN 'DocumentImageDisplayMode' THEN 1
				WHEN 'LockoutInactiveUserAccounts' THEN 1
				WHEN 'MaxLogonAttempts' THEN 1
				WHEN 'MinPasswordLength' THEN 1
				WHEN 'PasswordExpiration' THEN 1
				WHEN 'PasswordHistoryRetention' THEN 1
				WHEN 'PasswordRequiredFormat' THEN 1
				WHEN 'SessionExpiration' THEN 1
				WHEN 'DisplayScannedCheckOnline' THEN 1
				WHEN 'UseCutoff' THEN 1
				WHEN 'LockboxSummaryRecordsPerPage' THEN 0
				WHEN 'ChangePasswordMinutesExpired' THEN 1
				WHEN 'ChangePasswordNotificationSubject' THEN 1
				WHEN 'ForgotPasswordNotificationSubject' THEN 1
				WHEN 'PasswordNotificationFromAddress' THEN 1
				WHEN 'MaximumQueriesSaved' THEN 1
				WHEN 'MaxSecurityQuestionAttempts' THEN 1
				WHEN 'DisplayScannedCheckResearch' THEN 1
				WHEN 'UseCutoffResearch' THEN 1
				WHEN 'ViewProcessAheadResearch' THEN 1
				WHEN 'AdminRecordsPerPage' THEN 1
				WHEN 'ViewingDays' THEN 1
				ELSE NULL
			END AS IsSystem, 
			DefaultSetting,
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 99
				WHEN 'CheckImageDisplayMode' THEN 99
				WHEN 'DisplayRemitterNameInPDF' THEN 99
				WHEN 'DocumentImageDisplayMode' THEN 99
				WHEN 'LockoutInactiveUserAccounts' THEN 99
				WHEN 'MaxLogonAttempts' THEN 99
				WHEN 'MinPasswordLength' THEN 99
				WHEN 'PasswordExpiration' THEN 99
				WHEN 'PasswordHistoryRetention' THEN 99
				WHEN 'PasswordRequiredFormat' THEN 99
				WHEN 'SessionExpiration' THEN 99
				WHEN 'DisplayScannedCheckOnline' THEN 0
				WHEN 'UseCutoff' THEN 0
				WHEN 'LockboxSummaryRecordsPerPage' THEN 0
				WHEN 'ChangePasswordMinutesExpired' THEN 0
				WHEN 'ChangePasswordNotificationSubject' THEN 0
				WHEN 'ForgotPasswordNotificationSubject' THEN 0
				WHEN 'PasswordNotificationFromAddress' THEN 0
				WHEN 'MaximumQueriesSaved' THEN 0
				WHEN 'MaxSecurityQuestionAttempts' THEN 0
				WHEN 'DisplayScannedCheckResearch' THEN 1
				WHEN 'UseCutoffResearch' THEN 1
				WHEN 'ViewProcessAheadResearch' THEN 1
				WHEN 'AdminRecordsPerPage' THEN 2
				WHEN 'ViewingDays' THEN 2
				ELSE NULL
			END AS AppType, 
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 'Data Retrieval Options'
				WHEN 'CheckImageDisplayMode' THEN 'Image Retrieval Options'
				WHEN 'DisplayRemitterNameInPDF' THEN 'Image Retrieval Options'
				WHEN 'DocumentImageDisplayMode' THEN 'Image Retrieval Options'
				WHEN 'LockoutInactiveUserAccounts' THEN 'Security Options'
				WHEN 'MaxLogonAttempts' THEN 'Security Options'
				WHEN 'MinPasswordLength' THEN 'Security Options'
				WHEN 'PasswordExpiration' THEN 'Security Options'
				WHEN 'PasswordHistoryRetention' THEN 'Security Options'
				WHEN 'PasswordRequiredFormat' THEN 'Security Options'
				WHEN 'SessionExpiration' THEN 'Session Options'
				WHEN 'DisplayScannedCheckOnline' THEN 'Data Availability Options'
				WHEN 'UseCutoff' THEN 'Data Availability Options'
				WHEN 'LockboxSummaryRecordsPerPage' THEN 'Data Retrieval Options'
				WHEN 'ChangePasswordMinutesExpired' THEN 'Password Notification Options'
				WHEN 'ChangePasswordNotificationSubject' THEN 'Password Notification Options'
				WHEN 'ForgotPasswordNotificationSubject' THEN 'Password Notification Options'
				WHEN 'PasswordNotificationFromAddress' THEN 'Password Notification Options'
				WHEN 'MaximumQueriesSaved' THEN 'Security Options'
				WHEN 'MaxSecurityQuestionAttempts' THEN 'Security Options'
				WHEN 'DisplayScannedCheckResearch' THEN 'Data Availability Options'
				WHEN 'UseCutoffResearch' THEN 'Data Availability Options'
				WHEN 'ViewProcessAheadResearch' THEN 'Data Availability Options'
				WHEN 'AdminRecordsPerPage' THEN 'Data Retrieval Options'
				WHEN 'ViewingDays' THEN 'Online Setup Options'
				ELSE NULL
			END AS PreferenceGroup, 
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 6
				WHEN 'CheckImageDisplayMode' THEN 101
				WHEN 'DisplayRemitterNameInPDF' THEN 4
				WHEN 'DocumentImageDisplayMode' THEN 101
				WHEN 'LockoutInactiveUserAccounts' THEN 6
				WHEN 'MaxLogonAttempts' THEN 6
				WHEN 'MinPasswordLength' THEN 6
				WHEN 'PasswordExpiration' THEN 6
				WHEN 'PasswordHistoryRetention' THEN 5
				WHEN 'PasswordRequiredFormat' THEN 103
				WHEN 'SessionExpiration' THEN 6
				WHEN 'DisplayScannedCheckOnline' THEN 4
				WHEN 'UseCutoff' THEN 4
				WHEN 'LockboxSummaryRecordsPerPage' THEN 6
				WHEN 'ChangePasswordMinutesExpired' THEN 6
				WHEN 'ChangePasswordNotificationSubject' THEN 1
				WHEN 'ForgotPasswordNotificationSubject' THEN 1
				WHEN 'PasswordNotificationFromAddress' THEN 102
				WHEN 'MaximumQueriesSaved' THEN 6
				WHEN 'MaxSecurityQuestionAttempts' THEN 6
				WHEN 'DisplayScannedCheckResearch' THEN 4
				WHEN 'UseCutoffResearch' THEN 4
				WHEN 'ViewProcessAheadResearch' THEN 4
				WHEN 'AdminRecordsPerPage' THEN 6
				WHEN 'ViewingDays' THEN 6
				ELSE NULL
			END AS fldDataTypeEnum,   
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 5
				WHEN 'CheckImageDisplayMode' THEN 0
				WHEN 'DisplayRemitterNameInPDF' THEN 0
				WHEN 'DocumentImageDisplayMode' THEN 0
				WHEN 'LockoutInactiveUserAccounts' THEN 5
				WHEN 'MaxLogonAttempts' THEN 5
				WHEN 'MinPasswordLength' THEN 6
				WHEN 'PasswordExpiration' THEN 5
				WHEN 'PasswordHistoryRetention' THEN 6
				WHEN 'PasswordRequiredFormat' THEN 30
				WHEN 'SessionExpiration' THEN 5
				WHEN 'DisplayScannedCheckOnline' THEN 0
				WHEN 'UseCutoff' THEN 0
				WHEN 'LockboxSummaryRecordsPerPage' THEN 5
				WHEN 'ChangePasswordMinutesExpired' THEN 5
				WHEN 'ChangePasswordNotificationSubject' THEN 30
				WHEN 'ForgotPasswordNotificationSubject' THEN 30
				WHEN 'PasswordNotificationFromAddress' THEN 30
				WHEN 'MaximumQueriesSaved' THEN 5
				WHEN 'MaxSecurityQuestionAttempts' THEN 5
				WHEN 'DisplayScannedCheckResearch' THEN 0
				WHEN 'UseCutoffResearch' THEN 0
				WHEN 'ViewProcessAheadResearch' THEN 0
				WHEN 'AdminRecordsPerPage' THEN 5
				WHEN 'ViewingDays' THEN 5
				ELSE NULL
			END AS fldSize,     
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 10
				WHEN 'CheckImageDisplayMode' THEN 0
				WHEN 'DisplayRemitterNameInPDF' THEN 0
				WHEN 'DocumentImageDisplayMode' THEN 0
				WHEN 'LockoutInactiveUserAccounts' THEN 5
				WHEN 'MaxLogonAttempts' THEN 10
				WHEN 'MinPasswordLength' THEN 10
				WHEN 'PasswordExpiration' THEN 10
				WHEN 'PasswordHistoryRetention' THEN 10
				WHEN 'PasswordRequiredFormat' THEN 1000
				WHEN 'SessionExpiration' THEN 10
				WHEN 'DisplayScannedCheckOnline' THEN 0
				WHEN 'UseCutoff' THEN 0
				WHEN 'LockboxSummaryRecordsPerPage' THEN 10
				WHEN 'ChangePasswordMinutesExpired' THEN 10
				WHEN 'ChangePasswordNotificationSubject' THEN 60
				WHEN 'ForgotPasswordNotificationSubject' THEN 60
				WHEN 'PasswordNotificationFromAddress' THEN 60
				WHEN 'MaximumQueriesSaved' THEN 10
				WHEN 'MaxSecurityQuestionAttempts' THEN 10
				WHEN 'DisplayScannedCheckResearch' THEN 0
				WHEN 'UseCutoffResearch' THEN 0
				WHEN 'ViewProcessAheadResearch' THEN 0
				WHEN 'AdminRecordsPerPage' THEN 10
				WHEN 'ViewingDays' THEN 10
				ELSE NULL
			END AS fldMaxLength,
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN NULL
				WHEN 'CheckImageDisplayMode' THEN NULL
				WHEN 'DisplayRemitterNameInPDF' THEN NULL
				WHEN 'DocumentImageDisplayMode' THEN NULL
				WHEN 'LockoutInactiveUserAccounts' THEN NULL
				WHEN 'MaxLogonAttempts' THEN NULL
				WHEN 'MinPasswordLength' THEN NULL
				WHEN 'PasswordExpiration' THEN NULL
				WHEN 'PasswordHistoryRetention' THEN NULL
				WHEN 'PasswordRequiredFormat' THEN NULL
				WHEN 'SessionExpiration' THEN NULL
				WHEN 'DisplayScannedCheckOnline' THEN NULL
				WHEN 'UseCutoff' THEN NULL
				WHEN 'LockboxSummaryRecordsPerPage' THEN NULL
				WHEN 'ChangePasswordMinutesExpired' THEN NULL
				WHEN 'ChangePasswordNotificationSubject' THEN NULL
				WHEN 'ForgotPasswordNotificationSubject' THEN NULL
				WHEN 'PasswordNotificationFromAddress' THEN '[^-0-9A-Za-z @._/\''/]+'
				WHEN 'MaximumQueriesSaved' THEN NULL
				WHEN 'MaxSecurityQuestionAttempts' THEN NULL
				WHEN 'DisplayScannedCheckResearch' THEN NULL
				WHEN 'UseCutoffResearch' THEN NULL
				WHEN 'ViewProcessAheadResearch' THEN NULL
				WHEN 'AdminRecordsPerPage' THEN NULL
				WHEN 'ViewingDays' THEN NULL
				ELSE NULL
			END AS fldExp,  
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 0
				WHEN 'CheckImageDisplayMode' THEN 0
				WHEN 'DisplayRemitterNameInPDF' THEN 0
				WHEN 'DocumentImageDisplayMode' THEN 0
				WHEN 'LockoutInactiveUserAccounts' THEN 0
				WHEN 'MaxLogonAttempts' THEN 0
				WHEN 'MinPasswordLength' THEN 0
				WHEN 'PasswordExpiration' THEN 0
				WHEN 'PasswordHistoryRetention' THEN 0
				WHEN 'PasswordRequiredFormat' THEN 1
				WHEN 'SessionExpiration' THEN 0
				WHEN 'DisplayScannedCheckOnline' THEN 0
				WHEN 'UseCutoff' THEN 0
				WHEN 'LockboxSummaryRecordsPerPage' THEN 0
				WHEN 'ChangePasswordMinutesExpired' THEN 0
				WHEN 'ChangePasswordNotificationSubject' THEN 0
				WHEN 'ForgotPasswordNotificationSubject' THEN 0
				WHEN 'PasswordNotificationFromAddress' THEN 0
				WHEN 'MaximumQueriesSaved' THEN 0
				WHEN 'MaxSecurityQuestionAttempts' THEN 0
				WHEN 'DisplayScannedCheckResearch' THEN 0
				WHEN 'UseCutoffResearch' THEN 0
				WHEN 'ViewProcessAheadResearch' THEN 0
				WHEN 'AdminRecordsPerPage' THEN 0
				WHEN 'ViewingDays' THEN 0
				ELSE NULL
			END AS EncodeOnSerialization,   
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 5
				WHEN 'CheckImageDisplayMode' THEN NULL
				WHEN 'DisplayRemitterNameInPDF' THEN NULL
				WHEN 'DocumentImageDisplayMode' THEN NULL
				WHEN 'LockoutInactiveUserAccounts' THEN NULL
				WHEN 'MaxLogonAttempts' THEN 1
				WHEN 'MinPasswordLength' THEN 1
				WHEN 'PasswordExpiration' THEN 3
				WHEN 'PasswordHistoryRetention' THEN NULL
				WHEN 'PasswordRequiredFormat' THEN NULL
				WHEN 'SessionExpiration' THEN 5
				WHEN 'DisplayScannedCheckOnline' THEN NULL
				WHEN 'UseCutoff' THEN NULL
				WHEN 'LockboxSummaryRecordsPerPage' THEN 5
				WHEN 'ChangePasswordMinutesExpired' THEN 1
				WHEN 'ChangePasswordNotificationSubject' THEN NULL
				WHEN 'ForgotPasswordNotificationSubject' THEN NULL
				WHEN 'PasswordNotificationFromAddress' THEN NULL
				WHEN 'MaximumQueriesSaved' THEN 0
				WHEN 'MaxSecurityQuestionAttempts' THEN 1
				WHEN 'DisplayScannedCheckResearch' THEN NULL
				WHEN 'UseCutoffResearch' THEN NULL
				WHEN 'ViewProcessAheadResearch' THEN NULL
				WHEN 'AdminRecordsPerPage' THEN 5
				WHEN 'ViewingDays' THEN 1
				ELSE NULL
			END AS fldIntMinValue,  
			CASE PreferenceName
				WHEN 'RecordsPerPage' THEN 255
				WHEN 'CheckImageDisplayMode' THEN NULL
				WHEN 'DisplayRemitterNameInPDF' THEN NULL
				WHEN 'DocumentImageDisplayMode' THEN NULL
				WHEN 'LockoutInactiveUserAccounts' THEN NULL
				WHEN 'MaxLogonAttempts' THEN 255
				WHEN 'MinPasswordLength' THEN 20
				WHEN 'PasswordExpiration' THEN 9999
				WHEN 'PasswordHistoryRetention' THEN NULL
				WHEN 'PasswordRequiredFormat' THEN NULL
				WHEN 'SessionExpiration' THEN 9999
				WHEN 'DisplayScannedCheckOnline' THEN NULL
				WHEN 'UseCutoff' THEN NULL
				WHEN 'LockboxSummaryRecordsPerPage' THEN 255
				WHEN 'ChangePasswordMinutesExpired' THEN 9999
				WHEN 'ChangePasswordNotificationSubject' THEN NULL
				WHEN 'ForgotPasswordNotificationSubject' THEN NULL
				WHEN 'PasswordNotificationFromAddress' THEN NULL
				WHEN 'MaximumQueriesSaved' THEN 1000
				WHEN 'MaxSecurityQuestionAttempts' THEN 255
				WHEN 'DisplayScannedCheckResearch' THEN NULL
				WHEN 'UseCutoffResearch' THEN NULL
				WHEN 'ViewProcessAheadResearch' THEN NULL
				WHEN 'AdminRecordsPerPage' THEN 255
				WHEN 'ViewingDays' THEN 9999
				ELSE NULL
			END AS fldIntMaxValue,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
	FROM 	OLTA.OLD_OLPreferences

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.OLUserPreferences ADD 
		CONSTRAINT FK_OLPreferences_OLUserPreferences FOREIGN KEY (OLPreferenceID) REFERENCES OLTA.OLPreferences(OLPreferenceID)

	RAISERROR('Removing work table',10,1) WITH NOWAIT
	IF OBJECT_ID('OLTA.OLD_OLPreferences') IS NOT NULL
		DROP TABLE OLTA.OLD_OLPreferences
END