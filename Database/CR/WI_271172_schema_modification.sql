--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271172
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factExtractTraces.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factExtractTraces')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factExtractTraces'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factExtractTraces_DepositDatefactExtractKey')
			DROP INDEX IDX_factExtractTraces_DepositDatefactExtractKey ON RecHubData.factExtractTraces;

		/****** Object:  Index PK_factDocuments    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factExtractTraces')
			ALTER TABLE RecHubData.factExtractTraces DROP CONSTRAINT PK_factExtractTraces;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factExtractTraces ADD
			CONSTRAINT PK_factExtractTraces PRIMARY KEY NONCLUSTERED (factExtractKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factExtractTraces_DepositDatefactExtractKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factExtractTraces_DepositDatefactExtractKey ON RecHubData.factExtractTraces
		(
			DepositDateKey,
			factExtractKey
		) $(OnDataPartition); 

	END
	ELSE
		RAISERROR('WI 271172 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271172 RecHubData.factExtractTraces table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd