--WFSScriptProcessorPrint CR 51289
--WFSScriptProcessorPrint Removing OLTA.usp_factBatchData_GetDateKey if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_factBatchData_GetDateKey') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure OLTA.usp_factBatchData_GetDateKey.',10,1) WITH NOWAIT
	DROP PROCEDURE OLTA.usp_factBatchData_GetDateKey
END
ELSE
	RAISERROR('CR has already been applied to this database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd