﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 236113
--WFSScriptProcessorPrint Dropping obsolete (deprecated) objects from the schema
--WFSScriptProcessorCRBegin

IF OBJECT_ID('RecHubUser.usp_Users_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get;
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID;
IF OBJECT_ID('RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization;
IF OBJECT_ID('RecHubAlert.usp_EventLog_Get_EmailAddress_ByEventLogID') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventLog_Get_EmailAddress_ByEventLogID;
--  For WI256852
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Del;
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Get;
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Get_ByOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Get_ByOLOrganizationID;
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Get_ByUserID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Get_ByUserID;
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Ins;
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccounts_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccounts_Upd;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganizations_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganizations_Del;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganizations_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganizations_Get;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganizations_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganizations_Ins;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganizations_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganizations_Upd;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganizations_Get_ByOrganizationCode') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganizations_Get_ByOrganizationCode;
IF OBJECT_ID('RecHubUser.usp_API_OLOrganization_Get_ByFilterSearchType') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLOrganization_Get_ByFilterSearchType;
IF OBJECT_ID('RecHubUser.usp_API_Permissions_Get_ByLogonName') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Permissions_Get_ByLogonName;
IF OBJECT_ID('RecHubUser.usp_API_UserPermissions_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_UserPermissions_Del;
IF OBJECT_ID('RecHubUser.usp_API_UserPermissions_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_UserPermissions_Ins;
IF OBJECT_ID('RecHubUser.usp_API_Users_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Del;
IF OBJECT_ID('RecHubUser.usp_API_Users_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Get;
IF OBJECT_ID('RecHubUser.usp_API_Users_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Ins;
IF OBJECT_ID('RecHubUser.usp_API_Users_Get_ByLogonName') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Get_ByLogonName;
IF OBJECT_ID('RecHubUser.usp_API_Users_Get_ByOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Get_ByOLOrganizationID;
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_ByOLClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_ByOLClientAccountID;
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_ByUserIDOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_ByUserIDOLOrganizationID;
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_DisplayModeByBankClientAccountBatchDepositDateOrganization') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_DisplayModeByBankClientAccountBatchDepositDateOrganization;
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_DisplayModeByClientAccountOrganization') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_DisplayModeByClientAccountOrganization;
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get_OLOrganizationTreePruned_ByUserID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get_OLOrganizationTreePruned_ByUserID;
IF OBJECT_ID('RecHubUser.usp_OLOrganizations_Get_ByOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLOrganizations_Get_ByOLOrganizationID;
IF OBJECT_ID('RecHubUser.usp_OLOrganizations_Get_OrganizationCodes') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLOrganizations_Get_OrganizationCodes;
IF OBJECT_ID('RecHubUser.usp_OLOrganizations_Get_ParentOrganizations') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLOrganizations_Get_ParentOrganizations;
IF OBJECT_ID('RecHubUser.usp_Session_Ins_CreateSession') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Session_Ins_CreateSession;
IF OBJECT_ID('RecHubUser.usp_Session_Ins_CreateSessionNoOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Session_Ins_CreateSessionNoOLOrganizationID;
IF OBJECT_ID('RecHubUser.usp_Users_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get;
IF OBJECT_ID('RecHubUser.usp_Users_Get_SuperUserClientAccounts') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_SuperUserClientAccounts;
IF OBJECT_ID('RecHubUser.usp_Users_Get_UserClientAccounts') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_UserClientAccounts;
IF OBJECT_ID('RecHubUser.usp_Users_Get_UserClientAccounts_Unpruned') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_UserClientAccounts_Unpruned;
IF OBJECT_ID('RecHubUser.usp_Users_Get_UserOrganizations') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_UserOrganizations;
IF OBJECT_ID('RecHubUser.usp_Users_Get_UserOrganizationsAsSuperUser') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_UserOrganizationsAsSuperUser;
IF OBJECT_ID('RecHubUser.usp_Users_Session_Get_BySID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Session_Get_BySID;
IF OBJECT_ID('RecHubAudit.usp_WFS_factExtractAudits_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_WFS_factExtractAudits_Ins;
--WFSScriptProcessorCREnd