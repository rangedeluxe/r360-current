--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 140890
--WFSScriptProcessorPrint Changing  CK_Events_EventLevel constraint to include value 0 if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubAlert' 
	AND CONSTRAINT_NAME = 'CK_Events_EventLevel' AND CHECK_CLAUSE = '([EventLevel]=(3) OR [EventLevel]=(2) OR [EventLevel]=(1) OR [EventLevel]=(0))')
BEGIN   

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubAlert' AND TABLE_NAME = 'Events' AND COLUMN_NAME = 'MessageSubject')
	BEGIN
	RAISERROR('ReBuilding Table RecHubAlert.Events.',10,1) WITH NOWAIT
			
	RAISERROR('Dropping Events contraints.',10,1) WITH NOWAIT
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubAlert' AND CONSTRAINT_NAME = 'CK_Events_EventLevel')
		ALTER TABLE RecHubAlert.[Events] DROP CONSTRAINT CK_Events_EventLevel;
		
	RAISERROR('Updating RecHubAlert.Events table properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAlert', 'TABLE', 'Events', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubAlert',
			@level1type = N'TABLE',
			@level1name = N'Events';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Stores Events for R360 Alerts
*	
* Check Constraint definitions:
* EventLevel
*	0: System Level
*	1: Site
*	2: Bank
*	3: ClientAccount	   
*
* Modification History
* 05/03/2013 WI 99837 JBS	Created.  Adding EventLevel column
* 05/08/2014 WI 140814 JPB	Added EventSubject and Modification columns.
* 05/09/2014 WI 140890 JBS	Change Constraint CK_Events_EventLevel to allow value 0 for System level Events
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubAlert,
	@level1type = N'TABLE',@level1name = Events;

	RAISERROR('Rebuilding CK_Events_EventLevel Constraint.',10,1) WITH NOWAIT 

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubAlert' AND CONSTRAINT_NAME = 'CK_Events_EventLevel')
		ALTER TABLE RecHubAlert.[Events]
			ADD CONSTRAINT CK_Events_EventLevel CHECK (EventLevel IN (0,1,2,3));
	END
	ELSE 
		RAISERROR('WI 140814 Needs to be applied to the database before WI 140890.',10,1) WITH NOWAIT;
END
ELSE
	RAISERROR('WI 140890 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
