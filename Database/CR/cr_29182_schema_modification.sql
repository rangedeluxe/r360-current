--WFSScriptProcessorPrint CR 29182
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factChecks]') AND name = N'IDX_factChecks_BankCustomerLockboxProcessingDateDepositDateKeyBatchID')
	CREATE NONCLUSTERED INDEX [IDX_factChecks_BankCustomerLockboxProcessingDateDepositDateKeyBatchID] ON [OLTA].[factChecks] 
	(
		[BankKey] ASC,
		[CustomerKey] ASC,
		[LockboxKey] ASC,
		[ProcessingDateKey] ASC,
		[DepositDateKey] ASC,
		[BatchID] ASC
	) ON OLTAFacts_Partition_Scheme(DepositDateKey)
--WFSScriptProcessorCREnd
