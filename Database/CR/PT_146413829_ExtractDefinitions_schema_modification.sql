--WFSScriptProcessorDoNotFormat
DECLARE @SQLCmd	VARCHAR(MAX) = 'UPDATE RecHubConfig.ExtractDefinitions SET ExtractDefinitionVARBinary = Convert(Varbinary(max), ExtractDefinition);'
		
IF NOT EXISTS 
	(
	SELECT * 
	FROM sys.columns
	Join sys.tables ON	
	sys.tables.object_id = sys.columns.object_id
	join sys.types ON sys.columns.user_type_id = sys.types.user_type_id
	where sys.tables.name = 'ExtractDefinitions'  
	AND sys.columns.name = 'ExtractDefinition'
	AND sys.types.name = 'varbinary'
	)
BEGIN
	RAISERROR('Beginning process convert ExtractDefinition column datatype from XML to VARBINARY in ExtractDefinitions table',10,1) WITH NOWAIT;
	
	RAISERROR('Adding New column ExtractDefinitionVARBinary',10,1) WITH NOWAIT;
	ALTER TABLE RecHubConfig.ExtractDefinitions ADD ExtractDefinitionVARBinary Varbinary(max);

	RAISERROR('Populating New column ExtractDefinitionVARBinary',10,1) WITH NOWAIT;
	EXEC(@SQLCmd);

	RAISERROR('Dropping Column ExtractDefinition',10,1) WITH NOWAIT;
	ALTER TABLE RecHubConfig.ExtractDefinitions DROP COLUMN ExtractDefinition;

	RAISERROR('Dropping Column ExtractDefinition',10,1) WITH NOWAIT;

	EXEC sp_rename 'RecHubConfig.ExtractDefinitions.ExtractDefinitionVARBinary', 'ExtractDefinition', 'COLUMN';
	
	RAISERROR('ExtractDefinitions Table has been successfully updated.',10,1) WITH NOWAIT;
	ALTER TABLE RecHubConfig.ExtractDefinitions ALTER COLUMN ExtractDefinition VARBINARY(MAX) NOT NULL;
END
ELSE
	RAISERROR('PT 146413829 has already been applied to the database.',10,1) WITH NOWAIT;

