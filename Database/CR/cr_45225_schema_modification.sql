--WFSScriptProcessorPrint CR 45225
--WFSScriptProcessorPrint Adding SiteID to OLTA.IMSInterfaceQueue if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND COLUMN_NAME='SiteID')
BEGIN
	/* Verify that CR 34330 has been applied before continuing. */
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'IMSInterfaceQueue' AND COLUMN_NAME = 'QueueData' AND IS_NULLABLE = 'YES')
	BEGIN /* CR 34330 has been applied, continue. */
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND CONSTRAINT_NAME='FK_ProcessingDate_IMSInterfaceQueue' )
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT FK_ProcessingDate_IMSInterfaceQueue
			
		RAISERROR('Dropping IMSInterfaceQueue contraints',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND CONSTRAINT_NAME='PK_IMSInterfaceQueue' )
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT PK_IMSInterfaceQueue

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_QueueID')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_QueueID

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_QueueType')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_QueueType

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_IMSInterfaceQueue_QueueStatus')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT CK_IMSInterfaceQueue_QueueStatus

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_QueueDataStatus')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_QueueDataStatus

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_QueueImageStatus')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_QueueImageStatus

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_RetryCount')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_RetryCount

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_CreationDate')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_CreationDate

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_CreatedBy')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_CreatedBy

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_ModificationDate')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_ModificationDate

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_ModifiedBy')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_ModifiedBy

		RAISERROR('Rebuilding OLTA.IMSInterfaceQueue',10,1) WITH NOWAIT
		EXEC sp_rename 'OLTA.IMSInterfaceQueue', 'OLDIMSInterfaceQueue'

		CREATE TABLE OLTA.IMSInterfaceQueue 
		(
			QueueID uniqueidentifier NOT NULL
				CONSTRAINT PK_IMSInterfaceQueue PRIMARY KEY NONCLUSTERED (QueueID)
				CONSTRAINT DF_IMSInterfaceQueue_QueueID DEFAULT(NEWID()),
			SiteID int NOT NULL
				CONSTRAINT DF_IMSInterfaceQueue_SiteID DEFAULT(-1),
			SiteBankID int NOT NULL,
			SiteLockboxID int NOT NULL,
			ProcessingDateKey int NOT NULL,
			BatchID int NOT NULL,
			QueueType smallint NOT NULL
				CONSTRAINT DF_IMSInterfaceQueue_QueueType DEFAULT(0),
			QueueStatus smallint NOT NULL
				CONSTRAINT CK_IMSInterfaceQueue_QueueStatus CHECK(QueueStatus IN (0,10,20,30,40,99,130,150)),
			QueueDataStatus bit NOT NULL
				CONSTRAINT DF_IMSInterfaceQueue_QueueDataStatus DEFAULT(0),
			QueueImageStatus bit NOT NULL 
				CONSTRAINT DF_IMSInterfaceQueue_QueueImageStatus DEFAULT(0),
			QueueData xml NULL,
			RetryCount TINYINT NOT NULL CONSTRAINT DF_IMSInterfaceQueue_RetryCount DEFAULT (0),
			CreationDate datetime NOT NULL CONSTRAINT DF_IMSInterfaceQueue_CreationDate DEFAULT(GETDATE()),
			CreatedBy varchar(32) NOT NULL CONSTRAINT DF_IMSInterfaceQueue_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate datetime NOT NULL CONSTRAINT DF_IMSInterfaceQueue_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy varchar(32) NOT NULL CONSTRAINT DF_IMSInterfaceQueue_ModifiedBy DEFAULT(SUSER_SNAME())       
		)

		RAISERROR('Creating index IDX_CreationDate_QueueType_QueueDataStatus',10,1) WITH NOWAIT
		CREATE INDEX IDX_CreationDate_QueueType_QueueDataStatus ON OLTA.IMSInterfaceQueue (CreationDate,QueueType,QueueDataStatus)

		RAISERROR('Creating index IDX_CreationDate_QueueType_QueueImageStatus',10,1) WITH NOWAIT
		CREATE INDEX IDX_CreationDate_QueueType_QueueImageStatus ON OLTA.IMSInterfaceQueue (CreationDate,QueueType,QueueImageStatus)

		RAISERROR('Creating index IDX_SiteBankID_SiteLockboxID_ProcessingDateKey_BatchID',10,1) WITH NOWAIT
		CREATE INDEX IDX_SiteBankID_SiteLockboxID_ProcessingDateKey_BatchID ON OLTA.IMSInterfaceQueue (SiteBankID,SiteLockboxID,ProcessingDateKey,BatchID)

		RAISERROR('Creating index IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType',10,1) WITH NOWAIT
		CREATE CLUSTERED INDEX [IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType] ON [OLTA].[IMSInterfaceQueue] 
		(
			[QueueStatus] ASC,
			[QueueDataStatus] ASC,
			[QueueType] ASC
		)
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'IMSInterfaceQueue', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'IMSInterfaceQueue';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2009
*
* Purpose: This table will be populated by the XBatchImport stored procedure. 
*	The table will act as an interface point between the OLTA database and an
*	external Image Management System (IMS).
*
* Check Constraint definitions:
* QueueType
*	0: Delete
*	1: Insert
*	2: RPS Insert
*
* QueueStatus
*	0: Not processed
*	10: Available for processing
*	20: In Process
*	30: Error
*	40: Image(s) not found for Batch
*	99: Complete
*	130: An error file was received from Hyland indicating a rejection
*	150: A response file was received from Hyland, and OLTA was updated appropriately. 
*
* Modification History
* 07/08/2009 CR 25817 JPB	Created
* 12/23/2009 CR 28537 JPB	Added cluster index 
*							IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType
* 10/05/2010 CR 31236 JPB	Alter table to meet new requirements
* 03/02/2011 CR 32827 JPB	Added status values 130 and 150.
* 05/13/2011 CR 34323 JPB	Added status values 40.
* 05/13/2011 CR 34330 JPB	Allow QueueData column to be NULL.
* 06/16/2011 CR 45225 JPB	Added SiteID to table.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = IMSInterfaceQueue

		/* NOTE: The PO Box number is not handle here since it does not exist in the 'old' table. 
			This is being done to keep the columns in the same order as a newly created DB. */

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
			
		INSERT INTO OLTA.IMSInterfaceQueue 
		(
			QueueID,
			SiteID,
			SiteBankID,
			SiteLockboxID,
			ProcessingDateKey,
			BatchID,
			QueueType,
			QueueStatus,
			QueueDataStatus,
			QueueImageStatus,
			QueueData,
			RetryCount,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy       
		) 
		SELECT	QueueID,
				-1,
				SiteBankID,
				SiteLockboxID,
				ProcessingDateKey,
				BatchID,
				QueueType,
				QueueStatus,
				QueueDataStatus,
				QueueImageStatus,
				QueueData,
				RetryCount,
				CreationDate,
				CreatedBy,
				ModificationDate,
				ModifiedBy       
		FROM 	OLTA.OLDIMSInterfaceQueue

		RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
		ALTER TABLE OLTA.IMSInterfaceQueue ADD 
			CONSTRAINT FK_ProcessingDate_IMSInterfaceQueue FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey)

		IF OBJECT_ID('OLTA.OLDIMSInterfaceQueue') IS NOT NULL
			DROP TABLE OLTA.OLDIMSInterfaceQueue
	END
	ELSE
	BEGIN
		RAISERROR('CR 34330 must be applied before running this CR.',10,1) WITH NOWAIT
	END
END