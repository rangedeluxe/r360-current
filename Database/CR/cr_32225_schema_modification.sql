--WFSScriptProcessorPrint CR 32225
--WFSScriptProcessorPrint Removing OLTA.usp_factTransactionDetailsUpdate if necessary
--WFSScriptProcessorCRBegin
	IF OBJECT_ID('OLTA.usp_factTransactionDetailsUpdate') IS NOT NULL
		DROP PROCEDURE OLTA.usp_factTransactionDetailsUpdate
--WFSScriptProcessorCREnd