--WFSScriptProcessorPrint WI 135315
--WFSScriptProcessorPrint Updating RecHubData.factChecks.BatchID if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factChecks' AND COLUMN_NAME = 'BatchID' AND DATA_TYPE = 'bigint' )
BEGIN
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factChecks' AND COLUMN_NAME = 'DDAKey' )
	BEGIN /* verify that WI DDAKey has been applied before continuing. */
		RAISERROR('WI 131199 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Dropping indexes with BatchID',10,1) WITH NOWAIT;

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID

		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name='IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID' AND object_id = OBJECT_ID('RecHubData.factChecks') )
			DROP INDEX RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID

		RAISERROR('Updating RecHubData.factChecks.BatchID',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factChecks ALTER COLUMN BatchID BIGINT;

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey ON RecHubData.factChecks 
		(
			DepositDateKey,
			ClientAccountKey,
			SourceProcessingDateKey,
			BatchID,
			BatchSequence,
			BankKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber ON RecHubData.factChecks
		(
			[BankKey] ASC,
			[OrganizationKey] ASC,
			[ClientAccountKey] ASC,
			[ImmutableDateKey] ASC,
			[DepositDateKey] ASC,
			[BatchID] ASC,
			[BatchNumber] ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
		(
			[DepositDateKey] ASC,
			[ClientAccountKey] ASC,
			[ImmutableDateKey] ASC,
			[BankKey] ASC,
			[OrganizationKey] ASC,
			[BatchID] ASC,
			[BatchNumber] ASC,
			[TransactionID] ASC
		)
		INCLUDE 
		( 
			[BatchSequence],
			[CheckSequence],
			[NumericSerial],
			[Serial]
		) $(OnDataPartition);

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus ON RecHubData.factChecks
		(
			ClientAccountKey ASC,
			DepositDateKey ASC,
			ImmutableDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			Amount ASC,
			Serial ASC,
			DepositStatus ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			Amount ASC,
			BankKey ASC,
			OrganizationKey ASC,
			ImmutableDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			TransactionID ASC
		)
		INCLUDE
		(
			DepositStatus,
			NumericSerial,
			CheckSequence,
			Serial
		)  $(OnDataPartition);

		RAISERROR('Rebuilding RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factChecks
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			BatchID ASC,
			TransactionID ASC
		)  $(OnDataPartition);

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factChecks', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'factChecks';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every check.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51372 JPB	Renamed index and added INCLUDE columns.
* 04/05/2012 CR 51866 JPB	Added new index to improve remittence search.
* 03/26/2012 CR 51541 JPB	Added BatchPaymentTypeKey.
* 04/06/2012 CR 51416 JPB	Added new index to improve search performance.
* 07/05/2012 CR 53625 JPB	Added BatchCueID.
* 07/16/2012 CR 54121 JPB	Added BatchNumber.
* 07/17/2012 CR 54131 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54132 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54133 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54206 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71800 JPB	2.0 release. Change Schema name.
*							Adding columns: factCheckKey, IsDeleted, NumericRoutingNumber,
*							NumericSerial, RoutingNumber, Account, RemitterName.
*							Renaming columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey.
*							Remove Columns: ImageInfoXML, RemitterKey.
*							Added factCheckKey to ClusteredIndex.
*							Renamed indexes to match schema and column changes	
*							Create New Index based from old clustered index.				
*							Forward Patch: WI 87430
* 02/28/2014 WI 131199 JBS	Add DDAKey, including FK constraint and Index
* 04/10/2014 WI 135315 JPB	Changed BatchID from INT to BIGINT.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factChecks;
	END
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
