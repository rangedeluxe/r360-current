--WFSScriptProcessorPrint WI 77758
--WFSScriptProcessorPrint Updating factBatchSummary if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]') AND name = N'IDX_factBatchSummary_LockboxBankProcessingDateKeyBatchID' )
BEGIN
	IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]') AND name = N'IDX_factBatchSummary_BankCustomerLockboxProcessingDateDepositDateKeyBatchIDNumer' )
	BEGIN
		RAISERROR('CR 54129 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factBatchSummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factBatchSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 11/19/2009 CR 28189 JPB	Allow GlobalBatchID to be NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
* 01/11/2012 CR 49274 JPB	Added SourceProcessingDateKey.
* 01/19/2012 CR 48978 JPB	Added BatchSiteCode.
* 03/26/2012 CR 51540 JPB	Added BatchPaymentTypeKey.
* 07/03/2012 CR 53623 JPB	Added BatchCueID.
* 07/16/2012 CR 54119 JPB	Added BatchNumber.
* 07/16/2012 CR 54129 JPB	Renamed and updated index with BatchNumber.
* 01/03/2013 WI 77758 JPB	Added new index.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factBatchSummary
		
		RAISERROR('Creating index OLTA.factBatchSummary.IDX_factBatchSummary_LockboxBankProcessingDateKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_LockboxBankProcessingDateKeyBatchID ON OLTA.factBatchSummary
		(
			[LockboxKey] ASC,
			[BankKey] ASC,
			[ProcessingDateKey] ASC,
			[BatchID] ASC
		)
		INCLUDE 
		( 
			[BatchSourceKey]
		);
	END
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd