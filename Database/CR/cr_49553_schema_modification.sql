--WFSScriptProcessorPrint CR 49553
--WFSScriptProcessorPrint Adding BatchSiteCode to OLTA.factStubs if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factStubs' AND COLUMN_NAME = 'BatchSiteCode' )
BEGIN /* verify that CR 49278 has been applied before continuing. */
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factStubs' AND COLUMN_NAME = 'SourceProcessingDateKey' )
	BEGIN
		RAISERROR('CR 49278 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimBanks_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimBanks_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimCustomers_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimCustomers_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimLockboxes_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimLockboxes_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_ProcessingDate_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_ProcessingDate_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_SourceProcessingDate_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_DepositDate_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_DepositDate_factStubs
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimBatchSources_factStubs' )
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimBatchSources_factStubs

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factStubs_ModificationDate ')
			ALTER TABLE OLTA.factStubs DROP CONSTRAINT DF_factStubs_ModificationDate

		RAISERROR('Rebuilding OLTA.factStubs adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factStubs', 'OLDfactStubs'

		CREATE TABLE OLTA.factStubs
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL,
			ProcessingDateKey int NOT NULL,
			SourceProcessingDateKey int NOT NULL, --CR 49278 JPB 01/12/2012
			GlobalBatchID int NOT NULL, --CR 28194 JPB 11/13/2009
			BatchID int NOT NULL,
			SystemType tinyint NOT NULL,
			DepositStatus int NOT NULL,
			TransactionID int NOT NULL,
			TxnSequence int NOT NULL,
			SequenceWithinTransaction int NOT NULL,
			BatchSequence int NOT NULL,
			StubSequence int NOT NULL,
			GlobalStubID int NULL,
			IsOMRDetected bit NULL,
			AccountNumber varchar(80) NULL,
			Amount money NULL,
			DocumentBatchSequence int NULL,
			BatchSourceKey tinyint NOT NULL, --CR 32299 JPB 01/13/2011
			BatchSiteCode int NULL, --CR 49553 JPB 01/23/2012
			LoadDate datetime NOT NULL,
			ModificationDate datetime NOT NULL --CR 28999 JPB 02/12/2010
				CONSTRAINT DF_factStubs_ModificationDate DEFAULT GETDATE()
		) $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factStubs ADD
			CONSTRAINT FK_dimBanks_factStubs FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factStubs FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factStubs FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_ProcessingDate_factStubs FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_DepositDate_factStubs FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factStubs FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimBatchSources_factStubs FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
			
		RAISERROR('Creating Index OLTA.factStubs.IDX_factStubs_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factStubs_DepositDateKey ON OLTA.factStubs (DepositDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factStubs.IDX_factStubs_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factStubs_BankKey ON OLTA.factStubs (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factStubs.IDX_factStubs_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factStubs_CustomerKey ON OLTA.factStubs (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factStubs.IDX_factStubs_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factStubs_LockboxKey ON OLTA.factStubs (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factStubs.IDX_factStubs_ProcessingDate',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factStubs_ProcessingDateKey ON OLTA.factStubs (ProcessingDateKey) $(OnPartition)
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factStubs', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factStubs';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28194 JPB	GlobalBatchID is now nullable.
* 02/12/2010 CR 28999 JPB	Added ModificationDate.
* 04/05/2010 CR 29302 JPB	Renamed Account to AccountNumber.
* 01/13/2011 CR 32299 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49278 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49553 JPB	Added BatchSiteCode.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factStubs
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factStubs
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			GlobalBatchID,
			BatchID,
			SystemType,
			DepositStatus,
			TransactionID,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			StubSequence,
			GlobalStubID,
			IsOMRDetected,
			AccountNumber,
			Amount,
			DocumentBatchSequence,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				GlobalBatchID,
				BatchID,
				SystemType,
				DepositStatus,
				TransactionID,
				TxnSequence,
				SequenceWithinTransaction,
				BatchSequence,
				StubSequence,
				GlobalStubID,
				IsOMRDetected,
				AccountNumber,
				Amount,
				DocumentBatchSequence,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactStubs

		IF OBJECT_ID('OLTA.OLDfactStubs') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factStubs table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactStubs
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd

