--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 130478903
--WFSScriptProcessorPrint Dropping unuser SQL users if necessary.
--WFSScriptProcessorCRBegin

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubAlert_User' )
BEGIN
	RAISERROR('Removing RecHubAlert_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubAlert_User];
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubAlert_User' )
BEGIN
	RAISERROR('Removing RecHubAlert_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubAlert_User];
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubAudit_User' )
BEGIN
	RAISERROR('Removing RecHubAudit_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubAudit_User];
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubAudit_User' )
BEGIN
	RAISERROR('Removing RecHubAudit_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubAudit_User];
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubConfig_Admin' )
BEGIN
	RAISERROR('Removing RecHubConfig_Admin from database',10,1) WITH NOWAIT;
	DROP USER [RecHubConfig_Admin]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubConfig_Admin' )
BEGIN
	RAISERROR('Removing RecHubConfig_Admin from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubConfig_Admin]
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubConfig_User' )
BEGIN
	RAISERROR('Removing RecHubConfig_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubConfig_User]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubConfig_User' )
BEGIN
	RAISERROR('Removing RecHubConfig_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubConfig_User]
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubData_User' )
BEGIN
	RAISERROR('Removing RecHubData_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubData_User]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubData_User' )
BEGIN
	RAISERROR('Removing RecHubData_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubData_User]
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubDBO_User' )
BEGIN
	RAISERROR('Removing RecHubDBO_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubDBO_User]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubDBO_User' )
BEGIN
	RAISERROR('Removing RecHubDBO_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubDBO_User]
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubException_User' )
BEGIN
	RAISERROR('Removing RecHubException_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubException_User]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubException_User' )
BEGIN
	RAISERROR('Removing RecHubException_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubException_User]
END

IF EXISTS( SELECT 1 FROM sys.database_principals WHERE name = N'RecHubSystem_User' )
BEGIN
	RAISERROR('Removing RecHubSystem_User from database',10,1) WITH NOWAIT;
	DROP USER [RecHubSystem_User]
END

IF EXISTS( SELECT 1 FROM sys.server_principals WHERE name = N'RecHubSystem_User' )
BEGIN
	RAISERROR('Removing RecHubSystem_User from SQL Server',10,1) WITH NOWAIT;
	DROP LOGIN [RecHubSystem_User]
END
--WFSScriptProcessorCREnd
