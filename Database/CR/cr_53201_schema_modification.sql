--WFSScriptProcessorPrint CR 53201
--WFSScriptProcessorPrint Updates to OLTA.DataImportQueue if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='DataImportQueue' AND COLUMN_NAME='ClientProcessCode')
BEGIN
	RAISERROR('Dropping DataImportQueue contraints.',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='DataImportQueue' AND CONSTRAINT_NAME='PK_DataImportQueue' )
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT PK_DataImportQueue;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND CONSTRAINT_NAME='CK_DataImportQueue_QueueType' )
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_QueueType;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_QueueStatus')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_QueueStatus;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND CONSTRAINT_NAME='CK_DataImportQueue_QueueStatus' )
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_QueueStatus;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ResponseStatus')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ResponseStatus;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND CONSTRAINT_NAME='CK_DataImportQueue_ResponseStatus' )
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_ResponseStatus;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_CreationDate')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_CreatedBy')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_CreatedBy;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ModificationDate')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ModificationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ModifiedBy')
		ALTER TABLE OLTA.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ModifiedBy;
		
	RAISERROR('Rebuilding OLTA.DataImportQueue.',10,1) WITH NOWAIT;
	EXEC sp_rename 'OLTA.DataImportQueue', 'OLDDataImportQueue';
		
	CREATE TABLE OLTA.DataImportQueue
	(
		DataImportQueueID BIGINT NOT NULL IDENTITY(1,1)
			CONSTRAINT [PK_DataImportQueue] PRIMARY KEY CLUSTERED,
		QueueType TINYINT NOT NULL
			CONSTRAINT CK_DataImportQueue_QueueType CHECK(QueueType IN (0,1,2)),
		QueueStatus TINYINT NOT NULL
			CONSTRAINT DF_DataImportQueue_QueueStatus DEFAULT 10
			CONSTRAINT CK_DataImportQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99,120,145,150)),
		ResponseStatus TINYINT NOT NULL
			CONSTRAINT DF_DataImportQueue_ResponseStatus DEFAULT 255
			CONSTRAINT CK_DataImportQueue_ResponseStatus CHECK(ResponseStatus IN (0,1,2,30,255)),
		XSDVersion VARCHAR(12) NOT NULL,
		ClientProcessCode VARCHAR(40) NOT NULL, 
		SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
		EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
		ResponseTrackingID UNIQUEIDENTIFIER NULL,
		XMLDataDocument XML NOT NULL,
		XMLResponseDocument XML NULL,
		CreationDate datetime NOT NULL
			CONSTRAINT DF_DataImportQueue_CreationDate DEFAULT GETDATE(),
		CreatedBy varchar(32) NOT NULL
			CONSTRAINT DF_DataImportQueue_CreatedBy DEFAULT SUSER_NAME(),
		ModificationDate datetime NOT NULL
			CONSTRAINT DF_DataImportQueue_ModificationDate DEFAULT GETDATE(),
		ModifiedBy varchar(32) NOT NULL
			CONSTRAINT DF_DataImportQueue_ModifiedBy DEFAULT SUSER_NAME()
	)
		
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'DataImportQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'DataImportQueue';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Queue work from external data sources via the Web Service.
*		   
* QueueType
*	0 - Client Setup
*	1 - Batch Data
*	2 - Notification
*
* QueueStatus
*	10 - Ready to process (default)
*	15 - Failed processing on OLTA - but can be reprocessed
*	20 - In process
*	30 - Failed processing on OLTA - but cannot be reprocessed
*	99 - Successfully imported, response ready
*	120 - Response send, waiting for receipt confirmation
*	145 - Client unknown error. "Dead" record.
*	150 - Receipt confirmation received, response process complete
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	30 = Failed processing on OLTA, data could not be processed
*	255 = Status not set
*
* Modification History
* 01/24/2012 CR 49666 JPB	Created
* 05/30/2012 CR 53201 JPB	Added Column ClientProcessCode, QueueType 2, 
*								QueueStatus 145, and ResponseStatus 30.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = DataImportQueue;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	SET IDENTITY_INSERT OLTA.DataImportQueue ON;
	INSERT INTO OLTA.DataImportQueue
	(
		DataImportQueueID,
		QueueType,
		QueueStatus,
		ResponseStatus,
		XSDVersion,
		ClientProcessCode, 
		SourceTrackingID,
		EntityTrackingID,
		ResponseTrackingID,
		XMLDataDocument,
		XMLResponseDocument,
		CreatedBy,
		ModificationDate,
		ModifiedBy
	)
	SELECT 	DataImportQueueID,
			QueueType,
			QueueStatus,
			ResponseStatus,
			XSDVersion,
			'Unassigned', 
			SourceTrackingID,
			EntityTrackingID,
			ResponseTrackingID,
			XMLDataDocument,
			XMLResponseDocument,
			CreatedBy,
			ModificationDate,
			ModifiedBy
	FROM OLTA.OLDDataImportQueue
	
	SET IDENTITY_INSERT OLTA.DataImportQueue OFF;
	
	RAISERROR('Creating index IDX_DataImportQueue_EntityTrackingID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_DataImportQueue_EntityTrackingID ON OLTA.DataImportQueue (EntityTrackingID);
	RAISERROR('Creating index IDX_DataImportQueue_SourceTrackingID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_DataImportQueue_SourceTrackingID ON OLTA.DataImportQueue (SourceTrackingID);
	RAISERROR('Creating index IDX_DataImportQueue_ClientProcessCode',10,1) WITH NOWAIT;
	CREATE INDEX IDX_DataImportQueue_ClientProcessCode ON OLTA.DataImportQueue (ClientProcessCode);

	IF OBJECT_ID('OLTA.OLDDataImportQueue') IS NOT NULL
			DROP TABLE OLTA.OLDDataImportQueue
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd