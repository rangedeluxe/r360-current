--WFSScriptProcessorPrint CR 32871
--WFSScriptProcessorPrint Adding POBox to OLTA.dimLockboxes if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='dimLockboxes' AND COLUMN_NAME='POBox')
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factBatchSummary' )
		ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimLockboxes_factBatchSummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimLockboxes_factChecks' )
		ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimLockboxes_factChecks

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimLockboxes_factDataEntryDetails' )
		ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimLockboxes_factDataEntryDetails

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimLockboxes_factDataEntrySummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimLockboxes_factDocuments' )
		ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimLockboxes_factDocuments

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimLockboxes_factStubs' )
		ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimLockboxes_factStubs

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionDetails' AND CONSTRAINT_NAME='FK_dimLockboxes_factTransactionDetails' )
		ALTER TABLE OLTA.factTransactionDetails DROP CONSTRAINT FK_dimLockboxes_factTransactionDetails

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimLockboxes_factTransactionSummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND CONSTRAINT_NAME='FK_dimLockboxes_IMSInterfaceQueue' )
		ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT FK_dimLockboxes_IMSInterfaceQueue

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='LockboxesDataEntryColumns' AND CONSTRAINT_NAME='FK_dimLockboxes_LockboxesDataEntryColumns' )
		ALTER TABLE OLTA.LockboxesDataEntryColumns DROP CONSTRAINT FK_dimLockboxes_LockboxesDataEntryColumns

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='LockboxesDataEntrySetups' AND CONSTRAINT_NAME='FK_dimLockboxes_LockboxesDataEntrySetups' )
		ALTER TABLE OLTA.LockboxesDataEntrySetups DROP CONSTRAINT FK_dimLockboxes_LockboxesDataEntrySetups

	RAISERROR('Dropping dimLockbox contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimLockboxes' AND CONSTRAINT_NAME='PK_dimLockboxes' )
		ALTER TABLE OLTA.dimLockboxes DROP CONSTRAINT PK_dimLockboxes

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimLockboxes_OnlineColorMode')
		ALTER TABLE OLTA.dimLockboxes DROP CONSTRAINT DF_dimLockboxes_OnlineColorMode

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimLockboxes_IsCommingled')
		ALTER TABLE OLTA.dimLockboxes DROP CONSTRAINT DF_dimLockboxes_IsCommingled

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimLockboxes_SiteLockboxKey')
		ALTER TABLE OLTA.dimLockboxes DROP CONSTRAINT DF_dimLockboxes_SiteLockboxKey

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimLockboxes_ModificationDate')
		ALTER TABLE OLTA.dimLockboxes DROP CONSTRAINT DF_dimLockboxes_ModificationDate

	RAISERROR('Rebuilding OLTA.dimLockboxes',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.dimLockboxes', 'OLDdimLockboxes'

	CREATE TABLE OLTA.dimLockboxes
	(
		LockboxKey int NOT NULL IDENTITY(1,1) CONSTRAINT PK_dimLockboxes PRIMARY KEY CLUSTERED,
		SiteCode int NOT NULL,
		SiteLockboxID int NOT NULL,
		SiteBankID int NOT NULL,
		SiteCustomerID int NOT NULL,
		ShortName varchar(11) NOT NULL,
		LongName varchar(40) NULL,
		POBox varchar(32) NULL,
		DDA varchar(40) NULL,
		OnlineColorMode tinyint NOT NULL
			CONSTRAINT DF_dimLockboxes_OnlineColorMode DEFAULT 1,
		CutOff tinyint NOT NULL,
		IsActive tinyint NOT NULL,
		IsCommingled bit NOT NULL
			CONSTRAINT DF_dimLockboxes_IsCommingled DEFAULT 0,
		SiteLockboxKey UNIQUEIDENTIFIER NOT NULL 
			CONSTRAINT DF_dimLockboxes_SiteLockboxKey DEFAULT '00000000-0000-0000-0000-000000000000',
		MostRecent bit NOT NULL,
		LoadDate datetime NOT NULL,
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_dimLockboxes_ModificationDate DEFAULT GETDATE()
	)
	RAISERROR('Creating index IDX_dimLockboxes_SiteBankID_SiteCustomerID_SiteLockboxID',10,1) WITH NOWAIT
	CREATE INDEX IDX_dimLockboxes_SiteBankID_SiteCustomerID_SiteLockboxID ON OLTA.dimLockboxes (SiteBankID, SiteCustomerID, SiteLockboxID)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimLockboxes', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'dimLockboxes';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Lockboxes dimension is a SCD type 2 holding Lockbox info.  Most 
*	recent flag of 1 indicates the current Lockbox row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 05/05/2010 CR 29158 JPB	Added IsCommingled.
* 05/13/2010 CR 29709 JPB	Added SiteLockboxKey.
* 08/02/2010 CR 30378 JPB	Added standard ModificationDate.
* 08/02/2010 CR 30307 JPB	Added default contraint 1 to OnlineColorMode.
* 01/07/2011 CR 31470 JPB 	Added POBox (32871 install script)
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = dimLockboxes

	/* NOTE: The PO Box number is not handle here since it does not exist in the 'old' table. 
		This is being done to keep the columns in the same order as a newly created DB. */

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
	SET IDENTITY_INSERT OLTA.dimLockboxes ON
	INSERT INTO OLTA.dimLockboxes 
	(
		LockboxKey,
		SiteCode,
		SiteLockboxID,
		SiteBankID,
		SiteCustomerID,
		ShortName,
		LongName,
		POBox,
		DDA,
		OnlineColorMode,
		CutOff,
		IsActive,
		IsCommingled,
		SiteLockboxKey,
		MostRecent,
		LoadDate,
		ModificationDate
	) 
	SELECT	OLTA.OLDdimLockboxes.LockboxKey,
			OLTA.OLDdimLockboxes.SiteCode,
			OLTA.OLDdimLockboxes.SiteLockboxID,
			OLTA.OLDdimLockboxes.SiteBankID,
			OLTA.OLDdimLockboxes.SiteCustomerID,
			OLTA.OLDdimLockboxes.ShortName,
			OLTA.OLDdimLockboxes.LongName,
			dbo.Lockbox.POBox,
			OLTA.OLDdimLockboxes.DDA,
			OLTA.OLDdimLockboxes.OnlineColorMode,
			OLTA.OLDdimLockboxes.CutOff,
			OLTA.OLDdimLockboxes.IsActive,
			OLTA.OLDdimLockboxes.IsCommingled,
			OLTA.OLDdimLockboxes.SiteLockboxKey,
			OLTA.OLDdimLockboxes.MostRecent,
			OLTA.OLDdimLockboxes.LoadDate,
			OLTA.OLDdimLockboxes.LoadDate
	FROM 	OLTA.OLDdimLockboxes
			LEFT JOIN dbo.Lockbox on dbo.Lockbox.BankID = OLTA.OLDdimLockboxes.SiteBankID
				AND dbo.Lockbox.LockboxID = OLTA.OLDdimLockboxes.SiteLockboxID
	SET IDENTITY_INSERT OLTA.dimLockboxes OFF

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.factBatchSummary ADD 
       CONSTRAINT FK_dimLockboxes_factBatchSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.factChecks ADD
		CONSTRAINT FK_dimLockboxes_factChecks FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)
			
	ALTER TABLE OLTA.factDataEntryDetails ADD
		CONSTRAINT FK_dimLockboxes_factDataEntryDetails FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.factDataEntrySummary ADD
		CONSTRAINT FK_dimLockboxes_factDataEntrySummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.factDocuments ADD
		CONSTRAINT FK_dimLockboxes_factDocuments FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.factStubs ADD
		CONSTRAINT FK_dimLockboxes_factStubs FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)
			
	ALTER TABLE OLTA.factTransactionDetails ADD 
		CONSTRAINT FK_dimLockboxes_factTransactionDetails FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.factTransactionSummary ADD 
		CONSTRAINT FK_dimLockboxes_factTransactionSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)

	ALTER TABLE OLTA.LockboxesDataEntryColumns ADD
		CONSTRAINT FK_dimLockboxes_LockboxesDataEntryColumns FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)
			
	ALTER TABLE OLTA.LockboxesDataEntrySetups ADD
		CONSTRAINT FK_dimLockboxes_LockboxesDataEntrySetups FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey)
			
	IF OBJECT_ID('OLTA.OLDdimLockboxes') IS NOT NULL
		DROP TABLE OLTA.OLDdimLockboxes

	RAISERROR('Granting SELECT ON OLTA.dimLockboxes',10,1) WITH NOWAIT
	GRANT SELECT ON [OLTA].[dimLockboxes] TO [OLTAUser]
END