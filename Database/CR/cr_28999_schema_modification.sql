--WFSScriptProcessorPrint CR 28999 
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.factStubs
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factStubs ADD ModificationDate DATETIME NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Updating existing OLTA.factStubs.ModificationDate with LoadDate
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factStubs 
SET		ModificationDate = LoadDate
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factStubs.ModificationDate to NOT NULL
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factStubs ALTER COLUMN ModificationDate DATETIME NOT NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factStubs.ModificationDate adding default constraint
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM	sysobjects WHERE name = 'DF_factStubs_ModificationDate')
	AND EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factStubs ADD
		CONSTRAINT DF_factStubs_ModificationDate DEFAULT GETDATE() FOR ModificationDate
--WFSScriptProcessorCREnd