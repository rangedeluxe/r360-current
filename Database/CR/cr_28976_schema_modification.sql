--WFSScriptProcessorPrint CR 28976 
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.factChecks
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factChecks ADD ModificationDate DATETIME NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Updating existing OLTA.factChecks.ModificationDate with LoadDate
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factChecks 
SET		ModificationDate = LoadDate
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factChecks.ModificationDate to NOT NULL
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factChecks ALTER COLUMN ModificationDate DATETIME NOT NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factChecks.ModificationDate adding default constraint
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM	sysobjects WHERE name = 'DF_factChecks_ModificationDate')
	AND EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factChecks ADD
		CONSTRAINT DF_factChecks_ModificationDate DEFAULT GETDATE() FOR ModificationDate
--WFSScriptProcessorCREnd
