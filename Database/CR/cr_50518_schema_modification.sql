--WFSScriptProcessorPrint CR 50518
--WFSScriptProcessorPrint Modifing OLTA.ImportSchemas if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'ImportSchemas' AND COLUMN_NAME = 'XSDVersion' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='ImportSchemas' AND CONSTRAINT_NAME='FK_ImportSchemaTypes_ImportSchemas' )
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT FK_ImportSchemaTypes_ImportSchemas

	RAISERROR('Dropping ImportSchemas contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='ImportSchemas' AND CONSTRAINT_NAME='PK_ImportSchemas' )
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT PK_ImportSchemas
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ImportSchemas_SchemaDate')
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT DF_ImportSchemas_SchemaDate
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ImportSchemas_CreationDate')
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT DF_ImportSchemas_CreationDate
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ImportSchemas_CreatedBy')
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT DF_ImportSchemas_CreatedBy
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ImportSchemas_ModificationDate')
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT DF_ImportSchemas_ModificationDate
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_ImportSchemas_ModifiedBy')
		ALTER TABLE OLTA.ImportSchemas DROP CONSTRAINT DF_ImportSchemas_ModifiedBy

	RAISERROR('Rebuilding OLTA.ImportSchemas',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.ImportSchemas', 'OLDImportSchemas'

	CREATE TABLE OLTA.ImportSchemas
	(
		ImportSchemaTypeID int NOT NULL,
		XSDVersion VARCHAR(12) NOT NULL,
		ImportSchema xml NOT NULL,
		CreationDate datetime NOT NULL 
			CONSTRAINT DF_ImportSchemas_CreationDate DEFAULT(GETDATE()),
		CreatedBy varchar(32) NOT NULL 
			CONSTRAINT DF_ImportSchemas_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_ImportSchemas_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy varchar(32) NOT NULL 
			CONSTRAINT DF_ImportSchemas_ModifiedBy DEFAULT(SUSER_SNAME())
		CONSTRAINT PK_ImportSchemas PRIMARY KEY(ImportSchemaTypeID,XSDVersion)              
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'ImportSchemas', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'ImportSchemas';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table is used by the dynamic partitioning routine it will hold 
*	static data associated with the fact table partitioning.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/23/2012 CR 50518 JPB	Renamed ImportScheamRevison to XSDVersion and 
*							replaced SchemaDate with standard creation/modification
*							columns.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = ImportSchemas

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	INSERT INTO OLTA.ImportSchemas
	(
		ImportSchemaTypeID,
		XSDVersion,
		ImportSchema,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy	
	)
	SELECT	ImportSchemaTypeID,
			'1.0',
			ImportSchema,
			SchemaDate,
			'sa',
			SchemaDate,
			'sa'	
	FROM	OLTA.OLDImportSchemas

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.ImportSchemas ADD        
		   CONSTRAINT FK_ImportSchemaTypes_ImportSchemas FOREIGN KEY(ImportSchemaTypeID) REFERENCES OLTA.ImportSchemaTypes (ImportSchemaTypeID)

	IF OBJECT_ID('OLTA.OLDImportSchemas') IS NOT NULL
		DROP TABLE OLTA.OLDImportSchemas

END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd