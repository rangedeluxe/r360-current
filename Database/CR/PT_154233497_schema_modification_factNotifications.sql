﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 154233497
--WFSScriptProcessorPrint Adding RecHubData.factNotifications.MessageWorkgroups if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT name FROM sys.indexes WHERE name = 'IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime') 
	RAISERROR('WI 216416 must be applied first.',16,1) WITH NOWAIT;
ELSE	
BEGIN
	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factNotifications' AND COLUMN_NAME = 'MessageWorkgroups' )
		RAISERROR('PT has already been applied to the database.',10,1) WITH NOWAIT;
	ELSE
	BEGIN
		RAISERROR('Adding RecHubData.factNotifications.MessageWorkgroups',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factNotifications ADD MessageWorkgroups UNIQUEIDENTIFIER NULL;
		
		IF EXISTS(SELECT 1 FROM sys.fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factNotifications', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factNotifications';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factNotifications,
		@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*
*
* Column Information
* UserNotification - Used to determine if record is a "workgroup" or user
*	notification. 0 mean user, 1 means workgroup notification.
* UserID - Will be 0 for workgroup notification.
*
*
* Modification History
* 05/30/2012 CR 53200 JPB	Created
* 03/06/2013 WI 90858 JBS	Update table to 2.0 release. Change schema Name.
*							Rename Column: CustomerKey to OrganizationKey,
*							factNotificationID to factNotificationKey,
*							LockboxKey to ClientAccountKey.
*							Rename FK constraints to match schema and column name changes.
*							Add factNotificationKey to Clustered Index. 
*							Removed Constraints: DF_factNotifications_CreationDate DEFAULT(GETDATE()),
*							DF_factNotifications_ModificationDate DEFAULT(GETDATE()),
*							DF_factNotifications_CreatedBy DEFAULT(SUSER_SNAME()),
*							DF_factNotifications_ModifiedBy DEFAULT(SUSER_SNAME()).
*							Added Column: IsDeleted
* 08/22/2014 WI 139597 JPB	Added UserNotification, UserID, FK to Users and 
*							Changed NotificationSourceKey to SMALLINT.
*							IDX_factNotifications_UserIDNotificationMessageGroup.
*							Added UserNotification to 
*							IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification.
* 02/11/2015 WI 189332 JBS	Added indexes for duplicate detect logic
*							IDX_factNotifications_BankKeySourceNotificationID
*							IDX_factNotifications_NotificationMessageGroupBankKey
* 06/01/2015 WI 216416 JBS  Add index IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime. Per 217727
* 02/02/2018 PT 154233497 JPB	Added MessageWorkgroups
******************************************************************************/';
	END
END
--WFSScriptProcessorCREnd