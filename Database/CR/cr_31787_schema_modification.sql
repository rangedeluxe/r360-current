--WFSScriptProcessorPrint CR 31787
--WFSScriptProcessorPrint Adding IMSDocumentType if it does not exist
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM [information_schema].[columns] WHERE table_name = 'dimDocumentTypes' and column_name = 'IMSDocumentType' )
BEGIN
	
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimDocumentTypes_factDocuments' )
		ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimDocumentTypes_factDocuments

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimDocumentTypes' AND CONSTRAINT_NAME='PK_dimDocumentTypes' )
		ALTER TABLE OLTA.dimDocumentTypes DROP CONSTRAINT PK_dimDocumentTypes
	
	PRINT 'Rebuilding OLTA.dimDocumentTypes'
	EXEC sp_rename 'OLTA.dimDocumentTypes', 'OLDdimDocumentTypes'	
	
	CREATE TABLE OLTA.dimDocumentTypes
	(
		DocumentTypeKey int IDENTITY(0,1) NOT NULL 
			CONSTRAINT PK_dimDocumentTypes PRIMARY KEY CLUSTERED,
		FileDescriptor varchar(30) NOT NULL,
		DocumentTypeDescription varchar(16) NOT NULL,
		IMSDocumentType varchar(30) NOT NULL, 
		MostRecent bit NOT NULL,
		LoadDate datetime NOT NULL
	)	

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', '', default, default) )
		EXEC sys.sp_dropextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE', @level1name = dimDocumentTypes

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Document Types dimension is a static data dimension for document types. 
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2010 CR 31787 JPB	Added IMSDocumentType, added IDX_dimDocumentTypes_FileDescriptor
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE', @level1name = dimDocumentTypes
	
	CREATE INDEX IDX_dimDocumentTypes_FileDescriptor ON OLTA.dimDocumentTypes (FileDescriptor)

	SET IDENTITY_INSERT OLTA.dimDocumentTypes ON
	
	INSERT INTO OLTA.dimDocumentTypes
	(
		DocumentTypeKey,
		FileDescriptor,
		DocumentTypeDescription,
		IMSDocumentType,
		MostRecent,
		LoadDate
	)
	SELECT	DocumentTypeKey,
			FileDescriptor,
			DocumentTypeDescription,
			'Invoice',
			MostRecent,
			LoadDate
	FROM	OLTA.OLDdimDocumentTypes

	SET IDENTITY_INSERT OLTA.dimDocumentTypes OFF
	
	ALTER TABLE OLTA.factDocuments ADD
		CONSTRAINT FK_dimDocumentTypes_factDocuments FOREIGN KEY(DocumentTypeKey) REFERENCES OLTA.dimDocumentTypes(DocumentTypeKey)
	PRINT 'Dropping old OLTA.dimDocumentTypes'
	DROP TABLE OLTA.OLDdimDocumentTypes

END
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Updating OLTA.dimDocumentTypes.IMSDocumentType with data from dbo.DocumentTypes.IMSDocumentType
--WFSScriptProcessorCRBegin
IF EXISTS( SELECT 1 FROM [information_schema].[columns] WHERE table_name = 'dimDocumentTypes' and column_name = 'IMSDocumentType' )
BEGIN
	UPDATE OLTADocs
	SET OLTADocs.IMSDocumentType = DBODocs.IMSDocumentType
	FROM OLTA.dimDocumentTypes OLTADocs
		INNER JOIN dbo.DocumentTypes DBODocs ON DBODocs.FileDescriptor = OLTADocs.FileDescriptor
END
--WFSScriptProcessorCREnd
