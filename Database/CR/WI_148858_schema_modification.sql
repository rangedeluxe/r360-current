--WFSScriptProcessorPrint WI 148858
--WFSScriptProcessorPrint Update RecHubData.dimBatchDataSetupFields if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimBatchDataSetupFields' AND COLUMN_NAME='ImportTypeKey' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_factBatchData_dimBatchDataSetupFields' )
		ALTER TABLE RecHubData.factBatchData DROP CONSTRAINT FK_factBatchData_dimBatchDataSetupFields;

	RAISERROR('Dropping dimBatchSources contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimBatchDataSetupFields' AND CONSTRAINT_NAME='PK_dimBatchDataSetupFields' )
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT PK_dimBatchDataSetupFields;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchDataSetupFields_CreatedBy')
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT DF_dimBatchDataSetupFields_CreatedBy;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchDataSetupFields_CreationDate')
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT DF_dimBatchDataSetupFields_CreationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchDataSetupFields_ModifiedBy')
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT DF_dimBatchDataSetupFields_ModifiedBy;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchDataSetupFields_ModificationDate')
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT DF_dimBatchDataSetupFields_ModificationDate;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubData' AND CONSTRAINT_NAME = 'CK_dimBatchDataSetupFields_DataType')
		ALTER TABLE RecHubData.dimBatchDataSetupFields DROP CONSTRAINT CK_dimBatchDataSetupFields_DataType;

	RAISERROR('Rebuilding RecHubData.dimBatchDataSetupFields',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.dimBatchDataSetupFields', 'Old_dimBatchDataSetupFields';

	CREATE TABLE RecHubData.dimBatchDataSetupFields
	(
		BatchDataSetupFieldKey INT NOT NULL 
			CONSTRAINT PK_dimBatchDataSetupFields PRIMARY KEY CLUSTERED,
		ImportTypeKey TINYINT NOT NULL,
		DataType TINYINT NOT NULL
			CONSTRAINT CK_dimBatchDataSetupFields_DataType CHECK (DataType IN (1,3,4,6,7,11)),
		CreatedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_dimBatchDataSetupFields_CreatedBy DEFAULT SUSER_SNAME(),
		CreationDate DATETIME NOT NULL
			CONSTRAINT DF_dimBatchDataSetupFields_CreationDate DEFAULT GETDATE(),
		ModifiedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_dimBatchDataSetupFields_ModifiedBy DEFAULT SUSER_SNAME(),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimBatchDataSetupFields_ModificationDate DEFAULT GETDATE(),
		Keyword VARCHAR(32) NOT NULL,
		[Description] VARCHAR(256) NOT NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimBatchDataSetupFields', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimBatchDataSetupFields';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2011
*
* Purpose: Stores Item Data trail keywords referenced by factItemData. 
*
* Check Constraint definitions:
* DataType
*	1: Alphanumeric
*	3: Integer
*	4: Bit
*	6: Float
*	7: Money
* 	11: Date/time
*
* Modification History
* 03/07/2011 CR 33211 JPB	Created
* 03/01/2013 WI 89963 JBS	Update table to 2.0 release. Change Schema Name	
* 06/20/2014 WI 148858 JPB	Replaced BatchSourceKey/FK with ImportTypeKey/FK.
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimBatchDataSetupFields;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
	INSERT INTO RecHubData.dimBatchDataSetupFields 
	(
		BatchDataSetupFieldKey,
		ImportTypeKey,
		DataType,
		CreatedBy,
		CreationDate,
		ModifiedBy,
		ModificationDate,
		Keyword,
		[Description]	
	) 
	SELECT	
		RecHubData.Old_dimBatchDataSetupFields.BatchDataSetupFieldKey,
		RecHubData.dimBatchSources.ImportTypeKey,
		RecHubData.Old_dimBatchDataSetupFields.DataType,
		RecHubData.Old_dimBatchDataSetupFields.CreatedBy,
		RecHubData.Old_dimBatchDataSetupFields.CreationDate,
		RecHubData.Old_dimBatchDataSetupFields.ModifiedBy,
		RecHubData.Old_dimBatchDataSetupFields.ModificationDate,
		RecHubData.Old_dimBatchDataSetupFields.Keyword,
		RecHubData.Old_dimBatchDataSetupFields.[Description]	
	FROM 	
		RecHubData.Old_dimBatchDataSetupFields
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.Old_dimBatchDataSetupFields.BatchSourceKey;

	RAISERROR('Creating index IDX_dimBatchDataSetupFields_Keyword_ImportTypeKey',10,1) WITH NOWAIT;
	CREATE UNIQUE INDEX IDX_dimBatchDataSetupFields_Keyword_ImportTypeKey ON RecHubData.dimBatchDataSetupFields (Keyword,ImportTypeKey);

	/* 
		Special note: Normally the FKs would be added back here. However, since the data type is being changed, all the tables that have a FK need 
		to be updated as well. Therefore, the scripts that alter the FK tables will add the FK.

		This script will be applied BEFORE the changes to the other tables are applied.
	*/

	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.Old_dimBatchDataSetupFields') IS NOT NULL
		DROP TABLE RecHubData.Old_dimBatchDataSetupFields;
END
ELSE
	RAISERROR('WI 148858 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
