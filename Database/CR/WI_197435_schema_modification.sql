--WFSScriptProcessorPrint WI 197435
--WFSScriptProcessorPrint Removing RecHubData.SessionEmulation and related stored procedures if necessary
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'RecHubUser' AND TABLE_NAME = 'SessionEmulation')
BEGIN
	IF OBJECT_ID('RecHubUser.usp_SessionEmulation_Get_ByEmulationID') IS NOT NULL
	BEGIN
		RAISERROR('Dropping RecHubUser.usp_SessionEmulation_Get_ByEmulationID',10,1) WITH NOWAIT;
		DROP PROCEDURE RecHubUser.usp_SessionEmulation_Get_ByEmulationID
	END

	IF OBJECT_ID('RecHubUser.usp_SessionEmulation_Ins_IsBillable') IS NOT NULL
	BEGIN
		RAISERROR('Dropping RecHubUser.usp_SessionEmulation_Ins_IsBillable',10,1) WITH NOWAIT;
		DROP PROCEDURE RecHubUser.usp_SessionEmulation_Ins_IsBillable
	END
 	
	RAISERROR('Dropping RecHubUser.SessionEmulation',10,1) WITH NOWAIT;
	DROP TABLE RecHubUser.SessionEmulation;
END
--WFSScriptProcessorCREnd