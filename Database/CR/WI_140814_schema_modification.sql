--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 140814
--WFSScriptProcessorPrint Adding MessageSubject to RecHubAlert.Events table name if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubAlert' AND TABLE_NAME = 'Events' AND COLUMN_NAME = 'MessageSubject')
BEGIN
	RAISERROR('ReBuilding Table RecHubAlert.Events.',10,1) WITH NOWAIT
			
	RAISERROR('Dropping Events contraints.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND CONSTRAINT_NAME='PK_Events' )
		ALTER TABLE RecHubAlert.[Events] DROP CONSTRAINT PK_Events;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubAlert' AND CONSTRAINT_NAME = 'CK_Events_EventType')
		ALTER TABLE RecHubAlert.[Events] DROP CONSTRAINT CK_Events_EventType;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubAlert' AND CONSTRAINT_NAME = 'CK_Events_EventLevel')
		ALTER TABLE RecHubAlert.[Events] DROP CONSTRAINT CK_Events_EventLevel;
		
	RAISERROR('Building RecHubAlert.Events.',10,1) WITH NOWAIT
	EXEC sp_rename 'RecHubAlert.[Events]', 'OLD_Events'

	CREATE TABLE RecHubAlert.[Events]
	(
		EventID				SMALLINT NOT NULL
			CONSTRAINT PK_Events PRIMARY KEY CLUSTERED,
		EventName			VARCHAR(30) NOT NULL,
		EventType			TINYINT NOT NULL
			CONSTRAINT CK_Events_EventType CHECK (EventType IN (0,1)),
		EventLevel			TINYINT NOT NULL
			CONSTRAINT CK_Events_EventLevel CHECK (EventLevel IN (1,2,3)),
		IsActive			BIT NOT NULL,
		MessageDefault		VARCHAR(1028) NOT NULL,
		MessageSubject		VARCHAR(64) NULL,
		EventSchema			VARCHAR(128) NULL,
		EventTable			VARCHAR(128) NULL,
		EventColumn			VARCHAR(128) NULL,
		EventOperators		VARCHAR(64) NULL,
		ModificationDate	DATETIME NOT NULL,
		ModifiedBy			VARCHAR(128) NOT NULL
	);		

	RAISERROR('Updating RecHubAlert.Events table properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAlert', 'TABLE', 'Events', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubAlert',
			@level1type = N'TABLE',
			@level1name = N'Events';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Stores Events for R360 Alerts
*	
* Check Constraint definitions:
* EventLevel
*	1: Site
*	2: Bank
*	3: ClientAccount	   
*
* Modification History
* 05/03/2013 WI 99837 JBS	Created.  Adding EventLevel column
* 05/08/2014 WI 140814 JPB	Added EventSubject and Modification columns.
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubAlert,
	@level1type = N'TABLE',@level1name = Events;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

	INSERT INTO RecHubAlert.[Events]
	(
		EventID,
		EventName,
		EventType,
		EventLevel,
		IsActive,
		MessageDefault,
		EventSchema,
		EventTable,
		EventColumn,
		EventOperators,
		ModificationDate,
		ModifiedBy
	)
	SELECT	
		EventID,
		EventName,
		EventType,
		EventLevel,
		IsActive,
		MessageDefault,
		EventSchema,
		EventTable,
		EventColumn,
		EventOperators,
		GETDATE(),
		SUSER_SNAME()
	FROM	
		RecHubAlert.OLD_Events;

	IF OBJECT_ID('RecHubAlert.OLD_Events') IS NOT NULL
		DROP TABLE RecHubAlert.OLD_Events;
END
ELSE
	RAISERROR('WI 140814 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
