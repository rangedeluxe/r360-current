--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #137726225
--WFSScriptProcessorPrint Remove deprecated DIT objects if necessary.
--WFSScriptProcessorCRBegin
--SSIS Packages
DECLARE @PackageExists INT,
		@PackageFolderID UNIQUEIDENTIFIER

;WITH PackageFolder(folderid,parentfolderid,FullFolderName) AS
(
	SELECT 
		folderid,
		parentfolderid,
		CAST(foldername AS VARCHAR(1024)) AS FullFolderName
	FROM 
		[msdb].[dbo].[sysssispackagefolders] 
	WHERE 
		parentfolderid = '00000000-0000-0000-0000-000000000000'
	UNION ALL
	SELECT 
		SubFolders.folderid,
		SubFolders.parentfolderid,
		CAST((PackageFolder.FullFolderName+'\'+SubFolders.foldername) AS VARCHAR(1024)) AS FullFolderName
	FROM  
		[msdb].[dbo].[sysssispackagefolders] AS SubFolders
		INNER JOIN PackageFolder ON SubFolders.parentfolderid = PackageFolder.folderid
) 
SELECT TOP(1)
	@PackageFolderID = [msdb].[dbo].[sysssispackages].folderid
FROM 
	[msdb].[dbo].[sysssispackages]
	INNER JOIN PackageFolder ON PackageFolder.folderid = [msdb].[dbo].[sysssispackages].folderid
WHERE 
	FullFolderName LIKE 'WFSPackages\' + DB_NAME() + '%'
	AND [name] LIKE 'DataImportIntegrationServices_BatchData_Extract_%'
ORDER by FullFolderName;

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_Batches' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_Batches from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_Batches', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_DocumentItemData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_DocumentItemData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_DocumentItemData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_DocumentRemittanceData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_Documents' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_Documents from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_Documents', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_GhostDocumentItemData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_GhostDocumentItemData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_GhostDocumentItemData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_GhostDocumentRemittanceData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_GhostDocumentRemittanceData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_GhostDocumentRemittanceData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_GhostDocuments' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_GhostDocuments from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_GhostDocuments', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_PaymentItemData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_PaymentItemData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_PaymentItemData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_PaymentRawData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_PaymentRawData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_PaymentRawData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_PaymentRemittanceData' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_PaymentRemittanceData from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_PaymentRemittanceData', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_Payments' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_Payments from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_Payments', @folderid = @PackageFolderID;
END

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_Extract_Transactions' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_Extract_Transactions from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_Extract_Transactions', @folderid = @PackageFolderID;
END
--WFSScriptProcessorCREnd
