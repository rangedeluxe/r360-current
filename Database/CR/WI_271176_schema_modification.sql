--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271176
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factNotifications.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factNotifications')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factNotifications'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_NotificationDateKey')
			DROP INDEX IDX_factNotifications_NotificationDateKey ON RecHubData.factNotifications;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification')
			DROP INDEX IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification ON RecHubData.factNotifications;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_UserIDNotificationMessageGroupUserNotification')
			DROP INDEX IDX_factNotifications_UserIDNotificationMessageGroupUserNotification ON RecHubData.factNotifications;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_BankKeySourceNotificationID')
			DROP INDEX IDX_factNotifications_BankKeySourceNotificationID ON RecHubData.factNotifications;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_NotificationMessageGroupBankKey')
			DROP INDEX IDX_factNotifications_NotificationMessageGroupBankKey ON RecHubData.factNotifications;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime')
			DROP INDEX IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime ON RecHubData.factNotifications;


		/****** Object:  Index PK_PK_factNotifications    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factNotifications')
			ALTER TABLE RecHubData.factNotifications DROP CONSTRAINT PK_factNotifications;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factNotifications ADD
			CONSTRAINT PK_factNotifications PRIMARY KEY NONCLUSTERED (factNotificationKey,NotificationDateKey) $(OnNotificationPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factNotifications_NotificationDateKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factNotifications_NotificationDateKey ON RecHubData.factNotifications
		(
			NotificationDateKey,
			factNotificationKey
		) $(OnNotificationPartition);

		RAISERROR('Rebuilding index IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification ON RecHubData.factNotifications
		(
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			NotificationMessageGroup,
			UserNotification
		) $(OnNotificationPartition);

		RAISERROR('Rebuilding index IDX_factNotifications_UserIDNotificationMessageGroupUserNotification',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factNotifications_UserIDNotificationMessageGroupUserNotification ON RecHubData.factNotifications
		(
			UserID,
			NotificationMessageGroup,
			UserNotification
		) $(OnNotificationPartition);

		RAISERROR('Rebuilding index IDX_factNotifications_BankKeySourceNotificationID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factNotifications_BankKeySourceNotificationID ON RecHubData.factNotifications
		(
			BankKey,
			SourceNotificationID
		)
		INCLUDE 
		(
			factNotificationKey,
			NotificationDateKey
		) $(OnNotificationPartition);

		RAISERROR('Rebuilding index IDX_factNotifications_NotificationMessageGroupBankKey',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factNotifications_NotificationMessageGroupBankKey ON RecHubData.factNotifications 
		(
			NotificationMessageGroup,
			BankKey
		)
		INCLUDE 
		(
			factNotificationKey,
			NotificationDateKey
		) $(OnNotificationPartition);

		RAISERROR('Rebuilding index IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime ON RecHubData.factNotifications 
		(
			IsDeleted, 
			OrganizationKey,
			UserID, 
			NotificationMessagePart,
			ClientAccountKey, 
			NotificationDateTime
		) 
		INCLUDE 
		(
			factNotificationKey, 
			NotificationMessageGroup, 
			NotificationFileCount, 
			SourceNotificationID, 
			MessageText
		);
	
	END
	ELSE
		RAISERROR('WI 271176 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271176 RecHubData.factNotifications table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd