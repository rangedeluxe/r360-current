--WFSScriptProcessorPrint CR 32359
--WFSScriptProcessorPrint Populating OLTA.dimRemitters will all known RT/Account/RemitterName combinations.
--WFSScriptProcessorCRBegin
DECLARE @xRemitters XML,
		@LoadDate DATETIME

SELECT @xRemitters = 
	(
		SELECT 	
		(
			SELECT 	DISTINCT RTRIM(Checks.RT) AS RT,
					RTRIM(Checks.Account) AS Account,
					RTRIM(Checks.RemitterName) AS RemitterName
			FROM	dbo.Checks Checks
			FOR XML AUTO, TYPE, ROOT('Transaction')
		)
		FOR XML PATH('Batch'), TYPE, ROOT('Batches')
	)

SELECT @LoadDate = GETDATE()

EXEC OLTA.usp_LoadRemittersDimension @xRemitters, @LoadDate
--WFSScriptProcessorCREnd
