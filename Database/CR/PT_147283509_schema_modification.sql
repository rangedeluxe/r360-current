--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #147283509
--WFSScriptProcessorPrint Drop RecHubException.usp_Complete_Transaction if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('RecHubException.usp_Complete_Transaction') IS NOT NULL
BEGIN
	RAISERROR('Dropping RecHubException.usp_Complete_Transaction.',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubException.usp_Complete_Transaction
END
--WFSScriptProcessorCREnd

