--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271180
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factTransactionSummary.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factTransactionSummary')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factTransactionSummary'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey')
			DROP INDEX IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID')
			DROP INDEX IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_BankKey')
			DROP INDEX IDX_factTransactionSummary_BankKey ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_ClientAccountKey')
			DROP INDEX IDX_factTransactionSummary_ClientAccountKey ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_OrganizationKey')
			DROP INDEX IDX_factTransactionSummary_OrganizationKey ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_ImmutableDateKey')
			DROP INDEX IDX_factTransactionSummary_ImmutableDateKey ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence')
			DROP INDEX IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID')
			DROP INDEX IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount')
			DROP INDEX IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount')
			DROP INDEX IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary;

		/****** Object:  Index PK_factDocuments    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factTransactionSummary')
			ALTER TABLE RecHubData.factTransactionSummary DROP CONSTRAINT PK_factTransactionSummary;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factTransactionSummary ADD
			CONSTRAINT PK_factTransactionSummary PRIMARY KEY NONCLUSTERED (factTransactionSummaryKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey ON RecHubData.factTransactionSummary 
		(
			DepositDateKey,
			factTransactionSummaryKey
		) $(OnDataPartition);


		RAISERROR('Rebuilding index IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factTransactionSummary 
		(
			DepositDateKey,
			ClientAccountKey,
			BatchID
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factTransactionSummary_BankKey ON RecHubData.factTransactionSummary (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_ClientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factTransactionSummary_ClientAccountKey ON RecHubData.factTransactionSummary (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factTransactionSummary_OrganizationKey ON RecHubData.factTransactionSummary (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_ImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factTransactionSummary_ImmutableDateKey ON RecHubData.factTransactionSummary (ImmutableDateKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence ON RecHubData.factTransactionSummary
		(
			ClientAccountKey ASC,
			DepositDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			DepositStatus ASC,
			BankKey ASC,
			OrganizationKey ASC,
			ImmutableDateKey ASC,
			TransactionID ASC,
			TxnSequence ASC
		)
		INCLUDE 
		( 
			CheckCount,
			DocumentCount,
			ScannedCheckCount,
			StubCount,
			OMRCount,
			CheckTotal
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID ON RecHubData.factTransactionSummary
		(
			BankKey ASC,
			OrganizationKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			DepositDateKey ASC,
			BatchID ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary 
		(
			IsDeleted,
			DepositDateKey,
			SourceBatchID,
			BatchNumber,
			DepositStatus,
			OMRCount
		)
		INCLUDE 
		(
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			ImmutableDateKey,
			BatchID,
			BatchSourceKey,
			BatchPaymentTypeKey,
			TransactionID,
			TxnSequence,
			CheckCount,
			StubCount,
			DocumentCount
		) $(OnDataPartition);

RAISERROR('Rebuilding index IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID',10,1) WITH NOWAIT;
CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary 
(
	IsDeleted,
	BatchSourceKey,
	DepositDateKey,
	SourceBatchID,
	BatchNumber,
	DepositStatus,
	OMRCount
)
INCLUDE 
(
	BankKey,
	OrganizationKey,
	ClientAccountKey,
	ImmutableDateKey,
	BatchID,
	BatchPaymentTypeKey,
	TransactionID,
	TxnSequence,
	CheckCount,
	StubCount,
	DocumentCount
) $(OnDataPartition);



	END
	ELSE
		RAISERROR('WI 271180 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271180 RecHubData.factTransactionSummary table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd