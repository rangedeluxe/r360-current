--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint #141048573
--WFSScriptProcessorPrint Adding select permission for RecHubExtractWizard_User if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT * FROM sys.database_permissions
		INNER JOIN sys.database_principals ON sys.database_principals.principal_id = sys.database_permissions.grantee_principal_id
		WHERE major_id = OBJECT_ID('RecHubData.dimWorkgroupDataEntryColumns') )
BEGIN
	RAISERROR('Adding SELECT permisson to RecHubData.dimWorkgroupDataEntryColumns for RecHubExtractWizard_User',10,1) WITH NOWAIT;
	GRANT SELECT ON RecHubData.dimWorkgroupDataEntryColumns TO RecHubExtractWizard_User;
END
ELSE 
	RAISERROR('PT 141048573 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
