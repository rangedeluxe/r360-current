--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 127604197
--WFSScriptProcessorPrint Delete SSIS Package DataImportIntegrationServices_BatchData_LoadStaging if necessary.
--WFSScriptProcessorCRBegin
DECLARE @PackageExists INT,
		@PackageFolderID UNIQUEIDENTIFIER

;WITH PackageFolder(folderid,parentfolderid,FullFolderName) AS
(
	SELECT 
		folderid,
		parentfolderid,
		CAST(foldername AS VARCHAR(1024)) AS FullFolderName
	FROM 
		[msdb].[dbo].[sysssispackagefolders] 
	WHERE 
		parentfolderid = '00000000-0000-0000-0000-000000000000'
	UNION ALL
	SELECT 
		SubFolders.folderid,
		SubFolders.parentfolderid,
		CAST((PackageFolder.FullFolderName+'\'+SubFolders.foldername) AS VARCHAR(1024)) AS FullFolderName
	FROM  
		[msdb].[dbo].[sysssispackagefolders] AS SubFolders
		INNER JOIN PackageFolder ON SubFolders.parentfolderid = PackageFolder.folderid
) 
SELECT TOP(1)
	@PackageFolderID = [msdb].[dbo].[sysssispackages].folderid
FROM 
	[msdb].[dbo].[sysssispackages]
	INNER JOIN PackageFolder ON PackageFolder.folderid = [msdb].[dbo].[sysssispackages].folderid
WHERE 
	FullFolderName like 'WFSPackages\' + DB_NAME() + '%'
	AND [name] = 'DataImportIntegrationServices_BatchData_LoadStaging'
ORDER by FullFolderName;

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'DataImportIntegrationServices_BatchData_LoadStaging' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing DataImportIntegrationServices_BatchData_LoadStaging from SSIS package.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'DataImportIntegrationServices_BatchData_LoadStaging', @folderid = @PackageFolderID;
END
--WFSScriptProcessorCREnd
