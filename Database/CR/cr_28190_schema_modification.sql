--WFSSCRIPTPROCESSORSCRNAME Updating OLTA.factChecks.GlobalBatchID
--WFSSCRIPTPROCESSORSCRBEGIN
IF (SELECT syscolumns.isnullable FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factChecks' AND syscolumns.name = 'GlobalBatchID' AND sysobjects.xtype='U') = 0
	ALTER TABLE OLTA.factChecks ALTER COLUMN GlobalBatchID INT NULL
--WFSSCRIPTPROCESSORSCRCREND