--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 146868813
--WFSScriptProcessorPrint Removing SSISLoggingDatabaseMaintenance package if necessary.
--WFSScriptProcessorCRBegin
--SSIS Packages
DECLARE @PackageExists INT,
		@PackageFolderID UNIQUEIDENTIFIER

;WITH PackageFolder(folderid,parentfolderid,FullFolderName) AS
(
	SELECT 
		folderid,
		parentfolderid,
		CAST(foldername AS VARCHAR(1024)) AS FullFolderName
	FROM 
		[msdb].[dbo].[sysssispackagefolders] 
	WHERE 
		parentfolderid = '00000000-0000-0000-0000-000000000000'
	UNION ALL
	SELECT 
		SubFolders.folderid,
		SubFolders.parentfolderid,
		CAST((PackageFolder.FullFolderName+'\'+SubFolders.foldername) AS VARCHAR(1024)) AS FullFolderName
	FROM  
		[msdb].[dbo].[sysssispackagefolders] AS SubFolders
		INNER JOIN PackageFolder ON SubFolders.parentfolderid = PackageFolder.folderid
) 
SELECT TOP(1)
	@PackageFolderID = [msdb].[dbo].[sysssispackages].folderid
FROM 
	[msdb].[dbo].[sysssispackages]
	INNER JOIN PackageFolder ON PackageFolder.folderid = [msdb].[dbo].[sysssispackages].folderid
WHERE 
	FullFolderName LIKE 'WFSPackages\' + DB_NAME() + '%'
	AND [name] = 'SSISLoggingDatabaseMaintenance'
ORDER by FullFolderName;

IF EXISTS( SELECT 1 FROM [msdb].[dbo].[sysssispackages] WHERE [name] = 'SSISLoggingDatabaseMaintenance' AND [folderid] = @PackageFolderID )
BEGIN
	RAISERROR('Removing SSISLoggingDatabaseMaintenance from SSIS packages.',10,1) WITH NOWAIT;
	
	EXEC [msdb].[dbo].[sp_ssis_deletepackage] @name = 'SSISLoggingDatabaseMaintenance', @folderid = @PackageFolderID;
END
ELSE
	RAISERROR('SSISLoggingDatabaseMaintenance SSIS package already removed.',10,1) WITH NOWAIT;

--SQL Agent job
IF EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Updating SSISLoggingDatabaseMaintenance job.',10,1) WITH NOWAIT;
	DELETE 
		msdb.dbo.sysjobsteps
	FROM 
		msdb.dbo.sysjobsteps
		INNER JOIN msdb.dbo.sysjobs ON msdb.dbo.sysjobs.job_id = msdb.dbo.sysjobsteps.job_id
	WHERE 
		msdb.dbo.sysjobs.name = '$(SSISJobName)';
END	
ELSE 
	RAISERROR('SSISLoggingDatabaseMaintenance job steps do not exist.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd