﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 234253
--WFSScriptProcessorPrint Adding indexes IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted') 
	BEGIN
		RAISERROR('WI 219268 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted ON RecHubData.factStubs
(
	DepositDateKey ASC,
	BatchID ASC,
	TransactionID ASC,
	TxnSequence ASC,
	IsDeleted ASC
)
INCLUDE
(
	BatchSequence,
	StubSequence,
	factStubKey
) $(OnDataPartition);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factStubs', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factStubs';	

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factStubs,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Stub.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28194 JPB	GlobalBatchID is now nullable.
* 02/12/2010 CR 28999 JPB	Added ModificationDate.
* 04/05/2010 CR 29302 JPB	Renamed Account to AccountNumber.
* 01/13/2011 CR 32299 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49278 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49553 JPB	Added BatchSiteCode.
* 03/27/2012 CR 51545 JPB	Added BatchPaymentTypeKey.
* 07/11/2012 CR 53626 JPB	Added BatchCueID.
* 07/16/2012 CR 54122 JPB	Added BatchNumber.
* 03/06/2013 WI 90862 JBS	Update table to 2.0 release. Change schema name.
*							Add Column: factStubKey, IsDeleted.
*							Remove Column: GlobalBatchID, GlobalStubID.
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factStubKey to Clustered Index. 
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factStubs_BatchPaymentTypeKey DEFAULT(0),
*							DF_factStubs_BatchCueID DEFAULT(-1), DF_factStubs_ModificationDate DEFAULTGETDATE().
*							Forward Patch:  WI 87216
* 05/29/2014 WI 135322 JPB	Changed BatchID to BIGINT.
* 05/29/2014 WI 144742 JPB	Added SourceBatchID.
* 05/29/2014 WI 144743 JPB	Changed SourceBatchKey to SMALLINT.
* 08/07/2014 WI 157623 JPB	Changed DocumentBatchSequence to NULL.
* 09/04/2015 WI 234253 MGE	Added index for bug 233291 - timeout at BOE
******************************************************************************/';
	END
--WFSScriptProcessorCREnd