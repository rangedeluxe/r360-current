--WFSScriptProcessorPrint CR 34349
--WFSScriptProcessorPrint Update factBatchSummary IDX_factBatchSummary_DepositDateKey if necessary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]') AND name = N'IDX_factBatchSummary_DepositDateKey')
BEGIN
	/* check for last known change to table. If it is not there, do not continue with the change. */
	IF EXISTS (	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factBatchSummary' AND COLUMN_NAME = 'BatchSourceKey'	)
	BEGIN
		DROP INDEX [IDX_factBatchSummary_DepositDateKey] ON [OLTA].[factBatchSummary]
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factBatchSummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factBatchSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factBatchSummary

		CREATE CLUSTERED INDEX IDX_factBatchSummary_DepositDateKey ON OLTA.factBatchSummary (DepositDateKey,LockboxKey, BatchID)-- $(OnPartition)
	END
	ELSE
	BEGIN
		RAISERROR('Missing CR 32284',10,1) WITH NOWAIT
	END
END
--WFSScriptProcessorCREnd
