--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271163
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factBatchExtracts.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factBatchExtracts')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factBatchExtracts'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchExtracts_DepositDatefactBatchExtractKey')
			DROP INDEX IDX_factBatchExtracts_DepositDatefactBatchExtractKey ON RecHubData.factBatchExtracts;

		/****** Object:  Index PK_factBatchExtracts    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factBatchExtracts')
			ALTER TABLE RecHubData.factBatchExtracts DROP CONSTRAINT PK_factBatchExtracts;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factBatchExtracts ADD 
			CONSTRAINT PK_factBatchExtracts PRIMARY KEY NONCLUSTERED (factBatchExtractKey,DepositDateKey) 
			 $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factBatchExtracts_DepositDatefactBatchExtractKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factBatchExtracts_DepositDatefactBatchExtractKey ON RecHubData.factBatchExtracts 
		(
			DepositDateKey,
			factBatchExtractKey
		) $(OnDataPartition);
	END
	ELSE
		RAISERROR('WI 271163 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271163 RecHubData.FactBatchExtracts table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
