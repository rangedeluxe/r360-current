﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 154233497
--WFSScriptProcessorPrint Adding RecHubData.factNotificationFiles.MessageWorkgroups if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factNotificationFiles' AND COLUMN_NAME = 'FileSize' )
	RAISERROR('PT 139475715 must be applied first.',160,1) WITH NOWAIT;
ELSE	
BEGIN
	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factNotificationFiles' AND COLUMN_NAME = 'MessageWorkgroups' )
		RAISERROR('PT has already been applied to the database.',10,1) WITH NOWAIT;
	ELSE
	BEGIN
		RAISERROR('Adding RecHubData.factNotificationFiles.MessageWorkgroups',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factNotificationFiles ADD MessageWorkgroups UNIQUEIDENTIFIER NULL;
		
		IF EXISTS(SELECT 1 FROM sys.fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factNotificationFiles', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factNotificationFiles';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factNotificationFiles,
		@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Column Information
* UserNotification - Used to determine if record is a "workgroup" or user
*	notification. 0 mean user, 1 means workgroup notification.
* UserID - Will be 0 for workgroup notification.
*
*
* Modification History
* 05/30/2012 CR 53529 JPB	Created
* 03/06/2013 WI 90821 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factNotificationFileKey, IsDeleted.
*							Rename Column: CustomerKey to OrganizationKey,
*							factNotificationFileID to factNotificationFileKey
*							LockboxKey to ClientAccountKey, LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factNotificationFileKey to Clustered Index. 
*							Removed Constraints: DF_factNotificationFiles_CreationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_ModificationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_CreatedBy DEFAULT(SUSER_SNAME()),
*							DF_factNotificationFiles_ModifiedBy DEFAULT(SUSER_SNAME()).
*							Added Column: IsDeleted
* 08/24/2014 WI 160437 JPB	Added UserNotification, UserID, FK to Users.
*							Changed NotificationSourceKey to SMALLINT.
* 03/06/2017 PT 139475715 JBS	Add Column FileSize.
* 02/02/2018 PT 154233497 JPB	Added MessageWorkgroups
******************************************************************************/';
	END
END
--WFSScriptProcessorCREnd
