--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 152545607
--WFSScriptProcessorPrint Drop integraPAYStaging.Organizations if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('integraPAYStaging.Organizations') IS NOT NULL
BEGIN
	RAISERROR('Dropping integraPAYStaging.Organizations.',10,1) WITH NOWAIT;
	DROP TABLE integraPAYStaging.Organizations;
END
--WFSScriptProcessorCREnd
