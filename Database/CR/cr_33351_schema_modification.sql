--WFSScriptProcessorPrint CR 33351
--WFSScriptProcessorPrint Adding IDX_factDocuments_DepositDateProcessingDateBankLockboxKeyBatchID to OLTA.factDocuments if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factDocuments]') AND name = N'IDX_factDocuments_DepositDateProcessingDateBankLockboxKeyBatchID')
	CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateProcessingDateBankLockboxKeyBatchID ON OLTA.factDocuments
	(
		[DepositDateKey] ASC,
		[ProcessingDateKey] ASC,
		[BankKey] ASC,
		[LockboxKey] ASC,
		[BatchID] ASC
	) $(OnPartition)
--WFSScriptProcessorCREnd