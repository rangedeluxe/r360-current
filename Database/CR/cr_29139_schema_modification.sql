--WFSScriptProcessorPrint CR 29139
--WFSScriptPRocessorStaticDataCreateBegin
IF NOT EXISTS ( SELECT * FROM [Events] WHERE EventName = 'Service Broker Error Report' )
    INSERT [dbo].[Events]( EventName, [Initiator], EventIsActive, MessageDefault, OnlineViewable, AllowExpression, ProcessingDateRequired, OnlineModifiable ) 
		VALUES ( 'Service Broker Error Report', NULL, 1, '<Message>[Message]', 0, 0, 0, 0 )
--WFSScriptPRocessorStaticDataCreateEnd
--WFSScriptPRocessorStaticDataCreateBegin
IF NOT EXISTS ( SELECT * FROM [Events] WHERE EventName = 'Service Broker No Error Report' )
    INSERT [dbo].[Events]( EventName, [Initiator], EventIsActive, MessageDefault, OnlineViewable, AllowExpression, ProcessingDateRequired, OnlineModifiable ) 
		VALUES ( 'Service Broker No Error Report', NULL, 1, '<Message>[Message]', 0, 0, 0, 0 )
--WFSScriptPRocessorStaticDataCreateEnd