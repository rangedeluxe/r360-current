--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 159804525 
--WFSScriptProcessorPrint Updating Notification Service setting in SSIS Config Database if necessary
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM dbo.SSISConfigurations WHERE ConfigurationFilter = 'CommonConfigurations' AND PackagePath = '\Package.Variables[User::NotificationServiceURL].Properties[Value]' AND ConfiguredValue LIKE '%wfsnotificationservice/notificationservice.svc%')
BEGIN
	RAISERROR('Updating Notification Service URL',10,1) WITH NOWAIT;
	UPDATE 
		dbo.SSISConfigurations 
	SET ConfiguredValue = SUBSTRING(ConfiguredValue,1,PATINDEX('%wfs%',ConfiguredValue)-1) + SUBSTRING(ConfiguredValue,PATINDEX('%wfs%',ConfiguredValue)+3,LEN(ConfiguredValue)-(1-PATINDEX('%wfs%',ConfiguredValue)-1)-3)
	WHERE 
		ConfigurationFilter = 'CommonConfigurations' 
		AND PackagePath = '\Package.Variables[User::NotificationServiceURL].Properties[Value]'
END
ELSE
	RAISERROR('PT 159804525 has already been applied to this database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd