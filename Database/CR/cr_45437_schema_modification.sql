--WFSScriptProcessorPrint CR 45437
--WFSScriptProcessorPrint Updating OLTA.Users.Password length if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'Users' AND COLUMN_NAME = 'Password' AND CHARACTER_MAXIMUM_LENGTH = 44)
BEGIN
	RAISERROR('Updating OLTA.Users.Password length to 44 characters',10,1) WITH NOWAIT
		
	ALTER TABLE OLTA.Users
		ALTER COLUMN Password varchar(44) NOT NULL
	
	RAISERROR('Updating OLTA.Users table properties',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'Users', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'Users';		

EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 07/25/2011 CR 45437 JPB	Changed password to 44.
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = Users
END