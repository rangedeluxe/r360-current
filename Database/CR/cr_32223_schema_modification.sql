--WFSScriptProcessorPrint CR 32223
--WFSScriptProcessorPrint Removing OLTA.usp_factTransactionDetailsDelete if necessary
--WFSScriptProcessorCRBegin
	IF OBJECT_ID('OLTA.usp_factTransactionDetailsDelete') IS NOT NULL
		DROP PROCEDURE OLTA.usp_factTransactionDetailsDelete
--WFSScriptProcessorCREnd