--WFSScriptProcessorPrint CR 49281
--WFSScriptProcessorPrint Rebuilding OLTA.factBatchData if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factBatchData' AND COLUMN_NAME = 'SourceProcessingDateKey' )
BEGIN 	

	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_dimBanks_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_dimBanks_factBatchData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_dimCustomers_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_dimCustomers_factBatchData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_dimLockboxes_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_dimLockboxes_factBatchData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_ProcessingDate_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_ProcessingDate_factBatchData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_DepositDate_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_DepositDate_factBatchData
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_dimBatchDataSetupFields_factBatchData' )
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT FK_dimBatchDataSetupFields_factBatchData


	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factBatchData_ModifiedBy')
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT DF_factBatchData_ModifiedBy
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factBatchData_ModificationDate')
		ALTER TABLE OLTA.factBatchData DROP CONSTRAINT DF_factBatchData_ModificationDate

	RAISERROR('Rebuilding OLTA.factBatchData adding new columns',10,1) WITH NOWAIT 
	EXEC sp_rename 'OLTA.factBatchData', 'OLDfactBatchData'

	CREATE TABLE OLTA.factBatchData
	(
		BankKey INT NOT NULL,
		CustomerKey INT NOT NULL,
		LockboxKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ProcessingDateKey INT NOT NULL,
		SourceProcessingDateKey int NOT NULL, --CR 49281 JPB 01/12/2012
		BatchDataSetupFieldKey INT NOT NULL,
		BatchID int NOT NULL,
		LoadDate datetime NOT NULL,
		ModifiedBy VARCHAR(32) NOT NULL
			CONSTRAINT DF_factBatchData_ModifiedBy DEFAULT SUSER_SNAME(),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_factBatchData_ModificationDate DEFAULT GETDATE(),
		DataValueDateTime DATETIME NULL,
		DataValueMoney MONEY NULL,
		DataValueInteger INT NULL,
		DataValueBit BIT NULL,
		DataValue VARCHAR(256) NOT NULL
	) $(OnPartition)
		
	RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
	ALTER TABLE OLTA.factBatchData ADD 
		CONSTRAINT FK_dimBanks_factBatchData FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
		CONSTRAINT FK_dimCustomers_factBatchData FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
		CONSTRAINT FK_dimLockboxes_factBatchData FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
		CONSTRAINT FK_ProcessingDate_factBatchData FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_DepositDate_factBatchData FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_SourceProcessingDate_factBatchData FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_dimBatchDataSetupFields_factBatchData FOREIGN KEY(BatchDataSetupFieldKey) REFERENCES OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey)
	RAISERROR('Creating Index OLTA.factBatchData.IDX_factBatchData_DepositDateKey',10,1) WITH NOWAIT 
	CREATE CLUSTERED INDEX IDX_factBatchData_DepositDateKey ON OLTA.factBatchData (DepositDateKey)
	
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factBatchData', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factBatchData';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*		   
*
* Modification History
* 03/07/2011 CR 33212 JPB	Created
* 01/12/2012 CR 49281 JPB	Added SourceProcessingDateKey.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = factBatchData
	
	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	
	INSERT INTO OLTA.factBatchData
	(
		BankKey,
		CustomerKey,
		LockboxKey,
		DepositDateKey,
		ProcessingDateKey,
		SourceProcessingDateKey, --CR 49281 JPB 01/12/2012
		BatchDataSetupFieldKey,
		BatchID,
		LoadDate,
		ModifiedBy,
		ModificationDate,
		DataValueDateTime,
		DataValueMoney,
		DataValueInteger,
		DataValueBit,
		DataValue
	)
	SELECT 	BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			ProcessingDateKey, --rebuild setting the SourceProcessingDateKey date to ProcessingDateKey
			BatchDataSetupFieldKey,
			BatchID,
			LoadDate,
			ModifiedBy,
			ModificationDate,
			DataValueDateTime,
			DataValueMoney,
			DataValueInteger,
			DataValueBit,
			DataValue
	FROM 	OLTA.OLDfactBatchData

	IF OBJECT_ID('OLTA.OLDfactBatchData') IS NOT NULL
	BEGIN
		RAISERROR('Removing old factBatchData table.',10,1) WITH NOWAIT 
		DROP TABLE OLTA.OLDfactBatchData
	END
END
--WFSScriptProcessorCREnd

