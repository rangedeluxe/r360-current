--WFSScriptProcessorPrint CR 34330
--WFSScriptProcessorPrint Update IMSInterfaceQueue QueueData if necessary
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'IMSInterfaceQueue' AND COLUMN_NAME = 'QueueData' AND IS_NULLABLE = 'NO')
BEGIN

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'IMSInterfaceQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'IMSInterfaceQueue';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2009
*
* Purpose: This table will be populated by the XBatchImport stored procedure. 
*	The table will act as an interface point between the OLTA database and an
*	external Image Management System (IMS).
*
* Check Constraint definitions:
* QueueType
*	0: Delete
*	1: Insert
*	2: RPS Insert
*
* QueueStatus
*	0: Not processed
*	10: Available for processing
*	20: In Process
*	30: Error
*	40: Image(s) not found for Batch
*	99: Complete
*	130: An error file was received from Hyland indicating a rejection
*	150: A response file was received from Hyland, and OLTA was updated appropriately. 
*
* Modification History
* 07/08/2009 CR 25817 JPB	Created
* 12/23/2009 CR 28537 JPB	Added cluster index 
*							IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType
* 10/05/2010 CR 31236 JPB	Alter table to meet new requirements
* 03/02/2011 CR 32827 JPB	Added status values 130 and 150.
* 05/13/2011 CR 34323 JPB	Added status values 40.
* 05/13/2011 CR 34330 JPB	Allow QueueData column to be NULL.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = IMSInterfaceQueue

	ALTER TABLE OLTA.IMSInterfaceQueue ALTER COLUMN QueueData XML NULL 
END
ELSE
BEGIN
	RAISERROR('CR 34323 must be applied before this CR',10,1) WITH NOWAIT
END
--WFSScriptProcessorCREnd
