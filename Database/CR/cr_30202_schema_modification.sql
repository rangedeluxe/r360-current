--WFSScriptProcessorPrint CR 30202
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]') AND name = N'IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID')
	CREATE NONCLUSTERED INDEX [IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID] ON [OLTA].[factTransactionSummary] 
	(
		[LockboxKey] ASC,
		[DepositDateKey] ASC,
		[BatchID] ASC,
		[DepositStatus] ASC,
		[BankKey] ASC,
		[CustomerKey] ASC,
		[ProcessingDateKey] ASC,
		[TransactionID] ASC
	) $(OnPartition)
--WFSScriptProcessorCREnd
