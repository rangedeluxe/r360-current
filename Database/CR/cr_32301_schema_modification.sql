--WFSScriptProcessorPrint CR 32301
--WFSScriptProcessorPrint Rebuilding OLTA.factDataEntrySummary if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS(SELECT 1 FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factDataEntrySummary' AND syscolumns.name = 'BatchSourceKey' AND sysobjects.xtype='U')
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimBanks_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimBanks_factDataEntrySummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimCustomers_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimCustomers_factDataEntrySummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimLockboxes_factDataEntrySummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_ProcessingDate_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_ProcessingDate_factDataEntrySummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_DepositDate_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_DepositDate_factDataEntrySummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary

	RAISERROR('Rebuilding OLTA.factDataEntrySummary adding new columns',10,1) WITH NOWAIT 
	EXEC sp_rename 'OLTA.factDataEntrySummary', 'OLDfactDataEntrySummary'

	CREATE TABLE OLTA.factDataEntrySummary
	(
		BankKey int NOT NULL,
		CustomerKey int NOT NULL,
		LockboxKey int NOT NULL,
		ProcessingDateKey int NOT NULL, --CR 28221 JPB 11-14-2009
		DepositDateKey int NOT NULL,
		DataEntryColumnKey int NOT NULL,
		GlobalBatchID int NOT NULL,
		BatchID int NOT NULL,
		DepositStatus int NOT NULL,
		BatchSourceKey tinyint NOT NULL, --CR 32301 JPB 01/13/2011
		LoadDate datetime NOT NULL
	) $(OnPartition)
	RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
	ALTER TABLE OLTA.factDataEntrySummary ADD
		CONSTRAINT FK_dimBanks_factDataEntrySummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
		CONSTRAINT FK_dimCustomers_factDataEntrySummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
		CONSTRAINT FK_dimLockboxes_factDataEntrySummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
		CONSTRAINT FK_DepositDate_factDataEntrySummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_ProcessingDate_factDataEntrySummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey),
		CONSTRAINT FK_dimBatchSources_factDataEntrySummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_DepositDateKey',10,1) WITH NOWAIT 
	CREATE CLUSTERED INDEX IDX_factDataEntrySummary_DepositDateKey ON OLTA.factDataEntrySummary(DepositDateKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_BankKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factDataEntrySummary_BankKey ON OLTA.factDataEntrySummary (BankKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_CustomerKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factDataEntrySummary_CustomerKey ON OLTA.factDataEntrySummary (CustomerKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_LockboxKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factDataEntrySummary_LockboxKey ON OLTA.factDataEntrySummary (LockboxKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_ProcessingDate',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factDataEntrySummary_ProcessingDateKey ON OLTA.factDataEntrySummary(ProcessingDateKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_DataEntryColumnKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factDataEntrySummary_DataEntryColumnKey ON OLTA.factDataEntrySummary (DataEntryColumnKey) $(OnPartition)
	
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDataEntrySummary', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factDataEntrySummary';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28221 JPB	ProcessingDateKey now NOT NULL.
* 02/12/2010 CR 28240 JPB	Added missing index on processing date key.
* 02/12/2010 CR 29011 JPB	Added missing foreign key on processing date.
* 01/13/2011 CR 32301 JPB	Added BatchSourceKey.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = factDataEntrySummary
	
	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	
	INSERT INTO OLTA.factDataEntrySummary
	(
		BankKey,
		CustomerKey,
		LockboxKey,
		ProcessingDateKey,
		DepositDateKey,
		DataEntryColumnKey,
		GlobalBatchID,
		BatchID,
		DepositStatus,
		BatchSourceKey,
		LoadDate
	)
	SELECT 	BankKey,
			CustomerKey,
			LockboxKey,
			ProcessingDateKey,
			DepositDateKey,
			DataEntryColumnKey,
			GlobalBatchID,
			BatchID,
			DepositStatus,
			(SELECT BatchSourceKey FROM OLTA.dimBatchSources WHERE ShortName = 'integraPAY') AS BatchSourceKey,
			LoadDate
	FROM 	OLTA.OLDfactDataEntrySummary

	IF OBJECT_ID('OLTA.OLDfactDataEntrySummary') IS NOT NULL
	BEGIN
		RAISERROR('Removing old factDataEntrySummary table.',10,1) WITH NOWAIT 
		DROP TABLE OLTA.OLDfactDataEntrySummary
	END
END
--WFSScriptProcessorCREnd

