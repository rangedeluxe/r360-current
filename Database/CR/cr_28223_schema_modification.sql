--WFSScriptProcessorCRPrint Updating OLTA.factDataEntryDetails.ProcessingDateKey
--WFSScriptProcessorCRBegin
IF (SELECT syscolumns.isnullable FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factDataEntryDetails' AND syscolumns.name = 'ProcessingDateKey' AND sysobjects.xtype='U') = 1
	ALTER TABLE OLTA.factDataEntryDetails ALTER COLUMN ProcessingDateKey INT NOT NULL
--WFSScriptProcessorCREnd