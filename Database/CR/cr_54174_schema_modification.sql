--WFSScriptProcessorPrint CR 54174
--WFSScriptProcessorPrint Adding BrandingSchemeID to OLTA.OLCustomers if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'OLCustomers' AND COLUMN_NAME = 'DisplayBatchID' )
BEGIN /* verify that CR 49637 has been applied before continuing. */
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'OLCustomers' AND COLUMN_NAME = 'BrandingSchemeID' )
	BEGIN
		RAISERROR('CR 49637 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLCustomers' AND CONSTRAINT_NAME='FK_BrandingSchemes_OLCustomers' )
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT FK_BrandingSchemes_OLCustomers
			
		RAISERROR('Dropping Constraints',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLCustomers' AND CONSTRAINT_NAME='PK_OLCustomers' )
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT PK_OLCustomers
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_OLCustomerID')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_OLCustomerID
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_CheckImageDisplayMode')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_CheckImageDisplayMode
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_DocumentImageDisplayMode')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_DocumentImageDisplayMode
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_BrandingSchemeID')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_BrandingSchemeID
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_CreationDate')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_CreationDate
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_CreatedBy')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_CreatedBy
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_ModificationDate')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_ModificationDate
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLCustomers_ModifiedBy')
			ALTER TABLE OLTA.OLCustomers DROP CONSTRAINT DF_OLCustomers_ModifiedBy

		RAISERROR('Rebuilding OLTA.OLCustomers adding new column.',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.OLCustomers', 'OLDOLCustomers'

		CREATE TABLE OLTA.OLCustomers 
		(
			OLCustomerID uniqueidentifier NOT NULL 
				CONSTRAINT PK_OLCustomers PRIMARY KEY CLUSTERED
				CONSTRAINT DF_OLCustomers_OLCustomerID DEFAULT(NEWID()),
			CustomerCode varchar(20) NOT NULL,
			Description varchar(255) NOT NULL,
			IsActive bit NOT NULL,
			ViewingDays int NULL,
			MaximumSearchDays int NULL,
			CheckImageDisplayMode tinyint NOT NULL CONSTRAINT DF_OLCustomers_CheckImageDisplayMode DEFAULT(0),
			DocumentImageDisplayMode tinyint NOT NULL CONSTRAINT DF_OLCustomers_DocumentImageDisplayMode DEFAULT(0),
			DisplayBatchID bit NOT NULL
				CONSTRAINT DF_OLCustomers_DisplayBatchID DEFAULT(0),
			BrandingSchemeID int NOT NULL CONSTRAINT DF_OLCustomers_BrandingSchemeID DEFAULT(1),
			ExternalID1 varchar(32) NULL,
			ExternalID2 varchar(32) NULL,
			CreationDate datetime NOT NULL CONSTRAINT DF_OLCustomers_CreationDate DEFAULT(GETDATE()),
			CreatedBy varchar(32) NOT NULL CONSTRAINT DF_OLCustomers_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate datetime NOT NULL CONSTRAINT DF_OLCustomers_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy varchar(32) NOT NULL CONSTRAINT DF_OLCustomers_ModifiedBy DEFAULT(SUSER_SNAME())       
		)
		
		RAISERROR('Creating Foreign Keys.',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.OLCustomers ADD 
			   CONSTRAINT FK_BrandingSchemes_OLCustomers FOREIGN KEY (BrandingSchemeID) REFERENCES OLTA.BrandingSchemes(BrandingSchemeID)
			
		RAISERROR('Creating Index OLTA.OLCustomers.IDX_OLCustomers_CustomerCode.',10,1) WITH NOWAIT 
		CREATE UNIQUE INDEX IDX_OLCustomers_CustomerCode ON OLTA.OLCustomers(CustomerCode)
	
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'OLCustomers', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'OLCustomers';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/30/2009 CR 29277 JNE   Changed ViewingDays to be Nullable.
* 01/18/2012 CR 49637 JCS   Added column BrandingSchemeID with default value of 1
*							as that is the initial branding in BrandingSchemes table.
* 08/23/2012 CR 54174 JPB	Added DisplayBatchID.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = OLCustomers
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.OLCustomers
		(
			OLCustomerID,
			CustomerCode,
			[Description],
			IsActive,
			ViewingDays,
			MaximumSearchDays,
			CheckImageDisplayMode,
			DocumentImageDisplayMode,
			BrandingSchemeID,
			ExternalID1,
			ExternalID2,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
		)
		SELECT OLCustomerID,
				CustomerCode,
				[Description],
				IsActive,
				ViewingDays,
				MaximumSearchDays,
				CheckImageDisplayMode,
				DocumentImageDisplayMode,
				BrandingSchemeID,
				ExternalID1,
				ExternalID2,
				CreationDate,
				CreatedBy,
				ModificationDate,
				ModifiedBy
		FROM 	OLTA.OLDOLCustomers

		IF OBJECT_ID('OLTA.OLDOLCustomers') IS NOT NULL
		BEGIN
			RAISERROR('Removing old OLCustomers table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDOLCustomers
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd

