--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 263694
--WFSScriptProcessorPrint Update existing dimOrganizations/dimClientAccounts SiteOrganizationID to -1 if necessary.
--WFSScriptProcessorCRBegin
UPDATE RecHubData.dimOrganizations SET SiteOrganizationID = -1 WHERE SiteOrganizationID <> -1
UPDATE RecHubData.dimClientAccounts SET SiteOrganizationID = -1 WHERE SiteOrganizationID <> -1
--WFSScriptProcessorCREnd