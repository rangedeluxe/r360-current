--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 141435715, 142595657
--WFSScriptProcessorPrint Add CreationDateKey to Session Table and partition it
--WFSScriptProcessorCRBegin

DECLARE @SessionsRetentionDays	INT = 49,
		@BeginDate				DATETIME,
		@BeginDateKey			INT,
		@Message				VARCHAR(90)

IF NOT EXISTS  (SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'Session'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubUser') = sys.tables.schema_id )
BEGIN
	RAISERROR('Beginning Process to Add CreationDateKey to the Session table',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionsRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionsRetentionDays'
	SELECT @BeginDate = GETDATE() - @SessionsRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @BeginDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate <= @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest date of Session rows to keep is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='Session' AND CONSTRAINT_NAME='FK_Session_Users' )
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT FK_Session_Users;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionActivityLog' AND CONSTRAINT_NAME='FK_SessionActivityLog_Session' )
		ALTER TABLE RecHubUser.SessionActivityLog DROP CONSTRAINT FK_SessionActivityLog_Session;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionLog' AND CONSTRAINT_NAME='FK_SessionLog_Session' )
		ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT FK_SessionLog_Session;
	
	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	--PK

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='Session' AND CONSTRAINT_NAME='PK_Sessions' )
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT PK_Sessions;

	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Sessions_CreationDateKey')
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT DF_Sessions_CreationDateKey;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Sessions_PageCounter')
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT DF_Sessions_PageCounter;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Sessions_IsSuccess')
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT DF_Sessions_IsSuccess;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Sessions_IsSessionEnded')
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT DF_Sessions_IsSessionEnded;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_Sessions_IsRegistered')
		ALTER TABLE RecHubUser.Session DROP CONSTRAINT DF_Sessions_IsRegistered;

	RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_Sessions_CreationDateKeySessionID') 
		DROP INDEX RecHubUser.Session.IDX_Sessions_CreationDateKeySessionID;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_Sessions_LogonDateTime') 
		DROP INDEX RecHubUser.Session.IDX_Sessions_LogonDateTime;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_Sessions_UserID') 
		DROP INDEX RecHubUser.Session.IDX_Sessions_UserID;

	RAISERROR('Rebuilding Table RecHubUser.Session.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubUser.Session', 'OLD_Session';

	CREATE TABLE RecHubUser.[Session]
	(
		SessionID UNIQUEIDENTIFIER NOT NULL, 
		UserID INT NOT NULL,
		CreationDateKey INT NOT NULL
			CONSTRAINT DF_Sessions_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
		IPAddress VARCHAR(64) NOT NULL,
		BrowserType VARCHAR(1024) NOT NULL,
		LogonDateTime DATETIME NOT NULL,
		LastPageServed DATETIME NULL,
		PageCounter INT NOT NULL 
			CONSTRAINT DF_Sessions_PageCounter DEFAULT(0),
		IsSuccess BIT NOT NULL 
			CONSTRAINT DF_Sessions_IsSuccess DEFAULT(0),
		IsSessionEnded BIT NOT NULL 
			CONSTRAINT DF_Sessions_IsSessionEnded DEFAULT(0),
		LogonName VARCHAR(50) NULL,
		OLOrganizationID UNIQUEIDENTIFIER NULL,
		OrganizationCode VARCHAR(20) NULL,
		IsRegistered BIT NOT NULL 
			CONSTRAINT DF_Sessions_IsRegistered DEFAULT(0)
	) ON Sessions (CreationDateKey);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	--SET IDENTITY_INSERT RecHubUser.Session ON;
	INSERT INTO RecHubUser.Session
	(
		SessionID,
		UserID,
		CreationDateKey,
		IPAddress,
		BrowserType,
		LogonDateTime,
		LastPageServed,
		PageCounter,
		IsSuccess,
		IsSessionEnded,
		LogonName,
		OLOrganizationID,
		OrganizationCode,
		IsRegistered
	)
	SELECT
		SessionID,
		UserID,
		CAST(CONVERT(VARCHAR(10), LogonDateTime, 112) AS INT) AS CreationDateKey,
		IPAddress,
		BrowserType,
		LogonDateTime,
		LastPageServed,
		PageCounter,
		IsSuccess,
		IsSessionEnded,
		LogonName,
		OLOrganizationID,
		OrganizationCode,
		IsRegistered
	FROM
		RecHubUser.OLD_Session
	WHERE
		LogonDateTime > @BeginDate - 1;

	--SET IDENTITY_INSERT RecHubUser.Session OFF;

	RAISERROR('Adding Primary/Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubUser.Session ADD 
		CONSTRAINT PK_Sessions PRIMARY KEY NONCLUSTERED (SessionID, CreationDateKey);
	ALTER TABLE RecHubUser.Session ADD 
		CONSTRAINT FK_Session_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);
	ALTER TABLE RecHubUser.SessionLog ADD 
		CONSTRAINT FK_SessionLog_Session FOREIGN KEY(SessionID, CreationDateKey) REFERENCES RecHubUser.Session(SessionID, CreationDateKey);

	RAISERROR('Adding IDX_Sessions_UserID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_Sessions_UserID ON RecHubUser.[Session](UserID) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_Sessions_LogonDateTime',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_Sessions_LogonDateTime ON RecHubUser.[Session](LogonDateTime) ON Sessions (CreationDateKey);
		
	RAISERROR('Adding IDX_Sessions_CreationDateKeySessionID',10,1) WITH NOWAIT;
	CREATE CLUSTERED INDEX IDX_Sessions_CreationDateKeySessionID ON RecHubUser.[Session](CreationDateKey,SessionID) ON Sessions (CreationDateKey);

	RAISERROR('Updating RecHubUser.Session.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'Session', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubUser',
			@level1type = N'TABLE',
			@level1name = N'Session';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubUser,
	@level1type = N'TABLE',@level1name = Session,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Static Data
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 09/29/2011 CR 46575 JPB 	Updated IPAddress to VARCHAR(64)
* 11/15/2011 CR 47915 JPB	Made FK_Users_Sessions NOCHECK
* 01/05/2012 CR 49091 JPB 	Updated BrowserType to VARCHAR(1024)
* 02/17/2013 WI 89487 JBS	Update table to 2.0 release. Change Schema Name
* 03/28/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added partition keys
**********************************************************************************/';
		
		
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubUser.OLD_Session') IS NOT NULL
		DROP TABLE RecHubUser.OLD_Session;

		
END
ELSE
	RAISERROR('PT 142595657 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd