--WFSScriptProcessorPrint CR 28555
--WFSScriptProcessorPrint Dropping OLTA.LoadStatDetails.FK_LoadStats_LoadStatID
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_LoadStats_LoadStatID]') AND parent_object_id = OBJECT_ID(N'[OLTA].[LoadStatDetails]'))
	ALTER TABLE [OLTA].[LoadStatDetails] DROP CONSTRAINT [FK_LoadStats_LoadStatID]
--WFSScriptProcessorPrint Dropping OLTA.LoadStats.PK_LoadStats
IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[LoadStats]') AND name = N'PK_LoadStats')
	ALTER TABLE [OLTA].[LoadStats] DROP CONSTRAINT [PK_LoadStats]
--WFSScriptProcessorIndex OLTA.LoadStats.IDX_LoadStats_LoadStatTimeStamp
CREATE CLUSTERED INDEX [IDX_LoadStats_LoadStatTimeStamp] ON [OLTA].[LoadStats] 
(
	[LoadStatTimeStamp] ASC
)
--WFSScriptProcessorIndex OLTA.LoadStats.PK_LoadStats
ALTER TABLE [OLTA].[LoadStats] ADD  CONSTRAINT [PK_LoadStats] PRIMARY KEY NONCLUSTERED 
(
	[LoadStatID] ASC
)
--WFSScriptProcessorForeignKey
ALTER TABLE [OLTA].[LoadStatDetails]  WITH CHECK ADD  
	CONSTRAINT [FK_LoadStats_LoadStatID] FOREIGN KEY([LoadStatID]) REFERENCES [OLTA].[LoadStats] ([LoadStatID])
--WFSScriptProcessorAddCheckConstraint FK_LoadStats_LoadStatID
ALTER TABLE [OLTA].[LoadStatDetails] CHECK CONSTRAINT [FK_LoadStats_LoadStatID]
