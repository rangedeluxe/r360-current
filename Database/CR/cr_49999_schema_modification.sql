--WFSScriptProcessorPrint CR 49999
--WFSScriptProcessorPrint Removing OLTA.usp_IMSTranItemReport_GetDocumentData if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_IMSTranItemReport_GetDocumentData') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure OLTA.usp_IMSTranItemReport_GetDocumentData.',10,1) WITH NOWAIT
	DROP PROCEDURE OLTA.usp_IMSTranItemReport_GetDocumentData
END
ELSE
	RAISERROR('CR has already been applied to this database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd