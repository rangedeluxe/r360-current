--WFSScriptProcessorCRPrint Creating Temp table for OLTA.factBatchSummary
--WFSScriptProcessorCRBegin
CREATE TABLE #tmpFactBatchSummary
(
   BankKey int NOT NULL,
   CustomerKey int NOT NULL,
   LockboxKey int NOT NULL,
   ProcessingDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
   DepositDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
   GlobalBatchID int NOT NULL,
   BatchID int NOT NULL,
   DESetupID int NULL,
   DepositDDA varchar(40) NULL,
   SystemType tinyint NOT NULL,
   DepositStatus int NOT NULL,
   DepositStatusDisplayName varchar(80) NOT NULL,
   TransactionCount int NOT NULL,
   CheckCount int NOT NULL,
   DocumentCount int NOT NULL,
   ScannedCheckCount int NOT NULL,
   StubCount int NOT NULL,
   CheckTotal money NOT NULL,
   LoadDate datetime NOT NULL
)
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Removing invalid records
--WFSScriptProcessorCRBegin
DELETE FROM OLTA.factBatchSummary WHERE DepositDateKey IS NULL
--WFSScriptProcessorCREnd


--WFSScriptProcessorCRPrint Copy factBatchSummary to temp table
--WFSScriptProcessorCRBegin
INSERT INTO #tmpFactBatchSummary
SELECT * FROM OLTA.factBatchSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factBatchSummary.FK_DepositDate_factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_DepositDate_factBatchSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]'))
	ALTER TABLE [OLTA].[factBatchSummary] DROP CONSTRAINT [FK_DepositDate_factBatchSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factBatchSummary.FK_dimBanks_factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimBanks_factBatchSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]'))
	ALTER TABLE [OLTA].[factBatchSummary] DROP CONSTRAINT [FK_dimBanks_factBatchSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factBatchSummary.FK_dimCustomers_factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimCustomers_factBatchSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]'))
	ALTER TABLE [OLTA].[factBatchSummary] DROP CONSTRAINT [FK_dimCustomers_factBatchSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factBatchSummary.FK_dimLockboxes_factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimLockboxes_factBatchSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]'))
	ALTER TABLE [OLTA].[factBatchSummary] DROP CONSTRAINT [FK_dimLockboxes_factBatchSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factBatchSummary.FK_ProcessingDate_factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_ProcessingDate_factBatchSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]'))
	ALTER TABLE [OLTA].[factBatchSummary] DROP CONSTRAINT [FK_ProcessingDate_factBatchSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping table OLTA.factBatchSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]') AND type in (N'U'))
	DROP TABLE OLTA.factBatchSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Creating table OLTA.factBatchSummary
--WFSScriptProcessorCRBegin
--Cannot check to see if the batch summary table exists because it was just delete, use the checks table instead
IF EXISTS(SELECT 1 FROM sys.partitions WHERE object_id = object_id('OLTA.factChecks') and partition_number > 1 )
BEGIN
	CREATE TABLE OLTA.factBatchSummary(
		   BankKey int NOT NULL,
		   CustomerKey int NOT NULL,
		   LockboxKey int NOT NULL,
		   ProcessingDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
		   DepositDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
		   GlobalBatchID int NOT NULL,
		   BatchID int NOT NULL,
		   DESetupID int NULL,
		   DepositDDA varchar(40) NULL,
		   SystemType tinyint NOT NULL,
		   DepositStatus int NOT NULL,
		   DepositStatusDisplayName varchar(80) NOT NULL,
		   TransactionCount int NOT NULL,
		   CheckCount int NOT NULL,
		   DocumentCount int NOT NULL,
		   ScannedCheckCount int NOT NULL,
		   StubCount int NOT NULL,
		   CheckTotal money NOT NULL,
		   LoadDate datetime NOT NULL
	) ON OLTAFacts_Partition_Scheme(DepositDateKey)
END
ELSE
BEGIN
	CREATE TABLE OLTA.factBatchSummary(
		   BankKey int NOT NULL,
		   CustomerKey int NOT NULL,
		   LockboxKey int NOT NULL,
		   ProcessingDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
		   DepositDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
		   GlobalBatchID int NOT NULL,
		   BatchID int NOT NULL,
		   DESetupID int NULL,
		   DepositDDA varchar(40) NULL,
		   SystemType tinyint NOT NULL,
		   DepositStatus int NOT NULL,
		   DepositStatusDisplayName varchar(80) NOT NULL,
		   TransactionCount int NOT NULL,
		   CheckCount int NOT NULL,
		   DocumentCount int NOT NULL,
		   ScannedCheckCount int NOT NULL,
		   StubCount int NOT NULL,
		   CheckTotal money NOT NULL,
		   LoadDate datetime NOT NULL
	)
END
--WFSScriptProcessorCREnd

--WFSScriptProcessorForeignKey
ALTER TABLE OLTA.factBatchSummary ADD 
       CONSTRAINT FK_dimBanks_factBatchSummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
       CONSTRAINT FK_dimCustomers_factBatchSummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
       CONSTRAINT FK_dimLockboxes_factBatchSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
       CONSTRAINT FK_ProcessingDate_factBatchSummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
       CONSTRAINT FK_DepositDate_factBatchSummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey)
--WFSScriptProcessorIndex OLTA.factBatchSummary.IDX_factBatchSummary_DepositDateKey
CREATE CLUSTERED INDEX IDX_factBatchSummary_DepositDateKey ON OLTA.factBatchSummary (DepositDateKey)
--WFSScriptProcessorIndex OLTA.factBatchSummary.IDX_factBatchSummary_BankKey
CREATE INDEX IDX_factBatchSummary_BankKey ON OLTA.factBatchSummary (BankKey)
--WFSScriptProcessorIndex OLTA.factBatchSummary.IDX_factBatchSummary_LockboxKey
CREATE INDEX IDX_factBatchSummary_LockboxKey ON OLTA.factBatchSummary (LockboxKey)
--WFSScriptProcessorIndex OLTA.factBatchSummary.IDX_factBatchSummary_CustomerKey
CREATE INDEX IDX_factBatchSummary_CustomerKey ON OLTA.factBatchSummary (CustomerKey)
--WFSScriptProcessorIndex OLTA.factBatchSummary.IDX_factBatchSummary_ProcessingDate
CREATE INDEX IDX_factBatchSummary_ProcessingDate ON OLTA.factBatchSummary (ProcessingDateKey)

--WFSScriptProcessorCRPrint Copy factBatchSummary to temp table
--WFSScriptProcessorCRBegin
INSERT INTO OLTA.factBatchSummary
SELECT * FROM #tmpFactBatchSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRBegin
DROP TABLE #tmpFactBatchSummary
--WFSScriptProcessorCREnd
