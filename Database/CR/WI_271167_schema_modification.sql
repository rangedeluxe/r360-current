--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271167
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factChecks.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factChecks')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factChecks'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		IF( (SELECT is_cdc_enabled FROM sys.databases WHERE [name] = DB_NAME()) = 1 )
              EXEC sys.sp_cdc_disable_db;

		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DepositDatefactCheckKey')
			DROP INDEX IDX_factChecks_DepositDatefactCheckKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey')
			DROP INDEX IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_BankKey')
			DROP INDEX IDX_factChecks_BankKey ON RecHubData.factChecks; 		
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_OrganizationKey')
			DROP INDEX IDX_factChecks_OrganizationKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_ClientAccountKey')
			DROP INDEX IDX_factChecks_ClientAccountKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_ImmutableDateKey')
			DROP INDEX IDX_factChecks_ImmutableDateKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DDAKey')
			DROP INDEX IDX_factChecks_DDAKey ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber')
			DROP INDEX IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID')
			DROP INDEX IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus')
			DROP INDEX IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID')
			DROP INDEX IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID')
			DROP INDEX IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factChecks; 


		/****** Object:  Index PK_factChecks    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factChecks')
			ALTER TABLE RecHubData.factChecks DROP CONSTRAINT PK_factChecks;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factChecks ADD
			CONSTRAINT PK_factChecks PRIMARY KEY NONCLUSTERED (factCheckKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factChecks_DepositDatefactCheckKey',10,1) WITH NOWAIT;  
		CREATE CLUSTERED INDEX IDX_factChecks_DepositDatefactCheckKey ON RecHubData.factChecks 
		(
			DepositDateKey,
			factCheckKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey',10,1) WITH NOWAIT; 
		CREATE INDEX IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey ON RecHubData.factChecks 
		(
			DepositDateKey,
			ClientAccountKey,
			SourceProcessingDateKey,
			BatchID,
			BatchSequence,
			BankKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_BankKey',10,1) WITH NOWAIT; 
		CREATE INDEX IDX_factChecks_BankKey ON RecHubData.factChecks (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_OrganizationKey',10,1) WITH NOWAIT; 
		CREATE INDEX IDX_factChecks_OrganizationKey ON RecHubData.factChecks (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_ClientAccountKey',10,1) WITH NOWAIT; 
		CREATE INDEX IDX_factChecks_ClientAccountKey ON RecHubData.factChecks (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_ImmutableDateKey',10,1) WITH NOWAIT; 
		CREATE INDEX IDX_factChecks_ImmutableDateKey ON RecHubData.factChecks (ImmutableDateKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_DDAKey',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_DDAKey ON RecHubData.factChecks (DDAKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber ON RecHubData.factChecks
		(
			BankKey ASC,
			OrganizationKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			DepositDateKey ASC,
			BatchID ASC,
			BatchNumber ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			BankKey ASC,
			OrganizationKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			TransactionID ASC
		)
		INCLUDE 
		( 
			BatchSequence,
			CheckSequence,
			NumericSerial,
			Serial
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus ON RecHubData.factChecks
		(
			ClientAccountKey ASC,
			DepositDateKey ASC,
			ImmutableDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			Amount ASC,
			Serial ASC,
			DepositStatus ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			Amount ASC,
			BankKey ASC,
			OrganizationKey ASC,
			ImmutableDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			TransactionID ASC
		)
		INCLUDE
		(
			DepositStatus,
			NumericSerial,
			CheckSequence,
			Serial
		)  $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID',10,1) WITH NOWAIT; 
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factChecks
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			BatchID ASC,
			TransactionID ASC
		)  $(OnDataPartition);   


	END
	ELSE
		RAISERROR('WI 271167 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271167 RecHubData.factChecks table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd