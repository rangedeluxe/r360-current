--WFSScriptProcessorPrint CR 34348
--WFSScriptProcessorPrint Update factChecks IDX_factChecks_DepositDateKey if necessary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factChecks]') AND name = N'IDX_factChecks_DepositDateKey')
BEGIN
	IF NOT EXISTS (	SELECT	1
					FROM	sys.tables
							INNER JOIN sys.indexes ON sys.tables.[object_id] = sys.indexes.[object_id]
							INNER JOIN sys.index_columns ON sys.index_columns.[object_id] = sys.indexes.[object_id] and sys.index_columns.[index_id] = sys.indexes.[index_id]
							INNER JOIN sys.all_columns ON sys.index_columns.[object_id] = sys.all_columns.[object_id] and sys.index_columns.[column_id] = sys.all_columns.[column_id]
					WHERE	sys.tables.[name] = 'factChecks' AND sys.indexes.[name] = 'IDX_factChecks_DepositDateKey' AND sys.all_columns.[name] = 'LockboxKey'
				)
	BEGIN

		DROP INDEX [IDX_factChecks_DepositDateKey] ON [OLTA].[factChecks]
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factChecks', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factChecks';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factChecks

		CREATE CLUSTERED INDEX IDX_factChecks_DepositDateKey ON OLTA.factChecks (DepositDateKey,LockboxKey,ProcessingDateKey,BatchID,BatchSequence,BankKey,RemitterKey) $(OnPartition)
	END
END
--WFSScriptProcessorCREnd
