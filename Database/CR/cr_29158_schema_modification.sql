--WFSScriptProcessorPrint CR 29158

--WFSScriptProcessorPrint Adding olta.Lockboxes.IsCommingled if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'dimLockboxes' AND syscolumns.name = 'IsCommingled' AND sysobjects.xtype='U')
	ALTER TABLE [olta].[dimLockboxes]
	ADD IsCommingled bit NOT NULL
        CONSTRAINT DF_dimLockboxes_IsCommingled DEFAULT 0
--WFSScriptProcessorCREnd











