--WFSScriptProcessorPrint CR 46018
--WFSScriptProcessorPrint Updating OLTA.OLPreferences DisplayRemitterNameInPDF to 1 if necessary.
--WFSScriptProcessorStaticDataCreateBegin
IF EXISTS (SELECT * FROM OLTA.OLPreferences WHERE PreferenceName = 'DisplayRemitterNameInPDF' AND IsOnline = 0 )
	UPDATE OLTA.OLPreferences SET IsOnline = 1, IsSystem = 0 WHERE PreferenceName = 'DisplayRemitterNameInPDF' AND IsOnline = 0
--WFSScriptProcessorStaticDataCreateEnd
