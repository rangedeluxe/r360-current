--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #147282137
--WFSScriptProcessorPrint Rebuilding RecHubException.PostDepositDataEntryDetails table if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM sys.tables INNER JOIN sys.columns 
		ON sys.columns.object_id = sys.tables.object_id WHERE sys.tables.name = 'PostDepositDataEntryDetails' and sys.columns.name = 'IsUIAddedField' )
BEGIN
	RAISERROR('Dropping RecHubException.PostDepositDataEntryDetails to add new column.',10,1) WITH NOWAIT;

	DROP TABLE RecHubException.PostDepositDataEntryDetails;

	/* The table build script will be completed by the PostDepositDataEntryDetails.SQL script later in command file */

END
--WFSScriptProcessorCREnd

