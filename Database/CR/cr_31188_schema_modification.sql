--WFSScriptProcessorPrint CR 31188
--WFSScriptProcessorPrint Removing dbo.BatchTrace.OLTAQueueInsert if it exists.
--WFSScriptProcessorCRBegin
IF EXISTS(SELECT 1 FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'BatchTrace' AND syscolumns.name = 'OLTAQueueInsert' AND sysobjects.xtype='U')
	ALTER TABLE [dbo].[BatchTrace] DROP COLUMN OLTAQueueInsert
--WFSScriptProcessorCREnd
