--WFSScriptProcessorCRPrint Updating OLTA.factBatchSummary.GlobalBatchID
--WFSScriptProcessorCRBegin
IF (SELECT syscolumns.isnullable FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factBatchSummary' AND syscolumns.name = 'GlobalBatchID' AND sysobjects.xtype='U') = 0
	ALTER TABLE OLTA.factBatchSummary ALTER COLUMN GlobalBatchID INT NULL
--WFSScriptProcessorCREnd
