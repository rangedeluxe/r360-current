﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 283038
--WFSScriptProcessorPrint Add IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount to factChecks if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE NAME='IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount' )
BEGIN

	RAISERROR('Adding index IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount ON RecHubData.factChecks
	(
		IsDeleted ASC,
		DepositDateKey ASC,
		BatchID ASC,
		TransactionID ASC,
		Amount ASC
	)
	INCLUDE 
	(
		DDAKey,
		BatchSequence,
		CheckSequence,
		NumericRoutingNumber,
		NumericSerial,
		RoutingNumber,
		Account,
		TransactionCode,
		Serial,
		RemitterName
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factChecks', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'factChecks';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = factChecks,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every check.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51372 JPB	Renamed index and added INCLUDE columns.
* 04/05/2012 CR 51866 JPB	Added new index to improve remittence search.
* 03/26/2012 CR 51541 JPB	Added BatchPaymentTypeKey.
* 04/06/2012 CR 51416 JPB	Added new index to improve search performance.
* 07/05/2012 CR 53625 JPB	Added BatchCueID.
* 07/16/2012 CR 54121 JPB	Added BatchNumber.
* 07/17/2012 CR 54131 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54132 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54133 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54206 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71800 JPB	2.0 release. Change Schema name.
*							Adding columns: factCheckKey, IsDeleted, NumericRoutingNumber,
*							NumericSerial, RoutingNumber, Account, RemitterName.
*							Renaming columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey.
*							Remove Columns: ImageInfoXML, RemitterKey.
*							Added factCheckKey to ClusteredIndex.
*							Renamed indexes to match schema and column changes	
*							Create New Index based from old clustered index.				
*							Forward Patch: WI 87430
* 02/28/2014 WI 131199 JBS	Add DDAKey, including FK constraint and Index
* 04/10/2014 WI 135315 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 142797 JBS	Added SourceBatchID.
* 05/30/2014 WI 144578 JBS	Changed SourceBatchKey to SMALLINT.
* 06/01/2016 WI 283038 MGE  Added Index for Advanced Search improvement
******************************************************************************/';
END
ELSE
	RAISERROR('WI 283038 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
