--WFSScriptProcessorPrint CR 52942
--WFSScriptProcessorPrint Removing obsoleted stored procedures if necessary
--WFSScriptProcessorCRBegin
IF OBJECT_ID('OLTA.usp_dimImageRPSAliasMappings_GetByLockboxID') IS NOT NULL
BEGIN
	RAISERROR('Removing usp_dimImageRPSAliasMappings_GetByLockboxID',10,1) WITH NOWAIT;
	DROP PROCEDURE OLTA.usp_dimImageRPSAliasMappings_GetByLockboxID
END
IF OBJECT_ID('OLTA.usp_dimImageRPSAliasMappingsUpsert') IS NOT NULL
BEGIN
	RAISERROR('Removing usp_dimImageRPSAliasMappingsUpsert',10,1) WITH NOWAIT;
	DROP PROCEDURE OLTA.usp_dimImageRPSAliasMappingsUpsert
END
--WFSScriptProcessorCREnd
