--WFSScriptProcessorPrint CR 53405
--WFSScriptProcessorPrint Renaming IMSInterfaceQueue.SiteID if necessary
--WFSScriptProcessorCRBegin
IF EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'IMSInterfaceQueue' AND COLUMN_NAME = 'SiteID' )
BEGIN
	/* check for last known change to table. If it is not there, do not continue with the change. */
	IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[IMSInterfaceQueue]') AND name = N'IDX_IMSInterfaceQueue_QueueStatusProcessingDateKeyCreationDateQueueTypeModificationDate' )
	BEGIN /* verify that CR 47584 has been applied before continuing. */
		RAISERROR('CR 47584 must be applied before this CR.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Dropping DF_IMSInterfaceQueue_SiteID constraint',10,1) WITH NOWAIT
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_IMSInterfaceQueue_SiteID')
			ALTER TABLE OLTA.IMSInterfaceQueue DROP CONSTRAINT DF_IMSInterfaceQueue_SiteID
	
		RAISERROR('Dropping SiteID column',10,1) WITH NOWAIT;
		ALTER TABLE OLTA.IMSInterfaceQueue DROP COLUMN SiteID;

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'IMSInterfaceQueue', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'IMSInterfaceQueue';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2009
*
* Purpose: This table will be populated by the XBatchImport stored procedure. 
*	The table will act as an interface point between the OLTA database and an
*	external Image Management System (IMS).
*
* Check Constraint definitions:
* QueueType
*	0: Delete
*	1: Insert
*	2: RPS Insert
*
* QueueStatus
*	0: Not processed
*	10: Available for processing
*	20: In Process
*	30: Error
*	40: Image(s) not found for Batch
*	99: Complete
*	130: An error file was received from Hyland indicating a rejection
*	150: A response file was received from Hyland, and OLTA was updated appropriately. 
*
* Modification History
* 07/08/2009 CR 25817 JPB	Created
* 12/23/2009 CR 28537 JPB	Added cluster index 
*							IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType
* 10/05/2010 CR 31236 JPB	Alter table to meet new requirements
* 03/02/2011 CR 32827 JPB	Added status values 130 and 150.
* 05/13/2011 CR 34323 JPB	Added status values 40.
* 05/13/2011 CR 34330 JPB	Allow QueueData column to be NULL.
* 06/16/2011 CR 45225 JPB	Added SiteID to table.
* 11/02/2011 CR 47584 JPB	Added new index.
* 06/13/2012 CR	53405 JPB	Removed SiteID.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = IMSInterfaceQueue;
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
