--WFSScriptProcessorPrint CR 28537
--WFSScriptProcessorIndex OLTA.IMSInterfaceQueue.IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType
CREATE CLUSTERED INDEX [IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType] ON [OLTA].[IMSInterfaceQueue] 
(
	[QueueStatus] ASC,
	[QueueDataStatus] ASC,
	[QueueType] ASC
)
