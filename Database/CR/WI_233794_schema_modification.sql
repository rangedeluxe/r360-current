--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 233794
--WFSScriptProcessorPrint Renaming obsolete (deprecated) objects
--WFSScriptProcessorCRBegin
IF OBJECT_ID('RecHubData.ClientAccountsDataEntryColumns') IS NOT NULL
	OR OBJECT_ID('RecHubData.dimDataEntryColumns') IS NOT NULL
	OR OBJECT_ID('RecHubData.factDataEntrySummary') IS NOT NULL
	OR OBJECT_ID('DITStaging.DataImportWorkDataEntryColumnKeys') IS NOT NULL
BEGIN
	IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'DeprecatedObjects')
	BEGIN
		RAISERROR('Creating DeprecatedObjects schema',10,1) WITH NOWAIT;
		EXEC ('CREATE SCHEMA DeprecatedObjects AUTHORIZATION DBO');
	END

	IF OBJECT_ID('RecHubData.ClientAccountsDataEntryColumns') IS NOT NULL
	BEGIN
		RAISERROR('Transferring RecHubData.ClientAccountsDataEntryColumns to DeprecatedObjects schema',10,1) WITH NOWAIT;
		ALTER SCHEMA DeprecatedObjects TRANSFER RecHubData.ClientAccountsDataEntryColumns;
	END

	IF OBJECT_ID('RecHubData.dimDataEntryColumns') IS NOT NULL
	BEGIN
		RAISERROR('Transferring RecHubData.dimDataEntryColumns to DeprecatedObjects schema',10,1) WITH NOWAIT;
		ALTER SCHEMA DeprecatedObjects TRANSFER RecHubData.dimDataEntryColumns;
	END

	IF OBJECT_ID('RecHubData.factDataEntrySummary') IS NOT NULL
	BEGIN
		RAISERROR('Transferring RecHubData.factDataEntrySummary to DeprecatedObjects schema',10,1) WITH NOWAIT;
		ALTER SCHEMA DeprecatedObjects TRANSFER RecHubData.factDataEntrySummary;
	END

	IF OBJECT_ID('DITStaging.DataImportWorkDataEntryColumnKeys') IS NOT NULL
	BEGIN
		RAISERROR('Dropping deprecated table DITStaging.DataImportWorkDataEntryColumnKeys',10,1) WITH NOWAIT;
		DROP TABLE DITStaging.DataImportWorkDataEntryColumnKeys;
	END
END
--WFSScriptProcessorCREnd

