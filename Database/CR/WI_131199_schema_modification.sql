--WFSScriptProcessorPrint WI 131199
--WFSScriptProcessorPrint Updating factChecks if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID(N'[RecHubData].[factChecks]') AND name = N'DDAKey' )
BEGIN
	IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[RecHubData].[factChecks]') )
	BEGIN
		RAISERROR('WI 71800 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factChecks', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'factChecks';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every check.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51372 JPB	Renamed index and added INCLUDE columns.
* 04/05/2012 CR 51866 JPB	Added new index to improve remittence search.
* 03/26/2012 CR 51541 JPB	Added BatchPaymentTypeKey.
* 04/06/2012 CR 51416 JPB	Added new index to improve search performance.
* 07/05/2012 CR 53625 JPB	Added BatchCueID.
* 07/16/2012 CR 54121 JPB	Added BatchNumber.
* 07/17/2012 CR 54131 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54132 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54133 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54206 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71800 JPB	2.0 release. Change Schema name.
*							Adding columns: factCheckKey, IsDeleted, NumericRoutingNumber,
*							NumericSerial, RoutingNumber, Account, RemitterName.
*							Renaming columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey.
*							Remove Columns: ImageInfoXML, RemitterKey.
*							Added factCheckKey to ClusteredIndex.
*							Renamed indexes to match schema and column changes	
*							Create New Index based from old clustered index.				
*							Forward Patch: WI 87430
* 02/28/2014 WI 131199 JBS	Add DDAKey, including FK constraint and Index
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factChecks
		
		RAISERROR('Creating Column RecHubData.factChecks.DDAKey',10,1) WITH NOWAIT;

		ALTER TABLE RecHubData.factChecks
			ADD DDAKey		INT NULL;
		
		RAISERROR('Creating Foreign Key Constraint for RecHubData.factChecks.DDAKey',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factChecks ADD
				CONSTRAINT FK_factChecks_dimDDAs FOREIGN KEY(DDAKey) REFERENCES RecHubData.dimDDAs(DDAKey);

		RAISERROR('Creating index RecHubData.factChecks.IDX_factChecks_DDAKey',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factChecks_DDAKey ON RecHubData.factChecks (DDAKey) $(OnDataPartition);

	END
END
ELSE
	RAISERROR('WI 131199 has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd