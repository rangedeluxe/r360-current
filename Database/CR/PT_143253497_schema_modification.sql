--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #143253497
--WFSScriptProcessorPrint Remove SessionMaintenance schema stored procedures and tables if necessary.
--WFSScriptProcessorCRBegin
--Stored Procedures

RAISERROR('Dropping stored procedures for SessionMaintenance Schema',10,1) WITH NOWAIT;

IF OBJECT_ID('SessionMaintenance.usp_CopySession') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CopySession;
IF OBJECT_ID('SessionMaintenance.usp_CopySessionActivityLog') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CopySessionActivityLog;
IF OBJECT_ID('SessionMaintenance.usp_CopySessionClientAccountEntitlements') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CopySessionClientAccountEntitlements;
IF OBJECT_ID('SessionMaintenance.usp_CopySessionLog') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CopySessionLog;
IF OBJECT_ID('SessionMaintenance.usp_CreateDefaultConstraints') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CreateDefaultConstraints;
IF OBJECT_ID('SessionMaintenance.usp_CreateExtendedProperties') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CreateExtendedProperties;
IF OBJECT_ID('SessionMaintenance.usp_CreateForeignKeys') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CreateForeignKeys;
IF OBJECT_ID('SessionMaintenance.usp_CreateIndexes') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CreateIndexes;
IF OBJECT_ID('SessionMaintenance.usp_CreateTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_CreateTables;
IF OBJECT_ID('SessionMaintenance.usp_DropDefaultConstraints') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_DropDefaultConstraints;
IF OBJECT_ID('SessionMaintenance.usp_DropExtendedProperties') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_DropExtendedProperties;
IF OBJECT_ID('SessionMaintenance.usp_DropForeignKeys') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_DropForeignKeys;
IF OBJECT_ID('SessionMaintenance.usp_DropIndexes') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_DropIndexes;
IF OBJECT_ID('SessionMaintenance.usp_DropTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_DropTables;
IF OBJECT_ID('SessionMaintenance.usp_GenerateDefaultConstraintSQL') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateDefaultConstraintSQL;
IF OBJECT_ID('SessionMaintenance.usp_GenerateExtendedPropertySQL') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateExtendedPropertySQL;
IF OBJECT_ID('SessionMaintenance.usp_GenerateForeignKeySQL') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateForeignKeySQL;
IF OBJECT_ID('SessionMaintenance.usp_GenerateIndexSQL') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateIndexSQL;
IF OBJECT_ID('SessionMaintenance.usp_GenerateTableColumns') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateTableColumns;
IF OBJECT_ID('SessionMaintenance.usp_GenerateTableSQL') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GenerateTableSQL;
IF OBJECT_ID('SessionMaintenance.usp_GetTableColumns') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_GetTableColumns;
IF OBJECT_ID('SessionMaintenance.usp_RecoverTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_RecoverTables;
IF OBJECT_ID('SessionMaintenance.usp_TableList_Ins') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_TableList_Ins;
IF OBJECT_ID('SessionMaintenance.usp_TransferTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_TransferTables;
IF OBJECT_ID('SessionMaintenance.usp_TransferTablesBack') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_TransferTablesBack;
IF OBJECT_ID('SessionMaintenance.usp_TruncateWorkTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_TruncateWorkTables;
IF OBJECT_ID('SessionMaintenance.usp_UpdateCompletedTables') IS NOT NULL
	DROP PROCEDURE SessionMaintenance.usp_UpdateCompletedTables;

RAISERROR('Dropping tables for SessionMaintenance Schema',10,1) WITH NOWAIT;
IF OBJECT_ID('SessionMaintenance.TableColumns') IS NOT NULL
	DROP TABLE SessionMaintenance.TableColumns;
IF OBJECT_ID('SessionMaintenance.TableList') IS NOT NULL
	DROP TABLE SessionMaintenance.TableList;
IF OBJECT_ID('SessionMaintenance.DefaultConstraintSQL') IS NOT NULL
	DROP TABLE SessionMaintenance.DefaultConstraintSQL;
IF OBJECT_ID('SessionMaintenance.ExtendedPropertySQL') IS NOT NULL
	DROP TABLE SessionMaintenance.ExtendedPropertySQL;
IF OBJECT_ID('SessionMaintenance.ForeignKeySQL') IS NOT NULL
	DROP TABLE SessionMaintenance.ForeignKeySQL;
IF OBJECT_ID('SessionMaintenance.IndexSQL') IS NOT NULL
	DROP TABLE SessionMaintenance.IndexSQL;
IF OBJECT_ID('SessionMaintenance.TableSQL') IS NOT NULL
	DROP TABLE SessionMaintenance.TableSQL;
--WFSScriptProcessorCREnd
