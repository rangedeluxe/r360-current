--WFSScriptProcessorPrint CR 55576
--WFSScriptProcessorPrint Adding MostRecent to OLTA.dimImageRPSAliasMappings if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='dimImageRPSAliasMappings' AND COLUMN_NAME='MostRecent')
BEGIN
	RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimImageRPSAliasMappings' AND CONSTRAINT_NAME='PK_dimImageRPSAliasMappings' )
		ALTER TABLE OLTA.dimImageRPSAliasMappings DROP CONSTRAINT PK_dimImageRPSAliasMappings

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimImageRPSAliasMappings_ModificationDate')
		ALTER TABLE OLTA.dimImageRPSAliasMappings DROP CONSTRAINT DF_dimImageRPSAliasMappings_ModificationDate

	RAISERROR('Rebuilding OLTA.dimImageRPSAliasMappings.',10,1) WITH NOWAIT;
	EXEC sp_rename 'OLTA.dimImageRPSAliasMappings', 'OLDdimImageRPSAliasMappings';

	CREATE TABLE OLTA.dimImageRPSAliasMappings
	(
		ImageRPSAliasMappingKey INT NOT NULL IDENTITY(1,1) 
			CONSTRAINT PK_dimImageRPSAliasMappings PRIMARY KEY CLUSTERED,
		SiteBankID INT NOT NULL,
		SiteLockboxID INT NOT NULL,
		ExtractType TINYINT NOT NULL,
		DocType TINYINT NOT NULL,
		MostRecent BIT NOT NULL,
		LoadDate DATETIME NOT NULL,
		ModificationDate DATETIME NOT NULL
			CONSTRAINT DF_dimImageRPSAliasMappings_ModificationDate DEFAULT GETDATE(),
		FieldType CHAR(1) NOT NULL,
		AliasName VARCHAR(256) NOT NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimImageRPSAliasMappings', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'dimImageRPSAliasMappings';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2011
*
* Purpose: Stores ImageRPS Alias names to OLTA table/columns. 
*
*
* Modification History
* 04/04/2011 CR 33683 JPB	Created
* 09/10/2012 CR 55576 JPB	Added MostRecent
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = dimImageRPSAliasMappings;
	
	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT OLTA.dimImageRPSAliasMappings ON;
	
	INSERT INTO OLTA.dimImageRPSAliasMappings
	(	
		ImageRPSAliasMappingKey,
		SiteBankID,
		SiteLockboxID,
		ExtractType,
		DocType,
		MostRecent,
		LoadDate,
		ModificationDate,
		FieldType,
		AliasName
	)
	SELECT 	ImageRPSAliasMappingKey,
			SiteBankID,
			SiteLockboxID,
			ExtractType,
			DocType,
			1, --MostRecent
			LoadDate,
			ModificationDate,
			FieldType,
			AliasName
	FROM	OLTA.OLDdimImageRPSAliasMappings;

	SET IDENTITY_INSERT OLTA.dimImageRPSAliasMappings OFF;
	
	RAISERROR('Creating Index OLTA.dimImageRPSAliasMappings.IDX_dimImageRPSAliasMappings_SiteBankIDSiteLockboxIDDocTypeAliasName.',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimImageRPSAliasMappings_SiteBankIDSiteLockboxIDDocTypeAliasName ON OLTA.dimImageRPSAliasMappings (SiteBankID,SiteLockboxID,DocType,AliasName);

	IF OBJECT_ID('OLTA.OLDdimImageRPSAliasMappings') IS NOT NULL
		DROP TABLE OLTA.OLDdimImageRPSAliasMappings;

END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
