--WFSScriptProcessorPrint WI 139983
--WFSScriptProcessorPrint Changing DecisioningTransactions table name if necessary.
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorCRBegin
SET ARITHABORT ON
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'DecisioningTransactions' AND COLUMN_NAME = 'TransactionStatusKey')
BEGIN
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'DecisioningTransactions' ) 
	BEGIN
		RAISERROR('WI 101864 must be applied before this WI.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('ReBuilding Table RecHubException.DecisioningTransactions.',10,1) WITH NOWAIT
			
		RAISERROR('Dropping Foreign Keys.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='DecisioningTransactions' AND CONSTRAINT_NAME='FK_DecisioningTransactions_Users' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT FK_DecisioningTransactions_Users

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='DecisioningTransactions' AND CONSTRAINT_NAME='FK_DecisioningTransactions_CommonExceptions' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT FK_DecisioningTransactions_CommonExceptions
		

		RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='DecisioningTransactions' AND CONSTRAINT_NAME='PK_DecisioningTransactions' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT PK_DecisioningTransactions

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'RecHubException.DecisioningTransactions') AND sys.objects.name = 'DF_DecisioningTransactions_CreationDate' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT DF_DecisioningTransactions_CreationDate

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'RecHubException.DecisioningTransactions') AND sys.objects.name = 'DF_DecisioningTransactions_ModificationDate' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT DF_DecisioningTransactions_ModificationDate
		
		RAISERROR('Dropping Indexes.',10,1) WITH NOWAIT
		IF EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'IDX_DecisioningTransactions_UserCommonExceptionID')
			DROP INDEX IDX_DecisioningTransactions_UserCommonExceptionID 
				ON RecHubException.DecisioningTransactions;

		RAISERROR('Building RecHubException.DecisioningTransactions.',10,1) WITH NOWAIT
		EXEC sp_rename 'RecHubException.DecisioningTransactions', 'OLD_DecisioningTransactions'

		CREATE TABLE RecHubException.DecisioningTransactions 
		(
			DecisioningTransactionKey  BIGINT NOT NULL IDENTITY(1,1) 
				CONSTRAINT PK_DecisioningTransactions PRIMARY KEY CLUSTERED,
			TransactionKey			BIGINT,
			IntegraPAYExceptionKey	BIGINT,
			UserID					INT,
			TransactionStatusKey	SMALLINT NOT NULL,
			TransactionBeginWork	DATETIME NOT NULL,
			TransactionReset		BIT	NOT NULL,
			TransactionEndWork		DATETIME,
			CreationDate		DATETIME NOT NULL
				CONSTRAINT DF_DecisioningTransactions_CreationDate DEFAULT GETDATE(),
			ModificationDate	DATETIME NOT NULL
				CONSTRAINT DF_decisioningTransactions_ModificationDate DEFAULT GETDATE(),
			ModifiedBy			VARCHAR(128) NOT NULL
		);
		
		RAISERROR('Updating RecHubException.DecisioningTransactions table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubException', 'TABLE', 'DecisioningTransactions', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubException',
				@level1type = N'TABLE',
				@level1name = N'DecisioningTransactions';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/13/2013
*
* Purpose: Holds data Decisioning Transactions.
*
* Modification History
* 05/13/2013 WI 101864 JBS	Created 
* 05/06/2014 WI 139983 JBS  Add FK to Transactions, TransactionStatus and IntegraPAYExceptions tables
*							change Key field name to DecisioningTransactionKey, Add ModifiedBy column
*							Removing Unique index IDX_DecisioningTransactions_UserCommonExceptionID
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubException,
		@level1type = N'TABLE',@level1name = DecisioningTransactions

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT RecHubException.DecisioningTransactions ON

		INSERT INTO RecHubException.DecisioningTransactions 
		(
			DecisioningTransactionKey,
			IntegraPAYExceptionKey,
			UserID,
			TransactionStatusKey,
			TransactionBeginWork,
			TransactionReset,
			TransactionEndWork,
			CreationDate,
			ModificationDate,
			ModifiedBy		
		)
		SELECT	
			RecHubException.OLD_DecisioningTransactions.DecisioningTransactionID,
			RecHubException.OLD_DecisioningTransactions.CommonExceptionID,
			RecHubException.OLD_DecisioningTransactions.UserID,
			CASE RecHubException.IntegraPAYExceptions.DecisioningStatus 
				WHEN 0 THEN 1
				WHEN 1 THEN 2
				WHEN 2 THEN 3
				WHEN 3 THEN 2
			END AS TransactionStatusKey,
			TransactionBeginWork,
			TransactionReset,
			TransactionEndWork,
			RecHubException.OLD_DecisioningTransactions.CreationDate,
			RecHubException.OLD_DecisioningTransactions.ModificationDate,
			SUSER_SNAME()
		FROM	
			RecHubException.OLD_DecisioningTransactions
				INNER JOIN RecHubException.IntegraPAYExceptions 
					ON RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.OLD_DecisioningTransactions.CommonExceptionID
					

		SET IDENTITY_INSERT RecHubException.DecisioningTransactions OFF

		RAISERROR('Rebuilding Foreign Keys.',10,1) WITH NOWAIT;
		ALTER TABLE RecHubException.DecisioningTransactions ADD CONSTRAINT FK_DecisioningTransactions_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
		ALTER TABLE RecHubException.DecisioningTransactions ADD CONSTRAINT FK_DecisioningTransactions_IntegraPAYExceptions FOREIGN KEY (IntegraPAYExceptionKey) REFERENCES RecHubException.IntegraPAYExceptions(IntegraPAYExceptionKey);
		ALTER TABLE RecHubException.DecisioningTransactions ADD CONSTRAINT FK_DecisioningTransactions_Transactions FOREIGN KEY (TransactionKey) REFERENCES RecHubException.Transactions(TransactionKey);
		ALTER TABLE RecHubException.DecisioningTransactions ADD CONSTRAINT FK_DecisioningTransactions_TransactionStatuses FOREIGN KEY (TransactionStatusKey) REFERENCES RecHubException.TransactionStatuses(TransactionStatusKey);

		RAISERROR('Creating Indexes.',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_DecisioningTransactions_IntegraPAYExceptionKey ON RecHubException.DecisioningTransactions
		(
			IntegraPAYExceptionKey ASC
		);
		--WFSScriptProcessorIndex RecHubException.DecisioningTransactions.IDX_DecisioningTransactions_TransactionKey
		CREATE NONCLUSTERED INDEX IDX_DecisioningTransactions_TransactionKey ON RecHubException.DecisioningTransactions
		(
			TransactionKey ASC
		);

		IF OBJECT_ID('RecHubException.OLD_DecisioningTransactions') IS NOT NULL
			DROP TABLE RecHubException.OLD_DecisioningTransactions;
	END
END
ELSE
	RAISERROR('WI 139983 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
