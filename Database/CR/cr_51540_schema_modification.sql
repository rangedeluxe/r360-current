--WFSScriptProcessorPrint CR 51540
--WFSScriptProcessorPrint Adding BatchPaymentTypeKey to OLTA.factBatchSummary if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factBatchSummary' AND COLUMN_NAME = 'BatchPaymentTypeKey' )
BEGIN /* verify that CR 48978 has been applied before continuing. */
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factBatchSummary' AND COLUMN_NAME = 'BatchSiteCode' )
	BEGIN
		RAISERROR('CR 48978 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimBanks_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimBanks_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimCustomers_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimCustomers_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimLockboxes_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_ProcessingDate_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_ProcessingDate_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_SourceProcessingDate_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_DepositDate_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_DepositDate_factBatchSummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factBatchSummary' )
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimBatchSources_factBatchSummary

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factBatchSummary_ModificationDate ')
			ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT DF_factBatchSummary_ModificationDate

		RAISERROR('Rebuilding OLTA.factBatchSummary adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factBatchSummary', 'OLDfactBatchSummary'

		CREATE TABLE OLTA.factBatchSummary
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
			ProcessingDateKey int NOT NULL, --CR 28219 JPB 11/13/2009
			SourceProcessingDateKey int NOT NULL, --CR 49274 JPB 01/11/2012
			GlobalBatchID int NOT NULL,
			BatchID int NOT NULL,
			DESetupID int NULL,
			DepositDDA varchar(40) NULL,
			SystemType tinyint NOT NULL,
			DepositStatus int NOT NULL,
			DepositStatusDisplayName varchar(80) NOT NULL,
			TransactionCount int NOT NULL,
			CheckCount int NOT NULL,
			DocumentCount int NOT NULL,
			ScannedCheckCount int NOT NULL,
			StubCount int NOT NULL,
			CheckTotal money NOT NULL,
			BatchSourceKey tinyint NOT NULL, --CR 32284 JPB 01/10/2011
			BatchPaymentTypeKey tinyint NOT NULL --CR 51540 JPB 03/26/2012
				CONSTRAINT DF_factBatchSummary_BatchPaymentTypeKey DEFAULT(0),
			BatchSiteCode int NULL, --CR 48978 JPB 01/19/2012
			LoadDate datetime NOT NULL,
			ModificationDate datetime NOT NULL --CR 28982 JPB 02/10/2010
				CONSTRAINT DF_factBatchSummary_ModificationDate DEFAULT GETDATE()
		) $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factBatchSummary ADD 
			CONSTRAINT FK_dimBanks_factBatchSummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factBatchSummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factBatchSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_ProcessingDate_factBatchSummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_DepositDate_factBatchSummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factBatchSummary FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimBatchSources_factBatchSummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey),
			CONSTRAINT FK_dimBatchPaymentTypes_factBatchSummary FOREIGN KEY(BatchPaymentTypeKey) REFERENCES OLTA.dimBatchPaymentTypes(BatchPaymentTypeKey)
			
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factBatchSummary_DepositDateKey ON OLTA.factBatchSummary (DepositDateKey,LockboxKey, BatchID) $(OnPartition)
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factBatchSummary_BankKey ON OLTA.factBatchSummary (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factBatchSummary_LockboxKey ON OLTA.factBatchSummary (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factBatchSummary_CustomerKey ON OLTA.factBatchSummary (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_ProcessingDate',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factBatchSummary_ProcessingDate ON OLTA.factBatchSummary (ProcessingDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factBatchSummary.IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID ON OLTA.factBatchSummary (BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,BatchID) $(OnPartition) -- (CR 28128 - 11/09/2009 JPB)
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factBatchSummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factBatchSummary';		
		
	EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
* 01/11/2012 CR 49274 JPB	Added SourceProcessingDateKey.
* 01/19/2012 CR 48978 JPB	Added BatchSiteCode.
* 03/26/2012 CR 51540 JPB	Added BatchPaymentTypeKey
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factBatchSummary
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factBatchSummary
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			GlobalBatchID,
			BatchID,
			DESetupID,
			DepositDDA,
			SystemType,
			DepositStatus,
			DepositStatusDisplayName,
			TransactionCount,
			CheckCount,
			DocumentCount,
			ScannedCheckCount,
			StubCount,
			CheckTotal,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				GlobalBatchID,
				BatchID,
				DESetupID,
				DepositDDA,
				SystemType,
				DepositStatus,
				DepositStatusDisplayName,
				TransactionCount,
				CheckCount,
				DocumentCount,
				ScannedCheckCount,
				StubCount,
				CheckTotal,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactBatchSummary

		IF OBJECT_ID('OLTA.OLDfactBatchSummary') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factBatchSummary table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactBatchSummary
		END
	END
END
ELSE
BEGIN
	RAISERROR('CR already applied to the database.',10,1) WITH NOWAIT 
END
--WFSScriptProcessorCREnd

