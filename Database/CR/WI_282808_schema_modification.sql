--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 282808
--WFSScriptProcessorPrint Add IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID to dimWorkgroupDataEntryColumns if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE NAME='IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID' )
BEGIN

	RAISERROR('Adding index IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID ON RecHubData.dimWorkgroupDataEntryColumns
	(
		SiteBankID,
		SiteClientAccountID
	)
	INCLUDE
	(
		IsCheck,
		DataType,
		SourceDisplayName
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimWorkgroupDataEntryColumns', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimWorkgroupDataEntryColumns';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimWorkgroupDataEntryColumns,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/06/2015
*
* Purpose: Workgoup Data Entry Columns dimension is a type 0 SCD holding  
*	an entry for each Workgroup/BatchSource data entry column. 
*		   
*
* Modification History
* 07/06/2015 WI 221698 JPB	Created
* 05/25/2016 WI 282808 JPB	Added new index for OTIS import performance
******************************************************************************/';
END
ELSE
	RAISERROR('WI 282808 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
