--WFSScriptProcessorPrint CR 51367
--WFSScriptProcessorPrint Updating IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence in OLTA.factTransactionSummary if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT	1
					FROM	sys.tables
							INNER JOIN sys.indexes ON sys.tables.[object_id] = sys.indexes.[object_id]
							INNER JOIN sys.index_columns ON sys.index_columns.[object_id] = sys.indexes.[object_id] and sys.index_columns.[index_id] = sys.indexes.[index_id]
							INNER JOIN sys.all_columns ON sys.index_columns.[object_id] = sys.all_columns.[object_id] and sys.index_columns.[column_id] = sys.all_columns.[column_id]
					WHERE	sys.tables.[name] = 'factTransactionSummary' AND sys.indexes.[name] = 'IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence' AND sys.all_columns.[name] = 'CheckCount'
				)
BEGIN
	/* verify that CR 49552 has been applied before continuing. */
	IF NOT EXISTS (	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factTransactionSummary' AND COLUMN_NAME = 'BatchSiteCode' )
	BEGIN
		RAISERROR('CR 49552 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Index OLTA.factTransactionSummary.IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence.',10,1) WITH NOWAIT	
		DROP INDEX IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence ON OLTA.factTransactionSummary;
		RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence.',10,1) WITH NOWAIT	
		CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence ON OLTA.factTransactionSummary
		(
			[LockboxKey] ASC,
			[DepositDateKey] ASC,
			[BatchID] ASC,
			[DepositStatus] ASC,
			[BankKey] ASC,
			[CustomerKey] ASC,
			[ProcessingDateKey] ASC,
			[TransactionID] ASC,
			[TxnSequence] ASC
		)
		INCLUDE 
		( 
			[CheckCount],
			[DocumentCount],
			[ScannedCheckCount],
			[StubCount],
			[OMRCount],
			[CheckTotal]
		) $(OnPartition)
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factTransactionSummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factTransactionSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28224 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 03/11/2010 CR 29181 JPB	Added new index for Lockbox Search.
* 04/12/2010 CR 29358 JPB	Added ModificationDate.
* 01/12/2011 CR 32285 JPB	Added BatchSourceKey.
* 10/27/2011 CR 47513 JPB	Added TxnSequence to index.
* 01/12/2012 CR 49275 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49552 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51367 JPB	Added INCLUDE columns.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factTransactionSummary
	END
END
ELSE
	RAISERROR('CR has already been applied to this database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
