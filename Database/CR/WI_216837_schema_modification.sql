--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 216837
--WFSScriptProcessorPrint Adding index IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive') 
	BEGIN
		RAISERROR('WI 216837 Already applied. Rebuilding index',10,1) WITH NOWAIT;  -- however we will rebuild index
		DROP INDEX IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive ON RecHubData.dimClientAccounts;

		CREATE INDEX IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive ON RecHubData.dimClientAccounts
		(
			SiteCodeID, 
			MostRecent, 
			IsActive
		) 
		INCLUDE 
		(
			ClientAccountKey, 
			SiteBankID, 
			SiteClientAccountID, 
			CutOff, 
			IsCommingled, 
			SiteClientAccountKey, 
			ShortName, 
			LongName
		);
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive',10,1) WITH NOWAIT;
		CREATE INDEX IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive ON RecHubData.dimClientAccounts
		(
			SiteCodeID, 
			MostRecent, 
			IsActive
		) 
		INCLUDE 
		(
			ClientAccountKey, 
			SiteBankID, 
			SiteClientAccountID, 
			CutOff, 
			IsCommingled, 
			SiteClientAccountKey, 
			ShortName, 
			LongName
		);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimClientAccounts', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'dimClientAccounts';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = dimClientAccounts,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Lockboxes dimension is a SCD type 2 holding Lockbox info.  Most 
*	recent flag of 1 indicates the current Lockbox row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 05/05/2010 CR 29158 JPB	Added IsCommingled.
* 05/13/2010 CR 29709 JPB	Added SiteLockboxKey.
* 08/02/2010 CR 30378 JPB	Added standard ModificationDate.
* 08/02/2010 CR 30307 JPB	Added default contraint 1 to OnlineColorMode.
* 01/07/2011 CR 31470 JPB 	Added POBox
* 09/11/2012 CR 55090 JPB	Added FileGroup
* 11/01/2012 CR 56617 JPB	Added new index.
* 03/01/2013 WI 89985 JBS	Update table to 2.0 release. Change Schema Name
*							Renamed all Constraints, Renamed Columns:
*							LockboxKey to ClientAccountKey
*							SiteCustomerID to SiteOrganizationID
*							SiteLockboxID to SiteClientAccountID
*							LoadDate to CreationDate
*							SiteLockboxKey to SiteClientAccountKey 
*							Renamed SiteCode to SiteCodeID, add FK to dimSiteCodes
*							FP: WI 85044, WI 85094, WI 87239
* 05/07/2013 WI 96817 JBS	Added columns for Purging process:
*							DataRetentionDays,  ImageRetentionDays
* 06/03/2015 WI 216837 JBS	Add index IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive
******************************************************************************/';

	END
--WFSScriptProcessorCREnd