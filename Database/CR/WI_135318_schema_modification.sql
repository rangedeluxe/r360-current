--WFSScriptProcessorPrint WI 135318
--WFSScriptProcessorPrint Updating RecHubData.factDocumentImages.BatchID if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factDocumentImages' AND COLUMN_NAME = 'BatchID' AND DATA_TYPE = 'bigint' )
BEGIN
	RAISERROR('Updating RecHubData.factDocumentImages.BatchID',10,1) WITH NOWAIT;
	ALTER TABLE RecHubData.factDocumentImages ALTER COLUMN BatchID BIGINT;

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factDocumentImages', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'factDocumentImages';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/17/2013
*
* Purpose: Grain: one row for every Document Image.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/05/2013 WI 102409 JBS	Created
* 04/09/2014 WI 135318 JPB	Changed BatchID from INT to BIGINT.
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = factDocumentImages;
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
