--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #136913453
--WFSScriptProcessorPrint Remove renamed invoice stored procedures if necessary.
--WFSScriptProcessorCRBegin
--Stored Procedures
IF OBJECT_ID('RecHubData.usp_InvoiceSearch') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_InvoiceSearch',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubData.usp_InvoiceSearch
END
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_GetClientAccountList') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_InvoiceSearch_GetClientAccountList',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_GetClientAccountList
END
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_GetSearchRequest') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_InvoiceSearch_GetSearchRequest',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_GetSearchRequest
END
--WFSScriptProcessorCREnd
