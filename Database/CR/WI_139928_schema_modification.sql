--WFSScriptProcessorPrint WI 139928
--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions Schema="RecHubData" Object="dimBatchSources">
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorPrint Update RecHubData.dimBatchSources if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimBatchSources' AND COLUMN_NAME='EntityID' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_factBatchSummary_dimBatchSources' )
		ALTER TABLE RecHubData.factBatchSummary DROP CONSTRAINT FK_factBatchSummary_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_factChecks_dimBatchSources' )
		ALTER TABLE RecHubData.factChecks DROP CONSTRAINT FK_factChecks_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factCheckImages' AND CONSTRAINT_NAME='FK_factCheckImages_dimBatchSources' )
		ALTER TABLE RecHubData.factCheckImages DROP CONSTRAINT FK_factCheckImages_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_factDataEntryDetails_dimBatchSources' )
		ALTER TABLE RecHubData.factDataEntryDetails DROP CONSTRAINT FK_factDataEntryDetails_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_factDataEntrySummary_dimBatchSources' )
		ALTER TABLE RecHubData.factDataEntrySummary DROP CONSTRAINT FK_factDataEntrySummary_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_factDocuments_dimBatchSources' )
		ALTER TABLE RecHubData.factDocuments DROP CONSTRAINT FK_factDocuments_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDocumentImages' AND CONSTRAINT_NAME='FK_factDocumentImages_dimBatchSources' )
		ALTER TABLE RecHubData.factDocumentImages DROP CONSTRAINT FK_factDocumentImages_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_factStubs_dimBatchSources' )
		ALTER TABLE RecHubData.factStubs DROP CONSTRAINT FK_factStubs_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_factTransactionSummary_dimBatchSources' )
		ALTER TABLE RecHubData.factTransactionSummary DROP CONSTRAINT FK_factTransactionSummary_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotifications' AND CONSTRAINT_NAME='FK_factNotifications_dimBatchSources' )
		ALTER TABLE RecHubData.factNotifications DROP CONSTRAINT FK_factNotifications_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotificationFiles' AND CONSTRAINT_NAME='FK_factNotificationFiles_dimBatchSources' )
		ALTER TABLE RecHubData.factNotificationFiles DROP CONSTRAINT FK_factNotificationFiles_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimItemDataSetupFields' AND CONSTRAINT_NAME='FK_dimItemDataSetupFields_dimBatchSources' )
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT FK_dimItemDataSetupFields_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='CommonExceptions' AND CONSTRAINT_NAME='FK_CommonExceptions_dimBatchSources' )
		ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT FK_CommonExceptions_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='EventLog' AND CONSTRAINT_NAME='FK_EventLog_dimBatchSources' )
		ALTER TABLE RecHubAlert.EventLog DROP CONSTRAINT FK_EventLog_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factRawPaymentData' AND CONSTRAINT_NAME='FK_factRawPaymentData_dimBatchSources' )
		ALTER TABLE RecHubData.factRawPaymentData DROP CONSTRAINT FK_factRawPaymentData_dimBatchSources;

	RAISERROR('Dropping dimBatchSources contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimBatchSources' AND CONSTRAINT_NAME='PK_dimBatchSources' )
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT PK_dimBatchSources;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchSources_CreatedBy')
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT DF_dimBatchSources_CreatedBy;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchSources_CreationDate')
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT DF_dimBatchSources_CreationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchSources_IsActive')
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT DF_dimBatchSources_IsActive;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchSources_ModificationDate')
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT DF_dimBatchSources_ModificationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBatchSources_ModifiedBy')
		ALTER TABLE RecHubData.dimBatchSources DROP CONSTRAINT DF_dimBatchSources_ModifiedBy;

	RAISERROR('Rebuilding RecHubData.dimBatchSources',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.dimBatchSources', 'OLDdimBatchSources';

	CREATE TABLE RecHubData.dimBatchSources
	(
		BatchSourceKey SMALLINT NOT NULL IDENTITY(103,1)
			CONSTRAINT PK_dimBatchSources PRIMARY KEY CLUSTERED,
		IsActive BIT NOT NULL
			CONSTRAINT DF_dimBatchSources_IsActive DEFAULT(0),
		ImportTypeKey TINYINT NOT NULL,
		CreationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimBatchSources_CreationDate DEFAULT(GETDATE()),
		CreatedBy VARCHAR(128) NOT NULL 
			CONSTRAINT DF_dimBatchSources_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimBatchSources_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy VARCHAR(128) NOT NULL 
			CONSTRAINT DF_dimBatchSources_ModifiedBy DEFAULT(SUSER_SNAME()),
		EntityID INT NULL,
		ShortName VARCHAR(30) NOT NULL,
		LongName VARCHAR(128) NOT NULL
	);	

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimBatchSources', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimBatchSources';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/10/2011
*
* Purpose: Batch Sources dimension is a static data dimension for batch sources. 
*
* Modification History
* 01/10/2011 CR 32310 JPB	Created
* 03/01/2011 CR 33123 JPB	Removed IDENTITY property from BatchSourceKey.
* 01/24/2012 CR 49629 JPB	Added standard Creation/Modification columns,
*							removed LoadDate.
* 03/01/2013 WI 89976 JBS	Update table to 2.0 release.
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
* 06/13/2013 WI 105327 JPB	Added IsActive.
* 03/11/2014 WI 132464 JBS	Adding permission tags for table
* 05/22/2014 WI 139928 JPB	Changes to support RAAM.
*							-BatchSourceKey to SMALLINT IDENTITY
*							-Added EntityID
*							-Added SystemType
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimBatchSources;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
	SET IDENTITY_INSERT RecHubData.dimBatchSources ON;
	
	;WITH dimBatchSources_CTE AS
	(
		SELECT	
			BatchSourceKey,
			IsActive,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy,
			ShortName,
			LongName,
			CASE BatchSourceKey
				WHEN 0 THEN 'Unknown'
				WHEN 1 THEN 'integraPAY'
				WHEN 2 THEN 'integraPAY'
				WHEN 3 THEN 'integraPAY'
				ELSE 'DIT'
			END AS ImportShortName
		FROM 	
			RecHubData.OLDdimBatchSources
		WHERE 
			ShortName NOT LIKE 'Reserved%'
	)
	INSERT INTO RecHubData.dimBatchSources 
	(
		BatchSourceKey,
		IsActive,
		ImportTypeKey,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		ShortName,
		LongName	
	) 
	SELECT	
		dimBatchSources_CTE.BatchSourceKey,
		dimBatchSources_CTE.IsActive,
		RecHubData.dimImportTypes.ImportTypeKey,
		dimBatchSources_CTE.CreationDate,
		dimBatchSources_CTE.CreatedBy,
		dimBatchSources_CTE.ModificationDate,
		dimBatchSources_CTE.ModifiedBy,
		dimBatchSources_CTE.ShortName,
		dimBatchSources_CTE.LongName
	FROM 	
		dimBatchSources_CTE
		INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimImportTypes.ShortName = dimBatchSources_CTE.ImportShortName;

	SET IDENTITY_INSERT RecHubData.dimBatchSources OFF;

	RAISERROR('Creating index IDX_dimBatchSources_ShortName',10,1) WITH NOWAIT;
	CREATE UNIQUE INDEX IDX_dimBatchSources_ShortName ON RecHubData.dimBatchSources (ShortName) INCLUDE (IsActive);

	/* 
		Special note: Normally the FKs would be added back here. However, since the data type is being changed, all the tables that have a FK need 
		to be updated as well. Therefore, the scripts that alter the FK tables will add the FK.

		This script will be applied BEFORE the changes to the other tables are applied.
	*/

	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.OLDdimBatchSources') IS NOT NULL
		DROP TABLE RecHubData.OLDdimBatchSources;

END
ELSE
	RAISERROR('WI 139928 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
