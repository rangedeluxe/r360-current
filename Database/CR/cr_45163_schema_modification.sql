--WFSScriptProcessorPrint CR 45163
--WFSScriptProcessorPrint Updating AutoFillRemitterDuringEncode to FALSE if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS( SELECT 1 FROM dbo.SystemSetup WHERE Section = 'REMITTER' AND SetupKey = 'AutoFillRemitterDuringEncode' AND Value = 'TRUE' )
BEGIN
	UPDATE 	dbo.SystemSetup 
	SET 	Value = 'FALSE' 
	WHERE 	Section = 'REMITTER' 
			AND SetupKey = 'AutoFillRemitterDuringEncode'
END
--WFSScriptProcessorCREnd

