--WFSScriptProcessorPrint CR 54246
--WFSScriptProcessorPrint Adding OLTA.Users.ExternalIDHash if necessary.
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorCRBegin
SET ARITHABORT ON
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'Users' AND COLUMN_NAME = 'ExternalIDHash')
BEGIN
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'Users' AND COLUMN_NAME = 'FailedSecurityQuestionAttempts') 
	BEGIN
		RAISERROR('CR 47241 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Adding OLTA.Users.ExternalIDHash.',10,1) WITH NOWAIT
			
		RAISERROR('Dropping Foreign Keys.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='Users' AND CONSTRAINT_NAME='FK_Contacts_Users' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT FK_Contacts_Users

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='Session' AND CONSTRAINT_NAME='FK_Users_Sessions' )
			ALTER TABLE OLTA.[Session] DROP CONSTRAINT FK_Users_Sessions

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLUserPreferences' AND CONSTRAINT_NAME='FK_Users_OLUserPreferences' )
			ALTER TABLE OLTA.OLUserPreferences DROP CONSTRAINT FK_Users_OLUserPreferences

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLUserQuestions' AND CONSTRAINT_NAME='FK_Users_OLUserQuestions' )
			ALTER TABLE OLTA.OLUserQuestions DROP CONSTRAINT FK_Users_OLUserQuestions

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLUserMachines' AND CONSTRAINT_NAME='FK_Users_OLUserMachines' )
			ALTER TABLE OLTA.OLUserMachines DROP CONSTRAINT FK_Users_OLUserMachines

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='UserPermissions' AND CONSTRAINT_NAME='FK_UserPermissions_Users' )
			ALTER TABLE OLTA.UserPermissions DROP CONSTRAINT FK_UserPermissions_Users

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxUsers' AND CONSTRAINT_NAME='FK_Users_OLLockboxUsers' )
			ALTER TABLE OLTA.OLLockboxUsers DROP CONSTRAINT FK_Users_OLLockboxUsers

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='UserPasswordHistory' AND CONSTRAINT_NAME='FK_Users_UserPasswordHistory' )
			ALTER TABLE OLTA.UserPasswordHistory DROP CONSTRAINT FK_Users_UserPasswordHistory

		RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='Users' AND CONSTRAINT_NAME='PK_Users' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT PK_Users

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_UserType' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_UserType

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_SuperUser' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_SuperUser

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_FailedLogonAttempts' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_FailedLogonAttempts

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_FailedSecurityQuestionAttempts' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_FailedSecurityQuestionAttempts

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_CreationDate' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_CreationDate

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_CreatedBy' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_CreatedBy

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_ModificationDate' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_ModificationDate

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'OLTA.Users') AND sys.objects.name = 'DF_Users_ModifiedBy' )
			ALTER TABLE OLTA.Users DROP CONSTRAINT DF_Users_ModifiedBy

		RAISERROR('Rebuilding OLTA.Users.',10,1) WITH NOWAIT
		EXEC sp_rename 'OLTA.Users', 'OLD_Users'

		CREATE TABLE OLTA.Users 
		(
			UserID int IDENTITY(1,1) NOT NULL 
				CONSTRAINT PK_Users PRIMARY KEY CLUSTERED,
			OLCustomerID uniqueidentifier NULL,
			UserType tinyint NOT NULL 
				CONSTRAINT DF_Users_UserType DEFAULT(0),
			LogonName varchar(16) NOT NULL,
			FirstName varchar(16) NOT NULL,
			MiddleInitial varchar(3) NULL,
			LastName varchar(31) NOT NULL,
			SuperUser bit NOT NULL 
				CONSTRAINT DF_Users_SuperUser DEFAULT(0),
			Password varchar(44) NOT NULL,
			PasswordSet datetime NOT NULL,
			PasswordExpirationTime datetime NULL,
			IsFirstTime bit NOT NULL,
			FailedLogonAttempts tinyint NOT NULL 
				CONSTRAINT DF_Users_FailedLogonAttempts DEFAULT(0),
			FailedSecurityQuestionAttempts tinyint NOT NULL 
				CONSTRAINT DF_Users_FailedSecurityQuestionAttempts DEFAULT(0),
			EmailAddress varchar(50) NULL,
			IsActive bit NOT NULL,
			ContactID uniqueidentifier NULL,
			ExternalID1 varchar(32) NULL,
			ExternalID2 varchar(32) NULL,
			ExternalIDHash varchar(32) NULL,
			SetupToken varchar(36) NULL,
			CreationDate datetime NOT NULL 
				CONSTRAINT DF_Users_CreationDate DEFAULT(GETDATE()),
			CreatedBy varchar(32) NOT NULL 
				CONSTRAINT DF_Users_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate datetime NOT NULL 
				CONSTRAINT DF_Users_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy varchar(32) NOT NULL 
				CONSTRAINT DF_Users_ModifiedBy DEFAULT(SUSER_SNAME())
		)
		
		RAISERROR('Updating OLTA.Users table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'Users', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'Users';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 07/25/2011 CR 45437 JPB	Changed password to 44.
* 09/26/2011 CR 46988 JPB	Added PasswordExpirationTime.
* 10/12/2011 CR 47241 JPB	Added FailedSecurityQuestionAttempts.
* 07/23/2012 CR 54246 JPB	Added ExternalIDHash.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = Users

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT OLTA.Users ON

		INSERT INTO OLTA.Users 
		(
			UserID,
			OLCustomerID,
			UserType,
			LogonName,
			FirstName,
			MiddleInitial,
			LastName,
			SuperUser,
			Password,
			PasswordSet,
			PasswordExpirationTime,
			IsFirstTime,
			FailedLogonAttempts,
			FailedSecurityQuestionAttempts,
			EmailAddress,
			IsActive,
			ContactID,
			ExternalID1,
			ExternalID2,
			SetupToken,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
		)
		SELECT	UserID,
				OLCustomerID,
				UserType,
				LogonName,
				FirstName,
				MiddleInitial,
				LastName,
				SuperUser,
				Password,
				PasswordSet,
				PasswordExpirationTime,
				IsFirstTime,
				FailedLogonAttempts,
				FailedSecurityQuestionAttempts,
				EmailAddress,
				IsActive,
				ContactID,
				ExternalID1,
				ExternalID2,
				SetupToken,
				CreationDate,
				CreatedBy,
				ModificationDate,
				ModifiedBy
		FROM	OLTA.OLD_Users

		SET IDENTITY_INSERT OLTA.Users OFF

		RAISERROR('Rebuilding Foreign Keys.',10,1) WITH NOWAIT;
		ALTER TABLE OLTA.Users ADD CONSTRAINT FK_Contacts_Users FOREIGN KEY (ContactID) REFERENCES dbo.Contacts(ContactID)
		ALTER TABLE OLTA.[Session] WITH NOCHECK ADD CONSTRAINT FK_Users_Sessions FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)
		ALTER TABLE OLTA.OLUserPreferences ADD CONSTRAINT FK_Users_OLUserPreferences FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)
		ALTER TABLE OLTA.OLUserQuestions ADD CONSTRAINT FK_Users_OLUserQuestions FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)
		ALTER TABLE OLTA.OLUserMachines ADD CONSTRAINT FK_Users_OLUserMachines FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)
		ALTER TABLE OLTA.UserPermissions ADD CONSTRAINT FK_UserPermissions_Users FOREIGN KEY(UserID) REFERENCES OLTA.Users (UserID)
		ALTER TABLE OLTA.OLLockboxUsers ADD CONSTRAINT FK_Users_OLLockboxUsers FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)
		ALTER TABLE OLTA.UserPasswordHistory WITH NOCHECK ADD CONSTRAINT FK_Users_UserPasswordHistory FOREIGN KEY (UserID) REFERENCES OLTA.Users(UserID)

		ALTER TABLE OLTA.[Session] NOCHECK CONSTRAINT FK_Users_Sessions;
		ALTER TABLE OLTA.UserPasswordHistory NOCHECK CONSTRAINT FK_Users_UserPasswordHistory;

		
		RAISERROR('Creating index IDX_Users_ExternalIDHash.',10,1) WITH NOWAIT;
		CREATE INDEX IDX_Users_ExternalIDHash ON OLTA.Users (ExternalIDHash)
		
		IF OBJECT_ID('OLTA.OLD_Users') IS NOT NULL
			DROP TABLE OLTA.OLD_Users;
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
