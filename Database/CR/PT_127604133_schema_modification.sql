--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 127604133
--WFSScriptProcessorPrint Adding IsRequired to RecHubData.dimWorkgroupDataEntryColumns if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupDataEntryColumns' AND COLUMN_NAME='IsRequired' )
BEGIN
	IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE NAME='IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID' )
	BEGIN
		RAISERROR('WI 282808 must be applied first.',16,1) WITH NOWAIT;
	END

	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupDataEntryColumns' AND CONSTRAINT_NAME='FK_dimWorkgroupDataEntryColumns_dimBatchSources' )
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT FK_dimWorkgroupDataEntryColumns_dimBatchSources;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_factDataEntryDetails_dimWorkgroupDataEntryColumns' )
		ALTER TABLE RecHubData.factDataEntryDetails DROP CONSTRAINT FK_factDataEntryDetails_dimWorkgroupDataEntryColumns;

	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	--PK
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupDataEntryColumns' AND CONSTRAINT_NAME='PK_dimWorkgroupDataEntryColumns' )
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT PK_dimWorkgroupDataEntryColumns;

	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupDataEntryColumns_IsCheck')
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsCheck;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupDataEntryColumns_IsActive')
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsActive;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupDataEntryColumns_MarkSense')
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT DF_dimWorkgroupDataEntryColumns_MarkSense;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupDataEntryColumns_ModificationDate')
		ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns DROP CONSTRAINT DF_dimWorkgroupDataEntryColumns_ModificationDate;

	RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_dimWorkgroupDataEntryColumns_FieldName') 
		DROP INDEX RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_FieldName;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey') 
		DROP INDEX RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID') 
		DROP INDEX RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID;

	RAISERROR('Rebuilding Table RecHubData.dimWorkgroupDataEntryColumns.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.dimWorkgroupDataEntryColumns', 'OLD_dimWorkgroupDataEntryColumns';

	CREATE TABLE RecHubData.dimWorkgroupDataEntryColumns
	(
		WorkgroupDataEntryColumnKey BIGINT IDENTITY(1,1) NOT NULL 
			CONSTRAINT PK_dimWorkgroupDataEntryColumns PRIMARY KEY CLUSTERED,
		SiteBankID INT NOT NULL,
		SiteClientAccountID INT NOT NULL,
		BatchSourceKey SMALLINT NOT NULL,
		IsCheck BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsCheck DEFAULT 0,
		IsActive BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsActive DEFAULT 1,
		IsRequired BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsRequired DEFAULT 0,
		MarkSense BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupDataEntryColumns_MarkSense DEFAULT 0,
		DataType TINYINT NOT NULL,
		ScreenOrder TINYINT NOT NULL,
		CreationDate DATETIME NOT NULL,
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimWorkgroupDataEntryColumns_ModificationDate DEFAULT GETDATE(),
		UILabel NVARCHAR(64) NOT NULL,
		FieldName NVARCHAR(256) NOT NULL,
		SourceDisplayName NVARCHAR(256) NOT NULL
	);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT RecHubData.dimWorkgroupDataEntryColumns ON;
	INSERT INTO RecHubData.dimWorkgroupDataEntryColumns
	(
		WorkgroupDataEntryColumnKey,
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey,
		IsCheck,
		IsActive,
		MarkSense,
		DataType,
		ScreenOrder,
		CreationDate,
		ModificationDate,
		UILabel,
		FieldName,
		SourceDisplayName
	)
	SELECT
		WorkgroupDataEntryColumnKey,
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey,
		IsCheck,
		IsActive,
		MarkSense,
		DataType,
		ScreenOrder,
		CreationDate,
		ModificationDate,
		UILabel,
		FieldName,
		SourceDisplayName
	FROM
		RecHubData.OLD_dimWorkgroupDataEntryColumns;

	SET IDENTITY_INSERT RecHubData.dimWorkgroupDataEntryColumns OFF;

	RAISERROR('Adding Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns ADD 
		CONSTRAINT FK_dimWorkgroupDataEntryColumns_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);
	ALTER TABLE RecHubData.factDataEntryDetails ADD
		CONSTRAINT FK_factDataEntryDetails_dimWorkgroupDataEntryColumns FOREIGN KEY(WorkgroupDataEntryColumnKey) REFERENCES RecHubData.dimWorkgroupDataEntryColumns(WorkgroupDataEntryColumnKey);

	RAISERROR('Adding IDX_dimWorkgroupDataEntryColumns_FieldName',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimWorkgroupDataEntryColumns_FieldName ON RecHubData.dimWorkgroupDataEntryColumns(FieldName);
	RAISERROR('Adding IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey ON RecHubData.dimWorkgroupDataEntryColumns
	(
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey
	);		
	RAISERROR('Adding IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID ON RecHubData.dimWorkgroupDataEntryColumns
	(
		SiteBankID,
		SiteClientAccountID
	)
	INCLUDE
	(
		IsCheck,
		DataType,
		SourceDisplayName
	);	
	RAISERROR('Updating RecHubData.dimWorkgroupDataEntryColumns.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimWorkgroupDataEntryColumns', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimWorkgroupDataEntryColumns';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimWorkgroupDataEntryColumns,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/06/2015
*
* Purpose: Workgoup Data Entry Columns dimension is a type 0 SCD holding  
*	an entry for each Workgroup/BatchSource data entry column. 
*		   
*
* Modification History
* 07/06/2015 WI 221698 JPB	Created
* 05/25/2016 WI 282808 JPB	Added new index for OTIS import performance
* 12/02/2016 PT 127604133 JPB	Added IsRequired
******************************************************************************/';
		
		
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.OLD_dimWorkgroupDataEntryColumns') IS NOT NULL
		DROP TABLE RecHubData.OLD_dimWorkgroupDataEntryColumns;

		
END
ELSE
	RAISERROR('PT 127604133 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
