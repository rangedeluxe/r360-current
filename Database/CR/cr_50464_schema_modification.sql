--WFSScriptProcessorPrint CR 50464
--WFSScriptProcessorPrint Adding AutoUpdateCurrentProcessingDate column to OLTA.dimSiteCodes if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'dimSiteCodes' AND COLUMN_NAME = 'AutoUpdateCurrentProcessingDate' )
BEGIN
	RAISERROR('Dropping dimSiteCodes contraints',10,1) WITH NOWAIT
	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimSiteCodes' AND CONSTRAINT_NAME='PK_dimSiteCodes' )
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT PK_dimSiteCodes
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_CWDBActiveSite')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_CWDBActiveSite
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_LocalTimeZoneBias')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_LocalTimeZoneBias
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_CreationDate')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_CreationDate
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_CreatedBy')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_CreatedBy
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_ModificationDate')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_ModificationDate
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimSiteCodes_ModifiedBy')
		ALTER TABLE OLTA.dimSiteCodes DROP CONSTRAINT DF_dimSiteCodes_ModifiedBy
	
	RAISERROR('Rebuilding OLTA.dimSiteCodes',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.dimSiteCodes', 'OLDdimSiteCodes'

	CREATE TABLE OLTA.dimSiteCodes
	(
		SiteCodeID int NOT NULL 
			CONSTRAINT PK_dimSiteCodes PRIMARY KEY CLUSTERED,
		ShortName varchar(30) NOT NULL,
		LongName varchar(128) NOT NULL,
		CurrentProcessingDate datetime,
		CWDBActiveSite bit NOT NULL 
			CONSTRAINT DF_dimSiteCodes_CWDBActiveSite DEFAULT (0),
		LocalTimeZoneBias int NOT NULL 
			CONSTRAINT DF_dimSiteCodes_LocalTimeZoneBias DEFAULT (0),
		AutoUpdateCurrentProcessingDate bit NOT NULL
			CONSTRAINT DF_dimSiteCodes_AutoUpdateCurrentProcessingDate DEFAULT(0),
		CreationDate datetime NOT NULL 
			CONSTRAINT DF_dimSiteCodes_CreationDate DEFAULT(GETDATE()),
		CreatedBy varchar(32) NOT NULL 
			CONSTRAINT DF_dimSiteCodes_CreatedBy DEFAULT(SUSER_SNAME()),
		ModificationDate datetime NOT NULL 
			CONSTRAINT DF_dimSiteCodes_ModificationDate DEFAULT(GETDATE()),
		ModifiedBy varchar(32) NOT NULL 
			CONSTRAINT DF_dimSiteCodes_ModifiedBy DEFAULT(SUSER_SNAME())
	)
	
	RAISERROR('Creating index OLTA.dimSiteCodes.IDX_dimSiteCodes_SiteCodeID',10,1) WITH NOWAIT
	CREATE INDEX IDX_dimSiteCodes_SiteCodeID ON OLTA.dimSiteCodes (SiteCodeID)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimSiteCodes', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'dimSiteCodes';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Site Code dimension is a SCD type 2 holding Site Code info.  Most 
*	recent flag of 1 indicates the current Site Code row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/21/2012 CR 50464 JPB	Added columnn AutoUpdateCurrentProcessingDate.
******************************************************************************/
',	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = dimSiteCodes

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
	INSERT INTO OLTA.dimSiteCodes
	(
		SiteCodeID,
		ShortName,
		LongName,
		CurrentProcessingDate,
		CWDBActiveSite,
		LocalTimeZoneBias,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy
	)
	SELECT	SiteCodeID,
			ShortName,
			LongName,
			CurrentProcessingDate,
			CWDBActiveSite,
			LocalTimeZoneBias,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
	FROM	OLTA.OLDdimSiteCodes
	
	IF OBJECT_ID('OLTA.OLDdimSiteCodes') IS NOT NULL
		DROP TABLE OLTA.OLDdimSiteCodes
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
