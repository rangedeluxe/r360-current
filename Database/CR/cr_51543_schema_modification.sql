--WFSScriptProcessorPrint CR 51543
--WFSScriptProcessorPrint Adding BatchPaymentTypeKey to OLTA.factDataEntrySummary if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factDataEntrySummary' AND COLUMN_NAME = 'BatchPaymentTypeKey' )
BEGIN /* verify that CR 49279 has been applied before continuing. */
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factDataEntrySummary' AND COLUMN_NAME = 'SourceProcessingDateKey' )
	BEGIN
		RAISERROR('CR 49279 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimBanks_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimBanks_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimCustomers_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimCustomers_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimLockboxes_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_ProcessingDate_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_ProcessingDate_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_SourceProcessingDate_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_DepositDate_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_DepositDate_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factDataEntrySummary' )
			ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimBatchSources_factDataEntrySummary

		RAISERROR('Rebuilding OLTA.factDataEntrySummary adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factDataEntrySummary', 'OLDfactDataEntrySummary'

		CREATE TABLE OLTA.factDataEntrySummary
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL,
			ProcessingDateKey int NOT NULL, --CR 28221 JPB 11-14-2009
			SourceProcessingDateKey int NOT NULL, --CR 49274 JPB 01/11/2012
			DataEntryColumnKey int NOT NULL,
			GlobalBatchID int NOT NULL,
			BatchID int NOT NULL,
			DepositStatus int NOT NULL,
			BatchSourceKey tinyint NOT NULL, --CR 32301 JPB 01/13/2011
			BatchPaymentTypeKey tinyint NOT NULL --CR 51543 JPB 03/27/2012
				CONSTRAINT DF_factDataEntrySummary_BatchPaymentTypeKey DEFAULT(0),
			LoadDate datetime NOT NULL
		) $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factDataEntrySummary ADD
			CONSTRAINT FK_dimBanks_factDataEntrySummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factDataEntrySummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factDataEntrySummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_DepositDate_factDataEntrySummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_ProcessingDate_factDataEntrySummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factDataEntrySummary FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimDataEntryColumns_factDataEntrySummary FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey),
			CONSTRAINT FK_dimBatchSources_factDataEntrySummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey),
			CONSTRAINT FK_dimBatchPaymentTypes_factDataEntrySummary FOREIGN KEY(BatchPaymentTypeKey) REFERENCES OLTA.dimBatchPaymentTypes(BatchPaymentTypeKey)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factDataEntrySummary_DepositDateKey ON OLTA.factDataEntrySummary(DepositDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntrySummary_BankKey ON OLTA.factDataEntrySummary (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntrySummary_CustomerKey ON OLTA.factDataEntrySummary (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntrySummary_LockboxKey ON OLTA.factDataEntrySummary (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_ProcessingDate',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntrySummary_ProcessingDateKey ON OLTA.factDataEntrySummary(ProcessingDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntrySummary.IDX_factDataEntrySummary_DataEntryColumnKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntrySummary_DataEntryColumnKey ON OLTA.factDataEntrySummary (DataEntryColumnKey) $(OnPartition)
	
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDataEntrySummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factDataEntrySummary';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28221 JPB	ProcessingDateKey now NOT NULL.
* 02/12/2010 CR 28240 JPB	Added missing index on processing date key.
* 02/12/2010 CR 29011 JPB	Added missing foreign key on processing date.
* 01/13/2011 CR 32301 JPB	Added BatchSourceKey.
* 01/11/2012 CR 49279 JPB	Added SourceProcessingDateKey.
* 03/27/2012 CR 51543 JPB	Added BatchPaymentTypeKey
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factDataEntrySummary
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factDataEntrySummary
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			DataEntryColumnKey,
			GlobalBatchID,
			BatchID,
			DepositStatus,
			BatchSourceKey,
			LoadDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				DataEntryColumnKey,
				GlobalBatchID,
				BatchID,
				DepositStatus,
				BatchSourceKey,
				LoadDate
		FROM 	OLTA.OLDfactDataEntrySummary

		IF OBJECT_ID('OLTA.OLDfactDataEntrySummary') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factDataEntrySummary table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactDataEntrySummary
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT 
--WFSScriptProcessorCREnd

