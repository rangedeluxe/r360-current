--WFSScriptProcessorPrint CR 47915
--WFSScriptProcessorPrint Update Session FK_Users_Sessions to NOCHECK if necessary
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE SCHEMA_NAME(sys.foreign_keys.schema_id) = 'OLTA' AND OBJECT_NAME(sys.foreign_keys.parent_object_id) = 'Session' AND name = 'FK_Users_Sessions'	AND is_disabled = 0)
BEGIN
	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'Session' AND COLUMN_NAME = 'IPAddress' AND CHARACTER_MAXIMUM_LENGTH = 64 )
	BEGIN
		ALTER TABLE OLTA.Session NOCHECK CONSTRAINT FK_Users_Sessions;

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'Session', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'Session';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Static Data
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 09/29/2011 CR 46575 JPB 	Updated IPAddress to VARCHAR(64)
* 11/15/2011 CR 47915 JPB	Made FK_Users_Sessions NOCHECK
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = Session
	END
	ELSE
		RAISERROR('CR 46575 must be applied before running this CR.',16,1) WITH NOWAIT
END
--WFSScriptProcessorCREnd
