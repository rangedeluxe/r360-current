--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 160347603
--WFSScriptProcessorPrint Updating IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT COL_NAME(ic.object_id,ic.column_id) AS column_name  
	FROM sys.indexes AS i  
	INNER JOIN sys.index_columns AS ic   
		ON i.object_id = ic.object_id AND i.index_id = ic.index_id  
	WHERE i.name = 'IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID'
	AND COL_NAME(ic.object_id,ic.column_id) = 'StartDateKey') 
	BEGIN
		RAISERROR('PT 160347603 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		IF EXISTS (SELECT name FROM sys.indexes WHERE name = 'IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID')
			DROP INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID ON RecHubUser.SessionClientAccountEntitlements
		
		RAISERROR('Creating index IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID ON RecHubUser.SessionClientAccountEntitlements
		(
			SessionID ASC,
			SiteBankID ASC,
			SiteClientAccountID ASC
		)
		INCLUDE ( 	
			EntityID,
			ViewingDays,
			MaximumSearchDays,
			ClientAccountKey,
			StartDateKey,
			EndDateKey,
			ViewAhead,
			EntityName
			) ON Sessions (CreationDateKey);

IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'SessionClientAccountEntitlements', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubUser',
					@level1type = N'TABLE',
					@level1name = N'SessionClientAccountEntitlements';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubUser,
		@level1type = N'TABLE',@level1name = SessionClientAccountEntitlements,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2014
*
* Purpose: Client Account Keys a session is entitled to view.
*		   
*
* Modification History
* 05/11/2014 WI 145456		JPB	Created
* 07/08/2014 WI 145456		KLC	Changed Logon entity to be actual owning entity
* 09/18/2014 WI 166671		JPB	Added new index.
* 09/18/2014 WI 166672		JPB	Updated IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey
*								with includes.
* 02/23/2015 WI 191263		JBS	Add columns ViewAhead and EndDateKey, Also Change StartDateKey to NOT NULL
* 03/23/2015 WI 197226		JPB	Add IDX_SessionIDCreationDate (used by Session Maint package)
* 03/25/2015 WI 197953		JBS	Add IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead  
*							(used by Advanced Search)
* 06/01/2015 WI 197953		JBS	Changing IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID to Clustered index. Per 216076
* 03/29/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added new index and partition keys
* 04/24/2017 PT 143253497	MGE updated column name to remove extra Session
* 09/27/2018 PT 160347603	MGE added columns to index to improve Dashboard performance.
******************************************************************************************************************/';
	END
--WFSScriptProcessorCREnd