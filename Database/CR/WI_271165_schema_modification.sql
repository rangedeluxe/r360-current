--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271165
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factBatchSummary.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factBatchSummary')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factBatchSummary'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_DepositDatefactBatchSummaryKey')
			DROP INDEX IDX_factBatchSummary_DepositDatefactBatchSummaryKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_DepositDateClientAccountKeyBatchID')
			DROP INDEX IDX_factBatchSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_BankKey')
			DROP INDEX IDX_factBatchSummary_BankKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_ClientAccountKey')
			DROP INDEX IDX_factBatchSummary_ClientAccountKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_OrganizationKey')
			DROP INDEX IDX_factBatchSummary_OrganizationKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_SourceProcessingDateKey')
			DROP INDEX IDX_factBatchSummary_SourceProcessingDateKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber')
			DROP INDEX IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_ClientAccountBankSourceProcessingDateKeyBatchID')
			DROP INDEX IDX_factBatchSummary_ClientAccountBankSourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID')
			DROP INDEX IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_SourceProcessingDateKeyBatchID')
			DROP INDEX IDX_factBatchSummary_SourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID')
			DROP INDEX IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey')
			DROP INDEX IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey ON RecHubData.factBatchSummary;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey')
			DROP INDEX IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey ON RecHubData.factBatchSummary;

		/****** Object:  Index PK_factBatchSummary    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factBatchSummary')
			ALTER TABLE RecHubData.factBatchSummary DROP CONSTRAINT PK_factBatchSummary;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factBatchSummary ADD
			CONSTRAINT PK_factBatchSummary PRIMARY KEY NONCLUSTERED (factBatchSummaryKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factBatchSummary_DepositDatefactBatchSummaryKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factBatchSummary_DepositDatefactBatchSummaryKey ON RecHubData.factBatchSummary 
		(
			DepositDateKey,
			factBatchSummaryKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_DepositDateClientAccountKeyBatchID',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factBatchSummary 
		(
			DepositDateKey,
			ClientAccountKey,
			BatchID
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_BankKey ON RecHubData.factBatchSummary (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_ClientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_ClientAccountKey ON RecHubData.factBatchSummary (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_OrganizationKey ON RecHubData.factBatchSummary (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_SourceProcessingDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_SourceProcessingDateKey ON RecHubData.factBatchSummary (SourceProcessingDateKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber ON RecHubData.factBatchSummary 
		(
			BankKey ASC,
			OrganizationKey ASC,
			ClientAccountKey ASC,
			SourceProcessingDateKey ASC,
			DepositDateKey ASC,
			BatchID ASC,
			BatchNumber ASC
		) $(OnDataPartition);  

		RAISERROR('Rebuilding index IDX_factBatchSummary_ClientAccountBankSourceProcessingDateKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountBankSourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary
		(
			ClientAccountKey ASC,
			BankKey ASC,
			SourceProcessingDateKey ASC,
			BatchID ASC
		)
		INCLUDE 
		( 
			BatchSourceKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID ON RecHubData.factBatchSummary
		(
			CreationDate ASC,
			ClientAccountKey ASC,
			SourceProcessingDateKey ASC,
			DepositDateKey ASC, 
			BatchID ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_SourceProcessingDateKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_SourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary
		(
			SourceProcessingDateKey ASC,
			BatchID ASC
		)
		INCLUDE 
		( 
			BankKey,
			ClientAccountKey,
			DepositDateKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID  ON RecHubData.factBatchSummary
		(
			ClientAccountKey ASC,
			SourceProcessingDateKey ASC,
			BatchID ASC
		)
		INCLUDE 
		( 
			BankKey,
			BatchSourceKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey  ON RecHubData.factBatchSummary
		(
			ClientAccountKey ASC,
			BatchID ASC,
			SourceProcessingDateKey ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey ON RecHubData.factBatchSummary
		(
			BatchID ASC,
			DepositDateKey ASC,
			factBatchSummaryKey ASC,
			IsDeleted ASC,
			SourceProcessingDateKey ASC,
			ImmutableDateKey ASC
		)
		INCLUDE
		(
			SourceBatchID,
			BankKey,
			ClientAccountKey,
			OrganizationKey,
			BatchPaymentTypeKey
		) $(OnDataPartition);


	END
	ELSE
		RAISERROR('WI 271165 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271165 RecHubData.factBatchSummary table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd