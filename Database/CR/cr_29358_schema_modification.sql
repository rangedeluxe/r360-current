--WFSScriptProcessorPrint CR 29358 
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.factTransactionSummary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factTransactionSummary ADD ModificationDate DATETIME NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Updating existing OLTA.factTransactionSummary.ModificationDate with LoadDate
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factTransactionSummary 
SET		ModificationDate = LoadDate
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factTransactionSummary.ModificationDate to NOT NULL
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factTransactionSummary ALTER COLUMN ModificationDate DATETIME NOT NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factTransactionSummary.ModificationDate adding default constraint
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM	sysobjects WHERE name = 'DF_factTransactionSummary_ModificationDate')
	AND EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factTransactionSummary ADD
		CONSTRAINT DF_factTransactionSummary_ModificationDate DEFAULT GETDATE() FOR ModificationDate
--WFSScriptProcessorCREnd