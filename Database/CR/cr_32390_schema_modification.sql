--WFSScriptProcessorPrint CR 32390
--WFSScriptProcessorPrint Populating dbo.LegacyDataMove with all partition names.
--WFSScriptProcessorCRBegin

INSERT INTO LegacyDataMove (PartitionName,StartDate,EndDate)
SELECT 	[Name] AS PartitionName,
		CAST(CONVERT(varchar,DATEADD(DAY,-6,CONVERT(DATETIME,CONVERT(VARCHAR,SUBSTRING([name],LEN('OLTAFacts')+1,LEN([name])-LEN('OTLAFacts'))),112)),112) AS INT) AS StartDate,
		SUBSTRING([name],LEN('OLTAFacts')+1,LEN([name])-LEN('OTLAFacts')) AS EndDate
FROM sys.database_files
WHERE [type] = 0 AND data_space_id > 1

--WFSScriptProcessorCREnd

