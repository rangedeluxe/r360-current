--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 148859
--WFSScriptProcessorPrint Update RecHubData.dimItemDataSetupFields if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimItemDataSetupFields' AND COLUMN_NAME='ImportTypeKey' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimItemDataSetupFields' AND CONSTRAINT_NAME='FK_dimItemDataSetupFields_dimBatchSources' )
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT FK_dimItemDataSetupFields_dimBatchSources;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_factItemData_dimItemDataSetupFields' )
		ALTER TABLE RecHubData.factItemData DROP CONSTRAINT FK_factItemData_dimItemDataSetupFields;

	RAISERROR('Dropping dimBatchSources contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimItemDataSetupFields' AND CONSTRAINT_NAME='PK_dimItemDataSetupFields' )
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT PK_dimItemDataSetupFields;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimItemDataSetupFields_CreationDate')
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT DF_dimItemDataSetupFields_CreationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimItemDataSetupFields_CreatedBy')
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT DF_dimItemDataSetupFields_CreatedBy;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimItemDataSetupFields_ModificationDate')
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT DF_dimItemDataSetupFields_ModificationDate;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimItemDataSetupFields_ModifiedBy')
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT DF_dimItemDataSetupFields_ModifiedBy;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.CHECK_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'RecHubData' AND CONSTRAINT_NAME = 'CK_dimItemDataSetupFields_DataType')
		ALTER TABLE RecHubData.dimItemDataSetupFields DROP CONSTRAINT CK_dimItemDataSetupFields_DataType;

	RAISERROR('Rebuilding RecHubData.dimItemDataSetupFields',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.dimItemDataSetupFields', 'Old_dimItemDataSetupFields';

	CREATE TABLE RecHubData.dimItemDataSetupFields
	(
		ItemDataSetupFieldKey INT NOT NULL 
			CONSTRAINT PK_dimItemDataSetupFields PRIMARY KEY CLUSTERED,
		ImportTypeKey TINYINT NOT NULL,
		DataType TINYINT NOT NULL
			CONSTRAINT CK_dimItemDataSetupFields_DataType CHECK (DataType IN (1,3,4,6,7,11)),
		CreationDate DATETIME NOT NULL
			CONSTRAINT DF_dimItemDataSetupFields_CreationDate DEFAULT GETDATE(),
		CreatedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_dimItemDataSetupFields_CreatedBy DEFAULT SUSER_SNAME(),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_dimItemDataSetupFields_ModificationDate DEFAULT GETDATE(),
		ModifiedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_dimItemDataSetupFields_ModifiedBy DEFAULT SUSER_SNAME(),
		Keyword VARCHAR(32) NOT NULL,
		[Description] VARCHAR(256) NOT NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimItemDataSetupFields', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimItemDataSetupFields';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2011
*
* Purpose: Stores Item Data trail keywords referenced by factItemData. 
*
* Check Constraint definitions:
* DataType
*	1: Alphanumeric
*	3: Integer
*	4: Bit
*	6: Float
*	7: Money
* 	11: Date/time
*
* Modification History
* 03/07/2011 CR 33209 JPB	Created
* 02/14/2013 WI 90130 JBS	Update table to	2.0 release. Change Schema Name
*							Change column length on ModifiedBy and CreatedBy
*							From 32 to 128. 
*							Add FK FK_dimItemDataSetupFields_dimBatchSources
* 06/20/2014 WI	148859 JPB	Changed BatchSourceKey to ImportTypeKey.			
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimItemDataSetupFields;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
	INSERT INTO RecHubData.dimItemDataSetupFields 
	(
		ItemDataSetupFieldKey,
		ImportTypeKey,
		DataType,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		Keyword,
		[Description]	
	) 
	SELECT	
		RecHubData.Old_dimItemDataSetupFields.ItemDataSetupFieldKey,
		RecHubData.dimBatchSources.ImportTypeKey,
		RecHubData.Old_dimItemDataSetupFields.DataType,
		RecHubData.Old_dimItemDataSetupFields.CreatedBy,
		RecHubData.Old_dimItemDataSetupFields.CreationDate,
		RecHubData.Old_dimItemDataSetupFields.ModifiedBy,
		RecHubData.Old_dimItemDataSetupFields.ModificationDate,
		RecHubData.Old_dimItemDataSetupFields.Keyword,
		RecHubData.Old_dimItemDataSetupFields.[Description]	
	FROM 	
		RecHubData.Old_dimItemDataSetupFields
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.Old_dimItemDataSetupFields.BatchSourceKey;

	RAISERROR('Creating index IDX_dimItemDataSetupFields_Keyword_ImportTypeKey',10,1) WITH NOWAIT;
	CREATE UNIQUE INDEX IDX_dimItemDataSetupFields_Keyword_ImportTypeKey ON RecHubData.dimItemDataSetupFields (Keyword,ImportTypeKey);

	/* 
		Special note: Normally the FKs would be added back here. However, since the data type is being changed, all the tables that have a FK need 
		to be updated as well. Therefore, the scripts that alter the FK tables will add the FK.

		This script will be applied BEFORE the changes to the other tables are applied.

		We will add back the one FK we can here.
	*/
	RAISERROR('Applying Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubData.dimItemDataSetupFields ADD 
	CONSTRAINT FK_dimItemDataSetupFields_dimImportTypes FOREIGN KEY(ImportTypeKey) REFERENCES RecHubData.dimImportTypes(ImportTypeKey);



	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.Old_dimItemDataSetupFields') IS NOT NULL
		DROP TABLE RecHubData.Old_dimItemDataSetupFields;
END
ELSE
	RAISERROR('WI 148858 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
