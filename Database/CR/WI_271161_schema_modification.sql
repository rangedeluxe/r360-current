--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI271161
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factBatchData.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factBatchData')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factBatchData'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factBatchData_DepositDatefactBatchDataKey')
			DROP INDEX IDX_factBatchData_DepositDatefactBatchDataKey ON RecHubData.factBatchData;

		/****** Object:  Index [PK_factBatchData]    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factBatchData')
			ALTER TABLE RecHubData.factBatchData DROP CONSTRAINT PK_factBatchData;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factBatchData ADD 
			CONSTRAINT PK_factBatchData PRIMARY KEY NONCLUSTERED (factBatchDataKey,DepositDateKey)
			$(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factBatchData_DepositDatefactBatchDataKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factBatchData_DepositDatefactBatchDataKey ON RecHubData.factBatchData 
		(
			DepositDateKey,
			factBatchDataKey
		) $(OnDataPartition);
	END
	ELSE
		RAISERROR('WI 271161 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271161 RecHubData.FactBatchData table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
