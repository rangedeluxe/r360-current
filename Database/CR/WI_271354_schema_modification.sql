--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271354
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factRawPaymentData.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factRawPaymentData')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factRawPaymentData'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factRawPaymentData_DepositDatefactRawPaymentDataKey')
			DROP INDEX IDX_factRawPaymentData_DepositDatefactRawPaymentDataKey ON RecHubData.factRawPaymentData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factRawPaymentData_BankKey')
			DROP INDEX IDX_factRawPaymentData_BankKey ON RecHubData.factRawPaymentData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factRawPaymentData_OrganizationKey')
			DROP INDEX IDX_factRawPaymentData_OrganizationKey ON RecHubData.factRawPaymentData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factRawPaymentData_ClientAccountKey')
			DROP INDEX IDX_factRawPaymentData_ClientAccountKey ON RecHubData.factRawPaymentData;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factRawPaymentData_ImmutableDateKey')
			DROP INDEX IDX_factRawPaymentData_ImmutableDateKey ON RecHubData.factRawPaymentData;

		/****** Object:  Index PK_PK_factRawPaymentData    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factRawPaymentData')
			ALTER TABLE RecHubData.factRawPaymentData DROP CONSTRAINT PK_factRawPaymentData;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factRawPaymentData ADD
			CONSTRAINT PK_factRawPaymentData PRIMARY KEY NONCLUSTERED (factRawPaymentDataKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factRawPaymentData_DepositDatefactRawPaymentDataKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factRawPaymentData_DepositDatefactRawPaymentDataKey ON RecHubData.factRawPaymentData 
		(
			DepositDateKey,
			factRawPaymentDataKey
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factRawPaymentData_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factRawPaymentData_BankKey ON RecHubData.factRawPaymentData (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factRawPaymentData_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factRawPaymentData_OrganizationKey ON RecHubData.factRawPaymentData (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factRawPaymentData_ClientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factRawPaymentData_ClientAccountKey ON RecHubData.factRawPaymentData (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factRawPaymentData_ImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factRawPaymentData_ImmutableDateKey ON RecHubData.factRawPaymentData (ImmutableDateKey) $(OnDataPartition);

	
	END
	ELSE
		RAISERROR('WI 271354 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271354 RecHubData.factRawPaymentData table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd