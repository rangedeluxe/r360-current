--WFSScriptProcessorDoNotFormat
DECLARE @DITRetentionDays INT,
		@BeginDate	DATETIME,
		@EndDate	DATETIME,
		@Message	VARCHAR(90),
		@TotalNewCount	INT,
		@TotalOldCount	INT;

IF NOT EXISTS 
	(
	SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'DataImportQueue'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubSystem') = sys.tables.schema_id
	)
BEGIN
	RAISERROR('Beginning process to partition DataImportQueue',10,1) WITH NOWAIT;
	RAISERROR('Building Partitions',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @DITRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'DataImportQueueRetentionDays'
	SELECT @BeginDate = GETDATE() - @DITRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest Partition to build is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	SELECT @EndDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Last Future Partition to build is: ' + CONVERT(CHAR(10),@EndDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Build partitions by calling usp_CreatePartitionsForRange >>>
	RAISERROR('Creating Partitions for DataImport',10,1) WITH NOWAIT;
	EXEC RecHubSystem.usp_CreatePartitionsForRange 'DataImport', @BeginDate, @EndDate

	--<<< Rename existing DataImportQueue to OLD_DataImportQueue >>>
	RAISERROR('Dropping Constraints on DataImportQueue',10,1) WITH NOWAIT;
	IF EXISTS (SELECT * FROM sys.objects WHERE name = 'CK_DataImportQueue_QueueType')
	ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_QueueType;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_DataImportQueue_QueueStatus')
	ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_QueueStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_QueueStatus')
	ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_QueueStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ResponseStatus')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ResponseStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_DataImportQueue_ResponseStatus')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT CK_DataImportQueue_ResponseStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_RetryCount')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_RetryCount;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_CreationDate')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_CreatedBy')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_CreatedBy;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ModificationDate')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ModificationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataImportQueue_ModifiedBy')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT DF_DataImportQueue_ModifiedBy;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'PK_DataImportQueue')
		ALTER TABLE RecHubSystem.DataImportQueue DROP CONSTRAINT PK_DataImportQueue;
	RAISERROR('Dropping Indexes on DataImportQueue',10,1) WITH NOWAIT;
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_DataImportQueue_ClientProcessCode')
		DROP INDEX IDX_DataImportQueue_ClientProcessCode ON [RecHubSystem].[DataImportQueue];
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'PK_DataImportQueue')
		DROP INDEX PK_DataImportQueue ON [RecHubSystem].[DataImportQueue];
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_DataImportQueue_EntityTrackingID')
		DROP INDEX IDX_DataImportQueue_EntityTrackingID ON [RecHubSystem].[DataImportQueue];
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_DataImportQueue_SourceTrackingID')
		DROP INDEX IDX_DataImportQueue_SourceTrackingID ON [RecHubSystem].[DataImportQueue];
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus')
		DROP INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON [RecHubSystem].[DataImportQueue];
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_DataImportQueue_QueueStatusResponseTrackingID')
		DROP INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON [RecHubSystem].[DataImportQueue];

	RAISERROR('Renaming DataImportQueue to OLD_DataImportQueue',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubSystem.DataImportQueue', 'OLD_DataImportQueue';
	
	--<<< Create partitioned DataImportQueue (use DataImportQueue.sql) (must set environment variable) >>>
	RAISERROR('Building partitioned RecHubSystem.DataImportQueue',10,1) WITH NOWAIT;
	CREATE TABLE RecHubSystem.DataImportQueue
	(
	DataImportQueueID BIGINT NOT NULL IDENTITY(1,1),
		--CONSTRAINT [PK_DataImportQueue] PRIMARY KEY CLUSTERED,
	QueueType TINYINT NOT NULL
		CONSTRAINT CK_DataImportQueue_QueueType CHECK(QueueType IN (0,1,2)),
	QueueStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_QueueStatus DEFAULT 10
		CONSTRAINT CK_DataImportQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99,120,145,150)),
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_ResponseStatus DEFAULT 255
		CONSTRAINT CK_DataImportQueue_ResponseStatus CHECK(ResponseStatus IN (0,1,2,30,255)),
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_RetryCount DEFAULT 0,
	AuditDateKey INT NOT NULL,
	XSDVersion VARCHAR(12) NOT NULL,
	ClientProcessCode VARCHAR(40) NOT NULL, 
	SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
	EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseTrackingID UNIQUEIDENTIFIER NULL,
	XMLDataDocument XML NOT NULL,
	XMLResponseDocument XML NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_ModifiedBy DEFAULT SUSER_NAME()
	) ON DataImport (AuditDateKey);
	
	ALTER TABLE RecHubSystem.DataImportQueue ADD
		CONSTRAINT PK_DataImportQueue PRIMARY KEY NONCLUSTERED (DataImportQueueID, AuditDateKey)

	RAISERROR('Building Indexes on DataImportQueue',10,1) WITH NOWAIT;

	CREATE CLUSTERED INDEX IDX_DataImportQueue_DataImportQueueID_AuditDateKey ON RecHubSystem.DataImportQueue (DataImportQueueID, AuditDateKey) ON DataImport (AuditDateKey);
	CREATE INDEX IDX_DataImportQueue_EntityTrackingID ON RecHubSystem.DataImportQueue (EntityTrackingID) ON DataImport (AuditDateKey);
	CREATE INDEX IDX_DataImportQueue_SourceTrackingID ON RecHubSystem.DataImportQueue (SourceTrackingID) ON DataImport (AuditDateKey);
	CREATE INDEX IDX_DataImportQueue_ClientProcessCode ON RecHubSystem.DataImportQueue (ClientProcessCode) ON DataImport (AuditDateKey);

	CREATE INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON RecHubSystem.DataImportQueue 
		(
		QueueType, 
		ClientProcessCode,
		QueueStatus
		) 
	INCLUDE 
		(SourceTrackingID) ON DataImport (AuditDateKey);

	CREATE INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON RecHubSystem.DataImportQueue 
		(
		QueueStatus, 
		ResponseTrackingID
		) ON DataImport (AuditDateKey);
	
	--<<< Copy rows (based on retention) from OLD_DataImportQueue to DataImportQueue >>>
	INSERT INTO RecHubSystem.DataImportQueue
	(QueueType, 
	QueueStatus, 
	ResponseStatus, 
	RetryCount, 
	AuditDateKey, 
	XSDVersion, 
	ClientProcessCode,
	SourceTrackingID,
	EntityTrackingID,
	ResponseTrackingID,
	XMLDataDocument,
	XMLResponseDocument,
	CreationDate)
	SELECT 
	QueueType, 
	QueueStatus, 
	ResponseStatus, 
	RetryCount, 
	AuditDateKey, 
	XSDVersion, 
	ClientProcessCode,
	SourceTrackingID,
	EntityTrackingID,
	ResponseTrackingID,
	XMLDataDocument,
	XMLResponseDocument,
	CreationDate
	FROM RecHubSystem.OLD_DataImportQueue
	WHERE CreationDate >= @BeginDate;

	--<<< Verify expected rows have been copied >>>

	SELECT @TotalOldCount = COUNT(*) FROM RecHubSystem.OLD_DataImportQueue WHERE CreationDate >= @BeginDate;
	SELECT @TotalNewCount = COUNT(*) FROM RecHubSystem.DataImportQueue;
	SELECT @Message = 'New DataImportQueue Reord Count: ' + CAST(@TotalNewCount AS VARCHAR(10)) + ' - Old DataImportQueue Reord Count Within Range: ' + CAST(@TotalOldCount AS VARCHAR(10))
	RAISERROR (@Message,10,1);

	IF @TotalNewCount = @TotalOldCount
	BEGIN
		RAISERROR('Partitioned table record count matches what was expected.',10,1) WITH NOWAIT;
	END 
	ELSE
		RAISERROR('Partitioned table record count is NOT what was expected.',10,1) WITH NOWAIT;
	
	--<<< Drop OLD_DataImportQueue if expected rows have been copied >>> 
	DROP TABLE RecHubSystem.OLD_DataImportQueue;
	RAISERROR('DataImportQueue has been successfully partitioned.',10,1) WITH NOWAIT;
END
ELSE
	RAISERROR('PT 127604193 has already been applied to the database.',10,1) WITH NOWAIT;

