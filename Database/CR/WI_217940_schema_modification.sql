--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 217940
--WFSScriptProcessorPrint Adding indexes IDX_factItemData_ItemDataSetupFieldKey and IDX_factItemData_SourceBatchIDDepositDateKey
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factItemData_ItemDataSetupFieldKey') 
	BEGIN
		RAISERROR('WI 217940 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_factItemData_ItemDataSetupFieldKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factItemData_ItemDataSetupFieldKey ON RecHubData.factItemData 
		(
			ItemDataSetupFieldKey
		) 
		INCLUDE 
		(
			ClientAccountKey, 
			ImmutableDateKey, 
			BatchID, 
			BatchSequence, 
			ModificationDate, 
			DataValue
		);

		RAISERROR('Creating index IDX_factItemData_SourceBatchIDDepositDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factItemData_SourceBatchIDDepositDateKey ON RecHubData.factItemData 
		(
			SourceBatchID,
			DepositDateKey
		)
		INCLUDE 
		(
			factItemDataKey, 
			IsDeleted, 
			BankKey, 
			OrganizationKey, 
			ClientAccountKey, 
			ImmutableDateKey, 
			SourceProcessingDateKey, 
			BatchID, 
			BatchNumber, 
			TransactionID, 
			BatchSequence, 
			ItemDataSetupFieldKey, 
			CreationDate, 
			ModificationDate, 
			DataValueDateTime, 
			DataValueMoney, 
			DataValueInteger, 
			DataValueBit, 
			ModifiedBy, 
			DataValue, 
			BatchSourceKey
		) $(OnDataPartition);


		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factItemData', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factItemData';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factItemData,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*
*
* Modification History
* 03/07/2011 CR 33210 JPB	Created
* 01/12/2012 CR 49282 JPB	Added SourceProcessingDateKey.
* 03/19/2012 CR 51050 WJS	Added index.
* 07/19/2012 CR 54128 JPB	Added BatchNumber.
* 07/16/2012 CR 54137 JPB	Renamed and updated index with BatchNumber.
* 03/06/2013 WI 90542 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factItemDataKey, IsDeleted.
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factItemDataKey to Clustered Index. 
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factItemData_ModificationDate DEFAULT GETDATE()
* 05/31/2014 WI 135321 JPB	Changed BatchID from INT to BIGINT.
* 05/31/2014 WI	144972 JPB	Add SourceBatchID.
* 03/20/2015 WI 196953 JPB	Added BatchSourceKey.
* 06/10/2015 WI 217940 JBS	Add indexes based on Regression Analysis.
******************************************************************************/';
	END
--WFSScriptProcessorCREnd