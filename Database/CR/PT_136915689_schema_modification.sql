--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #136915689
--WFSScriptProcessorPrint Rebuilding RecHubData.InvoiceSearchSearchRequestTable if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM sys.table_types INNER JOIN sys.columns ON sys.columns.object_id = sys.table_types.type_table_object_id WHERE sys.table_types.name = 'InvoiceSearchSearchRequestTable' and sys.columns.name = 'EntityName' )
BEGIN
	RAISERROR('Renaming user defined type RecHubData.InvoiceSearchSearchRequestTable',10,1) WITH NOWAIT;

	DECLARE @Dependencies TABLE
	(
		RowID SMALLINT IDENTITY(1,1),
		SchemaName SYSNAME,
		ObjectName SYSNAME
	);

	INSERT INTO @Dependencies(SchemaName,ObjectName)
	SELECT 
		referencing_schema_name,
		referencing_entity_name
	FROM
		sys.dm_sql_referencing_entities('RecHubData.InvoiceSearchSearchRequestTable','TYPE');

	EXEC sys.sp_rename 'RecHubData.InvoiceSearchSearchRequestTable','RecHubData.InvoiceSearchSearchRequestTable_old';
	RAISERROR('Rebuilding user defined type RecHubData.InvoiceSearchSearchRequestTable',10,1) WITH NOWAIT;
	CREATE TYPE RecHubData.InvoiceSearchSearchRequestTable AS TABLE
	(
		SessionID UNIQUEIDENTIFIER,
		DepositDateFrom DATE,
		DepositDateTo DATE,
		DepositDateFromKey INT,
		DepositDateToKey INT,
		RecordsPerPage INT,
		StartRecord INT,
		BatchSourceKey SMALLINT,
		BatchPaymentTypeKey SMALLINT,
		EntityName NVARCHAR(50),
		OrderDirection VARCHAR(4),
		OrderBy VARCHAR(64),
		SortBy VARCHAR(125),
		UserName VARCHAR(256)
	);

	DECLARE @Loop INT = 1,
			@RefreshName NVARCHAR(300);

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Dependencies) )
	BEGIN
		SELECT 
			@RefreshName = SchemaName + '.' + ObjectName
		FROM 
			@Dependencies
		WHERE RowID = @Loop;

		EXEC sys.sp_refreshsqlmodule @name = @RefreshName;

		SET @Loop += 1;
	END

	RAISERROR('Dropping old user defined type RecHubData.InvoiceSearchSearchRequestTable',10,1) WITH NOWAIT;
	IF TYPE_ID('RecHubData.InvoiceSearchSearchRequestTable_old') IS NOT NULL
		DROP TYPE RecHubData.InvoiceSearchSearchRequestTable_old;

END
--WFSScriptProcessorCREnd

