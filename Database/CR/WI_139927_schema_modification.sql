--WFSScriptProcessorPrint WI 139927
--WFSScriptProcessorPrint Add EntityID to RecHubData.dimBanks if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimBanks' AND COLUMN_NAME='EntityID' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factBatchData' AND CONSTRAINT_NAME='FK_factBatchData_dimBanks' )
		ALTER TABLE RecHubData.factBatchData DROP CONSTRAINT FK_factBatchData_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factBatchExtracts' AND CONSTRAINT_NAME='FK_factBatchExtracts_dimBanks' )
		ALTER TABLE RecHubData.factBatchExtracts DROP CONSTRAINT FK_factBatchExtracts_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_factBatchSummary_dimBanks' )
		ALTER TABLE RecHubData.factBatchSummary DROP CONSTRAINT FK_factBatchSummary_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_factChecks_dimBanks' )
		ALTER TABLE RecHubData.factChecks DROP CONSTRAINT FK_factChecks_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factCheckImages' AND CONSTRAINT_NAME='FK_factCheckImages_dimBanks' )
		ALTER TABLE RecHubData.factCheckImages DROP CONSTRAINT FK_factCheckImages_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_factDataEntryDetails_dimBanks' )
		ALTER TABLE RecHubData.factDataEntryDetails DROP CONSTRAINT FK_factDataEntryDetails_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_factDataEntrySummary_dimBanks' )
		ALTER TABLE RecHubData.factDataEntrySummary DROP CONSTRAINT FK_factDataEntrySummary_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_factDocuments_dimBanks' )
		ALTER TABLE RecHubData.factDocuments DROP CONSTRAINT FK_factDocuments_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDocumentImages' AND CONSTRAINT_NAME='FK_factDocumentImages_dimBanks' )
		ALTER TABLE RecHubData.factDocumentImages DROP CONSTRAINT FK_factDocumentImages_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factExtractTraces' AND CONSTRAINT_NAME='FK_factExtractTraces_dimBanks' )
		ALTER TABLE RecHubData.factExtractTraces DROP CONSTRAINT FK_factExtractTraces_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factItemData' AND CONSTRAINT_NAME='FK_factItemData_dimBanks' )
		ALTER TABLE RecHubData.factItemData DROP CONSTRAINT FK_factItemData_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotificationFiles' AND CONSTRAINT_NAME='FK_factNotificationFiles_dimBanks' )
		ALTER TABLE RecHubData.factNotificationFiles DROP CONSTRAINT FK_factNotificationFiles_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factNotifications' AND CONSTRAINT_NAME='FK_factNotifications_dimBanks' )
		ALTER TABLE RecHubData.factNotifications DROP CONSTRAINT FK_factNotifications_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_factStubs_dimBanks' )
		ALTER TABLE RecHubData.factStubs DROP CONSTRAINT FK_factStubs_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_factTransactionSummary_dimBanks' )
		ALTER TABLE RecHubData.factTransactionSummary DROP CONSTRAINT FK_factTransactionSummary_dimBanks;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='EventLog' AND CONSTRAINT_NAME='FK_EventLog_dimBanks' )
		ALTER TABLE RecHubAlert.EventLog DROP CONSTRAINT FK_EventLog_dimBanks;

	RAISERROR('Dropping dimBatchSources contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimBanks' AND CONSTRAINT_NAME='PK_dimBanks' )
		ALTER TABLE RecHubData.dimBanks DROP CONSTRAINT PK_dimBanks;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimBanks_ModificationDate')
		ALTER TABLE RecHubData.dimBanks DROP CONSTRAINT DF_dimBanks_ModificationDate;

	RAISERROR('Rebuilding RecHubData.dimBatchSources',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.dimBanks', 'OLD_dimBanks';

	CREATE TABLE RecHubData.dimBanks
	(
		BankKey INT NOT NULL IDENTITY(1,1) 
			CONSTRAINT PK_dimBanks PRIMARY KEY CLUSTERED,
		SiteBankID INT NOT NULL,
		MostRecent BIT NOT NULL,
		EntityID INT NULL,
		CreationDate DATETIME NOT NULL,
		ModificationDate DATETIME NOT NULL
			CONSTRAINT DF_dimBanks_ModificationDate DEFAULT GETDATE(),
		BankName VARCHAR(128) NOT NULL,
		ABA VARCHAR(10) NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimBanks', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimBanks';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = dimBanks,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Banks dimension is a SCD type 2 holding Bank info.  Most recent 
*	flag of 1 indicates the current Bank row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/28/2013 WI 89910 JBS	Update table to 2.0 Release. Rename Schema
*							Renamed Columns:
*								LoadDate to CreationDate
*							Added Column:
*								ModificationDate (With Constraint)
*							Deleted Column:
*								AddressID	
*							Expanded columns: BankName to VARCHAR(128)
* 06/02/2014 WI 139927 JPB	Added EntityID.
******************************************************************************/';

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
	SET IDENTITY_INSERT RecHubData.dimBanks ON;
	
	INSERT INTO RecHubData.dimBanks 
	(
		BankKey,
		SiteBankID,
		MostRecent,
		CreationDate,
		ModificationDate,
		BankName,
		ABA	
	) 
	SELECT	
		BankKey,
		SiteBankID,
		MostRecent,
		CreationDate,
		ModificationDate,
		BankName,
		ABA	
	FROM 	
		RecHubData.OLD_dimBanks

	SET IDENTITY_INSERT RecHubData.dimBanks OFF;

	RAISERROR('Creating index IDX_dimBanks_SiteBankID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_dimBanks_SiteBankID ON RecHubData.dimBanks (SiteBankID);
	
	RAISERROR('Applying Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubData.factBatchData ADD 
		CONSTRAINT FK_factBatchData_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factBatchExtracts ADD 
		CONSTRAINT FK_factBatchExtracts_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factBatchSummary ADD 
		CONSTRAINT FK_factBatchSummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factCheckImages ADD
		CONSTRAINT FK_factCheckImages_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factChecks ADD
		CONSTRAINT FK_factChecks_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factDataEntryDetails ADD
		CONSTRAINT FK_factDataEntryDetails_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factDataEntrySummary ADD
		CONSTRAINT FK_factDataEntrySummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factDocumentImages ADD
		CONSTRAINT FK_factDocumentImages_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factDocuments ADD
		CONSTRAINT FK_factDocuments_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factExtractTraces ADD 
		CONSTRAINT FK_factExtractTraces_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factItemData ADD 
		CONSTRAINT FK_factItemData_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factNotificationFiles ADD
		CONSTRAINT FK_factNotificationFiles_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factNotifications ADD
		CONSTRAINT FK_factNotifications_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factStubs ADD
		CONSTRAINT FK_factStubs_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubData.factTransactionSummary ADD 
		CONSTRAINT FK_factTransactionSummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	ALTER TABLE RecHubAlert.EventLog ADD
		CONSTRAINT FK_EventLog_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey);

	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.OLD_dimBanks') IS NOT NULL
		DROP TABLE RecHubData.OLD_dimBanks;
END
ELSE
	RAISERROR('WI 139927 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
