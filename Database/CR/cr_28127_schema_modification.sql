--WFSSCRIPTPROCESSORSCRNAME Updating OLTA.factBatchSummary Index
--WFSSCRIPTPROCESSORSCRBEGIN
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factBatchSummary]') AND name = N'IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID')
	CREATE INDEX IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID ON OLTA.factBatchSummary (BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,BatchID) ON OLTAFacts_Partition_Scheme(DepositDateKey) -- (CR 28127 - 11/09/2009 JPB)
--WFSSCRIPTPROCESSORSCRCREND