--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 151219084
--WFSScriptProcessorPrint Changing PaymentsOnlyTransaction to InvoiceRequired in RecHubData.dimWorkgroupBusinessRules if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS
	( 
		SELECT 
			1
		FROM 
			sys.database_permissions
			INNER JOIN sys.database_principals ON sys.database_principals.principal_id = sys.database_permissions.grantee_principal_id
			INNER JOIN sys.types ON sys.types.user_type_id = sys.database_permissions.major_id
			INNER JOIN sys.schemas on sys.schemas.schema_id = sys.types.schema_id
		WHERE 
			sys.database_permissions.class = 6 /* Type */
			AND sys.schemas.name = 'RecHubException'
			AND sys.types.name = 'DataEntryValueChangeTable'
			AND sys.database_principals.name = 'dbRole_RecHubException'
			AND sys.database_permissions.permission_name = 'EXECUTE'
			AND sys.database_permissions.state_desc = 'GRANT'
	)
BEGIN
	RAISERROR('Adding EXECUTE permission for dbRole_RecHubException to RecHubException.DataEntryValueChangeTable',10,1) WITH NOWAIT;
	GRANT EXECUTE ON TYPE::[RecHubException].[DataEntryValueChangeTable] TO [dbRole_RecHubException];
END
ELSE
	RAISERROR('EXECUTE permission for dbRole_RecHubException to RecHubException.DataEntryValueChangeTable already exists',10,1) WITH NOWAIT;

IF( IS_ROLEMEMBER('dbRole_RecHubException','RecHubUser_User') = 0 )
BEGIN
	RAISERROR('Adding RecHubUser_User to dbRole_RecHubException',10,1) WITH NOWAIT;
	EXEC SP_ADDROLEMEMBER 'dbRole_RecHubException', 'RecHubUser_User'; 
END
ELSE
	RAISERROR('RecHubUser_User already belongs to dbRole_RecHubException',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
