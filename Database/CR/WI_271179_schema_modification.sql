--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271179
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factStubs.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factStubs')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factStubs'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_DepositDatefactStubKey')
			DROP INDEX IDX_factStubs_DepositDatefactStubKey ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_BankKey')
			DROP INDEX IDX_factStubs_BankKey ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_OrganizationKey')
			DROP INDEX IDX_factStubs_OrganizationKey ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_ClientAccountKey')
			DROP INDEX IDX_factStubs_ClientAccountKey ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_ImmutableDateKey')
			DROP INDEX IDX_factStubs_ImmutableDateKey ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID')
			DROP INDEX IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factStubs;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted')
			DROP INDEX IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted ON RecHubData.factStubs;


		/****** Object:  Index PK_factStubs    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factStubs')
			ALTER TABLE RecHubData.factStubs DROP CONSTRAINT PK_factStubs;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factStubs ADD
			CONSTRAINT PK_factStubs PRIMARY KEY NONCLUSTERED (factStubKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factStubs_DepositDatefactStubKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factStubs_DepositDatefactStubKey ON RecHubData.factStubs 
		(
			DepositDateKey,
			factStubKey
		) $(OnDataPartition);


		RAISERROR('Rebuilding index IDX_factStubs_BankKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factStubs_BankKey ON RecHubData.factStubs (BankKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factStubs_OrganizationKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factStubs_OrganizationKey ON RecHubData.factStubs (OrganizationKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factStubs_ClientAccountKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factStubs_ClientAccountKey ON RecHubData.factStubs (ClientAccountKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factStubs_ImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factStubs_ImmutableDateKey ON RecHubData.factStubs (ImmutableDateKey) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factStubs
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			BatchID ASC,
			TransactionID ASC
		) $(OnDataPartition);

		RAISERROR('Rebuilding index IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted ON RecHubData.factStubs
		(
			DepositDateKey ASC,
			BatchID ASC,
			TransactionID ASC,
			TxnSequence ASC,
			IsDeleted ASC
		)
		INCLUDE
		(
			BatchSequence,
			StubSequence,
			factStubKey
		) $(OnDataPartition);


	END
	ELSE
		RAISERROR('WI 271179 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271179 RecHubData.factStubs table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd