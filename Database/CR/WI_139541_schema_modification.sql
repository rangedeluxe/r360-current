--WFSScriptProcessorPrint WI 139541
--WFSScriptProcessorPrint Script for the initial load of RecHubData.ClientAccountBatchPaymentTypes.
--WFSScriptProcessorPrint Needs to run against R360 database. It will not hurt if ran additional times.
--WFSScriptProcessorCRBegin

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'ClientAccountBatchPaymentTypes')
BEGIN

	RAISERROR('Loading table RecHubData.ClientAccountBatchPaymentTypes.',10,1) WITH NOWAIT

	INSERT INTO RecHubData.ClientAccountBatchPaymentTypes
	(
		ClientAccountKey,
		BatchPaymentTypeKey
	)
	SELECT DISTINCT
		RecHubData.factBatchSummary.ClientAccountKey,
		RecHubData.factBatchSummary.BatchPaymentTypeKey
	FROM
		RecHubData.factBatchSummary 
		LEFT OUTER JOIN RecHubData.ClientAccountBatchPaymentTypes
		ON RecHubData.factBatchSummary.ClientAccountKey  = RecHubData.ClientAccountBatchPaymentTypes.ClientAccountKey
			AND RecHubData.factBatchSummary.BatchPaymentTypeKey = RecHubData.ClientAccountBatchPaymentTypes.BatchPaymentTypeKey
	WHERE 
		RecHubData.ClientAccountBatchPaymentTypes.ClientAccountKey IS NULL
END
ELSE
	RAISERROR('WI 135935 needs to be applied to the database before this script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd


