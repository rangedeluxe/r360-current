--WFSScriptProcessorPrint CR 34259
--WFSScriptProcessorPrint Update factDocuments IDX_factDocuments_DepositDateKey if necessary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factDocuments]') AND name = N'IDX_factDocuments_DepositDateKey')
BEGIN
	IF NOT EXISTS (	SELECT	1
					FROM	sys.tables
							INNER JOIN sys.indexes ON sys.tables.[object_id] = sys.indexes.[object_id]
							INNER JOIN sys.index_columns ON sys.index_columns.[object_id] = sys.indexes.[object_id] and sys.index_columns.[index_id] = sys.indexes.[index_id]
							INNER JOIN sys.all_columns ON sys.index_columns.[object_id] = sys.all_columns.[object_id] and sys.index_columns.[column_id] = sys.all_columns.[column_id]
					WHERE	sys.tables.[name] = 'factDocuments' AND sys.indexes.[name] = 'IDX_factDocuments_DepositDateKey' AND sys.all_columns.[name] = 'LockboxKey'
				)
	BEGIN

		DROP INDEX [IDX_factDocuments_DepositDateKey] ON [OLTA].[factDocuments]
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDocuments', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factDocuments';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28193 JPB	GlobalBatchID is now nullable.
* 03/11/2010 CR 29183 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32300 JPB	Added BatchSourceKey.
* 02/07/2011 CR 32598 JPB 	Added ModificationDate.
* 03/15/2011 CR 33351 JPB	Added new index.
* 05/13/2011 CR 34259 JPB	Updated clustered index for performance.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factDocuments

		CREATE CLUSTERED INDEX IDX_factDocuments_DepositDateKey ON OLTA.factDocuments (DepositDateKey, LockboxKey,ProcessingDateKey,BatchID,BatchSequence) $(OnPartition)
	END
END
--WFSScriptProcessorCREnd
