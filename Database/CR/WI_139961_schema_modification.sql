--WFSScriptProcessorPrint WI 139961
--WFSScriptProcessorPrint Updating RecHubConfig.ExtractDefinitions if necessary
--WFSScriptProcessorCRBegin
IF EXISTS( SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID(N'RecHubConfig.ExtractDefinitions') )
BEGIN
	IF NOT EXISTS( SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID(N'RecHubConfig.ExtractDefinitions') AND name = N'ExtractDefinitionType')
	BEGIN

		RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ExtractDefinitions' AND CONSTRAINT_NAME='PK_ExtractDefinitions' )
			ALTER TABLE RecHubConfig.ExtractDefinitions DROP CONSTRAINT PK_ExtractDefinitions

		RAISERROR('ReBuilding RecHubConfig.ExtractDefinitions.',10,1) WITH NOWAIT
		EXEC sp_rename 'RecHubConfig.ExtractDefinitions', 'OLD_ExtractDefinitions'

		CREATE TABLE RecHubConfig.ExtractDefinitions
		(
			ExtractDefinitionID		BIGINT IDENTITY(1,1) NOT NULL
				CONSTRAINT PK_ExtractDefinitions PRIMARY KEY CLUSTERED,				
			ExtractDefinition		XML NOT NULL,
			ExtractName				VARCHAR(128) NOT NULL,
			[Description]			VARCHAR(255) NOT NULL,
			ExtractDefinitionSizeKb INT NOT NULL,
			ExtractDefinitionType	INT,
			IsActive				BIT NOT NULL,
			ExtractFilename			VARCHAR(128),
			CreationDate			DATETIME NOT NULL,
			ModificationDate		DATETIME NOT NULL,
			CreatedBy				VARCHAR(128) NOT NULL,
			ModifiedBy				VARCHAR(128) NOT NULL
		)

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubConfig', 'TABLE', 'ExtractDefinitions', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubConfig',
				@level1type = N'TABLE',
				@level1name = N'ExtractDefinitions';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 08/23/2013
*
* Purpose: Store the Extract Definition.
*
*
* Modification History
* 05/15/2013 WI 97649 JBS	Created for 2.0 
* 05/06/2014 WI 139961 JBS	Add ExtractDefinitionType and ExtractFilename
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = RecHubConfig,
	@level1type = N'TABLE',@level1name = ExtractDefinitions

		RAISERROR('Copying data from old table to New table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT RecHubConfig.ExtractDefinitions ON

		INSERT INTO RecHubConfig.ExtractDefinitions 
		(
			ExtractDefinitionID,				
			ExtractDefinition,
			ExtractName,
			[Description],
			ExtractDefinitionSizeKb,
			IsActive,
			CreationDate,
			ModificationDate,
			CreatedBy,
			ModifiedBy	
		)
		SELECT	
			ExtractDefinitionID,				
			ExtractDefinition,
			ExtractName,
			[Description],
			ExtractDefinitionSizeKb,
			IsActive,
			CreationDate,
			ModificationDate,
			CreatedBy,
			ModifiedBy	
		FROM	
			RecHubConfig.OLD_ExtractDefinitions;

		SET IDENTITY_INSERT RecHubConfig.ExtractDefinitions OFF

		IF OBJECT_ID('RecHubConfig.OLD_ExtractDefinitions') IS NOT NULL
			DROP TABLE RecHubConfig.OLD_ExtractDefinitions;

	END
	ELSE
		RAISERROR('WI 139961 has already been applied to the database.',10,1) WITH NOWAIT;
	END
ELSE
	RAISERROR('Need to apply WI 97649, before adding WI 139961',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd