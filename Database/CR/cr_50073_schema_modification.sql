--WFSScriptProcessorPrint CR 50073
--WFSScriptProcessorPrint Update UserPasswordHistory IDX_UserPasswordHistory_UserID to NOCHECK if necessary
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM sys.foreign_keys WHERE SCHEMA_NAME(sys.foreign_keys.schema_id) = 'OLTA' AND OBJECT_NAME(sys.foreign_keys.parent_object_id) = 'UserPasswordHistory' AND name = 'FK_Users_UserPasswordHistory' AND is_disabled = 0)
BEGIN
	ALTER TABLE OLTA.UserPasswordHistory NOCHECK CONSTRAINT FK_Users_UserPasswordHistory;

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'UserPasswordHistory', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'UserPasswordHistory';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/05/2011
*
* Purpose: Store old user passwords
*		   
*
* Modification History
* 08/05/2011 CR 45876 JPB	Created
* 02/08/2012 CR 50073 JPB	Made FK_Users_UserPasswordHistory NOCHECK
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = UserPasswordHistory
END
ELSE
	RAISERROR('CR has already been applied to this database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
