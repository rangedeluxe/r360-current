--WFSScriptProcessorPrint CR 31169 
--WFSScriptProcessorPrint Making DepositDateKey and CustomerKey columns nullable on OLTA.IMSInterfaceQueue table

--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND COLUMN_NAME='DepositDateKey')
	ALTER TABLE OLTA.IMSInterfaceQueue 
	ALTER COLUMN DepositDateKey int NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='IMSInterfaceQueue' AND COLUMN_NAME='CustomerKey')
	ALTER TABLE OLTA.IMSInterfaceQueue 
	ALTER COLUMN CustomerKey int NULL
--WFSScriptProcessorCREnd


