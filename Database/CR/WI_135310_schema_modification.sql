--WFSScriptProcessorPrint WI 135310
--WFSScriptProcessorPrint Updating RecHubData.factBatchData.BatchID if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factBatchData' AND COLUMN_NAME = 'BatchID' AND DATA_TYPE = 'bigint' )
BEGIN
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factBatchData' AND COLUMN_NAME = 'IsDeleted' )
	BEGIN /* verify that WI 90207 has been applied before continuing. */
		RAISERROR('WI 90207 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Updating RecHubData.factBatchData.BatchID',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factBatchData ALTER COLUMN BatchID BIGINT;

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factBatchData', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'factBatchData';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*		   
*
* Modification History
* 03/07/2011 CR 33212 JPB	Created
* 01/12/2012 CR 49281 JPB	Added SourceProcessingDateKey.
* 07/19/2012 CR 54127 JPB	Added BatchNumber.
* 09/08/2012 CR 55277 JPB	Expanded DataValue to 1024.
* 02/14/2013 WI 90207 JBS	Update to 2.0 release.  Change schema name.
*							Renaming columns:
*							CustomerKey to OrganizationKey
*							LockboxKey to ClientAccountKey
*							ProcessingDateKey to ImmutableDateKey
*							LoadDate to CreationDate
*							Renaming all Constraints. 
*							Removed Default Constraints on ModifiedDate, ModifiedBy
*							Change ModifiedBy from 	32 to 128
*							Added factBatchDataKey to the Clustered Index.
* 04/09/2014 WI 135310 JPB	Changed BatchID from INT to BIGINT.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factBatchData;
	END
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
