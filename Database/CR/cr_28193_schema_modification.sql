--WFSSCRIPTPROCESSORSCRNAME Updating OLTA.factDocuments.GlobalBatchID
--WFSSCRIPTPROCESSORSCRBEGIN
IF (SELECT syscolumns.isnullable FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'factDocuments' AND syscolumns.name = 'GlobalBatchID' AND sysobjects.xtype='U') = 0
	ALTER TABLE OLTA.factDocuments ALTER COLUMN GlobalBatchID INT NULL
--WFSSCRIPTPROCESSORSCRCREND