--WFSScriptProcessorPrint CR 32222
--WFSScriptProcessorPrint Removing OLTA.factTransactionDetails if necessary
--WFSScriptProcessorCRBegin
	IF OBJECT_ID('OLTA.factTransactionDetails') IS NOT NULL
	BEGIN
		DROP TABLE OLTA.factTransactionDetails
	END
--WFSScriptProcessorCREnd