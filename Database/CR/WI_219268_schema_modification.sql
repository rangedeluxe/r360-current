--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 219268
--WFSScriptProcessorPrint Adding indexes IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount') 
	BEGIN
		RAISERROR('WI 219268 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary 
		(
			IsDeleted,
			BatchSourceKey,
			DepositDateKey,
			SourceBatchID,
			BatchNumber,
			DepositStatus,
			OMRCount
		)
		INCLUDE 
		(
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			ImmutableDateKey,
			BatchID,
			BatchPaymentTypeKey,
			TransactionID,
			TxnSequence,
			CheckCount,
			StubCount,
			DocumentCount
		) $(OnDataPartition);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factTransactionSummary', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factTransactionSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factTransactionSummary,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28224 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 03/11/2010 CR 29181 JPB	Added new index for Lockbox Search.
* 04/12/2010 CR 29358 JPB	Added ModificationDate.
* 01/12/2011 CR 32285 JPB	Added BatchSourceKey.
* 10/27/2011 CR 47513 JPB	Added TxnSequence to index.
* 01/12/2012 CR 49275 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49552 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51367 JPB	Added INCLUDE columns.
* 03/27/2012 CR 51546 JPB	Added BatchPaymentTypeKey.
* 07/05/2012 CR 53624 JPB	Added BatchCueID.
* 07/17/2012 CR 54120 JPB	Added BatchNumber.
* 07/17/2012 CR 54138 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 CR 71799 JBS 	Update table to 2.0 release. Change schema Name.
*							Add Column: factTransactionSummaryKey, IsDeleted, StubTotal.
*							Remove Column: DESetupID. 
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factTransactionSummaryKey to Clustered Index. 
*							Create New Index based from old clustered index.
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factTransactionSummary_BatchPaymentTypeKey DEFAULT(0),
*							DF_factTransactionSummary_BatchCueID DEFAULT(-1), DF_factTransactionSummary_ModificationDate DEFAULT GETDATE().
*							Forward Patch:  WI 85096
* 05/29/2014 WI 135323 JPB	Changed BatchID to BIGINT.
* 05/29/2014 WI 143853 JPB	Added SourceBatchID.
* 05/29/2014 WI 143854 JPB	Changed SourceBatchKey to SMALLINT.
* 08/18/2014 WI 158618 JPB	Added TransactionExceptionStatuKey.
* 06/10/2015 WI 217860 JBS	Add Index from Regression Analysis.
* 06/19/2015 WI 219268 JBS  Add Index IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount
******************************************************************************/';
	END
--WFSScriptProcessorCREnd