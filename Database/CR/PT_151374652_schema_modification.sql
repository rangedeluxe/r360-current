--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #151374652
--WFSScriptProcessorPrint Drop RecHubData.usp_GetPaymentTypeTotals if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('RecHubData.usp_GetPaymentTypeTotals') IS NOT NULL
BEGIN
	RAISERROR('Dropping RecHubData.usp_GetPaymentTypeTotals.',10,1) WITH NOWAIT;
	DROP PROCEDURE RecHubData.usp_GetPaymentTypeTotals
END
--WFSScriptProcessorCREnd

