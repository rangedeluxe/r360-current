--WFSScriptProcessorCRPrint Creating Temp table for OLTA.factTransactionSummary
--WFSScriptProcessorCRBegin
CREATE TABLE #tmpFactTransactionSummary
(
	BankKey int NOT NULL,
	CustomerKey int NOT NULL,
	LockboxKey int NOT NULL,
	ProcessingDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
	DepositDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
	BatchID int NOT NULL,
	DESetupID int NULL,
	SystemType tinyint NOT NULL,
	DepositStatus int NOT NULL,
	TransactionID int NOT NULL,
	TxnSequence int NOT NULL,
	CheckCount int NOT NULL,
	DocumentCount int NOT NULL,
	ScannedCheckCount int NOT NULL,
	StubCount int NOT NULL,
	OMRCount int NOT NULL,
	CheckTotal money NOT NULL,
	LoadDate datetime NOT NULL
)
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Removing invalid records
--WFSScriptProcessorCRBegin
DELETE FROM OLTA.factTransactionSummary WHERE DepositDateKey IS NULL
--WFSScriptProcessorCREnd


--WFSScriptProcessorCRPrint Copy factBatchSummary to temp table
--WFSScriptProcessorCRBegin
INSERT INTO #tmpFactTransactionSummary
SELECT * FROM OLTA.factTransactionSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factTransactionSummary.FK_DepositDate_factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_DepositDate_factTransactionSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]'))
	ALTER TABLE [OLTA].[factTransactionSummary] DROP CONSTRAINT [FK_DepositDate_factTransactionSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factTransactionSummary.FK_dimBanks_factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimBanks_factTransactionSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]'))
	ALTER TABLE [OLTA].[factTransactionSummary] DROP CONSTRAINT [FK_dimBanks_factTransactionSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factTransactionSummary.FK_dimCustomers_factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimCustomers_factTransactionSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]'))
	ALTER TABLE [OLTA].[factTransactionSummary] DROP CONSTRAINT [FK_dimCustomers_factTransactionSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factTransactionSummary.FK_dimLockboxes_factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_dimLockboxes_factTransactionSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]'))
	ALTER TABLE [OLTA].[factTransactionSummary] DROP CONSTRAINT [FK_dimLockboxes_factTransactionSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping foreign key OLTA.factTransactionSummary.FK_ProcessingDate_factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[OLTA].[FK_ProcessingDate_factTransactionSummary]') AND parent_object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]'))
	ALTER TABLE [OLTA].[factTransactionSummary] DROP CONSTRAINT [FK_ProcessingDate_factTransactionSummary]
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Dropping table OLTA.factTransactionSummary
--WFSScriptProcessorCRBegin
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[OLTA].[factTransactionSummary]') AND type in (N'U'))
	DROP TABLE OLTA.factTransactionSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRPrint Creating table OLTA.factBatchSummary
--WFSScriptProcessorCRBegin
--Cannot check to see if the batch summary table exists because it was just delete, use the checks table instead
IF EXISTS(SELECT 1 FROM sys.partition_schemes where name = 'OLTAFacts_Partition_Scheme')
BEGIN
	CREATE TABLE OLTA.factTransactionSummary(
		   BankKey int NOT NULL,
		   CustomerKey int NOT NULL,
		   LockboxKey int NOT NULL,
		   ProcessingDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		   DepositDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		   BatchID int NOT NULL,
		   DESetupID int NULL,
		   SystemType tinyint NOT NULL,
		   DepositStatus int NOT NULL,
		   TransactionID int NOT NULL,
		   TxnSequence int NOT NULL,
		   CheckCount int NOT NULL,
		   DocumentCount int NOT NULL,
		   ScannedCheckCount int NOT NULL,
		   StubCount int NOT NULL,
		   OMRCount int NOT NULL,
		   CheckTotal money NOT NULL,
		   LoadDate datetime NOT NULL
	) ON OLTAFacts_Partition_Scheme(DepositDateKey)
END
ELSE
BEGIN
	CREATE TABLE OLTA.factTransactionSummary(
		   BankKey int NOT NULL,
		   CustomerKey int NOT NULL,
		   LockboxKey int NOT NULL,
		   ProcessingDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		   DepositDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		   BatchID int NOT NULL,
		   DESetupID int NULL,
		   SystemType tinyint NOT NULL,
		   DepositStatus int NOT NULL,
		   TransactionID int NOT NULL,
		   TxnSequence int NOT NULL,
		   CheckCount int NOT NULL,
		   DocumentCount int NOT NULL,
		   ScannedCheckCount int NOT NULL,
		   StubCount int NOT NULL,
		   OMRCount int NOT NULL,
		   CheckTotal money NOT NULL,
		   LoadDate datetime NOT NULL
	)
END
--WFSScriptProcessorCREnd

--WFSScriptProcessorForeignKey
ALTER TABLE OLTA.factTransactionSummary ADD 
       CONSTRAINT FK_dimBanks_factTransactionSummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
       CONSTRAINT FK_dimCustomers_factTransactionSummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
       CONSTRAINT FK_dimLockboxes_factTransactionSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
       CONSTRAINT FK_ProcessingDate_factTransactionSummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
       CONSTRAINT FK_DepositDate_factTransactionSummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey)
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_DepositDateKey
CREATE CLUSTERED INDEX IDX_factTransactionSummary_DepositDateKey ON OLTA.factTransactionSummary (DepositDateKey)
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_BankKey
CREATE INDEX IDX_factTransactionSummary_BankKey ON OLTA.factTransactionSummary (BankKey)
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_LockboxKey
CREATE INDEX IDX_factTransactionSummary_LockboxKey ON OLTA.factTransactionSummary (LockboxKey)
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_CustomerKey
CREATE INDEX IDX_factTransactionSummary_CustomerKey ON OLTA.factTransactionSummary (CustomerKey)
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_ProcessingDate
CREATE INDEX IDX_factTransactionSummary_ProcessingDate ON OLTA.factTransactionSummary (ProcessingDateKey)

--WFSScriptProcessorCRPrint Copy factTransactionSummary to temp table
--WFSScriptProcessorCRBegin
INSERT INTO OLTA.factTransactionSummary
SELECT * FROM #tmpFactTransactionSummary
--WFSScriptProcessorCREnd

--WFSScriptProcessorCRBegin
DROP TABLE #tmpFactTransactionSummary
--WFSScriptProcessorCREnd
