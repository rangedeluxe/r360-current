--WFSScriptProcessorPrint CR 46026
--WFSScriptProcessorPrint Creating IDX_Session_LogonNameCustomerCode if necessary.
--WFSScriptProcessorIndex OLTA.Session.IDX_Session_LogonNameCustomerCode
--WFSScriptProcessorCRBegin
IF NOT EXISTS (	SELECT	1 
				FROM	sys.indexes
						INNER JOIN sys.tables on sys.indexes.object_id = sys.tables.object_id
				WHERE	SCHEMA_NAME(schema_id) = 'OLTA' AND sys.tables.name = 'Session' AND sys.indexes.name = 'IDX_Session_LogonNameCustomerCode'
			)
BEGIN
	/* Verify that CR 45209 has been applied before continuing. */
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'Session' AND COLUMN_NAME = 'BrowserType' AND CHARACTER_MAXIMUM_LENGTH = 1024)
	BEGIN /* CR 34330 has been applied, continue. */
		RAISERROR('Creating IDX_Session_LogonNameCustomerCode index',10,1) WITH NOWAIT
		CREATE INDEX IDX_Session_LogonNameCustomerCode ON OLTA.[Session] (LogonName,CustomerCode)
	
		RAISERROR('Updating table header information',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'Session', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'Session';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Static Data
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 06/13/2011 CR 45209 JPB 	Updated BrowserType to VARCHAR(1024)
* 08/12/2011 CR 46026 JPB	Added IDX_Session_LogonNameCustomerCode
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = [Session]
	END
	ELSE
	BEGIN
		RAISERROR('CR 45209 must be applied before running this CR.',16,1) WITH NOWAIT
	END
END
