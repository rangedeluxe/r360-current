--WFSScriptProcessorPrint WI 77760
--WFSScriptProcessorPrint Updating OLLockboxes if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[OLLockboxes]') AND name = N'IDX_OLLockboxes_OLCustomerID_SiteBankLockboxID' )
BEGIN
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxes' AND COLUMN_NAME='DisplayBatchID' )
	BEGIN
		RAISERROR('CR 54175 must be applied before this WI.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'OLLockboxes', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'OLLockboxes';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 08/09/2011 CR 46141 JPB	Added InvoiceBalancingOption.
* 02/14/2012 CR 50240 JPB	Added HOA.
* 08/23/2012 CR 54175 JPB	Added DisplayBatchID.
* 01/02/2013 WI 77760 JPB	Added nex index. (FP:77754)
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = OLLockboxes
		
		RAISERROR('Creating index OLTA.OLLockboxes.IDX_OLLockboxes_OLCustomerID_SiteBankLockboxID',10,1) WITH NOWAIT;
		CREATE INDEX IDX_OLLockboxes_OLCustomerID_SiteBankLockboxID ON OLTA.OLLockboxes
		(
			OLCustomerID,
			SiteBankID,
			SiteLockboxID
		)
		INCLUDE
		(    
			CheckImageDisplayMode,
			DocumentImageDisplayMode
		);
	END
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd