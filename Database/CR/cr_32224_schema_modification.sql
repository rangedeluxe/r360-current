--WFSScriptProcessorPrint CR 32224
--WFSScriptProcessorPrint Removing OLTA.usp_factTransactionDetailsInsert if necessary
--WFSScriptProcessorCRBegin
	IF OBJECT_ID('OLTA.usp_factTransactionDetailsInsert') IS NOT NULL
		DROP PROCEDURE OLTA.usp_factTransactionDetailsInsert
--WFSScriptProcessorCREnd