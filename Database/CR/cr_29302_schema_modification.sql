--WFSScriptProcessorPrint CR 29302 
--WFSScriptProcessorPrint Renaming Account to AccountNumber on OLTA.factStubs
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND COLUMN_NAME='Account')
	EXEC sp_rename
    @objname = 'OLTA.factStubs.Account',
    @newname = 'AccountNumber',
    @objtype = 'COLUMN'
--WFSScriptProcessorCREnd
