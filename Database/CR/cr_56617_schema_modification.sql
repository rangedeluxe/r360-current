--WFSScriptProcessorPrint CR 56617
--WFSScriptProcessorPrint Updating dimLockboxes if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[dimLockboxes]') AND name = N'IDX_dimLockboxes_SiteBankLockboxID' )
BEGIN
	/* verify that CR 31470 has been applied before continuing. */
	IF NOT EXISTS (	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'dimLockboxes' AND COLUMN_NAME = 'FileGroup' )
	BEGIN
		RAISERROR('CR 55090 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimLockboxes', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'dimLockboxes';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Lockboxes dimension is a SCD type 2 holding Lockbox info.  Most 
*	recent flag of 1 indicates the current Lockbox row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 05/05/2010 CR 29158 JPB	Added IsCommingled.
* 05/13/2010 CR 29709 JPB	Added SiteLockboxKey.
* 08/02/2010 CR 30378 JPB	Added standard ModificationDate.
* 08/02/2010 CR 30307 JPB	Added default contraint 1 to OnlineColorMode.
* 01/07/2011 CR 31470 JPB 	Added POBox
* 09/11/2012 CR 55090 JPB	Added FileGroup
* 11/01/2012 CR 56617 JPB	Added new index.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = dimLockboxes
		
		RAISERROR('Creating index OLTA.dimLockboxes.IDX_dimLockboxes_SiteBankLockboxID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_dimLockboxes_SiteBankLockboxID ON OLTA.dimLockboxes (SiteBankID, SiteLockboxID) INCLUDE (LockboxKey);
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd