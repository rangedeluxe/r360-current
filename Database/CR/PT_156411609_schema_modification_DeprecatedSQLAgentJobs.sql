--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 156411609 
--WFSScriptProcessorPrint Remove deprecated SQL Agent jobs if necessary.
--WFSScriptProcessorBeginCR


--WFSScriptProcessorSQLAgentJobName integraPAYOTISServices


IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'integraPAYOTISServices')
	BEGIN
		RAISERROR('DELETING integraPAYOTISServices job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'integraPAYOTISServices', @delete_unused_schedule=1;
	END
	ELSE
		RAISERROR('integraPAYOTISServices does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName (WFSDB_R360) RecHubFactTableStats
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'RecHubFactTableStats')
	BEGIN
		RAISERROR('DELETING RecHubFactTableStats job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'RecHubFactTableStats', @delete_unused_schedule=1;
	END
	ELSE
		RAISERROR('RecHubFactTableStats does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName NotificationImportIntegrationServices
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'NotificationImportIntegrationServices')
	BEGIN
		RAISERROR('DELETING NotificationImportIntegrationServices job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'NotificationImportIntegrationServices', @delete_unused_schedule=1;
	END
	ELSE
		RAISERROR('NotificationImportIntegrationServices does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName R360Alerts
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'R360Alerts')
	BEGIN
		RAISERROR('DELETING R360Alerts job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'R360Alerts', @delete_unused_schedule=1;
	END
	ELSE
		RAISERROR('R360Alerts does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName Receivables Hub Update Fact Table Stats
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'Receivables Hub Update Fact Table Stats')
	BEGIN
		RAISERROR('DELETING Receivables Hub Update Fact Table Stats job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'Receivables Hub Update Fact Table Stats', @delete_unused_schedule=1;
	END
	ELSE
		RAISERROR('Receivables Hub Update Fact Table Stats does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName RecHub Table Maintenance
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'RecHub Table Maintenance')
	BEGIN
		RAISERROR('DELETING RecHub Table Maintenance job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'RecHub Table Maintenance', @delete_unused_schedule=1;
	END
	ELSE 
		RAISERROR('RecHub Table Maintenance does not exist',10,1) WITH NOWAIT;

--WFSScriptProcessorSQLAgentJobName RecHub Table Maintenance
IF EXISTS (SELECT 1 FROM msdb.dbo.sysjobs WHERE name = 'RecHub Purge')
	BEGIN
		RAISERROR('DELETING RecHub Purge job',10,1) WITH NOWAIT;
		EXEC  msdb.dbo.sp_delete_job @job_name=N'RecHub Purge', @delete_unused_schedule=1;
	END
	ELSE 
		RAISERROR('RecHub Purge does not exist',10,1) WITH NOWAIT;
--WFSScriptProcessorEndCR