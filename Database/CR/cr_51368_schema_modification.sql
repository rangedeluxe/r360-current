--WFSScriptProcessorPrint CR 51368
--WFSScriptProcessorPrint Creating IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLokboxProcessingDateKeyBatchIDTransactionID in OLTA.factDataEntryDetails if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factDataEntryDetails]') AND name = N'IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID')
BEGIN
	/* verify that CR 49280 has been applied before continuing. */
	IF NOT EXISTS (	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factDataEntryDetails' AND COLUMN_NAME = 'SourceProcessingDateKey' )
	BEGIN
		RAISERROR('CR 49280 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID.',10,1) WITH NOWAIT	
		CREATE NONCLUSTERED INDEX [IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID] ON [OLTA].[factDataEntryDetails] 
		(
			[DataEntryColumnKey] ASC,
			[DepositDateKey] ASC,
			[BankKey] ASC,
			[CustomerKey] ASC,
			[LockboxKey] ASC,
			[ProcessingDateKey] ASC,
			[BatchID] ASC,
			[TransactionID] ASC
		)
		INCLUDE 
		( 
			[BatchSequence],
			[DataEntryValue],
			[DataEntryValueDateTime],
			[DataEntryValueMoney]
		) $(OnPartition)

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDataEntryDetails', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factDataEntryDetails';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28223 JPB	ProcessingDateKey now NOT NULL.
* 12/23/2009 CR 28238  CS	Add index on ProcessingDateKey
* 02/10/2010 CR 28977 JPB	Added ModificationDate.
* 02/12/2010 CR 29012 JPB	Added missing foreign key on processing date.
* 01/06/2011 CR 32233 JPB	Added native data types.
* 01/11/2011 CR 32302 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49280 JPB	Added SourceProcessingDateKey.
* 03/20/2012 CR 51368 JPB	Created new index.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factDataEntryDetails
	END
END
ELSE
	RAISERROR('CR has already been applied to this database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
