--WFSScriptProcessorPrint CR 32329
--WFSScriptProcessorCRBegin
DECLARE @xDESetup XML,
		@LoadDate DATETIME

SELECT @xDESetup = 
	(
		SELECT 	
		(
			SELECT 	DISTINCT RTRIM(DataEntry.TableName) AS TableName,
					RTRIM(DataEntry.FldName) AS FldName,
					COALESCE(RTRIM(DataEntry.ReportTitle),RTRIM(DataEntry.FldPrompt),RTRIM(DataEntry.FldName)) AS ReportTitle,
					COALESCE(DataEntry.FldDataTypeEnum,NULL) AS FldDataTypeEnum,
					COALESCE(DataEntry.FldLength,NULL) AS FldLength,
					COALESCE(DataEntry.ScreenOrder,NULL) AS ScreenOrder
			FROM	dbo.DESetupFields DataEntry
			FOR XML AUTO, TYPE, ROOT('DataEntry')
		)
		FOR XML PATH('Batch'), TYPE, ROOT('Batches')
	)

SELECT @LoadDate = GETDATE()

EXEC OLTA.usp_LoadDataEntryColumnsDimension @xDESetup, @LoadDate
--WFSScriptProcessorCREnd
