--WFSScriptProcessorPrint WI 203744
--WFSScriptProcessorPrint Updating RecHubData.dimClientAccounts DataRetentionDays and ImageRetentionDays to MostRecent value
--WFSScriptProcessorCRBegin
UPDATE 
	RecHubData.dimClientAccounts
SET 
	RecHubData.dimClientAccounts.DataRetentionDays = RecHubData.dimClientAccountsView.DataRetentionDays,
	RecHubData.dimClientAccounts.ImageRetentionDays = RecHubData.dimClientAccountsView.ImageRetentionDays
FROM
	RecHubData.dimClientAccounts
	INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
		AND  RecHubData.dimClientAccountsView.SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID
		AND  RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
WHERE
	RecHubData.dimClientAccounts.MostRecent = 0;
--WFSScriptProcessorCREnd