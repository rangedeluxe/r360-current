--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSBatchNotifyOLTAServiceBroker
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_CDSBatchNotifyOLTAServiceBroker') IS NOT NULL
       DROP PROCEDURE dbo.usp_CDSBatchNotifyOLTAServiceBroker
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[usp_CDSBatchNotifyOLTAServiceBroker]
	@parmProcessingDate datetime,
	@parmBankID int,
	@parmLockboxID int,
	@parmBatchID int,
	@parmActionCode int,
	@parmNotifyIMS bit,
	@parmKeepStats bit = 1
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/08/2009
*
* Purpose: Notify SSB that some action is required on a dbo.batch record.
*	Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* NOTE: Custom for BNYM to be used only during testing of uninstall process.
* Modification History
* 04/18/2011 CR 33920 JPB	Created
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

RETURN