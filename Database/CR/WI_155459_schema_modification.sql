--WFSScriptProcessorPrint WI 155459
--WFSScriptProcessorPrint Script for the initial load of RecHubData.ClientAccountBatchSources.
--WFSScriptProcessorPrint Needs to run against R360 database. It will not hurt if ran additional times.
--WFSScriptProcessorCRBegin

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'ClientAccountBatchSources')
BEGIN

	RAISERROR('Loading table RecHubData.ClientAccountBatchSources.',10,1) WITH NOWAIT

	INSERT INTO RecHubData.ClientAccountBatchSources
	(
		ClientAccountKey,
		BatchSourceKey
	)
	SELECT DISTINCT
		RecHubData.factBatchSummary.ClientAccountKey,
		RecHubData.factBatchSummary.BatchSourceKey
	FROM
		RecHubData.factBatchSummary 
		LEFT OUTER JOIN RecHubData.ClientAccountBatchSources
		ON RecHubData.factBatchSummary.ClientAccountKey  = RecHubData.ClientAccountBatchSources.ClientAccountKey
			AND RecHubData.factBatchSummary.BatchSourceKey = RecHubData.ClientAccountBatchSources.BatchSourceKey
	WHERE 
		RecHubData.ClientAccountBatchSources.ClientAccountKey IS NULL
END
ELSE
	RAISERROR('WI 155456 needs to be applied to the database before this script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd


