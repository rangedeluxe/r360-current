--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 138212537
--WFSScriptProcessorPrint Adding BalancingRequired to RecHubData.dimWorkgroupBusinessRules if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupBusinessRules' AND COLUMN_NAME='BalancingRequired' )
BEGIN

	IF EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupBusinessRules')  -- Proceed
	BEGIN

		RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
		--PK
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupBusinessRules' AND CONSTRAINT_NAME='PK_dimWorkgroupBusinessRules' )
			ALTER TABLE RecHubData.dimWorkgroupBusinessRules DROP CONSTRAINT PK_dimWorkgroupBusinessRules;

		--Defaults
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupBusinessRules_PaymentsOnlyTransaction')
			ALTER TABLE RecHubData.dimWorkgroupBusinessRules DROP CONSTRAINT DF_dimWorkgroupBusinessRules_PaymentsOnlyTransaction;
		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimWorkgroupBusinessRules_ModificationDate')
			ALTER TABLE RecHubData.dimWorkgroupBusinessRules DROP CONSTRAINT DF_dimWorkgroupBusinessRules_ModificationDate; 

		RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
		IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_dimWorkgroupBusinessRules_BankClientAccountID') 
			DROP INDEX RecHubData.dimWorkgroupBusinessRules.IDX_dimWorkgroupBusinessRules_BankClientAccountID;


		RAISERROR('Rebuilding Table RecHubData.dimWorkgroupBusinessRules.',10,1) WITH NOWAIT;
		EXEC sp_rename 'RecHubData.dimWorkgroupBusinessRules', 'OLD_dimWorkgroupBusinessRules';


		CREATE TABLE RecHubData.dimWorkgroupBusinessRules
		(
			dimWorkgroupBusinessRuleKey BIGINT IDENTITY(1,1) NOT NULL 
				CONSTRAINT PK_dimWorkgroupBusinessRules PRIMARY KEY CLUSTERED,
			SiteBankID				INT NOT NULL,
			SiteClientAccountID		INT NOT NULL,
			PaymentsOnlyTransaction BIT NOT NULL
				CONSTRAINT DF_dimWorkgroupBusinessRules_PaymentsOnlyTransaction DEFAULT 0,
			BalancingRequired		BIT NOT NULL
				CONSTRAINT DF_dimWorkgroupBusinessRules_BalancingRequired DEFAULT 0,
			CreationDate			DATETIME NOT NULL,
			ModificationDate		DATETIME NOT NULL 
				CONSTRAINT DF_dimWorkgroupBusinessRules_ModificationDate DEFAULT GETDATE()
		);

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

		SET IDENTITY_INSERT RecHubData.dimWorkgroupBusinessRules ON;
		INSERT INTO RecHubData.dimWorkgroupBusinessRules
		(
			dimWorkgroupBusinessRuleKey,
			SiteBankID ,
			SiteClientAccountID ,
			PaymentsOnlyTransaction ,
			BalancingRequired,
			CreationDate ,
			ModificationDate 
		)
		SELECT
			dimWorkgroupBusinessRuleKey,
			SiteBankID ,
			SiteClientAccountID ,
			PaymentsOnlyTransaction ,
			0,
			CreationDate ,
			ModificationDate 
		FROM
			RecHubData.OLD_dimWorkgroupBusinessRules;

		SET IDENTITY_INSERT RecHubData.dimWorkgroupBusinessRules OFF;

		RAISERROR('Adding IDX_dimWorkgroupBusinessRules_BankClientAccountID',10,1) WITH NOWAIT;
		CREATE UNIQUE NONClUSTERED INDEX IDX_dimWorkgroupBusinessRules_BankClientAccountID ON RecHubData.dimWorkgroupBusinessRules
		(
			SiteBankID,
			SiteClientAccountID
		)
		INCLUDE
		(
			PaymentsOnlyTransaction,
			BalancingRequired
		);


		RAISERROR('Updating RecHubData.dimWorkgroupBusinessRules.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimWorkgroupBusinessRules', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'dimWorkgroupBusinessRules';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = dimWorkgroupBusinessRules,
		@value = N'/******************************************************************************
		** WAUSAU Financial Systems (WFS)
		** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
		*******************************************************************************
		*******************************************************************************
		** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
		*******************************************************************************
		* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
		* other trademarks cited herein are property of their respective owners.
		* These materials are unpublished confidential and proprietary information 
		* of WFS and contain WFS trade secrets.  These materials may not be used, 
		* copied, modified or disclosed except as expressly permitted in writing by 
		* WFS (see the WFS license agreement for details).  All copies, modifications 
		* and derivative works of these materials are property of WFS.
		*
		* Author: JPB
		* Date: 01/05/2017
		*
		* Purpose: Workgoup Business Rules dimension is a type 0 SCD holding  
		*	an entry for each Workgroup/BatchSource business rule. 
		*		   
		*
		* Modification History
		* 01/05/2017 PT#132047521 JPB	Created
		* 02/03/2017 PT 138212537 JBS	Added BalancingRequired
		******************************************************************************/';
		
		
		RAISERROR('Dropping old table',10,1) WITH NOWAIT;
		IF OBJECT_ID('RecHubData.OLD_dimWorkgroupBusinessRules') IS NOT NULL AND EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimWorkgroupBusinessRules')  -- Proceed
			DROP TABLE RecHubData.OLD_dimWorkgroupBusinessRules;


	ELSE    -- Table does not exist  must add it before we can add new column
		RAISERROR('PT 132047521 must be applied first.',16,1) WITH NOWAIT;
	END
		
END
ELSE
	RAISERROR('PT 138212537 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
