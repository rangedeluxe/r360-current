--WFSScriptProcessorPrint CR 28619
--WFSScriptProcessorCRBegin
--WFSScriptProcessorPrint Dropping foriegn key
ALTER TABLE OLTA.LoadStatDetails DROP CONSTRAINT FK_LoadStats_LoadStatID
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Clearing OLTA.LoadStatDetails table
DELETE FROM OLTA.LoadStatDetails
--WFSScriptProcessorCREnd
--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorTableName LoadStats
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/07/2009
*
* Purpose: Store how long it takes to process a batch through the OLTA schema.
*		   
*
* Modification History
* 08/07/2009 CR 25817 JPB	Created
* 12/22/2009 CR 28536 JPB	Changed PK from clustered to non clustered.
* 12/22/2009 CR 28555 JPB	Added IDX_LoadStats_LoadStatTimeStamp as a
*							clustered index to improve performance.
* 01/05/2010 CR 28619 JPB	Added addtional stat columns.
*							Renamed XMLData to OLTAData.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE OLTA.LoadStats
(
		LoadStatID uniqueidentifier NOT NULL 
			CONSTRAINT PK_LoadStats PRIMARY KEY NONCLUSTERED 
			CONSTRAINT [DF_LoadStats_LoadStatID]  DEFAULT NEWID(),
		LoadType smallint NOT NULL,
		OLTAData xml NOT NULL,
		LoadTime int NOT NULL,
		StartTime datetime NOT NULL,
		EndTime datetime NOT NULL,
		LoadStatTimeStamp datetime CONSTRAINT [DF_LoadStats_LoadStatTimeStamp]  DEFAULT GETDATE()
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex OLTA.LoadStats.IDX_LoadStats_LoadStatTimeStamp
CREATE CLUSTERED INDEX [IDX_LoadStats_LoadStatTimeStamp] ON [OLTA].[LoadStats] 
(
	[LoadStatTimeStamp] ASC
)

--WFSScriptProcessorCRBegin
--WFSScriptProcessorPrint Recreating foriegn key
ALTER TABLE OLTA.LoadStatDetails ADD
       CONSTRAINT FK_LoadStats_LoadStatID FOREIGN KEY(LoadStatID) REFERENCES OLTA.LoadStats(LoadStatID)
--WFSScriptProcessorCREnd
