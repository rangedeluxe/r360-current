--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI R360-15398
--WFSScriptProcessorPrint Updating IDX_factBatchSummary_DepositDateClientAccountKeyBatchID
--WFSScriptProcessorCRBegin

IF EXISTS (SELECT COL_NAME(ic.object_id,ic.column_id) AS column_name  
	FROM sys.indexes AS i  
	INNER JOIN sys.index_columns AS ic   
		ON i.object_id = ic.object_id AND i.index_id = ic.index_id  
	WHERE i.name = 'IDX_factBatchSummary_DepositDateClientAccountKeyBatchID'
	AND COL_NAME(ic.object_id,ic.column_id) = 'BatchPaymentTypeKey') 
	BEGIN
		RAISERROR('WI R360-15398 Already applied to factBatchSummary',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		IF EXISTS (SELECT name FROM sys.indexes WHERE name = 'IDX_factBatchSummary_DepositDateClientAccountKeyBatchID')
			DROP INDEX IDX_factBatchSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factBatchSummary
		
		RAISERROR('Creating index IDX_factBatchSummary_DepositDateClientAccountKeyBatchID',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factBatchSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factBatchSummary
		(
			DepositDateKey ASC,
			ClientAccountKey ASC,
			BatchID ASC,
			IsDeleted ASC,				--new for dashboard pie data
			DepositStatus DESC,			--new for dashboard pie data
			BatchSourceKey ASC,			--new for dashboard pie data
			BatchPaymentTypeKey			--new for dashboard pie data
		)
		INCLUDE
		(
			CheckCount,					--new for dashboard pie data
			CheckTotal,					--new for dashboard pie data
			TransactionCount			--new for dashboard pie data
		) $(OnDataPartition);

IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factBatchSummary', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factBatchSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factBatchSummary,
		@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2019 Deluxe Corporation All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 11/19/2009 CR 28189 JPB	Allow GlobalBatchID to be NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
* 01/11/2012 CR 49274 JPB	Added SourceProcessingDateKey.
* 01/19/2012 CR 48978 JPB	Added BatchSiteCode.
* 03/26/2012 CR 51540 JPB	Added BatchPaymentTypeKey.
* 07/03/2012 CR 53623 JPB	Added BatchCueID.
* 07/16/2012 CR 54119 JPB	Added BatchNumber.
* 07/16/2012 CR 54129 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71798 JPB	Update table to 2.0 release. Change schema name.
*							Add columns: factBatchSummaryKey, IsDeleted, DepositStatusKey,
*							StubTotal, DepositDisplayNameKey. 
*							Delete columns: GlobalBatchId, DepositDisplayName.  
*							Rename Columns: 
*							CustomerKey to OrganizationKey,  LockboxKey to ClientAccountKey,
*							ProcessingDateKey to ImmutableDateKey.
*							LoadDate to CreationDate.
*							Renamed ALL constraints.
*							Added factBatchSummaryKey to end of Clustered Index.
*							Create New Index based from old clustered index.
*							FP: WI 77752, WI 85066, WI 85092, WI 85093, WI 85098, WI 87220
* 04/09/2014 WI 135312 JPB	Changed BatchID from INT to BIGINT.
* 05/22/2014 WI	143850 JPB	Add SourceBatchID.
* 05/22/2014 WI 143851 JPB	Changed BatchSourceKey to SMALLINT.
* 09/28/2014 WI 168336 JPB	Added BatchExceptionStatuKey.
* 06/19/2015 WI 219201 MGE  Added index per 2.01 regression testing analysis
* 03/20/2019 R360-15940 MGE	Added index columns for Dashboard response improvement
******************************************************************************/';
	END
--WFSScriptProcessorCREnd