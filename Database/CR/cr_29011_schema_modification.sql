--WFSScriptProcessorPrint CR 29011
--WFSScriptProcessorForeignKey
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS WHERE CONSTRAINT_SCHEMA = 'OLTA' AND CONSTRAINT_NAME = 'FK_ProcessingDate_factDataEntrySummary')
	ALTER TABLE OLTA.factDataEntrySummary ADD
			   CONSTRAINT FK_ProcessingDate_factDataEntrySummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey)
