--WFSScriptProcessorPrint CR 28240 
--WFSScriptProcessorIndex OLTA.factDataEntrySummary.IDX_factDataEntrySummary_ProcessingDateKey
IF NOT EXISTS(SELECT 1 FROM sysindexes WHERE sysindexes.name = 'IDX_factDataEntrySummary_ProcessingDateKey')
	CREATE INDEX IDX_factDataEntrySummary_ProcessingDateKey ON OLTA.factDataEntrySummary(ProcessingDateKey)
