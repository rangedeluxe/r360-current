--WFSScriptProcessorPrint CR 51542
--WFSScriptProcessorPrint Adding BatchPaymentTypeKey to OLTA.factDataEntryDetails if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factDataEntryDetails' AND COLUMN_NAME = 'BatchPaymentTypeKey' )
BEGIN /* verify that CR 51368 has been applied before continuing. */
	IF  NOT EXISTS( SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factDataEntryDetails]') AND name = N'IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID')
	BEGIN
		RAISERROR('CR 51368 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimBanks_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimBanks_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimCustomers_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimCustomers_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimLockboxes_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimLockboxes_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_DepositDate_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_DepositDate_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_ProcessingDate_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_ProcessingDate_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_SourceProcessingDate_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimDataEntryColumns_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimDataEntryColumns_factDataEntryDetails
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimBatchSources_factDataEntryDetails' )
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimBatchSources_factDataEntryDetails

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factDataEntryDetails_ModificationDate')
			ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT DF_factDataEntryDetails_ModificationDate

		RAISERROR('Rebuilding OLTA.factDataEntryDetails adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factDataEntryDetails', 'OLDfactDataEntryDetails'

		CREATE TABLE OLTA.factDataEntryDetails
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL,
			ProcessingDateKey int NOT NULL, --CR 28223 JPB 11/14/2009
			SourceProcessingDateKey int NOT NULL, --CR 49280 JPB 01/12/2012
			DataEntryColumnKey int NOT NULL,
			GlobalBatchID int NOT NULL,
			BatchID int NOT NULL,
			DepositStatus int NOT NULL,
			TransactionID int NOT NULL,
			BatchSequence int NOT NULL,
			DataEntryValue varchar(256) NOT NULL,
			DataEntryValueDateTime DATETIME NULL, --CR 32233 JPB 01/06/2011
			DataEntryValueFloat FLOAT NULL, --CR 32233 JPB 01/06/2011
			DataEntryValueMoney MONEY NULL, --CR 32233 JPB 01/06/2011
			BatchSourceKey TINYINT NOT NULL, --CR 32302 JPB 01/11/2011
			BatchPaymentTypeKey TINYINT NOT NULL --CR 51542 JPB 03/26/2012
				CONSTRAINT DF_factDataEntryDetails_BatchPaymentTypeKey DEFAULT(0),
			LoadDate datetime NOT NULL,
			ModificationDate datetime NOT NULL --CR 28977 JPB 02/10/2010
				CONSTRAINT [DF_factDataEntryDetails_ModificationDate] DEFAULT GETDATE()
		) $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factDataEntryDetails ADD
			CONSTRAINT FK_dimBanks_factDataEntryDetails FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factDataEntryDetails FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factDataEntryDetails FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_DepositDate_factDataEntryDetails FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_ProcessingDate_factDataEntryDetails FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factDataEntryDetails FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimDataEntryColumns_factDataEntryDetails FOREIGN KEY(DataEntryColumnKey) REFERENCES OLTA.dimDataEntryColumns(DataEntryColumnKey),
			CONSTRAINT FK_dimBatchSources_factDataEntryDetails FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey),
			CONSTRAINT FK_dimBatchPaymentTypes_factDataEntryDetails FOREIGN KEY(BatchPaymentTypeKey) REFERENCES OLTA.dimBatchPaymentTypes(BatchPaymentTypeKey)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factDataEntryDetails_DepositDateKey ON OLTA.factDataEntryDetails(DepositDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntryDetails_BankKey ON OLTA.factDataEntryDetails (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntryDetails_CustomerKey ON OLTA.factDataEntryDetails (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntryDetails_LockboxKey ON OLTA.factDataEntryDetails (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryColumnKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntryDetails_DataEntryColumnKey ON OLTA.factDataEntryDetails (DataEntryColumnKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_ProcessingDateKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factDataEntryDetails_ProcessingDateKey ON OLTA.factDataEntryDetails (ProcessingDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID.',10,1) WITH NOWAIT	
		CREATE NONCLUSTERED INDEX [IDX_factDataEntryDetails_DataEntryColumnDepositDateBankCustomerLockboxProcessingDateKeyBatchIDTransactionID] ON [OLTA].[factDataEntryDetails] 
		(
			  [DataEntryColumnKey] ASC,
			  [DepositDateKey] ASC,
			  [BankKey] ASC,
			  [CustomerKey] ASC,
			  [LockboxKey] ASC,
			  [ProcessingDateKey] ASC,
			  [BatchID] ASC,
			  [TransactionID] ASC
		)
		INCLUDE 
		( 
			[BatchSequence],
			[DataEntryValue],
			[DataEntryValueDateTime],
			[DataEntryValueMoney]
		) $(OnPartition)

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factDataEntryDetails', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factDataEntryDetails';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28223 JPB	ProcessingDateKey now NOT NULL.
* 12/23/2009 CR 28238  CS	Add index on ProcessingDateKey
* 02/10/2010 CR 28977 JPB	Added ModificationDate.
* 02/12/2010 CR 29012 JPB	Added missing foreign key on processing date.
* 01/06/2011 CR 32233 JPB	Added native data types.
* 01/11/2011 CR 32302 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49280 JPB	Added SourceProcessingDateKey.
* 03/20/2012 CR 51368 JPB	Created new index.
* 03/26/2012 CR 51542 JPB	Added BatchPaymentTypeKey
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factDataEntryDetails
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factDataEntryDetails
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,	
			ProcessingDateKey,
			SourceProcessingDateKey,
			DataEntryColumnKey,
			GlobalBatchID,
			BatchID,
			DepositStatus,
			TransactionID,
			BatchSequence,
			DataEntryValue,
			DataEntryValueDateTime,
			DataEntryValueFloat,
			DataEntryValueMoney,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				DataEntryColumnKey,
				GlobalBatchID,
				BatchID,
				DepositStatus,
				TransactionID,
				BatchSequence,
				DataEntryValue,
				DataEntryValueDateTime,
				DataEntryValueFloat,
				DataEntryValueMoney,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactDataEntryDetails

		IF OBJECT_ID('OLTA.OLDfactDataEntryDetails') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factDataEntryDetails table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactDataEntryDetails
		END
	END	
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT 
--WFSScriptProcessorCREnd

