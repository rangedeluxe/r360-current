--WFSScriptProcessorPrint CR 51659
--WFSScriptProcessorPrint Adding IDX_factChecks_LockboxKeyDepositProcessingDateKeyRemitterKeyBatchIDAmountSerialDepositStatus to OLTA.factChecks if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factChecks]') AND name = N'IDX_factChecks_LockboxKeyDepositProcessingDateKeyRemitterKeyBatchIDAmountSerialDepositStatus')
BEGIN
	RAISERROR('Adding index IDX_factChecks_LockboxKeyDepositProcessingDateKeyRemitterKeyBatchIDAmountSerialDepositStatus to OLTA.factChecks.',10,1) WITH NOWAIT
	CREATE NONCLUSTERED INDEX [IDX_factChecks_LockboxKeyDepositProcessingDateKeyRemitterKeyBatchIDAmountSerialDepositStatus] ON [OLTA].[factChecks]
	(
			[LockboxKey] ASC,
			[DepositDateKey] ASC,
			[ProcessingDateKey] ASC,
			[RemitterKey] ASC,
			[BatchID] ASC,
			[Amount] ASC,
			[Serial] ASC,
			[DepositStatus] ASC
	) $(OnPartition)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factChecks', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factChecks';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51372 JPB	Renamed index and added INCLUDE columns.
* 04/02/2012 CR 51659 JPB	Added new index to improve remittence search.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = factChecks


END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd