--WFSScriptProcessorPrint WI 138814
--WFSScriptProcessorPrint Changing CommonExceptions table name if necessary.
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorCRBegin
SET ARITHABORT ON
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'IntegraPAYExceptions')
BEGIN
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'CommonExceptions' AND COLUMN_NAME = 'TransactionID') 
	BEGIN
		RAISERROR('WI 101865 must be applied before this WI.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Renaming Table RecHubException.CommonExceptions to RecHubException.IntegraPAYExceptions.',10,1) WITH NOWAIT
		
		IF(EXISTS(SELECT 1 FROM sys.databases INNER JOIN INFORMATION_SCHEMA.TABLES ON TABLE_CATALOG = [NAME] WHERE is_cdc_enabled = 1 AND TABLE_SCHEMA = 'RecHubException' AND TABLE_NAME = 'CommonExceptions'))
		IF( EXISTS(SELECT 1 FROM cdc.change_tables WHERE capture_instance = N'RecHubException_CommonExceptions') )
		BEGIN
		RAISERROR('Disable Change Data Capture for RecHubException.CommonExceptions.',10,1) WITH NOWAIT
		EXECUTE sys.sp_cdc_disable_table 
			@source_schema = N'RecHubException', 
			@source_name = N'CommonExceptions',
			@capture_instance = N'RecHubException_CommonExceptions';
		END
			
		RAISERROR('Dropping Foreign Keys.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='CommonExceptions' AND CONSTRAINT_NAME='FK_CommonExceptions_dimBatchSources' )
			ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT FK_CommonExceptions_dimBatchSources

		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='CommonExceptions' AND CONSTRAINT_NAME='FK_CommonExceptions_dimBatchPaymentTypes' )
			ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT FK_CommonExceptions_dimBatchPaymentTypes
		
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='DecisioningTransactions' AND CONSTRAINT_NAME='FK_DecisioningTransactions_CommonExceptions' )
			ALTER TABLE RecHubException.DecisioningTransactions DROP CONSTRAINT FK_DecisioningTransactions_CommonExceptions

		RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubException' AND TABLE_NAME='CommonExceptions' AND CONSTRAINT_NAME='PK_CommonExceptions' )
			ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT PK_CommonExceptions

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'RecHubException.CommonExceptions') AND sys.objects.name = 'DF_CommonExceptions_CreationDate' )
			ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT DF_CommonExceptions_CreationDate

		IF EXISTS(	SELECT	1 
					FROM	sys.columns LEFT OUTER JOIN sys.objects ON sys.objects.object_id = sys.columns.default_object_id AND sys.objects.type = 'D' 
					WHERE sys.columns.object_id = object_id(N'RecHubException.CommonExceptions') AND sys.objects.name = 'DF_CommonExceptions_ModificationDate' )
			ALTER TABLE RecHubException.CommonExceptions DROP CONSTRAINT DF_CommonExceptions_ModificationDate


		RAISERROR('Building RecHubException.IntegraPAYExceptions.',10,1) WITH NOWAIT
		EXEC sp_rename 'RecHubException.CommonExceptions', 'OLD_CommonExceptions'

		CREATE TABLE RecHubException.IntegraPAYExceptions 
		(
			IntegraPAYExceptionKey   BIGINT NOT NULL IDENTITY(1,1) 
				CONSTRAINT PK_IntegraPAYExceptions PRIMARY KEY CLUSTERED,
			DepositDateKey		INT NOT NULL,
			DecisioningStatus	TINYINT NOT NULL,
			InProcessException	BIT NOT NULL,
			BatchSourceKey		SMALLINT NOT NULL, 
			BatchPaymentTypeKey	TINYINT,
			Deadline			DATETIME NOT NULL,
			CheckAmount			MONEY,
			GlobalBatchID		INT,
			SiteBankID			INT,
			SiteClientAccountID	INT,
			ImmutableDateKey	INT,
			BatchID				BIGINT,
			TransactionID		INT,
			CreationDate		DATETIME NOT NULL
				CONSTRAINT DF_IntegraPAYExceptions_CreationDate DEFAULT GETDATE(),
			ModificationDate	DATETIME NOT NULL
				CONSTRAINT DF_IntegraPAYExceptions_ModificationDate DEFAULT GETDATE()
		);
		
		RAISERROR('Updating RecHubException.IntegraPAYExceptions table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubException', 'TABLE', 'IntegraPAYExceptions', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubException',
				@level1type = N'TABLE',
				@level1name = N'IntegraPAYExceptions';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/13/2013
*
* Purpose: Holds data for IntegraPay Common Exception.
* PaymentSource
*	0 - Unknown
*   1 - integraPAY
*   2 - WebDDL
*   3 - ImageRPS   
*		   
*
* Modification History
* 05/13/2013 WI 101865 JBS	Created 
* 06/04/2013 WI 101865 WJS	Added TransactionID
* 05/05/2014 WI 138814 JBS	Changed table name from CommonExceptions and PK, 
*							Change BatchSourceKey TO SMALLINT
* 11/25/2014 WI 138814 JBS	Change BatchID to BIGINT
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubException,
		@level1type = N'TABLE',@level1name = IntegraPAYExceptions

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT RecHubException.IntegraPAYExceptions ON

		INSERT INTO RecHubException.IntegraPAYExceptions 
		(
			IntegraPAYExceptionKey,
			DepositDateKey,
			DecisioningStatus,
			InProcessException,
			BatchSourceKey, 
			BatchPaymentTypeKey,
			Deadline,
			CheckAmount,
			GlobalBatchID,
			SiteBankID,
			SiteClientAccountID,
			ImmutableDateKey,
			BatchID,
			TransactionID,
			CreationDate,
			ModificationDate
		)
		SELECT	
			CommonExceptionID,
			DepositDateKey,
			DecisioningStatus,
			InProcessException,
			BatchSourceKey, 
			BatchPaymentTypeKey,
			Deadline,
			CheckAmount,
			GlobalBatchID,
			SiteBankID,
			SiteClientAccountID,
			ImmutableDateKey,
			BatchID,
			TransactionID,
			CreationDate,
			ModificationDate
		FROM	
			RecHubException.OLD_CommonExceptions

		SET IDENTITY_INSERT RecHubException.IntegraPAYExceptions OFF

		RAISERROR('Rebuilding Foreign Keys.',10,1) WITH NOWAIT;
		ALTER TABLE RecHubException.IntegraPAYExceptions ADD CONSTRAINT FK_IntegraPAYExceptions_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);
		ALTER TABLE RecHubException.IntegraPAYExceptions ADD CONSTRAINT FK_IntegraPAYExceptions_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);
		
		IF OBJECT_ID('RecHubException.OLD_CommonExceptions') IS NOT NULL
			DROP TABLE RecHubException.OLD_CommonExceptions;
	END
END
ELSE
	RAISERROR('WI 138814 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
