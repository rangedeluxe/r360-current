--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 141435715, 142595657 
--WFSScriptProcessorPrint Add CreationDateKey to SessionLog Table and partition it
--WFSScriptProcessorCRBegin

DECLARE @SessionRetentionDays	INT = 49,
		@BeginDate				DATETIME,
		@BeginDateKey			INT,
		@Message				VARCHAR(90)

IF NOT EXISTS  (SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'SessionLog'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubUser') = sys.tables.schema_id )
BEGIN
	RAISERROR('Beginning Process to Add CreationDateKey and/or partition the SessionLog table',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionsRetentionDays'
	SELECT @BeginDate = GETDATE() - @SessionRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @BeginDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate <= @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest date of SessionLog rows to keep is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionLog' AND CONSTRAINT_NAME='FK_SessionLog_Session' )
		ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT FK_SessionLog_Session;
	
	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	--PK

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionLog' AND CONSTRAINT_NAME='PK_SessionLog' )
		ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT PK_SessionLog;

	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionLog_CreationDateKey')
		ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT DF_SessionLog_CreationDateKey;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionLog_DateTimeEntered')
		ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT DF_SessionLog_DateTimeEntered;
		
	RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionLog_SessionIDPageCounter') 
		DROP INDEX RecHubUser.SessionLog.IDX_SessionLog_SessionIDPageCounter;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionLog_CreationDateKeyLogEntryID') 
		DROP INDEX RecHubUser.SessionLog.IDX_SessionLog_CreationDateKeyLogEntryID;


	RAISERROR('Rebuilding Table RecHubUser.SessionLog.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubUser.SessionLog', 'OLD_SessionLog';

	CREATE TABLE RecHubUser.[SessionLog]
	(
	LogEntryID BIGINT IDENTITY(1,1) NOT NULL,
	SessionID UNIQUEIDENTIFIER NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_SessionLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	PageCounter INT NOT NULL,
	PageName VARCHAR(30) NOT NULL,
	DateTimeEntered DATETIME NOT NULL
		CONSTRAINT DF_SessionLog_DateTimeEntered DEFAULT (GETDATE()),
	ScriptName VARCHAR(255) NOT NULL,
	FormFields TEXT NULL
	) ON Sessions (CreationDateKey);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT RecHubUser.SessionLog ON;
	INSERT INTO RecHubUser.SessionLog
	(
		LogEntryID,
		SessionID,
		CreationDateKey,
		PageCounter,
		PageName,
		DateTimeEntered,
		ScriptName,
		FormFields
	)
	SELECT
		LogEntryID,
		SessionID,
		CAST(CONVERT(VARCHAR(10), DateTimeEntered, 112) AS INT) AS CreationDateKey,
		PageCounter,
		PageName,
		DateTimeEntered,
		ScriptName,
		FormFields
	FROM
		RecHubUser.OLD_SessionLog
	WHERE
		DateTimeEntered > @BeginDate - 1

	SET IDENTITY_INSERT RecHubUser.SessionLog OFF;

	RAISERROR('Adding Primary/Foreign Keys',10,1) WITH NOWAIT;

	ALTER TABLE RecHubUser.SessionLog ADD 
		CONSTRAINT PK_SessionLog PRIMARY KEY NONCLUSTERED (LogEntryID, CreationDateKey)
	ALTER TABLE RecHubUser.SessionLog ADD 
		CONSTRAINT FK_SessionLog_Session FOREIGN KEY(SessionID) REFERENCES RecHubUser.[Session](SessionID);
	

	RAISERROR('Adding IDX_SessionLog_CreationDateKeyLogEntryID',10,1) WITH NOWAIT;
	CREATE CLUSTERED INDEX IDX_SessionLog_CreationDateKeyLogEntryID ON RecHubUser.SessionLog
	(
		CreationDateKey ASC,
		LogEntryID ASC
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionLog_SessionIDPageCounter',10,1) WITH NOWAIT;
	CREATE INDEX IDX_SessionLog_SessionIDPageCounter ON RecHubUser.SessionLog
	(
		SessionID ASC,
		PageCounter ASC
	) ON Sessions (CreationDateKey);

	
	RAISERROR('Updating RecHubUser.SessionLog.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'SessionLog', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubUser',
			@level1type = N'TABLE',
			@level1name = N'SessionLog';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubUser,
	@level1type = N'TABLE',@level1name = SessionLog,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Table stores Session log
*		   
*
* Modification History
* 03/09/2009 CR 25817		JJR	Created
* 10/22/2012 CR 56447		JPB	Added new index.
* 02/28/2013 WI 89653		JBS	Update Table to 2.0 release. Change Schema Name.
*								Change LogEntryID from INT to BIGINT
* 06/01/2015 WI 216344		JBS	Change IDX_SessionLog_SessionIDPageCounter to Clustered INDEX. Per 216076
* 03/29/2017 PT 141435715	MGE	Add CreationDateKey & as part of clustered index to prepare for patitioning.
* 04/03/2017 PT 142595657	MGE Add partition key
*************************************************************************************************************/';
				
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubUser.OLD_SessionLog') IS NOT NULL
		DROP TABLE RecHubUser.OLD_SessionLog;

END
ELSE
	RAISERROR('PT 142595657 SessionLog has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd