--WFSScriptProcessorPrint CR 33123
--WFSScriptProcessorPrint Removed IDENTITY property from BatchSourceKey if necessary
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='dimBatchSources' AND COLUMN_NAME='BatchSourceKey' AND COLUMNPROPERTY(object_id('OLTA.dimBatchSources'), 'BatchSourceKey', 'IsIdentity') = 1 )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factBatchSummary' )
		ALTER TABLE OLTA.factBatchSummary DROP CONSTRAINT FK_dimBatchSources_factBatchSummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimBatchSources_factChecks' )
		ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimBatchSources_factChecks

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntryDetails' AND CONSTRAINT_NAME='FK_dimBatchSources_factDataEntryDetails' )
		ALTER TABLE OLTA.factDataEntryDetails DROP CONSTRAINT FK_dimBatchSources_factDataEntryDetails

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDataEntrySummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factDataEntrySummary' )
		ALTER TABLE OLTA.factDataEntrySummary DROP CONSTRAINT FK_dimBatchSources_factDataEntrySummary

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_dimBatchSources_factDocuments' )
		ALTER TABLE OLTA.factDocuments DROP CONSTRAINT FK_dimBatchSources_factDocuments

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factStubs' AND CONSTRAINT_NAME='FK_dimBatchSources_factStubs' )
		ALTER TABLE OLTA.factStubs DROP CONSTRAINT FK_dimBatchSources_factStubs

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimBatchSources_factTransactionSummary

	RAISERROR('Dropping dimBatchSources contraints',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='dimBatchSources' AND CONSTRAINT_NAME='PK_dimBatchSources' )
		ALTER TABLE OLTA.dimBatchSources DROP CONSTRAINT PK_dimBatchSources

	RAISERROR('Rebuilding OLTA.dimBatchSources',10,1) WITH NOWAIT
	EXEC sp_rename 'OLTA.dimBatchSources', 'OLDdimBatchSources'

	CREATE TABLE OLTA.dimBatchSources
	(
		BatchSourceKey TINYINT NOT NULL 
			CONSTRAINT PK_dimBatchSources PRIMARY KEY CLUSTERED,
		ShortName varchar(30) NOT NULL,
		LongName varchar(128) NOT NULL,
		LoadDate datetime NOT NULL
	)
	
	RAISERROR('Creating index IDX_dimBatchSources_ShortName',10,1) WITH NOWAIT
	CREATE UNIQUE INDEX IDX_dimBatchSources_ShortName ON OLTA.dimBatchSources (ShortName)

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'dimBatchSources', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'dimBatchSources';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/10/2011
*
* Purpose: Batch Sources dimension is a static data dimension for batch sources. 
*
* Modification History
* 01/10/2011 CR 32310 JPB	Created
* 03/01/2011 CR 33123 JPB	Removed IDENTITY property from BatchSourceKey.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = dimBatchSources

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
	INSERT INTO OLTA.dimBatchSources 
	(
		BatchSourceKey,
		ShortName,
		LongName,
		LoadDate
	) 
	SELECT	BatchSourceKey,
			ShortName,
			LongName,
			LoadDate
	FROM 	OLTA.OLDdimBatchSources

	RAISERROR('Rebuilding Foreign Keys',10,1) WITH NOWAIT
	ALTER TABLE OLTA.factBatchSummary ADD 
       CONSTRAINT FK_dimBatchSources_factBatchSummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)

	ALTER TABLE OLTA.factChecks ADD
		CONSTRAINT FK_dimBatchSources_factChecks FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
			
	ALTER TABLE OLTA.factDataEntryDetails ADD
		CONSTRAINT FK_dimBatchSources_factDataEntryDetails FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)

	ALTER TABLE OLTA.factDataEntrySummary ADD
		CONSTRAINT FK_dimBatchSources_factDataEntrySummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)

	ALTER TABLE OLTA.factDocuments ADD
		CONSTRAINT FK_dimBatchSources_factDocuments FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)

	ALTER TABLE OLTA.factStubs ADD
		CONSTRAINT FK_dimBatchSources_factStubs FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
			
	ALTER TABLE OLTA.factTransactionSummary ADD 
		CONSTRAINT FK_dimBatchSources_factTransactionSummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)

	RAISERROR('Dropping old table',10,1) WITH NOWAIT
	IF OBJECT_ID('OLTA.OLDdimBatchSources') IS NOT NULL
		DROP TABLE OLTA.OLDdimBatchSources

END