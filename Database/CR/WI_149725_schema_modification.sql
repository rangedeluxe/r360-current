--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 149725
--WFSScriptProcessorPrint Update RecHubConfig.ReportConfigurations if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubConfig' AND TABLE_NAME='ReportConfigurations' AND COLUMN_NAME='RAAMResourceName' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ReportConfigurations' AND CONSTRAINT_NAME='FK_ReportConfigurations_ReportGroups' )
		ALTER TABLE RecHubConfig.ReportConfigurations DROP CONSTRAINT FK_ReportConfigurations_ReportGroups;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ReportParameters' AND CONSTRAINT_NAME='FK_ReportParameters_ReportConfigurations' )
		ALTER TABLE RecHubConfig.ReportParameters DROP CONSTRAINT FK_ReportParameters_ReportConfigurations;

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='ReportInstance' AND CONSTRAINT_NAME='FK_ReportInstance_ReportConfigurations' )
		ALTER TABLE RecHubUser.ReportInstance DROP CONSTRAINT FK_ReportInstance_ReportConfigurations;

	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ReportConfigurations' AND CONSTRAINT_NAME='PK_ReportConfigurations' )
		ALTER TABLE RecHubConfig.ReportConfigurations DROP CONSTRAINT PK_ReportConfigurations;

	RAISERROR('Rebuilding RecHubConfig.ReportConfigurations',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubConfig.ReportConfigurations', 'Old_ReportConfigurations';

	CREATE TABLE RecHubConfig.ReportConfigurations
	(
		ReportID			INT				NOT NULL
			CONSTRAINT PK_ReportConfigurations PRIMARY KEY,
		GroupID				INT				NULL,
		OrderValue			INT				NOT NULL,
		RAAMResourceName	NVARCHAR(50)	NOT NULL,
		ReportName			VARCHAR(70)		NOT NULL,
		XmlParameterName	VARCHAR(40)		NOT NULL,	
		ReportFileName		VARCHAR(70)		NOT NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubConfig', 'TABLE', 'ReportConfigurations', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubConfig',
			@level1type = N'TABLE',
			@level1name = N'ReportConfigurations';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/24/2013
*
* Purpose:	Contains the report configuration information.
*
* Modification History
* 06/24/2013 WI 107179 KLC	Created
* 06/24/2014 WI 149725 JPB	RAAM updates
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubConfig,
	@level1type = N'TABLE',@level1name = ReportConfigurations;

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;
	
	INSERT INTO RecHubConfig.ReportConfigurations 
	(
		ReportID,
		GroupID,
		OrderValue,
		RAAMResourceName,
		ReportName,
		XmlParameterName,	
		ReportFileName	
	) 
	SELECT	
		ReportID,
		GroupID,
		OrderValue,
		PermissionName,
		ReportName,
		XmlParameterName,	
		ReportFileName	
	FROM 	
		RecHubConfig.Old_ReportConfigurations
		INNER JOIN RecHubUser.[Permissions] ON RecHubUser.[Permissions].PermissionID = RecHubConfig.Old_ReportConfigurations.PermissionID;

	RAISERROR('Applying Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubConfig.ReportConfigurations ADD 
		CONSTRAINT FK_ReportConfigurations_ReportGroups FOREIGN KEY (GroupID) REFERENCES RecHubConfig.ReportGroups(GroupID);

	ALTER TABLE RecHubConfig.ReportParameters ADD 
		CONSTRAINT FK_ReportParameters_ReportConfigurations FOREIGN KEY (ReportID) REFERENCES RecHubConfig.ReportConfigurations(ReportID);

	ALTER TABLE RecHubUser.ReportInstance ADD 
		CONSTRAINT FK_ReportInstance_ReportConfigurations FOREIGN KEY (ReportID) REFERENCES RecHubConfig.ReportConfigurations(ReportID);

	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubConfig.Old_ReportConfigurations') IS NOT NULL
		DROP TABLE RecHubConfig.Old_ReportConfigurations;
END
ELSE
	RAISERROR('WI 149725 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
