--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 199060
--WFSScriptProcessorPrint Removing deprecated columns from RecHubUser.Users if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubUser' AND TABLE_NAME='UserPermissions' )
BEGIN
	RAISERROR('Dropping Table RecHubUser.UserPermissions',10,1) WITH NOWAIT;
	DROP TABLE RecHubUser.UserPermissions;

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubUser' AND TABLE_NAME='UserPasswordHistory' )
	BEGIN
		RAISERROR('Dropping Table RecHubUser.UserPasswordHistory',10,1) WITH NOWAIT;
		DROP TABLE RecHubUser.UserPasswordHistory;
	END

	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubUser' AND TABLE_NAME='Permissions' )
	BEGIN
		RAISERROR('Dropping Table RecHubUser.Permissions',10,1) WITH NOWAIT;
		DROP TABLE RecHubUser.[Permissions];
	END


	DECLARE @Loop INT = 1,
			@SQLCommand VARCHAR(MAX),
			@SchemaName SYSNAME,
			@ProcedureName SYSNAME;

	DECLARE @StoredProcedures TABLE
	(
		RowID INT IDENTITY(1,1),
		SchemaName VARCHAR(30),
		ProcedureName VARCHAR(128)
	)

	INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
	SELECT 'RecHubUser','usp_Users_Get_PasswordExpiredTime'
	UNION ALL SELECT 'RecHubUser','usp_Session_Get_ByUserType'
	UNION ALL SELECT 'RecHubUser','usp_AuthorizeBatchToUser'
	UNION ALL SELECT 'RecHubUser','usp_DeleteCSRUser'
	UNION ALL SELECT 'RecHubUser','usp_InsertOLUserQuestions'
	UNION ALL SELECT 'RecHubUser','usp_RemoveUserPasswordHistory'
	UNION ALL SELECT 'RecHubUser','usp_UserPasswordExistsInHistory'
	UNION ALL SELECT 'RecHubUser','usp_UserPasswordHistory_Ins'
	UNION ALL SELECT 'RecHubUser','usp_Users_Del_SetupToken'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByEmailAddress'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByOLOrganizationID'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByOLOrganizationIDAndLogonName'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByOrganizationExtID1'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByOrganizationExtID1ByUserExtID1'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByOrganizationExtID1ByUserExtIDHash'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_BySetupToken'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByUserExtID1'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByUserTypeAndLogonName'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_InternalUsers'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_SystemUsers'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_SystemUsersWithPassword'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_UserFields'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ValidateSetupToken'
	UNION ALL SELECT 'RecHubUser','usp_Users_Upd_Password'
	UNION ALL SELECT 'RecHubUser','usp_Users_UpdateFailedLogonAttempts'
	UNION ALL SELECT 'RecHubUser','usp_Users_UpdateFailedSecurityQuestionAttempts'
	UNION ALL SELECT 'RecHubUser','usp_UserSecretQuestionDelete'
	UNION ALL SELECT 'RecHubUser','usp_Users_Upd_UserSetupToken'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_UserPassword'
	UNION ALL SELECT 'RecHubData','usp_Report_Get_ParameterDescriptions'
	UNION ALL SELECT 'RecHubData','usp_factBatchSummary_Upd_CheckTotal'
	UNION ALL SELECT 'RecHubException','usp_OLClientAccounts_Get_InvoiceBalancingOption'
	UNION ALL SELECT 'RecHubUser','usp_GetUsers'
	UNION ALL SELECT 'RecHubUser','usp_OLClientAccounts_Get_ByUserID_Reporting'
	UNION ALL SELECT 'RecHubUser','usp_Session_Ins'
	UNION ALL SELECT 'RecHubUser','usp_Session_Ins_CreateSession'
	UNION ALL SELECT 'RecHubUser','usp_Session_Ins_CreateSessionNoOLOrganizationID'
	UNION ALL SELECT 'RecHubUser','usp_Session_Ins_RegisterSession'
	UNION ALL SELECT 'RecHubUser','usp_Session_Maintenance'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ActiveUsers'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByEventRuleID'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_ByLogonName'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_BySiteClientAccountID'
	UNION ALL SELECT 'RecHubUser','usp_Users_Get_BySiteID'
	UNION ALL SELECT 'RecHubUser','usp_Users_Upd_UserSetupToken'
	UNION ALL SELECT 'RecHubUser','usp_Permissions_Del_ByUserID'
	UNION ALL SELECT 'RecHubUser','usp_Permissions_Ins_ByUserID'
	UNION ALL
	SELECT 	
		ROUTINE_SCHEMA,
		ROUTINE_NAME
	FROM 	
		INFORMATION_SCHEMA.ROUTINES
	WHERE 	
		ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_API_%'
	UNION ALL
	SELECT 	
		ROUTINE_SCHEMA,
		ROUTINE_NAME
	FROM 	
		INFORMATION_SCHEMA.ROUTINES
	WHERE 	
		ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_SCHEMA = 'RecHubUser'
		AND ROUTINE_NAME LIKE 'usp_Permissions_%';


	WHILE( @Loop <= (SELECT MAX(RowID) FROM @StoredProcedures) )
	BEGIN
		SELECT 
			@SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + ProcedureName  + CHAR(39) + ') IS NOT NULL DROP PROCEDURE ' + SchemaName + '.' + ProcedureName,
			@SchemaName = SchemaName,
			@ProcedureName = ProcedureName
		FROM 
			@StoredProcedures
		WHERE 
			RowID = @Loop;

		RAISERROR('Dropping Stored Procedure %s.%s',10,1,@SchemaName,@ProcedureName) WITH NOWAIT;
		EXEC(@SQLCommand);
		SET @Loop = @Loop + 1;
	END

	IF OBJECT_ID('RecHubUser.udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID') IS NOT NULL
		DROP FUNCTION RecHubUser.udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID;
END
ELSE
	RAISERROR('WI 199060 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd