--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 216114
--WFSScriptProcessorPrint Adding New Indexes to RecHubSystem.DataImportQueue  
--WFSScriptProcessorCRBegin
IF 2 <> (SELECT COUNT(1) FROM sys.indexes WHERE NAME='IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus' OR NAME='IDX_DataImportQueue_QueueStatusResponseTrackingID')
BEGIN   -- If we do not have both indexes we will build them
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubSystem', 'TABLE', 'DataImportQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubSystem',
			@level1type = N'TABLE',
			@level1name = N'DataImportQueue';		


	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubSystem,
	@level1type = N'TABLE',@level1name = DataImportQueue,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Queue work from external data sources via the Web Service.
*		   
* QueueType
*	0 - Client Setup
*	1 - Batch Data
*	2 - Notification
*
* QueueStatus
*	10 - Ready to process (default)
*	15 - Failed processing on OLTA - but can be reprocessed
*	20 - In process
*	30 - Failed processing on OLTA - but cannot be reprocessed
*	99 - Successfully imported, response ready
*	120 - Response send, waiting for receipt confirmation
*	145 - Client unknown error. "Dead" record.
*	150 - Receipt confirmation received, response process complete
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	30 = Failed processing on OLTA, data could not be processed
*	255 = Status not set
*
* Modification History
* 01/24/2012 CR 49666 JPB	Created
* 05/30/2012 CR 53201 JPB	Added Column ClientProcessCode, QueueType 2, 
*								QueueStatus 145, and ResponseStatus 30.
* 02/17/2013 WI 91424 JBS	Update Table to	2.0 release. Change Schema Name.
*							Changes columns: CreatedBy, ModifiedBy changed to VARCHAR(128)
* 02/05/2015 WI 177621 JPB	Added RetryCount.
* 05/29/2015 WI 216114 JBS	Add indexes for performance.
******************************************************************************/';

	IF EXISTS(SELECT 1 FROM sys.indexes WHERE NAME='IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus' )
		DROP INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON RecHubSystem.DataImportQueue;

	IF EXISTS(SELECT 1 FROM sys.indexes WHERE NAME='IDX_DataImportQueue_QueueStatusResponseTrackingID' )
		DROP INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON RecHubSystem.DataImportQueue;


	RAISERROR('Creating index IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus',10,1) WITH NOWAIT;
	CREATE INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON RecHubSystem.DataImportQueue 
		(
		QueueType, 
		ClientProcessCode,
		QueueStatus
		) 
	INCLUDE 
		(SourceTrackingID);

	RAISERROR('Creating index IDX_DataImportQueue_QueueStatusResponseTrackingID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON RecHubSystem.DataImportQueue 
		(
		QueueStatus, 
		ResponseTrackingID
		);

END 
ELSE
	BEGIN
		RAISERROR('WI 216114 has already been applied to the database.',10,1) WITH NOWAIT;
	END
--WFSScriptProcessorCREnd