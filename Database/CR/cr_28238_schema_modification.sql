--WFSScriptProcessorPrint CR 28238
--WFSScriptProcessorIndex OLTA.factDataEntryDetails.IDX_factDataEntryDetails_ProcessingDateKey
IF NOT EXISTS(SELECT 1 FROM sysindexes WHERE sysindexes.name = 'IDX_factDataEntryDetails_ProcessingDateKey')
	CREATE INDEX IDX_factDataEntryDetails_ProcessingDateKey ON OLTA.factDataEntryDetails(ProcessingDateKey)
