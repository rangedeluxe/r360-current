--WFSScriptProcessorPrint CR 47741
--WFSScriptProcessorCRBegin

DECLARE @LoadDate DATETIME

SET @LoadDate = GETDATE();

/* Insert all rows from factBatchSummary that do not exist in factBatchData */
RAISERROR('Insert Process Date into factBatchData',10,1) WITH NOWAIT
;WITH BatchData AS
(
SELECT	BankKey,
		CustomerKey,
		LockboxKey,
		DepositDateKey,
		ProcessingDateKey
FROM	OLTA.factBatchSummary
EXCEPT
SELECT	BankKey,
		CustomerKey,
		LockboxKey,
		DepositDateKey,
		ProcessingDateKey
FROM	OLTA.factBatchData
WHERE	BatchDataSetupFieldKey = 24
)
INSERT INTO OLTA.factBatchData(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,BatchDataSetupFieldKey,BatchID,DataValue,DataValueDateTime,LoadDate)
SELECT	OLTA.factBatchSummary.BankKey,
		OLTA.factBatchSummary.CustomerKey,
		OLTA.factBatchSummary.LockboxKey,
		OLTA.factBatchSummary.ProcessingDateKey,
		OLTA.factBatchSummary.DepositDateKey,
		24 AS BatchDataSetupFieldKey,
		BatchID,
		CONVERT(varchar,CalendarDate,101) AS DataValue,
		CalendarDate AS DataValueDateTime,
		@LoadDate
FROM	OLTA.factBatchSummary
		INNER JOIN OLTA.dimDates ON OLTA.dimDates.DateKey = OLTA.factBatchSummary.ProcessingDateKey
		INNER JOIN BatchData ON BatchData.BankKey = OLTA.factBatchSummary.BankKey
			AND BatchData.CustomerKey = OLTA.factBatchSummary.CustomerKey
			AND BatchData.LockboxKey = OLTA.factBatchSummary.LockboxKey
			AND BatchData.DepositDateKey = OLTA.factBatchSummary.DepositDateKey
			AND BatchData.ProcessingDateKey = OLTA.factBatchSummary.ProcessingDateKey

--WFSScriptProcessorCREnd