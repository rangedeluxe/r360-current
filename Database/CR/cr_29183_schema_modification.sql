--WFSScriptProcessorPrint CR 29183
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factDocuments]') AND name = N'IDX_factDocuments_BankCustomerLockboxProcessingDateDepositDateKeyBatchID')
	CREATE NONCLUSTERED INDEX [IDX_factDocuments_BankCustomerLockboxProcessingDateDepositDateKeyBatchID] ON [OLTA].[factDocuments] 
	(
		[BankKey] ASC,
		[CustomerKey] ASC,
		[LockboxKey] ASC,
		[ProcessingDateKey] ASC,
		[DepositDateKey] ASC,
		[BatchID] ASC,
		[BatchSequence] ASC
	) ON OLTAFacts_Partition_Scheme(DepositDateKey)
--WFSScriptProcessorCREnd
