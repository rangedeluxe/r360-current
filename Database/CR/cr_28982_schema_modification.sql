--WFSScriptProcessorPrint CR 28982 
--WFSScriptProcessorPrint Adding ModificationDate to OLTA.factBatchSummary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factBatchSummary ADD ModificationDate DATETIME NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Updating existing OLTA.factBatchSummary.ModificationDate with LoadDate
--WFSScriptProcessorCRBegin
UPDATE	OLTA.factBatchSummary 
SET		ModificationDate = LoadDate
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factBatchSummary.ModificationDate to NOT NULL
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factBatchSummary ALTER COLUMN ModificationDate DATETIME NOT NULL
--WFSScriptProcessorCREnd

--WFSScriptProcessorPrint Altering OLTA.factBatchSummary.ModificationDate adding default constraint
--WFSScriptProcessorCRBegin
IF NOT EXISTS(SELECT 1 FROM	sysobjects WHERE name = 'DF_factBatchSummary_ModificationDate')
	AND EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='factBatchSummary' AND COLUMN_NAME='ModificationDate')
	ALTER TABLE OLTA.factBatchSummary ADD
		CONSTRAINT DF_factBatchSummary_ModificationDate DEFAULT GETDATE() FOR ModificationDate
--WFSScriptProcessorCREnd