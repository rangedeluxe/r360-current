--WFSScriptProcessorPrint CR 49552
--WFSScriptProcessorPrint Adding BatchSiteCode to OLTA.factTransactionSummary if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factTransactionSummary' AND COLUMN_NAME = 'BatchSiteCode' )
BEGIN /* verify that CR 49275 has been applied before continuing. */ --change this check to the clustered index
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factTransactionSummary' AND COLUMN_NAME = 'SourceProcessingDateKey' )
	BEGIN
		RAISERROR('CR 49275 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimBanks_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimBanks_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimCustomers_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimCustomers_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimLockboxes_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimLockboxes_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_ProcessingDate_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_ProcessingDate_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_SourceProcessingDate_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_DepositDate_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_DepositDate_factTransactionSummary
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factTransactionSummary' AND CONSTRAINT_NAME='FK_dimBatchSources_factTransactionSummary' )
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT FK_dimBatchSources_factTransactionSummary

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'factTransactionSummary')
		ALTER TABLE OLTA.factTransactionSummary DROP CONSTRAINT DF_factTransactionSummary_ModificationDate

	RAISERROR('Rebuilding OLTA.factTransactionSummary adding new columns',10,1) WITH NOWAIT 
	EXEC sp_rename 'OLTA.factTransactionSummary', 'OLDfactTransactionSummary'

	CREATE TABLE OLTA.factTransactionSummary
	(
		BankKey int NOT NULL,
		CustomerKey int NOT NULL,
		LockboxKey int NOT NULL,
		DepositDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		ProcessingDateKey int NOT NULL, --CR 28224 JPB 11/14/2009
		SourceProcessingDateKey int NOT NULL, --CR 49275 JPB 01/12/2012
		BatchID int NOT NULL,
		DESetupID int NULL,
		SystemType tinyint NOT NULL,
		DepositStatus int NOT NULL,
		TransactionID int NOT NULL,
		TxnSequence int NOT NULL,
		CheckCount int NOT NULL,
		DocumentCount int NOT NULL,
		ScannedCheckCount int NOT NULL,
		StubCount int NOT NULL,
		OMRCount int NOT NULL,
		CheckTotal money NOT NULL,
		BatchSourceKey tinyint NOT NULL, --CR 32285 JPB 01/10/2011
		BatchSiteCode int NULL, --CR 49552 JPB 01/23/2012
		LoadDate datetime NOT NULL,
		ModificationDate datetime NOT NULL --CR 29358 JPB 04/12/2010
			CONSTRAINT DF_factTransactionSummary_ModificationDate DEFAULT GETDATE()
	) $(OnPartition)
	
	RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
	ALTER TABLE OLTA.factTransactionSummary ADD 
		CONSTRAINT FK_dimBanks_factTransactionSummary FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
		CONSTRAINT FK_dimCustomers_factTransactionSummary FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
		CONSTRAINT FK_dimLockboxes_factTransactionSummary FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
		CONSTRAINT FK_ProcessingDate_factTransactionSummary FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_DepositDate_factTransactionSummary FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_SourceProcessingDate_factTransactionSummary FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
		CONSTRAINT FK_dimBatchSources_factTransactionSummary FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_DepositDateKey',10,1) WITH NOWAIT 
	CREATE CLUSTERED INDEX IDX_factTransactionSummary_DepositDateKey ON OLTA.factTransactionSummary (DepositDateKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_BankKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factTransactionSummary_BankKey ON OLTA.factTransactionSummary (BankKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_LockboxKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factTransactionSummary_LockboxKey ON OLTA.factTransactionSummary (LockboxKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_CustomerKey',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factTransactionSummary_CustomerKey ON OLTA.factTransactionSummary (CustomerKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_ProcessingDate',10,1) WITH NOWAIT 
	CREATE INDEX IDX_factTransactionSummary_ProcessingDate ON OLTA.factTransactionSummary (ProcessingDateKey) $(OnPartition)
	RAISERROR('Creating Index OLTA.factTransactionSummary.IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence',10,1) WITH NOWAIT 
	CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonIDTxnSequence ON OLTA.factTransactionSummary
	(
		[LockboxKey] ASC,
		[DepositDateKey] ASC,
		[BatchID] ASC,
		[DepositStatus] ASC,
		[BankKey] ASC,
		[CustomerKey] ASC,
		[ProcessingDateKey] ASC,
		[TransactionID] ASC,
		[TxnSequence] ASC
	) $(OnPartition)
	
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factTransactionSummary', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'factTransactionSummary';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28224 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 03/11/2010 CR 29181 JPB	Added new index for Lockbox Search.
* 04/12/2010 CR 29358 JPB	Added ModificationDate.
* 01/12/2011 CR 32285 JPB	Added BatchSourceKey.
* 10/27/2011 CR 47513 JPB	Added TxnSequence to index.
* 01/12/2012 CR 49275 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49552 JPB	Added BatchSiteCode.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factTransactionSummary
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factTransactionSummary
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			BatchID,
			DESetupID,
			SystemType,
			DepositStatus,
			TransactionID,
			TxnSequence,
			CheckCount,
			DocumentCount,
			ScannedCheckCount,
			StubCount,
			OMRCount,
			CheckTotal,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				BatchID,
				DESetupID,
				SystemType,
				DepositStatus,
				TransactionID,
				TxnSequence,
				CheckCount,
				DocumentCount,
				ScannedCheckCount,
				StubCount,
				OMRCount,
				CheckTotal,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactTransactionSummary

		IF OBJECT_ID('OLTA.OLDfactTransactionSummary') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factTransactionSummary table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactTransactionSummary
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd

