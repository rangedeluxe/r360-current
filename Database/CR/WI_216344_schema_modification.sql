--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 216344
--WFSScriptProcessorPrint Change IDX_SessionLog_SessionIDPageCounter to Clustered on RecHubUser.SessionLog 
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM sys.indexes WHERE NAME='IDX_SessionLog_SessionIDPageCounter' AND type_desc = 'NONCLUSTERED')
BEGIN    
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'SessionLog', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubUser',
			@level1type = N'TABLE',
			@level1name = N'SessionLog';		


	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubUser,
	@level1type = N'TABLE',@level1name = SessionLog,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Table stores Session log
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 10/22/2012 CR 56447 JPB	Added new index.
* 02/28/2013 WI 89653 JBS	Update Table to 2.0 release. Change Schema Name.
*							Change LogEntryID from INT to BIGINT
* 06/01/2015 WI 216344 JBS	Change IDX_SessionLog_SessionIDPageCounter to Clustered INDEX. Per 216076
******************************************************************************/';

	IF EXISTS(SELECT 1 FROM sys.indexes WHERE NAME='IDX_SessionLog_SessionIDPageCounter' )
		DROP INDEX IDX_SessionLog_SessionIDPageCounter ON RecHubUser.SessionLog;

	RAISERROR('Creating index IDX_SessionLog_SessionIDPageCounter',10,1) WITH NOWAIT;
	CREATE CLUSTERED INDEX IDX_SessionLog_SessionIDPageCounter ON RecHubUser.SessionLog
	(
		SessionID ASC,
		PageCounter ASC
	);

END 
ELSE
BEGIN
	RAISERROR('WI 216344 has already been applied to the database.',10,1) WITH NOWAIT;
END
--WFSScriptProcessorCREnd