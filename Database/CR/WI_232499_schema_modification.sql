--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 232499
--WFSScriptProcessorPrint Updating RecHubData.DataEntryTemplateColumns if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='DataEntryTemplateColumns' AND COLUMN_NAME='IsCheck' )
BEGIN
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='DataEntryTemplateColumns' AND CONSTRAINT_NAME='FK_DataEntryTemplateColumns_DataEntryTemplates' )
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT FK_DataEntryTemplateColumns_DataEntryTemplates;

	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='DataEntryTemplateColumns' AND CONSTRAINT_NAME='PK_DataEntryTemplateColumns' )
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT PK_DataEntryTemplateColumns;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataEntryTemplateColumns_CreationDate')
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT DF_DataEntryTemplateColumns_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataEntryTemplateColumns_CreatedBy')
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT DF_DataEntryTemplateColumns_CreatedBy;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataEntryTemplateColumns_ModificationDate')
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT DF_DataEntryTemplateColumns_ModificationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_DataEntryTemplateColumns_ModifiedBy')
		ALTER TABLE RecHubData.DataEntryTemplateColumns DROP CONSTRAINT DF_DataEntryTemplateColumns_ModifiedBy;

	RAISERROR('Rebuilding Table RecHubData.DataEntryTemplateColumns.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubData.DataEntryTemplateColumns', 'OLD_DataEntryTemplateColumns';

	CREATE TABLE RecHubData.DataEntryTemplateColumns
	(
		DataEntryTemplateColumnID INT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_DataEntryTemplateColumns PRIMARY KEY CLUSTERED,
		DataEntryTemplateID INT NOT NULL,
		IsCheck BIT NOT NULL,
		DataType TINYINT NOT NULL,
		ScreenOrder TINYINT NOT NULL,
		UILabel NVARCHAR(64) NOT NULL,
		FieldName NVARCHAR(256) NOT NULL,
		CreationDate DATETIME NOT NULL
			CONSTRAINT DF_DataEntryTemplateColumns_CreationDate DEFAULT GETDATE(),
		CreatedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_DataEntryTemplateColumns_CreatedBy DEFAULT SUSER_SNAME(),
		ModificationDate DATETIME NOT NULL 
			CONSTRAINT DF_DataEntryTemplateColumns_ModificationDate DEFAULT GETDATE(),
		ModifiedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_DataEntryTemplateColumns_ModifiedBy DEFAULT SUSER_SNAME()
	);

	RAISERROR('Updating RecHubData.Events table properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'DataEntryTemplateColumns', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'DataEntryTemplateColumns';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubData,
	@level1type = N'TABLE',@level1name = DataEntryTemplateColumns,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2014
*
* Purpose: Template columns for electronic payments.
*		   
*
* Modification History
* 06/01/2014 WI 143427 JPB	Created
* 08/27/2015 WI 232499 JPB	Updates to match new data entry column definitions
******************************************************************************/';

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	SET IDENTITY_INSERT RecHubData.DataEntryTemplateColumns ON;
	INSERT INTO RecHubData.DataEntryTemplateColumns
	(
		DataEntryTemplateColumnID,
		DataEntryTemplateID,
		IsCheck,
		DataType,
		ScreenOrder,
		UILabel,
		FieldName,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy
	)
	SELECT
		DataEntryTemplateColumnID,
		DataEntryTemplateID,
		CASE TableName
			WHEN 'Checks' THEN 1
			WHEN 'ChecksDataEntry' THEN 1
			WHEN 'Stubs' THEN 0
			WHEN 'StubsDataEntry' THEN 0
		END AS IsCheck,
		DataType,
		ScreenOrder,
		DisplayName,
		FldName,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy
	FROM
		RecHubData.OLD_DataEntryTemplateColumns;

	SET IDENTITY_INSERT RecHubData.DataEntryTemplateColumns OFF;

	RAISERROR('Adding Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubData.DataEntryTemplateColumns ADD
		CONSTRAINT FK_DataEntryTemplateColumns_DataEntryTemplates FOREIGN KEY(DataEntryTemplateID) REFERENCES RecHubData.DataEntryTemplates(DataEntryTemplateID);

	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubData.OLD_DataEntryTemplateColumns') IS NOT NULL
		DROP TABLE RecHubData.OLD_DataEntryTemplateColumns;


END
ELSE
	RAISERROR('WI 232499 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
