--WFSSCRIPTPROCESSORPrint CR 29709

--WFSSCRIPTPROCESSORSCRNAME Adding dimLockboxes.SiteLockboxKey
--WFSSCRIPTPROCESSORSCRBEGIN
IF NOT EXISTS(SELECT 1 FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'dimLockboxes' AND syscolumns.name = 'SiteLockboxKey' AND sysobjects.xtype='U')
	ALTER TABLE OLTA.dimLockboxes 
	ADD SiteLockboxKey UNIQUEIDENTIFIER NOT NULL 
	CONSTRAINT [DF_dimLockboxes_SiteLockboxKey] DEFAULT (('00000000-0000-0000-0000-000000000000'))

--WFSSCRIPTPROCESSORSCRCREND

--WFSSCRIPTPROCESSORSCRBEGIN
IF EXISTS(SELECT 1 FROM sysobjects JOIN syscolumns ON sysobjects.id = syscolumns.id JOIN systypes ON syscolumns.xtype=systypes.xtype WHERE sysobjects.name = 'dimLockboxes' AND syscolumns.name = 'SiteLockboxKey' AND sysobjects.xtype='U')
	UPDATE o
	SET o.SiteLockboxKey = d.LockboxKey
	FROM OLTA.dimLockboxes o
	JOIN dbo.Lockbox d ON d.BankID = o.SiteBankID
		AND d.CustomerID = o.SiteCustomerID
		AND d.LockboxID = o.SiteLockboxID
--WFSSCRIPTPROCESSORSCRCREND


