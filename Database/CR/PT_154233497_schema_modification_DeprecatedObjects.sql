﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 154233497
--WFSScriptProcessorPrint Remove deprecated objects if necessary
--WFSScriptProcessorCRBegin
IF OBJECT_ID('FITStaging.XMLNotificationFileNodes') IS NOT NULL
       DROP TABLE FITStaging.XMLNotificationFileNodes;
IF OBJECT_ID('FITStaging.XMLNotificationMessageNodes') IS NOT NULL
       DROP TABLE FITStaging.XMLNotificationMessageNodes;
IF OBJECT_ID('FITStaging.XMLNotificationNodes') IS NOT NULL
       DROP TABLE FITStaging.XMLNotificationNodes;

IF OBJECT_ID('FITStaging.usp_XMLNotificationFileNodes_Ins') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotificationFileNodes_Ins;
IF OBJECT_ID('FITStaging.usp_XMLNotificationMessageNodes_Ins') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotificationMessageNodes_Ins;
IF OBJECT_ID('FITStaging.usp_XMLNotificationNodes_Ins') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotificationNodes_Ins;
--WFSScriptProcessorCREnd