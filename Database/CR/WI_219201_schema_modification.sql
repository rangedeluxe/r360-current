--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 219201
--WFSScriptProcessorPrint Adding indexes IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey') 
	BEGIN
		RAISERROR('WI 219201 already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey ON RecHubData.factBatchSummary
(
	BatchID ASC,
	DepositDateKey ASC,
	factBatchSummaryKey ASC,
	IsDeleted ASC,
	SourceProcessingDateKey ASC,
	ImmutableDateKey ASC
)
INCLUDE
(
	SourceBatchID,
	BankKey,
	ClientAccountKey,
	OrganizationKey,
	BatchPaymentTypeKey
) $(OnDataPartition);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'factBatchSummary', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'RecHubData',
					@level1type = N'TABLE',
					@level1name = N'factBatchSummary';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = factBatchSummary,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 11/19/2009 CR 28189 JPB	Allow GlobalBatchID to be NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
* 01/11/2012 CR 49274 JPB	Added SourceProcessingDateKey.
* 01/19/2012 CR 48978 JPB	Added BatchSiteCode.
* 03/26/2012 CR 51540 JPB	Added BatchPaymentTypeKey.
* 07/03/2012 CR 53623 JPB	Added BatchCueID.
* 07/16/2012 CR 54119 JPB	Added BatchNumber.
* 07/16/2012 CR 54129 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71798 JPB	Update table to 2.0 release. Change schema name.
*							Add columns: factBatchSummaryKey, IsDeleted, DepositStatusKey,
*							StubTotal, DepositDisplayNameKey. 
*							Delete columns: GlobalBatchId, DepositDisplayName.  
*							Rename Columns: 
*							CustomerKey to OrganizationKey,  LockboxKey to ClientAccountKey,
*							ProcessingDateKey to ImmutableDateKey.
*							LoadDate to CreationDate.
*							Renamed ALL constraints.
*							Added factBatchSummaryKey to end of Clustered Index.
*							Create New Index based from old clustered index.
*							FP: WI 77752, WI 85066, WI 85092, WI 85093, WI 85098, WI 87220
* 04/09/2014 WI 135312 JPB	Changed BatchID from INT to BIGINT.
* 05/22/2014 WI	143850 JPB	Add SourceBatchID.
* 05/22/2014 WI 143851 JPB	Changed BatchSourceKey to SMALLINT.
* 09/28/2014 WI 168336 JPB	Added BatchExceptionStatuKey.
* 06/19/2015 WI 219201 MGE  Added index per 2.01 regression testing analysis
******************************************************************************/';
	END
--WFSScriptProcessorCREnd