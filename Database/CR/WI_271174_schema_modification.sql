--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271174
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factNotificationFiles.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factNotificationFiles')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factNotificationFiles'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factNotifications_NotificationDatefactNotificationFileKey')
			DROP INDEX IDX_factNotifications_NotificationDatefactNotificationFileKey ON RecHubData.factNotificationFiles;

		/****** Object:  Index PK_factNotificationFiles    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factNotificationFiles')
			ALTER TABLE RecHubData.factNotificationFiles DROP CONSTRAINT PK_factNotificationFiles;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factNotificationFiles ADD
			CONSTRAINT PK_factNotificationFiles PRIMARY KEY NONCLUSTERED (factNotificationFileKey,NotificationDateKey) $(OnNotificationPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factNotifications_NotificationDatefactNotificationFileKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factNotifications_NotificationDatefactNotificationFileKey ON RecHubData.factNotificationFiles
		(
			NotificationDateKey,
			factNotificationFileKey
		) $(OnNotificationPartition);

	
	END
	ELSE
		RAISERROR('WI 271174 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271174 RecHubData.factNotificationFiles table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd