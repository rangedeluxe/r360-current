

DECLARE @PartitionIdentifier varchar(20),
		@BeginDate datetime,
		@EndDate datetime,
		@value int,
		@YearsAdded INT,
		@PartitionMethod TINYINT

/* by default, add 6 years. Change only if needed. */
SET @YearsAdded = 6
/* find the max partition number */
SELECT @value = CONVERT(int,MAX(value)) FROM sys.partition_range_values
/* set the partition identifier and lookup the partition method */
SELECT	@PartitionIdentifier='OLTAFacts'
SELECT @PartitionMethod = RangeSetting --0:none/1:weekly/2:monthly
FROM OLTA.PartitionManager
WHERE PartitionIdentifier=@PartitionIdentifier

IF @PartitionMethod <> 0 
BEGIN /* only attempt if the database is partitioned */
	/* Convert the max partition number to a date/time */
	SELECT @BeginDate=CONVERT(datetime, CONVERT(varchar, @value), 112)
	/* add the correct value (based on the partition method) to the current max partition number to get the new beginning partition date */
	SELECT @BeginDate = 
			CASE
				WHEN @PartitionMethod = 1 THEN DATEADD(DAY,7,@BeginDate)
				WHEN @PartitionMethod = 2 THEN DATEADD(MONTH,1,@BeginDate)
			END

	/* now set the new end date to the new begin date plus @YearsAdded years */
	SET @EndDate = DATEADD(YEAR,@YearsAdded,@BeginDate)
	PRINT 'Creating Partitions for date range: ' + CONVERT(varchar, @BeginDate, 101) + ' to ' + CONVERT(varchar, @EndDate, 101)
	/* call the SP to add the partitions */
	EXEC OLTA.usp_CreatePartitionsForRange @PartitionIdentifier, @BeginDate, @EndDate
END

