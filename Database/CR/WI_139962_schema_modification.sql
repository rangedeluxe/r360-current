--WFSScriptProcessorPrint WI 139962
--WFSScriptProcessorPrint Updating RecHubConfig.ExtractSchedules if necessary
--WFSScriptProcessorCRBegin
IF EXISTS( SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID(N'RecHubConfig.ExtractSchedules') AND name = N'ExtractRunArguments')
BEGIN
	IF NOT EXISTS( SELECT 1 FROM sys.columns WHERE object_id = OBJECT_ID(N'RecHubConfig.ExtractSchedules') AND name = N'ScheduleType')
	BEGIN

		RAISERROR('Dropping Users contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubConfig' AND TABLE_NAME='ExtractSchedules' AND CONSTRAINT_NAME='PK_ExtractSchedules' )
			ALTER TABLE RecHubConfig.ExtractSchedules DROP CONSTRAINT PK_ExtractSchedules

		RAISERROR('ReBuilding RecHubConfig.ExtractSchedules.',10,1) WITH NOWAIT
		EXEC sp_rename 'RecHubConfig.ExtractSchedules', 'OLD_ExtractSchedules'
		
		CREATE TABLE RecHubConfig.ExtractSchedules
		(
			ExtractScheduleID     BIGINT IDENTITY(1,1) NOT NULL 
				CONSTRAINT PK_ExtractSchedules PRIMARY KEY,
			ExtractDefinitionID   BIGINT NOT NULL,
			[Description]         VARCHAR(255) NOT NULL,
			IsActive              BIT NOT NULL,
			DaysOfWeek            VARCHAR(7) NOT NULL,
			DayInMonth			  INT,		
			ScheduleTime          TIME (7) NOT NULL,
			ScheduleType		  INT,
			ExtractRunArguments   VARCHAR(255) NOT NULL,
			CreationDate          DATETIME NOT NULL,
			ModificationDate      DATETIME NOT NULL,
			CreatedBy             VARCHAR(128) NOT NULL,
			ModifiedBy            VARCHAR(128) NOT NULL
		)

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubConfig', 'TABLE', 'ExtractSchedules', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubConfig',
				@level1type = N'TABLE',
				@level1name = N'ExtractSchedules';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 05/15/2013
*
* Purpose: Store the Extract Schedule.
*
*
* Modification History
* 05/15/2013 WI 97699 DRP	Created for 2.0
* 05/29/2013 WI 103459 DRP  Added ExtractRunArguments
* 05/06/2014 WI 139962 JBS	Add columns ScheduleType and DayInMonth
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = RecHubConfig,
	@level1type = N'TABLE',@level1name = ExtractSchedules

	RAISERROR('Copying data from old table to New table.',10,1) WITH NOWAIT 

		SET IDENTITY_INSERT RecHubConfig.ExtractSchedules ON

		INSERT INTO RecHubConfig.ExtractSchedules 
		(
			ExtractScheduleID,
			ExtractDefinitionID,
			[Description],
			IsActive,
			DaysOfWeek,
			ScheduleTime,
			ExtractRunArguments,
			CreationDate,
			ModificationDate,
			CreatedBy,
			ModifiedBy
		)
		SELECT	
			ExtractScheduleID,
			ExtractDefinitionID,
			[Description],
			IsActive,
			DaysOfWeek,
			ScheduleTime,
			ExtractRunArguments,
			CreationDate,
			ModificationDate,
			CreatedBy,
			ModifiedBy
		FROM	
			RecHubConfig.OLD_ExtractSchedules;

		SET IDENTITY_INSERT RecHubConfig.ExtractSchedules OFF

		IF OBJECT_ID('RecHubConfig.OLD_ExtractSchedules') IS NOT NULL
			DROP TABLE RecHubConfig.OLD_ExtractSchedules;
	END
	ELSE
		RAISERROR('WI 139962 has already been applied to the database.',10,1) WITH NOWAIT;
	END
ELSE
	RAISERROR('Need to apply WI 103459, before adding WI 139962',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd