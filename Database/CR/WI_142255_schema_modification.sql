--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 142255
--WFSScriptProcessorPrint Disable CDC on RecHubData.factChecks table name if necessary.
--WFSScriptProcessorCRBegin
IF( EXISTS( SELECT 1 FROM sys.databases INNER JOIN INFORMATION_SCHEMA.TABLES ON TABLE_CATALOG = [NAME] WHERE is_cdc_enabled = 1 AND TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factChecks' ) )
	IF( NOT EXISTS( SELECT 1 FROM cdc.captured_columns WHERE OBJECT_NAME(object_id) = N'RecHubData_factChecks_CT' AND column_name = 'RoutingNumber' ) )
	BEGIN
		EXEC sys.sp_cdc_disable_table 
			@source_schema = N'RecHubData',
			@source_name = N'factChecks',
			@capture_instance = 'RecHubData_factChecks'
	END
	ELSE
		RAISERROR('WI 142255 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd

