--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 141435715, 142595657 
--WFSScriptProcessorPrint Adding CreationDateKey to SessionClientAccountEntitlements Table and partition it
--WFSScriptProcessorCRBegin

DECLARE @SessionClientAcctEntitlementRetentionDays	INT = 90,
		@BeginDate				DATETIME,
		@BeginDateKey			INT,
		@Message				VARCHAR(90)

IF NOT EXISTS  (SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'SessionClientAccountEntitlements'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('RecHubUser') = sys.tables.schema_id )
BEGIN
	RAISERROR('Beginning Process to Add CreationDateKey to the SessionClientAccountEntitlements table',10,1) WITH NOWAIT;
	
	--<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionClientAcctEntitlementRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionsRetentionDays'
	SELECT @BeginDate = GETDATE() - @SessionClientAcctEntitlementRetentionDays		-- n days prior to today
	SELECT @BeginDate = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @BeginDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate <= @BeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest date of SessionClientAccountEntitlements rows to keep is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
	
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionClientAccountEntitlements' AND CONSTRAINT_NAME='FK_SessionClientAccountEntitlements_dimClientAccounts' )
		ALTER TABLE RecHubUser.SessionClientAccountEntitlements DROP CONSTRAINT FK_SessionClientAccountEntitlements_dimClientAccounts;
	
	RAISERROR('Dropping Contraints',10,1) WITH NOWAIT;
--	--PK

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionClientAccountEntitlements' AND CONSTRAINT_NAME='PK_SessionClientAccountEntitlements' )
		ALTER TABLE RecHubUser.SessionClientAccountEntitlements DROP CONSTRAINT PK_SessionClientAccountEntitlements;

--	--Defaults
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionClientAccountEntitlement_CreationDate')
		ALTER TABLE RecHubUser.SessionClientAccountEntitlements DROP CONSTRAINT DF_SessionClientAccountEntitlement_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_SessionClientAccountEntitlement_CreationDateKey')
		ALTER TABLE RecHubUser.SessionClientAccountEntitlements DROP CONSTRAINT DF_SessionClientAccountEntitlement_CreationDateKey;
		
	RAISERROR('Dropping Indexes',10,1) WITH NOWAIT;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionIDCreationDate') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionIDCreationDate;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead;
	IF EXISTS (SELECT 1 FROM sysindexes WHERE Name = 'IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID') 
		DROP INDEX RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID;

	RAISERROR('Rebuilding Table RecHubUser.SessionClientAccountEntitlements.',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHubUser.SessionClientAccountEntitlements', 'OLD_SessionClientAccountEntitlements';

	CREATE TABLE RecHubUser.SessionClientAccountEntitlements
	(
	SessionClientAccountEntitlementID BIGINT IDENTITY(1,1) NOT NULL, 
	SessionID			UNIQUEIDENTIFIER,
	CreationDateKey		INT NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	EntityID			INT NOT NULL,
	SiteBankID			INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	ViewingDays			INT NULL,
	MaximumSearchDays	INT NULL,
	StartDateKey		INT NOT NULL,
	EndDateKey			INT NOT NULL,
	ViewAhead			BIT NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDate DEFAULT GETDATE(),
	EntityName			VARCHAR(128) NOT NULL
	) ON Sessions (CreationDateKey);

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

	INSERT INTO RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		CreationDateKey,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		ClientAccountKey,
		ViewingDays,
		MaximumSearchDays,
		StartDateKey,
		EndDateKey,
		ViewAhead,
		CreationDate,
		EntityName
	)
	SELECT
		SessionID,
		CAST(CONVERT(VARCHAR(10), CreationDate, 112) AS INT) AS CreationDateKey,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		ClientAccountKey,
		ViewingDays,
		MaximumSearchDays,
		StartDateKey,
		EndDateKey,
		ViewAhead,
		CreationDate,
		EntityName
	FROM
		RecHubUser.OLD_SessionClientAccountEntitlements
	WHERE
		CreationDate > @BeginDate - 1;

	RAISERROR('Adding Primary/Foreign Keys',10,1) WITH NOWAIT;
	ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
		CONSTRAINT PK_SessionClientAccountEntitlements PRIMARY KEY NONCLUSTERED (SessionClientAccountEntitlementID, CreationDateKey)
	ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
		CONSTRAINT FK_SessionClientAccountEntitlements_dimClientAccounts FOREIGN KEY (ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID',10,1) WITH NOWAIT;
	CREATE CLUSTERED INDEX IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID ON RecHubUser.SessionClientAccountEntitlements
	(
		CreationDateKey,
		SessionClientAccountEntitlementID
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID ON RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		SiteBankID,
		SiteClientAccountID
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey ON RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		ClientAccountKey
	)
	INCLUDE
	(
		EntityID,
		EntityName
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName ON RecHubUser.SessionClientAccountEntitlements
	(
		SessionID
	) 
	INCLUDE 
	(
		EntityID,
		EntityName
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_SessionIDCreationDate',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDCreationDate ON RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		CreationDate
	) ON Sessions (CreationDateKey);

	RAISERROR('Adding IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead ON RecHubUser.SessionClientAccountEntitlements
	(
		SessionID
	) 
	INCLUDE 
	(
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		StartDateKey,
		EndDateKey,
		ViewAhead
	) ON Sessions (CreationDateKey);
	RAISERROR('Adding [IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID]',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX [IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID] ON [RecHubUser].[SessionClientAccountEntitlements]
	(
	 SessionID ASC,
	 SiteBankID ASC,
	 SiteClientAccountID ASC
	)
	INCLUDE
	(
	 EntityID,
	 ViewingDays,
	 MaximumSearchDays
	) ON Sessions (CreationDateKey);

	
	RAISERROR('Updating RecHubUser.SessionClientAccountEntitlements.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubUser', 'TABLE', 'SessionClientAccountEntitlements', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubUser',
			@level1type = N'TABLE',
			@level1name = N'SessionClientAccountEntitlements';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = RecHubUser,
	@level1type = N'TABLE',@level1name = SessionClientAccountEntitlements,
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2014
*
* Purpose: Client Account Keys a session is entitled to view.
*		   
*
* Modification History
* 05/11/2014 WI 145456		JPB	Created
* 07/08/2014 WI 145456		KLC	Changed Logon entity to be actual owning entity
* 09/18/2014 WI 166671		JPB	Added new index.
* 09/18/2014 WI 166672		JPB	Updated IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey
*								with includes.
* 02/23/2015 WI 191263		JBS	Add columns ViewAhead and EndDateKey, Also Change StartDateKey to NOT NULL
* 03/23/2015 WI 197226		JPB	Add IDX_SessionIDCreationDate (used by Session Maint package)
* 03/25/2015 WI 197953		JBS	Add IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead  
*							(used by Advanced Search)
* 06/01/2015 WI 197953		JBS	Changing IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID to Clustered index. Per 216076
* 03/29/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added new index and partition keys
******************************************************************************************************************/';
		
		
	RAISERROR('Dropping old table',10,1) WITH NOWAIT;
	IF OBJECT_ID('RecHubUser.OLD_SessionClientAccountEntitlements') IS NOT NULL
		DROP TABLE RecHubUser.OLD_SessionClientAccountEntitlements;

END
ELSE
	RAISERROR('PT 142595657 SessionClientAccountEntitlements has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd