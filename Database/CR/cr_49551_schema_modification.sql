--WFSScriptProcessorPrint CR 49551
--WFSScriptProcessorPrint Adding BatchSiteCode to OLTA.factChecks if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factChecks' AND COLUMN_NAME = 'BatchSiteCode' )
BEGIN /* verify that CR 49276 has been applied before continuing. */
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factChecks' AND COLUMN_NAME = 'SourceProcessingDateKey' )
	BEGIN
		RAISERROR('CR 49276 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT 
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimBanks_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimBanks_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimCustomers_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimCustomers_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimLockboxes_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimLockboxes_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_ProcessingDate_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_ProcessingDate_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_SourceProcessingDate_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_SourceProcessingDate_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_DepositDate_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_DepositDate_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimRemitters_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimRemitters_factChecks
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='factChecks' AND CONSTRAINT_NAME='FK_dimBatchSources_factChecks' )
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT FK_dimBatchSources_factChecks

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_factChecks_ModificationDate ')
			ALTER TABLE OLTA.factChecks DROP CONSTRAINT DF_factChecks_ModificationDate

		RAISERROR('Rebuilding OLTA.factChecks adding new columns',10,1) WITH NOWAIT 
		EXEC sp_rename 'OLTA.factChecks', 'OLDfactChecks'

		CREATE TABLE OLTA.factChecks
		(
			BankKey int NOT NULL,
			CustomerKey int NOT NULL,
			LockboxKey int NOT NULL,
			DepositDateKey int NOT NULL,
			ProcessingDateKey int NOT NULL,
			SourceProcessingDateKey int NOT NULL, --CR 49276 JPB 01/12/2012
			RemitterKey int NOT NULL,
			GlobalBatchID int NULL, --CR 28190 JPB 11/13/2009
			BatchID int NOT NULL,
			DepositStatus int NOT NULL,
			TransactionID int NOT NULL,
			TxnSequence int NOT NULL,
			SequenceWithinTransaction int NOT NULL,
			BatchSequence int NOT NULL,
			CheckSequence int NOT NULL,
			GlobalCheckID int NULL,
			Serial varchar(30) NULL,
			NumericSerial bigint NULL,
			TransactionCode varchar(30) NULL,
			Amount money NULL,
			ImageInfoXML xml NULL,
			BatchSourceKey tinyint NOT NULL, --CR 32298 JPB 01/13/2011
			BatchSiteCode int NULL, --CR 49551 JPB 01/23/2012
			LoadDate datetime NOT NULL,
			ModificationDate datetime NOT NULL --CR 28976 JPB 02/10/2010
				CONSTRAINT DF_factChecks_ModificationDate DEFAULT GETDATE()
		) $(OnPartition)
		
		RAISERROR('Creating Foreign Keys',10,1) WITH NOWAIT 
		ALTER TABLE OLTA.factChecks ADD
			CONSTRAINT FK_dimBanks_factChecks FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
			CONSTRAINT FK_dimCustomers_factChecks FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
			CONSTRAINT FK_dimLockboxes_factChecks FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
			CONSTRAINT FK_ProcessingDate_factChecks FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_DepositDate_factChecks FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_SourceProcessingDate_factChecks FOREIGN KEY(SourceProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
			CONSTRAINT FK_dimRemitters_factChecks FOREIGN KEY(RemitterKey) REFERENCES OLTA.dimRemitters(RemitterKey),
			CONSTRAINT FK_dimBatchSources_factChecks FOREIGN KEY(BatchSourceKey) REFERENCES OLTA.dimBatchSources(BatchSourceKey)
			
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_DepositDateKey',10,1) WITH NOWAIT 
		CREATE CLUSTERED INDEX IDX_factChecks_DepositDateKey ON OLTA.factChecks (DepositDateKey,LockboxKey,ProcessingDateKey,BatchID,BatchSequence,BankKey,RemitterKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_BankKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factChecks_BankKey ON OLTA.factChecks (BankKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_CustomerKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factChecks_CustomerKey ON OLTA.factChecks (CustomerKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_LockboxKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factChecks_LockboxKey ON OLTA.factChecks (LockboxKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_ProcessingDateKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factChecks_ProcessingDateKey ON OLTA.factChecks (ProcessingDateKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_RemitterKey',10,1) WITH NOWAIT 
		CREATE INDEX IDX_factChecks_RemitterKey ON OLTA.factChecks (RemitterKey) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_BankCustomerLockboxProcessingDateDepositDateKeyBatchID',10,1) WITH NOWAIT 
		CREATE NONCLUSTERED INDEX IDX_factChecks_BankCustomerLockboxProcessingDateDepositDateKeyBatchID ON OLTA.factChecks
		(
			[BankKey] ASC,
			[CustomerKey] ASC,
			[LockboxKey] ASC,
			[ProcessingDateKey] ASC,
			[DepositDateKey] ASC,
			[BatchID] ASC
		) $(OnPartition)
		RAISERROR('Creating Index OLTA.factChecks.IDX_factChecks_DepositDateProcessingDateBankLockboxKeyBatchID',10,1) WITH NOWAIT 
		CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateProcessingDateBankLockboxKeyBatchID ON OLTA.factChecks
		(
			[DepositDateKey] ASC,
			[ProcessingDateKey] ASC,
			[BankKey] ASC,
			[LockboxKey] ASC,
			[BatchID] ASC
		) $(OnPartition)
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factChecks', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factChecks';		
	
EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factChecks
		
		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		
		INSERT INTO OLTA.factChecks
		(
			BankKey,
			CustomerKey,
			LockboxKey,
			DepositDateKey,
			ProcessingDateKey,
			SourceProcessingDateKey,
			RemitterKey,
			GlobalBatchID,
			BatchID,
			DepositStatus,
			TransactionID,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			CheckSequence,
			GlobalCheckID,
			Serial,
			NumericSerial,
			TransactionCode,
			Amount,
			ImageInfoXML,
			BatchSourceKey,
			LoadDate,
			ModificationDate
		)
		SELECT 	BankKey,
				CustomerKey,
				LockboxKey,
				DepositDateKey,
				ProcessingDateKey,
				SourceProcessingDateKey,
				RemitterKey,
				GlobalBatchID,
				BatchID,
				DepositStatus,
				TransactionID,
				TxnSequence,
				SequenceWithinTransaction,
				BatchSequence,
				CheckSequence,
				GlobalCheckID,
				Serial,
				NumericSerial,
				TransactionCode,
				Amount,
				ImageInfoXML,
				BatchSourceKey,
				LoadDate,
				ModificationDate
		FROM 	OLTA.OLDfactChecks

		IF OBJECT_ID('OLTA.OLDfactChecks') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factChecks table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactChecks
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT 
--WFSScriptProcessorCREnd

