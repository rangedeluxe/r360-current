--WFSScriptProcessorSchema DBO
--WFSScriptProcessorPrint CR 31821

--WFSScriptPRocessorStaticDataCreateBegin
if not exists (select 1 from [dbo].[SystemSetup] where Section = 'OTIS' and SetupKey = 'SynchOLCustomers' and Value = '0')
    INSERT [dbo].[SystemSetup](Section, SetupKey, Value, Description, Datatype, ValidValues) 
	VALUES ( 
			'OTIS', 
			'SynchOLCustomers', 
			'0', 
			'Synch OL Customers from OLTA to dbo', 
			'Integer', 
			'0, 1' 
		   )
--WFSScriptPRocessorStaticDataCreateEnd


