--WFSScriptProcessorPrint WI 70811
--WFSScriptProcessorPrint Updating SessionLog if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[SessionLog]') AND name = N'IDX_SessionLog_SessionIDPageCounter' )
BEGIN

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'SessionLog', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'SessionLog';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 12/04/2012 WI 70811 JPB	Added new index (FP:CR 56447).
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = SessionLog
	
	RAISERROR('Creating index OLTA.SessionLog.IDX_SessionLog_SessionIDPageCounter',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionLog_SessionIDPageCounter ON OLTA.SessionLog
	(
		SessionID ASC,
		PageCounter ASC
	)
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd