--WFSScriptProcessorPrint CR 33331
--WFSScriptProcessorPrint Adding IDX_factChecks_DepositDateProcessingDateBankLockboxKeyBatchID to OLTA.factChecks if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factChecks]') AND name = N'IDX_factChecks_DepositDateProcessingDateBankLockboxKeyBatchID')
	CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateProcessingDateBankLockboxKeyBatchID ON OLTA.factChecks
	(
		[DepositDateKey] ASC,
		[ProcessingDateKey] ASC,
		[BankKey] ASC,
		[LockboxKey] ASC,
		[BatchID] ASC
	) $(OnPartition)
--WFSScriptProcessorCREnd