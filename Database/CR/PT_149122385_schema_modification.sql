--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 149122385 
--WFSScriptProcessorPrint Updating index on dimImageRPSAliasMappings if necessary
--WFSScriptProcessorCRBegin
RAISERROR('Dropping index IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName',10,1) WITH NOWAIT;

	IF EXISTS( SELECT * FROM sys.indexes WHERE name='IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName' AND object_id = OBJECT_ID('RecHubData.dimImageRPSAliasMappings') )
		DROP INDEX RecHubData.dimImageRPSAliasMappings.IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName

	RAISERROR('Updating RecHubData.dimImageRPSAliasMappings.IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName ON RecHubData.dimImageRPSAliasMappings 
	(
	SiteBankID ASC,
	SiteClientAccountID ASC,
	DocType ASC,
	MostRecent ASC,
	ModificationDate ASC,
	CreationDate ASC,
	AliasName ASC
	)
	INCLUDE 
	( 	
	FieldType,
	ExtractType
	)

IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimImageRPSAliasMappings', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHubData',
			@level1type = N'TABLE',
			@level1name = N'dimImageRPSAliasMappings';

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2011
*
* Purpose: Stores ImageRPS Alias names to RecHubData table/columns. 
*
*
* Modification History
* 04/04/2011 CR 33683 JPB	Created
* 09/10/2012 CR 55576 JPB	Added MostRecent
* 01/13/2013 WI 119080 JBS	Update to 2.1 Schema.
* 08/09/2017 PT 149122385 MGE	Updated Index
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = dimImageRPSAliasMappings;
--WFSScriptProcessorCREnd
