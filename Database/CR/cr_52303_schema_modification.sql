--WFSScriptProcessorPrint CR 52303
--WFSScriptProcessorPrint Fixing OLTA.BrandingSchemes IsActive constraint name if necessary.
--WFSScriptProcessorCRBegin
IF EXISTS(	SELECT 1 FROM sys.objects WHERE SCHEMA_NAME(schema_id) = 'OLTA' AND OBJECT_NAME(parent_object_id) = 'BrandingSchemes' AND OBJECT_NAME(OBJECT_ID) LIKE 'DF__BrandingS__IsAct__%'	)
BEGIN /* verify that CR 51099 has been applied before continuing. */
	IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'BrandingSchemes' AND COLUMN_NAME = 'ExternalID1'	)
	BEGIN
		RAISERROR('CR 51099 must be applied before this CR.',16,1) WITH NOWAIT;
	END
	ELSE
	BEGIN
		RAISERROR('Renaming IsActive constraint',10,1) WITH NOWAIT;

		DECLARE @ConstraintName NVARCHAR(1035);
		
		SELECT @ConstraintName=OBJECT_NAME(OBJECT_ID) FROM sys.objects WHERE SCHEMA_NAME(schema_id) = 'OLTA' AND OBJECT_NAME(parent_object_id) = 'BrandingSchemes' AND OBJECT_NAME(OBJECT_ID) LIKE 'DF__BrandingS__IsAct__%';
		
		SET @ConstraintName = 'OLTA.'+@ConstraintName;
		
		EXEC SP_RENAME @ConstraintName,
		   'DF_BrandingSchemes_IsActive',
		   'OBJECT';
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'BrandingSchemes', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'BrandingSchemes';		
	
		EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose: Table BrandingSchemes that contains the defined branding schemes.
*
* Modification History
* 01/18/2012 CR 49636 JCS   Created table.
* 02/29/2012 CR 51099 JCS   Added ExternalID1, ExternalID2 fields to table.
* 04/26/2012 CR 52303 JPB   Named the IsActive constraint.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = BrandingSchemes;
		
		IF OBJECT_ID('OLTA.OLDfactTransactionSummary') IS NOT NULL
		BEGIN
			RAISERROR('Removing old factTransactionSummary table.',10,1) WITH NOWAIT 
			DROP TABLE OLTA.OLDfactTransactionSummary
		END
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
