﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 214615
--WFSScriptProcessorPrint Adding index IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey
--WFSScriptProcessorCRBegin

IF NOT EXISTS (SELECT Table_Name FROM Information_Schema.Tables WHERE Table_Type='BASE TABLE' AND Table_Name='factEventAuditSummary')
	RAISERROR('RecHubAudit.factEventAuditSummary does not exist.  Run factEventAuditSummary.sql, then try again.',10,1) WITH NOWAIT;
ELSE
	BEGIN
	IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey') 
		
		--DROP INDEX RecHubAudit.factEventAuditSummary.IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey
		PRINT 'Index IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey already exists - WI 214615 previously applied'
	ELSE	
	Begin
	CREATE NONCLUSTERED INDEX IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey ON RecHubAudit.factEventAuditSummary
	(
		IsDeleted,
		AuditEventKey,
		AuditDateKey
	)
	INCLUDE
	(
		UserID,
		AuditApplicationKey,
		AuditKey,
		ModificationDate
	)
	PRINT 'Created index IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey'

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAudit', 'TABLE', 'factEventAuditSummary', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubAudit',
				@level1type = N'TABLE',
				@level1name = N'factEventAuditSummary';		


		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHubAudit,
		@level1type = N'TABLE',@level1name = factEventAuditSummary,
		@value = N'/******************************************************************************
	** WAUSAU Financial Systems (WFS)
	** Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
	*******************************************************************************
	*******************************************************************************
	** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
	*******************************************************************************
	* Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
	* other trademarks cited herein are property of their respective owners.
	* These materials are unpublished confidential and proprietary information 
	* of WFS and contain WFS trade secrets.  These materials may not be used, 
	* copied, modified or disclosed except as expressly permitted in writing by 
	* WFS (see the WFS license agreement for details).  All copies, modifications 
	* and derivative works of these materials are property of WFS.
	*
	* Author: JPB
	* Date: 05/30/2013
	*
	* Purpose: Grain: one row for every audit event.
	*		   
	*
	* Modification History
	* 05/30/2013 WI 103660 JPB	Created
	* 05/15/2015 WI 214615 MGE  Added index IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey
	*****************************************************************************************************/';
	END
END
--WFSScriptProcessorCREnd