--WFSScriptProcessorPrint WI 174073
--WFSScriptProcessorPrint Update RecHubData.dimDocumentTypes if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimDocumentTypes' AND COLUMN_NAME='IsReadOnly' )
BEGIN
	IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='dimDocumentTypes' AND COLUMN_NAME='ModificationDate' )
	BEGIN
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='factDocuments' AND CONSTRAINT_NAME='FK_factDocuments_dimDocumentTypes' )
			ALTER TABLE RecHubData.factDocuments DROP CONSTRAINT FK_factDocuments_dimDocumentTypes;

		RAISERROR('Dropping dimDocumentTypes contraints',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubData' AND TABLE_NAME='dimDocumentTypes' AND CONSTRAINT_NAME='PK_dimDocumentTypes' )
			ALTER TABLE RecHubData.dimDocumentTypes DROP CONSTRAINT PK_dimDocumentTypes;

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_dimDocumentTypes_ModifiedBy')
			ALTER TABLE RecHubData.dimDocumentTypes DROP CONSTRAINT DF_dimDocumentTypes_ModifiedBy;

		RAISERROR('Rebuilding RecHubData.dimDocumentTypes',10,1) WITH NOWAIT;
		EXEC sp_rename 'RecHubData.dimDocumentTypes', 'Old_dimDocumentTypes';

		CREATE TABLE RecHubData.dimDocumentTypes
		(
			DocumentTypeKey INT IDENTITY(0,1) NOT NULL 
				CONSTRAINT PK_dimDocumentTypes PRIMARY KEY CLUSTERED,
			MostRecent		BIT NOT NULL,
			CreationDate	DATETIME NOT NULL,
			ModificationDate DATETIME NOT NULL,
			ModifiedBy		VARCHAR(128) NOT NULL
				CONSTRAINT DF_dimDocumentTypes_ModifiedBy DEFAULT SUSER_SNAME(),
			FileDescriptor	VARCHAR(30) NOT NULL,
			DocumentTypeDescription VARCHAR(64) NOT NULL,
			IMSDocumentType VARCHAR(30) NOT NULL,
			IsReadOnly		BIT NOT NULL 
				CONSTRAINT DF_dimDocumentTypes_IsReadOnly DEFAULT 0
		);

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubData', 'TABLE', 'dimDocumentTypes', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubData',
				@level1type = N'TABLE',
				@level1name = N'dimDocumentTypes';		

		EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Document Types dimension is a static data dimension for document types. 
*
*
* Defaults:
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2010 CR 31787 JPB	Added IMSDocumentType, added IDX_dimDocumentTypes_FileDescriptor
* 03/04/2013 WI 90112 JBS	Update table to 2.0 release. Change Schema Name
*							Add Columns ModificationDate and ModifiedBy.
*							Rename LoadDate to CreationDate.  Change length of
*							DocumentTypeDescription from 16 to 64 and ModifiedBy
*							from 32 to 128.
* 10/24/2014 WI 174073 JBS	Adding column IsReadOnly for management of DocumentTypes.
******************************************************************************/',
		@level0type = N'SCHEMA',@level0name = RecHubData,
		@level1type = N'TABLE',@level1name = dimDocumentTypes;

		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT;

		SET IDENTITY_INSERT RecHubData.dimDocumentTypes ON;
	
		INSERT INTO RecHubData.dimDocumentTypes 
		(
			DocumentTypeKey,
			MostRecent,
			CreationDate,
			ModificationDate,
			ModifiedBy,		
			FileDescriptor,	
			DocumentTypeDescription,
			IMSDocumentType, 
			IsReadOnly	
		) 
		SELECT	
			RecHubData.Old_dimDocumentTypes.DocumentTypeKey,
			RecHubData.Old_dimDocumentTypes.MostRecent,
			RecHubData.Old_dimDocumentTypes.CreationDate,
			RecHubData.Old_dimDocumentTypes.ModificationDate,
			RecHubData.Old_dimDocumentTypes.ModifiedBy,
			RecHubData.Old_dimDocumentTypes.FileDescriptor,
			RecHubData.Old_dimDocumentTypes.DocumentTypeDescription,
			RecHubData.Old_dimDocumentTypes.IMSDocumentType,
			0		
		FROM 	
			RecHubData.Old_dimDocumentTypes;

		SET IDENTITY_INSERT RecHubData.dimDocumentTypes OFF;

		ALTER TABLE RecHubData.factDocuments ADD
			CONSTRAINT FK_factDocuments_dimDocumentTypes FOREIGN KEY(DocumentTypeKey) REFERENCES RecHubData.dimDocumentTypes(DocumentTypeKey);


		RAISERROR('Dropping old table',10,1) WITH NOWAIT;
		IF OBJECT_ID('RecHubData.Old_dimDocumentTypes') IS NOT NULL
			DROP TABLE RecHubData.Old_dimDocumentTypes;
	END
	ELSE 
		RAISERROR('WI 90112 Needs to be applied First to the database.',10,1) WITH NOWAIT;
		
END
ELSE
	RAISERROR('WI 174073 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd