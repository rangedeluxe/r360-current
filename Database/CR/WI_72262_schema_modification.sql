--WFSScriptProcessorPrint WI 72262
--WFSScriptProcessorPrint Updating OTISQueue if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OTISQueue]') AND name = N'IDX_OTISQueue_QueueStatusModificationDateRetryCount' )
BEGIN

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'dbo', 'TABLE', 'OTISQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'dbo',
			@level1type = N'TABLE',
			@level1name = N'OTISQueue';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/04/2010
*
* Purpose: 
*		   
*
* Modification History
* 10/04/2010 CR 31308 JPB	Created
* 01/02/2013 WI 72262 JPB	Added index.
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = dbo,
	@level1type = N'TABLE',@level1name = OTISQueue
	
	RAISERROR('Creating index IDX_OTISQueue_QueueStatusModificationDateRetryCount',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_OTISQueue_QueueStatusModificationDateRetryCount ON dbo.OTISQueue
	(
		QueueStatus,
		ModificationDate,
		RetryCount
	)
	INCLUDE
	(
		BankID,
		LockboxID,
		ProcessingDate,
		BatchID,
		ActionCode,
		NotifyIMS,
		KeepStats
	);
END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd