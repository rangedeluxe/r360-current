--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 204445
--WFSScriptProcessorPrint Changing  Table to Add Add IsActive, CreationDate and ModificationDate.
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND COLUMN_NAME='IsAssigned' )
BEGIN   

	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubAlert' AND TABLE_NAME = 'Alerts' AND COLUMN_NAME = 'IsActive')
	BEGIN
		RAISERROR('ReBuilding Table RecHubAlert.Alerts.',10,1) WITH NOWAIT
			
		RAISERROR('Dropping Foreign Keys',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND CONSTRAINT_NAME='FK_Alerts_Users' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT FK_Alerts_Users;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND CONSTRAINT_NAME='FK_Alerts_EventRules' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT FK_Alerts_EventRules;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND CONSTRAINT_NAME='FK_Alerts_DeliveryMethods' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT FK_Alerts_DeliveryMethods;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='EventLog' AND CONSTRAINT_NAME='FK_EventLog_Alerts' )
			ALTER TABLE RecHubAlert.EventLog DROP CONSTRAINT FK_EventLog_Alerts;
		
		RAISERROR('Dropping Default Constraints',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM sys.default_Constraints WHERE NAME='DF_Alerts_CreationDate' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT DF_Alerts_CreationDate;
		IF EXISTS(SELECT 1 FROM sys.default_Constraints WHERE NAME='DF_Alerts_CreatedBy' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT DF_Alerts_CreatedBy;
		IF EXISTS(SELECT 1 FROM sys.default_Constraints WHERE NAME='DF_Alerts_ModificationDate' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT DF_Alerts_ModificationDate;
		IF EXISTS(SELECT 1 FROM sys.default_Constraints WHERE NAME='DF_Alerts_ModifiedBy' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT DF_Alerts_ModifiedBy;
		IF EXISTS(SELECT 1 FROM sys.default_Constraints WHERE NAME='DF_Alerts_IsActive' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT DF_Alerts_IsActive;


		RAISERROR('Dropping RecHubAlert.Alerts contraints',10,1) WITH NOWAIT;
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubAlert' AND TABLE_NAME='Alerts' AND CONSTRAINT_NAME='PK_Alerts' )
			ALTER TABLE RecHubAlert.Alerts DROP CONSTRAINT PK_Alerts;

		RAISERROR('Rebuilding RecHubAlert.Alerts',10,1) WITH NOWAIT;
		EXEC sp_rename 'RecHubAlert.Alerts', 'OLDAlerts'

		CREATE TABLE RecHubAlert.Alerts
		(
			AlertID				BIGINT IDENTITY
				CONSTRAINT PK_Alerts PRIMARY KEY CLUSTERED,
			UserID				INT NULL,
			DeliveryMethodID	SMALLINT NOT NULL,
			EventRuleID			BIGINT NOT NULL,
			IsActive			BIT	NOT NULL 
				CONSTRAINT DF_Alerts_IsActive DEFAULT(1),
			IsAssigned			BIT NULL
				CONSTRAINT DF_Alerts_IsAssigned DEFAULT(1),
			CreationDate DATETIME NOT NULL 
				CONSTRAINT DF_Alerts_CreationDate DEFAULT(GETDATE()),
			CreatedBy VARCHAR(128) NOT NULL 
				CONSTRAINT DF_Alerts_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate DATETIME NOT NULL 
				CONSTRAINT DF_Alerts_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy VARCHAR(128) NOT NULL 
				CONSTRAINT DF_Alerts_ModifiedBy DEFAULT(SUSER_SNAME())
		);
		
		RAISERROR('Updating RecHubAlert.Alerts table properties.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHubAlert', 'TABLE', 'Alerts', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'RecHubAlert',
				@level1type = N'TABLE',
				@level1name = N'Alerts';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores Alerts for R360 Alerts.
*
*
* Modification History
* 05/06/2013 WI 99839  JBS	Created
* 01/26/2015 WI 186370 JBS	Add IsActive, CreationDate and ModificationDate
* 05/28/2015 WI 204445 CMC  Add IsAssigned Column
******************************************************************************/',
	@level0type = N'SCHEMA',@level0name = RecHubAlert,
	@level1type = N'TABLE',@level1name = Alerts;

		RAISERROR('Copying data from old RecHubAlert.Alerts table to rebuilt table.',10,1) WITH NOWAIT;
	
		DECLARE @CurrentDate	DATETIME;
		SET @CurrentDate = GETDATE();

		SET IDENTITY_INSERT RecHubAlert.Alerts ON;
	
		INSERT INTO RecHubAlert.Alerts 
		(
			AlertID,
			UserID,
			DeliveryMethodID,
			EventRuleID,
			IsActive,
			IsAssigned,
			CreationDate ,
			CreatedBy ,
			ModificationDate,
			ModifiedBy 
		) 
		SELECT	
			AlertID,
			UserID,
			DeliveryMethodID,
			EventRuleID,
			IsActive,
			CASE 
				WHEN IsActive = 1 THEN 1 
				ELSE 0 
			END,
			@CurrentDate ,
			SUSER_SNAME() ,
			@CurrentDate,
			SUSER_SNAME() 
		FROM 	
			RecHubAlert.OLDAlerts;

		SET IDENTITY_INSERT RecHubAlert.Alerts OFF;
 
		ALTER TABLE RecHubAlert.Alerts ADD
			CONSTRAINT FK_Alerts_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID),
			CONSTRAINT FK_Alerts_EventRules FOREIGN KEY(EventRuleID) REFERENCES RecHubAlert.EventRules(EventRuleID),
			CONSTRAINT FK_Alerts_DeliveryMethods FOREIGN KEY(DeliveryMethodID) REFERENCES RecHubAlert.DeliveryMethods(DeliveryMethodID);
		ALTER TABLE RecHubAlert.EventLog ADD
			CONSTRAINT FK_EventLog_Alerts FOREIGN KEY(AlertID) REFERENCES RecHubAlert.Alerts(AlertID);

		IF (SELECT COUNT(*) FROM RecHubAlert.Alerts) = (SELECT COUNT(*) FROM RecHubAlert.OLDAlerts)
			DROP TABLE RecHubAlert.OLDAlerts
		ELSE
			RAISERROR('WI 204445 - not complete.  Error in copying table from existing table. RecHubAlert.Alerts should equal RecHubAlert.OLDAlerts',16,1) WITH NOWAIT;

	END
	ELSE 
		RAISERROR('WI 186370 Needs to be applied to the database before WI 204445.',10,1) WITH NOWAIT;
END
ELSE
	RAISERROR('WI 204445 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd