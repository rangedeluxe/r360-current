--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT #137726225
--WFSScriptProcessorPrint Remove deprecated DIT objects if necessary.
--WFSScriptProcessorCRBegin
--Stored Procedures
IF OBJECT_ID('DITStaging.usp_XMLBatch_Parse') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLBatch_Parse',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLBatch_Parse
END
IF OBJECT_ID('DITStaging.usp_XMLBatchDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLBatchDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLBatchDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLBatchDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLBatchDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLBatchDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLBatchNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLBatchNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLBatchNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLBatchNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLBatchNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLBatchNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentItemDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentItemDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentItemDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentItemDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentItemDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentItemDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentRemittanceDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentRemittanceDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentRemittanceDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentRemittanceDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentRemittanceDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentRemittanceDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLDocumentRemittanceDataRecordNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLDocumentRemittanceDataRecordNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLDocumentRemittanceDataRecordNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentItemDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentItemDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentItemDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentItemDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentItemDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentItemDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentRemittanceDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentRemittanceDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentRemittanceDataRecordNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLGhostDocumentRemittanceDataRecordNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLGhostDocumentRemittanceDataRecordNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentItemDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentItemDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentItemDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentItemDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentItemDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentItemDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentRawPaymentDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentRawPaymentDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentRawPaymentDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentRemittanceDataNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentRemittanceDataNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentRemittanceDataNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLPaymentRemittanceDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLPaymentRemittanceDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLPaymentRemittanceDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLRawPaymentDataNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLRawPaymentDataNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLRawPaymentDataNodes_Ins
END
IF OBJECT_ID('DITStaging.usp_XMLTransactionNodes_Get') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLTransactionNodes_Get',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLTransactionNodes_Get
END
IF OBJECT_ID('DITStaging.usp_XMLTransactionNodes_Ins') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure usp_XMLTransactionNodes_Ins',10,1) WITH NOWAIT;
	DROP PROCEDURE DITStaging.usp_XMLTransactionNodes_Ins
END

--Tables
IF OBJECT_ID('DITStaging.XMLBatchDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLBatchDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLBatchDataNodes
END
IF OBJECT_ID('DITStaging.XMLBatchNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLBatchNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLBatchNodes
END
IF OBJECT_ID('DITStaging.XMLDocumentItemDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLDocumentItemDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLDocumentItemDataNodes
END
IF OBJECT_ID('DITStaging.XMLDocumentNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLDocumentNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLDocumentNodes
END
IF OBJECT_ID('DITStaging.XMLDocumentRemittanceDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLDocumentRemittanceDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLDocumentRemittanceDataNodes
END
IF OBJECT_ID('DITStaging.XMLDocumentRemittanceDataRecordNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLDocumentRemittanceDataRecordNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLDocumentRemittanceDataRecordNodes
END
IF OBJECT_ID('DITStaging.XMLGhostDocumentItemDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLGhostDocumentItemDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLGhostDocumentItemDataNodes
END
IF OBJECT_ID('DITStaging.XMLGhostDocumentNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLGhostDocumentNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLGhostDocumentNodes
END
IF OBJECT_ID('DITStaging.XMLGhostDocumentRemittanceDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLGhostDocumentRemittanceDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLGhostDocumentRemittanceDataNodes
END
IF OBJECT_ID('DITStaging.XMLGhostDocumentRemittanceDataRecordNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLGhostDocumentRemittanceDataRecordNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLGhostDocumentRemittanceDataRecordNodes
END
IF OBJECT_ID('DITStaging.XMLPaymentItemDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLPaymentItemDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLPaymentItemDataNodes
END
IF OBJECT_ID('DITStaging.XMLPaymentNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLPaymentNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLPaymentNodes
END
IF OBJECT_ID('DITStaging.XMLPaymentRemittanceDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLPaymentRemittanceDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLPaymentRemittanceDataNodes
END
IF OBJECT_ID('DITStaging.XMLRawPaymentDataNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLRawPaymentDataNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLRawPaymentDataNodes
END
IF OBJECT_ID('DITStaging.XMLTransactionNodes') IS NOT NULL
BEGIN
	RAISERROR('Dropping table XMLTransactionNodes',10,1) WITH NOWAIT;
	DROP TABLE DITStaging.XMLTransactionNodes
END
--WFSScriptProcessorCREnd
