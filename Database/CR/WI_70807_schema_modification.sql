--WFSScriptProcessorPrint WI 70807
--WFSScriptProcessorPrint Updating SessionActivityLog if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS( SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[SessionActivityLog]') AND name = N'IDX_SessionActivityLogs_BankLockboxIDProcessingDateDeliveredToWebActivityCode' )
BEGIN

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'SessionActivityLog', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'OLTA',
			@level1type = N'TABLE',
			@level1name = N'SessionActivityLog';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 12/04/2012 WI 70807 JPB	Added index for billing (FP:CR 56577)
******************************************************************************/
',
	@level0type = N'SCHEMA',@level0name = OLTA,
	@level1type = N'TABLE',@level1name = SessionActivityLog
	
	RAISERROR('Creating index OLTA.SessionActivityLog.IDX_SessionActivityLogs_BankLockboxIDProcessingDateDeliveredToWebActivityCode',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_SessionActivityLogs_BankLockboxIDProcessingDateDeliveredToWebActivityCode ON OLTA.SessionActivityLog(BankID,LockboxID,ProcessingDate,DeliveredToWeb,ActivityCode);
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd