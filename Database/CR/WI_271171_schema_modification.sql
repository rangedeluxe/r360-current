--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 271171
--WFSScriptProcessorPrint Drop Indexes if exist and Add back on Partition for factDocuments.
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='RecHubData' AND TABLE_NAME='factDocuments')
BEGIN
	IF NOT EXISTS(SELECT 1 	FROM sys.tables 
		JOIN sys.indexes 
			ON sys.tables.object_id = sys.indexes.object_id
		JOIN sys.columns 
			ON sys.tables.object_id = sys.columns.object_id
		JOIN sys.partition_schemes 
			ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
		WHERE sys.tables.name = 'factDocuments'  
		AND sys.indexes.type = 1 )   -- this will determine if the Clustered index is partitioned already and not rerun this script
	BEGIN
		RAISERROR('Dropping indexes',10,1) WITH NOWAIT; 
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_DepositDatefactDocumentKey')
			DROP INDEX IDX_factDocuments_DepositDatefactDocumentKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence')
			DROP INDEX IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_BankKey')
			DROP INDEX IDX_factDocuments_BankKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_OrganizationKey')
			DROP INDEX IDX_factDocuments_OrganizationKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_ClientAccountKey')
			DROP INDEX IDX_factDocuments_ClientAccountKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_ImmutableDateKey')
			DROP INDEX IDX_factDocuments_ImmutableDateKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_DocumentTypeKey')
			DROP INDEX IDX_factDocuments_DocumentTypeKey ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence')
			DROP INDEX IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber')
			DROP INDEX IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber ON RecHubData.factDocuments;
		If EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'IDX_factDocuments_DepositDateKeyBatchIDDepositStatus')
			DROP INDEX IDX_factDocuments_DepositDateKeyBatchIDDepositStatus ON RecHubData.factDocuments;
		

		/****** Object:  Index PK_factDocuments    ******/
		IF EXISTS(SELECT 1 FROM sys.key_constraints WHERE name = 'PK_factDocuments')
			ALTER TABLE RecHubData.factDocuments DROP CONSTRAINT PK_factDocuments;

		RAISERROR('Adding Primary Key ',10,1) WITH NOWAIT;
		ALTER TABLE RecHubData.factDocuments ADD
			CONSTRAINT PK_factDocuments PRIMARY KEY NONCLUSTERED (factDocumentKey,DepositDateKey) $(OnDataPartition);

		RAISERROR('Rebuild indexes on Partition',10,1) WITH NOWAIT;
		RAISERROR('Rebuilding index IDX_factDocuments_DepositDatefactDocumentKey',10,1) WITH NOWAIT;
		CREATE CLUSTERED INDEX IDX_factDocuments_DepositDatefactDocumentKey ON RecHubData.factDocuments
		(
			DepositDateKey,
			factDocumentKey
		) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence',10,1) WITH NOWAIT;
		CREATE INDEX IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence ON RecHubData.factDocuments
		(
			DepositDateKey,
			ClientAccountKey,
			ImmutableDateKey,
			BatchID,
			BatchSequence
		) $(OnDataPartition); 

		RAISERROR('Rebuilding index IDX_factDocuments_BankKey',10,1) WITH NOWAIT;
			CREATE INDEX IDX_factDocuments_BankKey ON RecHubData.factDocuments (BankKey) $(OnDataPartition); 
		RAISERROR('Rebuilding index IDX_factDocuments_OrganizationKey',10,1) WITH NOWAIT;
			CREATE INDEX IDX_factDocuments_OrganizationKey ON RecHubData.factDocuments (OrganizationKey) $(OnDataPartition); 
		RAISERROR('Rebuilding index IDX_factDocuments_ClientAccountKey',10,1) WITH NOWAIT;
			CREATE INDEX IDX_factDocuments_ClientAccountKey ON RecHubData.factDocuments (ClientAccountKey) $(OnDataPartition); 
		RAISERROR('Rebuilding index IDX_factDocuments_ImmutableDateKey',10,1) WITH NOWAIT;
			CREATE INDEX IDX_factDocuments_ImmutableDateKey ON RecHubData.factDocuments (ImmutableDateKey) $(OnDataPartition); 
		RAISERROR('Rebuilding index IDX_factDocuments_DocumentTypeKey',10,1) WITH NOWAIT;
			CREATE INDEX IDX_factDocuments_DocumentTypeKey ON RecHubData.factDocuments (DocumentTypeKey) $(OnDataPartition);
 
		RAISERROR('Rebuilding index IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factDocuments 
		(
			BankKey ASC,
			OrganizationKey ASC,
			ClientAccountKey ASC,
			ImmutableDateKey ASC,
			DepositDateKey ASC,
			BatchID ASC,
			BatchNumber ASC,
			BatchSequence ASC
		) $(OnDataPartition);
 
		RAISERROR('Rebuilding index IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber ON RecHubData.factDocuments
		(
			DepositDateKey ASC,
			ImmutableDateKey ASC,
			BankKey ASC,
			ClientAccountKey ASC,
			BatchID ASC,
			BatchNumber ASC
		) $(OnDataPartition);
 
		RAISERROR('Rebuilding index IDX_factDocuments_DepositDateKeyBatchIDDepositStatus',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateKeyBatchIDDepositStatus ON RecHubData.factDocuments
		(
			DepositDateKey ASC,
			BatchID ASC,
			DepositStatus ASC
		) 
		INCLUDE
		(
			ClientAccountKey,
			ImmutableDateKey,
			DocumentTypeKey,
			TransactionID,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			DocumentSequence
		) $(OnDataPartition);

	END
	ELSE
		RAISERROR('WI 271171 has already been applied to the database.',10,1) WITH NOWAIT;
END
	ELSE
		RAISERROR('WI 271171 RecHubData.factDocuments table is not in database.  Add table and rerun script.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd