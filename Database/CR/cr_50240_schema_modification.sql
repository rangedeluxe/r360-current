--WFSScriptProcessorPrint CR 50240
--WFSScriptProcessorPrint Adding HOA to OLTA.OLLockboxes if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxes' AND COLUMN_NAME='HOA')
BEGIN
/* verify that CR 46141 has been applied before continuing. */
	IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxes' AND COLUMN_NAME='InvoiceBalancingOption' )
	BEGIN
		RAISERROR('CR 46141 must be applied before this CR.',16,1) WITH NOWAIT	
	END
	ELSE
	BEGIN

		RAISERROR('Dropping Foreign Keys.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxUsers' AND CONSTRAINT_NAME='FK_OLLockboxes_OLLockboxUsers' )
			ALTER TABLE OLTA.OLLockboxUsers DROP CONSTRAINT FK_OLLockboxes_OLLockboxUsers
			
		RAISERROR('Dropping OLLockboxes contraints.',10,1) WITH NOWAIT
		IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='OLTA' AND TABLE_NAME='OLLockboxes' AND CONSTRAINT_NAME='PK_OLLockboxes' )
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT PK_OLLockboxes

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_OLLockboxID')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_OLLockboxID

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_InvoiceBalancingOption')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_InvoiceBalancingOption

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_CreationDate')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_CreationDate

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_CreatedBy')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_CreatedBy

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_ModificationDate')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_ModificationDate

		IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_OLLockboxes_ModifiedBy')
			ALTER TABLE OLTA.OLLockboxes DROP CONSTRAINT DF_OLLockboxes_ModifiedBy

		RAISERROR('Rebuilding OLTA.OLLockboxes.',10,1) WITH NOWAIT
		EXEC sp_rename 'OLTA.OLLockboxes', 'OLDOLLockboxes'

		CREATE TABLE OLTA.OLLockboxes 
		(
			OLLockboxID uniqueidentifier NOT NULL 
				CONSTRAINT PK_OLLockboxes PRIMARY KEY CLUSTERED
				CONSTRAINT DF_OLLockboxes_OLLockboxID DEFAULT(NEWID()),
			OLCustomerID uniqueidentifier NOT NULL,
			SiteBankID int NOT NULL,
			SiteLockboxID int NOT NULL,
			DisplayName varchar(50) NOT NULL,
			IsActive bit NOT NULL,
			ViewingDays int NULL,
			MaximumSearchDays int NULL,
			CheckImageDisplayMode tinyint NOT NULL,
			DocumentImageDisplayMode tinyint NOT NULL,
			InvoiceBalancingOption tinyint NOT NULL
				CONSTRAINT DF_OLLockboxes_InvoiceBalancingOption DEFAULT(0),
			HOA bit NOT NULL
				CONSTRAINT DF_OLLockboxes_HOA DEFAULT(0),
			CreationDate datetime NOT NULL 
				CONSTRAINT DF_OLLockboxes_CreationDate DEFAULT(GETDATE()),
			CreatedBy varchar(32) NOT NULL 
				CONSTRAINT DF_OLLockboxes_CreatedBy DEFAULT(SUSER_SNAME()),
			ModificationDate datetime NOT NULL 
				CONSTRAINT DF_OLLockboxes_ModificationDate DEFAULT(GETDATE()),
			ModifiedBy varchar(32) NOT NULL 
				CONSTRAINT DF_OLLockboxes_ModifiedBy DEFAULT(SUSER_SNAME())       
		)

		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'OLLockboxes', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'OLLockboxes';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 08/09/2011 CR 46141 JPB	Added InvoiceBalancingOption.
* 02/14/2012 CR 50240 JPB	Added HOA.
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = OLLockboxes


		RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 
		INSERT INTO OLTA.OLLockboxes
		(
			OLLockboxID, 
			OLCustomerID,
			SiteBankID,
			SiteLockboxID,
			DisplayName,
			IsActive,
			ViewingDays,
			MaximumSearchDays,
			CheckImageDisplayMode,
			DocumentImageDisplayMode,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
		)
		SELECT 	OLLockboxID,
				OLCustomerID,
				SiteBankID,
				SiteLockboxID,
				DisplayName,
				IsActive,
				ViewingDays,
				MaximumSearchDays,
				CheckImageDisplayMode,
				DocumentImageDisplayMode,
				CreationDate,
				CreatedBy,
				ModificationDate,
				ModifiedBy
		FROM	OLTA.OLDOLLockboxes
		
		RAISERROR('Rebuilding Foreign Keys.',10,1) WITH NOWAIT
		ALTER TABLE OLTA.OLLockboxUsers ADD 
		   CONSTRAINT FK_OLLockboxes_OLLockboxUsers FOREIGN KEY (OLLockboxID) REFERENCES OLTA.OLLockboxes(OLLockboxID)

		IF OBJECT_ID('OLTA.OLDOLLockboxes') IS NOT NULL
			DROP TABLE OLTA.OLDOLLockboxes
	END
END
ELSE
	RAISERROR('CR has already been applied to the database.',10,1) WITH NOWAIT
--WFSScriptProcessorCREnd
