--WFSScriptProcessorPrint CR 50277
--WFSScriptProcessorPrint Add factItemData Index IDX_factItemData_BankCustomerLockboxProcessingDateDepositDateKeyBatchID if necessary
--WFSScriptProcessorCRBegin
IF  NOT EXISTS (SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID(N'[OLTA].[factItemData]') AND name = N'IDX_factItemData_BankCustomerLockboxProcessingDateDepositDateKeyBatchID')
BEGIN
	/* check for last known change to table. If it is not there, do not continue with the change. */
	IF EXISTS (	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'OLTA' AND TABLE_NAME = 'factItemData' AND COLUMN_NAME = 'BatchSequence' )
	BEGIN
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'OLTA', 'TABLE', 'factItemData', default, default) )
			EXEC sys.sp_dropextendedproperty 
				@name = N'Table_Description',
				@level0type = N'SCHEMA',
				@level0name = N'OLTA',
				@level1type = N'TABLE',
				@level1name = N'factItemData';		

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*		   
*
* Modification History
* 03/07/2011 CR 33210 JPB	Created
* 03/19/2012 CR 51050 WJS	Added index
******************************************************************************/
',
		@level0type = N'SCHEMA',@level0name = OLTA,
		@level1type = N'TABLE',@level1name = factItemData

		CREATE NONCLUSTERED INDEX IDX_factItemData_BankCustomerLockboxProcessingDateDepositDateKeyBatchID ON OLTA.factItemData
		(
			[BankKey] ASC,
			[LockboxKey] ASC,
			[ProcessingDateKey] ASC,
			[DepositDateKey] ASC,
			[BatchID] ASC,
			[BatchSequence] ASC
		) 
	END
	ELSE
	BEGIN
		RAISERROR('FactItem Data is not created. You should create it before running this script..',10,1) WITH NOWAIT
	END
END
--WFSScriptProcessorCREnd
