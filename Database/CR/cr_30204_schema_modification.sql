--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorTableName factTransactionDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/31/2010
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/22/2010 CR 29285 JPB	Created
* 07/13/2010 CR 30204 JPB	Release of CR Create script to allow for non-partition
*							installation.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE OLTA.factTransactionDetails
(
	BankKey int NOT NULL,
	CustomerKey int NOT NULL,
	LockboxKey int NOT NULL,
	ProcessingDateKey int NOT NULL,
	DepositDateKey int NOT NULL,
	BatchID int NOT NULL,
	DESetupID int NULL,
	SystemType tinyint NOT NULL,
	DepositStatus int NOT NULL,
	TransactionID int NOT NULL,
	TxnSequence int NOT NULL,
	CheckCount int NOT NULL,
	DocumentCount int NOT NULL,
	ScannedCheckCount int NOT NULL,
	StubCount int NOT NULL,
	OMRCount int NOT NULL,
	CheckTotal money NOT NULL,
	RemitterKey int NULL,
	CheckSequence int NULL,
	Serial varchar(30) NULL,
	NumericSerial bigint NULL,
	Amount money NULL,
	DataEntryBatchSequence int NULL,
	LoadDate datetime NOT NULL,
	ModificationDate datetime NOT NULL 
		CONSTRAINT DF_factTransactionDetails_ModificationDate DEFAULT GETDATE()
) $(OnPartition)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE OLTA.factTransactionDetails ADD 
       CONSTRAINT FK_dimBanks_factTransactionDetails FOREIGN KEY(BankKey) REFERENCES OLTA.dimBanks(BankKey),
       CONSTRAINT FK_dimCustomers_factTransactionDetails FOREIGN KEY(CustomerKey) REFERENCES OLTA.dimCustomers(CustomerKey),
       CONSTRAINT FK_dimLockboxes_factTransactionDetails FOREIGN KEY(LockboxKey) REFERENCES OLTA.dimLockboxes(LockboxKey),
       CONSTRAINT FK_ProcessingDate_factTransactionDetails FOREIGN KEY(ProcessingDateKey) REFERENCES OLTA.dimDates(DateKey),
       CONSTRAINT FK_DepositDate_factTransactionDetails FOREIGN KEY(DepositDateKey) REFERENCES OLTA.dimDates(DateKey)
--WFSScriptProcessorIndex OLTA.factTransactionDetails.IDX_factTransactionDetails_DepositDateKey
CREATE CLUSTERED INDEX IDX_factTransactionDetails_DepositDateKey ON OLTA.factTransactionDetails (DepositDateKey) $(OnPartition)
--WFSScriptProcessorIndex OLTA.factTransactionDetails.IDX_factTransactionDetails_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID
CREATE NONCLUSTERED INDEX IDX_factTransactionDetails_LockboxDepositDateBatchIDDepositStatusBankCustomerProcessingDateTransactonID ON OLTA.factTransactionDetails
(
	[LockboxKey] ASC,
	[DepositDateKey] ASC,
	[BatchID] ASC,
	[DepositStatus] ASC,
	[BankKey] ASC,
	[CustomerKey] ASC,
	[ProcessingDateKey] ASC,
	[TransactionID] ASC
) $(OnPartition)
