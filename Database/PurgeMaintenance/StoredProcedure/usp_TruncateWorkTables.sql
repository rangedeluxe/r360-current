--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_TruncateWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_TruncateWorkTables') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_TruncateWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_TruncateWorkTables 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2015
*
* Purpose: Truncate the SSIS package working tables.
*
* Modification History
* 04/07/2015 WI 201483 JPB	Created
* 03/25/2016 WI 207854 JBS	Add new table for Permissions
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	TRUNCATE TABLE PurgeMaintenance.CheckConstraintSQL;
	TRUNCATE TABLE PurgeMaintenance.PermissionSQL;
	TRUNCATE TABLE PurgeMaintenance.DefaultConstraintSQL;
	TRUNCATE TABLE PurgeMaintenance.ExtendedPropertySQL;
	TRUNCATE TABLE PurgeMaintenance.ForeignKeySQL;
	TRUNCATE TABLE PurgeMaintenance.IndexSQL;
	TRUNCATE TABLE PurgeMaintenance.LogicalDeleteSQL;
	TRUNCATE TABLE PurgeMaintenance.PhysicalDeleteSQL;
	TRUNCATE TABLE PurgeMaintenance.PurgeProcessorSQL0;
	TRUNCATE TABLE PurgeMaintenance.PurgeProcessorSQL1;
	TRUNCATE TABLE PurgeMaintenance.PurgeProcessorSQL2;
	TRUNCATE TABLE PurgeMaintenance.PurgeProcessorSQL3;
	TRUNCATE TABLE PurgeMaintenance.SyncSQL;
	TRUNCATE TABLE PurgeMaintenance.TableColumns;
	TRUNCATE TABLE PurgeMaintenance.TableList;
	TRUNCATE TABLE PurgeMaintenance.TableSQL;
	TRUNCATE TABLE PurgeMaintenance.TablePartitionSchemeSQL;

	IF OBJECT_ID('PurgeMaintenance.dimClientAccounts') IS NOT NULL
		   DROP TABLE PurgeMaintenance.dimClientAccounts;

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
