--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_CreateTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_CreateTables') IS NOT NULL
       DROP PROCEDURE PurgeMaintenance.usp_CreateTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_CreateTables 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Create tables.
*
* Modification History
* 04/08/2015 WI 201457 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SQL VARCHAR(MAX) = '',
		@Loop INT=1;	

BEGIN TRY

	WHILE( @Loop <= (SELECT MAX(TableID) FROM PurgeMaintenance.TableSQL) )
	BEGIN
		SET @SQL='';
		SELECT 
			@SQL = DropNewCommand+CreateCommand
		FROM
			PurgeMaintenance.TableSQL
			INNER JOIN PurgeMaintenance.TableList ON PurgeMaintenance.TableList.TableID = PurgeMaintenance.TableSQL.TableID
		WHERE 
			PurgeMaintenance.TableSQL.TableID = @Loop
		ORDER BY 
			PurgeMaintenance.TableSQL.TableID;

		IF( LEN(@SQL) > 0 )
			EXEC(@SQL);

		SET @Loop=@Loop+1;
	END

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
