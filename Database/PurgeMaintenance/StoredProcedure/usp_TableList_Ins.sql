--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TableList_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_TableList_Ins') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_TableList_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_TableList_Ins 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2015
*
* Purpose: Insert the list of tables to purge.
*
*
* Modification History
* 04/07/2015 WI 201482 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @TableXML XML;

BEGIN TRY

	INSERT INTO PurgeMaintenance.TableList
	(
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TABLE_SCHEMA,
		TABLE_NAME,
		'[' + TABLE_SCHEMA + ']',
		'[' + TABLE_NAME + ']',
		'[' + TABLE_SCHEMA + '].[' + TABLE_NAME + ']'
	FROM 
		INFORMATION_SCHEMA.TABLES
	WHERE 
		TABLE_SCHEMA IN ('RecHubAudit','RecHubData') AND TABLE_NAME LIKE 'fact%'
	ORDER BY 
		TABLE_SCHEMA,TABLE_NAME;

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
