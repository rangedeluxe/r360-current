--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_PurgeSSISHistory_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_PurgeSSISHistory_Ins') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_PurgeSSISHistory_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE PurgeMaintenance.usp_PurgeSSISHistory_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: Storing Value for purging 
*	
*
* Modification History
* 06/01/2013 WI 117322 PS	Created.
* 04/07/2015 WI 201481 JPB	Moved to PurgeMaintenance 
*							Renamed from usp_Purge_SetControlValue
* 09/21/2016 PT #129555329 JBS	Make PurgePercentage a variable from SystemSetup table
******************************************************************************/
SET NOCOUNT ON;

DECLARE @TotalRecordCount BIGINT,
		@DeletedCount BIGINT,
		@DeletePrecentage FLOAT,
		@PurgePrecentage FLOAT;

	
BEGIN TRY
--@PurgePrecentage FLOAT = 0.05;
	-- SET default percentage from systemtable, DEFAULT to 5% if not found
	SELECT 
		@PurgePrecentage = COALESCE(CAST(RecHubConfig.SystemSetup.Value AS FLOAT),0.05)
	FROM 
		RecHubConfig.SystemSetup
	WHERE 
		RecHubConfig.SystemSetup.Section = 'Rec Hub Table Maintenance'
		AND RecHubConfig.SystemSetup.SetupKey = 'PurgeDeletePercentage';
 
	/* Calculate the percentage of factDocuments records that are marked for deletion */
	SET @DeletedCount = ( SELECT COUNT(*) FROM RecHubData.factDocuments WHERE IsDeleted = 1 );
	SET @TotalRecordCount = ( SELECT COUNT(*) FROM RecHubData.factDocuments ) + 1;
	SET @DeletePrecentage = (CAST(@DeletedCount AS FLOAT)/CAST(@TotalRecordCount AS FLOAT));

	/* If the percentage of records marked for deletion is at 5%, the threshold has been met */
	IF(  @DeletePrecentage >= @PurgePrecentage )
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factDocuments',
			@DeletedCount,
			1
		);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factDocuments',
			@DeletedCount,
			0
		);
	END 

	/* Calculate the percentage of factChecks records that are marked for deletion */
	SET @DeletedCount = ( SELECT COUNT(*) FROM RecHubData.factChecks WHERE IsDeleted = 1 );
	SET @TotalRecordCount = ( SELECT COUNT(*) FROM RecHubData.factChecks ) + 1;
	SET @DeletePrecentage = (CAST(@DeletedCount AS FLOAT)/CAST(@TotalRecordCount AS FLOAT));

	/* If the percentage of records marked for deletion is at 5%, the threshold has been met */
	IF(  @DeletePrecentage >= @PurgePrecentage )
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factChecks',
			@DeletedCount,
			1
		);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factChecks',
			@DeletedCount,
			0
		);
	END 

	/* Calculate the percentage of factDataEntryDetails records that are marked for deletion */
	SET @DeletedCount = ( SELECT COUNT(*) FROM RecHubData.factDataEntryDetails WHERE IsDeleted = 1 );
	SET @TotalRecordCount = ( SELECT COUNT(*) FROM RecHubData.factDataEntryDetails ) + 1;
	SET @DeletePrecentage = (CAST(@DeletedCount AS FLOAT)/CAST(@TotalRecordCount AS FLOAT));

	/* If the percentage of records marked for deletion is at 5%, the threshold has been met */
	IF(  @DeletePrecentage >= @PurgePrecentage )
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factDataEntryDetails',
			@DeletedCount,
			1
		);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factDataEntryDetails',
			@DeletedCount,
			0
		);
	END 
 
 	SET @DeletedCount = ( SELECT COUNT(*) FROM RecHubData.factBatchSummary WHERE IsDeleted =1 );
	SET @TotalRecordCount = ( SELECT COUNT(*) FROM RecHubData.factBatchSummary ) + 1;
	SET @DeletePrecentage = (CAST(@DeletedCount AS FLOAT)/CAST(@TotalRecordCount AS FLOAT));

	/* If the percentage of records marked for deletion is at 5%, the threshold has been met */
	IF(  @DeletePrecentage >= @PurgePrecentage )
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factBatchSummary',
			@DeletedCount,
			1
		);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory
		(
			TableName,
			IsDeletedCount,
			ExceedThreshold
		)
		VALUES
		(
			'RecHubData.factBatchSummary',
			@DeletedCount,
			0
		);
	END 

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
