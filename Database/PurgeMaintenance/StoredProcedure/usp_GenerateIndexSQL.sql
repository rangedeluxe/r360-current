--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateIndexSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateIndexSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateIndexSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateIndexSQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Generate index SQL scripts.
*
* Modification History
* 04/08/2015 WI 201470 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 
DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPurgeIndexSQL')) 
		DROP TABLE #tmpPurgeIndexSQL;

	CREATE TABLE #tmpPurgeIndexSQL
	(
		TableID				INT NOT NULL,
		IndexName			NVARCHAR(128) NOT NULL,
		CreateCommand		VARCHAR(MAX) NOT NULL,
		DropCommand			VARCHAR(MAX) NOT NULL
	);

	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName 
	FROM 
		PurgeMaintenance.TableList;

	INSERT INTO #tmpPurgeIndexSQL
	(
		TableID,
		IndexName,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateIndexSQL
		@parmScriptTableList=@TableList;

	INSERT INTO PurgeMaintenance.IndexSQL
	(
		TableID,
		IndexName,
		CreateCommand,
		DropCommand,
		CreateNewCommand,
		RenameActualToOld,
		RenameNewToActual
	)
	SELECT
		PurgeMaintenance.TableList.TableID,
		#tmpPurgeIndexSQL.IndexName,
		#tmpPurgeIndexSQL.CreateCommand,
		#tmpPurgeIndexSQL.DropCommand,
		REPLACE(REPLACE(CreateCommand,'['+IndexName+']','[N_'+IndexName+']'),'['+PurgeMaintenance.TableList.TableName+']','[New_'+PurgeMaintenance.TableList.TableName+']') AS CreateNewCommand,
		'EXEC SP_RENAME '+CHAR(39)+PurgeMaintenance.TableList.SchemaName+'.'+PurgeMaintenance.TableList.TableName+'.'+#tmpPurgeIndexSQL.IndexName+CHAR(39)+','+CHAR(39)+'O_'+#tmpPurgeIndexSQL.IndexName+CHAR(39) AS RenameActualToOld,
		'EXEC SP_RENAME '+CHAR(39)+PurgeMaintenance.TableList.SchemaName+'.New_'+PurgeMaintenance.TableList.TableName+'.N_'+#tmpPurgeIndexSQL.IndexName+CHAR(39)+','+CHAR(39)+#tmpPurgeIndexSQL.IndexName+CHAR(39) AS RenameNewToActual
	FROM 
		#tmpPurgeIndexSQL
		INNER JOIN PurgeMaintenance.TableList ON PurgeMaintenance.TableList.TableID = #tmpPurgeIndexSQL.TableID;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpIndexSQL')) 
		DROP TABLE #tmpIndexSQL;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpIndexSQL')) 
		DROP TABLE #tmpIndexSQL;

	EXEC SharedCommon.usp_RethrowException;
END CATCH
