--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateAuditLogicalDeleteSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateAuditLogicalDeleteSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateAuditLogicalDeleteSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateAuditLogicalDeleteSQL
(
	@parmDataRetentionDays INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/10/2015
*
* Purpose: Create SQL scripts to mark RecHubAudit.fact*  as deleted.
*
*
* Modification History
* 04/10/2015 WI 201462 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @RetentionDays VARCHAR(10);

BEGIN TRY

	SELECT 
		@RetentionDays = CAST(@parmDataRetentionDays AS VARCHAR(10));

	DECLARE @RecHubTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE SchemaName = 'RecHubAudit';

	INSERT INTO PurgeMaintenance.LogicalDeleteSQL(LogicalDeleteSQL)
	SELECT 
		'UPDATE '+PurgeMaintenance.TableList.FullyQualifiedTableName+' SET IsDeleted=1,ModificationDate=GETDATE() FROM '+PurgeMaintenance.TableList.FullyQualifiedTableName+' WHERE '+
		'AuditDateKey <= (YEAR(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*10000 + MONTH(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*100+DAY(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))) AND IsDeleted=0;'
	FROM
		@RecHubTables RecHubTables 
		INNER JOIN PurgeMaintenance.TableList ON PurgeMaintenance.TableList.TableName = RecHubTables.TableName
	WHERE
		PurgeMaintenance.TableList.SchemaName = 'RecHubAudit';

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
