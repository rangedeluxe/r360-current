--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateNotificationSyncSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateNotificationSyncSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateNotificationSyncSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateNotificationSyncSQL
(
	@parmPurgeStartTime DATETIME
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/09/2015
*
* Purpose: Generate SQL needed to copy late arriving RecHubData.factNotification% records. 
*
*
* Modification History
* 04/09/2015 WI 201473 JPB	Created
* 04/08/2016 WI 275622 JBS  Added check for Duplicate switch and fixed logic to bring in late arriving rows.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Loop INT = 1,
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@PurgeStartTime VARCHAR(32),
		@TableID INT,
		@TableName SYSNAME,
		@FITDuplicateCheck AS INT = 0;

SELECT 
	@FITDuplicateCheck = Value    -- Grab setting to determine if we need to detect for dups in scripting logic
FROM
	RecHubConfig.SystemSetup
WHERE
	RecHubConfig.SystemSetup.Section = 'FIT' AND
	RecHubConfig.SystemSetup.SetupKey = 'FITDuplicateCheck'; 

BEGIN TRY

	SELECT 
		@PurgeStartTime = CONVERT(VARCHAR(32),@parmPurgeStartTime,25);

	DECLARE @RecHubAuditTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubAuditTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubData' 
		AND TableName LIKE 'factNotification%';

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @RecHubAuditTables) )
	BEGIN
		SELECT
			@TableName = TableName
		FROM
			@RecHubAuditTables
		WHERE
			RowID = @Loop;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@InsertColumnList OUT;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@SelectColumnList OUT;
		
		IF @FITDuplicateCheck = 0
		BEGIN 
			INSERT INTO PurgeMaintenance.SyncSQL(SyncSQL)
			SELECT 
				'INSERT INTO '+FullyQualifiedSourceTableName+' WITH(TABLOCK) ('+@InsertColumnList+') '+
				'SELECT '+@SelectColumnList+' FROM ['+SourceSchemaName+'].[Old_'+SourceTableName+'] '+
				'WHERE (CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+');'
			FROM
				PurgeMaintenance.TableList
				INNER JOIN PurgeMaintenance.TableSQL ON PurgeMaintenance.TableSQL.TableID = PurgeMaintenance.TableList.TableID
			WHERE
				SchemaName = 'RecHubData'
				AND TableName = @TableName;
		END 
		ELSE 
		BEGIN
			INSERT INTO PurgeMaintenance.SyncSQL(SyncSQL)
			SELECT 
				';WITH locateKeys AS (
				---  We hard code factNotificationFiles to get to FileExtension
				--  Criteria for duplicate is as follows 
				--  SiteBankID
				--  SiteClientAccountID
				--  Notification Date (ignoring any time component)
				--  User File Name
				--  File Extension
				SELECT DISTINCT [RecHubData].factNotificationFiles.NotificationMessageGroup
				FROM [RecHubData].factNotificationFiles INNER JOIN [RecHubData].[Old_factNotificationFiles]
					ON [RecHubData].factNotificationFiles.NotificationDateKey = [RecHubData].[Old_factNotificationFiles].NotificationDateKey
						AND [RecHubData].factNotificationFiles.UserFileName = [RecHubData].[Old_factNotificationFiles].UserFileName
						AND [RecHubData].factNotificationFiles.FileExtension = [RecHubData].[Old_factNotificationFiles].FileExtension
				WHERE
					(SELECT [RecHubData].dimBanks.SiteBankID FROM [RecHubData].dimBanks WHERE [RecHubData].dimBanks.BankKey = [RecHubData].factNotificationFiles.BankKey) = 
					(SELECT [RecHubData].dimBanks.SiteBankID FROM [RecHubData].dimBanks WHERE [RecHubData].dimBanks.BankKey = [RecHubData].[Old_factNotificationFiles].BankKey)
					AND
					(SELECT [RecHubData].dimOrganizations.SiteOrganizationID FROM [RecHubData].dimOrganizations WHERE [RecHubData].dimOrganizations.OrganizationKey = [RecHubData].factNotificationFiles.OrganizationKey) =
					(SELECT [RecHubData].dimOrganizations.SiteOrganizationID FROM [RecHubData].dimOrganizations WHERE [RecHubData].dimOrganizations.OrganizationKey = [RecHubData].[Old_factNotificationFiles].OrganizationKey) 
					AND
					(SELECT [RecHubData].dimClientAccounts.SiteClientAccountID FROM [RecHubData].dimClientAccounts WHERE [RecHubData].dimClientAccounts.ClientAccountKey = [RecHubData].factNotificationFiles.ClientAccountKey) =
					(SELECT [RecHubData].dimClientAccounts.SiteClientAccountID FROM [RecHubData].dimClientAccounts WHERE [RecHubData].dimClientAccounts.ClientAccountKey = [RecHubData].[Old_factNotificationFiles].ClientAccountKey)
					AND
					([RecHubData].[Old_factNotificationFiles].CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR [RecHubData].[Old_factNotificationFiles].ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
				
				+') UPDATE '+FullyQualifiedSourceTableName+' SET '+FullyQualifiedSourceTableName+'.IsDeleted=1,'+FullyQualifiedSourceTableName+'.ModificationDate=GETDATE() '+
				'FROM '+FullyQualifiedSourceTableName+
				' INNER JOIN '+'locateKeys ON locateKeys.NotificationMessageGroup='+
				FullyQualifiedSourceTableName+'.NotificationMessageGroup '+
				'WHERE ('+FullyQualifiedSourceTableName+'.CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR '+FullyQualifiedSourceTableName+'.ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+');'+
			
				'INSERT INTO '+FullyQualifiedSourceTableName+' WITH(TABLOCK) ('+@InsertColumnList+') '+
				'SELECT '+@SelectColumnList+' FROM ['+SourceSchemaName+'].[Old_'+SourceTableName+'] '+
				'WHERE (CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+');'
			FROM
				PurgeMaintenance.TableList
				INNER JOIN PurgeMaintenance.TableSQL ON PurgeMaintenance.TableSQL.TableID = PurgeMaintenance.TableList.TableID
			WHERE
				SchemaName = 'RecHubData'
				AND TableName = @TableName;
		END

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
