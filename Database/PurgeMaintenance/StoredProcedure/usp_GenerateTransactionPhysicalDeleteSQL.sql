--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateTransactionPhysicalDeleteSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateTransactionPhysicalDeleteSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateTransactionPhysicalDeleteSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateTransactionPhysicalDeleteSQL
(
	@parmPurgeStartTime DATETIME
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Create SQL scripts RecHubData.factNotification* records that will be kept.
*
*
* Modification History
* 04/08/2015 WI 201478 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Loop INT = 1,
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@PurgeStartTime VARCHAR(32),
		@TableID INT,
		@TableName SYSNAME;

BEGIN TRY

	SELECT 
		@PurgeStartTime = CONVERT(VARCHAR(32),@parmPurgeStartTime,25);

	DECLARE @RecHubTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubData' 
		AND TableName NOT LIKE 'factNotification%';

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @RecHubTables) )
	BEGIN
		SELECT
			@TableName = TableName
		FROM
			@RecHubTables
		WHERE
			RowID = @Loop;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@InsertColumnList OUT;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=1,
			@parmColumnList=@SelectColumnList OUT;
		
		INSERT INTO PurgeMaintenance.PhysicalDeleteSQL(PhysicalDeleteSQL)
		SELECT 
			'INSERT INTO '+FullyQualifiedTargetTableName+' WITH(TABLOCK) ('+@InsertColumnList+') '+
			'SELECT '+@SelectColumnList+' FROM '+FullyQualifiedSourceTableName+' WHERE IsDeleted=0 AND '+
			CASE UPPER(@TableName)
				WHEN 'FACTBATCHEXTRACTS' THEN FullyQualifiedSourceTableName+'.ModificationDate <= '+QUOTENAME(@PurgeStartTime,CHAR(39))
				WHEN 'FACTEXTRACTTRACES' THEN '('+FullyQualifiedSourceTableName+'.ExtractDateTime <= '+QUOTENAME(@PurgeStartTime,CHAR(39))+' AND '+FullyQualifiedSourceTableName+'.ModificationDate <= '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
				ELSE '('+FullyQualifiedSourceTableName+'.CreationDate <= '+QUOTENAME(@PurgeStartTime,CHAR(39))+' AND '+FullyQualifiedSourceTableName+'.ModificationDate <= '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
			END+
			'; '
		FROM
			PurgeMaintenance.TableList
			INNER JOIN PurgeMaintenance.TableSQL ON PurgeMaintenance.TableSQL.TableID = PurgeMaintenance.TableList.TableID
		WHERE
			SchemaName = 'RecHubData'
			AND TableName = @TableName;

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
