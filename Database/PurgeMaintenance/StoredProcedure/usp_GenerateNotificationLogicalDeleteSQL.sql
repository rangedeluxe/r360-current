--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateNotificationLogicalDeleteSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateNotificationLogicalDeleteSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateNotificationLogicalDeleteSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateNotificationLogicalDeleteSQL
(
	@parmNotificationsRetentionDays INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Create SQL scripts to mark RecHubData.factNotification* as deleted.
*
*
* Modification History
* 04/08/2015 WI 201471 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @RetentionDays VARCHAR(10);

BEGIN TRY

	SELECT 
		@RetentionDays = CAST(@parmNotificationsRetentionDays AS VARCHAR(10))

	DECLARE @RecHubTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubData' 
		AND TableName LIKE 'factNotification%';

	INSERT INTO PurgeMaintenance.LogicalDeleteSQL(LogicalDeleteSQL)
	SELECT 
		'UPDATE '+FullyQualifiedTableName+' SET IsDeleted=1,ModificationDate=GETDATE() FROM '+FullyQualifiedTableName+' WHERE '+
		'NotificationDateKey <= (YEAR(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*10000 + MONTH(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))*100+DAY(DATEADD(d,-'+@RetentionDays+
		',GETDATE()))) AND IsDeleted=0;'
	FROM
			@RecHubTables RecHubTables
		INNER JOIN PurgeMaintenance.TableList ON PurgeMaintenance.TableList.TableName = RecHubTables.TableName
	WHERE
		SchemaName = 'RecHubData';

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
