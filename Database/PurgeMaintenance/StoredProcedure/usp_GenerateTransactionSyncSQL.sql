--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateTransactionSyncSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateTransactionSyncSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateTransactionSyncSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateTransactionSyncSQL
(
	@parmPurgeStartTime DATETIME
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/09/2015
*
* Purpose: Create SQL scripts for RecHubData.fact* records that will be kept.
*
* NOTE: None RecHubData.factNotification* tables.
*
*
* Modification History
* 04/09/2015 WI 201479 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Loop INT = 1,
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@PurgeStartTime VARCHAR(32),
		@TableID INT,
		@TableName SYSNAME;

BEGIN TRY

	SELECT 
		@PurgeStartTime = CONVERT(VARCHAR(32),@parmPurgeStartTime,25);

	DECLARE @RecHubAuditTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubAuditTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubData' 
		AND TableName NOT LIKE 'factNotification%';

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @RecHubAuditTables) )
	BEGIN
		SELECT
			@TableName = TableName
		FROM
			@RecHubAuditTables
		WHERE
			RowID = @Loop;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@InsertColumnList OUT;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubData',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@SelectColumnList OUT;
		
		INSERT INTO PurgeMaintenance.SyncSQL(SyncSQL)
		SELECT
			'UPDATE '+FullyQualifiedSourceTableName+' SET '+FullyQualifiedSourceTableName+'.IsDeleted=1,'+FullyQualifiedSourceTableName+'.ModificationDate=GETDATE()'+
			' FROM '+FullyQualifiedSourceTableName+
			' INNER JOIN '+'['+SourceSchemaName+'].[Old_'+SourceTableName+'] ON ['+SourceSchemaName+'].[Old_'+SourceTableName+'].DepositDateKey='+
			FullyQualifiedSourceTableName+'.DepositDateKey AND ['+SourceSchemaName+'].[Old_'+SourceTableName+'].BatchID='+
			FullyQualifiedSourceTableName+'.BatchID'+
			' WHERE '+
			CASE UPPER(@TableName)
				WHEN 'FACTBATCHEXTRACTS' THEN '['+SourceSchemaName+'].[Old_'+SourceTableName+'].ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))
				WHEN 'FACTEXTRACTTRACES' THEN '(['+SourceSchemaName+'].[Old_'+SourceTableName+'].ExtractDateTime > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ['+SourceSchemaName+'].[Old_'+SourceTableName+'].ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
				ELSE '(['+SourceSchemaName+'].[Old_'+SourceTableName+'].CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ['+SourceSchemaName+'].[Old_'+SourceTableName+'].ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
			END+';'+
			'INSERT INTO '+FullyQualifiedSourceTableName+' WITH(TABLOCK) ('+@InsertColumnList+') '+
			'SELECT '+@SelectColumnList+' FROM ['+SourceSchemaName+'].[Old_'+SourceTableName+'] '+
			'WHERE '+
			CASE UPPER(@TableName)
				WHEN 'FACTBATCHEXTRACTS' THEN 'ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))
				WHEN 'FACTEXTRACTTRACES' THEN '(ExtractDateTime > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
				ELSE '(CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+')'
			END+';'
		FROM
			PurgeMaintenance.TableList
			INNER JOIN PurgeMaintenance.TableSQL ON PurgeMaintenance.TableSQL.TableID = PurgeMaintenance.TableList.TableID
		WHERE
			SchemaName = 'RecHubData'
			AND TableName = @TableName;

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
