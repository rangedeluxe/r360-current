--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_Execute_PurgeProcessorSQL1
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_Execute_PurgeProcessorSQL1') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_Execute_PurgeProcessorSQL1
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_Execute_PurgeProcessorSQL1
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/13/2015
*
* Purpose: Execute SQL in the PurgeMaintenance.PurgeProcessorSQL1 table.
*
*
* Modification History
* 04/13/2015 WI 201459 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Loop INT=1,
		@RecCount INT,
		@SQL VARCHAR(MAX);

BEGIN TRY

	SELECT @RecCount = COUNT(*) FROM PurgeMaintenance.PurgeProcessorSQL1;

	WHILE( @Loop <= @RecCount )
	BEGIN
		SELECT 
			@SQL = PurgeSQL
		FROM
			PurgeMaintenance.PurgeProcessorSQL1
		WHERE
			TableID = @Loop;

		EXEC(@SQL);

		SET @Loop = @Loop + 1;
	END

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
