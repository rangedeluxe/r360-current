--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateExtendedPropertySQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateExtendedPropertySQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateExtendedPropertySQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateExtendedPropertySQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Generate extended property SQL scripts.
*
*
* Modification History
* 04/08/2015 WI 201468 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY

	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName 
	FROM 
		PurgeMaintenance.TableList;

	INSERT INTO PurgeMaintenance.ExtendedPropertySQL
	(
		TableID,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateExtendedPropertySQL
		@parmScriptTableList=@TableList;

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
