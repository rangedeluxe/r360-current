--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateDataAuditSyncSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateDataAuditSyncSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateDataAuditSyncSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateDataAuditSyncSQL
(
	@parmPurgeStartTime DATETIME
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/09/2015
*
* Purpose: Generate SQL needed to copy late arriving RecHubAudit.factData% records. 
*
*
* Modification History
* 04/09/2015 WI 201465 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Loop INT = 1,
		@InsertColumnList VARCHAR(MAX) = '',
		@SelectColumnList VARCHAR(MAX) = '',
		@PurgeStartTime VARCHAR(32),
		@TableID INT,
		@TableName SYSNAME;

BEGIN TRY

	SELECT 
		@PurgeStartTime = CONVERT(VARCHAR(32),@parmPurgeStartTime,25);

	DECLARE @RecHubAuditTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubAuditTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubAudit' 
		AND TableName LIKE 'factData%';

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @RecHubAuditTables) )
	BEGIN
		SELECT
			@TableName = TableName
		FROM
			@RecHubAuditTables
		WHERE
			RowID = @Loop;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubAudit',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@InsertColumnList OUT;

		EXEC PurgeMaintenance.usp_GetTableColumns 
			@parmSchemaName='RecHubAudit',
			@parmTableName=@TableName,
			@parmIncludeIdentity=0,
			@parmIncludeQualifiedTableName=0,
			@parmColumnList=@SelectColumnList OUT;
		
		INSERT INTO PurgeMaintenance.SyncSQL(SyncSQL)
		SELECT 
			'INSERT INTO '+FullyQualifiedSourceTableName+' WITH(TABLOCK) ('+@InsertColumnList+') '+
			'SELECT '+@SelectColumnList+' FROM ['+SourceSchemaName+'].[Old_'+SourceTableName+'] '+
			'WHERE (CreationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+' OR ModificationDate > '+QUOTENAME(@PurgeStartTime,CHAR(39))+');'
		FROM
			PurgeMaintenance.TableList
			INNER JOIN PurgeMaintenance.TableSQL ON PurgeMaintenance.TableSQL.TableID = PurgeMaintenance.TableList.TableID
		WHERE
			SchemaName = 'RecHubAudit'
			AND TableName = @TableName;

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
