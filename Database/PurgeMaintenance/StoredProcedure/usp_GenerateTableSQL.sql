--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateTableSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateTableSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateTableSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateTableSQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Generate table SQL scripts.
*
*
* Modification History
* 04/08/2015 WI 201476 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY
	/* Create temp tables for the SP */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

	CREATE TABLE #tmpTableSQL
	(
		TableID					INT,
		SchemaName				SYSNAME,
		TableName				SYSNAME,
		QualifiedSchemaName		SYSNAME,
		QualifiedTableName		SYSNAME,
		FullyQualifiedTableName NVARCHAR(261),
		CreateCommand			VARCHAR(MAX) NOT NULL,
		DropCommand				VARCHAR(MAX) NOT NULL,
	);

	/* Get partition scheme information */
	EXEC PurgeMaintenance.usp_GenerateTablePartitionSchemeSQL;

	/* Setup the shared data type for the SP call */
	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName,
		PartitionScheme
	)
	SELECT 
		PurgeMaintenance.TableList.TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName,
		PartitionScheme
	FROM 
		PurgeMaintenance.TableList
		LEFT JOIN PurgeMaintenance.TablePartitionSchemeSQL ON PurgeMaintenance.TablePartitionSchemeSQL.TableID = PurgeMaintenance.TableList.TableID;

	/* Call the shared SP to get the basic table information */
	INSERT INTO #tmpTableSQL
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateTableSQL 
		@parmScriptTableList=@TableList;

	/* Take the base table info and create what is needed for the purge package */
	INSERT INTO PurgeMaintenance.TableSQL
	(
		TableID,
		SourceSchemaName,
		SourceTableName,
		TargetSchemaName,
		TargetTableName,
		FullyQualifiedSourceTableName,
		FullyQualifiedTargetTableName,
		CreateCommand,
		DropCommand,
		DropNewCommand,
		RenameCurrentToOldCommand,
		RenameNewToCurrentCommand
	)
	SELECT 
		PurgeMaintenance.TableList.TableID,
		PurgeMaintenance.TableList.SchemaName AS SourceSchemaName,
		PurgeMaintenance.TableList.TableName AS SourceTableName,
		PurgeMaintenance.TableList.SchemaName AS TargetSchemaName,
		'New_'+PurgeMaintenance.TableList.TableName AS TargetTableName,
		PurgeMaintenance.TableList.QualifiedSchemaName+'.'+PurgeMaintenance.TableList.TableName AS FullyQualifiedSourceTableName,
		PurgeMaintenance.TableList.QualifiedSchemaName+'.[New_'+PurgeMaintenance.TableList.TableName+']' AS FullyQualifiedTargetTableName,
		REPLACE(CreateCommand,PurgeMaintenance.TableList.QualifiedTableName,'[New_'+PurgeMaintenance.TableList.TableName+']') AS CreateCommand,
		REPLACE(DropCommand,PurgeMaintenance.TableList.TableName,'Old_'+PurgeMaintenance.TableList.TableName) AS DropCommand,
		REPLACE(DropCommand,PurgeMaintenance.TableList.TableName,'New_'+PurgeMaintenance.TableList.TableName) AS DropCommand,
		'sp_rename '+CHAR(39)+PurgeMaintenance.TableList.SchemaName+'.'+PurgeMaintenance.TableList.TableName+CHAR(39)+','+CHAR(39)+'Old_'+PurgeMaintenance.TableList.TableName+CHAR(39)+';',
		'sp_rename '+CHAR(39)+PurgeMaintenance.TableList.SchemaName+'.'+'New_'+PurgeMaintenance.TableList.TableName+CHAR(39)+','+CHAR(39)+PurgeMaintenance.TableList.TableName+CHAR(39)+';'
	FROM
		PurgeMaintenance.TableList
		INNER JOIN #tmpTableSQL ON #tmpTableSQL.TableID = PurgeMaintenance.TableList.TableID
		LEFT JOIN PurgeMaintenance.TablePartitionSchemeSQL ON PurgeMaintenance.TablePartitionSchemeSQL.TableID = PurgeMaintenance.TableList.TableID

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpTableSQL')) 
		DROP TABLE #tmpTableSQL;

	EXEC SharedCommon.usp_RethrowException;
END CATCH
