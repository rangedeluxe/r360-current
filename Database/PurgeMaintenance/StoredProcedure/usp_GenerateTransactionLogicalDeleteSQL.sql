--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateTransactionLogicalDeleteSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateTransactionLogicalDeleteSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateTransactionLogicalDeleteSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateTransactionLogicalDeleteSQL
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/10/2015
*
* Purpose: Create SQL scripts to mark RecHubData.fact* as deleted.
*
* NOTE: None RecHubData.factNotification* tables.
*
*
* Modification History
* 04/10/2015 WI 201477 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @RecHubTables TABLE
	(
		RowID INT IDENTITY(1,1),
		TableName SYSNAME
	)

	INSERT INTO @RecHubTables(TableName)
	SELECT 
		TableName 
	FROM 
		PurgeMaintenance.TableList 
	WHERE 
		SchemaName = 'RecHubData' 
		AND TableName NOT LIKE 'factNotification%';

	INSERT INTO PurgeMaintenance.LogicalDeleteSQL(LogicalDeleteSQL)
	SELECT 
		'UPDATE '+FullyQualifiedTableName+' SET IsDeleted=1, ModificationDate=GETDATE() FROM '+FullyQualifiedTableName+' INNER JOIN PurgeMaintenance.dimClientAccounts '+
		'ON PurgeMaintenance.dimClientAccounts.ClientAccountKey='+FullyQualifiedTableName+'.ClientAccountKey '+
		'WHERE DepositDateKey <= (YEAR(DATEADD(d,-PurgeMaintenance.dimClientAccounts.DataRetentionDays'+
		',GETDATE()))*10000 + MONTH(DATEADD(d,-PurgeMaintenance.dimClientAccounts.DataRetentionDays'+
		',GETDATE()))*100+DAY(DATEADD(d,-PurgeMaintenance.dimClientAccounts.DataRetentionDays'+
		',GETDATE()))) AND IsDeleted=0;'
	FROM
		@RecHubTables RecHubTables 
		INNER JOIN PurgeMaintenance.TableList ON PurgeMaintenance.TableList.TableName = RecHubTables.TableName
	WHERE
		PurgeMaintenance.TableList.SchemaName = 'RecHubData';

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
