--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GenerateDefaultConstraintSQL
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GenerateDefaultConstraintSQL') IS NOT NULL
	DROP PROCEDURE PurgeMaintenance.usp_GenerateDefaultConstraintSQL
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GenerateDefaultConstraintSQL 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Generate default constraint SQL scripts.
*
*
* Modification History
* 04/08/2015 WI 201466 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @TableList SharedSystem.ScriptTables;

BEGIN TRY

	INSERT INTO @TableList
	(
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName
	)
	SELECT 
		TableID,
		SchemaName,
		TableName,
		QualifiedSchemaName,
		QualifiedTableName,
		FullyQualifiedTableName 
	FROM 
		PurgeMaintenance.TableList;

	INSERT INTO PurgeMaintenance.DefaultConstraintSQL
	(
		TableID,
		DefaultConstraintName,
		CreateCommand,
		DropCommand
	)
	EXEC SharedSystem.usp_GenerateDefaultConstraintSQL
		@parmScriptTableList=@TableList;

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
