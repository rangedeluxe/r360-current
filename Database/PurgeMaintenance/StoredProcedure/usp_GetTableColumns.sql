--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorStoredProcedureName usp_GetTableColumns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('PurgeMaintenance.usp_GetTableColumns') IS NOT NULL
       DROP PROCEDURE PurgeMaintenance.usp_GetTableColumns
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE PurgeMaintenance.usp_GetTableColumns
(
	@parmSchemaName					SYSNAME,
	@parmTableName					SYSNAME,
	@parmIncludeIdentity			BIT,
	@parmIncludeQualifiedTableName	BIT,
	@parmColumnList					VARCHAR (MAX) OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Generate column list for a given table.
*
* Modification History
* 04/08/2015 WI 201480 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @ColumnList VARCHAR(MAX) = '';

BEGIN TRY

	SELECT
		@ColumnList = @ColumnList+
			CASE @parmIncludeQualifiedTableName
				WHEN 1 THEN QualifiedTableName+'.'
				ELSE ''
			END +
			'['+ColumnName+'],'
	FROM
		PurgeMaintenance.TableList
		INNER JOIN PurgeMaintenance.TableColumns ON PurgeMaintenance.TableColumns.TableID = PurgeMaintenance.TableList.TableID
	WHERE 
		PurgeMaintenance.TableList.SchemaName = @parmSchemaName
		AND PurgeMaintenance.TableList.TableName = @parmTableName
		AND PurgeMaintenance.TableColumns.IsIdentity = CASE @parmIncludeIdentity WHEN 0 THEN 0 ELSE PurgeMaintenance.TableColumns.IsIdentity END
	ORDER BY
		PurgeMaintenance.TableColumns.OrdinalPosition;
		
	SET @parmColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);

END TRY
BEGIN CATCH
	EXEC SharedCommon.usp_RethrowException;
END CATCH
