--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorTableName PurgeProcessorSQL3
--WFSScriptProcessorTableDrop
IF OBJECT_ID('PurgeMaintenance.PurgeProcessorSQL3') IS NOT NULL
	DROP TABLE PurgeMaintenance.PurgeProcessorSQL3
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2015
*
* Purpose: Store the SQL commands to process fact data.
*		   
*
* Modification History
* 04/07/2015 WI 201450 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE PurgeMaintenance.PurgeProcessorSQL3
(
	TableID	INT IDENTITY(1,1) NOT NULL,
	PurgeSQL VARCHAR(MAX) NOT NULL
);
--WFSScriptProcessorTableProperties
