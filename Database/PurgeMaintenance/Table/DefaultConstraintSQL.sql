--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorTableName DefaultConstraintSQL
--WFSScriptProcessorTableDrop
IF OBJECT_ID('PurgeMaintenance.DefaultConstraintSQL') IS NOT NULL
       DROP TABLE PurgeMaintenance.DefaultConstraintSQL
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Stores default constraint SQL scripts.
*		   
*
* Modification History
* 04/08/2015 WI 201443 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE PurgeMaintenance.DefaultConstraintSQL
(
	DefaultConstraintID		INT IDENTITY(1,1) NOT NULL,
	TableID					INT NOT NULL,
	DefaultConstraintName	NVARCHAR(128) NOT NULL,
	CreateCommand			VARCHAR(MAX) NOT NULL,
	DropCommand				VARCHAR(MAX) NOT NULL
);
--WFSScriptProcessorTableProperties
