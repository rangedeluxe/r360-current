--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorTableName TableColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('PurgeMaintenance.TableColumns') IS NOT NULL
	DROP TABLE PurgeMaintenance.TableColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/08/2015
*
* Purpose: Stores the table columns for RecHubPurge package. 
*
*
* Modification History
* 04/08/2015 WI 201453 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE PurgeMaintenance.TableColumns
(
	TableID				INT NOT NULL,
	ColumnName			SYSNAME NOT NULL,
	OrdinalPosition		INT NOT NULL,
	IsIdentity			BIT NOT NULL
);
--WFSScriptProcessorTableProperties