--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorTableName TableSQL
--WFSScriptProcessorTableDrop
IF OBJECT_ID('PurgeMaintenance.TableSQL') IS NOT NULL
	DROP TABLE PurgeMaintenance.TableSQL
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2015
*
* Purpose: Stores table create, drop, etc SQL scripts.
*		   
*
* Modification History
* 04/07/2015 WI 201456 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE PurgeMaintenance.TableSQL
(
	TableID							INT NOT NULL,
	SourceSchemaName				SYSNAME NOT NULL,
	SourceTableName					SYSNAME NOT NULL,
	TargetSchemaName				SYSNAME NOT NULL,
	TargetTableName					SYSNAME NOT NULL,
	FullyQualifiedSourceTableName	NVARCHAR(256) NOT NULL,
	FullyQualifiedTargetTableName	NVARCHAR(256) NOT NULL,
	CreateCommand					VARCHAR(MAX) NOT NULL,
	DropCommand						VARCHAR(MAX) NOT NULL,
	DropNewCommand					VARCHAR(MAX) NOT NULL,
	RenameCurrentToOldCommand		VARCHAR(MAX) NOT NULL,
	RenameNewToCurrentCommand		VARCHAR(MAX) NOT NULL
);
--WFSScriptProcessorTableProperties
