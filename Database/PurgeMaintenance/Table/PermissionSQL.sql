--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema PurgeMaintenance
--WFSScriptProcessorTableName PermissionSQL
--WFSScriptProcessorTableDrop
IF OBJECT_ID('PurgeMaintenance.PermissionSQL') IS NOT NULL
	DROP TABLE PurgeMaintenance.PermissionSQL
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/15/2016
*
* Purpose: Stores Permission SQL scripts.
*		   
*
* Modification History
* 03/15/2016 WI 201446 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE PurgeMaintenance.PermissionSQL
(
	PermissionID		INT IDENTITY(1,1) NOT NULL,
	TableID				INT NOT NULL,
	UserGroupName		NVARCHAR(128) NOT NULL,
	CreateCommand		VARCHAR(MAX) NOT NULL  
);
--WFSScriptProcessorTableProperties
