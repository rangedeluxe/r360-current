﻿
using System;
using Microsoft.AnalysisServices;
using NLog;
using ProcessSsasCubes.Helpers;

namespace ProcessSsasCubes
{
    public class ServerInfo
    {
        private static Logger _logger; 

        public ServerInfo(Logger logger)
        {
            _logger = logger;
        }
        
        public void DisplayInfo()
        {
            _logger.Debug("DisplayInfo:  Start Database Information Display");
            Server server = new Server();
            
            Connection connection = new Connection(_logger);
            var connectionString = connection.BuildConnectionString();
            if (string.IsNullOrEmpty(connectionString))
            {
                return;
            }

            try
            {
                server.Connect(connectionString);
                DisplayServerInfo(server);
            }
            catch (Exception ex)
            {
                _logger.Error(ex,ex.Message);
            }
            
        }
        private void DisplayServerInfo(Server server)
        {
            int index = 0;
            OutputLine(index, "Server Information");

            //Look for all databases
            if (server.Databases.Count > 0)
            {
                OutputLine(index, $"Found {server.Databases.Count} Databases");
                foreach (Database database in server.Databases)
                {
                    OutputLine(index, $"Database:  {database.Name}");
                    DatabaseInfo(database);
                }
            }
            else
            {
                OutputLine(index, $"No SSAS Databases Found on Server {server.Name}" );
            }
        }

        private void DatabaseInfo(Database database)
        {
            int tabs = 1;
            OutputLine(tabs, $"Last Processed:  {database.LastProcessed}");
            OutputLine(tabs, $"Dimension count:  {database.Dimensions.Count}");
            OutputLine(tabs, $"Last Updated:  {database.LastUpdate}");
            OutputLine(tabs, $"State:         {database.State}");

            if (database.Cubes.Count > 0)
            {
                OutputLine(tabs, $"Found {database.Cubes.Count} cubes");
                foreach (Cube cube in database.Cubes)
                {
                    CubeInfo(cube);
                }
            }
            else
            {
                OutputLine(tabs, $"Did not find any cubes associated with database '{database.Name}'");
            }

            if (database.Dimensions.Count > 0)
            {
                foreach (Dimension dimension in database.Dimensions)
                {
                    OutputLine(tabs, $"Demension ID={dimension.ID}");
                }
            }
            else
            {
                OutputLine(tabs,$"Did not find any dimensions associated with database '{database.Name}'");
            }
        }

        private void CubeInfo(Cube cube)
        {
            int tabs = 2;
            OutputLine(tabs, $"Cube            {cube.Name}");
            OutputLine(tabs, $"Last Processed: {cube.LastProcessed} ");
            OutputLine(tabs, $"Cube state      {cube.State}");
        }

        private void OutputLine(int tabs, string theData)
        {
            string preFix = "";
            string theBuffer = "    ";
            for (int i = 0; i < tabs; i++)
            {
                preFix = preFix + theBuffer;
            }
            _logger.Info(preFix + theData);
        }
    }
}
