﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NLog;

namespace ProcessSsasCubes.Helpers
{
    public class Connection
    {
        private static Logger _logger;

        public Connection(Logger logger)
        {
            _logger = logger;
        }
        public string BuildConnectionString()
        {
            string connectionString = null;

            var ssasServer = ConfigurationManager.AppSettings.Get("ssasserver");
            if (ssasServer == null)
            {
                _logger.Error("No ssas server found to process");
                return connectionString;
            }
            _logger.Info($"DisplayInfo:  processing server {ssasServer}");
            connectionString = "Data source=" + ssasServer + ";Timeout=7200000;Integrated Security=SSPI";

            return connectionString;
        }
    }
}
