﻿
using CommandLine;
using NLog;
using ProcessSsasCubes.CommandLineOptions;

namespace ProcessSsasCubes
{
    class Program
    {
        private static Logger _logger = LogManager.GetCurrentClassLogger();
        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<ProcessCubeOptions>(args)
                .WithParsed<ProcessCubeOptions>(opts => ProcessRequests(opts));
            
        }

        private static void ProcessRequests(ProcessCubeOptions options)
        {
            if (options.IsInfoRequested)
            {
                ServerInfo serverInfo = new ServerInfo(_logger);
                serverInfo.DisplayInfo();
            }

            if (options.DatabaseName == null)
            {
                _logger.Info("No database requested to process");
                return;
            }
            ProcessDatabase(options);
        }

        private static void ProcessDatabase(ProcessCubeOptions options)
        {
            if (options.CubeName == null)
            {
                _logger.Info($"all cubes in database {options.DatabaseName} will be processed.");
            }

        }
    }
}
