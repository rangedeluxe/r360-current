﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CommandLine;
using CommandLine.Text;

namespace ProcessSsasCubes.CommandLineOptions
{
    public class ProcessCubeOptions
    {
        [Option('i',"info", Default = false, HelpText = "Display server Info")]
        public bool IsInfoRequested { get; set; }
        [Option('d', "database", HelpText = "the database to be processed")]
        public string DatabaseName { get; set; }
        [Option('c', "cube", HelpText = "the cube to be processed")]
        public string CubeName { get; set; }
    }
}
