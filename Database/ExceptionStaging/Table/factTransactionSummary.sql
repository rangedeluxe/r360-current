--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorTable factTransactionSummary
--WFSScriptProcessorTableDrop
IF OBJECT_ID('ExceptionStaging.factTransactionSummary') IS NOT NULL
       DROP TABLE ExceptionStaging.factTransactionSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/05/2014
*
* Purpose: Copy of RecHubData.factBatchData minus FKs and indexes.
*		   
*
* Modification History
* 10/05/2014 WI 170059 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE ExceptionStaging.factTransactionSummary
(
	ExceptionBatchKey BIGINT NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	TransactionExceptionStatusKey TINYINT NOT NULL,
	BatchCueID int NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType tinyint NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	CheckCount INT NOT NULL,
	ScannedCheckCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	OMRCount INT NOT NULL,
	CheckTotal MONEY NOT NULL,
	StubTotal MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL
);
--WFSScriptProcessorTableProperties
