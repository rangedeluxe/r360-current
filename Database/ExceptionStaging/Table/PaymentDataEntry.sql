--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorTableName PaymentDataEntry
--WFSScriptProcessorTableDrop
IF OBJECT_ID('ExceptionStaging.PaymentDataEntry') IS NOT NULL
       DROP TABLE ExceptionStaging.PaymentDataEntry
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/04/2014
*
* Purpose: Workspace for the Upload Exception SSIS package.
*		   
*
* Modification History
* 10/04/2014 WI 170060 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE ExceptionStaging.PaymentDataEntry
(
	ExceptionBatchKey BIGINT NOT NULL,
	TransactionKey BIGINT NOT NULL,
	BatchID BIGINT NOT NULL,
	DepositDateKey INT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	FldName VARCHAR(128) NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT,
	DataEntryValueMoney MONEY,
	DataEntryValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
