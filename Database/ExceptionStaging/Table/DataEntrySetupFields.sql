--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorTableName DataEntrySetupFields
--WFSScriptProcessorTableDrop
IF OBJECT_ID('ExceptionStaging.DataEntrySetupFields') IS NOT NULL
       DROP TABLE ExceptionStaging.DataEntrySetupFields
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 12/02/2014
*
* Purpose: Workspace for the Exception SSIS package.
*		   
*
* Modification History
* 12/02/2014 WI 180978 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE ExceptionStaging.DataEntrySetupFields
(
	ClientAccountKey	INT NOT NULL,
	DataEntryColumnKey	BIGINT NOT NULL,
	TableType			TINYINT NOT NULL,
	FldName				VARCHAR(128) NOT NULL
);