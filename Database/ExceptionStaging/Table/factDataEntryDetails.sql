--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorTableName factDataEntryDetails
--WFSScriptProcessorTableDrop
IF OBJECT_ID('ExceptionStaging.factDataEntryDetails') IS NOT NULL
       DROP TABLE ExceptionStaging.factDataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/05/2014
*
* Purpose: Copy of RecHubData.factDataEntryDetails minus FKs and indexes.
*		   
*
* Modification History
* 10/05/2014 WI 170057 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE ExceptionStaging.factDataEntryDetails
(
	ExceptionBatchKey BIGINT NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	DepositStatus INT NOT NULL,
	DataEntryColumnKey int NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue VARCHAR(256) NOT NULL,
	FldName	VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
