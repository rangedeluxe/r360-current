--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorStoredProcedureName usp_TruncateWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('ExceptionStaging.usp_TruncateWorkTables') IS NOT NULL
       DROP PROCEDURE ExceptionStaging.usp_TruncateWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE ExceptionStaging.usp_TruncateWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/04/2014
*
* Purpose: Truncate tables used by SSIS for uploading exceptions.
*
* Modification History
* 10/04/2014 WI 170073 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	TRUNCATE TABLE ExceptionStaging.PendingExceptionBatches;
	TRUNCATE TABLE ExceptionStaging.ExceptionBatches;
	TRUNCATE TABLE ExceptionStaging.Transactions;
	TRUNCATE TABLE ExceptionStaging.Payments;
	TRUNCATE TABLE ExceptionStaging.PaymentDataEntry;
	TRUNCATE TABLE ExceptionStaging.Stubs;
	TRUNCATE TABLE ExceptionStaging.StubDataEntry;
	TRUNCATE TABLE ExceptionStaging.DataEntrySetupFields;

	TRUNCATE TABLE ExceptionStaging.factBatchSummary;
	TRUNCATE TABLE ExceptionStaging.factTransactionSummary;
	TRUNCATE TABLE ExceptionStaging.factChecks;
	TRUNCATE TABLE ExceptionStaging.factStubs;
	TRUNCATE TABLE ExceptionStaging.factDataEntryDetails;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
