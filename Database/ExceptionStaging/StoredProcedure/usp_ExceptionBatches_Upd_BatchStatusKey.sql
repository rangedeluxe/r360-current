--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorStoredProcedureName usp_ExceptionBatches_Upd_BatchStatusKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('ExceptionStaging.usp_ExceptionBatches_Upd_BatchStatusKey') IS NOT NULL
       DROP PROCEDURE ExceptionStaging.usp_ExceptionBatches_Upd_BatchStatusKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE ExceptionStaging.usp_ExceptionBatches_Upd_BatchStatusKey
(
	@parmPendingBatchStatusKey		TINYINT,
	@parmResolvedBatchStatusKey	TINYINT,
	@parmModificationDate			DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/06/2014
*
* Purpose: Update RecHubException.ExceptionBatches rows based from Exception 
*		Staging fact rows.
*
* NOTE: Aliases are used because the same table name is exposed in the join.
*
*
* Modification History
* 10/06/2014 WI 170067 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE
		H
	SET
		H.BatchStatusKey = @parmResolvedBatchStatusKey,
		H.ModificationDate = @parmModificationDate,
		H.ModifiedBy = SUSER_SNAME()
	FROM
		ExceptionStaging.ExceptionBatches S
		INNER JOIN RecHubException.ExceptionBatches H ON H.ExceptionBatchKey = S.ExceptionBatchKey
	WHERE
		H.BatchStatusKey = @parmPendingBatchStatusKey;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
