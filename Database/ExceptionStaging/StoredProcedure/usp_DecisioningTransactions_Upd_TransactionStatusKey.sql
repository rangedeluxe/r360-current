--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorStoredProcedureName usp_DecisioningTransactions_Upd_TransactionStatusKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('ExceptionStaging.usp_DecisioningTransactions_Upd_TransactionStatusKey') IS NOT NULL
       DROP PROCEDURE ExceptionStaging.usp_DecisioningTransactions_Upd_TransactionStatusKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE ExceptionStaging.usp_DecisioningTransactions_Upd_TransactionStatusKey
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/08/2014
*
* Purpose: Update RecHubException.DecisioningTransactions table.
*
* Modification History
* 10/08/2014 WI 170066 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

DECLARE @Complete SMALLINT;

BEGIN TRY
	SELECT 
		@Complete = TransactionStatusKey
	FROM
		RecHubException.TransactionStatuses
	WHERE
		TransactionStatusName = 'Complete'

	UPDATE
		RecHubException.DecisioningTransactions
	SET
		RecHubException.DecisioningTransactions.TransactionStatusKey = @Complete,
		RecHubException.DecisioningTransactions.ModificationDate = @parmModificationDate,
		RecHubException.DecisioningTransactions.ModifiedBy = SUSER_SNAME()
	FROM
		ExceptionStaging.Transactions
		INNER JOIN RecHubException.DecisioningTransactions ON RecHubException.DecisioningTransactions.TransactionKey = ExceptionStaging.Transactions.TransactionKey;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
