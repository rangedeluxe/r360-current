--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema ExceptionStaging
--WFSScriptProcessorStoredProcedureName usp_factStubs_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('ExceptionStaging.usp_factStubs_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE ExceptionStaging.usp_factStubs_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE ExceptionStaging.usp_factStubs_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/06/2014
*
* Purpose: Delete RecHubData fact rows based from Exception Staging rows.
*
* Modification History
* 10/06/2014 WI 170071 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		Data
	SET		
		Data.IsDeleted = 1,
		Data.ModificationDate = @parmModificationDate
	FROM	
		ExceptionStaging.factStubs Staging
		INNER JOIN RecHubData.factStubs Data ON Data.DepositDateKey = Staging.DepositDateKey
			AND Data.BatchID = Staging.BatchID
			AND Data.TransactionID = Staging.TransactionID
			AND Data.TxnSequence = Staging.TxnSequence
			AND Data.BatchSequence = Staging.BatchSequence
			AND Data.StubSequence = Staging.StubSequence
	WHERE
		Data.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
