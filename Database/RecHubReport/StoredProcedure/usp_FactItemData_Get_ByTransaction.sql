--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubReport
--WFSScriptProcessorStoredProcedureName usp_FactItemData_Get_ByTransaction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubReport.usp_FactItemData_Get_ByTransaction') IS NOT NULL
       DROP PROCEDURE RecHubReport.usp_FactItemData_Get_ByTransaction
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubReport.usp_FactItemData_Get_ByTransaction
(
	@parmSessionID				UNIQUEIDENTIFIER,	
	@parmSiteBankID				INT, 
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
	@parmTransactionID			INT
)

AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 09/15/2014
*
* Purpose: Retrieve the data for factItemData for a specified TransactionID
*
* Modification History
* 09/15/2014 WI 165982 SAS Created
* 04/10/2015 WI 201097 CMC Returning SourceBatchID
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey	INT;
DECLARE @EndDateKey		INT;

BEGIN TRY

	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

   SELECT
		RecHubData.factItemData.BatchSequence,
		RecHubData.dimItemDataSetupFields.Keyword,
		RecHubData.factItemData.DataValue,
		RecHubData.factItemData.SourceBatchID
	FROM 
		RecHubData.factItemData
		INNER JOIN RecHubData.dimItemDataSetupFields
			ON RecHubData.dimItemDataSetupFields.ItemDataSetupFieldKey = RecHubData.factItemData.ItemDataSetupFieldKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON	RecHubData.factItemData.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE
		RecHubUser.SessionClientAccountEntitlements.SessionID=@parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubData.factItemData.BatchID = @parmBatchID
		AND	RecHubData.factItemData.DepositDateKey = @StartDateKey
		AND RecHubData.factItemData.IsDeleted=0
		AND RecHubData.factItemData.TransactionID = @parmTransactionID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
