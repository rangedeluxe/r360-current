--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubReport
--WFSScriptProcessorStoredProcedureName usp_TranItemReport_GetCheckData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubReport.usp_TranItemReport_GetCheckData') IS NOT NULL
       DROP PROCEDURE RecHubReport.usp_TranItemReport_GetCheckData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubReport.usp_TranItemReport_GetCheckData
(
	@parmSessionID				UNIQUEIDENTIFIER,	
	@parmSiteBankID				INT,
	@parmSiteClientAccountID    INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				INT,
	@parmTransactionID			INT
)

AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 09/12/2014
*
* Purpose: Retrieve the data for document data for Report
*
* Modification History
* 09/12/2014 WI 165660 SAS Created
* 10/16/2014 WI 172843 SAS Changes done to give alias to column SiteClientAccountID
* 11/20/2014 WI 178988 SAS Changes done to add BatchSourceShortName
* 01/15/2015 WI 182247 TWE Check isdeleted (make sure the record has not been deleted)
******************************************************************************/
SET NOCOUNT ON;

DECLARE 
		@StartDateKey	INT,
		@EndDateKey		INT;

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID AS ClientAccountID,
		RecHubData.dimClientAccounts.OnlineColorMode,
		RecHubData.factChecks.ImmutableDateKey,
		RecHubData.factChecks.SourceProcessingDateKey,
		RecHubData.factChecks.DepositDateKey,
		RecHubData.factChecks.BatchID,
		RecHubData.factChecks.SourceBatchID,
		RecHubData.factChecks.TransactionID,
		RecHubData.factChecks.TxnSequence,
		RecHubData.factChecks.BatchSequence,
		'C' AS FileDescriptor,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.TransactionCode,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName
     FROM 	
		RecHubData.factChecks
		INNER JOIN RecHubData.dimClientAccounts
			ON  RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON	RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources
			ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factChecks.BatchSourceKey
		INNER JOIN RecHubData.dimImportTypes
			ON  RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey			
     WHERE
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
        AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
        AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID 
        AND RecHubData.factChecks.DepositDateKey = @StartDateKey
        AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.TransactionID = @parmTransactionID
		AND RecHubData.factChecks.IsDeleted = 0;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH