--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubReport
--WFSScriptProcessorStoredProcedureName usp_factBatchData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubReport.usp_factBatchData_Get') IS NOT NULL
       DROP PROCEDURE RecHubReport.usp_factBatchData_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubReport.usp_factBatchData_Get
(
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 09/15/2014
*
* Purpose: Retrieve the data for Batch for IMS report
*
* Modification History
* 09/15/2014 WI 165979 SAS Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDateKey	INT;				
DECLARE @EndDateKey		INT;

BEGIN TRY
		EXEC RecHubUser.usp_AdjustStartDateForViewingDays
			@parmSessionID = @parmSessionID,
			@parmSiteBankID = @parmSiteBankID,
			@parmSiteClientAccountID = @parmSiteClientAccountID,
			@parmDepositDateStart = @parmDepositDate,
			@parmStartDateKey = @StartDateKey OUT,
			@parmEndDateKey = @EndDateKey OUT;	
		
		SELECT
			RecHubData.dimBatchDataSetupFields.Keyword,
			RecHubData.factBatchData.DataValue
		FROM 
			RecHubData.factBatchData
			INNER JOIN RecHubData.dimBatchDataSetupFields 
				ON RecHubData.dimBatchDataSetupFields.BatchDataSetupFieldKey = RecHubData.factBatchData.BatchDataSetupFieldKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements 
				ON	RecHubData.factBatchData.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID=@parmSessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubData.factBatchData.BatchID=@parmBatchID
			AND RecHubData.factBatchData.IsDeleted=0
			AND RecHubData.factBatchData.DepositDateKey=@StartDateKey
		ORDER BY 
			RecHubData.dimBatchDataSetupFields.Keyword,
			RecHubData.factBatchData.DataValue;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


