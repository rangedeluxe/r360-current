--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubReport
--WFSScriptProcessorStoredProcedureName usp_TranItemReport_GetStubData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubReport.usp_TranItemReport_GetStubData') IS NOT NULL
       DROP PROCEDURE RecHubReport.usp_TranItemReport_GetStubData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubReport].[usp_TranItemReport_GetStubData]
(
	
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
    @parmTransactionID			INT
)

AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 09/12/2014
*
* Purpose: Retrieve the data for Stub data for IMS report
*
* Modification History
* 09/12/2014 WI 165674   SAS  Created
* 10/16/2014 WI 172842   SAS  Changes done to give alias to column SiteClientAccountID
* 11/20/2014 WI 178989   SAS  Changes done to add BatchSourceShortName
* 01/30/2015 WI 187395   CEJ  Modify RecHubReport.usp_TranItemReport_GetStubData to return the File Descriptor in the File Descriptor field
* 06/12/2015 WI 218383   MAA  Moved the check for deleted documents to the join to allow stubs to be returned when a document 
*                             in the transaction was deleted.
* 02/10/2015 WI 263035   LA   Updated Columns for new Workgroup DataEnrty table
* 06/08/2016 WI 285627   BLR  Fixed DocumentBatchSequence relationship.
* 08/24/2016 #127604101  JAW  Add HasDocument column
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey  INT;
DECLARE @EndDateKey    INT;

BEGIN TRY
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;


	IF EXISTS (
		SELECT TOP (1) 
			  1
		FROM
			RecHubData.factStubs
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON	RecHubData.factStubs.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.factStubs.DepositDateKey = @StartDateKey
			AND RecHubData.factStubs.BatchID = @parmBatchID
			AND RecHubData.factStubs.TransactionID = @parmTransactionID
			AND RecHubData.factStubs.IsDeleted = 0
	)
	BEGIN
		SELECT
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID AS ClientAccountID,
			RecHubData.dimClientAccounts.OnlineColorMode,
			RecHubData.factStubs.ImmutableDateKey,
			RecHubData.factStubs.SourceProcessingDateKey,
			RecHubData.factStubs.DepositDateKey,
			RecHubData.factStubs.BatchID,
			RecHubData.factStubs.SourceBatchID,
			RecHubData.factStubs.TransactionID,
			RecHubData.factStubs.TxnSequence,
			RecHubData.factStubs.BatchSequence,
			RecHubData.factStubs.DocumentBatchSequence,
			RecHubData.factStubs.Amount,
			RecHubData.factStubs.AccountNumber,
			CASE WHEN RecHubData.factDocuments.BatchSequence IS NOT NULL THEN 1 ELSE 0 END AS HasDocument,
			RecHubData.dimDocumentTypes.FileDescriptor as filedescriptor,
			RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName AS ImportTypeShortName
		FROM
			RecHubData.factStubs
			INNER JOIN RecHubData.dimClientAccounts
				ON  RecHubData.factStubs.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON	RecHubData.factStubs.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
			INNER JOIN RecHubData.dimBatchSources
				ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factStubs.BatchSourceKey
			INNER JOIN RecHubData.dimImportTypes
				ON  RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
			LEFT OUTER JOIN RecHubData.factDocuments
				ON	RecHubData.factDocuments.BatchID = RecHubData.factStubs.BatchID AND
					RecHubData.factDocuments.TransactionID= RecHubData.factStubs.TransactionID AND
					RecHubData.factDocuments.DepositDateKey=RecHubData.factStubs.DepositDateKey AND
					RecHubData.factDocuments.BatchSequence=RecHubData.factStubs.DocumentBatchSequence AND
					RecHubData.factDocuments.isdeleted = 0                --WI 218383
			LEFT OUTER JOIN RecHubData.dimDocumentTypes
				ON RecHubData.dimDocumentTypes.DocumentTypeKey=RecHubData.factDocuments.DocumentTypeKey
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.factStubs.DepositDateKey = @StartDateKey
			AND RecHubData.factStubs.BatchID = @parmBatchID
			AND RecHubData.factStubs.TransactionID = @parmTransactionID
			AND RecHubData.factStubs.IsDeleted = 0;
	END
	ELSE
	BEGIN
		SELECT
			DISTINCT RecHubData.factDataEntryDetails.BatchSequence
		FROM
			RecHubData.factDataEntryDetails
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns
				ON RecHubData.factDataEntryDetails.factDataEntryDetailKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON	RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID=@parmSessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.factDataEntryDetails.BatchID=@parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID=@parmTransactionID
			AND RecHubData.factDataEntryDetails.DepositDateKey=	@StartDateKey
			AND RecHubData.factDataEntryDetails.IsDeleted=0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0;
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
