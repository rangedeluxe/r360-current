--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubReport
--WFSScriptProcessorStoredProcedureName usp_IMSTranItemReport_GetDataEntryData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubReport.usp_IMSTranItemReport_GetDataEntryData') IS NOT NULL
       DROP PROCEDURE RecHubReport.usp_IMSTranItemReport_GetDataEntryData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubReport.usp_IMSTranItemReport_GetDataEntryData
(	
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
	@parmTransactionID			INT,
	@parmBatchSequence			INT = NULL
	
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 09/15/2014
*
* Purpose: Retrieve the data for document data for IMS report
*
* Modification History
* 09/15/2014 WI 165983 SAS	Created
* 07/22/2015 WI 224476 MGE	Updated for dimWorkgroupDataEntryColumns changes.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey	INT;
DECLARE @EndDateKey		INT;

BEGIN TRY
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 	
		RecHubData.factDataEntryDetails.BatchSequence,
		RecHubData.dimWorkgroupDataEntryColumns.UILabel AS Keyword,
		RecHubData.factDataEntryDetails.DataEntryValue AS DataValue,
		RecHubData.dimWorkgroupDataEntryColumns.MarkSense AS Marksense
	FROM
	 	RecHubData.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns
			ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE 	
		RecHubUser.SessionClientAccountEntitlements.SessionID=@parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubData.factDataEntryDetails.DepositDateKey=@StartDateKey
		AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
		AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
		AND RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RecHubData.factDataEntryDetails.BatchSequence = CASE WHEN @parmBatchSequence >= 0 THEN @parmBatchSequence ELSE RecHubData.factDataEntryDetails.BatchSequence END;			
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
