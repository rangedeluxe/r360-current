--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Upd_IsDeleted_ByWorkTableJoin
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_factNotifications_Upd_IsDeleted_ByWorkTableJoin') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_factNotifications_Upd_IsDeleted_ByWorkTableJoin
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_factNotifications_Upd_IsDeleted_ByWorkTableJoin
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/29/2012
*
* Purpose: Delete RecHub360 fact rows based from SSIS Staging fact rows.
*
* Modification History
* 08/29/2012 CR 55282 JPB	Created
* 06/20/2013 WI 92110 JBS	Update to 2.0 release. Change schema to FITStaging
*							Rename proc from usp_OLTAfactNotifications_DeleteByWorkTableJoin
* 11/26/2013 WI 123643 JBS	Adding SourceNotificationID to Join criteria for marking IsDeleted
* 03/05/2014 WI 131910 TWE  Modify delete logic
* 02/10/2015 WI 189067 TWE  Modify delete matching logic and to match on ID
* 07/14/2015 WI 223775 TWE  Modify logic to honor duplicate detect switch
* 02/06/2018 PT 154233497 JPB Removed OrganizationKey
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

DECLARE @FITDuplicateCheck AS INT = 0;

BEGIN TRY

    SELECT 
		@FITDuplicateCheck = Value
	FROM
		RecHubConfig.SystemSetup
	WHERE
		RecHubConfig.SystemSetup.Section = 'FIT' AND
		RecHubConfig.SystemSetup.SetupKey = 'FITDuplicateCheck'; 

    if  @FITDuplicateCheck = 1
	BEGIN;
		WITH locateKeys AS
		(
		-----------------------
		--  CASE 1
		--  Criteria for duplicate is as follows 
		--  SiteBankID
		--  SiteClientAccountID
		--  Notification Date (ignoring any time component)
		--  User File Name
		--  File Extension
		SELECT DISTINCT
			RecHubData.factNotificationFiles.NotificationDateKey
			,RecHubData.factNotificationFiles.NotificationMessageGroup
			,(SELECT RecHubData.dimBanks.SiteBankID 
			  FROM RecHubData.dimBanks 
			  WHERE RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey)                             AS BankID	
			,(SELECT RecHubData.dimClientAccounts.SiteClientAccountID 
			  FROM RecHubData.dimClientAccounts 
			  WHERE RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey)  AS SiteClientAccountID
		FROM	
			RecHubData.factNotificationFiles
			INNER JOIN FITStaging.factNotificationFiles AS NotificationFiles
				ON RecHubData.factNotificationFiles.NotificationDateKey = NotificationFiles.NotificationDateKey
					AND RecHubData.factNotificationFiles.UserFileName = NotificationFiles.UserFileName
					AND RecHubData.factNotificationFiles.FileExtension = NotificationFiles.FileExtension
		WHERE
			(SELECT RecHubData.dimBanks.SiteBankID FROM RecHubData.dimBanks WHERE RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey) = 
			(SELECT RecHubData.dimBanks.SiteBankID FROM RecHubData.dimBanks WHERE RecHubData.dimBanks.BankKey = NotificationFiles.BankKey)
			AND
			(SELECT RecHubData.dimClientAccounts.SiteClientAccountID FROM RecHubData.dimClientAccounts WHERE RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey) =
			(SELECT RecHubData.dimClientAccounts.SiteClientAccountID FROM RecHubData.dimClientAccounts WHERE RecHubData.dimClientAccounts.ClientAccountKey = NotificationFiles.ClientAccountKey)
		)
		UPDATE RecHubData.factNotifications
			SET	IsDeleted = 1,
				ModificationDate = GETDATE()
		FROM	
			RecHubData.factNotifications
			INNER JOIN locateKeys AS NotificationFiles
				ON	RecHubData.factNotifications.NotificationMessageGroup	= NotificationFiles.NotificationMessageGroup
		WHERE
			RecHubData.factNotifications.BankKey > 0     --Make sure alert notifications are not picked up	
		;
		-----------------------
		--  CASE 2
		-- they just submitted the same file load again.
		UPDATE      RecHubData.factNotifications
		SET         RecHubData.factNotifications.IsDeleted = 1,
					   RecHubData.factNotifications.ModificationDate = GETDATE()
		FROM   
			 RecHubData.factNotifications
			 INNER JOIN FITStaging.factNotifications AS Notifications
				 ON RecHubData.factNotifications.SourceNotificationID = Notifications.SourceNotificationID
		WHERE 
			RecHubData.factNotifications.SourceNotificationID != '00000000-0000-0000-0000-000000000000'
			AND RecHubData.factNotifications.BankKey > 0   --Make sure alert notifications are not picked up	
		;
	END;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH