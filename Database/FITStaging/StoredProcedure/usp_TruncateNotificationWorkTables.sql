--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateNotificationWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_TruncateNotificationWorkTables') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_TruncateNotificationWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_TruncateNotificationWorkTables
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Truncate tables used by SSIS for Notifications.
*
* Modification History
* 06/01/2012 CR 53551 JPB	Created
* 06/20/2013 WI 92115 JBS	Update to 2.0 release.  Change schema to FITStaging
* 02/06/2018 PT 154233497 JPB Removed deprecated tables.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON; 
BEGIN TRY
	TRUNCATE TABLE FITStaging.NotificationImportWork;
	TRUNCATE TABLE FITStaging.NotificationImportWorkResponses;
	TRUNCATE TABLE FITStaging.XMLNotification;
	TRUNCATE TABLE FITStaging.XMLNotifications;
	TRUNCATE TABLE FITStaging.XMLNotificationMessage;
	TRUNCATE TABLE FITStaging.XMLNotificationFile;
	TRUNCATE TABLE FITStaging.factNotifications;
	TRUNCATE TABLE FITStaging.factNotificationFiles;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
