--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_NotificationImportWork_SetXMLResponseDocument
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_NotificationImportWork_SetXMLResponseDocument') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_NotificationImportWork_SetXMLResponseDocument
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_NotificationImportWork_SetXMLResponseDocument
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/24/2012
*
* Purpose: 
*
* Modification History
* 08/24/2012 CR 55263 JPB	Created
* 06/20/2013 WI 92101 JBS	Update to 2.0. Change schema to FITStaging
* 05/28/2015 WI 215704 JPB	Updated to to use Notification_Id instead of NotificationTrackingID
*							(Duplicated detect work.)
* 02/06/2018 PT 154233497 JPB Removed OrganizationKey
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;

DECLARE @Notification_Id BIGINT,
		@Loop INT,
		@ResponseType TINYINT,
		@XMLResponse XML;

BEGIN TRY

	DECLARE @ResponseList TABLE 
	(
		RowID INT IDENTITY(1,1), 
		Notification_Id BIGINT,
		ResponseType TINYINT
	);

	/* Get a distinct list of batch tracking IDs from the response table */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT DISTINCT
		FITStaging.NotificationImportWorkResponses.Notification_Id,
		MIN(FITStaging.NotificationImportWorkResponses.ResponseStatus)
	FROM	
		FITStaging.NotificationImportWorkResponses
	GROUP BY 
		FITStaging.NotificationImportWorkResponses.Notification_Id;
		
	/* Update Work table with error if error row exists */
	UPDATE FITStaging.NotificationImportWork
	SET FITStaging.NotificationImportWork.ResponseStatus = ResponseType
	FROM	
		FITStaging.NotificationImportWork
		INNER JOIN @ResponseList RL 
			ON RL.Notification_Id = FITStaging.NotificationImportWork.Notification_Id;
	
	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get recods that exist in the response table but not in the work table */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT
		FITStaging.NotificationImportWorkResponses.Notification_Id,
		0
	FROM	
		FITStaging.NotificationImportWorkResponses
	EXCEPT 
	SELECT	
		FITStaging.NotificationImportWork.Notification_Id,
		0
	FROM	
		FITStaging.NotificationImportWork;

	UPDATE @ResponseList
	SET	ResponseType = FITStaging.NotificationImportWorkResponses.ResponseStatus
	FROM	
		@ResponseList RL
		INNER JOIN FITStaging.NotificationImportWorkResponses 
			ON FITStaging.NotificationImportWorkResponses.Notification_Id = RL.Notification_Id

	INSERT INTO FITStaging.NotificationImportWork
	(
		FITStaging.NotificationImportWork.NotificationTrackingID,
		FITStaging.NotificationImportWork.Notification_Id,
		FITStaging.NotificationImportWork.NotificationMessageGroup,
		FITStaging.NotificationImportWork.ResponseStatus,
		FITStaging.NotificationImportWork.BankKey,
		FITStaging.NotificationImportWork.ClientAccountKey,
		FITStaging.NotificationImportWork.NotificationDateKey,
		FITStaging.NotificationImportWork.NotificationSourceKey,
		FITStaging.NotificationImportWork.SourceNotificationID
	)
	SELECT	
		FITStaging.XMLNotification.NotificationTrackingID,
		FITStaging.XMLNotification.Notification_ID,
		0,
		RL.ResponseType, /*ResponseStatus*/
		0, /*BankKey*/
		0, /*ClientAccountKey*/
		0, /*NotificationDateKey*/
		0, /*NotificationSourceKey*/
		FITStaging.XMLNotification.SourceNotificationID
	FROM 
		FITStaging.XMLNotification
		INNER JOIN @ResponseList RL 
			ON RL.Notification_Id = FITStaging.XMLNotification.Notification_Id;

	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get all records that need to be processed */
	INSERT INTO @ResponseList(Notification_Id,ResponseType)
	SELECT	
		FITStaging.NotificationImportWork.Notification_Id,
		FITStaging.NotificationImportWork.ResponseStatus
	FROM	
		FITStaging.NotificationImportWork;
	
	SET @Loop = 1;

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ResponseList) )
	BEGIN

		SELECT	
			@Notification_Id = Notification_Id,
			@ResponseType = ResponseType
		FROM	
			@ResponseList 
		WHERE 
			RowID = @Loop;
		
		SELECT @XMLResponse = 
		(
			SELECT	TOP (1) NotificationTrackingID AS '@NotificationTrackingID',
					(
						SELECT CASE	FITStaging.NotificationImportWork.ResponseStatus
							WHEN 0 THEN 'Success'
							WHEN 1 THEN 'Fail' 
							WHEN 2 THEN 'Warning'
						END AS 'Results/*'
						FROM 
							FITStaging.NotificationImportWork
						WHERE 
							Notification_Id = @Notification_Id
						FOR XML PATH(''), TYPE
					),
					(
						SELECT	FITStaging.NotificationImportWorkResponses.ResultsMessage AS 'ErrorMessage/*',NULL
						FROM	
							FITStaging.NotificationImportWorkResponses
						WHERE	
							N1.Notification_Id = FITStaging.NotificationImportWorkResponses.Notification_Id
						FOR XML PATH(''), TYPE
					)
			FROM 
				FITStaging.NotificationImportWork N1
			WHERE 
				N1.Notification_Id = @Notification_Id
			FOR XML PATH('Notification'), TYPE
		);
				
		UPDATE RecHubSystem.DataImportQueue
		SET	
			RecHubSystem.DataImportQueue.XMLResponseDocument = @XMLResponse,
			RecHubSystem.DataImportQueue.QueueStatus = 99,
			RecHubSystem.DataImportQueue.ResponseStatus = @ResponseType,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_NAME()
		WHERE 
			RecHubSystem.DataImportQueue.DataImportQueueID = @Notification_Id;

		SET @Loop = @Loop + 1;	
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH