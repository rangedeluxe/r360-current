--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLNotification_Get_ClientAccountKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_XMLNotification_Get_ClientAccountKeys') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotification_Get_ClientAccountKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_XMLNotification_Get_ClientAccountKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/04/2012
*
* Purpose: Retrieve ClientAccount keys for notifications to be processed by the SSIS toolkit.
*
* Modification History
* 06/04/2012 CR 53550 JPB	Created
* 06/20/2013 WI 92100 JBS	Udpate to 2.0. Change schema to FITStaging
*							Rename from usp_Notification_GetLockboxKeys
*							Change all references of Lockbox to ClientAccount
*							Customer to Organization
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;

BEGIN TRY
	;WITH ClientAccountList AS
	(
		SELECT DISTINCT 
			FITStaging.XMLNotification.BankID,
			FITStaging.XMLNotification.ClientID
		FROM 
			FITStaging.XMLNotification
	)
	SELECT	
		RecHubData.dimClientAccounts.ClientAccountKey,
		RecHubData.dimClientAccounts.SiteClientAccountID AS SiteClientAccountID,
		RecHubData.dimClientAccounts.SiteBankID AS SiteBankID,
		RecHubData.dimClientAccounts.SiteOrganizationID AS SiteOrganizationID
	FROM	
		RecHubData.dimClientAccounts
		INNER JOIN ClientAccountList 
			ON RecHubData.dimClientAccounts.SiteBankID = ClientAccountList.BankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = ClientAccountList.ClientID
	WHERE	
		RecHubData.dimClientAccounts.MostRecent = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
