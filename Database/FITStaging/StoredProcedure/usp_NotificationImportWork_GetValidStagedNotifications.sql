--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_NotificationImportWork_GetValidStagedNotifications
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_NotificationImportWork_GetValidStagedNotifications') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_NotificationImportWork_GetValidStagedNotifications
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_NotificationImportWork_GetValidStagedNotifications
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2012
*
* Purpose: 
*
* Modification History
* 06/02/2017 PT 139495343 CEJ	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;
BEGIN TRY
	SELECT FITStaging.NotificationImportWork.NotificationTrackingID,
	       FITStaging.NotificationImportWork.Notification_Id,
	       FITStaging.NotificationImportWork.NotificationMessageGroup,
	       FITStaging.NotificationImportWork.ResponseStatus,
	       FITStaging.NotificationImportWork.BankKey,
	       FITStaging.NotificationImportWork.OrganizationKey,
	       FITStaging.NotificationImportWork.ClientAccountKey,
	       FITStaging.NotificationImportWork.NotificationDateKey,
	       FITStaging.NotificationImportWork.NotificationSourceKey,
	       FITStaging.NotificationImportWork.SourceNotificationID
	  FROM FITStaging.NotificationImportWork
		LEFT JOIN FITStaging.NotificationImportWorkResponses
			ON NotificationImportWork.Notification_Id = NotificationImportWorkResponses.Notification_Id
	  WHERE FITStaging.NotificationImportWorkResponses.ResponseStatus IS NULL;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH