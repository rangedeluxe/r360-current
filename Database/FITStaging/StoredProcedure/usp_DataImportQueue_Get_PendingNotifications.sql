--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_PendingNotifications
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_DataImportQueue_Get_PendingNotifications') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_DataImportQueue_Get_PendingNotifications
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_DataImportQueue_Get_PendingNotifications
(
	@parmPendingNotifications BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Retrieve notification data from OLTA.DataImportQueue that are ready for processing.
*
* Modification History
* 06/01/2012 CR 53546 JPB	Created
* 06/20/2013 WI 92086 JBS	Update to 2.0 release.  Change schema to FITStaging
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	INSERT INTO FITStaging.XMLNotifications(Notification_ID,xmlNotification,xmlString)
	SELECT	
		RecHubSystem.DataImportQueue.DataImportQueueID AS Notification_ID,
		N.Notification.query('.') AS xmlNotification,
		CAST(N.Notification.query('.') AS VARCHAR(MAX)) AS xmlString
	FROM
		RecHubSystem.DataImportQueue
		CROSS APPLY XMLDataDocument.nodes('/Notification') AS N(Notification)
	WHERE	
		RecHubSystem.DataImportQueue.QueueType = 2
		AND RecHubSystem.DataImportQueue.QueueStatus IN (10,20);
			
	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingNotifications = 1;
		UPDATE	
			RecHubSystem.DataImportQueue
		SET	
			RecHubSystem.DataImportQueue.QueueStatus = 20,
			RecHubSystem.DataImportQueue.ResponseStatus = 1,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_SNAME()
		FROM	
			RecHubSystem.DataImportQueue
			INNER JOIN FITStaging.XMLNotifications 
				ON FITStaging.XMLNotifications.Notification_ID = RecHubSystem.DataImportQueue.DataImportQueueID;
	END
	ELSE 
		SET @parmPendingNotifications = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH