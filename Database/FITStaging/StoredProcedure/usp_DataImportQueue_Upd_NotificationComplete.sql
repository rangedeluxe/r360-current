--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Upd_NotificationComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_DataImportQueue_Upd_NotificationComplete') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_DataImportQueue_Upd_NotificationComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_DataImportQueue_Upd_NotificationComplete
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/04/2012
*
* Purpose: Updating notifications in process to completed.
*
* Modification History
* 06/04/2012 CR 53547 JPB	Created
* 06/20/2013 WI 92089 JBS	Update to 2.0 release. Change schema to FITStaging
*							Rename from usp_DataImportQueue_UpdateNotificationComplete
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON ;

BEGIN TRY
	UPDATE 	RecHubSystem.DataImportQueue 
	SET 	
		RecHubSystem.DataImportQueue.QueueStatus = 99,
		RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
		RecHubSystem.DataImportQueue.ModifiedBY = SUSER_SNAME() 
	FROM
	 	RecHubSystem.DataImportQueue 
		INNER JOIN FITStaging.XMLNotification 
			ON RecHubSystem.DataImportQueue.EntityTrackingID = FITStaging.XMLNotification.NotificationTrackingID
	WHERE	
		RecHubSystem.DataImportQueue.QueueStatus = 20;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
