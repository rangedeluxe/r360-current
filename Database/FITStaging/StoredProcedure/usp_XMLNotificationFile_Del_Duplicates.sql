--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorStoredProcedureName usp_XMLNotificationFile_Del_Duplicates
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_XMLNotificationFile_Del_Duplicates') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotificationFile_Del_Duplicates
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_XMLNotificationFile_Del_Duplicates
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/18/2015
*
* Purpose: Remove notifications from work flow if the file is already in process.
*
* Modification History
* 05/18/2015 WI 213806 JPB	Created
* 07/07/2015 WI 221922 TWE  ADD duplicate check switch
* 06/07/2017 PT 139495343	MGE Fix bug found in regression-Notificatons w/o files get marked as deleted when run as same time as notifications with files
******************************************************************************/
SET NOCOUNT ON;

DECLARE @FITDuplicateCheck AS INT = 0;

BEGIN TRY

    SELECT 
		@FITDuplicateCheck = Value
	FROM
		RecHubConfig.SystemSetup
	WHERE
		RecHubConfig.SystemSetup.Section = 'FIT' AND
		RecHubConfig.SystemSetup.SetupKey = 'FITDuplicateCheck'; 

	IF  @FITDuplicateCheck = 1  AND	
	    EXISTS (SELECT FITStaging.XMLNotificationFile.Notification_ID FROM FITStaging.XMLNotificationFile)
	BEGIN;
		WITH FileList AS
		(
			SELECT DISTINCT
				MAX(FITStaging.XMLNotification.Notification_ID) AS Notification_ID
			FROM
				FITStaging.XMLNotification
				INNER JOIN FITStaging.XMLNotificationFile ON FITStaging.XMLNotificationFile.Notification_ID = FITStaging.XMLNotification.Notification_ID
			GROUP BY
				FITStaging.XMLNotification.BankID,
				FITStaging.XMLNotification.ClientID,
				CAST(CONVERT(VARCHAR,FITStaging.XMLNotification.NotificationDate,112) AS INT),
				FITStaging.XMLNotificationFile.UserFileName,
				FITStaging.XMLNotificationFile.FileExtension
		)
		INSERT INTO FITStaging.NotificationImportWorkResponses(Notification_Id,NotificationTrackingID,ResponseStatus,ResultsMessage)
		SELECT
			FITStaging.XMLNotification.Notification_Id,
			FITStaging.XMLNotification.NotificationTrackingID,
			1,
			'Duplicate Record In Process'
		FROM
			FITStaging.XMLNotification
			INNER JOIN FITStaging.XMLNotificationFile ON FITStaging.XMLNotificationFile.Notification_ID = FITStaging.XMLNotification.Notification_ID
		WHERE
			FITStaging.XMLNotification.Notification_ID NOT IN (SELECT Notification_ID FROM FileList);
	END;
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
