--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLNotification_Get_OrganizationKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('FITStaging.usp_XMLNotification_Get_OrganizationKeys') IS NOT NULL
       DROP PROCEDURE FITStaging.usp_XMLNotification_Get_OrganizationKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE FITStaging.usp_XMLNotification_Get_OrganizationKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/04/2012
*
* Purpose: Retrieve customer keys for notifications to be processed by the SSIS toolkit.
*
* Modification History
* 06/04/2012 CR 53549 JPB	Created
* 06/20/2013 WI 92099 JBS	Update to 2.0 release.  Change schema to FITStaging
*							Rename from usp_Notification_GetCustomerKeys
*							Change all references of Customer to Organization
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	;WITH BankList AS
	(
		SELECT DISTINCT FITStaging.XMLNotification.BankID
		FROM	FITStaging.XMLNotification
	)
	SELECT	
		RecHubData.dimOrganizations.OrganizationKey,
		RecHubData.dimOrganizations.SiteBankID			AS SiteBankID,
		RecHubData.dimOrganizations.SiteOrganizationID	AS SiteOrganizationID
	FROM	
		RecHubData.dimOrganizations
		INNER JOIN BankList 
				ON RecHubData.dimOrganizations.SiteBankID = BankList.BankID
	WHERE	
		RecHubData.dimOrganizations.MostRecent = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
