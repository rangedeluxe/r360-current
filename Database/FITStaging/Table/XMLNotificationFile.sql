--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName XMLNotificationFile
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.XMLNotificationFile') IS NOT NULL
       DROP TABLE FITStaging.XMLNotificationFile
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Workspace for the File Import Toolkit SSIS package.
*		   
*
* Modification History
* 06/01/2012 CR 53540		JPB	Created
* 06/11/2013 WI 92178		JBS	Update to 2.0 release. Change schema to FITStaging
* 05/22/2017 PT 139494807	MGE	Added file size column
**********************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.XMLNotificationFile
(
	Notification_ID		BIGINT NOT NULL,
	NotificationFile_ID BIGINT NOT NULL,
	FileType			VARCHAR(32) NOT NULL,
	FileIdentifier		UNIQUEIDENTIFIER NOT NULL,
	UserFileName		VARCHAR(255) NOT NULL,
	FileExtension		VARCHAR(24) NOT NULL,
	FileSize			BIGINT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.XMLNotification.IDX_XMLNotificationFile_NotificationNotificationFile_ID
CREATE CLUSTERED INDEX IDX_XMLNotificationFile_NotificationNotificationFile_ID ON FITStaging.XMLNotificationFile
(
	Notification_ID ASC,
	NotificationFile_ID ASC
);