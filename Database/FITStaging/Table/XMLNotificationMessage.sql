--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName XMLNotificationMessage
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.XMLNotificationMessage') IS NOT NULL
       DROP TABLE FITStaging.XMLNotificationMessage
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Workspace for the File Import Toolkit SSIS package.
*		   
*
* Modification History
* 06/01/2012 CR 53542 JPB	Created
* 06/12/2013 WI 92180 JBS	Update to 2.0 release.  Change schema to FITStaging
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.XMLNotificationMessage
(
	Notification_ID			BIGINT NOT NULL,
	NotificationMessagePart INT NOT NULL,
	MessageText				VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.XMLNotificationMessage.IDX_XMLNotificationMessage_Notification_ID
CREATE CLUSTERED INDEX IDX_XMLNotificationMessage_Notification_ID ON FITStaging.XMLNotificationMessage
(
	Notification_ID ASC
)