--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName factNotifications
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.factNotifications') IS NOT NULL
       DROP TABLE FITStaging.factNotifications
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Modification History
* 05/30/2012 CR 53535 JPB	Created
* 06/12/2013 WI 92076 JBS	Update to 2.0 release. Change schema to FITStaging.
*							Change references:  LockBox to ClientAccount,
*							Customer to Organization
* 03/02/2015 WI 193421 CMC	Changing NotificationSourceKey from tinyint to smallint.
* 02/06/2018 PT 154233497 JPB Added MessageWorkgroups, removed OrganizationKey
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate factNotifications
CREATE TABLE FITStaging.factNotifications
(
	NotificationTrackingID	UNIQUEIDENTIFIER NOT NULL,
	Notification_Id			BIGINT NOT NULL,
	NotificationMessageGroup BIGINT NOT NULL,
	BankKey					INT NOT NULL,
	ClientAccountKey		INT NOT NULL,
	NotificationDateKey		INT NOT NULL,
	NotificationFileCount	INT NOT NULL,
	NotificationMessagePart INT NOT NULL,
	NotificationSourceKey	SMALLINT NOT NULL,
	NotificationDateTime	DATETIME NOT NULL,
	SourceNotificationID	UNIQUEIDENTIFIER NOT NULL,
	MessageText				VARCHAR(128) NOT NULL,
	MessageWorkgroups		UNIQUEIDENTIFIER NULL
);
--WFSScriptProcessorTableProperties