--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName NotificationImportWorkResponses
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.NotificationImportWorkResponses') IS NOT NULL
       DROP TABLE FITStaging.NotificationImportWorkResponses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/04/2012
*
* Purpose: Workspace for the Notification Import Toolkit SSIS package.
*		   
*
* ResponseStatus:
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*	
*
* Modification History
* 06/04/2012 CR 53538 JPB	Created
* 06/11/2013 WI 92080 JBS	Update to 2.0 Release. Change schema to FITStaging
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.NotificationImportWorkResponses
(
	Notification_Id BIGINT NOT NULL,
	NotificationTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseStatus	TINYINT NOT NULL, 
	ResultsMessage	VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.NotificationResponses.IDX_NotificationImportWorkResponses_NotificationTrackingIDResponseStatus
CREATE INDEX IDX_NotificationImportWorkResponses_NotificationTrackingIDResponseStatus ON FITStaging.NotificationImportWorkResponses (NotificationTrackingID,ResponseStatus);
