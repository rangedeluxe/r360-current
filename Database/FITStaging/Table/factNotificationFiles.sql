--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName factNotificationFiles
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.factNotificationFiles') IS NOT NULL
       DROP TABLE FITStaging.factNotificationFiles
GO--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Modification History
* 05/30/2012 CR 53536		JPB	Created
* 06/12/2013 WI 92075		JBS	Update to 2.0 release. Change to Schema FITStaging
*							Change references: LockBox to ClientAccount
*							Customer to Organization
* 03/02/2015 WI 193422		CMC	Changing NotificationSourceKey from tinyint to smallint.
* 05/22/2017 PT 139494807	MGE	Added file size column
* 02/06/2018 PT 154233497 JPB Added MessageWorkgroups, removed OrganizationKey
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate factNotificationFiles
CREATE TABLE FITStaging.factNotificationFiles
(
	NotificationTrackingID	UNIQUEIDENTIFIER NOT NULL,
	Notification_Id			BIGINT NOT NULL,
	NotificationMessageGroup BIGINT NOT NULL,
	BankKey					INT NOT NULL,
	ClientAccountKey		INT NOT NULL,
	NotificationDateKey		INT NOT NULL,
	NotificationFileTypeKey INT NOT NULL, 
	NotificationSourceKey	SMALLINT NOT NULL,
	FileIdentifier			UNIQUEIDENTIFIER NOT NULL,
	SourceNotificationID	UNIQUEIDENTIFIER NOT NULL,
	UserFileName			VARCHAR(255) NOT NULL, 
	FileExtension			VARCHAR(24) NOT NULL,
	FileSize				BIGINT NOT NULL,
	MessageWorkgroups		UNIQUEIDENTIFIER NULL
);
--WFSScriptProcessorTableProperties
