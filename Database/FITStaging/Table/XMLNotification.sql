--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName XMLNotification
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.XMLNotification') IS NOT NULL
       DROP TABLE FITStaging.XMLNotification
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Workspace for the File Import Toolkit SSIS package.
*		   
*
* Modification History
* 06/01/2012 CR 53539 JPB	Created
* 06/11/2013 WI 92177 JBS	Update to 2.0 release. Change schema to FITStaging
* 03/02/2015 WI 193424	CMC	Changing NotificationSourceKey from tinyint to smallint.
* 02/05/2018 PT 154233497 JPB Added MessageWorkgroups
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.XMLNotification
(
	Notification_ID			BIGINT NOT NULL,
	NotificationTrackingID	UNIQUEIDENTIFIER NOT NULL,
	BankID					INT NOT NULL,
	ClientGroupID			INT NOT NULL,
	ClientID				INT NOT NULL,
	NotificationDate		DATETIME NOT NULL,
	NotificationSourceKey	SMALLINT NOT NULL,
	SourceNotificationID	UNIQUEIDENTIFIER NULL,
	MessageWorkgroups		UNIQUEIDENTIFIER NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.XMLNotification.IDX_XMLBatch_Notification_ID
CREATE CLUSTERED INDEX IDX_XMLBatch_Notification_ID ON FITStaging.XMLNotification
(
	Notification_ID ASC
);