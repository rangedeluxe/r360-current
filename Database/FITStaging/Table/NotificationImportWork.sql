--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName NotificationImportWork
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.NotificationImportWork') IS NOT NULL
       DROP TABLE FITStaging.NotificationImportWork
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 06/14/2012
*
* Purpose: Workspace for the File Import Toolkit SSIS package.
*		   
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*
* Modification History
* 06/14/2012 CR 53537 JPB	Created
* 06/11/2013 WI 92079 JBS	Update to 2.0 Release.  Change Schema to FITStaging.
*							Change references: Customer to Organization,
*							LockBox to ClientAccount
* 03/02/2015 WI 193423 CMC	Changing NotificationSourceKey from tinyint to smallint.
* 03/31/2015 WI 198845 JBS	Adding constraint DF_NotificationImportWork_NotificationMessageGroup
* 02/06/2018 PT 154233497 JPB Added MessageWorkgroups, removed OrganizationKey
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.NotificationImportWork
(
	NotificationTrackingID		UNIQUEIDENTIFIER NOT NULL,
	Notification_Id				BIGINT NOT NULL,
	NotificationMessageGroup	BIGINT NOT NULL
		CONSTRAINT DF_NotificationImportWork_NotificationMessageGroup DEFAULT (NEXT VALUE FOR RecHubSystem.NotificationMessageGroup),
	ResponseStatus				TINYINT NOT NULL
		CONSTRAINT DF_NotificationImportWork_ResponseStatus DEFAULT 255,
	BankKey						INT NOT NULL,
	ClientAccountKey			INT NOT NULL,
	NotificationDateKey			INT NOT NULL,
	NotificationSourceKey		SMALLINT NOT NULL,
	SourceNotificationID		UNIQUEIDENTIFIER NOT NULL,
	MessageWorkgroups			UNIQUEIDENTIFIER NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.NotificationImportWork.IDX_NotificationImportWork_NotificationDateBankOrganizationClientAccountKey
CREATE CLUSTERED INDEX IDX_NotificationImportWork_NotificationDateBankOrganizationClientAccountKey ON FITStaging.NotificationImportWork
(
	NotificationDateKey ASC,
	BankKey				ASC,
	ClientAccountKey 	ASC
);