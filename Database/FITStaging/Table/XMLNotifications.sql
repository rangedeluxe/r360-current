--WFSScriptProcessorSchema FITStaging
--WFSScriptProcessorTableName XMLNotifications
--WFSScriptProcessorTableDrop
IF OBJECT_ID('FITStaging.XMLNotifications') IS NOT NULL
       DROP TABLE FITStaging.XMLNotifications
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 06/01/2012 CR 53545 JPB	Created
* 06/11/2013 WI 92183 JBS	Update to 2.0 release. Change schema to FITStaging
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE FITStaging.XMLNotifications
(
	RowID			INT NOT NULL IDENTITY(1,1),
	Notification_ID BIGINT NOT NULL,
	xmlNotification XML NOT NULL,
	xmlString		VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex FITStaging.XMLNotifications.IDX_XMLNotifications_Notification_Id
CREATE CLUSTERED INDEX IDX_XMLNotifications_Notification_Id ON FITStaging.XMLNotifications
(
	Notification_ID ASC
);