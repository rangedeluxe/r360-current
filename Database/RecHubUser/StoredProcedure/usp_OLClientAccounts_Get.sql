--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLClientAccounts_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLClientAccounts_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLClientAccounts_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLClientAccounts_Get
(
	@parmOLWorkgroupID int
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Get Site info from client accounts table.
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 03/27/2013 WI 90708 JPB	Updated to 2.0.
*							Renamed and moved to RecHubUser schema.
*							Renamed @parmOLLockboxID to @parmOLClientAccountID.
* 08/20/2014 WI 160058 JMC	Modified to pull from OLWorkgroups
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT DISTINCT	RecHubUser.OLWorkgroups.SiteBankID			AS BankID,
			RecHubUser.OLWorkgroups.SiteClientAccountID AS ClientAccountID,
			RecHubUser.OLWorkgroups.OLWorkgroupID,
			RecHubData.dimClientAccountsView.DisplayLabel
	FROM	RecHubUser.OLWorkgroups 
		INNER JOIN RecHubData.dimClientAccountsView ON 
			RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
			AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
	WHERE	RecHubUser.OLWorkgroups.OLWorkgroupID = @parmOLWorkgroupID;	

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
