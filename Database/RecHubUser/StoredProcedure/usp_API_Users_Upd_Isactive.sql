--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_Users_Upd_Isactive
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_Users_Upd_Isactive') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Upd_Isactive
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_Users_Upd_Isactive 
(
    @parmUserID			INT,
	@parmIsActive		BIT,
    @parmOperator		VARCHAR(24) = NULL,
    @parmRowsReturned	INT OUTPUT,
    @parmErrorDescription VARCHAR(2000) OUTPUT,
    @parmSQLErrorNumber INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/22/2009
*
* Purpose: Set User to Active or Inactive using @parmIsactive
*		   
*
* Modification History
* 04/22/2009 CR 25817 JNE	Created
* 02/25/2010 CR 28906 JPB	@parmSQLErrorNumber was not set correctly for normal
*							course. (Always returned 50000.)
* 03/15/2013 WI 90573 JBS	Update to 2.0 release. Change schema name.
******************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

SET @parmRowsReturned = 0
SET @parmErrorDescription = ''
SET @parmSQLErrorNumber = 50000

DECLARE	@InsertUserID	INT = 0,
		@UserID			VARCHAR(10),
		@AuditMessage	VARCHAR(1024),
		@OldIsActive	BIT,
		@NewIsActive	VARCHAR(1)

SELECT @NewIsActive = CAST(@parmIsActive AS VARCHAR(1))

BEGIN TRANSACTION Users_SetIsActive;
BEGIN TRY
	IF @parmOperator IS NULL
	BEGIN
		SET @parmOperator = USER_NAME()
	END;

	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE RecHubUser.Users.UserID = @parmUserID)
	BEGIN
		SET @parmErrorDescription = 'Entry does not exist for UserID: '+ CAST(@parmUserID AS VARCHAR(72))
		RAISERROR(@parmErrorDescription,16,1)
	END;

	DECLARE @AuditInformation TABLE
	(
		OldIsActive	Bit NOT NULL,
		NewIsActive BIT NOT NULL
	)
	
	UPDATE RecHubUser.Users
	SET IsActive = @parmIsActive, ModifiedBy = @parmOperator, ModificationDate = GETDATE()
	OUTPUT
		deleted.IsActive,
		inserted.IsActive
	INTO
		@AuditInformation
	WHERE RecHubUser.Users.UserID=@parmUserID;

	SELECT @parmSQLErrorNumber = @@ERROR, @parmRowsReturned = @@ROWCOUNT;

	/* Insert Audit Records */
	SELECT @OldIsActive = (SELECT OldIsActive FROM @AuditInformation)
	SELECT 
		@InsertUserID = UserID
	FROM
		RecHubUser.Users
	WHERE LogonName = @parmOperator;

	SELECT @AuditMessage = 'Modified the IsActive bit for User ID: ' + CAST(@ParmUserID AS VARCHAR(10)) + 
		' FROM ' + CAST(@OldIsActive AS VARCHAR(1)) + ' TO ' + @NewIsActive + '.'
	SELECT @UserID = CAST(@parmUserID AS VARCHAR(10))

	EXEC RecHubCommon.usp_WFS_DataAudit_Ins
		@parmUserID					= @InsertUserID,
		@parmApplicationName		= 'RecHubUser.usp_API_Users_Upd_IsActive',
		@parmSchemaName				= 'RecHubUser',
		@parmTableName				= 'Users',
		@parmColumnName				= 'IsActive',
		@parmAuditType				= 'UPD',
		@parmAuditValue				= @NewIsActive,
		@parmAuditMessage			= @AuditMessage;

	COMMIT TRANSACTION Users_SetIsActive;
	
	RETURN 0
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION Users_SetIsActive;
	SELECT @parmSQLErrorNumber = @@ERROR;
	IF @parmErrorDescription = ''
		SET @parmErrorDescription = 'An error occured in the execution of usp_API_Users_Upd_Isactive';
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 1
END CATCH
GO







