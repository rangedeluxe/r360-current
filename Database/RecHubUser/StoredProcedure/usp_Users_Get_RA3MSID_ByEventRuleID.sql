--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_Get_RA3MSID_ByEventRuleID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_RA3MSID_ByEventRuleID') IS NOT NULL
	   DROP PROCEDURE RecHubUser.usp_Users_Get_RA3MSID_ByEventRuleID;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_RA3MSID_ByEventRuleID
(
	@parmEventRuleID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/27/2014
*
* Purpose: Get RA3M SIDs for Users tied to an Event Rule
*
* Modification History
* 06/27/2014 WI 150802 EAS	Created
* 02/11/2015 WI 189420 CMC	Filter by active alert records
* 3/13/2015  WI 195842 MAA  Allow selecting inactive record users and filter out repeats
* 05/27/2015 WI 215500 CMC	Filter by IsAssigned
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT     DISTINCT RecHubUser.Users.RA3MSID  
	FROM       RecHubUser.Users
	INNER JOIN RecHubAlert.Alerts ON (RecHubAlert.Alerts.UserID = RecHubUser.Users.UserID)
	WHERE      RecHubAlert.Alerts.EventRuleID = @parmEventRuleID
	  AND	   RecHubAlert.Alerts.IsAssigned = 1;	

END TRY

BEGIN CATCH	
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH