--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_PredefSearch_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_PredefSearch_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_PredefSearch_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_PredefSearch_Del
(
	@parmPredefSearchID UNIQUEIDENTIFIER, 
	@parmRowsReturned	INT OUTPUT 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/10/2011
*
* Purpose: Delete row in PreDefSearch table for given PreDefSearchID
*
* Modification History
* 01/12/2011 CR 31743 JNE	Created
* 04/03/2013 WI 90777 JBS	Update to 2.0 release. Change Schema name
******************************************************************************/
SET ARITHABORT ON;
SET XACT_ABORT ON;

SET @parmRowsReturned = 0;

BEGIN TRY
	DELETE 
	FROM	RecHubUser.PredefSearch
	WHERE 	RecHubUser.PredefSearch.PreDefSearchID = @parmPredefSearchID;

	SELECT @parmRowsReturned = @@ROWCOUNT;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
