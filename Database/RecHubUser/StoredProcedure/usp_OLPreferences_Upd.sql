--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Upd') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_OLPreferences_Upd;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Upd 
( 
	@parmOLPreferenceID UNIQUEIDENTIFIER,
	@parmDefaultSetting VARCHAR(255),
	@parmIsOnLine		BIT,
	@parmUserID         INT,
	@parmRowsReturned	INT = 0 OUTPUT  
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/13/2012
*
* Purpose: Update OL Preference record
*
* Modification History
* 12/13/2012 WI 76547 CRG Write store procedure for cPreferencesDAL.UpdateOLPreferences(Guid, string, bool, out int)
* 02/22/2013 WI 88853 CRG Change Schema for RecHubUser.usp_OLPreferences_Upd 
* 07/30/2014 WI 156370 EAS Audit Changes
* 05/26/2015 WI 215222 BLR Changed the audit message a bit to use the name, not the guid.
* 08/27/2015 WI 230368 BLR Changed the audit message a bit more, using more verbose messages.
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	DECLARE @auditColumns		RecHubCommon.AuditValueChangeTable,
			@auditMessage       VARCHAR(1024),
			@prevDefaultSetting VARCHAR(255),
	        @prevIsOnline       BIT,
			@prefDesc			VARCHAR(1024),
			@prefFullDesc		VARCHAR(255)

	UPDATE	
		RecHubUser.OLPreferences
	SET
		@prevDefaultSetting = RecHubUser.OLPreferences.DefaultSetting,
		@prevIsOnline = RecHubUser.OLPreferences.IsOnline,
		RecHubUser.OLPreferences.DefaultSetting	= @parmDefaultSetting,
		RecHubUser.OLPreferences.IsOnline		= @parmIsOnLine
    WHERE	
		RecHubUser.OLPreferences.OLPreferenceID	= @parmOLPreferenceID;

	SELECT
		@prefDesc = RecHubUser.OLPreferences.PreferenceName,
		@prefFullDesc = RecHubUser.OLPreferences.Description
	FROM
		RecHubUser.OLPreferences
	WHERE
		RecHubUser.OLPreferences.OLPreferenceID	= @parmOLPreferenceID;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;

	IF @prevDefaultSetting <> @parmDefaultSetting
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES(@prefDesc, CAST(@prevDefaultSetting AS VARCHAR), CAST(@parmDefaultSetting AS VARCHAR));

		IF @prevIsOnline <> @parmIsOnLine
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsOnline', CAST(@prevIsOnline AS VARCHAR), CAST(@parmIsOnLine AS VARCHAR));

		SET @auditMessage = 'Modified Admin System Settings: ' + @prefFullDesc + '. Value: ' + CAST(@parmDefaultSetting AS VARCHAR);
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLPreferences_Upd',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'System Settings',
			@parmColumnName			= 'OLPreferenceID',
			@parmAuditValue			= @parmOLPreferenceID,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;

	
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
