--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLWorkgroups_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLWorkgroups_Merge') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLWorkgroups_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLWorkgroups_Merge
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmEntityID				INT,
	@parmViewingDays			INT,
	@parmMaximumSearchDays		INT,
	@parmCheckImageDisplayMode	TINYINT,
	@parmDocumentImageDIsplayMode	TINYINT,
	@parmHOA					BIT,
	@parmDisplayBatchID			BIT,
	@parmInvoiceBalancingOption	TINYINT,
	@parmUserID					INT,
	@parmBillingAccount			VARCHAR(20) = NULL,
	@parmBillingField1			VARCHAR(20) = NULL,
	@parmBillingField2			VARCHAR(20) = NULL,
	@parmEntityName				VARCHAR(50) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/12/2014
*
* Purpose: Update or Insert into RecHubUser.OLWorkgroups
*
* Modification History
* 06/12/2014 WI 147418 KLC	Created
* 10/08/2014 WI 147418 KLC	Updated to support null display batch id
* 01/20/2015 WI 185215 RDS	Updated to include billing fields
* 08/31/2015 WI 233106 MAA  Display Image modes as text in audit
* 09/01/2015 WI 233201 MAA  Added @parmEntityName to be displayed in audit
******************************************************************************/
DECLARE @auditMessage					VARCHAR(1024),
		@auditType						VARCHAR(3),
		@AuditColumns					RecHubCommon.AuditValueChangeTable,
		@olWorkgroupID					INT,
		@prevViewingDays				INT,
		@prevMaximumSearchDays			INT,
		@prevCheckImageDisplayMode		TINYINT,
		@prevDocumentImageDisplayMode	TINYINT,
		@prevHOA						BIT,
		@prevDisplayBatchID				BIT,
		@prevInvoiceBalancingOption		TINYINT,
		@prevBillingAccount				VARCHAR(20),
		@prevBillingField1				VARCHAR(20),
		@prevBillingField2				VARCHAR(20),
		@checkImageDisplayModeMessage	varchar(30),
		@prevCheckImageDisplayModeMessage	varchar(30),
		@documentImageDisplayModeMessage	varchar(30),
		@prevDocumentImageDisplayModeMessage	varchar(30);
DECLARE @auditOLWorkgroup	TABLE(OLWorkgroupID INT, ViewingDays INT, MaximumSearchDays INT, CheckImageDisplayMode TINYINT, DocumentImageDisplayMode TINYINT,
									HOA BIT, DisplayBatchID BIT, InvoiceBalancingOption TINYINT,
									BillingAccount VARCHAR(20), BillingField1 VARCHAR(20), BillingField2 VARCHAR(20),
									AuditType VARCHAR(3));

SET NOCOUNT ON; 

DECLARE @LocalTransaction bit = 0;
DECLARE @errorDescription VARCHAR(1024);

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	--Verify the user exists
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert//update RecHubUser.OLWorkgroups record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	DECLARE @curTime datetime = GETDATE();

	MERGE RecHubUser.OLWorkgroups
	USING
	(
		VALUES (@parmSiteBankID, @parmSiteClientAccountID)
	) S (SiteBankID, SiteClientAccountID)
	ON RecHubUser.OLWorkgroups.SiteBankID = S.SiteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = S.SiteClientAccountID
	WHEN MATCHED
		AND (ISNULL(RecHubUser.OLWorkgroups.ViewingDays, -1) <> ISNULL(@parmViewingDays, -1)
			OR ISNULL(RecHubUser.OLWorkgroups.MaximumSearchDays, -1) <> ISNULL(@parmMaximumSearchDays, -1)
			OR RecHubUser.OLWorkgroups.CheckImageDisplayMode <> @parmCheckImageDisplayMode
			OR RecHubUser.OLWorkgroups.DocumentImageDisplayMode <> @parmDocumentImageDIsplayMode
			OR RecHubUser.OLWorkgroups.HOA <> @parmHOA
			OR ISNULL(CAST(RecHubUser.OLWorkgroups.DisplayBatchID AS SMALLINT), -1) <> ISNULL(CAST(@parmDisplayBatchID AS SMALLINT), -1)
			OR RecHubUser.OLWorkgroups.InvoiceBalancingOption <> @parmInvoiceBalancingOption
			OR ISNULL(RecHubUser.OLWorkgroups.BillingAccount, '') <> ISNULL(@parmBillingAccount, '')
			OR ISNULL(RecHubUser.OLWorkgroups.BillingField1, '') <> ISNULL(@parmBillingField1, '')
			OR ISNULL(RecHubUser.OLWorkgroups.BillingField2, '') <> ISNULL(@parmBillingField2, '')
			)THEN
		UPDATE SET ViewingDays = @parmViewingDays, MaximumSearchDays = @parmMaximumSearchDays,
					CheckImageDisplayMode = @parmCheckImageDisplayMode, DocumentImageDisplayMode = @parmDocumentImageDIsplayMode, 
					HOA = @parmHOA, DisplayBatchID = @parmDisplayBatchID, InvoiceBalancingOption = @parmInvoiceBalancingOption,
					BillingAccount = @parmBillingAccount, BillingField1 = @parmBillingField1, BillingField2 = @parmBillingField2
	WHEN NOT MATCHED THEN
		INSERT (EntityID, SiteBankID, SiteClientAccountID, ViewingDays, MaximumSearchDays,
				CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancingOption,
				BillingAccount, BillingField1, BillingField2,
				CreationDate, CreatedBy, ModificationDate, ModifiedBy)
		VALUES (@parmEntityID, @parmSiteBankID, @parmSiteClientAccountID, @parmViewingDays, @parmMaximumSearchDays,
				@parmCheckImageDisplayMode, @parmDocumentImageDIsplayMode, @parmHOA, @parmDisplayBatchID, @parmInvoiceBalancingOption,
				@parmBillingAccount, @parmBillingField1, @parmBillingField2,
				@curTime, SUSER_NAME(), @curTime, SUSER_NAME())
	OUTPUT INSERTED.OLWorkgroupID, DELETED.ViewingDays, DELETED.MaximumSearchDays, DELETED.CheckImageDisplayMode, DELETED.DocumentImageDisplayMode,
			DELETED.HOA, DELETED.DisplayBatchID, DELETED.InvoiceBalancingOption, DELETED.BillingAccount, DELETED.BillingField1, DELETED.BillingField2,
			CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END
	INTO @auditOLWorkgroup;

	--now select out and audit the changes
	SELECT	@auditType = AuditType,
			@olWorkgroupID = OLWorkgroupID,
			@prevViewingDays = ViewingDays,
			@prevMaximumSearchDays = MaximumSearchDays,
			@prevCheckImageDisplayMode = CheckImageDisplayMode,
			@prevDocumentImageDisplayMode = DocumentImageDisplayMode,
			@prevHOA = HOA,
			@prevDisplayBatchID = DisplayBatchID,
			@prevInvoiceBalancingOption = InvoiceBalancingOption,
			@prevBillingAccount = BillingAccount,
			@prevBillingField1 = BillingField1,
			@prevBillingField2 = BillingField2
	FROM @auditOLWorkgroup;

	IF @parmCheckImageDisplayMode = 0 
		SET @checkImageDisplayModeMessage = 'Use Inherited Setting'
	IF @parmCheckImageDisplayMode = 1
		SET @checkImageDisplayModeMessage = 'Use Front and Back'
	IF @parmCheckImageDisplayMode = 2 
		SET @checkImageDisplayModeMessage = 'Use Front only'
	--		
	IF @parmDocumentImageDisplayMode = 0 
		SET @documentImageDisplayModeMessage = 'Use Inherited Setting'
	IF @parmDocumentImageDisplayMode = 1
		SET @documentImageDisplayModeMessage = 'Use Front and Back'
	IF @parmDocumentImageDisplayMode = 2 
		SET @documentImageDisplayModeMessage = 'Use Front only'
				
	IF @auditType = 'UPD'
	BEGIN
		IF ISNULL(@prevViewingDays, -1) <> ISNULL(@parmViewingDays, -1)
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ViewingDays', ISNULL(CAST(@prevViewingDays AS VARCHAR(10)), 'NULL'), ISNULL(CAST(@parmViewingDays AS VARCHAR(10)), 'NULL'));
		IF ISNULL(@prevMaximumSearchDays, -1) <> ISNULL(@parmMaximumSearchDays, -1)
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('MaximumSearchDays', ISNULL(CAST(@prevMaximumSearchDays AS VARCHAR(10)), 'NULL'), ISNULL(CAST(@parmMaximumSearchDays AS VARCHAR(10)), 'NULL'));
		IF ISNULL(@prevCheckImageDisplayMode, -1) <> ISNULL(@parmCheckImageDisplayMode, -1)
		BEGIN
			IF @prevCheckImageDisplayMode = 0
				SET @prevCheckImageDisplayModeMessage = 'Use Inherited Setting'
			IF @prevCheckImageDisplayMode = 1
				SET @prevCheckImageDisplayModeMessage  = 'Use Front and Back'
			IF @prevCheckImageDisplayMode = 2
				SET @prevCheckImageDisplayModeMessage  = 'Use Front only'		
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('CheckImageDisplayMode', ISNULL(@prevCheckImageDisplayModeMessage, 'NULL'), ISNULL(@checkImageDisplayModeMessage, 'NULL'));
		END
		IF ISNULL(@prevDocumentImageDisplayMode, -1) <> ISNULL(@parmDocumentImageDisplayMode, -1)
		BEGIN
			IF @prevDocumentImageDisplayMode = 0
				SET @prevDocumentImageDisplayModeMessage = 'Use Inherited Setting'
			IF @prevDocumentImageDisplayMode = 1
				SET @prevDocumentImageDisplayModeMessage = 'Use Front and Back'
			IF @prevDocumentImageDisplayMode = 2
				SET @prevDocumentImageDisplayModeMessage = 'Use Front only'		
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DocumentImageDisplayMode', ISNULL(@prevDocumentImageDisplayModeMessage, 'NULL'), ISNULL(@documentImageDisplayModeMessage, 'NULL'));
		END		
		IF @prevHOA <> @parmHOA
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('HOA', CAST(@prevHOA AS VARCHAR(1)), CAST(@parmHOA AS VARCHAR(1)));
		IF ISNULL(CAST(@prevDisplayBatchID AS SMALLINT), -1) <> ISNULL(CAST(@parmDisplayBatchID AS SMALLINT), -1)
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DisplayBatchID', ISNULL(CAST(@prevDisplayBatchID AS VARCHAR(4)), 'NULL'), ISNULL(CAST(@parmDisplayBatchID AS VARCHAR(4)), 'NULL'));
		IF @prevInvoiceBalancingOption <> @parmInvoiceBalancingOption
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('InvoiceBalancingOption', CAST(@prevInvoiceBalancingOption AS VARCHAR(3)), CAST(@parmInvoiceBalancingOption AS VARCHAR(3)));
		IF ISNULL(@prevBillingAccount, '') <> ISNULL(@parmBillingAccount, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingAccount', ISNULL(@prevBillingAccount, 'NULL'), ISNULL(@parmBillingAccount, 'NULL'));
		IF ISNULL(@prevBillingField1, '') <> ISNULL(@parmBillingField1, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingField1', ISNULL(@prevBillingField1, 'NULL'), ISNULL(@parmBillingField1, 'NULL'));
		IF ISNULL(@prevBillingField2, '') <> ISNULL(@parmBillingField2, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingField2', ISNULL(@prevBillingField2, 'NULL'), ISNULL(@parmBillingField2, 'NULL'));

		-- Audit the RecHubUser.OLWorkgroups update
		SET @auditMessage = 'Modified OLWorkgroups for EntityID: ' + ISNULL(@parmEntityName, 'NULL')
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ' OLWorkgroupID: ' + CAST(@olWorkgroupID AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLWorkgroups_Merge',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'OLWorkgroups',
			@parmColumnName			= 'OLWorkgroupID',
			@parmAuditValue			= @olWorkgroupID,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END
	ELSE IF @auditType = 'INS'
	BEGIN
		-- Audit the RecHubUser.OLWorkgroups insert
		SET @auditMessage = 'Workgroup Maintenance: Added OLWorkgroups for Entity: ' + ISNULL(@parmEntityName, 'NULL')
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ' OLWorkgroupID: ' + CAST(@olWorkgroupID AS VARCHAR(10))
			+ ': ViewingDays = ' + ISNULL(CAST(@parmViewingDays AS VARCHAR(10)), 'NULL')
			+ ', MaximumSearchDays = ' + ISNULL(CAST(@parmMaximumSearchDays AS VARCHAR(10)), 'NULL')
			+ ', CheckImageDisplayMode = ' + ISNULL(@checkImageDisplayModeMessage, 'NULL')
			+ ', DocumentImageDisplayMode = ' + ISNULL(@documentImageDisplayModeMessage, 'NULL')
			+ ', HOA = ' + CAST(@parmHOA AS VARCHAR(1))
			+ ', DisplayBatchID = ' + ISNULL(CAST(@parmDisplayBatchID AS VARCHAR(4)), 'NULL')
			+ ', InvoiceBalancingOption = ' + CAST(@parmInvoiceBalancingOption AS VARCHAR(3))
			+ ', BillingAccount = ' + ISNULL(@parmBillingAccount, 'NULL')
			+ ', BillingField1 = ' + ISNULL(@parmBillingField1, 'NULL')
			+ ', BillingField2 = ' + ISNULL(@parmBillingField2, 'NULL')
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLWorkgroups_Merge',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'OLWorkgroups',
			@parmColumnName			= 'OLWorkgroupID',
			@parmAuditValue			= @olWorkgroupID,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;
	END

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

