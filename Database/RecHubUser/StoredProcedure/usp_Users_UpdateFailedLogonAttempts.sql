--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_UpdateFailedLogonAttempts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_UpdateFailedLogonAttempts') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_UpdateFailedLogonAttempts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_UpdateFailedLogonAttempts
(
	@parmUserID					INT,
	@parmFailedLogonAttempts	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Update the failed logon attempts column in the RecHubUser.Users table.
*
* Modification History
* 01/15/2010 CR 28680 JMC	Created
* 03/14/2013 WI 90812 CRG	Update Stored Procedure OLTA.usp_Users_UpdateFailedLogonAttempts for 2.0
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	UPDATE	
		RecHubUser.Users
	SET		
		RecHubUser.Users.FailedLogonAttempts = @parmFailedLogonAttempts
	WHERE	
		RecHubUser.Users.UserID = @parmUserID;
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

