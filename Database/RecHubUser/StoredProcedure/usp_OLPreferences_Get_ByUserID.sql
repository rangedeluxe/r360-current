--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get_ByUserID') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLPreferences_Get_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get_ByUserID 
(
	@parmUserID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/13/2012
*
* Purpose: Query OLPreferences for Preference Name by UserID
*
* Modification History
* 12/13/2012 WI 76544 CRG Write store procedure for cPreferencesDAL.GetOLPreferences(int, out DataTable)
* 02/22/2013 WI 88822 CRG Change Schema for RecHubUser.usp_OLPreferences_Get_ByUserID
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		RecHubUser.OLPreferences.PreferenceName,
        RecHubUser.OLPreferences.IsOnline,
        ISNULL(RecHubUser.OLPreferences.DefaultSetting, '') AS DefaultSetting,
        ISNULL(RecHubUser.OLUserPreferences.UserSetting, '') AS UserSetting
	FROM	
		RecHubUser.OLPreferences
		LEFT OUTER JOIN RecHubUser.OLUserPreferences 
			ON RecHubUser.OLUserPreferences.OLPreferenceID = RecHubUser.OLPreferences.OLPreferenceID
			AND	RecHubUser.OLUserPreferences.UserID = @parmUserID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;

