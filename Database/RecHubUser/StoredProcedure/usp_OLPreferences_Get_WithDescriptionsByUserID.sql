--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get_WithDescriptionsByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get_WithDescriptionsByUserID') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLPreferences_Get_WithDescriptionsByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get_WithDescriptionsByUserID 
(
	@parmUserID INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences Descriptions for Preference By User ID
*
* Modification History
* 12/12/2012 WI 76546	CRG Write store procedure for SQLPreferences.GetOLPreferencesWithDescriptions(int, out DataTable)
* 02/22/2013 WI 88848	CRG Change Schema for RecHubUser.usp_OLPreferences_Get_WithDescriptionsByUserID
* 01/02/2014 WI 125937	MLH Moved the parmUserID filter from the where clause to the join statement.  Only user preferences
*							were being returned.  All the preferences need to be returned.  
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT		
		RecHubUser.OLPreferences.OLPreferenceID,
		RecHubUser.OLPreferences.PreferenceName,
		RecHubUser.OLPreferences.[Description],
		RecHubUser.OLPreferences.IsOnline,
		OLPreferences.IsSystem,
		ISNULL(RecHubUser.OLPreferences.DefaultSetting, '') AS DefaultSetting,
		RecHubUser.OLPreferences.AppType,
		RecHubUser.OLPreferences.PreferenceGroup,
		RecHubUser.OLPreferences.fldDataTypeEnum,
		RecHubUser.OLPreferences.fldSize,
		RecHubUser.OLPreferences.fldMaxLength,
		RecHubUser.OLPreferences.fldExp,
		RecHubUser.OLPreferences.fldIntMinValue,
		RecHubUser.OLPreferences.fldIntMaxValue,
		RecHubUser.OLPreferences.EncodeOnSerialization,
		ISNULL(RecHubUser.OLUserPreferences.UserSetting, '') AS UserSetting
	FROM		
		RecHubUser.OLPreferences
	LEFT OUTER JOIN 
		RecHubUser.OLUserPreferences 
		ON RecHubUser.OLUserPreferences.OLPreferenceID = RecHubUser.OLPreferences.OLPreferenceID
		AND RecHubUser.OLUserPreferences.UserID = @parmUserID
	
    ORDER BY	
		RecHubUser.OLPreferences.PreferenceName;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

