--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_BrandingSchemes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_BrandingSchemes_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_BrandingSchemes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_BrandingSchemes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Return all BrandingSchemes records
*
* Modification History
* 01/18/2012 CR 49638 JCS   Created
* 02/29/2012 CR 51103 JCS   Added ExternalID1, ExternalID2 fields to returnset.
* 03/26/2013 WI 90587 JBS	Update to 2.0 release. Change Schema name to RecHubUser.
*							Change proc name from usp_BrandingSchemes_GetAll.							
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	BrandingSchemeID,
			BrandingSchemeName,
			BrandingSchemeDescription,
			BrandingSchemeFolder,
			IsActive,
			ExternalID1,
			ExternalID2,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
	FROM RecHubUser.BrandingSchemes
	ORDER BY BrandingSchemeName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
