--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_ByPermissionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_ByPermissionID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Permissions_Get_ByPermissionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_ByPermissionID
(
	@parmPermissionID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28695 JMC	Created
* 04/02/2013 WI 90772 JPB	Updated for 2.0.
*							Moved to RecHubUser schema.
*							Renamed from usp_Permissions_GetByID.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT
		RecHubUser.[Permissions].PermissionCategory, 
		RecHubUser.[Permissions].PermissionDefault, 
		RecHubUser.[Permissions].PermissionID, 
		RecHubUser.[Permissions].PermissionMode, 
		RecHubUser.[Permissions].PermissionName, 
		RecHubUser.[Permissions].ScriptFile
	FROM 
		RecHubUser.[Permissions]
	WHERE
		RecHubUser.[Permissions].PermissionID=@parmPermissionID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
