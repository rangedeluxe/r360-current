--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_BrandingSchemes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_BrandingSchemes_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_BrandingSchemes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_BrandingSchemes_Ins
(
	@parmBrandingSchemeName			VARCHAR(32),
	@parmBrandingSchemeDescription	VARCHAR(256),
	@parmBrandingSchemeFolder		VARCHAR(256),
	@parmIsActive					BIT, 
	@parmBrandingSchemeID			INT OUTPUT,
	@parmExternalID1				VARCHAR(32),
	@parmExternalID2				VARCHAR(32)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Insert new BrandingSchemes record
*
* Modification History
* 01/18/2012 CR 49641 JCS   Created
* 02/29/2012 CR 51101 JCS   Added ExternalID1, ExternalID2 fields to insert.
* 03/27/2013 WI 90590 JBS	Update to 2.0 release, Change schema name.
*							Rename proc from usp_BrandingSchemesInsert.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	
	SELECT @parmBrandingSchemeID = COALESCE(MAX(BrandingSchemeID),99)+1 FROM RecHubUser.BrandingSchemes WHERE BrandingSchemeID > 99
	
	INSERT INTO RecHubUser.BrandingSchemes
	(
		BrandingSchemeID,
		BrandingSchemeName,
		BrandingSchemeDescription,
		BrandingSchemeFolder,
		IsActive,
		ExternalID1,
		ExternalID2,
		CreatedBy,
		CreationDate,
		ModifiedBy,
		ModificationDate	
	)
	VALUES
	(
		@parmBrandingSchemeID,
		@parmBrandingSchemeName,
		@parmBrandingSchemeDescription,
		@parmBrandingSchemeFolder,
		@parmIsActive,
		@parmExternalID1,
		@parmExternalID2,
		SUSER_SNAME(),
		GETDATE(),
		SUSER_SNAME(),
		GETDATE() 
	);
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
