--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get_OLPreferenceIDByName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get_OLPreferenceIDByName') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_OLPreferences_Get_OLPreferenceIDByName;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get_OLPreferenceIDByName 
(
	@parmPreferenceName VARCHAR(64)
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences Descriptions for Preference By User ID
*
* Modification History
* 12/12/2012 WI 76549 CRG Write store procedure for SQLPreferences.GetOLUserPreferenceIDByName(string, out DataTable)
* 02/22/2013 WI 88838 CRG Change Schema for RecHubUser.usp_OLPreferences_Get_OLPreferenceIDByName
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		RecHubUser.OLPreferences.OLPreferenceID 
	FROM	
		RecHubUser.OLPreferences 
	WHERE	
		RecHubUser.OLPreferences.PreferenceName = @parmPreferenceName;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

