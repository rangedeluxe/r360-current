--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_UserPredefSearch_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_UserPredefSearch_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_UserPredefSearch_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_UserPredefSearch_Upd
(
	@parmUserID			INT,
	@parmPredefSearchID UNIQUEIDENTIFIER,
	@parmIsDefault		BIT,
	@parmRowsReturned	INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/17/2011
*
* Purpose: update entry from UserPredefSearch table.
*
* Modification History
* 01/17/2011 CR 31748 JNE	Created
* 04/04/2013 WI 90809 JBS	Update to 2.0 release. Change schema name 
******************************************************************************/
SET ARITHABORT ON;
SET XACT_ABORT ON;

SET @parmRowsReturned = 0;

BEGIN TRY
	BEGIN TRANSACTION
		UPDATE 	RecHubUser.UserPredefSearch 
		SET 	RecHubUser.UserPredefSearch.IsDefault = @parmIsDefault,
				RecHubUser.UserPredefSearch.ModifiedBy = SUSER_SNAME(),
				RecHubUser.UserPredefSearch.ModificationDate = GETDATE()
		FROM 	RecHubUser.UserPredefSearch
		WHERE 	RecHubUser.UserPredefSearch.UserID = @parmUserID 
				AND RecHubUser.UserPredefSearch.PreDefSearchID = @parmPredefSearchID;

		SELECT @parmRowsReturned = @@ROWCOUNT;
		

		/* uncheck other queries that would have been default before this update for the user. */
		IF @parmRowsReturned > 0 AND @parmIsDefault = 'True'
			UPDATE	RecHubUser.UserPredefSearch
			SET 	RecHubUser.UserPredefSearch.IsDefault = 'False',
					RecHubUser.UserPredefSearch.ModifiedBy = SUSER_SNAME(),
					RecHubUser.UserPredefSearch.ModificationDate = GETDATE()
			FROM 	RecHubUser.UserPredefSearch
			WHERE 	RecHubUser.UserPredefSearch.UserID = @parmUserID
					AND RecHubUser.UserPredefSearch.IsDefault = 'True'
					AND RecHubUser.UserPredefSearch.PreDefSearchID <> @parmPredefSearchID; 

	COMMIT TRANSACTION
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
