﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserQuestions_Get_OLUserQuestions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserQuestions_Get_OLUserQuestions') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserQuestions_Get_OLUserQuestions;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserQuestions_Get_OLUserQuestions 
(
	@parmUserID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences for Preference Name
*
* Modification History
* 12/12/2012 WI 83227 CRG Write store procedure to replace SQLLogon.SQL_GetOLUserQuestions(int userID)
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		RecHubUser.OLUserQuestions.OLUserQuestionID,
        RecHubUser.OLUserQuestions.UserID,
        RecHubUser.OLUserQuestions.QuestionText,
        RecHubUser.OLUserQuestions.OLUserQuestionID,
        RecHubUser.OLUserQuestions.CreationDate,
        RecHubUser.OLUserQuestions.CreatedBy,
        RecHubUser.OLUserQuestions.ModificationDate,
        RecHubUser.OLUserQuestions.ModifiedBy
    FROM	
		RecHubUser.OLUserQuestions
	WHERE	
		RecHubUser.OLUserQuestions.UserID = @parmUserID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;