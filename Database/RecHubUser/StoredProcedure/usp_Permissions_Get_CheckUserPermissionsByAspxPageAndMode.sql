﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_CheckUserPermissionsByAspxPageAndMode
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_CheckUserPermissionsByAspxPageAndMode') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPermissionsByAspxPageAndMode
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPermissionsByAspxPageAndMode 
(
	@parmScriptFile		VARCHAR(255),
	@parmUserID			INT,
	@parmMode			VARCHAR(30),
	@parmScriptFileAspx	VARCHAR(255) = NULL
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/16/2013
*
* Purpose: To check the user permissions by page and mode
*	This is part one of four store procedures used to determine permissions
*	This gets permissions by page, aspx page and mode by user id
*
* Modification History
* 01/16/2013 WI 85595 CRG Write a store procedure to replace ExecuteSQL(SQLPermissions.SQL_CheckUserPageModePermissions(ScriptFile, UserID, Mode), out dt)
* 02/27/2013 WI 89377 CRG Change Schema for RecHubUser.usp_Permissions_Get_CheckUserPermissionsByAspxPageAndMode
* 07/08/2013 WI 89377 WJS Merge this stored proc with usp_Permissions_Get_CheckUserPermissionsPageAndMode
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	IF @parmScriptFileAspx IS NOT NULL
		BEGIN
			SELECT	
				RecHubUser.[Permissions].PermissionName
			FROM    
				RecHubUser.[Permissions] 
			INNER JOIN 
				RecHubUser.UserPermissions 
				ON RecHubUser.[Permissions].PermissionID = RecHubUser.UserPermissions.PermissionID
			WHERE	
				( 	
					LOWER(RecHubUser.[Permissions].ScriptFile)		= LOWER(@parmScriptFile)
					OR LOWER(RecHubUser.[Permissions].ScriptFile)	= LOWER(@parmScriptFileAspx)
				)
				AND	RecHubUser.UserPermissions.UserID		= @parmUserID	
				AND	RecHubUser.[Permissions].PermissionMode	= @parmMode;
		END
	ELSE
		SELECT	
				RecHubUser.[Permissions].PermissionName
			FROM    
				RecHubUser.[Permissions] 
			INNER JOIN 
				RecHubUser.UserPermissions 
				ON RecHubUser.[Permissions].PermissionID = RecHubUser.UserPermissions.PermissionID
			WHERE	
				LOWER(RecHubUser.[Permissions].ScriptFile)		= LOWER(@parmScriptFile)
				AND	RecHubUser.UserPermissions.UserID		= @parmUserID	
				AND	RecHubUser.[Permissions].PermissionMode	= @parmMode;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH