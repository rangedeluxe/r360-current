--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLEntities_Upsert
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLEntities_Upsert') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLEntities_Upsert
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLEntities_Upsert(
	@parmUserId						INT,
	@parmEntityId					INT,
	@parmDisplayBatchId				BIT,
	@parmPaymentImageDisplayMode	INT,
	@parmDocumentImageDisplayMode	INT,
	@parmViewingDays				INT,
	@parmMaximumSearchDays			INT,
	@parmBillingAccount				VARCHAR(20) = NULL,
	@parmBillingField1				VARCHAR(20) = NULL,
	@parmBillingField2				VARCHAR(20) = NULL,
	@paramEntityName				varchar(50) = NULL
	)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 06/12/2014
*
* Purpose: Updates an OLEnties record, or creates the record if it doesn't exist
*
* Modification History
* 06/12/2014 WI 150302 RDS	Created
* 07/10/2014 WI 150302 RDS	Fix Auditing
* 01/19/2015 WI 184872 RDS	Add Billing Fields
* 09/01/2015 WI 225204 MAA  Added entityName(item being updated) parameter and more extensive auditing
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	DECLARE @auditMessage			VARCHAR(1024),
			@auditColumns			RecHubCommon.AuditValueChangeTable,
			@errorDescription		VARCHAR(1024),
			@checkImageDisplayModeMessage	varchar(30),
			@prevCheckImageDisplayModeMessage	varchar(30),
			@documentImageDisplayModeMessage	varchar(30),
			@prevDocumentImageDisplayModeMessage	varchar(30);
	DECLARE @curTime DATETIME = GETDATE();
	--audit message
	IF @parmPaymentImageDisplayMode = 0 
		SET @checkImageDisplayModeMessage = 'Use Inherited Setting'
	IF @parmPaymentImageDisplayMode = 1
		SET @checkImageDisplayModeMessage = 'Use Front and Back'
	IF @parmPaymentImageDisplayMode = 2 
		SET @checkImageDisplayModeMessage = 'Use Front only'
	--		
	IF @parmDocumentImageDisplayMode = 0 
		SET @documentImageDisplayModeMessage = 'Use Inherited Setting'
	IF @parmDocumentImageDisplayMode = 1
		SET @documentImageDisplayModeMessage = 'Use Front and Back'
	IF @parmDocumentImageDisplayMode = 2 
		SET @documentImageDisplayModeMessage = 'Use Front only'
	-- Verify User ID
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to update RecHubUser.OLEntities record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Normalize Billing Fields; do not store blank values
	IF LEN(@parmBillingAccount) = 0	SET @parmBillingAccount = NULL;
	IF LEN(@parmBillingField1) = 0	SET @parmBillingField1 = NULL;
	IF LEN(@parmBillingField2) = 0	SET @parmBillingField2 = NULL;

	-- Save Previous Values while attempting to update
	DECLARE @prevDisplayBatchId				BIT,
			@prevPaymentImageDisplayMode	INT,
			@prevDocumentImageDisplayMode	INT,
			@prevViewingDays				INT,
			@prevMaximumSearchDays			INT,
			@prevBillingAccount				VARCHAR(20),
			@prevBillingField1				VARCHAR(20),
			@prevBillingField2				VARCHAR(20);

	UPDATE	RecHubUser.OLEntities
	SET		@prevDisplayBatchId				= DisplayBatchID,
			@prevPaymentImageDisplayMode	= CheckImageDisplayMode,
			@prevDocumentImageDisplayMode	= DocumentImageDisplayMode,
			@prevViewingDays				= ViewingDays,
			@prevMaximumSearchDays			= MaximumSearchDays,
			@prevBillingAccount				= BillingAccount,
			@prevBillingField1				= BillingField1,
			@prevBillingField2				= BillingField2,
			
			DisplayBatchID					= @parmDisplayBatchId,
			CheckImageDisplayMode			= @parmPaymentImageDisplayMode,
			DocumentImageDisplayMode		= @parmDocumentImageDisplayMode,
			ViewingDays						= @parmViewingDays,
			MaximumSearchDays				= @parmMaximumSearchDays,
			BillingAccount					= @parmBillingAccount,
			BillingField1					= @parmBillingField1,
			BillingField2					= @parmBillingField2
	WHERE	
		EntityID = @parmEntityId
		AND (	-- Check if anything is changing
			DisplayBatchId <> @parmDisplayBatchId OR
			CheckImageDisplayMode <> @parmPaymentImageDisplayMode OR
			DocumentImageDisplayMode <> @parmDocumentImageDisplayMode OR
			ISNULL(ViewingDays, -1) <> ISNULL(@parmViewingDays, -1) OR
			ISNULL(MaximumSearchDays, -1) <> ISNULL(@parmMaximumSearchDays, -1) OR
			ISNULL(BillingAccount, '') <> ISNULL(@parmBillingAccount, '') OR
			ISNULL(BillingField1, '') <> ISNULL(@parmBillingField1, '') OR
			ISNULL(BillingFIeld2, '') <> ISNULL(@parmBillingField2, '')
			);

	IF @@ROWCOUNT = 0
	BEGIN
		-- Do we need to insert the row?
		IF NOT EXISTS (SELECT * from RecHubUser.OLEntities WHERE EntityID = @parmEntityId)
		BEGIN
			INSERT INTO RecHubUser.OLEntities (EntityID, DisplayBatchID, CheckImageDisplayMode, DocumentImageDisplayMode, ViewingDays, MaximumSearchDays, BillingAccount, BillingField1, BillingField2)
			VALUES (@parmEntityId, @parmDisplayBatchId, @parmPaymentImageDisplayMode, @parmDocumentImageDisplayMode, @parmViewingDays, @parmMaximumSearchDays, @parmBillingAccount, @parmBillingField1, @parmBillingField2);

			-- Audit the insert			
			SET @auditMessage = 'Workgroup Maintenance: Added OLEntities for Entity: ' + ISNULL(@paramEntityName, "NULL")
				+ ': DisplayBatchID = ' + CAST(@parmDisplayBatchID AS VARCHAR)
				+ ', PaymentImageDisplayMode = ' + ISNULL(@checkImageDisplayModeMessage, "NULL")
				+ ', DocumentImageDisplayMode = ' + ISNULL(@documentImageDisplayModeMessage, "NULL")
				+ ', ViewingDays = ' + ISNULL(CAST(@parmViewingDays AS VARCHAR), 'NULL')
				+ ', MaximumSearchDays = ' + ISNULL(CAST(@parmMaximumSearchDays AS VARCHAR), 'NULL')
				+ ', BillingAccount = ' + ISNULL(@parmBillingAccount, 'NULL')
				+ ', BillingField1 = ' + ISNULL(@parmBillingField1, 'NULL')
				+ ', BillingField2 = ' + ISNULL(@parmBillingField2, 'NULL')
				+ '.';

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubUser.usp_OLEntities_Upsert',
					@parmSchemaName			= 'RecHubUser',
					@parmTableName			= 'OLEntities',
					@parmColumnName			= 'EntityID',
					@parmAuditValue			= @parmEntityId,
					@parmAuditType			= 'INS',
					@parmAuditMessage		= @auditMessage;
		END
	END
	ELSE
	BEGIN
		-- Audit the Update (changes only)
		IF @prevDisplayBatchId <> @parmDisplayBatchId
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DisplayBatchID', CAST(@prevDisplayBatchId AS VARCHAR), CAST(@parmDisplayBatchId AS VARCHAR));
		IF @prevPaymentImageDisplayMode <> @parmPaymentImageDisplayMode
		BEGIN					
			IF @parmPaymentImageDisplayMode = 0
				SET @prevCheckImageDisplayModeMessage = 'Use Inherited Setting'
			IF @parmPaymentImageDisplayMode = 1
				SET @prevCheckImageDisplayModeMessage  = 'Use Front and Back'
			IF @parmPaymentImageDisplayMode = 2
				SET @prevCheckImageDisplayModeMessage  = 'Use Front only'		
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('CheckImageDisplayMode', ISNULL(@prevCheckImageDisplayModeMessage, 'NULL'), ISNULL(@checkImageDisplayModeMessage, 'NULL'));
		END
		IF @prevDocumentImageDisplayMode <> @parmDocumentImageDisplayMode
		BEGIN
			IF @prevDocumentImageDisplayMode = 0
				SET @prevDocumentImageDisplayModeMessage = 'Use Inherited Setting'
			IF @prevDocumentImageDisplayMode = 1
				SET @prevDocumentImageDisplayModeMessage = 'Use Front and Back'
			IF @prevDocumentImageDisplayMode = 2
				SET @prevDocumentImageDisplayModeMessage = 'Use Front only'
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DocumentImageDisplayMode', ISNULL(@prevDocumentImageDisplayModeMessage, 'NULL'), ISNULL(@documentImageDisplayModeMessage, 'NULL'));
		END
		IF ISNULL(@prevViewingDays, -1) <> ISNULL(@parmViewingDays, -1)
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ViewingDays', ISNULL(CAST(@prevViewingDays AS VARCHAR), 'NULL'), ISNULL(CAST(@parmViewingDays AS VARCHAR), 'NULL'));
		IF ISNULL(@prevMaximumSearchDays, -1) <> ISNULL(@parmMaximumSearchDays, -1)
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('MaximumSearchDays', ISNULL(CAST(@prevMaximumSearchDays AS VARCHAR), 'NULL'), ISNULL(CAST(@parmMaximumSearchDays AS VARCHAR), 'NULL'));
		IF ISNULL(@prevBillingAccount, '') <> ISNULL(@parmBillingAccount, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingAccount', ISNULL(@prevBillingAccount, 'NULL'), ISNULL(@parmBillingAccount, NULL));
		IF ISNULL(@prevBillingField1, '') <> ISNULL(@parmBillingField1, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingField1', ISNULL(@prevBillingField1, 'NULL'), ISNULL(@parmBillingField1, NULL));
		IF ISNULL(@prevBillingField2, '') <> ISNULL(@parmBillingField2, '')
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BillingField2', ISNULL(@prevBillingField2, 'NULL'), ISNULL(@parmBillingField2, NULL));

		SET @auditMessage = 'Workgroup Maintenance: Modified OLEntities for Entity: ' + ISNULL(@paramEntityName, "NULL") + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLEntities_Upsert',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'OLEntities',
			@parmColumnName			= 'EntityID',
			@parmAuditValue			= @parmEntityId,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END

	Cleanup:

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

