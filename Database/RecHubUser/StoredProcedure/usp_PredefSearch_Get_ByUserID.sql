--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_PredefSearch_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_PredefSearch_Get_ByUserID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_PredefSearch_Get_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_PredefSearch_Get_ByUserID
(
	@parmUserID			INT,
	@parmPreDefSearchID UNIQUEIDENTIFIER = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/17/2011
*
* Purpose: Select data from PreDefSearch table by UserPreDefSearch.UserID
*
* Modification History
* 01/17/2011 CR 31745 JNE	Created
* 04/04/2013 WI 90776 JBS	Update to 2.0 release.  Change schema to RecHubUser
*							Rename proc from usp_PredefSearch_GetByUserID
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY
	SELECT 	RecHubUser.UserPreDefSearch.UserID,
			RecHubUser.UserPreDefSearch.PredefSearchID,
			RecHubUser.UserPreDefSearch.IsDefault,
			RecHubUser.PredefSearch.[Name],
			RecHubUser.PredefSearch.[Description]
	FROM 	
			RecHubUser.UserPreDefSearch
			INNER JOIN RecHubUser.PredefSearch ON RecHubUser.PredefSearch.PredefSearchID = RecHubUser.UserPreDefSearch.PredefSearchID
	WHERE 	
			RecHubUser.UserPreDefSearch.UserID = @parmUserID
			AND ((@parmPreDefSearchID IS NULL) OR (RecHubUser.UserPreDefSearch.PredefSearchID = @parmPreDefSearchID))
	ORDER BY 
			RecHubUser.UserPreDefSearch.IsDefault DESC, RecHubUser.PredefSearch.[Name];

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
