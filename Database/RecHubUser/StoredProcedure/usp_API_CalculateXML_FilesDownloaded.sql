--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_CalculateXML_FilesDownloaded
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_CalculateXML_FilesDownloaded') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_CalculateXML_FilesDownloaded;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_CalculateXML_FilesDownloaded
(
	@parmBank				INT, 
	@parmLBX				INT, 
	@parmProcDate			DATETIME, 
	@parmRowsReturned		INT OUTPUT, 
	@parmErrorDescription	VARCHAR(2000) OUTPUT, 
	@parmSQLErrorNumber		INT OUTPUT 
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/08/2009
*
* Purpose: 
*		   
*
* Modification History
* 06/08/2009 CR 27186 JPB	Based on proc_VT_CalculateXML_FilesDownloaded.
* 06/24/2013 WI 71832 JPB	Updated to 2.0.
*							Renamed from proc_VT_OLTA_CalculateXML_FilesDownloaded
******************************************************************************/

SET NOCOUNT ON;

BEGIN TRY
	/* 
	Set up return values for output parameters assuming 
	no errors 
	*/ 
	SELECT @parmRowsReturned = 0, @parmErrorDescription = '', @parmSQLErrorNumber = 0;

	SET @parmProcDate = CONVERT(varchar(10), @parmProcDate, 101);

	SELECT	
		COUNT(*) as Volume
	FROM	
		RecHubUser.SessionActivityLog
		JOIN RecHubUser.[Session] ON RecHubUser.[Session].SessionID = RecHubUser.SessionActivityLog.SessionID
	WHERE	
		RecHubUser.SessionActivityLog.BankID = @parmBank
		AND RecHubUser.SessionActivityLog.ClientAccountID = @parmLBX
		AND RecHubUser.SessionActivityLog.ActivityCode IN 
		(
			SELECT	ActivityCode 
			FROM	RecHubUser.ActivityCodes 
			WHERE	ActivityCodeDescription IN 
			(
				'Search Results Download Xml'
			)
		)
		AND RecHubUser.SessionActivityLog.ProcessingDate = @parmProcDate
		AND RecHubUser.SessionActivityLog.DeliveredToWeb = 1
		AND RecHubUser.[Session].OLOrganizationID IS NOT NULL
		AND RecHubUser.SessionActivityLog.SessionID NOT IN
		(
			SELECT	SessionID
			FROM	RecHubUser.SessionEmulation
			WHERE	IsBillable <> 1
		);

	SELECT @parmRowsReturned = @@ROWCOUNT;

    RETURN 0;
END TRY
BEGIN CATCH
	SELECT @parmSQLErrorNumber = @@ERROR;
    SET @parmErrorDescription = 'An Error Occured in the execution of usp_API_CalculateXML_FilesDownloaded';
    RAISERROR(@parmErrorDescription, 16, 1);
	EXEC RecHubCommon.usp_WfsRethrowException;
    RETURN 1;
END CATCH
