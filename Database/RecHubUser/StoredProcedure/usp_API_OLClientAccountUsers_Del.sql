--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_OLClientAccountUsers_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccountUsers_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccountUsers_Del
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_OLClientAccountUsers_Del
(
    @parmUserID int,
    @parmOLClientAccountID uniqueidentifier,
    @parmOperator varchar(32),
    @parmErrorDescription varchar(2000) output,
    @parmSQLErrorNumber int output
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/23/2009
*
* Purpose: Delete User from ClientAccount table
*		   
*
* Modification History
* 04/23/2009 CR 25817 JNE	Created
* 03/27/2013 WI 90564 JBS	Update to 2.0 release. Change Schema name.
*							Rename proc from proc_OLLockboxUsers_RemoveOLLockboxUser.
*							Change references of Lockbox to ClientAccount.
*							Change Parameter @parmOLLockboxID to @parmOLClientAccountID.
* 06/19/2013 WI 104344 MGE	Added auditing
******************************************************************************/

SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

SET @parmErrorDescription = '';
SET @parmSQLErrorNumber = 0;

DECLARE
	@DeleteUserID	INT = 0,
	@AuditMessage	VARCHAR(1024),
	@CharUserID		VARCHAR(10)

BEGIN TRY

	IF @parmUserID IS NULL
		RAISERROR('UserID is NULL',16,1);

	IF @parmOLClientAccountID IS NULL
	   RAISERROR('OLClientAccountID is NULL',16,1);

	IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLClientAccountUsers WHERE RecHubUser.OLClientAccountUsers.OLClientAccountID = @parmOLClientAccountID AND RecHubUser.OLClientAccountUsers.UserID = @parmUserID)
	BEGIN
	   SET @parmSQLErrorNumber = -1;
	   SET @parmErrorDescription = 'Entry does not exist for OLClientAccountID: ' + CAST(@parmOLClientAccountID as varchar(72)) +
								   ' and UserID: ' + CAST(@parmUserID AS varchar(20));
	   RAISERROR(@parmErrorDescription,16,1);
	END

	BEGIN TRANSACTION OLClientAccountUsers_Delete

	DELETE FROM RecHubUser.OLClientAccountUsers
	WHERE RecHubUser.OLClientAccountUsers.OLClientAccountID = @parmOLClientAccountID
		AND RecHubUser.OLClientAccountUsers.UserID = @parmUserID;

	/* Insert Audit Record */
	SELECT 
		@DeleteUserID = UserID
	FROM 
		RecHubUser.Users
	WHERE 
		LogonName = @parmOperator;
	SELECT @CharUserID = CAST(@parmUserID AS VARCHAR(10));

	SET @AuditMessage = 'Deleted OLClientAccount entitlement for '  
	+ 'ClientAccountID = ' + CAST(@parmOLClientAccountID AS VARCHAR(72))
	+ ' for UserID = ' + @CharUserID +'.'
	
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 	
		@parmUserID					= @DeleteUserID,
		@parmApplicationName		= 'RecHubUser.usp_API_OLClientAccountUsers_Del',
		@parmSchemaName				= 'RecHubUser',
		@parmTableName				= 'OLClientAccountUsers',
		@parmColumnName				= 'UserID',
		@parmAuditValue				= @CharUserID,
		@parmAuditType				= 'DEL',
		@parmAuditMessage			= @AuditMessage
	
	COMMIT TRANSACTION OLClientAccountUsers_Delete

    RETURN 0
END TRY
BEGIN CATCH
	IF @@TRANCOUNT>0
		ROLLBACK TRANSACTION OLClientAccountUsers_Delete
	IF @parmErrorDescription = ''
		SET @parmErrorDescription = 'An error occured in the execution of usp_API_OLClientAccountUsers_Del';
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 1
END CATCH






