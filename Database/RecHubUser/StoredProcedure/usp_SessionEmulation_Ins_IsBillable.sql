﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionEmulation_Ins_IsBillable
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionEmulation_Ins_IsBillable') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionEmulation_Ins_IsBillable
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionEmulation_Ins_IsBillable 
(
	@parmEmulationID	UNIQUEIDENTIFIER,
	@parmSessionID		UNIQUEIDENTIFIER, 
	@parmUserID			INT,
	@parmIsBillable		BIT,
	@parmRowsReturned	INT = 0 OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/04/2013
*
* Purpose: Insert a record into SessionEmulation
*
* Modification History
* 01/04/2013 WI 83453 CRG Write a store procedure to replace SQLSessionEmulation.SQL_EmulateUser(EmulationID , SessionID , UserID)
* 01/04/2013 WI 83433 CRG Write a Store Procedure to replace SQLSesionEmulation.SQL_EmulateUser(Guid EmulationID, Guid SessionID, int UserID, bool IsBillable)
* 03/06/2013 WI 90339 CRG Change Schema for SessionEmulation in RecHubUser.usp_SessionEmulation_Ins_IsBillable
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO 
		RecHubUser.SessionEmulation 
		(
			RecHubUser.SessionEmulation.EmulationID, 
			RecHubUser.SessionEmulation.SessionID, 
			RecHubUser.SessionEmulation.UserID, 
			RecHubUser.SessionEmulation.IsBillable
		) 
	VALUES 
		(
			@parmEmulationID, 
			@parmSessionID, 
			@parmUserID,
			@parmIsBillable
		);

	SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH