﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionEmulation_Get_ByEmulationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionEmulation_Get_ByEmulationID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionEmulation_Get_ByEmulationID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionEmulation_Get_ByEmulationID 
(
    @parmEmulationID	UNIQUEIDENTIFIER
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/04/2013
*
* Purpose: Get Session Emulation by Emulation ID
*
* Modification History
* 01/04/2013 WI 83434 CRG Write a store procedure to replace SQLSessionEmulation.SQL_GetEmulationUserID(Guid emulationID)
* 03/06/2013 WI 90340 CRG Change Schema for SessionEmulation in RecHubUser.usp_SessionEmulation_Get_ByEmulationID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		RecHubUser.SessionEmulation.UserID
    FROM	
		RecHubUser.SessionEmulation 
	INNER JOIN 
		RecHubUser.[Session] 
		ON RecHubUser.[Session].SessionID = RecHubUser.SessionEmulation.SessionID
	WHERE	
		RecHubUser.SessionEmulation.EmulationID = @parmEmulationID
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH