﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubConfig_Admin">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Users_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_ByUserID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Users_Get_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_ByUserID
( 
	@parmUserID		INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/21/2013
*
* Purpose: Get Users by User ID
*
* Modification History
* 01/21/2013 WI 85601 CRG	Write a store procedure to replace ExecuteSQL(SQLUser.SQL_GetUser(UserID), out dt)
* 02/28/2013 WI 89721 CRG	Change Schema for RecHubUser.usp_Users_Get_ByUserID
* 11/14/2014 WI 177887 KLC	Updated to send back raam user sid.
* 04/10/2015 WI 200411 JBS	Remove columns no longer used.
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		RecHubUser.Users.UserID,
		RecHubUser.Users.LogonName,
        RecHubUser.Users.CreationDate,
        RecHubUser.Users.CreatedBy,
        RecHubUser.Users.ModificationDate,
        RecHubUser.Users.ModifiedBy,
        RecHubUser.Users.RA3MSID
	FROM	
		RecHubUser.Users
	WHERE	
		RecHubUser.Users.UserID = @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH