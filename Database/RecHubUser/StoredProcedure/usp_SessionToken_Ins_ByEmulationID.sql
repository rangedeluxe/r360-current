﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionToken_Ins_ByEmulationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionToken_Ins_ByEmulationID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionToken_Ins_ByEmulationID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionToken_Ins_ByEmulationID 
(
	@parmSessionTokenID	UNIQUEIDENTIFIER,
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmEmulationID	UNIQUEIDENTIFIER,
	@parmRowsReturned	INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/09/2013
*
* Purpose: insert session token by emulation id
*
* Modification History
* 01/09/2013 WI 83232 CRG Write store procedure to replace SQLLogon.SQL_InsertSessionToken(Guid SessionTokenID, Guid SessionID, Guid EmulationID)
* 03/06/2013 WI 90338 CRG Change Schema for SessionActivityLog in RecHubUser.usp_SessionActivityLog_Ins_ByEmulationID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO 
		RecHubUser.SessionToken 
		(
			RecHubUser.SessionToken.SessionTokenID, 
			RecHubUser.SessionToken.SessionID, 
			RecHubUser.SessionToken.EmulationID
		) 
	VALUES 
	(
		@parmSessionTokenID,
		@parmSessionID,
		@parmEmulationID
	);

	SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH