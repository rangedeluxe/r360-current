--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ReportInstance_Get_ByReportInstanceKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_ReportInstance_Get_ByReportInstanceKey') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_ReportInstance_Get_ByReportInstanceKey;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_ReportInstance_Get_ByReportInstanceKey
(
      @parmReportInstanceKey BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the Report Instance using the Report Instance Key
*
* Modification History
* Created
* 05/06/2013 WI 107443 KLC	Created.
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		RecHubUser.ReportInstance.ReportInstanceKey,
		RecHubUser.ReportInstance.InstanceID,
		RecHubUser.ReportInstance.UserID,
		RecHubUser.ReportInstance.ReportID,
		RecHubUser.ReportInstance.ReportStatus,
		RecHubUser.ReportInstance.StartTime,
		RecHubUser.ReportInstance.EndTime
	FROM
		RecHubUser.ReportInstance
	WHERE
		RecHubUser.ReportInstance.ReportInstanceKey = @parmReportInstanceKey;

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
