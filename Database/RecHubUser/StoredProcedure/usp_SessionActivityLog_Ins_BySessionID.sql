﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionActivityLog_Ins_BySessionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionActivityLog_Ins_BySessionID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionActivityLog_Ins_BySessionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionActivityLog_Ins_BySessionID 
(
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmModuleName			VARCHAR(50), 
	@parmActivityCode		INT,
	@parmOnlineImageQueueID	UNIQUEIDENTIFIER = NULL, 
	@parmItemCount			INT,
	@parmBankID				INT, 
	@parmClientAccountID	INT, 
	@parmRowsReturned		INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/14/2013
*
* Purpose: Add Session Activity Log record by session ID
*
* Modification History
* 01/14/2013 WI 85320	CRG Write store procedure to replace ExecuteSQL(SQLSessionLog.SQL_InsertSessionActivityLog(SessionID, ModuleName, activityCode, OnlineImageQueueID, itemCount, BankID, LockboxID), out RowsAffected)
* 03/06/2013 WI 90337	CRG Change Schema for SessionActivityLog in RecHubUser.usp_SessionActivityLog_Ins_BySessionID
* 01/09/2013 WI 126435	MLH Added default setting of NULL to @parmOnlineImageQueueID.  
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO 
		RecHubUser.SessionActivityLog 
		(
			RecHubUser.SessionActivityLog.ActivityCode, 
			RecHubUser.SessionActivityLog.SessionID, 
			RecHubUser.SessionActivityLog.OnlineImageQueueID, 
			RecHubUser.SessionActivityLog.DeliveredToWeb, 
			RecHubUser.SessionActivityLog.ModuleName, 
			RecHubUser.SessionActivityLog.ItemCount, 
			RecHubUser.SessionActivityLog.DateTimeEntered, 
			RecHubUser.SessionActivityLog.BankID, 
			RecHubUser.SessionActivityLog.ClientAccountID
		) 
	VALUES 
		(
			@parmActivityCode,
			@parmSessionID,
			@parmOnlineImageQueueID,
			1, 
			@parmModuleName,
			@parmItemCount,
			GETDATE(),
			@parmBankID,
			@parmClientAccountID
		);

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;   
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
