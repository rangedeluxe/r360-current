IF OBJECT_ID('RecHubUser.usp_OLEntities_Assign') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLEntities_Assign
GO

CREATE PROCEDURE RecHubUser.usp_OLEntities_Assign(
	@parmEntityId					INT,
	@parmViewingDays				INT = NULL,
	@parmMaxSearchDays				INT = NULL
	)
AS
/******************************************************************************
** Deluxe Corp. (DLX)
** Copyright � 2018-2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: RDS
* Date: 06/12/2014
*
* Purpose: Inserts an OLEntities record - originally designed for WorkgroupAssignments package.
*			Most calls from other sources should use OLEntities_Upsert as it provides an audit trail.
*			The package could not use that stored procedure because of the app lock.
*
* Modification History
* 03/12/2018 PT 155854476	MGE	Created
* 10/01/2019 R360-30456		MGE Allow ViewingDays and MaximumSearchDays to be updated
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...

BEGIN TRY
	
	IF NOT EXISTS(Select 1 FROM RecHubUser.OLEntities WHERE EntityID = @parmEntityId)
	BEGIN
	INSERT INTO RecHubUser.OLEntities (EntityID, DisplayBatchID, CheckImageDisplayMode, DocumentImageDisplayMode, ViewingDays, MaximumSearchDays, BillingAccount, BillingField1, BillingField2, CreatedBy)
	VALUES (@parmEntityId, 0, 0, 0, @parmViewingDays, @parmMaxSearchDays, NULL, NULL, NULL,'WorkgroupAssignment');
	END
END TRY
BEGIN CATCH
	-- Cleanup local transaction
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

GRANT EXECUTE TO dbRole_RecHubUser