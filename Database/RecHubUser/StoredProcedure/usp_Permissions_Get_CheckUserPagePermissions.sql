﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_CheckUserPagePermissions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_CheckUserPagePermissions') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPagePermissions
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPagePermissions
(
	@parmScriptFile	VARCHAR(255),
	@parmUserID		INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/16/2013
*
* Purpose: Check User Page Permissions by page and user id
*
* Modification History
* 01/16/2013 WI 85594 CRG Write a store procedure to replace ExecuteSQL(SQLPermissions.SQL_CheckUserPagePermissions(ScriptFile, UserID), out dt)
* 02/27/2013 WI 89399 CRG Change Schema for RecHubUser.usp_Permissions_Get_CheckUserPagePermissions
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT 
		@parmScriptFile = LOWER(@parmScriptFile)

	SELECT	
		RecHubUser.[Permissions].PermissionName
	FROM	
		RecHubUser.[Permissions] 
	INNER JOIN RecHubUser.UserPermissions 
		ON RecHubUser.[Permissions].PermissionID = RecHubUser.UserPermissions.PermissionID
	WHERE	
		LOWER(RecHubUser.[Permissions].ScriptFile)	= @parmScriptFile
		AND	RecHubUser.UserPermissions.UserID		= @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
