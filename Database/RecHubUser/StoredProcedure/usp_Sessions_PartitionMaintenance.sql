
IF OBJECT_ID('RecHubUser.usp_Sessions_PartitionMaintenance') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Sessions_PartitionMaintenance
GO

CREATE PROCEDURE RecHubUser.usp_Sessions_PartitionMaintenance
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright 2017-2019 WAUSAU Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 4/12/2017
*
* Purpose: Switches out oldest partitions (based on RetentionDays in SystemSetup) and adds partitions necessary
*      to remain 52 partitions into the future.
*
* Modification History
* 04/12/2017 PT	143253497	MGE	Created
* 08/23/2017 PT 149894451	MGE	Add call to increase size of next two partitions to be used.
* 11/04/2019 R360-31416		MGE Make more tolerant for gaps in executions
************************************************************************************************************************************/
DECLARE @ReturnValue INT,
       @RangeDateKey INT,
	   @HighestRangeDateKey INT,
	   @SwitchPartitionNumber INT,
	   @HighestRangeDateTime DATETIME,
	   @OldestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonDateTime DATETIME,
	   @TempDateTime DATETIME,
       @DiskPath VARCHAR(1000),
       @SessionsRetentionDays INT,
       @OldestCalendarDateToKeep DateTime,
	   @EndDate	DATETIME,
       @LastDiskID INT, 
	   @Message VARCHAR(max),
       @SQLcmd VARCHAR(max),
	   @DefaultBeginDate DATETIME

SET @ReturnValue = 0
--RAISERROR('Beginning process to maintain Sessions partitions.',10,1) WITH NOWAIT;
BEGIN TRY
       --<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionsRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionsRetentionDays'
	PRINT 'SessionsRetentionDays = ' + Convert(VARCHAR(10), @SessionsRetentionDays, 101)
	SELECT @OldestCalendarDateToKeep = GETDATE() - @SessionsRetentionDays		-- n days prior to today
	PRINT 'Calculated Oldest Date to keep = ' + Convert(VARCHAR(10), @OldestCalendarDateToKeep, 101)
	SELECT @RangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @OldestCalendarDateToKeep AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest Partition to keep is: ' + CONVERT(CHAR(10),@RangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

    --<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	SELECT @HighestRangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @HighestRangeDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Last Future Partition to add is: ' + CONVERT(CHAR(10),@HighestRangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	--<<< Find oldest existing Partition >>>

	select @OldestExistingPartitonKey = Value from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'Sessions_PartitionFunction' AND boundary_id = 1;
	SELECT @Message = 'Oldest Existing Partition is: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Find highest existing Partition >>>

	select @HighestExistingPartitonKey = MAX(Value) from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'Sessions_PartitionFunction'
	SELECT @Message = 'Highest Existing Partition Key is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	SELECT @HighestExistingPartitonDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE DateKey = @HighestExistingPartitonKey AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Highest Existing Partition DateTime is: ' + CONVERT(CHAR(10),@HighestExistingPartitonDateTime,111)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	--<<< Determine if there is work to do >>>

	IF	(@OldestExistingPartitonKey >= @RangeDateKey)
		BEGIN
			SELECT @Message = 'No partitions to Switch out '
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
	ELSE
	  BEGIN
	  	  
	  WHILE (@OldestExistingPartitonKey < @RangeDateKey AND @OldestExistingPartitonKey < @HighestExistingPartitonKey)
		BEGIN
			--<<<<<<<< NEED TO DO 3 TABLES HERE: SESSION, SESSIONLOG, SESSIONCLIENTACCOUNTENTITLEMENTS >>>>>>>>>>>>>>
			
			IF EXISTS (Select 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'RecHubUser'
				AND sys.tables.name = 'SwitchableSession')
				BEGIN
					--RAISERROR('Dropping SwitchableSession table', 10,1) WITH NOWAIT;
					DROP TABLE RecHubUser.SwitchableSession;
				END

			IF EXISTS (Select 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'RecHubUser'
				AND sys.tables.name = 'SwitchableSessionLog')
				BEGIN
					--RAISERROR('Dropping SwitchableSessionLog table', 10,1) WITH NOWAIT;
					DROP TABLE RecHubUser.SwitchableSessionLog;
				END

			IF EXISTS (Select 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'RecHubUser'
				AND sys.tables.name = 'SwitchableSessionClientAccountEntitlements')
				BEGIN
					--RAISERROR('Dropping SwitchableSessionClientAccountEntitlements table', 10,1) WITH NOWAIT;
					DROP TABLE RecHubUser.SwitchableSessionClientAccountEntitlements;
				END

			SELECT @Message = 'Creating SwitchableSession table on Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE RecHubUser.SwitchableSession
			(
				SessionID UNIQUEIDENTIFIER NOT NULL, 
				UserID INT NOT NULL,
				CreationDateKey INT NOT NULL,
				IPAddress VARCHAR(64) NOT NULL,
				BrowserType VARCHAR(1024) NOT NULL,
				LogonDateTime DATETIME NOT NULL,
				LastPageServed DATETIME NULL,
				PageCounter INT NOT NULL,
				IsSuccess BIT NOT NULL, 
				IsSessionEnded BIT NOT NULL, 
				LogonName VARCHAR(50) NULL,
				OLOrganizationID UNIQUEIDENTIFIER NULL,
				OrganizationCode VARCHAR(20) NULL,
				IsRegistered BIT NOT NULL, 
			) ON Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			
			EXEC (@SQLcmd);
			CREATE CLUSTERED INDEX IDX_Sessions_CreationDateKeySessionID ON RecHubUser.SwitchableSession(CreationDateKey,SessionID);

			SELECT @Message = 'Creating SwitchableSessionLog table on Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE RecHubUser.SwitchableSessionLog
			(
				LogEntryID BIGINT IDENTITY(1,1) NOT NULL,
				SessionID UNIQUEIDENTIFIER NOT NULL,
				CreationDateKey INT NOT NULL,
				PageCounter INT NOT NULL,
				PageName VARCHAR(30) NOT NULL,
				DateTimeEntered DATETIME NOT NULL,
				ScriptName VARCHAR(255) NOT NULL,
				FormFields TEXT NULL
			) ON Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			
			EXEC (@SQLcmd);
			CREATE CLUSTERED INDEX IDX_SessionLog_CreationDateKeyLogEntryID ON RecHubUser.SwitchableSessionLog
			(
				CreationDateKey ASC,
				LogEntryID ASC
			);

			SELECT @Message = 'Creating SwitchableSessionClientAccountEntitlements table on Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE RecHubUser.SwitchableSessionClientAccountEntitlements
			(
				SessionClientAccountEntitlementID BIGINT IDENTITY(1,1) NOT NULL,
		
				SessionID			UNIQUEIDENTIFIER,
				CreationDateKey		INT NOT NULL,
				EntityID			INT NOT NULL,
				SiteBankID			INT NOT NULL,
				SiteClientAccountID INT NOT NULL,
				ClientAccountKey	INT NOT NULL,
				ViewingDays			INT NULL,
				MaximumSearchDays	INT NULL,
				StartDateKey		INT NOT NULL,
				EndDateKey			INT NOT NULL,
				ViewAhead			BIT NOT NULL,
				CreationDate		DATETIME NOT NULL,
				EntityName			VARCHAR(128) NOT NULL
			) ON Sessions' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));

			EXEC (@SQLcmd);
			CREATE CLUSTERED INDEX IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID ON RecHubUser.SwitchableSessionClientAccountEntitlements
			(
				CreationDateKey,
				SessionClientAccountEntitlementID
			);
			
			/* <<<<REMOVE FK's to allow SWITCH to work >>>> */		
			
			IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionLog' AND CONSTRAINT_NAME='FK_SessionLog_Session' )
				ALTER TABLE RecHubUser.SessionLog DROP CONSTRAINT FK_SessionLog_Session;
				
			/* <<<< Now do the SWITCHES  >>>>>>>>>>>> */	
			SELECT @SwitchPartitionNumber = partition_number FROM RecHubSystem.FileGroupDetailView 
				WHERE range_value = @OldestExistingPartitonKey AND partition_scheme_name = 'Sessions'
			
			SELECT @Message = 'Switching out the oldest partition: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER TABLE RecHubUser.Session SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO RecHubUser.SwitchableSession'
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			SELECT @SQLcmd = 'ALTER TABLE RecHubUser.SessionLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO RecHubUser.SwitchableSessionLog'
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			SELECT @SQLcmd = 'ALTER TABLE RecHubUser.SessionClientAccountEntitlements SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO RecHubUser.SwitchableSessionClientAccountEntitlements'
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			 
			SELECT @Message = 'Merging the oldest range: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER PARTITION FUNCTION Sessions_PartitionFunction() MERGE RANGE (' + (CONVERT(CHAR(10),@OldestExistingPartitonKey,101)) + ')';
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			DROP TABLE RecHubUser.SwitchableSession
			DROP TABLE RecHubUser.SwitchableSessionLog
			DROP TABLE RecHubUser.SwitchableSessionClientAccountEntitlements

			SELECT @Message = 'Removing the oldest partiton file ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILE Sessions'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SELECT @Message = 'Removing the oldest partition filegroup: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILEGROUP Sessions'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SET @TempDateTime = Convert(VARCHAR(10),(CONVERT(datetime, convert(varchar(10), @OldestExistingPartitonKey)) + 7),101);
			select @OldestExistingPartitonKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate = @TempDateTime AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC); 
		END
	  END
	  
	/* <<<<Add bacl the FK's removed to allow SWITCH to work >>>> */
	IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHubUser' AND TABLE_NAME='SessionLog' AND CONSTRAINT_NAME='FK_SessionLog_Session' )
		ALTER TABLE RecHubUser.SessionLog ADD CONSTRAINT FK_SessionLog_Session FOREIGN KEY(SessionID, CreationDateKey) REFERENCES RecHubUser.Session(SessionID, CreationDateKey);

	--<<< Add Partitions up through @HighestRangeKey >>>

	EXEC RecHubSystem.usp_SetPartitionSize 'Sessions'

	IF @HighestExistingPartitonKey IS NULL
		BEGIN
			SELECT @DefaultBeginDate = GETDATE() - 14						--Start 2 weeks ago
			SELECT @HighestExistingPartitonDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @DefaultBeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
			SELECT @HighestExistingPartitonKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @DefaultBeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
			SELECT @Message = 'Default Existing Partition to add is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
    
	IF (@HighestExistingPartitonKey < @HighestRangeDateKey)
		BEGIN
		RAISERROR('Creating new Sessions partitions',10,1) WITH NOWAIT;
		SELECT @Message = 'EXEC usp_CreatePartitionsForRange Sessions ' + Convert(Varchar(10),@HighestExistingPartitonDateTime,101) + ' ' + Convert(Varchar(10),@HighestRangeDateTime,101);
		RAISERROR(@Message, 10,1) WITH NOWAIT;
		EXEC RecHubSystem.usp_CreatePartitionsForRange 'Sessions', @HighestExistingPartitonDateTime, @HighestRangeDateTime;
	
		END

   
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
