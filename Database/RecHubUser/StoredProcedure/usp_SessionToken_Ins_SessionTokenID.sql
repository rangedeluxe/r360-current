--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionToken_Ins_SessionTokenID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionToken_Ins_SessionTokenID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionToken_Ins_SessionTokenID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionToken_Ins_SessionTokenID 
(
	@parmSessionTokenID	UNIQUEIDENTIFIER,
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmRowsReturned	INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/09/2013
*
* Purpose:	Insert session token by session token id 
*
* Modification History
* 01/09/2013 WI 83233 CRG Write store procedure to replace SQLLogon.SQL_InsertSessionToken(Guid SessionTokenID, Guid SessionID)
* 03/21/2013 WI 90349 JBS Update to 2.0 Release.  Chagne Schema Name.
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO 
		RecHubUser.SessionToken 
		(
			RecHubUser.SessionToken.SessionTokenID, 
			RecHubUser.SessionToken.SessionID
		) 
	VALUES 
	(
		@parmSessionTokenID,
		@parmSessionID
	);	

	SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH