﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_ByUserID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Permissions_Get_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_ByUserID 
(
	@parmUserID	INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/16/2013
*
* Purpose: get permissions by user id
*
* Modification History
* 01/16/2013 WI 85589 CRG Write a store procedure to replace ExecuteSQL(SQLPermissions.SQL_GetUserPermissions(UserID), out dt)
* 02/27/2013 WI 89405 CRG Change Schema for RecHubUser.usp_Permissions_Get_ByUserID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT		
		RecHubUser.[Permissions].PermissionID,
		RecHubUser.[Permissions].PermissionCategory,
		RecHubUser.[Permissions].PermissionName,
		RecHubUser.[Permissions].PermissionDefault,
		RecHubUser.[Permissions].ScriptFile,
        RecHubUser.[Permissions].PermissionMode 
	FROM		
		RecHubUser.[Permissions] 
	INNER JOIN 
		RecHubUser.UserPermissions 
		ON RecHubUser.[Permissions].PermissionID	= RecHubUser.UserPermissions.PermissionID
		AND RecHubUser.UserPermissions.UserID		= @parmUserID
	WHERE		
		RecHubUser.[Permissions].PermissionCategory <> 'System'
	ORDER BY	
		RecHubUser.[Permissions].PermissionCategory,
		RecHubUser.[Permissions].PermissionName;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
