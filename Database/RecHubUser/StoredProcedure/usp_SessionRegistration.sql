--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionRegistration 
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionRegistration ') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_SessionRegistration 
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionRegistration 
(
	@parmSession				XML,
	@parmRefreshEntitlements	BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/16/2014
*
* Purpose: Add record(s) to the session entitlements table. 
*		Add a RA3MSID to the user table if needed.
*
* Modification History
* 05/16/2014 WI 145457 JPB	Created
* 06/29/2014 WI 145457 RDS	Minor syntax updates
* 07/07/2014 WI	145457 KLC	Added refreshing of entitlements for existing session
*							 and added distinct to entitlement select as RAAM
*							 does not protect from resources being setup multiple
*							 times.
* 07/08/2014 WI 145457 KLC	Changed Logon to owning entity of workgroup
* 09/12/2014 WI 165638 RDS	Include User columns (Logon, Entity)
* 11/13/2014 WI 177658 TWE  Add LogonName to Session record when it is created
* 02/24/2015 WI 191266 JBS	Add logic for new ViewBatchDataAheadOfDeposit permission and EndDateKey columns
* 04/10/2015 WI 200412 JBS	Remove columns that are no longer used.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @DefaultViewDays	INT,
		@RA3MSID			UNIQUEIDENTIFIER,
		@SessionID			UNIQUEIDENTIFIER,
		@UserID				INT,
		@Today				VARCHAR(10),
		@ViewAhead			BIT = 0,
		@LocalTransaction	BIT = 0;

SET @parmRefreshEntitlements = ISNULL(@parmRefreshEntitlements, 0)

BEGIN TRY

	/* Before we get started, check to see if the session already exists */
	DECLARE @SessionExists TABLE (SessionExists BIT)
	;WITH SessionID_CTE AS
	(
		SELECT
			SessionInfo.att.value('SessionID[1]','UNIQUEIDENTIFIER') AS SessionID
		FROM 
			@parmSession.nodes('/SessionEntitlements/Session') SessionInfo(att)
	)
	INSERT INTO @SessionExists(SessionExists)
	SELECT DISTINCT 1
	FROM 
		RecHubUser.Session
		INNER JOIN SessionID_CTE ON SessionID_CTE.SessionID = RecHubUser.Session.SessionID;

	IF EXISTS(SELECT 1 FROM @SessionExists)
	BEGIN
		IF @parmRefreshEntitlements <> 1
		BEGIN
			RETURN;
		END
	END
	ELSE IF @parmRefreshEntitlements = 1
	BEGIN
		RETURN;
	END

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpIDs')) 
		DROP TABLE #tmpIDs;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpProcessingDates')) 
		DROP TABLE #tmpProcessingDates;

	/* Get defaults from OLPreferences table */
	SELECT 
		@DefaultViewDays = CAST(DefaultSetting AS INT)
	FROM
		RecHubUser.OLPreferences
	WHERE 
		PreferenceName = 'ViewingDays'
		AND PreferenceGroup = 'Online Setup Options'

	SET @Today = CONVERT(VARCHAR,GETDATE(),101);

	/* Create working tables */
	DECLARE @Session TABLE
	(
		SessionID		UNIQUEIDENTIFIER,
		RA3MSID			UNIQUEIDENTIFIER,
		IPAddress		VARCHAR(64),
		BrowserType		VARCHAR(1024),
		LogonEntityID	INT,
		LogonEntityName	VARCHAR(50),
		LogonName		VARCHAR(24),
		ViewAhead		BIT
	);

	CREATE TABLE #tmpIDs
	(
		SessionID		UNIQUEIDENTIFIER,
		EntityID		INT,
		SiteBankID		INT,
		SiteClientAccountID INT,
		ViewingDays		INT NULL,
		MaximumSearchDays INT NULL,
		StartDateKey	INT NULL,
		EndDateKey		INT	NULL,
		EntityName		VARCHAR(128)
	);

	CREATE TABLE #tmpProcessingDates
	(
		SiteBankID					INT,
		SiteClientAccountID			INT,
		CurrentProcessingDateKey	INT
	);

	/* Parse the XML for the session setup info */
	INSERT INTO @Session(SessionID,RA3MSID,IPAddress,BrowserType,LogonEntityID,LogonEntityName,LogonName,ViewAhead)
	SELECT
		SessionInfo.att.value('SessionID[1]','UNIQUEIDENTIFIER'),
		SessionInfo.att.value('(RA3MSID)[1]','UNIQUEIDENTIFIER'),
		COALESCE(SessionInfo.att.value('(IPAddress)[1]','VARCHAR(64)'),'1.1.1.1'),
		COALESCE(SessionInfo.att.value('(BrowserType)[1]','VARCHAR(128)'),'Undefined'),
		COALESCE(SessionInfo.att.value('(LogonEntityID)[1]','INT'),0),
		COALESCE(SessionInfo.att.value('(LogonEntityName)[1]','VARCHAR(50)'),'Undefined'),
		COALESCE(SessionInfo.att.value('(LogonName)[1]','VARCHAR(24)'),'Undefined'),
		COALESCE(SessionInfo.att.value('(ViewBatchDataAheadOfDeposit)[1]','BIT'),'0')
	FROM 
		@parmSession.nodes('/SessionEntitlements/Session') SessionInfo(att);

	SELECT 
		@RA3MSID = RA3MSID,
		@SessionID = SessionID,
		@ViewAhead = ViewAhead
	FROM 
		@Session;

	/* Now create a temp table with the session info and the list of SiteBankID/SiteClientAccountIDs 
		We will also throw out any invalid siteBankID/ClientAccountIDs that do not match our dimension table*/
	;WITH SessionEntitlements AS
	(
		SELECT DISTINCT
			@SessionID AS SessionID,
			REPLACE(Workgroups.WG.value('@ID','VARCHAR(32)'),'|','.') AS InternalReferenceID,
			Workgroups.WG.value('@EntityID', 'INT') AS EntityID,
			Workgroups.WG.value('@EntityName', 'VARCHAR(128)') AS EntityName
		FROM
			@parmSession.nodes('/SessionEntitlements/ClientAccounts/ClientAccount') Workgroups(WG)
	)
	INSERT INTO #tmpIDs(SessionID,EntityID,SiteBankID,SiteClientAccountID,EntityName)
	SELECT
		S.SessionID,
		SessionEntitlements.EntityID,
		PARSENAME(SessionEntitlements.InternalReferenceID, 2) AS SiteBankID,
		PARSENAME(SessionEntitlements.InternalReferenceID, 1) AS SiteClientAccountID,
		SessionEntitlements.EntityName
	FROM
		SessionEntitlements
		INNER JOIN @Session S ON SessionEntitlements.SessionID = S.SessionID
		INNER JOIN RecHubData.dimClientAccountsView ON PARSENAME(SessionEntitlements.InternalReferenceID, 2) = RecHubData.dimClientAccountsView.SiteBankID 
			AND PARSENAME(SessionEntitlements.InternalReferenceID, 1) = RecHubData.dimClientAccountsView.SiteClientAccountID
		INNER JOIN RecHubData.dimSiteCodes ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
	WHERE 
		RecHubData.dimClientAccountsView.IsActive = 1;

	/* Gather Distinct list of the CurrentProcessingDates for SiteCodeID*/

	INSERT INTO #tmpProcessingDates	(SiteBankID, SiteClientAccountID,CurrentProcessingDateKey)
	SELECT DISTINCT
		#tmpIDs.SiteBankID,
		#tmpIDs.SiteClientAccountID,
		CAST(CONVERT(VARCHAR, RecHubData.dimSiteCodes.CurrentProcessingDate, 112) AS INT) AS CurrentProcessingDateKey
	FROM #tmpIDs
		INNER JOIN RecHubData.dimClientAccountsView ON #tmpIDs.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID 
			AND #tmpIDs.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		INNER JOIN RecHubData.dimSiteCodes ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
	WHERE 
		RecHubData.dimClientAccountsView.IsActive = 1;

	/* Set the viewing days, max search days and start date key */
	UPDATE 
		#tmpIDs
	SET
		ViewingDays = COALESCE(RecHubUser.OLWorkGroups.ViewingDays,RecHubUser.OLEntities.ViewingDays,@DefaultViewDays),
		MaximumSearchDays = COALESCE(RecHubUser.OLWorkGroups.MaximumSearchDays,RecHubUser.OLEntities.MaximumSearchDays,-1),
		StartDateKey = CAST(CONVERT(VARCHAR,DATEADD(DAY,(-1*(COALESCE(RecHubUser.OLWorkGroups.ViewingDays,RecHubUser.OLEntities.ViewingDays,@DefaultViewDays))), @Today),112) AS INT),
		EndDateKey = CurrentProcessingDateKey 
	FROM
		#tmpIDs
		INNER JOIN #tmpProcessingDates ON #tmpIDs.SiteBankID = #tmpProcessingDates.SiteBankID 
			AND #tmpIDs.SiteClientAccountID = #tmpProcessingDates.SiteClientAccountID
		LEFT JOIN RecHubUser.OLWorkGroups ON #tmpIDs.EntityID = RecHubUser.OLWorkGroups.EntityID 
			AND #tmpIDs.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
			AND #tmpIDs.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID 
		LEFT JOIN RecHubUser.OLEntities ON #tmpIDs.EntityID = RecHubUser.OLEntities.EntityID;


	/* Begin a transaction, this really needs to be an all or nothing event */
	BEGIN TRANSACTION; 
	SET @LocalTransaction = 1;
	/* First verify that the RA3MSID exists in the user table */
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE RA3MSID = @RA3MSID)
		INSERT INTO RecHubUser.Users(RA3MSID,LogonName,LogonEntityID,LogonEntityName)
		SELECT 
			@RA3MSID,LogonName,LogonEntityID,LogonEntityName
		FROM 
			@Session;
	ELSE
		UPDATE RecHubUser.Users
		SET 
			LogonName = tmpSession.LogonName,
			LogonEntityID = tmpSession.LogonEntityID,
			LogonEntityName = tmpSession.LogonEntityName
		FROM 
			RecHubUser.Users
		INNER JOIN @Session tmpSession ON RecHubUser.Users.RA3MSID = tmpSession.RA3MSID
		WHERE 
			RecHubUser.Users.LogonName IS NULL 
			OR CAST(RecHubUser.Users.LogonName AS VARBINARY(24)) <> CAST(tmpSession.LogonName AS VARBINARY(24))
			OR RecHubUser.Users.LogonEntityName IS NULL 
			OR CAST(RecHubUser.Users.LogonEntityName AS VARBINARY(50)) <> CAST(tmpSession.LogonEntityName AS VARBINARY(50));	
	
	SELECT @UserID = UserID FROM RecHubUser.Users WHERE RA3MSID = @RA3MSID;

	IF @parmRefreshEntitlements <> 1
	BEGIN
		INSERT INTO RecHubUser.[Session](SessionID,UserID,IPAddress,BrowserType,LogonDateTime,LogonName)
		SELECT
			@SessionID,
			@UserID,
			IPAddress,
			BrowserType,
			GETDATE(),
			LogonName
		FROM 
			@Session;
	END
	ELSE
	BEGIN
		--Refreshing entitlements, wipe out existing ones for this session
		DELETE FROM RecHubUser.SessionClientAccountEntitlements WHERE SessionID = @SessionID;
	END

	/* Insert the complete list of IDs and Keys into the session table */
	INSERT INTO RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		ClientAccountKey,
		ViewingDays,
		MaximumSearchDays,
		StartDateKey,
		EndDateKey,
		ViewAhead,
		EntityName
	)
	SELECT
		SessionID,
		#tmpIDs.EntityID,
		#tmpIDs.SiteBankID,
		#tmpIDs.SiteClientAccountID,
		ClientAccountKey,
		ViewingDays,
		MaximumSearchDays,
		StartDateKey,
		EndDateKey,
		@ViewAhead,
		EntityName
	FROM
		#tmpIDs
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = #tmpIDs.SiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = #tmpIDs.SiteClientAccountID;

	COMMIT TRANSACTION;
	SET @LocalTransaction = 0;
	/* Clean up the work tables */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpIDs')) 
		DROP TABLE #tmpIDs;
END TRY
BEGIN CATCH
	IF @LocalTransaction = 1
		ROLLBACK TRANSACTION;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpIDs')) 
		DROP TABLE #tmpIDs;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH