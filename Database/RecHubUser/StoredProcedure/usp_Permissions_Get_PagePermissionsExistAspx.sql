﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_PagePermissionsExistAspx
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_PagePermissionsExistAspx') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Permissions_Get_PagePermissionsExistAspx
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_PagePermissionsExistAspx 
(
	@parmScriptFile		VARCHAR(255),
	@parmScriptFileAspx	VARCHAR(255)
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/17/2013
*
* Purpose: get page permissions 
*
* Modification History
* 01/17/2013 WI 85593 CRG Write a store procedure to replace ExecuteSQL(SQLPermissions.SQL_PagePermissionsExist(ScriptFile), out dt)
* 01/17/2013 WI 89193 CRG Change Schema for RecHubUser.usp_Permissions_Get_PagePermissionsExistAspx
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT
		@parmScriptFile = LOWER( @parmScriptFile);

	SELECT
		@parmScriptFileAspx = LOWER( @parmScriptFileAspx);

	SELECT DISTINCT 
		RecHubUser.[Permissions].ScriptFile
	FROM	
		RecHubUser.[Permissions]
	WHERE
		LOWER(RecHubUser.[Permissions].ScriptFile)		= @parmScriptFile
		OR LOWER(RecHubUser.[Permissions].ScriptFile)	= @parmScriptFileAspx
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
