﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserMachines_Get_ValidateMachineToken
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserMachines_Get_ValidateMachineToken') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserMachines_Get_ValidateMachineToken;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserMachines_Get_ValidateMachineToken
(
	@parmUserID			INT,
	@parmMachineToken	UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/20/2012
*
* Purpose: Get OL User Machines to Validate Machine Token
*
* Modification History
* 12/19/2012 WI 83229 CRG Write store procedure to replace SQLLogon.SQL_ValidateMachineToken(int userID, Guid machineToken)
* 02/25/2013 WI 88952 CRG Change Schema for RecHubUser.usp_OLUserMachines_Get_ValidateMachineToken
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		COUNT(*) AS theCount
    FROM	
		RecHubUser.OLUserMachines
    WHERE		
		RecHubUser.OLUserMachines.UserID			= @parmUserID
		AND RecHubUser.OLUserMachines.MachineToken	= @parmMachineToken;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
