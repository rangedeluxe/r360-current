--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserPreferences_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserPreferences_Get_ByUserID') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserPreferences_Get_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserPreferences_Get_ByUserID 
(
	@parmUserID INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences Descriptions for Preference By User ID
*
* Modification History
* 12/12/2012 WI 76553 CRG Write store procedure for cPreferencesDAL.GetOLUserPreferences(int, out DataTable)
* 02/22/2013 WI 88877 CRG Change Schema for RecHubUser.usp_OLUserPreferences_Get_ByUserID
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT		
		RecHubUser.OLPreferences.OLPreferenceID,
		RecHubUser.OLPreferences.PreferenceName,
		RecHubUser.OLPreferences.[Description], 
		RecHubUser.OLPreferences.DefaultSetting,
		RecHubUser.OLUserPreferences.OLUserPreferenceID,
		RecHubUser.OLUserPreferences.UserSetting
	FROM		
		RecHubUser.OLPreferences
	LEFT OUTER JOIN
		RecHubUser.OLUserPreferences
		ON RecHubUser.OLUserPreferences.UserID			= @parmUserID
		AND RecHubUser.OLUserPreferences.OLPreferenceID	= RecHubUser.OLPreferences.OLPreferenceID
    WHERE		
		RecHubUser.OLPreferences.IsOnline		= 1
		AND RecHubUser.OLPreferences.IsSystem	= 0
    ORDER BY	
		RecHubUser.OLPreferences.PreferenceName ASC;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

