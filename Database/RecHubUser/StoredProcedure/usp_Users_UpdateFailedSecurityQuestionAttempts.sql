--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_UpdateFailedSecurityQuestionAttempts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_UpdateFailedSecurityQuestionAttempts') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_UpdateFailedSecurityQuestionAttempts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_UpdateFailedSecurityQuestionAttempts
(
	@parmAttempts INT,
	@parmUserID  INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/6/2011
*
* Purpose: Update failed security question attempts
*
* Modification History
* 10/06/2011 CR 47246 WJS	Created
* 03/14/2013 WI 90813 CRG	Update Stored Procedure OLTA.usp_Users_UpdateFailedSecurityQuestionAttempts for 2.0
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	UPDATE 
		RecHubUser.Users
	SET 
		RecHubUser.Users.FailedSecurityQuestionAttempts = @parmAttempts
	WHERE  
		RecHubUser.Users.UserID = @parmUserID
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

                   
