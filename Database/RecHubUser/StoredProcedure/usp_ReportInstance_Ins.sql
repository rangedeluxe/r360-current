--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ReportInstance_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_ReportInstance_Ins') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_ReportInstance_Ins;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_ReportInstance_Ins 
(
      @parmInstanceID			UNIQUEIDENTIFIER,
      @parmUserID				INT,
      @parmReportID				INT,
      @parmReportStatus			TINYINT,
      @parmStartTime			DATETIME,
      @parmEndTime				DATETIME,
      @parmReportInstanceKey	BIGINT OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Inserts new Report Instance database entries
*
* Modification History
* Created
* 05/06/2013 WI 107444 KLC	Created.
****************************************************************************** */
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY   

	IF EXISTS(SELECT 1 FROM RecHubUser.ReportInstance 
				WHERE RecHubUser.ReportInstance.InstanceID = @parmInstanceID )
		BEGIN
			DECLARE @formatInstanceID VARCHAR(50) = CONVERT(VARCHAR(50), @parmInstanceID);
			RAISERROR('There already is a database entry for InstanceID: %d.',16, 1,@formatInstanceID);
		END
		
	INSERT INTO RecHubUser.ReportInstance(InstanceID, UserID, ReportId, ReportStatus, StartTime, EndTime)
	VALUES(@parmInstanceID, @parmUserID, @parmReportID, @parmReportStatus, @parmStartTime, @parmEndTime);

	SET @parmReportInstanceKey = SCOPE_IDENTITY()
END TRY

BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
