--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_BrandingSchemes_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_BrandingSchemes_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_BrandingSchemes_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_BrandingSchemes_Upd
(
	@parmBrandingSchemeID			INT,
	@parmBrandingSchemeName			VARCHAR(32),
	@parmBrandingSchemeDescription	VARCHAR(256),
	@parmBrandingSchemeFolder		VARCHAR(256),
	@parmIsActive					BIT, 
	@parmRowsReturned				INT OUTPUT,
	@parmExternalID1				VARCHAR(32),
	@parmExternalID2				VARCHAR(32)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Update BrandingSchemes record
*
* Modification History
* 01/18/2012 CR 49643 JCS   Created
* 02/29/2012 CR 51102 JCS   Added ExternalID1, ExternalID2 fields to update.
* 03/27/2013 WI 90591 JBS	Update to 2.0 release. Change schema name.
*							Change proc name from usp_BrandingSchemesUpdate.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	UPDATE RecHubUser.BrandingSchemes
	SET	BrandingSchemeName = @parmBrandingSchemeName,
		BrandingSchemeDescription = @parmBrandingSchemeDescription,
		BrandingSchemeFolder = @parmBrandingSchemeFolder,
		IsActive = @parmIsActive,
		ExternalID1 = @parmExternalID1,
		ExternalID2 = @parmExternalID2,
		ModifiedBy = SUSER_SNAME(),
		ModificationDate = GetDate()
	WHERE BrandingSchemeID = @parmBrandingSchemeID;

	SELECT @parmRowsReturned = @@ROWCOUNT;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
