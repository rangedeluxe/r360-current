--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_AdjustStartDateForViewingDays
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_AdjustStartDateForViewingDays') IS NOT NULL
	   DROP PROCEDURE RecHubUser.usp_AdjustStartDateForViewingDays;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_AdjustStartDateForViewingDays
(
	@parmSessionID UNIQUEIDENTIFIER,
	@parmSiteBankID INT,
	@parmSiteClientAccountID INT,
	@parmDepositDateStart DATETIME,
	@parmDepositDateEnd DATETIME = NULL,
	@parmStartDateKey INT OUT,
	@parmEndDateKey INT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2014
*
* Purpose: Convert dates to key, check start date key against entitlement table.
*
* Modification History
* 05/11/2014 WI 149471 JPB	Created
* 02/24/2015 WI 191268 JBS	Check new ViewAhead Permission and adjust EndDateKey if necessary
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDateKey INT,
		@EndDateKey INT,
		@ViewAhead BIT;

BEGIN TRY

	/*  Convert incoming values			*/
	SELECT 
		@parmStartDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateStart,112) AS INT),
		@parmEndDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateEnd,112) AS INT);
	/*  Pull out the user's access		*/
	SELECT TOP(1)
		@StartDateKey = StartDateKey,
		@EndDateKey = EndDateKey,
		@ViewAhead = ViewAhead
	FROM 
		RecHubUser.SessionClientAccountEntitlements 
	WHERE 
		SessionID = @parmSessionID 
		AND SiteBankID = @parmSiteBankID 
		AND SiteClientAccountID = @parmSiteClientAccountID;

	/*  Make adjustments				*/
	IF	(@parmStartDateKey <= @StartDateKey)
	BEGIN
		SELECT
			@parmStartDateKey = @StartDateKey;
	END
	IF (@parmEndDateKey IS NOT NULL AND @ViewAhead = 0 AND @parmEndDateKey > @EndDateKey )
	BEGIN
		SELECT
			@parmEndDateKey = @EndDateKey;	-- No ViewAhead permission, Bump it back to CurrentProcessingdate
	END

END TRY
BEGIN CATCH
	   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
