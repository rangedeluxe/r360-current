--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Permissions_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get
(
	@parmIncludeSystem BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Get All Permissions
*
*
* Parameter Information
*	@parmIncludeSystem - Determines if system permissions are returned.
*		0 - Return all but System permissions
*		1 - Return all permissions
*
*
* Modification History
* 01/15/2010 CR 28696 JMC	Created
* 01/16/2013 WI 85630 CRG	Write a store procedure to replace ExecuteSQL(SQLPermissions.SQL_GetAllPermissions(), out dt)
* 02/25/2013 WI 88977 CRG	Change Schema for RecHubUser.usp_Permissions_Get_AllPermissions
* 04/04/2013 WI 90771 JPB	Updated to 2.0.
*							Moved to RecHubUser schema.
*							Merged 85630/88977.
*							Added @parmIncludeSystem
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	SELECT		
		RecHubUser.[Permissions].PermissionID,
		RecHubUser.[Permissions].PermissionCategory,
		RecHubUser.[Permissions].PermissionName,
		RecHubUser.[Permissions].ScriptFile,
		RecHubUser.[Permissions].PermissionDefault,
		RecHubUser.[Permissions].PermissionMode
    FROM		
		RecHubUser.[Permissions] 
	WHERE 
		(
			@parmIncludeSystem = 0
			AND RecHubUser.[Permissions].PermissionCategory <> 'System'
		)
		OR
		(
			@parmIncludeSystem = 1
			AND RecHubUser.[Permissions].PermissionCategory = RecHubUser.[Permissions].PermissionCategory
		)
    ORDER BY
		RecHubUser.[Permissions].PermissionCategory, 
		RecHubUser.[Permissions].PermissionName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
