﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLClientAccountUsers_Del_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLClientAccountUsers_Del_ByUserID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_OLClientAccountUsers_Del_ByUserID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLClientAccountUsers_Del_ByUserID 
(
	@parmUserID 		INT,
	@parmRowsReturned	INT = 0		OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/21/2013
*
* Purpose: Delete OL Client Account Users by User ID
*
* Modification History
* 01/21/2013 WI 85605 CRG Write a store procedure to replace ExecuteSQL(SQLUser.SQL_DeleteOLLockboxUser(UserID))
* 02/25/2013 WI 88942 CRG Change Schema for RecHubUser.usp_OLClientAccountUsers_Del_ByUserID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	DELETE FROM	
		RecHubUser.OLClientAccountUsers 
	WHERE	
		RecHubUser.OLClientAccountUsers.UserID = @parmUserID

	SELECT	
		@parmRowsReturned = @@ROWCOUNT    
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH