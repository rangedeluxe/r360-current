--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_Get_ByEventRuleID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_ByEventRuleID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_ByEventRuleID
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_ByEventRuleID
(
	@parmEventRuleID INT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/02/2013
*
* Purpose: Get Users that are tied to Alerts
*
* Modification History
* 07/02/2013  WI  107696	EAS	Created
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

	SELECT RecHubUser.Users.UserID,
		   RecHubUser.Users.FirstName,
		   RecHubUser.Users.LastName,
		   RecHubUser.Users.EmailAddress
	FROM RecHubUser.Users
	INNER JOIN RecHubAlert.Alerts ON (RecHubAlert.Alerts.UserID = RecHubUser.Users.UserID)
	WHERE RecHubAlert.Alerts.EventRuleID = @parmEventRuleID

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH