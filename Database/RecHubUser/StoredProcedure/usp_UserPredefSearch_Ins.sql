--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_UserPredefSearch_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_UserPredefSearch_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_UserPredefSearch_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_UserPredefSearch_Ins 
(
	@parmUserID			INT,
	@parmPreDefSearchID UNIQUEIDENTIFIER, 
	@parmIsDefault		BIT,
	@parmRowsReturned	INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/17/2011
*
* Purpose: Insert row into RecHubUser.UserPredefSearch
*
* Modification History
* 01/17/2011 CR 31746  JNE	Created
* 08/29/2011 CR 31746 JNE	Updated Error handling
* 08/30/2011 CR 31746 JNE	Updated Copyright and added SQL to set error message
* 04/04/2013 WI 90807 JBS	Update to 2.0 release.  Change schema name
******************************************************************************/

SET ARITHABORT ON;
SET XACT_ABORT ON;

SET @parmRowsReturned = 0;

BEGIN TRY
	BEGIN TRANSACTION
		INSERT INTO RecHubUser.UserPredefSearch
		(
			RecHubUser.UserPredefSearch.UserID,
			RecHubUser.UserPredefSearch.PreDefSearchID,
			RecHubUser.UserPredefSearch.IsDefault,
			RecHubUser.UserPredefSearch.CreatedBy,
			RecHubUser.UserPredefSearch.CreationDate,
			RecHubUser.UserPredefSearch.ModifiedBy,
			RecHubUser.UserPredefSearch.ModificationDate	
		)
		VALUES 
		(
			@parmUserID,
			@parmPreDefSearchID,
			@parmIsDefault,
			SUSER_SNAME(),
			GetDate(),
			SUSER_SNAME(),
			GetDate() 
		);

		SELECT @parmRowsReturned = @@ROWCOUNT
		--uncheck other queries that would have been default before this update for the user.
			IF @parmRowsReturned > 0 AND @parmIsDefault = 'True'
				BEGIN
					UPDATE RecHubUser.UserPredefSearch
					SET RecHubUser.UserPredefSearch.IsDefault = 'False',
						RecHubUser.UserPredefSearch.ModifiedBy = SUSER_SNAME(),
						RecHubUser.UserPredefSearch.ModificationDate = GetDate()
					FROM RecHubUser.UserPredefSearch
					WHERE RecHubUser.UserPredefSearch.UserID = @parmUserID
						AND RecHubUser.UserPredefSearch.IsDefault = 'True'
						AND RecHubUser.UserPredefSearch.PreDefSearchID <> @parmPredefSearchID;
				END
	COMMIT TRANSACTION;
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
	IF @@NESTLEVEL = 1
		BEGIN
			EXEC RecHubCommon.usp_WfsRethrowException;
		END
	ELSE
		BEGIN
			DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorProcedure	 NVARCHAR(200),
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine		 INT;
		
			SELECT	@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
					@ErrorLine = ERROR_LINE();

			SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
		END
END CATCH
