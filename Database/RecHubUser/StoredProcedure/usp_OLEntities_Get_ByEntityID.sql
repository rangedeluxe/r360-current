--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLEntities_Get_ByEntityID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLEntities_Get_ByEntityID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLEntities_Get_ByEntityID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLEntities_Get_ByEntityID(
	@parmUserId			INT,
	@parmEntityId		INT
	)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 06/12/2014
*
* Purpose: Reads the corresponding record from the RecHubUser.OLEntities table 
*
* Modification History
* 06/12/2014 WI 150295 RDS	Created
* 01/19/2015 WI 184873 RDS	Add Billing Fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	RecHubUser.OLEntities.EntityID,
			RecHubUser.OLEntities.DisplayBatchID,
			RecHubUser.OLEntities.CheckImageDisplayMode AS PaymentImageDisplayMode,
			RecHubUser.OLEntities.DocumentImageDisplayMode,
			RecHubUser.OLEntities.ViewingDays,
			RecHubUser.OLEntities.MaximumSearchDays,
			RecHubUser.OLEntities.BillingAccount,
			RecHubUser.OLEntities.BillingField1,
			RecHubUser.OLEntities.BillingField2
	FROM	
		RecHubUser.OLEntities
	WHERE
		RecHubUser.OLEntities.EntityID = @parmEntityId

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

