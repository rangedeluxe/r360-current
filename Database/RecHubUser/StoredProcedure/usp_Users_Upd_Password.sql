--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_Upd_Password
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Upd_Password') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Upd_Password
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Upd_Password
(
	@parmUserID					INT,
	@parmEncryptedPassword		VARCHAR(44),
	@parmIsFirstTime			BIT,
	@parmHistoryRetentionDays	INT,
	@parmIsForgotPassword		BIT,
	@parmForgotPasswordMinutes	INT,
	@parmSQLErrorNumber			INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Change Password
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 07/25/2011 CR 45440 JPB	Changed @parmEncryptedPassword length to 44.
* 08/08/2011 CR 45956 WJS 	Added support for password history
* 03/01/2013 WI 90009 CRG 	Change Schema for RecHubUser.usp_UpdateUserPassword
******************************************************************************/
SET NOCOUNT ON 
DECLARE  
	@passwordExpiredTime DATETIME,
	@ReturnValue INT = 0

BEGIN TRY

	SET @parmSQLErrorNumber = 0
	SET @passwordExpiredTime = NULL

	IF @parmIsForgotPassword  = 1
	BEGIN
		SET @passwordExpiredTime = DATEADD(mi,@parmForgotPasswordMinutes, GETDATE())
	END    
	
	EXEC	
		RecHubUser.usp_RemoveUserPasswordHistory 
			@parmUserID, 
			@parmHistoryRetentionDays

	EXEC 
		@ReturnValue =  RecHubUser.usp_UserPasswordExistsInHistory
			@parmUserID,
			@parmEncryptedPassword

	SET @parmSQLErrorNumber = 0

	UPDATE	
		RecHubUser.Users 
	SET		
		RecHubUser.Users.[Password] = @parmEncryptedPassword,
		RecHubUser.Users.PasswordSet = GETDATE() ,
		RecHubUser.Users.IsFirstTime = @parmIsFirstTime, 
		RecHubUser.Users.FailedLogonAttempts = 0 ,
		RecHubUser.Users.IsActive = 1, 
		RecHubUser.Users.PasswordExpirationTime = @passwordExpiredTime,
		RecHubUser.Users.ModificationDate=GETDATE(),
		RecHubUser.Users.ModifiedBy=SUSER_SNAME()
	WHERE	
		UserID = @parmUserID

	EXEC 
		RecHubUser.usp_UserPasswordHistory_Ins
			@parmUserID, 
			@parmEncryptedPassword
END TRY
BEGIN CATCH
	IF 
	(	
		ERROR_MESSAGE() LIKE '%Password Exists%'
	)
	BEGIN
		SET @parmSQLErrorNumber = 1
	END
	ELSE
	BEGIN
		SET @parmSQLErrorNumber = 2
	END
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

                   
