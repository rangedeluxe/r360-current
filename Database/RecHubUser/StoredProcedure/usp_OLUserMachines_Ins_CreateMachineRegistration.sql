--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserMachines_Ins_CreateMachineRegistration
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserMachines_Ins_CreateMachineRegistration') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserMachines_Ins_CreateMachineRegistration;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserMachines_Ins_CreateMachineRegistration
(
	@parmUserID			INT,
	@parmMachineToken	UNIQUEIDENTIFIER,
	@parmRowsReturned	INT = 0 OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/20/2012
*
* Purpose: Add a OL User Machine record to Create Machine Registration 
*
* Modification History
* 12/19/2012 WI 83225 CRG Write store procedure to replace SQLLogon.SQL_CreateMachineRegistration(int userID, Guid machineToken)	
* 02/22/2013 WI 88897 CRG Change Schema for RecHubUser.usp_OLUserMachines_Ins_CreateMachineRegistration
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	INSERT INTO RecHubUser.OLUserMachines 
		(
			RecHubUser.OLUserMachines.OLUserMachineID,
			RecHubUser.OLUserMachines.UserID,
			RecHubUser.OLUserMachines.MachineToken,
			RecHubUser.OLUserMachines.CreationDate,
			RecHubUser.OLUserMachines.CreatedBy,
			RecHubUser.OLUserMachines.ModificationDate,
			RecHubUser.OLUserMachines.ModifiedBy
		) 
	VALUES 
		(
			NEWID(),
			@parmUserID,
			@parmMachineToken,
			GETDATE(),
			SUSER_SNAME(),
			GETDATE(),
			SUSER_SNAME()
		);

	SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
