--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_SelOLUserQuestionAnswer
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SelOLUserQuestionAnswer') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_SelOLUserQuestionAnswer
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SelOLUserQuestionAnswer
(
	@parmUserID				INT,
	@parmSecretQuestionID	UNIQUEIDENTIFIER,
	@parmSecretAnswer		VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 9/27/2011
*
* Purpose: Get user question/answer
*
* Modification History
* 09/27/2011 CR 47026 WJS	Created
* 02/22/2013 WI 88891 CRG Change Schema for RecHubUser.usp_SelOLUserQuestionAnswer
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	SELECT 
		COUNT(*) AS FOUND 
	FROM 
		RecHubUser.OLUserQuestions 
	WHERE 
		RecHubUser.OLUserQuestions.UserId				= @parmUserID
		AND RecHubUser.OLUserQuestions.OLUSERQUESTIONID	= @parmSecretQuestionID
		AND RecHubUser.OLUserQuestions.AnswerText		= @parmSecretAnswer
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

                   
