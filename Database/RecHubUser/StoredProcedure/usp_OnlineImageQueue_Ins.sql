--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OnlineImageQueue_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OnlineImageQueue_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OnlineImageQueue_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OnlineImageQueue_Ins 
(
    @parmOnlineImageQueueID   UNIQUEIDENTIFIER,
    @parmSessionUserID        INT,
    @parmUserFileName         VARCHAR(50),
    @parmDisplayScannedCheck  BIT,
    @parmRowsAffected         INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Insert into Image Queue table
*
* Modification History
* 01/24/2013 WI 86257 TWE   Created by converting embedded SQL into stored procedure
*					  JBS	Changed Schema to RecHubUser.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    INSERT INTO RecHubUser.OnlineImageQueue 
    (
        OnlineImageQueueID,
        UserID,
        ProcessStatusID,
        UserFileName,
        DisplayScannedCheck
    )
    VALUES
    (
        @parmOnlineImageQueueID,
        @parmSessionUserID, 
        1, 
        @parmUserFileName,
        @parmDisplayScannedCheck
    );
    SELECT @parmRowsAffected = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH