﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionActivityLog_Upd_SetWebDeliveredFlag
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionActivityLog_Upd_SetWebDeliveredFlag') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionActivityLog_Upd_SetWebDeliveredFlag
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionActivityLog_Upd_SetWebDeliveredFlag 
(
	@parmOnlineImageQueueID		UNIQUEIDENTIFIER,
	@parmInternetDelivered		BIT,
	@parmRowsReturned			INT = 0	OUTPUT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/14/2013
*
* Purpose: Set Web Delivered Flag for Session Activity Log 
*
* Modification History
* 01/14/2013 WI 85321 CRG Write store procedure to replace ExecuteSQL(SQLSessionLog.SQL_SetWebDeliveredFlag(OnlineImageQueueID, InternetDelivered)
* 03/06/2013 WI 90330 CRG Change Schema for SessionActivityLog in RecHubUser.usp_SessionActivityLog_Upd_SetWebDeliveredFlag
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	UPDATE	
		RecHubUser.SessionActivityLog 
	SET		
		RecHubUser.SessionActivityLog.deliveredtoweb = @parmInternetDelivered	 
	WHERE	
		RecHubUser.SessionActivityLog.OnlineImageQueueID = @parmOnlineImageQueueID;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;     
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
