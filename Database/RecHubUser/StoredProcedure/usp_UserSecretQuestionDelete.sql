--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_UserSecretQuestionDelete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_UserSecretQuestionDelete') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_UserSecretQuestionDelete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_UserSecretQuestionDelete
(
	@parmUserID			INT,
	@parmRowsReturned	INT = 0 OUTPUT 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 5/22/2012
*
* Purpose: Delete secret questions for given user id
*
* Modification History
* 05/22/2012 CR 53021 JNE	Created
* 02/22/2013 WI 88820 CRG Change Schema for RecHubUser.usp_UserSecretQuestionDelete
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DELETE FROM 	
		RecHubUser.OLUserQuestions
	WHERE 	
		RecHubUser.OLUserQuestions.UserID = @parmUserID;

	SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
