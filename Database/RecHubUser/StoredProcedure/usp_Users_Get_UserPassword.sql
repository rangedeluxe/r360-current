﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Users_Get_UserPassword
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_UserPassword') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Users_Get_UserPassword
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_UserPassword 
(
	@parmUserID	INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/23/2013
*
* Purpose: Get User Password
*
* Modification History
* WI 85603 CRG 01/23/2013  
*	Write a store procedure to replace ExecuteSQL(SQLUser.SQL_GetUserPassword(UserID), out dt)
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		[password]
	FROM	
		RecHubUser.Users 
	WHERE	
		RecHubUser.Users.UserID = @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
