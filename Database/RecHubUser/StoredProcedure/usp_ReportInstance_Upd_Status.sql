--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ReportInstance_Upd_Status
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_ReportInstance_Upd_Status') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_ReportInstance_Upd_Status;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_ReportInstance_Upd_Status
(
      @parmInstanceID		UNIQUEIDENTIFIER,
      @parmReportStatus		TINYINT,
      @parmEndTime			DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Updates Report Instance database entry's status and end time
*
* Modification History
* Created
* 05/06/2013 WI 107445 KLC	Created.
*******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY
	UPDATE	RecHubUser.ReportInstance 
	SET		RecHubUser.ReportInstance.ReportStatus =  COALESCE(@parmReportStatus, RecHubUser.ReportInstance.ReportStatus),
			RecHubUser.ReportInstance.EndTime = COALESCE(@parmEndTime, RecHubUser.ReportInstance.EndTime)
	WHERE	RecHubUser.ReportInstance.InstanceID = @parmInstanceID;
	
	IF @@ROWCOUNT = 0
	BEGIN
		DECLARE @errorInstanceID VARCHAR(50) = CONVERT(VARCHAR(50), @parmInstanceID);
		RAISERROR('There is not a database entry for InstanceID: %d.', 16, 1, @errorInstanceID);
	END
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH