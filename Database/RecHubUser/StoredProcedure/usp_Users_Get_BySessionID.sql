﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Users_Get_BySessionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_BySessionID') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_Users_Get_BySessionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_BySessionID
(
	@parmSessionID	UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/02/2013
*
* Purpose: Get User Fields
*
* Modification History
* 01/02/2013 WI 83450 CRG Write a store procedure to replace SQLUser.SQL_GetUserBySessionID(SessionID)
* 04/10/2015 WI 200431 JBS	Remove columns not needed and add permissions
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		RecHubUser.Users.UserID,
		RecHubUser.Users.LogonName,
		RecHubUser.Users.CreationDate,
		RecHubUser.Users.CreatedBy,
		RecHubUser.Users.ModificationDate,
		RecHubUser.Users.ModifiedBy
	FROM	
		RecHubUser.[Session] 
	INNER JOIN 
		RecHubUser.Users 
		ON RecHubUser.[Session].UserID = RecHubUser.Users.UserID
    WHERE	
		RecHubUser.[Session].SessionID = @parmSessionID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
