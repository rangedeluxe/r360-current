﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Users_Ins_OlClientAccountUser
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Ins_OlClientAccountUser') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Users_Ins_OlClientAccountUser
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Ins_OlClientAccountUser 
(
	@parmOLClientAccountID	UNIQUEIDENTIFIER, 
	@parmUserID				INT, 
	@parmLogonName			VARCHAR(24), 
	@parmRowsReturned		INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/23/2013
*
* Purpose: 
*
* Modification History
* 01/23/2013 WI 85606 CRG Write a store procedure to replace ExecuteSQL(SQLUser.SQL_InsertOLClientAccountUser(OLClientAccountID, UserID, LogonName), out RowsAffected)
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO	RecHubUser.OLClientAccountUsers 
	( 
		RecHubUser.OLClientAccountUsers.UserID,
		RecHubUser.OLClientAccountUsers.OLClientAccountID,
		RecHubUser.OLClientAccountUsers.CreationDate,
		RecHubUser.OLClientAccountUsers.CreatedBy,
		RecHubUser.OLClientAccountUsers.ModificationDate,
		RecHubUser.OLClientAccountUsers.ModifiedBy
	) 
	VALUES 
	(
		@parmUserID, 
        @parmOLClientAccountID, 
		GETDATE(),
        @parmLogonName, 
        GETDATE(),
        @parmLogonName
	);

	SELECT	@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
