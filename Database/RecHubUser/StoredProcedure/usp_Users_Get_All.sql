--WFSScriptProcessorSystemName RecHub 
--WFSScriptProcessorPermissions <Permissions> 
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubConfig">EXECUTE</Permission> 
--WFSScriptProcessorPermissions </Permissions> 
--WFSScriptProcessorSchema RecHubUser 
--WFSScriptProcessorStoredProcedureName usp_Users_Get_All
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_Get_All') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_Get_All
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Users_Get_All

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 08/12/2013
*
* Purpose: Get All Users
*
* Modification History
* 08/12/2013 WI 111528  KLC	Created
* 08/05/2014 WI 157181  LA  Modified
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

		
	SELECT
		RecHubUser.Users.UserID,
        RecHubUser.Users.LogonName,
        RecHubUser.Users.RA3MSID
	FROM 
		RecHubUser.Users
	ORDER BY
		RecHubUser.Users.LogonName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
