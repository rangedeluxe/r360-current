--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_Permissions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_Permissions_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Permissions_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_Permissions_Get
(
	@parmRowsReturned		INT OUTPUT,
	@parmErrorDescription	VARCHAR(2000) OUTPUT,
	@parmSQLErrorNumber		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/22/2009
*
* Purpose: Get all permissions
*		   
*
* Modification History
* 04/22/2009 CR 25817 JNE	Created
* 03/26/2031 WI 90565 JPB	Upgraded to 2.0.
*							Renamed and moved to RecHubUser schema.
******************************************************************************/
SET NOCOUNT ON;

SET @parmRowsReturned = 0;
SET @parmErrorDescription = '';
SET @parmSQLErrorNumber = 0;

BEGIN TRY
	SELECT	RecHubUser.[Permissions].PermissionName, 
			RecHubUser.[Permissions].[PermissionCategory], 
			RecHubUser.[Permissions].[PermissionID], 
			RecHubUser.[Permissions].[PermissionDefault]
	FROM	RecHubUser.[Permissions]
	ORDER BY 
			RecHubUser.[Permissions].[PermissionCategory], 
			RecHubUser.[Permissions].[PermissionName];

	SELECT @parmRowsReturned = @@ROWCOUNT;
	RETURN 0;
END TRY
BEGIN CATCH
	SELECT @parmSQLErrorNumber = @@ERROR;
	SET @parmErrorDescription = 'An error occured in the execution of usp_API_Permissions_Get';
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 1;
END CATCH
