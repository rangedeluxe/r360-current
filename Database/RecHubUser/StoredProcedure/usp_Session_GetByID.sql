--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Session_GetByID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_GetByID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Session_GetByID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_GetByID
(
	@parmSessionID	UNIQUEIDENTIFIER 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Get Session record by session ID
*
* Modification History
* 01/15/2010 CR 28687 JMC	Created
* 02/27/2013 WI 89577 CRG	Change Schema for RecHubUser.usp_Session_GetByID
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	SELECT 
		RecHubUser.[Session].SessionID,
		RecHubUser.[Session].UserID,
		RecHubUser.[Session].IPAddress,
		RecHubUser.[Session].BrowserType,
		RecHubUser.[Session].LogonDateTime,
		RecHubUser.[Session].LastPageServed,
		RecHubUser.[Session].PageCounter,
		RecHubUser.[Session].IsSuccess,
		RecHubUser.[Session].IsSessionEnded,
		RecHubUser.[Session].LogonName,
		RecHubUser.[Session].OLOrganizationID,
		RecHubUser.[Session].OrganizationCode,
		RecHubUser.[Session].IsRegistered
	FROM 
		RecHubUser.[Session] 
	WHERE 
		RecHubUser.[Session].SessionID = @parmSessionID
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

