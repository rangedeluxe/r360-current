--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserPreferences_Get_Count
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserPreferences_Get_Count') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserPreferences_Get_Count;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserPreferences_Get_Count 
(
	@parmOLPreferenceID UNIQUEIDENTIFIER,
	@parmUserID			INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/13/2012
*
* Purpose: Get count of User Preferences By OL Preference ID and User ID
*
* Modification History
* 12/14/2012 WI 76548 CRG Write store procedure for cPreferencesDAL.GetOLUserPreferenceCount(Guid, int, out DataTable)
* 02/22/2013 WI 88882 CRG Change Schema for RecHubUser.usp_OLUserPreferences_Get_Count
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		COUNT(*) AS OLUserPreference 
    FROM	
		RecHubUser.OLUserPreferences
    WHERE	
		RecHubUser.OLUserPreferences.OLPreferenceID = @parmOLPreferenceID
		AND RecHubUser.OLUserPreferences.UserID		= @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;

