--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLWorkgroups_Get_DisplayModeByBankWorkgroupEntity
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLWorkgroups_Get_DisplayModeByBankWorkgroupEntity') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLWorkgroups_Get_DisplayModeByBankWorkgroupEntity
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLWorkgroups_Get_DisplayModeByBankWorkgroupEntity 
(
    @parmBankID INT,
    @parmSessionID UNIQUEIDENTIFIER,
    @parmClientAccountID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 07/08/2014
*
* Purpose: Return DisplayModes
*
* Modification History
* 07/08/2014 JBS 152341 Created
* 07/09/2014 WI 152341 KLC	Changed from LogonEntityID to workgroup's EntityID
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

    SELECT 
        CASE 
          WHEN RecHubUser.OLWorkgroups.CheckImageDisplayMode > 0 
			THEN RecHubUser.OLWorkgroups.CheckImageDisplayMode 
			ELSE RecHubUser.OLEntities.CheckImageDisplayMode 
        END As CheckImageDisplayMode,
        CASE 
          WHEN RecHubUser.OLWorkgroups.DocumentImageDisplayMode > 0 
			THEN RecHubUser.OLWorkgroups.DocumentImageDisplayMode 
			ELSE RecHubUser.OLEntities.DocumentImageDisplayMode 
        END As DocumentImageDisplayMode 
    FROM RecHubUser.OLWorkgroups 
        INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID 
				AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
		INNER JOIN RecHubUser.OLEntities 
			ON RecHubUser.OLEntities.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID 
     WHERE 
        RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmBankID
        AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmClientAccountID
		AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

