﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionActivityLog_Ins_ByActivityLogID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID 
(
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmModuleName		VARCHAR(50),
	@parmActivityCode	INT,
	@parmRowsAffected	INT = 0	OUTPUT,
	@parmActivityID		BIGINT = 0 OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/14/2013
*
* Purpose: Add Session Activity Log by Activity Log ID
*
* Modification History
* 01/14/2013 WI 85319 CRG Write store procedure to replace ExecuteSQL(SQLSessionLog.SQL_InsertSessionActivityLog(ActivityLogID, SessionID, ModuleName, ActivityCode), out RowsAffected)
* 03/06/2013 WI 91010 CRG Change Schema for SessionActivityLog in RecHubUser.usp_SessionActivityLog_Ins_ByActivityLogID,
*						  Added Output Parm	@parmActivityID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO		
		RecHubUser.SessionActivityLog 
		( 
			RecHubUser.SessionActivityLog.ActivityCode, 
			RecHubUser.SessionActivityLog.SessionID, 
			RecHubUser.SessionActivityLog.DeliveredToWeb, 
			RecHubUser.SessionActivityLog.ModuleName, 
			RecHubUser.SessionActivityLog.ItemCount, 
			RecHubUser.SessionActivityLog.DateTimeEntered, 
			RecHubUser.SessionActivityLog.BankID, 
			RecHubUser.SessionActivityLog.ClientAccountID
		) 
	VALUES 
		(
			@parmActivityCode,
			@parmSessionID, 
			1,
			@parmModuleName,
			1, 
			GETDATE(), 
			-1, 
			-1
		);

	SELECT 
		@parmRowsAffected = @@ROWCOUNT;
    SELECT 
        @parmActivityID = SCOPE_IDENTITY();

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
