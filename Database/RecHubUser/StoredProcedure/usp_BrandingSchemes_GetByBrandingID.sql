--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_BrandingSchemes_GetByBrandingID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_BrandingSchemes_GetByBrandingID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_BrandingSchemes_GetByBrandingID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_BrandingSchemes_GetByBrandingID
(
	@parmBrandingSchemeID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Return BrandingSchemes record with specified BrandingSchemeID
*
* Modification History
* 01/18/2012 CR 49639 JCS   Created
* 02/29/2012 CR 51104 JCS   Added ExternalID1, ExternalID2 fields to returnset.
* 03/12/2013 WI 90588 CRG   Update Stored Procedure RecHubUser.usp_BrandingSchemes_GetByBrandingID for 2.0
******************************************************************************/
SET NOCOUNT ON
BEGIN TRY
	SELECT	
		RecHubUser.BrandingSchemes.BrandingSchemeID,
		RecHubUser.BrandingSchemes.BrandingSchemeName,
		RecHubUser.BrandingSchemes.BrandingSchemeDescription,
		RecHubUser.BrandingSchemes.BrandingSchemeFolder,
		RecHubUser.BrandingSchemes.IsActive,
		RecHubUser.BrandingSchemes.ExternalID1,
		RecHubUser.BrandingSchemes.ExternalID2,
		RecHubUser.BrandingSchemes.CreationDate,
		RecHubUser.BrandingSchemes.CreatedBy,
		RecHubUser.BrandingSchemes.ModificationDate,
		RecHubUser.BrandingSchemes.ModifiedBy
	FROM 
		RecHubUser.BrandingSchemes
	WHERE 
		RecHubUser.BrandingSchemes.BrandingSchemeID = @parmBrandingSchemeID
	ORDER BY 
		RecHubUser.BrandingSchemes.BrandingSchemeName;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
