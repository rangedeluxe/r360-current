﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionToken_Get_BySessionTokenID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionToken_Get_BySessionTokenID') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionToken_Get_BySessionTokenID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionToken_Get_BySessionTokenID 
(
    @parmSessionTokenID	UNIQUEIDENTIFIER
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/04/2013
*
* Purpose: Get Session Token data by Session Token ID
*
* Modification History
* 01/04/2013 WI 83235 CRG Write store procedure to replace SQLLogon.SQL_GetSessionToken(Guid SessionTokenID)
* 03/06/2013 WI 90344 CRG Change Schema for SessionToken in RecHubUser.usp_SessionToken_Get_BySessionTokenID
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		RecHubUser.SessionToken.SessionTokenID, 
		RecHubUser.SessionToken.SessionID, 
		RecHubUser.SessionToken.EmulationID, 
		RecHubUser.SessionToken.CreationDate, 
		RecHubUser.SessionToken.DateUsed, 
		GETDATE() AS LogonDate
	FROM	
		RecHubUser.SessionToken
    WHERE	
		RecHubUser.SessionToken.SessionTokenID = @parmSessionTokenID
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH