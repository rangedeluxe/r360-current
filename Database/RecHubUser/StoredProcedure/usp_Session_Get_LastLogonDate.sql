--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Session_Get_LastLogonDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_Get_LastLogonDate') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_Session_Get_LastLogonDate;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_Get_LastLogonDate
(
	@parmUserID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 8/12/2011
*
* Purpose: Check for session last logon date
*
* Modification History
* CR 46027	WJS	08/09/2011   
*	Created
* 12/19/2012 WI 83237 CRG	Write store procedure to replace SQLLogon.SQL_GetSessionLastLogonDate(int userID)
* 02/27/2013 WI 89611 CRG	Change Schema for RecHubUser.usp_Session_Get_LastLogonDate
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		MAX(RecHubUser.[Session].LogonDateTime) as logonDateTime
	FROM	
		RecHubUser.[Session]
	WHERE	
		RecHubUser.[Session].UserID = @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
GO
                   
