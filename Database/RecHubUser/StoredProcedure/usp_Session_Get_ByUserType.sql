﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Session_Get_ByUserType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_Get_ByUserType') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Session_Get_ByUserType
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_Get_ByUserType 
(
	@parmSessionID	UNIQUEIDENTIFIER,
	@parmUserType	INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/04/2013
*
* Purpose: Get Session data by Session ID
*
* Modification History
* 01/04/2013 WI 85327 CRG Write store procedure to replace ExecuteSQL(SQLSession.SQL_GetSession(sessionID, userType), out datatable)
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	SELECT	
		RecHubUser.[Session].SessionID,
		RecHubUser.[Session].UserID,
		RecHubUser.[Session].IPAddress,
		RecHubUser.[Session].BrowserType,
		RecHubUser.[Session].LogonDateTime,
		RecHubUser.[Session].LastPageServed,
		RecHubUser.[Session].PageCounter,
		RecHubUser.[Session].IsSuccess,
		RecHubUser.[Session].IsSessionEnded,
		RecHubUser.[Session].LogonName,
		RecHubUser.[Session].OLOrganizationID,
		RecHubUser.[Session].OrganizationCode,
		RecHubUser.[Session].IsRegistered
	FROM	
		RecHubUser.[Session] 
	INNER JOIN 
		RecHubUser.Users 
		ON RecHubUser.[Session].UserID = RecHubUser.Users.UserID
	WHERE	
		RecHubUser.[Session].SessionID = @parmSessionID
		AND RecHubUser.Users.UserType = @parmUserType
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH