--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OnlineImageQueue_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OnlineImageQueue_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OnlineImageQueue_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OnlineImageQueue_Upd 
(
    @parmJobStatusID  INT,
    @parmJobStatus    VARCHAR(512),
    @parmFileSize     INT,
    @parmJobID        UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Update the image queue informartion
*
* Modification History
* 01/24/2013 WI 86363 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
   
	UPDATE
		RecHubUser.OnlineImageQueue
	SET
		RecHubUser.OnlineImageQueue.ProcessStatusID   = @parmJobStatusID,
		RecHubUser.OnlineImageQueue.ProcessStatusText = @parmJobStatus,
		RecHubUser.OnlineImageQueue.FileSize          = @parmFileSize,
		RecHubUser.OnlineImageQueue.ModificationDate  = GETDATE()
	WHERE
		RecHubUser.OnlineImageQueue.OnlineImageQueueID = @parmJobID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

