--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_UserPredefSearch_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_UserPredefSearch_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_UserPredefSearch_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_UserPredefSearch_Del
(
	@parmUserID			INT,
	@parmPredefSearchID UNIQUEIDENTIFIER,
	@parmRowsReturned	INT OUTPUT 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/17/2011
*
* Purpose: Delete Row from RecHubUser.UserPredefSearch table 
*
* Modification History
* 01/17/2011 CR 31747 JNE	Created
* 04/04/2013 WI 90806 JBS	Update to 2.0 release. Change Schema Name to RecHubUser
******************************************************************************/

SET ARITHABORT ON;
SET XACT_ABORT ON;

SET @parmRowsReturned = 0;

BEGIN TRY
	DELETE 
	FROM 	RecHubUser.UserPredefSearch
	WHERE 	RecHubUser.UserPredefSearch.UserID = @parmUserID 
			AND RecHubUser.UserPredefSearch.PreDefSearchID = @parmPredefSearchID;

	SELECT @parmRowsReturned = @@ROWCOUNT;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
