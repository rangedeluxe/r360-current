﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_CheckUserPermissionForFunction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_CheckUserPermissionForFunction') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPermissionForFunction
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_CheckUserPermissionForFunction 
(
	@parmPermissionName		VARCHAR(255),
	@parmMode				VARCHAR(255),
	@parmUserID			INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date:  7/05/2013 
*
* Purpose: To Validate if permission exist for a given permissionName and Mode
*
* Modification History
* 07/05/2013 WI 108090 - Initial Creation 
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY


	SELECT	
		1
	FROM    
		RecHubUser.[Permissions] 
	INNER JOIN 
		RecHubUser.UserPermissions 
		ON RecHubUser.[Permissions].PermissionID = RecHubUser.UserPermissions.PermissionID
	WHERE	
		RecHubUser.[Permissions].PermissionName		=  @parmPermissionName
		AND RecHubUser.[Permissions].PermissionMode	=  @parmMode
		AND	RecHubUser.UserPermissions.UserID		= @parmUserID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH