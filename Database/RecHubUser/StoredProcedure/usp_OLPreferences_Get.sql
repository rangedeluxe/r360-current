--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLPreferences_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences for Data
*
* Modification History
* 12/12/2012 WI 76541 CRG Write store procedure for GetOLPreferences(out Data Table)
* 02/22/2013 WI 88817 CRG Change Schema for RecHubUser.usp_OLPreferences_Get
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	
		RecHubUser.OLPreferences.PreferenceName,
		RecHubUser.OLPreferences.IsOnline,
		ISNULL(RecHubUser.OLPreferences.DefaultSetting, '') AS DefaultSetting
	FROM	
		RecHubUser.OLPreferences; 
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
