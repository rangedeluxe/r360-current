--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserPreferences_Upd_UserSetting
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLUserPreferences_Upd_UserSetting') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLUserPreferences_Upd_UserSetting
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLUserPreferences_Upd_UserSetting 
(
	@parmOLPreferenceID UNIQUEIDENTIFIER,
	@parmUserID			INT,
	@parmUserSettings	VARCHAR(255),
	@parmRowsReturned	INT = 0 OUTPUT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/13/2012
*
* Purpose: Update On Line User Preferences record
*
* Modification History
* 12/14/2012 WI 76543 CRG Write store procedure for cPreferencesDAL.UpdateOLUserPreference(Guid, int, string, out int)
* 02/22/2013 WI 88890 CRG Change Schema for RecHubUser.usp_OLUserPreferences_Upd_UserSetting
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	UPDATE	
		RecHubUser.OLUserPreferences
    SET		
		RecHubUser.OLUserPreferences.UserSetting	= @parmUserSettings	
    WHERE	
		RecHubUser.OLUserPreferences.OLPreferenceID	= @parmOLPreferenceID
		AND	RecHubUser.OLUserPreferences.UserID		= @parmUserID;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

