--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_PredefSearch_Get_ByPredefSearchID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_PredefSearch_Get_ByPredefSearchID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_PredefSearch_Get_ByPredefSearchID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_PredefSearch_Get_ByPredefSearchID
(
	@parmUserID			INT,
	@parmPreDefSearchID UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright  � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright  � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/24/2011
*
* Purpose: Retrieve data from PreDefSearch by PreDefSearchID
*
* Modification History
* 08/24/2011 CR 45625 JNE	Created
* 04/04/2013 WI 90775 JBS	Update to 2.0 release. ChangeSchema to RecHubUser
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY
	SELECT 	RecHubUser.UserPreDefSearch.UserID,
			RecHubUser.UserPreDefSearch.PredefSearchID,
			RecHubUser.UserPreDefSearch.IsDefault,
			RecHubUser.PredefSearch.[Name],
			RecHubUser.PredefSearch.[Description],
			RecHubUser.PredefSearch.SearchParmsXML
	FROM 	
			RecHubUser.UserPreDefSearch
			INNER JOIN RecHubUser.PredefSearch ON RecHubUser.PredefSearch.PredefSearchID = RecHubUser.UserPreDefSearch.PredefSearchID
	WHERE 	
			RecHubUser.UserPreDefSearch.UserID = @parmUserID
			AND RecHubUser.UserPreDefSearch.PredefSearchID = @parmPreDefSearchID
	ORDER BY PredefSearch.[Name];

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
 
