--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_OLClientAccountUsers_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_OLClientAccountUsers_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_OLClientAccountUsers_Ins
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_OLClientAccountUsers_Ins
(
    @parmUserID				INT,
    @parmOLClientAccountID	UNIQUEIDENTIFIER, 
    @parmOperator			VARCHAR(32),
    @parmErrorDescription	VARCHAR(2000) OUTPUT,
    @parmSQLErrorNumber		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/23/2009
*
* Purpose: Get user information from RecHubUser.OLClientAccountUsers
*		   
*
* Modification History
* 04/23/2009 CR 25817 JNE	Created
* 03/27/2013 WI 90563 JBS	Update to 2.0 release. Change Schema name
*							Rename proc from  proc_OLLockboxUsers_AddOLLockboxUser.
*							Change all references from Lockbox to ClientAccount.
*							Changed parameter: @parmOLLockboxID to @parmOLClientAccountID.
*							Added logic to prevent adding UserID more than once to OLClientAccountID
* 06/06/2013 WI 104345 MGE	Add auditing 
******************************************************************************/

SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

SET @parmErrorDescription = '';
SET @parmSQLErrorNumber = 0;

DECLARE	@InsertUserID		INT = 0;
DECLARE @AuditMessage		VARCHAR(1024);
DECLARE @ClientAccountID	VARCHAR(10);			--Fill with SiteClientAccountID for Audit Message

BEGIN TRY
	IF @parmUserID IS NULL
		RAISERROR('UserID is NULL',16,1)

	IF @parmOLClientAccountID IS NULL
		RAISERROR('OLClientAccountID is NULL',16,1)
	
	IF EXISTS(SELECT 1 FROM RecHubUser.OLClientAccountUsers 
			WHERE RecHubUser.OLClientAccountUsers.UserID = @parmUserID 
				AND RecHubUser.OLClientAccountUsers.OLClientAccountID = @parmOLClientAccountID)
	BEGIN
		SET @parmErrorDescription = 'User ' + ISNULL(CAST(@parmUserID as VARCHAR(12)), 'NULL') + ' already associated to OLClientAccountID: ' + ISNULL(CAST(@parmOLClientAccountID as varchar(36)),'NULL') + CHAR(46);
		RAISERROR(@parmErrorDescription,16,1);
	END


	BEGIN TRANSACTION AddOLClientAccountUsers;
	INSERT INTO RecHubUser.OLClientAccountUsers
	(UserID, OLClientAccountID, CreationDate, CreatedBy, ModificationDate, ModifiedBy)
	VALUES
	(@parmUserID, @parmOLClientAccountID, GetDate(), @parmOperator, GetDate(), @parmOperator)

	/* create AuditFile table entry  */
	SELECT 
		@InsertUserID = UserID
	FROM 
		RecHubUser.Users
	WHERE 
		LogonName = @parmOperator;

	SELECT @ClientAccountID = (SELECT CAST(SiteClientAccountID AS VARCHAR(10)) FROM RecHubUser.OLClientAccounts WHERE OLClientAccountID = @parmOLClientAccountID)
    
	SET @AuditMessage = 'Added OLClientAccountUser entitlement - ClientAccountID =' + @ClientAccountID
	+ ', UserID = ' +CAST(@parmUserID AS VARCHAR(10)) +'.'
	EXEC RecHubCommon.usp_WFS_DataAudit_ins
		@parmUserID				= @InsertUserID,
		@parmApplicationName	= 'RecHubUser.usp_API_OLClientAccountUsers_Ins',
		@parmSchemaName			= 'RecHubUser',
		@parmTableName			= 'OLClientAccountUsers',
		@parmColumnName			= 'OLClientAccountID',
		@parmAuditValue			= @ClientAccountID,
		@parmAuditType			= 'INS',
		@parmAuditMessage		= @AuditMessage

   COMMIT TRANSACTION AddOLClientAccountUsers;

   RETURN 0
END TRY
BEGIN CATCH
	IF @parmErrorDescription = ''
		SET @parmErrorDescription = 'An error occured in the execution of usp_API_OLClientAccountUsers_Ins';
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 1
END CATCH
