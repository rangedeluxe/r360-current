﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Users_UserID_Get_BySID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Users_UserID_Get_BySID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Users_UserID_Get_BySID;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubUser].[usp_Users_UserID_Get_BySID]
(
		@parmSID							UNIQUEIDENTIFIER
)
AS
/* ****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CEJ
* Date: 05/13/2013
*
* Purpose: returns the UserID give the SID
*
* Modification History
* 05/13/2013 WI 109089 CEJ	Created
******************************************************************************/

SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY
	SELECT RecHubUser.Users.UserID
		FROM RecHubUser.Users
			
		WHERE RecHubUser.Users.RA3MSID = @parmSID
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
