--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_API_Users_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_API_Users_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_API_Users_Upd
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_API_Users_Upd
(
	@parmUserID				INT,
	@parmFirstName			VARCHAR(16),
	@parmMiddleInitial		VARCHAR(3) = NULL,
	@parmLastName			VARCHAR(31),
	@parmSuperUser			BIT=0,
	@parmEmailAddress		VARCHAR(50) = NULL,
	@parmOperator			VARCHAR(24),
	@parmRowsReturned		INT OUTPUT,
	@parmErrorDescription	VARCHAR(2000) OUTPUT,
	@parmSQLErrorNumber		INT OUTPUT,
	@parmExternalID1		VARCHAR(32) = NULL,
	@parmExternalID2		VARCHAR(32) = NULL,
	@parmExternalIDHash		VARCHAR(32) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/22/2009
*
* Purpose: Update User Information
*		   
*
* Modification History
* 04/22/2009 CR 25817 JNE	Created
* 01/13/2010 CR 28636 JPB	@parmSQLErrorNumber was not set correctly for normal
*							course. (Always returned 50000.)
* 07/24/2012 CR 54440 JCS	Added ExternalIDHash as an additional input parameter.
* 03/15/2013 WI 90575 JBS	Update to 2.0 release. Change Schema name.
* 05/21/2013 WI 102867 JPB	Added auditing. 
******************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

SET @parmRowsReturned = 0;
SET @parmErrorDescription = '';
SET @parmSQLErrorNumber = 50000;

DECLARE @InsertUserID	INT = 0,
		@UserID			VARCHAR(10),
		@AuditMessage	VARCHAR(1024),
		@AuditColumns	RecHubCommon.AuditValueChangeTable;

BEGIN TRY
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE RecHubUser.Users.UserID = @parmUserID)
	BEGIN
	   SET @parmErrorDescription = 'There is no database entry for UserID: '+CAST(@parmUserID as varchar(10))+'.';
	   RAISERROR(@parmErrorDescription,16,1);
	END

	IF @parmFirstName IS NULL
	BEGIN
	   SET @parmErrorDescription = 'Invalid input value submitted for FirstName in usp_API_Users_Upd';
	   RAISERROR(@parmErrorDescription,16,1);
	END

	IF @parmLastName IS NULL
	BEGIN
	   SET @parmErrorDescription = 'Invalid input value submitted for LastName in usp_API_Users_Upd';
	   RAISERROR(@parmErrorDescription,16,1);
	END

	DECLARE @AuditInformation TABLE
	(
		OldFirstName VARCHAR(16) NOT NULL,
		OldMiddleInitial VARCHAR(3) NULL,
		OldLastName VARCHAR(31) NOT NULL,
		OldSuperUser BIT NOT NULL,
		OldEmailAddress VARCHAR(50) NULL,
		OldExternalID1 VARCHAR(32) NULL,
		OldExternalID2 VARCHAR(32) NULL,
		OldExternalIDHash VARCHAR(32) NULL,
		NewFirstName VARCHAR(16) NOT NULL,
		NewMiddleInitial VARCHAR(3) NULL,
		NewLastName VARCHAR(31) NOT NULL,
		NewSuperUser BIT NOT NULL,
		NewEmailAddress VARCHAR(50) NULL,
		NewExternalID1 VARCHAR(32) NULL,
		NewExternalID2 VARCHAR(32) NULL,
		NewExternalIDHash VARCHAR(32) NULL
	)

	BEGIN TRAN Users_UpdateUser;
	UPDATE RecHubUser.Users
	SET FirstName = COALESCE(@parmFirstName,FirstName),
		LastName = COALESCE(@parmLastName,LastName),
		MiddleInitial = COALESCE(@parmMiddleInitial,MiddleInitial),
		SuperUser = COALESCE(@parmSuperUser,SuperUser),
		EmailAddress = COALESCE(@parmEmailAddress,EmailAddress),
		ExternalID1 = COALESCE(@parmExternalID1,ExternalID1),
		ExternalID2 = COALESCE(@parmExternalID2, ExternalID2),
		ExternalIDHash = COALESCE(@parmExternalIDHash, ExternalIDHash),
		ModificationDate = GETDATE(),
		ModifiedBy = COALESCE(@parmOperator,suser_sname())
	OUTPUT 
		deleted.FirstName,
		deleted.MiddleInitial,
		deleted.LastName,
		deleted.SuperUser,
		deleted.EmailAddress,
		deleted.ExternalID1,
		deleted.ExternalID2,
		deleted.ExternalIDHash,
		inserted.FirstName,
		inserted.MiddleInitial,
		inserted.LastName,
		inserted.SuperUser,
		inserted.EmailAddress,
		inserted.ExternalID1,
		inserted.ExternalID2,
		inserted.ExternalIDHash
	INTO 
		@AuditInformation
	WHERE 
		RecHubUser.Users.UserID = @parmUserID;
	
	SELECT @parmSQLErrorNumber = @@ERROR, @parmRowsReturned = @@ROWCOUNT;

	/* Insert Audit */
	SELECT 
		@InsertUserID = UserID
	FROM 
		RecHubUser.Users
	WHERE LogonName = @parmOperator;

	SELECT @AuditMessage = 'Modified Users table entry for UserID: ' + CAST(@parmUserID AS VARCHAR(10)) + ' FirstName: ' + @parmFirstName + ' LastName: ' + @parmLastName + '.'

	/* Fill the audit column table type to pass to audit SP */
	INSERT INTO @AuditColumns(ColumnName,OldValue,NewValue)
	SELECT 'FirstName',OldFirstName,NewFirstName FROM @AuditInformation WHERE OldFirstName <> NewFirstName
	UNION ALL
	SELECT 'MiddleInitial',OldMiddleInitial,NewMiddleInitial FROM @AuditInformation WHERE OldMiddleInitial <> NewMiddleInitial
	UNION ALL
	SELECT 'LastName',OldLastName,NewLastName FROM @AuditInformation WHERE OldLastName <> NewLastName
	UNION ALL
	SELECT 'SuperUser',CAST(OldSuperUser AS VARCHAR),CAST(NewSuperUser AS VARCHAR) FROM @AuditInformation WHERE OldSuperUser <> NewSuperUser
	UNION ALL
	SELECT 'EmailAddress',OldEmailAddress,NewEmailAddress FROM @AuditInformation WHERE OldEmailAddress <> NewEmailAddress
	UNION ALL
	SELECT 'ExternalID1',OldExternalID1,NewExternalID1 FROM @AuditInformation WHERE OldExternalID1 <> NewExternalID1
	UNION ALL
	SELECT 'ExternalID2',OldExternalID2,NewExternalID2 FROM @AuditInformation WHERE OldExternalID2 <> NewExternalID2
	UNION ALL
	SELECT 'ExternalIDHash',OldExternalIDHash,NewExternalIDHash FROM @AuditInformation WHERE OldExternalIDHash <> NewExternalIDHash

	SELECT @UserID = CAST(@parmUserID AS VARCHAR);

	/* Now call the SP to insert the audit records */
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
		@parmUserID					= @InsertUserID,
		@parmApplicationName		= 'RecHubUser.usp_API_Users_Upd',
		@parmSchemaName				= 'RecHubUser',
		@parmTableName				= 'Users',
		@parmColumnName				= 'UserID',
		@parmAuditType				= 'UPD',
		@parmAuditValue				= @UserID,
		@parmAuditColumns			= @AuditColumns,
		@parmAuditMessage			= @AuditMessage;

	COMMIT TRAN Users_UpdateUser;

    RETURN 0;
END TRY
BEGIN CATCH
	SELECT @parmSQLErrorNumber = @@ERROR, @parmRowsReturned = 0;
	IF XACT_STATE() != 0 ROLLBACK TRAN Users_UpdateUser;
    IF @parmErrorDescription = ''
        SET @parmErrorDescription = 'An error occured in the execution of usp_API_Users_Upd';
    EXEC RecHubCommon.usp_WfsRethrowException;
    RETURN 1;
END CATCH
