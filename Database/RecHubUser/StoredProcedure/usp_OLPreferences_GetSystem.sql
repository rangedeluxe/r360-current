--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_GetSystem
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_GetSystem') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLPreferences_GetSystem
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_GetSystem 
    
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/26/2012
*
* Purpose: Return OLPreferences for the system
*
* Modification History
* 12/26/2012 WI 86247 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT
        RecHubUser.OLPreferences.PreferenceName,
        RecHubUser.OLPreferences.IsOnline,
        ISNULL(RecHubUser.OLPreferences.DefaultSetting, '') AS DefaultSetting
    FROM 
        RecHubUser.OLPreferences
    WHERE 
        RecHubUser.OLPreferences.IsSystem = 1;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

