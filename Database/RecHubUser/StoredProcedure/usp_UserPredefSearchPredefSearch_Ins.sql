--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_UserPredefSearchPredefSearch_Ins
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_UserPredefSearchPredefSearch_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_UserPredefSearchPredefSearch_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_UserPredefSearchPredefSearch_Ins
(
	@parmUserID			INT,
	@parmIsDefault		INT, 
	@parmName			VARCHAR(256), 
	@parmDescription	VARCHAR(1024), 
	@parmSearchParmsXML XML,
	@parmPreDefSearchID UNIQUEIDENTIFIER OUTPUT,
	@parmRowsReturned	INT OUTPUT,
	@parmError			INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 06/01/2011
*
* Purpose: Inserts record in UserPredefSearch and PredefSearch_Ins by calling the
*						individual procedures.
*
* Modification History
* 06/01/2011 CR 31746 JNE	Created
* 04/04/2013 WI 90808 JBS	Update to 2.0 release. Change Schema name
******************************************************************************/

SET ARITHABORT ON;
SET XACT_ABORT ON;
SET @parmError = 0;

BEGIN TRY
	BEGIN TRANSACTION
		BEGIN TRY
			EXEC RecHubUser.usp_PredefSearch_Ins @parmName, @parmDescription, @parmSearchParmsXML, @parmPredefSearchID OUTPUT;
		END TRY
		BEGIN CATCH
			SET @parmError = 1;
			EXEC RecHubCommon.usp_WfsRethrowException;
		END CATCH
		BEGIN TRY
			EXEC RecHubUser.usp_UserPredefSearch_Ins @parmUserID, @parmPredefSearchID, @parmIsDefault, @parmRowsReturned OUTPUT;
		END TRY
		BEGIN CATCH
			SET @parmError = 1;
			EXEC RecHubCommon.usp_WfsRethrowException;
		END CATCH
	COMMIT TRANSACTION
END TRY

BEGIN CATCH
	SET @parmError = 1;
	IF XACT_STATE() != 0 ROLLBACK TRANSACTION
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
