IF OBJECT_ID('RecHubUser.usp_SessionActivity_PartitionMaintenance') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_SessionActivity_PartitionMaintenance
GO

CREATE PROCEDURE RecHubUser.usp_SessionActivity_PartitionMaintenance
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright 2017-2019 WAUSAU Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 4/13/2017
*
* Purpose: Switches out oldest partitions (based on RetentionDays in SystemSetup) and adds partitions necessary
*      to remain 52 partitions into the future.
*
* Modification History
* 04/13/2017 PT	143253565	MGE	Created
* 08/23/2017 PT 149894451	MGE	Add call to increase size of next two partitions to be used.
* 10/31/2019 R360-31416		MGE improved the tolerance by checking to see if files and filegroups exist before removing them.
************************************************************************************************************************************/
DECLARE @ReturnValue INT,
       @RangeDateKey INT,
	   @HighestRangeDateKey INT,
	   @SwitchPartitionNumber INT,
	   @HighestRangeDateTime DATETIME,
	   @OldestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonDateTime DATETIME,
	   @TempDateTime DATETIME,
       @DiskPath VARCHAR(1000),
       @SessionActivityRetentionDays INT,
       @OldestCalendarDateToKeep DateTime,
	   @EndDate	DATETIME,
       @LastDiskID INT, 
	   @Message VARCHAR(max),
       @SQLcmd VARCHAR(max),
	   @DefaultBeginDate DATETIME

SET @ReturnValue = 0
RAISERROR('Beginning process to maintain SessionActivity partitions.',10,1) WITH NOWAIT;
BEGIN TRY
       --<<< CALCULATE First partition using RetentionDays >>>
	SELECT @SessionActivityRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'SessionActivityLogRetentionDays'
	PRINT 'SessionActivityRetentionDays = ' + Convert(VARCHAR(10), @SessionActivityRetentionDays, 101)
	SELECT @OldestCalendarDateToKeep = GETDATE() - @SessionActivityRetentionDays		-- n days prior to today
	PRINT 'Calculated Oldest Date to keep = ' + Convert(VARCHAR(10), @OldestCalendarDateToKeep, 101)
	SELECT @RangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @OldestCalendarDateToKeep AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest Partition to keep is: ' + CONVERT(CHAR(10),@RangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

    --<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	SELECT @HighestRangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @HighestRangeDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Last Future Partition to add is: ' + CONVERT(CHAR(10),@HighestRangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	--<<< Find oldest existing Partition >>>

	select @OldestExistingPartitonKey = Value from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'SessionActivity_PartitionFunction' AND boundary_id = 1;
	SELECT @Message = 'Oldest Existing Partition is: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Find highest existing Partition >>>

	select @HighestExistingPartitonKey = MAX(Value) from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'SessionActivity_PartitionFunction'
	SELECT @Message = 'Highest Existing Partition Key is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	SELECT @HighestExistingPartitonDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE DateKey = @HighestExistingPartitonKey AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Highest Existing Partition DateTime is: ' + CONVERT(CHAR(10),@HighestExistingPartitonDateTime,111)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	--<<< Determine if there is work to do >>>

	IF	(@OldestExistingPartitonKey >= @RangeDateKey)
		BEGIN
			SELECT @Message = 'No partitions to Switch out '
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
	ELSE
	  BEGIN
	  	  
	  WHILE (@OldestExistingPartitonKey < @RangeDateKey AND @OldestExistingPartitonKey < @HighestExistingPartitonKey)
		BEGIN
						
			IF EXISTS (Select 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'RecHubUser'
				AND sys.tables.name = 'SwitchableSessionActivityLog')
				BEGIN
					--RAISERROR('Dropping SwitchableSessionActivityLog table', 10,1) WITH NOWAIT;
					DROP TABLE RecHubUser.SwitchableSessionActivityLog;
				END

			SELECT @Message = 'Creating SwitchableSessionActivityLog table on SessionActivity' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE RecHubUser.SwitchableSessionActivityLog
			(
			SessionActivityLogID BIGINT IDENTITY(1,1) NOT NULL,
			ActivityCode INT NOT NULL, 
			SessionID UNIQUEIDENTIFIER NOT NULL,
			CreationDateKey	INT NOT NULL, 
			OnlineImageQueueID UNIQUEIDENTIFIER NULL,
			DeliveredToWeb BIT NOT NULL, 
			ModuleName VARCHAR(50) NOT NULL, 
			ItemCount int NOT NULL ,
			DateTimeEntered DATETIME NOT NULL,
			BankID INT NOT NULL, 
			ClientAccountID INT NOT NULL, 
			ProcessingDate DATETIME NOT NULL 
			) ON SessionActivity' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			
			EXEC (@SQLcmd);
			CREATE CLUSTERED INDEX IDX_SessionActivityLog_CreationDateKeySessionActivityLogID ON RecHubUser.SwitchableSessionActivityLog(CreationDateKey,SessionActivityLogID);
				
			/* <<<< Now do the SWITCHES  >>>>>>>>>>>> */	
			SELECT @SwitchPartitionNumber = partition_number FROM RecHubSystem.FileGroupDetailView 
				WHERE range_value = @OldestExistingPartitonKey AND partition_scheme_name = 'SessionActivity'
			
			SELECT @Message = 'Switching out the oldest partition: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER TABLE RecHubUser.SessionActivityLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO RecHubUser.SwitchableSessionActivityLog'
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
				 
			SELECT @Message = 'Merging the oldest range: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			--RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER PARTITION FUNCTION SessionActivity_PartitionFunction() MERGE RANGE (' + (CONVERT(CHAR(10),@OldestExistingPartitonKey,101)) + ')';
			--RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			DROP TABLE RecHubUser.SwitchableSessionActivityLog
			
			IF EXISTS (SELECT 1 FROM sys.database_files WHERE name Like 'SessionActivity'++CAST(@OldestExistingPartitonKey AS VARCHAR(8)))
			BEGIN
				SELECT @Message = 'Removing the oldest partiton file ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
				--RAISERROR(@Message, 10,1) WITH NOWAIT;
				SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILE SessionActivity'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
				EXEC (@SQLcmd);
			END
			
			IF EXISTS (SELECT 1 FROM sys.filegroups WHERE name Like 'SessionActivity'++CAST(@OldestExistingPartitonKey AS VARCHAR(8)))
			BEGIN
				SELECT @Message = 'Removing the oldest partition filegroup: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
				--RAISERROR(@Message, 10,1) WITH NOWAIT;
				SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILEGROUP SessionActivity'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
				EXEC (@SQLcmd);
			END
			
			SET @TempDateTime = Convert(VARCHAR(10),(CONVERT(datetime, convert(varchar(10), @OldestExistingPartitonKey)) + 7),101);
			select @OldestExistingPartitonKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate = @TempDateTime AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC); 
		END
	  END
	  
	--<< Add Partitions up through @HighestRangeKey >>

	EXEC RecHubSystem.usp_SetPartitionSize 'SessionActivity'

	IF @HighestExistingPartitonKey IS NULL
		BEGIN
			SELECT @DefaultBeginDate = GETDATE() - 14						--Start 2 weeks ago
			SELECT @HighestExistingPartitonDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate < @DefaultBeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
			SELECT @HighestExistingPartitonKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @DefaultBeginDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
			SELECT @Message = 'Default Existing Partition to add is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
    
	IF (@HighestExistingPartitonKey < @HighestRangeDateKey)
		BEGIN
		RAISERROR('Creating new SessionActivity partitions',10,1) WITH NOWAIT;
		SELECT @Message = 'EXEC usp_CreatePartitionsForRange SessionActivity ' + Convert(Varchar(10),@HighestExistingPartitonDateTime,101) + ' ' + Convert(Varchar(10),@HighestRangeDateTime,101);
		RAISERROR(@Message, 10,1) WITH NOWAIT;
		EXEC RecHubSystem.usp_CreatePartitionsForRange 'SessionActivity', @HighestExistingPartitonDateTime, @HighestRangeDateTime;
	
		END
   
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

