--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_PredefSearch_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_PredefSearch_Ins') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_PredefSearch_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_PredefSearch_Ins 
(
	@parmName			VARCHAR(256), 
	@parmDescription	VARCHAR(1024), 
	@parmSearchParmsXML XML,
	@parmPreDefSearchID UNIQUEIDENTIFIER OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/10/2011
*
* Purpose: 
*
* Modification History
* 01/10/2011 CR 31742 JNE	Created
* 08/29/2011 CR 31742 JNE   Updated Error Handling
* 04/03/2013 WI 90778 JBS	Update to 2.0 release.  Change schema name.
******************************************************************************/

SET ARITHABORT ON;
SET XACT_ABORT ON;


BEGIN TRY
	DECLARE @GUIDKEY uniqueidentifier;
	SET @GUIDKEY = NEWID(); -- Create new GUID value
	SET @parmSearchParmsXML.modify(
	'
		replace value of (/Root/PreDefSearchID/text())[1]
		with sql:variable("@GUIDKEY")
	');
	INSERT INTO RecHubUser.PredefSearch
	(
		RecHubUser.PredefSearch.PreDefSearchID,
		RecHubUser.PredefSearch.[Name], 
		RecHubUser.PredefSearch.[Description], 
		RecHubUser.PredefSearch.SearchParmsXML,
		RecHubUser.PredefSearch.CreatedBy,
		RecHubUser.PredefSearch.CreationDate,
		RecHubUser.PredefSearch.ModifiedBy,
		RecHubUser.PredefSearch.ModificationDate	
	)
	VALUES 
	(
		@GUIDKEY,
		@parmName, 
		@parmDescription, 
		@parmSearchParmsXML,
		SUSER_SNAME(),
		GetDate(),
		SUSER_SNAME(),
		GetDate() 
	);
	-- Should only insert 1 row
	IF @@ROWCOUNT = 1
	BEGIN
		SELECT @parmPreDefSearchID = @GUIDKEY;
	END
	ELSE
		BEGIN
			RAISERROR('Error inserting record.',16,1);
		END
END TRY

BEGIN CATCH
	-- If called directly
	IF @@NESTLEVEL = 1
		BEGIN
			EXEC RecHubCommon.usp_WfsRethrowException
		END
	ELSE
	BEGIN -- called from another stored procedure
		DECLARE @ErrorMessage    NVARCHAR(4000),
		@ErrorProcedure	 NVARCHAR(200),
		@ErrorSeverity   INT,
		@ErrorState      INT,
		@ErrorLine		 INT
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
END CATCH
