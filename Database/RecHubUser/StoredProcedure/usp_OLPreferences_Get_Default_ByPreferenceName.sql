--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get_Default_ByPreferenceName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get_Default_ByPreferenceName') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLPreferences_Get_Default_ByPreferenceName
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get_Default_ByPreferenceName 
(
	@parmPreferenceName VARCHAR(64)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28700 JMC	Created
* 04/15/2013 WI 90764 JBS	Update to 2.0 release. Change schema to RecHubUser
*							Rename proc FROM usp_OLPreferences_GetDefault
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT RecHubUser.OLPreferences.DefaultSetting
	FROM RecHubUser.OLPreferences
	WHERE RecHubUser.OLPreferences.PreferenceName = @parmPreferenceName;


END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

