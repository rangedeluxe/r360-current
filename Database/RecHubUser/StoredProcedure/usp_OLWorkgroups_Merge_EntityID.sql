--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OLWorkgroups_Merge_EntityID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLWorkgroups_Merge_EntityID') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLWorkgroups_Merge_EntityID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLWorkgroups_Merge_EntityID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmEntityID				INT,
	@parmUserID					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/12/2014
*
* Purpose: Make the association between the RecHubData.dimClientAccounts
*			and the EntityID, taking default Entity settings
*
* Modification History
* 06/12/2014 WI 151433 KLC	Created
* 10/08/2014 WI 151433 KLC	Updated to support null display batch id
******************************************************************************/
DECLARE @auditMessage					VARCHAR(1024),
		@auditType						VARCHAR(3),
		@AuditColumns					RecHubCommon.AuditValueChangeTable,
		@olWorkgroupID					INT,
		@prevEntityID					INT,
		@defaultInvoiceBalancingOption	TINYINT = 0/*TODO: remove 0 default once OLEntities has this column*/;

DECLARE @auditOLWorkgroup	TABLE(OLWorkgroupID INT, EntityID INT, AuditType VARCHAR(3));

SET NOCOUNT ON; 

DECLARE @LocalTransaction bit = 0;
DECLARE @errorDescription VARCHAR(1024);

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	--Verify the user exists
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert/update RecHubUser.OLWorkgroups record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	IF @parmEntityID IS NOT NULL
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLEntities WHERE EntityID = @parmEntityID)
		BEGIN
			SET @errorDescription = 'Unable to insert/update RecHubUser.OLWorkgroups record, EntityID (' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL') + ' does not exist';
			RAISERROR(@errorDescription, 16, 2);
		END
	END

	DECLARE @curTime datetime = GETDATE();

	MERGE RecHubUser.OLWorkgroups
	USING
	(
		VALUES (@parmSiteBankID, @parmSiteClientAccountID)
	) S (SiteBankID, SiteClientAccountID)
	ON RecHubUser.OLWorkgroups.SiteBankID = S.SiteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = S.SiteClientAccountID
	WHEN MATCHED AND ISNULL(RecHubUser.OLWorkgroups.EntityID, -1) <> ISNULL(@parmEntityID, -1)
		THEN UPDATE SET EntityID = @parmEntityID
	WHEN NOT MATCHED AND @parmEntityID IS NOT NULL THEN
		INSERT (EntityID, SiteBankID, SiteClientAccountID, ViewingDays, MaximumSearchDays,
				CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancingOption,
				CreationDate, CreatedBy, ModificationDate, ModifiedBy)
		VALUES (@parmEntityID, @parmSiteBankID, @parmSiteClientAccountID, null, null,
				0, 0, 0, NULL, @defaultInvoiceBalancingOption,
				@curTime, SUSER_NAME(), @curTime, SUSER_NAME())
	OUTPUT INSERTED.OLWorkgroupID, DELETED.EntityID, CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END
	INTO @auditOLWorkgroup;

	--now select out and audit the changes
	SELECT	@auditType = AuditType,
			@olWorkgroupID = OLWorkgroupID,
			@prevEntityID = EntityID
	FROM @auditOLWorkgroup;

	IF @auditType = 'UPD'
	BEGIN
		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EntityID', ISNULL(CAST(@prevEntityID AS VARCHAR(10)), 'NULL'), ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL'));

		-- Audit the RecHubException.BusinessRules update
		SET @auditMessage = 'Modified OLWorkgroups for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ' OLWorkgroupID: ' + CAST(@olWorkgroupID AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLWorkgroups_Merge_EntityID',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'OLWorkgroups',
			@parmColumnName			= 'OLWorkgroupID',
			@parmAuditValue			= @olWorkgroupID,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END
	ELSE IF @auditType = 'INS'
	BEGIN
		-- Audit the RecHubUser.OLWorkgroups insert
		SET @auditMessage = 'Added OLWorkgroups for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ' OLWorkgroupID: ' + CAST(@olWorkgroupID AS VARCHAR(10))
			+ ': ViewingDays = NULL'
			+ ', MaximumSearchDays = NULL'
			+ ', CheckImageDisplayMode = NULL'
			+ ', DocumentImageDisplayMode = NULL'
			+ ', HOA = 0'
			+ ', usp_OLWorkgroups_Merge = NULL'
			+ ', InvoiceBalancingOption = ' + CAST(@defaultInvoiceBalancingOption AS VARCHAR(3))
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubUser.usp_OLWorkgroups_Merge_EntityID',
			@parmSchemaName			= 'RecHubUser',
			@parmTableName			= 'OLWorkgroups',
			@parmColumnName			= 'OLWorkgroupID',
			@parmAuditValue			= @olWorkgroupID,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;
	END

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

