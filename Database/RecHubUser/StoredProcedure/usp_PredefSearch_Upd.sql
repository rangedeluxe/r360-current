--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_PredefSearch_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_PredefSearch_Upd') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_PredefSearch_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_PredefSearch_Upd
(
	@parmPredefSearchID		UNIQUEIDENTIFIER, 
	@parmName				VARCHAR(256),
	@parmDescription		VARCHAR(1024),
	@parmSearchParmsXML		XML,
	@parmRowsReturned		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/12/2011
*
* Purpose: update entry from PredefSearch table using PreDefSearchID and variables passed in.
*
* Modification History
* 01/12/2011 CR 31744 JNE	Created
* 04/03/2013 WI 90779 JBS	Update to 2.0 release.  Change Schema Name
******************************************************************************/
SET ARITHABORT ON;
SET XACT_ABORT ON;

SET @parmRowsReturned = 0;

BEGIN TRY
	UPDATE 	RecHubUser.PredefSearch 
	SET 	RecHubUser.PredefSearch.[Name] = COALESCE(@parmName,RecHubUser.PredefSearch.[Name]),
			RecHubUser.PredefSearch.[Description] = @parmDescription,
			RecHubUser.PredefSearch.SearchParmsXML = COALESCE(@parmSearchParmsXML,RecHubUser.PredefSearch.SearchParmsXML),
			RecHubUser.PredefSearch.ModifiedBy = SUSER_SNAME(),
			RecHubUser.PredefSearch.ModificationDate = GETDATE()
	FROM 	RecHubUser.PredefSearch
	WHERE 	RecHubUser.PredefSearch.PreDefSearchID = @parmPredefSearchID;

	SELECT @parmRowsReturned = @@ROWCOUNT;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
