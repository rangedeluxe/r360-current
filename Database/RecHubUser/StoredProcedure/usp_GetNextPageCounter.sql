--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_GetNextPageCounter
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_GetNextPageCounter') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_GetNextPageCounter
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_GetNextPageCounter
(
	@parmSessionID UNIQUEIDENTIFIER 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Gets the page-counter + 1 value from the SessionLog table for the
*          specified Session.  The approach here should probably be rewritten
*          to use something like a sequence table instead of the existing 
*          logic.
*
* Modification History
* 01/15/2010 CR 28706 JMC Created
* 01/14/2013 WI 85323 CRG Write store procedure to replace ExecuteSQL(SQLSessionLog.SQL_GetNextPageCounter(SessionID), out dt)
* 02/28/2013 WI 89622 CRG Change Schema for RecHubUser.usp_GetNextPageCounter
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	SELECT	
		ISNULL
		(
			MAX(RecHubUser.SessionLog.PageCounter) + 1, 1
		) AS NextPage 
	FROM	
		RecHubUser.SessionLog 
	WHERE	
		RecHubUser.SessionLog.SessionID = @parmSessionID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

