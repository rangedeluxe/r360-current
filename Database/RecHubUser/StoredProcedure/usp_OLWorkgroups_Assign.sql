IF OBJECT_ID('RecHubUser.usp_OLWorkgroups_Assign') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OLWorkgroups_Assign
GO

CREATE PROCEDURE RecHubUser.usp_OLWorkgroups_Assign
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmEntityID				INT,
	@parmViewingDays			INT = NULL,
	@parmMaxSearchDays			INT = NULL
)
AS
/******************************************************************************
** Deluxe Corp. (DLX)
** Copyright � 2014-2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 02/12/2018
*
* Purpose: Make the association between the RecHubData.dimClientAccounts
*			and the EntityID, taking default Entity settings
*
* Modification History
* 02/12/2014 PT 154804395	MGE	Created
* 10/01/2019 R360-30456		MGE Allow ViewingDays and MaximumSearchDays to be updated
******************************************************************************/
DECLARE @defaultInvoiceBalancingOption	TINYINT = 0/*TODO: remove 0 default once OLEntities has this column*/;

SET NOCOUNT ON; 

DECLARE @LocalTransaction bit = 0;
DECLARE @errorDescription VARCHAR(1024);
DECLARE @ExistingViewingDays INT;
DECLARE @ExistingMaxSearchDays INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF @parmEntityID IS NOT NULL
	BEGIN
		IF NOT EXISTS(SELECT 1 FROM RecHubUser.OLEntities WHERE EntityID = @parmEntityID)
		BEGIN
			SET @errorDescription = 'Unable to insert/update RecHubUser.OLWorkgroups record, EntityID (' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL') + ' does not exist';
			RAISERROR(@errorDescription, 16, 2);
		END
	END

	DECLARE @curTime datetime = GETDATE();
	IF @parmViewingDays IS NULL
		SET @ExistingViewingDays = (SELECT ViewingDays from RecHubUser.OLWorkgroups WHERE SiteBankID = @parmSiteBankID and SiteClientAccountID = @parmSiteClientAccountID);
	IF @parmMaxSearchDays IS NULL
		SET @ExistingMaxSearchDays = (SELECT MaximumSearchDays from RecHubUser.OLWorkgroups WHERE SiteBankID = @parmSiteBankID and SiteClientAccountID = @parmSiteClientAccountID);

	MERGE RecHubUser.OLWorkgroups
	USING
	(
		VALUES (@parmSiteBankID, @parmSiteClientAccountID)
	) S (SiteBankID, SiteClientAccountID)
	ON RecHubUser.OLWorkgroups.SiteBankID = S.SiteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = S.SiteClientAccountID
	WHEN MATCHED
		THEN UPDATE SET 
			EntityID = CASE WHEN ISNULL(RecHubUser.OLWorkgroups.EntityID, -1) <> ISNULL(@parmEntityID, -1) THEN @parmEntityID ELSE EntityID END, 
			ViewingDays = COALESCE(@parmViewingDays, @ExistingViewingDays),
			MaximumSearchDays = COALESCE(@parmMaxSearchDays, @ExistingViewingDays)
	WHEN NOT MATCHED AND @parmEntityID IS NOT NULL THEN
		INSERT (EntityID, SiteBankID, SiteClientAccountID, ViewingDays, MaximumSearchDays,
				CheckImageDisplayMode, DocumentImageDisplayMode, HOA, DisplayBatchID, InvoiceBalancingOption,
				CreationDate, CreatedBy, ModificationDate, ModifiedBy)
		VALUES (@parmEntityID, @parmSiteBankID, @parmSiteClientAccountID, @parmViewingDays, @parmMaxSearchDays,
				0, 0, 0, NULL, @defaultInvoiceBalancingOption,
				@curTime, SUSER_NAME(), @curTime, SUSER_NAME());
	

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

GO


