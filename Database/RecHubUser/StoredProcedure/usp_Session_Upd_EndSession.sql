﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Session_Upd_EndSession
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_Upd_EndSession') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_Session_Upd_EndSession
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_Upd_EndSession 
(
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmRowsReturned	INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/04/2013
*
* Purpose: Update Session for the End of the session
*
* Modification History
* 01/04/2013 WI 83435 CRG	Write a store procedure to replace SQLSession.SQL_EndSession(Guid SessionID)
* 02/27/2013 WI 89503 CRG	Change Schema for RecHubUser.usp_Session_Upd_EndSession
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	UPDATE	
		RecHubUser.[Session]
    SET		
		RecHubUser.[Session].IsSessionEnded = 1
	WHERE	
		RecHubUser.[Session].SessionID = @parmSessionID;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH