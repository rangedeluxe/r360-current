--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_SessionClientAccountEntitlements_Get_ByWorkgroup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionClientAccountEntitlements_Get_ByWorkgroup') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_SessionClientAccountEntitlements_Get_ByWorkgroup
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionClientAccountEntitlements_Get_ByWorkgroup
(
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmBankID				INT,
	@parmClientAccountID	INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS/BLR
* Date: 02/17/2015
*
* Purpose: Get Session Workgroups
*
* Modification History
* 02/17/2015 WI 190668 BLR  Initial Revision.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT DISTINCT
		RecHubUser.OLWorkgroups.EntityID,
		RecHubUser.OLWorkgroups.OLWorkGroupID,
		RecHubUser.SessionClientAccountEntitlements.SiteBankID			AS BankID,
		RecHubUser.SessionClientAccountEntitlements.EntityID			AS CustomerID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID AS LockboxID,
		RecHubData.dimClientAccountsView.ShortName,
		RecHubData.dimClientAccountsView.SiteCodeID						AS SiteCode,
		RecHubData.dimClientAccountsView.Cutoff,
		RecHubData.dimClientAccountsView.SiteClientAccountKey  As SiteLockboxKey,
		RecHubData.dimClientAccountsView.IsCommingled,
		RecHubUser.SessionClientAccountEntitlements.MaximumSearchDays,
		RecHubUser.OLWorkgroups.DocumentImageDisplayMode,
		RecHubUser.OLWorkgroups.CheckImageDisplayMode,
		RecHubData.dimSiteCodes.CurrentProcessingDate,
		RecHubUser.SessionClientAccountEntitlements.ViewingDays AS OnlineViewingDays,
		RecHubData.dimClientAccountsView.DisplayLabel AS LongName,
		RecHubData.dimClientAccountsView.ClientAccountKey As LockboxKey,
		CAST(COALESCE(RecHubUser.OLWorkgroups.DisplayBatchID, RecHubUser.OLEntities.DisplayBatchID, 0) AS BIT) AS DisplayBatchID
	FROM	
		RecHubUser.SessionClientAccountEntitlements
		INNER JOIN 	RecHubData.dimClientAccountsView ON
			RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		INNER JOIN RecHubUser.OLWorkgroups ON
			RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
			AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		INNER JOIN RecHubUser.OLEntities ON	
			RecHubUser.OLEntities.EntityID = RecHubUser.OLWorkgroups.EntityID
		INNER JOIN RecHubData.dimSiteCodes ON	
			RecHubData.dimSiteCodes.SiteCodeID = RecHubData.dimClientAccountsView.SiteCodeID
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubData.dimClientAccountsView.SiteBankID = @parmBankID
		AND RecHubData.dimClientAccountsView.SiteClientAccountID = @parmClientAccountID

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
