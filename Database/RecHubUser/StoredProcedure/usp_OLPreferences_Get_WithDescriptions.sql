--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLPreferences_Get_WithDescriptions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OLPreferences_Get_WithDescriptions') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_OLPreferences_Get_WithDescriptions;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OLPreferences_Get_WithDescriptions
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences Descriptions for Preference
*
* Modification History
* 02/22/2013 WI 88843 CRG Change Schema for RecHubUser.usp_OLPreferences_Get_WithDescriptions
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT		
		RecHubUser.OLPreferences.OLPreferenceID,
		RecHubUser.OLPreferences.PreferenceName,
		RecHubUser.OLPreferences.[Description],
		RecHubUser.OLPreferences.IsOnline,
		RecHubUser.OLPreferences.IsSystem,
		ISNULL(RecHubUser.OLPreferences.DefaultSetting, '') AS DefaultSetting,
		RecHubUser.OLPreferences.AppType,
		RecHubUser.OLPreferences.PreferenceGroup,
		RecHubUser.OLPreferences.fldDataTypeEnum,
		RecHubUser.OLPreferences.fldSize,
		RecHubUser.OLPreferences.fldMaxLength,
		RecHubUser.OLPreferences.fldExp,
		RecHubUser.OLPreferences.fldIntMinValue,
		RecHubUser.OLPreferences.fldIntMaxValue,
		RecHubUser.OLPreferences.EncodeOnSerialization
	FROM		
		RecHubUser.OLPreferences
    ORDER BY	
		RecHubUser.OLPreferences.PreferenceName;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

