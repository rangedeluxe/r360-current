--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_ImageDisplayModes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_ImageDisplayModes_Get') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_ImageDisplayModes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_ImageDisplayModes_Get(
	@paramSiteBankId INT,
	@paramSiteWorkgroupId INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: JAW
* Date: 10/12/2016
*
* Purpose: Determines the image display mode for checks and documents for a
*          given workgroup. Automatically applies the proper cascading logic
*          to the workgroup, entity, and system settings.
*
* Modification History
* 10/12/2016 #127604095 JAW Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @WorkgroupCheckImageDisplayMode INT;
	DECLARE @WorkgroupDocumentImageDisplayMode INT;
	DECLARE @EntityCheckImageDisplayMode INT;
	DECLARE @EntityDocumentImageDisplayMode INT;
	DECLARE @SystemCheckImageDisplayMode INT;
	DECLARE @SystemDocumentImageDisplayMode INT;
	DECLARE @EntityId INT;

	-- 1. Read the workgroup setting (and look up the entity ID in case we need entity settings later)
	SELECT
		@WorkgroupCheckImageDisplayMode = CheckImageDisplayMode,
		@WorkgroupDocumentImageDisplayMode = DocumentImageDisplayMode,
		@EntityId = EntityId
	FROM
		RecHubUser.OLWorkgroups
	WHERE
		SiteBankId = @paramSiteBankId AND
		SiteClientAccountId = @paramSiteWorkgroupId;

	IF @WorkgroupCheckImageDisplayMode = 0
		SET @WorkgroupCheckImageDisplayMode = NULL;
	IF @WorkgroupDocumentImageDisplayMode = 0
		SET @WorkgroupDocumentImageDisplayMode = NULL;

	-- 2. Read the entity setting (if needed)
	IF (@WorkgroupCheckImageDisplayMode IS NULL) OR (@WorkgroupDocumentImageDisplayMode IS NULL)
	BEGIN
		SELECT
			@EntityCheckImageDisplayMode = CheckImageDisplayMode,
			@EntityDocumentImageDisplayMode = DocumentImageDisplayMode
		FROM
			RecHubUser.OLEntities
		WHERE
			EntityId = @EntityId;

		IF @EntityCheckImageDisplayMode = 0
			SET @EntityCheckImageDisplayMode = NULL;
		IF @EntityDocumentImageDisplayMode = 0
			SET @EntityDocumentImageDisplayMode = NULL;

		-- 3. Read the system setting (if needed)
		IF (@EntityCheckImageDisplayMode IS NULL) OR (@EntityDocumentImageDisplayMode IS NULL)
		BEGIN
			SELECT
				@SystemCheckImageDisplayMode = DefaultSetting
			FROM
				RecHubUser.OLPreferences
			WHERE
				PreferenceName = 'CheckImageDisplayMode';

			SELECT
				@SystemDocumentImageDisplayMode = DefaultSetting
			FROM
				RecHubUser.OLPreferences
			WHERE
				PreferenceName = 'DocumentImageDisplayMode';
		END;
	END;

	SELECT
		COALESCE(
			@WorkgroupCheckImageDisplayMode,
			@EntityCheckImageDisplayMode,
			@SystemCheckImageDisplayMode,
			0
		) AS CheckImageDisplayMode,
		COALESCE(
			@WorkgroupDocumentImageDisplayMode,
			@EntityDocumentImageDisplayMode,
			@SystemDocumentImageDisplayMode,
			0
		) AS DocumentImageDisplayMode;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
