--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionRegistration_External 
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionRegistration_External') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_SessionRegistration_External 
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionRegistration_External
(
	@parmSID					UNIQUEIDENTIFIER,
	@parmUsername				VARCHAR(24),
	@parmIPAddress				VARCHAR(64)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 07/30/2014
*
* Purpose: Provides session registration for external (thick) clients.
*
* Modification History
* 07/30/2014 WI 155343 BLR	Initial
* 08/04/2014 WI 157027 CMC  Add LogonName to Session table insert
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @SessionID			UNIQUEIDENTIFIER,
		@UserID				INT,
		@LogonName			VARCHAR(24),
		@Password			VARCHAR(44) = 'password'

BEGIN TRY

	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE RA3MSID = @parmSID)
		INSERT INTO RecHubUser.Users(RA3MSID,IsActive,LogonName,FirstName,LastName,SuperUser,[Password],PasswordSet,IsFirstTime)
		SELECT @parmSID,1,@parmUsername,'FirstName','LastName',0,@Password,GETDATE(),1;
	
	SELECT 
		@UserID = UserID,
		@LogonName = LogonName
	FROM 
		RecHubUser.Users 
	WHERE 
		RA3MSID = @parmSID;

	SELECT @SessionID = SessionID FROM RecHubUser.Session WHERE UserID = @UserID AND IsSessionEnded = 0

	IF (@SessionID IS NULL)
	BEGIN
		SET @SessionID = NEWID()
		INSERT INTO RecHubUser.Session(SessionID,UserID,IPAddress,BrowserType,LogonDateTime,LogonName)
		SELECT @SessionID, @UserID, @parmIPAddress, 'Undefined', GETDATE(), @LogonName;
	END

	SELECT @UserID as 'UserID', @SessionID as 'SessionID'

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
