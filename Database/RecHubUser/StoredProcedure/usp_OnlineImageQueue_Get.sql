--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_OnlineImageQueue_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_OnlineImageQueue_Get') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_OnlineImageQueue_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_OnlineImageQueue_Get 
(
    @parmOnlineImageQueueID UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Retrieve image queue information
*
* Modification History
* 01/24/2013 WI 86343 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubUser.OnlineImageQueue.OnlineImageQueueID,
		RecHubUser.OnlineImageQueue.UserID,
		RecHubUser.OnlineImageQueue.ProcessStatusID,
		RecHubUser.OnlineImageQueue.ProcessStatusText,
		RecHubUser.OnlineImageQueue.UserFileName,
		RecHubUser.OnlineImageQueue.FileSize,
		RecHubUser.OnlineImageQueue.CreationDate,
		RecHubUser.OnlineImageQueue.ModificationDate,
		RecHubUser.OnlineImageQueue.CreatedBy,
		RecHubUser.OnlineImageQueue.ModifiedBy
	FROM 
		RecHubUser.OnlineImageQueue
	WHERE 
		RecHubUser.OnlineImageQueue.OnlineImageQueueID = @parmOnlineImageQueueID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
