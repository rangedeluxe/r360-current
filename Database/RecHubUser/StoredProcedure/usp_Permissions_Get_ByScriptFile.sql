--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Permissions_Get_ByScriptFile
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Permissions_Get_ByScriptFile') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Permissions_Get_ByScriptFile
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Permissions_Get_ByScriptFile 
(
	@parmUserID		INT,
	@parmScriptFile VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Retrieve from Permissions with UserID and ScriptFile
*
* Modification History
* 01/15/2010 CR 28683 JMC	Created
* 04/15/2013 WI 90805 JBS	Update to 2.0 release. Change schema to RecHubUser
*							Rename proc FROM usp_UserPermissions_GetByScript
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	RecHubUser.[Permissions].PermissionID,
			RecHubUser.[Permissions].PermissionCategory,
			RecHubUser.[Permissions].PermissionName,
			RecHubUser.[Permissions].PermissionDefault,
			RecHubUser.[Permissions].ScriptFile,
			RecHubUser.[Permissions].PermissionMode
	FROM	RecHubUser.UserPermissions
			INNER JOIN RecHubUser.[Permissions] 
				ON RecHubUser.[Permissions].PermissionID = RecHubUser.UserPermissions.PermissionID
	WHERE	RecHubUser.UserPermissions.UserID = @parmUserID
			AND LOWER(RecHubUser.[Permissions].ScriptFile) = LOWER(@parmScriptFile);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

