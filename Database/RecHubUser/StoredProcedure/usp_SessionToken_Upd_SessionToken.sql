﻿--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SessionToken_Upd_SessionToken
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionToken_Upd_SessionToken') IS NOT NULL
    DROP PROCEDURE RecHubUser.usp_SessionToken_Upd_SessionToken
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionToken_Upd_SessionToken 
(
	@parmSessionTokenID	UNIQUEIDENTIFIER, 
	@parmLogonDate		DATETIME,
	@parmRowsReturned	INT = 0 OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/09/2013
*
* Purpose: Update session token logon date
*
* Modification History
* 01/09/2013 WI 83236 CRG Write store procedure to replace SQLLogon.SQL_UpdateSessionToken(Guid SessionTokenID, DateTime LogonDate)
* 03/05/2013 WI 90350 CRG Change Schema for SessionToken in RecHubUser.usp_SessionToken_Upd_SessionToken
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	UPDATE	
		RecHubUser.SessionToken
	SET		
		RecHubUser.SessionToken.DateUsed = @parmLogonDate 
	WHERE	
		RecHubUser.SessionToken.SessionTokenID = @parmSessionTokenID;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

