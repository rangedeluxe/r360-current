--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Session_GetBreadCrumbs
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_GetBreadCrumbs') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Session_GetBreadCrumbs
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_GetBreadCrumbs
(
	@parmSessionID uniqueidentifier 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 07/26/2011
*
* Purpose: 
*
* Modification History
* 07/26/2011 CR 30310 JMC	Created
* 02/27/2013 WI 89587 CRG	Change Schema for RecHubUser.usp_Session_GetBreadCrumbs
******************************************************************************/
BEGIN TRY
	DECLARE @SessionInfo TABLE
	(
		RowID		INT IDENTITY(1,1),
		ScriptName	VARCHAR(30),
		PageCounter INT,
		FormFields	TEXT
	)
	
	INSERT INTO 
		@SessionInfo
		(
			ScriptName,
			PageCounter,
			FormFields
		)
	SELECT 
		RecHubUser.SessionLog.ScriptName,	
		RecHubUser.SessionLog.PageCounter,
		RecHubUser.SessionLog.FormFields
	FROM 
		RecHubUser.SessionLog
	WHERE 	
		RecHubUser.SessionLog.SessionID = @parmSessionID
	ORDER BY 
		RecHubUser.SessionLog.ScriptName,
		RecHubUser.SessionLog.DateTimeEntered

	SELECT	
		ScriptName,
		PageCounter,
		FormFields
	FROM	
		@SessionInfo
	WHERE	
		RowID IN --get only one row for each script name
			(
				SELECT	
					MAX(RowID)
				FROM	
					@SessionInfo
				GROUP BY 
					ScriptName
			)
	ORDER BY 
		PageCounter DESC
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
