--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_Session_UpdPageCounter
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_Session_UpdPageCounter') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_Session_UpdPageCounter
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_Session_UpdPageCounter
(
    @parmSessionID	  UNIQUEIDENTIFIER,
    @parmNextPage	  INT,
    @parmRowsReturned INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Update session for current page displayed
*
* Modification History
* 01/15/2010 CR 28685 JMC	Created
* 01/28/2013 WI 86452 TWE   Return RowsAffected
* 02/27/2013 WI 89475 CRG   Change Schema for RecHubUser.usp_Session_UpdPageCounter
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
    UPDATE	
		RecHubUser.[Session]
    SET 	
		RecHubUser.[Session].LastPageServed = GETDATE(),
        RecHubUser.[Session].PageCounter = @parmNextPage
    WHERE 	
		RecHubUser.[Session].SessionID = @parmSessionID;

    SELECT 
		@parmRowsReturned = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

