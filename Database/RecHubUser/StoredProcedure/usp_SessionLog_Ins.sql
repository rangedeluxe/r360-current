--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_SessionLog_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_SessionLog_Ins') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_SessionLog_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_SessionLog_Ins 
	@parmSessionID	UNIQUEIDENTIFIER, 
	@parmNextPage	INT, 
	@parmPageName	VARCHAR(30),
	@parmScriptName	VARCHAR(255), 
	@parmFormFields	TEXT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28684 JMC	Created
* 03/06/2013 WI 90326 CRG Change Schema for SessionLog in RecHubUser.usp_SessionLog_Ins
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	INSERT INTO RecHubUser.SessionLog 
	(
		RecHubUser.SessionLog.SessionID,
		RecHubUser.SessionLog.PageCounter, 
		RecHubUser.SessionLog.PageName,
		RecHubUser.SessionLog.DateTimeEntered,
		RecHubUser.SessionLog.ScriptName,
		RecHubUser.SessionLog.FormFields
	)
	VALUES 
	(
		@parmSessionID, 
		@parmNextPage, 
		@parmPageName,
		GETDATE(), 
		@parmScriptName, 
		@parmFormFields
	)
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

