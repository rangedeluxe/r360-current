--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorStoredProcedureName usp_BrandingSchemes_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_BrandingSchemes_Del') IS NOT NULL
       DROP PROCEDURE RecHubUser.usp_BrandingSchemes_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_BrandingSchemes_Del
(
	@parmBrandingSchemeID	INT,
	@parmRowsAffected		INT OUTPUT 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Delete BrandingSchemes record with specified BrandingSchemeID
*
* Modification History
* 01/18/2012 CR 49640 JCS   Created
* 03/27/2013 WI 90589 JBS	Update to 2.0 release,  change schema name.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	
	DELETE
	FROM RecHubUser.BrandingSchemes
	WHERE BrandingSchemeID = @parmBrandingSchemeID;

	SELECT @parmRowsAffected = @@ROWCOUNT;
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
