--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorFunctionName udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubUser.udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID') IS NOT NULL
       DROP FUNCTION RecHubUser.udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubUser.udf_OLClientAccounts_Get_OLOrganizationTree_ByUserID
(
	@parmUserID INT,
	@parmOLOrganizationID UNIQUEIDENTIFIER = 0x0
)
RETURNS @OLOrganizationTree TABLE
(
	SuperUser BIT,
	ParentOrganizationID UNIQUEIDENTIFIER, 
	ChildOrganizationID UNIQUEIDENTIFIER, 
	OrganizationCode VARCHAR(20), 
	ParentLevel TINYINT,
	SiteBankID INT,
	SiteClientAccountID INT,
	DDA VARCHAR(40)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/25/2013
*
* Purpose: 
*
* Modification History
* 10/25/2013 WI 118845 JPB	Created
******************************************************************************/
BEGIN

	;WITH OLOrganizationList( ChildOrganizationID, ParentOrganizationID, OrganizationCode, ParentLevel, DesiredBranch, SuperUser ) AS
	(
		SELECT 
			RecHubUser.OLOrganizations.OLOrganizationID,
			RecHubUser.OLOrganizations.OLOrganizationParentID,
			RecHubUser.OLOrganizations.OrganizationCode,
			0 AS ParentLevel,
			CASE 
				WHEN @parmOLOrganizationID = 0x0 OR @parmOLOrganizationID = RecHubUser.Users.OLOrganizationID THEN 1 
				ELSE 0 
			END AS DesiredBranch,
			RecHubUser.Users.SuperUser
			FROM
				RecHubUser.OLOrganizations
				INNER JOIN RecHubUser.Users ON RecHubUser.Users.OLOrganizationID = RecHubUser.OLOrganizations.OLOrganizationID
			WHERE 
				UserID = @parmUserID
				AND UserType = 0
				AND RecHubUser.OLOrganizations.IsActive = 1
		UNION ALL
		SELECT 
			Children.OLOrganizationID,
			Children.OLOrganizationParentID,
			Children.OrganizationCode,
			ParentLevel + 1,
			CASE WHEN DesiredBranch = 1 OR @parmOLOrganizationID = Children.OLOrganizationID THEN 1 ELSE 0 END AS DesiredBranch,
			SuperUser
		FROM
			RecHubUser.OLOrganizations AS Children
			INNER JOIN OLOrganizationList ON Children.OLOrganizationParentID = OLOrganizationList.ChildOrganizationID
		WHERE
			Children.IsActive = 1
	),
	ClientAccountsList AS 
	(
		SELECT 
			OLOrganizationList.*, 
			RecHubUser.OLClientAccounts.SiteBankID,
			RecHubUser.OLClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DDA,
			CASE 
				WHEN OLOrganizationList.SuperUser = 1 OR RecHubUser.OLClientAccountUsers.UserID IS NOT NULL THEN 1 
				ELSE 0 
			END AS UserHasRights
		FROM 
			OLOrganizationList
			INNER JOIN RecHubUser.OLClientAccounts ON RecHubUser.OLClientAccounts.OLOrganizationID = OLOrganizationList.ChildOrganizationID
			INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLClientAccounts.SiteBankID
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLClientAccounts.SiteClientAccountID
			LEFT OUTER JOIN RecHubUser.OLClientAccountUsers ON RecHubUser.OLClientAccounts.OLClientAccountID = RecHubUser.OLClientAccountUsers.OLClientAccountID
				AND RecHubUser.OLClientAccountUsers.UserID = @parmUserID
		WHERE
			OLOrganizationList.DesiredBranch = 1
			AND RecHubUser.OLClientAccounts.IsActive = 1
			AND RecHubData.dimClientAccountsView.IsActive = 1
	)
	INSERT INTO @OLOrganizationTree
	SELECT
		SuperUser,
		ParentOrganizationID,
		ChildOrganizationID,
		OrganizationCode,
		ParentLevel,
		SiteBankID,
		SiteClientAccountID,
		DDA
	FROM 
		ClientAccountsList
	WHERE
		UserHasRights = 1
	ORDER BY 
		ParentLevel
	OPTION (MAXRECURSION 4);

	RETURN;
END
