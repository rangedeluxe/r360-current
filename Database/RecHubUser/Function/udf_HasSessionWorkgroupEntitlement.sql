--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorFunctionName udf_HasSessionWorkgroupEntitlement
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubUser.udf_HasSessionWorkgroupEntitlement') IS NOT NULL
    DROP FUNCTION RecHubUser.udf_HasSessionWorkgroupEntitlement
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION [RecHubUser].[udf_HasSessionWorkgroupEntitlement]
(
    @parmSessionID      UNIQUEIDENTIFIER,
    @parmBankID         INT,
    @parmWorkgroupID    INT
)
RETURNS BIT
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MSV
* Date: 06/12/2019
*
* Purpose: Checks to see if the given session has a workgroup entitlement for the
*           specified bankid/workgroupid.
*
* Modification History
* 06/12/2019  R360-29369  MSV   Created
******************************************************************************/
BEGIN

    IF EXISTS(
        SELECT RecHubUser.SessionClientAccountEntitlements.SessionClientAccountEntitlementId 
        FROM RecHubUser.SessionClientAccountEntitlements
        WHERE (RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID) 
          AND (RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmBankID)
          AND (RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmWorkgroupID)
        )
        RETURN 1;

    RETURN 0;

END
