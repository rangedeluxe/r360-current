--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorDataTypeName OLOrganizationTree
--WFSScriptProcessorDataTypeCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/27/2013
*
* Purpose: Predfined table to store the OL Organization tree. 
*
*
* Examples of use an be found in usp_factNotifications_ValidateUserAccess.
*
* DECLARE  @OLOrganizationTree RecHubUser.OLOrganizationTree;
*
* INSERT INTO @OLOrganizationTree
* EXEC RecHubUser.usp_OLClientAccounts_Get_OLOrganizationTreePruned_ByUserID @parmUserID
*
*
* Modification History
* 10/27/2013 WI 118941 JPB	Created
******************************************************************************/
CREATE TYPE RecHubUser.OLOrganizationTree AS TABLE
(
	SuperUser BIT,
	ParentOrganizationID UNIQUEIDENTIFIER, 
	ChildOrganizationID UNIQUEIDENTIFIER, 
	OrganizationCode VARCHAR(20), 
	ParentLevel TINYINT,
	SiteBankID INT,
	SiteClientAccountID INT,
	DDA VARCHAR(40)
);