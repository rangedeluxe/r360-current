--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName OnlineImageQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/27/2013 WI 89472 JBS	Update Table to 2.0 release. 
*							Rename Schema to RecHubUser. 
*					   Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OnlineImageQueue 
(
	OnlineImageQueueID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_OnlineImageQueue PRIMARY KEY NONCLUSTERED
		CONSTRAINT DF_OnlineImageQueue_OnlineImageQueueID DEFAULT(NEWID()),
	UserID INT NOT NULL,
	ProcessStatusID INT NOT NULL,
	ProcessStatusText VARCHAR(512) NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_ProcessStatusText DEFAULT('Pending'),
	UserFileName VARCHAR(50) NOT NULL,
	FileSize INT NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_FileSize DEFAULT(0),
	DisplayScannedCheck BIT NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_DisplayScannedCheck DEFAULT(0),
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OnlineImageQueue_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.OnlineImageQueue.IDX_OnlineImageQueue_CreationDate
CREATE CLUSTERED INDEX IDX_OnlineImageQueue_CreationDate ON RecHubUser.OnlineImageQueue(CreationDate);

