--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName ActivityCodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Static Data
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/19/2013 WI 89142 JBS	Update table to 2.0 release
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.ActivityCodes 
(
	ActivityCode INT NOT NULL 
		CONSTRAINT PK_ActivityCodes PRIMARY KEY CLUSTERED,
	ActivityCodeDescription VARCHAR(50) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ActivityCodes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_ActivityCodes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ActivityCodes_ModificationDate DEFAULT (GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_ActivityCodes_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.ActivityCodes.IDX_ActivityCodes_Activity
CREATE UNIQUE INDEX IDX_ActivityCodes_Activity ON RecHubUser.ActivityCodes(ActivityCodeDescription);
