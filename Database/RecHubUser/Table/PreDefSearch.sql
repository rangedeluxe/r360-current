--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName PreDefSearch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE 
* Date: 07/13/2011
*
* Purpose: Predefined Search information  
*		   
*
* Modification History
* 07/13/2011 CR 31749 JNE
* 04/03/2013 WI 94823 JBS	2.0 - Changed Schema name on table object. 
*							Change CreatedBy and ModifiedBy from 32 to 128
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.PreDefSearch
(
	PreDefSearchID UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_PreDefSearch PRIMARY KEY CLUSTERED
		CONSTRAINT DK_PreDefSearch_PreDefSearchID DEFAULT(NEWID()),
	[Name]			VARCHAR(256) NOT NULL,
	[Description]	VARCHAR(1024) NULL,
	SearchParmsXML	XML NOT NULL,
	CreationDate	DATETIME NOT NULL 
		CONSTRAINT DF_PreDefSearch_CreationDate DEFAULT(GETDATE()),
	CreatedBy		VARCHAR(128) NOT NULL 
		CONSTRAINT DF_PreDefSearch_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_PreDefSearch_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy		VARCHAR(128) NOT NULL 
		CONSTRAINT DF_PreDefSearch_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.PreDefSearch.IDX_PreDefSearch_Name
CREATE INDEX IDX_PreDefSearch_Name ON RecHubUser.PreDefSearch([Name]);
