--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName OLPreferences
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Customers dimension is a SCD type 2 holding Customer info.  
*	Most recent flag of 1 indicates the current Customer row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 09/29/2011 CR 46869 JPB	Added Preference columns
* 02/16/2013 WI 89329 JBS	Update Table to 2.0 release.
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLPreferences 
(
	OLPreferenceID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_OLPreferences PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLPreferences_OLPreferenceID DEFAULT(NEWID()),
	PreferenceName VARCHAR(64) NOT NULL,
	[Description] VARCHAR(255) NULL,
	IsOnline BIT NOT NULL CONSTRAINT DF_OLPreferences_IsOnline DEFAULT(0),
	IsSystem BIT NOT NULL CONSTRAINT DF_OLPreferences_IsSystem DEFAULT(0),
	DefaultSetting VARCHAR(255) NULL,
	AppType TINYINT NOT NULL,
		CONSTRAINT CK_OLPreferences_AppType CHECK(AppType IN (0,1,2,99)),
	PreferenceGroup VARCHAR(64) NOT NULL,
	fldDataTypeEnum INT NOT NULL
		CONSTRAINT CK_OLPreferences_fldDataTypeEnum CHECK(fldDataTypeEnum IN (1,4,5,6,101,102,103)),
	fldSize INT NOT NULL,
	fldMaxLength INT NOT NULL,
	fldExp VARCHAR(64) NULL,
	EncodeOnSerialization BIT NOT NULL,
	fldIntMinValue INT NULL,
	fldIntMaxValue INT NULL,
	CreationDate datetime NOT NULL 
		CONSTRAINT DF_OLPreferences_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLPreferences_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate datetime NOT NULL 
		CONSTRAINT DF_OLPreferences_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLPreferences_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.OLPreferences.IDX_OLPreferences_PreferenceName
CREATE UNIQUE INDEX IDX_OLPreferences_PreferenceName ON RecHubUser.OLPreferences(OLPreferenceID);
