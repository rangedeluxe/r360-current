--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName Permissions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Permission definitions
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/15/2013 WI 88021 JBS	2.0 - Changed Schema name on table object. Changed
*							PK from Non-Clustered to Clustered Index.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.[Permissions]
(
	PermissionID INT NOT NULL
		CONSTRAINT PK_Permissions PRIMARY KEY CLUSTERED,
	PermissionCategory VARCHAR(30) NOT NULL,
	PermissionName VARCHAR(30) NOT NULL,
	ScriptFile VARCHAR(255) NULL,
	PermissionDefault INT NOT NULL 
		CONSTRAINT DF_Permissions_PermissionDefault  DEFAULT ((0)),
	PermissionMode VARCHAR(30) NULL       
);
--WFSScriptProcessorTableProperties
