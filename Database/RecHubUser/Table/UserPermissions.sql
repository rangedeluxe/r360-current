--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName UserPermissions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Customers dimension is a SCD type 2 holding Customer info.  
*	Most recent flag of 1 indicates the current Customer row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/16/2013 WI 87992 JBS	Update to 2.0 release. Add Unique Index 
*							ON (UserID, PermissionId) Include (RecordID)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.UserPermissions
(
	RecordID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_UserPermissions PRIMARY KEY NONCLUSTERED,
	UserID INT NOT NULL,
	PermissionID INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.UserPermissions ADD 
	CONSTRAINT FK_UserPermissions_Permissions FOREIGN KEY(PermissionID) REFERENCES RecHubUser.[Permissions] (PermissionID),
	CONSTRAINT FK_UserPermissions_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users (UserID);
-- WI 87992
--WFSScriptProcessorIndex RecHubUser.UserPermissions.IDX_UserPermissions_UserPermissionID
CREATE UNIQUE NONCLUSTERED INDEX IDX_UserPermissions_UserPermissionID ON RecHubUser.UserPermissions
(
	UserID ASC,
	PermissionID ASC
) 
INCLUDE 
(
	RecordID
);
