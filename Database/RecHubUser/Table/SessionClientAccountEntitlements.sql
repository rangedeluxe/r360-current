--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName SessionClientAccountEntitlements
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2019 Deluxe Corporation All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/11/2014
*
* Purpose: Client Account Keys a session is entitled to view.
*		   
*
* Modification History
* 05/11/2014 WI 145456		JPB	Created
* 07/08/2014 WI 145456		KLC	Changed Logon entity to be actual owning entity
* 09/18/2014 WI 166671		JPB	Added new index.
* 09/18/2014 WI 166672		JPB	Updated IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey
*								with includes.
* 02/23/2015 WI 191263		JBS	Add columns ViewAhead and EndDateKey, Also Change StartDateKey to NOT NULL
* 03/23/2015 WI 197226		JPB	Add IDX_SessionIDCreationDate (used by Session Maint package)
* 03/25/2015 WI 197953		JBS	Add IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead  
*							(used by Advanced Search)
* 06/01/2015 WI 197953		JBS	Changing IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID to Clustered index. Per 216076
* 03/29/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added new index and partition keys
* 04/24/2017 PT 143253497	MGE updated column name to remove extra 'Session'
* 09/27/2018 PT 160347603	MGE added columns to index to improve Dashboard performance.
* 03/20/2019 R360-15940		MGE updated index to improve Dashboard performnace for one plan optimizer uses
******************************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.SessionClientAccountEntitlements
(
	SessionClientAccountEntitlementID BIGINT IDENTITY(1,1) NOT NULL,
		
	SessionID			UNIQUEIDENTIFIER,
	CreationDateKey		INT NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	EntityID			INT NOT NULL,
	SiteBankID			INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	ViewingDays			INT NULL,
	MaximumSearchDays	INT NULL,
	StartDateKey		INT NOT NULL,
	EndDateKey			INT NOT NULL,
	ViewAhead			BIT NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_SessionClientAccountEntitlement_CreationDate DEFAULT GETDATE(),
	EntityName			VARCHAR(128) NOT NULL
) ON Sessions (CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
    CONSTRAINT PK_SessionClientAccountEntitlements PRIMARY KEY NONCLUSTERED (SessionClientAccountEntitlementID, CreationDateKey)
ALTER TABLE RecHubUser.SessionClientAccountEntitlements ADD 
	CONSTRAINT FK_SessionClientAccountEntitlements_dimClientAccounts FOREIGN KEY (ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);

--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID
CREATE CLUSTERED INDEX IDX_SessionClientAccountEntitlements_CreationDateKeyClientAccountEntitlementID ON RecHubUser.SessionClientAccountEntitlements
(
	CreationDateKey,
	SessionClientAccountEntitlementID
) ON Sessions (CreationDateKey);

--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	SiteBankID,
	SiteClientAccountID
) ON Sessions (CreationDateKey);

--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDClientAccountKey ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	ClientAccountKey
)
INCLUDE --166672
(
	EntityID,
	SiteBankID,
	SiteClientAccountID,
	StartDateKey,
	EndDateKey,
	ViewAhead,
	EntityName
) ON Sessions (CreationDateKey);

--WI 166671
--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityIDEntityName ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID
) 
INCLUDE 
(
	EntityID,
	EntityName
) ON Sessions (CreationDateKey);

--WI 197226
--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDCreationDate
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDCreationDate ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID,
	CreationDate
) ON Sessions (CreationDateKey);
--WI 197953
--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionIDIncludeEntityEntitySiteBankSiteClientAccountIDStartDateEndDateKeyViewAhead ON RecHubUser.SessionClientAccountEntitlements
(
	SessionID
) 
INCLUDE 
(
	EntityID,
	SiteBankID,
	SiteClientAccountID,
	StartDateKey,
	EndDateKey,
	ViewAhead
) ON Sessions (CreationDateKey);
--WFSScriptProcessorIndex RecHubUser.SessionClientAccountEntitlements.[IDX_SessionClientAccountEntitlements_SessionSiteBankClientAccountID]
CREATE NONCLUSTERED INDEX IDX_SessionClientAccountEntitlements_SessionSiteBankIDClientAccountID ON RecHubUser.SessionClientAccountEntitlements
(
 SessionID ASC,
 SiteBankID ASC,
 SiteClientAccountID ASC
)
INCLUDE
(
 EntityID,
 ViewingDays,
 MaximumSearchDays,
 ClientAccountKey,
 StartDateKey,
 EndDateKey,
 ViewAhead,
 EntityName
) ON Sessions (CreationDateKey);
