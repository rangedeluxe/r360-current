--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName OLUserMachines
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-20213 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/16/2013 WI 89332 JBS	Update Table to 2.0 release. Change Schema name
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLUserMachines 
(
	OLUserMachineID UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_OLUserMachines PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLUserMachines_OLUserMachineID DEFAULT(NEWID()),
	UserID INT NOT NULL,
	MachineToken UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserMachines_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserMachines_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserMachines_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserMachines_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLUserMachines ADD CONSTRAINT FK_Users_OLUserMachines FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
--WFSScriptProcessorIndex RecHubUser.OLUserMachines.IDX_OLUserMachines_UserID
CREATE INDEX IDX_OLUserMachines_UserID ON RecHubUser.OLUserMachines(UserID);
--WFSScriptProcessorIndex RecHubUser.OLUserMachines.IDX_OLUserMachines_MachineToken
CREATE UNIQUE INDEX IDX_OLUserMachines_MachineToken ON RecHubUser.OLUserMachines(MachineToken);
