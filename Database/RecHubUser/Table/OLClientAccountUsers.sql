--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable OLClientAccountUsers
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/18/2013 WI 89242 JBS	Update table to 2.0 Release. 
*							Rename table from OLLockboxUsers
*							Renamed all Constraints, Renamed Columns:
*							OLLockboxID to OLClientAccountID
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLClientAccountUsers 
(
	OLClientAccountID UNIQUEIDENTIFIER NOT NULL,
	UserID INT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLClientAccountUsers_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLClientAccountUsers_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLClientAccountUsers_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLClientAccountUsers_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLClientAccountUsers ADD 
	CONSTRAINT FK_OLClientAccountUsers_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_OLClientAccountUsers_OLClientAccounts FOREIGN KEY (OLClientAccountID) REFERENCES RecHubUser.OLClientAccounts(OLClientAccountID);
