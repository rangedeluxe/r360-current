--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable OLWorkgroups
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/13/2014
*
* Purpose: Stores Entity attributes specific to Receivables 360.
*		   
*
* Modification History
* 05/13/2014 WI 145460 JPB	Created
* 06/24/2014 WI 145460 KLC	Lowercased the G, removed DisplayName, added InvoiceBalancingOption
* 07/02/2014 WI 145460 KLC	Updated EntityID to be nullable
* 10/08/2014 WI 168684 KLC	Updated to allow DisplayBatchID to be null and removed default constraint
* 01/09/2015 WI 183469 RDS	Add Billing Fields
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLWorkgroups 
(
	OLWorkgroupID INT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_OLWorkgroups PRIMARY KEY CLUSTERED,
	EntityID INT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ViewingDays INT NULL,
	MaximumSearchDays INT NULL,
	CheckImageDisplayMode TINYINT NOT NULL,
	DocumentImageDisplayMode TINYINT NOT NULL,
	HOA BIT NOT NULL
		CONSTRAINT DF_OLWorkgroups_HOA DEFAULT(0),
	DisplayBatchID BIT NULL,
	InvoiceBalancingOption TINYINT NOT NULL
		CONSTRAINT DF_OLWorkgroups_InvoiceBalancingOption DEFAULT(0),
	BillingAccount VARCHAR(20) NULL,
	BillingField1 VARCHAR(20) NULL,
	BillingField2 VARCHAR(20) NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLWorkgroups_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLWorkgroups_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLWorkgroups_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLWorkgroups_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLWorkgroups ADD 
       CONSTRAINT FK_OLWorkgroups_OLEntities FOREIGN KEY (EntityID) REFERENCES RecHubUser.OLEntities(EntityID);

--WFSScriptProcessorIndex RecHubUser.OLWorkgroups.IDX_OLClientAccountss_OLOrganizationID_SiteBankClientAccountID
CREATE INDEX IDX_OLWorkgroups_EntitySiteBankClientAccountID ON RecHubUser.OLWorkgroups
(
	EntityID,
	SiteBankID,
	SiteClientAccountID
)
INCLUDE
(    
	CheckImageDisplayMode,
	DocumentImageDisplayMode
);
