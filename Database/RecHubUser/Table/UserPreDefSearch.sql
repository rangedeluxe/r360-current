--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName UserPreDefSearch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE 
* Date: 07/13/2011
*
* Purpose: User Predefined Search, connects the user to the Predefined Search 
*          Query. 
*		   
*
* Modification History
* 07/13/2011 CR 31750 JNE
* 04/03/2013 WI 94825 JBS	Update Table to 2.0 release.  Rename Schema Name
*							Changed CreatedBy and ModifiedBy from 32 to 128
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.UserPreDefSearch
(
	UserID			INT NOT NULL,
	PreDefSearchID	UNIQUEIDENTIFIER NOT NULL,
	IsDefault		BIT NOT NULL,
	CreationDate	DATETIME NOT NULL 
		CONSTRAINT DF_UserPreDefSearch_CreationDate DEFAULT(GETDATE()),
	CreatedBy		VARCHAR(128) NOT NULL 
		CONSTRAINT DF_UserPreDefSearch_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_UserPreDefSearch_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy		VARCHAR(128) NOT NULL 
		CONSTRAINT DF_UserPreDefSearch_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.UserPreDefSearch ADD
	CONSTRAINT PK_UserPreDefSearch PRIMARY KEY CLUSTERED (UserID ASC, PreDefSearchID ASC),
	CONSTRAINT FK_UserPreDefSearch_PreDefSearch FOREIGN KEY(PreDefSearchID) REFERENCES RecHubUser.PreDefSearch(PreDefSearchID);

