--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable SessionLog
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Table stores Session log
*		   
*
* Modification History
* 03/09/2009 CR 25817		JJR	Created
* 10/22/2012 CR 56447		JPB	Added new index.
* 02/28/2013 WI 89653		JBS	Update Table to 2.0 release. Change Schema Name.
*								Change LogEntryID from INT to BIGINT
* 06/01/2015 WI 216344		JBS	Change IDX_SessionLog_SessionIDPageCounter to Clustered INDEX. Per 216076
* 03/29/2017 PT 141435715	MGE	Add CreationDateKey & as part of clustered index to prepare for patitioning.
* 04/03/2017 PT 142595657	MGE Add partition key
*************************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.SessionLog 
(
	LogEntryID BIGINT IDENTITY(1,1) NOT NULL,
	SessionID UNIQUEIDENTIFIER NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_SessionLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	PageCounter INT NOT NULL,
	PageName VARCHAR(30) NOT NULL,
	DateTimeEntered DATETIME NOT NULL
		CONSTRAINT DF_SessionLog_DateTimeEntered DEFAULT (GETDATE()),
	ScriptName VARCHAR(255) NOT NULL,
	FormFields TEXT NULL
) ON Sessions (CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.SessionLog ADD 
	CONSTRAINT PK_SessionLog PRIMARY KEY NONCLUSTERED (LogEntryID, CreationDateKey)
ALTER TABLE RecHubUser.SessionLog ADD 
	CONSTRAINT FK_SessionLog_Session FOREIGN KEY(SessionID, CreationDateKey) REFERENCES RecHubUser.[Session](SessionID, CreationDateKey);
	
--WFSScriptProcessorIndex RecHubUser.SessionLog.IDX_SessionLog_CreationDateKeyLogEntryID
CREATE CLUSTERED INDEX IDX_SessionLog_CreationDateKeyLogEntryID ON RecHubUser.SessionLog
(
	CreationDateKey ASC,
	LogEntryID ASC
) ON Sessions (CreationDateKey);
--CR 56447 JPB 10/22/12
--WFSScriptProcessorIndex RecHubUser.SessionLog.IDX_SessionLog_SessionIDPageCounter
CREATE INDEX IDX_SessionLog_SessionIDPageCounter ON RecHubUser.SessionLog
(
	SessionID ASC,
	PageCounter ASC
) ON Sessions (CreationDateKey);