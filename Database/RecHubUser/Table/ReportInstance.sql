--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName ReportInstance
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/24/2013
*
* Purpose:	Contains the report instance information
*
* Modification History
* 06/24/2013 WI 107181 KLC	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.ReportInstance
(
	ReportInstanceKey	BIGINT				IDENTITY(1, 1) NOT NULL
		CONSTRAINT PK_ReportInstance PRIMARY KEY,
	InstanceID			UNIQUEIDENTIFIER	NOT NULL,
	UserID				INT					NOT NULL,
	ReportId			INT					NOT NULL,
	ReportStatus		TINYINT				NOT NULL,
	StartTime			DATETIME			NULL,
	EndTime				DATETIME			NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.ReportInstance ADD 
    CONSTRAINT FK_ReportInstance_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID),
    CONSTRAINT FK_ReportInstance_ReportConfigurations FOREIGN KEY (ReportID) REFERENCES RecHubConfig.ReportConfigurations(ReportID);
