--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName Session
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Static Data
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 09/29/2011 CR 46575 JPB 	Updated IPAddress to VARCHAR(64)
* 11/15/2011 CR 47915 JPB	Made FK_Users_Sessions NOCHECK
* 01/05/2012 CR 49091 JPB 	Updated BrowserType to VARCHAR(1024)
* 02/17/2013 WI 89487 JBS	Update table to 2.0 release. Change Schema Name
* 03/28/2017 PT 141435715	MGE	Added CreationDateKey to table for Partitioning plans
* 04/03/2017 PT 142595657	MGE Added partition keys
**********************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.[Session]
(
	SessionID UNIQUEIDENTIFIER NOT NULL, 
	UserID INT NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_Sessions_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	IPAddress VARCHAR(64) NOT NULL,
	BrowserType VARCHAR(1024) NOT NULL,
	LogonDateTime DATETIME NOT NULL,
	LastPageServed DATETIME NULL,
	PageCounter INT NOT NULL 
		CONSTRAINT DF_Sessions_PageCounter DEFAULT(0),
	IsSuccess BIT NOT NULL 
		CONSTRAINT DF_Sessions_IsSuccess DEFAULT(0),
	IsSessionEnded BIT NOT NULL 
		CONSTRAINT DF_Sessions_IsSessionEnded DEFAULT(0),
	LogonName VARCHAR(50) NULL,
	OLOrganizationID UNIQUEIDENTIFIER NULL,
	OrganizationCode VARCHAR(20) NULL,
	IsRegistered BIT NOT NULL 
		CONSTRAINT DF_Sessions_IsRegistered DEFAULT(0)
) ON Sessions (CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.Session ADD 
	CONSTRAINT PK_Sessions PRIMARY KEY NONCLUSTERED (SessionID, CreationDateKey);
ALTER TABLE RecHubUser.[Session] ADD 
	CONSTRAINT FK_Session_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);

ALTER TABLE RecHubUser.[Session] NOCHECK CONSTRAINT FK_Session_Users;

--WFSScriptProcessorIndex RecHubUser.Session.IDX_Sessions_CreationDateKeySessionID
CREATE CLUSTERED INDEX IDX_Sessions_CreationDateKeySessionID ON RecHubUser.[Session](CreationDateKey,SessionID) ON Sessions (CreationDateKey);
--WFSScriptProcessorIndex RecHubUser.Session.IDX_Sessions_LogonDateTime
CREATE NONCLUSTERED INDEX IDX_Sessions_LogonDateTime ON RecHubUser.[Session](LogonDateTime) ON Sessions (CreationDateKey);
--WFSScriptProcessorIndex RecHubUser.Session.IDX_Sessions_UserID
CREATE INDEX IDX_Sessions_UserID ON RecHubUser.[Session](UserID) ON Sessions (CreationDateKey);
