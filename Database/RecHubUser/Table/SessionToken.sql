--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName SessionToken
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/17/2013 WI 89522 JBS	Update table to 2.0 release. Change Schema Name
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.SessionToken 
(
	SessionTokenID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_SessionToken PRIMARY KEY NONCLUSTERED
		CONSTRAINT DF_SessionToken_SessionTokenID DEFAULT(NEWID()),
	SessionID UNIQUEIDENTIFIER NOT NULL,
	EmulationID UNIQUEIDENTIFIER NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_SessionToken_CreationDate DEFAULT(GETDATE()),
	DateUsed DATETIME NULL 
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForgeignKey
ALTER TABLE RecHubUser.SessionToken ADD 
       CONSTRAINT FK_SessionToken_Session FOREIGN KEY (SessionID) REFERENCES RecHubUser.[Session](SessionID),
       CONSTRAINT FK_SessionToken_Emulations FOREIGN KEY (EmulationID) REFERENCES RecHubUser.SessionEmulation(EmulationID);
--WFSScriptProcessorIndex RecHubUser.SessionToken.IDX_SessionToken_SessionID
CREATE INDEX IDX_SessionToken_SessionID ON RecHubUser.SessionToken(SessionID);
--WFSScriptProcessorIndex RecHubUser.SessionToken.IDX_SessionToken_EmulationID
CREATE INDEX IDX_SessionToken_EmulationID ON RecHubUser.SessionToken(EmulationID);

