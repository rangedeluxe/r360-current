--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName OLUserQuestions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/27/2013 WI 89341 JBS	Update table to 2.0 release. Rename Schema to RechubUser
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLUserQuestions 
(
	OLUserQuestionID UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_OLUserQuestions PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLUserQuestions_OLUserQuestionID DEFAULT(NEWID()),
	UserID INT NOT NULL,
	QuestionText VARCHAR(255) NOT NULL,
	AnswerText VARCHAR(255) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserQuestions_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserQuestions_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserQuestions_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserQuestions_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLUserQuestions 
	ADD CONSTRAINT FK_OLUserQuestions_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID);
--WFSScriptProcessorIndex RecHubUser.OLUserQuestions.IDX_OLUserQuestions_UserID
CREATE INDEX IDX_OLUserQuestions_UserID ON RecHubUser.OLUserQuestions(UserID);
