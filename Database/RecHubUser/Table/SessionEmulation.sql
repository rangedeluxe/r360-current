--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableNameName SessionEmulation
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 12/04/2012 WI 70808 JPB	Added IDX_SessionEmulation_SessionIDIsBillable (FP:CR 56099)
* 02/17/2013 WI 89512 JBS	Update table to 2.0 release. Change Schema name
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.SessionEmulation 
(
	EmulationID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_SessionEmulationID PRIMARY KEY NONCLUSTERED,
	SessionID UNIQUEIDENTIFIER NOT NULL,
	UserID INT NOT NULL,
	SessionCreated DATETIME NOT NULL 
		CONSTRAINT DF_SessionEmulation_SessionCreated DEFAULT(GETDATE()),
	IsBillable BIT NOT NULL 
		CONSTRAINT DF_SessionEmulation_IsBillable DEFAULT(0)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.SessionEmulation ADD 
	CONSTRAINT FK_SessionEmulation_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID),
    CONSTRAINT FK_SessionEmulation_Session FOREIGN KEY (SessionID) REFERENCES RecHubUser.[Session](SessionID);
--WFSScriptProcessorIndex RecHubUser.SessionEmulation.IDX_SessionEmulation_SessionID
CREATE INDEX IDX_SessionEmulation_SessionID ON RecHubUser.SessionEmulation(SessionID);
--WI 70808 JPB 12/04/2012
--WFSScriptProcessorIndex RecHubUser.SessionEmulation.IDX_SessionEmulation_SessionIDIsBillable
CREATE NONCLUSTERED INDEX IDX_SessionEmulation_SessionIDIsBillable ON RecHubUser.SessionEmulation(SessionID,IsBillable);
