--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable OLOrganizations
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/30/2009 CR 29277 JNE   Changed ViewingDays to be Nullable.
* 01/18/2012 CR 49637 JCS   Added column BrandingSchemeID with default value of 1
*							as that is the initial branding in BrandingSchemes table.
* 08/23/2012 CR 54174 JPB	Added DisplayBatchID.
* 02/17/2013 WI 89294 JBS	Update table to 2.0 release. Rename table from OLCustomers
*							Renamed all Constraints, Renamed Indexes, Renamed Columns:
*							OLCustomerID to OLOrganizationID
*							CustomerCode to OrganizationCode
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
* 05/28/2013 WI 103299 JBS	Adding columns:
*							IsParent,  OLOrganizationParentID 
*							Added FK FK_OLOrganizations_OLOrganizations
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLOrganizations 
(
	OLOrganizationID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_OLOrganizations PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLOrganizations_OLOrganizationID DEFAULT(NEWID()),
	OrganizationCode VARCHAR(20) NOT NULL,
	[Description] VARCHAR(255) NOT NULL,
	IsActive BIT NOT NULL,
	IsParent BIT NOT NULL,
	ViewingDays INT NULL,
	MaximumSearchDays INT NULL,
	CheckImageDisplayMode TINYINT NOT NULL 
		CONSTRAINT DF_OLOrganizations_CheckImageDisplayMode DEFAULT(0),
	DocumentImageDisplayMode TINYINT NOT NULL 
		CONSTRAINT DF_OLOrganizations_DocumentImageDisplayMode DEFAULT(0),
	DisplayBatchID BIT NOT NULL
		CONSTRAINT DF_OLOrganizations_DisplayBatchID DEFAULT(0),
	BrandingSchemeID INT NOT NULL 
		CONSTRAINT DF_OLOrganizations_BrandingSchemeID DEFAULT(1),
	ExternalID1 VARCHAR(32) NULL,
	ExternalID2 VARCHAR(32) NULL,
	OLOrganizationParentID UNIQUEIDENTIFIER NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_OLOrganizations_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLOrganizations_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLOrganizations_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLOrganizations_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLOrganizations ADD 
       CONSTRAINT FK_OLOrganizations_BrandingSchemes FOREIGN KEY (BrandingSchemeID) REFERENCES RecHubUser.BrandingSchemes(BrandingSchemeID),
	   CONSTRAINT FK_OLOrganizations_OLOrganizations FOREIGN KEY (OLOrganizationParentID) REFERENCES RecHubUser.OLOrganizations(OLOrganizationID);
--WFSScriptProcessorIndex RecHubUser.OLOrganizations.IDX_OLOrganizations_OrganizationCode
CREATE UNIQUE INDEX IDX_OLOrganizations_OrganizationCode ON RecHubUser.OLOrganizations(OrganizationCode);

