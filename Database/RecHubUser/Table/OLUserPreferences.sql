--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName OLUserPreferences
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Customers dimension is a SCD type 2 holding Customer info.  
*	Most recent flag of 1 indicates the current Customer row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/18/2013 WI 87993 JBS	Update to 2.0 release.  Add AK_OLUserPreferences
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLUserPreferences 
(
	OLUserPreferenceID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_OLUserPreferences PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLUserPreferences_OLUserPreferenceID DEFAULT(NEWID()),
	UserID INT NOT NULL,
	OLPreferenceID UNIQUEIDENTIFIER NOT NULL, 
	UserSetting VARCHAR(255) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserPreferences_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserPreferences_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLUserPreferences_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLUserPreferences_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLUserPreferences ADD 
	CONSTRAINT FK_OLUserPreferences_OLPreferences FOREIGN KEY (OLPreferenceID) REFERENCES RecHubUser.OLPreferences(OLPreferenceID),
	CONSTRAINT FK_OLUserPreferences_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT AK_OLUserPreferences UNIQUE NONCLUSTERED 
	(
		OLPreferenceID,
		UserID
	);
--WFSScriptProcessorIndex RecHubUser.OLUserPreferences.IDX_OLUserPreferences_OLPreferenceID
CREATE INDEX IDX_OLUserPreferences_OLPreferenceID ON RecHubUser.OLUserPreferences(OLPreferenceID);
--WFSScriptProcessorIndex RecHubUser.OLUserPreferences.IDX_OLUserPreferences_UserID
CREATE INDEX IDX_OLUserPreferences_UserID ON RecHubUser.OLUserPreferences(UserID);
--WFSScriptProcessorIndex RecHubUser.OLUserPreferences.IDX_OLUserPreferences_OLPreferenceUserID
CREATE INDEX IDX_OLUserPreferences_OLPreferenceUserID ON RecHubUser.OLUserPreferences
	(
		OLPreferenceID ASC,
		UserID ASC
	);
