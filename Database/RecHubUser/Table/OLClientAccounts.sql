--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable OLClientAccounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 08/09/2011 CR 46141 JPB	Added InvoiceBalancingOption.
* 02/14/2012 CR 50240 JPB	Added HOA.
* 08/23/2012 CR 54175 JPB	Added DisplayBatchID.
* 01/02/2013 WI 77760 JPB	Added nex index. (FP:77754)
* 02/17/2013 WI 89211 JBS	Update table to 2.0. Rename table from OLLockboxes
*							Renamed all Constraints, Renamed Columns:
*							OLLockboxID to OLClientAccountID
*							OLCustomerID to OLOrganizationID
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLClientAccounts 
(
	OLClientAccountID UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT PK_OLClientAccounts PRIMARY KEY CLUSTERED
		CONSTRAINT DF_OLClientAccounts_OLClientAccountID DEFAULT(NEWID()),
	OLOrganizationID UNIQUEIDENTIFIER NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	DisplayName VARCHAR(50) NOT NULL,
	IsActive BIT NOT NULL,
	ViewingDays INT NULL,
	MaximumSearchDays INT NULL,
	CheckImageDisplayMode TINYINT NOT NULL,
	DocumentImageDisplayMode TINYINT NOT NULL,
	InvoiceBalancingOption TINYINT NOT NULL
		CONSTRAINT DF_OLClientAccounts_InvoiceBalancingOption DEFAULT(0),
	HOA BIT NOT NULL
		CONSTRAINT DF_OLClientAccounts_HOA DEFAULT(0),
	DisplayBatchID BIT NOT NULL
		CONSTRAINT DF_OLClientAccounts_DisplayBatchID DEFAULT(0),
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLClientAccounts_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLClientAccounts_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLClientAccounts_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLClientAccounts_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.OLClientAccounts ADD 
       CONSTRAINT FK_OLClientAccounts_OLOrganizations FOREIGN KEY (OLOrganizationID) REFERENCES RecHubUser.OLOrganizations(OLOrganizationID);
--WI 77760 JPB 01/02/13
--WFSScriptProcessorIndex RecHubUser.OLClientAccounts.IDX_OLClientAccountss_OLOrganizationID_SiteBankClientAccountID
CREATE INDEX IDX_OLClientAccounts_OLOrganizationSiteBankClientAccountID ON RecHubUser.OLClientAccounts
(
	OLOrganizationID,
	SiteBankID,
	SiteClientAccountID
)
INCLUDE
(    
	CheckImageDisplayMode,
	DocumentImageDisplayMode
);
