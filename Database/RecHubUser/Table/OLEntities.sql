--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTable OLEntities
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/13/2014
*
* Purpose: Stores Entity attributes specific to Receivables 360.
*		   
*
* Modification History
* 05/13/2014 WI 145459 JPB	Created
* 01/09/2015 WI 183470 RDS	Add Billing Fields
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.OLEntities 
(
	EntityID INT NOT NULL 
		CONSTRAINT PK_OLEntities PRIMARY KEY CLUSTERED,
	DisplayBatchID BIT NOT NULL
		CONSTRAINT DF_OLEntities_DisplayBatchID DEFAULT(0),
	CheckImageDisplayMode TINYINT NOT NULL 
		CONSTRAINT DF_OLEntities_CheckImageDisplayMode DEFAULT(0),
	DocumentImageDisplayMode TINYINT NOT NULL 
		CONSTRAINT DF_OLEntities_DocumentImageDisplayMode DEFAULT(0),
	ViewingDays INT NULL,
	MaximumSearchDays INT NULL,
	BillingAccount VARCHAR(20) NULL,
	BillingField1 VARCHAR(20) NULL,
	BillingField2 VARCHAR(20) NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_OLEntities_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLEntities_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_OLEntities_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_OLEntities_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.OLEntities.IDX_OLEntities_EntityID
CREATE UNIQUE INDEX IDX_OLEntities_EntityID ON RecHubUser.OLEntities (EntityID);
