--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName BrandingSchemes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose: Table BrandingSchemes that contains the defined branding schemes.
*
* Modification History
* 01/18/2012 CR 49636 JCS   Created table.
* 02/29/2012 CR 51099 JCS   Added ExternalID1, ExternalID2 fields to table.
* 04/26/2012 CR 52303 JPB   Named the IsActive constraint.
* 02/17/2013 WI 89156 JBS	Update table to 2.0 release
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.BrandingSchemes
(
	BrandingSchemeID INT NOT NULL
		CONSTRAINT PK_BrandingSchemes PRIMARY KEY CLUSTERED,
	BrandingSchemeName VARCHAR(32) NOT NULL,
	BrandingSchemeDescription VARCHAR(256) NULL,
	BrandingSchemeFolder VARCHAR(256) NOT NULL,
	IsActive BIT NOT NULL 
		CONSTRAINT DF_BrandingSchemes_IsActive DEFAULT (1),
	ExternalID1 VARCHAR(32) NULL,
	ExternalID2 VARCHAR(32) NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_BrandingSchemes_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_BrandingSchemes_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_BrandingSchemes_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_BrandingSchemes_ModifiedBy DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.BrandingSchemes.IDX_BrandingSchemes_BrandingSchemeName
CREATE UNIQUE INDEX IDX_BrandingSchemes_BrandingSchemeName ON RecHubUser.BrandingSchemes(BrandingSchemeName);
