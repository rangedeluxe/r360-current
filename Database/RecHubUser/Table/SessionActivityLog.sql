--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableNameName SessionActivityLog
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817	JJR		Created
* 12/04/2012 WI 70807	JPB		Added index for billing (FP:CR 56577)
* 01/02/2013 WI 77759	JPB		Added missing index (FP:77753)
* 02/17/2013 WI 88324	JBS		Update for 2.0 release.  Added Included column to
*								IDX_SessionActivityLog_OnlineImageQueueID
* 03/29/2017 PT 141435715 MGE	Add CreationDateKey & as part of clustered index to prepare for patitioning.
* 04/03/2017 PT 142595657 MGE	Add parition keys
*************************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.SessionActivityLog 
(
	SessionActivityLogID BIGINT IDENTITY(1,1) NOT NULL  
		CONSTRAINT PK_SessionActivityLog PRIMARY KEY NONCLUSTERED,
	ActivityCode INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ActivityCode DEFAULT(0),
	SessionID UNIQUEIDENTIFIER NOT NULL,
	CreationDateKey	INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	OnlineImageQueueID UNIQUEIDENTIFIER NULL,
	DeliveredToWeb BIT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_DeliveredToWeb DEFAULT(0),
	ModuleName VARCHAR(50) NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ModuleName DEFAULT('Unknown'),
	ItemCount int NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ItemCount DEFAULT(0),
	DateTimeEntered DATETIME NOT NULL,
	BankID INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_BankID DEFAULT((-1)), 
	ClientAccountID INT NOT NULL 
		CONSTRAINT DF_SessionActivityLog_LockboxID DEFAULT((-1)),
	ProcessingDate DATETIME NOT NULL 
		CONSTRAINT DF_SessionActivityLog_ProcessingDate DEFAULT(CONVERT(datetime,CONVERT(varchar(10),GETDATE(),101)))  
) ON SessionActivity (CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.SessionActivityLog ADD 
	CONSTRAINT PK_SessionActivityLog PRIMARY KEY NONCLUSTERED (SessionActivityLogID, CreationDateKey)
ALTER TABLE RecHubUser.SessionActivityLog ADD 
	CONSTRAINT FK_SessionActivityLog_ActivityCodes FOREIGN KEY (ActivityCode) REFERENCES RecHubUser.ActivityCodes(ActivityCode),
	CONSTRAINT FK_SessionActivityLog_OnlineImageQueue FOREIGN KEY (OnlineImageQueueID) REFERENCES RecHubUser.OnlineImageQueue(OnlineImageQueueID);
ALTER TABLE RecHubUser.SessionActityLog WITH NOCHECK ADD
	CONSTRAINT FK_SessionActivityLog_Session FOREIGN KEY (SessionID,CreationDateKey) REFERENCES RecHubUser.[Session](SessionID,CreationDateKey);

--WFSScriptProcessorIndex RecHubUser.SessionActivityLog.IDX_SessionActivityLog_CreationDateKeySessionActivityLogID
CREATE CLUSTERED INDEX IDX_SessionActivityLog_CreationDateKeySessionActivityLogID ON RecHubUser.SessionActivityLog(CreationDateKey,SessionActivityLogID)
	 ON SessionActivity (CreationDateKey);
--WFSScriptProcessorIndex RecHubUser.SessionActivityLog.IDX_SessionActivityLog_ActivityCode
CREATE INDEX IDX_SessionActivityLog_ActivityCode ON RecHubUser.SessionActivityLog(ActivityCode)  ON SessionActivity (CreationDateKey);
--WFSScriptProcessorIndex RecHubUser.SessionActivityLog.IDX_SessionActivityLog_SessionID
CREATE INDEX IDX_SessionActivityLog_SessionID ON RecHubUser.SessionActivityLog(SessionID) INCLUDE (CreationDateKey, ActivityCode)  ON SessionActivity (CreationDateKey);
--WI 70807 JPB 12/04/12
--WFSScriptProcessorIndex RecHubUser.SessionActivityLog.IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode
CREATE NONCLUSTERED INDEX IDX_SessionActivityLog_BankClientAccountIDProcessingDateDeliveredToWebActivityCode ON RecHubUser.SessionActivityLog
(
	BankID,
	ClientAccountID,
	ProcessingDate,
	DeliveredToWeb,
	ActivityCode
) ON SessionActivity (CreationDateKey);
--CR 77759
--WFSScriptProcessorIndex RecHubUser.SessionActivityLog.IDX_SessionActivityLog_OnlineImageQueueID
CREATE NONCLUSTERED INDEX IDX_SessionActivityLog_OnlineImageQueueID ON RecHubUser.SessionActivityLog (OnlineImageQueueID) INCLUDE (DeliveredToWeb)
	 ON SessionActivity (CreationDateKey);
