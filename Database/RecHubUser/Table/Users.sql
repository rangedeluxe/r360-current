--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName Users
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 07/25/2011 CR 45437 JPB	Changed password to 44.
* 09/26/2011 CR 46988 JPB	Added PasswordExpirationTime.
* 10/12/2011 CR 47241 JPB	Added FailedSecurityQuestionAttempts.
* 07/23/2012 CR 54246 JPB	Added ExternalIDHash.
* 02/16/2013 WI 89534 JBS	Update table to 2.0 release. Change Schema Name
*							Remove ContactID and the FK to dbo.Contact
*							LogonName Increase to VARCHAR(24)
*							FP: WI 83964, WI 85099
*							Adding RA3MSID and index for RA3M Mapping
* 09/12/2014 WI 165627 RDS	Add LogonEntityID and LogonEntityName columns
* 04/01/2015 WI 199061 JBS	Removed columns no longer needed.
* 12/13/2016 #127604133  JAW  Corrected indexes.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.Users 
(
	UserID INT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_Users PRIMARY KEY CLUSTERED,
	RA3MSID UNIQUEIDENTIFIER NOT NULL,
	LogonName VARCHAR(24) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_Users_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_Users_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_Users_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_Users_ModifiedBy DEFAULT(SUSER_SNAME()),
	LogonEntityID INT NULL,
	LogonEntityName VARCHAR(50) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubUser.Users.IDX_Users_LogonName
CREATE NONCLUSTERED INDEX IDX_Users_LogonName ON RecHubUser.Users (LogonName);
--WFSScriptProcessorIndex RecHubUser.Users.IDX_Users_RA3MSID
CREATE NONCLUSTERED INDEX IDX_Users_RA3MSID ON RecHubUser.Users 
(
	RA3MSID
)
INCLUDE
(
	UserID
);
--WFSScriptProcessorIndex RecHubUser.Users.IDX_Users_UserIDLogonName
CREATE NONCLUSTERED INDEX IDX_Users_UserIDLogonName ON RecHubUser.Users
(
	UserID,
	LogonName
);
