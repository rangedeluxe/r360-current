--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorTableName UserPasswordHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/05/2011
*
* Purpose: Store old user passwords
*		   
*
* Modification History
* 08/05/2011 CR 45876 JPB	Created
* 02/08/2012 CR 50073 JPB	Made FK_Users_UserPasswordHistory NOCHECK
* 02/28/2013 WI 89772 JBS	Update Table to 2.0 release. Rename Schema Name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubUser.UserPasswordHistory 
(
	UserPasswordHistoryID INT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_UserPasswordHistory PRIMARY KEY CLUSTERED,
	UserID INT NOT NULL,
	PasswordHistory VARCHAR(44) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_UserPasswordHistory_CreationDate DEFAULT(GETDATE())
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubUser.UserPasswordHistory WITH NOCHECK
	ADD CONSTRAINT FK_Users_UserPasswordHistory FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID)
	
ALTER TABLE RecHubUser.UserPasswordHistory NOCHECK CONSTRAINT FK_Users_UserPasswordHistory

--WFSScriptProcessorIndex RecHubUser.UserPasswordHistory.IDX_UserPasswordHistory_UserID
CREATE NONCLUSTERED INDEX IDX_UserPasswordHistory_UserID ON RecHubUser.UserPasswordHistory (UserID)
