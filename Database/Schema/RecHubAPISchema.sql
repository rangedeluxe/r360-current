--WFSScriptProcessorDoNotFormat
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Chris Colombo
* Date: 07/09/2018
*
* Purpose: Create the RecHubAPI Schema
*
* Modification History
* 07/09/2018 PT 155569216 CMC	Created
******************************************************************************/
IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'RecHubAPI')
       EXEC ('CREATE SCHEMA RecHubAPI AUTHORIZATION DBO')