--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: Create the RecHubBillingSchema Schema
*
* Modification History
* 04/15/2014 WI 136857 RDS	Created
* 06/14/2016 WI 286519 JBS  Adding back to Database
******************************************************************************/
IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'RecHubBilling')
       EXEC ('CREATE SCHEMA RecHubBilling AUTHORIZATION DBO')

