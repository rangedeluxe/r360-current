/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/05/2019
*
* Purpose: Create the CommonStaging Schema
*
* Modification History
* 11/05/2019 WI R360-30933	MGE	Created
******************************************************************************/
IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'CommonStaging')
       EXEC ('CREATE SCHEMA CommonStaging AUTHORIZATION DBO')