--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/03/2012
*
* Purpose: Create the RecHubSystem Schema
*
* Modification History
* 10/03/2012 WI 88803 JBS	Created
******************************************************************************/
IF NOT EXISTS(SELECT * FROM sys.schemas WHERE name = 'RecHubSystem')
       EXEC ('CREATE SCHEMA RecHubSystem AUTHORIZATION DBO')
