SET NOCOUNT ON 

DECLARE @SQLCommand VARCHAR(MAX),
		@Message VARCHAR(MAX),
		@DBName VARCHAR(128),
		@Loop INT,
		@RecCount INT,
		@handle UNIQUEIDENTIFIER,
		@CRLF CHAR(2),
		@TAB CHAR(1)

SET @CRLF = CHAR(13)+ CHAR(10)
SET @TAB = CHAR(9)

/* STEP 1: Alter the database to kill all messages that are in SSB */
USE MASTER
ALTER DATABASE $(DBName)
SET NEW_BROKER 

USE $(DBName)

/* STEP 2: Remove all stored procedures */
DECLARE @StoredProcedures TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	ProcedureName VARCHAR(128)
)

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'OLTA'
		AND ROUTINE_TYPE = 'PROCEDURE'
ORDER BY ROUTINE_NAME

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_OLTA%'
ORDER BY ROUTINE_NAME

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_CDS%'
ORDER BY ROUTINE_NAME

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_ICON%'
ORDER BY ROUTINE_NAME

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_OTIS%'
ORDER BY ROUTINE_NAME

INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'dbo'
		AND ROUTINE_TYPE = 'PROCEDURE'
		AND ROUTINE_NAME LIKE 'usp_ServiceBroker%'
ORDER BY ROUTINE_NAME


/* Add other dbo OLTA specific stored procedures */
INSERT INTO @StoredProcedures(SchemaName,ProcedureName)
SELECT 'dbo','usp_AuditProcess'
UNION ALL SELECT 'dbo','usp_Ins_ServiceBrokerError'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculateCSV_Downloads'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculateCSV_RowsDownloaded'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculateFrontImage_FilesViewed'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculateImage_FilesViewed'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculatePDF_FilesViewed'
UNION ALL SELECT 'dbo','proc_VT_OLTA_CalculateXML_FilesDownloaded'
UNION ALL SELECT 'dbo','usp_SQLMailEventContacts_GetEmailAddressesByEventName'
UNION ALL SELECT 'dbo','usp_QueueXSetupsforTransformation'
UNION ALL SELECT 'dbo','usp_OLTError'

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @StoredProcedures) )
BEGIN
	SELECT @SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + ProcedureName  + CHAR(39) + ') IS NOT NULL' + @CRLF + @TAB + 'DROP PROCEDURE ' + SchemaName + '.' + ProcedureName 
	FROM @StoredProcedures
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* STEP 3: Remove all OLTA functions */
DECLARE @Functions TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	FunctionName VARCHAR(128)
)

INSERT INTO @Functions(SchemaName,FunctionName)
SELECT 	ROUTINE_SCHEMA,ROUTINE_NAME
FROM 	INFORMATION_SCHEMA.ROUTINES
WHERE 	ROUTINE_SCHEMA = 'OLTA'
		AND ROUTINE_TYPE = 'FUNCTION'
ORDER BY ROUTINE_NAME

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @Functions) )
BEGIN
	SELECT @SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + FunctionName  + CHAR(39) + ') IS NOT NULL' + @CRLF + @TAB + 'DROP FUNCTION ' + SchemaName + '.' + FunctionName 
	FROM @Functions
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* STEP 4: Remove Triggers from DBO */
DECLARE @Triggers TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	TriggerName VARCHAR(128)
)

INSERT INTO @Triggers(SchemaName,TriggerName)
SELECT 'dbo','OLTAXDocumentType'
UNION ALL SELECT 'dbo','OLTAXSetupBank'
UNION ALL SELECT 'dbo','OLTAXSetupCustomer'
UNION ALL SELECT 'dbo','OLTAXSetupLockbox'
UNION ALL SELECT 'dbo','OLTAXSiteCode'

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @Triggers) )
BEGIN
	SELECT @SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + TriggerName  + CHAR(39) + ') IS NOT NULL' + @CRLF + @TAB + 'DROP TRIGGER ' + SchemaName + '.' + TriggerName 
	FROM @Triggers
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* Special case. There is a database level trigger that needs to be dropped */
	
IF EXISTS( SELECT 1 FROM sys.triggers WHERE [name] = 'PartitionEventTracking')
	DROP TRIGGER PartitionEventTracking ON DATABASE

/* STEP 5: Remove all foreign keys */
DECLARE @ForeignKeys TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	TableName VARCHAR(128),
	ConstraintName VARCHAR(128)
)

INSERT INTO @ForeignKeys(SchemaName,TableName,ConstraintName)
SELECT 	OBJECT_SCHEMA_NAME(parent_object_id) AS SchemaName, 
		OBJECT_NAME(parent_object_id) AS TableName, 
		[name] AS ConstraintName
FROM 	sys.foreign_keys
WHERE 	OBJECT_SCHEMA_NAME(parent_object_id) = 'OLTA'

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @ForeignKeys) )
BEGIN
	SELECT @SQLCommand = 'IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA=' + CHAR(39) + SchemaName + CHAR(39) +' AND TABLE_NAME=' + CHAR(39) + TableName  + CHAR(39) +' AND CONSTRAINT_NAME=' + CHAR(39) + ConstraintName + CHAR(39) + ')' + @CRLF + @TAB + 'ALTER TABLE ' + SchemaName + '.' + TableName + ' DROP CONSTRAINT ' + ConstraintName
	FROM @ForeignKeys
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* STEP 6: Remove Tables/Views */
DECLARE @Tables TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	TableName VARCHAR(128),
	ObjectType VARCHAR(128)
)

/* Add OLTA Views */
INSERT INTO @Tables(SchemaName,TableName,ObjectType)
SELECT 	'OLTA',
		TABLE_NAME,
		'VIEW' 
FROM 	INFORMATION_SCHEMA.VIEWS
WHERE 	TABLE_SCHEMA = 'OLTA'

/* Add OLTA Tables */
INSERT INTO @Tables(SchemaName,TableName,ObjectType)
SELECT 	'OLTA',
		TABLE_NAME,
		'TABLE'
FROM 	INFORMATION_SCHEMA.TABLES
WHERE 	TABLE_SCHEMA = 'OLTA'

/* Add dbo specific tables */
INSERT INTO @Tables(SchemaName,TableName,ObjectType)
SELECT 'dbo','AuditProcess','TABLE'
UNION ALL SELECT 'dbo','ServiceBrokerErrors','TABLE'
UNION ALL SELECT 'dbo','OTISAudit','TABLE'
UNION ALL SELECT 'dbo','OTISLockboxQueue','TABLE'
UNION ALL SELECT 'dbo','OTISQueue','TABLE'
UNION ALL SELECT 'dbo','LegacyDataMove','TABLE'

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @Tables) )
BEGIN
	SELECT @SQLCommand = 'IF OBJECT_ID(' + CHAR(39) + + SchemaName + '.' + TableName  + CHAR(39) + ') IS NOT NULL' + @CRLF + @TAB + 'DROP ' + ObjectType + ' ' + SchemaName + '.' + TableName 
	FROM @Tables
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* STEP 7: Remove Service Broker Objects */
DECLARE @SSB TABLE
(
	RowID INT IDENTITY(1,1),
	SchemaName VARCHAR(30),
	SSBName VARCHAR(128),
	SSBObjectType VARCHAR(128)
)

INSERT INTO @SSB(SchemaName,SSBName,SSBObjectType)
SELECT 'dbo','CDSBatchDeleteSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchDeleteReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchDeleteSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchDeleteReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchDeleteContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSBatchDeleteMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSBatchInsertSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchInsertReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchInsertSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchInsertReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchInsertContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSBatchInsertMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSBatchReconSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchReconReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSBatchReconSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchReconReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSBatchReconContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSBatchReconMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSDocumentTypeSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSDocumentTypeReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSDocumentTypeSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSDocumentTypeReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSDocumentTypeContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSDocumentTypeMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSResponseSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSResponseReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSResponseSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSResponseReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSResponseContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSResponseMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSSetupSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSSetupReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSSetupSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSSetupReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSSetupContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSSetupMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','CDSSiteCodeSenderService','SERVICE'
UNION ALL SELECT 'dbo','CDSSiteCodeReceiverService','SERVICE'
UNION ALL SELECT 'dbo','CDSSiteCodeSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSSiteCodeReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','CDSSiteCodeContract','CONTRACT'
UNION ALL SELECT 'dbo','CDSSiteCodeMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTABatchDeleteSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchDeleteReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchDeleteSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchDeleteReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchDeleteContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTABatchDeleteMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTABatchInsertSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchInsertReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchInsertSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchInsertReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchInsertContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTABatchInsertMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTABatchReconSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchReconReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchReconSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchReconReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchReconContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTABatchReconMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTABatchUpdateSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchUpdateReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTABatchUpdateSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchUpdateReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTABatchUpdateContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTABatchUpdateMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTADocumentTypeSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTADocumentTypeReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTADocumentTypeSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTADocumentTypeReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTADocumentTypeContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTADocumentTypeMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTASetupSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTASetupReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTASetupSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTASetupReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTASetupContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTASetupMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTASiteCodeSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTASiteCodeReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTASiteCodeSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTASiteCodeReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTASiteCodeContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTASiteCodeMsg','MESSAGE TYPE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertSenderService','SERVICE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertReceiverService','SERVICE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertSenderQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertReceiverQueue','QUEUE'
UNION ALL SELECT 'dbo','OLTAICONClientInsertContract','CONTRACT'
UNION ALL SELECT 'dbo','OLTAICONClientInsertMsg','MESSAGE TYPE'

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @SSB) )
BEGIN
	SELECT @SQLCommand = 'IF EXISTS(SELECT * FROM '
		+CASE
			WHEN SSBObjectType = 'MESSAGE TYPE' THEN 'sys.service_message_types'
			WHEN SSBObjectType = 'CONTRACT' THEN 'sys.service_contracts'
			WHEN SSBObjectType = 'QUEUE' THEN 'sys.service_queues'
			WHEN SSBObjectType = 'SERVICE' THEN 'sys.services'
		END
		+  ' WHERE Name = ' + CHAR(39) + SSBName  + CHAR(39) + ')' + @CRLF + @TAB + 'DROP ' + SSBObjectType + ' ' + SSBName
	FROM @SSB
	WHERE RowID = @Loop

	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand)
	SET @Loop = @Loop + 1
END

/* STEP 8: DROP OLTA Schema */
IF EXISTS(SELECT 1 FROM sys.schemas WHERE [name] = 'OLTA')
	DROP SCHEMA OLTA

/* STEP 9: DROP Partition Schema */
IF EXISTS(SELECT 1 FROM sys.partition_schemes WHERE [name] = 'OLTAFacts_Partition_Scheme')
	DROP PARTITION SCHEME OLTAFacts_Partition_Scheme

/* STEP 10: DROP Partition Function */
IF EXISTS(SELECT 1 FROM sys.partition_functions WHERE [name] = 'DepositDate_Partition_Range')
	DROP PARTITION FUNCTION DepositDate_Partition_Range

/* STEP 10: Remove OLTA File Groups */
SELECT @DBName = DB_NAME()
DECLARE @FileGroups TABLE
(	
	RowID INT IDENTITY(1,1),
	FileGroupName VARCHAR(128),
	PhysicalName VARCHAR(512)
)

INSERT INTO @FileGroups(FileGroupName,PhysicalName) 
SELECT [Name],physical_name
FROM sys.database_files
WHERE [type] = 0 AND data_space_id > 1

SET @Loop = 1

WHILE( @Loop <= (SELECT MAX(RowID) FROM @SSB) )
BEGIN
	SELECT @SQLCommand = 'ALTER DATABASE ' + @DBName + ' REMOVE FILE [' + FileGroupName + '];'
	FROM 	@FileGroups
	WHERE RowID = @Loop
	
	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand);

	SELECT @SQLCommand = 'ALTER DATABASE ' + @DBName + ' REMOVE FILEGROUP [' + FileGroupName + '];'
	FROM 	@FileGroups
	WHERE RowID = @Loop
	
	RAISERROR(@SQLCommand,10,1) WITH NOWAIT
	EXEC(@SQLCommand);
	
	SET @Loop = @Loop + 1
END


