--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTrigger
CREATE TRIGGER PartitionEventTracking
ON DATABASE
FOR CREATE_PARTITION_FUNCTION, CREATE_PARTITION_SCHEME, ALTER_PARTITION_FUNCTION, ALTER_PARTITION_SCHEME
AS
/******************************************************************************
** Wausau Financial Systems
** Copyright � 2009 Wausau Financial Systems All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau Financial Systems 2009.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau Financial Systems and contain Wausau Financial Systems 
* trade secrets.  These materials may not be used, copied, modified or disclosed 
* except as expressly permitted in writing by Wausau Financial Systems (see 
* the Wausau Financial Systems license agreement for details).  All copies, 
* modifications and derivative works of these materials are property of Wausau 
* Financial Systems.
*
* Author: JJR
* Date: 3/09/2009
*
* Purpose: DDL trigger designed to capture all create/alter partition 
*      scheme/function events.
*
* Modification History
* 03/09/2008 JJR - Created
******************************************************************************/
SET ARITHABORT ON
BEGIN TRY
	INSERT INTO OLTA.PartitionEventTracking(EventType, ObjectType, ObjectName, CommandText, PostDateTime)
	SELECT EVENTDATA().value ('(/EVENT_INSTANCE/EventType)[1]','nvarchar(128)') as EventType,
		EVENTDATA().value ('(/EVENT_INSTANCE/ObjectType)[1]','nvarchar(128)') as ObjectType,
		EVENTDATA().value ('(/EVENT_INSTANCE/ObjectName)[1]','nvarchar(128)') as ObjectName,
		EVENTDATA().value ('(/EVENT_INSTANCE/TSQLCommand)[1]','nvarchar(max)') as CommandText,
		EVENTDATA().value ('(/EVENT_INSTANCE/PostTime)[1]','datetime') as PostDateTime
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
GO
