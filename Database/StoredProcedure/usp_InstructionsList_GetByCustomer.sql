--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_InstructionsList_GetByCustomer
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_InstructionsList_GetByCustomer') IS NOT NULL
       DROP PROCEDURE OLTA.usp_InstructionsList_GetByCustomer
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_InstructionsList_GetByCustomer]
	@parmSiteBankID int,
	@parmSiteCustomerID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28704 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT 
	InstructionsText.BankID,
	InstructionsText.CustomerID,
	InstructionsText.LockboxID,
	InstructionsText.Type,
	ISNULL(RTRIM(InstructionTypes.Description), 'Not Defined') AS InstructionType,
	RTRIM(InstructionsText.Description) AS InstructionsDescription,
	RTRIM(InstructionsText.Tag) AS Tag,
	MAX(InstructionsText.AsOf) AS AsOf, 
	'instructions_viewer.aspx?bank='+CAST(InstructionsText.BankID AS Varchar(20))+'&customer='+CAST(InstructionsText.CustomerID AS Varchar(20))+'&lockbox='+CAST(InstructionsText.LockboxID AS Varchar(20))+'&type='+CAST(InstructionsText.Type AS Varchar(20))+'&tag='+RTRIM(InstructionsText.Tag)+'&asof='+CONVERT(varchar,MAX(InstructionsText.AsOf),101) AS linkurl
FROM InstructionsText
	LEFT OUTER JOIN InstructionTypes ON 
		InstructionsText.Type = InstructionTypes.InstructionTypeID
WHERE 
	InstructionsText.BankID = @parmSiteBankID 
	AND InstructionsText.CustomerID = @parmSiteCustomerID
GROUP BY 
	BankID, 
	CustomerID, 
	LockboxID, 
	Type, 
	InstructionTypes.Description, 
	InstructionsText.Description, 
	Tag
ORDER BY 
	BankID ASC, 
	CustomerID ASC, 
	LockboxID ASC, 
	Type ASC, 
	Tag ASC

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

