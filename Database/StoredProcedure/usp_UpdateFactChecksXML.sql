--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_UpdateFactChecksXML
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('[OLTA].[usp_UpdateFactChecksXML]') IS NOT NULL
	DROP PROCEDURE [OLTA].[usp_UpdateFactChecksXML]
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_UpdateFactChecksXML]
	@parmLockboxKey int,
	@parmProcessingDateKey int,
	@parmDepositDateKey int,
	@parmBatchID int,
	@parmBatchSequence int,
	@parmImageInfoXML xml
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/4/2009
*
* Purpose: Update fact checks XML for a given batch
*
* Modification History
* 05/04/2009 CR 25817 WJS	Created
* 02/07/2011 CR 32597 WJS 	Add modification date and changed type of processingdate to int
* 02/18/2011 CR 32597 WJS	Remove BankKey
* 02/28/2011 CR 32597 WJS 	Add depositDateKey
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	UPDATE	OLTA.factChecks 
	SET		ImageInfoXML = @parmImageInfoXML,
			ModificationDate = GETDATE()		
	FROM	OLTA.factChecks   
	WHERE	DepositDateKey = @parmDepositDateKey 
			AND LockboxKey=@parmLockboxKey  
			AND ProcessingDateKey = @parmProcessingDateKey
			AND BatchID=@parmBatchID 
			AND BatchSequence= @parmBatchSequence
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
