--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchDataUpdate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factBatchDataUpdate') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factBatchDataUpdate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_factBatchDataUpdate
(
	@parmSiteBankID				int,
	@parmSiteLockboxID			int, 
	@parmProcessingDateKey		int,
	@parmBatchID				int,
	@parmBatchDataSetupFieldKey	int,
	@parmDataValue				varchar(256),
	@parmDataValueDateTime		datetime,
	@parmDataValueMoney 		money,
	@parmDataValueInteger		int,
	@parmDataValueBit			bit
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR 33801 JMC	Created
******************************************************************************/

DECLARE @BankKey int
DECLARE @LockboxKey int
DECLARE @DepositDateKey int

BEGIN TRY

	EXEC OLTA.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteLockboxID = @parmSiteLockboxID,
		@parmBatchID = @parmBatchID,
		@parmProcessingDateKey = @parmProcessingDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmLockboxKey = @LockboxKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

	UPDATE 	OLTA.factBatchData
	SET 	DataValue = @parmDataValue,
			DataValueDateTime = @parmDataValueDateTime,
			DataValueMoney = @parmDataValueMoney,
			DataValueInteger = @parmDataValueInteger,
			DataValueBit = @parmDataValueBit,
			ModifiedBy = suser_sname(),
			ModificationDate = GetDate()
	FROM	OLTA.factBatchData 
	WHERE	OLTA.factBatchData.DepositDateKey = @DepositDateKey
			AND OLTA.factBatchData.LockboxKey = @LockboxKey
			AND OLTA.factBatchData.ProcessingDateKey = @parmProcessingDateKey
			AND OLTA.factBatchData.BatchID = @parmBatchID
			AND OLTA.factBatchData.BatchDataSetupFieldKey = @parmBatchDataSetupFieldKey

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
