--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICONProcessClientSetup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_ICONProcessClientSetup') IS NOT NULL
       DROP PROCEDURE OLTA.usp_ICONProcessClientSetup
GO

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/31/2011
*
* Purpose: Allows ICON to submit a client setup file directly to OLTA.
*
* Modification History
* 03/31/2011 CR 33602 JPB	Created
* 10/27/2012 WI 70463 WJS	Removed No longer used
******************************************************************************/
