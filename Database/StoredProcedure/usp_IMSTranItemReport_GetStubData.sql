--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSTranItemReport_GetStubData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSTranItemReport_GetStubData') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSTranItemReport_GetStubData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_IMSTranItemReport_GetStubData]
(
	@parmBankID				int,
	@parmLockboxID			int, 
	@parmProcessingDateKey	int,
	@parmDepositDateKey		int,
	@parmBatchID			int,
	@parmTransactionID      int,
	@parmBatchSourceKey		int
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/02/2011
*
* Purpose: Retrieve the data for Stub data for IMS report
*
* Modification History
* 04/02/2011 CR 33625 WJS	Created
* 10/10/2011 CR 47422 WJS Modify ItemReport to pull processingDate from factBatchData table
* 02/06/2012 CR 49998 WJS   Modified to get sourceProcessingDateKey
* 02/29/2012 CR 49998 WJS  Add ProcessingDateKey back
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON


DECLARE @FILEDESCRIPT varchar(32)
DECLARE @DOCUMENTBATCHSEQ int

BEGIN TRY


	IF EXISTS (
				SELECT	1
				FROM 	OLTA.factStubs
						INNER JOIN OLTA.dimLockboxes ON OLTA.factStubs.LockboxKey = OLTA.dimLockboxes.LockboxKey
				WHERE 	OLTA.factStubs.DepositDateKey = @parmDepositDateKey
						AND OLTA.dimLockboxes.SiteBankID = @parmBankID
						AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
						AND OLTA.factStubs.ProcessingDateKey = @parmProcessingDateKey
						AND OLTA.factStubs.BatchID = @parmBatchID
						AND OLTA.factStubs.TransactionID = @parmTransactionID
						AND OLTA.factStubs.BatchSourceKey = @parmBatchSourceKey
				)	
	BEGIN
		SELECT @DOCUMENTBATCHSEQ =OLTA.factStubs.DocumentBatchSequence
				FROM 	OLTA.factStubs
				 INNER JOIN OLTA.dimLockboxes ON OLTA.factStubs.LockboxKey = OLTA.dimLockboxes.LockboxKey
		WHERE 	OLTA.factStubs.DepositDateKey = @parmDepositDateKey
				AND OLTA.dimLockboxes.SiteBankID = @parmBankID
				AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
				AND OLTA.factStubs.ProcessingDateKey = @parmProcessingDateKey
				AND OLTA.factStubs.BatchID = @parmBatchID
				AND OLTA.factStubs.TransactionID = @parmTransactionID
				AND OLTA.factStubs.BatchSourceKey = @parmBatchSourceKey
			
		SELECT @FILEDESCRIPT = 	OLTA.dimDocumentTypes.FileDescriptor
				FROM 	OLTA.factDocuments
						INNER JOIN OLTA.dimLockboxes ON	OLTA.factDocuments.LockboxKey = OLTA.dimLockboxes.LockboxKey
						INNER JOIN olta.dimDocumentTypes ON	OLTA.factDocuments.DocumentTypeKey = OLTA.dimDocumentTypes.DocumentTypeKey
				WHERE	OLTA.factDocuments.DepositDateKey = @parmDepositDateKey
					AND OLTA.dimLockboxes.SiteBankID = @parmBankID
					AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
					AND OLTA.factDocuments.ProcessingDateKey = @parmProcessingDateKey
					AND OLTA.factDocuments.BatchID = @parmBatchID
					AND OLTA.factDocuments.TransactionID = @parmTransactionID
					AND OLTA.factDocuments.BatchSequence = @DOCUMENTBATCHSEQ
					AND OLTA.factDocuments.BatchSourceKey = @parmBatchSourceKey
		
		SELECT	OLTA.dimLockboxes.SiteBankID,
				OLTA.dimLockboxes.SiteLockboxID,
				OLTA.dimLockboxes.OnlineColorMode,
				OLTA.factStubs.ProcessingDateKey,
				OLTA.factStubs.SourceProcessingDateKey,
				OLTA.factStubs.DepositDateKey,
				OLTA.factStubs.BatchID,
				OLTA.factStubs.TransactionID,
				OLTA.factStubs.TxnSequence,
				OLTA.factStubs.BatchSequence,
				OLTA.factStubs.DocumentBatchSequence,
				OLTA.factStubs.Amount,
				OLTA.factStubs.AccountNumber,
				@FILEDESCRIPT  as filedescriptor			
		FROM 	OLTA.factStubs
				 INNER JOIN OLTA.dimLockboxes ON OLTA.factStubs.LockboxKey = OLTA.dimLockboxes.LockboxKey
		WHERE 	OLTA.factStubs.DepositDateKey = @parmDepositDateKey
				AND OLTA.dimLockboxes.SiteBankID = @parmBankID
				AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
				AND OLTA.factStubs.ProcessingDateKey = @parmProcessingDateKey
				AND OLTA.factStubs.BatchID = @parmBatchID
				AND OLTA.factStubs.TransactionID = @parmTransactionID
				AND OLTA.factStubs.BatchSourceKey = @parmBatchSourceKey
	END
	ELSE
	BEGIN
		SELECT	DISTINCT OLTA.factDataEntryDetails.BatchSequence
		FROM	OLTA.factDataEntryDetails
				INNER JOIN OLTA.dimLockboxes ON OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
				INNER JOIN OLTA.dimDates AS dimDepositDates ON OLTA.factDataEntryDetails.DepositDateKey = dimDepositDates.DateKey
				INNER JOIN OLTA.dimDates AS dimProcessingDates ON OLTA.factDataEntryDetails.ProcessingDateKey = dimProcessingDates.DateKey
				INNER JOIN OLTA.dimDataEntryColumns ON OLTA.factDataEntryDetails.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
		WHERE  	OLTA.dimLockboxes.SiteBankID = @parmBankID
				AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
				AND OLTA.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
				AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
				AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
				AND OLTA.dimDataEntryColumns.TableType IN (2,3)
	END
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @parmErrorMessage    NVARCHAR(4000),
				@parmErrorProcedure	 NVARCHAR(200),
				@parmErrorSeverity   INT,
				@parmErrorState      INT,
				@parmErrorLine		 INT
		
		SELECT	@parmErrorMessage = ERROR_MESSAGE(),
				@parmErrorSeverity = ERROR_SEVERITY(),
				@parmErrorState = ERROR_STATE(),
				@parmErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@parmErrorLine = ERROR_LINE()

		SET @parmErrorMessage = @parmErrorProcedure + ' (Line: %d)-> ' + @parmErrorMessage

		RAISERROR(@parmErrorMessage,@parmErrorSeverity,@parmErrorState,@parmErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
