--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_UpdatedimLockboxFileGroup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_UpdatedimLockboxFileGroup') IS NOT NULL
       DROP PROCEDURE OLTA.usp_UpdatedimLockboxFileGroup
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_UpdatedimLockboxFileGroup]
	@parmBankID int,
	@parmCustomerID int,
	@parmLockboxID int,
	@parmFileGroup varchar(max)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 09/23/2012
*
* Purpose: Update FileGroup for given Lockbox (only to the most recent)
*
* Modification History
* 09/23/2012 CR 56033 JNE - Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	Update OLTA.dimLockboxes
		SET OLTA.dimLockboxes.FileGroup = @parmFileGroup
	FROM OLTA.dimLockboxes 
	WHERE OLTA.dimLockboxes.SiteBankID = @parmBankID
		AND OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
		AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
		AND OLTA.dimLockboxes.MostRecent = 1
		
END TRY
BEGIN CATCH
	   EXEC OLTA.usp_WfsRethrowException
END CATCH

