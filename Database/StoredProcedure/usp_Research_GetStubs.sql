--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetStubs
--WFSScriptProcessorStoredProcedureDrop
IF EXISTS	(
				SELECT	* 
				FROM	INFORMATION_SCHEMA.ROUTINES 
				WHERE	SPECIFIC_SCHEMA = N'OLTA' 
						AND SPECIFIC_NAME = N'usp_Research_GetStubs' 
			)
   DROP PROCEDURE OLTA.usp_Research_GetStubs
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetStubs] 
       @parmSiteBankID int, 
       @parmSiteLockboxID int,
       @parmDepositDate datetime,
       @parmBatchID int,
       @parmTransactionID int
WITH RECOMPILE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/01/2010
*
* Purpose: 
*
* Modification History
* 02/01/2010 CR 28958 JNE   Created
* 04/05/2010 CR 29309 JPB   Account renamed to AccountNumber.
* 03/18/2011 CR 33440 JNE   Add BatchSourceKey, DepositDate, and ProcessingDate to return set.
* 01/19/2012 CR 49142 JNE   Add StubSequence to return set.
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT	OLTA.dimLockboxes.SiteBankID,
			OLTA.dimLockboxes.SiteLockboxID,
			OLTA.factStubs.DepositDatekey,
			OLTA.factStubs.BatchID,
			OLTA.factStubs.TransactionID,
			Convert(Char(8), OLTA.factStubs.ProcessingDateKey, 112) AS PICSDate,
			OLTA.factStubs.GlobalStubID,
			OLTA.factStubs.BatchSequence,
			OLTA.factStubs.Amount,
			OLTA.factStubs.AccountNumber,
			OLTA.factStubs.IsOMRDetected,
			OLTA.factStubs.DocumentBatchSequence,
			OLTA.factStubs.SystemType,
			OLTA.factStubs.BatchSourceKey,
			CONVERT(varchar, dimProcessingDates.CalendarDate, 101) AS ProcessingDate,
			CONVERT(varchar, dimDepositDates.CalendarDate, 101) AS DepositDate,
			OLTA.factStubs.StubSequence
	FROM	OLTA.factStubs
			INNER JOIN OLTA.dimLockboxes ON OLTA.factStubs.LockboxKey = OLTA.dimLockboxes.LockboxKey
			INNER JOIN OLTA.dimDates AS dimDepositDates ON OLTA.factStubs.DepositDateKey = dimDepositDates.DateKey
			INNER JOIN OLTA.dimDates AS dimProcessingDates ON OLTA.factStubs.ProcessingDateKey = dimProcessingDates.DateKey
	WHERE  	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
			AND OLTA.factStubs.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
			AND OLTA.factStubs.BatchID = @parmBatchID
			AND OLTA.factStubs.TransactionID = @parmTransactionID
	ORDER BY OLTA.factStubs.BatchID ASC,
			OLTA.factStubs.TxnSequence ASC,
			OLTA.factStubs.SequenceWithinTransaction ASC

END TRY
BEGIN CATCH
       EXEC OLTA.usp_WfsRethrowException
END CATCH
