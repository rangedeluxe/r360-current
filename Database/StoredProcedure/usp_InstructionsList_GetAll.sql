--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureCreate 
--WFSScriptProcessorStoredProcedureName usp_InstructionsList_GetAll
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_InstructionsList_GetAll') IS NOT NULL
       DROP PROCEDURE OLTA.usp_InstructionsList_GetAll
GO
CREATE PROCEDURE [OLTA].[usp_InstructionsList_GetAll]
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/24/2010
*
* Purpose: Retreive instructions for all bank/lockboxes with formated URL.
*
* Modification History
* 02/24/2010 CR 29093 JNE	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT	dbo.InstructionsText.BankID,
			dbo.InstructionsText.CustomerID,
			dbo.InstructionsText.LockboxID,
			dbo.InstructionsText.[Type],
			ISNULL(RTRIM(dbo.InstructionTypes.[Description]), 'Not Defined') AS InstructionType,
			RTRIM(dbo.InstructionsText.[Description]) AS InstructionsDescription,
			RTRIM(dbo.InstructionsText.Tag) AS Tag,
			MAX(dbo.InstructionsText.AsOf) AS AsOf, 
			'instructions_viewer.aspx?bank='+CAST(dbo.InstructionsText.BankID AS Varchar(20))+'&customer='+CAST(dbo.InstructionsText.CustomerID AS Varchar(20))+'&lockbox='+CAST(dbo.InstructionsText.LockboxID AS Varchar(20))+'&type='+CAST(dbo.InstructionsText.Type AS Varchar(20))+'&tag='+RTRIM(dbo.InstructionsText.Tag)+'&asof='+CONVERT(varchar,MAX(dbo.InstructionsText.AsOf),101) AS linkurl
	FROM	dbo.InstructionsText
			LEFT OUTER JOIN dbo.InstructionTypes ON dbo.InstructionsText.[Type] = dbo.InstructionTypes.InstructionTypeID
	GROUP BY dbo.InstructionsText.BankID, 
			dbo.InstructionsText.CustomerID, 
			dbo.InstructionsText.LockboxID, 
			dbo.InstructionsText.[Type], 
			dbo.InstructionTypes.[Description], 
			dbo.InstructionsText.[Description], 
			dbo.InstructionsText.Tag
	ORDER BY dbo.InstructionsText.BankID ASC, 
			dbo.InstructionsText.CustomerID ASC, 
			dbo.InstructionsText.LockboxID ASC, 
			dbo.InstructionsText.[Type] ASC, 
			dbo.InstructionsText.Tag ASC

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

