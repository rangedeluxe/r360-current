--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_ProcessRequest
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSInterfaceQueue_ProcessRequest') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSInterfaceQueue_ProcessRequest
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSInterfaceQueue_ProcessRequest
		@XBatch					xml,
		@parmBankKey			int,
		@parmCustomerKey		int,
		@parmLockboxKey			int, 
		@parmProcessingDateKey	int,
		@parmDepositDateKey		int,
		@parmBatchID			int,
		@parmActionCode			int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/01/2009
*
* Purpose: Insert a message into the IMS queue table to alert the IMS system that
*     new documents have arrived for processing.
*<Batch ProcessingDate="10/07/2009" BankID="1" LockboxID="123" BatchID="1000"/>
* Modification History
* 07/01/2009 CR 25817 JPB	Created
* 12/15/2009 CR 28501 JPB	Corrected misspelled ProcessingDate
* 12/26/2009 CR 28559 JPB	Performance improvements:
*								Keys passed in
* 01/15/2010 CR 28719 JPB	Updates for ICON.
* 02/02/2010 CR 28862 JPB	Check for nest level so the correct message is passed
*								back to the calling stored procedure.
* 10/05/2010 CR 31171  CS	Updated to account for OLTA.IMSInterfaceQueue schema changes
* 02/16/2011 CR 32834  CS	Removed calls to usp_IMSCheckDelete and usp_IMSDocumentDelete
* 05/13/2011 CR 34333 JMC	Xml will now be NULL for all non-DELETE requests.
* 06/27/2011 CR 45198 WJS	Populate SiteID, populate XML for ICON Batches since we can't tell difference for ICON
* 11/12/2011 CR 48210 WJS	Remove populate of XML Data
* 01/04/2013 WI 84015 JPB	Removed SiteID
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON
DECLARE	@bDataStatus BIT,
		@bImageStatus BIT,
		@iQueueType SMALLINT,
		@iQueueStatus INT,
		@iSourceIdentifier SMALLINT,
		@xmlData xml,
		@dtTimeStamp datetime,
		@guidQueueID uniqueidentifier,
		@SiteBankID int,
		@SiteLockboxID int

BEGIN TRY

	SELECT	@SiteBankID = Batch.att.value('@BankID','int'),
			@SiteLockboxID = Batch.att.value('@LockboxID','int')
	FROM	@XBatch.nodes('/Batches/Batch') AS Batch(att)
	
	IF @parmActionCode = 6
	BEGIN
			-- make sure the SSB process creates a row in IMSInterfaceQueue table with QueueType = 0 and QueueStatus = 10              
			-- then, insert a row in IMSInterfaceQueue table with QueueType = 1, QueueImageStatus = 1, QueueDataStatus = 1, QueueStatus = 10
	
			
		SET @dtTimeStamp = GETDATE()
--
		--EXEC OLTA.usp_IMSCheckDelete @parmBankKey,@parmCustomerKey,@parmLockboxKey,@parmProcessingDateKey,@parmDepositDateKey,@parmBatchID,@SiteBankID,@SiteLockboxID
		--EXEC OLTA.usp_IMSDocumentDelete @parmBankKey,@parmCustomerKey,@parmLockboxKey,@parmProcessingDateKey,@parmDepositDateKey,@parmBatchID,@SiteBankID,@SiteLockboxID
--
		INSERT INTO OLTA.IMSInterfaceQueue 
			(
					SiteBankID,
					SiteLockboxID,
					ProcessingDateKey,
					BatchID,
					QueueType,
					QueueData,
					QueueDataStatus,
					QueueImageStatus,
					QueueStatus,
					CreationDate,
					ModificationDate
			) 
		VALUES 
			(
					@SiteBankID,
					@SiteLockboxID,
					@parmProcessingDateKey,
					@parmBatchID,
					1,
					NULL,
					1,
					1,
					10,
					DATEADD(millisecond,5,@dtTimeStamp),
					DATEADD(millisecond,5,@dtTimeStamp)
			)
	END
	ELSE
	BEGIN
		IF @parmActionCode <> 4
			AND EXISTS( SELECT 1 
								FROM  OLTA.IMSInterfaceQueue 
								WHERE OLTA.IMSInterfaceQueue.SiteBankID = @SiteBankID
											AND OLTA.IMSInterfaceQueue.SiteLockboxID = @SiteLockboxID
											AND OLTA.IMSInterfaceQueue.ProcessingDateKey = @parmProcessingDateKey
											AND OLTA.IMSInterfaceQueue.BatchID = @parmBatchID
											AND OLTA.IMSInterfaceQueue.QueueStatus = 0) 
		BEGIN -- record already exists, update the status
			SELECT      @guidQueueID = QueueID,
						@bDataStatus = QueueDataStatus,
						@bImageStatus = QueueImageStatus,
						@iQueueType = QueueType
			FROM  OLTA.IMSInterfaceQueue
			WHERE OLTA.IMSInterfaceQueue.SiteBankID = @SiteBankID
						AND OLTA.IMSInterfaceQueue.SiteLockboxID = @SiteLockboxID
						AND OLTA.IMSInterfaceQueue.ProcessingDateKey = @parmProcessingDateKey
						AND OLTA.IMSInterfaceQueue.BatchID = @parmBatchID
						AND OLTA.IMSInterfaceQueue.QueueStatus = 0

			IF @parmActionCode = 1
					SET @bDataStatus = 1
			IF @parmActionCode = 2
					SET @bImageStatus = 1
			IF (@bDataStatus = 1 AND @bImageStatus = 1 AND @iQueueType = 1)
					OR (@bDataStatus = 1 AND @bImageStatus = 1 AND @iQueueType = 2 AND @parmActionCode = 1)
			BEGIN
					UPDATE OLTA.IMSInterfaceQueue
					SET         QueueDataStatus = 1,
								QueueImageStatus = 1,
								QueueStatus = 10,
								ModificationDate = GETDATE(),
								ModifiedBy = SUSER_SNAME() 
					WHERE       QueueID = @guidQueueID
			END
			
			IF @bDataStatus = 1 AND @bImageStatus = 1 AND @iQueueType = 2 AND @parmActionCode = 2
			BEGIN
					UPDATE OLTA.IMSInterfaceQueue
					SET         QueueDataStatus = 1,
								QueueImageStatus = 1,
								QueueStatus = 10,
								QueueData = NULL,
								ModificationDate = GETDATE(),
								ModifiedBy = SUSER_SNAME() 
					WHERE       QueueID = @guidQueueID
			END
		END -- record already exists, update the status
		ELSE
		BEGIN -- record is new, insert it
			--set the default status
			SET @iQueueStatus = 0
			
			--set status based on action code
			IF @parmActionCode = 1 --data complete
					SET @bDataStatus = 1
			ELSE SET @bDataStatus = 0
			
			IF @parmActionCode = 2 --image complete
					SET @bImageStatus = 1
			ELSE SET @bImageStatus = 0
--			
--			IF @parmActionCode = 4 OR @parmActionCode = 6 --delete request
--			BEGIN --Delete Request
--				EXEC OLTA.usp_IMSCheckDelete @parmBankKey,@parmCustomerKey,@parmLockboxKey,@parmProcessingDateKey,@parmDepositDateKey,@parmBatchID,@SiteBankID,@SiteLockboxID
--				EXEC OLTA.usp_IMSDocumentDelete @parmBankKey,@parmCustomerKey,@parmLockboxKey,@parmProcessingDateKey,@parmDepositDateKey,@parmBatchID,@SiteBankID,@SiteLockboxID
--			END
--			ELSE 
--
			IF (@parmActionCode NOT IN(4, 6)) --delete request
			BEGIN
				SELECT	@iQueueType = Batch.att.value('@SystemType','smallint'),
						@iSourceIdentifier = Batch.att.value('@SourceIdentifier', 'smallint')
				FROM  @XBatch.nodes('/Batches/Batch') AS Batch(att)

				IF @iQueueType = 0
					SET @iQueueType = 1

				IF @iSourceIdentifier IS NULL
					SET @iSourceIdentifier = 0
					
				IF @iQueueType = 2 AND @iSourceIdentifier = 0
					SET @iQueueType = 1 --this is a retail batch from integraPAY, so it has to be queue type 1
					
				----IF @iQueueType = 1
				----	SELECT @xmlData = 
				----			(
				----				SELECT	Batch.att.value('@ProcessingDate','datetime') AS ProcessingDate,
				----						Batch.att.value('@BankID','int') AS BankID,
				----						Batch.att.value('@LockboxID','int') AS LockboxID,
				----						Batch.att.value('@BatchID','int') AS BatchID
				----				FROM  @XBatch.nodes('/Batches/Batch') AS Batch(att)
				----				FOR XML AUTO, TYPE
				----			)
				
				----IF @iQueueType = 2 AND @parmActionCode = 1
				----	SET @xmlData = ''
				----IF @iQueueType = 2 AND @parmActionCode = 2
				----	SET @xmlData = @XBatch 
				
				INSERT INTO OLTA.IMSInterfaceQueue 
					(
							SiteBankID,
							SiteLockboxID,
							ProcessingDateKey,
							BatchID,
							QueueType,
							QueueData,
							QueueDataStatus,
							QueueImageStatus,
							QueueStatus
					) 
				VALUES 
					(
							@SiteBankID,
							@SiteLockboxID,
							@parmProcessingDateKey,
							@parmBatchID,
							@iQueueType,
							NULL,
							@bDataStatus,
							@bImageStatus,
							@iQueueStatus
					)
			END
		END
	END
END TRY
BEGIN CATCH
      DECLARE @ErrorMessage   NVARCHAR(4000),
                  @ErrorSeverity    INT,
                  @ErrorState       INT,
                  @ErrorLine        INT
      SELECT      @ErrorMessage = ERROR_MESSAGE(),
                  @ErrorSeverity = ERROR_SEVERITY(),
                  @ErrorState = ERROR_STATE(),
                  @ErrorLine = ERROR_LINE()
      --CR 28862
--      IF @@NESTLEVEL > 4 --only alter the message if we got here by XBatchImport->XBatchDelete
            SET @ErrorMessage = 'usp_IMSInterfaceQueue_ProcessRequest (Line: %d)-> ' + @ErrorMessage
      RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
END CATCH
