--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetDocumentSequenceIMSMapping
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetDocumentSequenceIMSMapping') IS NOT NULL
	DROP PROCEDURE OLTA.usp_GetDocumentSequenceIMSMapping
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetDocumentSequenceIMSMapping] 
	@parmSiteBankID int, 
	@parmSiteLockboxID int,
	@parmProcessingDateKey int, 
	@parmBatchID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/30/2011
*
* Purpose: Query DocumentSequenceIMSMapping Table
*
* Modification History
* 05/30/2011 CR 45018 WJS		Created
******************************************************************************/

BEGIN TRY

	SELECT OldBatchSequence, NewBatchSequence
	FROM OLTA.DocumentSequenceIMSMapping	
	WHERE BankID = @parmSiteBankID 
		AND LockboxID = @parmSiteLockboxID 
		AND ProcessingDateKey = @parmProcessingDateKey
		AND BatchID = @parmBatchID	

END TRY
BEGIN CATCH
       EXEC OLTA.usp_WfsRethrowException
END CATCH
