--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_dimCustomersView_Get_CustomerInfoByLockboxNumber
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_dimCustomersView_Get_CustomerInfoByLockboxNumber') IS NOT NULL
       DROP PROCEDURE OLTA.usp_dimCustomersView_Get_CustomerInfoByLockboxNumber
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_dimCustomersView_Get_CustomerInfoByLockboxNumber 
(
    @parmLockboxNumber INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 12/12/2012
*
* Purpose: Return Customer information using Lockbox number for search
*
* Modification History
* 12/12/2012 WI 86274 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT
        OLTA.dimLockboxesView.SiteLockboxID   AS 'Lockbox Number',
        RTrim(OLTA.dimLockboxesView.LongName) AS 'Lockbox Name',
        RTrim(OLTA.dimLockboxesView.POBox)    AS 'P.O. Box',
        OLTA.dimLockboxesView.SiteCustomerID  AS 'Customer Number',
        RTrim(OLTA.dimCustomersView.Name)     AS 'Customer Name',
        OLTA.dimLockboxesView.SiteBankID      AS 'Bank Number',
        RTrim(OLTA.dimBanksView.BankName)     AS 'Bank Name'
    FROM 
        OLTA.dimLockboxesView
        INNER JOIN OLTA.dimCustomersView ON 
            OLTA.dimLockboxesView.SiteBankID = OLTA.dimCustomersView.SiteBankID 
            AND OLTA.dimLockboxesView.SiteCustomerID = OLTA.dimCustomersView.SiteCustomerID
        INNER JOIN OLTA.dimBanksView ON 
            OLTA.dimLockboxesView.SiteBankID = OLTA.dimBanksView.SiteBankID
    WHERE 
       OLTA. dimLockboxesView.SiteLockboxID = @parmLockboxNumber
    ORDER BY
        OLTA.dimLockboxesView.SiteLockboxID, 
        OLTA.dimLockboxesView.LongName, 
        OLTA.dimLockboxesView.POBox, 
        OLTA.dimLockboxesView.SiteCustomerID, 
        OLTA.dimCustomersView.Name, 
        OLTA.dimLockboxesView.SiteBankID, 
        OLTA.dimBanksView.BankName ASC;

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

