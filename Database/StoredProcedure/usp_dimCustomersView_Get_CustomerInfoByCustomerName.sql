--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_dimCustomersView_Get_CustomerInfoByCustomerName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_dimCustomersView_Get_CustomerInfoByCustomerName') IS NOT NULL
       DROP PROCEDURE OLTA.usp_dimCustomersView_Get_CustomerInfoByCustomerName
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_dimCustomersView_Get_CustomerInfoByCustomerName 
(
    @parmCustomerName VARCHAR(20)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 12/12/2012
*
* Purpose: Return Customer information using Customer Name for search
*
* Modification History
* 12/12/2012 WI 86269 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT
        RTrim(OLTA.dimCustomersView.Name)    AS 'Customer Name',
        OLTA.dimCustomersView.SiteCustomerID AS 'Customer Number',
        OLTA.dimCustomersView.SiteBankID     AS 'Bank Number',
        RTRim(OLTA.dimBanksView.BankName)    AS 'Bank Name'
    FROM
        OLTA.dimCustomersView 
        INNER JOIN OLTA.dimBanksView ON
            OLTA.dimCustomersView.SiteBankID = OLTA.dimBanksView.SiteBankID
    WHERE
        OLTA.dimCustomersView.Name LIKE '%' + @parmCustomerName + '%'
    ORDER BY 
        OLTA.dimCustomersView.Name,
        OLTA.dimCustomersView.SiteCustomerID,
        OLTA.dimCustomersView.SiteBankID,
        OLTA.dimBanksView.BankName;

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

