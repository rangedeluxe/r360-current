--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetHOAPMACustomers
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetHOAPMACustomers') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetHOAPMCustomers
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetHOAPMACustomers] 
	@parmUserID int,
    @parmDepositDateKey int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/26/2012
*
* Purpose: Query to retrieve the HOA active Customer ID and Customer Name giving user and deposit date.
*
* Modification History
* 02/11/2012 CR 50234 JNE	Created
* 05/02/2012 CR 52472 JNE	Added Customersview & lockboxesview to display the most recent names.
* 07/09/2012 CR 54028 JNE	Limit SuperUser to OLCustomerID assigned
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	DECLARE @IsSuperUser bit

	SELECT	@IsSuperUser = SuperUser 
	FROM	OLTA.Users 
	WHERE	OLTA.Users.UserID = @parmUserID
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
	DROP TABLE #tmpHOALockboxTemp
	
	--Create a temp table to hold dimlockbox info
	CREATE TABLE #tmpHOALockboxTemp
	(
		CustomerKey INT,
		LockboxKey INT,
		SiteCustomerID INT,
		Customer_Name VARCHAR(20),
		Lockbox_Number INT,
		Lockbox_Name VARCHAR(40)
	)
	
	IF @IsSuperUser > 0
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomersView.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxesView.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.dimCustomersView ON OLTA.dimCustomers.SiteBankID = OLTA.dimCustomersView.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimCustomersView.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.dimLockboxesView ON OLTA.dimLockboxesView.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimLockboxesView.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID AND OLTA.dimLockboxesView.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.Users ON OLTA.Users.OLCustomerID = OLTA.OLLockboxes.OLCustomerID
		WHERE OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.Users.UserID = @parmUserID	
	END
	ELSE
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomersView.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxesView.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.dimCustomersView ON OLTA.dimCustomers.SiteBankID = OLTA.dimCustomersView.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimCustomersView.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.dimLockboxesView ON OLTA.dimLockboxesView.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimLockboxesView.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID AND OLTA.dimLockboxesView.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.OLLockboxUsers ON OLTA.OLLockboxUsers.OLLockboxID = OLTA.OLLockboxes.OLLockboxID
		WHERE OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.OLLockboxUsers.UserID = @parmUserID
	END
	
	SELECT DISTINCT #tmpHOALockboxTemp.SiteCustomerID as Customer_ID,
			#tmpHOALockboxTemp.Customer_Name
	FROM OLTA.factbatchsummary
		INNER JOIN #tmpHOALockboxTemp ON #tmpHOALockboxTemp.CustomerKey = OLTA.factbatchsummary.CustomerKey AND #tmpHOALockboxTemp.LockboxKey = OLTA.factbatchsummary.LockboxKey
	WHERE OLTA.factbatchsummary.DepositDateKey = @parmDepositDateKey

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp

	EXEC OLTA.usp_wfsRethrowException
END CATCH
