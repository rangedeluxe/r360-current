﻿--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Session_Upd_NextPage
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Session_Upd_NextPage') IS NOT NULL
    DROP PROCEDURE OLTA.usp_Session_Upd_NextPage
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_Session_Upd_NextPage 
(
	@parmNextPage		INT, 
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmRowsReturned	INT = 0	OUTPUT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/14/2013
*
* Purpose: Update Session Log for Next Page
*
* Modification History
* 01/14/2013 WI 85325 CRG Write store procedure to replace ExecuteSQL(SQLSession.SQL_UpdateSession(NextPage, SessionID), out RowsAffected)
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	UPDATE	
		OLTA.[Session]
	SET		
		OLTA.[Session].LastPageServed	= GETDATE(),
		OLTA.[Session].PageCounter		= @parmNextPage
	WHERE	
		OLTA.[Session].SessionID		= @parmSessionID

	SELECT	
		@parmRowsReturned = @@ROWCOUNT;     
END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH
