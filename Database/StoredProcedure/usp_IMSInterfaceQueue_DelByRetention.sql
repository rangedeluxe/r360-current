--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_DelByRetention
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSInterfaceQueue_DelByRetention') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSInterfaceQueue_DelByRetention
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSInterfaceQueue_DelByRetention
       @MaxItemsToDelete INT = 100
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/07/2011
*
* Purpose: Delete old records for OLTA.IMSInterfaceQueue table.
*
* Modification History
* 10/07/2011 CR 47249 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON

DECLARE @DaysToKeep INT,
		@Continue BIT

BEGIN TRY
	/* Get the number of days to keep from the SystemSetup table. */
	SELECT @DaysToKeep = CAST(Value AS INT)
	FROM dbo.SystemSetup 
	WHERE Section = 'OLTA Table Maintenance' AND SetupKey = 'IMSInterfaceQueue'	
	
	/* no transaction control required as this is a single table delete */
	SET @Continue = 1
	WHILE( @Continue = 1 )
	BEGIN
		DELETE TOP(@MaxItemsToDelete)
		FROM  OLTA.IMSInterfaceQueue
		WHERE ModificationDate <= DATEADD(DAY,-@DaysToKeep,GETDATE())
		
		IF( @@ROWCOUNT > 0 )
			SET @Continue = 1
		ELSE SET @Continue = 0
	END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException
END CATCH
