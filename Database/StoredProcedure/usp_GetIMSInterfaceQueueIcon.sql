--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetIMSInterfaceQueueIcon
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('OLTA.usp_GetIMSInterfaceQueueIcon') IS NOT NULL
	DROP PROCEDURE [OLTA].[usp_GetIMSInterfaceQueueIcon]
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetIMSInterfaceQueueIcon]
       @paramNumItemsToRetrive smallint = 5
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
** Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/15/2009
*
* Purpose: Get the data from the interface queue.
*
* Modification History
* 10/15/2009 CR 27910 WJS 	Created
* 11/05/2009 CR 28112 WJS	Updated QueueDataStatus from 10 to 1.
* 11/12/2009 CR 28112 WJS	Added QueueStatus
* 12/16/2009 CR 28507 WJS	Added transaction and temp table. Also update status in stored proc
* 6/10/2011  CR 45200 WJS   Returned additional fields for use when queueData is Empty
* 06/13/2012 CR 53437 WJS   Remove siteID
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRANSACTION IMSInterfaceQueue_Icon
	
BEGIN TRY
	
	DECLARE @IMSInterfaceQueue_Icon TABLE
	(
		[QueueId] [uniqueidentifier] NOT NULL
	)

	INSERT INTO @IMSInterfaceQueue_Icon 
	SELECT  TOP(@paramNumItemsToRetrive)  QueueId 
	FROM	OLTA.IMSInterfaceQueue 
	WHERE	QueueDataStatus = 1  AND QueueType = 2 AND QueueStatus=10
	ORDER BY 
			CreationDate ASC


	SELECT  TOP(@paramNumItemsToRetrive)  QueueId,QueueData,SiteBankID, SiteLockboxID, ProcessingDateKey,BatchID
	FROM	OLTA.IMSInterfaceQueue 
	WHERE	QueueDataStatus = 1  AND QueueType = 2 and QueueStatus=10
	ORDER BY 
			CreationDate ASC
	
	UPDATE OLTA.IMSInterfaceQueue  
	SET QueueStatus = 20 
	FROM OLTA.IMSInterfaceQueue 
	INNER JOIN @IMSInterfaceQueue_Icon AS IC ON IC.QueueId = OLTA.IMSInterfaceQueue.QueueId

	COMMIT TRANSACTION IMSInterfaceQueue_Icon

END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION IMSInterfaceQueue_Icon
	EXEC OLTA.usp_WfsRethrowException
END CATCH
