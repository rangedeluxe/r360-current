--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_UpdateUserPassword
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_UpdateUserPassword') IS NOT NULL
       DROP PROCEDURE OLTA.usp_UpdateUserPassword
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_UpdateUserPassword]
	@parmUserID				int,
	@parmEncryptedPassword	varchar(44),
	@parmIsFirstTime		bit,
	@parmHistoryRetentionDays int,
	@parmIsForgotPassword     bit,
	@parmForgotPasswordMinutes int,
	@parmSQLErrorNumber int output
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Change Password
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 07/25/2011 CR 45440 JPB	Changed @parmEncryptedPassword length to 44.
* 08/08/2011 CR 45956 WJS 	Added support for password history
* 09/26/2011 CR 46993 WJS 	Add support for password expired time
******************************************************************************/
--SET NOCOUNT ON 
DECLARE @ErrorMessage	NVARCHAR(4000),
		@ErrorProcedure	NVARCHAR(200),
		@ErrorSeverity	INT,
		@ErrorState		INT,
		@ErrorLine		INT

DECLARE  @passwordExpiredTime dateTime

BEGIN TRY

	SET @parmSQLErrorNumber = 0
	SET @passwordExpiredTime = NULL

	if @parmIsForgotPassword  = 1
	BEGIN
		SET @passwordExpiredTime = DATEADD(mi,@parmForgotPasswordMinutes, GETDATE())
	END    
	EXEC OLTA.usp_RemoveUserPasswordHistory @parmUserID, @parmHistoryRetentionDays

	BEGIN TRY
		EXEC OLTA.usp_UserPasswordExistsInHistory @parmUserID, @parmEncryptedPassword

		UPDATE	OLTA.Users 
		SET		[Password] = @parmEncryptedPassword,
				PasswordSet = GETDATE() ,
				IsFirstTime = @parmIsFirstTime, 
				FailedLogonAttempts = 0 ,
				IsActive = 1, 
				PasswordExpirationTime = @passwordExpiredTime,
				ModificationDate=GETDATE(),
				ModifiedBy=SUSER_SNAME()
		WHERE	UserID=@parmUserID

		EXEC OLTA.usp_AddUserPasswordHistory @parmUserID, @parmEncryptedPassword
	END TRY
	BEGIN CATCH
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-')

		/* if the error coming back from OLTA.usp_UserPasswordExistsInHistory is not 16, then a real error happened so pass it on. */		
		IF( @ErrorSeverity = 16 AND @ErrorState = 1 AND @ErrorMessage = 'Password Exists' )
		BEGIN
			SET @parmSQLErrorNumber = 1
		END
		ELSE 
		BEGIN
			SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage
			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
		END
	END CATCH
END TRY
BEGIN CATCH
	SET @parmSQLErrorNumber = 2
	EXEC OLTA.usp_WfsRethrowException
END CATCH

                   
