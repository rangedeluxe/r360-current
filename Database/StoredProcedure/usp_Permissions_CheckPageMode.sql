--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Permissions_CheckPageMode
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Permissions_CheckPageMode') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Permissions_CheckPageMode
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Permissions_CheckPageMode] 
	@parmScriptFile	varchar(255),
	@parmPermissionMode varchar(30),
	@parmUserID	int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28697 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT PermissionName
FROM Permissions
    INNER JOIN UserPermissions ON
        Permissions.PermissionID = UserPermissions.PermissionID
WHERE
    LOWER(Permissions.ScriptFile) = LOWER(@parmScriptFile)
    AND LOWER(Permissions.PermissionMode)= LOWER(@parmPermissionMode)
    AND UserPermissions.UserID= @parmUserID


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

