--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetImageDisplayModeForBatch
--WFSScriptProcessorStoredProcedureDrop
IF EXISTS	(
				SELECT	* 
				FROM	INFORMATION_SCHEMA.ROUTINES 
				WHERE	SPECIFIC_SCHEMA = N'OLTA' 
						AND SPECIFIC_NAME = N'usp_Research_GetImageDisplayModeForBatch' 
			)
   DROP PROCEDURE OLTA.usp_Research_GetImageDisplayModeForBatch
GO
