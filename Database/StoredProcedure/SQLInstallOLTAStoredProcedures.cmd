@echo off

REM Call SQL Install with replacement arguments for installshield

REM %1 = server
REM %2 = user
REM %3 = password
REM %4 = log file path

SET _LOG_=%4
if (%_LOG_%)==() SET _LOG_=.\SqlInstallOLTAStoredProcedures.log

SET _WFSSIPATH_=c:\WFSSI\
IF NOT EXIST %_WFSSIPATH_%wfssi.exe SET _WFSSIPATH_=

echo Calling %_WFSSIPATH_%wfssi.exe /s=%1 /u=%2 /p=***** /quiet /verbose /log=%_LOG_% SQLInstallOLTAStoredProcedures.xml /args=SQLInstallOLTAArgs.xml
%_WFSSIPATH_%wfssi.exe /s=%1 /u=%2 /p=%3 /quiet /verbose /log=%_LOG_% SQLInstallOLTAStoredProcedures.xml /args=SQLInstallOLTAArgs.xml

IF %ERRORLEVEL%==0 GOTO DONE

echo.
echo There was an error running the SQL Install utility. See log at %_LOG_%
echo.

pause

:DONE
