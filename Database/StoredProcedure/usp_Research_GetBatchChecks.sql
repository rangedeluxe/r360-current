--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetBatchChecks
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Research_GetBatchChecks') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Research_GetBatchChecks
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetBatchChecks]
	@parmSiteBankID int,
	@parmSiteLockboxID int,
	@parmBatchID int,
	@parmDepositDate DateTime,
	@parmMinDepositStatus int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28692 JMC	Created
* 01/19/2010 CR 32262 JPB	Added Batch Source values to result set.
* 03/18/2011 CR 33441 JNE	Add BatchSourceKey and ProcessingDate to result set.
* 03/15/2012 CR 49158 JNE	Add BatchSiteCode to result set.
* 06/21/2012 CR 53621 JNE	Add BatchCueID to result set.
* 07/20/2012 CR 54164 JNE	Add BatchNumber to result set.
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

IF @parmBatchID > 0
BEGIN
SELECT
    Convert(Char(8), factChecks.ProcessingDateKey) AS PICSDate,
    dimLockboxes.SiteBankID        AS BankID, 
    dimLockboxes.SiteLockboxID     AS LockboxID, 
    dimLockboxes.SiteCustomerID    AS CustomerID, 
    factChecks.BatchID, 
    dimDepositDates.CalendarDate     AS DepositDate, 
    factChecks.TransactionID,
    factChecks.TxnSequence, 
    factChecks.BatchSequence,
    RTRIM(factChecks.Serial) AS Serial, 
    factChecks.Amount,
    RTRIM(dimRemitters.RoutingNumber) AS RT, 
    RTRIM(dimRemitters.Account) AS Account,
    RTRIM(dimRemitters.RemitterName) AS RemitterName,
    (SELECT StatusDisplayName 
     FROM StatusTypes 
     WHERE 
		StatusType = 'Deposit' 
		AND StatusValue = factChecks.DepositStatus) AS DepositStatusDisplay,
    dimBatchSources.ShortName AS BatchSourceShortName,
    dimBatchSources.LongName AS BatchSourceLongName,
    factChecks.BatchSourceKey,
    CONVERT(varchar, dimProcessingDates.CalendarDate, 101) AS ProcessingDate,
    factChecks.BatchSiteCode,
    factChecks.BatchCueID,
    factChecks.BatchNumber
FROM factChecks 
    INNER JOIN dimLockboxes ON 
        factChecks.LockboxKey = dimLockboxes.LockboxKey
    INNER JOIN dimDates AS dimDepositDates ON 
        factChecks.DepositDateKey = dimDepositDates.DateKey
    INNER JOIN dimRemitters ON 
        factChecks.RemitterKey = dimRemitters.RemitterKey
	INNER JOIN dimBatchSources ON
		factChecks.BatchSourceKey = dimBatchSources.BatchSourceKey
    INNER JOIN dimDates AS dimProcessingDates ON
        factChecks.ProcessingDateKey = dimProcessingDates.DateKey
WHERE dimLockboxes.SiteBankID = @parmSiteBankID
    AND dimLockboxes.SiteLockboxID = @parmSiteLockboxID
	AND factChecks.BatchID = @parmBatchID
    AND factChecks.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
    AND factChecks.DepositStatus >= @parmMinDepositStatus
ORDER BY 
    factChecks.ProcessingDateKey ASC, 
    factChecks.BatchID ASC, 
    factChecks.TxnSequence ASC, 
    factChecks.SequenceWithinTransaction ASC
END
ELSE
BEGIN
SELECT
    Convert(Char(8), factChecks.ProcessingDateKey) AS PICSDate,
    dimLockboxes.SiteBankID        AS BankID, 
    dimLockboxes.SiteLockboxID     AS LockboxID, 
    dimLockboxes.SiteCustomerID    AS CustomerID, 
    factChecks.BatchID, 
    dimDepositDates.CalendarDate     AS DepositDate, 
    factChecks.TransactionID,
    factChecks.TxnSequence, 
    factChecks.BatchSequence,
    RTRIM(factChecks.Serial) AS Serial, 
    factChecks.Amount,
    RTRIM(dimRemitters.RoutingNumber) AS RT, 
    RTRIM(dimRemitters.Account) AS Account,
    RTRIM(dimRemitters.RemitterName) AS RemitterName,
    (SELECT StatusDisplayName 
     FROM StatusTypes 
     WHERE 
		StatusType = 'Deposit' 
		AND StatusValue = factChecks.DepositStatus) AS DepositStatusDisplay,
    dimBatchSources.ShortName AS BatchSourceShortName,
    dimBatchSources.LongName AS BatchSourceLongName,
    factChecks.BatchSourceKey,
    CONVERT(varchar, dimProcessingDates.CalendarDate, 101) AS ProcessingDate,
    factChecks.BatchSiteCode,
    factChecks.BatchCueID,
    factChecks.BatchNumber
FROM factChecks 
    INNER JOIN dimLockboxes ON 
        factChecks.LockboxKey = dimLockboxes.LockboxKey
    INNER JOIN dimDates AS dimDepositDates ON 
        factChecks.DepositDateKey = dimDepositDates.DateKey
    INNER JOIN dimRemitters ON 
        factChecks.RemitterKey = dimRemitters.RemitterKey
	INNER JOIN dimBatchSources ON
		factChecks.BatchSourceKey = dimBatchSources.BatchSourceKey
    INNER JOIN dimDates AS dimProcessingDates ON
        factChecks.ProcessingDateKey = dimProcessingDates.DateKey
WHERE dimLockboxes.SiteBankID = @parmSiteBankID
    AND dimLockboxes.SiteLockboxID = @parmSiteLockboxID
    AND factChecks.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
    AND factChecks.DepositStatus >= @parmMinDepositStatus
ORDER BY 
    factChecks.ProcessingDateKey ASC, 
    factChecks.BatchID ASC, 
    factChecks.TxnSequence ASC, 
    factChecks.SequenceWithinTransaction ASC
END


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

