--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetHOAPMADetailData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetHOAPMADetailData') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetHOAPMADetailData
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetHOAPMADetailData] 
	@parmCustomerID int,
    @parmLockboxID int,
    @parmDepositDateKey int,
    @parmUserID int,
    @parmHOA_Number VARCHAR(80)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/09/2012
*
* Purpose: Query HOA Report Summary Detail Header
*
* Modification History
* 02/10/2012 CR 50229 JNE	Created
* 07/09/2012 CR 54029 JNE	Limit SuperUser to OLCustomerID assigned.
* 09/17/2012 CR 55575 JNE	Return BatchNumber instead of BatchID as BatchNumber
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY
	DECLARE @IsSuperUser bit

	SELECT	@IsSuperUser = SuperUser 
	FROM	OLTA.Users 
	WHERE	OLTA.Users.UserID = @parmUserID

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
	DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
	DROP TABLE #tmpHOAMainTemp
	
	--Create a temp table to hold dimlockbox info
	CREATE TABLE #tmpHOALockboxTemp
	(
		CustomerKey INT,
		LockboxKey INT,
		SiteCustomerID INT,
		Customer_Name VARCHAR(20),
		Lockbox_Number INT,
		Lockbox_Name VARCHAR(40)
	)
	
	--Create a temp table to hold sum information for the records 
	CREATE TABLE #tmpHOAMainTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		AccountNumber VARCHAR(80),
		BatchID INT,
		BatchNumber INT,
		TransactionID INT
	)
	
	IF @IsSuperUser > 0
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.Users ON OLTA.Users.OLCustomerID = OLTA.OLLockboxes.OLCustomerID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.Users.UserID = @parmUserID
			
	END
	ELSE
	BEGIN		
		
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.OLLockboxUsers ON OLTA.OLLockboxUsers.OLLockboxID = OLTA.OLLockboxes.OLLockboxID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.OLLockboxUsers.UserID = @parmUserID
	END
	
	--Insert into Main Temp
	INSERT INTO #tmpHOAMainTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,AccountNumber, BatchID, BatchNumber, TransactionID)
	SELECT DISTINCT OLTA.factStubs.BankKey,
		OLTA.factStubs.CustomerKey,
		OLTA.factStubs.LockboxKey,
		OLTA.factStubs.ProcessingDateKey,
		OLTA.factStubs.DepositDateKey,
		OLTA.factStubs.AccountNumber,
		OLTA.factStubs.BatchID,
		OLTA.factStubs.BatchNumber,
		OLTA.factStubs.TransactionID
	FROM OLTA.factStubs
		INNER JOIN #tmpHOALockboxTemp ON #tmpHOALockboxTemp.CustomerKey = OLTA.factStubs.CustomerKey AND #tmpHOALockboxTemp.LockboxKey = OLTA.factStubs.LockboxKey
	WHERE OLTA.factStubs.DepositDateKey = @parmDepositDateKey
		AND OLTA.factStubs.AccountNumber = @parmHOA_Number	

		
	-- HOA PMA Detail data
	SELECT #tmpHOAMainTemp.BatchNumber,
		OLTA.factchecks.TxnSequence as TransactionNumber,
		'' as RemitterAccount,
		OLTA.factchecks.serial as CheckNumber,
		OLTA.factchecks.amount as CheckAmount,
		'' as StubAmount
	FROM #tmpHOAMainTemp
		INNER JOIN OLTA.factchecks ON OLTA.factchecks.BankKey = #tmpHOAMainTemp.BankKey
									AND OLTA.factchecks.CustomerKey = #tmpHOAMainTemp.CustomerKey
									AND OLTA.factchecks.LockboxKey = #tmpHOAMainTemp.LockboxKey
									AND OLTA.factchecks.ProcessingDateKey = #tmpHOAMainTemp.ProcessingDateKey
									AND OLTA.factchecks.DepositDateKey = #tmpHOAMainTemp.DepositDateKey
									AND OLTA.factchecks.BatchID = #tmpHOAMainTemp.BatchID
									AND OLTA.factchecks.TransactionID = #tmpHOAMainTemp.TransactionID
	UNION
	SELECT #tmpHOAMainTemp.BatchNumber,
		OLTA.factstubs.TxnSequence as TransactionNumber,
		OLTA.factstubs.accountnumber as RemitterAccount,
		'' as CheckNumber,
		'' as CheckAmount,
		OLTA.factstubs.amount as StubAmount
	FROM #tmpHOAMainTemp
		INNER JOIN OLTA.factstubs ON OLTA.factstubs.BankKey = #tmpHOAMainTemp.BankKey
									AND OLTA.factstubs.CustomerKey = #tmpHOAMainTemp.CustomerKey
									AND OLTA.factstubs.LockboxKey = #tmpHOAMainTemp.LockboxKey
									AND OLTA.factstubs.ProcessingDateKey = #tmpHOAMainTemp.ProcessingDateKey
									AND OLTA.factstubs.DepositDateKey = #tmpHOAMainTemp.DepositDateKey
									AND OLTA.factstubs.AccountNumber = #tmpHOAMainTemp.AccountNumber
									AND OLTA.factstubs.BatchID = #tmpHOAMainTemp.BatchID
									AND OLTA.factstubs.TransactionID = #tmpHOAMainTemp.TransactionID

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp

END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp

	EXEC OLTA.usp_wfsRethrowException
END CATCH
								
