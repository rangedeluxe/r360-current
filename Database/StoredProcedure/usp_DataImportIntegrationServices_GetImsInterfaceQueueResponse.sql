--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse') IS NOT NULL
       DROP PROCEDURE OLTA.usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse] 
(
	@parmClientProcessCode VARCHAR(40) = 'Unassigned'
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/18/2012
*
* Purpose: 
*
* Modification History
* 10/18/2012 WI 70639 WJS	Created
*
******************************************************************************/
SET NOCOUNT ON 



BEGIN TRY
	SELECT SiteBankID, SiteLockboxID, ProcessingDateKey,
		BatchID, NumArchiveImages, NumOfUnArchiveImages
		FROM
		OLTA.IMSInterfaceQueue
		WHERE QueueType = 0 
			AND QueueStatus IN (99)
			AND ClientProcessCode = @parmClientProcessCode;
	
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH