--WFSScriptProcessorStoredProcedureName usp_BrandingSchemes_GetByBrandingID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_BrandingSchemes_GetByBrandingID') IS NOT NULL
       DROP PROCEDURE OLTA.usp_BrandingSchemes_GetByBrandingID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_BrandingSchemes_GetByBrandingID]
 @parmBrandingSchemeID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JCS
* Date: 01/18/2012
*
* Purpose:	Return BrandingSchemes record with specified BrandingSchemeID
*
* Modification History
* 01/18/2012 CR 49639 JCS   Created
* 02/29/2012 CR 51104 JCS   Added ExternalID1, ExternalID2 fields to returnset.
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY

	SELECT	BrandingSchemeID,
			BrandingSchemeName,
			BrandingSchemeDescription,
			BrandingSchemeFolder,
			IsActive,
			ExternalID1,
			ExternalID2,
			CreationDate,
			CreatedBy,
			ModificationDate,
			ModifiedBy
	FROM OLTA.BrandingSchemes
	WHERE BrandingSchemeID = @parmBrandingSchemeID
	ORDER BY BrandingSchemeName

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
