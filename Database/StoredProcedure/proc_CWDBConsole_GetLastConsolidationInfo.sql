--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.proc_CWDBConsole_GetLastConsolidationInfo') IS NOT NULL
       DROP PROCEDURE dbo.proc_CWDBConsole_GetLastConsolidationInfo
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[proc_CWDBConsole_GetLastConsolidationInfo]
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/23/2010
*
* Purpose: 
*
* Modification History
* 04/23/2010 CR 29458 JMC	Initial Version.
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

    SELECT MAX(CWDBBatchStatus) AS MaxCWDBBatchStatus FROM BatchTrace

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException
END CATCH
