--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetBatchBYGlobalBatchIDOLCustomerIDOLLockboxID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetBatchBYGlobalBatchIDOLCustomerIDOLLockboxID') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetBatchBYGlobalBatchIDOLCustomerIDOLLockboxID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetBatchBYGlobalBatchIDOLCustomerIDOLLockboxID] 
       @parmOLCustomerID uniqueidentifier, 
       @parmGlobalBatchID int,
       @parmOLLockboxID uniqueidentifier
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by OLCustomerID, GlobalBatchID, and 
*          OLLockboxID
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
******************************************************************************/

BEGIN TRY

	SELECT	OLTA.OLLockboxes.OLLockboxID,
			OLTA.dimLockboxes.SiteBankID	AS BankID,
			OLTA.dimLockboxes.SiteLockboxID	AS LockboxID,
			OLTA.dimDates.CalendarDate		AS DepositDate
	FROM	OLTA.factBatchSummary
			INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey 
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.dimDates ON OLTA.factBatchSummary.DepositDateKey = OLTA.dimDates.DateKey
	WHERE	 OLTA.factBatchSummary.DepositStatus >= 850    
			AND OLTA.factBatchSummary.GlobalBatchID = @parmGlobalBatchID
			AND OLTA.OLLockboxes.OLCustomerID = @parmOLCustomerID
			AND OLTA.OLLockboxes.OLLockboxID= @parmOLLockboxID

END TRY
BEGIN CATCH
       EXEC OLTA.usp_WfsRethrowException
END CATCH

