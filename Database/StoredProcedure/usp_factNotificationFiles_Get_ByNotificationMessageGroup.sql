--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotificationFiles_Get_ByNotificationMessageGroup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factNotificationFiles_Get_ByNotificationMessageGroup') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factNotificationFiles_Get_ByNotificationMessageGroup
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [olta].[usp_factNotificationFiles_Get_ByNotificationMessageGroup]
	@parmNotificationMessageGroup int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: 
*
* Modification History
* 09/04/2012 CR 55380 JMC	Created
******************************************************************************/

SET NOCOUNT ON

BEGIN TRY

SELECT 
	OLTA.factNotificationFiles.factNotificationFileID,
	OLTA.factNotificationFiles.NotificationMessageGroup,
	OLTA.factNotificationFiles.BankKey,
	OLTA.factNotificationFiles.CustomerKey,
	OLTA.factNotificationFiles.LockboxKey,
	OLTA.factNotificationFiles.NotificationDateKey,
	OLTA.factNotificationFiles.NotificationSourceKey,
	OLTA.factNotificationFiles.CreationDate,
	OLTA.factNotificationFiles.ModificationDate,
	OLTA.factNotificationFiles.CreatedBy,
	OLTA.factNotificationFiles.ModifiedBy,
	OLTA.factNotificationFiles.FileIdentifier,
	OLTA.factNotificationFiles.UserFileName,
	OLTA.factNotificationFiles.FileExtension,
	OLTA.dimNotificationFileTypes.FileTypeDescription,
	CAST(OLTA.factNotificationFiles.NotificationDateKey AS VARCHAR(8)) + '\' +
	CAST(OLTA.dimCustomers.SiteBankID AS VARCHAR(16)) + '\' +
	CASE WHEN OLTA.dimLockboxes.SiteLockboxID IS NULL THEN CAST(OLTA.dimCustomers.SiteCustomerID AS VARCHAR(16)) ELSE 'Lbx' + '\' END +
	CASE WHEN OLTA.dimLockboxes.SiteLockboxID IS NULL THEN '' ELSE CAST(OLTA.dimLockboxes.SiteLockboxID AS VARCHAR(16)) + '\' END +
	CAST(OLTA.factNotificationFiles.FileIdentifier AS VARCHAR(36)) + 
	CASE WHEN LEN(OLTA.factNotificationFiles.FileExtension) > 0 AND SUBSTRING(OLTA.factNotificationFiles.FileExtension, 1, 1) <> '.' THEN '.' ELSE '' END +
	OLTA.factNotificationFiles.FileExtension AS FilePath
FROM
	OLTA.factNotificationFiles
		INNER JOIN OLTA.dimCustomers ON
			OLTA.factNotificationFiles.CustomerKey = OLTA.dimCustomers.CustomerKey
		LEFT OUTER JOIN OLTA.dimLockboxes ON
			OLTA.factNotificationFiles.LockboxKey = OLTA.dimLockboxes.LockboxKey
		INNER JOIN OLTA.dimNotificationFileTypes ON
			OLTA.factNotificationFiles.NotificationFileTypeKey = OLTA.dimNotificationFileTypes.NotificationFileTypeKey
WHERE
	OLTA.factNotificationFiles.NotificationMessageGroup = @parmNotificationMessageGroup

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH