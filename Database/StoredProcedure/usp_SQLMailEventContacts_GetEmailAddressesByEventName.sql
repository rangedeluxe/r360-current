--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_SQLMailEventContacts_GetEmailAddressesByEventName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_SQLMailEventContacts_GetEmailAddressesByEventName') IS NOT NULL
       DROP PROCEDURE dbo.usp_SQLMailEventContacts_GetEmailAddressesByEventName
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[usp_SQLMailEventContacts_GetEmailAddressesByEventName]
	@parmEventName varchar(max),
	@parmEmailAddresses varchar(max) OUT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/14/2010
*
* Purpose: Retrieve the service broker errors for a given date range.
*
* Modification History
* 02/14/2010 CR 29191 JPB	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SET @parmEmailAddresses = ''
	SELECT	@parmEmailAddresses = @parmEmailAddresses + RTRIM(email) + ';'
	FROM	dbo.Contacts
			INNER JOIN dbo.Alerts ON dbo.Alerts.ContactID = dbo.Contacts.ContactID
			INNER JOIN dbo.EventRules ON dbo.EventRules.EventRuleID = dbo.Alerts.EventRuleID
			INNER JOIN dbo.[Events] ON dbo.[Events].EventID = dbo.EventRules.EventID
	WHERE	EventName = @parmEventName
			AND IsActive = 1
	--Remove the last ; from the list
	SELECT @parmEmailAddresses = LEFT(@parmEmailAddresses, LEN(@parmEmailAddresses)-1)

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException
END CATCH