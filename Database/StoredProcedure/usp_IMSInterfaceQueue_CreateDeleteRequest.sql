--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_CreateDeleteRequest
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSInterfaceQueue_CreateDeleteRequest') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSInterfaceQueue_CreateDeleteRequest
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_IMSInterfaceQueue_CreateDeleteRequest]
		@parmSiteBankID			int,
		@parmSiteLockboxID		int,
		@parmProcessingDateKey	int,
		@parmBatchID			int,
		@parmXmlData			xml
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/16/2011
*
* Purpose: Insert a message into the MSMQ queue to alert the IMS system that
*	documents need to be deleted.
*
* Modification History
* 02/16/2011 CR 32838 JMC	Created
******************************************************************************/
SET ARITHABORT ON

BEGIN TRY

	INSERT INTO OLTA.IMSInterfaceQueue 
	(
		QueueType,
		QueueData,
		QueueDataStatus,
		QueueImageStatus,
		QueueStatus,
		SiteBankID,
		SiteLockboxID,
		BatchID,
		ProcessingDateKey
	) 
	SELECT  
		0,
		@parmXmlData,
		1,
		1,
		10,
		@parmSiteBankID,
		@parmSiteLockboxID,
		@parmBatchID,
		@parmProcessingDateKey
	
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
