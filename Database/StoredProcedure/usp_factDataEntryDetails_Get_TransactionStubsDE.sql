--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_TransactionStubsDE
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factDataEntryDetails_Get_TransactionStubsDE') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factDataEntryDetails_Get_TransactionStubsDE
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_factDataEntryDetails_Get_TransactionStubsDE 
(
    @parmSiteBankID        INT,
    @parmSiteLockboxID     INT,
    @parmDepositDate       DATETIME,
    @parmBatchID           INT,
    @parmTransactionID     INT,
    @parmBatchSequence     INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Locate transaction stubs
*
* Modification History
* 01/24/2013 WI 86358 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DepositDateKey INT;

BEGIN TRY
    SET @DepositDateKey = CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);

    SELECT
        OLTA.dimDataEntryColumns.TableName,
        OLTA.dimDataEntryColumns.FldName,
        OLTA.dimDataEntryColumns.DataType               AS FldDataTypeEnum,
        OLTA.factDataEntryDetails.DataEntryValue,
        COALESCE(OLTA.dimDataEntryColumns.DisplayName, OLTA.dimDataEntryColumns.FldName) AS FldTitle
    FROM OLTA.dimDataEntryColumns
        INNER JOIN OLTA.factDataEntryDetails ON
            OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
        INNER JOIN OLTA.dimLockboxes ON
            OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
    WHERE   
        OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
        AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
        AND OLTA.factDataEntryDetails.DepositDateKey = @DepositDateKey
        AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
        AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
        AND OLTA.factDataEntryDetails.BatchSequence = @parmBatchSequence
        AND OLTA.dimDataEntryColumns.TableType IN (2, 3)
    ORDER BY 
        OLTA.factDataEntryDetails.TransactionID,
        OLTA.factDataEntryDetails.BatchSequence,
        OLTA.dimDataEntryColumns.ScreenOrder ASC;

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

