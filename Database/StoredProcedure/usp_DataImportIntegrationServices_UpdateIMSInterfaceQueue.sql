--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue') IS NOT NULL
       DROP PROCEDURE OLTA.usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue 
		@parmBankID				int,
		@parmLockboxID				int, 
		@parmProcessingDate			datetime,
		@parmBatchID					int,
		@parmClientProcessCode    VARCHAR(40),
		@parmArchivedImages smallint,
		@parmUnArchivedImages smallint
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 12/11/2012
*
* Purpose: Update the archived and unArchived image
*
* Modification History
* 12/11/2012 WI 70640 WJS	Created

******************************************************************************/

DECLARE
		@processingDateKey			int
BEGIN TRY

	SELECT	@processingDateKey= OLTA.dimDates.DateKey
		FROM	OLTA.dimDates
		WHERE	OLTA.dimDates.CalendarDate = CAST(CONVERT(VARCHAR,@parmProcessingDate,101) AS DATETIME)

	UPDATE OLTA.IMSInterfaceQueue 
	SET 
		NumArchiveImages = @parmArchivedImages,
		NumOfUnArchiveImages = @parmUnArchivedImages,
		ModificationDate = GetDate(),
		ModifiedBy = suser_sname()
	WHERE 
		SiteBankID			= @parmBankID AND 
		SiteLockboxID		= @parmLockboxID AND
	    ProcessingDateKey	= @processingDateKey AND
		BatchID				= @parmBatchID AND
		ClientProcessCode	= @parmClientProcessCode

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
