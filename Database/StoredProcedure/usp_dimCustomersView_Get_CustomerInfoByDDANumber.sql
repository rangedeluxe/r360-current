--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_dimCustomersView_Get_CustomerInfoByDDANumber
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_dimCustomersView_Get_CustomerInfoByDDANumber') IS NOT NULL
       DROP PROCEDURE OLTA.usp_dimCustomersView_Get_CustomerInfoByDDANumber
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_dimCustomersView_Get_CustomerInfoByDDANumber 
(
    @parmDDANumber VARCHAR
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 12/12/2012
*
* Purpose: Return Customer information using DDA number for search
*
* Modification History
* 12/12/2012 WI 00000 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
WITH CTE_Cust_Batch_Types AS
(
    SELECT 
        bt1.BankID, 
        bt1.CustomerID, 
        bt1.DDA
    FROM 
        dbo.BatchTypes       AS bt1
    WHERE 
        bt1.LockboxID = 0
)
,
CTE_lb_search as
(
    SELECT 
        bt.BankID, 
        bt.CustomerID, 
        bt.LockboxID, 
        bt.DDA
    FROM 
        dbo.BatchTypes              AS bt
    Where 
        bt.LockboxID <> 0
    
    UNION ALL
    
    SELECT 
        lb.SiteBankID, 
        lb.SiteCustomerID, 
        lb.SiteLockboxID, 
        CTE_Cust_Batch_Types.DDA
    FROM 
        OLTA.dimLockboxesView AS lb 
        INNER JOIN CTE_Cust_Batch_Types ON
            lb.SiteBankID = CTE_Cust_Batch_Types.BankID
            AND lb.SiteCustomerID = CTE_Cust_Batch_Types.CustomerID

    UNION ALL

    SELECT 
        lb.SiteBankID, 
        lb.SiteCustomerID, 
        lb.SiteLockboxID, 
        lb.DDA
    FROM OLTA.dimLockboxesView AS lb
)
    SELECT DISTINCT
         OLTA.dimLockboxesView.SiteLockboxID        AS 'Lockbox Number'
        ,RTrim(OLTA.dimLockboxesView.LongName)      AS 'Lockbox Name'
        ,RTrim(OLTA.dimLockboxesView.POBox)         AS 'P.O. Box'
        ,OLTA.dimLockboxesView.SiteCustomerID       AS 'Customer Number'
        ,RTrim(OLTA.dimCustomersView.Name)          AS 'Customer Name'
        ,OLTA.dimLockboxesView.SiteBankID           AS 'Bank Number'
        ,RTrim(OLTA.dimBanksView.BankName)          AS 'Bank Name'
    FROM 
        OLTA.dimLockboxesView
        INNER JOIN OLTA.dimCustomersView ON 
            OLTA.dimLockboxesView.SiteBankID = OLTA.dimCustomersView.SiteBankID 
            AND OLTA.dimLockboxesView.SiteCustomerID = OLTA.dimCustomersView.SiteCustomerID
        INNER JOIN OLTA.dimBanksView ON 
            OLTA.dimLockboxesView.SiteBankID = OLTA.dimBanksView.SiteBankID
        INNER JOIN CTE_lb_search ON
            OLTA.dimLockboxesView.SiteBankID = CTE_lb_search.BankID
            AND OLTA.dimLockboxesView.SiteCustomerID = CTE_lb_search.CustomerID
            AND OLTA.dimLockboxesView.SiteLockboxID = CTE_lb_search.LockboxID
            AND CTE_lb_search.DDA LIKE '%' + @parmDDANumber + '%'
    ORDER BY
            OLTA.dimLockboxesView.SiteLockboxID
        ,RTrim(OLTA.dimLockboxesView.LongName)
        ,RTrim(OLTA.dimLockboxesView.POBox)
        ,OLTA.dimLockboxesView.SiteCustomerID
        ,RTrim(OLTA.dimCustomersView.Name)
        ,OLTA.dimLockboxesView.SiteBankID
        ,RTrim(OLTA.dimBanksView.BankName) ASC;

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

