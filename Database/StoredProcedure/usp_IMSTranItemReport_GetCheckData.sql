--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSTranItemReport_GetCheckData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSTranItemReport_GetCheckData') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSTranItemReport_GetCheckData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSTranItemReport_GetCheckData
(
	@parmBankID				int,
	@parmLockboxID			int, 
	@parmProcessingDateKey	int,
	@parmDepositDateKey		int,
	@parmBatchID			int,
	@parmTransactionID      int,
	@parmBatchSourceKey		int
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/01/2011
*
* Purpose: Retrieve the data for document data for IMS report
*
* Modification History
* 04/01/2011 CR 33626 WJS	Created
* 10/10/2011 CR 47420 WJS Modified to pull processing Date from factBatchtable
* 02/06/2012 CR 49996 WJS   Modified to get sourceProcessingDateKey
* 02/29/2012 CR 49996 WJS  Add ProcessingDateKey back
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON


BEGIN TRY



	SELECT	OLTA.dimLockboxes.SiteBankID,
			OLTA.dimLockboxes.SiteLockboxID,
			OLTA.dimLockboxes.OnlineColorMode,
			OLTA.factChecks.ProcessingDateKey,
			OLTA.factChecks.SourceProcessingDateKey,
			OLTA.factChecks.DepositDateKey,
			OLTA.factChecks.BatchID,
			OLTA.factChecks.TransactionID,
			OLTA.factChecks.TxnSequence,
			OLTA.factChecks.BatchSequence,
			'C' AS FileDescriptor,
			OLTA.factChecks.CheckSequence,
			OLTA.dimRemitters.RoutingNumber,
			OLTA.dimRemitters.Account,
			OLTA.factChecks.Serial,
			OLTA.factChecks.Amount,
			OLTA.dimRemitters.RemitterName,
			OLTA.factChecks.TransactionCode
     FROM 	OLTA.factChecks
			INNER JOIN OLTA.dimLockboxes ON OLTA.factChecks.LockboxKey = OLTA.dimLockboxes.LockboxKey
			INNER JOIN OLTA.dimRemitters ON OLTA.factChecks.RemitterKey = OLTA.dimRemitters.RemitterKey
     WHERE 	OLTA.factChecks.DepositDateKey = @parmDepositDateKey
			AND OLTA.dimLockboxes.SiteBankID = @parmBankID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.factChecks.ProcessingDateKey = @parmProcessingDateKey
			AND OLTA.factChecks.BatchID = @parmBatchID
			AND OLTA.factChecks.TransactionID = @parmTransactionID
			AND OLTA.factChecks.BatchSourceKey = @parmBatchSourceKey
		
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @parmErrorMessage    NVARCHAR(4000),
				@parmErrorProcedure	 NVARCHAR(200),
				@parmErrorSeverity   INT,
				@parmErrorState      INT,
				@parmErrorLine		 INT
		
		SELECT	@parmErrorMessage = ERROR_MESSAGE(),
				@parmErrorSeverity = ERROR_SEVERITY(),
				@parmErrorState = ERROR_STATE(),
				@parmErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@parmErrorLine = ERROR_LINE()

		SET @parmErrorMessage = @parmErrorProcedure + ' (Line: %d)-> ' + @parmErrorMessage

		RAISERROR(@parmErrorMessage,@parmErrorSeverity,@parmErrorState,@parmErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
