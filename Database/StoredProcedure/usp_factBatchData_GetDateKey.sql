--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchData_GetDateKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factBatchData_GetDateKey') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factBatchData_GetDateKey
GO

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/10/2011
*
* Purpose: Get fact batch data processing date
*
* Modification History
* 10/28/2011 CR 47418 WJS Created
* 02/08/2012 CR 51289 WJS remove this stored proc
******************************************************************************/
