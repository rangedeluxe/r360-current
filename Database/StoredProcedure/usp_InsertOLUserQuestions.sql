--WFSScriptProcessorSchema RecHubUser
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InsertOLUserQuestions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubUser.usp_InsertOLUserQuestions') IS NOT NULL
	DROP PROCEDURE RecHubUser.usp_InsertOLUserQuestions
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubUser.usp_InsertOLUserQuestions
(
	@parmUserID			INT,
	@parmSecretQuestion	VARCHAR(255),
	@parmSecretAnswer	VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 9/27/2011
*
* Purpose: Set user question
*
* Modification History
* 09/27/2011 CR 47025 WJS Created
* 02/25/2013 WI 88984 CRG Change Schema for RecHubUser.usp_InsertOLUserQuestions
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
	INSERT INTO RecHubUser.OLUserQuestions
		(
			RecHubUser.OLUserQuestions.UserID,
			RecHubUser.OLUserQuestions.QuestionText,
			RecHubUser.OLUserQuestions.AnswerText
		)
	VALUES
		(
			@parmUserID,
			@parmSecretQuestion,
			@parmSecretAnswer
		)
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

                   
