--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName proc_LockboxRemitterSelect
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.proc_LockboxRemitterSelect') IS NOT NULL
       DROP PROCEDURE OLTA.proc_LockboxRemitterSelect
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE [OLTA].[proc_LockboxRemitterSelect] (@BankID int = NULL, 
                                                      @CustomerID int = NULL, 
                                                      @LockboxID int = NULL, 
                                                      @RoutingNumber varchar(30) = NULL,
                                                      @Account varchar(30) = NULL,
                                                      @LockboxRemitterID uniqueidentifier = NULL
)
-- with encryption
AS

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/24/2009
*
* Purpose: Select Remitter
*		   
*
* Modification History
* 04/24/2009 CR 25817 JNE	Created
******************************************************************************/

DECLARE @TimesRemitterUsed int, 
        @DateLastUsed datetime,
        @ModificationDate datetime, 
        @columns_list nvarchar(1000), 
        @sql_string nvarchar(2000),
        @where_clause nvarchar(500),
        @or_clause nvarchar(500),
        @from_clause nvarchar(250),
        @msg_string varchar(300),
        @parameters nvarchar(300),
        @iResults int,
        @iResults2 int,
        @cEmptyString char(1),
        @iSearchArg varchar(30),
        @lbr_id uniqueidentifier

SET NOCOUNT ON

SET @DateLastUsed = getdate()

BEGIN TRY
	IF @LockboxRemitterID is not null
	BEGIN
	   --retrieve by LockboxRemitter
	   IF not exists(SELECT 1 FROM LockboxRemitter WHERE LockboxRemitter.LockboxRemitterID = @LockboxRemitterID)
	   BEGIN
		  set @msg_string = 'Could not find LockboxRemitter table entry for the input LockboxRemitterID.'
		  RAISERROR(@msg_string,16,1)
	   END
	   SET @iSearchArg = 1
	END
	ELSE
	IF (@BankID is not null and @CustomerID is not null and @LockboxID is not null and @RoutingNumber is not null and @Account is not null)
	BEGIN
	   --retrieve one existing LockboxRemitter by Bank, Customer and Lockbox; if no LockboxRemitter, then get one existing GlobalRemitter
	   SET @iSearchArg = 2
	END
	ELSE 
	BEGIN
	   SET @msg_string = 'Please provide a valid lookup argument to retrieve LockboxRemitter: LockboxRemitterID or BankID, CustomerID, LockboxID, RoutingNumber and Account.'
	   RAISERROR(@msg_string, 16, 1)
	END
	IF @iSearchArg = 1
	BEGIN
	   SET @columns_list = 'a.LockboxRemitterID, a.RemitterName as LockboxRemitterName, a.BankID, a.CustomerID, a.LockboxID, b.GlobalRemitterID, b.RemitterName GlobalRemitterName, b.RoutingNumber, b.Account'
	   SET @from_clause  = 'from LockboxRemitter as a inner join GlobalRemitter as b on a.GlobalRemitterID = b.GlobalRemitterID'
	   Set @where_clause = 'where a.LockboxRemitterID = @lbr_id'
	   GOTO RETRIEVE_RECORDS
	END
	ELSE
	BEGIN
	   SET @columns_list = 'a.LockboxRemitterID, a.RemitterName as LockboxRemitterName, a.BankID, a.CustomerID, a.LockboxID, b.GlobalRemitterID, b.RemitterName as GlobalRemitterName, b.RoutingNumber, b.Account'
	   SET @from_clause  = 'from LockboxRemitter as a inner join GlobalRemitter as b on a.GlobalRemitterID = b.GlobalRemitterID'
	   SET @where_clause = 'where'
	END

	IF @BankID is not null
	BEGIN
	   SET @where_clause = @where_clause + char(32) + 'a.BankID = ' + convert(varchar, @BankID) + char(32) + 'and'
	END

	IF @CustomerID is not null
	BEGIN
	   SET @where_clause = @where_clause + char(32) + 'a.CustomerID = ' + convert(varchar, @CustomerID) + char(32) + 'and'
	END

	IF @LockboxID is not null
	BEGIN
	   SET @where_clause = @where_clause + char(32) + 'a.LockboxID = ' + convert(varchar, @LockboxID) + char(32) + 'and'
	END

	SET @RoutingNumber = rtrim(@RoutingNumber)
	IF (@RoutingNumber is not null or @RoutingNumber <> @cEmptyString)
	BEGIN
	   SET @where_clause = @where_clause + char(32) + 'b.RoutingNumber = ' + char(39) + @RoutingNumber + char(39) + char(32) + 'and'
	END

	SET @Account = rtrim(@Account)
	IF (@Account is not null or @Account <> @cEmptyString)
	BEGIN
	   SET @where_clause = @where_clause + char(32) + 'b.Account = ' + char(39) + @Account + char(39) + char(32) + 'and'
	END

	RETRIEVE_RECORDS:
	IF @iSearchArg = 2
	BEGIN
	   SET @where_clause = substring(@where_clause, 1, (len(@where_clause) - len(' and')))
	   SET @sql_string = 'select ' + @columns_list + char(32) + @from_clause + char(32) + @where_clause
	END
	ELSE BEGIN
	   SET @sql_string = 'select ' + @columns_list + char(32) + @from_clause + char(32) + @where_clause
	END

	IF @iSearchArg = 1
	BEGIN
	   SET @parameters = '@lbr_id uniqueidentifier'
	   EXEC @iResults = sp_executesql @sql_string, @parameters, @LockboxRemitterID
	END
	ELSE
	BEGIN
	   EXEC @iResults = sp_executesql @sql_string
	END

	SET @iResults2 = @@rowcount
END TRY
BEGIN CATCH
	SET @msg_string = 'Could not retrieve LockboxRemitter table data for '
	IF @BankID is not null
		SET @msg_string = @msg_string + 'Bank: ' + cast(@BankID as varchar) + char(32)
	IF @CustomerID is not null
		SEt @msg_string = @msg_string + 'CustomerID: ' + cast(@CustomerID as varchar) + char(32)
	IF @LockboxID is not null
		SET @msg_string = @msg_string + 'LockboxID: ' + cast(@LockboxID as varchar) + char(32)
	IF @RoutingNumber is not null
		SET @msg_string = @msg_string + 'RoutingNumber: ' + @RoutingNumber + char(32)
	IF @Account is not null
		SET @msg_string = @msg_string + 'Account: ' + @Account + char(32)

	EXEC OLTA.usp_WfsRethrowException  
	RETURN -1
END CATCH

BEGIN TRANSACTION LockboxRemitterSelect
BEGIN TRY
	set @iResults2 = @@rowcount

	IF @iSearchArg = 1 and @iResults2 > 0
	BEGIN
	    UPDATE LockboxRemitter
		SET TimesRemitterUsed = TimesRemitterUsed + 1, 
			  DateLastUsed = convert(varchar(10), @DateLastUsed, 101)
		FROM LockboxRemitter
		WHERE LockboxRemitter.LockboxRemitterID = @LockboxRemitterID
	END

	IF @iSearchArg = 2 and @iResults2 > 0
	BEGIN
	   SET @parameters = '@DateLastUsed datetime'
	   SET @sql_string = 'UPDATE LockboxRemitter' + char(32)
	   SET @sql_string = @sql_string + 'set TimesRemitterUsed = TimesRemitterUsed + 1, DateLastUsed = convert(varchar(10), @DateLastUsed, 101)' + char(32)
	   SET @sql_string = @sql_string + 'from LockboxRemitter as a join GlobalRemitter as b on a.GlobalRemitterID = b.GlobalRemitterID' + char(32)
	   SET @sql_string = @sql_string + @where_clause
	   EXEC sp_executesql @sql_string, @parameters, @DateLastUsed
	END
	COMMIT TRANSACTION LockboxRemitterSelect
	RETURN 0
END TRY
BEGIN CATCH
	IF @@TRANCOUNT>0
		ROLLBACK TRANSACTION LockboxRemitterSelect
	SET @msg_string = 'Could not update TimesRemitterUsed and DateLastUsed columns in LockboxRemitter.'
	EXEC OLTA.usp_WfsRethrowException 
	RETURN -1
END CATCH
