--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetBankCustomerLockboxes
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetBankCustomerLockboxes') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetBankCustomerLockboxes
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetBankCustomerLockboxes]
	@parmBankID int,
	@parmCustomerID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 09/23/2012
*
* Purpose: Query Lockboxes for given Bank and Customer
*
* Modification History
* 09/23/2012 CR 56028 JNE - Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT OLTA.dimLockboxesView.SiteBankID AS BankID, 
		OLTA.dimLockboxesView.SiteCustomerID AS CustomerID, 
		OLTA.dimLockboxesView.SiteLockboxID AS LockboxID, 
		OLTA.dimLockboxesView.ShortName, 
		OLTA.dimLockboxesView.LongName, 
		OLTA.dimLockboxesView.CutOff, 
		OLTA.dimLockboxesView.SiteCode, 
		0 AS Assigned,
		OLTA.dimLockboxesView.[FileGroup]
	FROM OLTA.dimLockboxesView 
	WHERE OLTA.dimLockboxesView.SiteBankID = @parmBankID
		AND OLTA.dimLockboxesView.SiteCustomerID = @parmCustomerID
	ORDER BY OLTA.dimLockboxesView.SiteBankID, 
		OLTA.dimLockboxesView.SiteCustomerID, 
		OLTA.dimLockboxesView.SiteLockboxID
		
END TRY
BEGIN CATCH
	   EXEC OLTA.usp_WfsRethrowException
END CATCH