--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetIMSInterfaceQueue_GetDelete
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('OLTA.usp_GetIMSInterfaceQueue_GetDelete') IS NOT NULL
	DROP PROCEDURE [OLTA].[usp_GetIMSInterfaceQueue_GetDelete]
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetIMSInterfaceQueue_GetDelete]
       @paramNumItemsToRetrive smallint = 5
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
** Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 09/23/2009
*
* Purpose: Get the data from the interface queue.
*
* Modification History
* 09/23/2009 CR 25817 WJS	Created
* 11/05/2009 CR 28111 WJS	Updated QueueDataStatus from 10 to 1.
* 11/12/2009 CR 28111 WJS	Added QueueStatus
* 12/16/2009 CR 28505 WJS	Added transaction and temp table. Also update status in stored proc
* 10/28/2010 CR 31341 WJS	Added support duplicate batch id
* 02/16/2011 CR 32857 JMC   Removed logic that was excluding existing rows inprocess
******************************************************************************/
SET NOCOUNT ON 
	
BEGIN TRY

	SELECT TOP (@paramNumItemsToRetrive)
		QueueID,
		QueueData,
		RetryCount
	FROM IMSInterfaceQueue
	WHERE 
		QueueType = 0
		AND QueueStatus = 10
	ORDER BY CreationDate ASC

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
