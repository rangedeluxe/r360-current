--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.proc_CWDBConsole_GetBatchList') IS NOT NULL
       DROP PROCEDURE dbo.proc_CWDBConsole_GetBatchList
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[proc_CWDBConsole_GetBatchList]
	@parmShowOnlyTransferredBatches bit,
	@parmShowOnlyNewBatches bit,
	@parmShowOnlyInternetCustomers bit,
	@parmProcessingDateFrom DateTime = NULL,
	@parmProcessingDateTo DateTime = NULL,
	@parmBankID int = NULL,
	@parmLockboxID int = NULL
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/23/2010
*
* Purpose: Flags reconsolidation of data, images, or both for specified 
*          Batches.
*
* Modification History
* 04/23/2010 CR 29457 JMC   Initial Version.
******************************************************************************/
SET NOCOUNT ON 
DECLARE @SQLCommand varchar(max)
DECLARE @SQLCondition varchar(max)
DECLARE @SQLWhere varchar(7)
DECLARE @OLCustomerSynch bit

BEGIN TRY
    SELECT @OLCustomerSynch = CASE WHEN OBJECT_ID('dbo.proc_CWDBConsole_GetBatchList') IS NULL THEN 0 ELSE 1 END
    
    SET @SQLCommand = '
    SELECT DISTINCT
    Batch.GlobalBatchID,
    Batch.LockboxID          AS Lockbox,
    Batch.LockboxID,
    Customer.CustomerID,
    Batch.BankID             AS Bank,
    Batch.BankID,
    Lockbox.LongName         AS Name,
    Batch.BatchID            AS Batch,
    Bank.BankName            AS Bank,
    Customer.Name            AS Customer,
    Batch.ProcessingDate,
    Batch.DepositStatus,
    Batch.ScanStatus,
    Batch.DEStatus,
    BatchTrace.CWDBBatchStatus,
    BatchTrace.CWDBImageStatus,
    ISNull(BatchTrace.CWDBConsolidationPriority, 5)     AS CWDBConsolidationPriority
    FROM Batch
    INNER JOIN BatchTrace ON Batch.GlobalBatchID=BatchTrace.GlobalBatchID
    INNER JOIN Bank ON Batch.BankID=Bank.BankID
    INNER JOIN Lockbox ON Batch.BankID=Lockbox.BankID AND Batch.LockboxID=Lockbox.LockboxID
    INNER JOIN Customer ON Lockbox.BankID=Customer.BankID AND Lockbox.CustomerID=Customer.CustomerID'
    
    SET @SQLCondition = ''
    SET @SQLWhere = ' WHERE '
    
    IF @OLCustomerSynch > 0
    BEGIN
        SET @SQLCommand = @SQLCommand + '
        LEFT OUTER JOIN OLLockboxes ON Batch.BankID=OLLockboxes.BankID AND Batch.LockboxID=OLLockboxes.LockboxID
        LEFT OUTER JOIN OLCustomers ON OLLockboxes.OLCustomerID=OLCustomers.OLCustomerID'
                        
        IF @parmShowOnlyInternetCustomers > 0
        BEGIN
            SET @SQLCondition = ' WHERE OLCustomers.OLCustomerID IS NOT NULL'
            SET @SQLWhere = ' AND '
        END
    END
    
    IF @parmProcessingDateFrom IS NOT NULL
    BEGIN
        SET @SQLCondition = @SQLCondition + @SQLWhere +
        'Batch.ProcessingDate >= ''' + CONVERT(varchar,(@parmProcessingDateFrom),112) + ''
        SET @SQLWhere = ' AND '
    END
    
    IF @parmProcessingDateTo IS NOT NULL
    BEGIN
        SET @SQLCondition = @SQLCondition + @SQLWhere +
        'Batch.ProcessingDate <= ''' + CONVERT(varchar,(@parmProcessingDateTo),112) + ''
        SET @SQLWhere = ' AND '
    END
    
    IF @parmBankID IS NOT NULL
    BEGIN
        SET @SQLCondition = @SQLCondition + @SQLWhere +
        'Batch.BankID = ' + CONVERT(varchar,@parmBankID)
        SET @SQLWhere = ' AND '
    END
    
    IF @parmLockboxID IS NOT NULL
    BEGIN
        SET @SQLCondition = @SQLCondition + @SQLWhere +
        'Batch.LockboxID = ' + CONVERT(varchar,@parmLockboxID)
        SET @SQLWhere = ' AND '
    END
    
    IF @parmShowOnlyTransferredBatches > 0
    BEGIN
    -- show only transferred batch data
        SET @SQLCondition = @SQLCondition + @SQLWhere + 
        'NOT CWDBBatchStatus IS NULL and NOT CWDBImageStatus IS NULL'
        SET @SQLWhere = ' AND '
    END
    ELSE 
        IF @parmShowOnlyNewBatches > 0
        BEGIN
        -- restrict to untransferred batches
        SET @SQLCondition = @SQLCondition + @SQLWhere + 
        '((CWDBBatchStatus IS NULL or DATEDIFF(second,CWDBBatchStatus,Batch.ModificationDate) > 0)
        OR (CWDBImageStatus IS NULL or DATEDIFF(second,CWDBImageStatus,Batch.ModificationDate) > 0))'
        END
    
    EXEC(@SQLCommand + @SQLCondition)

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException
END CATCH
