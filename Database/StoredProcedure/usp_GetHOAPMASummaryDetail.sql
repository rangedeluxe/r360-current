--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetHOAPMASummaryDetail
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetHOAPMASummaryDetail') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetHOAPMASummaryDetail
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetHOAPMASummaryDetail] 
	@parmCustomerID int,
	@parmLockboxID int,
	@parmUserID int,
	@parmDepositDateKey int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/06/2012
*
* Purpose: Query HOA Report Summary Detail Data
*
* Modification History
* 02/06/2012 CR 50232 JNE	Created
* 04/27/2012 CR 52382 JNE	Added account number & BatchSequence to link the final temp tables
* 07/09/2012 CR 54035 JNE   	Added Userid Parameter to limit results to the user is assigned to.
* 				Added Users table for super user to limit it to the assigned OLcustomerid.
* 08/30/2012 CR 55116 JNE	Removed BatchSequence when summing totals & attaching hoa_name/ddano.
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY
	DECLARE @IsSuperUser bit
	
	SELECT	@IsSuperUser = SuperUser 
	FROM	OLTA.Users 
	WHERE	OLTA.Users.UserID = @parmUserID

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOATemp')) 
		DROP TABLE #tmpHOATemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOASumTemp')) 
		DROP TABLE #tmpHOASumTemp

	--Create a temp table to hold dimlockbox info
	CREATE TABLE #tmpHOALockboxTemp
	(
		CustomerKey INT,
		LockboxKey INT,
		SiteCustomerID INT,
		Customer_Name VARCHAR(20),
		Lockbox_Number INT,
		Lockbox_Name VARCHAR(40)
	)
	
	--Create a temp table to hold data entry information for the records 
	CREATE TABLE #tmpHOATemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		BatchID INT,
		TransactionID INT,
		AccountNumber VARCHAR(80),
		BatchSequence INT,
		TableName VARCHAR(36),
		FldName VARCHAR(32),
		DataEntryValue VARCHAR(256)
	)

	--Create a temp table to hold sum information for the records 
	CREATE TABLE #tmpHOASumTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		AccountNumber VARCHAR(80),
		StubCount INT,
		StubAmount MONEY
	)
	
	IF @IsSuperUser > 0
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.Users ON OLTA.Users.OLCustomerID = OLTA.OLLockboxes.OLCustomerID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.Users.UserID = @parmUserID	
	END
	ELSE
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.OLLockboxUsers ON OLTA.OLLockboxUsers.OLLockboxID = OLTA.OLLockboxes.OLLockboxID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.OLLockboxUsers.UserID = @parmUserID	
	END
	
	INSERT INTO #tmpHOATemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey, BatchID, TransactionID, AccountNumber, BatchSequence, TableName,FldName,DataEntryValue)
	SELECT DISTINCT OLTA.factDataEntryDetails.BankKey,
		OLTA.factDataEntryDetails.CustomerKey,
		OLTA.factDataEntryDetails.LockboxKey,
		OLTA.factDataEntryDetails.ProcessingDateKey,
		OLTA.factDataEntryDetails.DepositDateKey,
		OLTA.factDataEntryDetails.BatchID,
		OLTA.factDataEntryDetails.TransactionID,
		"",
		OLTA.factDataEntryDetails.BatchSequence,
		OLTA.dimDataEntryColumns.TableName, 
		OLTA.dimDataEntryColumns.FldName, 
		OLTA.factDataEntryDetails.DataEntryValue 
	FROM OLTA.factDataEntryDetails
		INNER JOIN #tmpHOALockboxTemp ON OLTA.factDataEntryDetails.LockboxKey = #tmpHOALockboxTemp.LockboxKey AND OLTA.factDataEntryDetails.CustomerKey = #tmpHOALockboxTemp.CustomerKey
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.factDataEntryDetails.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
	WHERE OLTA.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
		AND OLTA.dimDataEntryColumns.TableName = 'StubsDataEntry'
		AND (OLTA.dimDataEntryColumns.FldName = 'hoa_name' OR OLTA.dimDataEntryColumns.FldName = 'hoa_ddano')
		
	UPDATE #tmpHOATemp set #tmpHOATemp.AccountNumber = factStubs.AccountNumber 
	FROM OLTA.factStubs
	WHERE #tmpHOATemp.BankKey = factStubs.BankKey
		AND #tmpHOATemp.CustomerKey = factStubs.CustomerKey
		AND #tmpHOATemp.LockboxKey = factStubs.LockboxKey
		AND #tmpHOATemp.ProcessingDateKey = factStubs.ProcessingDateKey
		AND #tmpHOATemp.DepositDateKey = factStubs.DepositDateKey
		AND #tmpHOATemp.BatchID = factStubs.BatchID
		AND #tmpHOATemp.TransactionID = factStubs.TransactionID
		


	INSERT INTO #tmpHOASumTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,AccountNumber, StubCount,StubAmount)
	SELECT OLTA.factStubs.BankKey,
		OLTA.factStubs.CustomerKey,
		OLTA.factStubs.LockboxKey,
		OLTA.factStubs.ProcessingDateKey,
		OLTA.factStubs.DepositDateKey,
		OLTA.factStubs.AccountNumber,
		COUNT(*) as StubCount, 
		SUM(OLTA.factStubs.Amount) as StubAmount
	FROM OLTA.factStubs
		INNER JOIN #tmpHOALockboxTemp ON OLTA.factStubs.LockboxKey = #tmpHOALockboxTemp.LockboxKey AND OLTA.factStubs.CustomerKey = #tmpHOALockboxTemp.CustomerKey
	WHERE OLTA.factStubs.DepositDateKey = @parmDepositDateKey
	GROUP BY OLTA.factStubs.BankKey,
		OLTA.factStubs.CustomerKey,
		OLTA.factStubs.LockboxKey,
		OLTA.factStubs.ProcessingDateKey,
		OLTA.factStubs.DepositDateKey,
		OLTA.factStubs.AccountNumber
		

	SELECT #tmpHOASumTemp.AccountNumber,
		HOA_NAME.DataEntryValue AS HOA_NAME,
		HOA_DDANO.DataEntryValue AS HOA_DDANO,
		#tmpHOASumTemp.StubCount, 
		#tmpHOASumTemp.StubAmount
	FROM #tmpHOASumTemp
		LEFT JOIN #tmpHOATemp AS HOA_NAME
			ON HOA_NAME.BankKey = #tmpHOASumTemp.BankKey
				AND HOA_NAME.CustomerKey = #tmpHOASumTemp.CustomerKey
				AND HOA_NAME.LockboxKey = #tmpHOASumTemp.LockboxKey
				AND HOA_NAME.ProcessingDateKey = #tmpHOASumTemp.ProcessingDateKey
				AND HOA_NAME.DepositDateKey = #tmpHOASumTemp.DepositDateKey
				AND HOA_NAME.AccountNumber = #tmpHOASumTemp.AccountNumber
				AND HOA_NAME.FldName = 'hoa_name'
		LEFT JOIN #tmpHOATemp AS HOA_DDANO
			ON HOA_DDANO.BankKey = #tmpHOASumTemp.BankKey
				AND HOA_DDANO.CustomerKey = #tmpHOASumTemp.CustomerKey
				AND HOA_DDANO.LockboxKey = #tmpHOASumTemp.LockboxKey
				AND HOA_DDANO.ProcessingDateKey = #tmpHOASumTemp.ProcessingDateKey
				AND HOA_DDANO.DepositDateKey = #tmpHOASumTemp.DepositDateKey
				AND HOA_DDANO.AccountNumber = #tmpHOASumTemp.AccountNumber
				AND HOA_DDANO.FldName = 'hoa_ddano'
	ORDER BY #tmpHOASumTemp.AccountNumber

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOATemp')) 
		DROP TABLE #tmpHOATemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOASumTemp')) 
		DROP TABLE #tmpHOASumTemp

END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOATemp')) 
		DROP TABLE #tmpHOATemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOASumTemp')) 
		DROP TABLE #tmpHOASumTemp

	EXEC OLTA.usp_wfsRethrowException
END CATCH

GO
