--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Payee_GetByCustomer
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Payee_GetByCustomer') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Payee_GetByCustomer
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Payee_GetByCustomer] 
	@parmSiteBankID int,
	@parmSiteCustomerID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28699 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT
	Payee.PayeeID, 
	Payee.IsValid, 
	IsNull(Payee.PayeeName, '')                   AS PayeeName, 
	Payee.BankID, 
	Payee.CustomerID, 
	Payee.LockboxID, 
	CAST(Payee.LockboxID AS VARCHAR) + ' - ' + dimLockboxes.LongName	AS LockboxDescription, 
	CASE 
		WHEN Payee.IsValid=0 THEN 'Invalid' 
		ELSE 'Valid' 
	END	AS PayeeStatus
FROM 
	Payee INNER JOIN dimLockboxes ON 
		Payee.BankID = dimLockboxes.SiteBankID 
		AND Payee.CustomerID = dimLockboxes.SiteCustomerID 
		AND Payee.LockboxID = dimLockboxes.SiteLockboxID
WHERE 
	Payee.BankID = @parmSiteBankID
	AND Payee.CustomerID = @parmSiteCustomerID
	AND Payee.LockboxID > 0
ORDER BY 
	Payee.LockboxID, 
	Payee.IsValid, 
	Payee.PayeeName


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

