--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factItemDataUpdate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factItemDataUpdate') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factItemDataUpdate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_factItemDataUpdate
(
	@parmSiteBankID				int,
	@parmSiteLockboxID			int, 
	@parmProcessingDateKey		int,
	@parmBatchID				int,
	@parmBatchSequence			int,
	@parmItemDataSetupFieldKey	int,
	@parmDataValue				varchar(256),
	@parmDataValueDateTime		datetime,
	@parmDataValueMoney 		money,
	@parmDataValueInteger		int,
	@parmDataValueBit			bit
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR 33800 JMC	Created
******************************************************************************/

DECLARE @BankKey int
DECLARE @LockboxKey int
DECLARE @DepositDateKey int

BEGIN TRY

	EXEC OLTA.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteLockboxID = @parmSiteLockboxID,
		@parmBatchID = @parmBatchID,
		@parmProcessingDateKey = @parmProcessingDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmLockboxKey = @LockboxKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

	UPDATE 	OLTA.factItemData
	SET 	DataValue = @parmDataValue,
			DataValueDateTime = @parmDataValueDateTime,
			DataValueMoney = @parmDataValueMoney,
			DataValueInteger = @parmDataValueInteger,
			DataValueBit = @parmDataValueBit,
			ModifiedBy = SUSER_SNAME(),
			ModificationDate = GETDATE()
	FROM 	OLTA.factItemData
	WHERE	OLTA.factItemData.DepositDateKey = @DepositDateKey
			AND OLTA.factItemData.LockboxKey = @LockboxKey
			AND OLTA.factItemData.ProcessingDateKey = @parmProcessingDateKey
			AND OLTA.factItemData.BatchID = @parmBatchID
			AND OLTA.factItemData.BatchSequence = @parmBatchSequence
			AND OLTA.factItemData.ItemDataSetupFieldKey = @parmItemDataSetupFieldKey

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
