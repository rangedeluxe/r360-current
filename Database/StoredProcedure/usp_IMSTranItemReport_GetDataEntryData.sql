--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSTranItemReport_GetDataEntryData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSTranItemReport_GetDataEntryData') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSTranItemReport_GetDataEntryData
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSTranItemReport_GetDataEntryData
(
	@parmBankID				int,
	@parmLockboxID			int, 
	@parmProcessingDateKey	int,
	@parmDepositDateKey		int,
	@parmBatchID			int,
	@parmTransactionID      int,
	@parmBatchSequence      int = NULL,
	@parmBatchSourceKey		int
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/01/2011
*
* Purpose: Retrieve the data for document data for IMS report
*
* Modification History
* 04/01/2011 CR 33627 WJS	Created
* 10/10/2011 CR 47416 WJS Modified to pull processing Date from factBatchtable
* 02/06/2012 CR 49997 WJS   Modified to get sourceProcessingDateKey
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY


	SELECT 	OLTA.factDataEntryDetails.BatchSequence,
			OLTA.dimDataEntryColumns.DisplayName AS Keyword,
			OLTA.factDataEntryDetails.DataEntryValue AS DataValue, 
			OLTA.dimDataEntryColumns.MarkSense AS Marksense 
	FROM 	OLTA.factDataEntryDetails
			INNER JOIN OLTA.dimDataEntryColumns ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
			INNER JOIN OLTA.dimLockboxes ON	OLTA.dimLockboxes.LockboxKey = OLTA.factDataEntryDetails.LockboxKey
	WHERE 	OLTA.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
			AND OLTA.dimLockboxes.SiteBankID = @parmBankID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.factDataEntryDetails.ProcessingDateKey = @parmProcessingDateKey
			AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
			AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
			AND OLTA.factDataEntryDetails.BatchSequence = CASE WHEN @parmBatchSequence >= 0 THEN @parmBatchSequence ELSE OLTA.factDataEntryDetails.BatchSequence END
			AND OLTA.factDataEntryDetails.BatchSourceKey = @parmBatchSourceKey
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @parmErrorMessage    NVARCHAR(4000),
				@parmErrorProcedure	 NVARCHAR(200),
				@parmErrorSeverity   INT,
				@parmErrorState      INT,
				@parmErrorLine		 INT
		
		SELECT	@parmErrorMessage = ERROR_MESSAGE(),
				@parmErrorSeverity = ERROR_SEVERITY(),
				@parmErrorState = ERROR_STATE(),
				@parmErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@parmErrorLine = ERROR_LINE()

		SET @parmErrorMessage = @parmErrorProcedure + ' (Line: %d)-> ' + @parmErrorMessage

		RAISERROR(@parmErrorMessage,@parmErrorSeverity,@parmErrorState,@parmErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH

