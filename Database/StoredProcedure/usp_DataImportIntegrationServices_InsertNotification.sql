--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_InsertNotification
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_DataImportIntegrationServices_InsertNotification') IS NOT NULL
       DROP PROCEDURE OLTA.usp_DataImportIntegrationServices_InsertNotification
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_DataImportIntegrationServices_InsertNotification] 
(
	@parmNotification	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: Insert a notification XML document into the DataImportQueue table.
*
* Modification History
* 05/30/2012 CR 53203 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY
	INSERT INTO OLTA.DataImportQueue(QueueStatus,XSDVersion,ClientProcessCode,SourceTrackingID,QueueType,EntityTrackingID,XMLDataDocument)
	SELECT	10,
			NotificationImport.att.value('../@XSDVersion','varchar(12)'),
			NotificationImport.att.value('../@ClientProcessCode','varchar(40)'),
			NotificationImport.att.value('../@SourceTrackingID','varchar(36)'),
			2,
			NotificationImport.att.value('@NotificationTrackingID','varchar(36)'),
			NotificationImport.att.query('.') AS XMLDataDocument
	FROM	@parmNotification.nodes('/Notifications/Notification') AS NotificationImport(att);
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH