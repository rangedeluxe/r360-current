--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetStubsDE
--WFSScriptProcessorStoredProcedureDrop
IF EXISTS	(
				SELECT	* 
				FROM	INFORMATION_SCHEMA.ROUTINES 
				WHERE	SPECIFIC_SCHEMA = N'OLTA' 
						AND SPECIFIC_NAME = N'usp_Research_GetStubsDE' 
			)
   DROP PROCEDURE OLTA.usp_Research_GetStubsDE
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetStubsDE] 
		@parmSiteBankID int, 
		@parmSiteLockboxID int,
		@parmBatchID int,
		@parmDepositDate datetime,
		@parmTransactionID int

WITH RECOMPILE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/25/2010
*
* Purpose: Retrieve Stubs/StubsDataEntry Data (field names and values)
*
* Modification History
* 01/25/2010 CR 28961 JNE  -Created
* 06/16/2010 CR 29958 JMC  -Added fields to dataset: BankID, LockboxID, BatchID,
*                           ProcessingDateKey, DepositDateKey, TransactionID 
*                          -Changed order of input parameters to be consistent 
*                           with usp_Research_GetChecksDE
*                          -Added BatchSequence to ORDER BY clause
* 12/08/2010 CR 32055 JNE  -Modified SQL so that DataEntryColumns will be in output
*			    even when there is no data for the particular field. 
* 04/04/2011 CR 33704      - Added Batchsourcekey to return set
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT  OLTA.dimLockboxes.SiteBankID	AS BankID,
			OLTA.dimLockboxes.SiteLockboxID	AS LockboxID,
			OLTA.factDataEntrySummary.BatchID,
			OLTA.factDataEntrySummary.ProcessingDateKey,
			OLTA.factDataEntrySummary.DepositDateKey,
			StubDEData.TransactionID,
			StubDEData.BatchSequence,
			OLTA.dimDataEntryColumns.TableName,
			OLTA.dimDataEntryColumns.FldName,
			RTrim(COALESCE(OLTA.dimDataEntryColumns.DisplayName,OLTA.dimDataEntryColumns.FldName)) AS FldTitle,
			CASE
				WHEN ((OLTA.dimDataEntryColumns.TableType = 0 OR OLTA.dimDataEntryColumns.TableType = 2) 
					AND RTRIM(OLTA.dimDataEntryColumns.FldName) = 'Amount') 
				THEN 7
				ELSE OLTA.dimDataEntryColumns.DataType
				END AS FldDataTypeEnum,
			StubDEDataValue.FldValue,
			OLTA.dimDataEntryColumns.ScreenOrder,
			StubDEData.BatchSourceKey
	FROM	OLTA.dimDataEntryColumns
			INNER JOIN OLTA.factDataEntrySummary ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntrySummary.DataEntryColumnKey
			INNER JOIN OLTA.dimLockboxes ON	OLTA.factDataEntrySummary.LockboxKey = OLTA.dimLockboxes.LockboxKey
			LEFT JOIN (SELECT  DISTINCT OLTA.dimLockboxes.SiteBankID	AS BankID,
					OLTA.dimLockboxes.SiteLockboxID	AS LockboxID,
					OLTA.factDataEntryDetails.LockboxKey,
					OLTA.factDataEntryDetails.BatchID,
					OLTA.factDataEntryDetails.DepositDateKey,
					OLTA.factDataEntryDetails.TransactionID,
					OLTA.factDataEntryDetails.BatchSequence,
					OLTA.factDataEntryDetails.BatchSourceKey
				FROM	OLTA.dimDataEntryColumns
					INNER JOIN OLTA.factDataEntryDetails ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
					INNER JOIN OLTA.dimLockboxes ON	OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
				WHERE	OLTA.factDataEntryDetails.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
					AND OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
					AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
					AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
					AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
					AND OLTA.dimDataEntryColumns.TableType IN (2, 3)) AS StubDEData ON
				StubDEData.DepositDatekey = OLTA.factDataEntrySummary.DepositDateKey AND
				StubDEData.LockboxKey = OLTA.factDataEntrySummary.LockboxKey AND
				StubDEData.BatchID = OLTA.factDataEntrySummary.BatchID
			LEFT JOIN (SELECT  OLTA.dimLockboxes.SiteBankID	AS BankID,
				OLTA.dimLockboxes.SiteLockboxID	AS LockboxID,
				OLTA.factDataEntryDetails.BatchID,
				OLTA.factDataEntryDetails.DepositDateKey,
				OLTA.factDataEntryDetails.TransactionID,
				OLTA.factDataEntryDetails.BatchSequence,
				OLTA.dimDataEntryColumns.TableName,
				OLTA.dimDataEntryColumns.FldName,
				OLTA.factDataEntryDetails.DataEntryValue AS FldValue
			FROM	OLTA.dimDataEntryColumns
				INNER JOIN OLTA.factDataEntryDetails ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
				INNER JOIN OLTA.dimLockboxes ON	OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
			WHERE	OLTA.factDataEntryDetails.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
					AND OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
					AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
					AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
					AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
					AND OLTA.dimDataEntryColumns.TableType IN (2, 3)) AS StubDEDataValue ON
			StubDEDataValue.DepositDatekey = OLTA.factDataEntrySummary.DepositDateKey AND 
			StubDEDataValue.BatchID = OLTA.factDataEntrySummary.BatchID AND
			StubDEDataValue.TransactionID = StubDEData.TransactionID AND
			StubDEDataValue.BatchSequence = StubDEData.BatchSequence AND
			StubDEDataValue.TableName =  OLTA.dimDataEntryColumns.TableName AND
			StubDEDataValue.FldName = OLTA.dimDataEntryColumns.FldName
	WHERE	OLTA.factDataEntrySummary.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
			AND OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
			AND OLTA.factDataEntrySummary.BatchID = @parmBatchID
			AND StubDEData.TransactionID = @parmTransactionID
			AND OLTA.dimDataEntryColumns.TableType IN (2, 3)
	ORDER BY 
			StubDEData.BatchSequence,
			OLTA.dimDataEntryColumns.ScreenOrder
	                

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
