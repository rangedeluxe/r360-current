--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetHOAPMAHOANames
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetHOAPMAHOANames') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetHOAPMAHOANames
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetHOAPMAHOANames] 
	@parmCustomerID int,
    @parmLockboxID int,
    @parmDepositDateKey int,
    @parmUserID int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/09/2012
*
* Purpose: Query HOA Names for user selection
*
* Modification History
* 02/13/2012 CR 50231 JNE	Created
* 04/27/2012 CR 52384 JNE 	Added Accountnumber & BatchSequence when connecting final temp tables.
*				Changed HOA_NAME to HOA_Name, Removed DDANO, 
*				and changed to distinct list for the names.
* 07/09/2012 CR 54033 JNE	Limit superuser to OLCustomerID assigned.
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY
	DECLARE @IsSuperUser bit

	SELECT	@IsSuperUser = SuperUser 
	FROM	OLTA.Users 
	WHERE	OLTA.Users.UserID = @parmUserID
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
	DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp
		
	--Create a temp table to hold dimlockbox info
	CREATE TABLE #tmpHOALockboxTemp
	(
		CustomerKey INT,
		LockboxKey INT,
		SiteCustomerID INT,
		Customer_Name VARCHAR(20),
		Lockbox_Number INT,
		Lockbox_Name VARCHAR(40)
	)
	--Create a temp table to hold data entry information for the records 
	CREATE TABLE #tmpHOADataEntryTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		BatchID INT,
		TransactionID INT,
		AccountNumber VARCHAR(80),
		BatchSequence INT,
		TableName VARCHAR(36),
		FldName VARCHAR(32),
		DataEntryValue VARCHAR(256)
	)

	--Create a temp table to hold sum information for the records 
	CREATE TABLE #tmpHOAMainTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		AccountNumber VARCHAR(80),
		BatchSequence INT
	)
	
	IF @IsSuperUser > 0
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.Users ON OLTA.Users.OLCustomerID = OLTA.OLLockboxes.OLCustomerID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.Users.UserID = @parmUserID
			
	END
	ELSE
	BEGIN		
		
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.OLLockboxUsers ON OLTA.OLLockboxUsers.OLLockboxID = OLTA.OLLockboxes.OLLockboxID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.OLLockboxUsers.UserID = @parmUserID
	END	
	

	INSERT INTO #tmpHOADataEntryTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,BatchID,TransactionID,AccountNumber,BatchSequence,TableName,FldName,DataEntryValue)
	SELECT DISTINCT OLTA.factDataEntryDetails.BankKey,
		OLTA.factDataEntryDetails.CustomerKey,
		OLTA.factDataEntryDetails.LockboxKey,
		OLTA.factDataEntryDetails.ProcessingDateKey,
		OLTA.factDataEntryDetails.DepositDateKey,
		OLTA.factDataEntryDetails.BatchID,
		OLTA.factDataEntryDetails.TransactionID,
		"",
		OLTA.factDataEntryDetails.BatchSequence,
		OLTA.dimDataEntryColumns.TableName, 
		OLTA.dimDataEntryColumns.FldName, 
		OLTA.factDataEntryDetails.DataEntryValue 
	FROM OLTA.factDataEntryDetails
		INNER JOIN OLTA.dimLockboxes ON OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.factDataEntryDetails.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
	WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
		AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
		AND OLTA.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
		AND OLTA.dimDataEntryColumns.TableName = 'StubsDataEntry'
		AND (OLTA.dimDataEntryColumns.FldName = 'hoa_name')
	
	UPDATE #tmpHOADataEntryTemp set #tmpHOADataEntryTemp.AccountNumber = factStubs.AccountNumber 
	FROM OLTA.factStubs
	WHERE #tmpHOADataEntryTemp.BankKey = factStubs.BankKey
		AND #tmpHOADataEntryTemp.CustomerKey = factStubs.CustomerKey
		AND #tmpHOADataEntryTemp.LockboxKey = factStubs.LockboxKey
		AND #tmpHOADataEntryTemp.ProcessingDateKey = factStubs.ProcessingDateKey
		AND #tmpHOADataEntryTemp.DepositDateKey = factStubs.DepositDateKey
		AND #tmpHOADataEntryTemp.BatchID = factStubs.BatchID
		AND #tmpHOADataEntryTemp.TransactionID = factStubs.TransactionID

	--Insert into Main Temp
	INSERT INTO #tmpHOAMainTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,AccountNumber,BatchSequence)
	SELECT DISTINCT OLTA.factStubs.BankKey,
		OLTA.factStubs.CustomerKey,
		OLTA.factStubs.LockboxKey,
		OLTA.factStubs.ProcessingDateKey,
		OLTA.factStubs.DepositDateKey,
		OLTA.factStubs.AccountNumber,
		OLTA.factStubs.BatchSequence
	FROM OLTA.factStubs
		INNER JOIN #tmpHOALockboxTemp ON #tmpHOALockboxTemp.CustomerKey = OLTA.factStubs.CustomerKey AND #tmpHOALockboxTemp.LockboxKey = OLTA.factStubs.LockboxKey
	WHERE OLTA.factStubs.DepositDateKey = @parmDepositDateKey

		
	SELECT Distinct #tmpHOAMainTemp.AccountNumber AS HOA_Number,
		HOA_NAME.DataEntryValue AS HOA_Name
	FROM #tmpHOAMainTemp
		LEFT JOIN #tmpHOADataEntryTemp AS HOA_NAME
			ON HOA_NAME.BankKey = #tmpHOAMainTemp.BankKey
				AND HOA_NAME.CustomerKey = #tmpHOAMainTemp.CustomerKey
				AND HOA_NAME.LockboxKey = #tmpHOAMainTemp.LockboxKey
				AND HOA_NAME.ProcessingDateKey = #tmpHOAMainTemp.ProcessingDateKey
				AND HOA_NAME.DepositDateKey = #tmpHOAMainTemp.DepositDateKey
				AND HOA_NAME.AccountNumber = #tmpHOAMainTemp.AccountNumber
				AND HOA_NAME.FldName = 'hoa_name'
	ORDER BY #tmpHOAMainTemp.AccountNumber,HOA_NAME.DataEntryValue

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
	DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp
	
END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp

	EXEC OLTA.usp_wfsRethrowException
END CATCH

