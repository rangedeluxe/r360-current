--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_OLLockboxes_Get_ImageDisplayModeForLockbox
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_OLLockboxes_Get_ImageDisplayModeForLockbox') IS NOT NULL
       DROP PROCEDURE OLTA.usp_OLLockboxes_Get_ImageDisplayModeForLockbox
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_OLLockboxes_Get_ImageDisplayModeForLockbox 
(
    @parmCustomerID UNIQUEIDENTIFIER,
    @parmLockboxID  UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Get the image dislay mode for the lockbox
*
* Modification History
* 01/24/2013 WI 86306 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
   SELECT 
       CASE 
         WHEN OLTA.OLLockboxes.CheckImageDisplayMode > 0 THEN OLTA.OLLockboxes.CheckImageDisplayMode 
         ELSE OLTA.OLCustomers.CheckImageDisplayMode 
       END                                      AS CheckImageDisplayMode,
       CASE 
         WHEN OLTA.OLLockboxes.DocumentImageDisplayMode > 0 THEN OLTA.OLLockboxes.DocumentImageDisplayMode 
         ELSE OLTA.OLCustomers.DocumentImageDisplayMode 
       END                                      AS DocumentImageDisplayMode 
   FROM 
       OLTA.OLLockboxes 
       JOIN OLTA.OLCustomers ON 
           OLTA.OLCustomers.OLCustomerID = OLTA.OLLockboxes.OLCustomerID 
   WHERE 
       OLTA.OLLockboxes.OLLockboxID =  @parmLockboxID
       AND OLTA.OLCustomers.OLCustomerID =  @parmCustomerID;
END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

