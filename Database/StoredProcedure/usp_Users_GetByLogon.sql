--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Users_GetByLogon
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Users_GetByLogon') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Users_GetByLogon
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Users_GetByLogon]
	@parmLogonName	varchar(16),
	@parmUserType	int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28682 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	IF @parmUserType = 0
	BEGIN
		SELECT	OLTA.Users.UserID,
				OLTA.Users.LogonName,
				OLTA.Users.FirstName,
				OLTA.Users.MiddleInitial,
				OLTA.Users.LastName,
				OLTA.Users.SuperUser,
				OLTA.Users.[Password],
				OLTA.Users.PasswordSet, 
				OLTA.Users.IsFirstTime,
				OLTA.Users.CreationDate,    
				OLTA.Users.CreatedBy,
				OLTA.Users.ModificationDate,
				OLTA.Users.ModifiedBy,       
				OLTA.Users.FailedLogonAttempts, 
				OLTA.Users.EmailAddress,
				OLTA.Users.IsActive,
				OLTA.Users.OLCustomerID,
				OLTA.Users.UserType,
				OLTA.Users.ExternalID1,
				OLTA.Users.ExternalID2,
				OLTA.OLCustomers.CustomerCode 
		FROM	OLTA.Users 
				LEFT JOIN OLTA.OLCustomers ON OLTA.Users.OLCustomerID = OLTA.OLCustomers.OLCustomerID
		WHERE	OLTA.Users.LogonName = @parmLogonName
				AND OLTA.Users.UserType = @parmUserType
				AND OLTA.Users.OLCustomerID IS NOT NULL
	END
	ELSE
	BEGIN
		SELECT	OLTA.Users.UserID,
				OLTA.Users.LogonName,
				OLTA.Users.FirstName,
				OLTA.Users.MiddleInitial,
				OLTA.Users.LastName,
				OLTA.Users.SuperUser,
				OLTA.Users.[Password],
				OLTA.Users.PasswordSet, 
				OLTA.Users.IsFirstTime,
				OLTA.Users.CreationDate,    
				OLTA.Users.CreatedBy,
				OLTA.Users.ModificationDate,
				OLTA.Users.ModifiedBy,       
				OLTA.Users.FailedLogonAttempts, 
				OLTA.Users.EmailAddress,
				OLTA.Users.IsActive,
				OLTA.Users.OLCustomerID,
				OLTA.Users.UserType,
				OLTA.Users.ExternalID1,
				OLTA.Users.ExternalID2,
				OLTA.OLCustomers.CustomerCode 
		FROM	OLTA.Users 
				LEFT JOIN OLTA.OLCustomers ON OLTA.Users.OLCustomerID = OLTA.OLCustomers.OLCustomerID
		WHERE	OLTA.Users.LogonName = @parmLogonName
				AND OLTA.Users.UserType = @parmUserType
				AND OLTA.Users.OLCustomerID IS NULL
	END


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

