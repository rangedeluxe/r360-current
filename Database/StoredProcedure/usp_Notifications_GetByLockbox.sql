--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Notifications_GetByLockbox
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Notifications_GetByLockbox') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Notifications_GetByLockbox
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Notifications_GetByLockbox] 
	@parmSiteBankID int,
	@parmSiteCustomerID int,
	@parmSiteLockboxID int,
	@parmNotificationDate DateTime
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28701 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT
	Notifications.NotificationID,
	CONVERT(varchar, Notifications.NotificationDate, 108) AS NotificationTime,
	Notifications.DeliveryErrorMessage,
	Notifications.MessageText,
	EventRules.LockboxID,
	EventRules.EventRuleDescription,
	EventRules.EventAction,
	DeliveryMethods.DeliveryMethodName,
	Contacts.ContactName
FROM Notifications
	INNER JOIN Alerts ON Alerts.AlertID = Notifications.AlertID
	INNER JOIN EventRules ON EventRules.EventRuleID = Alerts.EventRuleID
	INNER JOIN DeliveryMethods ON DeliveryMethods.DeliveryMethodID = Alerts.DeliveryMethodID
	INNER JOIN Contacts ON Contacts.ContactID = Alerts.ContactID
WHERE 
	EventRules.RuleIsActive = 1
	AND CAST(CONVERT(varchar(10), Notifications.NotificationDate, 101) AS DateTime) = CAST(CONVERT(varchar(10), @parmNotificationDate, 101) AS DateTime)
	AND EventRules.BankID = @parmSiteBankID
	AND EventRules.CustomerID = @parmSiteCustomerID
	AND (EventRules.LockboxID = -1 OR EventRules.LockboxID = @parmSiteLockboxID)
ORDER BY NotificationTime DESC, EventRules.LockboxID, Contacts.ContactName, DeliveryMethods.DeliveryMethodName ASC

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

