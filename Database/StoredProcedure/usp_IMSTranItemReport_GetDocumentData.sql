--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSTranItemReport_GetDocumentData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSTranItemReport_GetDocumentData') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSTranItemReport_GetDocumentData
GO

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/01/2011
*
* Purpose: Retrieve the data for document data for IMS report
*
* Modification History
* 04/01/2011 CR 33630 WJS	Created
* 10/10/2011 CR 47343 WJS   Modified to pull processing Date from factBatchtable
* 02/06/2012 CR 49999 WJS   This stored proc is no longer need
******************************************************************************/


