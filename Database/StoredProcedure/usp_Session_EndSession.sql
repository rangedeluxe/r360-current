--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Session_EndSession
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Session_EndSession') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Session_EndSession
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Session_EndSession] 
@parmSessionID uniqueidentifier
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28688 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

UPDATE Session
SET IsSessionEnded = 1
WHERE SessionID = @parmSessionID


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

