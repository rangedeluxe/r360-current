--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Batch_GetByCustomer
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Batch_GetByCustomer') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Batch_GetByCustomer
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Batch_GetByCustomer]
	@parmSiteBankID int,
	@parmSiteCustomerID int,
	@parmDepositDate DateTime 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28717 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT 
	Batch.LockboxID,
	Batch.GlobalBatchID,
	Batch.BatchID,
	Batch.ProcessingDate,
	Batch.DepositStatus,
	Batch.DEStatus,
	Batch.ScanStatus,
	Batch.ModificationDate,
	StatA.StatusDisplayName AS DepositStatusDisplay,
	COALESCE(StatB.StatusDisplayName,'Not Set') AS DataEntryStatusDisplay,
	COALESCE(StatC.StatusDisplayName,'Not Set') AS ScanStatusDisplay
FROM Batch 
	INNER JOIN Lockbox ON 
		Lockbox.BankID =Batch.BankID 
		AND Lockbox.LockboxID = Batch.LockboxID
	LEFT JOIN StatusTypes StatA ON 
		StatA.StatusValue = Batch.DepositStatus
	LEFT JOIN StatusTypes StatB ON 
		StatB.StatusValue = Batch.DEStatus
	LEFT JOIN StatusTypes StatC ON 
		StatC.StatusValue = Batch.ScanStatus
WHERE Batch.BankID = @parmSiteBankID
	AND Lockbox.CustomerID = @parmSiteCustomerID
	AND Convert(varchar,Batch.DepositDate,101) = Convert(varchar,@parmDepositDate,101)
	AND StatA.StatusType = 'Deposit'
	AND StatB.StatusType = 'DataEntry'
	AND StatC.StatusType = 'Scan'
ORDER BY 
	Batch.LockboxID, 
	Batch.BatchID, 
	Batch.ProcessingDate ASC

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

