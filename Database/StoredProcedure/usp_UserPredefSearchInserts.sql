--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_UserPredefSearchInserts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_UserPredefSearchInserts') IS NOT NULL
       DROP PROCEDURE OLTA.usp_UserPredefSearchInserts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_UserPredefSearchInserts]
	@parmUserID int,
	@parmIsDefault int, 
	@parmName varchar(256), 
	@parmDescription varchar(1024), 
	@parmSearchParmsXML xml,
	@parmPreDefSearchID uniqueidentifier output,
	@parmRowsReturned int output,
	@parmError int output
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 06/01/2011
*
* Purpose: 
*
* Modification History
* 06/01/2011 CR 46103 JNE	Created
* 08/29/2011 CR 46103 JNE   Updated Error Handling
******************************************************************************/

SET ARITHABORT ON
SET XACT_ABORT ON

SET @parmError = 0

BEGIN TRY
	BEGIN TRANSACTION
		-- Insert into PreDefSearch 
		EXEC usp_PredefSearchInsert @parmName, @parmDescription, @parmSearchParmsXML, @parmPredefSearchID OUTPUT
		-- Insert into UserPreDefSearch 
		EXEC usp_UserPreDefSearchInsert @parmUserID, @parmPredefSearchID, @parmIsDefault, @parmRowsReturned OUTPUT
	COMMIT TRANSACTION
END TRY
BEGIN CATCH
	SET @parmError = 1
	IF XACT_STATE() != 0 ROLLBACK TRANSACTION
	IF @@NESTLEVEL = 1
		BEGIN
			EXEC dbo.usp_WfsRethrowException
		END
	ELSE
		BEGIN
			DECLARE @ErrorMessage    NVARCHAR(4000),
					@ErrorProcedure	 NVARCHAR(200),
					@ErrorSeverity   INT,
					@ErrorState      INT,
					@ErrorLine		 INT
		
			SELECT	@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
					@ErrorLine = ERROR_LINE()

			SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
		END
END CATCH
