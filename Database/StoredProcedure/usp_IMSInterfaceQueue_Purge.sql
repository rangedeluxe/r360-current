--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSInterfaceQueue_Purge') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSInterfaceQueue_Purge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_IMSInterfaceQueue_Purge]
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/13/2009
*
* Purpose: Delete 'old' records from the IMSIntefaceQueue table.
*
* Modification History
* 07/13/2009 CR 25817 JPB	Created
******************************************************************************/
SET NOCOUNT ON 

DECLARE @iRetentionDays SMALLINT,
		@xmlErrorMsg XML
		
BEGIN TRY
	SELECT	@iRetentionDays = COALESCE(Value,5)	--default to 5 days
	FROM	dbo.SystemSetup
	WHERE	Section = 'IMSInterfaceQueue'
			AND SetupKey = 'RetentionDays'
			
	DELETE	OLTA.IMSInterfaceQueue
	FROM	OLTA.IMSInterfaceQueue
	WHERE	DATEDIFF(DAY,OLTA.IMSInterfaceQueue.CreationDate,GETDATE()) >= @iRetentionDays
			AND QueueDataStatus = 99 --make sure the item has been processed before it is deleted
			AND QueueImageStatus = 99 -- make sure the item has been processed before it is deleted
END TRY
BEGIN CATCH
	SELECT @xmlErrorMsg = (SELECT @iRetentionDays AS RetentionDays FOR XML RAW ('IMSInterfaceQueuePurge'), TYPE, ROOT('IMSInterfaceQueuePurge'))
	EXEC OLTA.usp_Ins_ServiceBrokerError @xmlErrorMsg
END CATCH
