--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetImageInfoXML
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetImageInfoXML') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetImageInfoXML
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetImageInfoXML] 
       @parmSiteBankID int, 
       @parmSiteLockboxID int,
       @parmDepositDate datetime,
       @parmBatchID int,
       @parmTransactionID int,
       @parmBatchSequence int,
       @parmFileDescriptor varchar(30)
WITH RECOMPILE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MEH
* Date: 05/21/2009
*
* Purpose: To get the ImageInfoXML column from the factChecks or factDocuments
* 			table based on the deposit date, bank, lockbox, batch, and batch sequence
*
* Modification History
* 05/21/2009 CR 25817 MEH	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	IF @parmFileDescriptor = 'C'
	BEGIN
			SELECT	OLTA.factChecks.ImageInfoXML
			FROM	OLTA.factChecks
					INNER JOIN OLTA.dimLockboxes ON OLTA.factChecks.LockboxKey = OLTA.dimLockboxes.LockboxKey
					INNER JOIN OLTA.dimDates AS dimDepositDates ON OLTA.factChecks.DepositDateKey = dimDepositDates.DateKey
			WHERE	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
					AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
					AND OLTA.factChecks.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
					AND OLTA.factChecks.BatchID = @parmBatchID
					AND OLTA.factChecks.TransactionID = @parmTransactionID
					AND OLTA.factChecks.BatchSequence = @parmBatchSequence
	END
	ELSE
	BEGIN
			SELECT	OLTA.factDocuments.ImageInfoXML
			FROM	OLTA.factDocuments
					INNER JOIN OLTA.dimLockboxes ON OLTA.factDocuments.LockboxKey = OLTA.dimLockboxes.LockboxKey
					INNER JOIN OLTA.dimDates AS dimDepositDates ON OLTA.factDocuments.DepositDateKey = dimDepositDates.DateKey
			WHERE	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
					AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
					AND OLTA.factDocuments.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
					AND OLTA.factDocuments.BatchID = @parmBatchID
					AND OLTA.factDocuments.TransactionID = @parmTransactionID
					AND OLTA.factDocuments.BatchSequence = @parmBatchSequence
	END
END TRY
BEGIN CATCH
       EXEC OLTA.usp_WfsRethrowException
END CATCH
GO
