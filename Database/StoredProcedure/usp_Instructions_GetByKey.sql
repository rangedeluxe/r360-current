--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Instructions_GetByKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Instructions_GetByKey') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Instructions_GetByKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Instructions_GetByKey] 
	@parmSiteBankID int,
	@parmSiteCustomerID int,
	@parmSiteLockboxID int,
	@parmType int,
	@parmTag varchar(20),
	@parmAsOf datetime
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28705 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT 
	InstructionsText.Instructions AS Instructions,
	ISNULL(RTRIM(InstructionsText.Description), 'Not Defined') AS Description,
	ISNULL(RTRIM(InstructionTypes.Description), 'Not Defined') AS InstructionType
FROM InstructionsText
	LEFT OUTER JOIN InstructionTypes ON 
		InstructionsText.Type = InstructionTypes.InstructionTypeID
WHERE BankID = @parmSiteBankID
	AND CustomerID = @parmSiteCustomerID
	AND LockboxID = @parmSiteLockboxID
	AND Type = @parmType
	AND Tag = @parmTag
	AND AsOf = @parmAsOf


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

