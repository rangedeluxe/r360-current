--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_InsertBatch
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_DataImportQueue_InsertBatch') IS NOT NULL
       DROP PROCEDURE OLTA.usp_DataImportQueue_InsertBatch
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_DataImportQueue_InsertBatch] 
(
	@parmBatch	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: 
*
* Modification History
* 01/24/2012 CR 50172 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY
	INSERT INTO OLTA.DataImportQueue(ImportQueueStatus,SourceTrackingID,QueueType,EntityTrackingID,XMLDataDocument)
	SELECT	10,
			Batch.att.value('../@SourceTrackingID','varchar(36)'),
			1,
			Batch.att.value('@BatchTrackingID','varchar(36)'),
			Batch.att.query('.') AS XMLDataDocument
	FROM	@parmBatch.nodes('/Batches/Batch') AS Batch(att);
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH