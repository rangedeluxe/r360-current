--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Users_Get_ByUniqueUserExtID1
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Users_Get_ByUniqueUserExtID1') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Users_Get_ByUniqueUserExtID1
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_Users_Get_ByUniqueUserExtID1
(
       @parmUserExternalID1      VARCHAR(32)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 03/14/2013
*
* Purpose: Get User Fields for a unique user external id 1
*
* Modification History
* 03/14/2013 WI 92388 TWE Retrieve User information based on Unique External ID 1
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
    SELECT 
        OLTA.Users.UserID,
        OLTA.Users.LogonName,
        OLTA.Users.FirstName,
        OLTA.Users.MiddleInitial,
        OLTA.Users.LastName,
        OLTA.Users.SuperUser,
        OLTA.Users.PasswordSet,
        OLTA.Users.IsFirstTime,
        OLTA.Users.CreationDate,
        OLTA.Users.CreatedBy,
        OLTA.Users.ModificationDate,
        OLTA.Users.ModifiedBy,
        OLTA.Users.FailedLogonAttempts,
        OLTA.Users.FailedSecurityQuestionAttempts,
        OLTA.Users.EmailAddress,
        OLTA.Users.IsActive,
        OLTA.Users.OLCustomerID,
        OLTA.Users.UserType,
        OLTA.Users.ExternalID1,
        OLTA.Users.ExternalID2,
        OLTA.Users.SetupToken,
        (
          SELECT 
              COUNT(*) 
          FROM   
              OLTA.OLUserQuestions 
          WHERE  
              OLTA.OLUserQuestions.UserID = OLTA.Users.UserID
        ) 
              AS SecretQuestionCount
    FROM   
        OLTA.Users 
        INNER JOIN OLTA.OLCustomers 
            ON OLTA.OLCustomers.OLCustomerID = OLTA.Users.OLCustomerID
    WHERE  
        OLTA.Users.ExternalID1 = @parmUserExternalID1;
END TRY
BEGIN CATCH
       EXEC OLTA.usp_WfsRethrowException;
END CATCH;
