--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_IMSDocumentInsert
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSDocumentInsert') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSDocumentInsert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSDocumentInsert
       @parmGlobalBatchID INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2008 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/01/2009
*
* Purpose: Insert a message into the MSMQ queue to alert the IMS system that
*	new documents have arrived for processing.
*
* Modification History
* 07/01/2009 CR 25817 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
DECLARE @nvcGlobalBatchID NVARCHAR(30),
		@nvcMSMQPath NVARCHAR(128),
		@xmlData xml

BEGIN TRY
	SET @xmlData = '<GlobalBatchID>' + CAST(@parmGlobalBatchID AS NVARCHAR(30)) + '</GlobalBatchID>'
	INSERT INTO OLTA.IMSInterfaceQueue (QueueType,QueueData) VALUES (1,@xmlData)
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorSeverity   INT,
			@ErrorState      INT
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE()
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
