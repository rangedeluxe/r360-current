--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Users_GetByOLCustomerID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Users_GetByOLCustomerID') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Users_GetByOLCustomerID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Users_GetByOLCustomerID]
	@parmOLCustomerID uniqueidentifier 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/02/2010
*
* Purpose: 
*
* Modification History
* 02/02/2010 CR 28854 JMC	Created
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

 SELECT
     OLCustomers.CustomerCode,
     Users.UserType,
     Users.UserID,
     Users.LogonName,
     Users.IsActive,
     OLCustomers.CustomerCode,
     Users.UserType AS Expr2,
     Users.LastName,
     Users.FirstName,
     Users.EmailAddress,
     Users.ModificationDate  AS DateLastUpdated,
     MAX([Session].LastPageServed) AS LastPageServed
 FROM Users
     LEFT OUTER JOIN [Session] ON
         Users.UserID = [Session].UserID
     LEFT OUTER JOIN OLCustomers ON
         Users.OLCustomerID = OLCustomers.OLCustomerID
 WHERE
     Users.OLCustomerID = @parmOLCustomerID
     AND Users.UserType = 0
 GROUP BY
     Users.UserID,
     Users.LogonName,
     Users.FirstName,
     Users.LastName,
     Users.ModificationDate,
     Users.EmailAddress,
     OLCustomers.CustomerCode,
     Users.UserType,
     Users.IsActive,
     OLCustomers.CustomerCode
 ORDER BY
     Users.LogonName


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

