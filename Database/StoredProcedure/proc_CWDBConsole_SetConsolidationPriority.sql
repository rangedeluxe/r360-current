--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.proc_CWDBConsole_SetConsolidationPriority') IS NOT NULL
       DROP PROCEDURE dbo.proc_CWDBConsole_SetConsolidationPriority
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[proc_CWDBConsole_SetConsolidationPriority]
	@parmBatchInfoXML xml,
	@parmCWDBConsolidationPriority tinyint
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/23/2010
*
* Purpose: 
*
* XML Format:
*<Root>
*	<Batch GlobalBatchID="0000000001" />
*	<Batch GlobalBatchID="0000000002" />
*	<Batch GlobalBatchID="0000000003" />
*	<Batch GlobalBatchID="0000000004" />
*</Root>
*
* Modification History
* 04/23/2010 CR 29460 JMC	Initial Version.
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
DECLARE @bLocalTransaction	bit

BEGIN TRY


	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1
	END

    UPDATE BatchTrace 
	SET CWDBConsolidationPriority = @parmCWDBConsolidationPriority
	WHERE GlobalBatchID IN 
    (
		SELECT BatchInfoXML.att.value('@GlobalBatchID', 'int') AS GlobalBatchID 
		FROM @parmBatchInfoXML.nodes('/Root/Batch') BatchInfoXML (att)
	)
    
	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0
	END
END TRY
BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0
	END
    EXEC dbo.usp_WfsRethrowException
END CATCH
