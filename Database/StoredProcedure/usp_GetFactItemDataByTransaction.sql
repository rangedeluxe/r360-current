--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_GetFactItemDataByTransaction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetFactItemDataByTransaction') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetFactItemDataByTransaction
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_GetFactItemDataByTransaction
(
	@parmBankID				int,
	@parmLockboxID			int, 
	@parmProcessingDateKey	int,
	@parmBatchID			int,
	@parmTransactionID      int
)
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/01/2011
*
* Purpose: Retrieve the data for factItemData for a specified TransactionID
*
* Modification History
* 04/07/2011 CR 33773 JMC	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @BankKey int
DECLARE @LockboxKey int
DECLARE @DepositDateKey int

BEGIN TRY

	EXEC OLTA.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmBankID,
		@parmSiteLockboxID = @parmLockboxID,
		@parmBatchID = @parmBatchID,
		@parmProcessingDateKey = @parmProcessingDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmLockboxKey = @LockboxKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

   SELECT
		OLTA.factItemData.BatchSequence, 
		OLTA.dimItemDataSetupFields.Keyword,
		OLTA.factItemData.DataValue
	FROM OLTA.factItemData
		INNER JOIN OLTA.dimItemDataSetupFields ON
			OLTA.dimItemDataSetupFields.ItemDataSetupFieldKey = OLTA.factItemData.ItemDataSetupFieldKey
	WHERE
		OLTA.factItemData.DepositDateKey = @DepositDateKey
		AND OLTA.factItemData.LockboxKey = @LockboxKey
		AND OLTA.factItemData.ProcessingDateKey = @parmProcessingDateKey
		AND OLTA.factItemData.BatchID = @parmBatchID
		AND OLTA.factItemData.TransactionID = @parmTransactionID
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
