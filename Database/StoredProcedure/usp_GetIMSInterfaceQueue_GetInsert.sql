--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetIMSInterfaceQueue_GetInsert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('OLTA.usp_GetIMSInterfaceQueue_GetInsert') IS NOT NULL
	DROP PROCEDURE [OLTA].[usp_GetIMSInterfaceQueue_GetInsert]
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetIMSInterfaceQueue_GetInsert]
       @paramNumItemsToRetrive smallint = 5,
       @paramDelayWriteSeconds int = 120
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 09/23/2009
*
* Purpose: Get the data from the interface queue.
*
* Modification History
* 09/23/2009 CR 25817 WJS	Created
* 11/05/2009 CR 28110 WJS	Updated QueueDataStatus from 10 to 1.
* 11/12/2009 CR 28110 WJS	Added QueueStatus
* 12/16/2009 CR 28506 WJS	Added transaction and temp table. Also update status in stored proc
* 10/28/2010 CR 30919 JPB	Added a time delay to account for network write 
*							delay of image files.
* 10/28/2010 CR 31234 JMC	1.)Changed time-delay to be an optional input parameter
*                        	2.)Changed logic so the delay will be a product of the original 
*                              delay and the current retry iteration.  The purpose of this 
*                              is to stagger retries in the case where there is a condition 
*                              that will take a long time to recover from, we wil allow the 
*                              maximum time available to process the row.
* 10/28/2010 CR 31234 WJS   1) Changed it so you are not able to print back batch ID which are already in use
*							2) Changed so you are only bring back the earliest of the same batch ID
* 02/18/2011 CR 32910 JMC   -Added top level sort to ORDER BY ProcessingDate before IMSInterfaceQueue.CreationDate 
*							-Removed logic that used QueueDataStatus to determine elegibility for processing. 
*							-Changed sentinel values to avoid collisions when BatchID = 0 
*							  (this shouldn't really logically happen, but this condition DOES exist 
*							  in some production databases) 
* 05/09/2011 CR 34193 JPB	Use UTC when calculating times.
* 05/13/2011 CR 34332 JMC	Stored procedure now returns all ID fields and does not return Xml data.
* 09/28/2011 CR 46892 JPB	Changed timestamps back to Local Time.
* 06/13/2012 CR 53435 WJS	Drop siteID
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON

DECLARE @BATCHID int
DECLARE @BANKID int
DECLARE @LOCKBOXID INT
DECLARE	@ProcessingDateKey INT
DECLARE @COUNT int

DECLARE @QueueId uniqueidentifier
DECLARE @QueueData xml
DECLARE @RetryCount int
DECLARE @QueueType smallint


SET @BATCHID = 0
SET @BANKID = 0
SET @LOCKBOXID = 0
SET @ProcessingDateKey = 0
SET @COUNT = 0


BEGIN TRANSACTION IMSInterfaceQueue_Insert
	
	
BEGIN TRY
	
	-- this is used to just get the results you are going to process later
	DECLARE  @tmpOLTAIMSInterfaceQueueBatchIDTemp TABLE
	(
		BatchID INT,
		BankID  INT,
		LockboxID  INT,
		ProcessingDateKey	INT,
		CreationDate datetime
	)

	CREATE TABLE #tmpOLTAIMSInterfaceQueueBatchID
	(
		BatchID INT,
		BankID  INT,
		LockboxID  INT,
		ProcessingDateKey INT
	)

	DECLARE @tmpOLTAIMSInterfaceQueueBatchIDResults TABLE
	(
		QueueID uniqueidentifier,
		SiteBankID int,
		SiteLockboxID int,
		ProcessingDateKey int,
		BatchID int,
		RetryCount tinyint
	)
	
	-- get the items you need order by creation date
	-- limit these to 3 times your items to retrieve so you don't have unlimited number.
	/* retrieve records that have been modified more then 2 minutes ago */
	INSERT INTO @tmpOLTAIMSInterfaceQueueBatchIDTemp (
		BatchID,
		BankID,
		LockboxID,
		ProcessingDateKey,
		CreationDate
	)
	(SELECT TOP (@paramNumItemsToRetrive * 3) 
		BatchId,
		SiteBankID,
		SiteLockboxID,
		ProcessingDateKey,
		CreationDate
		
	 FROM OLTA.IMSInterfaceQueue 
	 WHERE	
		QueueType = 1
		AND QueueStatus=10 	
		AND OLTA.IMSInterfaceQueue.ModificationDate <= (DATEADD(second,(@paramDelayWriteSeconds * -1 * RetryCount),GETDATE()))	
	) 
	ORDER BY 
		ProcessingDateKey ASC,
		CreationDate ASC

	--exclude any that are in use for delete or insert (We will not specifally check for this)
	--only need to worry about those which are being processes
	INSERT INTO #tmpOLTAIMSInterfaceQueueBatchID (
		BatchID,
		BankID,
		LockboxID,
		ProcessingDateKey
	)
	(SELECT 
		BatchId,
		BankID,
		LockboxID,
		ProcessingDateKey
	 FROM 
		@tmpOLTAIMSInterfaceQueueBatchIDTemp) 
     EXCEPT (
	 SELECT 
		BatchId,
		SiteBankID,
		SiteLockboxID,
		ProcessingDateKey
	 FROM	
		OLTA.IMSInterfaceQueue 
	 WHERE	
		QueueStatus =20)
		
	IF @@ROWCOUNT > 0
	BEGIN

		SELECT TOP(1) 
			@BATCHID =  BatchID,
			@BANKID =  BankID,
			@LOCKBOXID =  LockboxID,
			@ProcessingDateKey =  ProcessingDateKey
		FROM #tmpOLTAIMSInterfaceQueueBatchID
		
		WHILE (
			@BATCHID  >= 0 
			AND @BANKID >= 0 
			AND @LOCKBOXID >= 0 
			AND @ProcessingDateKey > 0  
			AND @COUNT < @paramNumItemsToRetrive)
		BEGIN

			/* this will join back to the batchID from the select of the temp table */
			SELECT TOP(1) 
				@QueueID = QueueId,
				@BankID = SiteBankID, 
				@LockboxID = SiteLockboxID, 
				@ProcessingDateKey = #tmpOLTAIMSInterfaceQueueBatchID.ProcessingDateKey,   
				@BatchID = #tmpOLTAIMSInterfaceQueueBatchID.BatchID,  
				@QueueType = QueueType,
				@RetryCount = RetryCount


			FROM #tmpOLTAIMSInterfaceQueueBatchID 
				INNER JOIN OLTA.IMSInterfaceQueue  ON 
					OLTA.IMSInterfaceQueue.BatchID			= #tmpOLTAIMSInterfaceQueueBatchID.BatchID AND
					OLTA.IMSInterfaceQueue.SiteBankID		= #tmpOLTAIMSInterfaceQueueBatchID.BankID	 AND
					OLTA.IMSInterfaceQueue.SiteLockboxID	= #tmpOLTAIMSInterfaceQueueBatchID.LockboxID AND
					OLTA.IMSInterfaceQueue.ProcessingDateKey= #tmpOLTAIMSInterfaceQueueBatchID.ProcessingDateKey
			WHERE QueueType = 1 AND QueueStatus=10
				AND OLTA.IMSInterfaceQueue.BatchID = @BATCHID
				AND OLTA.IMSInterfaceQueue.SiteBankID = @BANKID
				AND OLTA.IMSInterfaceQueue.SiteLockboxID = @LOCKBOXID
				AND OLTA.IMSInterfaceQueue.ProcessingDateKey = @ProcessingDateKey
			ORDER BY 
				OLTA.IMSInterfaceQueue.ProcessingDateKey, OLTA.IMSInterfaceQueue.CreationDate ASC

			IF @QueueType = 1
			BEGIN
				INSERT INTO @tmpOLTAIMSInterfaceQueueBatchIDResults (QueueId, SiteBankID, SiteLockboxID, ProcessingDateKey, BatchID, RetryCount)
				SELECT @QueueId, @BankID, @LockboxID, @ProcessingDateKey, @BatchID, @RetryCount
			END

			DELETE FROM #tmpOLTAIMSInterfaceQueueBatchID 
			WHERE 
				BatchID= @BATCHID 
				AND BankID = @BANKID  
				AND LockboxID = @LOCKBOXID
				AND @ProcessingDateKey = ProcessingDateKey

			SET @BATCHID = -1
			SET @BANKID = -1
			SET @LOCKBOXID = -1
			SET @ProcessingDateKey = 0

			SET @QueueId =  NULL
			SET @QueueData =  NULL
			SET @QueueType = -1
			SET @RetryCount = 0
						 
			SELECT TOP(1) 
				@BATCHID =  BatchID,
				@BANKID =  BankID,
				@LOCKBOXID =  LockboxID,
				@ProcessingDateKey =  ProcessingDateKey
			FROM #tmpOLTAIMSInterfaceQueueBatchID

			SET @COUNT = (SELECT Count(*) FROM @tmpOLTAIMSInterfaceQueueBatchIDResults)
			
		END
	END
	SELECT * FROM @tmpOLTAIMSInterfaceQueueBatchIDResults

	COMMIT TRANSACTION IMSInterfaceQueue_Insert
	--Drop the temp tables
	DROP TABLE #tmpOLTAIMSInterfaceQueueBatchID
END TRY
BEGIN CATCH
	ROLLBACK TRANSACTION IMSInterfaceQueue_Insert
	EXEC OLTA.usp_WfsRethrowException
END CATCH






