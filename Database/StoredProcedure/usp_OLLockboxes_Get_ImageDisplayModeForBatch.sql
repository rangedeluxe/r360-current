--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_OLLockboxes_Get_ImageDisplayModeForBatch
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_OLLockboxes_Get_ImageDisplayModeForBatch') IS NOT NULL
       DROP PROCEDURE OLTA.usp_OLLockboxes_Get_ImageDisplayModeForBatch
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_OLLockboxes_Get_ImageDisplayModeForBatch 
(
    @parmBankID       INT,
    @parmCustomerID   UNIQUEIDENTIFIER,
    @parmDepositDate  DATETIME,
    @parmLockboxID    INT,
    @parmBatchID      INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Get the image display mode for the batch
*
* Modification History
* 01/24/2013 WI 86305 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DepositDate INT;

BEGIN TRY
    SET @DepositDate = CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);
    
    SELECT 
        CASE 
          WHEN OLTA.OLLockboxes.CheckImageDisplayMode > 0 THEN 
              OLTA.OLLockboxes.CheckImageDisplayMode 
          ELSE OLTA.OLCustomers.CheckImageDisplayMode 
        END                                            AS CheckImageDisplayMode,
        CASE 
          WHEN OLTA.OLLockboxes.DocumentImageDisplayMode > 0 THEN 
              OLTA.OLLockboxes.DocumentImageDisplayMode 
          ELSE OLTA.OLCustomers.DocumentImageDisplayMode 
        END                                            AS DocumentImageDisplayMode 
    FROM OLTA.OLLockboxes 
        INNER JOIN OLTA.OLCustomers ON 
            OLTA.OLCustomers.OLCustomerID = OLTA.OLLockboxes.OLCustomerID 
        INNER JOIN OLTA.dimLockboxes ON 
            OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID 
            AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID 
        INNER JOIN OLTA.factBatchSummary ON 
            OLTA.dimLockboxes.LockboxKey = OLTA.factBatchSummary.LockboxKey
    WHERE 
        OLTA.dimLockboxes.SiteBankID = @parmBankID
        AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
        AND OLTA.factBatchSummary.BatchID = @parmBatchID
        AND OLTA.factBatchSummary.DepositDateKey = @DepositDate
        AND OLTA.OLCustomers.OLCustomerID = @parmCustomerID;
END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

