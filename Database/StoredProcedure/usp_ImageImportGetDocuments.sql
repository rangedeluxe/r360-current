--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_ImageImportGetDocuments
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('[OLTA].[usp_ImageImportGetDocuments]') IS NOT NULL
	DROP PROCEDURE [OLTA].[usp_ImageImportGetDocuments]
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_ImageImportGetDocuments]
	@parmBankID			int,
	@parmLockboxID		int,
	@parmBatchID		int,
	@parmProcessingDate datetime
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/1/2009
*
* Purpose: Get Document Information for a given global batch id
*
* Modification History
* 05/1/2009 CR 25817 WJS	Created
* 11/13/2009 CR 28204 MEH	Added ProcessingDate and DepositDate
* 11/09/2010 CR 31178 JPB	Retrieve data from OLTA schema.
* 12/03/2010 CR 32012 JPB	Updated to use the dimDocumentTypesView
* 02/12/2011 CR 32773 WJS   Removed inner join on lockbox key. Removed inner join for document types
* 03/11/2011 CR 33282 WJS   Added depositDate Key
*					  JPB	Changes to improve overall performance.
******************************************************************************/
SET NOCOUNT ON 

DECLARE @DepositDateKey 	INT,
		@ProcessingDateKey 	INT,
		@BankKey 			INT,
		@LockboxKey 		INT

BEGIN TRY

	SET @ProcessingDateKey = CAST(CONVERT(varchar,@parmProcessingDate,112) as int) 

	EXEC [OLTA].[usp_GetDepositDateKey_BySiteID] 
				@parmBankID,
				@parmLockboxID,
				@parmBatchID,
				@ProcessingDateKey,
				@BankKey OUTPUT,
				@LockboxKey OUTPUT,
				@DepositDateKey OUTPUT
								
	SELECT	CONVERT(VARCHAR,CAST(CONVERT(VARCHAR,OLTA.factDocuments.ProcessingDateKey,101) AS DATETIME),101) AS ProcessingDate,
			CONVERT(VARCHAR,CAST(CONVERT(VARCHAR,OLTA.factDocuments.DepositDateKey,101) AS DATETIME),101) AS DepositDate,
			OLTA.factDocuments.GlobalBatchID, 
			OLTA.factDocuments.TransactionID,
			OLTA.factDocuments.BatchSequence,
			OLTA.factDocuments.DocumentTypeKey
	FROM	OLTA.factDocuments
	WHERE	OLTA.factDocuments.DepositDateKey = @DepositDateKey
			AND OLTA.factDocuments.ProcessingDateKey = @ProcessingDateKey
			AND OLTA.factDocuments.BankKey = @BankKey
			AND OLTA.factDocuments.LockboxKey = @LockboxKey  
			AND OLTA.factDocuments.BatchID = @parmBatchID  
	ORDER BY OLTA.factDocuments.TransactionID, 
			OLTA.factDocuments.BatchSequence

END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
