--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetBatchSummary
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Research_GetBatchSummary') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Research_GetBatchSummary
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetBatchSummary] 
	@parmSiteBankID int,
	@parmSiteLockboxID int,
	@parmDepositDate DateTime,
	@parmMinDepositStatus int,
	@parmDisplayScannedCheck bit
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28691 JMC	Created
* 07/12/2010 CR 29983 JMC	Added DepositDDA to returned dataset.
* 01/19/2010 CR 32255 JPB	Added Batch Source values to result set.
* 03/15/2012 CR 49157 JNE	Added BatchSiteCode to result set.
* 06/21/2012 CR 53616 JNE	Added BatchCueID to result set.
* 07/20/2012 CR 54168 JNE	Added BatchNumber to result set.
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

SELECT
    dimLockboxes.SiteBankID          AS BankID,
    dimLockboxes.SiteCustomerID      AS CustomerID,
    dimLockboxes.SiteLockboxID       AS LockboxID,
    factBatchSummary.BatchID,
    dimDepositDates.CalendarDate     AS DepositDate,
    factBatchSummary.CheckTotal,
    factBatchSummary.TransactionCount,
    factBatchSummary.CheckCount, 
	CASE
		WHEN @parmDisplayScannedCheck > 0 THEN
			factBatchSummary.DocumentCount
		ELSE
			factBatchSummary.DocumentCount - factBatchSummary.ScannedCheckCount
	END AS DocumentCount,
	factBatchSummary.DepositStatusDisplayName    AS DepositStatusDisplay,
	factBatchSummary.DepositDDA,
	dimBatchSources.ShortName AS BatchSourceShortName,
	dimBatchSources.LongName AS BatchSourceLongName,
	factBatchSummary.BatchSiteCode,
	factBatchSummary.BatchCueID,
	factBatchSummary.BatchNumber
FROM factBatchSummary
     INNER JOIN dimLockboxes ON
         factBatchSummary.LockboxKey = dimLockboxes.LockboxKey
     INNER JOIN dimDates AS dimDepositDates ON
         factBatchSummary.DepositDateKey = dimDepositDates.DateKey
	INNER JOIN dimBatchSources ON
		factBatchSummary.BatchSourceKey = dimBatchSources.BatchSourceKey
WHERE dimLockboxes.SiteBankID = @parmSiteBankID
     AND dimLockboxes.SiteLockboxID = @parmSiteLockboxID
        AND factBatchSummary.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
        AND factBatchSummary.DepositStatus >= @parmMinDepositStatus
ORDER BY 
    factBatchSummary.BatchID ASC


END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

