--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetHOAPMADetailHeader
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetHOAPMADetailHeader') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetHOAPMADetailHeader
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetHOAPMADetailHeader] 
    @parmCustomerID int,
    @parmLockboxID int,
    @parmDepositDateKey int,
    @parmUserID int,
    @parmHOA_Number VARCHAR(80)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 02/09/2012
*
* Purpose: Query HOA Report Summary Detail Header
*
* Modification History
* 02/09/2012 CR 50228 JNE	Created
* 04/27/2012 CR 52383 JNE	Added Account Number & BatchSequence when connecting final temp tables.
* 07/09/2012 CR 54032 JNE	Limit superuser to OLCustomerID assigned
* 09/17/2012 CR 55783 JNE	Remove BatchSequence - prevent duplicate header information.
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY
	DECLARE @IsSuperUser bit

	SELECT	@IsSuperUser = SuperUser 
	FROM	OLTA.Users 
	WHERE	OLTA.Users.UserID = @parmUserID
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp

 	--Create a temp table to hold dimlockbox info
	CREATE TABLE #tmpHOALockboxTemp
	(
		CustomerKey INT,
		LockboxKey INT,
		SiteCustomerID INT,
		Customer_Name VARCHAR(20),
		Lockbox_Number INT,
		Lockbox_Name VARCHAR(40)
	)

	--Create a temp table to hold data entry information for the records 
	CREATE TABLE #tmpHOADataEntryTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		BatchID INT,
		TransactionID INT,
		AccountNumber VARCHAR(80),
		TableName VARCHAR(36),
		FldName VARCHAR(32),
		DataEntryValue VARCHAR(256)
	)

	--Create a temp table to hold sum information for the records 
	CREATE TABLE #tmpHOAMainTemp
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		AccountNumber VARCHAR(80),
		Customer_Name VARCHAR(20),
		Lockbox_Name VARCHAR(40),
		Lockbox_Number INT
	)
	
		
	IF @IsSuperUser > 0
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.Users ON OLTA.Users.OLCustomerID = OLTA.OLLockboxes.OLCustomerID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.Users.UserID = @parmUserID	
	END
	ELSE
	BEGIN
		INSERT INTO #tmpHOALockboxTemp(CustomerKey,LockboxKey,SiteCustomerID,Customer_Name,Lockbox_Number,Lockbox_Name)
		SELECT DISTINCT
			OLTA.dimCustomers.CustomerKey,
			OLTA.dimLockboxes.LockboxKey,
			OLTA.dimLockboxes.SiteCustomerID,
			OLTA.dimCustomers.Name as Customer_Name,
			OLTA.dimLockboxes.SiteLockboxID as Lockbox_Number,
			OLTA.dimLockboxes.LongName as Lockbox_Name
		FROM OLTA.dimLockboxes
			INNER JOIN OLTA.dimCustomers ON OLTA.dimCustomers.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.dimCustomers.SiteCustomerID = OLTA.dimLockboxes.SiteCustomerID
			INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.SiteBankID = OLTA.dimLockboxes.SiteBankID AND OLTA.OLLockboxes.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID
			INNER JOIN OLTA.OLLockboxUsers ON OLTA.OLLockboxUsers.OLLockboxID = OLTA.OLLockboxes.OLLockboxID
		WHERE OLTA.dimLockboxes.SiteCustomerID = @parmCustomerID
			AND OLTA.dimLockboxes.SiteLockboxID = @parmLockboxID
			AND OLTA.dimLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.IsActive = 1
			AND OLTA.OLLockboxes.HOA = 1
			AND OLTA.OLLockboxUsers.UserID = @parmUserID
	END
	
	INSERT INTO #tmpHOAMainTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,AccountNumber,Customer_Name,Lockbox_Number,Lockbox_Name)
	SELECT DISTINCT OLTA.factStubs.BankKey,
		OLTA.factStubs.CustomerKey,
		OLTA.factStubs.LockboxKey,
		OLTA.factStubs.ProcessingDateKey,
		OLTA.factStubs.DepositDateKey,
		OLTA.factStubs.AccountNumber,
		#tmpHOALockboxTemp.Customer_Name,
		#tmpHOALockboxTemp.Lockbox_Number,
		#tmpHOALockboxTemp.Lockbox_Name 
	FROM OLTA.factStubs
		INNER JOIN #tmpHOALockboxTemp ON #tmpHOALockboxTemp.CustomerKey = OLTA.factStubs.CustomerKey AND #tmpHOALockboxTemp.LockboxKey = OLTA.factStubs.LockboxKey
	WHERE OLTA.factStubs.DepositDateKey = @parmDepositDateKey
		AND OLTA.factStubs.AccountNumber = @parmHOA_Number
	
	INSERT INTO #tmpHOADataEntryTemp(BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey, BatchID, TransactionID, AccountNumber, TableName,FldName,DataEntryValue)
	SELECT DISTINCT OLTA.factDataEntryDetails.BankKey,
		OLTA.factDataEntryDetails.CustomerKey,
		OLTA.factDataEntryDetails.LockboxKey,
		OLTA.factDataEntryDetails.ProcessingDateKey,
		OLTA.factDataEntryDetails.DepositDateKey,
		OLTA.factDataEntryDetails.BatchID,
		OLTA.factDataEntryDetails.TransactionID,
		"",
		OLTA.dimDataEntryColumns.TableName, 
		OLTA.dimDataEntryColumns.FldName, 
		OLTA.factDataEntryDetails.DataEntryValue 
	FROM OLTA.factDataEntryDetails
		INNER JOIN #tmpHOALockboxTemp ON OLTA.factDataEntryDetails.LockboxKey = #tmpHOALockboxTemp.LockboxKey
		INNER JOIN OLTA.dimDataEntryColumns ON OLTA.factDataEntryDetails.DataEntryColumnKey = OLTA.dimDataEntryColumns.DataEntryColumnKey
	WHERE OLTA.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
		AND OLTA.dimDataEntryColumns.TableName = 'StubsDataEntry'
		AND (OLTA.dimDataEntryColumns.FldName = 'hoa_name' OR OLTA.dimDataEntryColumns.FldName = 'hoa_ddano')


	UPDATE #tmpHOADataEntryTemp set #tmpHOADataEntryTemp.AccountNumber = factStubs.AccountNumber 
	FROM OLTA.factStubs
	WHERE #tmpHOADataEntryTemp.BankKey = factStubs.BankKey
		AND #tmpHOADataEntryTemp.CustomerKey = factStubs.CustomerKey
		AND #tmpHOADataEntryTemp.LockboxKey = factStubs.LockboxKey
		AND #tmpHOADataEntryTemp.ProcessingDateKey = factStubs.ProcessingDateKey
		AND #tmpHOADataEntryTemp.DepositDateKey = factStubs.DepositDateKey
		AND #tmpHOADataEntryTemp.BatchID = factStubs.BatchID
		AND #tmpHOADataEntryTemp.TransactionID = factStubs.TransactionID
		
	SELECT #tmpHOAMainTemp.Customer_Name,
		#tmpHOAMainTemp.Lockbox_Name,
		#tmpHOAMainTemp.Lockbox_Number,
		#tmpHOAMainTemp.AccountNumber,
		HOA_NAME.DataEntryValue AS HOA_NAME,
		HOA_DDANO.DataEntryValue AS HOA_DDANO,
		#tmpHOAMainTemp.DepositDateKey,
		CONVERT(DATETIME, CAST(#tmpHOAMainTemp.DepositDateKey AS CHAR(12)), 112) AS DepositDate
	FROM #tmpHOAMainTemp
		LEFT JOIN #tmpHOADataEntryTemp AS HOA_NAME
			ON HOA_NAME.BankKey = #tmpHOAMainTemp.BankKey
				AND HOA_NAME.CustomerKey = #tmpHOAMainTemp.CustomerKey
				AND HOA_NAME.LockboxKey = #tmpHOAMainTemp.LockboxKey
				AND HOA_NAME.ProcessingDateKey = #tmpHOAMainTemp.ProcessingDateKey
				AND HOA_NAME.DepositDateKey = #tmpHOAMainTemp.DepositDateKey
				AND HOA_NAME.AccountNumber = #tmpHOAMainTemp.AccountNumber
				AND HOA_NAME.FldName = 'hoa_name'
		LEFT JOIN #tmpHOADataEntryTemp AS HOA_DDANO
			ON HOA_DDANO.BankKey = #tmpHOAMainTemp.BankKey
				AND HOA_DDANO.CustomerKey = #tmpHOAMainTemp.CustomerKey
				AND HOA_DDANO.LockboxKey = #tmpHOAMainTemp.LockboxKey
				AND HOA_DDANO.ProcessingDateKey = #tmpHOAMainTemp.ProcessingDateKey
				AND HOA_DDANO.DepositDateKey = #tmpHOAMainTemp.DepositDateKey
				AND HOA_DDANO.AccountNumber = #tmpHOAMainTemp.AccountNumber
				AND HOA_DDANO.FldName = 'hoa_ddano'
	ORDER BY #tmpHOAMainTemp.AccountNumber
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp
END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOALockboxTemp')) 
		DROP TABLE #tmpHOALockboxTemp
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOADataEntryTemp')) 
		DROP TABLE #tmpHOADataEntryTemp
		
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpHOAMainTemp')) 
		DROP TABLE #tmpHOAMainTemp

	EXEC OLTA.usp_wfsRethrowException
END CATCH