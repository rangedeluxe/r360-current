--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSCheckDelete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_IMSCheckDelete') IS NOT NULL
       DROP PROCEDURE OLTA.usp_IMSCheckDelete
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_IMSCheckDelete
		@parmBankKey			int,
		@parmCustomerKey		int,
		@parmLockboxKey			int, 
		@parmProcessingDateKey	int,
		@parmDepositDateKey		int,
		@parmBatchID			int,
		@parmSiteBankID			int,
		@parmSiteLockboxID		int
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/01/2009
*
* Purpose: Insert a message into the MSMQ queue to alert the IMS system that
*	documents need to be deleted.
*
* Modification History
* 07/01/2009 CR 25817 JPB	Created
* 01/02/2009 CR 28573 JPB	Performance improvements:
*								Keys passed in
* 10/06/2010 CR 31375 JPB	Changes to support IMSInterfaceQueue table changes.
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @bLocalTransaction	BIT,
		@ErrorMessage		NVARCHAR(4000),
		@ErrorSeverity		INT,
		@ErrorState			INT,
		@ErrorLine			INT,
		@xmlData 			XML

BEGIN TRY

	SET @bLocalTransaction = 0
	DECLARE @Batches TABLE
	(
		BankKey INT,
		CustomerKey INT,
		LockboxKey INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		BatchID INT
	)
	
	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1
	END

	INSERT INTO @Batches (BankKey,CustomerKey,LockboxKey,ProcessingDateKey,DepositDateKey,BatchID) VALUES 
		(@parmBankKey,@parmCustomerKey,@parmLockboxKey,@parmProcessingDateKey,@parmDepositDateKey,@parmBatchID)

	SET @xmlData = 
		(
			SELECT DISTINCT IInfo.att.value('@ExternalDocumentID','int')
			FROM	(
						SELECT	ImageInfoXML as ImageInfo
						FROM	OLTA.factChecks
								INNER JOIN @Batches Batch ON OLTA.factChecks.BankKey = Batch.BankKey
															AND OLTA.factChecks.CustomerKey = Batch.CustomerKey
															AND OLTA.factChecks.LockboxKey = Batch.LockboxKey
															AND OLTA.factChecks.ProcessingDateKey = Batch.ProcessingDateKey
															AND OLTA.factChecks.DepositDateKey = Batch.DepositDateKey
															AND OLTA.factChecks.BatchID = Batch.BatchID
					) Info
			CROSS APPLY ImageInfo.nodes('/Images/Image') AS IInfo(att)
			FOR XML RAW ('Image'), TYPE, ROOT('Images'), ELEMENTS
		)
		
	IF @xmlData IS NOT NULL
	BEGIN
		INSERT INTO OLTA.IMSInterfaceQueue (QueueType,QueueData,QueueDataStatus,QueueImageStatus,QueueStatus,SiteBankID,SiteLockboxID,BatchID,ProcessingDateKey) 
		SELECT  0,
				@xmlData,
				1,
				1,
				10,
				@parmSiteBankID,
				@parmSiteLockboxID,
				@parmBatchID,
				@parmProcessingDateKey
	END
	IF @bLocalTransaction = 1 COMMIT TRANSACTION
	
END TRY
BEGIN CATCH
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorLine = ERROR_LINE()
	SET @ErrorMessage = 'usp_IMSCheckDelete (Line: %d)-> ' + @ErrorMessage
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
END CATCH
