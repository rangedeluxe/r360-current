--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_ImageImportGetChecks
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('[OLTA].[usp_ImageImportGetChecks]') IS NOT NULL
	DROP PROCEDURE [OLTA].usp_ImageImportGetChecks
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_ImageImportGetChecks]
	@parmBankID			int,
	@parmLockboxID		int,
	@parmBatchID		int,
	@parmProcessingDate datetime
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/1/2009
*
* Purpose: Get Check Information for a processing date, batchId, bankId, lockboxId
*
* Modification History
* 05/01/2009 CR 25817 WJS	Created
* 10/09/2009 CR 27875 WJS	Change format
* 11/12/2009 CR 28180 WJS	Support for cross join IMSDocumentType
* 11/13/2009 CR 28203 MEH	Add ProcessingDate to the returned columns
* 11/17/2009 CR 28203 WJS	Add BankId,LockboxId,BatchId to the returned columns
* 11/09/2010 CR 31176 JPB	Retrieve data from OLTA schema.
* 12/03/2010 CR 31978 JPB	Updated to use dimDocumentTypesView.
* 02/14/2011 CR 32774 WJS   Remove inner join for lockbox
* 03/11/2011 CR 33281 WJS/JPB   Added depositDate Key, Calling by siteId not keys
******************************************************************************/
SET NOCOUNT ON 

DECLARE @DepositDateKey 	INT,
		@ProcessingDateKey 	INT,
		@BankKey 			INT,
		@LockboxKey 		INT
		
BEGIN TRY

	SET @ProcessingDateKey = CAST(CONVERT(varchar,@parmProcessingDate,112) as int) 

	EXEC [OLTA].[usp_GetDepositDateKey_BySiteID] 
				@parmBankID,
				@parmLockboxID,
				@parmBatchID,
				@ProcessingDateKey,
				@BankKey OUTPUT,
				@LockboxKey OUTPUT,
				@DepositDateKey OUTPUT

	SELECT	CONVERT(VARCHAR,CAST(CONVERT(VARCHAR,OLTA.factChecks.ProcessingDateKey,101) AS DATETIME),101) AS ProcessingDate,
			CONVERT(VARCHAR,CAST(CONVERT(VARCHAR,OLTA.factChecks.DepositDateKey,101) AS DATETIME),101) AS DepositDate,
			OLTA.factChecks.BatchID,
			OLTA.factChecks.GlobalCheckID,
			OLTA.factChecks.TransactionID,
			OLTA.factChecks.BatchSequence,
			OLTA.factChecks.CheckSequence,
			OLTA.dimRemitters.RoutingNumber AS RT,
			OLTA.factChecks.Serial,
			OLTA.dimRemitters.Account,
			OLTA.factChecks.TransactionCode,
			OLTA.factChecks.Amount,
			(SELECT IMSDocumentType FROM OLTA.dimDocumentTypesView WHERE OLTA.dimDocumentTypesView.FileDescriptor='C') AS IMSDocumentType
	FROM	OLTA.factChecks
			INNER JOIN OLTA.dimRemitters ON OLTA.factChecks.RemitterKey = OLTA.dimRemitters.RemitterKey
	WHERE	OLTA.factChecks.DepositDateKey = @DepositDateKey
			AND OLTA.factChecks.ProcessingDateKey = @ProcessingDateKey
			AND OLTA.factChecks.BankKey = @BankKey
			AND OLTA.factChecks.LockboxKey = @LockboxKey
			AND OLTA.factChecks.BatchID = @parmBatchID  
	ORDER BY OLTA.factChecks.TransactionID, 
			OLTA.factChecks.BatchSequence
	
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH
