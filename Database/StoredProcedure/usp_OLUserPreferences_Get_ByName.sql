--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_OLUserPreferences_Get_ByName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_OLUserPreferences_Get_ByName') IS NOT NULL
	DROP PROCEDURE OLTA.usp_OLUserPreferences_Get_ByName;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_OLUserPreferences_Get_ByName 
(
	@parmPreferenceName varchar(MAX)
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 12/12/2012
*
* Purpose: Query OLPreferences Descriptions for Preference By User ID
*
* Modification History
* WI 76549	12/12/2012	CRG		Created
*	Write stored procedure for cPreferencesDAL.GetOLUserPreferenceIDByName(string, out DataTable)
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT	OLTA.OLPreferences.OLPreferenceID
	FROM	OLTA.OLPreferences
	WHERE	OLTA.OLPreferences.PreferenceName = @parmPreferenceName;
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH

