--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetBatchTransactions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_Research_GetBatchTransactions') IS NOT NULL
       DROP PROCEDURE OLTA.usp_Research_GetBatchTransactions
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetBatchTransactions]
	@parmSiteBankID	int,				--0
	@parmSiteLockboxID int,				--1
	@parmDepositDate	DateTime,		--2
	@parmBatchID	int,				--3
	@parmDisplayScannedCheck bit,		--4
	@parmMinDepositStatus int,			--5
	@parmStartRecord int,				--6
	@parmRecordsPerPage int = 25,		--7
	@parmTotalRecords int OUTPUT		--8
	
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28690 JMC	Created
* 06/02/2010 CR 29815 JMC	Added CustomerID to both conditions of the IF.
* 01/19/2010 CR 32261 JPB	Added Batch Source values to result set.
* 04/08/2011 CR 33788 JNE	Added Batch Source key and Processing Date to result set.
* 03/21/2012 CR 51397 JNE	Added Batch Site Code to result set.
* 06/21/2012 CR 53618 JNE	Added BatchCueID to result set.
* 07/20/2012 CR 54167 JNE	Added BatchNumber to result set.
* 11/23/2012 WI 56928 CEJ	Paginate records within usp_Research_GetBatchTransactions
* 01/16/2013 WI 85670 CRG   Implement OLTA.usp_Research_GetBatchTransactions
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	DECLARE @TotalChecks int,
			@RowIndex int,
			@RecordCount int;

	IF EXISTS (SELECT * FROM tempdb..sysobjects
				WHERE id=OBJECT_ID('tempdb..#PagedResults_Research_Checks'))
		DROP TABLE #PagedResults_Research_Checks

	CREATE TABLE #PagedResults_Research_Checks(
		RecID INT,
		TotalRecords INT,
		PICSDate CHAR(8),
		BatchSequence INT,
		Serial VARCHAR(30),		--*
		Amount MONEY,			--*
		BankID INT,
		CustomerID INT,
		LockboxID INT,
		BatchID INT,
		DepositDateKey INT,
		TransactionID INT,
		TxnSequence INT,
		CheckCount INT,
		DocumentCount INT,
		DepositStatusDisplay VARCHAR(80),
		BatchSourceKey TINYINT,
		RemitterKey INT,		--*
		ProcessingDateKey INT,
		BatchSiteCode INT,
		BatchCueID INT,
		BatchNumber INT 
		);

	With PageRecords AS (
		SELECT ROW_NUMBER() OVER (
					ORDER BY
						factTransactionSummary.ProcessingDateKey ASC, 
						factTransactionSummary.BatchID ASC, 
						factTransactionSummary.TxnSequence ASC,
						factChecks.SequenceWithinTransaction ASC, 
						factChecks.SequenceWithinTransaction ASC
				) AS RecID,
				Convert(Char(8), factTransactionSummary.ProcessingDateKey) AS PICSDate,
				ISNULL(factChecks.BatchSequence, -1) AS BatchSequence,
				RTRIM(factChecks.Serial) AS Serial,
				factChecks.Amount,
				factTransactionSummary.BankKey,
				factTransactionSummary.CustomerKey,
				factTransactionSummary.LockboxKey,
				dimLockboxes.SiteBankID          AS BankID, 
				dimLockboxes.SiteCustomerID      AS CustomerID, 
				dimLockboxes.SiteLockboxID       AS LockboxID, 
				factTransactionSummary.BatchID, 
				factTransactionSummary.DepositDateKey,
				factTransactionSummary.TransactionID,
				factTransactionSummary.TxnSequence, 
				factTransactionSummary.CheckCount,
				CASE 
					WHEN @parmDisplayScannedCheck > 0 THEN factTransactionSummary.DocumentCount
					ELSE factTransactionSummary.DocumentCount - factTransactionSummary.ScannedCheckCount 
				END AS DocumentCount,
				(SELECT StatusDisplayName 
				 FROM StatusTypes 
				 WHERE StatusType = 'Deposit' 
					AND StatusValue = factTransactionSummary.DepositStatus) AS DepositStatusDisplay,
				factTransactionSummary.BatchSourceKey,
				factChecks.RemitterKey,
				factTransactionSummary.ProcessingDateKey,
				factTransactionSummary.BatchSiteCode,
				factTransactionSummary.BatchCueID,
				factTransactionSummary.BatchNumber 
			FROM factTransactionSummary
				INNER JOIN dimLockboxes 
					ON factTransactionSummary.LockboxKey=dimLockboxes.LockboxKey
				LEFT JOIN factChecks
					ON factTransactionSummary.ProcessingDateKey=factChecks.ProcessingDateKey
						AND factTransactionSummary.BankKey = factChecks.BankKey
						AND factTransactionSummary.CustomerKey = factChecks.CustomerKey 
						AND factTransactionSummary.LockboxKey = factChecks.LockboxKey 
						AND factTransactionSummary.BatchID = factChecks.BatchID
						AND factTransactionSummary.TransactionID = factChecks.TransactionID
						AND factTransactionSummary.TxnSequence = factChecks.TxnSequence 
			WHERE dimLockboxes.SiteBankID = @parmSiteBankID 
				AND dimLockboxes.SiteLockboxID = @parmSiteLockboxID
				AND ((@parmBatchID <= 0) OR (factTransactionSummary.BatchID=@parmBatchID))
				AND factTransactionSummary.DepositDateKey=CAST(CONVERT(varchar, @parmDepositDate, 112) AS INT)
				AND factTransactionSummary.DepositStatus >= @parmMinDepositStatus
		)

	INSERT INTO #PagedResults_Research_Checks (
			RecID,
			TotalRecords,
			PICSDate,
			BatchSequence,
			Serial,
			Amount,
			BankID,
			CustomerID,
			LockboxID,
			BatchID,
			DepositDateKey,
			TransactionID,
			TxnSequence,
			CheckCount,
			DepositStatusDisplay,
			DocumentCount,
			BatchSourceKey,
			RemitterKey,
			ProcessingDateKey,
			BatchSiteCode,
			BatchCueID,
			BatchNumber)
		SELECT RecID,
				TotalRecords=(SELECT MAX(RecID) FROM PageRecords),
				PICSDate,
				BatchSequence,
				Serial,
				Amount,
				BankID,
				CustomerID,
				LockboxID,
				BatchID,
				DepositDateKey,
				TransactionID,
				TxnSequence,
				CheckCount,
				DepositStatusDisplay,
				DocumentCount,
				BatchSourceKey,
				RemitterKey,
				ProcessingDateKey,
				BatchSiteCode,
				BatchCueID,
				BatchNumber
			FROM PageRecords
			WHERE (@parmRecordsPerPage > 0
				AND (PageRecords.RecID BETWEEN @parmStartRecord AND @parmStartRecord + @parmRecordsPerPage-1))
				OR (@parmRecordsPerPage <=0 And (RecID >= @parmStartRecord))
			ORDER BY RecID ASC

	SELECT TOP(1) @parmTotalRecords = TotalRecords
		FROM #PagedResults_Research_Checks 

	SELECT @RecordCount = COUNT(*)
		FROM #PagedResults_Research_Checks

	SELECT RecID,
				TempTable.PICSDate,
				BatchSequence,
				Serial,
				Amount,
				RTRIM(dimRemitters.RoutingNumber) AS RT,
				RTRIM(dimRemitters.Account) AS Account,
				TempTable.BankID,
				TempTable.CustomerID,
				TempTable.LockboxID,
				TempTable.BatchID,
				dimDepositDates.CalendarDate     AS DepositDate,
				TempTable.TransactionID,
				TempTable.TxnSequence,
				TempTable.CheckCount,
				TempTable.DocumentCount,
				TempTable.DepositStatusDisplay,
				dimBatchSources.ShortName AS BatchSourceShortName,
				dimBatchSources.LongName AS BatchSourceLongName,
				TempTable.BatchSourceKey,
				dimProcessingDates.CalendarDate AS ProcessingDate,
				TempTable.BatchSiteCode,
				TempTable.BatchCueID,
				TempTable.BatchNumber
		FROM #PagedResults_Research_Checks TempTable
			INNER JOIN dimDates AS dimDepositDates 
				ON TempTable.DepositDateKey=dimDepositDates.DateKey
			INNER JOIN dimBatchSources 
				ON TempTable.BatchSourceKey=dimBatchSources.BatchSourceKey
			INNER JOIN dimDates AS dimProcessingDates 
				ON TempTable.ProcessingDateKey = dimProcessingDates.DateKey
			LEFT JOIN dimRemitters 
				ON TempTable.RemitterKey = dimRemitters.RemitterKey
		ORDER BY RecID ASC

	IF EXISTS (SELECT * FROM tempdb..sysobjects
				WHERE id=OBJECT_ID('tempdb..#PagedResults_Research_Checks'))
		DROP TABLE #PagedResults_Research_Checks
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException
END CATCH

