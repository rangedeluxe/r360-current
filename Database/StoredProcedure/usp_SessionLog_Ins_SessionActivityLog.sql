﻿--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName OLTA.usp_SessionLog_Ins_SessionActivityLog
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_SessionLog_Ins_SessionActivityLog') IS NOT NULL
    DROP PROCEDURE OLTA.usp_SessionLog_Ins_SessionActivityLog
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_SessionLog_Ins_SessionActivityLog 
	@parmActivityLogID	UniqueIdentifier,
	@parmSessionID		UniqueIdentifier,
	@parmModuleName		VarChar(MAX),
	@parmActivityCode	Int,
	@parmRowsAffected	Int = 0	Output
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 01/14/2013
*
* Purpose: 
*
* Modification History
* WI 85319 CRG 01/14/2013
*	Write store procedure to replace ExecuteSQL(SQLSessionLog.SQL_InsertSessionActivityLog(ActivityLogID, SessionID, ModuleName, ActivityCode), out RowsAffected)
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO OLTA.SessionActivityLog ( 
		OLTA.SessionActivityLog.SessionActivityLogID, 
        OLTA.SessionActivityLog.ActivityCode, 
        OLTA.SessionActivityLog.SessionID, 
        OLTA.SessionActivityLog.DeliveredToWeb, 
		OLTA.SessionActivityLog.ModuleName, 
		OLTA.SessionActivityLog.ItemCount, 
		OLTA.SessionActivityLog.DateTimeEntered, 
		OLTA.SessionActivityLog.BankID, 
		OLTA.SessionActivityLog.LockboxID
	) VALUES (
		@parmActivityLogID,
		@parmActivityCode,
		@parmSessionID, 
		1,
        @parmModuleName,
		1, 
        GetDate(), 
        -1, 
        -1
	);

	SELECT @parmRowsAffected = @@ROWCOUNT;
END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH
