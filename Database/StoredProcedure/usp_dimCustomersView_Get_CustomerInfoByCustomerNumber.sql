--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_dimCustomersView_Get_CustomerInfoByCustomerNumber
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_dimCustomersView_Get_CustomerInfoByCustomerNumber') IS NOT NULL
       DROP PROCEDURE OLTA.usp_dimCustomersView_Get_CustomerInfoByCustomerNumber
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].usp_dimCustomersView_Get_CustomerInfoByCustomerNumber 
    @parmCustomerNumber int
    
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/12/2012
*
* Purpose: Return Customer information using Customer Number for search
*
* Modification History
* 12/12/2012 WI 72222 TWE   Created by converting embedded SQL
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT
        OLTA.dimCustomersView.SiteCustomerID AS 'Customer Number',
        RTrim(OLTA.dimCustomersView.Name)    AS 'Customer Name',
        OLTA.dimCustomersView.SiteBankID     AS 'Bank Number',
        RTRim(OLTA.dimBanksView.BankName)    AS 'Bank Name'
    FROM
        OLTA.dimCustomersView 
        INNER JOIN OLTA.dimBanksView ON
            OLTA.dimCustomersView.SiteBankID = OLTA.dimBanksView.SiteBankID
    WHERE
        OLTA.dimCustomersView.SiteCustomerID = @parmCustomerNumber
    ORDER BY 
        OLTA.dimCustomersView.SiteCustomerID,
        OLTA.dimCustomersView.Name,
        OLTA.dimCustomersView.SiteBankID,
        OLTA.dimBanksView.BankName;

END TRY
BEGIN CATCH
    EXEC OLTA.usp_WfsRethrowException;
END CATCH

