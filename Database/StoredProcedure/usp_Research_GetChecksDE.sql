--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_Research_GetChecksDE
--WFSScriptProcessorStoredProcedureDrop
IF EXISTS	(
				SELECT	* 
				FROM	INFORMATION_SCHEMA.ROUTINES 
				WHERE	SPECIFIC_SCHEMA = N'OLTA' 
						AND SPECIFIC_NAME = N'usp_Research_GetChecksDE' 
			)
   DROP PROCEDURE OLTA.usp_Research_GetChecksDE
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_Research_GetChecksDE] 
		@parmSiteBankID int, 
		@parmSiteLockboxID int,
		@parmBatchID int,
		@parmDepositDate datetime,
		@parmTransactionID int

WITH RECOMPILE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 01/22/2010
*
* Purpose: Query Data Entry Detail fact table for transaction checks 
*			data entry information
*
* Modification History
* 01/22/2010 CR 28959 JNE	Created
* 06/16/2010 CR 29964 JMC  -Added fields to dataset: BankID, LockboxID, BatchID,
*                           ProcessingDateKey, DepositDateKey, TransactionID,
*                           BatchSequence, TableName
*                          -Added BatchSequence to ORDER BY clause
******************************************************************************/
	SET NOCOUNT ON 

	BEGIN TRY
	                
	SELECT	OLTA.dimLockboxes.SiteBankID	AS BankID,
			OLTA.dimLockboxes.SiteLockboxID	AS LockboxID,
			OLTA.factDataEntryDetails.BatchID,
			OLTA.factDataEntryDetails.ProcessingDateKey,
			OLTA.factDataEntryDetails.DepositDateKey,
			OLTA.factDataEntryDetails.TransactionID,
			OLTA.factDataEntryDetails.BatchSequence,
			OLTA.dimDataEntryColumns.TableName,
			OLTA.dimDataEntryColumns.FldName,
				RTrim(COALESCE(OLTA.dimDataEntryColumns.DisplayName,OLTA.dimDataEntryColumns.FldName)) AS FldTitle,
				CASE
					WHEN ((OLTA.dimDataEntryColumns.TableType = 0 OR OLTA.dimDataEntryColumns.TableType = 2) 
						AND RTRIM(OLTA.dimDataEntryColumns.FldName) = 'Amount') 
					THEN 7
					ELSE OLTA.dimDataEntryColumns.DataType
				END AS FldDataTypeEnum,
			OLTA.factDataEntryDetails.DataEntryValue AS FldValue,
			OLTA.dimDataEntryColumns.ScreenOrder
		FROM	OLTA.dimDataEntryColumns
				INNER JOIN OLTA.factDataEntryDetails ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey
				INNER JOIN OLTA.dimLockboxes ON OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey
		WHERE	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
				AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
			AND OLTA.factDataEntryDetails.BatchID = @parmBatchID
				AND OLTA.factDataEntryDetails.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)
				AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID
				AND OLTA.dimDataEntryColumns.TableType IN (1)
		ORDER BY 
				OLTA.factDataEntryDetails.BatchSequence,
			OLTA.dimDataEntryColumns.ScreenOrder

	END TRY
	BEGIN CATCH
		   EXEC OLTA.usp_WfsRethrowException
	END CATCH
