--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchDataExists
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_factBatchDataExists') IS NOT NULL
       DROP PROCEDURE OLTA.usp_factBatchDataExists
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE OLTA.usp_factBatchDataExists
		@parmSiteBankID			int,
		@parmSiteLockBoxID		int, 
		@parmProcessingDateKey		int,
		@parmBatchID			int,
		@parmItemFound			BIT = 0 OUT
WITH RECOMPILE
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/10/2011
*
* Purpose: Checks for the existance of a factBatchData.
*
* Modification History
* 03/10/2011 CR 33293 WJS	Created
* 04/14/2011 CR 33293 WJS Change to use Id instead of keys
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @BankKey int
DECLARE @LockboxKey int
DECLARE @DepositDateKey int

BEGIN TRY
	 EXEC OLTA.usp_GetDepositDateKey_BySiteID
            @parmSiteBankID = @parmSiteBankID,
            @parmSiteLockboxID = @parmSiteLockboxID,
            @parmBatchID = @parmBatchID,
            @parmProcessingDateKey = @parmProcessingDateKey,
            @parmBankKey = @BankKey OUTPUT,
            @parmLockboxKey = @LockboxKey OUTPUT,
            @parmDepositDateKey = @DepositDateKey OUTPUT

	IF EXISTS( SELECT 1	
				FROM	OLTA.factBatchData
					INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchData.LockboxKey = OLTA.dimLockboxes.LockboxKey
				WHERE	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID
						AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID
						AND OLTA.factBatchData.ProcessingDateKey = @parmProcessingDateKey
						AND OLTA.factBatchData.DepositDateKey = @DepositDateKey
						AND OLTA.factBatchData.BatchID = @parmBatchID)
		BEGIN
				SET @parmItemFound = 1
		END	
	ELSE
		BEGIN
				SET @parmItemFound = 0	
		END
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC OLTA.usp_WfsRethrowException 
END CATCH
