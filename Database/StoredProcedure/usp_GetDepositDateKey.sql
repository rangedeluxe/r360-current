--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStoredProcedureName usp_GetDepositDateKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('OLTA.usp_GetDepositDateKey') IS NOT NULL
       DROP PROCEDURE OLTA.usp_GetDepositDateKey
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [OLTA].[usp_GetDepositDateKey] 
(
	@parmBankKey INT,
	@parmLockboxKey INT,
	@parmBatchID INT,
	@parmProcessingDateKey INT,
	@parmDepositDateKey INT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/10/2011
*
* Purpose: Retrieve a deposit date key based on Bank Key, Lockbox Key,
*	Processing Date Key and Batch ID.
*
*
*	The basis of this SP is the fact that most batches have the same prcessing
*	date and deposit date.
*	Step 1: Look for the batch deposit date key based on the input params using
*		DepositDateKey = @parmProcessingDateKey
*	Step 2: If step 1 returns 0 rows, look for the deposit date key based on
*		the input params using DepositDateKey BETWEEN @parmProcessingDateKey - 1
*		AND @parmProcessingDateKey + 7 
*		(I.e. look between 1 before and 7 days after the given processing date.)
*	Step 3: If both Step 1 and Step 2 fail to return a row, give up and search
*		for the batch based on the processing date. Note that this is the slowest 
*		method since the paritions cannot be used to norrow down the data being
*		looked at.
*
* Modification History
* 03/10/2011 CR 33044 JPB	Created
******************************************************************************/
BEGIN
	SET @parmDepositDateKey = -1;

	/* Look for the deposit date = processing date */
	SELECT 	@parmDepositDateKey = DepositDateKey
	FROM	OLTA.factBatchSummary
	WHERE	DepositDateKey = @parmProcessingDateKey
			AND BankKey = @parmBankKey
			AND LockboxKey = @parmLockboxKey
			AND BatchID = @parmBatchID
			
	IF( @@ROWCOUNT = 0 )
	BEGIN /* Look for the deposit date between 1 day before and 7 days after the give processing date */
		SELECT 	@parmDepositDateKey = DepositDateKey
		FROM	OLTA.factBatchSummary
		WHERE	DepositDateKey BETWEEN @parmProcessingDateKey-1 AND @parmProcessingDateKey + 7
				AND BankKey = @parmBankKey
				AND LockboxKey = @parmLockboxKey
				AND BatchID = @parmBatchID

		IF( @@ROWCOUNT = 0 )
		BEGIN /* give up, just scan the table. Hopefully we never get here */
			SELECT 	@parmDepositDateKey = DepositDateKey
			FROM	OLTA.factBatchSummary
			WHERE	ProcessingDateKey = @parmProcessingDateKey
					AND BankKey = @parmBankKey
					AND LockboxKey = @parmLockboxKey
					AND BatchID = @parmBatchID
		END
	END 
END
