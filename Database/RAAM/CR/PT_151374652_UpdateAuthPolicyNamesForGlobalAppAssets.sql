--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 151374652
--WFSScriptProcessorPrint Convert ApplicationAssets to Global and fix Names
--WFSScriptProcessorCRBegin

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished, confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Date: 05/03/2017
*
* Purpose: Fix issues from original conversions
*
*
*
* Modification History
* 11/01/2017 MGE   Created
***************************************************************************************************************************************/
DECLARE @paramApplicationName NVARCHAR(128);
SET @paramApplicationName = 'Receivables360Online';

SET NOCOUNT ON;

-- Get Application ID
DECLARE @applicationID INT;
SELECT @applicationID = ApplicationID from Applications
	WHERE ApplicationName = @paramApplicationName;

DECLARE @msg VARCHAR(1000);
DECLARE @RowsAffected NVARCHAR(50);
IF (@applicationID IS NULL)
BEGIN
	SET @msg = @paramApplicationName + ' application was not found';
	RAISERROR(@msg, 16, 0);
	RETURN;
END;

BEGIN TRY

UPDATE Resources SET IsVirtual = 1
WHERE IsVirtual = 0 and ApplicationID = @applicationID AND ResourceTypeCode <> 'Workgroup';

-- Convert Policies to Global
UPDATE AuthorizationPolicies SET IsGlobal = 1
FROM AuthorizationPolicies 
INNER JOIN Resources ON Resources.ResourceID = AuthorizationPolicies.ResourceID
WHERE AuthorizationPolicies.ApplicationID = @applicationID AND IsGlobal = 0 AND ResourceTypeCode = 'ApplicationAsset';

ALTER INDEX IDX_AuthorizationPolicies_AuthorizationPolicyNameResourceIDIsGlobal ON AuthorizationPolicies DISABLE;

-- Correct PolicyName
UPDATE AuthorizationPolicies SET AuthorizationPolicyName = AuthorizationPolicyTemplatesGlobal.AuthorizationPolicyTemplateName
FROM AuthorizationPolicies 
INNER JOIN Resources ON Resources.ResourceID = AuthorizationPolicies.ResourceID
INNER JOIN AuthorizationPolicyTemplatesGlobal
		ON AuthorizationPolicies.IsGlobal = 1
		AND AuthorizationPolicies.PermissionID = AuthorizationPolicyTemplatesGlobal.PermissionID
		AND Resources.ResourceName = AuthorizationPolicyTemplatesGlobal.ResourceTemplateName
		AND AuthorizationPolicies.ApplicationID = AuthorizationPolicyTemplatesGlobal.ApplicationID
		AND SUBSTRING(AuthorizationPolicies.AuthorizationPolicyName, 1, LEN(AuthorizationPolicyTemplatesGlobal.AuthorizationPolicyTemplateName)) = AuthorizationPolicyTemplatesGlobal.AuthorizationPolicyTemplateName
WHERE AuthorizationPolicies.ApplicationID = @applicationID AND ResourceTypeCode = 'ApplicationAsset';

END TRY
BEGIN CATCH
	EXEC RAAMCommon.usp_WfsRethrowException;
END CATCH;

--WFSScriptProcessorCREnd
