--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 142343567
--WFSScriptProcessorPrint Convert ApplicationAssets to Global and Virtual
--WFSScriptProcessorCRBegin

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished, confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Date: 05/03/2017
*
* Purpose: Converts an application's "ApplicationAsset" Resources and assigned
*          AuthorizationPolicies to virtual resources and global policies.
*
*
* Modification History
* 05/03/2017 MR   Created
***************************************************************************************************************************************/
DECLARE @paramApplicationName NVARCHAR(128);
SET @paramApplicationName = 'Receivables360Online';

SET NOCOUNT ON;

-- Get Application ID
DECLARE @applicationID INT;
SELECT @applicationID = ApplicationID from Applications
	WHERE ApplicationName = @paramApplicationName;

DECLARE @msg VARCHAR(1000);
DECLARE @RowsAffected NVARCHAR(50);
IF (@applicationID IS NULL)
BEGIN
	SET @msg = @paramApplicationName + ' application was not found';
	RAISERROR(@msg, 16, 0);
	RETURN;
END;

BEGIN TRY

-- Convert Policies to Global
UPDATE AuthorizationPolicies
	SET AuthorizationPolicyName = AuthorizationPolicyTemplatesConcrete.AuthorizationPolicyTemplateName,
	IsGlobal = 1
	FROM AuthorizationPolicies
		LEFT JOIN Resources ON AuthorizationPolicies.ResourceID = Resources.ResourceID
		LEFT JOIN Entities ON Resources.EntityID = Entities.EntityID
		INNER JOIN AuthorizationPolicyTemplatesConcrete -- Need to drop the entity name off for Global policies
			-- Matching on just the policy name piece prevents issues with renamed entities
			ON SUBSTRING(AuthorizationPolicies.AuthorizationPolicyName, 1, LEN(AuthorizationPolicyTemplatesConcrete.AuthorizationPolicyTemplateName)) = AuthorizationPolicyTemplatesConcrete.AuthorizationPolicyTemplateName
	WHERE AuthorizationPolicies.ApplicationID = @applicationID
		AND Resources.ResourceTypeCode = 'ApplicationAsset'
		AND AuthorizationPolicies.IsGlobal = 0;

-- Convert Resources to Virtual
UPDATE Resources
	SET IsVirtual = 1
	WHERE Resources.ApplicationID = @applicationID
		AND Resources.ResourceTypeCode = 'ApplicationAsset'
		AND Resources.IsVirtual = 0;

END TRY
BEGIN CATCH
	EXEC RAAMCommon.usp_WfsRethrowException;
END CATCH;

--WFSScriptProcessorCREnd
