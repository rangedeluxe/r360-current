--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 151374652
--WFSScriptProcessorPrint Re-enable IDX_AuthorizationPolicies_AuthorizationPolicyNameResourceIDIsGlobal
--WFSScriptProcessorCRBegin

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished, confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Date: 05/03/2017
*
* Purpose: Fix issues from original conversions
*
*
*
* Modification History
* 11/01/2017 MGE   Created
***************************************************************************************************************************************/


BEGIN TRY

ALTER INDEX IDX_AuthorizationPolicies_AuthorizationPolicyNameResourceIDIsGlobal ON AuthorizationPolicies REBUILD;

END TRY
BEGIN CATCH
	EXEC RAAMCommon.usp_WfsRethrowException;
END CATCH;

--WFSScriptProcessorCREnd
