--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint PT 129162417
--WFSScriptProcessorPrint Remove obsolete (deprecated) permissions if necessary.
--WFSScriptProcessorCRBegin

IF( (SELECT COUNT(*) FROM dbo.AuthorizationPolicies WHERE AuthorizationPolicyDescription = 'Search - Manage Search Queries') > 0 )
BEGIN
	RAISERROR('Removing Search - Manage Search Queries from RolesAuthorizationPoliciesCalculated',10,1) WITH NOWAIT; 
	DELETE 
		dbo.RolesAuthorizationPoliciesCalculated
	FROM
		dbo.RolesAuthorizationPoliciesCalculated
		INNER JOIN dbo.AuthorizationPolicies ON dbo.AuthorizationPolicies.AuthorizationPolicyID = dbo.RolesAuthorizationPoliciesCalculated.AuthorizationPolicyID
	WHERE
		AuthorizationPolicyDescription = 'Search - Manage Search Queries';

	RAISERROR('Removing Search - Manage Search Queries from RolesAuthorizationPolicies',10,1) WITH NOWAIT; 
	DELETE 
		dbo.RolesAuthorizationPolicies
	FROM
		dbo.RolesAuthorizationPolicies
		INNER JOIN dbo.AuthorizationPolicies ON dbo.AuthorizationPolicies.AuthorizationPolicyID = dbo.RolesAuthorizationPolicies.AuthorizationPolicyID
	WHERE
		AuthorizationPolicyDescription = 'Search - Manage Search Queries';

	RAISERROR('Removing Search - Manage Search Queries from AuthorizationPolicies',10,1) WITH NOWAIT; 
	DELETE 
	FROM	
		dbo.AuthorizationPolicies 
	WHERE 
		AuthorizationPolicyDescription = 'Search - Manage Search Queries';
END

IF( (SELECT COUNT(*) FROM dbo.AuthorizationPolicies WHERE AuthorizationPolicyDescription = 'Search - View Manage Queries Page') > 0 )
BEGIN
	RAISERROR('Removing Search - View Manage Queries Page from RolesAuthorizationPoliciesCalculated',10,1) WITH NOWAIT; 
	DELETE 
		dbo.RolesAuthorizationPoliciesCalculated
	FROM
		dbo.RolesAuthorizationPoliciesCalculated
		INNER JOIN dbo.AuthorizationPolicies ON dbo.AuthorizationPolicies.AuthorizationPolicyID = dbo.RolesAuthorizationPoliciesCalculated.AuthorizationPolicyID
	WHERE
		AuthorizationPolicyDescription = 'Search - View Manage Queries Page';

	RAISERROR('Removing Search - View Manage Queries Page from RolesAuthorizationPolicies',10,1) WITH NOWAIT; 
	DELETE 
		dbo.RolesAuthorizationPolicies
	FROM
		dbo.RolesAuthorizationPolicies
		INNER JOIN dbo.AuthorizationPolicies ON dbo.AuthorizationPolicies.AuthorizationPolicyID = dbo.RolesAuthorizationPolicies.AuthorizationPolicyID
	WHERE
		AuthorizationPolicyDescription = 'Search - View Manage Queries Page';

	RAISERROR('Removing Search - View Manage Queries Page from AuthorizationPolicies',10,1) WITH NOWAIT; 
	DELETE 
	FROM	
		dbo.AuthorizationPolicies 
	WHERE 
		AuthorizationPolicyDescription = 'Search - View Manage Queries Page';
END
--WFSScriptProcessorCREnd
