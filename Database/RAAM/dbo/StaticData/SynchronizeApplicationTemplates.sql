--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2015
*
* Purpose: Resync application templates.
*
* Modification History
* 02/16/2015 WI 190380 JPB	Created
* 07/14/2015 WI 221931 MAA	Copied JPB file
******************************************************************************/

RAISERROR('Synchronize Receivables 360',10,1) WITH NOWAIT;
EXEC dbo.usp_Templating_SynchronizeApplication 'Receivables360Online';
