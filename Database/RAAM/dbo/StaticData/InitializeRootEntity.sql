--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/24/2014
*
* Purpose: Initialize root entity
*
* Modification History
* 07/24/2014 WI 157744 JPB	Created
******************************************************************************/
DECLARE @ApplicationID INT,
		@EntityID INT,
		@GroupID INT;

BEGIN TRY
	RAISERROR('Initialize root entity if necessary',10,1) WITH NOWAIT;

	SELECT
		@ApplicationID = ApplicationID
	FROM
		dbo.Applications
	WHERE
		ApplicationName = 'Receivables360Online';

	SELECT
		@EntityID = EntityID
	FROM
		dbo.Entities
	WHERE EntityName = 'WFS';

	IF NOT EXISTS(SELECT 1 FROM dbo.Resources WHERE EntityID = @EntityID AND ApplicationID = @ApplicationID)
	BEGIN
		RAISERROR('Provision Application',10,1) WITH NOWAIT;
		EXEC dbo.usp_Templating_ProvisionApplication @paramEntityID=@EntityID, @paramApplicationID=@ApplicationID;
	END
	ELSE
		RAISERROR('Root entity already initializd',10,1) WITH NOWAIT;

	SELECT @GroupID = GroupID FROM dbo.Groups WHERE GroupName = 'OnlineAdmin'
	IF( @GroupID IS NOT NULL )
	BEGIN
		RAISERROR('Initialize OnlineAdmin Group',10,1) WITH NOWAIT;
		EXEC dbo.usp_Resources_Insert
			@paramEntityID = @EntityID,
			@paramApplicationID = @ApplicationID,  
			@paramResourceName = 'OnlineAdmin',
			@paramResourceTypeCode ='Group',
			@paramExternalReferenceID = null;
	END

	SELECT @GroupID = GroupID FROM dbo.Groups WHERE GroupName = 'OnlineUser'
	IF( @GroupID IS NOT NULL )
	BEGIN
		RAISERROR('Initialize OnlineUser Group',10,1) WITH NOWAIT;
		EXEC dbo.usp_Resources_Insert
			@paramEntityID = @EntityID,
			@paramApplicationID = @ApplicationID,  
			@paramResourceName = 'OnlineUser',
			@paramResourceTypeCode ='Group',
			@paramExternalReferenceID = null;
	END

END TRY
BEGIN CATCH
	EXEC RAAMCommon.usp_WfsRethrowException;
END CATCH
