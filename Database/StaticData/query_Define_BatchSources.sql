--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStaticDataName Batch Source Types
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/11/2011
*
* Purpose: Populate the dimBatchSources table.
*
* Modification History
* 01/11/2011 CR 32311 JPB	Created.
* 03/01/2011 CR 33123 JPB	Removed IDENTITY property from BatchSourceKey, define
*							the BatchSourceKey now.
* 03/01/2011 CR 33125 JPB	Add 101, ICON.
******************************************************************************/
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchSources] WHERE  BatchSourceKey = 0  )
	INSERT INTO OLTA.dimBatchSources(BatchSourceKey,ShortName, LongName, LoadDate)
	SELECT 0,'Unknown', 'Unknown', GETDATE()
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchSources] WHERE  BatchSourceKey = 1  )
	INSERT INTO OLTA.dimBatchSources(BatchSourceKey,ShortName, LongName, LoadDate)
	SELECT 1,'integraPAY', 'integraPAY', GETDATE()
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchSources] WHERE  BatchSourceKey = 2  )
	INSERT INTO OLTA.dimBatchSources(BatchSourceKey,ShortName, LongName, LoadDate)
	SELECT 2,'WebDDL', 'WebDDL', GETDATE()
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchSources] WHERE  BatchSourceKey = 3  )
	INSERT INTO OLTA.dimBatchSources(BatchSourceKey,ShortName, LongName, LoadDate)
	SELECT 3,'ImageRPS', 'ImageRPS', GETDATE()
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchSources] WHERE  BatchSourceKey = 101  )
	INSERT INTO OLTA.dimBatchSources(BatchSourceKey,ShortName, LongName, LoadDate)
	SELECT 101,'ICON', 'ICON', GETDATE()
--WFSScriptProcessorStaticDataCreateEnd
