--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStaticDataName XBatch XSD
--WFSScriptPRocessorStaticDataCreateBegin
IF NOT EXISTS(SELECT 1 FROM OLTA.ImportSchemaTypes WHERE ImportSchemaType = 'OLTA XBatch')
	INSERT INTO OLTA.ImportSchemaTypes(ImportSchemaType) 
	SELECT 'OLTA XBatch'
--WFSScriptPRocessorStaticDataCreateEnd
--WFSScriptPRocessorStaticDataCreateBegin
IF NOT EXISTS(SELECT	1 
				FROM	OLTA.ImportSchemas 
						INNER JOIN OLTA.ImportSchemaTypes ON OLTA.ImportSchemas.ImportSchemaTypeID = OLTA.ImportSchemaTypes.ImportSchemaTypeID 
				WHERE	ImportSchemaType = 'OLTA XBatch')
BEGIN
	DECLARE @SchemaType varchar(20),
		   @XSD xml

	SELECT @SchemaType='OLTA XBatch', 
		   @XSD=
	'<?xml version="1.0"?>
	<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	  <xs:element name="Batches">
		<xs:complexType>
		  <xs:sequence>
			<xs:element minOccurs="0" name="Batch">
			  <xs:complexType>
				<xs:sequence>
				  <xs:element minOccurs="0" maxOccurs="unbounded" name="Transaction">
					<xs:complexType>
					  <xs:sequence>
						<xs:element minOccurs="0" name="Checks">
						  <xs:complexType>
							<xs:attribute name="GlobalBatchID" type="xs:int" use="optional" />
							<xs:attribute name="GlobalCheckID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionSequence" type="xs:int" use="optional" />
							<xs:attribute name="BatchSequence" type="xs:int" use="optional" />
							<xs:attribute name="CheckSequence" type="xs:int" use="optional" />                        
							<xs:attribute name="Serial" type="xs:string" use="optional" />
							<xs:attribute name="RT" type="xs:string" use="optional" />
							<xs:attribute name="Account" type="xs:string" use="optional" />
							<xs:attribute name="TransactionCode" type="xs:string" use="optional" />
							<xs:attribute name="RemitterName" type="xs:string" use="optional" />
							<xs:attribute name="Amount" type="xs:decimal" use="optional" />
						  </xs:complexType>
						</xs:element>
						<xs:element minOccurs="0" name="Documents">
						  <xs:complexType>
							<xs:attribute name="GlobalBatchID" type="xs:int" use="optional" />
							<xs:attribute name="GlobalDocumentID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionSequence" type="xs:int" use="optional" />
							<xs:attribute name="BatchSequence" type="xs:int" use="optional" />
							<xs:attribute name="DocumentSequence" type="xs:int" use="optional" />
							<xs:attribute name="FileDescriptor" type="xs:string" use="optional" />
						  </xs:complexType>
						</xs:element>                                
						<xs:element minOccurs="0" name="Stubs">
						  <xs:complexType>
							<xs:attribute name="GlobalBatchID" type="xs:int" use="optional" />
							<xs:attribute name="GlobalStubID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionSequence" type="xs:int" use="optional" />
							<xs:attribute name="BatchSequence" type="xs:int" use="optional" />
							<xs:attribute name="StubSequence" type="xs:int" use="optional" />
							<xs:attribute name="Amount" type="xs:decimal" use="optional" />
							<xs:attribute name="AccountNumber" type="xs:string" use="optional" />
							<xs:attribute name="IsOMRDetected" type="xs:byte" use="optional" />
							<xs:attribute name="@DocumentBatchSequence" type="xs:int" use="optional" />
						  </xs:complexType>
						</xs:element>
					  </xs:sequence>
					  <xs:attribute name="GlobalBatchID" type="xs:int" use="optional" />
					  <xs:attribute name="TransactionID" type="xs:int" use="optional" />
					  <xs:attribute name="Sequence" type="xs:int" use="optional" />
					</xs:complexType>
				  </xs:element>
				  <xs:element minOccurs="0" name="DataEntry">
					<xs:complexType>
					  <xs:sequence>
						<xs:element minOccurs="0" maxOccurs="unbounded" name="DataEntry">
						  <xs:complexType>
							<xs:attribute name="GlobalID" type="xs:int" use="optional" />
							<xs:attribute name="GlobalBatchID" type="xs:int" use="optional" />
							<xs:attribute name="TransactionID" type="xs:int" use="optional" />
							<xs:attribute name="BatchSequence" type="xs:int" use="optional" />
							<xs:attribute name="FldName" type="xs:string" use="optional" />
							<xs:attribute name="ReportTitle" type="xs:string" use="optional" />
							<xs:attribute name="FldDataTypeEnum" type="xs:short" use="optional" />
							<xs:attribute name="FldLength" type="xs:byte" use="optional" />
							<xs:attribute name="TableName" type="xs:string" use="optional" />
							<xs:attribute name="ScreenOrder" type="xs:int" use="optional" />
							<xs:attribute name="DataEntryValue" type="xs:string" use="optional" />
						  </xs:complexType>
						</xs:element>
					  </xs:sequence>
					</xs:complexType>
				  </xs:element>
				</xs:sequence>
				<xs:attribute name="ActionCode" type="xs:int" use="required" />
				<xs:attribute name="NotifyIMS" type="xs:bit" use="required" />
				<xs:attribute name="GlobalBatchID" type="xs:int" use="required" />
				<xs:attribute name="BankID" type="xs:int" use="required" />
				<xs:attribute name="LockboxID" type="xs:int" use="required" />
				<xs:attribute name="CustomerID" type="xs:int" use="required" />
				<xs:attribute name="SiteID" type="xs:int" use="required" />
				<xs:attribute name="BatchID" type="xs:int" use="required" />
				<xs:attribute name="DepositStatus" type="xs:int" use="required" />
				<xs:attribute name="DepositDDA" type="xs:string" use="required" />
				<xs:attribute name="DepositStatusDisplayName" type="xs:string" use="required" />
				<xs:attribute name="SystemType" type="xs:byte" use="required" />
				<xs:attribute name="ProcessingDate" type="xs:dateTime" use="required" />
				<xs:attribute name="DepositDate" type="xs:dateTime" use="required" />
			  </xs:complexType>
			</xs:element>
		  </xs:sequence>
		</xs:complexType>
	  </xs:element>
	</xs:schema>'
	EXEC OLTA.usp_CreateSchemaRevision @SchemaType, @XSD
END
--WFSScriptPRocessorStaticDataCreateEnd
