--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStaticDataName Permissions
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/29/2011
*
* Purpose: Populate the OLPreferences table.
*
* Modification History
* 08/29/2011 CR 31752 JNE	Created.
******************************************************************************/
--WFSScriptPRocessorStaticDataCreateBegin
IF NOT EXISTS(SELECT 1 FROM OLTA.Permissions WHERE PermissionName = 'Search Queries')
	INSERT INTO OLTA.Permissions (PermissionCategory,PermissionName,ScriptFile,PermissionDefault) VALUES('General', 'Search Queries', 'maintainquery.aspx', 1)
--WFSScriptPRocessorStaticDataCreateEnd
