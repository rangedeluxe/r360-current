--WFSScriptProcessorSchema OLTA
--WFSScriptProcessorStaticDataName Batch Data Setup Fields
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2011
*
* Purpose: Populate the dimBatchDataSetupFields table.
*
* Modification History
* 01/11/2011 CR 33234 JPB	Created.
******************************************************************************/
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 0  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	0,
			'Unknown', 
			'Unknown',
			1,
			0
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 1  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	1,
			'Unknown', 
			'Unknown',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 2  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	2,
			'Batch Number', 
			'Batch Number',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 3  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	3,
			'Client Name', 
			'Client Name',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 4  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	4,
			'Doc Grp ID', 
			'Document Group ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 5  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	5,
			'Batch Date', 
			'Batch Date',
			11,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 6  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	6,
			'System Date', 
			'System Date',
			11,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 7  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	7,
			'Receive Date', 
			'Receive Date',
			11,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 8  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	8,
			'Deposit Number', 
			'Deposit Number',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 9  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	9,
			'Deposit Time', 
			'Deposit Time',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 10  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	10,
			'Consolidation Date', 
			'Consolidation Date',
			11,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 11  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	11,
			'Consolidation Number', 
			'Consolidation Number',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 12  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	12,
			'P1 Station ID', 
			'Pass 1 Station ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 13  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	13,
			'P2 Station ID', 
			'Pass 2 Station ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 14  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	14,
			'P1 Operator ID', 
			'Pass 1 Operator ID',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 15  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	15,
			'P2 Operator ID', 
			'Pass 2 Operator ID',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 16  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	16,
			'Process Type', 
			'Process Type',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 17  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	17,
			'Location ID', 
			'Location ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 18  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	18,
			'Location Name', 
			'Location Name',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 19  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	19,
			'Abbrev Client Name', 
			'Abbrev Client Name',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 20  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	20,
			'Site ID', 
			'Site ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 21  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	21,
			'Work Type', 
			'Work Type',
			1,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 22  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	22,
			'Work Flow ID', 
			'Work Flow ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
--WFSScriptProcessorStaticDataCreateBegin
IF NOT EXISTS (SELECT * FROM [OLTA].[dimBatchDataSetupFields] WHERE BatchDataSetupFieldKey = 23  )
	INSERT INTO OLTA.dimBatchDataSetupFields(BatchDataSetupFieldKey, Keyword, Description, DataType, BatchSourceKey)
	SELECT 	23,
			'Bank/End Point ID', 
			'Bank/End Point ID',
			3,
			101
--WFSScriptProcessorStaticDataCreateEnd
