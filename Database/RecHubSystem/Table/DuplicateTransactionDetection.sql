--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName DuplicateTransactionDetection
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/30/2014
*
* Purpose: Maintain list of files that have been imported.
*
* Modification History
* 10/30/2014 WI 151122 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.DuplicateTransactionDetection
(
    DuplicateTransactionDetectionID	BIGINT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_DuplicateTransactionDetectionID PRIMARY KEY CLUSTERED,
	DataImportQueueID BIGINT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	TransactionHashChecker BIGINT NOT NULL,
	TransactionHash VARCHAR(40) NOT NULL,
	TransactionSignature VARCHAR(65) NOT NULL,
	ImportDate DATETIME NOT NULL
);
--WFSScriptProcessorTableProperties
