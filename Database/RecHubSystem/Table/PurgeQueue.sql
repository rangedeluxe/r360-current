--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PurgeQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/07/2013
*
* Purpose: Purging Queue records
*		   
*
* Modification History
* 05/07/2013 WI 101286 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PurgeQueue
(
	PurgeQueueID BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT [PK_PurgeQueue] PRIMARY KEY CLUSTERED,
	BankKey				INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	SiteBankID			INT	NOT NULL,
	SiteClientAccountID INT	NOT	NULL,
	DataDepositDateKey	INT NOT NULL,
	DataPurged			BIT NOT NULL
		CONSTRAINT DF_PurgeQueue_DataPurged DEFAULT 0
);
--WFSScriptProcessorTableProperties