--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PurgeSSISHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 10/18/2013
*
* Purpose: Purging SSIS History records
*	
* Description:
*
*
* ExceedThreshold
*	1 = Exceed the threshod
*	0 = Under the threshold
*
*
* Modification History
* 10/18/2013 WI 118006 PS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PurgeSSISHistory
(
	PurgeSSISHistoryID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_PurgeSSISHistory] PRIMARY KEY CLUSTERED,
	TableName VARCHAR(50) NOT NULL,
	IsDeletedCount INT NOT NULL,
	ExceedThreshold INT NOT NULL,
	PurgeDate DATETIME NULL
		CONSTRAINT [DF_PurgeSSISHistory_PurgeDate]  DEFAULT GETDATE()
);
--WFSScriptProcessorTableProperties
