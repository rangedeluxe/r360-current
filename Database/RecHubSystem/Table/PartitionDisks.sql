--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PartitionDisks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/03/2012
*
* Purpose: this table is used to store the disks/paths that the dynamic partitioning 
*	routine will use to distribute the filegroups and partitions across.  This table 
*	must have at least one row in it before the fact tables can be created.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JNE	Created
* 03/11/2013 WI 91457 JBS	Update table to 2.0 release. Change Schema Name.
*
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PartitionDisks
(
	PartitionManagerID int NOT NULL,
	DiskOrder int NOT NULL,       
	DiskPath varchar(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubSystem.PartitionDisks ADD 
	CONSTRAINT FK_PartitionDisks_PartitionManager_PartitionManagerID FOREIGN KEY(PartitionManagerID)
		REFERENCES RecHubSystem.PartitionManager(PartitionManagerID),
	CONSTRAINT PK_PartitionDisks PRIMARY KEY(PartitionManagerID, DiskOrder);

