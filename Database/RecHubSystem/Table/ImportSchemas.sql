--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName ImportSchemas
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table is used by the dynamic partitioning routine it will hold 
*	static data associated with the fact table partitioning.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/23/2012 CR 50518 JPB	Renamed ImportScheamRevison to XSDVersion and 
*							replaced SchemaDate with standard creation/modification
*							columns.
* 02/17/2013 WI 91448 JBS	Update table to 2.0 release. Change schema name.
*							Change Columns: CreatedBy, ModifiedBy to VARCHAR(128)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.ImportSchemas
(
	ImportSchemaTypeID INT NOT NULL,
	XSDVersion VARCHAR(12) NOT NULL,
	ImportSchema XML NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ImportSchemas_CreationDate DEFAULT(GETDATE()),
	CreatedBy varchar(128) NOT NULL 
		CONSTRAINT DF_ImportSchemas_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ImportSchemas_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_ImportSchemas_ModifiedBy DEFAULT(SUSER_SNAME())
	CONSTRAINT PK_ImportSchemas PRIMARY KEY(ImportSchemaTypeID,XSDVersion)              
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubSystem.ImportSchemas ADD        
       CONSTRAINT FK_ImportSchemas_ImportSchemaTypes FOREIGN KEY(ImportSchemaTypeID) REFERENCES RecHubSystem.ImportSchemaTypes (ImportSchemaTypeID)
