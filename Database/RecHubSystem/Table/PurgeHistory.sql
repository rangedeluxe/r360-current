--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PurgeHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/07/2013
*
* Purpose: Purging History records
*	
* Description:
*	PurgeHistoryType
*	1 = Data
*	2 = Image
*
* Modification History
* 05/07/2013 WI 96904 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PurgeHistory
(
	PurgeHistoryID BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT [PK_PurgeHistory] PRIMARY KEY CLUSTERED,
	PurgeHistoryType	TINYINT NOT NULL
		CONSTRAINT CK_PurgeHistory_PurgeHistoryType CHECK(PurgeHistoryType IN (1,2)),
	BankKey				INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	ProcessingDateKey	INT NOT NULL,
	PurgeDate			DATETIME,
	DepositDateKey		INT,
	BatchID				INT,
	BatchNumber			INT
);
--WFSScriptProcessorTableProperties