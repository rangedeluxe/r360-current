--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName CDSQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/15/2013
*
* Purpose: Queue items to be processed by the intergraPAY import SSIS packages.
*		   
*
* ActionCode:
*	1 - Insert/Update
*	2 - Delete

* QueueStatus:
*	10 - Ready to sent to integraPAY Consolidated Database
*	15 - Failed processing on integraPAY Consolidated Database - but can resend
*	20 - Sent to integraPAY Consolidated Database, waiting for a response
*	30 - Failed processing on integraPAY Consolidated Database - but cannot resend
*	99 - Successfully processed on integraPAY Consolidated Database
*
*
* QueueType:
*	10 - OLOrganizationID
*	20 - OLClientAccountID
*	70 - Document Type
*
* 
* Modification History
* 06/15/2013 WI 103288 JPB	Created
* 12/11/2013 WI 125182 JPB	Added SiteBankID and SiteClientAccountID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.CDSQueue
(
	CDSQueueID BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_CDSQueue PRIMARY KEY CLUSTERED,
	QueueType TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_QueueType CHECK(QueueType IN (10,20,70)), 
	QueueStatus TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99)), 
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_CDSQueue_ResponseStatus DEFAULT 255,
	ActionCode TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_ActionCode CHECK(ActionCode IN (1,2)),
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_CDSQueue_RetryCount DEFAULT 0,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_CDSQueue_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_CDSQueue_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_CDSQueue_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_CDSQueue_ModifiedBy DEFAULT SUSER_NAME(),
	OLOrganizationID UNIQUEIDENTIFIER NULL,
	OrganizationCode VARCHAR(20) NULL,
	OLClientAccountID UNIQUEIDENTIFIER NULL,
	SiteBankID INT NULL,
	SiteClientAccountID INT NULL,
	FileDescriptor VARCHAR(30) NULL,
	FileDescription VARCHAR(64) NULL,
	XMLResponseDocument XML NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubSystem.CDSQueue.IDX_OTISQueue_BankIDLockboxIDProcessingDateBatchIDQueueStatus
CREATE INDEX IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID ON RecHubSystem.CDSQueue 
(
	QueueType,
	QueueStatus,
	CDSQueueID
);
--WFSScriptProcessorIndex RecHub.CDSQueue.IDX_OTISQueue_QueueStatusModificationDateRetryCount
CREATE NONCLUSTERED INDEX IDX_CDSQueue_QueueStatusModificationDateRetryCount ON RecHubSystem.CDSQueue
(
	QueueStatus,
	ModificationDate,
	RetryCount
);
