--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName DataImportQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Queue work from external data sources via the Web Service.
*		   
* QueueType
*	0 - Client Setup
*	1 - Batch Data
*	2 - Notification
*
* QueueStatus
*	10 - Ready to process (default)
*	15 - Failed processing on OLTA - but can be reprocessed
*	20 - In process
*	30 - Failed processing on OLTA - but cannot be reprocessed
*	99 - Successfully imported, response ready
*	120 - Response send, waiting for receipt confirmation
*	145 - Client unknown error. "Dead" record.
*	150 - Receipt confirmation received, response process complete
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	30 = Failed processing on OLTA, data could not be processed
*	255 = Status not set
*
* Modification History
* 01/24/2012 CR 49666 JPB	Created
* 05/30/2012 CR 53201 JPB	Added Column ClientProcessCode, QueueType 2, 
*								QueueStatus 145, and ResponseStatus 30.
* 02/17/2013 WI 91424 JBS	Update Table to	2.0 release. Change Schema Name.
*							Changes columns: CreatedBy, ModifiedBy changed to VARCHAR(128)
* 02/05/2015 WI 177621 JPB	Added RetryCount.
* 05/29/2015 WI 216114 JBS	Add indexes for performance.
* 01/28/2016 WI 260558 JPB	Added AuditDateKey for DIT stat tracking.
* 02/27/2017 PT 127604193	MGE	Make the table partitioned.
* 08/09/2017 PT 149120903	MGE Update index-add include columns 
* 06/29/2018 PT 157790606	JPB Added JSONDataDocument, made the following columns nullable:
*									XSDVersion, XMLDataDocument
*								Moved columns around to keep the page organized
*								Added partition variable
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.DataImportQueue
(
	DataImportQueueID BIGINT NOT NULL IDENTITY(1,1),
	QueueType TINYINT NOT NULL
		CONSTRAINT CK_DataImportQueue_QueueType CHECK(QueueType IN (0,1,2)),
	QueueStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_QueueStatus DEFAULT 10
		CONSTRAINT CK_DataImportQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99,120,145,150)),
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_ResponseStatus DEFAULT 255
		CONSTRAINT CK_DataImportQueue_ResponseStatus CHECK(ResponseStatus IN (0,1,2,30,255)),
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_RetryCount DEFAULT 0,
	AuditDateKey INT NOT NULL,
	XSDVersion VARCHAR(12) NULL,
	ClientProcessCode VARCHAR(40) NOT NULL, 
	SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
	EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseTrackingID UNIQUEIDENTIFIER NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_ModifiedBy DEFAULT SUSER_NAME(),
	XMLDataDocument XML NULL,
	XMLResponseDocument XML NULL,
	JSONDataDocument NVARCHAR(MAX)
) $(OnDataImportPartition);

--WFSScriptProcessorForeignKey
ALTER TABLE RecHubSystem.DataImportQueue ADD 
	CONSTRAINT PK_DataImportQueue PRIMARY KEY NONCLUSTERED (DataImportQueueID, AuditDateKey)

--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_EntityTrackingID
CREATE CLUSTERED INDEX IDX_DataImportQueue_DataImportQueueID_AuditDateKey ON RecHubSystem.DataImportQueue (DataImportQueueID, AuditDateKey) $(OnDataImportPartition);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_EntityTrackingID
CREATE INDEX IDX_DataImportQueue_EntityTrackingID ON RecHubSystem.DataImportQueue (EntityTrackingID) $(OnDataImportPartition);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_SourceTrackingID
CREATE INDEX IDX_DataImportQueue_SourceTrackingID ON RecHubSystem.DataImportQueue (SourceTrackingID) $(OnDataImportPartition);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_ClientProcessCode
CREATE INDEX IDX_DataImportQueue_ClientProcessCode ON RecHubSystem.DataImportQueue (ClientProcessCode) $(OnDataImportPartition);

--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus
CREATE INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON RecHubSystem.DataImportQueue 
(
	QueueType, 
	ClientProcessCode,
	QueueStatus
) 
INCLUDE 
	(SourceTrackingID) $(OnDataImportPartition);

--WFSScriptProcessorIndex  RecHubSystem.DataImportQueue.IDX_DataImportQueue_QueueStatusResponseTrackingID 
CREATE INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON RecHubSystem.DataImportQueue 
(
	QueueStatus, 
	ResponseTrackingID
) 
INCLUDE
(
	DataImportQueueID,
	AuditDateKey,
	SourceTrackingID,
	EntityTrackingID
) $(OnDataImportPartition);