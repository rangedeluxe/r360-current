--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName SystemAuditMessages
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/10/2016
*
* Purpose: Hold system events that are being audited. 
*
* Modification History
* 06/10/2016 WI 283033 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.SystemAuditMessages
(
	SystemAuditMessageID BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_SystemAuditMessages PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_SystemAuditMessages_CreationDate DEFAULT(GETDATE()),
	AuditSource NVARCHAR(256) NOT NULL,
	AuditTypeCode NVARCHAR(64) NOT NULL,
	Context NVARCHAR(128) NOT NULL,
	AuditMessage NVARCHAR(2048) NOT NULL
);
--WFSScriptProcessorTableProperties
