--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PartitionEventTracking
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table will be populated by a DDL trigger set up to capture all 
*	Create or Alter statements run against a partition function or scheme.  This 
*	will be used primarily for auditing the dynamic partitioning routine.
*
* Modification History
* 03/09/2009 CR 25817 JNE	Created
* 02/18/2013 WI 91460 JBS	Update table to 2.0 release. Change Schema Name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PartitionEventTracking 
(
	EventID INT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_PartitionEventTracking PRIMARY KEY NONCLUSTERED (EventID),
	EventType VARCHAR(128) NOT NULL,
	ObjectType VARCHAR(128) NOT NULL,
	ObjectName VARCHAR(128) NOT NULL,
	CommandText VARCHAR(max) NOT NULL,
	PostDateTime DATETIME NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex OLTA.PartitionEventTracking.IDX_PartitionEventTracking_PostDateTime
CREATE CLUSTERED INDEX IDX_PartitionEventTracking_PostDateTime ON RecHubSystem.PartitionEventTracking (PostDateTime);
