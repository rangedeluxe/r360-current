--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTable SSISWorkingValues
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/07/2013
*
* Purpose: Stores values that are needed and updated by SSIS package. These
*	are not configuration values.
*
*
* Modification History
* 06/07/2013 WI 104755 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.SSISWorkingValues
(
	RecID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_SSISWorkingValues PRIMARY KEY CLUSTERED,
	PackageName VARCHAR(128) NOT NULL,
	VariableName VARCHAR(128) NOT NULL,
	VariableDateTime DATETIME NULL,
	VariableInt INT NULL,
	VariableString VARCHAR(256) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubSystem.SSISWorkingValues.IDX_SSISWorkingValues_PackageVariableName
CREATE UNIQUE INDEX IDX_SSISWorkingValues_PackageVariableName ON RecHubSystem.SSISWorkingValues
(
	PackageName,
	VariableName
);
