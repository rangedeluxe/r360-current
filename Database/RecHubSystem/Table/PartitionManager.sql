--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName PartitionManager
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table is used by the dynamic partitioning routine it will hold 
*	static data associated with the fact table partitioning.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/11/2013 WI 91467 JBS	Update table to 2.0 release. Change Schema Name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.PartitionManager
(
	PartitionManagerID INT NOT NULL
		CONSTRAINT PK_PartitionManager PRIMARY KEY(PartitionManagerID),
	PartitionIdentifier VARCHAR(20) NOT NULL,
	PartitionFunctionName VARCHAR(256) NOT NULL,
	PartitionSchemeName VARCHAR(256) NOT NULL,
	SizeMB INT NOT NULL
		CONSTRAINT DF_PartitionManager_Size DEFAULT (100),
	RangeSetting TINYINT NOT NULL
		CONSTRAINT CK_PartitionManager_RangeSetting CHECK(RangeSetting IN (0,1,2)), --0: No Partitions / 1: Weekly / 2: Monthly 
	LastDiskID INT NOT NULL
		CONSTRAINT DF_PartitionManager_LastDiskID DEFAULT(0),
	IsLocked BIT NOT NULL 
		CONSTRAINT DF_PartitionManager_IsLocked DEFAULT(0)
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubSystem.PartitionManager.IDX_PartitionManager_PartitionIdentifier
CREATE UNIQUE INDEX IDX_PartitionManager_PartitionIdentifier ON RecHubSystem.PartitionManager(PartitionIdentifier)
--WFSScriptProcessorTrigger trgUpdateRangeSetting
CREATE TRIGGER RecHubSystem.trgUpdateRangeSetting ON RecHubSystem.PartitionManager
AFTER UPDATE 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/17/2009
*
* Purpose: Prevents changing RangeSetting from or to 0 (No Partitioning).  If
*      No Partitioning is to be used, it needs to be set up this way at DB 
*      installation.  RangeSetting can only be changed from 1-2 and 2-1.
*
* Modification History
* 03/17/2009 JJR - Created
* 03/11/2013 WI 91467 JBS	Update table to 2.0 release. Change Schema Name. 
******************************************************************************/
BEGIN TRY
   --zero * anything = zero...
   IF UPDATE(RangeSetting)  AND (SELECT D.RangeSetting * I.RangeSetting FROM Deleted D INNER JOIN Inserted I ON I.PartitionManagerID=D.PartitionManagerID) =0
          RAISERROR(N'ERROR: RecHubSystem.PartitionManager RangeSetting cannot be changed to or from Zero(No Partitioning)',16,1)
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
