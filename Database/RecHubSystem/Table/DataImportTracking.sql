--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName DataImportTracking
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Marco Aguirre
* Date: 06/01/2018
*
* Purpose: Status of file being processed by the servce side importer
*	
*
* Modification History
* 06/28/2018 PT 158332383 MA	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.DataImportTracking
(
	SourceTrackingId UNIQUEIDENTIFIER NOT NULL
		CONSTRAINT PK_DataImportTracking PRIMARY KEY CLUSTERED,
	FileStatus int NOT NULL,
	CreationDate DATETIME2(7) NOT NULL
		CONSTRAINT DF_DataImportTracking_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportTracking_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME2(7) NULL
		CONSTRAINT DF_DataImportTracking_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NULL
		CONSTRAINT DF_DataImportTracking_ModifiedBy DEFAULT SUSER_NAME()
);
