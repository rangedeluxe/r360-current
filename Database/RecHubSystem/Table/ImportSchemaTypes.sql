--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTableName ImportSchemaTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: This table is used by the dynamic partitioning routine it will hold 
*	static data associated with the fact table partitioning.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/09/2012 CR 50105 JPB	Removed IDENTITY from ImportSchemaTypeID, changed
*							length of ImportSchemaType to 128, added standard 
*							creation/modification columns.
* 02/17/2013 WI 91452 JBS	Update table to	2.0 release. Change Schema Name.
*							Change columns: CreatedBy, Modifiedby to VARCHAR(128).
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.ImportSchemaTypes
(
	ImportSchemaTypeID INT NOT NULL 
		CONSTRAINT PK_ImportSchemaTypes PRIMARY KEY CLUSTERED,
	ImportSchemaType VARCHAR(128) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_ImportSchemaTypes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_ImportSchemaTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_ImportSchemaTypes_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_ImportSchemaTypes_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubSystem.ImportSchemaTypes.IDX_ImportSchemaTypes_ImportSchemaType
CREATE UNIQUE INDEX IDX_ImportSchemaTypes_ImportSchemaType ON RecHubSystem.ImportSchemaTypes(ImportSchemaType);
