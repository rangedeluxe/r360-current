--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorTable DocumentSequenceIMSMapping
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 05/26/2011
*
* Purpose: 
*		   
*
* Modification History
* 05/26/2011 CR 44985 JNE	Created
* 05/30/2011 CR 44985 WJS	Modifed drop some columns
* 02/17/2013 WI 91433 JBS	Update table to	2.0 release. Change Schema Name.
*
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.DocumentSequenceIMSMapping
(
	RecordID INT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_DocumentSequenceIMSMapping PRIMARY KEY NONCLUSTERED,
	BankID	INT	NOT NULL,
	ClientAccountID INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	BatchID INT NOT NULL,
	OldBatchSequence INT NOT NULL,
	NewBatchSequence INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubSystem.DocumentSequenceIMSMapping.IDX_DocumentSequenceIMSMapping_BankCLientAccountIDImmutableDateKeyBatchID
CREATE CLUSTERED INDEX IDX_DocumentSequenceIMSMapping_BankCLientAccountIDImmutableDateKeyBatchID ON RecHubSystem.DocumentSequenceIMSMapping 
(
	BankID, 
	ClientAccountID, 
	ImmutableDateKey, 
	BatchID
);


