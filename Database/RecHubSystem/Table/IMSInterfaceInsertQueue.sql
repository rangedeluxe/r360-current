--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName IMSInterfaceQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2009
*
* Purpose: This table will be populated by the XBatchImport stored procedure. 
*	The table will act as an interface point between the OLTA database and an
*	external Image Management System (IMS).
*
* Check Constraint definitions:
* QueueStatus
*	0: Not processed
*	10: Available for processing
*	20: In Process
*	30: Error
*	40: Image(s) not found for Batch
*	99: Complete
*	130: An error file was received from Hyland indicating a rejection
*	150: A response file was received from Hyland, and OLTA was updated appropriately. 
*
* Modification History
* 07/08/2009 CR 25817 JPB	Created
* 12/23/2009 CR 28537 JPB	Added cluster index 
*							IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType
* 10/05/2010 CR 31236 JPB	Alter table to meet new requirements
* 03/02/2011 CR 32827 JPB	Added status values 130 and 150.
* 05/13/2011 CR 34323 JPB	Added status values 40.
* 05/13/2011 CR 34330 JPB	Allow QueueData column to be NULL.
* 06/16/2011 CR 45225 JPB	Added SiteID to table.
* 11/02/2011 CR 47584 JPB	Added new index.
* 06/13/2012 CR	53405 JPB	Removed SiteID.
* 10/19/2012 CR 55481 WJS   Remove SourceProcessingDateKey, Added NumArchiveImages, NumofUnArchiveImages  and ClientProcesCode
* 02/17/2013 WI 91584 JBS	Update table to 2.0 release.  Change Schema Name. 
*							Break apart table to Insert and Delete Queue.  WI(91587) creates the IMSInterfaceDeleteQueue
*							Add Columns: SiteID, BankKey, OrganizationKey, DepositDateKey, ProcessingDateKey.
*							Rename Column: QueueID to IMSInterfaceInsertQueueID, SiteLockboxID to ClientAccountID,
*							ProcessingDateKey to ImmutableDateKey.
*							Change Column: CreatedBy and ModifiedBy to VARCHAR(128).
*							Remove Columns:  QueueType, NumArchiveImages, NumOfUnArchiveImages, ClientProcessCode.
*							Rename Constraints: All Constraints to match Schema name and column changes.
*							Renamed FK's to match Schema and column changes.
*							Changed Indexes: IDX_CreationDate_QueueType_QueueDataStatus TO 
*							IDX_IMSInterfaceInsertQueue_CreationDateQueueDataStatusONRecHubSystem.IMSInterfaceInsertQueue,
*							IDX_CreationDate_QueueType_QueueImageStatus  TO 
*							IDX_IMSInterfaceInsertQueue_CreationDateQueueImageStatusONRecHubSystem.IMSInterfaceInsertQueue,
*							IDX_SiteBankID_SiteLockboxID_ProcessingDateKey_BatchID   TO 
*							IDX_IMSInterfaceInsertQueue_SiteBankSiteClientAccountIDImmutableDateKeyBatchID,
*							CLUSTERED INDEX IDX_IMSInterfaceQueue_QueueStatus_QueueDataStatus_QueueType   TO 
*							CLUSTERED INDEX IDX_IMSInterfaceInsertQueue_QueueStatusQueueDataStatus,
*							IDX_IMSInterfaceQueue_QueueStatusProcessingDateKeyCreationDateQueueTypeModificationDate  TO 
*							IDX_IMSInterfaceInsertQueue_QueueStatusImmutableDateKeyCreationModificationDate
*							Remove Constraints: DF_IMSInterfaceQueue_NumArchiveImages, DF_IMSInterfaceQueue_NumOfUnArchiveImages,
*							DF_IMSInterfaceQueue_ClientProcessCode.
*							Remove Index: IDX_IMSInterfaceQueue_ClientProcessCode.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.IMSInterfaceInsertQueue 
(
	IMSInterfaceInsertQueueID BIGINT IDENTITY NOT NULL
		CONSTRAINT PK_IMSInterfaceInsertQueue PRIMARY KEY NONCLUSTERED (IMSInterfaceInsertQueueID),
	QueueStatus SMALLINT NOT NULL
		CONSTRAINT CK_IMSInterfaceInsertQueue_QueueStatus CHECK(QueueStatus IN (0,10,20,30,40,99,130,150)),
	QueueDataStatus BIT NOT NULL
		CONSTRAINT DF_IMSInterfaceInsertQueue_QueueDataStatus DEFAULT(0),
	QueueImageStatus BIT NOT NULL 
		CONSTRAINT DF_IMSInterfaceInsertQueue_QueueImageStatus DEFAULT(0),
	SiteID INT NOT NULL,
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_IMSInterfaceInsertQueue_RetryCount DEFAULT (0),
	SiteBankID INT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	ProcessingDateKey INT NOT NULL,
	BatchID INT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_IMSInterfaceInsertQueue_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_IMSInterfaceInsertQueue_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_IMSInterfaceInsertQueue_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_IMSInterfaceInsertQueue_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubSystem.IMSInterfaceInsertQueue ADD 
       CONSTRAINT FK_IMSInterfaceInsertQueue_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey);
       
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceInsertQueue.IDX_IMSInterfaceInsertQueue_CreationDateQueueDataStatus
CREATE INDEX IDX_IMSInterfaceInsertQueue_CreationDateQueueDataStatus ON RecHubSystem.IMSInterfaceInsertQueue 
(
	CreationDate,
	QueueDataStatus
);
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceInsertQueue.IDX_IMSInterfaceInsertQueue_CreationDateQueueImageStatus
CREATE INDEX IDX_IMSInterfaceInsertQueue_CreationDateQueueImageStatus ON RecHubSystem.IMSInterfaceInsertQueue 
(
	CreationDate,
	QueueImageStatus
);
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceInsertQueue.IDX_IMSInterfaceInsertQueue_SiteBankSiteClientAccountIDImmutableDateKeyBatchID
CREATE INDEX IDX_IMSInterfaceInsertQueue_SiteBankSiteClientAccountIDImmutableDateKeyBatchID ON RecHubSystem.IMSInterfaceInsertQueue 
(
	SiteBankID,
	SiteClientAccountID,
	ImmutableDateKey,
	BatchID
);

--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceInsertQueue.IDX_IMSInterfaceInsertQueue_QueueStatusQueueDataStatus
CREATE CLUSTERED INDEX IDX_IMSInterfaceInsertQueue_QueueStatusQueueDataStatus ON RecHubSystem.IMSInterfaceInsertQueue 
(
	QueueStatus ASC,
	QueueDataStatus ASC
);

--CR 47584
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceInsertQueue.IDX_IMSInterfaceInsertQueue_QueueStatusImmutableDateKeyCreationModificationDate
CREATE NONCLUSTERED INDEX IDX_IMSInterfaceInsertQueue_QueueStatusImmutableDateKeyCreationModificationDate ON RecHubSystem.IMSInterfaceInsertQueue 
(
	QueueStatus ASC,
	ImmutableDateKey ASC,
	CreationDate ASC,
	ModificationDate ASC
);