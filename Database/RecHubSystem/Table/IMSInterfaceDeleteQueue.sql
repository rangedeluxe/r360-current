--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName IMSInterfaceDeleteQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2009
*
* Purpose: This table will be populated by the XBatchImport stored procedure. 
*	The table will act as an interface point between the OLTA database and an
*	external Image Management System (IMS).
*
* Check Constraint definitions:
* QueueStatus
*	0: Not processed
*	10: Available for processing
*	20: In Process
*	30: Error
*	40: Image(s) not found for Batch
*	99: Complete
*	130: An error file was received from Hyland indicating a rejection
*	150: A response file was received from Hyland, and OLTA was updated appropriately. 
*
* Modification History
* 02/17/2013 WI 91587 JBS	Create Table for 2.0 release. 
*
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubSystem.IMSInterfaceDeleteQueue 
(
	IMSInterfaceDeleteQueueID BIGINT IDENTITY NOT NULL
		CONSTRAINT PK_IMSInterfaceDeleteQueue PRIMARY KEY NONCLUSTERED (IMSInterfaceDeleteQueueID),
	QueueStatus SMALLINT NOT NULL
		CONSTRAINT CK_IMSInterfaceQueue_QueueStatus CHECK(QueueStatus IN (0,10,20,30,40,99,130,150)),
	RetryCount TINYINT NOT NULL 
		CONSTRAINT DF_IMSInterfaceDeleteQueue_RetryCount DEFAULT (0),
	ExternalDocumentID BIGINT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_IMSInterfaceDeleteQueue_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_IMSInterfaceDeleteQueue_ModificationDate DEFAULT(GETDATE())    
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceDeleteQueue.IDX_IMSInterfaceDeleteQueue_CreationDateQueueStatus
CREATE INDEX IDX_IMSInterfaceDeleteQueue_CreationDateQueueStatus ON RecHubSystem.IMSInterfaceDeleteQueue
(
	CreationDate ASC,
	QueueStatus ASC	 
);
--WFSScriptProcessorIndex RecHubSystem.IMSInterfaceDeleteQueue.IDX_IMSInterfaceDeleteQueue_ExternalDocumentID
CREATE INDEX IDX_IMSInterfaceDeleteQueue_ExternalDocumentID ON RecHubSystem.IMSInterfaceDeleteQueue
(
	ExternalDocumentID ASC
);