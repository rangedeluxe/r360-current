--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorSequenceName BatchNumber
--WFSScriptProcessorSequenceHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2014
*
* Purpose: Sequence number to be used as the BatchNumber for electronic batches. 
*		(I.E. ACH, Wires, etc.)
*		   
*
* Modification History
* 04/04/2014 WI 135303 JPB	Created. Renamed from ElectronicBatchNumber
******************************************************************************/
--WFSScriptProcessorSequenceHeaderEnd
--WFSScriptProcessorSequenceCreate
CREATE SEQUENCE RecHubSystem.BatchNumber 
AS
	INT
	START WITH 1
	MINVALUE 1
	MAXVALUE 999999999
	INCREMENT BY 1
	CYCLE
	CACHE 100;
--WFSScriptProcessorSequenceProperties