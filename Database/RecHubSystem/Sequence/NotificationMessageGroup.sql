--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorSequenceName NotificationMessageGroup
--WFSScriptProcessorSequenceHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/31/2015
*
* Purpose: Sequence number to be used as the NotificationMessageGroup in factNotification tables. 
*
* Modification History
* 03/31/2015 WI 198769 JBS	Created.  
******************************************************************************/
--WFSScriptProcessorSequenceHeaderEnd
--WFSScriptProcessorSequenceCreate
CREATE SEQUENCE RecHubSystem.NotificationMessageGroup 
AS
	BIGINT
	START WITH 1
	MINVALUE 1
	INCREMENT BY 1
	CYCLE
	CACHE 100;
--WFSScriptProcessorSequenceProperties