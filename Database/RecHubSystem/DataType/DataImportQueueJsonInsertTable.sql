--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorDataTypeName DataImportQueueJsonInsertTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE [RecHubSystem].[DataImportQueueJsonInsertTable] AS TABLE(
	[AuditDateKey] [int] NULL,
	[ClientProcessCode] [varchar](40) NULL,
	[SourceTrackingID] [uniqueidentifier] NULL,
	[BatchTrackingID] [uniqueidentifier] NULL,
	[JsonDataDocument] [nvarchar](max) NULL
)

