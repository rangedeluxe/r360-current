--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorDataTypeName DataImportQueueInsertTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubSystem.DataImportQueueInsertTable AS TABLE
(
	XSDVersion VARCHAR(12),
	ClientProcessCode VARCHAR(40),
	SourceTrackingID UNIQUEIDENTIFIER,
	BatchTrackingID UNIQUEIDENTIFIER,
	XMLDataDocument XML
);
