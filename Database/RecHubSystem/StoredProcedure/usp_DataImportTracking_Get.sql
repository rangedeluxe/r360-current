﻿--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataImportTracking_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportTracking_Get') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportTracking_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportTracking_Get
(
	@parmSourceTrackingId UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2018-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Marco Aguirre
* Date: 06/01/2018
*
* Purpose: Gets a single data import job 
*	
*
* Modification History
* 06/28/2018 PT 158332383 MA	Created
* 02/09/2019 R360-15391		JPB	Added BatchTrackingIDs
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT 	   
		FileStatus,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		COALESCE(
		(
			SELECT 
				EntityTrackingID AS '@BatchTrackingID' 
			FROM 
				RecHubSystem.DataImportQueue 
			WHERE 
				SourceTrackingID = @parmSourceTrackingId 
			FOR XML PATH('Batch'), TYPE
		),'<Batch />') AS BatchTrackingIds
	FROM
		RecHubSystem.DataImportTracking
	WHERE 
		SourceTrackingId = @parmSourceTrackingId;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
