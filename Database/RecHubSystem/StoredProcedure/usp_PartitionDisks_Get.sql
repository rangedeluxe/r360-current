--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionDisks_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionDisks_Get') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionDisks_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionDisks_Get 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/11/2009
*
* Purpose: Retrieve all rows from PartitionDisks table
*
* Modification History
* 08/10/2009 CR 27523 JNE	Created
* 09/18/2009 CR 27523 JNE	Instead of Select all, use column names
* 04/11/2013 WI 90710 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_GetPartitionDisks
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT	RecHubSystem.PartitionDisks.PartitionManagerID,
			RecHubSystem.PartitionDisks.DiskOrder,
			RecHubSystem.PartitionDisks.DiskPath
	FROM	RecHubSystem.PartitionDisks;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
