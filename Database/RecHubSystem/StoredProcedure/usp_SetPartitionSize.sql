IF OBJECT_ID('RecHubSystem.usp_SetPartitionSize') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_SetPartitionSize
GO

CREATE PROCEDURE RecHubSystem.usp_SetPartitionSize
(
@parmPartitionScheme	VARCHAR(128)
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2017-2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 08/22/2017
*
* Purpose: Sets the SizeMB column = to the average of the past 10 full partitions.
*    This will cause new partitions to be created equal to that size, reducing autogrow frequency.
*
* Modification History
* 08/22/2017 PT	149894451	MGE	Created
* 09/06/2017 PT 150714641	MGE Base calculation on space used instead of space allocated; simplify query
* 08/14/2019 CR R360-30019	MGE Prevent divide by zero in case where no partitions exist for last 10 weeks.
*********************************************************************************************************/

DECLARE @LastDateToAverage DATETIME,
		@CurrentDate DATETIME,
		@FirstPartitionToAverage INT,
		@LastPartitionToAverage INT,
		@PartitionDateKey1 INT,
		@PartitionDateKey2 INT,
		@FirstDateToAverage DATETIME,
		@AveragePartitionSizeMB FLOAT,
		@PSNAME VARCHAR(128),
		@PartitionName VARCHAR(128),
		@FirstPartitionName VARCHAR(128),
		@LastPartitionName VARCHAR(128),
		@PartitionCount INT,
		@Message VARCHAR(max),
		@Command VARCHAR(max)

RAISERROR('Beginning process to update size of next 2 partitions.',10,1) WITH NOWAIT;
BEGIN TRY

    SELECT @LastDateToAverage = GETDATE()-7
	SELECT @LastPartitionToAverage = (SELECT Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @LastDateToAverage AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @FirstDateToAverage = GETDATE() - 70
	SELECT @FirstPartitionToAverage = (SELECT Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @FirstDateToAverage AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @CurrentDate = GETDATE();
	SELECT @PartitionDateKey1 = (SELECT Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate > @CurrentDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @PartitionDateKey2 = (SELECT Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate > @CurrentDate + 7 AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	  	  
	SELECT @FirstPartitionName = @parmPartitionScheme + CAST(@FirstPartitionToAverage AS CHAR(8))
	SELECT @LastPartitionName = @parmPartitionScheme + CAST(@LastPartitionToAverage AS CHAR(8))

	SELECT @Message = 'First Partition to Average is: ' + @FirstPartitionName;
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	SELECT @Message = 'Last Partition to Average is: ' + @LastPartitionName;
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	SELECT @PartitionCount = Count(*) 
	FROM sys.destination_data_spaces 
		INNER JOIN sys.data_spaces  ON sys.destination_data_spaces.data_space_id = sys.data_spaces.data_space_id
		INNER JOIN sys.database_files ON sys.data_spaces.data_space_id = sys.database_files.data_space_id
		INNER JOIN sys.partition_schemes  ON sys.destination_data_spaces.partition_scheme_id = sys.partition_schemes.data_space_id
	WHERE sys.partition_schemes.name = @parmPartitionScheme
	AND sys.database_files.name >= @FirstPartitionName
	AND sys.database_files.name <= @LastPartitionName
	

	SELECT @Command = 'Number of Partitions available to average = ' + CAST(@PartitionCount AS VARCHAR(2))
			RAISERROR(@COMMAND, 10,1) WITH NOWAIT;
	
	IF @PartitionCount > 0
		BEGIN
			SELECT  
				@PSNAME = sys.partition_schemes.name,  
				@AveragePartitionSizeMB = CEILING(SUM(CAST(FILEPROPERTY(sys.database_files.name,'SpaceUsed') AS INT)) / 128.0 /@PartitionCount)
			FROM sys.destination_data_spaces 
				INNER JOIN sys.data_spaces ON sys.destination_data_spaces.data_space_id = sys.data_spaces.data_space_id
				INNER JOIN sys.database_files ON sys.data_spaces.data_space_id = sys.database_files.data_space_id
				INNER JOIN sys.partition_schemes ON sys.destination_data_spaces.partition_scheme_id = sys.partition_schemes.data_space_id
			WHERE sys.partition_schemes.name = @parmPartitionScheme
				AND sys.database_files.name >= @FirstPartitionName
				AND sys.database_files.name <= @LastPartitionName
			GROUP BY 
				sys.partition_schemes.name;
	
			SELECT @Message = 'Altering Partition Size for Partition Scheme ' + @PSNAME + ' to ' + CAST(@AveragePartitionSizeMB as VARCHAR(10)) + ' where size is smaller';
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @PartitionName = @PSNAME + CAST(@PartitionDateKey1 AS CHAR(8))
			IF (@AveragePartitionSizeMB > (SELECT CEILING(SIZE / 128.0) FROM sys.database_files WHERE name = @PartitionName))
				BEGIN
					SELECT @Command = 'ALTER DATABASE ' + DB_NAME() + ' MODIFY FILE(Name=' + @PSNAME + CAST(@PartitionDateKey1 AS CHAR(8)) + ',SIZE=' + CAST(@AveragePartitionSizeMB as VARCHAR(10)) + ' MB)'
					RAISERROR(@COMMAND, 10,1) WITH NOWAIT;
					EXEC(@Command);
				END
			SELECT @PartitionName = @PSNAME + CAST(@PartitionDateKey2 AS CHAR(8))
			IF (@AveragePartitionSizeMB > (SELECT CEILING(SIZE / 128.0) FROM sys.database_files WHERE name = @PartitionName))
				BEGIN
					SELECT @Command = 'ALTER DATABASE ' + DB_NAME() + ' MODIFY FILE(Name=' + @PSNAME + CAST(@PartitionDateKey2 AS CHAR(8)) + ',SIZE=' + CAST(@AveragePartitionSizeMB as VARCHAR(10)) + ' MB)'
					RAISERROR(@COMMAND, 10,1) WITH NOWAIT;
					EXEC(@Command);
				END
		END
	ELSE
		RAISERROR('No Partition Sizes will be updated because the average size cannot be calculated', 10,1) WITH NOWAIT;
		
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH