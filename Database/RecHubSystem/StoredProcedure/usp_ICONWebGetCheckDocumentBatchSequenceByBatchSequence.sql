--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence') IS NOT NULL
	DROP PROCEDURE RecHubSystem.usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence

SET QUOTED_IDENTIFIER ON
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence	
(
	@parmImmutableDate		DATETIME,
	@parmSiteBankID			INT,
	@parmSiteOrganizationID INT,
	@parmSiteClientAccountID	INT,
	@parmSiteCodeID			INT,	
	@parmSourceBatchID		BIGINT,
	@parmBatchSequence		INT,
	@parmBatchSource		VARCHAR(30)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MEH
* Date: 02/05/2009
*
* Purpose: Get BatchSequence from factChecks, factDocuments and factStubs for a batch
*
* Modification History
* 02/05/2009 CR 28912 MEH	Created
* 04/14/2011 CR 33872 JMC	Modified Stubs lookup to find factStubs.BatchSequence
*                           instead of factStubs.DocumentBatchSequence.
* 04/08/2014 WI 135712 JBS	Update to 2.01 release.  Change Schema Names. 
*							Rename: LockBox to ClientAccount, Customer to Organization, Processing to Immutable
*							Change Table references: factDocuments to factDocumentImages, factChecks to factCheckImages
*							Add Batch Collision logic
* 03/10/2015 WE 195097 TWE  Modify to handle extra output columns
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @ImmutableDateKey	   INT,
		@DepositDateKey		   INT,
		@BatchID			   BIGINT,
		@BatchNumber           INT,
	    @BatchPaymentTypeKey   TINYINT,
	    @DepositStatus         INT;

BEGIN TRY

	SET @ImmutableDateKey = CAST(CONVERT(VARCHAR,@parmImmutableDate,112) AS INT);

	EXEC RecHubData.usp_factBatchSummary_Get_BatchID @ImmutableDateKey, 
	                                                 @parmSiteBankID, 
													 @parmSiteOrganizationID, 
													 @parmSiteClientAccountID, 
													 @parmSiteCodeID, 
													 @parmSourceBatchID, 
													 @parmBatchSource, 
													 @BatchID             OUTPUT, 
													 @DepositDateKey      OUTPUT,
													 @BatchNumber         OUTPUT,
	                                                 @BatchPaymentTypeKey OUTPUT,
	                                                 @DepositStatus       OUTPUT;


	SELECT 
	    @BatchNumber         AS BatchNumber, 
		@BatchPaymentTypeKey AS BatchPaymentTypeKey,
		@DepositStatus       AS DepositStatus,
	    TransactionID, TxnSequence, DocumentSequence AS Sequence, BatchSequence, 1 AS IsDocument, 0 AS IsCheck, 0 AS IsStub
	FROM RecHubData.factDocuments
	WHERE
	    RecHubData.factDocuments.IsDeleted = 0
		AND RecHubData.factDocuments.DepositDateKey = @DepositDateKey
		AND RecHubData.factDocuments.BatchID = @BatchID
		AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence	
	UNION
	SELECT 
	    @BatchNumber         AS BatchNumber, 
		@BatchPaymentTypeKey AS BatchPaymentTypeKey,
		@DepositStatus       AS DepositStatus,
	    TransactionID, TxnSequence, CheckSequence AS Sequence, BatchSequence, 0 AS IsDocument, 1 AS IsCheck, 0 AS IsStub
	FROM RecHubData.factChecks
	WHERE
	    RecHubData.factChecks.IsDeleted = 0
		AND RecHubData.factChecks.DepositDateKey = @DepositDateKey
		AND RecHubData.factChecks.BatchID = @BatchID
		AND RecHubData.factChecks.BatchSequence = @parmBatchSequence			
	UNION
    SELECT 
	    @BatchNumber         AS BatchNumber, 
		@BatchPaymentTypeKey AS BatchPaymentTypeKey,
		@DepositStatus       AS DepositStatus,
	    TransactionID, TxnSequence, StubSequence AS Sequence, BatchSequence, 0 AS IsDocument, 0 AS IsCheck, 1 AS IsStub
    FROM RecHubData.factStubs
	WHERE
	    RecHubData.factStubs.IsDeleted = 0
		AND RecHubData.factStubs.DepositDateKey = @DepositDateKey
		AND RecHubData.factStubs.BatchID = @BatchID
		AND RecHubData.factStubs.BatchSequence = @parmBatchSequence;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


			
	
