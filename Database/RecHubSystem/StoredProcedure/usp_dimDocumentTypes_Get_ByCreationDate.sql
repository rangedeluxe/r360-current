--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_dimDocumentTypes_Get_ByCreationDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_dimDocumentTypes_Get_ByCreationDate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubSystem].[usp_dimDocumentTypes_Get_ByCreationDate] 
(
	@parmCreationDate dateTime = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/11/2012
*
* Purpose: Request Document Types 
*
* Modification History
* 05/11/2012 CR 52653 WJS	Created
* 01/23/2014 WI 127715 CMC	Update to 2.0 release.  Change schema name 
*							Rename proc FROM usp_DataImportIntegrationServices_RequestDocumentTypes
*							Change parameter @parmLoadDate to @parmCreationDate
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	
	SELECT FileDescriptor, IMSDocumentType 
	FROM
	RecHubData.dimDocumentTypes
	WHERE MostRecent =1
	  AND (
                (@parmCreationDate IS NULL AND CreationDate = CreationDate) OR  
                (@parmCreationDate IS NOT NULL AND CreationDate > @parmCreationDate)
          );
	


END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
