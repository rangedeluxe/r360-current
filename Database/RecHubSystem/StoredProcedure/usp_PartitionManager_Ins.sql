--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionManager_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionManager_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionManager_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionManager_Ins
(
	@parmPartitionIdentifier	VARCHAR(20),
	@parmPartitionFunctionName	VARCHAR(256),
	@parmPartitionSchemeName	VARCHAR(256),
	@parmSizeMB					INT,
	@parmRangeSetting			INT,
	@parmLastDiskID				INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/11/2009
*
* Purpose: Add New Partition to PartitionManager table.
*
* Modification History
* 08/10/2009 CR 27527 JNE	Created
* 04/10/2013 WI 90578 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_AddNewPartitionManager
******************************************************************************/
SET NOCOUNT ON; 
DECLARE @PartitionManagerID INT;

BEGIN TRY       
	SELECT	
		@PartitionManagerID = RecHubSystem.PartitionManager.PartitionManagerID
	FROM
		RecHubSystem.PartitionManager 
	WHERE
		RecHubSystem.PartitionManager.PartitionIdentifier = @parmPartitionIdentifier
		AND RecHubSystem.PartitionManager.PartitionFunctionName = @parmPartitionFunctionName
		AND RecHubSystem.PartitionManager.PartitionSchemeName = @parmPartitionSchemeName;
		
	IF @PartitionManagerID IS NULL
	BEGIN
		INSERT INTO RecHubSystem.PartitionManager
		(
			RecHubSystem.PartitionManager.PartitionIdentifier,
			RecHubSystem.PartitionManager.PartitionFunctionName,
			RecHubSystem.PartitionManager.PartitionSchemeName,
			RecHubSystem.PartitionManager.SizeMB, 
			RecHubSystem.PartitionManager.RangeSetting, 
			RecHubSystem.PartitionManager.LastDiskID
		)
		VALUES
		(
			@parmPartitionIdentifier, 
			@parmPartitionFunctionName, 
			@parmPartitionSchemeName, 
			@parmSizeMB, 
			@parmRangeSetting, 
			@parmLastDiskID
		);
	END
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
