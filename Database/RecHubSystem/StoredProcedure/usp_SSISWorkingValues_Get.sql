--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_SSISWorkingValues_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_SSISWorkingValues_Get') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_SSISWorkingValues_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_SSISWorkingValues_Get
(
	@parmPackageName			VARCHAR(128),
	@parmVariableName			VARCHAR(128) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/07/2013
*
* Purpose: Get rows from RecHubSystem.SSISWorkingValues.
*
* Modification History
* 06/07/2013 WI 104756 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		VariableName,
		VariableDateTime,
		VariableInt,
		VariableString
	FROM 
		RecHubSystem.SSISWorkingValues
	WHERE 
		PackageName = @parmPackageName
		AND VariableName = CASE WHEN @parmVariableName IS NOT NULL THEN @parmVariableName ELSE VariableName END; 

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH