--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_ClientSetupResponse
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse 
(
	@parmClientProcessCode VARCHAR(40) = 'Unassigned'
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
******************************************************************************* 
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: 
*
* Modification History
* 01/24/2012 CR 49653 JPB	Created
* 02/08/2012 CR 53929 CEJ 	Alter this proc so that it returns QueueStatus of both 99 and 120
* 09/27/2012 CR 56082 JPB 	Added ClientProcessCode filter.
* 04/05/2013 WI 90611 JBS	Update to 2.0 release. Change schema name to RecHubSystem
*							Renamed Proc FROM usp_DataImportIntegrationServices_GetResponseClientSetup
* 03/02/2018 PT #139509337	Return each response document as a row.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @Loop INT,
		@SourceTrackingID UNIQUEIDENTIFIER,
		@ResponseTrackingID UNIQUEIDENTIFIER,
		@Msg VARCHAR(MAX);

BEGIN TRY

	SELECT @ResponseTrackingID = NEWID();

	UPDATE	
		RecHubSystem.DataImportQueue
	SET		
		ResponseTrackingID = @ResponseTrackingID,
		QueueStatus = 120,
		ModificationDate = GETDATE(),
		ModifiedBy = SUSER_NAME()
	OUTPUT 
		INSERTED.ResponseTrackingID,
		INSERTED.SourceTrackingID,
		INSERTED.AuditDateKey,
		INSERTED.XMLResponseDocument
	FROM	
		RecHubSystem.DataImportQueue
	WHERE
		QueueType = 0 
		AND QueueStatus IN (30,99, 120)
		AND ClientProcessCode = @parmClientProcessCode;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH