--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_RecHubDataIndexMaintenance
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_RecHubDataIndexMaintenance') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_RecHubDataIndexMaintenance
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_RecHubDataIndexMaintenance
(
	@parmTimeToRun VARCHAR(8) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/12/2011
*
* Purpose: Index maintenance for the OLTA schema tables.
*
* Modification History
* 08/12/2011 CR 45805 JPB	FP of 45805.
* 04/15/2013 WI 90766 JBS	Update to 2.0 release. Change schema 
*							Rename Proc from usp_OLTAIndexMaintenance
* 06/21/2013 WI 83611 JBS	FP WI83610
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @TableGroupLoop INT,
		@TableLoop		INT,
		@IndexLoop		INT,
		@PartitionType	TINYINT,
		@TableName		VARCHAR(128),
		@IndexName		VARCHAR(128),
		@SQLCommand		VARCHAR(MAX),
		@EndTime		DATETIME;

		
DECLARE @TableGroup TABLE
(
	RowID			INT IDENTITY(1,1),
	TableGroup		VARCHAR(128),
	PartitionType	TINYINT
);

DECLARE @Reorg TABLE 
(
	RowID			INT IDENTITY(1,1),
	TableName		VARCHAR(128),
	IndexName		VARCHAR(128),
	FragPercent		FLOAT,
	PartitionNumber INT,
	PartitionType	TINYINT
);

DECLARE @Rebuild TABLE 
(
	RowID			INT IDENTITY(1,1),
	TableName		VARCHAR(128),
	IndexName		VARCHAR(128),
	FragPercent		FLOAT,
	PartitionNumber INT,
	PartitionType	TINYINT
);

BEGIN TRY
	/* if a time value was passed in, validate it is the correct format and then create ending time. */
	IF( @parmTimeToRun IS NOT NULL )
	BEGIN
		IF( LEN(@parmTimeToRun) < 8 ) 
			RAISERROR('@parmTimeToRun must be formatted as 00:00:00',16,1);
		IF( CHARINDEX(':',@parmTimeToRun,1) <> 3 )
			RAISERROR('@parmTimeToRun must be formatted as 00:00:00',16,1);
		IF( CHARINDEX(':',@parmTimeToRun,4) <> 6 )
			RAISERROR('@parmTimeToRun must be formatted as 00:00:00',16,1);
		SELECT @EndTime = DATEADD(s,(CAST(SUBSTRING(@parmTimeToRun,1,2) AS INT) * 3600) + (CAST(SUBSTRING(@parmTimeToRun,4,2) AS INT) * 60) + CAST(SUBSTRING(@parmTimeToRun,7,2) AS INT),GETDATE());
	END

	/* Find out if the database is partitioned */
	SELECT	@PartitionType = RecHubSystem.PartitionManager.RangeSetting
	FROM	RecHubSystem.PartitionManager
	WHERE	RecHubSystem.PartitionManager.PartitionIdentifier = 'RecHubFacts';

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TableList')) 
		DROP TABLE #TableList;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#IndexList')) 
		DROP TABLE #IndexList;

	CREATE TABLE #TableList
	(
		RowID		INT IDENTITY(1,1),
		TableName	VARCHAR(128)
	);

	CREATE TABLE #IndexList
	(
		RowID			INT IDENTITY(1,1),
		TableName		VARCHAR(128),
		IndexName		VARCHAR(128),
		FragPercent		FLOAT,
		PartitionNumber INT,
		PartitionType	TINYINT,
		Reorg			BIT
	);

	/* The entire process will go through the table list starting with facts, then dims, the all others. */
	INSERT INTO @TableGroup(TableGroup,PartitionType)
	SELECT 'TABLE_NAME LIKE ''fact%''',@PartitionType
	UNION ALL
	SELECT 'TABLE_NAME LIKE ''dim%''',0
	UNION ALL
	SELECT '(TABLE_NAME NOT LIKE ''fact%'' AND TABLE_NAME NOT LIKE ''dim%'')',0;

	SET @TableGroupLoop = 1;
	WHILE( @TableGroupLoop <= (SELECT MAX(RowID) FROM @TableGroup) )
	BEGIN
		/* Build list of tables names for the given table grouping */
		SELECT @SQLCommand = 'SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ''RecHubData'' AND ' + TableGroup, @PartitionType = PartitionType
		FROM @TableGroup
		WHERE RowID = @TableGroupLoop;

		INSERT INTO #TableList(TableName)
		EXEC(@SQLCommand);

		SET @TableLoop = 1;
		/* walk the table list and find the indexes that need to be reorg'ed or reindexed */
		WHILE( @TableLoop < (SELECT MAX(RowID) FROM #TableList) )
		BEGIN
			/* Get all of the indexes that are frag'ed between 5% and 30%. These will be reorg'ed */
			SELECT @SQLCommand = 'SELECT object_name(sys.dm_db_index_physical_stats.object_id) AS TableName,name,avg_fragmentation_in_percent,Partition_Number,' + CAST(@PartitionType AS VARCHAR) + ' AS PartitionType ' +
								'FROM sys.dm_db_index_physical_stats (DB_ID(), OBJECT_ID(N' + CHAR(39) + 'RecHubData.' + TableName + CHAR(39) + '),NULL, NULL, ''LIMITED'') ' +
								'	JOIN sys.indexes ON sys.dm_db_index_physical_stats.object_id = sys.indexes.object_id AND sys.dm_db_index_physical_stats.index_id = sys.indexes.index_id ' +
								'WHERE avg_fragmentation_in_percent >  5 AND name IS NOT NULL AND avg_fragmentation_in_percent <= 30 ORDER BY avg_fragmentation_in_percent DESC'
			FROM	#TableList
			WHERE	RowID = @TableLoop;

			/* Get all of the indexes that are frag'ed greather then 30%. These will be rebuilt. */
			INSERT INTO @Reorg(TableName,IndexName,FragPercent,PartitionNumber,PartitionType)
			EXEC(@SQLCommand);

			SELECT @SQLCommand = 'SELECT object_name(sys.dm_db_index_physical_stats.object_id) AS TableName,name,avg_fragmentation_in_percent,Partition_Number,' + CAST(@PartitionType AS VARCHAR) + ' AS PartitionType ' +
								'FROM sys.dm_db_index_physical_stats (DB_ID(), OBJECT_ID(N' + CHAR(39) + 'RecHubData.' + TableName + CHAR(39) + '),NULL, NULL, ''LIMITED'') ' +
								'	JOIN sys.indexes ON sys.dm_db_index_physical_stats.object_id = sys.indexes.object_id AND sys.dm_db_index_physical_stats.index_id = sys.indexes.index_id ' +
								'WHERE  avg_fragmentation_in_percent > 30 AND name IS NOT NULL ORDER BY avg_fragmentation_in_percent DESC'
			FROM	#TableList
			WHERE	RowID = @TableLoop;

			INSERT INTO @Rebuild(TableName,IndexName,FragPercent,PartitionNumber,PartitionType)
			EXEC(@SQLCommand);

			SET @TableLoop = @TableLoop + 1;
		END

		/* Combine the two lists order by frag percentage and partition number. This will cause the indexes to handled by the largest frag percentage to lowest, newest partation to oldest */
		INSERT INTO #IndexList( TableName,IndexName,FragPercent,PartitionNumber,Reorg,PartitionType)
		SELECT TableName,IndexName,FragPercent,PartitionNumber,1,PartitionType AS Reorg FROM @Reorg 
		UNION ALL
		SELECT TableName,IndexName,FragPercent,PartitionNumber,0,PartitionType FROM @Rebuild ORDER BY FragPercent DESC, PartitionNumber DESC;

		BEGIN TRY
			SET @IndexLoop = 1;
			WHILE( @IndexLoop <= (SELECT MAX(RowID) FROM #IndexList) )
			BEGIN
				SELECT @SQLCommand = 'ALTER INDEX [' + IndexName + '] ON [RecHubData].[' + TableName + '] ' + 
					CASE Reorg
						WHEN 1 THEN 'REORGANIZE'
						ELSE 'REBUILD'
					END + ' ' +
					CASE 
						WHEN PartitionType > 0 THEN	'PARTITION = ' + CAST(PartitionNumber AS VARCHAR) + ';'
						ELSE ';'
					END
				FROM	#IndexList
				WHERE	RowID = @IndexLoop;

				EXEC(@SQLCommand);
				
				/* Check the current time against the end time. Exit if the current time is greater than the end time. */
				IF( @parmTimeToRun IS NOT NULL AND GETDATE() > @EndTime )
				BEGIN
					/* force the loop to exit by the catch */
					RAISERROR('TimeToRunExpired',16,1);
				END

				SET @IndexLoop = @IndexLoop + 1;
			END

			/*Truncate working tables so we can do the next table list */
			TRUNCATE TABLE #TableList;
			TRUNCATE TABLE #IndexList;
			DBCC CHECKIDENT (#TableList,RESEED, 1);
			DBCC CHECKIDENT (#IndexList,RESEED, 1);
			DELETE FROM @Reorg;
			DELETE FROM @Rebuild;
			SET @TableGroupLoop = @TableGroupLoop + 1;
		END TRY
		BEGIN CATCH
			DECLARE @ErrorMessage    NVARCHAR(4000),
					@ErrorSeverity   INT,
					@ErrorState      INT;
			SELECT	@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE();

			/* Time expired, exit SP, else throw the message to the main catch block */
			IF( PATINDEX('%TimeToRunExpired%',@ErrorMessage) > 0 )
				BREAK;
			ELSE RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
		END CATCH
	END
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TableList')) 
		DROP TABLE #TableList;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#IndexList')) 
		DROP TABLE #IndexList;
END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TableList')) 
		DROP TABLE #TableList;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#IndexList')) 
		DROP TABLE #IndexList;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH