--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_ListFKConStraints
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_ListFKConStraints') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_ListFKConStraints
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_ListFKConStraints
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: listing all the constraints in facts tables 
*	
*
* Modification History
* 06/01/2013 WI 117316 PS	Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( OBJECT_ID(N'tempdb..#TempConstraintTable') IS NOT NULL )
		DROP TABLE #TempConstraintTable;
		
	IF( OBJECT_ID('dbo.WT_FKConstraintTable', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_FKConstraintTable;

    CREATE TABLE #TempConstraintTable 
	(
		Id INT PRIMARY KEY IDENTITY(1, 1),
		FKConstraintName VARCHAR(255),
		FKConstraintTableSchema VARCHAR(255),
		FKConstraintTableName VARCHAR(255),
		FKConstraintColumnName VARCHAR(255),
		PKConstraintName VARCHAR(255),
		PKConstraintTableSchema VARCHAR(255),
		PKConstraintTableName VARCHAR(255),
		PKConstraintColumnName VARCHAR(255)    
    )

	INSERT INTO #TempConstraintTable(FKConstraintName, FKConstraintTableSchema, FKConstraintTableName, FKConstraintColumnName)
	SELECT 
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_NAME, 
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE.TABLE_SCHEMA, 
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE.TABLE_NAME, 
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE.COLUMN_NAME 
	FROM 
		INFORMATION_SCHEMA.KEY_COLUMN_USAGE
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS ON INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_NAME = INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_NAME
	WHERE
		INFORMATION_SCHEMA.TABLE_CONSTRAINTS.CONSTRAINT_TYPE = 'FOREIGN KEY';

	UPDATE #TempConstraintTable 
	SET 
		PKConstraintName = UNIQUE_CONSTRAINT_NAME
	FROM 
		#TempConstraintTable
		INNER JOIN INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS ON #TempConstraintTable.FKConstraintName = INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS.CONSTRAINT_NAME

	UPDATE #TempConstraintTable 
	SET
		PKConstraintTableSchema  = TABLE_SCHEMA,
		PKConstraintTableName  = TABLE_NAME
	FROM 
		#TempConstraintTable tt
		INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS TableConstraints ON tt.PKConstraintName = TableConstraints.CONSTRAINT_NAME

	UPDATE #TempConstraintTable 
	SET
		PKConstraintColumnName = COLUMN_NAME
	FROM 
		#TempConstraintTable
		INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE ON #TempConstraintTable.PKConstraintName = INFORMATION_SCHEMA.KEY_COLUMN_USAGE.CONSTRAINT_NAME

    /************************************************************************************
    ****CREATING A TABLE THAT LIST ALL THE CONSTRAINT COMMANDS
    ****This table will be used for dropping or adding constraints if it does not exist.
    ************************************************************************************/

    SELECT 
		IDENTITY(INT,1,1) AS TableID,
		FKConstraintName,
		FKConstraintTableSchema,
        FKConstraintTableName,
		FKConstraintColumnName,
		PKConstraintName,
		PKConstraintTableSchema,
        PKConstraintTableName,
		PKConstraintColumnName,
		'
		ALTER TABLE [' + FKConstraintTableSchema + '].[' + FKConstraintTableName + '] 
		DROP CONSTRAINT ' + FKConstraintName AS DropConstSQL,
		'
		ALTER TABLE [' + FKConstraintTableSchema + '].[' + FKConstraintTableName + '1] WITH CHECK
		ADD CONSTRAINT ' + FKConstraintName + '1 FOREIGN KEY(' + FKConstraintColumnName + ') REFERENCES [' + PKConstraintTableSchema + '].[' + PKConstraintTableName + '](' + PKConstraintColumnName + ')' AS AddConstSQL
	INTO 
		dbo.WT_FKConstraintTable
	FROM
       #TempConstraintTable  
	WHERE
		RIGHT([FKConstraintTableSchema],7)<>'Staging' 
		AND 
		( 
			(LEFT(FKConstraintTableName,3)='fac') OR (LEFT(FKConstraintTableName, 3) = 'dat') OR (LEFT(FKConstraintTableName, 3) = 'Cds')
		)
	ORDER BY 
		FKConstraintTableSchema, 
		FKConstraintTableName;
  
     DROP TABLE #TempConstraintTable;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
