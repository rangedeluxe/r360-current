--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_DeleteFactRecords
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportIntegrationServices_DeleteFactRecords') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_DeleteFactRecords
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_DeleteFactRecords
(
       @parmBatches		XML,
	   @parmRowsDeleted	BIT OUT 
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/06/2012
*
* Purpose: Deletes fact table entries for a batch by:
* <Batches>
*     <Batch GlobalBatchID="" SiteID="" LockboxID="" BankID="" DepositDate="" />
* </Batches>
*
* Modification History
* 03/06/2012 CR 50981 WJS	Created
* 02/07/2014 WI 128839 JBS	Update to 2.00 Schema from 1.05.  Rename Customer to Organization,
*							Processing to Immutable, Lockbox to ClientAccount
*							Added ModificationDate for called procs to update IsDeleted columns
*							usp_ConvertSourceKeysToRecHubKeys replaces need for previous read of XML
* 04/17/2014 WI 137286 JBS	Add permissions for RechubUser_User
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON

DECLARE @iBankKey			INT,
		@iOrganizationKey	INT,
		@iClientAccountKey	INT,
		@iImmutableDateKey	INT,
		@iSourceProcessingDateKey INT,
		@iDepositDateKey	INT,
		@iBatchID			INT,
		@iBatchSourceKey	TINYINT,
		@ModificationDate	DATETIME;

BEGIN TRY
	SELECT @ModificationDate = GETDATE();

	EXEC RecHubData.usp_ConvertSourceKeysToRecHubKeys @parmBatches, @iBankKey OUTPUT, @iOrganizationKey OUTPUT, @iClientAccountKey OUTPUT, @iImmutableDateKey OUTPUT, @iDepositDateKey OUTPUT, @iSourceProcessingDateKey OUT, @iBatchSourceKey OUT;
	
	EXEC RecHubData.usp_factBatchSummary_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate, @parmRowsDeleted OUTPUT;
	EXEC RecHubData.usp_factTransactionSummary_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;
	EXEC RecHubData.usp_factChecks_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;
	EXEC RecHubData.usp_factDocuments_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;
	EXEC RecHubData.usp_factStubs_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;
	EXEC RecHubData.usp_factDataEntrySummary_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;
	EXEC RecHubData.usp_factDataEntryDetails_Upd_IsDeleted @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @ModificationDate;


END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
	
END CATCH
