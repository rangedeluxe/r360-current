--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionManager_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionManager_Get') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionManager_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionManager_Get 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/11/2009
*
* Purpose: Retrieve all rows from PartitionManager Table
*
* Modification History
* 08/10/2009 CR 27521 JNE	Created
* 09/18/2009 CR 27521 JNE 	Instead select all, use column names.
* 04/11/2013 WI 90712 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_GetPartitionManager
******************************************************************************/
SET NOCOUNT ON;
 
BEGIN TRY
	SELECT	RecHubSystem.PartitionManager.PartitionManagerID,
			RecHubSystem.PartitionManager.PartitionIdentifier,
			RecHubSystem.PartitionManager.PartitionFunctionName,
			RecHubSystem.PartitionManager.PartitionSchemeName,
			RecHubSystem.PartitionManager.SizeMB,
			RecHubSystem.PartitionManager.RangeSetting,
			RecHubSystem.PartitionManager.LastDiskID,
			RecHubSystem.PartitionManager.IsLocked
	FROM	RecHubSystem.PartitionManager;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
