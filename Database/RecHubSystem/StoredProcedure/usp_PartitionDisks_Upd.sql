--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionDisks_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionDisks_Upd') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionDisks_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionDisks_Upd
(
	@parmPartitionManagerID INT,
	@parmDiskOrder			INT,
	@parmDiskPath			VARCHAR(256)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/11/2009
*
* Purpose:  Update PartitionDisks with input parameters
*
* Modification History
* 08/10/2009 JNE	Created
* 04/15/2013 JBS	Update to 2.0 release. Change Schema to RecHubSystem
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY       
	UPDATE	RecHubSystem.PartitionDisks 
	SET		RecHubSystem.PartitionDisks.DiskPath = @parmDiskPath
	WHERE	RecHubSystem.PartitionDisks.PartitionManagerID = @parmPartitionManagerID
			AND RecHubSystem.PartitionDisks.DiskOrder = @parmDiskOrder;
END TRY

BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
