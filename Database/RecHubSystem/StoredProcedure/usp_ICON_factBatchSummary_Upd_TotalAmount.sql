--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICON_factBatchSummary_Upd_TotalAmount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICON_factBatchSummary_Upd_TotalAmount') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_factBatchSummary_Upd_TotalAmount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_factBatchSummary_Upd_TotalAmount
(
		@XBatch					     XML,
		@parmBankKey			     INT,
		@parmOrganizationKey	     INT,
		@parmClientAccountKey	     INT, 
		@parmImmutableDateKey	     INT,
		@parmDepositDateKey		     INT,
		@parmSourceProcessingDateKey INT,
		@parmBatchID			     BIGINT,
		@parmSourceBatchID           BIGINT,
		@parmSourceIdentifier	     SMALLINT,
		@parmModificationDate	     DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/12/2010
*
* Purpose: Updates factBatchSummary.Check and Stub totals row(s) from XBatch Checks XML segment
*
* Modification History
* 02/09/2010 CR 28996 JPB	Created.
* 04/18/2013 WI 90636 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc from usp_factBatchSummary_UpdateCheckTotal
*							Change parameters: @parmCustomerKey	to  @parmOrganizationKey,
*							@parmLockboxKey	to @parmClientAccountKey,
*							@parmProcessingDateKey to @parmImmutableDateKey
*							Change references: Customer to Organization,
*							Lockbox to ClientAccount, Processing to Immutable.	
*							Added IsDeleted = 0 for all Tables	
* 10/24/2014 WI 173991 TWE  Updated for 2.01
* 06/09/2015 WI 217502 JAW  Update stub totals. 
* 06/09/2015 WI 217502 JBS	Renaming from RecHubData.usp_factBatchSummary_Upd_CheckTotal
* 07/17/2015 WI 224969 JBS  Add BatchExceptionStatusKey to insert
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @bLocalTransaction	  BIT,
		@mCheckTotal		  MONEY,
        @mStubTotal           MONEY,
		@factBatchSummaryKey  BIGINT;

BEGIN TRY

	IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION  
			SET @bLocalTransaction = 1;
		END
	
    --Get the check total
    SELECT
	    @mCheckTotal = SUM(RecHubData.factChecks.Amount)
	FROM
	    RecHubData.factChecks
	WHERE
	    RecHubData.factChecks.BankKey = @parmBankKey
		AND RecHubData.factChecks.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factChecks.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factChecks.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factChecks.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factChecks.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factChecks.IsDeleted = 0;

    --Get the stub total
    SELECT
	    @mStubTotal = SUM(RecHubData.factStubs.Amount)
	FROM
	    RecHubData.factStubs
	WHERE
	    RecHubData.factStubs.BankKey = @parmBankKey
		AND RecHubData.factStubs.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factStubs.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factStubs.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factStubs.IsDeleted = 0;

    -------------------------------------------------------------------
	-- find the factBatchSummaryKey of the record we want to update.
	-- then insert a new record using the factBatchSummaryKey 
	-- then flag the original record as deleted
	SELECT
	    @factBatchSummaryKey = factBatchSummaryKey
	FROM 
	    RecHubData.factBatchSummary
	WHERE
	    RecHubData.factBatchSummary.BankKey = @parmBankKey
		AND RecHubData.factBatchSummary.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factBatchSummary.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factBatchSummary.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		AND RecHubData.factBatchSummary.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factBatchSummary.IsDeleted = 0;

		--- now insert our new record
	INSERT INTO RecHubData.factBatchSummary
	(
	    IsDeleted
       ,BankKey
       ,OrganizationKey
       ,ClientAccountKey
       ,DepositDateKey
       ,ImmutableDateKey
       ,SourceProcessingDateKey
       ,BatchID
       ,SourceBatchID
       ,BatchNumber
       ,BatchSourceKey
       ,BatchPaymentTypeKey
	   ,BatchExceptionStatusKey
       ,BatchCueID
       ,DepositStatus
       ,SystemType
       ,DepositStatusKey
       ,TransactionCount
       ,CheckCount
       ,ScannedCheckCount
       ,StubCount
       ,DocumentCount
       ,CheckTotal
       ,StubTotal
       ,CreationDate
       ,ModificationDate
       ,BatchSiteCode
       ,DepositDDA
	)
    SELECT 
	    IsDeleted
       ,BankKey
       ,OrganizationKey
       ,ClientAccountKey
       ,DepositDateKey
       ,ImmutableDateKey
       ,SourceProcessingDateKey
       ,BatchID
       ,SourceBatchID
       ,BatchNumber
       ,BatchSourceKey
       ,BatchPaymentTypeKey
	   ,BatchExceptionStatusKey
       ,BatchCueID
       ,DepositStatus
       ,SystemType
       ,DepositStatusKey
       ,TransactionCount
       ,CheckCount
       ,ScannedCheckCount
       ,StubCount
       ,DocumentCount
       ,COALESCE(@mCheckTotal, 0) AS CheckTotal        -- add our new check total
       ,COALESCE(@mStubTotal, 0)  AS StubTotal         -- add our new stub total
       ,@parmModificationDate     AS CreationDate      -- current modified date time
       ,@parmModificationDate     AS ModificationDate  -- current modified date time
       ,BatchSiteCode
       ,DepositDDA
    FROM 
	    RecHubData.factBatchSummary
    WHERE 
	    RecHubData.factBatchSummary.factBatchSummaryKey = @factBatchSummaryKey;
    
	--Now flag the old record as deleted
	UPDATE	
	    RecHubData.factBatchSummary
	SET 	
	    RecHubData.factBatchSummary.IsDeleted = 1,
		RecHubData.factBatchSummary.ModificationDate = @parmModificationDate
	WHERE	
	    RecHubData.factBatchSummary.factBatchSummaryKey = @factBatchSummaryKey;
			

	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0;
	END

END TRY
BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0;
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage	NVARCHAR(4000),
				@ErrorSeverity	INT,
				@ErrorState		INT,
				@ErrorLine		INT;
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE();

		IF @@NESTLEVEL > 3 --update only if we got here by two service broker calls
			SET @ErrorMessage = 'usp_ICON_factBatchSummary_Upd_TotalAmount (Line: %d)-> ' + @ErrorMessage;
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
END CATCH
