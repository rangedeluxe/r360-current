--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Upd_QueueStatus
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_Upd_QueueStatus') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_Upd_QueueStatus
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_Upd_QueueStatus 
(
	@parmResponseTrackingID UNIQUEIDENTIFIER,
	@parmEntityTrackingID	UNIQUEIDENTIFIER = NULL,
	@parmSourceTrackingID	UNIQUEIDENTIFIER = NULL,
	@parmQueueStatus		INT = 150
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Update Queue Status on RecHubSystem.DataImportQueue
*
* Modification History
* 01/24/2012 CR 50198 JPB	Created
* 08/14/2012 CR 55317 JPB	Added @parmEntityTrackingID and @parmQueueStatus.
* 04/10/2013 WI 90615 JBS	Update to 2.0 release. Change schema name to RecHubsystem
*							Rename proc FROM usp_DataImportIntegrationServices_SetResponseComplete
* 10/26/2015 WI 243288 CMC	Added @parmSourceTrackingID. And Permissions
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	UPDATE	RecHubSystem.DataImportQueue
	SET		RecHubSystem.DataImportQueue.QueueStatus = @parmQueueStatus,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_NAME()
	FROM	RecHubSystem.DataImportQueue
	WHERE	RecHubSystem.DataImportQueue.ResponseTrackingID = @parmResponseTrackingID
			AND RecHubSystem.DataImportQueue.EntityTrackingID = CASE WHEN @parmEntityTrackingID IS NULL THEN RecHubSystem.DataImportQueue.EntityTrackingID ELSE @parmEntityTrackingID END
			AND SourceTrackingID = CASE WHEN @parmSourceTrackingID IS NULL THEN SourceTrackingID ELSE @parmSourceTrackingID END
			AND RecHubSystem.DataImportQueue.QueueStatus = 120;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH