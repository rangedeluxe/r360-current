--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_CreatePartitionsForRange
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CreatePartitionsForRange') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CreatePartitionsForRange
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CreatePartitionsForRange
       @parmPartitionIdentifier VARCHAR(20),
       @parmBeginDate DATETIME, 
       @parmEndDate DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/24/2009
*
* Purpose: This SP will call the partition check/create SPs for a range of dates.
*      It accepts a date range and Partition Identifier and will call the checker
*      once for each week/month in the range based on the range setting for that 
*      Partition Identifier.
*
* Modification History
* 03/24/2009 CR 25817 JJR	Created
* 03/13/2013 WI 90600 JBS	Update to 2.0 release. Change Schema Name.
******************************************************************************/
SET NOCOUNT ON 
DECLARE @PartitionMethod TINYINT,
		@i INT,
		@DateValue DATETIME

SELECT @PartitionMethod = RangeSetting --0:none/1:weekly/2:monthly
FROM RecHubSystem.PartitionManager
WHERE PartitionIdentifier=@parmPartitionIdentifier

IF @PartitionMethod=0
       RETURN 0

BEGIN TRY
       --Get 1 day per week/month to pass into partition checker
       DECLARE @Dates TABLE (RowID INT IDENTITY(1,1), DateValue DATETIME)
       INSERT INTO @Dates
       SELECT @parmBeginDate
       UNION 
       SELECT CalendarDate
       FROM RecHubData.dimDates
       WHERE CalendarDate>=@parmBeginDate AND CalendarDate<=@parmEndDate
              AND CalendarDayName=CASE @PartitionMethod WHEN 1 THEN 'Monday' ELSE CalendarDayName END
              AND CalendarDayMonth=CASE @PartitionMethod WHEN 2 THEN 1 ELSE CalendarDayMonth END
       UNION
       SELECT @parmEndDate

       --pass each day into partition checker to create partitions
       SET @i=1
       WHILE @i<=(SELECT MAX(RowID) FROM @Dates)
       BEGIN
              SELECT @DateValue=DateValue
              FROM @Dates
              WHERE RowID=@i

              EXEC RecHubSystem.usp_CheckforValidPartition @DateValue, @parmPartitionIdentifier
          
              SET @i=@i+1
       END 
END TRY
BEGIN CATCH
	UPDATE RecHubSystem.PartitionManager 
	SET IsLocked = 0
	WHERE PartitionIdentifier = @parmPartitionIdentifier
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
