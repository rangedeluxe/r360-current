﻿--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataImportTracking_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportTracking_Upd') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportTracking_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportTracking_Upd
(
	@parmSourceTrackingId UNIQUEIDENTIFIER,
	@parmFileStatus INT,
	@parmModificationDate DATETIME2(7),
	@parmModifiedBy VARCHAR(128)
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Marco Aguirre
* Date: 06/01/2018
*
* Purpose: updates a single data import job 
*	
*
* Modification History
* 06/28/2018 PT 158332383 MA	Created
******************************************************************************/


BEGIN TRY
	UPDATE 
		RecHubSystem.DataImportTracking 
	SET	   
		FileStatus = @parmFileStatus,
		ModificationDate = @parmModificationDate,
		ModifiedBy  = @parmModifiedBy
	WHERE  
		SourceTrackingId = @parmSourceTrackingId;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
