--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingOLOrganizations
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CDSQueue_Get_PendingOLOrganizations') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingOLOrganizations;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingOLOrganizations
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/16/2013
*
* Purpose: Retreive list of OLOrganizations that are ready for processing.
*
* ActionCode:
*	1 - Insert/Update
*	2 - Delete

* QueueStatus:
*	10 - Ready to sent to integraPAY Consolidated Database
*	15 - Failed processing on integraPAY Consolidated Database - but can resend
*	20 - Sent to integraPAY Consolidated Database, waiting for a response
*	30 - Failed processing on integraPAY Consolidated Database - but cannot resend
*	99 - Successfully processed on integraPAY Consolidated Database
*
*
* QueueType:
*	10 - OLOrganizationID
*	20 - OLClientAccountID
*	70 - Document Type
*
* Modification History
* 06/16/2013 WI 106123 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH PendingList AS
	(
		SELECT 	
			TOP(150000) MIN(RecHubSystem.CDSQueue.CDSQueueID) AS RecordID,RecHubSystem.CDSQueue.OLOrganizationID
		FROM 	
			RecHubSystem.CDSQueue
		WHERE 	
			RecHubSystem.CDSQueue.QueueType = 10
			AND RecHubSystem.CDSQueue.QueueStatus <= 20
		GROUP BY 
			RecHubSystem.CDSQueue.OLOrganizationID
		ORDER BY RecordID
	)
	SELECT	
		PendingList.RecordID AS OLOrganization_Id,
		RecHubUser.OLOrganizations.OLOrganizationID AS OLCustomerID,
		RecHubUser.OLOrganizations.OrganizationCode AS CustomerCode,
		RecHubUser.OLOrganizations.[Description],
		RecHubUser.OLOrganizations.IsActive,
		RecHubUser.OLOrganizations.ViewingDays,
		RecHubUser.OLOrganizations.ExternalID1,
		RecHubUser.OLOrganizations.ExternalID2,
		RecHubUser.OLOrganizations.CreationDate,
		RecHubUser.OLOrganizations.CreatedBy,
		RecHubUser.OLOrganizations.ModificationDate,
		RecHubUser.OLOrganizations.ModifiedBy,		
		RecHubUser.OLOrganizations.MaximumSearchDays,
		RecHubUser.OLOrganizations.DocumentImageDisplayMode,
		RecHubUser.OLOrganizations.CheckImageDisplayMode,
		RecHubSystem.CDSQueue.ActionCode
	FROM	
		RecHubSystem.CDSQueue 
		INNER JOIN PendingList ON RecHubSystem.CDSQueue.CDSQueueID = PendingList.RecordID
		INNER JOIN RecHubUser.OLOrganizations ON RecHubUser.OLOrganizations.OLOrganizationID = PendingList.OLOrganizationID
	WHERE 	
		(RecHubSystem.CDSQueue.QueueStatus = 10) 
		OR (RecHubSystem.CDSQueue.QueueStatus = 20) 
		OR (RecHubSystem.CDSQueue.QueueStatus = 15 AND RecHubSystem.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RecHubSystem.CDSQueue.RetryCount),GETDATE()));

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
