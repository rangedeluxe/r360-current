--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICON_factTransactionSummary_Upd_TotalAmount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICON_factTransactionSummary_Upd_TotalAmount') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_factTransactionSummary_Upd_TotalAmount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_factTransactionSummary_Upd_TotalAmount
        @XBatch					     XML,
        @parmBankKey			     INT,
        @parmOrganizationKey	     INT,
        @parmClientAccountKey	     INT, 
        @parmImmutableDateKey        INT,
        @parmDepositDateKey		     INT,
		@parmSourceProcessingDateKey INT,
        @parmBatchID	    	     BIGINT,
		@parmSourceBatchID           BIGINT,
        @parmSourceIdentifier	     SMALLINT,
        @parmModificationDate	     DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/12/2010
*
* Purpose: Updates factTransactionSummary.Check and Stub Totals row from factChecks.Amount
*	table based on XML document.
*
* Modification History
* 04/12/2010 CR 29355 JPB	Created.
* 02/19/2015 WI 173990 TWE  Converted from 1.05 to 2.01
*                           Handle single transaction ID
* 06/09/2015 WI 217503 JAW  Update stub totals
* 06/09/2015 WI 217503 JBS	Renaming from RecHubData.usp_factTransactionSummary_Upd_CheckTotal
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
    DECLARE @bLocalTransaction	       BIT,
            @mCheckTotal               MONEY,
            @mStubTotal                MONEY,
			@TransactionID             INT,
			@factTransactionSummaryKey BIGINT;

    IF @@TRANCOUNT = 0
    BEGIN
        BEGIN TRANSACTION  
        SET @bLocalTransaction = 1;
    END

	SELECT	
	    @TransactionID = Transactions.att.value('@TransactionID', 'int') 
    FROM		
	    @XBatch.nodes('/Batches/Batch/Transaction') Transactions(att);  

    --What is the total amount of the checks
    SELECT
	    @mCheckTotal = SUM(RecHubData.factChecks.Amount)
    FROM
	    RecHubData.factChecks
	WHERE
	    RecHubData.factChecks.BankKey = @parmBankKey
		AND RecHubData.factChecks.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factChecks.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factChecks.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factChecks.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factChecks.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factChecks.TransactionID = @TransactionID
		AND RecHubData.factChecks.IsDeleted = 0;

    --What is the total amount of the stubs
    SELECT
	    @mStubTotal = SUM(RecHubData.factStubs.Amount)
    FROM
	    RecHubData.factStubs
	WHERE
	    RecHubData.factStubs.BankKey = @parmBankKey
		AND RecHubData.factStubs.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factStubs.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factStubs.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factStubs.TransactionID = @TransactionID
		AND RecHubData.factStubs.IsDeleted = 0;

    -------------------------------------------------------------------
	-- find the factTransactionSummaryKey of the record we want to update.
	-- then insert a new record using the factBatchSummaryKey 
	-- then flag the original record as deleted
	SELECT
	    @factTransactionSummaryKey = RecHubData.factTransactionSummary.factTransactionSummaryKey
	FROM 
	    RecHubData.factTransactionSummary
	WHERE
	    RecHubData.factTransactionSummary.BankKey = @parmBankKey
		AND RecHubData.factTransactionSummary.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factTransactionSummary.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factTransactionSummary.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factTransactionSummary.TransactionID = @TransactionID
		AND RecHubData.factTransactionSummary.IsDeleted = 0;


	INSERT INTO RecHubData.factTransactionSummary
	(
	    --factTransactionSummaryKey
        IsDeleted
        ,BankKey
        ,OrganizationKey
        ,ClientAccountKey
        ,DepositDateKey
        ,ImmutableDateKey
        ,SourceProcessingDateKey
        ,BatchID
        ,SourceBatchID
        ,BatchNumber
        ,BatchSourceKey
        ,BatchPaymentTypeKey
        ,BatchCueID
        ,DepositStatus
        ,SystemType
        ,TransactionID
        ,TxnSequence
        ,CheckCount
        ,ScannedCheckCount
        ,StubCount
        ,DocumentCount
        ,OMRCount
        ,CheckTotal
        ,StubTotal
        ,CreationDate
        ,ModificationDate
        ,BatchSiteCode
        ,TransactionExceptionStatusKey
	)
	SELECT 
	    --factTransactionSummaryKey
        IsDeleted
        ,BankKey
        ,OrganizationKey
        ,ClientAccountKey
        ,DepositDateKey
        ,ImmutableDateKey
        ,SourceProcessingDateKey
        ,BatchID
        ,SourceBatchID
        ,BatchNumber
        ,BatchSourceKey
        ,BatchPaymentTypeKey
        ,BatchCueID
        ,DepositStatus
        ,SystemType
        ,TransactionID
        ,TxnSequence
        ,CheckCount
        ,ScannedCheckCount
        ,StubCount
        ,DocumentCount
        ,OMRCount        
        ,COALESCE(@mCheckTotal, 0) AS CheckTotal
        ,COALESCE(@mStubTotal, 0)  AS StubTotal
        ,@parmModificationDate     AS CreationDate
        ,@parmModificationDate     AS ModificationDate
        ,BatchSiteCode
        ,TransactionExceptionStatusKey
    FROM RecHubData.factTransactionSummary
    WHERE
      RecHubData.factTransactionSummary.factTransactionSummaryKey = @factTransactionSummaryKey;

	--  Now go flag the record as deleted
    UPDATE
	    RecHubData.factTransactionSummary
	SET
	    RecHubData.factTransactionSummary.IsDeleted = 1,
		RecHubData.factTransactionSummary.ModificationDate = @parmModificationDate
	WHERE
	    RecHubData.factTransactionSummary.factTransactionSummaryKey = @factTransactionSummaryKey;

    IF @bLocalTransaction = 1 
    BEGIN
        COMMIT TRANSACTION 
        SET @bLocalTransaction = 0;
    END

END TRY

BEGIN CATCH
    IF @bLocalTransaction = 1
      BEGIN --local transaction, handle the error
        IF (XACT_STATE()) = -1 --transaction is uncommittable
            ROLLBACK TRANSACTION
        IF (XACT_STATE()) = 1 --transaction is active and valid
            COMMIT TRANSACTION
        SET @bLocalTransaction = 0
      END
    ELSE
      BEGIN
        -- the transaction did not start here, so pass the error information up to the calling SP.
        DECLARE @ErrorMessage	NVARCHAR(4000),
                @ErrorSeverity	INT,
                @ErrorState		INT,
                @ErrorLine		INT
        SELECT	@ErrorMessage = ERROR_MESSAGE(),
                @ErrorSeverity = ERROR_SEVERITY(),
                @ErrorState = ERROR_STATE(),
                @ErrorLine = ERROR_LINE()

        IF @@NESTLEVEL > 3 --update only if we got here by two service broker calls
            SET @ErrorMessage = 'usp_ICON_factTransactionSummary_Upd_TotalAmount (Line: %d)-> ' + @ErrorMessage
        RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
      END
END CATCH
