--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICON_factStubs_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICON_factStubs_Upd') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_factStubs_Upd
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_factStubs_Upd
		@XBatch					      XML,
		@parmBankKey			      INT,
		@parmOrganizationKey	      INT,
		@parmClientAccountKey	      INT, 
		@parmSourceProcessingDateKey  INT,
		@parmImmutableDateKey	      INT,
		@parmDepositDateKey		      INT,
		@parmBatchID			      BIGINT,
		@parmSourceBatchID		      BIGINT,
		@parmBatchNumber              INT,
		@parmBatchSourceKey           SMALLINT,
		@parmBatchPaymentTypeKey      SMALLINT,
		@parmDepositStatus            INT,
		@parmSourceIdentifier	      SMALLINT,
		@parmModificationDate	      DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/10/2010
*
* Purpose: Updates factStubs row(s) from XBatch Stubs XML segment
*
* Modification History
* 02/09/2010 CR 28998 JPB	Created.
* 04/05/2010 CR 29304 JPB   Renamed Account to AccountNumber.
* 05/28/2010 CR	29763 JPB	Reformat XML and call usp_factDataEntryDetailsUpdate
*							to update factDataEntryDetail stub records.
* 02/19/2015 WI 173987 TWE  Converted from 1.05 to 2.01
*                           Add existing DataEntry node to New XML
* 06/09/2015 WI 173987 JBS	Renaming from RecHubData.usp_factStubs_Upd
* 08/18/2015 WI 225673 TWE  Remove old column references, and add new column references
* 08/22/2016 PT 128448991	JPB	Changed @parmBatchSourceKey to SMALLINT. 
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

DECLARE @bLocalTransaction	BIT,
		@XDataEntry         XML;

BEGIN TRY

	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1;
	END
	

	IF OBJECT_ID('tempdb..#StubKeys') IS NOT NULL
        DROP TABLE #StubKeys;

	CREATE TABLE #StubKeys
	(
	     factStubKey    BIGINT
	);

	--create a table to store the XML data
	IF OBJECT_ID('tempdb..#StubUpdate') IS NOT NULL
        DROP TABLE #StubUpdate;

	CREATE TABLE #StubUpdate
	(
       BankKey           INT         NOT NULL,
       OrganizationKey   INT         NOT NULL,
       ClientAccountKey  INT         NOT NULL,
       ImmutableDateKey  INT         NOT NULL,
       DepositDateKey    INT         NOT NULL,
       --GlobalBatchID INT NULL,
       BatchID           BIGINT      NOT NULL,
	   SourceBatchID     BIGINT      NOT NULL,
       TransactionID     INT         NOT NULL,
       BatchSequence     INT         NOT NULL,
       AccountNumber     VARCHAR(80) NULL,
       Amount            MONEY       NULL,
       ModificationDate  DATETIME    NOT NULL
	);
	
	
	INSERT INTO #StubUpdate
	(
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		ImmutableDateKey,
		DepositDateKey,
		BatchID,
		SourceBatchID,
		TransactionID,
		BatchSequence,
		AccountNumber,
		Amount,
		ModificationDate
	)
	SELECT	
	    @parmBankKey           AS BankKey,
		@parmOrganizationKey   AS OrganizationKey,
		@parmClientAccountKey  AS ClientAccountKey,
		@parmImmutableDateKey  AS ProcessingDate,
		@parmDepositDateKey    AS DepositDate,
		@parmBatchID,
		@parmSourceBatchID,
		T.TransactionID, 
		Stubs.BatchSequence,
		Stubs.AccountNumber, 
		Stubs.Amount,
		@parmModificationDate  AS ModificationDate
	FROM	(
				SELECT 
				    Transactions.att.value('@BatchID', 'bigint')       AS BatchID,
					Transactions.att.value('@SourceBatchID', 'bigint') AS SourceBatchID,
				    Transactions.att.value('@TransactionID', 'int')    AS TransactionID,
				    Transactions.att.value('@Sequence', 'int')         AS Sequence
				FROM @XBatch.nodes('/Batches/Batch/Transaction') Transactions(att)
			) T
			INNER JOIN 
			(
				SELECT	
				    Stubs.att.value('@BatchID', 'bigint')            AS BatchID,
					Stubs.att.value('@SourceBatchID', 'bigint')      AS SourceBatchID,
					Stubs.att.value('@GlobalStubID', 'int')          AS GlobalStubID,
					Stubs.att.value('@TransactionID', 'int')         AS TransactionID,
					Stubs.att.value('@BatchSequence', 'int')         AS BatchSequence,
					Stubs.att.value('@Amount', 'money')              AS Amount,
					Stubs.att.value('@AccountNumber', 'varchar(80)') AS AccountNumber
				FROM @XBatch.nodes('/Batches/Batch/Transaction/Stubs') Stubs(att)
			) Stubs 
			    ON Stubs.BatchID = T.BatchID 
				   AND Stubs.SourceBatchID = T.SourceBatchID 
				   AND Stubs.TransactionID = T.TransactionID;


    --Go collect the keys of the records
    INSERT INTO #StubKeys
	(
	    factStubKey
	)
	SELECT
	    RecHubData.factStubs.factStubKey
	FROM	
	    RecHubData.factStubs
		INNER JOIN #StubUpdate UpdateTable 
		    ON RecHubData.factStubs.BankKey = UpdateTable.BankKey
		   	    AND RecHubData.factStubs.OrganizationKey = UpdateTable.OrganizationKey
			    AND RecHubData.factStubs.ClientAccountKey = UpdateTable.ClientAccountKey
			    AND RecHubData.factStubs.ImmutableDateKey = UpdateTable.ImmutableDateKey
			    AND RecHubData.factStubs.DepositDateKey = UpdateTable.DepositDateKey
			    AND RecHubData.factStubs.BatchID = UpdateTable.BatchID
				AND RecHubData.factStubs.SourceBatchID = UpdateTable.SourceBatchID
			    AND RecHubData.factStubs.TransactionID = UpdateTable.TransactionID
			    AND RecHubData.factStubs.BatchSequence = UpdateTable.BatchSequence
				AND RecHubData.factStubs.IsDeleted = 0;


    --update the factStubs table by joining the xml data from the local table
    --COALESCE because the update record may not contain each field
	INSERT INTO	RecHubData.factStubs
	(
	    IsDeleted
        ,BankKey
        ,OrganizationKey
        ,ClientAccountKey
        ,DepositDateKey
        ,ImmutableDateKey
        ,SourceProcessingDateKey
        ,BatchID
        ,SourceBatchID
        ,BatchNumber
        ,BatchSourceKey
        ,BatchPaymentTypeKey
        ,BatchCueID
        ,DepositStatus
        ,SystemType
        ,TransactionID
        ,TxnSequence
        ,SequenceWithinTransaction
        ,BatchSequence
        ,StubSequence
        ,DocumentBatchSequence
        ,IsOMRDetected
        ,CreationDate
        ,ModificationDate
        ,BatchSiteCode
        ,Amount
        ,AccountNumber
	)
	SELECT
	    RecHubData.factStubs.IsDeleted
        ,RecHubData.factStubs.BankKey
        ,RecHubData.factStubs.OrganizationKey
        ,RecHubData.factStubs.ClientAccountKey
        ,RecHubData.factStubs.DepositDateKey
        ,RecHubData.factStubs.ImmutableDateKey
        ,RecHubData.factStubs.SourceProcessingDateKey
        ,RecHubData.factStubs.BatchID
        ,RecHubData.factStubs.SourceBatchID
        ,RecHubData.factStubs.BatchNumber
        ,RecHubData.factStubs.BatchSourceKey
        ,RecHubData.factStubs.BatchPaymentTypeKey
        ,RecHubData.factStubs.BatchCueID
        ,RecHubData.factStubs.DepositStatus
        ,RecHubData.factStubs.SystemType
        ,RecHubData.factStubs.TransactionID
        ,RecHubData.factStubs.TxnSequence
        ,RecHubData.factStubs.SequenceWithinTransaction
        ,RecHubData.factStubs.BatchSequence
        ,RecHubData.factStubs.StubSequence
        ,RecHubData.factStubs.DocumentBatchSequence
        ,RecHubData.factStubs.IsOMRDetected
        ,RecHubData.factStubs.CreationDate
        ,UpdateTable.ModificationDate                                           AS ModificationDate
        ,RecHubData.factStubs.BatchSiteCode
        ,COALESCE(UpdateTable.Amount,RecHubData.factStubs.Amount)               AS Amount
        ,COALESCE(UpdateTable.AccountNumber,RecHubData.factStubs.AccountNumber) AS AccountNumber
	FROM	
	    RecHubData.factStubs
		INNER JOIN #StubUpdate UpdateTable 
		    ON RecHubData.factStubs.BankKey = UpdateTable.BankKey
		   	    AND RecHubData.factStubs.OrganizationKey = UpdateTable.OrganizationKey
			    AND RecHubData.factStubs.ClientAccountKey = UpdateTable.ClientAccountKey
			    AND RecHubData.factStubs.ImmutableDateKey = UpdateTable.ImmutableDateKey
			    AND RecHubData.factStubs.DepositDateKey = UpdateTable.DepositDateKey
			    AND RecHubData.factStubs.BatchID = UpdateTable.BatchID
			    AND RecHubData.factStubs.TransactionID = UpdateTable.TransactionID
			    AND RecHubData.factStubs.BatchSequence = UpdateTable.BatchSequence
				AND RecHubData.factStubs.IsDeleted = 0;

    /*--- now logically delete the actual record  ---*/
	UPDATE
	    RecHubData.factStubs
    SET 
	    RecHubData.factStubs.IsDeleted = 1,
		RecHubData.factStubs.ModificationDate = @parmModificationDate
	FROM
	    RecHubData.factStubs
		INNER JOIN #StubKeys stubKey
		    ON RecHubData.factStubs.factStubKey = stubKey.factStubKey;

	/*----------------------------------------------------------------------
	--  Part 2:
	--reformat the stub data into a data entry XML format
	------------------------------------------------------------------------*/
	IF OBJECT_ID('tempdb..#StubsDataEntry') IS NOT NULL
        DROP TABLE #StubsDataEntry;

	CREATE TABLE #StubsDataEntry
	(
		TransactionID         INT,
		BatchSequence         INT,
		DocumentBatchSequence INT,
		IsCheck               BIT,
		FieldName             NVARCHAR(256),
		SourceDisplayName     NVARCHAR(256),
		DataEntryValue        VARCHAR(256)
	)

	/* Now move the existing data entry updates to the table */
	Insert into #StubsDataEntry
	(
	    TransactionID         ,
		BatchSequence         ,
		DocumentBatchSequence ,
		IsCheck               ,
		FieldName             ,
		SourceDisplayName     ,
		DataEntryValue        
	)
	SELECT	
		DataEntry.att.value('@TransactionID', 'int')                    AS TransactionID,		
		DataEntry.att.value('@BatchSequence','int')                     AS BatchSequence,	    
		DataEntry.att.value('@DocumentBatchSequence','int')             AS DocumentBatchSequence,
		DataEntry.att.value('@IsCheck','bit')                           AS IsCheck,
		DataEntry.att.value('@FieldName', 'nvarchar(256)')              AS FieldName, 
		DataEntry.att.value('@SourceDisplayName', 'nvarchar(256)')      AS SourceDisplayName,
		DataEntry.att.value('@DataEntryValue', 'varchar(256)')          AS DataEntryValue		
	FROM	
		@XBatch.nodes('/Batches/Batch/DataEntry/DataEntry') DataEntry(att);

	
	--The DEUpdate SP needs the XML formatted correctly
	SET @XDataEntry = 
	(
		SELECT 	Batch.att.value('@BatchID','bigint')             AS BatchID,
		        Batch.att.value('@SourceBatchID','bigint')       AS SourceBatchID,
				Batch.att.value('@BankID', 'int')                AS BankID,
				Batch.att.value('@CustomerID', 'int')            AS CustomerID,
				Batch.att.value('@ClientAccountID', 'int')       AS ClientAccountID,
				Batch.att.value('@SiteID', 'int')                AS SiteID,
				Batch.att.value('@ImmutableDate', 'datetime')    AS ImmutableDate,
				Batch.att.value('@SourceIdentifier', 'smallint') AS SourceIdentifier,
				(
					SELECT	TransactionID,
					        BatchSequence,
							DocumentBatchSequence,
							IsCheck,
							FieldName,
							SourceDisplayName,
							DataEntryValue
					FROM	#StubsDataEntry AS DataEntry --use an alias so the XML Node name is correct
					FOR XML AUTO, TYPE
				) DataEntry
		FROM	@XBatch.nodes('/Batches/Batch') Batch (att)
		FOR XML AUTO, TYPE, ROOT('Batches')
	);

	--TO DO: here is another place we need to figure out how to capture the stats
	EXEC RecHubData.usp_factDataEntryDetails_Upd_DataEntryValues @XDataEntry, 
	                                                             @parmBankKey, 
																 @parmOrganizationKey,
																 @parmClientAccountKey, 
																 @parmSourceProcessingDateKey,
																 @parmDepositDateKey,
																 @parmImmutableDateKey,  
																 @parmBatchID,
																 @parmSourceBatchID,
																 @parmBatchNumber,
																 @parmBatchSourceKey,
																 @parmBatchPaymentTypeKey,
																 @parmDepositStatus, 
																 @parmSourceIdentifier, 
																 @parmModificationDate;
	--CR 29763 End

	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0
	END

END TRY
BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage	NVARCHAR(4000),
				@ErrorSeverity	INT,
				@ErrorState		INT,
				@ErrorLine		INT
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE()

		IF @@NESTLEVEL > 3 --update only if we got here by two service broker calls
			SET @ErrorMessage = 'usp_ICON_factStubs_Upd (Line: %d)-> ' + @ErrorMessage
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
END CATCH
