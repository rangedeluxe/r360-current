--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_SetControlValue
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_SetControlValue') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_SetControlValue
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE RecHubSystem.usp_Purge_SetControlValue
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: Storing Value for purging 
*	
*
* Modification History
* 06/01/2013 WI 117322 PS	Created.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @Count11 INT, 
		@Count12 INT,
		@Count21 INT, 
		@Count22 INT,
		@Count31 INT, 
		@Count32 INT,
		@Percent1 FLOAT, 
		@D1 FLOAT, 
		@N1 FLOAT,
		@Percent2 FLOAT, 
		@D2 FLOAT, 
		@N2 FLOAT,
		@Percent3 FLOAT, 
		@D3 FLOAT, 
		@N3 FLOAT;

BEGIN TRY
	SET  @Count11 = ( SELECT COUNT(*) FROM RecHubData.factDocuments WHERE IsDeleted =1 );
	SET  @Count12 = ( SELECT COUNT(*) FROM RecHubData.factDocuments );
	SET  @Count12=@count12 + 1;

	SET @N1 = @Count11;
	SET @D1 = @Count12;

	SET @Percent1 = @N1/@D1;

	IF(  @Percent1 >= 0.05 )
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factDocuments',@N1,1);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factDocuments',@N1,0);
	END 
  
	SET  @Count21 = ( SELECT COUNT(*) FROM RecHubData.factChecks WHERE IsDeleted =1 );
	SET  @Count22 = ( SELECT COUNT(*) FROM RecHubData.factChecks );
	SET  @Count22 = @count22 + 1;

	SET @N2 = @Count21;
	SET @D2 = @Count22;

	SET @Percent2 = @N2/@D2;

	IF  @Percent2 >= 0.05
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factChecks',@N2,1);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factChecks',@N2,0);
	END 

	SET  @Count31 =  ( SELECT COUNT(*) FROM RecHubData.factDataEntryDetails WHERE IsDeleted =1 );
	SET  @Count32 =  ( SELECT COUNT(*) FROM RecHubData.factDataEntryDetails );
	SET  @Count32 = @count32 + 1;

	SET @N3 = @Count31;
	SET @D3 = @Count32;

	SET @Percent3 = @N3/@D3;

	IF  @Percent3 >= 0.05
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factDataEntryDetails',@N3,1);
	END
	ELSE 
	BEGIN
		INSERT INTO RecHubSystem.PurgeSSISHistory(TableName,IsDeletedCount,ExceedThreshold)
		VALUES('RecHubData.factDataEntryDetails',@N3,0);
	END 
    
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
