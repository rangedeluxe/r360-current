--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_CheckforValidPartition
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CheckforValidPartition') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CheckforValidPartition
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CheckforValidPartition
       @parmPartitionDate SMALLDATETIME, 
       @parmPartitionIdentifier VARCHAR(20)
AS
/******************************************************************************
** Wausau Financial Systems
** Copyright 2008-2013 Wausau Financial Systems All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau Financial Systems 2008-2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau Financial Systems and contain Wausau Financial Systems 
* trade secrets.  These materials may not be used, copied, modified or disclosed 
* except as expressly permitted in writing by Wausau Financial Systems (see 
* the Wausau Financial Systems license agreement for details).  All copies, 
* modifications and derivative works of these materials are property of Wausau 
* Financial Systems.
*
* Author: JJR
* Date: 4/20/2008
*
* Purpose:    Checks for the existance of a valid partition for the partition identifier
*             based on the partition date.  If no valid parition exists, one will be 
*             created.
*
* Modification History
* 04/20/2008 CR 25817 JJR	Created
* 03/13/2013 WI 90593 JBS	Update to 2.0 release.  Change Schema Name.
******************************************************************************/
SET NOCOUNT ON        

DECLARE @ReturnValue INT,
       @PartitionFunction SYSNAME,
       @PartitionScheme SYSNAME,
       @PartitionFromDate SMALLDATETIME,
       @PartitionToDate SMALLDATETIME,
       @PartitionMethod SMALLINT,
       @PreviousValidDate SMALLDATETIME, 
       @NextValidDate SMALLDATETIME, 
       @PartitionManagerID INT, 
       @FileSize INT, 
       @FirstDiskID INT, 
       @NextDiskID INT,
       @LastDiskID INT, 
       @DiskPath VARCHAR(1000),
       @NewFGFileName VARCHAR(1000),
       @SQLcmd VARCHAR(4000),
       @IsLockedUpdated BIT

SET @ReturnValue = 0
SET @IsLockedUpdated = 0

--Ensure that dates are always midnight
SET @parmPartitionDate = CAST(CONVERT(VARCHAR,@parmPartitionDate,112) as SMALLDATETIME)

BEGIN TRY        
       --Get setup values from OLTA.PartitionManager
       SELECT @PartitionManagerID = PartitionManagerID,
              @PartitionFunction = PartitionFunctionName,
              @PartitionScheme = PartitionSchemeName,
              @PartitionMethod = RangeSetting,    --0: No Partitioning / 1: weekly / 2: Monthly
              @FileSize = SizeMB
       FROM RecHubSystem.PartitionManager
       WHERE PartitionIdentifier = @parmPartitionIdentifier       

       IF @PartitionMethod = 0 --Exit if No Partitioning
              RETURN @ReturnValue

       --Force serialization from here out, we dont want this piece multi-threaded
       WHILE @IsLockedUpdated = 0
       BEGIN
              UPDATE RecHubSystem.PartitionManager
              SET IsLocked = 1
              WHERE PartitionIdentifier=@parmPartitionIdentifier
                     AND IsLocked = 0

              SET @IsLockedUpdated=CAST(@@ROWCOUNT as BIT)
              WAITFOR DELAY '00:00:01'
       END

       --find the closest partition range value less than PartitionDate
       SELECT @PartitionFromDate = CAST(CAST(MAX(prv.Value) as VARCHAR) as SMALLDATETIME)
       FROM sys.partition_range_values prv
              INNER JOIN sys.partition_functions pf
              ON prv.function_id = pf.function_id
       WHERE prv.Value <= CAST(CONVERT(VARCHAR, @parmPartitionDate, 112) as int)
              AND pf.name = @PartitionFunction

       --find the closest partition range value greater than PartitionDate
       SELECT @PartitionToDate = CAST(CAST(MIN(prv.Value) as VARCHAR) as SMALLDATETIME)
       FROM sys.partition_range_values prv
              INNER JOIN sys.partition_functions pf
              ON prv.function_id = pf.function_id
       WHERE prv.Value > CAST(CONVERT(VARCHAR, @parmPartitionDate, 112) as INT)
              AND pf.name = @PartitionFunction

       --get disk info for the partition
       SELECT @FirstDiskID = MIN(DiskOrder),
              @LastDiskID = MAX(DiskOrder) 
       FROM RecHubSystem.PartitionDisks
       WHERE PartitionManagerID = @PartitionManagerID

       --Find what the previous partition date should be based on setting weekly/monthly
       SET DATEFIRST 1
       SELECT @PreviousValidDate = 
              CASE @PartitionMethod 
                     WHEN 1 THEN DATEADD(DD, 1 - DATEPART(DW, @parmPartitionDate),@parmPartitionDate)
                     WHEN 2 THEN DATEADD(DD, 1 - DATEPART(DD, @parmPartitionDate),@parmPartitionDate)	
              END 

       --Find what the next partition date should be based on setting weekly/monthly
       SELECT @NextValidDate = 
              CASE @PartitionMethod 
                     WHEN 1 THEN DATEADD(DD, 7,@PreviousValidDate)
                     WHEN 2 THEN DATEADD(m,1,@PreviousValidDate)	
              END

       --If last partition date is less than the previous valid date, create that partition
       IF @PartitionFromDate < @PreviousValidDate OR @PartitionFromDate IS NULL
       BEGIN
              SELECT @NextDiskID=COALESCE(MIN(DiskOrder),@FirstDiskID)
              FROM RecHubSystem.PartitionManager PM
                     INNER JOIN RecHubSystem.PartitionDisks PD
                     ON PM.PartitionManagerID=PD.PartitionManagerID
              WHERE PartitionIdentifier = @parmPartitionIdentifier
                     AND DiskOrder > LastDiskID
              
              SET @NewFGFileName = @parmPartitionIdentifier + CONVERT(VARCHAR,@PreviousValidDate,112)
              EXEC RecHubSystem.usp_CreatePartitions 
                     @parmRangeDate = @PreviousValidDate, 
                     @parmPartitionScheme = @PartitionScheme, 
                     @parmPartitionFunction = @PartitionFunction, 
                     @parmFileGroupName = @NewFGFileName, 
                     @parmPartitionManagerID = @PartitionManagerID,
                     @parmFirstDiskID = @FirstDiskID,
                     @parmNextDiskID = @NextDiskID, 
                     @parmLastDiskID = @LastDiskID,
                     @parmFileSize = @FileSize
       END

       --If partition to date is not the next valid date, create that partition 
       IF @PartitionToDate > @NextValidDate OR @PartitionToDate IS NULL
       BEGIN
              SELECT @NextDiskID=COALESCE(MIN(DiskOrder),@FirstDiskID)
              FROM RecHubSystem.PartitionManager PM
                     INNER JOIN RecHubSystem.PartitionDisks PD
                     ON PM.PartitionManagerID=PD.PartitionManagerID
              WHERE PartitionIdentifier = @parmPartitionIdentifier
                     AND DiskOrder > LastDiskID
              
              SET @NewFGFileName = @parmPartitionIdentifier + CONVERT(VARCHAR,@NextValidDate,112)
              EXEC RecHubSystem.usp_CreatePartitions 
                     @parmRangeDate = @NextValidDate, 
                     @parmPartitionScheme = @PartitionScheme, 
                     @parmPartitionFunction = @PartitionFunction, 
                     @parmFileGroupName = @NewFGFileName, 
                     @parmPartitionManagerID = @PartitionManagerID,
                     @parmFirstDiskID = @FirstDiskID,
                     @parmNextDiskID = @NextDiskID, 
                     @parmLastDiskID = @LastDiskID,
                     @parmFileSize = @FileSize
       END

       --Release 'Lock' on process
       UPDATE RecHubSystem.PartitionManager
       SET IsLocked = 0
       WHERE PartitionIdentifier=@parmPartitionIdentifier

       RETURN @ReturnValue
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
