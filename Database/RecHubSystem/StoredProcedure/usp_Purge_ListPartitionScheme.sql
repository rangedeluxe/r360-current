--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_ListPartitionScheme
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_ListPartitionScheme') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_ListPartitionScheme
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE RecHubSystem.usp_Purge_ListPartitionScheme
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 07/01/2013
*
* Purpose: Creating a partion scheme to be used for generating table structure 
*	
*
* Modification History
* 07/01/2013 WI 117317 PS	Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( OBJECT_ID('dbo.WT_TablePartitionList', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_TablePartitionList
  
	SELECT  
		sys.schemas.name as TableSchema, 
		sys.tables.name as TableName, 
		sys.partition_schemes.name as PartitionschemeName, 
		sys.columns.name as ColumnName,
		'ON '+sys.partition_schemes.name + '(' + sys.columns.name + ') ' as cmdPartition
	INTO dbo.WT_TablePartitionList 
	FROM 
		sys.tables
		INNER JOIN sys.indexes ON (sys.indexes.object_id = sys.tables.object_id AND sys.indexes.index_id < 2)
		INNER JOIN sys.index_columns ON (sys.index_columns.partition_ordinal > 0 AND sys.index_columns.index_id = sys.indexes.index_id and sys.index_columns.object_id = sys.tables.object_id)
		INNER JOIN sys.columns
			on(sys.columns.object_id = sys.index_columns.object_id AND sys.columns.column_id = sys.index_columns.column_id)
		INNER JOIN sys.partition_schemes on sys.indexes.data_space_id= sys.partition_schemes.data_space_id
		INNER JOIN sys.schemas on sys.tables.schema_id=sys.schemas.schema_id
	ORDER BY  
		sys.schemas.name, 
		sys.tables.name;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
