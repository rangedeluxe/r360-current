﻿--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataImportTracking_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportTracking_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportTracking_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportTracking_Ins
(
	@parmSourceTrackingId UNIQUEIDENTIFIER,
	@parmFileStatus INT,
	@parmCreationDate DATETIME2(7),
	@parmCreatedBy VARCHAR(128)
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Marco Aguirre
* Date: 06/01/2018
*
* Purpose: creates a single data import job 
*	
*
* Modification History
* 06/28/2018 PT 158332383 MA	Created
******************************************************************************/

BEGIN TRY
	IF NOT EXISTS(SELECT 1 FROM RecHubSystem.DataImportTracking WHERE SourceTrackingId = @parmSourceTrackingId)
	BEGIN
		INSERT INTO RecHubSystem.DataImportTracking
		(
			SourceTrackingId,
			FileStatus,
			CreationDate,
			CreatedBy
		)
		VALUES
		(
			@parmSourceTrackingId,
			@parmFileStatus,
			@parmCreationDate,
			@parmCreatedBy
		);
	END 
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
