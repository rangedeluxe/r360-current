--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_IMSInterfaceQueue_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_Ins 
(
	@parmBankID				INT,
	@parmClientAccountID	INT, 
	@parmImmutableDate		DATETIME,
	@parmBatchID			INT,
	@parmClientProcessCode  VARCHAR(40)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 06/12/2012
*
* Purpose: 
*
* Modification History
* 06/12/2012 CR 53353 WJS	Created
* 08/15/2012 CR 53353 WJS	Remove SourceProcessingDateKey, change to ProcessingDateKey
* 10/19/2012 WI 70641 WJS	Add ClientProcessCode
* 02/06/2014 WI 128755 JBS	Update to 2.01 Schema.  Change name from usp_DataImportIntegrationServices_InsertIMSInterfaceQueue
*							Change all references to new schema.  Remove Use of dimDates table.
*							Renamed parms LockBox to ClientAccount, ProcessingDate to ImmutableDate
*					COMMENTED OUT CODE until later date
* 04/17/2014 WI 137291 JBS	Added permissions
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

--  Everything is commented out. This all will be worked out later

--DECLARE	@QueueDataStatus BIT,
--		@QueueImageStatus BIT,
--		@QueueType		INT,
--		@QueueStatus	INT,
--		@RetryCount		INT,
--		@QueueData		XML,
--		@ProcessingDateKey	INT;
		
--SET @QueueData =  NULL
--SET @QueueType = 1
--SET @RetryCount = 0
--SET @QueueStatus = 10
--SET @QueueDataStatus = 1
--SET @QueueImageStatus = 1

--BEGIN TRY

		--SELECT @ProcessingDateKey = CAST(CONVERT(VARCHAR,@parmProcessingDate,112) AS INT);	
			
		--INSERT INTO RecHubSystem.IMSInterfaceQueue(SiteBankID, SiteLockboxID, ProcessingDateKey,
		--		BatchID, ClientProcessCode, QueueType, QueueData, RetryCount, QueueDataStatus,  QueueImageStatus, QueueStatus)
		--	SELECT	@parmBankID,
		--			@parmLockboxID,
		--			@ProcessingDateKey,
		--			@parmBatchID,
		--			@parmClientProcessCode,
		--			@QueueType,
		--			@QueueData,
		--			@RetryCount,
		--			@QueueDataStatus,
		--		    @QueueImageStatus,
		--		    @QueueStatus
	
			
--END TRY
--BEGIN CATCH
--	EXEC RecHubCommon.usp_WfsRethrowException
--END CATCH