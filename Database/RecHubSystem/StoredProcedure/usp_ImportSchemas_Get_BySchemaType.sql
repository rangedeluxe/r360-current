--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ImportSchemas_Get_BySchemaType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ImportSchemas_Get_BySchemaType') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ImportSchemas_Get_BySchemaType
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ImportSchemas_Get_BySchemaType
(
	@parmSchemaType VARCHAR(128),
	@parmXSDVersion	VARCHAR(12)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/19/2009
*
* Purpose: This SP will return the latest .XSD schema revision for a Schema Type
*
* Modification History
* 03/19/2009 CR 25817 JJR	Created
* 02/10/2012 CR 50142 JPB	Changed @parmSchemaType to 128.
* 04/18/2013 WI 90677 JBS	Update to 2.0 release. Change Schema to RecHubSystem
*							Rename proc FROM usp_GetCurrentSchemaRevision
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY

	SELECT RecHubSystem.ImportSchemas.ImportSchema
	FROM RecHubSystem.ImportSchemas
			INNER JOIN RecHubSystem.ImportSchemaTypes 
			ON RecHubSystem.ImportSchemas.ImportSchemaTypeID = RecHubSystem.ImportSchemaTypes.ImportSchemaTypeID
	WHERE	RecHubSystem.ImportSchemaTypes.ImportSchemaType = @parmSchemaType
			AND RecHubSystem.ImportSchemas.XSDVersion = @parmXSDVersion;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
