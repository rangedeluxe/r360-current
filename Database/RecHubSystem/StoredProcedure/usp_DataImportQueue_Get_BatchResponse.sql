--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_BatchResponse
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_Get_BatchResponse') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_Get_BatchResponse
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_Get_BatchResponse
(
	@parmClientProcessCode VARCHAR(40) = 'Unassigned'
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: 
*
* Modification History
* 01/24/2012 CR 50173 JPB	Created
* 02/08/2012 CR 53927 CEJ 	Alter this proc so that it returns QueueStatus of both 99 and 120
* 09/27/2012 CR 56081 JPB 	Added ClientProcessCode filter.
* 04/23/2013 WI 90610 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_DataImportIntegrationServices_GetResponseBatch
* 02/02/2016 WI 259524 JPB	Added AuditDateKey as the last attribute in the Batch node
* 05/17/2016 WI 280516 JPB	Sort by CreationDate. (FP:277542)
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @Loop				INT,
		@SourceTrackingID	UNIQUEIDENTIFIER,
		@ResponseTrackingID UNIQUEIDENTIFIER,
		@Msg				VARCHAR(MAX);

BEGIN TRY
	DECLARE @Responses TABLE
	(
		RowID				INT NOT NULL IDENTITY(1,1),
		SourceTrackingID	UNIQUEIDENTIFIER NOT NULL,
		XMLResponseDocument XML NULL
	);

	SELECT @ResponseTrackingID = NEWID();

	INSERT INTO @Responses(SourceTrackingID)
	SELECT	RecHubSystem.DataImportQueue.SourceTrackingID
	FROM	RecHubSystem.DataImportQueue
	WHERE	RecHubSystem.DataImportQueue.QueueType = 1 
			AND RecHubSystem.DataImportQueue.QueueStatus IN (30,99, 120)
			AND RecHubSystem.DataImportQueue.ClientProcessCode = @parmClientProcessCode
	GROUP BY SourceTrackingID
	ORDER BY MIN(CreationDate);

	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Responses) )
	BEGIN
		SET @Msg = '';

		SELECT	@SourceTrackingID = SourceTrackingID
		FROM	@Responses
		WHERE	RowID = @Loop;
		
		SELECT	@Msg = @Msg + CAST(RecHubSystem.DataImportQueue.XMLResponseDocument AS VARCHAR(MAX))
		FROM	RecHubSystem.DataImportQueue 
		WHERE	RecHubSystem.DataImportQueue.SourceTrackingID = @SourceTrackingID
				AND RecHubSystem.DataImportQueue.QueueStatus IN (30,99, 120)
		ORDER BY CreationDate;
		
		UPDATE	@Responses 
		SET		XMLResponseDocument = CAST(@Msg AS XML)
		WHERE	SourceTrackingID = @SourceTrackingID;
		
		SET @Loop = @Loop + 1;
	END

	/* WI 259524 Add the AuditDateKey to the response XML */
	UPDATE 
		@Responses
	SET
		XMLResponseDocument.modify('insert attribute AuditDateKey {sql:column("AuditDateKey")} as last into (/Batch)[1]')
	FROM
		@Responses R
		INNER JOIN RecHubSystem.DataImportQueue ON RecHubSystem.DataImportQueue.SourceTrackingID = R.SourceTrackingID

	UPDATE	RecHubSystem.DataImportQueue
	SET		RecHubSystem.DataImportQueue.ResponseTrackingID = @ResponseTrackingID,
			RecHubSystem.DataImportQueue.QueueStatus = 120,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_NAME()
	FROM	RecHubSystem.DataImportQueue
			INNER JOIN @Responses R ON RecHubSystem.DataImportQueue.SourceTrackingID = R.SourceTrackingID
	WHERE	QueueStatus IN (30,99, 120);

	SET @Loop = 1;
	SET @Msg = '';
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Responses) )
	BEGIN

		SELECT @Msg = @Msg + CAST(
		(
			SELECT	SourceTrackingID AS '@SourceTrackingID',
					@parmClientProcessCode AS '@ClientProcessCode',
					XMLResponseDocument.query('.')
			FROM	@Responses
			WHERE RowID = @Loop
			FOR XML	PATH('Batches'), TYPE
		) AS VARCHAR(MAX));
				
		SET @Loop = @Loop + 1;
	END

	SELECT
	(
		SELECT	@ResponseTrackingID AS '@ResponseTrackingID',
				CAST(@Msg AS XML)
		FOR XML	PATH('BatchResponses'), TYPE
	) AS XMLResponseDocument;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH