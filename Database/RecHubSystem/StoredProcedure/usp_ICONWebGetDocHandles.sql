--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICONWebGetDocHandles
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICONWebGetDocHandles') IS NOT NULL
	DROP PROCEDURE RecHubSystem.usp_ICONWebGetDocHandles;

SET QUOTED_IDENTIFIER ON
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICONWebGetDocHandles	
	@parmImmutableDate		DATETIME,
	@parmSiteBankID			INT,
	@parmSiteOrganizationID INT,
	@parmSiteClientAccountID	INT,
	@parmSiteCodeID			INT,	
	@parmSourceBatchID		BIGINT,
	@parmBatchSource		VARCHAR(30)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MEH
* Date: 01/26/2009
*
* Purpose: Get Hyland document handles from ImageInfoXML in factChecks and factDocuments
*
* Modification History
* 01/26/2009 CR 28772 MEH	Created
* 04/08/2014 WI 135713 JBS	Update to 2.01 release.  Change Schema Names. 
*							Rename: LockBox to ClientAccount, Customer to Organization, Processing to Immutable
*							Change Table references: factDocuments to factDocumentImages, factChecks to factCheckImages
*							Add logic for Batch Collisions
* 03/10/2015 WI 195104 TWE  Modify to handle extra output columns
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @ImmutableDateKey	   INT,
		@DepositDateKey		   INT,
		@BatchID			   BIGINT,
		@BatchNumber           INT,
	    @BatchPaymentTypeKey   TINYINT,
	    @DepositStatus         INT;

BEGIN TRY

	SET @ImmutableDateKey = CAST(CONVERT(VARCHAR,@parmImmutableDate,112) AS INT);

    EXEC RecHubData.usp_factBatchSummary_Get_BatchID @ImmutableDateKey, 
	                                                 @parmSiteBankID, 
													 @parmSiteOrganizationID, 
													 @parmSiteClientAccountID, 
													 @parmSiteCodeID, 
													 @parmSourceBatchID, 
													 @parmBatchSource, 
													 @BatchID             OUTPUT, 
													 @DepositDateKey      OUTPUT,
													 @BatchNumber         OUTPUT,
	                                                 @BatchPaymentTypeKey OUTPUT,
	                                                 @DepositStatus       OUTPUT;

	SELECT	DISTINCT 
	    @BatchNumber         AS BatchNumber, 
		@BatchPaymentTypeKey AS BatchPaymentTypeKey,
		@DepositStatus       AS DepositStatus,
	    ExternalDocumentID   AS DocumentHandle
	FROM	RecHubData.factDocumentImages
	WHERE
	    RecHubData.factDocumentImages.IsDeleted = 0
		AND RecHubData.factDocumentImages.DepositDateKey = @DepositDateKey
		AND RecHubData.factDocumentImages.BatchID = @BatchID	
	UNION
	SELECT	DISTINCT 
	    @BatchNumber         AS BatchNumber, 
		@BatchPaymentTypeKey AS BatchPaymentTypeKey,
		@DepositStatus       AS DepositStatus,
	    ExternalDocumentID   AS DocumentHandle
	FROM	RecHubData.factCheckImages
	WHERE
	    RecHubData.factCheckImages.IsDeleted = 0
		AND RecHubData.factCheckImages.DepositDateKey = @DepositDateKey
		AND RecHubData.factCheckImages.BatchID = @BatchID;												

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


			
	
