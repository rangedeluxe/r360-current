--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PurgeHistory_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PurgeHistory_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PurgeHistory_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PurgeHistory_Ins
(
	@parmPurgeHistoryType	INT,
	@parmBankKey			INT,
	@parmClientAccountKey	INT,
	@parmProcessingDateKey	INT,
	@parmDepositDateKey		INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CRG
* Date: 06/06/2013
*
* Purpose: Add Rows to PurgeHistory for purging.
*
* Description:
*	@parmPurgeType
*		1 = Data Purge
*		2 = Image Purge
*
* Modification History
* 06/07/2013 WI 104703 CRG	Created
* 06/12/2015 WI 218292 JBS	Adding Permissions.
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY       
	INSERT INTO RecHubSystem.PurgeHistory
	(
		RecHubSystem.PurgeHistory.PurgeHistoryType,
		RecHubSystem.PurgeHistory.BankKey,
		RecHubSystem.PurgeHistory.ClientAccountKey,
		RecHubSystem.PurgeHistory.ProcessingDateKey,
		RecHubSystem.PurgeHistory.DepositDateKey
	)
	VALUES
	(
		@parmPurgeHistoryType,
		@parmBankKey,
		@parmClientAccountKey,
		@parmProcessingDateKey,
		@parmDepositDateKey
	);
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
