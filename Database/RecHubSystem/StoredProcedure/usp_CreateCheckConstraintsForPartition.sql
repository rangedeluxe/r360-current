IF OBJECT_ID('RecHubSystem.usp_CreateCheckConstraintsForPartition') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CreateCheckConstraintsForPartition
GO

CREATE PROCEDURE RecHubSystem.usp_CreateCheckConstraintsForPartition
       @parmPartitionManagerID INT
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright 2008-2019 WAUSAU Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2008-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/24/2009
*
* Purpose: This SP will create the check constraints for all tables on the partition.
*
* Modification History
* 03/24/2009 CR 25817 JJR	Created
* 03/13/2013 WI 90597 JPB	Update to 2.0 release. Change Schema Name.
* 03/20/2017 PT 138837605	MGE	Changes to accomodate values in keys will be 1 partition < the value of the partitionkey
*								--also Raised a message to show the values of the check constraint
* 04/24/2017 PT 143253497	MGE fixed bug when multiple tables exist for the same partition scheme
* 10/31/2019 R360-31416		MGE improved the tolerance by checking for the highest key value in the table.
***************************************************************************************************************************/
SET NOCOUNT ON;
DECLARE @LowerLimit DATETIME, 
		@UpperLimit DATETIME,
		@LowestKeyValue INT,
		@HighestKeyValue INT,
		@i INT,
		@SchemaName VARCHAR(25),
		@TableName VARCHAR(256),
		@ColumnName VARCHAR(128),
		@SQLcmd VARCHAR(MAX),
		@PartitionRangeSetting TINYINT,
		@PartitionSize INT;
DECLARE @LowestKeyValueTable TABLE (RowID INT IDENTITY(1,1), Value INT);
DECLARE @HighestKeyValueTable TABLE (RowID INT Identity(1,1), Value INT);

DECLARE @PartitionedTables TABLE (RowID INT IDENTITY(1,1), SchemaName VARCHAR(25),TableName VARCHAR(256), ColumnName VARCHAR(128));

BEGIN TRY
    /* Get lower and upper date values for constraining the tables */
    SELECT 
		@LowerLimit=CAST(CAST(MIN(sys.partition_range_values.Value) AS VARCHAR) AS SMALLDATETIME),
		@UpperLimit=CAST(CAST(MAX(sys.partition_range_values.Value) AS VARCHAR) AS SMALLDATETIME)
	FROM sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_range_values.function_id = sys.partition_functions.function_id
		INNER JOIN RecHubSystem.PartitionManager ON RecHubSystem.PartitionManager.PartitionFunctionName=sys.partition_functions.name
	WHERE
		 RecHubSystem.PartitionManager.PartitionManagerID=@parmPartitionManagerID;
	
	SELECT 
		@PartitionRangeSetting=PartitionManager.RangeSetting
	FROM 
		sys.partition_functions 
		INNER JOIN RecHubSystem.PartitionManager ON RecHubSystem.PartitionManager.PartitionFunctionName=sys.partition_functions.name
	WHERE
		 RecHubSystem.PartitionManager.PartitionManagerID=@parmPartitionManagerID;

	SELECT @LowerLimit =
		CASE @PartitionRangeSetting
		WHEN 1 THEN DATEADD(DD,-7,@LowerLimit)
		WHEN 2 THEN DATEADD(M,-1, @LowerLimit)
		END
	
    INSERT INTO @PartitionedTables(SchemaName, TableName, ColumnName)
	SELECT 
		SCHEMA_NAME(sys.tables.schema_id),
		sys.tables.name,
		sys.columns.name
	FROM
		sys.tables
		INNER JOIN sys.indexes ON sys.indexes.object_id = sys.tables.object_id AND sys.indexes.index_id < 2
		INNER JOIN sys.data_spaces ON sys.data_spaces.data_space_id = sys.indexes.data_space_id
		INNER JOIN RecHubSystem.PartitionManager ON sys.data_spaces.name = RecHubSystem.PartitionManager.PartitionIdentifier
		INNER JOIN sys.index_columns ON (sys.index_columns.partition_ordinal > 0) AND (sys.index_columns.index_id=sys.indexes.index_id AND sys.index_columns.object_id=CAST(sys.tables.object_id AS int))
		INNER JOIN sys.columns ON sys.columns.object_id = sys.index_columns.object_id and sys.columns.column_id = sys.index_columns.column_id
	WHERE	
		RecHubSystem.PartitionManager.PartitionManagerID=@parmPartitionManagerID;

    /* drop/create check constsraints for each table on partition */
    SET @i=1
    WHILE @i<=(SELECT MAX(RowID) FROM @PartitionedTables)
    BEGIN
        SELECT 
			@SchemaName=SchemaName,
            @TableName=TableName,
			@ColumnName=ColumnName
        FROM 
			@PartitionedTables
        WHERE 
			RowID=@i;



		SET @SQLcmd='IF EXISTS( SELECT 1 FROM sys.check_constraints WHERE object_id = OBJECT_ID('+CHAR(39)+@SchemaName+'.CK_'+@TableName+@ColumnName+'Range'+CHAR(39)+') AND parent_object_id = OBJECT_ID('+CHAR(39)+@SchemaName+'.'+@TableName+CHAR(39)+') )ALTER TABLE '+@SchemaName+'.'+@TableName+' DROP CONSTRAINT CK_'+@TableName+@ColumnName+'Range';
        EXEC(@SQLcmd);

		SET @SQLcmd='SELECT MIN(' + @ColumnName + ') FROM ' +@SchemaName+'.'+@TableName ;
        INSERT INTO @LowestKeyValueTable 
		EXEC(@SQLcmd);

		SELECT @LowestKeyValue = Value FROM @LowestKeyValueTable WHERE RowID=@i;
		IF CAST(CAST(@LowestKeyValue AS VARCHAR) AS SMALLDATETIME) < @LowerLimit
			SET @LowerLimit = CAST(CAST(@LowestKeyValue AS VARCHAR) AS SMALLDATETIME);

		SET @SQLcmd='SELECT MAX(' + @ColumnName + ') FROM ' +@SchemaName+'.'+@TableName ;
        INSERT INTO @HighestKeyValueTable 
		EXEC(@SQLcmd);

		SELECT @HighestKeyValue = Value FROM @HighestKeyValueTable WHERE RowID=@i;
		IF CAST(CAST(@HighestKeyValue AS VARCHAR) AS SMALLDATETIME) > @UpperLimit
			SET @UpperLimit = CAST(CAST(@HighestKeyValue AS VARCHAR) AS SMALLDATETIME);

        SET @SQLcmd = 
        'ALTER TABLE '+@SchemaName+'.'+@TableName+' ADD CONSTRAINT CK_'+@TableName+@ColumnName+'Range CHECK ('+@ColumnName+' >= ' 
                + CONVERT(VARCHAR,@LowerLimit,112) + ' AND '+@ColumnName+'<='
                + CONVERT(VARCHAR,@UpperLimit,112) + ')';
		RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
        EXEC(@SQLcmd);

        SET @i=@i+1;
    END
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
