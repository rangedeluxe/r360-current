--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_dimImageRPSAliasMappings_GetRecords
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords
(
	@parmBankID				INT,
	@parmClientAccountID	INT,
	@parmModificationDate	DATETIME = NULL
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/21/2012
*
* Purpose: Retrieve the Table and Field Name for a given alias name.
*
* Modification History
* 05/21/2012 CR 52941 WJS	Created
* 06/21/2012 CR 52941 WJS   Modified to filter out just bankid, lockboxid
* 09/10/2012 CR 52941 WJS   Add MostRecent
* 02/17/2014 WI 129825 CMC	Update to 2.01 Schema. 
* 04/17/2014 WI 137297 JBS	Added Permissions. Changed to RecHubSystem Schema
* 08/09/2017 PT 149122385 MGE Updated to eliminate parallelism and many un-needed reads/scans 
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	IF @parmModificationDate IS NULL
		BEGIN
		SELECT	
			SiteBankID, 
			SiteClientAccountID,
			ExtractType,
			DocType,
			FieldType,
			AliasName
		FROM	
			RecHubData.dimImageRPSAliasMappings
		WHERE 
			@parmBankID = SiteBankID AND
			@parmClientAccountID = SiteClientAccountID AND
			MostRecent = 1
		ORDER BY
			SiteBankID, 
			SiteClientAccountID,
			DocType;
		END
	ELSE
		BEGIN
		SELECT	
			SiteBankID, 
			SiteClientAccountID,
			ExtractType,
			DocType,
			FieldType,
			AliasName
		FROM	
			RecHubData.dimImageRPSAliasMappings
		WHERE 
			@parmBankID = SiteBankID AND
			@parmClientAccountID = SiteClientAccountID AND
			MostRecent = 1 AND
			CreationDate > @parmModificationDate
		ORDER BY
			SiteBankID, 
			SiteClientAccountID,
			DocType;
		END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

