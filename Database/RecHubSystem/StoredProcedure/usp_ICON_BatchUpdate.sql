--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICON_BatchUpdate
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubSystem.usp_ICON_BatchUpdate') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_BatchUpdate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_BatchUpdate
       @parmXML xml
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/09/2009
*
* Purpose: Update batch fact tables from XML
*
* Modification History
* 02/09/2009 CR 28956 JPB	Created
* 03/26/2010 CR 29291 JPB	Added factTransactionDetails
* 04/12/2010 CR 29356 JPB	Added factTransactionSummary_UpdateCheckTotal.
* 05/08/2010 CR 29369 JPB   Remove Source Identifer check to retrieve deposit 
*                           date key. (Returned from usp_ConvertDBOKeysToRecHubSystemKeys.)
* 05/24/2010 CR 29762 JPB	Removed call to usp_LoadRemittersDimension.
* 01/07/2011 CR 32228 JPB 	Removed factTransactionDetails.
* 10/24/2014 WI 173985 TWE  Convert from 1.05 to 2.01
*                           Previously known as olta.usp_ICON_BatchUpdate.sql
*							Adding Permission for RecHubUser_User
* 06/09/2015 WI 173985 JBS	Changing names of called procedures.
* 08/22/2016 PT 128448991	JPB	Changed @iBatchSourceKey to SMALLINT. 
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON

DECLARE @bLocalTransaction             BIT,
		@iBankKey                      INT,
		@iOrganizationKey              INT,
		@iClientAccountKey             INT,
		@iSourceProcessingDateKey      INT,
		@iImmutableDateKey             INT,
		@iDepositDateKey               INT,
		@iBatchID                      BIGINT,
		@iSourceBatchID                BIGINT,
		@iSourceIdentifier             SMALLINT,
		@iBatchNumber                  INT,
		@iBatchPaymentTypeKey          TINYINT,
		@iDepositStatus                INT,
		@dtModificationDate            DATETIME,
		@iBatchSourceKey               SMALLINT;		

BEGIN TRY

	SET @dtModificationDate = GETDATE();

	--Get the action code and IMS setting
	SELECT	
		@iBatchID = BatchID,
		@iSourceBatchID = SourceBatchID,
		@iSourceIdentifier = SourceIdentifier,
		@iBatchNumber = BatchNumber,
		@iBatchPaymentTypeKey = BatchPaymentTypeKey,
		@iDepositStatus = DepositStatus
	FROM 
	(
	SELECT	
		Batch.att.value('@BatchID', 'bigint')            AS BatchID,
		Batch.att.value('@SourceBatchID', 'bigint')      AS SourceBatchID,
		Batch.att.value('@SourceIdentifier', 'smallint') AS SourceIdentifier,
		Batch.att.value('@BatchNumber', 'int')           AS BatchNumber,
		Batch.att.value('@BatchPaymentTypeKey', 'smallint') AS BatchPaymentTypeKey,
		Batch.att.value('@DepositStatus', 'int')           AS DepositStatus
	FROM	
	    @parmXML.nodes('/Batches/Batch') Batch (att)
	) BatchData;

	IF @iSourceIdentifier IS NULL
		SET @iSourceIdentifier = 0;

	EXECUTE RecHubData.usp_ConvertSourceKeysToRecHubKeys @parmXML, 
	                                                     @iBankKey                 OUTPUT, 
														 @iOrganizationKey         OUTPUT, 
														 @iClientAccountKey        OUTPUT, 
														 @iImmutableDateKey        OUTPUT, 
														 @iDepositDateKey          OUTPUT, 
														 @iSourceProcessingDateKey OUTPUT, 
														 @iBatchSourceKey          OUTPUT;

	if (@iBankKey IS NULL                 OR
	    @iOrganizationKey IS NULL         OR
	    @iClientAccountKey IS NULL        OR
	    @iImmutableDateKey IS NULL        OR
		@iDepositDateKey IS NULL          OR
	    @iSourceProcessingDateKey IS NULL OR
	    @iBatchSourceKey IS NULL)
	BEGIN
		RAISERROR('Converting Source Keys to RecHubKeys failed',16,1);
	END
		
	--CR 28861 BEGIN
	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1
	END

	EXEC RecHubSystem.usp_ICON_factChecks_Upd @parmXML,@iBankKey,@iOrganizationKey,@iClientAccountKey,@iImmutableDateKey,@iDepositDateKey,@iBatchID,@iSourceBatchID,@iSourceIdentifier,@dtModificationDate

	EXEC RecHubSystem.usp_ICON_factStubs_Upd @parmXML,@iBankKey, @iOrganizationKey, @iClientAccountKey, @iSourceProcessingDateKey, @iImmutableDateKey, @iDepositDateKey, @iBatchID, @iSourceBatchID,@iBatchNumber, @iBatchSourceKey, @iBatchPaymentTypeKey, @iDepositStatus, @iSourceIdentifier, @dtModificationDate
	
	--Update after the check amounts have been updated.	
	EXEC RecHubSystem.usp_ICON_factTransactionSummary_Upd_TotalAmount @parmXML, @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @iSourceBatchID, @iSourceIdentifier, @dtModificationDate
	
	--This has to be done last so we get the updated check amounts and anything else that we might add later.	
	EXEC RecHubSystem.usp_ICON_factBatchSummary_Upd_TotalAmount @parmXML, @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID, @iSourceBatchID, @iSourceIdentifier, @dtModificationDate

	IF @bLocalTransaction = 1 
	    COMMIT TRANSACTION 
	RETURN 0
END TRY
BEGIN CATCH
	--CR 28861 changes in this block
	IF XACT_STATE() != 0 AND @bLocalTransaction = 1 ROLLBACK TRANSACTION
	IF @bLocalTransaction = 1 
	BEGIN
	    RETURN 0
		--EXEC RecHubSystem.usp_Ins_ServiceBrokerError @parmXML
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage	NVARCHAR(4000),
				@ErrorSeverity	INT,
				@ErrorState		INT,
				@ErrorLine		INT
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE()
		SET @ErrorMessage = 'usp_ICON_BatchUpdate (Line: %d)-> ' + @ErrorMessage
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	RETURN -1
END CATCH

