--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Ins_DocumentType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CDSQueue_Ins_DocumentType') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CDSQueue_Ins_DocumentType;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CDSQueue_Ins_DocumentType
(
	@parmFileDescriptor		VARCHAR(30),
	@parmFileDescription	VARCHAR(64),
	@parmActionCode			TINYINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/15/2013
*
* Purpose: Insert document type record into RecHubSystem.CDSQueue.
*
* Action codes could be:
*	1 - Insert/Update
*	2 - Delete
*
* Modification History
* 06/15/2013 WI 105575 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @RetryCount		INT,
		@ErrorMessage	NVARCHAR(4000),
		@ErrorSeverity	INT,
		@ErrorState		INT;

BEGIN TRY

	SET @RetryCount = 4;
	
	WHILE( @RetryCount >= 0 )
	BEGIN
		BEGIN TRY
			
			INSERT INTO RecHubSystem.CDSQueue
			(
				FileDescriptor,
				FileDescription,
				ActionCode,
				QueueStatus,
				QueueType
			)
			VALUES
			(
				@parmFileDescriptor,
				@parmFileDescription,
				@parmActionCode,
				10,
				70

			);
			
			SET @RetryCount = -1;
		END TRY
		BEGIN CATCH
			/* 
				Catch deadlock condition 
				If a deadlock is found, wait a few seconds and try again
				If the retry count is exceeded, pass the deadlock error to the main catch block
				If it is not a deadlock, pass control to the main catch block and 
					return that information to the application.
			*/
			
			IF (ERROR_NUMBER() = 1205) AND (@RetryCount > 0)
			BEGIN
				SET @RetryCount = @RetryCount - 1;
				WAITFOR DELAY '00:00:05'; /* wait 5 seconds and try again */
			END
			ELSE
			BEGIN
				SET @RetryCount = -1;		
				SELECT	@ErrorMessage = ERROR_MESSAGE(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE();
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			END
		END CATCH
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
