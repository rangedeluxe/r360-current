IF OBJECT_ID('RecHubSystem.usp_AddPartitionsToDatabase') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_AddPartitionsToDatabase
GO
CREATE PROCEDURE RecHubSystem.usp_AddPartitionsToDatabase
       @parmDatabaseName VARCHAR(256),						-- The DatabaseName to add partitions to
	   @parmPatitionSchemeName VARCHAR(256),				-- The PartitionScheme to create partitions on
       @parmPartitionsInAdvance INT							-- The number of partitions beyond today's date
AS
/******************************************************************************
** Deluxe Corporation (DLX) 
** Copyright � 2013-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 3/09/2009
*
* Purpose: Creates a new filegroup and file and creates/alters the partition 
*      scheme and function to use the new file/filegroup.
*
* Modification History
* 11/10/2015 WI 243967	MGE	Created for sprint 33
* 08/19/2019 R360-29886 MGE	added parameter to pass in partition scheme
******************************************************************************/
 
DECLARE	@OuterLoop				INT,
		@OuterIteration			INT,
		@MaxPartitionRangeValue	INT,
		@PartitionFunction		VARCHAR(128),
		@NewPartitionStart		DATETIME,
		@LastPartitionDate		DATETIME,
		@tempDateChar			VARCHAR(10),
		@TwelveMonthsFromNow	INT,
		@message				VARCHAR(256),
		@SQLCommand				VARCHAR(256);

DECLARE @MaxPartitionDates Table
	(
	RowID					INT IDENTITY,
	PartitionSchemeName		VARCHAR(256),
	PartitionFunctionName	VARCHAR(256),
	MaxPartitionRangeValue	sql_variant
	);

BEGIN TRY
    INSERT INTO @MaxPartitionDates
	SELECT RecHubSystem.PartitionManager.PartitionSchemeName, sys.partition_functions.name, max(sys.partition_range_values.value) 
	FROM RecHubSystem.PartitionManager
	INNER JOIN sys.partition_schemes ON 
		sys.partition_schemes.name = RecHubSystem.PartitionManager.PartitionSchemeName
	INNER JOIN sys.partition_functions ON 
		sys.partition_functions.function_id = sys.partition_schemes.function_id
	INNER JOIN sys.partition_range_values ON
		sys.partition_range_values.function_id = sys.partition_functions.function_id
	WHERE RecHubSystem.PartitionManager.PartitionSchemeName = @parmPatitionSchemeName
	GROUP BY RecHubSystem.PartitionManager.PartitionSchemeName, sys.partition_functions.name;
	SET @OuterLoop = @@ROWCOUNT;
	--select * from @MaxPartitionDates;					--DEBUG ONLY
	SELECT @TwelveMonthsFromNow = (CONVERT(VARCHAR(6),(DATEADD(MONTH,@parmPartitionsInAdvance,GETDATE())), 112) + '01');
	--select @TwelveMonthsFromNow							--DEBUG ONLY
	SELECT @tempDateChar = (Convert(VarChar(10),@TwelveMonthsFromNow,101));
	SELECT @LastPartitionDate = CAST(@tempDateChar as DATETIME); 
	SET @OuterIteration = 1;
	WHILE (@OuterIteration <= @OuterLoop)
		BEGIN	-- BEGIN WHILE
		
			SELECT 
				@MaxPartitionRangeValue=CAST(MaxPartitionRangeValue AS INT),
				@PartitionFunction = PartitionFunctionName
			 FROM @MaxPartitionDates WHERE RowID = @OuterIteration 
		
			IF @MaxPartitionRangeValue < @TwelveMonthsFromNow
				BEGIN
					/** Partitions must be added  **/
					SELECT @message = @PartitionFunction + ' New Partition Needed;' + ' Highest Current Partition Range=' + CAST(@MaxPartitionRangeValue AS VARCHAR(8)) + ' 12MonthsFromNowRangeStart=' + CAST(@TwelveMonthsFromNow AS VARCHAR(8))
					RAISERROR(@message, 10, 1) WITH NOWAIT;
				
					--SELECT CAST(Convert(VARCHAR(8),MaxPartitionRangeValue, 101) AS DATETIME) FROM @MaxPartitionDates WHERE RowID = @OuterIteration;	--DEBUG ONLY
					SET @NewPartitionStart = CAST(CONVERT(VARCHAR(8),@MaxPartitionRangeValue,101) AS DATETIME);
					SET @NewPartitionStart = DateAdd(MONTH,1,@NewPartitionStart);
					--SELECT @NewPartitionStart					--DEBUG ONLY
					SELECT @SQLCommand = 'EXEC RecHubSystem.usp_CreatePartitionsForRange ' + PartitionSchemeName + ', ''' + CONVERT(VARCHAR(10), @NewPartitionStart, 101) + ''', ''' 
						+ CONVERT(VARCHAR(10), @LastPartitionDate, 101) + ''''
					FROM @MaxPartitionDates
					WHERE RowID = @OuterIteration;
					RAISERROR (@SQLCommand, 10, 1) WITH NOWAIT;
					EXEC (@SQLCommand);
				END;
			ELSE
				BEGIN
					SELECT @message = @PartitionFunction + ' No New PartitionsNeeded;' + ' HighestPartitionRange=' + CAST(@MaxPartitionRangeValue AS VARCHAR(8)) + ' 12MonthsFromNowRangeStart=' + CAST(@TwelveMonthsFromNow AS VARCHAR(8))
					RAISERROR(@message, 10, 1) WITH NOWAIT;
				END;

			SET @OuterIteration = @OuterIteration + 1;

		END;		-- END WHILE  

END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH