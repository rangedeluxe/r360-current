--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICONBatchNotifyOLTAServiceBroker
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICONBatchNotifyOLTAServiceBroker') IS NOT NULL
       DROP PROCEDURE dbo.usp_ICONBatchNotifyOLTAServiceBroker
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICONBatchNotifyOLTAServiceBroker
(
	@parmXML		XML,
	@parmActionCode INT,
	@parmNotifyIMS	SMALLINT,
	@parmKeepStats	BIT = 1
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/21/2009
*
* Purpose: Notify SSB that an ImageRPS Batch is ready for processing.
*
* NOTE: This is a diffent model then the CDS process. While the CDS process
*	must create a XML document from dbo and send it to OLTA, the ICON 
*	application creates the XML document from the ImageRPS data passed to it. 
*	Therefore ICON data can be sent directly to the OLTA SSB object.
*	Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated - This is truely an update for the ICON process.
*	6 - Batch Images Updated
* 
* Modification History
* 10/08/2009 CR 27956 JPB	Created
* 02/08/2010 CR 28910 JPB	Added delete support.
* 02/10/2010 CR 28911 JPB	Added update support.
* 11/02/2011 CR 33219 JPB	Changed to KeepStatus = 1
* 11/27/2012 WI 70462 WJS	no longer needed. ICON Does not used this process anymore
* 04/10/2014 WI 135705 JBS	Adding back for ICON. Update to 2.01 schema.
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

DECLARE @Message XML,
		@QueuedTime VARCHAR(30),
		@ConversationHandle UNIQUEIDENTIFIER;

BEGIN TRY

	--setup information needed for all action codes
	SET @QueuedTime = CONVERT( VARCHAR(30), GETDATE(), 126)
	SET @parmXML.modify('insert attribute ActionCode {sql:variable("@parmActionCode")} as last into (/Batches/Batch)[1]')
	SET @parmXML.modify('insert attribute NotifyIMS {sql:variable("@parmNotifyIMS")} as last into (/Batches/Batch)[1]')
	IF @parmKeepStats = 1 --for some reason, sql:variable writes this as true or false rather then 0 or 1
		SET @parmXML.modify('insert attribute KeepStats {"1"} as last into (/Batches/Batch)[1]')
	ELSE SET @parmXML.modify('insert attribute KeepStats {"0"} as last into (/Batches/Batch)[1]')
	SET @parmXML.modify('insert attribute QueuedTime {sql:variable("@QueuedTime")} as last into (/Batches/Batch)[1]')

	SET @parmXML.modify('insert attribute SourceIdentifier {"1"} as last into (/Batches/Batch)[1]')

	IF @parmActionCode = 1 OR @parmActionCode = 2
	BEGIN	
		BEGIN DIALOG @ConversationHandle
			FROM SERVICE OLTABatchInsertSenderService
			TO SERVICE 'OLTABatchInsertReceiverService'
		ON CONTRACT OLTABatchInsertContract;

		SEND ON CONVERSATION @ConversationHandle
			MESSAGE TYPE OLTABatchInsertMsg (@parmXML)

		END CONVERSATION @ConversationHandle
	END;
	
	IF @parmActionCode = 4
	BEGIN
		BEGIN DIALOG @ConversationHandle
		FROM SERVICE OLTABatchDeleteSenderService
		TO SERVICE 'OLTABatchDeleteReceiverService'
		ON CONTRACT OLTABatchDeleteContract;

		SEND ON CONVERSATION @ConversationHandle
		MESSAGE TYPE OLTABatchDeleteMsg (@parmXML)

		END CONVERSATION @ConversationHandle
	END;

	IF @parmActionCode = 5
	BEGIN
		BEGIN DIALOG @ConversationHandle
		FROM SERVICE OLTABatchUpdateSenderService
		TO SERVICE 'OLTABatchUpdateReceiverService'
		ON CONTRACT OLTABatchUpdateContract;

		SEND ON CONVERSATION @ConversationHandle
		MESSAGE TYPE OLTABatchUpdateMsg (@parmXML)

		END CONVERSATION @ConversationHandle
	END;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
