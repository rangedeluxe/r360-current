--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PurgeQueue_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PurgeQueue_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PurgeQueue_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PurgeQueue_Ins
(
	@parmPurgeType	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/07/2013
*
* Purpose: Add Rows to PurgeQueue for purging.
*
* Description:
*	@parmPurgeType
*		1 = Data Purge, Populate RecHubSystem.PurgeQueue
*		2 = Image Purge, Return Result Set - DO not populate table
*
* Modification History
* 05/07/2013 WI 96899  JBS	Created
* 02/03/2015 WI 187718 BLR	0 Image retention days now means use the default
*                           image retention days instead.
* 02/03/2015 WI 187836 BLR  Need to select the FileGroup for FileGroup storage.
* 02/05/2015 WI 188372 BLR  Filtered results for ImagePurge by MostRecent.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DefaultPurgeDays INT = 0,
		@SetupKey	VARCHAR(50),
		@ErrorDescription VARCHAR(2000); 

-- If records exist and @parmPurgeType = 1 we will not insert any more rows.  
IF @parmPurgeType = 1 and EXISTS(SELECT RecHubSystem.PurgeQueue.PurgeQueueID FROM RecHubSystem.PurgeQueue)
	BEGIN
		RETURN 0
	END

SET @SetupKey = 
CASE 
	WHEN @parmPurgeType = '1' THEN 'DefaultDataRetentionDays'
	WHEN @parmPurgeType = '2' THEN 'DefaultImageRetentionDays'
	ELSE  'INVALID'
END


BEGIN TRY       

	-- SET default days from systemtable depending on @parmPurgeType 
	SELECT 
		@DefaultPurgeDays = RecHubConfig.SystemSetup.Value 
	FROM 
		RecHubConfig.SystemSetup
	WHERE 
		RecHubConfig.SystemSetup.Section = 'Rec Hub Table Maintenance'
		AND RecHubConfig.SystemSetup.SetupKey = @SetupKey;
    
	
	IF (@DefaultPurgeDays = 0 or @DefaultPurgeDays IS NULL)
		BEGIN
		   SET @ErrorDescription = 'There is no database entry for 1 or both Retention Days on RecHubConfig.SystemSetup or value is 0 ';
		   RAISERROR(@ErrorDescription,16,1);
		END
	
	-- Create Temp table to hold ALL Most Recent ClientAccount rows
	CREATE TABLE #TempCAs
	(
		SiteBankID			INT,
		SiteClientAccountID INT,
		RetentionDays		INT
	);

	INSERT INTO #TempCAs
	SELECT
		RecHubData.dimClientAccountsView.SiteBankID,
		RecHubData.dimClientAccountsView.SiteClientAccountID,
		CASE
			WHEN @parmPurgeType = 1 THEN RecHubData.dimClientAccountsView.DataRetentionDays	
			WHEN @parmPurgeType = 2 THEN RecHubData.dimClientAccountsView.ImageRetentionDays
		END 
	FROM
		RecHubData.dimClientAccountsView;

	-- Insert rows into RecHubSystem.PurgeQueue for @parmPurgeType = 1
	IF @parmPurgeType = '1'
		BEGIN
			INSERT INTO RecHubSystem.PurgeQueue
			(
				RecHubSystem.PurgeQueue.BankKey,
				RecHubSystem.PurgeQueue.ClientAccountKey,
				RecHubSystem.PurgeQueue.SiteBankID,
				RecHubSystem.PurgeQueue.SiteClientAccountID,
				RecHubSystem.PurgeQueue.DataDepositDateKey,
				RecHubSystem.PurgeQueue.DataPurged
			)
			SELECT 
				RecHubData.dimBanks.BankKey,
				RecHubData.dimClientAccounts.ClientAccountKey,
				CA.SiteBankID,
				CA.SiteClientAccountID,
				CAST(CONVERT(VARCHAR(10),COALESCE(GETDATE() - RetentionDays, GETDATE() - @DefaultPurgeDays), 112) AS INT),
				'0'
			FROM 
				RecHubData.dimClientAccounts 
				JOIN RecHubData.dimBanks
					ON RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimBanks.SiteBankID
				JOIN #TempCAs CA
					ON CA.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
					AND CA.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID ;
		END
	
	-- Return Result set on all Run types
	BEGIN
		SELECT 
			RecHubData.dimBanks.BankKey,
			RecHubData.dimClientAccounts.ClientAccountKey,
			CA.SiteBankID,
			CA.SiteClientAccountID,
			CAST(CONVERT(VARCHAR(10),GETDATE() - ISNULL(NULLIF(RetentionDays,0),@DefaultPurgeDays), 112) AS INT) AS DataDepositDateKey,
			'0' AS DataPurged,
			FileGroup
		FROM 
			RecHubData.dimClientAccounts 
			JOIN RecHubData.dimBanks
				ON RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimBanks.SiteBankID
			JOIN #TempCAs CA
				ON CA.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
				AND CA.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID 
			WHERE RecHubData.dimClientAccounts.MostRecent = 1
			    AND RecHubData.dimBanks.MostRecent = 1;
	END

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
