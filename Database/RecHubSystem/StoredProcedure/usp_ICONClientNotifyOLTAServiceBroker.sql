--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICONClientNotifyOLTAServiceBroker
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICONClientNotifyOLTAServiceBroker') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICONClientNotifyOLTAServiceBroker
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICONClientNotifyOLTAServiceBroker
(
	@parmXML xml
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/21/2009
*
* Purpose: Notify SSB that an ImageRPS client data is ready for processing.
*
* NOTE: This is a diffent model then the CDS process. While the CDS process
*	must create a XML document from dbo and send it to OLTA, the ICON 
*	application creates the XML document from the ImageRPS data passed to it. 
*	Therefore ICON data can be sent directly to the OLTA SSB object.
* 
* Modification History
* 10/08/2009 CR 27826 JPB	Created
* 04/10/2014 WI 135710 JBS	Update to 2.01 release.  Change Schema Names. 
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

DECLARE @Message XML,
		@ConversationHandle UNIQUEIDENTIFIER;

BEGIN TRY

	BEGIN DIALOG @ConversationHandle
		FROM SERVICE OLTAICONClientInsertSenderService
		TO SERVICE 'OLTAICONClientInsertReceiverService'
	ON CONTRACT OLTAICONClientInsertContract;

	SEND ON CONVERSATION @ConversationHandle
		MESSAGE TYPE OLTAICONClientInsertMsg (@parmXML)

	END CONVERSATION @ConversationHandle

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH