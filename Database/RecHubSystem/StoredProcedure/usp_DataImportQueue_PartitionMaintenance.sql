--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_PartitionMaintenance
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_PartitionMaintenance') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_PartitionMaintenance
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_PartitionMaintenance
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2017-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 3/09/2017
*
* Purpose: Switches out oldest partitions (based on RetentionDays in SystemSetup) and adds partitions necessary
*      to remain 52 partitions into the future.
*
* Modification History
* 03/09/2017 PT	138837605	MGE	Created
* 03/22/2017 PT 138837605	MGE Updated to make it more robust by dynamically getting partition numbers and boundary values.
* 03/23/2017 PT 138837605	MGE Updated so the switch to table is not partitioned.  Made double sure of partition number to switch.
* 03/23/2017 PT 138837605	MGE Also used new FileGroupDetail view to find partition number for range value being switched out.
* 03/23/2017 PT 138837605	MGE Fix the cacluation of HighestExistingPartitionDateTime
* 05/03/2017 PT 144220483	MGE Filter the query to determine the switch partition correctly
* 08/23/2017 PT 149894451	MGE	Add call to increase size of next two partitions to be used.
* 08/13/2018 PT 159736152	JPB Added JSONDataDocument to CREATE TABLE dynamic SQL statement.
************************************************************************************************************************************/
DECLARE @ReturnValue INT,
       @RangeDateKey INT,
	   @HighestRangeDateKey INT,
	   @SwitchPartitionNumber INT,
	   @HighestRangeDateTime DATETIME,
	   @OldestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonDateTime DATETIME,
	   @TempDateTime DATETIME,
       @DiskPath VARCHAR(1000),
       @DITRetentionDays INT,
       @OldestCalendarDateToKeep DateTime,
	   @EndDate	DATETIME,
       @LastDiskID INT, 
	   @Message VARCHAR(max),
       @SQLcmd VARCHAR(max)

SET @ReturnValue = 0
RAISERROR('Beginning process to maintain DataImportQueue partitions.',10,1) WITH NOWAIT;
BEGIN TRY
       --<<< CALCULATE First partition using RetentionDays >>>
	SELECT @DITRetentionDays = Value FROM RecHubConfig.SystemSetup WHERE SetupKey = 'DataImportQueueRetentionDays'
	PRINT 'DITRetentionDays = ' + Convert(VARCHAR(10), @DITRetentionDays, 101)
	SELECT @OldestCalendarDateToKeep = GETDATE() - @DITRetentionDays		-- n days prior to today
	PRINT 'Calculated Oldest Date to keep = ' + Convert(VARCHAR(10), @OldestCalendarDateToKeep, 101)
	SELECT @RangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @OldestCalendarDateToKeep AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest Partition to keep is: ' + CONVERT(CHAR(10),@RangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

    --<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	SELECT @HighestRangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @HighestRangeDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE CalendarDate > @EndDate AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Last Future Partition to add is: ' + CONVERT(CHAR(10),@HighestRangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	--<<< Find oldest existing Partition >>>

	select @OldestExistingPartitonKey = Value from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'DataImport_PartitionFunction' AND boundary_id = 1;
	SELECT @Message = 'Oldest Existing Partition is: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Find highest existing Partition >>>

	select @HighestExistingPartitonKey = MAX(Value) from sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'DataImport_PartitionFunction'
	SELECT @Message = 'Highest Existing Partition Key is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	SELECT @HighestExistingPartitonDateTime = (Select Top 1 CalendarDate FROM RecHubData.dimDates WHERE DateKey > @HighestExistingPartitonKey AND CalendarDayName = 'Monday' ORDER BY CalendarDate ASC)
	SELECT @Message = 'Highest Existing Partition DateTime is: ' + CONVERT(CHAR(10),@HighestExistingPartitonDateTime,111)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	--<<< Determine if there is work to do >>>

	IF	(@OldestExistingPartitonKey >= @RangeDateKey)
		BEGIN
			SELECT @Message = 'No partitions to Switch out '
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
	ELSE
	  BEGIN
	  	  
	  WHILE (@OldestExistingPartitonKey < @RangeDateKey)
		BEGIN
			IF EXISTS (Select 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'RecHubSystem'
				AND sys.tables.name = 'SwitchableDataImportQueue')
				BEGIN
					RAISERROR('Dropping SwitchableDataImportQueue table', 10,1) WITH NOWAIT;
					DROP TABLE RecHubSystem.SwitchableDataImportQueue;
				END

			SELECT @Message = 'Creating SwitchableDataImportQueue table on FileGroup' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE RecHubSystem.SwitchableDataImportQueue
			(
				DataImportQueueID BIGINT NOT NULL IDENTITY(1,1),
				QueueType TINYINT NOT NULL,
				QueueStatus TINYINT NOT NULL,
				ResponseStatus TINYINT NOT NULL,
				RetryCount TINYINT NOT NULL,
				AuditDateKey INT NOT NULL,
				XSDVersion VARCHAR(12) NULL,
				ClientProcessCode VARCHAR(40) NOT NULL, 
				SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
				EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
				ResponseTrackingID UNIQUEIDENTIFIER NULL,
				CreationDate DATETIME NOT NULL,
				CreatedBy VARCHAR(128) NOT NULL,
				ModificationDate DATETIME NOT NULL,
				ModifiedBy VARCHAR(128) NOT NULL,
				XMLDataDocument XML NULL,
				XMLResponseDocument XML NULL,
				JSONDataDocument NVARCHAR(MAX) NULL
			) ON DataImport' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			EXEC (@SQLcmd);
			
			CREATE CLUSTERED INDEX IDX_DataImportQueue_DataImportQueueID_AuditDateKey ON RecHubSystem.SwitchableDataImportQueue (DataImportQueueID, AuditDateKey);
			
			SELECT @SwitchPartitionNumber = partition_number FROM RecHubSystem.FileGroupDetailView 
			WHERE range_value = @OldestExistingPartitonKey
			AND partition_scheme_name = 'DataImport'
			
			SELECT @Message = 'Switching out the oldest partition: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER TABLE RecHubSystem.DataImportQueue SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO RecHubSystem.SwitchableDataImportQueue'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			 
			SELECT @Message = 'Merging the oldest range: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER PARTITION FUNCTION DataImport_PartitionFunction() MERGE RANGE (' + (CONVERT(CHAR(10),@OldestExistingPartitonKey,101)) + ')';
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			DROP TABLE RecHubSystem.SwitchableDataImportQueue

			SELECT @Message = 'Removing the oldest partiton file ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILE DataImport'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SELECT @Message = 'Removing the oldest partition filegroup: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILEGROUP DataImport'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SET @TempDateTime = Convert(VARCHAR(10),(CONVERT(datetime, convert(varchar(10), @OldestExistingPartitonKey)) + 7),101);
			select @OldestExistingPartitonKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate = @TempDateTime AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC); 
		END
	  END
	  
	--<<< Add Partitions up through @HighestRangeKey >>>

	EXEC RecHubSystem.usp_SetPartitionSize 'DataImport'
    
	IF (@HighestExistingPartitonKey < @HighestRangeDateKey)
		BEGIN
		RAISERROR('Creating new DataImport partitions',10,1) WITH NOWAIT;
		SELECT @Message = 'EXEC usp_CreatePartitionsForRange DataImport ' + Convert(Varchar(10),@HighestExistingPartitonDateTime,101) + ' ' + Convert(Varchar(10),@HighestRangeDateTime,101);
		RAISERROR(@Message, 10,1) WITH NOWAIT;
		EXEC RecHubSystem.usp_CreatePartitionsForRange 'DataImport', @HighestExistingPartitonDateTime, @HighestRangeDateTime;
	
		END

   
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
