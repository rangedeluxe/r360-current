--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ICON_factChecks_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ICON_factChecks_Upd') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_factChecks_Upd
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_factChecks_Upd
		@XBatch					XML,
		@parmBankKey			INT,
		@parmOrganizationKey	INT,
		@parmClientAccountKey	INT, 
		@parmImmutableDateKey	INT,
		@parmDepositDateKey		INT,
		@parmBatchID            BIGINT,
		@parmSourceBatchID		BIGINT,
		@parmSourceIdentifier	SMALLINT,
		@parmModificationDate	DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/10/2010
*
* Purpose: Updates factChecks row(s) from XBatch Checks XML segment
*
* Modification History
* 02/09/2010 CR 28954    JPB  Created.
* 05/21/2010 CR 29761    JPB  Updated to handling changing remitter information.
* 02/19/2015 WI 173986   TWE  Convert from 1.05 to 2.01
* 06/09/2015 WI 173986   JBS  Renamed from RecHubData.usp_factChecks_Upd
* 08/26/2016 #127604079  JAW  Use previous NumericSerial value if Serial isn't in the request.
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

DECLARE @bLocalTransaction	BIT,
		@XRemitterInfo      XML;

BEGIN TRY

	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1;
	END
	
	--Create temp file to hold the variable information contained in the xml input parameter
	IF OBJECT_ID('tempdb..#CheckUpdate') IS NOT NULL
        DROP TABLE #CheckUpdate;

	IF OBJECT_ID('tempdb..#Checkkeys') IS NOT NULL
        DROP TABLE #Checkkeys;
	
	--create a table to store the XML data
	CREATE TABLE #Checkkeys	
	(
		factCheckKey      BIGINT    
	);

	--create a table to store the XML data
	CREATE TABLE #CheckUpdate	
	(
	    --IsDeleted
		RowID             INT         NOT NULL IDENTITY(1,1),
		BankKey           INT         NOT NULL,
		OrganizationKey   INT         NOT NULL,
		ClientAccountKey  INT         NOT NULL,
		ImmutableDateKey  INT         NOT NULL,
		DepositDateKey    INT         NOT NULL,
		--RemitterKey       INT         NULL,
		BatchID           BIGINT      NULL,
		SourceBatchID     BIGINT      NOT NULL,
		TransactionID     INT         NOT NULL,
		BatchSequence     INT         NOT NULL,
		Serial            VARCHAR(30) NULL,
		NumericSerial     BIGINT      NULL,
		TransactionCode   VARCHAR(30) NULL,
		Amount            MONEY       NULL,
		RemitterName      VARCHAR(60) NULL,
		Account           VARCHAR(30) NULL,
		RoutingNumber     VARCHAR(30) NULL,
		ModificationDate  DATETIME    NULL
	);
	
	INSERT INTO #CheckUpdate
	(
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		ImmutableDateKey,
		DepositDateKey,
		BatchID,
		SourceBatchID,
		TransactionID,
		BatchSequence,
		Serial,
		NumericSerial,
		TransactionCode,
		Amount,
		RemitterName,
		Account,
		RoutingNumber,
		ModificationDate
	)
	SELECT
		@parmBankKey                     AS BankKey,
		@parmOrganizationKey             AS OrganizationKey,
		@parmClientAccountKey            AS ClientAccountKey,
		@parmImmutableDateKey            AS ImmutableDateKey,
		@parmDepositDateKey              AS DepositDate,
		B.BatchID,     
		B.SourceBatchID,
		T.TransactionID, 
		Checks.BatchSequence,
		Checks.Serial, 
		CASE 
			WHEN (isNumeric(REPLACE(REPLACE(REPLACE(isNull(Checks.Serial,''),' ',''),'-',''),'!','')) > 0) 
				THEN CAST(REPLACE(REPLACE(REPLACE(Checks.Serial,' ',''),'-',''),'!','') AS BIGINT)
				ELSE NULL
		END                             AS NumericSerial,
		Checks.TransactionCode,
		Checks.Amount,
		Checks.RemitterName,
		Checks.Account,
		Checks.RT,
		@parmModificationDate           AS ModificiationDate
	FROM (
		  SELECT 
			   Transactions.att.value('@BatchID', 'bigint')            AS BatchID,
			   Transactions.att.value('@SourceBatchID', 'bigint')      AS SourceBatchID,
			   Transactions.att.value('@TransactionID', 'int')         AS TransactionID,
			   Transactions.att.value('@BatchSequence', 'int')         AS BatchSequence,
			   Transactions.att.value('@Sequence', 'int')              AS Sequence
		  FROM @XBatch.nodes('/Batches/Batch/Transaction') Transactions(att)
		  ) T
		  INNER JOIN (
				 SELECT
					 d1.DateKey AS ImmutableDateKey,      
					 d2.DateKey AS DepositDateKey,
					 B.BatchID,
					 B.SourceBatchID,
					 B.TransactionID, 
					 B.BatchSequence,						
					 B.DepositStatus
				 FROM (
						SELECT 
							   Batch.att.value('@ImmutableDate', 'datetime')    AS ImmutableDateKey,
							   Batch.att.value('@ImmutableDate', 'datetime')    AS DepositDateKey,		
							   Batch.att.value('@BatchID', 'bigint')            AS BatchID,
							   Batch.att.value('@SourceBatchID', 'bigint')      AS SourceBatchID,
							   Batch.att.value('@TransactionID', 'int')         AS TransactionID,
							   Batch.att.value('@DepositStatus', 'varchar(80)') AS DepositStatus,
							   Batch.att.value('@BatchSequence', 'int')         AS BatchSequence,
							   Batch.att.value('@SystemType', 'tinyint')        AS SystemType							   
						FROM @XBatch.nodes('/Batches/Batch') Batch(att)
						) B
						LEFT JOIN RecHubData.dimDates d1 ON CAST(CONVERT(VARCHAR,B.ImmutableDateKey,101) AS datetime) = d1.CalendarDate
						LEFT JOIN RecHubData.dimDates d2 ON CAST(CONVERT(VARCHAR,B.DepositDateKey,101) AS datetime) = d2.CalendarDate
				 ) B ON B.BatchID = T.BatchID
						AND B.SourceBatchID = T.SourceBatchID
						AND B.BatchSequence = T.BatchSequence
		  INNER JOIN (
				 SELECT
						Checks.att.value('@BatchID', 'bigint')              AS BatchID,
						Checks.att.value('@SourceBatchID', 'bigint')        AS SourceBatchID,
						Checks.att.value('@TransactionID', 'int')           AS TransactionID,
						Checks.att.value('@TransactionSequence', 'int')     AS TransactionSequence,
						Checks.att.value('@BatchSequence', 'int')           AS BatchSequence,
						Checks.att.value('@CheckSequence', 'int')           AS CheckSequence,
						Checks.att.value('@Serial', 'varchar(30)')          AS Serial,
						Checks.att.value('@RT', 'varchar(30)')              AS RT,
						Checks.att.value('@Account', 'varchar(30)')         AS Account,
						Checks.att.value('@RemitterName', 'varchar(60)')    AS RemitterName,
						Checks.att.value('@TransactionCode', 'varchar(30)') AS TransactionCode, 
						Checks.att.value('@Amount', 'money')                AS Amount
				 FROM @XBatch.nodes('/Batches/Batch/Transaction/Checks') Checks(att)
				 ) Checks 
				  ON Checks.BatchID = T.BatchID
				    AND Checks.SourceBatchID = T.SourceBatchID
				    AND Checks.BatchSequence = T.BatchSequence
					AND Checks.TransactionID = T.TransactionID ;


    --update the factChecks table by joining the xml data from the local table
    --COALESCE because the update record may not contain each field
	-------------------------------------------------------------------------
	-- collect all the keys of the records we are going to logically change
	-- insert new records to match the record with changes applied
	-- then flag the older records as logically deleted
	insert into #CheckKeys
	(
	    factCheckKey
	)	
	SELECT
	    RecHubData.factChecks.factCheckKey
	FROM	
	    RecHubData.factChecks
		INNER JOIN #CheckUpdate UpdateTable 
		    ON RecHubData.factChecks.BankKey = UpdateTable.BankKey
				AND RecHubData.factChecks.OrganizationKey = UpdateTable.OrganizationKey
				AND RecHubData.factChecks.ClientAccountKey = UpdateTable.ClientAccountKey
				AND RecHubData.factChecks.ImmutableDateKey = UpdateTable.ImmutableDateKey
				AND RecHubData.factChecks.DepositDateKey = UpdateTable.DepositDateKey
				AND RecHubData.factChecks.BatchID = UpdateTable.BatchID
				AND RecHubData.factChecks.SourceBatchID = UpdateTable.SourceBatchID
				AND RecHubData.factChecks.TransactionID = UpdateTable.TransactionID
				AND RecHubData.factChecks.BatchSequence = UpdateTable.BatchSequence
				AND RecHubData.factChecks.IsDeleted = 0;

	INSERT INTO RecHubData.factChecks
	(
	     IsDeleted
         ,BankKey
         ,OrganizationKey
         ,ClientAccountKey
         ,DepositDateKey
         ,ImmutableDateKey
         ,SourceProcessingDateKey
         ,BatchID
         ,SourceBatchID
         ,BatchNumber
         ,BatchSourceKey
         ,BatchPaymentTypeKey
         ,DDAKey
         ,BatchCueID
         ,DepositStatus
         ,TransactionID
         ,TxnSequence
         ,SequenceWithinTransaction
         ,BatchSequence
         ,CheckSequence
         ,NumericRoutingNumber
         ,NumericSerial
         ,Amount
         ,CreationDate
         ,ModificationDate
         ,BatchSiteCode
         ,RoutingNumber
         ,Account
         ,TransactionCode
         ,Serial
         ,RemitterName
	)
	SELECT
	     RecHubData.factChecks.IsDeleted
         ,RecHubData.factChecks.BankKey
         ,RecHubData.factChecks.OrganizationKey
         ,RecHubData.factChecks.ClientAccountKey
         ,RecHubData.factChecks.DepositDateKey
         ,RecHubData.factChecks.ImmutableDateKey
         ,RecHubData.factChecks.SourceProcessingDateKey
         ,RecHubData.factChecks.BatchID
         ,RecHubData.factChecks.SourceBatchID
         ,RecHubData.factChecks.BatchNumber
         ,RecHubData.factChecks.BatchSourceKey
         ,RecHubData.factChecks.BatchPaymentTypeKey
         ,RecHubData.factChecks.DDAKey
         ,RecHubData.factChecks.BatchCueID
         ,RecHubData.factChecks.DepositStatus
         ,RecHubData.factChecks.TransactionID
         ,RecHubData.factChecks.TxnSequence
         ,RecHubData.factChecks.SequenceWithinTransaction
         ,RecHubData.factChecks.BatchSequence
         ,RecHubData.factChecks.CheckSequence
         ,RecHubData.factChecks.NumericRoutingNumber
         ,COALESCE(UpdateTable.NumericSerial,RecHubData.factChecks.NumericSerial)     AS NumericSerial
         ,COALESCE(UpdateTable.Amount,RecHubData.factChecks.Amount)                   AS Amount
         ,RecHubData.factChecks.CreationDate
         ,UpdateTable.ModificationDate                                                AS ModificatoinDate
         ,RecHubData.factChecks.BatchSiteCode
         ,RecHubData.factChecks.RoutingNumber
         ,RecHubData.factChecks.Account
         ,COALESCE(UpdateTable.TransactionCode,RecHubData.factChecks.TransactionCode) AS TransactionCode
         ,COALESCE(UpdateTable.Serial,RecHubData.factChecks.Serial)                   AS Serial
         ,COALESCE(UpdateTable.RemitterName,RecHubData.factChecks.RemitterName)       AS RemitterName
	FROM	
	    RecHubData.factChecks
		INNER JOIN #CheckUpdate UpdateTable 
		  ON RecHubData.factChecks.BankKey = UpdateTable.BankKey
			AND RecHubData.factChecks.OrganizationKey = UpdateTable.OrganizationKey
			AND RecHubData.factChecks.ClientAccountKey = UpdateTable.ClientAccountKey
			AND RecHubData.factChecks.ImmutableDateKey = UpdateTable.ImmutableDateKey
			AND RecHubData.factChecks.DepositDateKey = UpdateTable.DepositDateKey
			AND RecHubData.factChecks.BatchID = UpdateTable.BatchID
			AND RecHubData.factChecks.SourceBatchID = UpdateTable.SourceBatchID
			AND RecHubData.factChecks.TransactionID = UpdateTable.TransactionID
			AND RecHubData.factChecks.BatchSequence = UpdateTable.BatchSequence
			AND RecHubData.factChecks.IsDeleted = 0;

	----------------------
	--Now we have the records inserted
	--Go flag the previous records as deleted
	UPDATE
	    RecHubData.factChecks
	Set
	    RecHubData.factChecks.IsDeleted = 1,
		RecHubData.factChecks.ModificationDate = @parmModificationDate
	FROM
	    RecHubData.factChecks
		INNER JOIN #CheckKeys AS ChkKey
		    ON RecHubData.factChecks.factCheckKey =  ChkKey.factCheckKey;


	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0;
	END

END TRY
BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage	NVARCHAR(4000),
				@ErrorSeverity	INT,
				@ErrorState		INT,
				@ErrorLine		INT
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE()

		IF @@NESTLEVEL > 3 --update only if we got here by two service broker calls
			SET @ErrorMessage = 'usp_ICON_factChecks_Upd (Line: %d)-> ' + @ErrorMessage
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
END CATCH
