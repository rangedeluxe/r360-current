--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionDisks_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionDisks_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionDisks_Ins
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionDisks_Ins
(
	@parmPartitionIdentifier	VARCHAR(20), 
    @parmDiskPath				VARCHAR(256)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: RecHubSystem.PartitionDisks insert stored procedure
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 04/22/2013 WI 90767 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_PartitionDisksCreate
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

    INSERT INTO RecHubSystem.PartitionDisks
		(PartitionManagerID, DiskOrder, DiskPath)
    SELECT 
		PM.PartitionManagerID, 
        COALESCE(MAX(PD.DiskOrder)+1,1) as NextDisk,
        @parmDiskPath as DiskPath
    FROM 
		RecHubSystem.PartitionManager PM 
        LEFT JOIN RecHubSystem.PartitionDisks PD
            ON PM.PartitionManagerID = PD.PartitionManagerID
    WHERE 
		PartitionIdentifier = @parmPartitionIdentifier
    GROUP BY 
		PM.PartitionManagerID;

END TRY

BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
