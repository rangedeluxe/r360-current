--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DIT_Del_Facts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DIT_Del_Facts') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DIT_Del_Facts
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DIT_Del_Facts
(
	@parmBatches		XML,
	@parmRowsDeleted	BIT OUT 
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/06/2012
*
* Purpose: Deletes fact table entries for a batch by:
* <Batches>
*     <Batch GlobalBatchID="" SiteID="" LockboxID="" BankID="" DepositDate="" />
* </Batches>
*
* Modification History
* 03/06/2012 CR 50981 WJS	Created
* 04/25/2013 WI 90609 JPB	Updated for 2.0.
*							Moved to RecHubSystem.
*							Renamed from usp_DataImportIntegrationServices_DeleteFactRecords.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE @iBankKey INT,
		@iCustomerKey INT,
		@iLockboxKey INT,
		@iProcessingDateKey INT,
		@iSourceProcessingDateKey INT,
		@iDepositDateKey INT,
		@iBatchID INT,
		@bKeepStats BIT,
		@dtQueuedTime DATETIME,
		@dtProcessingDate DATETIME;
		

BEGIN TRY
	SELECT
		@iBatchID = BatchID,
		@dtProcessingDate = ProcessingDate
	FROM 
	(
		SELECT	Batch.att.value('@ProcessingDate','datetime') AS ProcessingDate,
				Batch.att.value('@BatchID', 'int') AS BatchID
		FROM	@parmBatches.nodes('/Batches/Batch') Batch (att)
	) BatchData;


	EXEC RecHubData.usp_ConvertSourceKeysToRecHubKeys @parmBatches, @iBankKey OUTPUT, @iCustomerKey OUTPUT, @iLockboxKey OUTPUT, @iProcessingDateKey OUTPUT, @iDepositDateKey OUTPUT, @iSourceProcessingDateKey OUT;
	EXEC RecHubData.usp_factBatchSummary_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID,  @parmRowsDeleted OUTPUT;
	EXEC RecHubData.usp_factTransactionSummary_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
	EXEC RecHubData.usp_factChecks_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
	EXEC RecHubData.usp_factDocuments_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
	EXEC RecHubData.usp_factStubs_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
	EXEC RecHubData.usp_factDataEntrySummary_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
	EXEC RecHubData.usp_factDataEntryDetails_Upd_IsDeleted @iBankKey, @iCustomerKey, @iLockboxKey, @iProcessingDateKey, @iDepositDateKey, @iSourceProcessingDateKey, @iBatchID;
END TRY
BEGIN CATCH
	IF( @@NESTLEVEL > 1 )
	BEGIN	-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
