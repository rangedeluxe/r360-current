--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_UpdateResponse
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_IMSInterfaceQueue_UpdateResponse') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_UpdateResponse
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_UpdateResponse
(
	@SiteBankID			INT,
	@SiteLockboxID		INT,
	@ProcessingDateKey	INT,
	@BatchID			INT,
	@NewQueueStatus		SMALLINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/15/2011
*
* Purpose: Update the status of a record.
*
* Modification History
* 02/15/2011 CR 32828 JMC	Created
* 02/25/2013 WI 87325 CRG Remove SQLIMSIntegration.cs
* 02/25/2013 WI 89022 CRG Change Schema for RecHubSystem.usp_IMSInterfaceQueue_UpdateResponse
******************************************************************************/
BEGIN TRY
	UPDATE 
		RecHubSystem.IMSInterfaceQueue 
	SET 
		RecHubSystem.IMSInterfaceQueue.QueueStatus		= @NewQueueStatus,
		RecHubSystem.IMSInterfaceQueue.ModificationDate = GETDATE(),
		RecHubSystem.IMSInterfaceQueue.ModifiedBy		= SUSER_SNAME()
	WHERE
		RecHubSystem.IMSInterfaceQueue.SiteBankID				= @SiteBankID
		AND RecHubSystem.IMSInterfaceQueue.SiteLockboxID		= @SiteLockboxID
		AND RecHubSystem.IMSInterfaceQueue.ProcessingDateKey	= @ProcessingDateKey
		AND RecHubSystem.IMSInterfaceQueue.BatchID				= @BatchID
		AND RecHubSystem.IMSInterfaceQueue.QueueStatus			= 99
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
