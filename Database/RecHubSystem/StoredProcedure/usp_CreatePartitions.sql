--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_CreatePartitions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CreatePartitions') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CreatePartitions
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CreatePartitions
       @parmRangeDate SMALLDATETIME,
       @parmPartitionScheme SYSNAME,
       @parmPartitionFunction SYSNAME,
       @parmFileGroupName VARCHAR(1000),
       @parmPartitionManagerID INT,
       @parmFirstDiskID INT,
       @parmNextDiskID INT,
       @parmLastDiskID INT,
       @parmFileSize INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 3/09/2009
*
* Purpose: Creates a new filegroup and file and creates/alters the partition 
*      scheme and function to use the new file/filegroup.
*
* Modification History
* 03/09/2008 CR 25817		JJR	Created
* 07/09/2012 CR 53205		JPB	Removed call to create check constraints.
* 03/13/2013 WI 90599		JBS	Update to 2.0 release. Change Schema Name.
* 03/22/2013 PT 138837605	MGE Update Create Partition Function to use Right Boundary and Create Partition Scheme to work properly with Right Boundary
* 03/28/2013 PT 138837605	MGE Initalize CatchAll filegroup and file
******************************************************************************/
DECLARE @ReturnValue INT,
       @RangeDate INT,
       @DiskPath VARCHAR(1000),
       @FirstDiskID INT,
       @NextDiskID INT,
       @LastDiskID INT, 
       @SQLcmd VARCHAR(max),
	   @msg VARCHAR(max),
	   @CatchAllFGName VARCHAR(128)

SET @ReturnValue = 0

BEGIN TRY
       SELECT @RangeDate=DateKey
       FROM RecHubData.dimDates
       WHERE CalendarDate=@parmRangeDate

       SET @SQLcmd = 'ALTER DATABASE '+ DB_NAME() +' ADD FILEGROUP '+ @parmFileGroupName
       RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
	   EXEC (@SQLcmd)

       --add file to filegroup
       SELECT @DiskPath = CASE RIGHT(DiskPath,1) WHEN '\' THEN DiskPath ELSE DiskPath + '\' END       
       FROM RecHubSystem.PartitionDisks
       WHERE PartitionManagerID = @parmPartitionManagerID
              AND DiskOrder = @parmNextDiskID

       SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' ADD FILE  (NAME = '+ @parmFileGroupName + ','
       SET @SQLcmd = @SQLcmd + ' FILENAME = ' + ''''+ @DiskPath + @parmFileGroupName + '.mdf'+'''' + ','
       SET @SQLcmd = @SQLcmd + ' SIZE = ' + CAST(@parmFileSize as VARCHAR) + ','
       SET @SQLcmd = @SQLcmd + ' MAXSIZE = UNLIMITED, FILEGROWTH = 10%)'
       SET @SQLcmd = @SQLcmd + ' TO FILEGROUP '+ @parmFileGroupName
	   RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
       EXEC (@SQLcmd)      

       --CREATE Partition Scheme/Function if it does not already exist; otherwise, ALTER
	   --ALSO - Check to see if CatchAll File and FileGroup exist and create if necessary
		IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = @parmPartitionFunction)
		   BEGIN

				RAISERROR ('INITIALIZING CATCHALL AND SCHEME AND FUNCTION',10,1) WITH NOWAIT;
				Print @DiskPath;
				SET @CatchAllFGName = @parmPartitionScheme + 'CatchAll'
				IF EXISTS(SELECT 1 from sys.filegroups where name = @CatchAllFGName)
					RAISERROR('DataImportCatchAll filegroup already exists',10,1) WITH NOWAIT
				ELSE
					BEGIN
					RAISERROR('Creating Filegroup CatchAll',10,1) WITH NOWAIT;
					SELECT @SQLcmd = 'ALTER DATABASE ' + DB_Name() + ' ADD FILEGROUP ' +@parmPartitionScheme + 'CatchAll'
					RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
					EXEC (@SQLcmd)
					END;
												
				RAISERROR('Creating database File CatchAll',10,1) WITH NOWAIT;
				
				IF EXISTS(SELECT 1 from sys.database_files where name = @CatchAllFGName)
					RAISERROR('DataImportCatchAll database file already exists',10,1) WITH NOWAIT
				ELSE
					BEGIN
					SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' ADD FILE  (NAME = '+ @parmPartitionScheme + 'CatchAll,'
					SET @SQLcmd = @SQLcmd + ' FILENAME = ' + ''''+ @DiskPath + @parmPartitionScheme + 'CatchAll.mdf' + '''' + ','
					SET @SQLcmd = @SQLcmd + ' SIZE = ' + CAST(@parmFileSize as VARCHAR) + ','
					SET @SQLcmd = @SQLcmd + ' MAXSIZE = UNLIMITED, FILEGROWTH=10MB)' 
					SET @SQLcmd = @SQLcmd + ' TO FILEGROUP '+ @parmPartitionScheme  + 'CatchAll'
					RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
					EXEC (@SQLcmd)
					END;
				SET @SQLcmd='CREATE PARTITION FUNCTION ['+@parmPartitionFunction+'](int) AS RANGE RIGHT FOR VALUES ('+CAST(@RangeDate as VARCHAR)+')';
				RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
				EXEC (@SQLcmd);

				SET @SQLcmd='CREATE PARTITION SCHEME ['+@parmPartitionScheme+'] AS PARTITION ['+@parmPartitionFunction+'] TO (' + @parmPartitionScheme + 'CatchAll,' +@parmFileGroupName + ')'
				RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
				EXEC (@SQLcmd)

       END
       ELSE
       BEGIN
              SET @SQLcmd = 'ALTER PARTITION SCHEME '+@parmPartitionScheme+' NEXT USED '+@parmFileGroupName+''
              EXEC (@SQLcmd)       

              SET @SQLcmd = 'ALTER PARTITION FUNCTION '+@parmPartitionFunction+'() SPLIT RANGE('+CAST(@RangeDate as VARCHAR)+')'
              EXEC (@SQLcmd)       
              --Set CHECK constraint on fact tables
              EXEC RecHubSystem.usp_CreateCheckConstraintsForPartition @parmPartitionManagerID
       END
       
       --Update Last Disk used
       UPDATE RecHubSystem.PartitionManager
       SET LastDiskID = @parmNextDiskID
       WHERE PartitionManagerID = @parmPartitionManagerID       

       RETURN @ReturnValue
END TRY 
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
