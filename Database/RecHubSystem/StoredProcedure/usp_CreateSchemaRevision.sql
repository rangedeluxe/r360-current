--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CreateSchemaRevision
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CreateSchemaRevision') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CreateSchemaRevision
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CreateSchemaRevision
(
	@parmSchemaType VARCHAR(128),
	@parmXSDVersion	VARCHAR(12),
	@parmXSD		XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/19/2009
*
* Purpose: This SP will add a new .XSD file into the OLTA.ImportSchemas table with
*      the next Revision #.
*
* Modification History
* 03/19/2009 CR 25817 JJR	Created
* 02/09/2012 CR 50116 JPB	Changed @parmSchemaType to 128, added @parmXSDVersion,
*							added updates to standard modificiation columns.
* 03/15/2013 WI 90601 JBS	Update to 2.0 release. Change Schema Name
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON
DECLARE @ImportSchemaTypeID  INT;

BEGIN TRY

	SELECT	@ImportSchemaTypeID  = COALESCE(ImportSchemaTypeID ,-1)
	FROM	RecHubSystem.ImportSchemaTypes
	WHERE	ImportSchemaType = @parmSchemaType;
	
	IF( @ImportSchemaTypeID > 0 )
	BEGIN
		IF EXISTS(SELECT 1 FROM RecHubSystem.ImportSchemas WHERE ImportSchemaTypeID = @ImportSchemaTypeID AND XSDVersion = @parmXSDVersion )
		BEGIN
			UPDATE	RecHubSystem.ImportSchemas 
			SET		ImportSchema = @parmXSD,
					ModificationDate = GETDATE(),
					ModifiedBy = SUSER_SNAME()
			WHERE	ImportSchemaTypeID = @ImportSchemaTypeID 
					AND XSDVersion = @parmXSDVersion;
		END
		ELSE
		BEGIN
			INSERT INTO RecHubSystem.ImportSchemas(ImportSchemaTypeID,XSDVersion,ImportSchema)
			VALUES (@ImportSchemaTypeID,@parmXSDVersion,@parmXSD);
		END
	END
	ELSE RAISERROR('Could not locate SchemaType %s, schema not saved.',16,1,@parmSchemaType);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
