--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_ICON_UpdateBatchLevelInfo
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubSystem.usp_ICON_UpdateBatchLevelInfo') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ICON_UpdateBatchLevelInfo
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ICON_UpdateBatchLevelInfo
(
	@parmBatchID BIGINT,
	@parmDepositDateKey INT,
	@parmSourceProcessingDateKey INT,
	@parmBatchNumber INT,
	@parmBatchPaymentType VARCHAR(30)
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/26/2015
*
* Purpose: Update batch fact tables with new Processing Date, Batch Number, Payment Type
*
* Modification History
* 05/26/2015 WI 215102 JPB	Created
******************************************************************************/
SET NOCOUNT ON 

DECLARE @BatchPaymentTypeKey TINYINT,
		@ModificationDate DATETIME,
		@NewProcessingDateKey BIT,
		@NewBatchNumber BIT,
		@NewBatchPaymentTypeKey BIT;

BEGIN TRY

	SELECT
		@BatchPaymentTypeKey=BatchPaymentTypeKey
	FROM
		RecHubData.dimBatchPaymentTypes
	WHERE
		UPPER(ShortName) = UPPER(@parmBatchPaymentType);

	SELECT
		@NewProcessingDateKey = CASE
			WHEN @parmSourceProcessingDateKey = SourceProcessingDateKey THEN 0
			ELSE 1
			END,
		@NewBatchNumber = CASE
			WHEN @parmBatchNumber = BatchNumber THEN 0
			ELSE 1
			END,
		@NewBatchPaymentTypeKey = CASE
			WHEN @BatchPaymentTypeKey = BatchPaymentTypeKey THEN 0
			ELSE 1
			END
	FROM
		RecHubData.factBatchSummary
	WHERE
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND IsDeleted = 0;

	IF( @NewProcessingDateKey=1 OR @NewBatchNumber=1 OR @NewBatchPaymentTypeKey=1 )
	BEGIN
		BEGIN TRAN;

		SET @ModificationDate = GETDATE();

		UPDATE 
			RecHubData.factBatchData
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			ModificationDate = @ModificationDate,
			ModifiedBy = SUSER_SNAME()
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;
		
		UPDATE
			RecHubData.factBatchSummary
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factCheckImages
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE 
			RecHubData.factChecks
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;
		UPDATE
			RecHubData.factDataEntryDetails
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factDataEntrySummary
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factDocumentImages
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factDocuments
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factItemData
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factRawPaymentData
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factStubs
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		UPDATE
			RecHubData.factTransactionSummary
		SET
			SourceProcessingDateKey = @parmSourceProcessingDateKey,
			BatchNumber = @parmBatchNumber,
			BatchPaymentTypeKey = @BatchPaymentTypeKey,
			ModificationDate = @ModificationDate
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND IsDeleted = 0;

		COMMIT TRAN;
	END

END TRY
BEGIN CATCH
	IF XACT_STATE() != 0 ROLLBACK TRANSACTION
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


