--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_IMSInterfaceQueue_UpdateStatus
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_IMSInterfaceQueue_UpdateStatus') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_UpdateStatus
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_IMSInterfaceQueue_UpdateStatus
(
       @parmQueueID				UNIQUEIDENTIFIER,
       @parmQueueStatus			SMALLINT,
       @parmIncrementRetryCount BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright (c) 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/01/2009
*
* Purpose: Update the status of a record.
*
* Modification History
* 07/09/2009 CR 25817 JPB	Created
* 04/16/2010 CR 29388 WJS   Removed Set no count on and add QueueStatus=10
* 10/28/2010 CR 31235 JMC	1.)Added ModificationDate and ModifiedBy as fields 
*                        	   being updated by the stored procedure. 
*                        	2.)Added parameter '@parmIncrementRetryCount' that
*                        	   will optionally increment RetryCount 	
* 02/25/2013 WI 87325 CRG Remove SQLIMSIntegration.cs
* 02/25/2013 WI 89017 CRG Change Schema for RecHubSystem.usp_IMSInterfaceQueue_UpdateStatus
******************************************************************************/

BEGIN TRY

IF @parmIncrementRetryCount > 0
BEGIN
	UPDATE 
		RecHubSystem.IMSInterfaceQueue 
	SET 
		RecHubSystem.IMSInterfaceQueue.QueueStatus		= @parmQueueStatus,
		RecHubSystem.IMSInterfaceQueue.RetryCount		= RecHubSystem.IMSInterfaceQueue.RetryCount + 1,
		RecHubSystem.IMSInterfaceQueue.ModificationDate	= GETDATE(),
		RecHubSystem.IMSInterfaceQueue.ModifiedBy		= SUSER_SNAME()
	WHERE 
		RecHubSystem.IMSInterfaceQueue.QueueID			= @parmQueueID 
END
ELSE
BEGIN
	UPDATE 
		RecHubSystem.IMSInterfaceQueue 
	SET 
		RecHubSystem.IMSInterfaceQueue.QueueStatus		= @parmQueueStatus,
		RecHubSystem.IMSInterfaceQueue.ModificationDate	= GETDATE(),
		RecHubSystem.IMSInterfaceQueue.ModifiedBy			= SUSER_SNAME()
	WHERE 
		RecHubSystem.IMSInterfaceQueue.QueueID			= @parmQueueID 
END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
