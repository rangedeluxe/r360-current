--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingDocumentTypes
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CDSQueue_Get_PendingDocumentTypes') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingDocumentTypes;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingDocumentTypes
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/16/2013
*
* Purpose: Retreive list of Document Types that are ready for processing.
*
* Action codes could be:
* ActionCode:
*	1 - Insert/Update
*	2 - Delete

* QueueStatus:
*	10 - Ready to sent to integraPAY Consolidated Database
*	15 - Failed processing on integraPAY Consolidated Database - but can resend
*	20 - Sent to integraPAY Consolidated Database, waiting for a response
*	30 - Failed processing on integraPAY Consolidated Database - but cannot resend
*	99 - Successfully processed on integraPAY Consolidated Database
*
*
* QueueType:
*	10 - OLOrganizationID
*	20 - OLClientAccountID
*	70 - Document Type
*
*
* Modification History
* 06/16/2013 WI 106119 JPB	Created
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

BEGIN TRY
	;WITH PendingList AS
	(
		SELECT 	TOP(150000) MIN(RecHubSystem.CDSQueue.CDSQueueID) AS RecordID,RecHubSystem.CDSQueue.FileDescriptor
		FROM 	RecHubSystem.CDSQueue
		WHERE 	RecHubSystem.CDSQueue.QueueType = 70
				AND RecHubSystem.CDSQueue.QueueStatus <= 20
		GROUP BY RecHubSystem.CDSQueue.FileDescriptor
		ORDER BY RecordID
	)
	SELECT	PendingList.RecordID AS DocumentType_Id,
			RecHubSystem.CDSQueue.FileDescriptor,
			RecHubSystem.CDSQueue.FileDescription,
			RecHubSystem.CDSQueue.ModifiedBy,
			RecHubSystem.CDSQueue.ModificationDate,
			RecHubSystem.CDSQueue.ActionCode
	FROM	RecHubSystem.CDSQueue 
			INNER JOIN PendingList ON RecHubSystem.CDSQueue.CDSQueueID = PendingList.RecordID
	WHERE 	(RecHubSystem.CDSQueue.QueueStatus = 10) OR (RecHubSystem.CDSQueue.QueueStatus = 20) OR (RecHubSystem.CDSQueue.QueueStatus = 15 AND RecHubSystem.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RecHubSystem.CDSQueue.RetryCount),GETDATE()));
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
