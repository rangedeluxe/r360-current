--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_LogicalDelete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_LogicalDelete') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_LogicalDelete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_LogicalDelete
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 07/20/2013
*
* Purpose: Preparing work tables for logical-deleting 
*	
*
* Modification History
* 07/20/2013 WI 117320 PS	Created.
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	IF( OBJECT_ID('dbo.WT_TableListIsDeleted1', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_TableListIsDeleted1;
	IF( OBJECT_ID('dbo.WT_TableListIsDeleted2', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_TableListIsDeleted2;
	IF( OBJECT_ID('dbo.WT_TableListIsDeleted3', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_TableListIsDeleted3;
	IF( OBJECT_ID('dbo.WT_ProcessorLDeleteA', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_ProcessorLDeleteA;
	IF( OBJECT_ID('dbo.WT_ProcessorLDeleteB', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_ProcessorLDeleteB;
	IF( OBJECT_ID('dbo.WT_ProcessorLDeleteC', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_ProcessorLDeleteC;
	IF( OBJECT_ID('dbo.WT_ProcessorLDeleteD', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_ProcessorLDeleteD;

	CREATE TABLE [dbo].[WT_ProcessorLDeleteA]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[DeleteType] [int] NULL,
		[CmdExecDel] [nvarchar](1000) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_ProcessorLDeleteB]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[DeleteType] [int] NULL,
		[CmdExecDel] [nvarchar](1000) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_ProcessorLDeleteC]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[DeleteType] [int] NULL,
		[CmdExecDel] [nvarchar](1000) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_ProcessorLDeleteD]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[DeleteType] [int] NULL,
		[CmdExecDel] [nvarchar](1000) NULL
	) ON [PRIMARY];


	SELECT TOP 1000 
		[Table_Schema],
		[Table_Name],
		1 AS DeleteType
	INTO 
		dbo.WT_TableListIsDeleted1
	FROM 
		[dbo].[WT_TableColumnlist]
	WHERE 
		[Column_Name] = 'IsDeleted' 
		AND [Table_Schema] = 'RecHubData' 
		AND LEFT([Table_Name], 8) <> 'factNoti'
	UNION  
	SELECT TOP 1000 
		[Table_Schema],
		[Table_Name],
		2 AS DeleteType
	FROM 
		[dbo].[WT_TableColumnlist]
	WHERE 
		(LEFT([Table_Name], 8) = 'factNoti')
	UNION
	SELECT TOP 1000 
		[Table_Schema],
		[Table_Name],
		3 AS DeleteType
	FROM 
		[dbo].[WT_TableColumnlist]
	WHERE 
		[Column_Name] = 'IsDeleted' 
		AND [Table_Schema] <> 'RecHubData';

	SELECT 
		IDENTITY(int,1,1) AS TblID, 
		* 
	INTO 
		dbo.WT_TableListIsDeleted2
	FROM 
		dbo.WT_TableListIsDeleted1;

	SELECT 
		*, 
		CASE 
			WHEN TblID%4 = 0 THEN 'A' 
			WHEN TblID%4 = 1 THEN 'B'
			WHEN TblID%4 = 2 THEN 'C'
			WHEN TblID%4 = 3 THEN 'D'
		END AS CATEGORY
	INTO 
		dbo.WT_TableListIsDeleted3
	FROM 
		dbo.WT_TableListIsDeleted2;

	DROP TABLE dbo.WT_TableListIsDeleted1;
	DROP TABLE dbo.WT_TableListIsDeleted2;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
