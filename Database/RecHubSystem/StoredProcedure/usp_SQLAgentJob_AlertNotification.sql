--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_SQLAgentJob_AlertNotification
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_SQLAgentJob_AlertNotification') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_SQLAgentJob_AlertNotification
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_SQLAgentJob_AlertNotification  
(
	@Job_ID		UNIQUEIDENTIFIER 
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/11/2015
*
* Purpose: This will be used in SQL Agent jobs as the last step to query if the job or steps failed. 
*          If Error is detected add that Error to EventLog as PROCESSEXCEPTION using RecHubAlert.usp_EventLog_SystemLevel_Ins.
*			Reference for original source http://www.sqlteam.com/forums/topic.asp?TOPIC_ID=108788 
*
*  Statuses we can trap and report on
*  @Stat_Failed			= 0
*  @Stat_Succeeded		= 1
*  @Stat_Retry			= 2
*  @Stat_Canceled		= 3 
*  @Stat_In_progress	= 4 
*
* Modification History
* 03/11/2015 WI 191974	JBS	Created 
* 03/30/2015 WI 191974	JBS	Right sized some working fields to remove truncation errors.
* 12/07/2016 PT 133387873 JBS	Fix formating the Time portion for Run_DateTime.  If between midnight and 1am
*								The column had minimum placeholders for digits to convert to string
*********************************************************************************************************/
SET NOCOUNT ON; 

DECLARE @Today				DATETIME,
		@Crlf				VARCHAR(10),
		@Stat_Failed		TINYINT,
		@Body				VARCHAR(8000) = '',
		@Job_name			SYSNAME, 
		@Step_Name			SYSNAME, 
		@Err_severity		INT, 
		@Run_datetime		DATETIME, 
		@DBname				SYSNAME,
		@Command			VARCHAR(MAX), 
		@ErrMessage			VARCHAR(4000)

BEGIN TRY 

	SET @Body = '';
	SET @CrLf = CHAR(10); 
	SET @Stat_Failed	  = 0;
	SET @Today = GETDATE();

	DECLARE @CurFailedJobs TABLE
	(
		[Name] SYSNAME, 
		Step_Name SYSNAME, 
		Sql_Severity INT, 
		Database_Name SYSNAME, 
		Run_DateTime DATETIME, 
		Command		VARCHAR(MAX), 
		[Message]	VARCHAR(4000)
	);

	INSERT INTO @CurFailedJobs([Name], Step_Name, Sql_Severity, Database_Name, Run_DateTime, Command, [Message])
		SELECT 
			SJ.[Name], 
			SJH.Step_Name, 
			SJH.Sql_Severity, 
			SJS.Database_Name ,
			Run_DateTime= CONVERT(DATETIME, LEFT( Run_Date ,4)+'/'+SUBSTRING( Run_Date ,5,2)+'/'+RIGHT( Run_Date ,2)+' '+ LEFT( Run_Time ,2)+':'+SUBSTRING( Run_Time ,3,2)+':'+RIGHT( Run_Time ,2) ),
			SJS.Command, 
			SJH.[Message]	
		FROM MSDB.dbo.SYSJOBS SJ 
			INNER JOIN (SELECT 
					Instance_ID,
					Job_ID,
					Step_ID,
					Step_Name,
					Sql_Severity,
					[Message],
					Run_Status,
					[Server],
					Run_Date = CONVERT(VARCHAR(8), Run_Date ),
					Run_Time= CONVERT(VARCHAR(6), REPLACE(STR(Run_Time,6), ' ', 0))				 --  PT 133387873 Replacing spaces wtih zeroes so when we substring above it will have values needed
			  FROM MSDB.dbo.SYSJOBHISTORY) SJH ON SJ.Job_ID = SJH.Job_ID 
	
			INNER JOIN  MSDB.dbo.SYSJobsteps SJS ON  SJS.Job_ID = SJH.Job_ID AND SJS.Step_ID = SJH.Step_ID 
	
			-- SJH_Min contains the most recent Instance_ID (an identity column) from where we should start checking for any failed status records.
			INNER JOIN (  
				-- to account for when there is are multiple log history
				SELECT  
					Job_ID, 
					Instance_ID = MAX(Instance_ID) 
				FROM 
					MSDB.dbo.SYSJOBHISTORY 
				WHERE 
					Job_ID = @Job_ID  AND Step_ID = 0 
				GROUP BY Job_ID
				UNION  
				-- to account for when you run the job for the first time, there is no history, there will not be any records where the Step_ID=0.
				SELECT
					Job_ID, 
					Instance_ID = MIN(Instance_ID) 
				FROM 
					MSDB.dbo.SYSJOBHISTORY 
				WHERE 
					Job_ID = @Job_ID  
					AND NOT EXISTS (SELECT 1 FROM MSDB.dbo.SYSJOBHISTORY WHERE Job_ID = @Job_ID  AND Step_ID = 0 ) 
				GROUP BY Job_ID
				)SJH_Min ON SJH_Min.Job_ID = SJ.Job_ID 
					AND SJH.Instance_ID > SJH_Min.Instance_ID -- we only want the most recent error Message(s).
		WHERE  
			SJ.Job_ID = @Job_ID  
			AND SJH.Step_ID <> 0					--exclude the job outcome step
			AND SJH.run_status IN (@stat_Failed )	--filter for only failed status
		ORDER BY 
			SJH.Instance_ID;

	-- Process Errors collected
	SET @job_name = NULL;
	SELECT TOP 1 
		@job_name		= [Name], 
		@Step_Name		= Step_Name, 
		@Err_severity	= Sql_Severity, 
		@DBname			= Database_Name, 
		@Run_DateTime	= Run_DateTime, 
		@Command		= Command, 
		@ErrMessage		= [Message]
	FROM @CurFailedJobs;      

	WHILE @job_name IS NOT NULL
		BEGIN
			-- Build the Email Body
			SET @Body = @Body + 
					 'Step name= ' + @Step_Name + @CrLf + 
					 'DB Name  = ' + convert(varchar(50), ISNULL(@DBname,''))   + @CrLf + 
       					 'Run Date = ' + convert(varchar(50), @Run_DateTime )	    + @CrLf ;
       			 
			IF (@Err_severity<>0) 
				SET @Body = @Body + 
						 'Severity = ' + convert(varchar(10),@Err_severity) + @CrLf 
				SET @Body = @Body + 
                 				 'Error    = ' + ISNULL(@ErrMessage,'') + @CrLf +  @CrLf + 
						 'Command  = ' + ISNULL(@Command,'') + @CrLf;  

			-- Add the Message to the Event Log
			IF (RTRIM(@Body)<>'')
				BEGIN 
					SET @Body = 'Server= ' + @@servername + @CrLf  +
 					'Job_name = ' + @Job_name  + @CrLf  +
 					'-------------------------'+ @CrLf  + @Body;
					SET @Body = CAST(@Body AS VARCHAR(1024)) ;
					EXEC RecHubAlert.usp_EventLog_SystemLevel_Ins @parmEventName = 'PROCESSINGEXCEPTION', @parmMessage = @Body;
				END

			DELETE FROM @CurFailedJobs WHERE [Name] = @job_name AND Step_Name = @Step_Name;

			SET @job_name = NULL;
			SELECT TOP 1 
				@job_name		= [Name], 
				@Step_Name		= Step_Name, 
				@Err_severity	= Sql_Severity, 
				@DBname			= Database_Name, 
				@Run_DateTime	= Run_DateTime, 
				@Command		= Command, 
				@ErrMessage		= [Message]
			FROM @CurFailedJobs;   

		END   -- End While Loop

END TRY 

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH