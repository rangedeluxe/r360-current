--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_PivotTableColumnList
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PurgePivotTableColumnList') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_PivotTableColumnList
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_PivotTableColumnList
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 07/22/2013
*
* Purpose: Pivoting the table columns list 
*	
*
* Modification History
* 07/22/2013 WI 117321 PS	Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( OBJECT_ID('dbo.WT_ShortList', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_ShortList;
	IF( OBJECT_ID('dbo.WT_TableColumnPivotList', 'U') IS NOT NULL )
		DROP TABLE  dbo.WT_TableColumnPivotList;

	SELECT 
		Table_Schema,Table_Name,
		Column_Name as VariableValue,
		[Ordinal_Position] as Variable 
	INTO 
		dbo.WT_ShortList
	FROM 
		dbo.WT_TableColumnlist;

	SELECT 
		*
	INTO 
		dbo.WT_TableColumnPivotList
	FROM 
		dbo.WT_ShortList
	PIVOT
	(
		MAX(VariableValue)
		FOR [Variable]
		IN ([1],[2],[3],[4],[5],[6],[7],[8],[9],[10],
		[11],[12],[13],[14],[15],[16],[17],[18],[19],[20],
		[21],[22],[23],[24],[25],[26],[27],[28],[29],[30],
		[31],[32],[33],[34],[35],[36],[37],[38],[39],[40],
		[41],[42],[43],[44],[45],[46],[47],[48],[49],[50])
	)
	AS p;

	DROP TABLE dbo.WT_ShortList;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
