--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_LoadDataHistory
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_LoadDataHistory') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_LoadDataHistory
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_LoadDataHistory
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 09/01/2013
*
* Purpose: Copying BatchSummary for the purged FactBatchSummary record 	
*
* Modification History
* 09/20/2013 WI 114970 PS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO RecHubSystem.PurgeHistory WITH(TABLOCK)
	(
		PurgeHistoryType,
		BankKey,
		ClientAccountKey,
		ProcessingDateKey,
		PurgeDate,
		DepositDateKey,
		BatchID,
		BatchNumber
	)
	SELECT                   
		CAST(1 AS TINYINT) AS PurgeHistoryType,
		RecHubData.factBatchSummaryOld.BankKey, 
		RecHubData.factBatchSummaryOld.ClientAccountKey,
		RecHubData.factBatchSummaryOld.SourceProcessingDateKey,
		CAST(GETDATE() AS DATETIME) AS PurgeDate,
		RecHubData.factBatchSummaryOld.DepositDateKey,
		RecHubData.factBatchSummaryOld.BatchID,
		RecHubData.factBatchSummaryOld.BatchNumber
	FROM 
		RecHubData.factBatchSummaryOld  
		INNER JOIN RecHubData.dimClientAccounts D ON RecHubData.factBatchSummaryOld.ClientAccountKey = D.ClientAccountKey
	WHERE   
		RecHubData.factBatchSummaryOld.DepositDateKey <=
		( 
			YEAR(DATEADD(dd,-D.DataRetentionDays,GETDATE())) * 10000 + 
			MONTH(DATEADD(d,-D.DataRetentionDays,GETDATE()))  * 100 +  
			DAY(DATEADD(d,-D.DataRetentionDays,GETDATE()))
		)
		OR RecHubData.factBatchSummaryOld.IsDeleted = 1;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
