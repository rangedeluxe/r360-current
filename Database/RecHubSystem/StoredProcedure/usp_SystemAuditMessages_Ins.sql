--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_SystemAuditMessages_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_SystemAuditMessages_Ins') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_SystemAuditMessages_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_SystemAuditMessages_Ins 
(
	@parmAuditSource	NVARCHAR(256),
	@parmAuditTypeCode	NVARCHAR(64),
	@parmAuditMessage	NVARCHAR(2048),
	@parmContext		NVARCHAR(128) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/10/2016
*
* Purpose: Insert new an audit for a system event.
*
*
* Modification History
* 06/10/2016 WI 283031 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( @parmContext IS NULL )
		SET @parmContext=SYSTEM_USER;

	INSERT INTO RecHubSystem.SystemAuditMessages
	(
		AuditSource,
		AuditTypeCode,
		Context,
		AuditMessage
	)
	VALUES
	(
		@parmAuditSource,
		@parmAuditTypeCode,
		@parmContext,
		@parmAuditMessage
	);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
