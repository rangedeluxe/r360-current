--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_PartitionDisks_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_PartitionDisks_Del') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_PartitionDisks_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_PartitionDisks_Del
(
	@parmPartitionManagerID INT,
	@parmDiskOrder			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 08/11/2009
*
* Purpose: 
*
* Modification History
* 08/10/2009 CR 27526 JNE	Created
* 04/10/2013 WI 90617 JBS	Update to 2.0 Release. Change schema to RechubSystem
*							Rename Proc FROM usp_DeletePartitionDisks
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY       
	DELETE	RecHubSystem.PartitionDisks
	FROM	RecHubSystem.PartitionDisks 
	WHERE	RecHubSystem.PartitionDisks.PartitionManagerID = @parmPartitionManagerID
			AND RecHubSystem.PartitionDisks.DiskOrder = @parmDiskOrder;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
