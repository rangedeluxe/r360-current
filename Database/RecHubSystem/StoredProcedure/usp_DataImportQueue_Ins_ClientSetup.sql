--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Ins_ClientSetup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_Ins_ClientSetup') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_Ins_ClientSetup
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_Ins_ClientSetup 
(
	@parmClientSetup	XML,
	@parmAuditDateKey	INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Set up Client from XML source
*
* Modification History
* 01/24/2012 CR 49652 JPB	Created
* 08/24/2012 CR 55209 JPB	Added ClientProcessCode.
* 04/10/2013 WI 90613 JBS	Update to 2.0 release. Change Schema to RecHubSystem
*							Rename proc FROM usp_DataImportIntegrationServices_InsertClientSetup
* 04/16/2014 WI 137257 JBS	Updated permissions for DIT service process
* 01/29/2016 WI 260998 JPB	Added @parmAuditDate key
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @AuditDate DATETIME;

BEGIN TRY
	SET @AuditDate = GETDATE();

	INSERT INTO RecHubSystem.DataImportQueue(QueueStatus,AuditDateKey,XSDVersion,ClientProcessCode,SourceTrackingID,QueueType,EntityTrackingID,XMLDataDocument)
	SELECT	
		10,
		COALESCE(@parmAuditDateKey,(DATEPART(YEAR,@AuditDate)*10000)+(DATEPART(MONTH,@AuditDate)*100)+DATEPART(DAY,@AuditDate)),
		Client.att.value('../@XSDVersion','varchar(12)'),
		Client.att.value('../@ClientProcessCode','varchar(40)'),
		Client.att.value('../@SourceTrackingID','varchar(36)'),
		0,
		Client.att.value('@ClientTrackingID','varchar(36)'),
		Client.att.query('.')
	FROM	
		@parmClientSetup.nodes('/ClientGroups/ClientGroup') AS Client(att);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH