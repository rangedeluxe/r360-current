--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Ins_Batch
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportQueue_Ins_Batch') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportQueue_Ins_Batch
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportQueue_Ins_Batch 
(
	@parmAuditDateKey INT,
	@parmDataImportQueueInsertTable RecHubSystem.DataImportQueueInsertTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: 
*
* Modification History
* 01/24/2012 CR 50172 JPB	Created
* 08/24/2012 CR 55208 JPB	Added ClientProcessCode.
* 04/23/2013 WI 90612 JBS	Update to 2.0 release. Change schema to RecHubSystem
*							Rename proc from usp_DataImportIntegrationServices_InsertBatch
* 01/29/2016 WI 260997 JPB	Added @parmAuditDate key
* 11/29/2017 PT 153177633 MGE	Updated to use table variable as parameter instead of XML document
**************************************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY
	
		
	INSERT INTO RecHubSystem.DataImportQueue(QueueStatus,AuditDateKey,XSDVersion,ClientProcessCode,SourceTrackingID,QueueType,EntityTrackingID,XMLDataDocument)
	SELECT	
		10,
		@parmAuditDateKey,
		XSDVersion,
		ClientProcessCode,
		SourceTrackingID,
		1,
		BatchTrackingID,
		XMLDataDocument
	FROM
		@parmDataImportQueueInsertTable;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH