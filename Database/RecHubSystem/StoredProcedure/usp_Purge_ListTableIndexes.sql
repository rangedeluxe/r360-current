--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_ListTableIndexes
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_ListTableIndexes') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_ListTableIndexes
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_ListTableIndexes
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: Creating Staging Fact tables to be used for purging 
*	
*
* Modification History
* 06/01/2013 WI 117319	PS	Created.
******************************************************************************/
SET NOCOUNT ON;
DECLARE @IdxSchema SYSNAME,
		@idxTableName SYSNAME,
		@idxTableID INT,
		@idxname SYSNAME,
		@idxid INT,
		@colCount INT,
		@IxColumn SYSNAME,
		@IxFirstColumn BIT,
		@ColumnIDInTable INT,
		@ColumnIDInIndex INT,
		@IsIncludedColumn INT,
		@sIncludeCols VARCHAR(MAX),
		@sIndexCols VARCHAR(MAX),
		@sSQL VARCHAR(MAX),
		@sSQL1 VARCHAR(MAX), 
		@sSQLDrop VARCHAR(MAX),
		@sParamSQL VARCHAR(MAX),
		@sFilterSQL VARCHAR(MAX),
		@location SYSNAME,
		@IndexCount INT,
		@CurrentIndex INT,
		@CurrentCol INT,
		@Name VARCHAR(128),
		@IsPrimaryKey TINYINT,
		@Fillfactor INT,
		@FilterDefinition VARCHAR(MAX),
		@IsClustered BIT, -- used solely for putting information into the result table
		@TableToScript SYSNAME,
		@SchemaToScript SYSNAME;

BEGIN TRY

	SET @TableToScript=NULL;
	SET @SchemaToScript=NULL;

	/* remove working tables */
	IF OBJECT_ID('[dbo].[WT_IndexSQL]', 'U') IS NOT NULL
		DROP TABLE [dbo].[WT_IndexSQL];
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'[tempdb].[dbo].[#IndexListing]'))
		DROP TABLE [dbo].[#IndexListing];
	IF EXISTS (SELECT * FROM tempdb.dbo.sysobjects WHERE id = OBJECT_ID(N'[tempdb].[dbo].[#ColumnListing]'))
		DROP TABLE [dbo].[#ColumnListing];

	CREATE TABLE WT_IndexSQL
	(
		TableID INT IDENTITY(1,1),
		SchemaName VARCHAR(128) NOT NULL,
		TableName VARCHAR(128) NOT NULL,
		IndexName VARCHAR(128) NOT NULL,
		IsClustered BIT NOT NULL,
		IsPrimaryKey BIT NOT NULL,
		IndexCreateSQL VARCHAR(MAX) NOT NULL,
		IndexDropSQL VARCHAR(MAX) NOT NULL
	);

	CREATE TABLE #IndexListing
	(
		[IndexListingID] INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
		[SchemaName] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[TableName] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ObjectID] INT NOT NULL,
		[IndexName] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[IndexID] INT NOT NULL,
		[IsPrimaryKey] TINYINT NOT NULL,
		[FillFactor] INT,
		[FilterDefinition] NVARCHAR(MAX) NULL
	);

	CREATE TABLE #ColumnListing
	(
		[ColumnListingID] INT IDENTITY(1,1) PRIMARY KEY CLUSTERED,
		[ColumnIDInTable] INT NOT NULL,
		[Name] SYSNAME COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ColumnIDInIndex] INT NOT NULL,
		[IsIncludedColumn] BIT NULL
	);

	IF( ISNULL(@TableToScript,'')='' )
	BEGIN
		INSERT INTO #IndexListing
		(
			[SchemaName],
			[TableName], 
			[ObjectID], 
			[IndexName], 
			[IndexID], 
			[IsPrimaryKey], 
			[FILLFACTOR], 
			[FilterDefinition] 
		)
		SELECT 
			sys.schemas.name, 
			OBJECT_NAME(sys.indexes.object_id), 
			sys.indexes.object_id, 
			sys.indexes.name, 
			sys.indexes.index_id, 
			sys.indexes.Is_Primary_Key, 
			sys.indexes.Fill_Factor, 
			NULL --si.filter_definition
		FROM 
			sys.indexes
			LEFT OUTER JOIN information_schema.table_constraints tc ON sys.indexes.name = tc.constraint_name AND OBJECT_NAME(sys.indexes.object_id) = tc.table_name
			INNER JOIN sys.objects ON sys.objects.object_id=sys.indexes.object_id
			INNER JOIN sys.schemas ON sys.schemas.schema_id=sys.objects.schema_id
		WHERE 
			OBJECTPROPERTY(sys.indexes.object_id, 'IsUserTable') = 1 
		ORDER BY 
			OBJECT_NAME(sys.indexes.object_id), 
			sys.indexes.index_id;
	END
	ELSE
	BEGIN
		INSERT INTO #IndexListing
		( 
			[SchemaName], 
			[TableName], 
			[ObjectID], 
			[IndexName], 
			[IndexID], 
			[IsPrimaryKey], 
			[FILLFACTOR], 
			[FilterDefinition] 
		)
		SELECT 
			sys.schemas.name, 
			OBJECT_NAME(sys.indexes.object_id), 
			sys.indexes.object_id, 
			sys.indexes.name, 
			sys.indexes.index_id, 
			sys.indexes.Is_Primary_Key, 
			sys.indexes.Fill_Factor, 
			NULL --si.filter_definition
		FROM 
			sys.indexes
			LEFT OUTER JOIN information_schema.table_constraints tc ON sys.indexes.name = tc.constraint_name AND OBJECT_NAME(sys.indexes.object_id) = tc.table_name
			INNER JOIN sys.objects ON sys.objects.object_id=sys.indexes.object_id
			INNER JOIN sys.schemas ON sys.objects.schema_id=sys.schemas.schema_id
		WHERE 
			OBJECTPROPERTY(sys.indexes.object_id, 'IsUserTable') = 1 
			AND OBJECT_NAME(sys.indexes.object_id)=@TableToScript
			and sys.schemas.name=@SchemaToScript
		ORDER BY 
			OBJECT_NAME(sys.indexes.object_id), sys.indexes.index_id;
	END

	SELECT
		@IndexCount = @@ROWCOUNT, 
		@CurrentIndex = 1;
	WHILE( @CurrentIndex <= @IndexCount )
	BEGIN /* WHILE( @CurrentIndex <= @IndexCount ) */
		SELECT
			@IdxSchema = [SchemaName],
			@idxTableName = [TableName],
			@idxTableID = [ObjectID],
			@idxname = [IndexName],
			@idxid = [IndexID],
			@IsPrimaryKey = [IsPrimaryKey],
			@FillFactor = [FILLFACTOR],
			@FilterDefinition = [FilterDefinition]
		FROM
			#IndexListing
		WHERE
			[IndexListingID] = @CurrentIndex;

		/* So - it is either an index or a constraint Check if the index is unique */
		IF( @IsPrimaryKey = 1 )
		BEGIN
			SET @sSQL = 'ALTER TABLE ['+@IdxSchema+'].[' + @idxTableName + '1] ADD CONSTRAINT [' + @idxname + '1] PRIMARY KEY ';
			SET @sSQLDrop='ALTER TABLE ['+@IdxSchema+'].[' + @idxTableName + '] DROP CONSTRAINT [' + @idxname + ']';
			/* Check if the index is clustered */
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsClustered') = 0 )
			BEGIN
				SET @sSQL = @sSQL + 'NON';
				SET @IsClustered = 0;
			END
			ELSE
			BEGIN
				SET @IsClustered = 1;
			END
			SET @sSQL = @sSQL + 'CLUSTERED' + CHAR(13) + '(' + CHAR(13);
		END
		ELSE
		BEGIN
			SET @sSQL = 'CREATE ';
			SET @sSQLDrop = 'DROP INDEX [' + @idxname + '] ON ['+@IdxSchema+'].[' + @idxTableName + ']';
			/* Check if the index is unique */
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsUnique') = 1 )
			BEGIN
				SET @sSQL = @sSQL + 'UNIQUE ';
			END
			/* Check if the index is clustered */
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsClustered') = 1 )
			BEGIN
				SET @sSQL = @sSQL + 'CLUSTERED ';
				SET @IsClustered = 1;
			END
			ELSE
			BEGIN
				SET @IsClustered = 0;
			END

			SELECT
				@sSQL = @sSQL + 'INDEX [' + @idxname + '1] ON ['+@IdxSchema+'].[' + @idxTableName + '1]' + CHAR(13) + '(' + CHAR(13),
				@colCount = 0;
		END 

		/* Get the number of cols in the index */
		SELECT
			@colCount = COUNT(*)
		FROM
			sys.index_columns
			INNER JOIN sys.columns ON sys.index_columns.object_id = sys.columns.object_id AND sys.index_columns.column_id = sys.columns.column_id
		WHERE
			sys.index_columns.object_id = @idxtableid 
			AND index_id = @idxid AND sys.index_columns.is_included_column = 0;

		/* Get the file group info */
		SELECT
			@location = sys.filegroups.[name]
		FROM
			sys.indexes
			INNER JOIN sys.filegroups ON sys.indexes.data_space_id = sys.filegroups.data_space_id
			INNER JOIN sys.all_objects ON sys.indexes.[object_id] = sys.all_objects.[object_id]
		WHERE
			sys.all_objects.object_id = @idxTableID 
			AND sys.indexes.index_id = @idxid;

		/* Get all columns of the index */
		INSERT INTO #ColumnListing( [ColumnIDInTable], [Name], [ColumnIDInIndex],[IsIncludedColumn] )
		SELECT
			sys.columns.column_id, sys.columns.name, sys.index_columns.index_column_id, sys.index_columns.is_included_column
		FROM
			sys.index_columns
			INNER JOIN sys.columns ON sys.index_columns.object_id = sys.columns.object_id AND sys.index_columns.column_id = sys.columns.column_id
		WHERE
			sys.index_columns.object_id = @idxTableID 
			AND index_id = @idxid
		ORDER BY 
			sys.index_columns.index_column_id;
	
		IF(	@@ROWCOUNT > 0 )
		BEGIN
			SELECT
				@IxFirstColumn = 1, 
				@sIncludeCols = '', 
				@sIndexCols = '',
				@CurrentCol = 1;

			WHILE( @CurrentCol <= @ColCount )
			BEGIN /* WHILE( @CurrentCol <= @ColCount ) */
				SELECT
					@ColumnIDInTable = ColumnIDInTable,
					@Name = Name,
					@ColumnIDInIndex = ColumnIDInIndex,
					@IsIncludedColumn = IsIncludedColumn
				FROM
					#ColumnListing
				WHERE
					[ColumnListingID] = @CurrentCol;

				IF(	@IsIncludedColumn = 0 )
				BEGIN
					SET	@sIndexCols = CHAR(9) + @sIndexCols + '[' + @Name + '] ';
					/* Check the sort order of the index cols ???????? */
					IF( INDEXKEY_PROPERTY (@idxTableID,@idxid,@ColumnIDInIndex,'IsDescending')) = 0 
					BEGIN
						SET	@sIndexCols = @sIndexCols + ' ASC ';
					END
					ELSE
					BEGIN
						SET	@sIndexCols = @sIndexCols + ' DESC ';
					END
					IF(	@CurrentCol < @colCount )
					BEGIN
						SET	@sIndexCols = @sIndexCols + ', ';
					END
				END
				ELSE
				BEGIN
					/* Check for any include columns */
					IF( LEN(@sIncludeCols) > 0 )
						SET @sIncludeCols = @sIncludeCols + ',';
					SET	@sIncludeCols = @sIncludeCols + '[' + @IxColumn + ']';
				END
				SET	@CurrentCol = @CurrentCol + 1;
			END /* WHILE( @CurrentCol <= @ColCount ) */

			TRUNCATE TABLE #ColumnListing;
			/* append to the result */
			IF(	LEN(@sIncludeCols) > 0 )
				SET @sIndexCols = @sSQL + @sIndexCols + CHAR(13) + ') ' + ' INCLUDE ( ' + @sIncludeCols + ' ) ';
			ELSE
				SET @sIndexCols = @sSQL + @sIndexCols + CHAR(13) + ') ';
			/* Add filtering */
			IF(	@FilterDefinition IS NOT NULL )
				SET @sFilterSQL = ' WHERE ' + @FilterDefinition + ' ' + CHAR(13);
			ELSE
				SET @sFilterSQL = '';
			/* Build the options */
			SET @sParamSQL = 'WITH ( PAD_INDEX = ';
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsPadIndex') = 1 )
				SET @sParamSQL = @sParamSQL + 'ON,';
			ELSE
				SET @sParamSQL = @sParamSQL + 'OFF,';
			SET @sParamSQL = @sParamSQL + ' ALLOW_PAGE_LOCKS = ';
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsPageLockDisallowed') = 0 )
				SET @sParamSQL = @sParamSQL + 'ON,';
			ELSE
				SET @sParamSQL = @sParamSQL + 'OFF,';
			SET @sParamSQL = @sParamSQL + ' ALLOW_ROW_LOCKS = ';
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsRowLockDisallowed') = 0 )
				SET @sParamSQL = @sParamSQL + 'ON,';
			ELSE
				SET @sParamSQL = @sParamSQL + 'OFF,';
			SET @sParamSQL = @sParamSQL + ' STATISTICS_NORECOMPUTE = ';
			/* THIS DOES NOT WORK PROPERLY; IsStatistics only says what generated the last set, not what it was set to do. */
			IF( INDEXPROPERTY(@idxTableID, @idxname, 'IsStatistics') = 1 )
				SET @sParamSQL = @sParamSQL + 'ON';
			ELSE
				SET	@sParamSQL = @sParamSQL + 'OFF';
			/* Fillfactor 0 is actually not a valid percentage on SQL 2008 R2 */
			IF( ISNULL( @FillFactor, 90 ) <> 0 )
				SET @sParamSQL = @sParamSQL + ' ,FILLFACTOR = ' + CAST( ISNULL( @FillFactor, 90 ) AS VARCHAR(3) );
			IF( @IsPrimaryKey = 1 ) /* DROP_EXISTING isn't valid for PK's */
				SET @sParamSQL = @sParamSQL + ' ) ';
			ELSE
				SET @sParamSQL = @sParamSQL + ' ,DROP_EXISTING = OFF, STATISTICS_NORECOMPUTE = OFF,SORT_IN_TEMPDB = OFF,ONLINE = OFF ) ';
			SET @sSQL = @sIndexCols + CHAR(13) + @sFilterSQL + CHAR(13) + @sParamSQL;
			/* 2008 R2 allows ON [filegroup] for primary keys as well, negating the old "IF THE INDEX IS NOT A PRIMARY KEY - ADD THIS - ELSE DO NOT" IsPrimaryKey IF statement */
			SET @sSQL = @sSQL + ' ON [' + @location + ']';
			INSERT INTO dbo.WT_IndexSQL (SchemaName, TableName, IndexName, IsClustered, IsPrimaryKey, IndexCreateSQL, IndexDropSQL) 
			VALUES (@IdxSchema, @idxTableName, @idxName, @IsClustered, @IsPrimaryKey, @sSQL, @sSQLDrop);
		END
		SET @CurrentIndex = @CurrentIndex + 1;
	END /* WHILE( @CurrentIndex <= @IndexCount ) */
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
