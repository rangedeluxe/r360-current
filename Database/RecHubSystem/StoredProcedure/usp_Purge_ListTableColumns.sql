--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_ListTableColumns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_ListTableColumns') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_ListTableColumns
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_ListTableColumns
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permiWTed in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: listing all the constraints in facts tables 
*	
*
* Modification History
* 06/01/2013 WI 117318 PS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( OBJECT_ID('dbo.WT_TableColumnlist', 'U') IS NOT NULL )
		DROP TABLE dbo.WT_TableColumnlist; 

	CREATE TABLE dbo.WT_TableColumnlist 
	(
		Table_Schema  NVARCHAR(128),
		Table_Name NVARCHAR(128),
		Column_Name NVARCHAR(128),
		Data_Type NVARCHAR(128),
		DataCode INT,
		IsNullable INT,
		Column_Default NVARCHAR(128),
		Ordinal_Position INT,
		Collation_Name NVARCHAR(128),
		Character_Maximum_Lenght INT,
		Numeric_Precision TINYINT,
		Numeric_Scale INT,
		Datetime_Precision SMALLINT 
	);

	INSERT INTO dbo.WT_TableColumnlist 
	(
		Table_Schema ,
		Table_Name ,
		Column_Name ,
		Data_Type ,
		DataCode ,
		IsNullable,
		Column_Default,
		Ordinal_Position,
		Collation_Name ,
		Character_Maximum_Lenght,
		Numeric_Precision,
		Numeric_Scale,
		Datetime_Precision
	)
	SELECT  
		TABLE_SCHEMA,
		TABLE_NAME,
		COLUMN_NAME, 
		DATA_TYPE, 
		CASE 
			WHEN DATA_TYPE='VARCHAR'  OR DATA_TYPE='VARBINARY'   THEN 1 
			ELSE 0
		END AS DATA_CODE, 
		CASE 
			WHEN  IS_NULLABLE='YES' THEN 1
			ELSE 0
		END AS ISNULLABLE, 
		COLUMN_DEFAULT, 
		ORDINAL_POSITION, 
		COLLATION_NAME,
		[CHARACTER_MAXIMUM_LENGTH],
		[NUMERIC_PRECISION],
		[NUMERIC_SCALE],
		[DATETIME_PRECISION]
	FROM 
		INFORMATION_SCHEMA.COLUMNS 
	WHERE  
		RIGHT([Table_Schema],7)<>'Staging' 
		AND 
		(
			(LEFT(Table_Name,3)='fac') OR (LEFT(Table_Name, 3) = 'dat') OR (LEFT(Table_Name, 3) = 'cds') 
		)
	ORDER BY 
		TABLE_SCHEMA, 
		Table_Name,
		ORDINAL_POSITION;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
