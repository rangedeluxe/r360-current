--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_SSISWorkingValues_UpSert
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_SSISWorkingValues_UpSert') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_SSISWorkingValues_UpSert
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_SSISWorkingValues_UpSert
(
	@parmPackageName			VARCHAR(128),
	@parmVariableName			VARCHAR(128),
	@parmVariableDateTime		DATETIME = NULL,
	@parmVariableInt			INT = NULL,
	@parmVariableString			VARCHAR(256) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/07/2013
*
* Purpose: Insert or update row in RecHubSystem.SSISWorkingValues.
*
* Modification History
* 06/07/2013 WI 104757 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	IF NOT EXISTS
		(
				SELECT 1 
				FROM 
					RecHubSystem.SSISWorkingValues
				WHERE 
					PackageName = @parmPackageName
					AND VariableName = @parmVariableName 
		)
		INSERT INTO RecHubSystem.SSISWorkingValues
		(
			PackageName,
			VariableName,
			VariableDateTime,
			VariableInt,
			VariableString
		)
		VALUES
		(
			@parmPackageName,
			@parmVariableName,
			@parmVariableDateTime,
			@parmVariableInt,
			@parmVariableString
		)
	ELSE
		UPDATE 
			RecHubSystem.SSISWorkingValues
		SET
			VariableDateTime = COALESCE(@parmVariableDateTime,VariableDateTime),
			VariableInt = COALESCE(@parmVariableInt,VariableInt),
			VariableString = COALESCE(@parmVariableString,VariableString)
		FROM 
			RecHubSystem.SSISWorkingValues
		WHERE 
			PackageName = @parmPackageName
			AND VariableName = @parmVariableName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH