--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingOLClientAccounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_CDSQueue_Get_PendingOLClientAccounts') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingOLClientAccounts;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_CDSQueue_Get_PendingOLClientAccounts
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/16/2013
*
* Purpose: Retreive list of OLClientAccounts that are ready for processing.
*
* ActionCode:
*	1 - Insert/Update
*	2 - Delete

* QueueStatus:
*	10 - Ready to sent to integraPAY Consolidated Database
*	15 - Failed processing on integraPAY Consolidated Database - but can resend
*	20 - Sent to integraPAY Consolidated Database, waiting for a response
*	30 - Failed processing on integraPAY Consolidated Database - but cannot resend
*	99 - Successfully processed on integraPAY Consolidated Database
*
*
* QueueType:
*	10 - OLOrganizationID
*	20 - OLClientAccountID
*	70 - Document Type
*
* Modification History
* 06/16/2013 WI 106122 JPB	Created
* 12/11/2013 WI 125184 JPB	Added OrganizationCode/SiteBankID/SiteClientAccountID.
* 01/13/2014 WI 126812 JPB	Added OLClientAccountID to result set.
* 01/21/2014 WI 127571 JPB	Update JOIN to OLOrganizations.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH PendingList AS
	(
		SELECT TOP(150000) 
			MIN(CDSQueueID) AS RecordID,
			OLOrganizationID,
			OLClientAccountID,
			OrganizationCode,
			SiteBankID,
			SiteClientAccountID
		FROM 	
			RecHubSystem.CDSQueue
		WHERE 	
			QueueType = 20
			AND QueueStatus <= 20
		GROUP BY 
			OLOrganizationID,
			OLClientAccountID,
			OrganizationCode,
			SiteBankID,
			SiteClientAccountID
		ORDER BY RecordID
	)
	SELECT	
		PendingList.RecordID AS OLClientAccount_Id,
		PendingList.OLClientAccountID,
		COALESCE(RecHubUser.OLOrganizations.OrganizationCode,PendingList.OrganizationCode) AS CustomerCode,
		COALESCE(RecHubUser.OLClientAccounts.SiteBankID,PendingList.SiteBankID) AS BankID,
		COALESCE(RecHubUser.OLClientAccounts.SiteClientAccountID,PendingList.SiteClientAccountID) AS LockboxID,
		RecHubUser.OLClientAccounts.OLOrganizationID AS OLCustomerID,
		RecHubUser.OLClientAccounts.DisplayName,
		RecHubUser.OLClientAccounts.IsActive,
		RecHubUser.OLClientAccounts.ViewingDays,
		RecHubUser.OLClientAccounts.CreationDate,
		RecHubUser.OLClientAccounts.CreatedBy,
		RecHubUser.OLClientAccounts.ModificationDate,
		RecHubUser.OLClientAccounts.ModifiedBy,
		RecHubUser.OLClientAccounts.MaximumSearchDays,
		RecHubUser.OLClientAccounts.DocumentImageDisplayMode,
		RecHubUser.OLClientAccounts.CheckImageDisplayMode,
		RecHubSystem.CDSQueue.ActionCode
	FROM	
		RecHubSystem.CDSQueue 
		INNER JOIN PendingList ON RecHubSystem.CDSQueue.CDSQueueID = PendingList.RecordID
		LEFT JOIN RecHubUser.OLClientAccounts ON RecHubUser.OLClientAccounts.OLOrganizationID = PendingList.OLOrganizationID
			AND RecHubUser.OLClientAccounts.OLClientAccountID = PendingList.OLClientAccountID
			AND PendingList.SiteBankID = COALESCE(RecHubUser.OLClientAccounts.SiteBankID,PendingList.SiteBankID)
			AND PendingList.SiteClientAccountID =  COALESCE(RecHubUser.OLClientAccounts.SiteClientAccountID,PendingList.SiteClientAccountID)
		LEFT JOIN RecHubUser.OLOrganizations ON PendingList.OLOrganizationID = RecHubUser.OLOrganizations.OLOrganizationID
	WHERE 	
		(RecHubSystem.CDSQueue.QueueStatus = 10) 
		OR (RecHubSystem.CDSQueue.QueueStatus = 20) 
		OR (RecHubSystem.CDSQueue.QueueStatus = 15 AND RecHubSystem.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RecHubSystem.CDSQueue.RetryCount),GETDATE()));
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
