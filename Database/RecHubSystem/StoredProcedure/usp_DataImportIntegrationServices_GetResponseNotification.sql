--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_GetResponseNotification
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportIntegrationServices_GetResponseNotification') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_GetResponseNotification
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_GetResponseNotification
(
	@parmClientProcessCode VARCHAR(40) = 'Unassigned'
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/18/2012
*
* Purpose: Used in File Import Toolkit SSIS package
*
* Modification History
* 06/18/2012 CR 53530  JPB	Created
* 09/13/2013 WI 113982 JBS	Update for 2.0 Release.  Changes Schema
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @Loop INT,
		@SourceTrackingID UNIQUEIDENTIFIER,
		@ResponseTrackingID UNIQUEIDENTIFIER,
		@Msg VARCHAR(MAX);

BEGIN TRY
	DECLARE @Responses TABLE
	(
		RowID INT NOT NULL IDENTITY(1,1),
		SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
		ResponseTrackingID UNIQUEIDENTIFIER NULL,
		XMLResponseDocument XML NULL
	)

	SELECT @ResponseTrackingID = NEWID();

	INSERT INTO @Responses(SourceTrackingID)
	SELECT	DISTINCT SourceTrackingID
	FROM	RecHubSystem.DataImportQueue
	WHERE	QueueType = 2 
			AND QueueStatus IN (30,99, 120)
			AND ClientProcessCode = @parmClientProcessCode;

	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Responses) )
	BEGIN
		SET @Msg = '';

		SELECT	@SourceTrackingID = SourceTrackingID
		FROM	@Responses
		WHERE	RowID = @Loop;
		
		SELECT	@Msg = @Msg + CAST(XMLResponseDocument AS VARCHAR(MAX)),
				@ResponseTrackingID = COALESCE(ResponseTrackingID,@ResponseTrackingID)
		FROM	RecHubSystem.DataImportQueue 
		WHERE	SourceTrackingID = @SourceTrackingID
				AND QueueStatus IN (30,99,120);
		
		UPDATE	@Responses 
		SET		XMLResponseDocument = CAST(@Msg AS XML),
				ResponseTrackingID = @ResponseTrackingID
		WHERE	SourceTrackingID = @SourceTrackingID;
		
		SET @Loop = @Loop + 1;
	END

	UPDATE	RecHubSystem.DataImportQueue
	SET		ResponseTrackingID = @ResponseTrackingID,
			QueueStatus = 120,
			ModificationDate = GETDATE(),
			ModifiedBy = SUSER_NAME()
	FROM	RecHubSystem.DataImportQueue
			INNER JOIN @Responses R ON RecHubSystem.DataImportQueue.SourceTrackingID = R.SourceTrackingID
	WHERE	QueueStatus IN (30,99,120);

	SET @Loop = 1;
	SET @Msg = '';
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @Responses) )
	BEGIN

		SELECT @Msg = @Msg + CAST(
		(
			SELECT	SourceTrackingID AS '@SourceTrackingID',
					ResponseTrackingID AS '@ResponseTrackingID',
					@parmClientProcessCode AS '@ClientProcessCode',
					XMLResponseDocument.query('.')
			FROM	@Responses
			WHERE RowID = @Loop
			FOR XML	PATH('Notifications'), TYPE
		) AS VARCHAR(MAX));
				
		SET @Loop = @Loop + 1;
	END

	SELECT
	(
		SELECT	CAST(@Msg AS XML)
		FOR XML	PATH('NotificationResponses'), TYPE
	) AS XMLResponseDocument;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH