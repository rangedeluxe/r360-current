--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportIntegrationServices_InsertNotification
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_DataImportIntegrationServices_InsertNotification') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_InsertNotification
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_DataImportIntegrationServices_InsertNotification
(
	@parmNotification	XML,
	@parmAuditDateKey	INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: Insert a notification XML document into the DataImportQueue table.
*
* Modification History
* 05/30/2012 CR 53203 JPB	Created
* 10/08/2013 WI 116280 JPB	Updated for 2.0. 
*							Moved to RecHubSystem schema.
* 01/29/2016 WI 260999 JPB	Added @parmAuditDate key
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @AuditDate DATETIME;

BEGIN TRY
	SET @AuditDate = GETDATE();

	INSERT INTO RecHubSystem.DataImportQueue(QueueStatus,AuditDateKey,XSDVersion,ClientProcessCode,SourceTrackingID,QueueType,EntityTrackingID,XMLDataDocument)
	SELECT	
		10,
		COALESCE(@parmAuditDateKey,(DATEPART(YEAR,@AuditDate)*10000)+(DATEPART(MONTH,@AuditDate)*100)+DATEPART(DAY,@AuditDate)),
		NotificationImport.att.value('../@XSDVersion','varchar(12)'),
		NotificationImport.att.value('../@ClientProcessCode','varchar(40)'),
		NotificationImport.att.value('../@SourceTrackingID','varchar(36)'),
		2,
		NotificationImport.att.value('@NotificationTrackingID','varchar(36)'),
		NotificationImport.att.query('.') AS XMLDataDocument
	FROM	
		@parmNotification.nodes('/Notifications/Notification') AS NotificationImport(att);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH