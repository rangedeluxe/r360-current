--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorStoredProcedureName usp_ResetPartitionScheme
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_ResetPartitionScheme') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_ResetPartitionScheme
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_ResetPartitionScheme
	@parmPartitionScheme SYSNAME,
	@parmPartitionFunction SYSNAME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 3/22/2017
*
* Purpose: Drop and Rebuild the partition scheme 
*      to remain 52 partitions into the future.
*
* Modification History
* 03/09/2017 PT	138837605	MGE	Created
* 03/22/2017 PT 138837605	MGE Updated to make it more robust by dynamically getting partition numbers and boundary values.
* 04/03/2017 PT 142595657	MGE Updated IF statement to use parameter instead of hard code.
****************************************************************************************************************************/
DECLARE	@BoundaryValues	TABLE	(RowID INT IDENTITY(1,1), BoundaryValue INT, boundary_id INT);
DECLARE	@FanoutLessOne INT,
		@Loop INT, 
		@BoundaryValue INT,
		@Message VARCHAR(max),
		@SQLcmd VARCHAR(max)

--SET @ReturnValue = 0
SELECT @Message = 'Resetting partition scheme for ' + CAST(@parmPartitionScheme AS VARCHAR(30));
RAISERROR(@Message,10,1) WITH NOWAIT;
BEGIN TRY
	IF EXISTS(SELECT 1 FROM sys.partition_schemes WHERE name = @parmPartitionScheme)
	BEGIN    
		SELECT @Message = 'Dropping partition scheme for' + CAST(@parmPartitionScheme AS VARCHAR(30));
		RAISERROR(@Message,10,1) WITH NOWAIT;

		SELECT @SQLcmd = 'DROP PARTITION SCHEME ['  + CAST(@parmPartitionScheme AS VARCHAR(30)) +']';
		SELECT @Message = @SQLcmd;
		RAISERROR(@Message,10,1) WITH NOWAIT;
		EXEC (@SQLcmd);
	END;
	SELECT @FanoutLessOne = fanout -1  FROM sys.partition_functions WHERE name = @parmPartitionFunction;  --This is number of filegroups to assign to partition scheme
	
	INSERT INTO @BoundaryValues 
	SELECT CAST(value AS INT), boundary_id FROM sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
		WHERE sys.partition_functions.name = @parmPartitionFunction
	
	SELECT @Message = 'Re-creating partition scheme with ' + CAST(@FanoutLessOne AS VARCHAR(8)) + ' filegroups.'
	RAISERROR(@Message,10,1) WITH NOWAIT;
	--<<< Add the partition scheme back >>>
    
	SELECT @SQLcmd = 'CREATE PARTITION SCHEME [' + @parmPartitionScheme + '] AS PARTITION [' + @parmPartitionFunction + '] TO (' + @parmPartitionScheme + 'CatchAll' 
	SET @Loop = 1;
	WHILE @Loop <= @FanoutLessOne
		BEGIN
			SELECT @BoundaryValue = BoundaryValue FROM @BoundaryValues WHERE RowID = @Loop;
		
			SELECT @SQLcmd = @SQLcmd + ',' + Cast(@parmPartitionScheme AS VARCHAR(30)) + CAST(@BoundaryValue as VARCHAR(8));
			SET @Loop = @Loop + 1;
		END
	SELECT @SQLcmd = @SQLcmd + ')';
	SELECT @Message = @SQLcmd;
	RAISERROR(@Message,10,1) WITH NOWAIT;
	EXEC (@SQLcmd);
END TRY 
BEGIN CATCH
	RAISERROR('Stored Procedure Failed',10,1) WITH NOWAIT;
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
