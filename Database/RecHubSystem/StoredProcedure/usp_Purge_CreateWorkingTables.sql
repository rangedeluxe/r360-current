--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_Purge_CreateWorkingTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_Purge_CreateWorkingTables') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_Purge_CreateWorkingTables
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_Purge_CreateWorkingTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permiWTed in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 06/01/2013
*
* Purpose: Create working tables for purging 
*	
*
* Modification History
* 06/01/2013 WI 117315 PS	Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF( OBJECT_ID('[dbo].[WT_RenameIndexes]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_RenameIndexes];
	IF( OBJECT_ID('[dbo].[WT_RenameFKConstraints]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_RenameFKConstraints];
	IF( OBJECT_ID('[dbo].[WT_RenameTable]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_RenameTable];
	IF( OBJECT_ID('[dbo].[WT_RenameDFConstr]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_RenameDFConstr];
	IF( OBJECT_ID('[dbo].[WT_PurgingTableStructure]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_PurgingTableStructure];
	IF( OBJECT_ID('[dbo].[WT_ExtendedPropTable]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_ExtendedPropTable];
	IF( OBJECT_ID('[dbo].[WT_CKConstraintTable]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_CKConstraintTable];
	IF( OBJECT_ID('[dbo].[WT_PurgeLoadData]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_PurgeLoadData];
	IF( OBJECT_ID('[dbo].[WT_PurgeBulkLoad0]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_PurgeBulkLoad0];
	IF( OBJECT_ID('[dbo].[WT_PurgeBulkLoad1]', 'U') IS NOT NULL )
		DROP TABLE [dbo].[WT_PurgeBulkLoad1];
	IF OBJECT_ID('[dbo].[WT_PurgeBulkLoad2]', 'U') IS NOT NULL
		DROP TABLE [dbo].[WT_PurgeBulkLoad2];
	IF OBJECT_ID('[dbo].[WT_PurgeBulkLoad3]', 'U') IS NOT NULL
		DROP TABLE [dbo].[WT_PurgeBulkLoad3];

	CREATE TABLE [dbo].[WT_PurgingTableStructure]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[TableSchema] [nvarchar](128) NULL,
		[TableName] [nvarchar](128) NULL,
		[ColumnName] [nvarchar](128) NULL,
		[DataType] [nvarchar](128) NULL,
		[Default] [nvarchar](128) NULL,
		[OrdinalPosition] [int] NULL,
		[CollationName] [nvarchar](128) NULL,
		[DataTypeSize] [nvarchar](128) NULL,
		[CharacterMaximumLenght] [int] NULL,
		[IsNullable] [nvarchar](30) NULL,
		[CmdCreateTable] [nvarchar](800) NULL
	) ON [PRIMARY];
  
	CREATE TABLE [dbo].[WT_ExtendedPropTable]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[TableSchema] [varchar](128) NULL,
		[TableName] [varchar](128) NULL,
		[CmdAddExtendedProperties] [nvarchar](3800) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_CKConstraintTable]
	(
		[TableID] [int] IDENTITY(1,1) NOT NULL,
		[TableSchema] [nvarchar](256) NULL,
		[ConstrName] [nvarchar](256) NULL,
		[TableName] [nvarchar](256) NULL,
		[TableNameOld] [nvarchar](256) NULL,
		[ConstrNameOld] [nvarchar](256) NULL,
		[TableNameStg] [nvarchar](256) NULL,
		[ConstrNameStg] [nvarchar](256) NULL,
		[ColumnName] [varchar](256) NULL,
		[Definit] [nvarchar](600) NULL,
		RnmStgToCurrent  varchar(600),
		RnmCurrentToOld  varchar(600),
		RnmOldToCurrent  varchar(600),
		RnmCurrentToStg  varchar(600),
		CmdAddCKConstraintToStg  nvarchar(2400)
	) ON [PRIMARY];
  
	CREATE TABLE [dbo].[WT_RenameDFConstr]
	(
		TableID INT IDENTITY(1,1),
		[TableSchema] [varchar](128) NULL,
		[TableName] [varchar](256) NULL,
		[ConstrName] [nvarchar](256) NULL,
		[TableOld] [nvarchar](256) NULL,
		[TableStag] [nvarchar](256) NULL,
		[SchemaDFConstraint] nvarchar(256) null,
		[SchemaDFConstraintStg] [nvarchar](256) NULL,
		[SchemaDFConstraintOld] [nvarchar](256) NULL,
		[RnmCurrentToOld] [nvarchar](600) NULL,
		[RnmCurrentToStg] [nvarchar](600) NULL,
		[RnmStgToCurrent] [nvarchar](600) NULL,
		[RnmOldToCurrent] [nvarchar](600) NULL,
		CmdAddDFConstraintToStg  nvarchar(2400)
	) ON [PRIMARY];

  
	CREATE TABLE [dbo].[WT_RenameFKConstraints]
	(
		[TableID] [int] NULL,
		[TableName] [varchar](256) NULL,
		[ColumnName] [varchar](256) NULL,
		[TableSchema] [varchar](256) NULL,
		[TableOld] [nvarchar](256) NULL,
		[TableStag] [nvarchar](256) NULL,
		[ConstraintName] [varchar](256) NULL,
		[ConstraintNameStg] [nvarchar](256) NULL,
		[ConstraintNameOld] [nvarchar](256) NULL,
		[SchemaConstraint] [nvarchar](256) NULL,
		[SchemaConstraintStg] [nvarchar](256) NULL,
		[SchemaConstraintOld] [nvarchar](256) NULL,
		[RnmCurrentToOld] [nvarchar](600) NULL,
		[RnmCurrentToStg] [nvarchar](600) NULL,
		[RnmStgToCurrent] [nvarchar](600) NULL,
		[RnmOldToCurrent] [nvarchar](600) NULL,
		CmdAddFKConstraintToStg  nvarchar(600)
	) ON [PRIMARY];

  
	CREATE TABLE [dbo].[WT_RenameIndexes]
	(
		[TableID] [int] NULL,
		[TableName] [nvarchar](256) NULL,
		[TableStag] [nvarchar](256) NULL,
		[TableOld] [nvarchar](256) NULL,
		[SchemaName] [varchar](256) NULL,
		[IndexName] [varchar](256) NULL,
		[IndexStg] [nvarchar](256) NULL,
		[IndexOld] [nvarchar](256) NULL,
		[SchemaIndex] [nvarchar](256) NULL,
		[SchemaIndexStg] [nvarchar](256) NULL,
		[SchemaIndexOld] [nvarchar](256) NULL,
		[IsClustered] [bit] NULL,
		[IsPrimaryKey] [bit] NULL,
		[RnmCurrentToOld] [nvarchar](600) NULL,
		[RnmCurrentToStg] [nvarchar](600) NULL,
		[RnmStgToCurrent] [nvarchar](600) NULL,
		[RnmOldToCurrent] [nvarchar](600) NULL,
		CmdAddIndexToStg  nvarchar(600)
	) ON [PRIMARY];

 	CREATE TABLE [dbo].[WT_RenameTable]
	(
		[TableID] INT IDENTITY(1,1),
		[TableName] [nvarchar](256) NULL,
		[TableStag] [nvarchar](256) NULL,
		[TableOld] [nvarchar](256) NULL,
		[SchemaName] [varchar](256) NULL,
		[RnmCurrentToOld] [nvarchar](600) NULL,
		[RnmCurrentToStg] [nvarchar](600) NULL,
		[RnmStgToCurrent] [nvarchar](600) NULL,
		[RnmOldToCurrent] [nvarchar](600) NULL,
		[CmdDropOldTables] [nvarchar](600) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_PurgeLoadData]
	(
		TableID INT IDENTITY(1,1) not null,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[CmdSelectF1] [nvarchar](2520) NULL,
		[CmdInsertInto] [nvarchar](2577) NULL,
		[CmdSelectF1Notif] [nvarchar](4000) NULL,
		[CmdSelectF1Queu] [nvarchar](4000) NULL,
		[CmdSelectF1Others] [nvarchar](4000) NULL,
		[CMD2] [nvarchar](4000) NULL
	) ON [PRIMARY];
                 
	CREATE TABLE [dbo].[WT_PurgeBulkLoad0]
	(
		TableID INT IDENTITY(1,1) not null,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[CmdInsertInto] [nvarchar](2577) NULL,
		[CMD2] [nvarchar](4000) NULL
	) ON [PRIMARY];
 
	CREATE TABLE [dbo].[WT_PurgeBulkLoad1]
	(
		TableID INT IDENTITY(1,1) not null,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[CmdInsertInto] [nvarchar](2577) NULL,
		[CMD2] [nvarchar](4000) NULL
	) ON [PRIMARY];
 
	CREATE TABLE [dbo].[WT_PurgeBulkLoad2]
	(
		TableID INT IDENTITY(1,1) not null,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[CmdInsertInto] [nvarchar](2577) NULL,
		[CMD2] [nvarchar](4000) NULL
	) ON [PRIMARY];

	CREATE TABLE [dbo].[WT_PurgeBulkLoad3]
	(
		TableID INT IDENTITY(1,1) not null,
		[Table_Schema] [nvarchar](128) NULL,
		[Table_Name] [nvarchar](128) NULL,
		[CmdInsertInto] [nvarchar](2577) NULL,
		[CMD2] [nvarchar](4000) NULL
	) ON [PRIMARY];
 
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

