--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_ClientSetup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubSystem.usp_dimClientAccounts_Get_ClientSetup') IS NOT NULL
       DROP PROCEDURE RecHubSystem.usp_dimClientAccounts_Get_ClientSetup
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubSystem.usp_dimClientAccounts_Get_ClientSetup 
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2103 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/06/2012
*
* Purpose: Get Client Setup from a data import queue
*
* Modification History
* 03/06/2012 CR 50906 WJS	Created
* 04/10/2013 WI 90614 JBS	Update to 2.0 release.  Change schema name 
*							Rename proc FROM usp_DataImportIntegrationServices_RequestClientSetup
*							Change All references from Lockbox to ClientAccount
*							Change parameter @parmSiteLockboxID to @parmSiteClientAccountID
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT  
		RecHubData.dimClientAccounts.ShortName, 
        RecHubData.dimClientAccounts.LongName, 
        RecHubData.dimClientAccounts.POBox,
        RecHubData.dimClientAccounts.DDA,
        RecHubData.dimClientAccounts.OnlineColorMode,
        RecHubData.dimBanks.BankName, 
        RecHubData.dimBanks.ABA,
        RecHubData.dimDataEntryColumns.TableName, 
        RecHubData.dimDataEntryColumns.FldName, 
        RecHubData.dimDataEntryColumns.DisplayName, 
        RecHubData.dimDataEntryColumns.FldLength, 
        RecHubData.dimDataEntryColumns.DataType, 
        RecHubData.dimDataEntryColumns.ScreenOrder
	FROM
		RecHubData.dimClientAccounts  
			LEFT JOIN RecHubData.ClientAccountsDataEntryColumns 
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey 
			LEFT JOIN RecHubData.dimDataEntryColumns 
				ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey 
			INNER JOIN  RecHubData.dimBanks 
				ON RecHubData.dimBanks.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
	WHERE  
		  RecHubData.dimBanks.MostRecent=1
		  AND RecHubData.dimClientAccounts.MostRecent=1
		  AND RecHubData.dimBanks.SiteBankID=@parmSiteBankID
		  AND RecHubData.dimClientAccounts.SiteClientAccountID=@parmSiteClientAccountID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
