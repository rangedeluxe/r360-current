--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 04/04/2017
*
* Purpose: To add PartitionManager and PartitionDisk to an existing Database for the SessionActivityLog table partitions
*				
* Caution: You must either edit this script to fill in the correct path for the PartitionDisk or edit the table after running this script
* Caution: Be sure that the folder specified is created on the disk
* Caution: Backup the database before beginning this process
* Caution: Be sure to connect to the database you are creating partitions for prior to executing this script
*
* Modification History
* 04/04/2017 PT 141435715	MGE		Created
******************************************************************************/

IF NOT EXISTS (SELECT 1 FROM RecHubSystem.PartitionManager WHERE PartitionIdentifier = 'SessionActivity')
BEGIN
	RAISERROR ('Adding PartitionManager SessionActivity', 10, 1) WITH NOWAIT
	INSERT INTO RecHubSystem.PartitionManager
	(
	PartitionManagerID,
	PartitionIdentifier,
	PartitionFunctionName,
	PartitionSchemeName,
	SizeMB,
	RangeSetting,
	LastDiskID,
	IsLocked
	)
	VALUES 
	(6,'SessionActivity','SessionActivity_PartitionFunction','SessionActivity',1,1,1,0)
	RAISERROR ('PartitionManager Added for SessionActivity', 10, 1) WITH NOWAIT
END
ELSE
	BEGIN
	RAISERROR ('SessionActivity PartitionManager already exists.  No rows added.', 10, 1) WITH NOWAIT
	END


IF NOT EXISTS (SELECT 1 FROM RecHubSystem.PartitionDisks 
		WHERE PartitionManagerID = (Select PartitionManagerID FROM RecHubSystem.PartitionDisks WHERE PartitionManagerID = 6))
BEGIN
	RAISERROR ('Adding PartitionDisks for SessionActivity', 10, 1) WITH NOWAIT;
	INSERT INTO RecHubSystem.PartitionDisks
	(PartitionManagerID,
	DiskOrder,
	DiskPath
	)
	VALUES
	(6,
	1,
	'Path to SessionActivity Disks');   -- EDIT this value to be the actual path of the partitions
	RAISERROR ('PartitionDisk Added for SessionActivity - Be sure to edit the path!!!', 10, 1) WITH NOWAIT;
END
ELSE
	BEGIN
	RAISERROR ('Sessions PartitionDisks already exists.  No rows added.', 10, 1) WITH NOWAIT
	END