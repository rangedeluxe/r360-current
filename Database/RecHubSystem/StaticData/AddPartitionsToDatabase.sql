--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 10/04/16
*
* Purpose: To add partitions to an existing Database
*				Enter the desired BeginDate and EndDate before running
* Caution: Only enter dates prior to or after existing partitions.
* Caution: Be sure that the partitions do not already exist for the date range
* Caution: Backup the database before running this script
* Caution: Be sure to connect to the database you desire to add partitions to prior to executing this script
* Note:		The date is always the first day of any partition.  That is, 12/01/2015 contains 
*				all December data (for monthly partitions)
*
* Modification History
* 10/04/2016 PT 131197305	MGE		Created
* 02/22/2017 PT 127604193	MGE		Added DataImport partition (note: they are weekly partitions)
******************************************************************************/

DECLARE		@BeginDate	DATETIME = 'mm/dd/yyyy';		--For example, 10/01/2012
DECLARE		@EndDate		DATETIME = 'mm/dd/yyyy';		--For example, 12/01/2015

PRINT 'Creating Data Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubFacts', @BeginDate, @EndDate;

PRINT 'Creating Notification Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubNotifications', @BeginDate, @EndDate;

PRINT 'Creating Audit Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubAudits', @BeginDate, @EndDate;
