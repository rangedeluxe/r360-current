--WI 108876 JPB 07/12/13
--WFSScriptProcessorPrint Set Partition Check Constraints
--WFSScriptProcessorStaticDataName SetPartitionCheckConstraint
--WFSScriptPRocessorStaticDataCreateBegin
DECLARE @Loop SMALLINT,
		@PartitionManagerID SMALLINT;

DECLARE @PartitionManagerList TABLE
(
	RowID SMALLINT IDENTITY(1,1),
	PartitionManagerID SMALLINT
);

INSERT INTO @PartitionManagerList(PartitionManagerID)
SELECT	PartitionManagerID
FROM	RecHubSystem.PartitionManager;

SET @Loop = 1;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @PartitionManagerList) )
BEGIN
	SELECT @PartitionManagerID = PartitionManagerID
	FROM @PartitionManagerList
	WHERE RowID = @Loop;

	EXEC RecHubSystem.usp_CreateCheckConstraintsForPartition @PartitionManagerID;

	SET @Loop = @Loop + 1;
END
--WFSScriptPRocessorStaticDataCreateEnd