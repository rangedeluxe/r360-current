--WFSScriptProcessorSchema RecHubSystem
--WFSScriptProcessorStaticDataName query_Define_ImportSchemas
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/04/2012
*
* Purpose: Populate the ImportSchemas table.
*
* Modification History
* 04/04/2012 CR 45572 JPB	Created with DIT Batch XSD.
* 04/04/2012 CR 45573 JPB	DIT Client Setup XSD.
* 05/21/2012 CR 53016 JPB	Added version 1.01.00 for DIT XSD, XLS
* 08/07/2012 CR 53533 JPB	Added Notification XSD.
* 08/07/2012 CR 53534 JPB	Added Notification XSL.
* 07/19/2013 WI 109370 JPB	Updaed to 2.0.
*							Removed all OLTA* items.
* 02/12/2014 WI 129338 JPB	Added 2.01.00.00 version XSD and XSLT for DIT.
* 02/26/2014 WI 130926 CMC	Added FieldType of 'X' to 1.01.00 XSD version.
* 04/11/2014 WI 136089 CEJ	Change the DDA field from optional to required
* 04/15/2014 WI 136928 CMC	Added ABA and DDA to 2.01 Batch Data xsl
* 04/16/2014 WI 134607 JPB	Updated ABA and DDA for 2.01 Batch Data xsl
* 04/16/2014 WI 135304 JPB	Updated ABA and DDA for 2.01 Batch Data xsd
* 05/07/2014 WI 140076 JPB	Added RawPaymentData and Duplicate Detect to 2.01 Batch Data xsd
* 05/07/2014 WI 140078 JPB	Added RawPaymentData to 2.01 Batch Data xsl
* 05/09/2014 WI 140917 JPB	Removed required RT/Account from Batch Data xsd/updated XSLT
* 05/14/2014 WI 142176 CMC	Fix single tick issue on RT and Account in ImportSchemas DIT Batch Data XSL
* 05/23/2014 WI 143945 CMC  Need to add simpleContent to DIT schema
* 05/27/2014 WI 135695 CMC  Change BatchSourceKey to BatchSource
* 05/27/2014 WI 139423 CMC  Change BatchPaymentTypeKey to PaymentType
* 06/04/2014 WI 145847 CMC  Increase maxLength of TransactionSignature attribute
* 06/04/2014 WI 145848 CMC  Change RemittanceDataRecord to Unbounded
* 06/06/2014 WI 146276 CMC	Added FieldType of 'X' to 2.01.00.00 XSD version.
* 07/08/2014 WI 152404 CMC	Update Canonical Schema to support bigint BatchID.
* 10/24/2014 WI 174161 CMC	Add FieldLength to ImageRPSAliasMappings.
* 11/04/2014 WI 176132 CMC	Increase Client ShortName MaxLength to 20.
* 11/05/2014 WI 176387 JPB	Copy File* from the Batches node to Batch node.
* 03/27/2015 WI 196288 CMC	Add MinLength restriction to make Payment DDA required to get a value.
* 07/21/2015 WI 224322 CMC	New 2.01.02.00 version of xsd.
* 07/21/2015 WI 224323 CMC  New 2.01.02.00 version of xsl.
* 07/08/2015 WI 288728 JPB  New 2.02.04.00 version of xsd. Added Ghost Doc Item Data.
* 07/08/2015 WI 290792 JPB  New 2.02.04.00 version of xsl. Added Ghost Doc Item Data.
* 09/30/2016 PT 127613917 JPB	New 2.02.07.00 version of xsd,xsl. Added IsCorrespondence.
* 02/09/2017 PT 154233497 JPB	Added MessageWorkgroups to FIT xsd,xsl.
******************************************************************************/

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Hyland Document', @parmXSDVersion='1.0', @parmXSD=
'<xs:schema xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema" id="Images">
  <xs:element msdata:IsDataSet="true" msdata:UseCurrentLocale="true" name="Images">
    <xs:complexType>
      <xs:choice minOccurs="0" maxOccurs="unbounded">
        <xs:element name="Image">
          <xs:complexType>
            <xs:sequence>
              <xs:element minOccurs="0" name="Side" type="xs:string" />
              <xs:element minOccurs="0" name="ColorMode" type="xs:string" />
              <xs:element minOccurs="0" name="FileSize" type="xs:integer" />
              <xs:element minOccurs="0" name="ExternalDocumentID" type="xs:string" />
              <xs:element minOccurs="0" name="Page" type="xs:integer" />
            </xs:sequence>
          </xs:complexType>
        </xs:element>
      </xs:choice>
    </xs:complexType>
  </xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 45573
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup', @parmXSDVersion='1.0', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="ClientGroups" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
			<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
			<xs:maxLength value="36"/>
			<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="ClientGroups" >
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ClientGroup" minOccurs="1" maxOccurs="unbounded">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="Bank" minOccurs="1" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
									</xs:sequence>
									<xs:attribute name="BankName" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="25"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="ABA" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="10"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="BankID" type="xs:int" use="required" />
								</xs:complexType>
							</xs:element>
							<xs:element name="Site" minOccurs="0" maxOccurs="unbounded">
								<xs:complexType>
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LocalTimeZoneBias" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="128"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="DocumentTypes" minOccurs="0" maxOccurs="unbounded">
								<xs:complexType>
									<xs:attribute name="FileDescriptor" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="DocumentTypeDescription" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="16"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="Client"  minOccurs="0" maxOccurs="unbounded">
								<xs:complexType>
									<xs:sequence>
										<xs:element maxOccurs="unbounded" minOccurs="0"  name="DataEntryColumns">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="DataEntryColumn" minOccurs="0" maxOccurs="unbounded">
														<xs:complexType>
															<xs:sequence>
															</xs:sequence>
															<xs:attribute name="FieldName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayGroup" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="36"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DataType" type="xs:short" use="required" />
															<xs:attribute name="FieldLength" type="xs:unsignedByte" use="required" />
															<xs:attribute name="ScreenOrder" type="xs:unsignedByte" use="required" />
															<xs:attribute name="DataEntryColumnID" type="xs:int" use="required" />
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="DataEntryColumnsID" type="xs:int" use="required" />
											</xs:complexType>
										</xs:element>
										<xs:element maxOccurs="unbounded" minOccurs="0"  name="ImageRPSAliasMappings">
											<xs:complexType>
												<xs:sequence>
												</xs:sequence>
												<xs:attribute name="ExtractType" type="xs:short" use="required" />
												<xs:attribute name="DocType" type="xs:short" use="required" />
												<xs:attribute name="AliasName" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldType" use="required">
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:enumeration value="D"/>
															<xs:enumeration value="M"/>
															<xs:enumeration value="A"/>
															<xs:enumeration value="O"/>
															<xs:enumeration value=" "/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
									<xs:attribute name="ClientID" type="xs:int" use="required" />
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="11"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="OnlineColorMode" type="xs:unsignedByte" use="required" />
									<xs:attribute name="DDA" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="POBOX" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:maxLength value="32"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="ClientGroupID" type="xs:int" use="required" />
						<xs:attribute name="ClientGroupName" use="required" >
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="20"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="ClientTrackingID" type="GUID" use="required" />
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
			<xs:attribute name="XSDVersion" use="required" >
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="12"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 45572
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='1.0', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
			<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
			<xs:maxLength value="36"/>
			<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="Batches">
		<xs:complexType>
			<xs:sequence>
				<xs:element name="Batch" maxOccurs="unbounded" minOccurs="0">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="Transaction" maxOccurs="unbounded" minOccurs="0">
								<xs:complexType>
									<xs:sequence>
										<xs:element name="Payment" maxOccurs="unbounded" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" minOccurs="0" name="PaymentData">
														<xs:complexType>
															<xs:attribute name="FieldName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="FieldValue" use="required">
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:maxLength value="256"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												<xs:attribute name="Amount" type="xs:decimal" use="required" />
												<xs:attribute name="CheckSequence" type="xs:int"  />
												<xs:attribute name="Serial" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="30"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="RemitterName" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="60"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="RT" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="30"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="Account" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="30"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="TransactionCode" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="30"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
										<xs:element name="Document" maxOccurs="unbounded" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" minOccurs="0" name="RemittanceDataRecord">
														<xs:complexType>
															<xs:sequence>
																<xs:element maxOccurs="unbounded" minOccurs="0" name="RemittanceData">
																	<xs:complexType>
																		<xs:attribute name="FieldName" use="required" >
																			<xs:simpleType>
																				<xs:restriction base="xs:string">
																					<xs:minLength value="1"/>
																					<xs:maxLength value="32"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="FieldValue" use="required" >
																			<xs:simpleType>
																				<xs:restriction base="xs:string">
																					<xs:maxLength value="256"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																	</xs:complexType>
																</xs:element>
															</xs:sequence>
															<xs:attribute name="BatchSequence" type="xs:int" use="required" />
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												<xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
												<xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
												<xs:attribute name="DocumentDescriptor" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:maxLength value="30"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
										<xs:element name="GhostDocument" maxOccurs="unbounded" minOccurs="0">
											<xs:complexType>
												<xs:sequence>
													<xs:element maxOccurs="unbounded" minOccurs="0" name="RemittanceDataRecord">
														<xs:complexType>
															<xs:sequence>
																<xs:element maxOccurs="unbounded" minOccurs="0" name="RemittanceData">
																	<xs:complexType>
																		<xs:attribute name="FieldName" use="required" >
																			<xs:simpleType>
																				<xs:restriction base="xs:string">
																					<xs:minLength value="1"/>
																					<xs:maxLength value="32"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																		<xs:attribute name="FieldValue" use="required" >
																			<xs:simpleType>
																				<xs:restriction base="xs:string">
																					<xs:maxLength value="256"/>
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																	</xs:complexType>
																</xs:element>
															</xs:sequence>
															<xs:attribute name="BatchSequence" type="xs:int" use="required" />
														</xs:complexType>
													</xs:element>
												</xs:sequence>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
									<xs:attribute name="TransactionID" type="xs:int" use="required" />
									<xs:attribute name="TransactionSequence" type="xs:int" use="required" />
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="ProcessingDate" type="xs:date" use="required" />
						<xs:attribute name="DepositDate" type="xs:date" use="required" />
						<xs:attribute name="BankID" type="xs:int" use="required" />
						<xs:attribute name="ClientID" type="xs:int" use="required" />
						<xs:attribute name="BatchID" type="xs:int" use="required" />
						<xs:attribute name="BatchDate" type="xs:date" use="required" />
						<xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
						<xs:attribute name="BatchPaymentTypeKey" type="xs:unsignedByte" use="required" />
						<xs:attribute name="BatchSourceKey" type="xs:unsignedByte" use="required" />
						<xs:attribute name="BatchTrackingID" type="GUID" use="required" />
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
			<xs:attribute name="XSDVersion" use="required">
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="12"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='1.0', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSourceKey">
				<xsl:value-of select="@BatchSourceKey"/>
			</xsl:attribute>
			<xsl:attribute name="BatchPaymentTypeKey">
				<xsl:value-of select="@BatchPaymentTypeKey"/>
			</xsl:attribute>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:attribute name="RT">
				<xsl:value-of select="@RT"/>
			</xsl:attribute>
			<xsl:attribute name="Account">
				<xsl:value-of select="@Account"/>
			</xsl:attribute>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:if test="CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord">
		<xsl:element name="DocumentRemittanceDataRecord">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord">
		<xsl:element name="GhostDocumentRemittanceDataRecord">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup XSL', @parmXSDVersion='1.0', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ClientGroups">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup">
		<xsl:copy>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupName">
				<xsl:value-of select="@ClientGroupName"/>
			</xsl:attribute>
			<xsl:attribute name="ClientTrackingID">
				<xsl:value-of select="@ClientTrackingID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Bank">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="BankName">
				<xsl:value-of select="@BankName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Site">
		<xsl:copy>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:attribute name="LongName">
				<xsl:value-of select="@LongName"/>
			</xsl:attribute>
			<xsl:attribute name="LocalTimeZoneBias">
				<xsl:value-of select="@LocalTimeZoneBias"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/DocumentTypes">
		<xsl:copy>
			<xsl:attribute name="FileDescriptor">
				<xsl:value-of select="@FileDescriptor"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentTypeDescription">
				<xsl:value-of select="@DocumentTypeDescription"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:attribute name="LongName">
				<xsl:value-of select="@LongName"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:attribute name="POBOX">
				<xsl:value-of select="@POBOX"/>
			</xsl:attribute>
			<xsl:attribute name="OnlineColorMode">
				<xsl:value-of select="@OnlineColorMode"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnsID">
				<xsl:value-of select="@DataEntryColumnsID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns/DataEntryColumn">
		<xsl:copy>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnID">
				<xsl:value-of select="@DataEntryColumnID"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="ScreenOrder">
				<xsl:value-of select="@ScreenOrder"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayGroup">
				<xsl:value-of select="@DisplayGroup"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayName">
				<xsl:value-of select="@DisplayName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/ImageRPSAliasMappings">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ExtractType">
				<xsl:value-of select="@ExtractType"/>
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="@DocType"/>
			</xsl:attribute>
			<xsl:attribute name="FieldType">
				<xsl:value-of select="@FieldType"/>
			</xsl:attribute>
			<xsl:attribute name="AliasName">
				<xsl:value-of select="@AliasName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53016
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup', @parmXSDVersion='1.01.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="ClientGroups" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
			<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
			<xs:maxLength value="36"/>
			<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="ClientGroups" >
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ClientGroup" minOccurs="1" maxOccurs="500000">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="Bank" minOccurs="1" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
									</xs:sequence>
									<xs:attribute name="BankName" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="25"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="ABA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="10"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="BankID" type="xs:int" use="required" />
								</xs:complexType>
							</xs:element>
							<xs:element name="Site" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LocalTimeZoneBias" type="xs:int" use="required" />
									<xs:attribute name="AutoUpdateCurrentProcessingDate" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="128"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="DocumentTypes" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="FileDescriptor" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="DocumentTypeDescription" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="16"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="Client" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
										<xs:element maxOccurs="500000" minOccurs="0" name="DataEntryColumns">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="DataEntryColumn" minOccurs="0" maxOccurs="500000">
														<xs:complexType>
															<xs:sequence>
															</xs:sequence>
															<xs:attribute name="FieldName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayGroup" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="36"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DataType" type="xs:short" use="required" />
															<xs:attribute name="FieldLength" type="xs:unsignedByte" use="required" />
															<xs:attribute name="ScreenOrder" type="xs:unsignedByte" use="required" />
															<xs:attribute name="MarkSense" type="xs:unsignedByte" use="optional" />
															<xs:attribute name="DataEntryColumnID" type="xs:int" use="required" />
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="DataEntryColumnsID" type="xs:int" use="required" />
											</xs:complexType>
										</xs:element>
										<xs:element maxOccurs="500000" minOccurs="0" name="ImageRPSAliasMappings">
											<xs:complexType>
												<xs:sequence>
												</xs:sequence>
												<xs:attribute name="ExtractType" type="xs:short" use="required" />
												<xs:attribute name="DocType" type="xs:short" use="required" />
                        <xs:attribute name="DataType" type="xs:short" use="required" />
                        <xs:attribute name="AliasName" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldType" use="required">
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:enumeration value="D"/>
															<xs:enumeration value="M"/>
															<xs:enumeration value="A"/>
															<xs:enumeration value="O"/>
                              <xs:enumeration value="$"/>
                              <xs:enumeration value=""/>
															<xs:enumeration value="X"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
									<xs:attribute name="ClientID" type="xs:int" use="required" />
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="11"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="OnlineColorMode" type="xs:unsignedByte" use="optional" />
									<xs:attribute name="DDA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="POBOX" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:maxLength value="32"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="ClientGroupID" type="xs:int" use="required" />
						<xs:attribute name="ClientGroupName" use="optional" >
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="20"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="ClientTrackingID" type="GUID" use="required" />
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required" >
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="12"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
    </xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53016
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='1.01.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
      <xs:maxLength value="36"/>
      <xs:minLength value="36"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Batches">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Batch" maxOccurs="500000" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="1" minOccurs="0" name="BatchDataRecord">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element maxOccurs="500000" minOccurs="0" name="BatchData">
                      <xs:complexType>
                        <xs:attribute name="FieldName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="32"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="FieldValue" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Transaction" maxOccurs="500000" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="Payment" maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="Images"  maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="Amount" type="xs:decimal" use="required" />
                        <xs:attribute name="CheckSequence" type="xs:int"  />
                        <xs:attribute name="Serial" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RemitterName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="60"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RT" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="Account" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="TransactionCode" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="Document" maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="Images" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentDescriptor" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="GhostDocument" maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name="TransactionID" type="xs:int" use="required" />
                  <xs:attribute name="TransactionSequence" type="xs:int" use="required" />
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="ProcessingDate" type="xs:date" use="required" />
            <xs:attribute name="DepositDate" type="xs:date" use="required" />
            <xs:attribute name="BankID" type="xs:int" use="required" />
            <xs:attribute name="ClientID" type="xs:int" use="required" />
            <xs:attribute name="BatchCueID" type="xs:string" use="optional" />
            <xs:attribute name="BatchID" type="xs:int" use="required" />
            <xs:attribute name="BatchDate" type="xs:date" use="required" />
            <xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
            <xs:attribute name="BatchNumber" type="xs:int" use="optional" />
            <xs:attribute name="BatchPaymentTypeKey" type="xs:unsignedByte" use="required" />
            <xs:attribute name="BatchSourceKey" type="xs:unsignedByte" use="required" />
            <xs:attribute name="BatchTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:maxLength value="12"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53016
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup XSL', @parmXSDVersion='1.01.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ClientGroups">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup">
		<xsl:copy>
			<xsl:attribute name="ClientTrackingID">
				<xsl:value-of select="@ClientTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:if test="@ClientGroupName">
				<xsl:attribute name="ClientGroupName">
					<xsl:value-of select="@ClientGroupName"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Bank">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:if test="@BankName">
				<xsl:attribute name="BankName">
					<xsl:value-of select="@BankName"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@ABA">
				<xsl:attribute name="ABA">
					<xsl:value-of select="@ABA"/>
				</xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Site">
		<xsl:copy>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="LocalTimeZoneBias">
				<xsl:value-of select="@LocalTimeZoneBias"/>
			</xsl:attribute>
			<xsl:attribute name="AutoUpdateCurrentProcessingDate">
				<xsl:value-of select="@AutoUpdateCurrentProcessingDate"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/DocumentTypes">
		<xsl:copy>
			<xsl:attribute name="FileDescriptor">
				<xsl:value-of select="@FileDescriptor"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentTypeDescription">
				<xsl:value-of select="@DocumentTypeDescription"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@OnlineColorMode">
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="@OnlineColorMode"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@POBOX">
				<xsl:attribute name="POBox">
					<xsl:value-of select="@POBOX"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@DDA">
				<xsl:attribute name="DDA">
					<xsl:value-of select="@DDA"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnsID">
				<xsl:value-of select="@DataEntryColumnsID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns/DataEntryColumn">
		<xsl:copy>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnID">
				<xsl:value-of select="@DataEntryColumnID"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="ScreenOrder">
				<xsl:value-of select="@ScreenOrder"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@MarkSense">
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="@MarkSense"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="0"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="DisplayGroup">
				<xsl:value-of select="@DisplayGroup"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayName">
				<xsl:value-of select="@DisplayName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/ImageRPSAliasMappings">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ExtractType">
				<xsl:value-of select="@ExtractType"/>
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="@DocType"/>
			</xsl:attribute>
      <xsl:attribute name="DataType">
        <xsl:value-of select="@DataType"/>
      </xsl:attribute>
      <xsl:attribute name="FieldType">
				<xsl:value-of select="@FieldType"/>
			</xsl:attribute>
			<xsl:attribute name="AliasName">
				<xsl:value-of select="@AliasName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53016
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='1.01.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
      <xsl:attribute name="XSDVersion">
        <xsl:value-of select="@XSDVersion"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSourceKey">
				<xsl:value-of select="@BatchSourceKey"/>
			</xsl:attribute>
			<xsl:attribute name="BatchPaymentTypeKey">
				<xsl:value-of select="@BatchPaymentTypeKey"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:attribute name="RT">
				<xsl:value-of select="@RT"/>
			</xsl:attribute>
			<xsl:attribute name="Account">
				<xsl:value-of select="@Account"/>
			</xsl:attribute>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:if test="CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53533
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Notification Import Integration Services', @parmXSDVersion='1.00.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
      <xs:maxLength value="36"/>
      <xs:minLength value="36"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Notifications">
		<xs:complexType>
			<xs:sequence>
				<xs:element maxOccurs="unbounded" name="Notification">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="MessageParts" minOccurs="0" maxOccurs="1">
								<xs:complexType>
									<xs:sequence>
										<xs:element minOccurs="0" maxOccurs="unbounded" name="MessagePart" type="xs:string" />
									</xs:sequence>
								</xs:complexType>
							</xs:element>
							<xs:element name="Files">
								<xs:complexType>
									<xs:sequence>
										<xs:element minOccurs="0" maxOccurs="unbounded" name="File">
											<xs:complexType>
												<xs:attribute name="FileIdentifier" type="xs:string" use="required" />
												<xs:attribute name="ClientFilePath" type="xs:string" use="required" />
												<xs:attribute name="FileType" type="xs:string" use="required" />
												<xs:attribute name="UserFileName" type="xs:string" use="required" />
												<xs:attribute name="FileExtension" type="xs:string" use="required" />
											</xs:complexType>
										</xs:element>
									</xs:sequence>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="BankID" type="xs:integer" use="required" />
						<xs:attribute name="ClientGroupID" type="xs:integer" use="required" />
						<xs:attribute name="ClientID" type="xs:integer" use="required" />
						<xs:attribute name="NotificationDate" type="xs:string" use="required" />
						<xs:attribute name="SourceNotificationID" type="GUID" use="optional" />
						<xs:attribute name="NotificationSourceKey" type="xs:integer" use="required" />
						<xs:attribute name="NotificationTrackingID" type="GUID" use="required" />
          </xs:complexType>
				</xs:element>
			</xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required" >
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:maxLength value="12"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Notification Import Integration Services', @parmXSDVersion='2.02.14', @parmXSD=
'<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="unqualified" elementFormDefault="qualified">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}" />
      <xs:maxLength value="36" />
      <xs:minLength value="36" />
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Notifications">
    <xs:complexType>
      <xs:sequence>
        <xs:element maxOccurs="unbounded" name="Notification">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MessageParts" minOccurs="0" maxOccurs="1">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs="0" maxOccurs="unbounded" name="MessagePart" type="xs:string" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Files">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs="0" maxOccurs="unbounded" name="File">
                      <xs:complexType>
                        <xs:attribute name="FileIdentifier" type="xs:string" use="required" />
                        <xs:attribute name="ClientFilePath" type="xs:string" use="required" />
                        <xs:attribute name="FileType" type="xs:string" use="required" />
                        <xs:attribute name="UserFileName" type="xs:string" use="required" />
                        <xs:attribute name="FileExtension" type="xs:string" use="required" />
						<xs:attribute name="FileSize" type="xs:long" use="required" />
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="BankID" type="xs:integer" use="required" />
            <xs:attribute name="ClientGroupID" type="xs:integer" use="required" />
            <xs:attribute name="ClientID" type="xs:integer" use="required" />
            <xs:attribute name="NotificationDate" type="xs:string" use="required" />
            <xs:attribute name="SourceNotificationID" type="GUID" use="optional" />
            <xs:attribute name="NotificationSourceKey" type="xs:integer" use="required" />
            <xs:attribute name="NotificationTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40" />
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1" />
            <xs:maxLength value="12" />
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
    </xs:complexType>
  </xs:element>
</xs:schema>
';

--WFSScriptProcessorStaticDataCreateBegin
--CR 53534
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Notification Import Integration Services XSL', @parmXSDVersion='1.00.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Notifications">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationDate">
				<xsl:value-of select="@NotificationDate"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationTrackingID">
				<xsl:value-of select="@NotificationTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationSourceKey">
				<xsl:value-of select="@NotificationSourceKey"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@SourceNotificationID">
          <xsl:attribute name="SourceNotificationID">
            <xsl:value-of select="@SourceNotificationID"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="SourceNotificationID">00000000-0000-0000-0000-000000000000</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/MessageParts">
    <xsl:if test="normalize-space(.) != ''''">
      <xsl:copy>
        <xsl:attribute name="MessageParts_Id">
          <xsl:value-of select="count(preceding::MessageParts)+1"/>
        </xsl:attribute>
        <xsl:apply-templates select="node()"/>
      </xsl:copy>
    </xsl:if>
	</xsl:template>
	<xsl:template match="Notifications/Notification/MessageParts/MessagePart">
		<xsl:copy>
			<xsl:attribute name="NotificationMessagePart">
				<xsl:value-of select="count(preceding-sibling::MessagePart)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/Files">
		<xsl:copy>
			<xsl:attribute name="Files_Id">
				<xsl:value-of select="count(preceding::Files)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/Files/File">
		<xsl:copy>
			<xsl:attribute name="File_Id">
				<xsl:value-of select="count(preceding::File)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FileType">
				<xsl:value-of select="@FileType"/>
			</xsl:attribute>
			<xsl:attribute name="UserFileName">
				<xsl:value-of select="@UserFileName"/>
			</xsl:attribute>
			<xsl:attribute name="FileExtension">
				<xsl:value-of select="@FileExtension"/>
			</xsl:attribute>
			<xsl:attribute name="FileIdentifier">
				<xsl:value-of select="@FileIdentifier"/>
			</xsl:attribute>
			<xsl:attribute name="FileSize">
			  <xsl:value-of select="@FileSize"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 129338
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup', @parmXSDVersion='2.01.00.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="ClientGroups" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
			<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
			<xs:maxLength value="36"/>
			<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="ClientGroups" >
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ClientGroup" minOccurs="1" maxOccurs="500000">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="Bank" minOccurs="1" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
									</xs:sequence>
									<xs:attribute name="BankName" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="25"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="ABA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="10"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="BankID" type="xs:int" use="required" />
								</xs:complexType>
							</xs:element>
							<xs:element name="Site" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LocalTimeZoneBias" type="xs:int" use="required" />
									<xs:attribute name="AutoUpdateCurrentProcessingDate" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="128"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="DocumentTypes" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="FileDescriptor" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="DocumentTypeDescription" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="16"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="Client" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
										<xs:element maxOccurs="500000" minOccurs="0" name="DataEntryColumns">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="DataEntryColumn" minOccurs="0" maxOccurs="500000">
														<xs:complexType>
															<xs:sequence>
															</xs:sequence>
															<xs:attribute name="FieldName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayGroup" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="36"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DataType" type="xs:short" use="required" />
															<xs:attribute name="FieldLength" type="xs:unsignedByte" use="required" />
															<xs:attribute name="ScreenOrder" type="xs:unsignedByte" use="required" />
															<xs:attribute name="MarkSense" type="xs:unsignedByte" use="optional" />
															<xs:attribute name="DataEntryColumnID" type="xs:int" use="required" />
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="DataEntryColumnsID" type="xs:int" use="required" />
											</xs:complexType>
										</xs:element>
										<xs:element maxOccurs="500000" minOccurs="0" name="ImageRPSAliasMappings">
											<xs:complexType>
												<xs:sequence>
												</xs:sequence>
												<xs:attribute name="ExtractType" type="xs:short" use="required" />
												<xs:attribute name="DocType" type="xs:short" use="required" />
                        <xs:attribute name="DataType" type="xs:short" use="required" />
                        <xs:attribute name="AliasName" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldType" use="required">
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:enumeration value="D"/>
															<xs:enumeration value="M"/>
															<xs:enumeration value="A"/>
															<xs:enumeration value="O"/>
                              <xs:enumeration value="$"/>
                              <xs:enumeration value=""/>
															<xs:enumeration value="X"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldLength" type="xs:short" />
											</xs:complexType>
										</xs:element>
									</xs:sequence>
									<xs:attribute name="ClientID" type="xs:int" use="required" />
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="20"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="OnlineColorMode" type="xs:unsignedByte" use="optional" />
									<xs:attribute name="DDA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="POBOX" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:maxLength value="32"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
                  <xs:attribute name="DataRetentionDays" use="required">
                    <xs:simpleType>
                      <xs:restriction base="xs:integer">
                        <xs:minInclusive value="365"></xs:minInclusive>
                        <xs:maxInclusive value="2557"></xs:maxInclusive>
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
                  <xs:attribute name="ImageRetentionDays" use="required">
                    <xs:simpleType>
                      <xs:restriction base="xs:integer">
                        <xs:minInclusive value="365"></xs:minInclusive>
                        <xs:maxInclusive value="2557"></xs:maxInclusive>
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="ClientGroupID" type="xs:int" use="required" />
						<xs:attribute name="ClientGroupName" use="optional" >
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="20"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="ClientTrackingID" type="GUID" use="required" />
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required" >
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="12"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
    </xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 129338
--WI 131116
--WI 135304
--WI 140078
--WI 140917
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='2.01.00.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
      <xs:maxLength value="36"/>
      <xs:minLength value="36"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Batches">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Batch"  maxOccurs="500000" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="1" minOccurs="0" name="BatchDataRecord">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element  maxOccurs="500000" minOccurs="0" name="BatchData">
                      <xs:complexType>
                        <xs:attribute name="FieldName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="32"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="FieldValue" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Transaction"  maxOccurs="500000" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="Payment"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="Images"  maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                           <xs:element name="RawDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RawData">
                            <xs:complexType>
                              <xs:simpleContent>
                                <xs:extension base="xs:string">
                                  <xs:attribute name="RawSequence" type="xs:int" use="required" />
                                  <xs:attribute name="RawDataPart" use="required">
                                    <xs:simpleType>
                                      <xs:restriction base="xs:int">
                                      </xs:restriction>
                                    </xs:simpleType>
                                  </xs:attribute>
                                </xs:extension>
                              </xs:simpleContent>
                            </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="Amount" type="xs:decimal" use="required" />
                        <xs:attribute name="CheckSequence" type="xs:int"  />
                        <xs:attribute name="Serial" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RemitterName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="60"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RT" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="Account" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="TransactionCode" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
						<xs:attribute name="ABA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:maxLength value="9" />
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="DDA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="35" />
								</xs:restriction>
								</xs:simpleType>
						</xs:attribute>
						</xs:complexType>
                    </xs:element>
                    <xs:element name="Document"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="Images" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
				  <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
						  <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentDescriptor" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="GhostDocument"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name="TransactionID" type="xs:int" use="required" />
                  <xs:attribute name="TransactionSequence" type="xs:int" use="required" />
				  <xs:attribute name="TransactionHash">
					<xs:simpleType>
					  <xs:restriction base="xs:string">
						<xs:minLength value="40"/>
						<xs:maxLength value="40"/>
					  </xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
				  <xs:attribute name="TransactionSignature">
					<xs:simpleType>
						<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="65"/>
						</xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="ProcessingDate" type="xs:date" use="required" />
            <xs:attribute name="DepositDate" type="xs:date" use="required" />
            <xs:attribute name="BankID" type="xs:int" use="required" />
            <xs:attribute name="ClientID" type="xs:int" use="required" />
            <xs:attribute name="BatchCueID" type="xs:string" use="optional" />
            <xs:attribute name="BatchID" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:long">
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchDate" type="xs:date" use="required" />
            <xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
            <xs:attribute name="BatchNumber" type="xs:int" use="optional" />
            <xs:attribute name="PaymentType" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchSource" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:maxLength value="12"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
 	<xs:attribute name="FileHash">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="40"/>
			<xs:maxLength value="40"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	<xs:attribute name="FileSignature">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="55"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	</xs:complexType>
</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 129338
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup XSL', @parmXSDVersion='2.01.00.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ClientGroups">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup">
		<xsl:copy>
			<xsl:attribute name="ClientTrackingID">
				<xsl:value-of select="@ClientTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:if test="@ClientGroupName">
				<xsl:attribute name="ClientGroupName">
					<xsl:value-of select="@ClientGroupName"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Bank">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="BankName">
				<xsl:value-of select="@BankName"/>
			</xsl:attribute>
			<xsl:if test="@ABA">
				<xsl:attribute name="ABA">
					<xsl:value-of select="@ABA"/>
				</xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Site">
		<xsl:copy>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="LocalTimeZoneBias">
				<xsl:value-of select="@LocalTimeZoneBias"/>
			</xsl:attribute>
			<xsl:attribute name="AutoUpdateCurrentProcessingDate">
				<xsl:value-of select="@AutoUpdateCurrentProcessingDate"/>
			</xsl:attribute>
      
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/DocumentTypes">
		<xsl:copy>
			<xsl:attribute name="FileDescriptor">
				<xsl:value-of select="@FileDescriptor"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentTypeDescription">
				<xsl:value-of select="@DocumentTypeDescription"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@OnlineColorMode">
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="@OnlineColorMode"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@POBOX">
				<xsl:attribute name="POBox">
					<xsl:value-of select="@POBOX"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@DDA">
				<xsl:attribute name="DDA">
					<xsl:value-of select="@DDA"/>
				</xsl:attribute>
			</xsl:if>
      <xsl:attribute name="DataRetentionDays">
        <xsl:value-of select="@DataRetentionDays"/>
      </xsl:attribute>
      <xsl:attribute name="ImageRetentionDays">
        <xsl:value-of select="@ImageRetentionDays"/>
      </xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnsID">
				<xsl:value-of select="@DataEntryColumnsID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns/DataEntryColumn">
		<xsl:copy>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnID">
				<xsl:value-of select="@DataEntryColumnID"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="ScreenOrder">
				<xsl:value-of select="@ScreenOrder"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@MarkSense">
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="@MarkSense"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="0"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="DisplayGroup">
				<xsl:value-of select="@DisplayGroup"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayName">
				<xsl:value-of select="@DisplayName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/ImageRPSAliasMappings">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ExtractType">
				<xsl:value-of select="@ExtractType"/>
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="@DocType"/>
			</xsl:attribute>
      <xsl:attribute name="DataType">
        <xsl:value-of select="@DataType"/>
      </xsl:attribute>
      <xsl:attribute name="FieldType">
				<xsl:value-of select="@FieldType"/>
			</xsl:attribute>
			<xsl:attribute name="AliasName">
				<xsl:value-of select="@AliasName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 129338
--WI 134607
--WI 140078
--WI 140917
--WI 176387
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='2.01.00.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
      <xsl:attribute name="XSDVersion">
        <xsl:value-of select="@XSDVersion"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="../@FileHash">
        <xsl:attribute name="FileHash">
          <xsl:value-of select="../@FileHash"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="../@FileSignature">
        <xsl:attribute name="FileSignature">
          <xsl:value-of select="../@FileSignature"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentType">
				<xsl:value-of select="@PaymentType"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ABA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="DDA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
      <xsl:if test="@TransactionHash">
        <xsl:attribute name="TransactionHash">
				  <xsl:value-of select="@TransactionHash"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:if test="@TransactionSignature">
        <xsl:attribute name="TransactionSignature">
				  <xsl:value-of select="@TransactionSignature"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@RT">
					<xsl:attribute name="RT">
						<xsl:value-of select="@RT"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="RT">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@Account">
					<xsl:attribute name="Account">
						<xsl:value-of select="@Account"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Account">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:if test="@CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RawDataRecord/RawData">
		<xsl:element name="PaymentRawData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RawSequence">
				<xsl:value-of select="@RawSequence"/>
			</xsl:attribute>
			<xsl:attribute name="RawDataPart">
				<xsl:value-of select="@RawDataPart"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 224322
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup', @parmXSDVersion='2.01.02.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema id="ClientGroups" xmlns="" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
			<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
			<xs:maxLength value="36"/>
			<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="ClientGroups" >
		<xs:complexType>
			<xs:sequence>
				<xs:element name="ClientGroup" minOccurs="1" maxOccurs="500000">
					<xs:complexType>
						<xs:sequence>
							<xs:element name="Bank" minOccurs="1" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
									</xs:sequence>
									<xs:attribute name="BankName" use="required">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="25"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="ABA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="10"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="BankID" type="xs:int" use="required" />
								</xs:complexType>
							</xs:element>
							<xs:element name="Site" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LocalTimeZoneBias" type="xs:int" use="required" />
									<xs:attribute name="AutoUpdateCurrentProcessingDate" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="128"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="DocumentTypes" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:attribute name="FileDescriptor" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="30"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="DocumentTypeDescription" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="16"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
								</xs:complexType>
							</xs:element>
							<xs:element name="Client" minOccurs="0" maxOccurs="500000">
								<xs:complexType>
									<xs:sequence>
										<xs:element maxOccurs="500000" minOccurs="0" name="DataEntryColumns">
											<xs:complexType>
												<xs:sequence>
													<xs:element name="DataEntryColumn" minOccurs="0" maxOccurs="500000">
														<xs:complexType>
															<xs:sequence>
															</xs:sequence>
															<xs:attribute name="FieldName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayName" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="32"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DisplayGroup" use="required" >
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1"/>
																		<xs:maxLength value="36"/>
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
															<xs:attribute name="DataType" type="xs:short" use="required" />
															<xs:attribute name="FieldLength" type="xs:unsignedByte" use="required" />
															<xs:attribute name="ScreenOrder" type="xs:unsignedByte" use="required" />
															<xs:attribute name="MarkSense" type="xs:unsignedByte" use="optional" />
															<xs:attribute name="DataEntryColumnID" type="xs:int" use="required" />
															<xs:attribute name="BatchSource" use="required">
																<xs:simpleType>
																	<xs:restriction base="xs:string">
																		<xs:minLength value="1" />
																		<xs:maxLength value="30" />
																	</xs:restriction>
																</xs:simpleType>
															</xs:attribute>
														</xs:complexType>
													</xs:element>
												</xs:sequence>
												<xs:attribute name="DataEntryColumnsID" type="xs:int" use="required" />
											</xs:complexType>
										</xs:element>
										<xs:element maxOccurs="500000" minOccurs="0" name="ImageRPSAliasMappings">
											<xs:complexType>
												<xs:sequence>
												</xs:sequence>
												<xs:attribute name="ExtractType" type="xs:short" use="required" />
												<xs:attribute name="DocType" type="xs:short" use="required" />
												<xs:attribute name="DataType" type="xs:short" use="required" />
												<xs:attribute name="AliasName" use="required" >
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:minLength value="1"/>
															<xs:maxLength value="256"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldType" use="required">
													<xs:simpleType>
														<xs:restriction base="xs:string">
															<xs:enumeration value="D"/>
															<xs:enumeration value="M"/>
															<xs:enumeration value="A"/>
															<xs:enumeration value="O"/>
															<xs:enumeration value="$"/>
															<xs:enumeration value=""/>
															<xs:enumeration value="X"/>
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
												<xs:attribute name="FieldLength" type="xs:short" />
												<xs:attribute name="BatchSource" use="required">
													<xs:simpleType>
														<xs:restriction base="xs:string">
														<xs:minLength value="1" />
														<xs:maxLength value="30" />
														</xs:restriction>
													</xs:simpleType>
												</xs:attribute>
											</xs:complexType>
										</xs:element>
									</xs:sequence>
									<xs:attribute name="ClientID" type="xs:int" use="required" />
									<xs:attribute name="ShortName" use="required" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="20"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="SiteCode" type="xs:int" use="required" />
									<xs:attribute name="LongName" use="optional" >
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="OnlineColorMode" type="xs:unsignedByte" use="optional" />
									<xs:attribute name="DDA" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:minLength value="1"/>
												<xs:maxLength value="40"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
									<xs:attribute name="POBOX" use="optional">
										<xs:simpleType>
											<xs:restriction base="xs:string">
												<xs:maxLength value="32"/>
											</xs:restriction>
										</xs:simpleType>
									</xs:attribute>
                  <xs:attribute name="DataRetentionDays" use="required">
                    <xs:simpleType>
                      <xs:restriction base="xs:integer">
                        <xs:minInclusive value="365"></xs:minInclusive>
                        <xs:maxInclusive value="2557"></xs:maxInclusive>
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
                  <xs:attribute name="ImageRetentionDays" use="required">
                    <xs:simpleType>
                      <xs:restriction base="xs:integer">
                        <xs:minInclusive value="365"></xs:minInclusive>
                        <xs:maxInclusive value="2557"></xs:maxInclusive>
                      </xs:restriction>
                    </xs:simpleType>
                  </xs:attribute>
								</xs:complexType>
							</xs:element>
						</xs:sequence>
						<xs:attribute name="ClientGroupID" type="xs:int" use="required" />
						<xs:attribute name="ClientGroupName" use="optional" >
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="20"/>
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="ClientTrackingID" type="GUID" use="required" />
					</xs:complexType>
				</xs:element>
			</xs:sequence>
			<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required" >
				<xs:simpleType>
					<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="12"/>
					</xs:restriction>
				</xs:simpleType>
			</xs:attribute>
    </xs:complexType>
	</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='2.01.02.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
      <xs:maxLength value="36"/>
      <xs:minLength value="36"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Batches">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Batch"  maxOccurs="500000" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="1" minOccurs="0" name="BatchDataRecord">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element  maxOccurs="500000" minOccurs="0" name="BatchData">
                      <xs:complexType>
                        <xs:attribute name="FieldName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="32"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="FieldValue" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Transaction"  maxOccurs="500000" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="Payment"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="Images"  maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                           <xs:element name="RawDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RawData">
                            <xs:complexType>
                              <xs:simpleContent>
                                <xs:extension base="xs:string">
                                  <xs:attribute name="RawSequence" type="xs:int" use="required" />
                                  <xs:attribute name="RawDataPart" use="required">
                                    <xs:simpleType>
                                      <xs:restriction base="xs:int">
                                      </xs:restriction>
                                    </xs:simpleType>
                                  </xs:attribute>
                                </xs:extension>
                              </xs:simpleContent>
                            </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="Amount" type="xs:decimal" use="required" />
                        <xs:attribute name="CheckSequence" type="xs:int"  />
                        <xs:attribute name="Serial" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RemitterName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="60"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RT" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="Account" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="TransactionCode" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
						<xs:attribute name="ABA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:maxLength value="9" />
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="DDA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="35" />
								</xs:restriction>
								</xs:simpleType>
						</xs:attribute>
						</xs:complexType>
                    </xs:element>
                    <xs:element name="Document"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="Images" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
				  <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
						  <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentDescriptor" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="GhostDocument"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name="TransactionID" type="xs:int" use="required" />
                  <xs:attribute name="TransactionSequence" type="xs:int" use="required" />
				  <xs:attribute name="TransactionHash">
					<xs:simpleType>
					  <xs:restriction base="xs:string">
						<xs:minLength value="40"/>
						<xs:maxLength value="40"/>
					  </xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
				  <xs:attribute name="TransactionSignature">
					<xs:simpleType>
						<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="65"/>
						</xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="ProcessingDate" type="xs:date" use="required" />
            <xs:attribute name="DepositDate" type="xs:date" use="required" />
            <xs:attribute name="BankID" type="xs:int" use="required" />
            <xs:attribute name="ClientID" type="xs:int" use="required" />
            <xs:attribute name="BatchCueID" type="xs:string" use="optional" />
            <xs:attribute name="BatchID" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:long">
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchDate" type="xs:date" use="required" />
            <xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
            <xs:attribute name="BatchNumber" type="xs:int" use="optional" />
            <xs:attribute name="PaymentType" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchSource" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:maxLength value="12"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
 	<xs:attribute name="FileHash">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="40"/>
			<xs:maxLength value="40"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	<xs:attribute name="FileSignature">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="55"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	</xs:complexType>
</xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 224323
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Client Setup XSL', @parmXSDVersion='2.01.02.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ClientGroups">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup">
		<xsl:copy>
			<xsl:attribute name="ClientTrackingID">
				<xsl:value-of select="@ClientTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:if test="@ClientGroupName">
				<xsl:attribute name="ClientGroupName">
					<xsl:value-of select="@ClientGroupName"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Bank">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="BankName">
				<xsl:value-of select="@BankName"/>
			</xsl:attribute>
			<xsl:if test="@ABA">
				<xsl:attribute name="ABA">
					<xsl:value-of select="@ABA"/>
				</xsl:attribute>
			</xsl:if>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Site">
		<xsl:copy>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="LocalTimeZoneBias">
				<xsl:value-of select="@LocalTimeZoneBias"/>
			</xsl:attribute>
			<xsl:attribute name="AutoUpdateCurrentProcessingDate">
				<xsl:value-of select="@AutoUpdateCurrentProcessingDate"/>
			</xsl:attribute>
      
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/DocumentTypes">
		<xsl:copy>
			<xsl:attribute name="FileDescriptor">
				<xsl:value-of select="@FileDescriptor"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentTypeDescription">
				<xsl:value-of select="@DocumentTypeDescription"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@LongName">
					<xsl:attribute name="LongName">
						<xsl:value-of select="@LongName"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="LongName">
						<xsl:value-of select="@ShortName"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@OnlineColorMode">
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="@OnlineColorMode"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="OnlineColorMode">
						<xsl:value-of select="1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="@POBOX">
				<xsl:attribute name="POBox">
					<xsl:value-of select="@POBOX"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@DDA">
				<xsl:attribute name="DDA">
					<xsl:value-of select="@DDA"/>
				</xsl:attribute>
			</xsl:if>
      <xsl:attribute name="DataRetentionDays">
        <xsl:value-of select="@DataRetentionDays"/>
      </xsl:attribute>
      <xsl:attribute name="ImageRetentionDays">
        <xsl:value-of select="@ImageRetentionDays"/>
      </xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnsID">
				<xsl:value-of select="@DataEntryColumnsID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns/DataEntryColumn">
		<xsl:copy>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnID">
				<xsl:value-of select="@DataEntryColumnID"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="ScreenOrder">
				<xsl:value-of select="@ScreenOrder"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@MarkSense">
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="@MarkSense"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="MarkSense">
						<xsl:value-of select="0"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="DisplayGroup">
				<xsl:value-of select="@DisplayGroup"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayName">
				<xsl:value-of select="@DisplayName"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/ImageRPSAliasMappings">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ExtractType">
				<xsl:value-of select="@ExtractType"/>
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="@DocType"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="FieldType">
				<xsl:value-of select="@FieldType"/>
			</xsl:attribute>
			<xsl:attribute name="AliasName">
				<xsl:value-of select="@AliasName"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='2.01.02.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
      <xsl:attribute name="XSDVersion">
        <xsl:value-of select="@XSDVersion"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="../@FileHash">
        <xsl:attribute name="FileHash">
          <xsl:value-of select="../@FileHash"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="../@FileSignature">
        <xsl:attribute name="FileSignature">
          <xsl:value-of select="../@FileSignature"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentType">
				<xsl:value-of select="@PaymentType"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ABA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="DDA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
      <xsl:if test="@TransactionHash">
        <xsl:attribute name="TransactionHash">
				  <xsl:value-of select="@TransactionHash"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:if test="@TransactionSignature">
        <xsl:attribute name="TransactionSignature">
				  <xsl:value-of select="@TransactionSignature"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@RT">
					<xsl:attribute name="RT">
						<xsl:value-of select="@RT"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="RT">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@Account">
					<xsl:attribute name="Account">
						<xsl:value-of select="@Account"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Account">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:if test="@CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RawDataRecord/RawData">
		<xsl:element name="PaymentRawData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RawSequence">
				<xsl:value-of select="@RawSequence"/>
			</xsl:attribute>
			<xsl:attribute name="RawDataPart">
				<xsl:value-of select="@RawDataPart"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--WI 288728 
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='2.02.04.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
      <xs:maxLength value="36"/>
      <xs:minLength value="36"/>
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Batches">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="Batch"  maxOccurs="500000" minOccurs="0">
          <xs:complexType>
            <xs:sequence>
              <xs:element maxOccurs="1" minOccurs="0" name="BatchDataRecord">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element  maxOccurs="500000" minOccurs="0" name="BatchData">
                      <xs:complexType>
                        <xs:attribute name="FieldName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:minLength value="1"/>
                              <xs:maxLength value="32"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="FieldValue" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="256"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Transaction"  maxOccurs="500000" minOccurs="0">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element name="Payment"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:all>
                          <xs:element name="Images"  maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                           <xs:element name="RawDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RawData">
                            <xs:complexType>
                              <xs:simpleContent>
                                <xs:extension base="xs:string">
                                  <xs:attribute name="RawSequence" type="xs:int" use="required" />
                                  <xs:attribute name="RawDataPart" use="required">
                                    <xs:simpleType>
                                      <xs:restriction base="xs:int">
                                      </xs:restriction>
                                    </xs:simpleType>
                                  </xs:attribute>
                                </xs:extension>
                              </xs:simpleContent>
                            </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:all>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="Amount" type="xs:decimal" use="required" />
                        <xs:attribute name="CheckSequence" type="xs:int"  />
                        <xs:attribute name="Serial" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RemitterName" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="60"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="RT" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="Account" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                        <xs:attribute name="TransactionCode" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
						<xs:attribute name="ABA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:maxLength value="9" />
								</xs:restriction>
							</xs:simpleType>
						</xs:attribute>
						<xs:attribute name="DDA" use="required">
							<xs:simpleType>
								<xs:restriction base="xs:string">
									<xs:minLength value="1"/>
									<xs:maxLength value="35" />
								</xs:restriction>
								</xs:simpleType>
						</xs:attribute>
						</xs:complexType>
                    </xs:element>
                    <xs:element name="Document"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="Images" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
								<xs:element maxOccurs="6" minOccurs="0" name="img">
                                  <xs:complexType>
                                    <xs:attribute name="ClientFilePath" use="required" />
                                    <xs:attribute name="Page" use="required" />
                                    <xs:attribute name="ColorMode" use="required" />
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                            </xs:complexType>
                          </xs:element>
						  <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                        <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                        <xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
                        <xs:attribute name="DocumentDescriptor" use="required" >
                          <xs:simpleType>
                            <xs:restriction base="xs:string">
                              <xs:maxLength value="30"/>
                            </xs:restriction>
                          </xs:simpleType>
                        </xs:attribute>
                      </xs:complexType>
                    </xs:element>
                    <xs:element name="GhostDocument"  maxOccurs="500000" minOccurs="0">
                      <xs:complexType>
                        <xs:sequence>
                          <xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                          <xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
                            <xs:complexType>
                              <xs:sequence>
                                <xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
                                  <xs:complexType>
                                    <xs:attribute name="FieldName" use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:minLength value="1"/>
                                          <xs:maxLength value="32"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                    <xs:attribute name="FieldValue"  use="required" >
                                      <xs:simpleType>
                                        <xs:restriction base="xs:string">
                                          <xs:maxLength value="256"/>
                                        </xs:restriction>
                                      </xs:simpleType>
                                    </xs:attribute>
                                  </xs:complexType>
                                </xs:element>
                              </xs:sequence>
                              <xs:attribute name="BatchSequence" type="xs:int" use="required" />
                            </xs:complexType>
                          </xs:element>
                        </xs:sequence>
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                  <xs:attribute name="TransactionID" type="xs:int" use="required" />
                  <xs:attribute name="TransactionSequence" type="xs:int" use="required" />
				  <xs:attribute name="TransactionHash">
					<xs:simpleType>
					  <xs:restriction base="xs:string">
						<xs:minLength value="40"/>
						<xs:maxLength value="40"/>
					  </xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
				  <xs:attribute name="TransactionSignature">
					<xs:simpleType>
						<xs:restriction base="xs:string">
						<xs:minLength value="1"/>
						<xs:maxLength value="65"/>
						</xs:restriction>
					</xs:simpleType>
				  </xs:attribute>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="ProcessingDate" type="xs:date" use="required" />
            <xs:attribute name="DepositDate" type="xs:date" use="required" />
            <xs:attribute name="BankID" type="xs:int" use="required" />
            <xs:attribute name="ClientID" type="xs:int" use="required" />
            <xs:attribute name="BatchCueID" type="xs:string" use="optional" />
            <xs:attribute name="BatchID" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:long">
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchDate" type="xs:date" use="required" />
            <xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
            <xs:attribute name="BatchNumber" type="xs:int" use="optional" />
            <xs:attribute name="PaymentType" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchSource" use="required">
              <xs:simpleType>
                <xs:restriction base="xs:string">
                  <xs:minLength value="1" />
                  <xs:maxLength value="30" />
                </xs:restriction>
              </xs:simpleType>
            </xs:attribute>
            <xs:attribute name="BatchTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1"/>
            <xs:maxLength value="12"/>
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
 	<xs:attribute name="FileHash">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="40"/>
			<xs:maxLength value="40"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	<xs:attribute name="FileSignature">
		<xs:simpleType>
		  <xs:restriction base="xs:string">
			<xs:minLength value="1"/>
			<xs:maxLength value="55"/>
		  </xs:restriction>
		</xs:simpleType>
	</xs:attribute>
	</xs:complexType>
</xs:element>
</xs:schema>';
--WFSScriptProcessorStaticDataCreateEnd


--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='2.02.04.00', @parmXSD=
'
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
      <xsl:attribute name="XSDVersion">
        <xsl:value-of select="@XSDVersion"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="../@FileHash">
        <xsl:attribute name="FileHash">
          <xsl:value-of select="../@FileHash"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="../@FileSignature">
        <xsl:attribute name="FileSignature">
          <xsl:value-of select="../@FileSignature"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentType">
				<xsl:value-of select="@PaymentType"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ABA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="DDA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
      <xsl:if test="@TransactionHash">
        <xsl:attribute name="TransactionHash">
				  <xsl:value-of select="@TransactionHash"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:if test="@TransactionSignature">
        <xsl:attribute name="TransactionSignature">
				  <xsl:value-of select="@TransactionSignature"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@RT">
					<xsl:attribute name="RT">
						<xsl:value-of select="@RT"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="RT">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@Account">
					<xsl:attribute name="Account">
						<xsl:value-of select="@Account"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Account">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:if test="@CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RawDataRecord/RawData">
		<xsl:element name="PaymentRawData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RawSequence">
				<xsl:value-of select="@RawSequence"/>
			</xsl:attribute>
			<xsl:attribute name="RawDataPart">
				<xsl:value-of select="@RawDataPart"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/ItemDataRecord/ItemData">
		<xsl:element name="GhostDocumentItemData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ItemDataDataRecord_Id">
				<xsl:value-of select="count(preceding::ItemDataDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>';
--WFSScriptProcessorStaticDataCreateEnd





--WFSScriptProcessorStaticDataCreateBegin
--PT 127613917
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data', @parmXSDVersion='2.02.07.00', @parmXSD=
'<?xml version="1.0" encoding="utf-8"?>
<xs:schema attributeFormDefault="unqualified" elementFormDefault="qualified" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:simpleType name="GUID">
		<xs:annotation>
		<xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
		<xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}"/>
		<xs:maxLength value="36"/>
		<xs:minLength value="36"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="Batches">
		<xs:complexType>
		<xs:sequence>
			<xs:element name="Batch"  maxOccurs="500000" minOccurs="0">
				<xs:complexType>
				<xs:sequence>
					<xs:element maxOccurs="1" minOccurs="0" name="BatchDataRecord">
						<xs:complexType>
							<xs:sequence>
								<xs:element  maxOccurs="500000" minOccurs="0" name="BatchData">
									<xs:complexType>
										<xs:attribute name="FieldName" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:minLength value="1"/>
													<xs:maxLength value="32"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="FieldValue" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="256"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
									</xs:complexType>
								</xs:element>
							</xs:sequence>
						</xs:complexType>
					</xs:element>
					<xs:element name="Transaction"  maxOccurs="500000" minOccurs="0">
						<xs:complexType>
							<xs:choice maxOccurs="unbounded">
								<xs:element name="Payment"  maxOccurs="500000" minOccurs="0">
									<xs:complexType>
										<xs:all>
											<xs:element name="Images"  maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element maxOccurs="6" minOccurs="0" name="img">
															<xs:complexType>
																<xs:attribute name="ClientFilePath" use="required" />
																<xs:attribute name="Page" use="required" />
																<xs:attribute name="ColorMode" use="required" />
																<xs:attribute name="AllowMultiPageTiff" use="optional" type="xs:boolean" />
															</xs:complexType>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="RemittanceDataRecord" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
												<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
											<xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue"  use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
											<xs:element name="RawDataRecord" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000" minOccurs="0" name="RawData">
															<xs:complexType>
																<xs:simpleContent>
																	<xs:extension base="xs:string">
																		<xs:attribute name="RawSequence" type="xs:int" use="required" />
																		<xs:attribute name="RawDataPart" use="required">
																			<xs:simpleType>
																				<xs:restriction base="xs:int">
																				</xs:restriction>
																			</xs:simpleType>
																		</xs:attribute>
																	</xs:extension>
																</xs:simpleContent>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
										</xs:all>
										<xs:attribute name="BatchSequence" type="xs:int" use="required" />
										<xs:attribute name="Amount" type="xs:decimal" use="required" />
										<xs:attribute name="CheckSequence" type="xs:int"  />
										<xs:attribute name="Serial" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="30"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="RemitterName" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="60"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="RT" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="30"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="Account" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="30"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="TransactionCode" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="30"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="ABA" use="required">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="9" />
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="DDA" use="required">
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:minLength value="1"/>
													<xs:maxLength value="35" />
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
									</xs:complexType>
								</xs:element>
								<xs:element name="Document"  maxOccurs="500000" minOccurs="0">
									<xs:complexType>
										<xs:sequence>
											<xs:element name="Images" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element maxOccurs="6" minOccurs="0" name="img">
															<xs:complexType>
																<xs:attribute name="ClientFilePath" use="required" />
																<xs:attribute name="Page" use="required" />
																<xs:attribute name="ColorMode" use="required" />
																<xs:attribute name="AllowMultiPageTiff" use="optional" type="xs:boolean" />
															</xs:complexType>
														</xs:element>
													</xs:sequence>
												</xs:complexType>
											</xs:element>
											<xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
											<xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue"  use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
										</xs:sequence>
										<xs:attribute name="BatchSequence" type="xs:int" use="required" />
										<xs:attribute name="SequenceWithinTransaction" use="required" type="xs:int"  />
										<xs:attribute name="DocumentSequence" use="required" type="xs:int"  />
										<xs:attribute name="DocumentDescriptor" use="required" >
											<xs:simpleType>
												<xs:restriction base="xs:string">
													<xs:maxLength value="30"/>
												</xs:restriction>
											</xs:simpleType>
										</xs:attribute>
										<xs:attribute name="IsCorrespondence" use="optional" type="xs:boolean"  />
									</xs:complexType>
								</xs:element>
								<xs:element name="GhostDocument"  maxOccurs="500000" minOccurs="0">
									<xs:complexType>
										<xs:sequence>
											<xs:element name="RemittanceDataRecord" maxOccurs="500000" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000" minOccurs="0" name="RemittanceData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
											<xs:element name="ItemDataRecord" maxOccurs="1" minOccurs="0">
												<xs:complexType>
													<xs:sequence>
														<xs:element  maxOccurs="500000"  minOccurs="0"  name="ItemData">
															<xs:complexType>
																<xs:attribute name="FieldName" use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:minLength value="1"/>
																			<xs:maxLength value="32"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
																<xs:attribute name="FieldValue"  use="required" >
																	<xs:simpleType>
																		<xs:restriction base="xs:string">
																			<xs:maxLength value="256"/>
																		</xs:restriction>
																	</xs:simpleType>
																</xs:attribute>
															</xs:complexType>
														</xs:element>
													</xs:sequence>
													<xs:attribute name="BatchSequence" type="xs:int" use="required" />
												</xs:complexType>
											</xs:element>
										</xs:sequence>
										<xs:attribute name="IsCorrespondence" use="optional" type="xs:boolean"  />
									</xs:complexType>
								</xs:element>
							</xs:choice>
							<xs:attribute name="TransactionID" type="xs:int" use="required" />
							<xs:attribute name="TransactionSequence" type="xs:int" use="required" />
							<xs:attribute name="TransactionHash">
								<xs:simpleType>
									<xs:restriction base="xs:string">
										<xs:minLength value="40"/>
										<xs:maxLength value="40"/>
									</xs:restriction>
								</xs:simpleType>
							</xs:attribute>
							<xs:attribute name="TransactionSignature">
								<xs:simpleType>
									<xs:restriction base="xs:string">
										<xs:minLength value="1"/>
										<xs:maxLength value="65"/>
									</xs:restriction>
								</xs:simpleType>
							</xs:attribute>
						</xs:complexType>
					</xs:element>
				</xs:sequence>
				<xs:attribute name="ProcessingDate" type="xs:date" use="required" />
				<xs:attribute name="DepositDate" type="xs:date" use="required" />
				<xs:attribute name="BankID" type="xs:int" use="required" />
				<xs:attribute name="ClientID" type="xs:int" use="required" />
				<xs:attribute name="BatchCueID" type="xs:string" use="optional" />
				<xs:attribute name="BatchID" use="required">
					<xs:simpleType>
						<xs:restriction base="xs:long">
						</xs:restriction>
					</xs:simpleType>
				</xs:attribute>
				<xs:attribute name="BatchDate" type="xs:date" use="required" />
				<xs:attribute name="BatchSiteCode" type="xs:int" use="required" />
				<xs:attribute name="BatchNumber" type="xs:int" use="optional" />
				<xs:attribute name="PaymentType" use="required">
					<xs:simpleType>
						<xs:restriction base="xs:string">
							<xs:minLength value="1" />
							<xs:maxLength value="30" />
						</xs:restriction>
					</xs:simpleType>
				</xs:attribute>
				<xs:attribute name="BatchSource" use="required">
					<xs:simpleType>
						<xs:restriction base="xs:string">
							<xs:minLength value="1" />
							<xs:maxLength value="30" />
						</xs:restriction>
					</xs:simpleType>
				</xs:attribute>
				<xs:attribute name="BatchTrackingID" type="GUID" use="required" />
				</xs:complexType>
			</xs:element>
		</xs:sequence>
		<xs:attribute name="SourceTrackingID" type="GUID" use="required" />
		<xs:attribute name="ClientProcessCode" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:maxLength value="40"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="XSDVersion" use="required">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="12"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="FileHash">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="40"/>
					<xs:maxLength value="40"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		<xs:attribute name="FileSignature">
			<xs:simpleType>
				<xs:restriction base="xs:string">
					<xs:minLength value="1"/>
					<xs:maxLength value="55"/>
				</xs:restriction>
			</xs:simpleType>
		</xs:attribute>
		</xs:complexType>
	</xs:element>
</xs:schema>';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Data Import Integration Services for Batch Data XSL', @parmXSDVersion='2.02.07.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ClientProcessCode">
					<xsl:attribute name="ClientProcessCode">
						<xsl:value-of select="@ClientProcessCode"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentType">
				<xsl:value-of select="@PaymentType"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="ABA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="DDA">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
			<xsl:if test="@TransactionHash">
				<xsl:attribute name="TransactionHash">
					<xsl:value-of select="@TransactionHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@TransactionSignature">
				<xsl:attribute name="TransactionSignature">
					<xsl:value-of select="@TransactionSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@RT">
					<xsl:attribute name="RT">
						<xsl:value-of select="@RT"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="RT">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@Account">
					<xsl:attribute name="Account">
						<xsl:value-of select="@Account"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Account">
						<xsl:value-of select="''''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:if test="@CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RawDataRecord/RawData">
		<xsl:element name="PaymentRawData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RawSequence">
				<xsl:value-of select="@RawSequence"/>
			</xsl:attribute>
			<xsl:attribute name="RawDataPart">
				<xsl:value-of select="@RawDataPart"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@IsCorrespondence">
					<xsl:attribute name="IsCorrespondence">
						<xsl:value-of select="@IsCorrespondence"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="IsCorrespondence">0</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="RemittanceDataRecord/@BatchSequence">
					<xsl:attribute name="BatchSequence">
						<xsl:value-of select="RemittanceDataRecord/@BatchSequence"/>
					</xsl:attribute>		
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchSequence">
						<xsl:value-of select="ItemDataRecord/@BatchSequence"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@IsCorrespondence">
					<xsl:attribute name="IsCorrespondence">
						<xsl:value-of select="@IsCorrespondence"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="IsCorrespondence">0</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/ItemDataRecord/ItemData">
		<xsl:element name="GhostDocumentItemData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ItemDataDataRecord_Id">
				<xsl:value-of select="count(preceding::ItemDataDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>';
--WFSScriptProcessorStaticDataCreateEnd

--PT 154233497
--WFSScriptProcessorStaticDataCreateBegin
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Notification Import Integration Services', @parmXSDVersion='2.03.02.00', @parmXSD=
'<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" attributeFormDefault="unqualified" elementFormDefault="qualified">
  <xs:simpleType name="GUID">
    <xs:annotation>
      <xs:documentation xml:lang="en">The representation of a GUID, generally the id of an element.</xs:documentation>
    </xs:annotation>
    <xs:restriction base="xs:string">
      <xs:pattern value="[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}" />
      <xs:maxLength value="36" />
      <xs:minLength value="36" />
    </xs:restriction>
  </xs:simpleType>
  <xs:element name="Notifications">
    <xs:complexType>
      <xs:sequence>
        <xs:element maxOccurs="unbounded" name="Notification">
          <xs:complexType>
            <xs:sequence>
              <xs:element name="MessageParts" minOccurs="0" maxOccurs="1">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs="0" maxOccurs="unbounded" name="MessagePart" type="xs:string" />
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
              <xs:element name="Files">
                <xs:complexType>
                  <xs:sequence>
                    <xs:element minOccurs="0" maxOccurs="unbounded" name="File">
                      <xs:complexType>
                        <xs:attribute name="FileIdentifier" type="xs:string" use="required" />
                        <xs:attribute name="ClientFilePath" type="xs:string" use="required" />
                        <xs:attribute name="FileType" type="xs:string" use="required" />
                        <xs:attribute name="UserFileName" type="xs:string" use="required" />
                        <xs:attribute name="FileExtension" type="xs:string" use="required" />
						<xs:attribute name="FileSize" type="xs:long" use="required" />
                      </xs:complexType>
                    </xs:element>
                  </xs:sequence>
                </xs:complexType>
              </xs:element>
            </xs:sequence>
            <xs:attribute name="BankID" type="xs:integer" use="required" />
            <xs:attribute name="ClientGroupID" type="xs:integer" use="required" />
            <xs:attribute name="ClientID" type="xs:integer" use="required" />
            <xs:attribute name="NotificationDate" type="xs:string" use="required" />
            <xs:attribute name="SourceNotificationID" type="GUID" use="optional" />
            <xs:attribute name="NotificationSourceKey" type="xs:integer" use="required" />
            <xs:attribute name="NotificationTrackingID" type="GUID" use="required" />
          </xs:complexType>
        </xs:element>
      </xs:sequence>
      <xs:attribute name="SourceTrackingID" type="GUID" use="required" />
      <xs:attribute name="ClientProcessCode" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:maxLength value="40" />
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
      <xs:attribute name="XSDVersion" use="required">
        <xs:simpleType>
          <xs:restriction base="xs:string">
            <xs:minLength value="1" />
            <xs:maxLength value="12" />
          </xs:restriction>
        </xs:simpleType>
      </xs:attribute>
	  <xs:attribute name="MessageWorkgroups" type="GUID" use="optional" />
    </xs:complexType>
  </xs:element>
</xs:schema>
';
--WFSScriptProcessorStaticDataCreateEnd

--WFSScriptProcessorStaticDataCreateBegin
--CR 53534
EXEC RecHubSystem.usp_CreateSchemaRevision @parmSchemaType='Notification Import Integration Services XSL', @parmXSDVersion='2.03.02.00', @parmXSD=
'<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Notifications">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationDate">
				<xsl:value-of select="@NotificationDate"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationTrackingID">
				<xsl:value-of select="@NotificationTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="NotificationSourceKey">
				<xsl:value-of select="@NotificationSourceKey"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@SourceNotificationID">
					<xsl:attribute name="SourceNotificationID">
						<xsl:value-of select="@SourceNotificationID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="SourceNotificationID">00000000-0000-0000-0000-000000000000</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@MessageWorkgroups">
				<xsl:attribute name="MessageWorkgroups">
					<xsl:value-of select="../@MessageWorkgroups"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/MessageParts">
    <xsl:if test="normalize-space(.) != ''''">
      <xsl:copy>
        <xsl:attribute name="MessageParts_Id">
          <xsl:value-of select="count(preceding::MessageParts)+1"/>
        </xsl:attribute>
        <xsl:apply-templates select="node()"/>
      </xsl:copy>
    </xsl:if>
	</xsl:template>
	<xsl:template match="Notifications/Notification/MessageParts/MessagePart">
		<xsl:copy>
			<xsl:attribute name="NotificationMessagePart">
				<xsl:value-of select="count(preceding-sibling::MessagePart)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/Files">
		<xsl:copy>
			<xsl:attribute name="Files_Id">
				<xsl:value-of select="count(preceding::Files)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Notifications/Notification/Files/File">
		<xsl:copy>
			<xsl:attribute name="File_Id">
				<xsl:value-of select="count(preceding::File)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FileType">
				<xsl:value-of select="@FileType"/>
			</xsl:attribute>
			<xsl:attribute name="UserFileName">
				<xsl:value-of select="@UserFileName"/>
			</xsl:attribute>
			<xsl:attribute name="FileExtension">
				<xsl:value-of select="@FileExtension"/>
			</xsl:attribute>
			<xsl:attribute name="FileIdentifier">
				<xsl:value-of select="@FileIdentifier"/>
			</xsl:attribute>
			<xsl:attribute name="FileSize">
			  <xsl:value-of select="@FileSize"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
';
--WFSScriptProcessorStaticDataCreateEnd
