--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 03/28/2017
*
* Purpose: To add PartitionManager and PartitionDisk to an existing Database for Session tables partitions
*				
* Caution: You must either edit this script to fill in the correct path for the PartitionDisk or edit the table after running this script
* Caution: Be sure that the folder specified is created on the disk
* Caution: Backup the database before beginning this process
* Caution: Be sure to connect to the database you are creating partitions for prior to executing this script
*
* Modification History
* 03/28/2017 PT 141435715	MGE		Created
******************************************************************************/

IF NOT EXISTS (SELECT 1 FROM RecHubSystem.PartitionManager WHERE PartitionIdentifier = 'Sessions')
BEGIN
	RAISERROR ('Adding PartitionManager Sessions', 10, 1) WITH NOWAIT
	INSERT INTO RecHubSystem.PartitionManager
	(
	PartitionManagerID,
	PartitionIdentifier,
	PartitionFunctionName,
	PartitionSchemeName,
	SizeMB,
	RangeSetting,
	LastDiskID,
	IsLocked
	)
	VALUES 
	(5,'Sessions','Sessions_PartitionFunction','Sessions',1,1,1,0)
	RAISERROR ('PartitionManager Added for Sessions', 10, 1) WITH NOWAIT
END
ELSE
	BEGIN
	RAISERROR ('Sessions PartitionManager already exists.  No rows added.', 10, 1) WITH NOWAIT
	END


IF NOT EXISTS (SELECT 1 FROM RecHubSystem.PartitionDisks 
		WHERE PartitionManagerID = (Select PartitionManagerID FROM RecHubSystem.PartitionDisks WHERE PartitionManagerID = 5))
BEGIN
	RAISERROR ('Adding PartitionDisks for Sessions', 10, 1) WITH NOWAIT;
	INSERT INTO RecHubSystem.PartitionDisks
	(PartitionManagerID,
	DiskOrder,
	DiskPath
	)
	VALUES
	(5,
	1,
	'Path to Sessions Disks');   -- EDIT this value to be the actual path of the partitions
	RAISERROR ('PartitionDisk Added for Sessions - Be sure to edit the path!!!', 10, 1) WITH NOWAIT;
END
ELSE
	BEGIN
	RAISERROR ('Sessions PartitionDisks already exists.  No rows added.', 10, 1) WITH NOWAIT
	END