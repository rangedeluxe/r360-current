--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/12/13
*
* Purpose: Create the RecHubData Schema
*
* Modification History
* 07/12/13 WI 108875 JPB Created
******************************************************************************/
SET NOCOUNT ON;
DECLARE @BeginDate DATETIME,
		@EndDate DATETIME;


SELECT	@BeginDate='$(PartitionStartDate)',
		@EndDate=DATEADD(year,$(PartitionYears),@BeginDate);

PRINT 'Creating Data Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubFacts', @BeginDate, @EndDate;

PRINT 'Creating Notification Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubNotifications', @BeginDate, @EndDate;

PRINT 'Creating Audit Partitions for date range: ' + CONVERT(VARCHAR, @BeginDate, 101) + ' to ' + CONVERT(VARCHAR, @EndDate, 101);
EXEC RecHubSystem.usp_CreatePartitionsForRange 'RecHubAudits', @BeginDate, @EndDate;
