--WFSScriptProcessorDoNotFormat
--WFSScriptProcessorStaticDataName Partition Manager Settings
--WFSScriptPRocessorStaticDataCreateBeginPRINT 'Inserting partition manager info'
SET NOCOUNT ON
IF NOT EXISTS ( SELECT 1 FROM RecHubSystem.PartitionManager WHERE PartitionIdentifier = 'RecHubFacts' ) 
	INSERT INTO RecHubSystem.PartitionManager (PartitionManagerID,PartitionIdentifier, PartitionFunctionName, PartitionSchemeName, SizeMB, RangeSetting)
	VALUES(1,'RecHubFacts', 'DepositDate_Partition_Range', 'RecHubFacts', 1, $(PartitionType))
IF NOT EXISTS ( SELECT 1 FROM RecHubSystem.PartitionManager WHERE PartitionIdentifier = 'RecHubNotifications' ) 
	INSERT INTO RecHubSystem.PartitionManager (PartitionManagerID,PartitionIdentifier, PartitionFunctionName, PartitionSchemeName, SizeMB, RangeSetting)
	VALUES(2,'RecHubNotifications', 'NotificationDate_Partition_Range', 'RecHubNotifications', 1, $(PartitionType))
IF NOT EXISTS ( SELECT 1 FROM RecHubSystem.PartitionManager WHERE PartitionIdentifier = 'RecHubAudits' ) 
	INSERT INTO RecHubSystem.PartitionManager (PartitionManagerID,PartitionIdentifier, PartitionFunctionName, PartitionSchemeName, SizeMB, RangeSetting)
	VALUES(3,'RecHubAudits', 'AuditDate_Partition_Range', 'RecHubAudits', 1, $(PartitionType))
SET NOCOUNT OFF
--WFSScriptPRocessorStaticDataCreateEnd
