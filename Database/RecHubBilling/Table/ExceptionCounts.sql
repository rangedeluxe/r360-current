--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName ExceptionCounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Holds daily accumulation volumes / counters based on the 
*			RecHubException.* tables
*
* Modification History
* 08/22/2014 WI 160838 RDS	Created 
* 09/02/2014 WI 160838 RDS	Renamed, include DDA, count items, etc.
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.ExceptionCounts
(
	DepositDateKey			INT NOT NULL,
	SiteBankID				INT NOT NULL,
	WorkgroupID				INT NOT NULL,
	DDAKey					INT NOT NULL,
	ExceptionType			TINYINT NOT NULL,
	TransactionCount		INT NOT NULL,
	AcceptedTransactions	INT NOT NULL,
	AcceptedNoChanges		INT NOT NULL,
	RejectedTransactions	INT NOT NULL,
	PaymentItemCount		INT NOT NULL,
	StubItemCount			INT NOT NULL,
	UpdatedItemCount		INT NOT NULL,
	AddedItemCount			INT NOT NULL,
	DeletedItemCount		INT NOT NULL,
	
	CONSTRAINT PK_RecHubBilling_ExceptionCounts PRIMARY KEY CLUSTERED (DepositDateKey, SiteBankID, WorkgroupID, DDAKey, ExceptionType)
);
--WFSScriptProcessorTableProperties