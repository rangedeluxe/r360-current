--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName ExceptionCountsAuditMonitor
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Holds indicator of max row value processed, to verify status
*
* Modification History
* 09/15/2014 WI 171574 RDS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.ExceptionCountsAuditMonitor
(
	MaxEventAuditMessageKey	BIGINT NOT NULL
);
--WFSScriptProcessorTableProperties