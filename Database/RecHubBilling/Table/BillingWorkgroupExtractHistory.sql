--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName BillingWorkgroupExtractHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 05/02/2017
*
* Purpose: Grain: one row for every Batch. This is to record what Billing has been extracted so we do not extract more than once.
*		   
*
* Defaults:
*	ModificationDate = GETDATE()
*
*
* Modification History
* 05/02/2016 PT #142166535 TWE	Created 
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.BillingWorkgroupExtractHistory
(
	BillingWorkgroupExtractHistoryKey BIGINT IDENTITY(1,1),
	DateKey INT NOT NULL,
	BankID INT NOT NULL,
	ClientAccountID INT NOT NULL,	
	TableUsed varchar(256) NOT NULL,
	TableKey BIGINT NOT NULL		
) ON RecHubFacts(DateKey) ;
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubBilling.BillingWorkgroupExtractHistory ADD 
	CONSTRAINT PK_BillingWorkgroupExtractHistory PRIMARY KEY NONCLUSTERED (BillingWorkgroupExtractHistoryKey,DateKey)

--WFSScriptProcessorIndex RecHubBilling.BillingWorkgroupExtractHistory.IDX_BillingWorkgroupExtractHistory_DateBillingWorkgroupExtractHistoryKey
CREATE CLUSTERED INDEX IDX_BillingWorkgroupExtractHistory_DateBillingWorkgroupExtractHistoryKey ON RecHubBilling.BillingWorkgroupExtractHistory 
(
    DateKey,
	BillingWorkgroupExtractHistoryKey
) ON RecHubFacts(DateKey) ;
