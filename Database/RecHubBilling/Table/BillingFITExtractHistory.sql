--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName BillingFITExtractHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 05/16/2017
*
* Purpose: Grain: one row for every Notification. This is to record what Billing has been extracted so we do not extract more than once.
*		   
*
* Defaults:
*	ModificationDate = GETDATE()
*
*
* Modification History
* 05/16/2016 PT #139494807 MGE	Created 
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.BillingFITExtractHistory
(
	BillingFITExtractHistoryKey BIGINT IDENTITY(1,1),
	DateKey INT NOT NULL,
	BankID INT NOT NULL,
	ClientAccountID INT NOT NULL,	
	TableUsed varchar(256) NOT NULL,
	TableKey BIGINT NOT NULL,
	FileList VARCHAR(2048) NULL	
) ON RecHubFacts(DateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubBilling.BillingFITExtractHistory ADD 
	CONSTRAINT PK_BillingFITExtractHistory PRIMARY KEY NONCLUSTERED (BillingFITExtractHistoryKey,DateKey)

--WFSScriptProcessorIndex RecHubBilling.BillingFITExtractHistory.IDX_BillingFITExtractHistory_DateBillingFITExtractHistoryKey
CREATE CLUSTERED INDEX IDX_BillingFITExtractHistory_DateBillingFITExtractHistoryKey ON RecHubBilling.BillingFITExtractHistory 
(
    DateKey,
	BillingFITExtractHistoryKey
) ON RecHubFacts(DateKey);
