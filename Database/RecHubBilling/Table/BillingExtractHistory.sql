--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName BillingExtractHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 06/13/2016
*
* Purpose: Grain: one row for every Batch. This is to record what Billing has been extracted so we do not extract more than once.
*		   
*
* Defaults:
*	ModificationDate = GETDATE()
*
*
* Modification History
* 06/13/2016 WI 285728 JBS	Created 
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.BillingExtractHistory
(
	BillingExtractHistoryKey BIGINT IDENTITY(1,1),
	BankID INT NOT NULL,
	ClientAccountID INT NOT NULL,
	DepositDateKey INT NOT NULL, 
	ImmutableDateKey INT NOT NULL, 
	SourceBatchID BIGINT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_BillingExtractHistory_CreationDate DEFAULT(GETDATE())
) $(OnDataPartition) ;
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubBilling.BillingExtractHistory ADD 
	CONSTRAINT PK_BillingExtractHistory PRIMARY KEY NONCLUSTERED (BillingExtractHistoryKey,DepositDateKey),
	CONSTRAINT FK_BillingExtractHistory_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_BillingExtractHistory_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_BillingExtractHistory_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey)

--WFSScriptProcessorIndex RecHubBilling.BillingExtractHistory.IDX_BillingExtractHistory_DepositDateBillingExtractHistoryKey
CREATE CLUSTERED INDEX IDX_BillingExtractHistory_DepositDateBillingExtractHistoryKey ON RecHubBilling.BillingExtractHistory 
(
	DepositDateKey,
	BillingExtractHistoryKey
) $(OnDataPartition) ;
