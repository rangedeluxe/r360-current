--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName ActivityCounts_21_SearchResultsPrintView
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Holds daily accumulation volumes / counters based on the 
*			RecHubUser.SessionActivityLog table	for bulk file download activity
*
* Modification History
* 09/15/2014 WI 171561 RDS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.ActivityCounts_21_SearchResultsPrintView
(
	ProcessingDateKey		INT NOT NULL,
	LogonEntityID			INT NOT NULL,
	SiteBankID				INT NOT NULL,
	WorkgroupID				INT NOT NULL,
	FileCount				INT NOT NULL,
	ItemCount				INT NOT NULL,
	
	CONSTRAINT PK_RecHubBilling_ActivityCounts_21_SearchResultsPrintView PRIMARY KEY CLUSTERED (ProcessingDateKey, LogonEntityID, SiteBankID, WorkgroupID)
);
--WFSScriptProcessorTableProperties