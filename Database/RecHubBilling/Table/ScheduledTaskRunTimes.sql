--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName ScheduledTaskRunTimes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Database-based tracking of task run timestamps for multi-server environments
*
* Modification History
* 08/22/2014 WI 160803 RDS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.ScheduledTaskRunTimes
(
	TaskDescription			VARCHAR(50) NOT NULL,
	LastRunBy				VARCHAR(128) NOT NULL,
	LastRunAt				DATETIME NOT NULL
	
	CONSTRAINT PK_RecHubBilling_ScheduledTaskRunTimes PRIMARY KEY CLUSTERED (TaskDescription)
);
--WFSScriptProcessorTableProperties