--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName DocumentCounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Holds daily accumulation volumes / counters based on the 
*			RecHubData.factDocuments table
*
* Modification History
* 08/22/2014 WI 160837 RDS	Created 
* 09/02/2014 WI 160837 RDS	Renamed, include DDA
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.DocumentCounts
(
	DepositDateKey			INT NOT NULL,
	SiteBankID				INT NOT NULL,
	WorkgroupID				INT NOT NULL,
	BatchSourceKey			INT NOT NULL,
	DDAKey					INT NOT NULL,
	ItemCount				INT NOT NULL,
	
	CONSTRAINT PK_RecHubBilling_DocumentCounts PRIMARY KEY CLUSTERED (DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey)
);
--WFSScriptProcessorTableProperties