--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorTableName WorkgroupFlags
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/22/2014
*
* Purpose: Holds monthly flag values related to workgroups
*
* Modification History
* 08/22/2014 WI 160849 RDS	Created 
* 09/02/2014 WI 160849 RDS	Renamed, added flag columns and check constraint
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubBilling.WorkgroupFlags
(
	DepositDateKey			INT NOT NULL CONSTRAINT CK_RecHubBilling_WorkgroupFlags_DepositDateKey CHECK (DepositDateKey % 100 = 1), -- Must be 1st of the month
	SiteBankID				INT NOT NULL,
	WorkgroupID				INT NOT NULL,
	Active					BIT NOT NULL,
	
	CONSTRAINT PK_RecHubBilling_WorkgroupFlags PRIMARY KEY CLUSTERED (DepositDateKey, SiteBankID, WorkgroupID)
);
--WFSScriptProcessorTableProperties