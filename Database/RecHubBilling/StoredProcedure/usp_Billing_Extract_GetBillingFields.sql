--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_GetBillingFields
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_GetBillingFields') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_GetBillingFields
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_GetBillingFields 
(
	@parmEntityId			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 09/11/2014
*
* Purpose: This procedure retrieves the billing fields associated with an OLEntity
*
* Modification History
* 01/15/2015 WI 184540 RDS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT BillingAccount, BillingField1, BillingField2
	FROM RecHubUser.OLEntities
	WHERE EntityID = @parmEntityId;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH