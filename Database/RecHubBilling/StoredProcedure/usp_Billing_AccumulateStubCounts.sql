--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulateStubCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulateStubCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulateStubCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulateStubCounts 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates factStub statistics and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 08/22/2014 WI 160889 RDS	Created
* 09/02/2014 WI 160889 RDS	Group by DDA, Update Table Names
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- Aggregate values for each date in the range of dates missing / being rechecked
DECLARE @targetFactKey BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	SET @targetDateKey = CAST(CONVERT(VARCHAR(8), @loopDate, 112) AS INT);
		
	-- Check for the last key we summarized
	SET @targetFactKey = ISNULL((SELECT MaxFactStubKey FROM RecHubBilling.StubCountsMonitor WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubData.factStubs WHERE DepositDateKey = @targetDateKey AND factStubKey > @targetFactKey)
	BEGIN
		-- Data has been updated; recalculate

		-- Delete existing counts
		DELETE RecHubBilling.StubCountsMonitor WHERE DepositDateKey = @targetDateKey;
		DELETE RecHubBilling.StubCounts WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetFactKey = ISNULL((SELECT TOP 1 factStubKey FROM RecHubData.factStubs WHERE DepositDateKey = @targetDateKey ORDER BY factStubKey DESC), 0);

		-- Store counts to the summary table
		-- TODO: Should factStubs be updated to include DDAKey, to avoid this (poorly performing) join to factChecks?
		-- Option 1: Join / Group By
		/*WITH CTE_factStubsWithDDA AS
		(
			SELECT RecHubData.factStubs.ClientAccountKey, RecHubData.factStubs.BatchSourceKey, MIN(RecHubData.factChecks.DDAKey) DDAKey, RecHubData.factStubs.factStubKey
			FROM RecHubData.factStubs
			LEFT JOIN RecHubData.factChecks
				ON RecHubData.factStubs.DepositDateKey = RecHubdata.factChecks.DepositDateKey
				AND RecHubData.factStubs.BatchID = RecHubData.factChecks.BatchID
				AND RecHubData.factStubs.TransactionID = RecHubData.factChecks.TransactionID
				AND RecHubData.factChecks.IsDeleted = 0
			WHERE RecHubData.factStubs.DepositDateKey = @targetDateKey
				AND RecHubData.factChecks.DepositDateKey = @targetDateKey
				AND RecHubData.factStubs.IsDeleted = 0
			GROUP BY RecHubData.factStubs.ClientAccountKey, RecHubData.factStubs.BatchSourceKey, 
					 RecHubData.factStubs.BatchID, RecHubData.factStubs.TransactionID, RecHubData.factStubs.factStubKey
		)
		INSERT INTO RecHubBilling.StubCounts(DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, ISNULL(DDAKey, 0), COUNT(*)
		FROM CTE_factStubsWithDDA
		INNER JOIN RecHubData.dimClientAccounts
			ON CTE_factStubsWithDDA.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;*/

		-- Option 2: Partition & Filter / Join
		WITH CTE_factCheckDDA AS
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY TransactionID ORDER BY TransactionID) Sequence, TransactionID, DDAKey
			FROM RecHubData.factChecks
			WHERE DepositDateKey = @targetDateKey
				AND IsDeleted = 0
		),
		CTE_factTransactionDDA AS
		(
			SELECT TransactionID, DDAKey
			FROM CTE_factCheckDDA
			WHERE Sequence = 1
		)
		INSERT INTO RecHubBilling.StubCounts(DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, ISNULL(DDAKey, 0), COUNT(*)
		FROM RecHubData.factStubs
		LEFT JOIN CTE_factTransactionDDA
			ON RecHubData.factStubs.TransactionID = CTE_factTransactionDDA.TransactionID
		INNER JOIN RecHubData.dimClientAccounts
			ON RecHubData.factStubs.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE RecHubData.factStubs.DepositDateKey = @targetDateKey
			AND RecHubData.factStubs.IsDeleted = 0
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.StubCountsMonitor(DepositDateKey, MaxFactStubKey) VALUES (@targetDateKey, @targetFactKey);
	END
	ELSE IF @targetFactKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.StubCountsMonitor(DepositDateKey, MaxFactStubKey) VALUES (@targetDateKey, 0);
	END
END