--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulatePaymentCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulatePaymentCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulatePaymentCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulatePaymentCounts 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates factCheck statistics and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 08/22/2014 WI 160888 RDS	Created
* 09/02/2014 WI 160888 RDS	Group by DDA, Update Table Names
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- Aggregate values for each date in the range of dates missing / being rechecked
DECLARE @targetFactKey BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	SET @targetDateKey = CAST(CONVERT(VARCHAR(8), @loopDate, 112) AS INT);
		
	-- Check for the last key we summarized
	SET @targetFactKey = ISNULL((SELECT MaxFactPaymentKey FROM RecHubBilling.PaymentCountsMonitor WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubData.factChecks WHERE DepositDateKey = @targetDateKey AND factCheckKey > @targetFactKey)
	BEGIN
		-- Data has been updated; recalculate

		-- Delete existing counts
		DELETE RecHubBilling.PaymentCountsMonitor WHERE DepositDateKey = @targetDateKey;
		DELETE RecHubBilling.PaymentCounts WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetFactKey = ISNULL((SELECT TOP 1 factCheckKey FROM RecHubData.factChecks WHERE DepositDateKey = @targetDateKey ORDER BY factCheckKey DESC), 0);

		-- Store counts to the summary table
		INSERT INTO RecHubBilling.PaymentCounts(DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey, COUNT(*)
		FROM RecHubData.factChecks
		INNER JOIN RecHubData.dimClientAccounts
			ON RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE DepositDateKey = @targetDateKey and IsDeleted = 0
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.PaymentCountsMonitor(DepositDateKey, MaxFactPaymentKey) VALUES (@targetDateKey, @targetFactKey);
	END
	ELSE IF @targetFactKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.PaymentCountsMonitor(DepositDateKey, MaxFactPaymentKey) VALUES (@targetDateKey, 0);
	END
END