﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_BillingExtract
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_factBatchSummary_Get_BillingExtract') IS NOT NULL
	   DROP PROCEDURE RecHubBilling.usp_factBatchSummary_Get_BillingExtract;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_factBatchSummary_Get_BillingExtract 
(
	@parmDepositDateStart	DATETIME,
	@parmDepositDateEnd		DATETIME,
	@parmRerun				BIT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	JBS
* Date:		06/09/2016
*
* Purpose: Query Batch Summary fact table for Billing Extract process for Date range supplied.
*
* Modification History
* 06/09/2016 WI 285690 JBS	Create 
* 06/27/2016 WI 289013 JBS	Add Bank and Workgroup Name
* 07/08/2016 WI 288617 JBS  Add EntityID from the Associative OLWorkGroups table for Lookup in SSIS package
* 08/23/2016 PT 127613859 JBS	Add new columns to output:  ViewingDays, MaximumSearchDays
* 05/16/2017 PT 144875111 TWE   Pick up workgroup based on sitebankid, siteclientaccountid
******************************************************************************/
SET NOCOUNT ON 

DECLARE @StartDateKey	INT,	 
        @EndDateKey		INT,
		@StartDate		VARCHAR(10),
		@EndDate		VARCHAR(10),
		@DefaultRetentionDays	INT;	 

		 
--SET	@parmDepositDateStart	 = '07/01/2014';
--SET	@parmDepositDateEnd		 = '12/01/2017';
--SET	@parmRerun		         = 1;


BEGIN TRY

	/*  Convert incoming values			*/
	SELECT 
		@StartDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateStart,112) AS INT),
		@EndDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateEnd,112) AS INT),
		@StartDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateStart,101)AS VARCHAR(10)),'/',''),
		@EndDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateEnd,101) AS VARCHAR(10)),'/','');

	SELECT 
		@DefaultRetentionDays = Value
	FROM 
		RecHubConfig.SystemSetup
	WHERE 
		Section = 'Rec Hub Table Maintenance' 
		AND SetupKey = 'DefaultDataRetentionDays'

	IF EXISTS (SELECT 1 FROM BillingStaging.BillingExtractHistory)  -- Clear out work table
	BEGIN 
		TRUNCATE TABLE BillingStaging.BillingExtractHistory;   
	END

    INSERT INTO BillingStaging.BillingExtractHistory  WITH (TABLOCK)
	(
		BankID,
		ClientAccountID,
		DepositDateKey , 
		ImmutableDateKey , 
		SourceBatchID ,
		BatchSourceKey ,
		CheckCount ,
		StubCount ,
		DocumentCount,
		BankName,
		WorkgroupName,
		EntityID,
		ViewingDays,
		RetentionDays
	) 	
	SELECT	
		RecHubData.dimBanks.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.factBatchSummary.DepositDateKey,
		RecHubData.factBatchSummary.ImmutableDateKey,
		RecHubData.factBatchSummary.SourceBatchID,
		RecHubData.factBatchSummary.BatchSourceKey,
		RecHubData.factBatchSummary.CheckCount,
		RecHubData.factBatchSummary.StubCount,
		RecHubData.factBatchSummary.DocumentCount,
		RecHubData.dimBanks.BankName,
		'Undefined'                      AS WorkgroupName,
		COALESCE(RecHubUser.OLWorkgroups.EntityID,'0'),       -- if 0 this workgroup is unassociated
		COALESCE(RecHubUser.OLWorkgroups.ViewingDays,RecHubUser.OLEntities.ViewingDays),
		COALESCE(RecHubData.dimClientAccounts.DataRetentionDays,@DefaultRetentionDays)
	FROM	
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		LEFT OUTER JOIN RecHubUser.OLWorkgroups ON RecHubData.dimBanks.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
											AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
		LEFT OUTER JOIN RecHubUser.OLEntities ON RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		LEFT OUTER JOIN RecHubBilling.BillingExtractHistory ON RecHubData.dimBanks.SiteBankID = RecHubBilling.BillingExtractHistory.BankID
															AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubBilling.BillingExtractHistory.ClientAccountID
															AND RecHubData.factBatchSummary.DepositDateKey = RecHubBilling.BillingExtractHistory.DepositDateKey
															AND RecHubData.factBatchSummary.ImmutableDateKey = RecHubBilling.BillingExtractHistory.ImmutableDateKey
															AND RecHubData.factBatchSummary.SourceBatchID = RecHubBilling.BillingExtractHistory.SourceBatchID
															AND RecHubData.factBatchSummary.BatchSourceKey = RecHubBilling.BillingExtractHistory.BatchSourceKey
	WHERE   
		RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey								
		AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey						
		AND RecHubData.factBatchSummary.DepositStatus >= 850
		AND RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.dimBanks.MostRecent = 1
		AND RecHubData.factBatchSummary.DepositDateKey IS NOT NULL
		AND RecHubData.factBatchSummary.BankKey != 6
		AND (RecHubBilling.BillingExtractHistory.BillingExtractHistoryKey IS NULL
			OR @parmRerun = 1) ;   --  We will load only rows not previously extracted

	--  Result set for Data Flow in Billing.dtsx
	SELECT	
		BankID,
		BankName,
		ClientAccountID,
		RecHubData.dimClientAccounts.ShortName AS WorkgroupName,
		EntityID,
		@StartDate AS StartDate,
		@EndDate AS EndDate,
		ViewingDays,
		RetentionDays,
		SUM(COALESCE(CheckCount,0)) AS CheckCount,
		SUM(COALESCE(DocumentCount,0)) AS DocumentCount,
		SUM(COALESCE(StubCount,0)) AS StubCount
	FROM	
		BillingStaging.BillingExtractHistory
		INNER JOIN RecHubData.dimClientAccounts 
		    ON BillingStaging.BillingExtractHistory.ClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
				AND BillingStaging.BillingExtractHistory.BankID = RecHubData.dimClientAccounts.SiteBankID
		WHERE
	        RecHubData.dimClientAccounts.MostRecent = 1
	GROUP BY  
		BankID,
		BankName,
		ClientAccountID,
		RecHubData.dimClientAccounts.ShortName,
		EntityID,
		ViewingDays,
		RetentionDays
	ORDER BY 
		BankID,
		ClientAccountID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
