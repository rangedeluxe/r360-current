--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulateExceptionCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulateExceptionCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulateExceptionCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulateExceptionCounts 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates RecHubException statistics and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 08/22/2014 WI 160886 RDS	Created
* 09/02/2014 WI 160886 RDS	Renamed, group by DDA, count items, etc.
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- Aggregate values for each date in the range of dates missing / being rechecked
DECLARE @targetRowKey BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	set @targetDateKey = CAST(CONVERT(VARCHAR(8), @loopDate, 112) AS INT);
	
	-- Step 1: Post Process Batches	
	-- Check for the last key
	SET @targetRowKey = ISNULL((SELECT MaxExceptionBatchKey FROM RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubException.ExceptionBatches WHERE DepositDateKey = @targetDateKey AND ExceptionBatchKey > @targetRowKey)
	BEGIN
		-- Data has been updated; recalculate
		DELETE RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetRowKey = ISNULL((SELECT TOP 1 ExceptionBatchKey FROM RecHubException.ExceptionBatches WHERE DepositDateKey = @targetDateKey ORDER BY ExceptionBatchKey DESC), 0);

		-- Store counts to the summary table
		WITH CTE_ExceptionTransactions AS	-- TODO: Should we update the schema to store the DDAKey somewhere else, to avoid this join?
		(
			SELECT SiteBankID, SiteWorkgroupID, RecHubException.ExceptionBatches.BatchID, RecHubException.Transactions.TransactionKey, MIN(DDAKey) DDAKey
			FROM RecHubException.Transactions
			INNER JOIN RecHubException.DecisioningTransactions
				ON RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
			INNER JOIN RecHubException.ExceptionBatches
				ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
			INNER JOIN RecHubData.factChecks
				ON RecHubException.ExceptionBatches.BatchID = RecHubData.factChecks.BatchID
				AND RecHubException.Transactions.TransactionID = RecHubData.factChecks.TransactionID
			WHERE RecHubException.ExceptionBatches.DepositDateKey = @targetDateKey
				AND RecHubData.factChecks.DepositDateKey = @targetDateKey
				AND RecHubException.DecisioningTransactions.TransactionStatusKey <> 4 -- 4 = No Exception
			GROUP BY SiteBankID, SiteWorkgroupID, RecHubException.ExceptionBatches.BatchID, RecHubException.Transactions.TransactionKey
		),
		CTE_Counts AS
		(
			SELECT SiteBankID, SiteWorkgroupID, DDAKey, COUNT(*) 'TranCount', 0 'PaymentCount', 0 'StubCount'
			FROM CTE_ExceptionTransactions
			GROUP BY SiteBankID, SiteWorkgroupID, DDAKey

			UNION ALL

			SELECT SiteBankID, SiteWorkgroupID, DDAKey, 0, COUNT(*), 0
			FROM CTE_ExceptionTransactions
			INNER JOIN RecHubException.Payments
				ON RecHubException.Payments.TransactionKey = CTE_ExceptionTransactions.TransactionKey
			GROUP BY SiteBankID, SiteWorkgroupID, DDAKey

			UNION ALL

			SELECT SiteBankID, SiteWorkgroupID, DDAKey, 0, 0, COUNT(*)
			FROM CTE_ExceptionTransactions
			INNER JOIN RecHubException.Stubs
				ON RecHubException.Stubs.TransactionKey = CTE_ExceptionTransactions.TransactionKey
			GROUP BY SiteBankID, SiteWorkgroupID, DDAKey
		),
		CTE_GroupedCounts AS
		(
			SELECT SiteBankID, SiteWorkgroupID, DDAKey, SUM(TranCount) 'TranCount', SUM(PaymentCount) 'PaymentCount', SUM(StubCount) 'StubCount'
			FROM CTE_Counts
			GROUP BY SiteBankID, SiteWorkgroupID, DDAKey
		)
		MERGE INTO RecHubBilling.ExceptionCounts
		USING CTE_GroupedCounts
			ON RecHubBilling.ExceptionCounts.DepositDateKey = @targetDateKey
			AND RecHubBilling.ExceptionCounts.SiteBankID = CTE_GroupedCounts.SiteBankID
			AND RecHubBilling.ExceptionCounts.WorkgroupID = CTE_GroupedCounts.SiteWorkgroupID
			AND RecHubBilling.ExceptionCounts.DDAKey = CTE_GroupedCounts.DDAKey
			AND RecHubBilling.ExceptionCounts.ExceptionType = 1 -- 1 = Post Process
		WHEN MATCHED THEN UPDATE SET 
			TransactionCount = CTE_GroupedCounts.TranCount,
			PaymentItemCount = CTE_GroupedCounts.PaymentCount,
			StubItemCount = CTE_GroupedCounts.StubCount
		WHEN NOT MATCHED BY TARGET THEN INSERT 
			(DepositDateKey, SiteBankID, WorkgroupID, DDAKey, ExceptionType,
			 TransactionCount, AcceptedTransactions, AcceptedNoChanges, RejectedTransactions,
			 PaymentItemCount, StubItemCount, UpdatedItemCount, AddedItemCount, DeletedItemCount)
			VALUES (@targetDateKey, SiteBankID, SiteWorkgroupID, DDAKey, 1, TranCount, 0, 0, 0, PaymentCount, StubCount, 0, 0, 0);

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.ExceptionCountsPostProcessBatchMonitor(DepositDateKey, MaxExceptionBatchKey) VALUES (@targetDateKey, @targetRowKey);
	END
	ELSE IF @targetRowKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.ExceptionCountsPostProcessBatchMonitor(DepositDateKey, MaxExceptionBatchKey) VALUES (@targetDateKey, 0);
	END

	-- Step 2: IntegraPAY Transactions
	-- Check for the last key
	SET @targetRowKey = ISNULL((SELECT MaxIntegraPAYExceptionKey FROM RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubException.IntegraPAYExceptions WHERE DepositDateKey = @targetDateKey AND IntegraPAYExceptionKey > @targetRowKey)
	BEGIN
		-- Data has been updated; recalculate
		DELETE RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetRowKey = ISNULL((SELECT TOP 1 IntegraPAYExceptionKey FROM RecHubException.IntegraPAYExceptions WHERE DepositDateKey = @targetDateKey ORDER BY IntegraPAYExceptionKey DESC), 0);

		-- Store counts to the summary table
		WITH CTE_GroupedCounts AS
		(
			SELECT SiteBankID, SiteClientAccountID, COUNT(IntegraPAYExceptionKey) TranCount
			FROM RecHubException.IntegraPAYExceptions
			WHERE RecHubException.IntegraPAYExceptions.DepositDateKey = @targetDateKey
			GROUP BY SiteBankID, SiteClientAccountID
		)
		MERGE INTO RecHubBilling.ExceptionCounts
		USING CTE_GroupedCounts
			ON RecHubBilling.ExceptionCounts.DepositDateKey = @targetDateKey
			AND RecHubBilling.ExceptionCounts.SiteBankID = CTE_GroupedCounts.SiteBankID
			AND RecHubBilling.ExceptionCounts.WorkgroupID = CTE_GroupedCounts.SiteClientAccountID
			AND RecHubBilling.ExceptionCounts.DDAKey = 0
			AND RecHubBilling.ExceptionCounts.ExceptionType = 2 -- 2 = In Process
		WHEN MATCHED THEN UPDATE SET 
			TransactionCount = CTE_GroupedCounts.TranCount
		WHEN NOT MATCHED BY TARGET THEN INSERT 
			(DepositDateKey, SiteBankID, WorkgroupID, DDAKey, ExceptionType,
			 TransactionCount, AcceptedTransactions, AcceptedNoChanges, RejectedTransactions,
			 PaymentItemCount, StubItemCount, UpdatedItemCount, AddedItemCount, DeletedItemCount)
			VALUES (@targetDateKey, SiteBankID, SiteClientAccountID, 0, 2,TranCount, 0, 0, 0, 0, 0, 0, 0, 0);

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.ExceptionCountsIntegraPAYMonitor(DepositDateKey, MaxIntegraPAYExceptionKey) VALUES (@targetDateKey, @targetRowKey);
	END
	ELSE IF @targetRowKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.ExceptionCountsIntegraPAYMonitor(DepositDateKey, MaxIntegraPAYExceptionKey) VALUES (@targetDateKey, 0);
	END
END;

-- Step 3: Accepted / Rejected / Updated Audit events
DECLARE @tblMessages TABLE (AuditKey UNIQUEIDENTIFIER NOT NULL, AuditMessage VARCHAR(8000) NOT NULL, AuditMessagePart INT NOT NULL);
DECLARE @tblTransactions TABLE (DecisioningTransactionKey BIGINT NOT NULL, Accepted BIT NOT NULL, Rejected BIT NOT NULL, 
								Added INT NOT NULL, Updated INT NOT NULL, Deleted INT NOT NULL);
DECLARE @auditMessagePart INT;
DECLARE @targetAuditEventKey INT;
DECLARE @maxAuditEventKey BIGINT;

-- Accepted Events (TODO: Is there a separate event for In Process...?)
SET @targetAuditEventKey = (SELECT AuditEventKey from RecHubAudit.dimAuditEvents where EventType = 'Exceptions' and EventName = 'E Accepted');
IF @targetAuditEventKey IS NOT NULL
BEGIN
	-- These are not searched by date; they are recorded under the date of the transaction, not the date of the audit, so we just look at everything we haven't looked at before...
	-- Check for the last key
	SET @targetRowKey = ISNULL((SELECT TOP 1 MaxEventAuditMessageKey FROM RecHubBilling.ExceptionCountsAuditMonitor), -1);
	IF (@targetRowKey = -1) INSERT INTO RecHubBilling.ExceptionCountsAuditMonitor(MaxEventAuditMessageKey) VALUES (0);

	-- Remember highest value reviewed
	SET @maxAuditEventKey = ISNULL((SELECT TOP 1 factEventAuditMessageKey FROM RecHubAudit.factEventAuditMessages ORDER BY factEventAuditMessageKey DESC), 0);

	SET @auditMessagePart = 1;

	-- Select the relevant audit messages
	INSERT INTO @tblMessages (AuditKey, AuditMessage, AuditMessagePart)
	SELECT AuditKey, AuditMessage, AuditMessagePart
	FROM RecHubAudit.factEventAuditMessages
	WHERE	factEventAuditMessageKey > @targetRowKey AND factEventAuditMessageKey <= @maxAuditEventKey
		AND AuditEventKey = @targetAuditEventKey;

	-- Combine the messages as necessary
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @auditMessagePart = @auditMessagePart + 1;

		UPDATE T1
		SET AuditMessage = T1.AuditMessage + T2.AuditMessage
		FROM @tblMessages T1
		INNER JOIN @tblMessages T2
			ON T1.AuditKey = T2.AuditKey
			AND T1.AuditMessagePart = 1
			AND T2.AuditMessagePart = @auditMessagePart;
	END

	-- Clean up temporary table
	DELETE @tblMessages WHERE AuditMessagePart > 1;

	-- Parse the Message and temporarily accumulate it
	INSERT INTO @tblTransactions (DecisioningTransactionKey, Accepted, Rejected, Added, Updated, Deleted)
	SELECT DISTINCT ParsedMessages.DecisioningTransactionKey, 1, 0, 0, 0, 0
	FROM @tblMessages tblMessages
	CROSS APPLY RecHubBilling.fn_Exceptions_ParseAuditMessage(tblMessages.AuditMessage) ParsedMessages;
END

-- Rejected Transactions
SET @targetAuditEventKey = (SELECT AuditEventKey from RecHubAudit.dimAuditEvents where EventType = 'Exceptions' and EventName = 'E Rejected');
IF @targetAuditEventKey IS NOT NULL
BEGIN
	-- Select the relevant audit messages
	DELETE @tblMessages;
	INSERT INTO @tblMessages (AuditKey, AuditMessage, AuditMessagePart)
	SELECT AuditKey, AuditMessage, AuditMessagePart
	FROM RecHubAudit.factEventAuditMessages
	WHERE	factEventAuditMessageKey > @targetRowKey AND factEventAuditMessageKey <= @maxAuditEventKey
		AND AuditEventKey = @targetAuditEventKey;

	-- Combine the messages as necessary
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @auditMessagePart = @auditMessagePart + 1;

		UPDATE T1
		SET AuditMessage = T1.AuditMessage + T2.AuditMessage
		FROM @tblMessages T1
		INNER JOIN @tblMessages T2
			ON T1.AuditKey = T2.AuditKey
			AND T1.AuditMessagePart = 1
			AND T2.AuditMessagePart = @auditMessagePart;
	END

	-- Clean up temporary table
	DELETE @tblMessages WHERE AuditMessagePart > 1;

	-- Parse the Message and temporarily accumulate it
	INSERT INTO @tblTransactions (DecisioningTransactionKey, Accepted, Rejected, Added, Updated, Deleted)
	SELECT DISTINCT ParsedMessages.DecisioningTransactionKey, 0, 1, 0, 0, 0
	FROM @tblMessages tblMessages
	CROSS APPLY RecHubBilling.fn_Exceptions_ParseAuditMessage(tblMessages.AuditMessage) ParsedMessages;
END

-- Update Events (TODO: Is there a separate event for In Process...?)
SET @targetAuditEventKey = (SELECT AuditEventKey from RecHubAudit.dimAuditEvents where EventType = 'Exceptions' and EventName = 'E Update Item');
IF @targetAuditEventKey IS NOT NULL
BEGIN
	-- Select the relevant audit messages
	DELETE @tblMessages;
	INSERT INTO @tblMessages (AuditKey, AuditMessage, AuditMessagePart)
	SELECT AuditKey, AuditMessage, AuditMessagePart
	FROM RecHubAudit.factEventAuditMessages
	WHERE	factEventAuditMessageKey > @targetRowKey AND factEventAuditMessageKey <= @maxAuditEventKey
		AND AuditEventKey = @targetAuditEventKey;

	-- Combine the messages as necessary
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @auditMessagePart = @auditMessagePart + 1;

		UPDATE T1
		SET AuditMessage = T1.AuditMessage + T2.AuditMessage
		FROM @tblMessages T1
		INNER JOIN @tblMessages T2
			ON T1.AuditKey = T2.AuditKey
			AND T1.AuditMessagePart = 1
			AND T2.AuditMessagePart = @auditMessagePart;
	END

	-- Clean up temporary table
	DELETE @tblMessages WHERE AuditMessagePart > 1;

	-- Parse the Message and temporarily accumulate it
	MERGE INTO @tblTransactions tblTransactions
	USING
	(
		SELECT ParsedMessages.DecisioningTransactionKey, SUM(ParsedMessages.UpdatedItems) UpdatedItems
		FROM @tblMessages tblMessages
		CROSS APPLY RecHubBilling.fn_Exceptions_ParseAuditMessage(tblMessages.AuditMessage) ParsedMessages
		GROUP BY ParsedMessages.DecisioningTransactionKey
	) TransactionInfo
	ON tblTransactions.DecisioningTransactionKey = TransactionInfo.DecisioningTransactionKey
	WHEN MATCHED THEN UPDATE SET 
		Updated = TransactionInfo.UpdatedItems
	WHEN NOT MATCHED BY TARGET THEN INSERT (DecisioningTransactionKey, Accepted, Rejected, Added, Updated, Deleted)
		VALUES (DecisioningTransactionKey, 0, 0, 0, UpdatedItems, 0);
END

-- Add Events (In Process)
SET @targetAuditEventKey = (SELECT AuditEventKey from RecHubAudit.dimAuditEvents where EventType = 'Exceptions' and EventName = 'E Insert Item');
IF @targetAuditEventKey IS NOT NULL
BEGIN
	-- Select the relevant audit messages
	DELETE @tblMessages;
	INSERT INTO @tblMessages (AuditKey, AuditMessage, AuditMessagePart)
	SELECT AuditKey, AuditMessage, AuditMessagePart
	FROM RecHubAudit.factEventAuditMessages
	WHERE	factEventAuditMessageKey > @targetRowKey AND factEventAuditMessageKey <= @maxAuditEventKey
		AND AuditEventKey = @targetAuditEventKey;

	-- Combine the messages as necessary
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @auditMessagePart = @auditMessagePart + 1;

		UPDATE T1
		SET AuditMessage = T1.AuditMessage + T2.AuditMessage
		FROM @tblMessages T1
		INNER JOIN @tblMessages T2
			ON T1.AuditKey = T2.AuditKey
			AND T1.AuditMessagePart = 1
			AND T2.AuditMessagePart = @auditMessagePart;
	END

	-- Clean up temporary table
	DELETE @tblMessages WHERE AuditMessagePart > 1;

	-- Parse the Message and temporarily accumulate it
	MERGE INTO @tblTransactions tblTransactions
	USING
	(
		SELECT ParsedMessages.DecisioningTransactionKey, SUM(ParsedMessages.AddedItems) AddedItems
		FROM @tblMessages tblMessages
		CROSS APPLY RecHubBilling.fn_Exceptions_ParseAuditMessage(tblMessages.AuditMessage) ParsedMessages
		GROUP BY ParsedMessages.DecisioningTransactionKey
	) TransactionInfo
	ON tblTransactions.DecisioningTransactionKey = TransactionInfo.DecisioningTransactionKey
	WHEN MATCHED THEN UPDATE SET 
		Added = TransactionInfo.AddedItems
	WHEN NOT MATCHED BY TARGET THEN INSERT (DecisioningTransactionKey, Accepted, Rejected, Added, Updated, Deleted)
		VALUES (DecisioningTransactionKey, 0, 0, AddedItems, 0, 0);
END

-- Delete Events (In Process)
SET @targetAuditEventKey = (SELECT AuditEventKey from RecHubAudit.dimAuditEvents where EventType = 'Exceptions' and EventName = 'E Delete Item');
IF @targetAuditEventKey IS NOT NULL
BEGIN
	-- Select the relevant audit messages
	DELETE @tblMessages;
	INSERT INTO @tblMessages (AuditKey, AuditMessage, AuditMessagePart)
	SELECT AuditKey, AuditMessage, AuditMessagePart
	FROM RecHubAudit.factEventAuditMessages
	WHERE	factEventAuditMessageKey > @targetRowKey AND factEventAuditMessageKey <= @maxAuditEventKey
		AND AuditEventKey = @targetAuditEventKey;

	-- Combine the messages as necessary
	WHILE @@ROWCOUNT > 0
	BEGIN
		SET @auditMessagePart = @auditMessagePart + 1;

		UPDATE T1
		SET AuditMessage = T1.AuditMessage + T2.AuditMessage
		FROM @tblMessages T1
		INNER JOIN @tblMessages T2
			ON T1.AuditKey = T2.AuditKey
			AND T1.AuditMessagePart = 1
			AND T2.AuditMessagePart = @auditMessagePart;
	END

	-- Clean up temporary table
	DELETE @tblMessages WHERE AuditMessagePart > 1;

	-- Parse the Message and temporarily accumulate it
	MERGE INTO @tblTransactions tblTransactions
	USING
	(
		SELECT ParsedMessages.DecisioningTransactionKey, SUM(ParsedMessages.DeletedItems) DeletedItems
		FROM @tblMessages tblMessages
		CROSS APPLY RecHubBilling.fn_Exceptions_ParseAuditMessage(tblMessages.AuditMessage) ParsedMessages
		GROUP BY ParsedMessages.DecisioningTransactionKey
	) TransactionInfo
	ON tblTransactions.DecisioningTransactionKey = TransactionInfo.DecisioningTransactionKey
	WHEN MATCHED THEN UPDATE SET 
		Deleted = TransactionInfo.DeletedItems
	WHEN NOT MATCHED BY TARGET THEN INSERT (DecisioningTransactionKey, Accepted, Rejected, Added, Updated, Deleted)
		VALUES (DecisioningTransactionKey, 0, 0, 0, 0, DeletedItems);
END

-- Finally, update the accumulation table with the counts we collected above
BEGIN TRANSACTION;

	WITH CTE_ExceptionTransactions AS	-- TODO: Should we update the schema to store the DDAKey somewhere else, to avoid this join?
	(
		SELECT RecHubException.ExceptionBatches.DepositDateKey, SiteBankID, SiteWorkgroupID, RecHubException.ExceptionBatches.BatchID, RecHubException.Transactions.TransactionKey, MIN(DDAKey) DDAKey
		FROM RecHubException.Transactions
		INNER JOIN RecHubException.DecisioningTransactions
			ON RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
		INNER JOIN RecHubException.ExceptionBatches
			ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		INNER JOIN RecHubData.factChecks
			ON RecHubException.ExceptionBatches.BatchID = RecHubData.factChecks.BatchID
			AND RecHubException.Transactions.TransactionID = RecHubData.factChecks.TransactionID
		WHERE RecHubException.ExceptionBatches.DepositDateKey = @targetDateKey
			AND RecHubData.factChecks.DepositDateKey = @targetDateKey
			AND RecHubException.DecisioningTransactions.TransactionStatusKey <> 4 -- 4 = No Exception
		GROUP BY RecHubException.ExceptionBatches.DepositDateKey, SiteBankID, SiteWorkgroupID, RecHubException.ExceptionBatches.BatchID, RecHubException.Transactions.TransactionKey
	),	
	CTE_TransactionInfo AS
	(
		SELECT	ISNULL(CTE_ExceptionTransactions.DepositDateKey, RecHubException.IntegraPAYExceptions.DepositDateKey) DepositDateKey,
				ISNULL(CTE_ExceptionTransactions.SiteBankID, RecHubException.IntegraPAYExceptions.SiteBankID) SiteBankID,
				ISNULL(CTE_ExceptionTransactions.SiteWorkgroupID, RecHubException.IntegraPAYExceptions.SiteClientAccountID) WorkgroupID,
				ISNULL(CTE_ExceptionTransactions.DDAKey, 0) DDAKey,
				CASE WHEN RecHubException.DecisioningTransactions.TransactionKey IS NULL THEN 2 ELSE 1 END ExceptionType,
				tblTransactions.Accepted,
				tblTransactions.Rejected,
				tblTransactions.Added,
				tblTransactions.Updated,
				tblTransactions.Deleted
		FROM @tblTransactions tblTransactions
		INNER JOIN RecHubException.DecisioningTransactions
			ON tblTransactions.DecisioningTransactionKey = RecHubException.DecisioningTransactions.DecisioningTransactionKey
		LEFT JOIN CTE_ExceptionTransactions 
			ON CTE_ExceptionTransactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
		LEFT JOIN RecHubException.IntegraPAYExceptions
			ON RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
	),
	CTE_SummaryInfo AS
	(
		SELECT DepositDateKey, SiteBankID, WorkgroupID, DDAKey, ExceptionType,
				SUM(CAST(Accepted AS INT)) Accepted, SUM(CAST(Rejected AS INT)) Rejected,
				SUM(CASE WHEN Accepted = 1 AND Added = 0 AND Updated = 0 AND Deleted = 0 THEN 1 ELSE 0 END) AcceptedNoChange,
				SUM(Added) Added, SUM(Updated) Updated, SUM(Deleted) Deleted
		FROM CTE_TransactionInfo
		GROUP BY DepositDateKey, SiteBankID, WorkgroupID, DDAKey, ExceptionType
	)
	UPDATE RecHubBilling.ExceptionCounts
		SET AcceptedTransactions = AcceptedTransactions + Accepted,
			AcceptedNoChanges = AcceptedNoChanges + AcceptedNoChange,	-- Note: This is theoretically not guaranteed to be accurate (possible timing / race conditions), although in practice it should be (if this is run when the system is "idle")...  But if the number is too high, we'll need to redo the logic...
			RejectedTransactions = RejectedTransactions + Rejected,
			AddedItemCount = AddedItemCount + Added,
			UpdatedItemCount = UpdatedItemCount + Updated,
			DeletedItemCount = DeletedItemCount + Deleted
	FROM RecHubBilling.ExceptionCounts
	INNER JOIN CTE_SummaryInfo
		ON	RecHubBilling.ExceptionCounts.DepositDateKey = CTE_SummaryInfo.DepositDateKey
		AND RecHubBilling.ExceptionCounts.SiteBankID = CTE_SummaryInfo.SiteBankID
		AND RecHubBilling.ExceptionCounts.WorkgroupID = CTE_SummaryInfo.WorkgroupID
		AND RecHubBilling.ExceptionCounts.DDAKey = CTE_SummaryInfo.DDAKey
		AND RecHubBilling.ExceptionCounts.ExceptionType = CTE_SummaryInfo.ExceptionType;

	-- Insert status / index hint into monitor table
	UPDATE RecHubBilling.ExceptionCountsAuditMonitor SET MaxEventAuditMessageKey = @maxAuditEventKey;

COMMIT TRANSACTION
