--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulateDocumentCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulateDocumentCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulateDocumentCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulateDocumentCounts 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates factDocument statistics and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 08/22/2014 WI 160883 RDS	Created
* 09/02/2014 WI 160883 RDS	Group by DDA, Update Table Names
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- Aggregate values for each date in the range of dates missing / being rechecked
DECLARE @targetFactKey BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	SET @targetDateKey = CAST(CONVERT(VARCHAR(8), @loopDate, 112) AS INT);
		
	-- Check for the last key we summarized
	SET @targetFactKey = ISNULL((SELECT MaxFactDocumentKey FROM RecHubBilling.DocumentCountsMonitor WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubData.factDocuments WHERE DepositDateKey = @targetDateKey AND factDocumentKey > @targetFactKey)
	BEGIN
		-- Data has been updated; recalculate

		-- Delete existing counts
		DELETE RecHubBilling.DocumentCountsMonitor WHERE DepositDateKey = @targetDateKey;
		DELETE RecHubBilling.DocumentCounts WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetFactKey = ISNULL((SELECT TOP 1 factDocumentKey FROM RecHubData.factDocuments WHERE DepositDateKey = @targetDateKey ORDER BY factDocumentKey DESC), 0);

		-- Store counts to the summary table
		-- TODO: Should factDocuments be updated to include DDAKey, to avoid this (poorly performing) join to factChecks?
		-- Option 1: Join / Group By
		/*WITH CTE_factDocumentsWithDDA AS
		(
			SELECT RecHubData.factDocuments.ClientAccountKey, RecHubData.factDocuments.BatchSourceKey, MIN(RecHubData.factChecks.DDAKey) DDAKey, RecHubData.factDocuments.factDocumentKey
			FROM RecHubData.factDocuments
			LEFT JOIN RecHubData.factChecks
				ON RecHubData.factDocuments.DepositDateKey = RecHubdata.factChecks.DepositDateKey
				AND RecHubData.factDocuments.BatchID = RecHubData.factChecks.BatchID
				AND RecHubData.factDocuments.TransactionID = RecHubData.factChecks.TransactionID
				AND RecHubData.factChecks.IsDeleted = 0
			WHERE RecHubData.factDocuments.DepositDateKey = @targetDateKey
				AND RecHubData.factChecks.DepositDateKey = @targetDateKey
				AND RecHubData.factDocuments.IsDeleted = 0
			GROUP BY RecHubData.factDocuments.ClientAccountKey, RecHubData.factDocuments.BatchSourceKey, 
					 RecHubData.factDocuments.BatchID, RecHubData.factDocuments.TransactionID, RecHubData.factDocuments.factDocumentKey
		)
		INSERT INTO RecHubBilling.DocumentCounts(DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, ISNULL(DDAKey, 0), COUNT(*)
		FROM CTE_factDocumentsWithDDA
		INNER JOIN RecHubData.dimClientAccounts
			ON CTE_factDocumentsWithDDA.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;*/

		-- Option 2: Partition & Filter / Join
		WITH CTE_factCheckDDA AS
		(
			SELECT ROW_NUMBER() OVER(PARTITION BY TransactionID ORDER BY TransactionID) Sequence, TransactionID, DDAKey
			FROM RecHubData.factChecks
			WHERE DepositDateKey = @targetDateKey
				AND IsDeleted = 0
		),
		CTE_factTransactionDDA AS
		(
			SELECT TransactionID, DDAKey
			FROM CTE_factCheckDDA
			WHERE Sequence = 1
		)
		INSERT INTO RecHubBilling.DocumentCounts(DepositDateKey, SiteBankID, WorkgroupID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, ISNULL(DDAKey, 0), COUNT(*)
		FROM RecHubData.factDocuments
		LEFT JOIN CTE_factTransactionDDA
			ON RecHubData.factDocuments.TransactionID = CTE_factTransactionDDA.TransactionID
		INNER JOIN RecHubData.dimClientAccounts
			ON RecHubData.factDocuments.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE RecHubData.factDocuments.DepositDateKey = @targetDateKey
			AND RecHubData.factDocuments.IsDeleted = 0
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.DocumentCountsMonitor(DepositDateKey, MaxFactDocumentKey) VALUES (@targetDateKey, @targetFactKey);
	END
	ELSE IF @targetFactKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.DocumentCountsMonitor(DepositDateKey, MaxFactDocumentKey) VALUES (@targetDateKey, 0);
	END
END