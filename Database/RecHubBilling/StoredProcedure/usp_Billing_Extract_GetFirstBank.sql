--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_GetFirstBank
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_GetFirstBank') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_GetFirstBank
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_GetFirstBank 
(
	@parmFiEntityId			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 09/11/2014
*
* Purpose: This procedure retrieves the first bank associated with a given FI Entity ID
*
* Modification History
* 09/11/2014 WI 171621 RDS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT TOP 1 SiteBankID
	FROM RecHubData.dimBanksView
	WHERE EntityID = @parmFiEntityId
	ORDER BY SiteBankID; -- This order by isn't necessary; just want to make sure it is predictable, though...
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH