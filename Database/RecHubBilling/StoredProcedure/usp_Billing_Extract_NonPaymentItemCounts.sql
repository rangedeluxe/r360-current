--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_NonPaymentItemCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_NonPaymentItemCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_NonPaymentItemCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_NonPaymentItemCounts 
(
	@parmStartDateKey		INT,		-- yyyyMMdd as int
	@parmEndDateKey			INT, 		-- yyyyMMdd as int
	@parmRetentionCutoffs	XML,		-- <list><c id="1" val="3" unit="Days" /><c id="2" val="120" unit="Days" /></list>
	@parmEntities			XML = null	-- <list><e id='*' /></list>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure returns counts for the number of items stored
*			per entity, partitioned by retention-level
*          Note: The service will need to process the list of "entities" into the correct 
*			customer counts, and lookup associated IDs.
*
* Modification History
* 05/07/2014 WI 137138 RDS	Created
* 12/15/2014 WI 184533 RDS	Support Entity-Level grouping and targeted FI Entities
*							And return Billing Fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Convert date keys to datetime variables (and add one day to end date to allow for time values)
	--DECLARE @startDate DATETIME = CONVERT(DATETIME, CAST(@parmStartDateKey AS VARCHAR(8)), 112)
	--DECLARE @endDate DATETIME = DATEADD(DAY, 1, CONVERT(DATETIME, CAST(@parmEndDateKey AS VARCHAR(8)), 112))
	DECLARE @targetRowKey BIGINT;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.StubCountsMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in StubCountsMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxFactStubKey FROM RecHubBilling.StubCountsMonitor WHERE DepositDateKey = @parmEndDateKey), 0);
		IF EXISTS (SELECT * from RecHubData.factStubs WHERE DepositDateKey = @parmEndDateKey AND factStubKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in StubCounts', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.DocumentCountsMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in DocumentCountsMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxFactDocumentKey FROM RecHubBilling.DocumentCountsMonitor WHERE DepositDateKey = @parmEndDateKey), 0);
		IF EXISTS (SELECT * from RecHubData.factDocuments WHERE DepositDateKey = @parmEndDateKey AND factDocumentKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in DocumentCounts', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END

	-- Convert retention cutoffs to a temporary table
	DECLARE @tblRetentionCutoffs TABLE (CutoffID tinyint, CutoffValue INT, CutoffUnit varchar(10))
	INSERT INTO @tblRetentionCutoffs (CutoffID, CutoffValue, CutoffUnit)
	SELECT T.c.value('@id', 'tinyint'), T.c.value('@val', 'int'), T.c.value('@unit', 'varchar(10)')
		FROM @parmRetentionCutoffs.nodes('/list/c') T(c);

	-- All retentions are stored in the database in days -- so convert any other retention units to days via some "standard" conversions...
	-- TODO: What are the "standard" conversions ?!?!?
	UPDATE @tblRetentionCutoffs
		SET CutoffValue = CASE CutoffUnit 
			WHEN 'Months' THEN CutoffValue * 30
			WHEN 'Years' THEN CutoffValue * 366
			END
	WHERE CutoffUnit in ('Months', 'Years');

	-- Convert entity targets to a temporary table
	DECLARE @tblEntityTargets TABLE (EntityID INT PRIMARY KEY);
	IF (@parmEntities IS NOT NULL)
	BEGIN
		INSERT INTO @tblEntityTargets (EntityID)
		SELECT T.c.value('@id', 'int')
			FROM @parmEntities.nodes('/list/e') T(c)
			WHERE T.c.value('@id', 'varchar(20)') <> '*';
	END;

	IF NOT EXISTS(SELECT * FROM @tblEntityTargets)
	BEGIN
		-- No filter specified: allow all mapped FI Entity IDs
		INSERT INTO @tblEntityTargets (EntityID) SELECT DISTINCT EntityID FROM RecHubData.dimBanksView WHERE EntityID IS NOT NULL;
	END;

	-- Retention is defined in RecHubData.dimClientAccounts - so join there to group by retention

	-- Option 1: Use Row_Number to order the rows and select the top match
	--;WITH CTE_Workgroups_By_RetentionCutoff_Raw AS
	--(
	--	SELECT ROW_NUMBER() OVER (PARTITION BY WG.ClientAccountKey ORDER BY RC.CutoffValue) AS RowNumber, WG.ClientAccountKey, WG.SiteBankID, ISNULL(RC.CutoffID, 255) CutoffID
	--	FROM RecHubData.dimClientAccountsView WG
	--		LEFT JOIN @tblRetentionCutoffs RC		-- Left join, and assign ID of 255 if no match (real IDs should be less than 100...)
	--			ON WG.DataRetentionDays <= RC.CutoffValue
	--),
	--CTE_Workgroups_By_RetentionCutoff AS
	--(
	--	SELECT ClientAccountKey, SiteBankID, CutoffID
	--	FROM CTE_Workgroups_By_RetentionCutoff_Raw
	--	WHERE RowNumber = 1
	--)

	-- Option 2: Take advantage of the fact that CutoffID and CutoffValue will be in the same order, and use MIN / GROUP BY to select the top match
	WITH CTE_Workgroups_By_RetentionCutoff AS
	(
		SELECT SiteBankID, SiteClientAccountID, ISNULL(MIN(CutoffID), 255) CutoffID, LongName
		FROM RecHubData.dimClientAccountsView
			LEFT JOIN @tblRetentionCutoffs tblRetentionCutoffs
				ON RecHubData.dimClientAccountsView.DataRetentionDays <= tblRetentionCutoffs.CutoffValue
		GROUP BY RecHubData.dimClientAccountsView.SiteBankID, RecHubData.dimClientAccountsView.SiteClientAccountID, RecHubData.dimClientAccountsView.LongName
	),
	CTE_Workgroups AS
	(
		SELECT	RecHubData.dimBanksView.EntityID 'FIEntityID', 
				CTE_Workgroups_By_RetentionCutoff.SiteBankID, 
				RecHubUser.OLWorkgroups.EntityID,
				CTE_Workgroups_By_RetentionCutoff.SiteClientAccountID,
				CutoffID,
				CTE_Workgroups_By_RetentionCutoff.LongName,
				ISNULL(RecHubUser.OLWorkgroups.BillingAccount, RecHubUser.OLEntities.BillingAccount) 'BillingAccount',
				ISNULL(RecHubUser.OLWorkgroups.BillingField1, RecHubUser.OLEntities.BillingField1) 'BillingField1',
				ISNULL(RecHubUser.OLWorkgroups.BillingField2, RecHubUser.OLEntities.BillingField2) 'BillingField2'
		FROM CTE_Workgroups_By_RetentionCutoff
			INNER JOIN RecHubUser.OLWorkgroups
				ON	CTE_Workgroups_By_RetentionCutoff.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
				AND CTE_Workgroups_By_RetentionCutoff.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
			INNER JOIN RecHubData.dimBanksView
				ON CTE_Workgroups_By_RetentionCutoff.SiteBankID = RecHubData.dimBanksView.SiteBankID
			INNER JOIN @tblEntityTargets tblEntityTargets 
				ON tblEntityTargets.EntityID = RecHubData.dimBanksView.EntityID
			INNER JOIN RecHubUser.OLEntities
				ON RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
	)
	SELECT  FIEntityID, SiteBankID, EntityID, SiteClientAccountID, LongName, BillingAccount, BillingField1, BillingField2, SUM(ItemCount) 'ItemCount', CutoffID
	FROM
	(
		SELECT FIEntityID, CTE_Workgroups.SiteBankID, EntityID, SiteClientAccountID, LongName, BillingAccount, BillingField1, BillingField2, ItemCount, CutoffID
		FROM CTE_Workgroups
			INNER JOIN RecHubBilling.StubCounts		-- Use the pre-populated summary table for performance 
				ON RecHubBilling.StubCounts.SiteBankID = CTE_Workgroups.SiteBankID
				AND RecHubBilling.StubCounts.WorkgroupID = CTE_Workgroups.SiteClientAccountID
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey

		UNION ALL

		SELECT FIEntityID, CTE_Workgroups.SiteBankID, EntityID, SiteClientAccountID, LongName, BillingAccount, BillingField1, BillingField2, ItemCount, CutoffID
		FROM CTE_Workgroups
			INNER JOIN RecHubBilling.DocumentCounts		-- Use the pre-populated summary table for performance 
				ON RecHubBilling.DocumentCounts.SiteBankID = CTE_Workgroups.SiteBankID
				AND RecHubBilling.DocumentCounts.WorkgroupID = CTE_Workgroups.SiteClientAccountID
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey

	) AS AllDocs
	GROUP BY FIEntityID, SiteBankID, EntityID, SiteClientAccountID, LongName, BillingAccount, BillingField1, BillingField2, CutoffID;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH