--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_Customers
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_Customers') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_Customers
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_Customers 
(
	@parmStartDateKey		INT,		-- yyyyMMdd as int
	@parmEndDateKey			INT, 		-- yyyyMMdd as int
	@parmEntities			XML = null	-- <list><e id='*' /></list>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure retrieves the list of all "customers" (entities) for a given
*			month, as needed by the various Billing Formats.
*          Note: The service will need to process the list of "entities" into the correct 
*			customer counts, and lookup related IDs.
*
*
* Modification History
* 09/10/2014 WI 137137 RDS	Created 
* 12/15/2014 WI 137137 RDS	Support Entity-Level grouping and targeted FI Entities,
*							And return Billing Fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Convert date keys to datetime variables
	--DECLARE @startDate DATETIME = CONVERT(DATETIME, CAST(@parmStartDateKey AS VARCHAR(8)), 112)
	DECLARE @endDate DATE = CONVERT(DATE, CAST(@parmEndDateKey AS VARCHAR(8)), 112)

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.PaymentCountsMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in PaymentCounts', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		DECLARE @targetFactPaymentKey BIGINT = ISNULL((SELECT MaxFactPaymentKey FROM RecHubBilling.PaymentCountsMonitor WHERE DepositDateKey = @parmEndDateKey), 0)
		IF EXISTS (SELECT * from RecHubData.factChecks WHERE DepositDateKey = @parmEndDateKey AND factCheckKey > @targetFactPaymentKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in PaymentCounts', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is populated (can't verify up-to-date...) before continuing.
	DECLARE @monthDateKey INT = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, @endDate), 0), 112) AS INT);
	DECLARE @currentMonthDateKey INT = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0), 112) AS INT);
	IF NOT EXISTS (SELECT * FROM RecHubBilling.WorkgroupFlags WHERE DepositDateKey = @monthDateKey)
	BEGIN
		IF @monthDateKey = @currentMonthDateKey
		BEGIN
			RAISERROR('Date %d was not found in WorkgroupFlags', 16, 1,  @monthDateKey);
			RETURN;
		END
		ELSE
		BEGIN
			-- There is no way to get this data now; so ignore it...
			RAISERROR('Date %d was not found in WorkgroupFlags - continuing without that data', 10, 1,  @monthDateKey) WITH NOWAIT;
		END
	END;

	-- Convert entity targets to a temporary table
	DECLARE @tblEntityTargets TABLE (EntityID INT PRIMARY KEY);
	IF (@parmEntities IS NOT NULL)
	BEGIN
		INSERT INTO @tblEntityTargets (EntityID)
		SELECT T.c.value('@id', 'int')
			FROM @parmEntities.nodes('/list/e') T(c)
			WHERE T.c.value('@id', 'varchar(20)') <> '*';
	END;

	IF NOT EXISTS(SELECT * FROM @tblEntityTargets)
	BEGIN
		-- No filter specified: allow all mapped FI Entity IDs
		INSERT INTO @tblEntityTargets (EntityID) SELECT DISTINCT EntityID FROM RecHubData.dimBanksView WHERE EntityID IS NOT NULL;
	END;

	-- Determine this list based on the presence of items during the time range, or was marked Active
	WITH CTE_AllWorkgroups AS
	(
		-- Return any workgroup that has stored an item during the time range
		SELECT DISTINCT SiteBankID, WorkgroupID
		FROM RecHubBilling.PaymentCounts
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey

		UNION

		SELECT DISTINCT SiteBankID, WorkgroupID
		FROM RecHubBilling.StubCounts
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey

		UNION

		SELECT DISTINCT SiteBankID, WorkgroupID
		FROM RecHubBilling.DocumentCounts
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey

		UNION

		-- Return any workgroup that was marked active in the month the range ends
		SELECT SiteBankID, WorkgroupID
		FROM RecHubBilling.WorkgroupFlags
		WHERE DepositDateKey = @monthDateKey and Active = 1
	)
	SELECT DISTINCT RecHubData.dimBanksView.EntityID 'FIEntityID', CTE_AllWorkgroups.SiteBankID, RecHubUser.OLWorkgroups.EntityID,
		RecHubUser.OLWorkgroups.SiteClientAccountID, RecHubData.dimClientAccountsView.LongName, 
		--RecHubUser.OLWorkgroups.BillingAccount, RecHubUser.OLWorkgroups.BillingField1, RecHubUser.OLWorkgroups.BillingField2
		-- This is "customer"-level data, so actually return the "Customer" Billing field values, if populated...
		ISNULL(RecHubUser.OLEntities.BillingAccount, RecHubUser.OLWorkgroups.BillingAccount),
		ISNULL(RecHubUser.OLEntities.BillingField1, RecHubUser.OLWorkgroups.BillingField1),
		ISNULL(RecHubUser.OLEntities.BillingField2, RecHubUser.OLWorkgroups.BillingField2)
	FROM CTE_AllWorkgroups
	INNER JOIN RecHubUser.OLWorkgroups
		ON	CTE_AllWorkgroups.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
		AND CTE_AllWorkgroups.WorkgroupID = RecHubUser.OLWorkgroups.SiteClientAccountID
	INNER JOIN RecHubData.dimClientAccountsView
		ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
		AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
	INNER JOIN RecHubUser.OLEntities
		ON	RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
	INNER JOIN RecHubData.dimBanksView
		ON CTE_AllWorkgroups.SiteBankID = RecHubData.dimBanksView.SiteBankID
	INNER JOIN @tblEntityTargets tblEntityTargets 
		ON tblEntityTargets.EntityID = RecHubData.dimBanksView.EntityID
	ORDER BY FIEntityID, SiteBankID, EntityID;
		
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH