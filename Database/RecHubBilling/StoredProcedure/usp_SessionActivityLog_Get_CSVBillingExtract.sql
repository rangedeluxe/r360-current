--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_SessionActivityLog_Get_CSVBillingExtract
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_SessionActivityLog_Get_CSVBillingExtract') IS NOT NULL
	   DROP PROCEDURE RecHubBilling.usp_SessionActivityLog_Get_CSVBillingExtract;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_SessionActivityLog_Get_CSVBillingExtract 
(
	@parmDepositDateStart	DATETIME,
	@parmDepositDateEnd		DATETIME,
	@parmRerun				BIT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	TWE
* Date:		05/04/2017
*
* Purpose: Query Batch Summary fact table for Billing Extract process for Date range supplied.
*
* Modification History
* 05/04/2017 PT #142166535 TWE	Create 
* 05/15/2017 PT #142166535 TWE	Filter by activity code 25 and bankID 
******************************************************************************/
SET NOCOUNT ON 

DECLARE @StartDateKey	INT,	 
        @EndDateKey		INT,
		@StartDate		VARCHAR(10),
		@EndDate		VARCHAR(10),
		@DefaultRetentionDays	INT,
		@TableUsed      VARCHAR(256) = 'SessionActivityLog';	 

--set 	@parmDepositDateStart	= '07/01/2014';
--set	@parmDepositDateEnd		= '12/01/2017';
--set	@parmRerun		        = 1;


BEGIN TRY

	/*  Convert incoming values			*/
	SELECT 
		@StartDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateStart,112) AS INT),
		@EndDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateEnd,112) AS INT),
		@StartDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateStart,101)AS VARCHAR(10)),'/',''),
		@EndDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateEnd,101) AS VARCHAR(10)),'/','');
		
	SELECT 
		@DefaultRetentionDays = Value
	FROM 
		RecHubConfig.SystemSetup
	WHERE 
		Section = 'Rec Hub Table Maintenance' 
		AND SetupKey = 'DefaultDataRetentionDays';
	
	
	INSERT INTO BillingStaging.BillingWorkgroupExtractHistory  WITH (TABLOCK)
	(
		BankID
              ,ClientAccountID
              ,TableUsed
              ,TableKey
		,DateKey
              ,NotificationsWithAttachmentsCount
              ,NotificationsWithoutAttachmentsCount
              ,AttachmentCount
              ,AttachmentSize
              ,CSVDownloadCount
              ,CSVRowCount
              ,BankName
              ,WorkgroupName
              ,EntityID
		,ViewingDays
		,RetentionDays
	) 	
	SELECT	
		RecHubUser.SessionActivityLog.BankID
              ,RecHubUser.SessionActivityLog.ClientAccountID
	          ,@TableUsed                                            AS TableUsed
	          ,RecHubUser.SessionActivityLog.SessionActivityLogID    AS TableKey
		,RechubUser.SessionActivityLog.CreationDateKey         AS DateKey
	          ,0                                                     AS NotificationsWithAttachmentsCount
	          ,0                                                     AS NotificationsWithoutAttachmentsCount
	          ,0                                                     AS AttachmentCount
	          ,0                                                     AS AttachmentSize
	          ,1                                                     AS CSVDownloadCount
	          ,RecHubUser.SessionActivityLog.ItemCount               AS CSVRowCount
	          ,RecHubData.dimBanks.BankName                          AS BankName
              ,RecHubData.dimClientAccounts.ShortName                AS WorkgroupName
              ,COALESCE(RecHubUser.OLWorkgroups.EntityID,'0')        AS EntityID       -- if 0 this workgroup is unassociated
		,COALESCE(RecHubUser.OLWorkgroups.ViewingDays,RecHubUser.OLEntities.ViewingDays)  AS ViewingDays
		,COALESCE(RecHubData.dimClientAccounts.DataRetentionDays,@DefaultRetentionDays)   AS RetentionDays
	FROM 
              RecHubUser.SessionActivityLog 
              INNER JOIN RecHubData.dimBanks 
                  ON RecHubUser.SessionActivityLog.BankID = RecHubData.dimBanks.SiteBankID
              INNER JOIN RecHubData.dimClientAccounts 
                  ON RecHubUser.SessionActivityLog.ClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
                         AND RecHubUser.SessionActivityLog.BankID = RecHubData.dimClientAccounts.SiteBankID
                         AND RecHubData.dimClientAccounts.MostRecent = 1 --because I am comparing on value, not key
              LEFT OUTER JOIN RecHubUser.OLWorkgroups 
                  ON RecHubUser.SessionActivityLog.BankID = RecHubUser.OLWorkgroups.SiteBankID
                           AND RecHubUser.SessionActivityLog.ClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
              LEFT OUTER JOIN RecHubUser.OLEntities 
                  ON RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		LEFT OUTER JOIN RecHubBilling.BillingWorkgroupExtractHistory
		    ON RecHubBilling.BillingWorkgroupExtractHistory.TableUsed = @TableUsed
			   AND RecHubBilling.BillingWorkgroupExtractHistory.TableKey = RecHubUser.SessionActivityLog.SessionActivityLogID
	WHERE   
		RecHubUser.SessionActivityLog.CreationDateKey >= @StartDateKey								
		AND RecHubUser.SessionActivityLog.CreationDateKey <= @EndDateKey	
		AND RecHubUser.SessionActivityLog.ActivityCode = 25
		AND RecHubData.dimBanks.MostRecent = 1   --because I am comparing on value not key
		AND RecHubUser.SessionActivityLog.BankID > 0
	    AND RecHubUser.SessionActivityLog.ClientAccountID > 0
		AND (RecHubBilling.BillingWorkgroupExtractHistory.BillingWorkgroupExtractHistoryKey IS NULL
		    OR @parmRerun = 1)  --  We will load only rows not previously extracted
		;    
	

	--  Result set for Data Flow in Billing.dtsx
    SELECT	
		BankID
		,BankName
		,ClientAccountID
		,WorkgroupName
		,EntityID
		,@StartDate                          AS StartDate
		,@EndDate                            AS EndDate
		,ViewingDays
		,RetentionDays
	    ,SUM(COALESCE(CSVDownloadCount,0))   AS CSVDownloadCount
	    ,SUM(COALESCE(CSVRowCount,0))        AS CSVRowCount
	FROM	
		BillingStaging.BillingWorkgroupExtractHistory
	GROUP BY  
		BankID,
		BankName,
		ClientAccountID,
		WorkgroupName,
		EntityID,
		ViewingDays,
		RetentionDays
	ORDER BY 
		BankID,
		ClientAccountID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
