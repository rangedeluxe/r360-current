--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_ExecuteScheduledTask
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_ExecuteScheduledTask') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_ExecuteScheduledTask
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_ExecuteScheduledTask 
(
	@parmTaskDescription	VARCHAR(50),
	@parmTaskId				VARCHAR(50),
	@parmTargetDateKey		INT = NULL,	-- yyyyMMdd as int
	@parmRecheckDays		INT = 5,	-- Recheck the last 5 days by default
	@parmExecuted			BIT OUTPUT	-- Return indicator of whether work was done
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure manages the metadata for executing a scheduled billing task
*
* Modification History
* 08/22/2014 WI 160871 RDS	Created
* 10/13/2014 WI 160871 RDS	Transaction validations
******************************************************************************/
SET NOCOUNT ON;

-- Acquire a lock, or return immediately.
DECLARE @now smalldatetime = GETDATE();
EXEC RecHubBilling.usp_Billing_ScheduledTaskLocks_Aquire @parmTaskDescription, @now, @parmExecuted OUTPUT;
IF @parmExecuted = 0
	RETURN;

DECLARE @ParentTransaction BIT = CASE WHEN @@TRANCOUNT = 0 THEN 0 ELSE 1 END;
BEGIN TRY

	-- Auto-select a specific SP instance for this task
	DECLARE @specificSproc VARCHAR(100) = RecHubBilling.fn_GetAccumulateSprocName(@parmTaskId);
	IF @specificSproc IS NULL
	BEGIN
		-- Note: This should never be NULL.  But best-practice is to do a NULL check on anything that will concatentated into dynamic SQL...
		DECLARE @msg VARCHAR(1000) = 'Scheduled Task ''' + ISNULL(@parmTaskId, '<NULL>') + ''' was not recognized';
		RAISERROR(@msg, 16, 1);
	END

	-- Convert date key to DATE, or set default
	DECLARE @targetDate DATE;
	IF (@parmTargetDateKey IS NULL)
		SET @targetDate = DATEADD(day, -1, GETDATE()); -- Default to yesterday
	ELSE
		SET @targetDate = CONVERT(DATE, CAST(@parmTargetDateKey AS VARCHAR(8)), 112);

	-- Determine relevant dates
	DECLARE @startDate DATE = DATEADD(day, -1, @targetDate);	-- start checking the day before our target date
	DECLARE @minDate DATE = DATEADD(month, -2, @targetDate);	-- max of 2 months ago
	DECLARE @minDateKey INT = CAST(CONVERT(varchar(8), @minDate, 112) AS INT);
	DECLARE @maxDateKey INT = CAST(CONVERT(varchar(8), @startDate, 112) AS INT);
	DECLARE @spSQL NVARCHAR(1000) = N'SELECT @dt = RecHubBilling.fn_Billing_' + @specificSproc + N'_LastCompletedDate(@parmMinDateKey, @parmMaxDateKey)';
	EXEC sp_executeSql @spSQL, N'@parmMinDateKey INT, @parmMaxDateKey INT, @dt DATE OUTPUT', @minDateKey, @maxDateKey, @startDate OUTPUT;
	
	-- Recheck a certain number of days prior; TODO: Should this consider "business dates"...?
	DECLARE @lastCompletedDate DATE = DATEADD(day, 0 - @parmRecheckDays, @startDate);


	-- Execute the specific task SP
	SET @spSQL = N'EXEC RecHubBilling.usp_Billing_' + @specificSproc + N' @parmLastCompletedDate, @parmTargetDate';
	EXEC sp_executeSql @spSQL, N'@parmLastCompletedDate	DATE, @parmTargetDate DATE', @lastCompletedDate, @targetDate;


	-- Set Last Run Time and Unlock
	EXEC RecHubBilling.usp_Billing_ScheduledTask_UpdateRunTime_Unlock @parmTaskDescription;

END TRY

BEGIN CATCH
	-- Verify a transaction was not left open
	IF @ParentTransaction = 0 AND @@TRANCOUNT > 0
	BEGIN
		ROLLBACK TRANSACTION;
	END

	-- Void the lock on error
	UPDATE RecHubBilling.ScheduledTaskLocks SET LockExpiration = @now WHERE TaskDescription = @parmTaskDescription;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH