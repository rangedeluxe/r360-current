--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulateActiveWorkgroups
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulateActiveWorkgroups') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulateActiveWorkgroups
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulateActiveWorkgroups 
(
	@parmLastCompletedDate	DATE = NULL,	-- This parameter is ignored
	@parmTargetDate			DATE = NULL		-- This parameter is ignored
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates statistics on Workgroup configuration
*			and stores them by month, for later use in generating billing files
*
* Modification History
* 09/02/2014 WI 171545 RDS	Created
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- Always use the first of the month, to avoid storing duplicates during the month...
-- Also: This is based on current data, so we must target the current month... (Target Date is ignored...)
DECLARE @targetDateKey INT;
SET @targetDateKey = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0), 112) AS INT);
	
-- Record (for this month) "applicable" workgroups that currently exist
MERGE INTO RecHubBilling.WorkgroupFlags
USING
(
	SELECT SiteBankID, SiteClientAccountID 'WorkgroupID', IsActive
	FROM RecHubData.dimClientAccountsView
	WHERE IsActive = 1
) Workgroups
ON		
		RecHubBilling.WorkgroupFlags.SiteBankID = Workgroups.SiteBankID 
	AND RecHubBilling.WorkgroupFlags.WorkgroupID = Workgroups.WorkgroupID
	AND RecHubBilling.WorkgroupFlags.DepositDateKey = @targetDateKey

WHEN MATCHED AND RecHubBilling.WorkgroupFlags.Active = 0
	THEN UPDATE SET RecHubBilling.WorkgroupFlags.Active = 1

WHEN NOT MATCHED BY TARGET
	THEN INSERT (DepositDateKey, SiteBankID, WorkgroupID, Active)
		VALUES (@targetDateKey, Workgroups.SiteBankID, Workgroups.WorkgroupID, 1);

