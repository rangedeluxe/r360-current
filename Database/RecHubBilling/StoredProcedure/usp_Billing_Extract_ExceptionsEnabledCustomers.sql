--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_ExceptionsEnabledCustomers
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_ExceptionsEnabledCustomers') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_ExceptionsEnabledCustomers
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_ExceptionsEnabledCustomers 
(
	@parmStartDateKey		INT,		-- yyyyMMdd as int
	@parmEndDateKey			INT,	  	-- yyyyMMdd as int
	@parmEntities			XML = null	-- <list><e id='*' /></list>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: 
*
* Modification History
* 05/07/2014 WI 137124 RDS	Created
* 12/15/2014 WI 137124 RDS	Support Entity-Level grouping and targeted FI Entities
*							And return Billing Fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Convert date keys to datetime variables
	--DECLARE @startDate DATETIME = CONVERT(DATETIME, CAST(@parmStartDateKey AS VARCHAR(8)), 112)
	DECLARE @endDate DATE = CONVERT(DATE, CAST(@parmEndDateKey AS VARCHAR(8)), 112)
	DECLARE @targetRowKey BIGINT;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxExceptionBatchKey FROM RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @parmEndDateKey), -1);
		IF EXISTS (SELECT * from RecHubException.ExceptionBatches WHERE DepositDateKey = @parmEndDateKey AND ExceptionBatchKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxIntegraPAYExceptionKey FROM RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @parmEndDateKey), -1);
		IF EXISTS (SELECT * from RecHubException.IntegraPAYExceptions WHERE DepositDateKey = @parmEndDateKey AND IntegraPAYExceptionKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is populated (can't verify up-to-date...) before continuing.
	DECLARE @monthDateKey INT = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, @endDate), 0), 112) AS INT);
	DECLARE @currentMonthDateKey INT = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0), 112) AS INT);
	IF NOT EXISTS (SELECT * FROM RecHubBilling.EntityFlags WHERE DepositDateKey = @monthDateKey)
	BEGIN
		IF @monthDateKey = @currentMonthDateKey
		BEGIN
			RAISERROR('Date %d was not found in EntityFlags', 16, 1,  @parmEndDateKey);
			RETURN;
		END
		ELSE
		BEGIN
			-- There is no way to get this data now; so ignore it...
			RAISERROR('Date %d was not found in EntityFlags - continuing without that data', 10, 1,  @monthDateKey) WITH NOWAIT;
		END
	END;

	-- Convert entity targets to a temporary table
	DECLARE @tblEntityTargets TABLE (EntityID INT PRIMARY KEY);
	IF (@parmEntities IS NOT NULL)
	BEGIN
		INSERT INTO @tblEntityTargets (EntityID)
		SELECT T.c.value('@id', 'int')
			FROM @parmEntities.nodes('/list/e') T(c)
			WHERE T.c.value('@id', 'varchar(20)') <> '*';
	END;

	IF NOT EXISTS(SELECT * FROM @tblEntityTargets)
	BEGIN
		-- No filter specified: allow all mapped FI Entity IDs
		INSERT INTO @tblEntityTargets (EntityID) SELECT DISTINCT EntityID FROM RecHubData.dimBanksView WHERE EntityID IS NOT NULL;
	END;

	-- Determine this list based on the presence of items during the time range, or was marked enabled for Exceptions
	WITH CTE_ExceptionItemWorkgroups AS
	(
		-- Return any workgroup that has stored an exception item stat during the time range
		SELECT DISTINCT RecHubBilling.ExceptionCounts.SiteBankID, RecHubUser.OLWorkgroups.EntityID
		FROM RecHubBilling.ExceptionCounts
		INNER JOIN RecHubUser.OLWorkgroups
			ON	RecHubBilling.ExceptionCounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
			AND RecHubBilling.ExceptionCounts.WorkgroupID = RecHubUser.OLWorkgroups.SiteClientAccountID
		WHERE DepositDateKey between @parmStartDateKey and @parmEndDateKey
	),
	CTE_OtherEntities AS
	(
		-- Return any entity that was marked enabled for exceptions in the month the range ends
		SELECT 0 'SiteBankID', RecHubBilling.EntityFlags.EntityID
		FROM RecHubBilling.EntityFlags
		LEFT JOIN CTE_ExceptionItemWorkgroups
			ON CTE_ExceptionItemWorkgroups.EntityID = RecHubBilling.EntityFlags.EntityID
		WHERE DepositDateKey = @monthDateKey and ExceptionsEnabled = 1
			AND CTE_ExceptionItemWorkgroups.EntityID IS NULL	-- Suppress duplicates; use the item data which includes SiteBankID...
	),
	CTE_AllEntities AS
	(
		SELECT SiteBankID, EntityID FROM CTE_ExceptionItemWorkgroups
		UNION ALL
		SELECT SiteBankID, EntityID FROM CTE_OtherEntities
	)
	SELECT ISNULL(RecHubData.dimBanksView.EntityID, 0) 'FIEntityID', CTE_AllEntities.SiteBankID, CTE_AllEntities.EntityID,
		0, '', RecHubUser.OLEntities.BillingAccount, RecHubUser.OLEntities.BillingField1, RecHubUser.OLEntities.BillingField2
	FROM CTE_AllEntities
	INNER JOIN RecHubUser.OLEntities
		ON	CTE_AllEntities.EntityID = RecHubUser.OLEntities.EntityID
	LEFT JOIN RecHubData.dimBanksView
		ON CTE_AllEntities.SiteBankID = RecHubData.dimBanksView.SiteBankID
		AND CTE_AllEntities.SiteBankID > 0
	LEFT JOIN @tblEntityTargets tblEntityTargets 
		ON tblEntityTargets.EntityID = RecHubData.dimBanksView.EntityID
	WHERE (RecHubData.dimBanksView.SiteBankID IS NULL OR tblEntityTargets.EntityID IS NOT NULL)
	ORDER BY FIEntityID, SiteBankID, EntityID;	-- Note: This order by ensures that if there are any unknown FI's, they come first...

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH