--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_StoreExceptionCustomers
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_StoreExceptionCustomers') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_StoreExceptionCustomers
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_StoreExceptionCustomers 
(
	@parmTaskDescription	VARCHAR(50),
	@parmEntities			XML				-- <list><c id="1" /><c id="2" /></list>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 09/11/2014
*
* Purpose: This procedure stores the entity IDs that were pulled from RAAM
*
* Modification History
* 09/11/2014 WI 171553 RDS	Created
* 01/15/2015 WI 171553 RDS	Fix Quoted Identifiers
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Always use the first of the month, to avoid storing duplicates during the month...
	-- Also: This is based on current data, so we must target the current month... (No Target Date is provided...)
	DECLARE @targetDateKey INT;
	SET @targetDateKey = CAST(CONVERT(varchar(8), DATEADD(month, DATEDIFF(month, 0, GETDATE()), 0), 112) AS INT);
	
	-- Record (for this month) "applicable" entities that have exception permissions
	MERGE INTO RecHubBilling.EntityFlags
	USING
	(
		SELECT T.c.value('@id', 'int') 'EntityID' FROM @parmEntities.nodes('/list/c') T(c)
	) Entities
	ON		
		RecHubBilling.EntityFlags.EntityID = Entities.EntityID

	WHEN MATCHED AND RecHubBilling.EntityFlags.ExceptionsEnabled = 0
		THEN UPDATE SET RecHubBilling.EntityFlags.ExceptionsEnabled = 1

	WHEN NOT MATCHED BY TARGET
		THEN INSERT (DepositDateKey, EntityID, ExceptionsEnabled)
			VALUES (@targetDateKey, Entities.EntityID, 1);

	-- Set Last Run Time and Unlock
	EXEC RecHubBilling.usp_Billing_ScheduledTask_UpdateRunTime_Unlock @parmTaskDescription;

END TRY

BEGIN CATCH
	-- Void the lock on error
	UPDATE RecHubBilling.ScheduledTaskLocks SET LockExpiration = GETDATE() WHERE TaskDescription = @parmTaskDescription;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH