--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Accumulate_Template
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Accumulate_Template') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Accumulate_Template
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Accumulate_Template 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates statistics and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 05/07/2014 WI        RDS	Created
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

-- This is a template - it will not install / run as is
-- $SummaryTable = PaymentCounts
-- $MonitorTable = $SummaryTable + 'Monitor'
-- $MaxFactKeyColumn = MaxFactPaymentKey (in Monitor table...)

-- $FactTable = factChecks (in RecHubData schema...)
-- $FactKeyColumn = factCheckKey (in Fact table...)
-- Summary columns must be updated as needed to match the summary keys


-- Aggregate values for each date in the range of dates missing / being rechecked
DECLARE @targetFactKey BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	SET @targetDateKey = CAST(CONVERT(VARCHAR(8), @loopDate, 112) AS INT);
		
	-- Check for the last key we summarized
	SET @targetFactKey = ISNULL((SELECT $MaxFactKeyColumn FROM RecHubBilling.$MonitorTable WHERE DepositDateKey = @targetDateKey), -1);
	IF EXISTS (SELECT * from RecHubData.$FactTable WHERE DepositDateKey = @targetDateKey AND $FactKeyColumn > @targetFactKey)
	BEGIN
		-- Data has been updated; recalculate

		-- Delete existing counts
		DELETE RecHubBilling.$MonitorTable WHERE DepositDateKey = @targetDateKey;
		DELETE RecHubBilling.$SummaryTable WHERE DepositDateKey = @targetDateKey;

		-- Determine new max key value
		SET @targetFactKey = ISNULL((SELECT TOP 1 $FactKeyColumn FROM RecHubData.$FactTable WHERE DepositDateKey = @targetDateKey ORDER BY $FactKeyColumn DESC), 0);

		-- Store counts to the summary table
		INSERT INTO RecHubBilling.$SummaryTable(DepositDateKey, SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey, ItemCount)
		SELECT @targetDateKey, BankKey, ClientAccountKey, BatchSourceKey, DDAKey, COUNT(*)
		FROM RecHubData.$FactTable
		INNER JOIN RecHubData.dimClientAccounts
			ON RecHubData.$FactTable.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE DepositDateKey = @targetDateKey and IsDeleted = 0
		GROUP BY SiteBankID, SiteClientAccountID, BatchSourceKey, DDAKey;

		-- Remember the max key value for what was summarized
		INSERT INTO RecHubBilling.$MonitorTable(DepositDateKey, $MaxFactKeyColumn) VALUES (@targetDateKey, @targetFactKey);
	END
	ELSE IF @targetFactKey = -1
	BEGIN
		-- No data; record that we checked
		INSERT INTO RecHubBilling.$MonitorTable(DepositDateKey, $MaxFactKeyColumn) VALUES (@targetDateKey, 0);
	END
END