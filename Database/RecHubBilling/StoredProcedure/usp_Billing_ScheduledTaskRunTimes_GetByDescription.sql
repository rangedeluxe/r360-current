--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_ScheduledTaskRunTimes_GetByDescription
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_ScheduledTaskRunTimes_GetByDescription') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_ScheduledTaskRunTimes_GetByDescription
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_ScheduledTaskRunTimes_GetByDescription 
(
	@parmTaskDescription	VARCHAR(50)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure retrieves the last stored runtime for a given scheduled task
*
* Modification History
* 08/22/2014 WI 160874 RDS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT LastRunAt
	FROM RecHubBilling.ScheduledTaskRunTimes
	WHERE TaskDescription = @parmTaskDescription;
		
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH