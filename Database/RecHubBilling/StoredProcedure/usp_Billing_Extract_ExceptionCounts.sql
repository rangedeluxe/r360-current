--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_Extract_ExceptionCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_Extract_ExceptionCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_Extract_ExceptionCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_Extract_ExceptionCounts
(
	@parmStartDateKey		INT,		-- yyyyMMdd as int
	@parmEndDateKey			INT,		-- yyyyMMdd as int
	@parmEntities			XML = null	-- <list><e id='*' /></list>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure returns counts for the number of Exceptions
*			per "entity"
*
* Modification History
* 05/07/2014 WI 137127 RDS	Created
* 12/15/2014 WI 137127 RDS	Support Entity-Level grouping and targeted FI Entities
*							And return Billing Fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Convert date keys to datetime variables (and add one day to end date to allow for time values)
	--DECLARE @startDate DATETIME = CONVERT(DATETIME, CAST(@parmStartDateKey AS VARCHAR(8)), 112)
	--DECLARE @endDate DATETIME = DATEADD(DAY, 1, CONVERT(DATETIME, CAST(@parmEndDateKey AS VARCHAR(8)), 112))
	DECLARE @targetRowKey BIGINT;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxExceptionBatchKey FROM RecHubBilling.ExceptionCountsPostProcessBatchMonitor WHERE DepositDateKey = @parmEndDateKey), -1);
		IF EXISTS (SELECT * from RecHubException.ExceptionBatches WHERE DepositDateKey = @parmEndDateKey AND ExceptionBatchKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END;

	-- This stored procedure uses a pre-populated summary table.  Verify the summary table is up-to-date before continuing.
	IF NOT EXISTS (SELECT * FROM RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @parmEndDateKey)
	BEGIN
		RAISERROR('Date %d was not found in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
		RETURN;
	END
	ELSE
	BEGIN
		-- Check for the last key to make sure all counts are current (last date only); TODO: Should this consider business dates or check multiple dates?
		SET @targetRowKey = ISNULL((SELECT MaxIntegraPAYExceptionKey FROM RecHubBilling.ExceptionCountsIntegraPAYMonitor WHERE DepositDateKey = @parmEndDateKey), -1);
		IF EXISTS (SELECT * from RecHubException.IntegraPAYExceptions WHERE DepositDateKey = @parmEndDateKey AND IntegraPAYExceptionKey > @targetRowKey)
		BEGIN
			-- Summary data needs updating
			RAISERROR('Date %d totals are not current in ExceptionCountsPostProcessBatchMonitor', 16, 1,  @parmEndDateKey);
			RETURN;
		END
	END;

	-- This stored procedure uses a pre-populated summary table: ExceptionCountsAuditMonitor.  
	-- However, there is no way to verify this table is up to date, so assume previous validations are sufficient...

	-- Convert entity targets to a temporary table
	DECLARE @tblEntityTargets TABLE (EntityID INT PRIMARY KEY);
	IF (@parmEntities IS NOT NULL)
	BEGIN
		INSERT INTO @tblEntityTargets (EntityID)
		SELECT T.c.value('@id', 'int')
			FROM @parmEntities.nodes('/list/e') T(c)
			WHERE T.c.value('@id', 'varchar(20)') <> '*';
	END;

	IF NOT EXISTS(SELECT * FROM @tblEntityTargets)
	BEGIN
		-- No filter specified: allow all mapped FI Entity IDs
		INSERT INTO @tblEntityTargets (EntityID) SELECT DISTINCT EntityID FROM RecHubData.dimBanksView WHERE EntityID IS NOT NULL;
	END;

	-- Select the pre-accumulated counts
	SELECT RecHubData.dimBanksView.EntityID 'FIEntityID', 
			0 'LogonEntityID', 
			RecHubBilling.ExceptionCounts.SiteBankID, 
			RecHubUser.OLWorkgroups.EntityID,
			RecHubUser.OLWorkgroups.SiteClientAccountID,
			RecHubData.dimClientAccountsView.LongName,
			ISNULL(RecHubUser.OLWorkgroups.BillingAccount, RecHubUser.OLEntities.BillingAccount) 'BillingAccount',
			ISNULL(RecHubUser.OLWorkgroups.BillingField1, RecHubUser.OLEntities.BillingField1) 'BillingField1',
			ISNULL(RecHubUser.OLWorkgroups.BillingField2, RecHubUser.OLEntities.BillingField2) 'BillingField2',
			SUM(TransactionCount), SUM(AcceptedTransactions), SUM(AcceptedNoChanges), SUM(RejectedTransactions), 
			SUM(PaymentItemCount) + SUM(StubItemCount) + SUM(CASE WHEN ExceptionType = 2 THEN UpdatedItemCount + AddedItemCount + DeletedItemCount ELSE 0 END), 
			SUM(UpdatedItemCount), SUM(AddedItemCount), SUM(DeletedItemCount)
	FROM RecHubBilling.ExceptionCounts -- Use the pre-populated summary table for performance 
	INNER JOIN RecHubData.dimBanksView
		ON RecHubBilling.ExceptionCounts.SiteBankID = RecHubData.dimBanksView.SiteBankID
	INNER JOIN @tblEntityTargets tblEntityTargets 
		ON tblEntityTargets.EntityID = RecHubData.dimBanksView.EntityID
	INNER JOIN RecHubUser.OLWorkgroups
		ON RecHubBilling.ExceptionCounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
		AND RecHubBilling.ExceptionCounts.WorkgroupID = RecHubUser.OLWorkgroups.SiteClientAccountID
	INNER JOIN RecHubData.dimClientAccountsView
		ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
		AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
	INNER JOIN RecHubUser.OLEntities
		ON	RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
	WHERE RecHubBilling.ExceptionCounts.DepositDateKey >= @parmStartDateKey
		AND RecHubBilling.ExceptionCounts.DepositDateKey <= @parmEndDateKey
	GROUP BY RecHubData.dimBanksView.EntityID, 
			RecHubBilling.ExceptionCounts.SiteBankID,
			RecHubUser.OLWorkgroups.EntityID,
			RecHubUser.OLWorkgroups.SiteClientAccountID,
			RecHubData.dimClientAccountsView.LongName,
			RecHubUser.OLWorkgroups.BillingAccount, RecHubUser.OLEntities.BillingAccount,
			RecHubUser.OLWorkgroups.BillingField1, RecHubUser.OLEntities.BillingField1,
			RecHubUser.OLWorkgroups.BillingField2, RecHubUser.OLEntities.BillingField2;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH