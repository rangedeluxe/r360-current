--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Get_CSVBillingExtract
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_factNotifications_Get_CSVBillingExtract') IS NOT NULL
	   DROP PROCEDURE RecHubBilling.usp_factNotifications_Get_CSVBillingExtract;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_factNotifications_Get_CSVBillingExtract 
(
	@parmDepositDateStart	DATETIME,
	@parmDepositDateEnd		DATETIME,
	@parmRerun				BIT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	MGE
* Date:		05/04/2017
*
* Purpose: Query factNotifications fact table for Billing Extract process for Date range supplied.
*
* Modification History
* 05/04/2017 PT #139494807 MGE	Create 
* 05/10/2017 PT #139494807 MGE	Filter out system level alerts (SiteBankID = -1)
* 05/16/2017 PT #139494807 MGE	Add NotificationFile information to BillingFITExtractHistory to eliminate re-imports from counts
* 05/23/2017 PT #139494807 MGE	Fix cartesion expansion for simple file drops by include NotificatonMessageGroup in join
***********************************************************************************************************************************/
SET NOCOUNT ON 

DECLARE @StartDateKey	INT,	 
        @EndDateKey		INT,
		@StartDate		VARCHAR(10),
		@EndDate		VARCHAR(10),
		@DefaultRetentionDays	INT,
		@TableUsed      VARCHAR(256) = 'factNotifications',
		@tempCount		INT,
		@Loop			INT,
		@InnerCount		INT,
		@FileList		VARCHAR(2048);

DECLARE @tempNotificationFiles TABLE 
			(RowID INT identity (1,1) NOT NULL, 
			BankKey INT, ClientAccountKey INT, 
			NotificationDateKey INT, 
			UserFileName VARCHAR(255), 
			FileExtension VARCHAR(24), 
			NotificationMessageGroup bigint,
			NotificationFileCount int,
			SourceNotificationID UNIQUEIDENTIFIER);

DECLARE @ConcatenatedNotificationFiles TABLE 
			(SiteBankID INT, SiteClientAccountID INT, NotificationDateKey INT, SourceNotificationID UNIQUEIDENTIFIER, FileList VARCHAR(2048), NotificationMessageGroup BIGINT);	

BEGIN TRY

	/*  Convert incoming values			*/
	SELECT 
		@StartDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateStart,112) AS INT),
		@EndDateKey = CAST(CONVERT(VARCHAR,@parmDepositDateEnd,112) AS INT),
		@StartDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateStart,101)AS VARCHAR(10)),'/',''),
		@EndDate = REPLACE(CAST(CONVERT(VARCHAR,@parmDepositDateEnd,101) AS VARCHAR(10)),'/','');

	--We clear out the work table in the Control Flow of the Billing SSIS Package

	SELECT 
		@DefaultRetentionDays = Value
	FROM 
		RecHubConfig.SystemSetup
	WHERE 
		Section = 'Rec Hub Table Maintenance' 
		AND SetupKey = 'DefaultDataRetentionDays';
	
	INSERT INTO @tempNotificationFiles
	SELECT RecHubData.factNotifications.BankKey, 
	RecHubData.factNotifications.ClientAccountKey, 
	RecHubData.factNotifications.NotificationDateKey, 
	UserFileName, 
	FileExtension, 
	RecHubData.factNotifications.NotificationMessageGroup, 
	NotificationFileCount,
	RecHubData.factNotifications.SourceNotificationID
	FROM RecHubData.factNotificationFiles
	INNER JOIN RecHubData.factNotifications
		ON RecHubData.factNotifications.SourceNotificationID = RecHubData.factNotificationFiles.SourceNotificationID
		AND RecHubData.factNotifications.NotificationMessageGroup = RecHubData.factNotificationFiles.NotificationMessageGroup
		AND RecHubData.factNotifications.NotificationDateKey = RecHubData.factNotificationFiles.NotificationDateKey
	WHERE RecHubData.factNotificationFiles.NotificationDateKey >= @StartDateKey
	AND RecHubData.factNotificationFiles.NotificationDateKey <= @EndDateKey
	AND RecHubData.factNotificationFiles.IsDeleted = 0;

	SELECT @tempCount = @@ROWCOUNT
	--select * from @tempNotificationFiles			--DEBUG ONLY
	SET @Loop = 1
	SET @InnerCount = 0
	SET @FileList = ''

	WHILE @Loop <= @tempCount 
	BEGIN
		SELECT @InnerCount= @Loop + NotificationFileCount -1 from @tempNotificationFiles where RowID = @Loop;
		
		WHILE @Loop <= @InnerCount
		BEGIN
			SELECT @FileList = @FileList + UserFileName + FileExtension from @tempNotificationFiles where RowID = @Loop;
			SET @Loop = @Loop + 1;
		END

		INSERT INTO @ConcatenatedNotificationFiles
		SELECT SiteBankID, SiteClientAccountID, NotificationDateKey, SourceNotificationID, @FileList, NotificationMessageGroup
		FROM @tempNotificationFiles tnf
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = tnf.ClientAccountKey
		WHERE RowID = @Loop -1

		SET @FileList = ''
	END
	
	INSERT INTO BillingStaging.BillingFITExtractHistory  WITH (TABLOCK)
	(
		BankID,
        ClientAccountID,
        TableUsed,
        TableKey,
        NotificationsWithAttachmentsCount,
        NotificationsWithoutAttachmentsCount,
        AttachmentCount,
        AttachmentSize,
        CSVDownloadCount,
        CSVRowCount,
        BankName,
        EntityID,
		DateKey,
		ViewingDays,
		RetentionDays,
		FileList
	) 	
	SELECT	
		RecHubData.dimClientAccounts.SiteBankID					AS BankID,
        RecHubData.dimClientAccounts.SiteClientAccountID		AS ClientAccountID,
	    @TableUsed												AS TableUsed,
	    RecHubData.factNotifications.factNotificationKey	    AS TableKey,
		CASE NotificationFileCount
		WHEN
			0 THEN 0
			ELSE 1
		END														AS NotificationsWithAttachmentsCount,
	    CASE NotificationFileCount
		WHEN
			0 THEN 1
			ELSE 0
		END														AS NotificationsWithoutAttachmentsCount,
	    NotificationFileCount										AS AttachmentCount,
		COALESCE((Select sum(FileSize) FROM RecHubData.factNotificationFiles 
			WHERE RecHubData.factNotificationFiles.SourceNotificationID = RecHubData.factNotifications.SourceNotificationID
			AND RecHubData.factNotifications.NotificationFileCount > 0
			AND RecHubData.factNotificationFiles.IsDeleted = 0),0) AS AttachmentSize,
	    0															AS CVSDownloadCount,
		0														AS CVSRowCount,
		RecHubData.dimBanks.BankName							AS BankName,
        COALESCE(RecHubUser.OLWorkgroups.EntityID,'0')			AS EntityID,       -- if 0 this workgroup is unassociated
		RecHubData.factNotifications.NotificationDateKey		AS DateKey,	
		COALESCE(RecHubUser.OLWorkgroups.ViewingDays,RecHubUser.OLEntities.ViewingDays)  AS ViewingDays,
		COALESCE(RecHubData.dimClientAccounts.DataRetentionDays,@DefaultRetentionDays)   AS RetentionDays,
		ConcatenatedNotificationFiles.FileList
	FROM 
        RecHubData.factNotifications
		INNER JOIN RecHubData.dimBanks 
            ON RecHubData.factNotifications.BankKey = RecHubData.dimBanks.BankKey
        INNER JOIN RecHubData.dimClientAccounts 
            ON RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		LEFT JOIN @ConcatenatedNotificationFiles ConcatenatedNotificationFiles
			ON ConcatenatedNotificationFiles.SourceNotificationID = RecHubData.factNotifications.SourceNotificationID
			AND ConcatenatedNotificationFiles.NotificationDateKey = RecHubData.factNotifications.NotificationDateKey
			AND ConcatenatedNotificationFiles.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND ConcatenatedNotificationFiles.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND ConcatenatedNotificationFiles.NotificationMessageGroup = RecHubData.factNotifications.NotificationMessageGroup
        LEFT OUTER JOIN RecHubUser.OLWorkgroups 
            ON RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
                        AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
        LEFT OUTER JOIN RecHubUser.OLEntities 
            ON RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		LEFT OUTER JOIN RecHubBilling.BillingFITExtractHistory
			ON (RecHubBilling.BillingFITExtractHistory.TableUsed = @TableUsed
				AND RecHubBilling.BillingFITExtractHistory.TableKey = RecHubData.factNotifications.factNotificationKey)
			OR (RecHubBilling.BillingFITExtractHistory.TableUsed = @TableUsed
				AND RecHubBilling.BillingFITExtractHistory.TableKey <> RecHubData.factNotifications.factNotificationKey
				AND RecHubBilling.BillingFITExtractHistory.DateKey = ConcatenatedNotificationFiles.NotificationDateKey
				AND RecHubData.factNotifications.NotificationFileCount > 0
				AND RecHubData.factNotifications.IsDeleted = 0
				AND RecHubBilling.BillingFITExtractHistory.FileList = ConcatenatedNotificationFiles.FileList
				AND RecHubBilling.BillingFITExtractHistory.BankID = ConcatenatedNotificationFiles.SiteBankID
				AND RecHubBilling.BillingFITExtractHistory.ClientAccountID = ConcatenatedNotificationFiles.SiteClientAccountID
				)
	WHERE   
		RecHubData.factNotifications.NotificationDateKey >= @StartDateKey								
		AND RecHubData.factNotifications.NotificationDateKey <= @EndDateKey	
		AND RecHubData.factNotifications.NotificationMessagePart = 1
		AND RecHubData.factNotifications.IsDeleted = 0
		AND RecHubData.dimBanks.SiteBankID <> -1
		AND (RecHubBilling.BillingFITExtractHistory.BillingFITExtractHistoryKey IS NULL
			OR @parmRerun = 1);    --  We will load only rows not previously extracted

	--  Result set for Data Flow in Billing.dtsx
    SELECT	
		BankID,
		BankName,
		ClientAccountID,
		dimClientAccountsView.ShortName		AS WorkgroupName,
		EntityID,
		@StartDate                          AS StartDate,
		@EndDate                            AS EndDate,
		SUM(COALESCE(NotificationsWithAttachmentsCount,0))		AS NotificationsWithAttachmentsCount,
	    SUM(COALESCE(NotificationsWithoutAttachmentsCount,0))  AS NotificationsWithoutAttachmentsCount,
	    SUM(COALESCE(AttachmentCount,0))						AS AttachmentCount,
	    SUM(COALESCE(AttachmentSize,0))						AS AttachmentSize,
		MAX(COALESCE(ViewingDays,0))							AS ViewingDays,
		MAX(COALESCE(RecHubData.dimClientAccountsView.DataRetentionDays,@DefaultRetentionDays))	AS RetentionDays
	FROM	
		BillingStaging.BillingFITExtractHistory
		INNER JOIN RecHubData.dimClientAccountsView ON dimClientAccountsView.SiteBankID = BillingStaging.BillingFITExtractHistory.BankID
			AND dimClientAccountsView.SiteClientAccountID = BillingStaging.BillingFITExtractHistory.ClientAccountID
	GROUP BY  
		BankID,
		BankName,
		ClientAccountID,
		ShortName,
		EntityID
	ORDER BY 
		BankID,
		ClientAccountID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
