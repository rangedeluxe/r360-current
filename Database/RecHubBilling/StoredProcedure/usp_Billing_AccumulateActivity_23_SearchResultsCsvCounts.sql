--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_AccumulateActivity_23_SearchResultsCsvCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_AccumulateActivity_23_SearchResultsCsvCounts') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_AccumulateActivity_23_SearchResultsCsvCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_AccumulateActivity_23_SearchResultsCsvCounts 
(
	@parmLastCompletedDate	DATE,
	@parmTargetDate			DATE
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure accumulates billing record counts from the audit tables and stores them by
*			date, for later use in generating billing files
*
* Modification History
* 09/15/2014 WI 171548 RDS	Created
******************************************************************************/

-- Note: Errors must be handled by the calling stored procedure (usp_Billing_ExecuteScheduledTask)

DECLARE @tblBulkFileRecordCounts TABLE (LogonEntityID INT NOT NULL, SiteBankID INT NOT NULL, WorkgroupID INT NOT NULL, 
                                        FileCount INT NOT NULL, ItemCount INT NOT NULL, MaxID BIGINT NOT NULL);

-- Aggregate values for each date in the range of dates missing
DECLARE @targetSessionActivityLogID BIGINT;
DECLARE @targetDateKey INT;
DECLARE @loopDate DATE = @parmLastCompletedDate;
WHILE @loopDate < @parmTargetDate
BEGIN
	-- Increment date
	SET @loopDate = DATEADD(day, 1, @loopDate);
	set @targetDateKey = CAST(CONVERT(varchar(8), @loopDate, 112) as INT);
	
	-- Do not attempt to recalculate values...
	IF NOT EXISTS (SELECT * FROM RecHubBilling.ActivityCounts_23_SearchResultsCsvMonitor WHERE ProcessingDateKey = @targetDateKey)
	BEGIN
		-- Check for the last key
		SET @targetSessionActivityLogID = ISNULL((SELECT TOP 1 MaxSessionActivityLogID 
													FROM RecHubBilling.ActivityCounts_23_SearchResultsCsvMonitor 
													WHERE ProcessingDateKey < @targetDateKey
													ORDER BY ProcessingDateKey DESC), 0);

		-- Accumulate values for the given date
		DELETE @tblBulkFileRecordCounts;
		INSERT INTO @tblBulkFileRecordCounts(LogonEntityID, SiteBankID, WorkgroupID, FileCount, ItemCount, MaxID)
		SELECT LogonEntityID, BankID, ClientAccountID, COUNT(SessionActivityLogID), SUM(ItemCount), MAX(SessionActivityLogID)
		FROM RecHubUser.SessionActivityLog
		INNER JOIN RecHubUser.Session
			ON RecHubUser.Session.SessionID = RecHubUser.SessionActivityLog.SessionID
		INNER JOIN RecHubUser.Users
			ON RecHubUser.Session.UserID = RecHubUser.Users.UserID
		WHERE ProcessingDate = @loopDate 
			AND ActivityCode = 23
			AND SessionActivityLogID > @targetSessionActivityLogID -- Hint for index range scan if table size gets large enough...
		GROUP BY LogonEntityID, BankID, ClientAccountID;

		-- Insert into accumulation table
		INSERT INTO RecHubBilling.ActivityCounts_23_SearchResultsCsv(ProcessingDateKey, LogonEntityID, SiteBankID, WorkgroupID, FileCount, ItemCount)
		SELECT @targetDateKey, LogonEntityID, SiteBankID, WorkgroupID, FileCount, ItemCount
		FROM @tblBulkFileRecordCounts;

		-- Insert status / index hint into monitor table
		INSERT INTO RecHubBilling.ActivityCounts_23_SearchResultsCsvMonitor(ProcessingDateKey, MaxSessionActivityLogID) 
		SELECT @targetDateKey, ISNULL(MAX(MaxID), @targetSessionActivityLogID)
		FROM @tblBulkFileRecordCounts;
	END
END