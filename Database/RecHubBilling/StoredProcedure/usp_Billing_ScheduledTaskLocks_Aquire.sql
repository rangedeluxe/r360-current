--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorStoredProcedureName usp_Billing_ScheduledTaskLocks_Aquire
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubBilling.usp_Billing_ScheduledTaskLocks_Aquire') IS NOT NULL
    DROP PROCEDURE RecHubBilling.usp_Billing_ScheduledTaskLocks_Aquire
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubBilling.usp_Billing_ScheduledTaskLocks_Aquire 
(
	@parmTaskDescription	VARCHAR(50),
	@parmNow				SMALLDATETIME = NULL,
	@parmExecuted			BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/11/2014
*
* Purpose: This procedure updates the lock table to acquire a lock
*
* Modification History
* 08/22/2014 WI 160872 RDS	Created
* 09/11/2014 WI 160872 RDS	Include permissions, to allow calling from code,
							and can default the date
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	-- Prevent duplicate runs from multiple servers in a high-availability configuration
	IF @parmNow IS NULL SET @parmNow = GETDATE();

	DECLARE @lockExpiration smalldatetime = DATEADD(hour, 1, @parmNow);				-- If locked - our lock will expire in 1 hour
	MERGE INTO RecHubBilling.ScheduledTaskLocks L
		USING (SELECT @parmTaskDescription TaskDescription) S
			ON L.TaskDescription = S.TaskDescription

		WHEN MATCHED AND L.LockExpiration <= @parmNow THEN							-- If existing lock has expired ...
			UPDATE SET LockedBy = HOST_NAME(), LockExpiration = @lockExpiration		--	... Update row to acquire lock

		WHEN NOT MATCHED BY TARGET THEN												-- If no row for this task type ...
			INSERT (TaskDescription, LockedBy, LockExpiration)						--	... Insert a row, as "locked"
			VALUES (S.TaskDescription, HOST_NAME(), @lockExpiration);

	SET @parmExecuted = CASE @@ROWCOUNT WHEN 0 THEN 0 ELSE 1 END;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH