--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorFunctionName fn_Billing_TestWithDelay_LastCompletedDate
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubBilling.fn_Billing_TestWithDelay_LastCompletedDate') IS NOT NULL
       DROP Function RecHubBilling.fn_Billing_TestWithDelay_LastCompletedDate
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubBilling.fn_Billing_TestWithDelay_LastCompletedDate
(
	@parmMinDateKey INT, 
	@parmMaxDateKey INT
)
RETURNS  DATE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/20/2014
*
* Purpose: Return the last date (in the range) that the accumulation task ran for
*
*
* Modification History
* 08/20/2014 WI       RDS	Created
******************************************************************************/
BEGIN
	DECLARE @dt DATE = CONVERT(DATE, CAST(@parmMinDateKey AS VARCHAR(8)), 112);

	-- This is for development integration testing.  It will not be released to QA.

	RETURN @dt;
END

-- GRANT EXECUTE ON RecHubBilling.fn_Billing_TestWithDelay_LastCompletedDate TO dbRole_RecHubBilling