--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubBilling">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorFunctionName fn_GetAccumulateSprocName
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubBilling.fn_GetAccumulateSprocName') IS NOT NULL
       DROP Function RecHubBilling.fn_GetAccumulateSprocName
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubBilling.fn_GetAccumulateSprocName
(
	@parmTaskId		 VARCHAR(50)
)
RETURNS  VARCHAR(100)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 08/20/2014
*
* Purpose: Return the name of the stored procedure that performs the accumulation task
*
*
* Modification History
* 08/20/2014 WI 160870 RDS	Created
* 09/15/2014 WI 160870 RDS	More / Revised Names
******************************************************************************/
BEGIN
	DECLARE @sprocName VARCHAR(100);

	SELECT @sprocName = CASE @parmTaskId
		WHEN 'SummarizePaymentFacts'						THEN 'AccumulatePaymentCounts'
		WHEN 'SummarizeStubFacts'							THEN 'AccumulateStubCounts'
		WHEN 'SummarizeDocumentFacts'						THEN 'AccumulateDocumentCounts'

		WHEN 'SummarizeActiveWorkgroups'					THEN 'AccumulateActiveWorkgroups'

		WHEN 'SummarizeBulkFileRecordCounts_PrintView'		THEN 'AccumulateActivity_21_SearchResultsPrintViewCounts'
		WHEN 'SummarizeBulkFileRecordCounts_PdfView'		THEN 'AccumulateActivity_22_SearchResultsPdfViewCounts'
		WHEN 'SummarizeBulkFileRecordCounts_Csv'			THEN 'AccumulateActivity_23_SearchResultsCsvCounts'
		WHEN 'SummarizeBulkFileRecordCounts_ZipDownload'	THEN 'AccumulateActivity_24_SearchResultsZipDownloadCounts'
		WHEN 'SummarizeBulkFileRecordCounts_DownloadCsv'	THEN 'AccumulateActivity_25_SearchResultsDownloadCsvCounts'
		WHEN 'SummarizeBulkFileRecordCounts_DownloadXml'	THEN 'AccumulateActivity_26_SearchResultsDownloadXmlCounts'
		WHEN 'SummarizeBulkFileRecordCounts_DownloadHtml'	THEN 'AccumulateActivity_27_SearchResultsDownloadHtmlCounts'

		WHEN 'SummarizeExceptionCounts'						THEN 'AccumulateExceptionCounts'

		ELSE @parmTaskId
	END;

	RETURN @sprocName;
END