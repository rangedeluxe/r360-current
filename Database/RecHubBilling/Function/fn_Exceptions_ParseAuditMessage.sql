--WFSScriptProcessorSchema RecHubBilling
--WFSScriptProcessorFunctionName fn_Exceptions_ParseAuditMessage
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubBilling.fn_Exceptions_ParseAuditMessage') IS NOT NULL
       DROP Function RecHubBilling.fn_Exceptions_ParseAuditMessage
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubBilling.fn_Exceptions_ParseAuditMessage
(
	@auditMessage		 VARCHAR(8000)
)
RETURNS @Fields TABLE (DecisioningTransactionKey BIGINT NOT NULL, AddedItems INT NOT NULL, UpdatedItems INT NOT NULL, DeletedItems INT NOT NULL)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 09/17/2014
*
* Purpose: Parse the audit message to extract the IDs and Counts
*
* Modification History
* 09/17/2014 WI 171541 RDS	Created
******************************************************************************/
BEGIN
	DECLARE @indexStart INT,
			@indexEnd INT,
			@field VARCHAR(20);

	SET @indexStart = CHARINDEX('Exception ID: ', @auditMessage);
	IF (@indexStart > 0) -- Found it
	BEGIN
		-- Decisioning Transaction ID
		SET @indexStart = @indexStart + LEN('Exception ID: ');
		SET @indexEnd = CHARINDEX(',', @auditMessage, @indexStart);
		SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
		INSERT INTO @Fields (DecisioningTransactionKey, AddedItems, UpdatedItems, DeletedItems) VALUES (@field, 0, 0, 0);

		-- Updated Payments (Post Process)
		SET @indexEnd = CHARINDEX(' payments(s) updated;', @auditMessage);
		IF (@indexEnd > 0)
		BEGIN
			SET @indexStart = CHARINDEX(' - ', @auditMessage, @indexEnd - 10) + LEN(' - ');
			SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
			UPDATE @Fields SET UpdatedItems = UpdatedItems + CAST(@field AS INT);
		END

		-- Updated Stubs (Post Process)
		SET @indexEnd = CHARINDEX(' stub(s) updated;', @auditMessage);
		IF (@indexEnd > 0)
		BEGIN
			SET @indexStart = CHARINDEX(' - ', @auditMessage, @indexEnd - 10) + LEN(' - ');
			SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
			UPDATE @Fields SET UpdatedItems = UpdatedItems + CAST(@field AS INT);
		END

		-- TODO: Adds / Deletes for Post Process?

		-- Update Items (In Process)
		SET @indexEnd = CHARINDEX(' item(s) updated', @auditMessage);
		IF (@indexEnd > 0)
		BEGIN
			SET @indexStart = CHARINDEX(' - ', @auditMessage, @indexEnd - 10) + LEN(' - ');
			SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
			UPDATE @Fields SET UpdatedItems = UpdatedItems + CAST(@field AS INT);
		END

		-- Inserted Items (In Process)
		SET @indexEnd = CHARINDEX(' item(s) inserted', @auditMessage);
		IF (@indexEnd > 0)
		BEGIN
			SET @indexStart = CHARINDEX(' - ', @auditMessage, @indexEnd - 10) + LEN(' - ');
			SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
			UPDATE @Fields SET AddedItems = AddedItems + CAST(@field AS INT);
		END

		-- Deleted Items (In Process)
		SET @indexEnd = CHARINDEX(' item(s) deleted', @auditMessage);
		IF (@indexEnd > 0)
		BEGIN
			SET @indexStart = CHARINDEX(' - ', @auditMessage, @indexEnd - 10) + LEN(' - ');
			SET @field = SUBSTRING(@auditMessage, @indexStart, @indexEnd - @indexStart);
			UPDATE @Fields SET DeletedItems = DeletedItems + CAST(@field AS INT);
		END
	END

	RETURN;
END