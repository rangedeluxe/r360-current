--WFSScriptProcessorSchema SSISStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_GetWorkClientSetupResponses
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SSISStaging.usp_DataImportQueue_GetWorkClientSetupResponses') IS NOT NULL
       DROP PROCEDURE SSISStaging.usp_DataImportQueue_GetWorkClientSetupResponses
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [SSISStaging].[usp_DataImportQueue_GetWorkClientSetupResponses] 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Get all pending client setup responses.
*
* Modification History
* 01/24/2012 CR 50819 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY

	SELECT	DISTINCT ClientTrackingID,
			CASE ResponseStatus
				WHEN 1 THEN REPLACE(REPLACE(Responses.ResponseXML,'&lt;','<'),'&gt;','>')
			END AS ErrorMessage
	FROM	SSISStaging.DataImportWorkClientSetupResponses
	CROSS APPLY
	(
		SELECT	CASE ResponseStatus
					WHEN 0 THEN NULL
					WHEN 1 THEN '<ErrorMessage>' + ResultsMessage + '</ErrorMessage>'
				END AS [text()]
		FROM SSISStaging.DataImportWorkClientSetupResponses Msg
		WHERE Msg.ClientTrackingID = SSISStaging.DataImportWorkClientSetupResponses.ClientTrackingID
		FOR XML PATH('')
	) Responses(ResponseXML);
	
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH