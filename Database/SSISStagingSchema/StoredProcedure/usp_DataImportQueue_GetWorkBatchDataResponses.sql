--WFSScriptProcessorSchema SSISStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_GetWorkBatchDataResponses
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('SSISStaging.usp_DataImportQueue_GetWorkBatchDataResponses') IS NOT NULL
       DROP PROCEDURE SSISStaging.usp_DataImportQueue_GetWorkBatchDataResponses
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [SSISStaging].[usp_DataImportQueue_GetWorkBatchDataResponses] 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/26/2012
*
* Purpose: Get all pending batch responses.
*
* Modification History
* 01/26/2012 CR 50818 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY
	SELECT DISTINCT BatchTrackingID,
				CASE ResponseStatus
					WHEN 1 THEN REPLACE(REPLACE(Responses.ResponseXML,'&lt;','<'),'&gt;','>')
				END AS ErrorMessage
		FROM	SSISStaging.DataImportWorkBatchResponses
		CROSS APPLY
		(
			SELECT 
				CASE ResponseStatus
					WHEN 0 THEN NULL
					WHEN 1 THEN '<ErrorMessage>' + ResultsMessage + '</ErrorMessage>'
					WHEN 2 THEN NULL
				END AS [text()]
			FROM SSISStaging.DataImportWorkBatchResponses Msg
			WHERE Msg.BatchTrackingID = SSISStaging.DataImportWorkBatchResponses.BatchTrackingID
			FOR XML PATH('')
		) Responses(ResponseXML);
END TRY
BEGIN CATCH
	EXEC OLTA.usp_WfsRethrowException;
END CATCH