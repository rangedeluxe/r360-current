--WFSScriptProcessorPrint CR 51289
--WFSScriptProcessorPrint Removing obsolete SSIS stored procedures if necessary.
--WFSScriptProcessorCRBegin
IF OBJECT_ID('SSISStaging.usp_DataImportQueue_GetWorkBatchDataResponses') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure SSISStaging.usp_DataImportQueue_GetWorkBatchDataResponses.',10,1) WITH NOWAIT
	DROP PROCEDURE SSISStaging.usp_DataImportQueue_GetWorkBatchDataResponses
END
IF OBJECT_ID('SSISStaging.usp_DataImportQueue_GetWorkClientSetupResponses') IS NOT NULL
BEGIN
	RAISERROR('Dropping stored procedure SSISStaging.usp_DataImportQueue_GetWorkClientSetupResponses.',10,1) WITH NOWAIT
	DROP PROCEDURE SSISStaging.usp_DataImportQueue_GetWorkClientSetupResponses
END
--WFSScriptProcessorCREnd