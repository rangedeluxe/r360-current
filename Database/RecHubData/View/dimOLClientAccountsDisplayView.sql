--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimOLClientAccountsDisplayView
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimOLClientAccountsDisplayView') IS NOT NULL
	DROP VIEW RecHubData.dimOLClientAccountsDisplayView
GO

CREATE VIEW RecHubData.dimOLClientAccountsDisplayView
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CMC
* Date: 11/06/2013
*
* Purpose: View of the elements required to create the Client Account Name 
*
*
* Modification History
* 11/06/2013 WI 121896 CMC	Created
* 11/14/2013 WI 122564 CMC	Fixed Display Label to handle Null Long Name
******************************************************************************/

SELECT 
	Rechubuser.OLClientAccounts.OLOrganizationID,
	Rechubuser.OLClientAccounts.OLClientAccountID,
	RecHubUser.OLClientAccounts.SiteClientAccountID,
	RecHubUser.OLClientAccounts.SiteBankID,
	RecHubData.dimClientAccountsView.DDA,
	RecHubData.dimClientAccountsView.LongName,
	RecHubUser.OLClientAccounts.DisplayName,
	CAST(RecHubUser.OLClientAccounts.SiteClientAccountID AS VARCHAR) + 
		COALESCE(' (' + RTRIM(RecHubData.dimClientAccountsView.DDA) + ')', ' (N/A)') +
		CASE
			WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR RecHubUser.OLClientAccounts.DisplayName = ''
				THEN 
					CASE
						WHEN RecHubData.dimClientAccountsView.LongName IS NULL OR RecHubData.dimClientAccountsView.LongName = ''
							THEN ''
						ELSE
							' - ' + RecHubData.dimClientAccountsView.LongName
					END
			ELSE 
				' - ' + RecHubUser.OLClientAccounts.DisplayName
		END 
	AS DisplayLabel
FROM 
	RecHubUser.OLOrganizations 
	INNER JOIN RecHubUser.OLClientAccounts ON RecHubUser.OLClientAccounts.OLOrganizationID = RecHubUser.OLOrganizations.OLOrganizationID
	INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLClientAccounts.SiteBankID
		AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLClientAccounts.SiteClientAccountID

GO


