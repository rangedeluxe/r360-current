--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimOLWorkgroupsDisplayView
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimOLWorkgroupsDisplayView') IS NOT NULL
	DROP VIEW RecHubData.dimOLWorkgroupsDisplayView
GO

--WFSScriptProcessorViewCreate
CREATE VIEW RecHubData.dimOLWorkgroupsDisplayView
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 06/20/2014
*
* Purpose: View of the elements required to create the Client Account Name 
*
*
* Modification History
* 06/20/2014 WI 149113 JBS	Created for RAAM integration
******************************************************************************/

SELECT 
	Rechubuser.OLWorkgroups.EntityID,
	RecHubUser.OLWorkgroups.SiteClientAccountID,
	RecHubUser.OLWorkgroups.SiteBankID,
	RecHubData.dimClientAccountsView.DDA,
	RecHubData.dimClientAccountsView.LongName,
	CAST(RecHubUser.OLWorkgroups.SiteClientAccountID AS VARCHAR) + 
		COALESCE(' (' + RTRIM(RecHubData.dimClientAccountsView.DDA) + ')', ' (N/A)') +
		CASE
			WHEN RecHubData.dimClientAccountsView.LongName IS NULL OR RecHubData.dimClientAccountsView.LongName = ''
				THEN ''
			ELSE
				' - ' + RecHubData.dimClientAccountsView.LongName
		END
	AS DisplayLabel
FROM 
	RecHubUser.OLEntities 
	INNER JOIN RecHubUser.OLWorkgroups ON RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
	INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
		AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
