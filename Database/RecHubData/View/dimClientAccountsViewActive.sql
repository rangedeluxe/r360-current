--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimClientAccountsViewActive
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimClientAccountsViewActive') IS NOT NULL
	DROP VIEW RecHubData.dimClientAccountsViewActive
GO

--WFSScriptProcessorViewCreate
CREATE VIEW RecHubData.dimClientAccountsViewActive
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: LA
* Date: XX/XX/XXXX
*
* Purpose: Filter out only Active and Current ClientAccounts
*
*
* Modification History
* XX/XX/XXXX CR XXXXX XXX	Created
* 12/17/2014 WI 182276 LA	Initial Create
******************************************************************************/

SELECT
	ClientAccountKey,
	SiteCodeID,
	SiteClientAccountID,
	SiteBankID,
	SiteOrganizationID,
	ShortName,
	LongName,
	POBox,
	DDA,
	OnlineColorMode,
	CutOff,
	IsActive,
	MostRecent,
	CreationDate,
	ModificationDate,
	SiteClientAccountKey,
	IsCommingled,
	[FileGroup],
	DataRetentionDays,
	ImageRetentionDays,
	CAST(SiteClientAccountID AS VARCHAR) + 
		CASE
			WHEN LongName IS NULL OR LongName = ''
			THEN ''
			ELSE
				' - ' + LongName
		END
	AS DisplayLabel
FROM 
   RecHubData.dimClientAccounts 
WHERE MostRecent = 1 AND IsActive = 1

GO


