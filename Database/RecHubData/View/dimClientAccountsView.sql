--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimClientAccountsView
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimClientAccountsView') IS NOT NULL
	DROP VIEW RecHubData.dimClientAccountsView
GO

--WFSScriptProcessorViewCreate
CREATE VIEW RecHubData.dimClientAccountsView
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: XXX
* Date: XX/XX/XXXX
*
* Purpose:
*
*
* Modification History
* XX/XX/XXXX CR XXXXX XXX	Created
* 05/13/2010 CR 29707 CS	Re-releasing to refresh view for new column pickup
* 08/26/2010 CR 30462 JMC	Modified view to explicitly define it's columnset
* 09/27/2012 CR 56026 JNE	Added column FileGroup to view
* 03/18/2013 WI 92966 JBS	Change to 2.0 release. Change Schema name.
*							Renamed Columns:  SiteCustomerID to SiteOrganizationID,
*							LockboxKey to ClientAccountKey,  SiteLockboxID to SiteClientAccountID,
*							SiteLockboxKey to SiteClientAccountKey,  LoadDate to CreationDate,
*							SiteCode to SiteCodeID
*							Updated column list.
* 07/11/2014 WI 153142 JBS	Adding DisplayLabel derived column.  
******************************************************************************/

SELECT
	ClientAccountKey,
	SiteCodeID,
	SiteClientAccountID,
	SiteBankID,
	SiteOrganizationID,
	ShortName,
	LongName,
	POBox,
	DDA,
	OnlineColorMode,
	CutOff,
	IsActive,
	MostRecent,
	CreationDate,
	ModificationDate,
	SiteClientAccountKey,
	IsCommingled,
	[FileGroup],
	DataRetentionDays,
	ImageRetentionDays,
	CAST(SiteClientAccountID AS VARCHAR) + 
		CASE
			WHEN LongName IS NULL OR LongName = ''
			THEN ''
			ELSE
				' - ' + LongName
		END
	AS DisplayLabel
FROM RecHubData.dimClientAccounts WHERE MostRecent = 1