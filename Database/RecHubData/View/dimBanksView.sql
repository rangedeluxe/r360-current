--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimBanksView
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimBanksView') IS NOT NULL
	DROP VIEW RecHubData.dimBanksView
GO

--WFSScriptProcessorView
CREATE VIEW RecHubData.dimBanksView
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 12/02/2010
*
* Purpose:
*
*
* Modification History
* 12/02/2010 CR 32011 JPB	Created
* 03/18/2013 WI 92980 JBS	Update to 2.0 release. Change Schema name.
*							Added filter to eliminate seed record.
* 06/03/2014 WI 145891 RDS	Include EntityID
******************************************************************************/

SELECT 
	BankKey,
    SiteBankID,
    MostRecent,
    CreationDate,
    ModificationDate,
    BankName,
    ABA,
	EntityID
FROM 
	RecHubData.dimBanks 
WHERE 
	MostRecent = 1
	AND BankKey <> -1