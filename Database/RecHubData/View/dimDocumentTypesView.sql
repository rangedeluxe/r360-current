--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorViewName dimDocumentTypesView	
--WFSScriptProcessorViewDrop
IF OBJECT_ID('RecHubData.dimDocumentTypesView') IS NOT NULL
	DROP VIEW RecHubData.dimDocumentTypesView
GO

--WFSScriptProcessorView
CREATE VIEW RecHubData.dimDocumentTypesView
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 12/02/2010
*
* Purpose:
*
*
* Modification History
* 12/02/2010 CR 32011 JPB	Created
* 03/18/2013 WI 92975 JBS	Update to 2.0 release. Change Schema name.
******************************************************************************/

SELECT 
	DocumentTypeKey,
    MostRecent,
    CreationDate,
    ModificationDate,
    ModifiedBy,
    FileDescriptor,
    DocumentTypeDescription,
    IMSDocumentType
FROM 
	RecHubData.dimDocumentTypes 
WHERE 
	MostRecent = 1