﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factNotifications
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*
*
* Column Information
* UserNotification - Used to determine if record is a "workgroup" or user
*	notification. 0 mean user, 1 means workgroup notification.
* UserID - Will be 0 for workgroup notification.
*
*
* Modification History
* 05/30/2012 CR 53200 JPB	Created
* 03/06/2013 WI 90858 JBS	Update table to 2.0 release. Change schema Name.
*							Rename Column: CustomerKey to OrganizationKey,
*							factNotificationID to factNotificationKey,
*							LockboxKey to ClientAccountKey.
*							Rename FK constraints to match schema and column name changes.
*							Add factNotificationKey to Clustered Index. 
*							Removed Constraints: DF_factNotifications_CreationDate DEFAULT(GETDATE()),
*							DF_factNotifications_ModificationDate DEFAULT(GETDATE()),
*							DF_factNotifications_CreatedBy DEFAULT(SUSER_SNAME()),
*							DF_factNotifications_ModifiedBy DEFAULT(SUSER_SNAME()).
*							Added Column: IsDeleted
* 08/22/2014 WI 139597 JPB	Added UserNotification, UserID, FK to Users and 
*							Changed NotificationSourceKey to SMALLINT.
*							IDX_factNotifications_UserIDNotificationMessageGroup.
*							Added UserNotification to 
*							IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification.
* 02/11/2015 WI 189332 JBS	Added indexes for duplicate detect logic
*							IDX_factNotifications_BankKeySourceNotificationID
*							IDX_factNotifications_NotificationMessageGroupBankKey
* 06/01/2015 WI 216416 JBS  Add index IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime. Per 217727
* 02/02/2018 PT 154233497 JPB	Added MessageWorkgroups
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate factNotifications
CREATE TABLE RecHubData.factNotifications
(
	factNotificationKey BIGINT IDENTITY(1,1) NOT NULL,
	IsDeleted BIT NOT NULL,
	NotificationMessageGroup BIGINT NOT NULL,
	UserNotification BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	UserID INT NOT NULL,
	NotificationDateKey INT NOT NULL,
	NotificationSourceKey SMALLINT NOT NULL,
	NotificationFileCount INT NOT NULL,
	NotificationMessagePart INT NOT NULL,
	NotificationDateTime DATETIME NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	SourceNotificationID UNIQUEIDENTIFIER NOT NULL,
	CreatedBy VARCHAR(128) NOT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	MessageText VARCHAR(128) NOT NULL,
	MessageWorkgroups UNIQUEIDENTIFIER NULL
) $(OnNotificationPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factNotifications ADD
	CONSTRAINT PK_factNotifications PRIMARY KEY NONCLUSTERED (factNotificationKey,NotificationDateKey),
	CONSTRAINT FK_factNotifications_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factNotifications_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factNotifications_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factNotifications_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_factNotifications_NotificationDate FOREIGN KEY(NotificationDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factNotifications_dimBatchSources FOREIGN KEY(NotificationSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);

ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_dimBanks;
ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_dimOrganizations;
ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_dimClientAccounts;
ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_Users;

/*
ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_NotificationDate;
ALTER TABLE RecHubData.factNotifications NOCHECK CONSTRAINT FK_factNotifications_dimBatchSources;

*/
--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_NotificationDateKey
CREATE CLUSTERED INDEX IDX_factNotifications_NotificationDateKey ON RecHubData.factNotifications
(
	NotificationDateKey,
	factNotificationKey
) $(OnNotificationPartition);
--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification
CREATE INDEX IDX_factNotifications_BankOrganizationClientAccountKeyNotificationMessageGroupUserNotification ON RecHubData.factNotifications
(
	BankKey,
	OrganizationKey,
	ClientAccountKey,
	NotificationMessageGroup,
	UserNotification
) $(OnNotificationPartition);
--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_UserIDNotificationMessageGroupUserNotification
CREATE INDEX IDX_factNotifications_UserIDNotificationMessageGroupUserNotification ON RecHubData.factNotifications
(
	UserID,
	NotificationMessageGroup,
	UserNotification
) $(OnNotificationPartition);
--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_BankKeySourceNotificationID
CREATE NONCLUSTERED INDEX IDX_factNotifications_BankKeySourceNotificationID ON RecHubData.factNotifications
(
	BankKey,
	SourceNotificationID
)
INCLUDE 
(
	factNotificationKey,
	NotificationDateKey
)$(OnNotificationPartition);

--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_NotificationMessageGroupBankKey
CREATE NONCLUSTERED INDEX IDX_factNotifications_NotificationMessageGroupBankKey ON RecHubData.factNotifications 
(
	NotificationMessageGroup,
	BankKey
)
INCLUDE 
(
	factNotificationKey,
	NotificationDateKey
)$(OnNotificationPartition);
-- 216416
--WFSScriptProcessorIndex RecHubData.factNotifications.IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime
CREATE INDEX IDX_factNotifications_IsDeletedOrganizationKeyUserIDNotificationMessagePartClientAccountKeyNotificationDateTime ON RecHubData.factNotifications 
(
	IsDeleted, 
	OrganizationKey,
	UserID, 
	NotificationMessagePart,
	ClientAccountKey, 
	NotificationDateTime
) 
INCLUDE 
(
	factNotificationKey, 
	NotificationMessageGroup, 
	NotificationFileCount, 
	SourceNotificationID, 
	MessageText
);