--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factChecks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every check.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28190 JPB	GlobalBatchID is now nullable.
* 02/10/2010 CR 28976 JPB	Added ModificationDate.
* 03/11/2010 CR 29182 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32298 JPB	Added BatchSourceKey.
* 03/14/2011 CR 33331 JPB	Added new index for image retrieval support.
* 05/13/2011 CR 34348 JPB 	Updated clustered index for performance.
* 01/12/2012 CR 49276 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49551 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51372 JPB	Renamed index and added INCLUDE columns.
* 04/05/2012 CR 51866 JPB	Added new index to improve remittence search.
* 03/26/2012 CR 51541 JPB	Added BatchPaymentTypeKey.
* 04/06/2012 CR 51416 JPB	Added new index to improve search performance.
* 07/05/2012 CR 53625 JPB	Added BatchCueID.
* 07/16/2012 CR 54121 JPB	Added BatchNumber.
* 07/17/2012 CR 54131 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54132 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54133 JPB	Renamed and updated index with BatchNumber.
* 07/17/2012 CR 54206 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71800 JPB	2.0 release. Change Schema name.
*							Adding columns: factCheckKey, IsDeleted, NumericRoutingNumber,
*							NumericSerial, RoutingNumber, Account, RemitterName.
*							Renaming columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey.
*							Remove Columns: ImageInfoXML, RemitterKey.
*							Added factCheckKey to ClusteredIndex.
*							Renamed indexes to match schema and column changes	
*							Create New Index based from old clustered index.				
*							Forward Patch: WI 87430
* 02/28/2014 WI 131199 JBS	Add DDAKey, including FK constraint and Index
* 04/10/2014 WI 135315 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 142797 JBS	Added SourceBatchID.
* 05/30/2014 WI 144578 JBS	Changed SourceBatchKey to SMALLINT.
* 06/01/2016 WI 283038 MGE  Added Index for Advanced Search improvement
* 09/27/2018 PT 160347603 MGE Added ClientAccountKey to index to improve performance of dashboard.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factChecks
(
	factCheckKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL, 
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL, 
	BatchSourceKey SMALLINT NOT NULL, 
	BatchPaymentTypeKey TINYINT NOT NULL, 
	DDAKey		INT NOT NULL,   
	BatchCueID INT NOT NULL, 
	DepositStatus INT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	CheckSequence INT NOT NULL,
	NumericRoutingNumber BIGINT NOT NULL,
	NumericSerial BIGINT NOT NULL,
	Amount MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL, 
	BatchSiteCode INT NULL, 
	RoutingNumber VARCHAR(30) NOT NULL,
	Account VARCHAR(30) NOT NULL,
	TransactionCode VARCHAR(30) NULL,
	Serial VARCHAR(30) NULL,
	RemitterName VARCHAR(60) NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factChecks ADD
	CONSTRAINT PK_factChecks PRIMARY KEY NONCLUSTERED (factCheckKey,DepositDateKey),
	CONSTRAINT FK_factChecks_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factChecks_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factChecks_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factChecks_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factChecks_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factChecks_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factChecks_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factChecks_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factChecks_dimDDAs FOREIGN KEY(DDAKey) REFERENCES RecHubData.dimDDAs(DDAKey);

--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DepositDatefactCheckKey
CREATE CLUSTERED INDEX IDX_factChecks_DepositDatefactCheckKey ON RecHubData.factChecks 
(
	DepositDateKey,
	factCheckKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey
CREATE INDEX IDX_factChecks_DepositDateClientAccountKeySourceProcessingDateBatchIDBatchSequenceBankKey ON RecHubData.factChecks 
(
	DepositDateKey,
	ClientAccountKey,
	SourceProcessingDateKey,
	BatchID,
	BatchSequence,
	BankKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_BankKey
CREATE INDEX IDX_factChecks_BankKey ON RecHubData.factChecks (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_OrganizationKey
CREATE INDEX IDX_factChecks_OrganizationKey ON RecHubData.factChecks (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_ClientAccountKey
CREATE INDEX IDX_factChecks_ClientAccountKey ON RecHubData.factChecks (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_ImmutableDateKey
CREATE INDEX IDX_factChecks_ImmutableDateKey ON RecHubData.factChecks (ImmutableDateKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DDAKey
CREATE NONCLUSTERED INDEX IDX_factChecks_DDAKey ON RecHubData.factChecks (DDAKey) $(OnDataPartition);
--CR 29182
--CR 54206
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber
CREATE NONCLUSTERED INDEX IDX_factChecks_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumber ON RecHubData.factChecks
(
	[BankKey] ASC,
	[OrganizationKey] ASC,
	[ClientAccountKey] ASC,
	[ImmutableDateKey] ASC,
	[DepositDateKey] ASC,
	[BatchID] ASC,
	[BatchNumber] ASC
) $(OnDataPartition);
--CR 33331, 51372
--CR 54132
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID
CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountImmutableDateBankOrganizationKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
(
	[DepositDateKey] ASC,
	[ClientAccountKey] ASC,
	[ImmutableDateKey] ASC,
	[BankKey] ASC,
	[OrganizationKey] ASC,
	[BatchID] ASC,
	[BatchNumber] ASC,
	[TransactionID] ASC
)
INCLUDE 
( 
	[BatchSequence],
	[CheckSequence],
	[NumericSerial],
	[Serial]
) $(OnDataPartition);
--CR 51659
--CR 54133
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus
CREATE NONCLUSTERED INDEX IDX_factChecks_ClientAccountDepositDateImmutableDateKeyBatchIDBatchNumberAmountSerialDepositStatus ON RecHubData.factChecks
(
	ClientAccountKey ASC,
	DepositDateKey ASC,
	ImmutableDateKey ASC,
	BatchID ASC,
	BatchNumber ASC,
	Amount ASC,
	Serial ASC,
	DepositStatus ASC
) $(OnDataPartition);
--CR 51416
--CR 54131
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID
CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyAmountBankOrganizationImmutableDateKeyBatchIDBatchNumberTransactionID ON RecHubData.factChecks
(
    DepositDateKey ASC,
    ClientAccountKey ASC,
    Amount ASC,
    BankKey ASC,
    OrganizationKey ASC,
    ImmutableDateKey ASC,
    BatchID ASC,
    BatchNumber ASC,
    TransactionID ASC
)
INCLUDE
(
    DepositStatus,
    NumericSerial,
    CheckSequence,
    Serial
)  $(OnDataPartition);
--WI 71800 (FP:87430)
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID
CREATE NONCLUSTERED INDEX IDX_factChecks_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factChecks
(
	DepositDateKey ASC,
	ClientAccountKey ASC,
	BatchID ASC,
	TransactionID ASC
)  $(OnDataPartition);

--WI 283038 
--WFSScriptProcessorIndex RecHubData.factChecks.IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount
CREATE NONCLUSTERED INDEX IDX_factChecks_IsDeletedDepositDateBatchIDTransactionIDAmount ON RecHubData.factChecks
(
	IsDeleted ASC,
	DepositDateKey ASC,
	BatchID ASC,
	TransactionID ASC,
	ClientAccountKey ASC,
	Amount ASC
)
INCLUDE 
(
	DDAKey,
	BatchSequence,
	CheckSequence,
	NumericRoutingNumber,
	NumericSerial,
	RoutingNumber,
	Account,
	TransactionCode,
	Serial,
	RemitterName
)  
$(OnDataPartition);
