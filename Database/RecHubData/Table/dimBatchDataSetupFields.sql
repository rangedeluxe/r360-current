--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBatchDataSetupFields
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2011
*
* Purpose: Stores Item Data trail keywords referenced by factItemData. 
*
* Check Constraint definitions:
* DataType
*	1: Alphanumeric
*	3: Integer
*	4: Bit
*	6: Float
*	7: Money
* 	11: Date/time
*
* Modification History
* 03/07/2011 CR 33211 JPB	Created
* 03/01/2013 WI 89963 JBS	Update table to 2.0 release. Change Schema Name	
* 06/20/2014 WI 148858 JPB	Replaced BatchSourceKey/FK with ImportTypeKey/FK.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBatchDataSetupFields
(
	BatchDataSetupFieldKey INT NOT NULL 
		CONSTRAINT PK_dimBatchDataSetupFields PRIMARY KEY CLUSTERED,
	ImportTypeKey TINYINT NOT NULL,
	DataType TINYINT NOT NULL
		CONSTRAINT CK_dimBatchDataSetupFields_DataType CHECK (DataType IN (1,3,4,6,7,11)),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimBatchDataSetupFields_CreatedBy DEFAULT SUSER_SNAME(),
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_dimBatchDataSetupFields_CreationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimBatchDataSetupFields_ModifiedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchDataSetupFields_ModificationDate DEFAULT GETDATE(),
	Keyword VARCHAR(32) NOT NULL,
	[Description] VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBatchDataSetupFields.IDX_dimBatchDataSetupFields_Keyword_ImportTypeKey
CREATE UNIQUE INDEX IDX_dimBatchDataSetupFields_Keyword_ImportTypeKey ON RecHubData.dimBatchDataSetupFields (Keyword,ImportTypeKey);
