--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factRawPaymentData
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/06/2014
*
* Purpose: Grain: one row for every check.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 05/06/2014 WI 144027 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factRawPaymentData
(
	factRawPaymentDataKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL, --CR 53625 JPB 07/05/2012
	DepositStatus INT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	RawDataSequence INT NOT NULL,
	RawDataPart SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	RawData VARCHAR(128) NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factRawPaymentData ADD
	CONSTRAINT PK_factRawPaymentData PRIMARY KEY NONCLUSTERED (factRawPaymentDataKey,DepositDateKey),
	CONSTRAINT FK_factRawPaymentData_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factRawPaymentData_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factRawPaymentData_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factRawPaymentData_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factRawPaymentData_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factRawPaymentData_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factRawPaymentData_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factRawPaymentData_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);

--WFSScriptProcessorIndex RecHubData.factRawPaymentData.IDX_factfactRawPaymentData_DepositDatefactCheckKey
CREATE CLUSTERED INDEX IDX_factRawPaymentData_DepositDatefactRawPaymentDataKey ON RecHubData.factRawPaymentData 
(
	DepositDateKey,
	factRawPaymentDataKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factRawPaymentData.IDX_factRawPaymentData_BankKey
CREATE INDEX IDX_factRawPaymentData_BankKey ON RecHubData.factRawPaymentData (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factRawPaymentData.IDX_factRawPaymentData_OrganizationKey
CREATE INDEX IDX_factRawPaymentData_OrganizationKey ON RecHubData.factRawPaymentData (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factRawPaymentData.IDX_factRawPaymentData_ClientAccountKey
CREATE INDEX IDX_factRawPaymentData_ClientAccountKey ON RecHubData.factRawPaymentData (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factRawPaymentData.IDX_factRawPaymentData_ImmutableDateKey
CREATE INDEX IDX_factRawPaymentData_ImmutableDateKey ON RecHubData.factRawPaymentData (ImmutableDateKey) $(OnDataPartition);
