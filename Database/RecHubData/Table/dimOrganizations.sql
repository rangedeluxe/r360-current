--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimOrganizations
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Customers dimension is a SCD type 2 holding Customer info.  
*	Most recent flag of 1 indicates the current Customer row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/04/2013 WI 90161 JBS	Update table to 2.0 release.  Change Schema Name
*							Rename table name from dimCustomers to dimOrganizations
*							Add Column ModificationDate and Constraint
*							Rename column:
*							LoadDate to CreationDate
*							CustomerKey to OrganizationKey
*							SiteCustomerID to SiteOrganizationID
*							FP: WI 86823						
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimOrganizations
(
	OrganizationKey INT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_dimOrganizations PRIMARY KEY CLUSTERED,
	SiteOrganizationID INT NOT NULL,       
	SiteBankID INT NOT NULL,
	MostRecent BIT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_dimOrganizations_ModificationDate DEFAULT GETDATE(),
	Name VARCHAR(20) NULL,
	[Description] VARCHAR(255) NULL
)
--WFSScriptProcessorTableProperties
--WI 90161 (FP:86823)
--WFSScriptProcessorIndex RecHubData.dimOrganizations.IDX_dimCustomers_SiteBankCustomerID
CREATE INDEX IDX_dimOrganizations_SiteBankOrganizationID ON RecHubData.dimOrganizations(SiteBankID,SiteOrganizationID)
