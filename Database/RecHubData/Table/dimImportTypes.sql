--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimImportTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/19/2014
*
* Purpose: Import Types dimension is a static data dimension for import types. 
*
* Modification History
* 06/19/2014 WI 148855 JPB	Created. Added Permissions
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimImportTypes
(
	ImportTypeKey TINYINT NOT NULL
		CONSTRAINT PK_dimImportTypes PRIMARY KEY CLUSTERED,
	IsActive BIT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimImportTypes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimImportTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimImportTypes_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimImportTypes_ModifiedBy DEFAULT(SUSER_SNAME()),
	ShortName VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimImportTypes.IDX_dimImportTypes_ImportTypeName
CREATE UNIQUE INDEX IDX_dimImportTypes_ImportTypeName ON RecHubData.dimImportTypes(ShortName) INCLUDE (IsActive);
