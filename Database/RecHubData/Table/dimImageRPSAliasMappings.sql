--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimImageRPSAliasMappings
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2011
*
* Purpose: Stores ImageRPS Alias names to RecHubData table/columns. 
*
*
* Modification History
* 04/04/2011 CR 33683 JPB	Created
* 09/10/2012 CR 55576 JPB	Added MostRecent
* 01/13/2013 WI 119080 JBS	Update to 2.1 Schema.
* 08/09/2017 PT 149122385 MGE	Updated Index
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimImageRPSAliasMappings
(
	ImageRPSAliasMappingKey INT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_dimImageRPSAliasMappings PRIMARY KEY CLUSTERED,
	SiteBankID			INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ExtractType			TINYINT NOT NULL,
	DocType				TINYINT NOT NULL,
	MostRecent			BIT NOT NULL,
	CreationDate		DATETIME NOT NULL,
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_dimImageRPSAliasMappings_ModificationDate DEFAULT GETDATE(),
	FieldType			CHAR(1) NOT NULL,
	AliasName			VARCHAR(256) NOT NULL
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimImageRPSAliasMappings.IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName
CREATE NONCLUSTERED INDEX IDX_dimImageRPSAliasMappings_SiteBankIDSiteClientAccountIDDocTypeAliasName ON RecHubData.dimImageRPSAliasMappings
(
	SiteBankID ASC,
	SiteClientAccountID ASC,
	DocType ASC,
	MostRecent ASC,
	ModificationDate ASC,
	CreationDate ASC,
	AliasName ASC
)
INCLUDE 
( 	
	FieldType,
	ExtractType
)