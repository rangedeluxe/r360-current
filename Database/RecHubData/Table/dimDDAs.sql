--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimDDAs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/28/2014
*
* Purpose: DDA dimension table 
*	ABA,DDA combination is required to be unique 
*
* Modification History
* 02/28/2014 WI 131200 JBS	Created
* 06/04/2014 WI 145543 JBS	Added Select permission to RecHubExtractWizard_User
* 05/29/2014 WI 216143 JBS	Change Clustered index to IDX_dimDDAs_ABADDA
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimDDAs
CREATE TABLE RecHubData.dimDDAs
(
	DDAKey			INT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_dimDDAs PRIMARY KEY NONCLUSTERED,
	ABA				VARCHAR(10) NOT NULL,
	DDA				VARCHAR(35)	NOT NULL,
	CreationDate	DATETIME NOT NULL	
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimDDAs.IDX_dimDDAs_ABADDA
CREATE UNIQUE CLUSTERED INDEX IDX_dimDDAs_ABADDA ON RecHubData.dimDDAs 
(
	ABA	ASC,
	DDA ASC
);
--WFSScriptProcessorIndex RecHubData.dimDDAs.IDX_dimDDAs_DDA
CREATE NONCLUSTERED INDEX IDX_dimDDAs_DDA ON RecHubData.dimDDAs 
(
	DDA ASC
)
INCLUDE 
( 	
	DDAKey,
	ABA
);
