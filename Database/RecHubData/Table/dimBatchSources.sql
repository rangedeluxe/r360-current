--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBatchSources
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/10/2011
*
* Purpose: Batch Sources dimension is a static data dimension for batch sources. 
*
* Modification History
* 01/10/2011 CR 32310 JPB	Created
* 03/01/2011 CR 33123 JPB	Removed IDENTITY property from BatchSourceKey.
* 01/24/2012 CR 49629 JPB	Added standard Creation/Modification columns,
*							removed LoadDate.
* 03/01/2013 WI 89976 JBS	Update table to 2.0 release.
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
* 06/13/2013 WI 105327 JPB	Added IsActive.
* 03/11/2014 WI 132464 JBS	Adding permission tags for table
* 05/22/2014 WI 139928 JPB	Changes to support RAAM.
*							-BatchSourceKey to SMALLINT IDENTITY
*							-Added EntityID
*							-Added ImportTypeKey
* 08/27/2015 WI 237951 JPB	Added new index for SSIS.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBatchSources
(
	BatchSourceKey SMALLINT NOT NULL IDENTITY(103,1)
		CONSTRAINT PK_dimBatchSources PRIMARY KEY CLUSTERED,
	IsActive BIT NOT NULL
		CONSTRAINT DF_dimBatchSources_IsActive DEFAULT(0),
	ImportTypeKey TINYINT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchSources_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchSources_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchSources_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchSources_ModifiedBy DEFAULT(SUSER_SNAME()),
	EntityID INT NULL,
	ShortName VARCHAR(30) NOT NULL,
	LongName VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.PK_dimBatchSources.IDX_dimBatchSources_ShortName
CREATE UNIQUE INDEX IDX_dimBatchSources_ShortName ON RecHubData.dimBatchSources (ShortName) INCLUDE (IsActive);

--WI 237951
--WFSScriptProcessorIndex RecHubData.PK_dimBatchSources.IDX_dimBatchSources_ShortName
CREATE INDEX IDX_dimBatchSources_ShortNameBatchSourceImportTypeKeyIsActive ON RecHubData.dimBatchSources 
(
	ShortName,
	BatchSourceKey,
	ImportTypeKey,
	IsActive
);
