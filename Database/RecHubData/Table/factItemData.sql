--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable factItemData
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*
*
* Modification History
* 03/07/2011 CR 33210 JPB	Created
* 01/12/2012 CR 49282 JPB	Added SourceProcessingDateKey.
* 03/19/2012 CR 51050 WJS	Added index.
* 07/19/2012 CR 54128 JPB	Added BatchNumber.
* 07/16/2012 CR 54137 JPB	Renamed and updated index with BatchNumber.
* 03/06/2013 WI 90542 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factItemDataKey, IsDeleted.
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factItemDataKey to Clustered Index. 
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factItemData_ModificationDate DEFAULT GETDATE()
* 05/31/2014 WI 135321 JPB	Changed BatchID from INT to BIGINT.
* 05/31/2014 WI	144972 JPB	Add SourceBatchID.
* 03/20/2015 WI 196953 JPB	Added BatchSourceKey.
* 06/10/2015 WI 217940 JBS	Add indexes based on Regression Analysis.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factItemData
(
	factItemDataKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL, --CR 49282 JPB 01/12/2012
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL, --CR 54128 JPB 07/19/2012
	BatchSourceKey SMALLINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	ItemDataSetupFieldKey INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataValueDateTime DATETIME NULL,
	DataValueMoney MONEY NULL,
	DataValueInteger INT NULL,
	DataValueBit BIT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	DataValue VARCHAR(256) NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factItemData ADD 
	CONSTRAINT PK_factItemData PRIMARY KEY NONCLUSTERED (factItemDataKey,DepositDateKey),
	CONSTRAINT FK_factItemData_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factItemData_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factItemData_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factItemData_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factItemData_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factItemData_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factItemData_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factItemData_dimItemDataSetupFields FOREIGN KEY(ItemDataSetupFieldKey) REFERENCES RecHubData.dimItemDataSetupFields(ItemDataSetupFieldKey);
--WFSScriptProcessorPrint Disabling Foreign Keys
/*
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_dimBanks;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_dimOrganizations;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_dimClientAccounts;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_DepositDate;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_ImmutableDate;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_SourceProcessingDate;
ALTER TABLE RecHubData.factItemData NOCHECK CONSTRAINT FK_factItemData_dimItemDataSetupFields;
*/
	
--WFSScriptProcessorIndex RecHubData.factItemData.IDX_factItemData_DepositDatefactItemDataKey
CREATE CLUSTERED INDEX IDX_factItemData_DepositDatefactItemDataKey ON RecHubData.factItemData 
(
	DepositDateKey,
	factItemDataKey
) $(OnDataPartition);

--CR 51050, 54137
--WFSScriptProcessorIndex RecHubData.factItemData.IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence
CREATE NONCLUSTERED INDEX IDX_factItemData_BankClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factItemData
(
	BankKey ASC,
	ClientAccountKey ASC,
	ImmutableDateKey ASC,
	DepositDateKey ASC,
	BatchID ASC,
	BatchNumber ASC,
	BatchSequence ASC
) $(OnDataPartition);

-- 217940
--WFSScriptProcessorIndex RecHubData.factItemData.IDX_factItemData_ItemDataSetupFieldKey
CREATE INDEX IDX_factItemData_ItemDataSetupFieldKey ON RecHubData.factItemData 
(
	ItemDataSetupFieldKey
) 
INCLUDE 
(
	ClientAccountKey, 
	ImmutableDateKey, 
	BatchID, 
	BatchSequence, 
	ModificationDate, 
	DataValue
);
-- 217940
--WFSScriptProcessorIndex RecHubData.factItemData.IDX_factItemData_SourceBatchIDDepositDateKey
CREATE INDEX IDX_factItemData_SourceBatchIDDepositDateKey ON RecHubData.factItemData 
(
	SourceBatchID,
	DepositDateKey
)
INCLUDE 
(
	factItemDataKey, 
	IsDeleted, 
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	ImmutableDateKey, 
	SourceProcessingDateKey, 
	BatchID, 
	BatchNumber, 
	TransactionID, 
	BatchSequence, 
	ItemDataSetupFieldKey, 
	CreationDate, 
	ModificationDate, 
	DataValueDateTime, 
	DataValueMoney, 
	DataValueInteger, 
	DataValueBit, 
	ModifiedBy, 
	DataValue, 
	BatchSourceKey
) $(OnDataPartition);

