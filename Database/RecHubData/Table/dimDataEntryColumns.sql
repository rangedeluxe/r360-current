--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimDataEntryColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Data Entry Columns dimension is a type 1 SCD holding an entry for 
*	each unique data entry column name. 
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/10/2011 CR 33213 JPB	Added Marked Sense column. Also reorged columns.
* 04/22/2011 CR 33991 JPB	Added ModificationDate.
* 02/14/2013 WI 90097 JBS	Update table to 2.0 release. Change Schema Name.
*							Rename LoadDate to CreationDate.  Change column lengths
*							TableName from 36 to 64, FldName from 32 to 128,
*							DisplayName from 32 to 64
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimDataEntryColumns
(
	DataEntryColumnKey INT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_dimDataEntryColumns PRIMARY KEY CLUSTERED,
	TableType TINYINT NOT NULL
		CONSTRAINT CK_dimDataEntryColumns_TableType CHECK (TableType IN (0,1,2,3)),
	DataType SMALLINT NOT NULL,
	FldLength TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	MarkSense TINYINT NOT NULL
		CONSTRAINT DF_dimDataEntryColumns_MarkSense DEFAULT 0,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimDataEntryColumns_ModificationDate DEFAULT GETDATE(),
	TableName VARCHAR(64) NOT NULL,
	FldName VARCHAR(128) NOT NULL,
	DisplayName VARCHAR(64) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimDataEntryColumns.IDX_dimDataEntryColumns_FldName
CREATE INDEX IDX_dimDataEntryColumns_FldName ON RecHubData.dimDataEntryColumns(FldName);
