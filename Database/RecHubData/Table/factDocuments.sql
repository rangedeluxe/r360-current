--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factDocuments
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28193 JPB	GlobalBatchID is now nullable.
* 03/11/2010 CR 29183 JPB	Added new index for Lockbox Search.
* 01/13/2011 CR 32300 JPB	Added BatchSourceKey.
* 02/07/2011 CR 32598 JPB 	Added ModificationDate.
* 03/15/2011 CR 33351 JPB	Added new index.
* 05/13/2011 CR 34259 JPB	Updated clustered index for performance.
* 01/12/2012 CR 49277 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49554 JPB	Added BatchSiteCode.
* 03/27/2012 CR 51544 JPB	Added BatchPaymentTypeKey.
* 07/12/2012 CR 53627 JPB	Added BatchCueID.
* 07/17/2012 CR 54123 JPB	Added BatchNumber.
* 07/16/2012 CR 54135 JPB	Renamed and updated index with BatchNumber.
* 07/16/2012 CR 54136 JPB	Renamed and updated index with BatchNumber.
* 03/06/2013 WI 90501 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factDocumentKey, IsDeleted.
*							Remove Column: GlobalBatchID, GlobalDocumentID, ImageInfoXML
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factDocumentKey to Clustered Index. 
*							Create New Index based from old clustered index.
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factDocuments_BatchPaymentTypeKey DEFAULT(0),
*							DF_factDocuments_BatchCueID DEFAULT(-1), DF_factDocuments_ModificationDate DEFAULT GETDATE()
*							Forward Patch:  WI 85091
* 05/29/2014 WI 135319 JPB	Changed BatchID to BIGINT.
* 05/29/2014 WI 144577 JPB	Added SourceBatchID.
* 05/29/2014 WI 144580 JPB	Changed BatchSourceKey to SMALLINT.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factDocuments
(
	factDocumentKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	DocumentTypeKey INT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	DocumentSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL
)  $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factDocuments ADD
	CONSTRAINT PK_factDocuments PRIMARY KEY NONCLUSTERED (factDocumentKey,DepositDateKey),
	CONSTRAINT FK_factDocuments_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factDocuments_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factDocuments_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factDocuments_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocuments_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocuments_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocuments_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factDocuments_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factDocuments_dimDocumentTypes FOREIGN KEY(DocumentTypeKey) REFERENCES RecHubData.dimDocumentTypes(DocumentTypeKey);
/*
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimBanks;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimOrganizations;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimClientAccount;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_DepositDate;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_ImmutableDate;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_SourceProcessingDate;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimBatchSources;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimBatchPaymentTypes;
ALTER TABLE RecHubData.factDocuments NOCHECK CONSTRAINT FK_factDocuments_dimDocumentTypes;
*/

--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_DepositDatefactDocumentKey
CREATE CLUSTERED INDEX IDX_factDocuments_DepositDatefactDocumentKey ON RecHubData.factDocuments
(
	DepositDateKey,
	factDocumentKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence
CREATE INDEX IDX_factDocuments_DepositDateClientAccountImmutableDateKeyBatchIDBatchSequence ON RecHubData.factDocuments
(
	DepositDateKey,
	ClientAccountKey,
	ImmutableDateKey,
	BatchID,
	BatchSequence
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_BankKey
CREATE INDEX IDX_factDocuments_BankKey ON RecHubData.factDocuments (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_OrganizationKey
CREATE INDEX IDX_factDocuments_OrganizationKey ON RecHubData.factDocuments (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_ClientAccountKey
CREATE INDEX IDX_factDocuments_ClientAccountKey ON RecHubData.factDocuments (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_ImmutableDateKey
CREATE INDEX IDX_factDocuments_ImmutableDateKey ON RecHubData.factDocuments (ImmutableDateKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_DocumentTypeKey
CREATE INDEX IDX_factDocuments_DocumentTypeKey ON RecHubData.factDocuments (DocumentTypeKey) $(OnDataPartition);
--CR 29183, 54135
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence
CREATE NONCLUSTERED INDEX IDX_factDocuments_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchIDNumberSequence ON RecHubData.factDocuments 
(
	BankKey ASC,
	OrganizationKey ASC,
	ClientAccountKey ASC,
	ImmutableDateKey ASC,
	DepositDateKey ASC,
	BatchID ASC,
	BatchNumber ASC,
	BatchSequence ASC
) $(OnDataPartition);
--CR 33351, 54136
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber
CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateImmutableDateBankClientAccountKeyBatchIDNumber ON RecHubData.factDocuments
(
	DepositDateKey ASC,
	ImmutableDateKey ASC,
	BankKey ASC,
	ClientAccountKey ASC,
	BatchID ASC,
	BatchNumber ASC
) $(OnDataPartition);
--WI 90501 (FP:85091)
--WFSScriptProcessorIndex RecHubData.factDocuments.IDX_factDocuments_DepositDateKeyBatchIDDepositStatus
CREATE NONCLUSTERED INDEX IDX_factDocuments_DepositDateKeyBatchIDDepositStatus ON RecHubData.factDocuments
(
	DepositDateKey ASC,
	BatchID ASC,
	DepositStatus ASC
) 
INCLUDE
(
	ClientAccountKey,
	ImmutableDateKey,
	DocumentTypeKey,
	TransactionID,
	TxnSequence,
	SequenceWithinTransaction,
	BatchSequence,
	DocumentSequence
) $(OnDataPartition);
