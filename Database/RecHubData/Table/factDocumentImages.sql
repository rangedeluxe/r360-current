--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factDocumentImages
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/17/2013
*
* Purpose: Grain: one row for every Document Image.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/05/2013 WI 102409 JBS	Created
* 05/30/2014 WI 135318 JPB	Changed BatchID from INT to BIGINT.
* 05/29/2014 WI 144764 JPB	Added SourceBatchID.
* 05/29/2014 WI 144765 JPB	Changed SourceBatchKey to SMALLINT.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factDocumentImages
(
	factDocumentImageKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL, 
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	ItemSequence INT NOT NULL,
	ImageTypeKey SMALLINT NOT NULL,
	ExternalDocumentID BIGINT NOT NULL,
	FileSize INT NOT NULL,
	Side BIT NOT NULL,
	PageNumber SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL, --CR 32598 JPB 02/07/2011
	BatchSiteCode INT NULL --CR 49554 JPB 01/23/2012
)  $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factDocumentImages ADD
	CONSTRAINT PK_factDocumentImages PRIMARY KEY NONCLUSTERED (factDocumentImageKey,DepositDateKey),
	CONSTRAINT FK_factDocumentImages_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factDocumentImages_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factDocumentImages_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factDocumentImages_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocumentImages_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocumentImages_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDocumentImages_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factDocumentImages_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factDocumentImages_dimImageTypes FOREIGN KEY(ImageTypeKey) REFERENCES RecHubData.dimImageTypes(ImageTypeKey);

--WFSScriptProcessorPrint Disabling Foreign Keys
/*
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimBanks;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimOrganizations;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimClientAccounts;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_DepositDate;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_ImmutableDate;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_ProcessingDate;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimBatchSources;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimBatchPaymentTypes;
ALTER TABLE RecHubData.factDocumentImages NOCHECK CONSTRAINT FK_factDocumentImages_dimImageTypes;

*/

--WFSScriptProcessorIndex RecHubData.factDocumentImages.IDX_factDocumentImages_DepositDatefactImageKey
CREATE CLUSTERED INDEX IDX_factDocumentImages_DepositDatefactImageKey ON RecHubData.factDocumentImages 
(	
	DepositDateKey,
	factDocumentImageKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocumentImages.IDX_factDocumentImages_BankKey
CREATE INDEX IDX_factDocumentImages_BankKey ON RecHubData.factDocumentImages (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocumentImages.IDX_factDocumentImages_OrganizationKey
CREATE INDEX IDX_factDocumentImages_OrganizationKey ON RecHubData.factDocumentImages (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocumentImages.IDX_factDocumentImages_ClientAccountKey
CREATE INDEX IDX_factDocumentImages_LockboxKey ON RecHubData.factDocumentImages (ClientAccountKey)$(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDocumentImages.IDX_factDocumentImages_ImmutableDateKey
CREATE INDEX IDX_factDocumentImages_ImmutableDateKey ON RecHubData.factDocumentImages (ImmutableDateKey) $(OnDataPartition);
