--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factExtractTraces
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 05/30/2013
*
* Purpose: Request Batch Setup Fields  
*
*
* Modification History
* 05/30/2013 WI 103847 JMC  Initial Version
*							Added ModificationDate for  purge process
* 05/31/2014 WI 135320 JPB	Changed BatchID from INT to BIGINT.
* 05/31/2014 WI 144977 JPB	Add SourceBatchID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factExtractTraces
(
	factExtractKey			BIGINT IDENTITY(1, 1),
	IsDeleted				BIT NOT NULL,
	BankKey					INT NOT NULL,
	OrganizationKey			INT NOT NULL,
	ClientAccountKey		INT NOT NULL,
	DepositDateKey			INT NOT NULL,
	ImmutableDateKey		INT NOT NULL,
	SourceProcessingDateKey	INT NOT NULL,
	BatchID					BIGINT NOT NULL,
	SourceBatchID			BIGINT NOT NULL,
	ExtractDescriptorKey	INT NOT NULL,
	ExtractDateTime			DATETIME NOT NULL,
	ModificationDate		DATETIME NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factExtractTraces ADD 
	CONSTRAINT PK_factExtractTraces PRIMARY KEY NONCLUSTERED (factExtractKey,DepositDateKey),
	CONSTRAINT FK_factExtractTraces_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factExtractTraces_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factExtractTraces_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factExtractTraces_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factExtractTraces_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factExtractTraces_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factExtractTraces_dimExtractDescriptor FOREIGN KEY([ExtractDescriptorKey]) REFERENCES [RecHubData].[dimExtractDescriptor] ([ExtractDescriptorKey]);

--WFSScriptProcessorIndex RecHubData.factExtractTraces.IDX_factExtractTraces_DepositDatefactExtractKey
CREATE CLUSTERED INDEX IDX_factExtractTraces_DepositDatefactExtractKey ON RecHubData.factExtractTraces
(
	DepositDateKey,
	factExtractKey
) $(OnDataPartition);