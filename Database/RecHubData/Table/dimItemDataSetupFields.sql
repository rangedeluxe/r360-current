--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimItemDataSetupFields
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2011
*
* Purpose: Stores Item Data trail keywords referenced by factItemData. 
*
* Check Constraint definitions:
* DataType
*	1: Alphanumeric
*	3: Integer
*	4: Bit
*	6: Float
*	7: Money
* 	11: Date/time
*
* Modification History
* 03/07/2011 CR 33209 JPB	Created
* 02/14/2013 WI 90130 JBS	Update table to	2.0 release. Change Schema Name
*							Change column length on ModifiedBy and CreatedBy
*							From 32 to 128. 
*							Add FK FK_dimItemDataSetupFields_dimBatchSources
* 06/20/2014 WI	148859 JPB	Changed BatchSourceKey to ImportTypeKey.			
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimItemDataSetupFields
(
	ItemDataSetupFieldKey INT NOT NULL 
		CONSTRAINT PK_dimItemDataSetupFields PRIMARY KEY CLUSTERED,
	ImportTypeKey TINYINT NOT NULL,
	DataType TINYINT NOT NULL
		CONSTRAINT CK_dimItemDataSetupFields_DataType CHECK (DataType IN (1,3,4,6,7,11)),
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_dimItemDataSetupFields_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimItemDataSetupFields_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimItemDataSetupFields_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimItemDataSetupFields_ModifiedBy DEFAULT SUSER_SNAME(),
	Keyword VARCHAR(32) NOT NULL,
	[Description] VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.dimItemDataSetupFields ADD 
	CONSTRAINT FK_dimItemDataSetupFields_dimImportTypes FOREIGN KEY(ImportTypeKey) REFERENCES RecHubData.dimImportTypes(ImportTypeKey);

--WFSScriptProcessorIndex RecHubData.dimItemDataSetupFields.IDX_dimItemDataSetupFields_Keyword_ImportTypeKey
CREATE UNIQUE INDEX IDX_dimItemDataSetupFields_Keyword_ImportTypeKey ON RecHubData.dimItemDataSetupFields (Keyword,ImportTypeKey);
