--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimWorkgroupDataEntryColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2015-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 07/06/2015
*
* Purpose: Workgoup Data Entry Columns dimension is a type 0 SCD holding  
*	an entry for each Workgroup/BatchSource data entry column. 
*		   
*
* Modification History
* 07/06/2015 WI 221698 JPB	Created
* 05/25/2016 WI 282808 JPB	Added new index for OTIS import performance
* 12/02/2016 PT 127604133 JPB	Added IsRequired
* 03/31/2017 PT 141048573 JPB	Added SELECT permission for RecHubExtractWizard_User
* 02/06/2018 PT 154229602 MGE	Added index to support advanced search
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimWorkgroupDataEntryColumns
(
	WorkgroupDataEntryColumnKey BIGINT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_dimWorkgroupDataEntryColumns PRIMARY KEY CLUSTERED,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	IsCheck BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsCheck DEFAULT 0,
	IsActive BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsActive DEFAULT 1,
	IsRequired BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsRequired DEFAULT 0,
	MarkSense BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_MarkSense DEFAULT 0,
	DataType TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_ModificationDate DEFAULT GETDATE(),
	UILabel NVARCHAR(64) NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.dimWorkgroupDataEntryColumns ADD 
	CONSTRAINT FK_dimWorkgroupDataEntryColumns_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);

--WFSScriptProcessorIndex RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_FieldName
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_FieldName ON RecHubData.dimWorkgroupDataEntryColumns(FieldName);

--WFSScriptProcessorIndex RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_BankClientAccountIDBatchSourceKey ON RecHubData.dimWorkgroupDataEntryColumns
(
	SiteBankID,
	SiteClientAccountID,
	BatchSourceKey
);

--WI 282808
--WFSScriptProcessorIndex RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_SiteBankSiteClientAccountID ON RecHubData.dimWorkgroupDataEntryColumns
(
	SiteBankID,
	SiteClientAccountID
)
INCLUDE
(
	IsCheck,
	DataType,
	SourceDisplayName
);

--PT 154229602
--WFSScriptProcessorIndex RecHubData.dimWorkgroupDataEntryColumns.IDX_dimWorkgroupDataEntryColumns_BatchSourceIsCheckDataTypeUILabel
CREATE INDEX IDX_dimWorkgroupDataEntryColumns_BatchSourceIsCheckDataTypeUILabel ON RecHubData.dimWorkgroupDataEntryColumns
(
	BatchSourceKey,
	IsCheck,
	DataType,
	UILabel	
)
INCLUDE
(
	WorkgroupDataEntryColumnKey,
	SiteBankID,
	SiteClientAccountID,
	FieldName
);
