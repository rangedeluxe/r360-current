﻿--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factNotificationFiles
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2012-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2012-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Column Information
* UserNotification - Used to determine if record is a "workgroup" or user
*	notification. 0 mean user, 1 means workgroup notification.
* UserID - Will be 0 for workgroup notification.
*
*
* Modification History
* 05/30/2012 CR 53529 JPB	Created
* 03/06/2013 WI 90821 JBS	Update Table to 2.0 release. Change Schema Name.
*							Add Column: factNotificationFileKey, IsDeleted.
*							Rename Column: CustomerKey to OrganizationKey,
*							factNotificationFileID to factNotificationFileKey
*							LockboxKey to ClientAccountKey, LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factNotificationFileKey to Clustered Index. 
*							Removed Constraints: DF_factNotificationFiles_CreationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_ModificationDate DEFAULT(GETDATE()),
*							DF_factNotificationFiles_CreatedBy DEFAULT(SUSER_SNAME()),
*							DF_factNotificationFiles_ModifiedBy DEFAULT(SUSER_SNAME()).
*							Added Column: IsDeleted
* 08/24/2014 WI 160437 JPB	Added UserNotification, UserID, FK to Users.
*							Changed NotificationSourceKey to SMALLINT.
* 03/06/2017 PT 139475715 JBS	Add Column FileSize.
* 02/02/2018 PT 154233497 JPB	Added MessageWorkgroups
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate factNotificationFiles
CREATE TABLE RecHubData.factNotificationFiles
(
	factNotificationFileKey BIGINT IDENTITY(1,1) NOT NULL,
	IsDeleted BIT NOT NULL,
	NotificationMessageGroup BIGINT NOT NULL,
	FileSize BIGINT NOT NULL 
		CONSTRAINT DF_factNotificationFiles_FileSize DEFAULT 0,
	UserNotification BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	UserID INT NOT NULL,
	NotificationDateKey INT NOT NULL,
	NotificationFileTypeKey INT NOT NULL, 
	NotificationSourceKey SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	CreatedBy VARCHAR(128) NOT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	FileIdentifier UNIQUEIDENTIFIER NOT NULL, 
	SourceNotificationID UNIQUEIDENTIFIER NOT NULL,
	UserFileName VARCHAR(255) NOT NULL, 
	FileExtension VARCHAR(24) NOT NULL,
	MessageWorkgroups UNIQUEIDENTIFIER NULL
) $(OnNotificationPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factNotificationFiles ADD
	CONSTRAINT PK_factNotificationFiles PRIMARY KEY NONCLUSTERED (factNotificationFileKey,NotificationDateKey),
	CONSTRAINT FK_factNotificationFiles_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factNotificationFiles_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factNotificationFiles_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factNotificationFiles_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_factNotificationFiles_NotificationDateKey FOREIGN KEY(NotificationDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factNotificationFiles_dimNotificationFileTypes FOREIGN KEY(NotificationFileTypeKey) REFERENCES RecHubData.dimNotificationFileTypes(NotificationFileTypeKey),
	CONSTRAINT FK_factNotificationFiles_dimBatchSources FOREIGN KEY(NotificationSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);

/*
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimBanks;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimOrganizations;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimClientAccounts;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_NotificationDateKey;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimNotificationFileTypes;
ALTER TABLE RecHubData.factNotificationFiles NOCHECK CONSTRAINT FK_factNotificationFiles_dimBatchSources;

*/

--WFSScriptProcessorIndex OLTA.factNotificationFiles.IDX_factNotifications_NotificationDatefactNotificationFileKey
CREATE CLUSTERED INDEX IDX_factNotifications_NotificationDatefactNotificationFileKey ON RecHubData.factNotificationFiles
(
	NotificationDateKey,
	factNotificationFileKey
) $(OnNotificationPartition);
