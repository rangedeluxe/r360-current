--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBatchPaymentSubTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/29/2014
*
* Purpose: Batch Payment Sub Types dimension is a static data dimension for batch
*		Payment Sub Types. 
*
* Modification History
* 04/29/2014 WI 138850 KLC	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBatchPaymentSubTypes
(
	BatchPaymentSubTypeKey TINYINT NOT NULL 
		CONSTRAINT PK_dimBatchPaymentSubTypes PRIMARY KEY CLUSTERED,
	BatchPaymentTypeKey TINYINT NOT NULL,
	SubTypeDescription VARCHAR(30) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchPaymentSubTypes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchPaymentSubTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchPaymentSubTypes_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchPaymentSubTypes_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.dimBatchPaymentSubTypes ADD 
	CONSTRAINT FK_dimBatchPaymentSubTypes_BatchPaymentTypeKey FOREIGN KEY (BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);