--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName ElectronicAccounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/01/2014
*
* Purpose: Associates data from the dimDDAs and Workgroups for Electronic Accounts.
*
* Modification History
* 02/27/2014 WI 131378 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.ElectronicAccounts
(
	ElectronicAccountKey		INT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_ElectronicAccounts PRIMARY KEY CLUSTERED (ElectronicAccountKey),
	DDAKey						INT NOT NULL,	
	SiteCodeID					INT NOT NULL,
	SiteBankID					INT NOT NULL,
	SiteClientAccountID			INT NOT NULL,
	CreationDate				DATETIME NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.ElectronicAccounts ADD 
	CONSTRAINT FK_ElectronicAccounts_dimDDAs FOREIGN KEY(DDAKey) REFERENCES RecHubData.dimDDAs(DDAKey);

--WFSScriptProcessorIndex RecHubData.ElectronicAccounts.IDX_ElectronicAccounts_DDAKey
CREATE UNIQUE NONCLUSTERED INDEX IDX_ElectronicAccounts_DDAKey ON RecHubData.ElectronicAccounts 
(
	DDAKey
);
--WFSScriptProcessorIndex RecHubData.ElectronicAccounts.IDX_ElectronicAccounts_SiteCodeSiteBankSiteClientAccountID
CREATE NONCLUSTERED INDEX IDX_ElectronicAccounts_SiteCodeSiteBankSiteClientAccountID ON RecHubData.ElectronicAccounts 
(
	SiteCodeID,
	SiteBankID,
	SiteClientAccountID
);
