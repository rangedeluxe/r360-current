--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName ClientAccountBatchPaymentTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 04/11/2014
*
* Purpose: Associative table for dimClientAccount and dimBatchPaymentTypes
*			to create assignment of which BatchPaymentTypes are Active.
*
* Modification History
* 04/11/2014 WI 135935 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.ClientAccountBatchPaymentTypes
(
    ClientAccountKey	INT NOT NULL,
    BatchPaymentTypeKey	TINYINT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.ClientAccountBatchPaymentTypes ADD
       CONSTRAINT PK_ClientAccountBatchPaymentTypes PRIMARY KEY CLUSTERED (ClientAccountKey, BatchPaymentTypeKey),
       CONSTRAINT FK_ClientAccountBatchPaymentTypes_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
       CONSTRAINT FK_ClientAccountBatchPaymentTypes_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);
