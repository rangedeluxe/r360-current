--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factBatchSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2019 Deluxe Corporation All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Batch.
*		   
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2009 CR 28128 JPB	Added index IDX_factBatchSummary_BankKey_CustomeKey_LockboxKey_ProcessingDateKey_DepositDateKey_BatchID
* 11/13/2009 CR 28219 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 11/19/2009 CR 28189 JPB	Allow GlobalBatchID to be NULL.
* 02/10/2010 CR 28982 JPB	Added ModificationDate.
* 01/10/2011 CR 32284 JPB	Added BatchSourceKey.
* 06/08/2011 CR 34349 JPB 	Updated clustered index for performance.
* 01/11/2012 CR 49274 JPB	Added SourceProcessingDateKey.
* 01/19/2012 CR 48978 JPB	Added BatchSiteCode.
* 03/26/2012 CR 51540 JPB	Added BatchPaymentTypeKey.
* 07/03/2012 CR 53623 JPB	Added BatchCueID.
* 07/16/2012 CR 54119 JPB	Added BatchNumber.
* 07/16/2012 CR 54129 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 WI 71798 JPB	Update table to 2.0 release. Change schema name.
*							Add columns: factBatchSummaryKey, IsDeleted, DepositStatusKey,
*							StubTotal, DepositDisplayNameKey. 
*							Delete columns: GlobalBatchId, DepositDisplayName.  
*							Rename Columns: 
*							CustomerKey to OrganizationKey,  LockboxKey to ClientAccountKey,
*							ProcessingDateKey to ImmutableDateKey.
*							LoadDate to CreationDate.
*							Renamed ALL constraints.
*							Added factBatchSummaryKey to end of Clustered Index.
*							Create New Index based from old clustered index.
*							FP: WI 77752, WI 85066, WI 85092, WI 85093, WI 85098, WI 87220
* 04/09/2014 WI 135312 JPB	Changed BatchID from INT to BIGINT.
* 05/22/2014 WI	143850 JPB	Add SourceBatchID.
* 05/22/2014 WI 143851 JPB	Changed BatchSourceKey to SMALLINT.
* 09/28/2014 WI 168336 JPB	Added BatchExceptionStatuKey.
* 06/19/2015 WI 219201 MGE  Added index per 2.01 regression testing analysis
* 03/20/2019 R360-15940 MGE	Added index columns for Dashboard response improvement
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factBatchSummary
(
	factBatchSummaryKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL, --CR 28219 JPB 11/13/2009
	ImmutableDateKey INT NOT NULL, --CR 28219 JPB 11/13/2009
	SourceProcessingDateKey INT NOT NULL, --CR 49274 JPB 01/11/2012
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL, --CR 54119 JPB 07/16/2012
	BatchSourceKey SMALLINT NOT NULL, --CR 32284 JPB 01/10/2011
	BatchPaymentTypeKey TINYINT NOT NULL, --CR 51540 JPB 03/26/2012
	BatchExceptionStatusKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL, --CR 53623 JPB 07/03/2012
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	DepositStatusKey INT NOT NULL,
	TransactionCount INT NOT NULL,
	CheckCount INT NOT NULL,
	ScannedCheckCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	CheckTotal MONEY NOT NULL,
	StubTotal MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL, --CR 28982 JPB 02/10/2010
	BatchSiteCode INT NULL, --CR 48978 JPB 01/19/2012
	DepositDDA VARCHAR(40) NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factBatchSummary ADD 
	CONSTRAINT PK_factBatchSummary PRIMARY KEY NONCLUSTERED (factBatchSummaryKey,DepositDateKey),
	CONSTRAINT FK_factBatchSummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factBatchSummary_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factBatchSummary_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factBatchSummary_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchSummary_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchSummary_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchSummary_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factBatchSummary_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factBatchSummary_dimDepositStatuses FOREIGN KEY(DepositStatusKey) REFERENCES RecHubData.dimDepositStatuses(DepositStatusKey),
	CONSTRAINT FK_factBatchSummary_dimBatchExceptionStatuses FOREIGN KEY(BatchExceptionStatusKey) REFERENCES RecHubData.dimBatchExceptionStatuses(BatchExceptionStatusKey);
/*
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimBanks;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimOrganizations;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimClientAccounts;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_DepositDate;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_ImmutableDate;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_SourceProcessingDate;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimBatchSources;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimBatchPaymentTypes;
ALTER TABLE RecHubData.factBatchSummary NOCHECK CONSTRAINT FK_factBatchSummary_dimDepositStatuses;
*/
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_DepositDatefactBatchSummaryKey
CREATE CLUSTERED INDEX IDX_factBatchSummary_DepositDatefactBatchSummaryKey ON RecHubData.factBatchSummary 
(
	DepositDateKey,
	factBatchSummaryKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_DepositDateLockboxKeyBatchID
CREATE INDEX IDX_factBatchSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factBatchSummary 
(
	DepositDateKey,
	ClientAccountKey,
	BatchID,
	IsDeleted,
	DepositStatus,
	BatchSourceKey,
	BatchPaymentTypeKey
) 
INCLUDE
(
	CheckCount,
	CheckTotal,
	TransactionCount
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_BankKey
CREATE INDEX IDX_factBatchSummary_BankKey ON RecHubData.factBatchSummary (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_ClientAccountKey
CREATE INDEX IDX_factBatchSummary_ClientAccountKey ON RecHubData.factBatchSummary (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_OrganizationKey
CREATE INDEX IDX_factBatchSummary_OrganizationKey ON RecHubData.factBatchSummary (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_SourceProcessingDateKey
CREATE INDEX IDX_factBatchSummary_SourceProcessingDateKey ON RecHubData.factBatchSummary (SourceProcessingDateKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber
----CR 54129 Renamed and updated index with BatchNumber.
CREATE INDEX IDX_factBatchSummary_BankOrganizationClientAccountSourceProcessingDateDepositDateKeyBatchIDBatchNumber ON RecHubData.factBatchSummary 
(
	BankKey ASC,
	OrganizationKey ASC,
	ClientAccountKey ASC,
	SourceProcessingDateKey ASC,
	DepositDateKey ASC,
	BatchID ASC,
	BatchNumber ASC
) $(OnDataPartition); -- (CR 28128 - 11/09/2009 JPB)
--WI 71798 (FP:77752)
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_LockboxBankSourceProcessingDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountBankSourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary
(
    ClientAccountKey ASC,
    BankKey ASC,
    SourceProcessingDateKey ASC,
    BatchID ASC
)
INCLUDE 
( 
	BatchSourceKey
) $(OnDataPartition);
--WI 71798 (FP:85092)  -- Removed GlobalBatchID from index
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_factBatchSummary_CreationDateClientAccountSourceProcessingDateDepositDateKeyBatchID ON RecHubData.factBatchSummary
(
	CreationDate ASC,
	ClientAccountKey ASC,
	SourceProcessingDateKey ASC,
	DepositDateKey ASC, 
	BatchID ASC
) $(OnDataPartition);
--WI 71798 (FP:85093)
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_SourceProcessingDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_factBatchSummary_SourceProcessingDateKeyBatchID ON RecHubData.factBatchSummary
(
	SourceProcessingDateKey ASC,
	BatchID ASC
)
INCLUDE 
( 
	BankKey,
	ClientAccountKey,
	DepositDateKey
) $(OnDataPartition);
--WI 71798 (FP:85098)
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountSourceProcessingDateKeyBatchID  ON RecHubData.factBatchSummary
(
	ClientAccountKey ASC,
	SourceProcessingDateKey ASC,
	BatchID ASC
)
INCLUDE 
( 
	BankKey,
	BatchSourceKey
) $(OnDataPartition);
--WI 71798 (FP:87220)
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey
CREATE NONCLUSTERED INDEX IDX_factBatchSummary_ClientAccountKeyBatchIDSourceProcessingDateKey  ON RecHubData.factBatchSummary
(
	ClientAccountKey ASC,
	BatchID ASC,
	SourceProcessingDateKey ASC
) $(OnDataPartition);
--WI 219201
--WFSScriptProcessorIndex RecHubData.factBatchSummary.IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey
CREATE INDEX IDX_factBatchSummary_BatchIDDepositDatefactBatchSummaryIsDeletedSourceProcessingDateImmutableDateKey ON RecHubData.factBatchSummary
(
	BatchID ASC,
	DepositDateKey ASC,
	factBatchSummaryKey ASC,
	IsDeleted ASC,
	SourceProcessingDateKey ASC,
	ImmutableDateKey ASC
)
INCLUDE
(
	SourceBatchID,
	BankKey,
	ClientAccountKey,
	OrganizationKey,
	BatchPaymentTypeKey
) $(OnDataPartition);