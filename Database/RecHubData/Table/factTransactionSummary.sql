--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factTransactionSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Transaction.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28224 JPB	ProcessingDateKey and DepositDateKey are now NOT 
*							NULL.
* 03/11/2010 CR 29181 JPB	Added new index for Lockbox Search.
* 04/12/2010 CR 29358 JPB	Added ModificationDate.
* 01/12/2011 CR 32285 JPB	Added BatchSourceKey.
* 10/27/2011 CR 47513 JPB	Added TxnSequence to index.
* 01/12/2012 CR 49275 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49552 JPB	Added BatchSiteCode.
* 03/20/2012 CR 51367 JPB	Added INCLUDE columns.
* 03/27/2012 CR 51546 JPB	Added BatchPaymentTypeKey.
* 07/05/2012 CR 53624 JPB	Added BatchCueID.
* 07/17/2012 CR 54120 JPB	Added BatchNumber.
* 07/17/2012 CR 54138 JPB	Renamed and updated index with BatchNumber.
* 03/01/2013 CR 71799 JBS 	Update table to 2.0 release. Change schema Name.
*							Add Column: factTransactionSummaryKey, IsDeleted, StubTotal.
*							Remove Column: DESetupID. 
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factTransactionSummaryKey to Clustered Index. 
*							Create New Index based from old clustered index.
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factTransactionSummary_BatchPaymentTypeKey DEFAULT(0),
*							DF_factTransactionSummary_BatchCueID DEFAULT(-1), DF_factTransactionSummary_ModificationDate DEFAULT GETDATE().
*							Forward Patch:  WI 85096
* 05/29/2014 WI 135323 JPB	Changed BatchID to BIGINT.
* 05/29/2014 WI 143853 JPB	Added SourceBatchID.
* 05/29/2014 WI 143854 JPB	Changed SourceBatchKey to SMALLINT.
* 08/18/2014 WI 158618 JPB	Added TransactionExceptionStatuKey.
* 06/10/2015 WI 217860 JBS	Add Index from Regression Analysis.
* 06/19/2015 WI 219268 JBS  Add Index IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factTransactionSummary
(
	factTransactionSummaryKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	TransactionExceptionStatusKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	CheckCount INT NOT NULL,
	ScannedCheckCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	OMRCount INT NOT NULL,
	CheckTotal MONEY NOT NULL,
	StubTotal MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factTransactionSummary ADD 
	CONSTRAINT PK_factTransactionSummary PRIMARY KEY NONCLUSTERED (factTransactionSummaryKey,DepositDateKey),
	CONSTRAINT FK_factTransactionSummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factTransactionSummary_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factTransactionSummary_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factTransactionSummary_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factTransactionSummary_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factTransactionSummary_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factTransactionSummary_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factTransactionSummary_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factTransactionSummary_dimTransactionExceptionStatuses FOREIGN KEY(TransactionExceptionStatusKey) REFERENCES RecHubData.dimTransactionExceptionStatuses(TransactionExceptionStatusKey);
/*
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_dimBanks;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_dimCustomers;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_dimClientAccounts;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_DepositDate;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_ImmutableDate;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_SourceProcessingDate;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_dimBatchSources;
ALTER TABLE RecHubData.factTransactionSummary NOCHECK CONSTRAINT FK_factTransactionSummary_dimBatchPaymentTypes;
*/
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey
CREATE CLUSTERED INDEX IDX_factTransactionSummary_DepositDatefactTransactionSummaryKey ON RecHubData.factTransactionSummary 
(
	DepositDateKey,
	factTransactionSummaryKey
) $(OnDataPartition);
--WFSScriptProcessorIndex OLTA.factTransactionSummary.IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID
CREATE INDEX IDX_factTransactionSummary_DepositDateClientAccountKeyBatchID ON RecHubData.factTransactionSummary 
(
	DepositDateKey,
	ClientAccountKey,
	BatchID
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_BankKey
CREATE INDEX IDX_factTransactionSummary_BankKey ON RecHubData.factTransactionSummary (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_ClientAccountKey
CREATE INDEX IDX_factTransactionSummary_ClientAccountKey ON RecHubData.factTransactionSummary (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_OrganizationKey
CREATE INDEX IDX_factTransactionSummary_OrganizationKey ON RecHubData.factTransactionSummary (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_ImmutableDateKey
CREATE INDEX IDX_factTransactionSummary_ImmutableDateKey ON RecHubData.factTransactionSummary (ImmutableDateKey) $(OnDataPartition);
--CR 29181, CR 51367, 54138
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence
CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_LockboxDepositDateKeyBatchIDNumberDepositStatusBankCustomerProcessingDateKeyTransactonIDTxnSequence ON RecHubData.factTransactionSummary
(
	ClientAccountKey ASC,
	DepositDateKey ASC,
	BatchID ASC,
	BatchNumber ASC,
	DepositStatus ASC,
	BankKey ASC,
	OrganizationKey ASC,
	ImmutableDateKey ASC,
	TransactionID ASC,
	TxnSequence ASC
)
INCLUDE 
( 
	CheckCount,
	DocumentCount,
	ScannedCheckCount,
	StubCount,
	OMRCount,
	CheckTotal
) $(OnDataPartition);
--WI 71799 (FP:85096)
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_BankOrganizationClientAccountImmutableDateDepositDateKeyBatchID ON RecHubData.factTransactionSummary
(
	BankKey ASC,
	OrganizationKey ASC,
	ClientAccountKey ASC,
	ImmutableDateKey ASC,
	DepositDateKey ASC,
	BatchID ASC
) $(OnDataPartition);
--WI 217860 
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount
CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_IsDeletedDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary 
(
	IsDeleted,
	DepositDateKey,
	SourceBatchID,
	BatchNumber,
	DepositStatus,
	OMRCount
)
INCLUDE 
(
	BankKey,
	OrganizationKey,
	ClientAccountKey,
	ImmutableDateKey,
	BatchID,
	BatchSourceKey,
	BatchPaymentTypeKey,
	TransactionID,
	TxnSequence,
	CheckCount,
	StubCount,
	DocumentCount
) $(OnDataPartition);
--WI 219268 
--WFSScriptProcessorIndex RecHubData.factTransactionSummary.IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount
CREATE NONCLUSTERED INDEX IDX_factTransactionSummary_IsDeletedBatchSourceDepositDateKeySourceBatchIDBatchNumberDepositStatusOMRCount ON RecHubData.factTransactionSummary 
(
	IsDeleted,
	BatchSourceKey,
	DepositDateKey,
	SourceBatchID,
	BatchNumber,
	DepositStatus,
	OMRCount
)
INCLUDE 
(
	BankKey,
	OrganizationKey,
	ClientAccountKey,
	ImmutableDateKey,
	BatchID,
	BatchPaymentTypeKey,
	TransactionID,
	TxnSequence,
	CheckCount,
	StubCount,
	DocumentCount
) $(OnDataPartition);

