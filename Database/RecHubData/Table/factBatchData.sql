--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable factBatchData
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Grain: one row for every item data
*
*
* Modification History
* 03/07/2011 CR 33212 JPB	Created
* 01/12/2012 CR 49281 JPB	Added SourceProcessingDateKey.
* 07/19/2012 CR 54127 JPB	Added BatchNumber.
* 09/08/2012 CR 55277 JPB	Expanded DataValue to 1024.
* 02/14/2013 WI 90207 JBS	Update to 2.0 release.  Change schema name.
*							Renaming columns:
*							CustomerKey to OrganizationKey
*							LockboxKey to ClientAccountKey
*							ProcessingDateKey to ImmutableDateKey
*							LoadDate to CreationDate
*							Renaming all Constraints. 
*							Removed Default Constraints on ModifiedDate, ModifiedBy
*							Change ModifiedBy from 	32 to 128
*							Added factBatchDataKey to the Clustered Index.
* 05/31/2014 WI 135310 JPB	Changed BatchID from INT to BIGINT.
* 05/31/2014 WI 144957 JPB	Add SourceBatchID.
* 03/19/2015 WI 196952 JPB	Added BatchSourceKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factBatchData
(
	factBatchDataKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchDataSetupFieldKey INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataValueDateTime DATETIME NULL,
	DataValueMoney MONEY NULL,
	DataValueInteger INT NULL,
	DataValueBit BIT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	DataValue VARCHAR(1024) NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factBatchData ADD 
	CONSTRAINT PK_factBatchData PRIMARY KEY NONCLUSTERED (factBatchDataKey,DepositDateKey),
	CONSTRAINT FK_factBatchData_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factBatchData_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factBatchData_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factBatchData_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchData_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchData_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchData_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factBatchData_dimBatchDataSetupFields FOREIGN KEY(BatchDataSetupFieldKey) REFERENCES RecHubData.dimBatchDataSetupFields(BatchDataSetupFieldKey);
--WFSScriptProcessorPrint Disabling Foreign Key
/*
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_dimBanks;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_dimOrganizations;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_dimClientAccounts;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_DepositDate;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_ImmutableDate;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_SourceProcessingDate;
ALTER TABLE RecHubData.factBatchData NOCHECK CONSTRAINT FK_factBatchData_dimBatchDataSetupFields;

*/
--WFSScriptProcessorIndex RecHubData.factBatchData.IDX_factBatchData_DepositDatefactBatchDataKey
CREATE CLUSTERED INDEX IDX_factBatchData_DepositDatefactBatchDataKey ON RecHubData.factBatchData 
(
	DepositDateKey,
	factBatchDataKey
);
