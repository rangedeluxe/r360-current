--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factBatchExtracts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
*
* Modification History
* 06/04/2013 WI 103915 JMC  Initial Version
*							Added ModificationDate for purge process
* 05/22/2014 WI 135311 JPB	Changed BatchID from INT to BIGINT.
* 05/22/2014 WI	143850 JPB	Add SourceBatchID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factBatchExtracts
(
	factBatchExtractKey		BIGINT IDENTITY(1, 1),
	IsDeleted				BIT NOT NULL,
	BankKey					INT NOT NULL,
	OrganizationKey			INT NOT NULL,
	ClientAccountKey		INT NOT NULL,
	DepositDateKey			INT NOT NULL,
	ImmutableDateKey		INT NOT NULL,
	BatchID					BIGINT NOT NULL,
	SourceBatchID			BIGINT NOT NULL,
	ExtractSequenceNumber	BIGINT NOT NULL,
	ModificationDate		DATETIME NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factBatchExtracts ADD 
	CONSTRAINT PK_factBatchExtracts PRIMARY KEY NONCLUSTERED (factBatchExtractKey,DepositDateKey),
	CONSTRAINT FK_factBatchExtracts_DepositDates FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factBatchExtracts_ImmutableDates FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey);

--WFSScriptProcessorIndex RecHubData.factBatchExtracts.IDX_factBatchExtracts_DepositDatefactBatchExtractKey
CREATE CLUSTERED INDEX IDX_factBatchExtracts_DepositDatefactBatchExtractKey ON RecHubData.factBatchExtracts 
(
	DepositDateKey,
	factBatchExtractKey
) $(OnDataPartition);
