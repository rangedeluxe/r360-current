--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factDataEntrySummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Data Entry Item.
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28221 JPB	ProcessingDateKey now NOT NULL.
* 02/12/2010 CR 28240 JPB	Added missing index on processing date key.
* 02/12/2010 CR 29011 JPB	Added missing foreign key on processing date.
* 01/13/2011 CR 32301 JPB	Added BatchSourceKey.
* 01/11/2012 CR 49279 JPB	Added SourceProcessingDateKey.
* 03/27/2012 CR 51543 JPB	Added BatchPaymentTypeKey
* 07/16/2012 CR 54126 JPB	Added BatchNumber.
* 03/05/2013 WI 90495 JBS	Update table to 2.0 release.  Change Schema Name
*							Add Columns: factDataEntrySummaryKey, IsDeleted.
*							Rename Columns: LockboxKey to ClientAccountKey,
*							CustomerKey to OrganizationKey, LoadDate to CreationDate,
*							ProcessingDateKey to ImmutableDateKey.
*							Renamed FK constraints and indexes to match column renaming. 
*							Added factDataEntrySummaryKey to Clustered index. 
*							Remove Constraint: DF_factDataEntrySummary_BatchPaymentTypeKey DEFAULT(0)
*							Remove column:  GLobalBatchID
* 05/30/2014 WI 144026 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 144818 JPB	Added SourceBatchID.
* 05/30/2014 WI 144819 JPB	Changed SourceBatchKey to SMALLINT.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factDataEntrySummary
(
	factDataEntrySummaryKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL, 
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL, 
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL, 
	DepositStatus INT NOT NULL,
	DataEntryColumnKey INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factDataEntrySummary ADD
	CONSTRAINT PK_factDataEntrySummary PRIMARY KEY NONCLUSTERED (factDataEntrySummaryKey,DepositDateKey),
	CONSTRAINT FK_factDataEntrySummary_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factDataEntrySummary_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factDataEntrySummary_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factDataEntrySummary_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntrySummary_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntrySummary_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntrySummary_dimDataEntryColumns FOREIGN KEY(DataEntryColumnKey) REFERENCES RecHubData.dimDataEntryColumns(DataEntryColumnKey),
	CONSTRAINT FK_factDataEntrySummary_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factDataEntrySummary_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_DepositDateKey
CREATE CLUSTERED INDEX IDX_factDataEntrySummary_DepositDatefactDataEntrySummaryKey ON RecHubData.factDataEntrySummary
(
	DepositDateKey, 
	factDataEntrySummaryKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_ImmutableDateKey
CREATE INDEX IDX_factDataEntrySummary_ImmutableDateKey ON RecHubData.factDataEntrySummary(ImmutableDateKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_BankKey
CREATE INDEX IDX_factDataEntrySummary_BankKey ON RecHubData.factDataEntrySummary (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_OrganizationKey
CREATE INDEX IDX_factDataEntrySummary_OrganizationKey ON RecHubData.factDataEntrySummary (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_ClientAccountKey
CREATE INDEX IDX_factDataEntrySummary_ClientAccountKey ON RecHubData.factDataEntrySummary (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntrySummary.IDX_factDataEntrySummary_DataEntryColumnKey
CREATE INDEX IDX_factDataEntrySummary_DataEntryColumnKey ON RecHubData.factDataEntrySummary (DataEntryColumnKey) $(OnDataPartition);
