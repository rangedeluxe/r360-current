--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimNotificationFileTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2012
*
* Purpose: 
*		   
*
* Modification History
* 05/30/2012 CR 53199 JPB	Created
* 02/14/2013 WI 90418 JBS	Update table to 2.0 release.  Change Schema Name
*							Change column length on ModifiedBy and CreatedBy
*							From 32 to 128						
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimNotificationFileTypes
CREATE TABLE RecHubData.dimNotificationFileTypes
(
	NotificationFileTypeKey INT NOT NULL	
		IDENTITY(1,1) CONSTRAINT PK_dimNotificationFileTypes PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_dimNotificationFileTypes_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimNotificationFileTypes_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimNotificationFileTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimNotificationFileTypes_ModifiedBy DEFAULT(SUSER_SNAME()),
	FileType VARCHAR(32) NOT NULL,
	FileTypeDescription VARCHAR(255) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimNotificationFileTypes.IDX_dimNotificationFileTypes_FileType
CREATE UNIQUE INDEX IDX_dimNotificationFileTypes_FileType ON RecHubData.dimNotificationFileTypes(FileType);
