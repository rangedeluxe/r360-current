--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimSiteCodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Site Code dimension is a SCD type 2 holding Site Code info.  Most 
*	recent flag of 1 indicates the current Site Code row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/21/2012 CR 50464 JPB	Added columnn AutoUpdateCurrentProcessingDate.
* 02/18/2013 WI 90199 JBS	Update table to 2.0 release, Change Schema Name
*							Remove columns: CreatedBy, ModifiedBy
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimSiteCodes
(
	SiteCodeID INT NOT NULL 
		CONSTRAINT PK_dimSiteCodes PRIMARY KEY CLUSTERED,
	CWDBActiveSite BIT NOT NULL 
		CONSTRAINT DF_dimSiteCodes_CWDBActiveSite DEFAULT (0),
	LocalTimeZoneBias INT NOT NULL 
		CONSTRAINT DF_dimSiteCodes_LocalTimeZoneBias DEFAULT (0),
	AutoUpdateCurrentProcessingDate BIT NOT NULL
		CONSTRAINT DF_dimSiteCodes_AutoUpdateCurrentProcessingDate DEFAULT(0),
	CurrentProcessingDate DATETIME,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimSiteCodes_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimSiteCodes_ModificationDate DEFAULT(GETDATE()),
	ShortName VARCHAR(30) NOT NULL,
	LongName VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimSiteCodes.IDX_dimSiteCodes_SiteCodeID
CREATE INDEX IDX_dimSiteCodes_SiteCodeID ON RecHubData.dimSiteCodes (SiteCodeID);
