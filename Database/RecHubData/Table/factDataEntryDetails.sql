--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factDataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2018 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every data entry field.
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/14/2009 CR 28223 JPB	ProcessingDateKey now NOT NULL.
* 12/23/2009 CR 28238  CS	Add index on ProcessingDateKey
* 02/10/2010 CR 28977 JPB	Added ModificationDate.
* 02/12/2010 CR 29012 JPB	Added missing foreign key on processing date.
* 01/06/2011 CR 32233 JPB	Added native data types.
* 01/11/2011 CR 32302 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49280 JPB	Added SourceProcessingDateKey.
* 03/20/2012 CR 51368 JPB	Created new index.
* 03/26/2012 CR 51542 JPB	Added BatchPaymentTypeKey
* 07/19/2012 CR 54125 JPB	Added BatchNumber.
* 07/19/2012 CR 54134 JPB	Renamed and updated index with BatchNumber.
* 03/05/2013 WI 90483 JBS	Update table to 2.0 release.  Change Schema Name.
*							Added factDataEntryDetailKey to Clustered Index.
*							Changed Indexes to match schema and column renaming.
*							Added Columns: factDataEntryDetailKey, IsDeleted
*							Rename Columns: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey
*							LoadDate to CreationDate.
*							Remove: constraint on ModificationDate, column GlobalBatchID
*							Forward patch:  WI 83270, WI 87217
* 05/30/2014 WI 144025 JPB	Changed BatchID from INT to BIGINT.
* 05/30/2014 WI 144898 JPB	Added SourceBatchID.
* 05/30/2014 WI 144899 JPB	Changed SourceBatchKey to SMALLINT.
* 06/10/2015 WI 217785 JBS  Adding indexes from Regression Analysis.
* 07/07/2015 WI 221748 JPB	Added WorkgroupDataEntryColumnKey.
* 08/18/2015 WI 230082 JPB	Remove dimDataEntry items.
* 02/06/2018 PT 154229602	MGE	Added index to support Advanced Search
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factDataEntryDetails
(
	factDataEntryDetailKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	DepositStatus INT NOT NULL,
	WorkgroupDataEntryColumnKey BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue VARCHAR(256) NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factDataEntryDetails ADD
	CONSTRAINT PK_factDataEntryDetails PRIMARY KEY NONCLUSTERED (factDataEntryDetailKey,DepositDateKey),
	CONSTRAINT FK_factDataEntryDetails_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factDataEntryDetails_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factDataEntryDetails_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factDataEntryDetails_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataEntryDetails_dimWorkgroupDataEntryColumns FOREIGN KEY(WorkgroupDataEntryColumnKey) REFERENCES RecHubData.dimWorkgroupDataEntryColumns(WorkgroupDataEntryColumnKey),
	CONSTRAINT FK_factDataEntryDetails_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factDataEntryDetails_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey
CREATE CLUSTERED INDEX IDX_factDataEntryDetails_DepositDatefactDataEntryDetailKey ON RecHubData.factDataEntryDetails
(
	DepositDateKey,
	factDataEntryDetailKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_BankKey
CREATE INDEX IDX_factDataEntryDetails_BankKey ON RecHubData.factDataEntryDetails (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_OrganizationKey
CREATE INDEX IDX_factDataEntryDetails_OrganizationKey ON RecHubData.factDataEntryDetails (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ClientAccountKey
CREATE INDEX IDX_factDataEntryDetails_ClientAccountKey ON RecHubData.factDataEntryDetails (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey
CREATE INDEX IDX_factDataEntryDetails_WorkgroupDataEntryColumnKey ON RecHubData.factDataEntryDetails (WorkgroupDataEntryColumnKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ImmutableDateKey
CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKey ON RecHubData.factDataEntryDetails (ImmutableDateKey) $(OnDataPartition);
--CR 51368, 54134,230082
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_WGDataEntryColumnDepositDateBankOrganizationClientAccountImmutableDateKeyBatchIDNumberTransactionID ON RecHubData.factDataEntryDetails
(
    WorkgroupDataEntryColumnKey ASC,
    DepositDateKey ASC,
    BankKey ASC,
    OrganizationKey ASC,
    ClientAccountKey ASC,
    ImmutableDateKey ASC,
    BatchID ASC,
    BatchNumber ASC,
    TransactionID ASC
)
INCLUDE 
( 
	BatchSequence,
	DataEntryValue,
	DataEntryValueDateTime,
	DataEntryValueMoney
) $(OnDataPartition);
-- WI 90483 (FP:83270), 230082
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateKeyBatchTransactionIDBatchSequence ON RecHubData.factDataEntryDetails
(
	DepositDateKey ASC,
	BatchID ASC,
	TransactionID ASC,
	BatchSequence ASC
)
INCLUDE
(          
	OrganizationKey,
	WorkgroupDataEntryColumnKey,
	DataEntryValue
)  $(OnDataPartition);
-- WI 90483 (FP:87217)
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID
CREATE NONCLUSTERED INDEX IDX_factDataEntryDetails_DepositDateOrganizationKeyBatchTransactionID ON RecHubData.factDataEntryDetails
(
	DepositDateKey ASC,
	OrganizationKey ASC,
	BatchID ASC,
	TransactionID ASC
) $(OnDataPartition);
-- WI 217785, 230082
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey
CREATE INDEX IDX_factDataEntryDetails_ImmutableDateKeySourceBatchTransactionIDBatchSequenceDepositDateKey ON RecHubData.factDataEntryDetails 
(
	ImmutableDateKey, 
	SourceBatchID, 
	TransactionID, 
	BatchSequence,
	DepositDateKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	SourceProcessingDateKey, 
	BatchID, 
	BatchNumber, 
	BatchSourceKey, 
	BatchPaymentTypeKey, 
	DepositStatus, 
	WorkgroupDataEntryColumnKey, 
	CreationDate, 
	ModificationDate, 
	DataEntryValueDateTime, 
	DataEntryValueFloat, 
	DataEntryValueMoney, 
	DataEntryValue
) $(OnDataPartition);
-- WI 217785, 230082 
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_SourceBatchIDDepositDateKey
CREATE INDEX IDX_factDataEntryDetails_SourceBatchIDDepositDateKey ON RecHubData.factDataEntryDetails 
(
	SourceBatchID,
	DepositDateKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	ImmutableDateKey,
	SourceProcessingDateKey, 
	BatchID, 
	BatchNumber, 
	BatchSourceKey, 
	BatchPaymentTypeKey, 
	DepositStatus, 
	WorkgroupDataEntryColumnKey,
	TransactionID, 
	BatchSequence, 
	CreationDate, 
	ModificationDate, 
	DataEntryValueDateTime, 
	DataEntryValueFloat, 
	DataEntryValueMoney,
	DataEntryValue
) $(OnDataPartition);

-- PT 154229602
--WFSScriptProcessorIndex RecHubData.factDataEntryDetails.IDX_factDataEntryDetails_DataEntryValue_ClientAccountKey_BankKey
CREATE INDEX IDX_factDataEntryDetails_DataEntryValue_ClientAccountKey_BankKey ON RecHubData.factDataEntryDetails 
(
	DataEntryValue,
	ClientAccountKey,
	BankKey
) 
INCLUDE 
(
	factDataEntryDetailKey, 
	IsDeleted, 
	DepositDateKey,
	BatchID,
	TransactionID,
	BatchSequence
) $(OnDataPartition);