--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimTransactionExceptionStatuses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/18/2014
*
* Purpose: Static data dimension for Exception Statuses. 
*
* Modification History
* 08/18/2014 WI 158619 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimTransactionExceptionStatuses
(
	TransactionExceptionStatusKey TINYINT NOT NULL
		CONSTRAINT PK_dimTransactionExceptionStatuses PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimTransactionExceptionStatuses_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimTransactionExceptionStatuses_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimTransactionExceptionStatuses_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimTransactionExceptionStatuses_ModifiedBy DEFAULT(SUSER_SNAME()),
	ExceptionStatusName VARCHAR(36) NOT NULL,
	ExceptionStatusDescripion VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimTransactionExceptionStatuses.IDX_dimTransactionExceptionStatuses_ExceptionStatusName
CREATE UNIQUE INDEX IDX_dimTransactionExceptionStatuses_ExceptionStatusName ON RecHubData.dimTransactionExceptionStatuses(ExceptionStatusName);
