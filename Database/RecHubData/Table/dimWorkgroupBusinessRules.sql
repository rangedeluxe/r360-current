--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimWorkgroupBusinessRules
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2017-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 01/05/2017
*
* Purpose: Workgoup Business Rules dimension is a type 0 SCD holding  
*	an entry for each Workgroup/BatchSource business rule. 
*		   
*
* Modification History
* 01/05/2017 PT#132047521 JPB	Created
* 02/03/2017 PT 138212537 JBS	Added BalancingRequired
* 03/28/2017 PT#142143773 MAA	Changed PaymentsOnlyTransaction to InvoiceRequired
* 07/14/2017 PT#141767379 MAA	Added PostDepositBalancingRequired
* 01/18/2019 R360-14696	  JPB	Added PostDepositPayerRequired
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimWorkgroupBusinessRules
(
	dimWorkgroupBusinessRuleKey BIGINT IDENTITY(1,1) NOT NULL 
		CONSTRAINT PK_dimWorkgroupBusinessRules PRIMARY KEY CLUSTERED,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	InvoiceRequired BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupBusinessRules_InvoiceRequired DEFAULT 0,
	BalancingRequired BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupBusinessRules_BalancingRequired DEFAULT 0,
	PostDepositBalancingRequired BIT NOT NULL 
			CONSTRAINT DF_dimWorkgroupBusinessRules_PostDepositBalancingRequired DEFAULT 0,
	PostDepositPayerRequired BIT NOT NULL
			CONSTRAINT DF_dimWorkgroupBusinessRules_PostDepositPayerRequired DEFAULT 0,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimWorkgroupBusinessRules_ModificationDate DEFAULT GETDATE()
);
--WFSScriptProcessorTableProperties

--WFSScriptProcessorIndex RecHubData.dimWorkgroupBusinessRules.IDX_dimWorkgroupBusinessRules_BankClientAccountID
CREATE UNIQUE NONClUSTERED INDEX IDX_dimWorkgroupBusinessRules_BankClientAccountID ON RecHubData.dimWorkgroupBusinessRules
(
	SiteBankID,
	SiteClientAccountID
)
INCLUDE
(
	InvoiceRequired,
	BalancingRequired,
	PostDepositBalancingRequired,
	PostDepositPayerRequired
);
