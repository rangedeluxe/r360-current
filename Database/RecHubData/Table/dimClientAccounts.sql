--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimClientAccounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2019 Deluxe Corporation All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Lockboxes dimension is a SCD type 2 holding Lockbox info.  Most 
*	recent flag of 1 indicates the current Lockbox row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 05/05/2010 CR 29158 JPB	Added IsCommingled.
* 05/13/2010 CR 29709 JPB	Added SiteLockboxKey.
* 08/02/2010 CR 30378 JPB	Added standard ModificationDate.
* 08/02/2010 CR 30307 JPB	Added default contraint 1 to OnlineColorMode.
* 01/07/2011 CR 31470 JPB 	Added POBox
* 09/11/2012 CR 55090 JPB	Added FileGroup
* 11/01/2012 CR 56617 JPB	Added new index.
* 03/01/2013 WI 89985 JBS	Update table to 2.0 release. Change Schema Name
*							Renamed all Constraints, Renamed Columns:
*							LockboxKey to ClientAccountKey
*							SiteCustomerID to SiteOrganizationID
*							SiteLockboxID to SiteClientAccountID
*							LoadDate to CreationDate
*							SiteLockboxKey to SiteClientAccountKey 
*							Renamed SiteCode to SiteCodeID, add FK to dimSiteCodes
*							FP: WI 85044, WI 85094, WI 87239
* 05/07/2013 WI 96817 JBS	Added columns for Purging process:
*							DataRetentionDays,  ImageRetentionDays
* 06/03/2015 WI 216837 JBS	Add index IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive
* 03/20/2019 R360-15940 MGE Update index to support Dashboard performance
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimClientAccounts
CREATE TABLE RecHubData.dimClientAccounts
(
	ClientAccountKey INT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_dimClientAccounts PRIMARY KEY CLUSTERED,
	SiteCodeID INT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteOrganizationID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	MostRecent BIT NOT NULL,
	IsActive TINYINT NOT NULL,
	CutOff TINYINT NOT NULL,
	OnlineColorMode TINYINT NOT NULL
		CONSTRAINT DF_dimClientAccounts_OnlineColorMode DEFAULT 1,
	IsCommingled BIT NOT NULL
        CONSTRAINT DF_dimClientAccounts_IsCommingled DEFAULT 0,
	DataRetentionDays SMALLINT NOT NULL,
	ImageRetentionDays SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimClientAccounts_ModificationDate DEFAULT GETDATE(),
	SiteClientAccountKey UNIQUEIDENTIFIER NOT NULL 
		CONSTRAINT DF_dimClientAccounts_SiteClientAccountKey DEFAULT '00000000-0000-0000-0000-000000000000',
	ShortName VARCHAR(20) NOT NULL,
	LongName VARCHAR(40) NULL,
	POBox VARCHAR(32) NULL,
	DDA VARCHAR(40) NULL,
	[FileGroup] VARCHAR(256) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.dimClientAccounts ADD
	CONSTRAINT FK_dimClientAccounts_dimSiteCodes FOREIGN KEY(SiteCodeID) REFERENCES RecHubData.dimSiteCodes(SiteCodeID);
--WFSScriptProcessorIndex RecHubData.dimClientAccounts.IDX_dimClientAccounts_SiteBankOrganizationClientAccountID
CREATE INDEX IDX_dimClientAccounts_SiteBankOrganizationClientAccountID ON RecHubData.dimClientAccounts 
(
	SiteBankID ASC,
	SiteOrganizationID ASC, 
	SiteClientAccountID ASC
);
--CR 56617 
--WI 89985 (FP:85044)
--WFSScriptProcessorIndex RecHubData.IDX_dimClientAccounts_SiteBankClientAccountID
CREATE NONCLUSTERED INDEX IDX_dimClientAccounts_SiteBankClientAccountID ON RecHubData.dimClientAccounts
(
	SiteBankID ASC,
	SiteClientAccountID ASC
)
INCLUDE
(
	SiteOrganizationID,
	ClientAccountKey
);
--WI 89985 (FP:85094)
--WFSScriptProcessorIndex RecHubData.dimClientAccounts.IDX_dimClientAccounts_SiteClientAccountIDMostRecent
CREATE INDEX IDX_dimClientAccounts_SiteClientAccountIDMostRecent ON RecHubData.dimClientAccounts 
(
	SiteClientAccountID ASC, 
	MostRecent ASC
);
--WI 89985 (FP:87239)
--WFSScriptProcessorIndex RecHubData.dimClientAccounts.IDX_dimLockboxes_SiteBankLockboxIDMostRecent
CREATE INDEX IDX_dimClientAccounts_SiteBankClientAccountIDMostRecent ON RecHubData.dimClientAccounts
(
	SiteBankID ASC,
	SiteClientAccountID ASC,
	MostRecent ASC
)
INCLUDE
(
	LongName				--New for Dashboard Pie Data
);
--WI 216837 
--WFSScriptProcessorIndex RecHubData.dimClientAccounts.IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive
CREATE INDEX IDX_dimClientAccounts_SiteCodeIDMostRecentIsActive ON RecHubData.dimClientAccounts
(
	SiteCodeID, 
	MostRecent, 
	IsActive
) 
INCLUDE 
(
	ClientAccountKey, 
	SiteBankID, 
	SiteClientAccountID, 
	CutOff, 
	IsCommingled, 
	SiteClientAccountKey, 
	ShortName, 
	LongName
);




