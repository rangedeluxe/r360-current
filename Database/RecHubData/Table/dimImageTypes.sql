--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimImageTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/14/2013
*
* Purpose: Static data dimension for image types. 
*
*
* Modification History
* 02/14/2013 WI 90124 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimImageTypes
(
	ImageTypeKey SMALLINT NOT NULL 
		CONSTRAINT PK_dimImageTypes PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_dimImageTypes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimImageTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_dimImageTypes_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimImageTypes_ModifiedBy DEFAULT(SUSER_SNAME()),
	ShortName VARCHAR(32) NOT NULL,
	LongName VARCHAR(64) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimImageTypes.IDX_dimImageTypes_ShortName
CREATE UNIQUE INDEX IDX_dimImageTypes_ShortName ON RecHubData.dimImageTypes (ShortName);
