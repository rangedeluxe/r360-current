--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName ClientAccountsDataEntryColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/26/2009
*
* Purpose: Associative table 
*		   
*
* Modification History
* 03/26/2009 CR 25817 JPB	Created
* 02/28/2013 WI 89904 JBS	Update Table to 2.0 release.  Rename Schema Name
*							Rename table from LockboxesDataEntryColumns to 
*							ClientAccountsDataEntryColumns
*							Renamed all Constraints, Renamed Columns:
*							LockboxKey to ClientAccountKey
* 08/22/2014 WI 136947 KLC	Added HubCreated column
* 06/10/2015 WI 217876 JBS	Adding index based on Regression Analysis.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.ClientAccountsDataEntryColumns
(
       ClientAccountKey		INT NOT NULL,
       DataEntryColumnKey	INT NOT NULL,
	   HubCreated			BIT NOT NULL DEFAULT 0
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.ClientAccountsDataEntryColumns ADD
       CONSTRAINT PK_ClientAccountsDataEntryColumns PRIMARY KEY CLUSTERED (ClientAccountKey, DataEntryColumnKey),
       CONSTRAINT FK_ClientAccountsDataEntryColumns_dimDataEntryColumns FOREIGN KEY(DataEntryColumnKey) REFERENCES RecHubData.dimDataEntryColumns(DataEntryColumnKey),
       CONSTRAINT FK_ClientAccountsDataEntryColumns_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);
--WFSScriptProcessorIndex RecHubData.ClientAccountsDataEntryColumns.IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey
CREATE NONCLUSTERED INDEX IDX_ClientAccountsDataEntryColumns_DataEntryColumnKey ON RecHubData.ClientAccountsDataEntryColumns 
(
	DataEntryColumnKey
) 
INCLUDE 
(
	ClientAccountKey,
	HubCreated
);
