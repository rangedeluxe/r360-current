--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName WorkgroupPayers
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB/MGE
* Date: 04/24/2019
*
* Purpose: Workgoup Payers stores the Payer(aka Remitter) name by workgroup
*		   
* Modification History
* 04/24/2019 R360-15309 JPB/MGE	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate

CREATE TABLE RecHubData.WorkgroupPayers
(
	WorkgroupPayerKey BIGINT Identity(1,1) NOT NULL
		CONSTRAINT PK_WorkgroupPayer PRIMARY KEY CLUSTERED,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	RoutingNumber NVARCHAR(30) NOT NULL,
	Account NVARCHAR(30) NOT NULL,
	PayerName NVARCHAR(60) NOT NULL,
	IsDefault BIT 
		CONSTRAINT DF_WorkgroupPayer_IsDefault DEFAULT 0,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_WorkgroupPayer_CreationDate DEFAULT GETDATE(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_WorkgroupPayer_ModificationDate DEFAULT GETDATE(),
	CreatedBy NVARCHAR(128) NOT NULL
		CONSTRAINT DF_WorkgroupPayer_CreatedBy DEFAULT SUSER_SNAME(),
	ModifiedBy NVARCHAR(128) NOT NULL
		CONSTRAINT DF_WorkgroupPayer_ModifiedBy DEFAULT SUSER_SNAME()
);

--WFSScriptProcessorIndex RecHubData.WorkgroupRemitter.IDX_WorkgroupRemitter_BankClientAccountID
CREATE UNIQUE INDEX IDX_WorkgroupRemitter_BankClientAccountIDRTAccount ON RecHubData.WorkgroupPayers
(
	SiteBankID,
	SiteClientAccountID,
	RoutingNumber,
	Account
)
INCLUDE
(
	PayerName,
	IsDefault
);



