--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName DataEntryTemplates
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2014
*
* Purpose: Data Entry Templates.
*		   
*
* Modification History
* 06/01/2014 WI 143426 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.DataEntryTemplates
(
	DataEntryTemplateID INT NOT NULL
		CONSTRAINT PK_DataEntryTemplates PRIMARY KEY CLUSTERED,
	IsActive BIT NOT NULL,
	TemplateShortName VARCHAR(32) NOT NULL,
	TemplateDescription VARCHAR(128) NOT NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_DataEntryTemplates_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataEntryTemplates_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_DataEntryTemplates_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataEntryTemplates_ModifiedBy DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.DataEntryTemplates.IDX_DataEntryTemplatess_TemplateShortName
CREATE UNIQUE INDEX IDX_DataEntryTemplates_TemplateShortName ON RecHubData.DataEntryTemplates (TemplateShortName);
