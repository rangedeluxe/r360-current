--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBanks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Banks dimension is a SCD type 2 holding Bank info.  Most recent 
*	flag of 1 indicates the current Bank row.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 02/28/2013 WI 89910 JBS	Update table to 2.0 Release. Rename Schema
*							Renamed Columns:
*								LoadDate to CreationDate
*							Added Column:
*								ModificationDate (With Constraint)
*							Deleted Column:
*								AddressID	
*							Expanded columns: BankName to VARCHAR(128)
* 06/02/2014 WI 139927 JPB	Added EntityID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBanks
(
	BankKey INT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_dimBanks PRIMARY KEY CLUSTERED,
	SiteBankID INT NOT NULL,
	MostRecent BIT NOT NULL,
	EntityID INT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_dimBanks_ModificationDate DEFAULT GETDATE(),
	BankName VARCHAR(128) NOT NULL,
	ABA VARCHAR(10) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBanks.IDX_dimBanks_SiteBankID
CREATE INDEX IDX_dimBanks_SiteBankID ON RecHubData.dimBanks (SiteBankID);
