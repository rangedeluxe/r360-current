--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimDepositStatuses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/12/2013
*
* Purpose: Static data dimension for Deposit Status Display Name. 
*
* Modification History
* 02/12/2013 WI 90105 JBS	Created.  Renamed to dimDepositStatuses
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimDepositStatuses
(
	DepositStatusKey INT NOT NULL 
		CONSTRAINT PK_dimDepositStatuses PRIMARY KEY CLUSTERED,
	DepositStatus INT NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimDepositStatuses_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimDepositStatuses_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimDepositStatuses_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimDepositStatuses_ModifiedBy DEFAULT(SUSER_SNAME()),
	DespositDisplayName varchar(80) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimDepositStatuses.IDX_dimDepositStatuses_DepositStatus
CREATE INDEX IDX_dimDepositStatuses_DepositStatus ON RecHubData.dimDepositStatuses (DepositStatus);
