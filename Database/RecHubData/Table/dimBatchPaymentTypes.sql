--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBatchPaymentTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/26/2012
*
* Purpose: Batch Payment Types dimension is a static data dimension for batch
*		Payment Types. 
*
* Modification History
* 03/26/2012 CR 51538 JPB	Created
* 03/01/2013 WI 89964 JBS	Update table to 2.0 release. Change Schema Name
*							Expanded columns CreatedBy and ModifiedBy to VARCHAR(128)
* 03/11/2014 WI 132430 JBS	Adding permissions tag.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBatchPaymentTypes
(
	BatchPaymentTypeKey TINYINT NOT NULL 
		CONSTRAINT PK_dimBatchPaymentTypes PRIMARY KEY CLUSTERED,
	ShortName VARCHAR(30) NOT NULL,
	LongName VARCHAR(128) NOT NULL,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchPaymentTypes_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchPaymentTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchPaymentTypes_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchPaymentTypes_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBatchPaymentTypes.IDX_dimBatchPaymentTypes_ShortName
CREATE UNIQUE INDEX IDX_dimBatchPaymentTypes_ShortName ON RecHubData.dimBatchPaymentTypes (ShortName);
