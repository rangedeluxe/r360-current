--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimExtractDescriptor
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103845 JMC   Initial Version
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimExtractDescriptor
(
	ExtractDescriptorKey INT IDENTITY(1, 1) CONSTRAINT PK_dimExtractDescriptor PRIMARY KEY NONCLUSTERED,
	ExtractDescriptor VARCHAR(255) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey