--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName DataEntryTemplateColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2014
*
* Purpose: Template columns for electronic payments.
*		   
*
* Modification History
* 06/01/2014 WI 143427 JPB	Created
* 08/27/2015 WI 232499 JPB	Updates to match new data entry column definitions
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.DataEntryTemplateColumns
(
	DataEntryTemplateColumnID INT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_DataEntryTemplateColumns PRIMARY KEY CLUSTERED,
	DataEntryTemplateID INT NOT NULL,
	IsCheck BIT NOT NULL,
	DataType TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	UILabel NVARCHAR(64) NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_DataEntryTemplateColumns_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataEntryTemplateColumns_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_DataEntryTemplateColumns_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataEntryTemplateColumns_ModifiedBy DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.DataEntryTemplateColumns ADD
	CONSTRAINT FK_DataEntryTemplateColumns_DataEntryTemplates FOREIGN KEY(DataEntryTemplateID) REFERENCES RecHubData.DataEntryTemplates(DataEntryTemplateID);
