--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimBatchExceptionStatuses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/18/2014
*
* Purpose: Static data dimension for Batch Exception Statuses. 
*
* Modification History
* 09/28/2014 WI 168334 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimBatchExceptionStatuses
(
	BatchExceptionStatusKey TINYINT NOT NULL
		CONSTRAINT PK_dimBatchExceptionStatuses PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchExceptionStatuses_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchExceptionStatuses_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimBatchExceptionStatuses_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimBatchExceptionStatuses_ModifiedBy DEFAULT(SUSER_SNAME()),
	ExceptionStatusName VARCHAR(36) NOT NULL,
	ExceptionStatusDescripion VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBatchExceptionStatuses.IDX_dimBatchExceptionStatuses_ExceptionStatusName
CREATE UNIQUE INDEX IDX_dimBatchExceptionStatuses_ExceptionStatusName ON RecHubData.dimBatchExceptionStatuses(ExceptionStatusName);
