--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTable dimDocumentTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Document Types dimension is a static data dimension for document types. 
*
*
* Defaults:
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/09/2010 CR 31787 JPB	Added IMSDocumentType, added IDX_dimDocumentTypes_FileDescriptor
* 03/04/2013 WI 90112 JBS	Update table to 2.0 release. Change Schema Name
*							Add Columns ModificationDate and ModifiedBy.
*							Rename LoadDate to CreationDate.  Change length of
*							DocumentTypeDescription from 16 to 64 and ModifiedBy
*							from 32 to 128.
* 10/24/2014 WI 174073 JBS	Adding column IsReadOnly for management of DocumentTypes.
* 02/17/2015 WI 174073 JBS  Making default = 0 for IsReadOnly
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimDocumentTypes
(
	DocumentTypeKey INT IDENTITY(0,1) NOT NULL 
		CONSTRAINT PK_dimDocumentTypes PRIMARY KEY CLUSTERED,
	MostRecent		BIT NOT NULL,
	CreationDate	DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	ModifiedBy		VARCHAR(128) NOT NULL
		CONSTRAINT DF_dimDocumentTypes_ModifiedBy DEFAULT SUSER_SNAME(),
	FileDescriptor	VARCHAR(30) NOT NULL,
	DocumentTypeDescription VARCHAR(64) NOT NULL,
	IMSDocumentType VARCHAR(30) NOT NULL,
	IsReadOnly		BIT NOT NULL 
		CONSTRAINT DF_dimDocumentTypes_IsReadOnly DEFAULT 0
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimDocumentTypes.IDX_dimDocumentTypes_FileDescriptor
CREATE INDEX IDX_dimDocumentTypes_FileDescriptor ON RecHubData.dimDocumentTypes (FileDescriptor);
