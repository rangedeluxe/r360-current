--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factCheckImages
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/17/2013
*
* Purpose: Grain: one row for every Check Image.
*
*
* Defaults:
*	BatchPaymentTypeKey = 0
*	BatchCueID = -1
*	ModificationDate = GETDATE()
*
*
* Modification History
* 03/05/2013 WI 90539  JBS	Created
* 05/31/2014 WI 135314 JPB	Changed BatchID from INT to BIGINT.
* 05/31/2014 WI 143852 JPB	Add SourceBatchID.
* 05/31/2014 WI 144958 JPB	Changed BatchSourceKey to SMALLINT.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factCheckImages
(
	factCheckImageKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	ItemSequence INT NOT NULL,
	ImageTypeKey SMALLINT NOT NULL,
	ExternalDocumentID BIGINT NOT NULL,
	FileSize INT NOT NULL,
	Side BIT NOT NULL,
	PageNumber SMALLINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL
)  $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factCheckImages ADD
	CONSTRAINT PK_factCheckImages PRIMARY KEY NONCLUSTERED (factCheckImageKey,DepositDateKey),
	CONSTRAINT FK_factCheckImages_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factCheckImages_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factCheckImages_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factCheckImages_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factCheckImages_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factCheckImages_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factCheckImages_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factCheckImages_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_factCheckImages_dimImageTypes FOREIGN KEY(ImageTypeKey) REFERENCES RecHubData.dimImageTypes(ImageTypeKey);

--WFSScriptProcessorPrint Disabling Foreign Keys
/*
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimBanks;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimOrganizations;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimClientAccounts;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_DepositDate;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_ImmutableDate;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_ProcessingDate;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimBatchSources;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimBatchPaymentTypes;
ALTER TABLE RecHubData.factCheckImages NOCHECK CONSTRAINT FK_factImages_dimImageTypes;

*/

--WFSScriptProcessorIndex RecHubData.factCheckImages.IDX_factCheckImages_DepositDatefactImageKey
CREATE CLUSTERED INDEX IDX_factCheckImages_DepositDatefactCheckImageKey ON RecHubData.factCheckImages 
(	
	DepositDateKey,
	factCheckImageKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factCheckImages.IDX_factCheckImages_BankKey
CREATE INDEX IDX_factCheckImages_BankKey ON RecHubData.factCheckImages (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factCheckImages.IDX_factCheckImages_OrganizationKey
CREATE INDEX IDX_factCheckImages_OrganizationKey ON RecHubData.factCheckImages (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factCheckImages.IDX_factCheckImages_ClientAccountKey
CREATE INDEX IDX_factCheckImages_CLientAccountKey ON RecHubData.factCheckImages (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factCheckImages.IDX_factCheckImages_ImmutableDateKey
CREATE INDEX IDX_factCheckImages_ImmutableDateKey ON RecHubData.factCheckImages (ImmutableDateKey) $(OnDataPartition);
