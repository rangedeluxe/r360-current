--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName ClientAccountBatchSources
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 07/29/2014
*
* Purpose: Associative table for dimClientAccount and dimBatchSources
*			to create assignment of which BatchSources are Loaded in system per Client Account.
*
* Modification History
* 07/29/2014 WI 155456 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.ClientAccountBatchSources
(
    ClientAccountKey	INT NOT NULL,
    BatchSourceKey		SMALLINT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.ClientAccountBatchSources ADD
       CONSTRAINT PK_ClientAccountBatchSources PRIMARY KEY CLUSTERED (ClientAccountKey, BatchSourceKey),
       CONSTRAINT FK_ClientAccountBatchSources_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
       CONSTRAINT FK_ClientAccountBatchSources_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);
