--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName dimDates
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Dates dimension is a static calendar dimension.  It will be populated 
*	with one row for each calendar date within the specified range.
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 03/04/2013 WI 90100 JBS	Update Table to 2.0 release. Change Schema Name
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.dimDates
(
	DateKey INT NOT NULL 
		CONSTRAINT PK_dimDates PRIMARY KEY CLUSTERED,
	CalendarDate DATETIME NOT NULL,
	CalendarDayMonth TINYINT NOT NULL,
	CalendarDayYear INT NOT NULL,
	CalendarDayWeek TINYINT NOT NULL,
	CalendarDayName VARCHAR(10) NOT NULL,
	CalendarWeek TINYINT NOT NULL,
	CalendarMonth TINYINT NOT NULL,
	CalendarMonthName VARCHAR(15) NOT NULL,
	CalendarQuarter TINYINT NOT NULL,
	CalendarQuarterName VARCHAR(10) NOT NULL,
	CalendarYear INT NOT NULL     
)
--WFSScriptProcessorTableProperties
