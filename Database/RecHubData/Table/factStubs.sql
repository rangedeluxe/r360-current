--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">SELECT</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSearch">SELECT</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorTableName factStubs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Grain: one row for every Stub.
*		   
*
* Modification History
* 03/09/2009 CR 25817 JJR	Created
* 11/13/2009 CR 28194 JPB	GlobalBatchID is now nullable.
* 02/12/2010 CR 28999 JPB	Added ModificationDate.
* 04/05/2010 CR 29302 JPB	Renamed Account to AccountNumber.
* 01/13/2011 CR 32299 JPB	Added BatchSourceKey.
* 01/12/2012 CR 49278 JPB	Added SourceProcessingDateKey.
* 01/23/2012 CR 49553 JPB	Added BatchSiteCode.
* 03/27/2012 CR 51545 JPB	Added BatchPaymentTypeKey.
* 07/11/2012 CR 53626 JPB	Added BatchCueID.
* 07/16/2012 CR 54122 JPB	Added BatchNumber.
* 03/06/2013 WI 90862 JBS	Update table to 2.0 release. Change schema name.
*							Add Column: factStubKey, IsDeleted.
*							Remove Column: GlobalBatchID, GlobalStubID.
*							Rename Column: CustomerKey to OrganizationKey,
*							LockboxKey to ClientAccountKey, ProcessingDateKey to ImmutableDateKey,
*							LoadDate to CreationDate.
*							Rename FK constraints to match schema and column name changes.
*							Add factStubKey to Clustered Index. 
*							Changed indexes to match column name changes.
*							Removed Constraints: DF_factStubs_BatchPaymentTypeKey DEFAULT(0),
*							DF_factStubs_BatchCueID DEFAULT(-1), DF_factStubs_ModificationDate DEFAULTGETDATE().
*							Forward Patch:  WI 87216
* 05/29/2014 WI 135322 JPB	Changed BatchID to BIGINT.
* 05/29/2014 WI 144742 JPB	Added SourceBatchID.
* 05/29/2014 WI 144743 JPB	Changed SourceBatchKey to SMALLINT.
* 08/07/2014 WI 157623 JPB	Changed DocumentBatchSequence to NULL.
* 09/04/2015 WI 234253 MGE	Added index for bug 233291 - timeout at BOE
* 10/04/2016 PT 127613917 JPB	Added IsCorrespondence.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubData.factStubs
(
	factStubKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL, 
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	StubSequence INT NOT NULL,
	IsCorrespondence BIT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL, 
	DocumentBatchSequence INT NULL,
	BatchSiteCode INT NULL,
	IsOMRDetected BIT NULL,
	Amount MONEY NULL,
	AccountNumber VARCHAR(80) NULL
) $(OnDataPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubData.factStubs ADD
	CONSTRAINT PK_factStubs PRIMARY KEY NONCLUSTERED (factStubKey,DepositDateKey),
	CONSTRAINT FK_factStubs_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factStubs_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_factStubs_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_factStubs_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factStubs_ImmutableDate FOREIGN KEY(ImmutableDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factStubs_SourceProcessingDate FOREIGN KEY(SourceProcessingDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factStubs_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_factStubs_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);

/*
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_dimBanks;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_dimCustomers;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_dimClientAccounts;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_DepositDate;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_ImmutableDate;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_SourceProcessingDate;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_dimBatchSources;
ALTER TABLE RecHubData.factStubs NOCHECK CONSTRAINT FK_factStubs_dimBatchPaymentTypes;
*/
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_DepositDatefactStubKey
CREATE CLUSTERED INDEX IDX_factStubs_DepositDatefactStubKey ON RecHubData.factStubs 
(
	DepositDateKey,
	factStubKey
) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_BankKey
CREATE INDEX IDX_factStubs_BankKey ON RecHubData.factStubs (BankKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_OrganizationKey
CREATE INDEX IDX_factStubs_OrganizationKey ON RecHubData.factStubs (OrganizationKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_ClientAccountKey
CREATE INDEX IDX_factStubs_ClientAccountKey ON RecHubData.factStubs (ClientAccountKey) $(OnDataPartition);
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_ImmutableDateKey
CREATE INDEX IDX_factStubs_ImmutableDateKey ON RecHubData.factStubs (ImmutableDateKey) $(OnDataPartition);

--WI90862 (FP:87216)
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID
CREATE NONCLUSTERED INDEX IDX_factStubs_DepositDateClientAccountKeyBatchTransactionID ON RecHubData.factStubs
(
	DepositDateKey ASC,
	ClientAccountKey ASC,
	BatchID ASC,
	TransactionID ASC
) $(OnDataPartition);
--WI 234253
--WFSScriptProcessorIndex RecHubData.factStubs.IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted
CREATE NONCLUSTERED INDEX IDX_factStubs_DepositDateBatchIDTransactionIDTxnSequenceIsDeleted ON RecHubData.factStubs
(
	DepositDateKey ASC,
	BatchID ASC,
	TransactionID ASC,
	TxnSequence ASC,
	IsDeleted ASC
)
INCLUDE
(
	BatchSequence,
	StubSequence,
	factStubKey
) $(OnDataPartition);
