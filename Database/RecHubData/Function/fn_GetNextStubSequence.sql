--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorFunctionName fn_GetNextStubSequence
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubData.fn_GetNextStubSequence') IS NOT NULL
       DROP Function RecHubData.fn_GetNextStubSequence
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubData.fn_GetNextStubSequence
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT
)
RETURNS INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/25/2017
*
* Purpose: Return the next stub sequnece number
*
*
* Modification History
* 08/22/2017 PT 149441599 JPB	Created
******************************************************************************/
BEGIN
	DECLARE @StubSequence INT;

	SELECT
		@StubSequence = COALESCE(MAX(RecHubData.factStubs.StubSequence),0) + 1
	FROM
		RecHubData.factStubs
	WHERE
		RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID

	RETURN @StubSequence;
END
