--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorFunctionName fn_GetNextBatchSequence
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubData.fn_GetNextBatchSequence') IS NOT NULL
       DROP Function RecHubData.fn_GetNextBatchSequence
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubData.fn_GetNextBatchSequence
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT
)
RETURNS INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/22/2017
*
* Purpose: Return the next batch sequnece number
*
*
* Modification History
* 08/22/2017 PT 149441599 JPB	Created
******************************************************************************/
BEGIN
	DECLARE @BatchSequence INT;

	;WITH MaxBatchSequence AS
	(
		SELECT
			RecHubData.factBatchSummary.BatchID,
			COALESCE(RecHubData.factChecks.BatchSequence,0) AS BatchSequence
		FROM
			RecHubData.factBatchSummary
			LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey 
				AND RecHubData.factChecks.BatchID = RecHubData.factBatchSummary.BatchID
		WHERE
			RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		UNION ALL
		SELECT
			RecHubData.factBatchSummary.BatchID,
			COALESCE(RecHubData.factDocuments.BatchSequence,0) AS BatchSequence
		FROM
			RecHubData.factBatchSummary
			LEFT JOIN RecHubData.factDocuments ON RecHubData.factDocuments.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey
				AND RecHubData.factDocuments.BatchID = RecHubData.factBatchSummary.BatchID
		WHERE
			RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		UNION ALL
		SELECT
			RecHubData.factBatchSummary.BatchID,
			COALESCE(RecHubData.factStubs.BatchSequence,0) AS BatchSequence
		FROM
			RecHubData.factBatchSummary
			LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey 
				AND RecHubData.factStubs.BatchID = RecHubData.factBatchSummary.BatchID 
		WHERE
			RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
	)
	SELECT
		@BatchSequence = MAX(BatchSequence)+1
	FROM
		MaxBatchSequence
	GROUP BY
		BatchID;

	RETURN @BatchSequence;
END
