--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorFunctionName fn_dimDataEntryColumns_Get_ByClientAccountKey
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubData.fn_dimDataEntryColumns_Get_ByClientAccountKey') IS NOT NULL
       DROP Function RecHubData.fn_dimDataEntryColumns_Get_ByClientAccountKey
GO

--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubData.fn_dimDataEntryColumns_Get_ByClientAccountKey
(
	@parmClientAccountKey	 INT
)
RETURNS  @DEColumns TABLE 
(
	RowID				INT IDENTITY(1,1) NOT NULL,
	DataEntryColumnKey	INT NOT NULL,
	DataType			SMALLINT NOT NULL,
	FldLength			TINYINT NOT NULL,
	ScreenOrder			TINYINT NOT NULL,
	TableName			VARCHAR(36) NOT NULL,
	FldName				VARCHAR(32) NOT NULL,
	DisplayName			VARCHAR(32) NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/31/2011
*
* Purpose: Return a list (in table form) of the data entry columns
*		associated with the given ClientAccount key. Removes duplicates and
*		keeps the newest setup information.
*
*
* Modification History
* 03/31/2011 CR 33601 JPB	Created
* 09/29/2011 CR 45731 JPB	Added ScreenOrder to the delete process.
* 10/27/2011 CR 47235 JPB	Added FldLength to the delete process.
* 04/12/2013 WI 96340 JBS	Update to 2.0 release. Change schema 
*							Change references: Lockbox to ClientAccount
*							LoadDate to CreationDate.
*							Renamed parameter:  @parmLockboxKey to @parmClientAccountKey
*							Rename proc from fn_GetLockboxDEColumns
******************************************************************************/
BEGIN
	/* Get all of the DE fields assoc with the lockbox key. */
	;WITH SiteInfo AS
	(
		SELECT	RecHubData.dimClientAccounts.SiteBankID,
				RecHubData.dimClientAccounts.SiteClientAccountID
		FROM	RecHubData.dimClientAccounts
		WHERE	ClientAccountKey = @parmClientAccountKey
	)
	INSERT INTO @DEColumns
	(
		DataEntryColumnKey,
		DataType,
		FldLength,
		ScreenOrder,
		TableName,
		FldName,
		DisplayName
	)
	SELECT 	RecHubData.dimDataEntryColumns.DataEntryColumnKey,
			RecHubData.dimDataEntryColumns.DataType,
			RecHubData.dimDataEntryColumns.FldLength,
			RecHubData.dimDataEntryColumns.ScreenOrder,
			RecHubData.dimDataEntryColumns.TableName,
			RecHubData.dimDataEntryColumns.FldName,
			RecHubData.dimDataEntryColumns.DisplayName
	FROM	RecHubData.dimDataEntryColumns
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns 
				ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
			INNER JOIN RecHubData.dimClientAccounts 
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
			INNER JOIN SiteInfo 
				ON RecHubData.dimClientAccounts.SiteBankID = SIteInfo.SiteBankID 
				AND RecHubData.dimClientAccounts.SiteClientAccountID = SiteInfo.SiteClientAccountID
	ORDER BY RecHubData.dimDataEntryColumns.CreationDate;

	/* Remove duplicates based on the CreationDate (from the above query). */
	DELETE FROM @DEColumns
	WHERE RowID NOT IN
	(
		SELECT MAX(RowID)
		FROM	@DEColumns
		GROUP BY TableName,FldName,DisplayName,ScreenOrder,FldLength
	);

	RETURN
END