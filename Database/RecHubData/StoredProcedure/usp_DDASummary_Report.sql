--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_DDASummary_Report
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_DDASummary_Report') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_DDASummary_Report
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_DDASummary_Report
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Copied from the DDASummary by JPB
* Date: 09/15/2014
*
* Purpose: 
*		   
*
* Modification History
* 10/06/2014 WI 167353 LA	Created
* 11/21/2014 WI 167353 KLC	Updated the following
*							 - updated grouping to be by workgroup not by historical workgroup update
*							 - updated to return workgroup display label of most recent workgroup
*							 - updated to obey IsActive of most recent workgroup
*							 - updated to obey site processing date and days to view setting
*							 - added auditing
* 12/08/2014 WI 167353 KLC	Added the WFSScriptProcessorQuotedIdentifierOn script tag for install
* 03/09/2015 WI 194981 JBS	Add View Ahead Permission
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDateKey	INT,
			@filterDate		DATETIME,
			@sortBy			VARCHAR(20),
			@sortDir		VARCHAR(10);
			
	SELECT	@filterDateKey = CASE WHEN Parms.val.value('(FD)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(FD)[1]', 'DATETIME'), 112) ELSE NULL END,
			@sortBy = COALESCE(LOWER(Parms.val.value('(SB)[1]', 'VARCHAR(20)')), ''),
			@sortDir = COALESCE(LOWER(Parms.val.value('(SO)[1]', 'VARCHAR(10)')), '')
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);
	
	SET @filterDate = CONVERT(DATETIME, CONVERT(VARCHAR(8), @filterDateKey));

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpEntities')) 
		DROP TABLE #tmpEntities;

	/* Create working tables */
	CREATE TABLE #tmpEntities
	(
		EntityID INT,
		BreadCrumb VARCHAR(128)
	);

	INSERT INTO #tmpEntities
    (
		EntityID,
		BreadCrumb       
    )
	SELECT     
		entityRequest.att.value('@EntityID', 'int') AS EntityID,
        entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
    FROM 
        @parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

	CREATE TABLE #tmpResults
	(
		EntityID INT,
		SiteBankID INT,
		SiteClientAccountID INT,
		DisplayLabel VARCHAR(80),
		BatchSourceKey INT,
		BatchPaymentTypeKey INT,
		DDAKey INT,
		PaymentCount INT,
		PaymentTotal MONEY
	);
	
	/* Use a cte to get the batch IDs that the user can see and a cte to get the check data "rolled" up for display */
	;WITH factBatchSummary_cte AS
	(
		SELECT
			RecHubData.factBatchSummary.BatchID,
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel
		FROM
			RecHubUser.SessionClientAccountEntitlements
			INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
			INNER JOIN RecHubUser.OLWorkgroups /*Join through OLWorkgroups to ensure WG association still exist*/
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
					AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
			INNER JOIN RechubData.dimClientAccountsView
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubData.factBatchSummary.DepositDateKey = @filterDateKey
			AND RecHubData.factBatchSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey
			AND ((RecHubData.factBatchSummary.DepositDateKey <= RecHubUser.SessionClientAccountEntitlements.EndDateKey AND RecHubUser.SessionClientAccountEntitlements.ViewAhead = 0) -- WI 194981
				 OR (RecHubUser.SessionClientAccountEntitlements.ViewAhead = 1))  -- WI 194981
			AND RecHubData.factBatchSummary.DepositStatus >= 850/*Deposit Complete*/
			AND RecHubData.factBatchSummary.IsDeleted = 0
			AND RecHubData.dimClientAccountsView.IsActive = 1
	),
	factCheckData_cte AS
	(
		SELECT
			factBatchSummary_cte.EntityID,
			factBatchSummary_cte.SiteBankID,
			factBatchSummary_cte.SiteClientAccountID,
			factBatchSummary_cte.DisplayLabel,
			RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchPaymentTypeKey,
			RecHubData.factChecks.DDAKey,
			COUNT(RecHubData.factChecks.ClientAccountKey) AS PaymentCount,
			SUM(RecHubData.factChecks.Amount) AS PaymentTotal
		FROM
			factBatchSummary_cte
			INNER JOIN RecHubData.factChecks ON RecHubData.factChecks.BatchID = factBatchSummary_cte.BatchID
		WHERE 
			RecHubData.factChecks.IsDeleted = 0
			AND RecHubData.factChecks.DepositDateKey = @filterDateKey
		GROUP BY
			factBatchSummary_cte.EntityID,
			factBatchSummary_cte.SiteBankID,
			factBatchSummary_cte.SiteClientAccountID,
			factBatchSummary_cte.DisplayLabel,
			RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchPaymentTypeKey,
			RecHubData.factChecks.DDAKey
	)
	INSERT INTO #tmpResults
	(
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		DisplayLabel,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DDAKey,
		PaymentCount,
		PaymentTotal
	)
	SELECT
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		DisplayLabel,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DDAKey,
		PaymentCount,
		PaymentTotal
	FROM factCheckData_cte;

	/* Select the final results joining the friendly names and values for the UI */
	SELECT 
		#tmpEntities.BreadCrumb,
		#tmpResults.DisplayLabel,
		RecHubData.dimBatchSources.LongName AS PaymentSource,
		RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
		RecHubData.dimDDAs.DDA,
		#tmpResults.PaymentCount,
		#tmpResults.PaymentTotal
	FROM 
		#tmpResults
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = #tmpResults.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #tmpResults.BatchPaymentTypeKey
		INNER JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = #tmpResults.DDAKey
		INNER JOIN #tmpEntities ON #tmpEntities.EntityID = #tmpResults.EntityID
	ORDER BY	LEN(RecHubData.dimDDAs.DDA) ASC,
				RecHubData.dimDDAs.DDA ASC,
				CASE WHEN @sortDir = 'ascending'
					THEN CASE @sortBy	WHEN 'batchsource'			THEN RecHubData.dimBatchSources.LongName
										WHEN 'paymenttype'			THEN RecHubData.dimBatchPaymentTypes.LongName
										WHEN 'paymentcount'			THEN REPLICATE('0', 19 - LEN(#tmpResults.PaymentCount)) + CAST(#tmpResults.PaymentCount AS VARCHAR(20))
										WHEN 'totalamount'			THEN REPLICATE('0', 19 - LEN(#tmpResults.PaymentTotal)) + CAST(#tmpResults.PaymentTotal AS VARCHAR(20))
										WHEN 'entity'				THEN #tmpEntities.BreadCrumb
										WHEN 'workgroup'			THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + DisplayLabel
										END
					END ASC,
				CASE WHEN lower(@sortDir) = 'descending'
					THEN CASE @sortBy	WHEN 'batchsource'			THEN RecHubData.dimBatchSources.LongName
										WHEN 'paymenttype'			THEN RecHubData.dimBatchPaymentTypes.LongName
										WHEN 'paymentcount'			THEN REPLICATE('0', 19 - LEN(#tmpResults.PaymentCount)) + CAST(#tmpResults.PaymentCount AS VARCHAR(20))
										WHEN 'totalamount'			THEN REPLICATE('0', 19 - LEN(#tmpResults.PaymentTotal)) + CAST(#tmpResults.PaymentTotal AS VARCHAR(20))
										WHEN 'entity'				THEN #tmpEntities.BreadCrumb
										WHEN 'workgroup'			THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + DisplayLabel
										END
					END DESC;

	DECLARE @auditMessage VARCHAR(1024) = 'DDA Summary executed for Parameters -'
											+ ' Date: ' + CAST(@filterDateKey AS VARCHAR(8))
											+ ' Sorted By: ' + CASE WHEN @sortBy = '' THEN 'No sorting' ELSE @sortBy + ' ' + @sortDir END

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubData.usp_DDASummary_Report', @parmEventName='Report Executed', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpEntities')) 
		DROP TABLE #tmpEntities;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpEntities')) 
		DROP TABLE #tmpEntities;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH