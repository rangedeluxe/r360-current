--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ReceivablesDetail
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_Report_ReceivablesDetail') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_Report_ReceivablesDetail;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_Report_ReceivablesDetail
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the data needed for the Receivables Detail Report
*
* Modification History
* Created
* 05/06/2013 WI 107242 KLC	Created.
* 07/10/2013 WI 108501 KLC	Updated auditing to write individual parameters instead of the xml.
* 07/26/2013 WI 107242 JBS  Changing called proc from usp_OLClientAccounts_Get_ByUserID
*							to usp_OLClientAccounts_Get_ByUserID_Reporting
* 07/26/2013 WI 107242 KLC	Updated to use payment type's long name
* 10/22/2013 WI 107242 KLC	Updated to check the preference for respecting the cut off
* 10/22/2013 WI 107242 KLC	Updated to add the DDA to the Client Account name
* 10/31/2013 WI 119698 CMC	Updated to use new Org Hierarchy Tree
* 11/08/2013 WI 121981 CMC  Use new view for Account Name.
* 11/15/2013 WI 122681 CMC  Filtering out distinct batch summary id records.
* 07/11/2014 WI 151281 KLC	Updated to for new RAAM integration
*								and removed use of cutoff
* 07/30/2014 WI 151281 KLC	Updated to filter to active workgroups only
* 08/14/2014 WI 151281 KLC	Updated to add back current processing date check
* 08/29/2014 WI 162520 PKW	Updated SP with @parmEventType='Reports' for the RAAM Session Audits
* 12/19/2014 WI 162520 KLC	Updated order by clause
* 12/22/2014 WI 182752 LA   Add Entity Breadcrumb 
* 03/09/2015 WI 194999 JBS	Add View Ahead Permission
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDateKey	INT,
			@filterDate		DATETIME,
			@groupBy		VARCHAR(20),
			@sortBy			VARCHAR(20),
			@sortDir		VARCHAR(10);
			
	SELECT	@filterDateKey = CASE WHEN Parms.val.value('(FD)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(FD)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@groupBy = COALESCE(LOWER(Parms.val.value('(GB)[1]', 'VARCHAR(20)')), ''),
			@sortBy = COALESCE(LOWER(Parms.val.value('(SB)[1]', 'VARCHAR(20)')), ''),
			@sortDir = COALESCE(LOWER(Parms.val.value('(SO)[1]', 'VARCHAR(10)')), '')
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);
	
	SET @filterDate = CONVERT(DATETIME, CONVERT(VARCHAR(8), @filterDateKey));

	-- Add the EntityBreadcrumb data to a temp table
	-- So we can Join to it for RAAM data
	IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
		DROP TABLE #tmpEntities;

	CREATE TABLE #tmpEntities
	(
           EntityID              INT,
           BreadCrumb	         VARCHAR(100)
     );
     
     INSERT INTO #tmpEntities
     (
           EntityID,
           BreadCrumb       
	)

     SELECT     
           entityRequest.att.value('@EntityID', 'int') AS EntityID,
           entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
       FROM  
          @parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

	;WITH factBatchSummary AS
	(
		SELECT
			RecHubUser.OLWorkgroups.EntityID,
			#tmpEntities.BreadCrumb AS EntityName,
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel AS WorkgroupName,
			RecHubData.dimBatchSources.LongName AS BatchSourceName,
			RecHubData.dimBatchPaymentTypes.LongName AS BatchPaymentTypeName,
			RecHubData.factBatchSummary.SourceBatchID,
			RecHubData.factBatchSummary.TransactionCount,
			RecHubData.factBatchSummary.CheckTotal AS TotalAmount
		FROM 
			RecHubData.factBatchSummary 
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
			INNER JOIN RecHubUser.OLWorkgroups
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
					AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID --if these don't match, workgroup was moved and should not be displayed
			INNER JOIN RecHubData.dimBatchSources 
				ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
			INNER JOIN RecHubData.dimBatchPaymentTypes 
				ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
			INNER JOIN RecHubData.dimClientAccountsView
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
			INNER JOIN #tmpEntities
				ON RecHubUser.SessionClientAccountEntitlements.EntityID = #tmpEntities.EntityID
		WHERE 
			RecHubData.factBatchSummary.DepositDateKey = @filterDateKey
			AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubData.factBatchSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey
			AND ((RecHubData.factBatchSummary.DepositDateKey <= RecHubUser.SessionClientAccountEntitlements.EndDateKey AND RecHubUser.SessionClientAccountEntitlements.ViewAhead = 0) -- WI 194999
				 OR (RecHubUser.SessionClientAccountEntitlements.ViewAhead = 1))  -- WI 194999
			AND RecHubData.factBatchSummary.DepositStatus >= 850/*Deposit Complete*/
			AND RecHubData.factBatchSummary.IsDeleted = 0
			AND RecHubData.dimClientAccountsView.IsActive = 1
	)
	SELECT
		EntityName,
		WorkgroupName,
		BatchSourceName,
		BatchPaymentTypeName,
		SourceBatchID,
		TransactionCount,
		TotalAmount
	FROM	
		factBatchSummary
	ORDER BY	EntityName ASC,
				CASE WHEN @groupBy <> @sortBy --only perform sorting on the group if the group is not already our sorting criteria also
						THEN CASE @groupBy	WHEN 'workgroup'	THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + WorkgroupName
											WHEN 'batchsource'		THEN BatchSourceName
											WHEN 'paymenttype'		THEN BatchPaymentTypeName
						END
					END ASC,
				CASE WHEN @sortDir = 'ascending'
					THEN CASE @sortBy	WHEN 'workgroup'	THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + WorkgroupName
										WHEN 'batchsource'		THEN BatchSourceName
										WHEN 'paymenttype'		THEN BatchPaymentTypeName
										WHEN 'batchid'			THEN REPLICATE('0', 10 - LEN(SourceBatchID)) + CAST(SourceBatchID AS VARCHAR)
										WHEN 'trancount'		THEN REPLICATE('0', 10 - LEN(TransactionCount)) + CAST(TransactionCount AS VARCHAR(20))
										WHEN 'totalamount'		THEN REPLICATE('0', 18 - LEN(TotalAmount)) + CAST(TotalAmount AS VARCHAR(20))
										END
					END ASC,
				CASE WHEN @sortDir = 'descending'
					THEN CASE @sortBy	WHEN 'workgroup'	THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + WorkgroupName
										WHEN 'batchsource'		THEN BatchSourceName
										WHEN 'paymenttype'		THEN BatchPaymentTypeName
										WHEN 'batchid'			THEN REPLICATE('0', 10 - LEN(SourceBatchID)) + CAST(SourceBatchID AS VARCHAR)
										WHEN 'trancount'		THEN REPLICATE('0', 10 - LEN(TransactionCount)) + CAST(TransactionCount AS VARCHAR(20))
										WHEN 'totalamount'		THEN REPLICATE('0', 18 - LEN(TotalAmount)) + CAST(TotalAmount AS VARCHAR(20))
										END
					END DESC,
				SourceBatchID ASC
	OPTION( RECOMPILE );

	DECLARE @auditMessage VARCHAR(1024) = 'Receivables Detail executed for parameters -'
											+ ' Date: ' + CAST(@filterDateKey AS VARCHAR(8))
											+ ' Grouped By: ' + CASE WHEN @groupBy = '' THEN 'No grouping' ELSE @groupBy END
											+ ' Sorted By: ' + CASE WHEN @sortBy = '' THEN 'No sorting' ELSE @sortBy + ' ' + @sortDir END
											
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubData.usp_Report_ReceivablesDetail', @parmEventName='Report Executed', @parmEventType='Reports', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
