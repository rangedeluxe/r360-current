--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_PaymentTypeKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_PaymentTypeKey') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_PaymentTypeKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_PaymentTypeKey
(   
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: Get PaymentTypeKey 
*
* Modification History
* WI 146896 SAS 06/19/2014
*    -Created for Fetching the Payment Type
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	SELECT 
		RecHubData.factBatchSummary.BatchPaymentTypeKey
	FROM  
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey 
	WHERE 
		RecHubData.dimClientAccounts.SiteBankID =  @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID		
		AND IsDeleted = 0;
    
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

