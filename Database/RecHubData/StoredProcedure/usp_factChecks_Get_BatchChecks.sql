--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_BatchChecks
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_BatchChecks') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_BatchChecks
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_BatchChecks 
(
       @parmSessionID UNIQUEIDENTIFIER,  --WI 142832
       @parmSiteBankID INT, 
       @parmSiteClientAccountID INT,
       @parmDepositDate DATETIME,
       @parmBatchID BIGINT  --WI 142832
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2008-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2008-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date:11/9/2011 
*
* Purpose: 
*
* Modification History
* 11/9/2011  CR 48085 WJS	Created
* 11/11/2011 CR 48109 WJS   FP to 1.02
* 03/14/2012 CR 51240 WJS   Add BatchSiteCode
* 06/21/2012 CR 53612 JNE   Add BatchCueID to result set
* 07/20/2012 CR 54154 JNE   Add BatchNumter to result set
* 03/20/2013 WI 90665 JBS	Update to 2.0 release. Change Schema Name.
*							Change proc name from usp_GetBatchChecks.
*							Rename Input variable: @parmSiteLockboxID to @parmSiteClientAccountID
*							Rename SiteCode to SiteCodeID
*							Removed reference to dimDates and converted all dates
*							Changed Column Headings from Lockbox to ClientAccount and Processing to Immutable.
*							Table reference change: dimLockboxes to dimClientAccounts
* 05/27/2014 WI 142832 DLD  Batch Collision/RAAM Integration.
* 09/17/2014 WI	166586 SAS  Added ImportTypeShortName
* 10/24/2014 WI	174084 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175116 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176302 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey INT,  --WI 142832
        @EndDateKey   INT;  --WI 142832

BEGIN TRY

		/* Ensure the start date is not beyond the max viewing days */  --WI 142832
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;


	SELECT	RecHubData.factChecks.BatchID,
			RecHubData.factChecks.SourceBatchID,																		 --WI 142832
			RecHubData.dimClientAccounts.SiteBankID AS BankID,
			RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.DepositDateKey), 112),101) AS DepositDate,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.ImmutableDateKey), 112),101) AS ImmutableDate,
			CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factChecks.ImmutableDateKey), 112) AS PICSDate,
			RecHubData.factChecks.TransactionID,
			RecHubData.factChecks.TxnSequence,
			RecHubData.factChecks.BatchSequence,
			RTRIM(RecHubData.factChecks.RoutingNumber) AS RT,
			RTRIM(RecHubData.factChecks.Account) AS Account,
			RTRIM(RecHubData.factChecks.Serial) AS Serial,
			RTRIM(RecHubData.factChecks.RemitterName) AS RemitterName,
			RecHubData.factChecks.Amount,
			RecHubData.factChecks.CheckSequence,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode AS ClientAccountColorMode,
		    RecHubData.factChecks.BatchSourceKey,
		    RecHubData.factChecks.BatchSiteCode,
			RecHubData.factChecks.BatchCueID,
			RecHubData.factChecks.BatchNumber,
			RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName AS ImportTypeShortName
	FROM	RecHubData.factChecks
		    INNER JOIN RecHubUser.SessionClientAccountEntitlements														 --WI 142832
				ON RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142832
			INNER JOIN RecHubData.dimClientAccounts 
				ON RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
			INNER JOIN RecHubData.dimBatchSources ON 
				RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factChecks.BatchSourceKey	
			INNER JOIN RecHubData.dimImportTypes ON
				RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
						
	WHERE  	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID                                       --WI 142832
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID                                 --WI 142832
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID               --WI 142832
			AND RecHubData.factChecks.DepositDateKey = @StartDateKey                                                     --WI 142832
			AND RecHubData.factChecks.BatchID = @parmBatchID
	ORDER BY RecHubData.factChecks.BatchID ASC,
			RecHubData.factChecks.TransactionID ASC,
			RecHubData.factChecks.TxnSequence ASC,
			RecHubData.factChecks.SequenceWithinTransaction ASC;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH