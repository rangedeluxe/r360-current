--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_ClientAccountSummary
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_ClientAccountSummary') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountSummary
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountSummary
(
	@parmSessionID			UNIQUEIDENTIFIER,	-- WI 142819
	@parmDepositDateKey		INT,
	@parmRespectCutoff		BIT,
	@parmMinDepositStatus	INT = 850
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 08/23/2011
*
* Purpose: 
*
* Modification History
* 09/08/2011 CR 34101 JMC	Created
* 02/13/2012 CR 50235 JNE       Added HOA and CustomerID to result set
* 12/03/2012 WI 70683 TWE   Tune the stored procedure add customerID in Join
*                           to force the use of full index
* 04/01/2013 WI 90706 JPB	Updated for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_GetLockboxSummary.
*							Changed all dimLockboxes refs to dimClientAccounts.
*							Moved recompile from SP to SELECT.
* 06/17/2014 WI 142819 PKW  Batch Collision.
*							Replaced on CREATE SP @parmUserID with @parmSessionID.
*							Replaced on @tblUserLockboxes - SiteOrganizationID with EntityID.
*							  - RecHubData.dimClientAccountsView.SiteOrganizationID with RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
*							Removed on:
*								@tblUserLockboxes - OLOrganizationID, OLClientAccountID.
*								@tblLockboxTmp - OLClientAccountID
*								@tblReturn TABLE
*								@IsSuperUser = SuperUser - No longer needed. RAAM resolves it.
*								@OLOrganizationID = RecHubUser.OLEntities.EntityID
* 07/09/2014 WI 142819 KLC	Updated LogonEntityID/Name to workgroup's EntityID/Name
*							 and replaced OLWorkgroups.Display name as it doesn't exists anymore
******************************************************************************/
SET NOCOUNT ON;

/* Table to hold Lockbox information for all boxes for which the User has access. */
DECLARE 
		@tblUserLockboxes TABLE
	(
		SiteBankID					INT,
		EntityID					INT,
		SiteClientAccountID			INT,
		ShortName					VARCHAR(20),
		SiteCodeID					INT,
		Cutoff						BIT,
		SiteClientAccountKey		UNIQUEIDENTIFIER,
		IsCommingled				BIT,
		IsActive					BIT,
		DisplayName					VARCHAR(50),
		DocumentImageDisplayMode	TINYINT,
		CheckImageDisplayMode		TINYINT,
		CurrentProcessingDate		DATETIME,
		OnlineViewingDays			INT,
		LongName					VARCHAR(40),
		HOA							BIT
	);

/*	Intermediate structure to contain the User's Lockboxes along with a calculation
	to determine whether Batches for the specified DepositDate have been released
	and are viewable. */
DECLARE 
		@tblLockboxTmp TABLE
	(
		SiteBankID			INT,
		EntityID			INT,
		SiteClientAccountID INT,
		OnlineViewingDays	INT,
		Released			BIT,
		LongName			VARCHAR(40),
		Cutoff				BIT,
		SiteCodeID			INT,
		HOA					BIT
	);

BEGIN TRY

    /* Get all Lockboxes for which the User has access. */
    INSERT INTO @tblUserLockboxes
	(
		SiteBankID,
		EntityID,
		SiteClientAccountID,
		ShortName,
		SiteCodeID,
		Cutoff,
		SiteClientAccountKey,
		IsCommingled,
		IsActive,
		DisplayName,
		DocumentImageDisplayMode,
		CheckImageDisplayMode,
		CurrentProcessingDate,
		OnlineViewingDays,
		LongName,
		HOA
	)
	SELECT 
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			RecHubData.dimClientAccountsView.ShortName,
			RecHubData.dimClientAccountsView.SiteCodeID,
			RecHubData.dimClientAccountsView.Cutoff,
			RecHubData.dimClientAccountsView.SiteClientAccountKey,
			RecHubData.dimClientAccountsView.IsCommingled,
			RecHubData.dimClientAccountsView.IsActive,
			RecHubData.dimClientAccountsView.LongName,
			RecHubUser.OLWorkGroups.DocumentImageDisplayMode,
			RecHubUser.OLWorkGroups.CheckImageDisplayMode,
			RecHubData.dimSiteCodes.CurrentProcessingDate,
			CASE
				WHEN RecHubUser.SessionClientAccountEntitlements.StartDateKey IS NULL OR RecHubUser.SessionClientAccountEntitlements.StartDateKey<=0
				THEN ISNULL(RecHubUser.SessionClientAccountEntitlements.StartDateKey, 0)
			ELSE 
				RecHubUser.SessionClientAccountEntitlements.StartDateKey
			END AS OnlineViewingDays,
			RecHubData.dimClientAccountsView.LongName,
			RecHubUser.OLWorkGroups.HOA
		FROM 
			RecHubUser.OLEntities
			INNER JOIN RecHubUser.OLWorkGroups ON
				RecHubUser.OLEntities.EntityID = RecHubUser.OLWorkGroups.EntityID
			INNER JOIN RecHubData.dimClientAccountsView ON
				RecHubUser.OLWorkGroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
				AND RecHubUser.OLWorkGroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
			INNER JOIN RecHubData.dimSiteCodes ON
				RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON 
				RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID 
				AND	RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID 
		WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubData.dimClientAccountsView.IsActive = 1

    -- Calculate whether Batches for the specified DepositDate have been released and are viewable.
    INSERT INTO @tblLockboxTmp 
	( 
		SiteBankID,
        EntityID, 
        SiteClientAccountID, 
        OnlineViewingDays, 
        Released,
        Cutoff,
        LongName,
        SiteCodeID,
        HOA
	)
		SELECT 
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			CASE
				WHEN RecHubUser.OLWorkGroups.ViewingDays IS NULL OR RecHubUser.OLWorkGroups.ViewingDays<=0
				THEN ISNULL(RecHubUser.OLWorkGroups.ViewingDays, 0)
			ELSE 
				RecHubUser.OLWorkGroups.ViewingDays
			END AS OnlineViewingDays,
			CASE
				WHEN ((@parmRespectCutoff = 1 AND Cutoff = 0 AND @parmDepositDateKey >= 
					RecHubUser.SessionClientAccountEntitlements.StartDateKey 
						OR @parmDepositDateKey > RecHubData.dimSiteCodes.CurrentProcessingDate)) THEN 0
			ELSE 1
			END AS Released,
			[@tblUserLockboxes].Cutoff,
			[@tblUserLockboxes].LongName ,
			[@tblUserLockboxes].SiteCodeID,
			[@tblUserLockboxes].HOA
		FROM 
			@tblUserLockboxes
			INNER JOIN RecHubUser.OLWorkGroups ON
				[@tblUserLockboxes].EntityID = RecHubUser.OLWorkGroups.EntityID
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON 
				[@tblUserLockboxes].SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
				AND	[@tblUserLockboxes].SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			INNER JOIN RecHubData.dimSiteCodes ON
				[@tblUserLockboxes].SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			
    -- So now that we have all of the Lockboxes available to the User loaded into 
    -- @tblLockboxTmp, we can get the Deposit Totals for the date that was passed in.
    ;WITH factBatchSummary2 AS
    (
        SELECT 
            RecHubUser.SessionClientAccountEntitlements.SiteBankID, 
            RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID, 
            RecHubData.factBatchSummary.DepositDateKey, 
            SUM(RecHubData.factBatchSummary.TransactionCount) AS TransactionCount, 
            SUM(RecHubData.factBatchSummary.CheckTotal) AS CheckTotal,
            COUNT(*) AS BatchCount
        FROM 
			@tblUserLockboxes AS tblUserLockboxes 
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
					   [tblUserLockboxes].SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
					   AND [tblUserLockboxes].EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID
					   AND [tblUserLockboxes].SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			INNER JOIN RecHubData.factBatchSummary ON 
					   RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey 
		WHERE 
			RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey	--WI 142819
            AND RecHubData.factBatchSummary.DepositStatus >= @parmMinDepositStatus
			AND RecHubData.factBatchSummary.IsDeleted = 0
        GROUP BY
            RecHubUser.SessionClientAccountEntitlements.SiteBankID, 
            RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID, 
            RecHubData.factBatchSummary.DepositDateKey
    )
    SELECT
        theTemp.SiteBankID			AS BankID,
        theTemp.EntityID			AS OrganizationID,
        theTemp.SiteClientAccountID	AS ClientAccountID,
        theTemp.LongName			AS ClientAccountName,
        theTemp.Cutoff				AS ClientAccountCutoff,
        theTemp.SiteCodeID,
        theTemp.Released,
        @parmDepositDateKey AS DepositDateKey,
        CONVERT(DATETIME, CAST(@parmDepositDateKey AS CHAR(12)), 112) AS DepositDate,
        CASE WHEN @parmDepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey THEN 0 ELSE 1 END AS ExceedsViewingDays,
        CASE WHEN thetemp.Released = 0 THEN 0 ELSE SUM(COALESCE(BatchCount, 0)) END AS BatchCount,				--WI 142819
        CASE WHEN thetemp.Released = 0 THEN 0 ELSE SUM(COALESCE(TransactionCount, 0)) END AS TransactionCount,	--WI 142819
        CASE WHEN thetemp.Released = 0 THEN 0 ELSE SUM(COALESCE(CheckTotal, 0)) END AS DepositTotal,			--WI 142819
        theTemp.HOA
    FROM 
		@tblLockboxTmp as thetemp
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
			thetemp.SiteBankID =	RecHubUser.SessionClientAccountEntitlements.SiteBankID
			AND thetemp.SiteClientAccountID =	RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
		LEFT OUTER JOIN factBatchSummary2 ON 
            thetemp.SiteBankID = factBatchSummary2.SiteBankID
            AND thetemp.SiteClientAccountID = factBatchSummary2.SiteClientAccountID
    GROUP BY
        theTemp.SiteBankID,
        theTemp.EntityID,
        theTemp.SiteClientAccountID,
        theTemp.LongName,
        theTemp.Cutoff,
        theTemp.SiteCodeID,
        theTemp.Released,
        RecHubUser.SessionClientAccountEntitlements.StartDateKey,
        factBatchSummary2.DepositDateKey,
        theTemp.HOA		
    ORDER BY 
        theTemp.SiteClientAccountID
	OPTION( RECOMPILE );

END TRY
BEGIN CATCH
   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH