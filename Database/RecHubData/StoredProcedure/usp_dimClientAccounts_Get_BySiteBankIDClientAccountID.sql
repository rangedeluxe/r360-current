--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_BySiteBankIDClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_BySiteBankIDClientAccountID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmOLOrganizationID		UNIQUEIDENTIFIER,
	@parmUserID					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 03/27/2013 WI 90701 JPB	Updated for 2.0.
*							Renamed and moved to RecHubUser schema.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed @parmOLCustomerID to @parmOLOrganizationID.
*							Renamed SiteCode to SiteCodeID.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @IsSuperUser BIT;

	SELECT	@IsSuperUser = SuperUser 
	FROM	RecHubUser.Users 
	WHERE	RecHubUser.Users.UserID = @parmUserID;

	IF @IsSuperUser > 0
	BEGIN
		SELECT	
			RecHubUser.OLClientAccounts.OLOrganizationID,
			RecHubUser.OLClientAccounts.OLClientAccountID,
			RecHubData.dimClientAccounts.SiteBankID				AS BankID,
			RecHubData.dimClientAccounts.SiteOrganizationID		AS OrganizationID,
			RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
			RecHubData.dimClientAccounts.ShortName,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.Cutoff,
			RecHubUser.OLClientAccounts.IsActive,
			CASE
				WHEN RecHubUser.OLClientAccounts.ViewingDays IS NULL OR RecHubUser.OLClientAccounts.ViewingDays <=0
					THEN ISNULL(RecHubUser.OLOrganizations.ViewingDays, 0)
					ELSE RecHubUser.OLClientAccounts.ViewingDays
			END AS OnlineViewingDays,
			CASE
				WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR LEN(RecHubUser.OLClientAccounts.DisplayName)=0
					THEN RecHubData.dimClientAccounts.LongName
					ELSE RecHubUser.OLClientAccounts.DisplayName
			END AS LongName
		FROM	
			RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.OLClientAccounts ON 
				RecHubUser.OLClientAccounts.SiteBankID = RecHubData.dimClientAccounts.SiteBankID 
				AND RecHubUser.OLClientAccounts.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			INNER JOIN RecHubUser.OLOrganizations ON RecHubUser.OLOrganizations.OLOrganizationID = RecHubUser.OLClientAccounts.OLOrganizationID
		WHERE	
			RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID =  @parmSiteClientAccountID
			AND RecHubUser.OLClientAccounts.OLOrganizationID = @parmOLOrganizationID
			AND RecHubData.dimClientAccounts.IsActive=1
			AND RecHubUser.OLClientAccounts.IsActive = 1;	
	END
	ELSE
	BEGIN
		SELECT	
			RecHubUser.OLClientAccounts.OLOrganizationID,
			RecHubUser.OLClientAccounts.OLClientAccountID,
			RecHubData.dimClientAccounts.SiteBankID				AS BankID,
			RecHubData.dimClientAccounts.SiteOrganizationID		AS OrganizationID,
			RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
			RecHubData.dimClientAccounts.ShortName,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.Cutoff,
			RecHubUser.OLClientAccounts.IsActive,
			CASE
				WHEN RecHubUser.OLClientAccounts.ViewingDays IS NULL OR RecHubUser.OLClientAccounts.ViewingDays <=0
					THEN ISNULL(RecHubUser.OLOrganizations.ViewingDays, 0)
					ELSE RecHubUser.OLClientAccounts.ViewingDays
			END AS OnlineViewingDays,
			CASE
				WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR LEN(RecHubUser.OLClientAccounts.DisplayName)=0
					THEN RecHubData.dimClientAccounts.LongName
					ELSE RecHubUser.OLClientAccounts.DisplayName
			END AS LongName
		FROM	
			RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.OLClientAccounts ON 
				RecHubUser.OLClientAccounts.SiteBankID = RecHubData.dimClientAccounts.SiteBankID 
				AND RecHubUser.OLClientAccounts.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			INNER JOIN RecHubUser.OLClientAccountUsers ON RecHubUser.OLClientAccountUsers.OLClientAccountID = RecHubUser.OLClientAccounts.OLClientAccountID
			INNER JOIN RecHubUser.Users ON RecHubUser.Users.UserID =RecHubUser.OLClientAccountUsers.UserID
			INNER JOIN RecHubUser.OLOrganizations ON RecHubUser.OLOrganizations.OLOrganizationID = RecHubUser.OLClientAccounts.OLOrganizationID
		WHERE	
			RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID =  @parmSiteClientAccountID
			AND RecHubUser.OLClientAccounts.OLOrganizationID = @parmOLOrganizationID
			AND RecHubUser.OLClientAccountUsers.UserID = @parmUserID
			AND RecHubData.dimClientAccounts.IsActive=1
			AND RecHubUser.OLClientAccounts.IsActive = 1;	
	END
                
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

