--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get 
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142846
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,				--WI 142846
	@parmDisplayScannedCheck	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 11/9/2011
*
* Purpose: Query Batch Detail fact table for Document records
*
* Modification History
* 11/9/2011  CR 48084 WJS	Created
* 11/11/2011 CR 48108 WJS	FP to 1.02
* 03/14/2012 CR 51242 WJS   Add BatchSiteCode
* 04/09/2012 CR 51907 JNE	Add BatchSiteCode to SQL in else statement
* 06/21/2012 CR 53614 JNE	Add BatchCueID to result set.
* 07/20/2012 CR 54156 JNE	Add BatchNumber to result set.
* 03/29/2013 WI 90669 JBS	Update to 2.0 release.  Change schema name 
*							Renamed proc from usp_GetBatchDocuments.
*							Renamed SiteCode to SiteCodeID.
*							Changed parameter: @parmSiteLockboxID to @parmSiteClientAccountID
*							Changes references from Lockbox to ClientAccount
*							Added IsDeleted = 0 
*							FP: WI85062 Performance Tuning
* 05/27/2014 WI 142846 DLD  Batch Collision/RAAM Integration.
* 09/17/2014 WI	166533 SAS  Changes done to add ImportTypeShortName.
* 10/24/2014 WI	174088 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175121 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176339 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey INT,	--WI 142846
		@EndDateKey INT,	--WI 142846
		@CheckDocumentKey INT,
		@ScannedCheckDocumentKey INT;

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ --WI 142846
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	/* Get the document keys here to be used in the CTE */
	SELECT @CheckDocumentKey = DocumentTypeKey
	FROM	RecHubData.dimDocumentTypes
	WHERE	RecHubData.dimDocumentTypes.FileDescriptor = 'C';

	SELECT @ScannedCheckDocumentKey = DocumentTypeKey
	FROM	RecHubData.dimDocumentTypes
	WHERE	RecHubData.dimDocumentTypes.FileDescriptor = 'SC';

	/* Create the memory table with the column names used by the application. Keeps from using AS xxx in the final SELECT */
	DECLARE @ClientAccounts TABLE
	(
		ClientAccountKey INT,
		BankID INT,
		ClientAccountID INT,
		SiteCodeID INT,
		ClientAccountColorMode TINYINT
	);

	INSERT INTO @ClientAccounts(ClientAccountKey,BankID,ClientAccountID,SiteCodeID,ClientAccountColorMode)
	SELECT	RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode
	FROM	RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey	--WI 142846
	WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142846
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142846
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142846

	;WITH BatchDocuments AS
	(
		SELECT	RecHubData.factDocuments.BatchID,
				RecHubData.factDocuments.SourceBatchID,	--WI 142846
				CLA.BankID,
				CLA.ClientAccountID,
				CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.DepositDateKey), 112),101)		AS DepositDate,
				CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.ImmutableDateKey), 112),101)		AS ImmutableDate,
				RecHubData.factDocuments.ImmutableDateKey AS PICSDate,
				RecHubData.factDocuments.TransactionID,
				RecHubData.factDocuments.TxnSequence,
				RecHubData.factDocuments.BatchSequence,
				RecHubData.factDocuments.DocumentSequence,
				RecHubData.factDocuments.BatchSiteCode,
				CLA.SiteCodeID,
				CLA.ClientAccountColorMode,
				RecHubData.factDocuments.BatchCueID,
				RecHubData.factDocuments.BatchNumber, 
				RecHubData.factDocuments.DocumentTypeKey,
				RecHubData.factDocuments.SequenceWithinTransaction,
				RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
				RecHubData.dimImportTypes.ShortName AS ImportTypeShortName			
		FROM	RecHubData.factDocuments
				INNER JOIN @ClientAccounts CLA ON RecHubData.factDocuments.ClientAccountKey = CLA.ClientAccountKey				
				INNER JOIN RecHubData.dimBatchSources ON RecHubData.factDocuments.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey				
				INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
		WHERE  	RecHubData.factDocuments.DepositDateKey = @StartDateKey	--WI 142846
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.DepositStatus >= 850
				AND RecHubData.factDocuments.IsDeleted = 0
				AND ( 
						(	@parmDisplayScannedCheck > 0 
							AND RecHubData.factDocuments.DocumentTypeKey <> @CheckDocumentKey
						)	 
						OR 
						(
								@parmDisplayScannedCheck = 0 
								AND RecHubData.factDocuments.DocumentTypeKey <> @CheckDocumentKey
								AND RecHubData.factDocuments.DocumentTypeKey <> @ScannedCheckDocumentKey
						) 
					)
	)
	SELECT	BatchID,
			SourceBatchID,	--WI 142846
			BankID,
			ClientAccountID,
			DepositDate,
			ImmutableDate,
			PICSDate,
			TransactionID,
			TxnSequence,
			RTRIM(RecHubData.dimDocumentTypes.FileDescriptor) AS FileDescriptor,
			ISNULL(RTRIM(RecHubData.dimDocumentTypes.DocumentTypeDescription), 'Unknown')	AS [Description], 
			BatchSequence,
			DocumentSequence,
			BatchSiteCode,
			SiteCodeID,
			ClientAccountColorMode,
			BatchCueID,
			BatchNumber,
			BatchSourceShortName,
			ImportTypeShortName
	FROM	BatchDocuments
			INNER JOIN RecHubData.dimDocumentTypes ON BatchDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
	ORDER BY
			BatchID ASC,
			TransactionID ASC,
			TxnSequence ASC,
			SequenceWithinTransaction ASC
	OPTION (RECOMPILE);
	
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH