IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSelectNames') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectNames
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectNames
(
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmCheckAmountCN			VARCHAR(30) OUTPUT,
	@parmCheckSerialNumberCN	VARCHAR(30) OUTPUT,
	@parmCheckAccountNumberCN	VARCHAR(30) OUTPUT,
	@parmCheckRTCN				VARCHAR(30) OUTPUT,
	@parmCheckPayerCN			VARCHAR(30) OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Get the names to use for selected columns that need to be renamed
*
* Modification History
* 08/16/2019 R360-30221 JPB	Created - based on RecHubData.usp_ClientAccount_Search
*******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	IF EXISTS(SELECT 1  FROM @parmAdvancedSearchSelectFieldsTable)
	BEGIN
		SELECT 
			@parmCheckAmountCN = 'ChecksAmount',
			@parmCheckSerialNumberCN = 'ChecksSerial',
			@parmCheckAccountNumberCN = 'ChecksAccount',
			@parmCheckRTCN = 'ChecksRT',
			@parmCheckPayerCN = 'ChecksPayer'; 
	END
	ELSE
	BEGIN
		SELECT	
			@parmCheckAmountCN = 'Amount',
			@parmCheckSerialNumberCN = 'SerialNumber',
			@parmCheckAccountNumberCN = 'AccountNumber',
			@parmCheckRTCN = 'RT',
			@parmCheckPayerCN = 'Payer';
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
