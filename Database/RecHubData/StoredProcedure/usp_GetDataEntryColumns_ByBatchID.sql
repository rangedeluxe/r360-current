--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetDataEntryColumns_ByBatchID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetDataEntryColumns_ByBatchID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetDataEntryColumns_ByBatchID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_GetDataEntryColumns_ByBatchID 
(
	@parmSessionID				UNIQUEIDENTIFIER, --WI 142808
    @parmSiteBankID				INT,
    @parmSiteClientAccountID    INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
	@IsCheck					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Bring back the data entry column information for check type
*
* Modification History
* 01/24/2013 WI 86289 TWE   Created by converting embedded SQL
*					  JBS	Update to 2.0 release. Change Schema Name
*							Change Parm: @parmSiteLockboxID to @parmSiteClientAccountID
*							Change all references from LockBox to ClientAccount.
*							Changed CASE statement to match Where clause values for TableType.
* 05/19/2014 WI 142808 PKW  Update for RAAM Integration.
* 07/24/2015 WI 224440 MAA  renamed and updated to support WorkgroupDataEntryColumns
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE
			@StartDateKey INT,
			@EndDateKey INT;

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
	    @parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

    SELECT	
        COALESCE(RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName, RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldTitle,
        RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName)		AS FldName,
        CASE
			WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 
				THEN 'Checks'
				ELSE 'Stubs' 
		END AS 'TableName',
        CASE
          WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount')   THEN 7
          ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
        END    AS FldDataTypeEnum

    FROM
        RecHubData.factDataEntrySummary
        INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON
            RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntrySummary.DataEntryColumnKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142808
			RecHubData.factDataEntrySummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey

    WHERE  
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
        AND RecHubData.factDataEntrySummary.DepositDateKey = @StartDateKey  --WI 142808	
        AND RecHubData.factDataEntrySummary.BatchID = @parmBatchID		    --WI 142808
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = @IsCheck
    ORDER BY 
        RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder ASC;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH