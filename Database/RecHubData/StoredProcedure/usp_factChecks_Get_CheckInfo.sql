--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_CheckInfo
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_CheckInfo') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_CheckInfo
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_CheckInfo 
(
    @parmSessionID		   UNIQUEIDENTIFIER,	--WI 142837
	@parmBankID            INT,
    @parmClientAccountID   INT,
    @parmImmutableDateKey  INT,
    @parmBatchID           BIGINT,				--WI 142837
    @parmBatchSequence     INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Get Check information
*
* Modification History
* 01/24/2013 WI 86287 TWE   Created by converting embedded SQL.
*					  JBS	Updated to 2.0 release. Change Schema to RecHubData.
*							Renamed table references: 
*							dimLockboxes to dimClientAccounts
*							Renamed columns:
*							SiteLockboxID to SiteClientAccountID,	
*							LockboxKey to ClientAccountKey.
*							Removed table reference:	dimRemitters.
*							Renamed Parameters:
*							@parmLockboxID to @parmClientAccountID, 
*							@parmProcessingDateKey to @parmImmutableDateKey.
* 06/02/2014 WI 142837 PKW  Batch Collision.
******************************************************************************/
SET NOCOUNT ON;

DECLARE 
		@StartDateKey	INT,
		@EndDateKey		INT;

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmBankID,
		@parmSiteClientAccountID = @parmClientAccountID,
		@parmDepositDateStart = @parmImmutableDateKey,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

    SELECT
        RecHubData.dimClientAccounts.SiteBankID,
        RecHubData.dimClientAccounts.SiteClientAccountID,
        RecHubData.dimClientAccounts.OnlineColorMode,
        RecHubData.factChecks.ImmutableDateKey,
        RecHubData.factChecks.DepositDateKey,
        RecHubData.factChecks.BatchID,
		RecHubData.factChecks.SourceBatchID,
        RecHubData.factChecks.TransactionID,
        RecHubData.factChecks.TxnSequence,
        RecHubData.factChecks.BatchSequence,
        'C' AS FileDescriptor,
        RecHubData.factChecks.CheckSequence,
        RecHubData.factChecks.RoutingNumber,
        RecHubData.factChecks.Account,
        RecHubData.factChecks.Serial,
        RecHubData.factChecks.Amount,
        RecHubData.factChecks.RemitterName
    FROM 
		RecHubData.factChecks
        INNER JOIN RecHubData.dimClientAccounts ON
            RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON					--WI 142837
			RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
    WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
        AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmBankID		--WI 142837
        AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmClientAccountID --WI 142837
        AND RecHubData.factChecks.ImmutableDateKey = @StartDateKey						--WI 142837
        AND RecHubData.factChecks.BatchID = @parmBatchID
        AND RecHubData.factChecks.BatchSequence = @parmBatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH