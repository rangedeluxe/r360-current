﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimWorkgroupBusinessRules_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimWorkgroupBusinessRules_Merge') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimWorkgroupBusinessRules_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimWorkgroupBusinessRules_Merge
(
	@paramSiteBankId				INT,
	@paramSiteClientAccountId		INT,
	@paramInvoiceRequired			BIT,
	@paramBalancingRequired			BIT,
	@paramPDBalancingRequired		BIT,
	@paramPDPayerRequired			BIT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2017-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JAW
* Date: 01/12/2017
*
* Purpose: Insert or update data into the RecHubData.dimWorkgroupBusinessRules table.
*
* Modification History
* 01/12/2017  #132047521  JAW  Created.
* 02/03/2017 PT 138212537 JBS	Add BalancingRequired
* 03/28/2017 PT#142143773 MAA	Changed PaymentsOnlyTransaction to InvoiceRequired
* 07/14/2017 PT#141767379 MAA	Added PostDepositBalancingRequired
* 01/18/2019 R360-1928	  JPB	Added PostDepositPayerRequired
******************************************************************************/
SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END;

	MERGE INTO RecHubData.dimWorkgroupBusinessRules
	USING (
		SELECT
			@paramSiteBankId SiteBankId,
			@paramSiteClientAccountId SiteClientAccountId,
			@paramInvoiceRequired InvoiceRequired,
			@paramBalancingRequired BalancingRequired,
			@paramPDBalancingRequired PostDepositBalancingRequired,
			@paramPDPayerRequired PostDepositPayerRequired,
			GETDATE() ModificationDate
	) SaveData
	ON
		dimWorkgroupBusinessRules.SiteBankId = SaveData.SiteBankId AND
		dimWorkgroupBusinessRules.SiteClientAccountId = SaveData.SiteClientAccountId
	WHEN MATCHED THEN
		UPDATE SET
			InvoiceRequired = SaveData.InvoiceRequired,
			BalancingRequired = SaveData.BalancingRequired,
			ModificationDate = SaveData.ModificationDate,
			PostDepositBalancingRequired = SaveData.PostDepositBalancingRequired,
			PostDepositPayerRequired = SaveData.PostDepositPayerRequired
	WHEN NOT MATCHED THEN
		INSERT (
			SiteBankId,
			SiteClientAccountId,
			InvoiceRequired,
			BalancingRequired,
			PostDepositBalancingRequired,
			PostDepositPayerRequired,
			CreationDate,
			ModificationDate
		) VALUES (
			SaveData.SiteBankId,
			SaveData.SiteClientAccountId,
			SaveData.InvoiceRequired,
			SaveData.BalancingRequired,
			SaveData.PostDepositBalancingRequired,
			PostDepositPayerRequired,
			SaveData.ModificationDate,
			SaveData.ModificationDate
		);

	IF @LocalTransaction = 1 COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
