--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Get_SearchRequest
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Get_SearchRequest') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Get_SearchRequest
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Get_SearchRequest
(
	@parmSearchRequest	XML,
	@parmOrderBy VARCHAR(64) OUTPUT,
	@parmOrderDirection VARCHAR(4) OUTPUT,
	@parmRecordsPerPage INT OUTPUT,
	@parmStartRecord INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/31/2017
*
* Purpose: Parse Invoice Search search request XML
*
* Modification History
* 01/31/2017 #134363645 JPB Created
* 03/07/2017 #136913453 JPB Added UserName, DATE DepositDates 
* 03/10/2017 #136915689 JPB	Changed UserName to FullUserName
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @SearchRequest RecHubData.InvoiceSearchSearchRequestTable;
BEGIN TRY 

	INSERT INTO @SearchRequest
	(
		SessionID,
		DepositDateFrom,
		DepositDateTo,
		DepositDateFromKey,
		DepositDateToKey,
		SortBy,
		RecordsPerPage,
		StartRecord,
		BatchSourceKey,
		BatchPaymentTypeKey,
		UserName,
		EntityName
	)
	SELECT
		@parmSearchRequest.value('(/Root/SessionID)[1]','UNIQUEIDENTIFIER'),
		@parmSearchRequest.value('(/Root/DepositDateFrom)[1]','DATE'),
		@parmSearchRequest.value('(/Root/DepositDateTo)[1]','DATE'),
		CAST(CONVERT(VARCHAR,@parmSearchRequest.value('(/Root/DepositDateFrom)[1]','DATETIME'),112) AS INT),
		CAST(CONVERT(VARCHAR,@parmSearchRequest.value('(/Root/DepositDateTo)[1]','DATETIME') ,112) AS INT),
		@parmSearchRequest.value('(/Root/SortBy)[1]','VARCHAR(128)'),
		@parmSearchRequest.value('(/Root/RecordsPerPage)[1]','INT'),
		@parmSearchRequest.value('(/Root/StartRecord)[1]','INT'),
		CASE 
			WHEN LEN(@parmSearchRequest.value('(/Root/BatchSourceKey)[1]','VARCHAR(10)')) = 0 THEN -1
			ELSE @parmSearchRequest.value('(/Root/BatchSourceKey)[1]','SMALLINT')
		END,
		CASE 
			WHEN LEN(@parmSearchRequest.value('(/Root/BatchPaymentTypeKey)[1]','VARCHAR(10)')) = 0 THEN -1
			ELSE @parmSearchRequest.value('(/Root/BatchPaymentTypeKey)[1]','SMALLINT')
		END,
		@parmSearchRequest.value('(/Root/FullUserName)[1]','VARCHAR(256)'),
		@parmSearchRequest.value('(/Root/EntityName)[1]','NVARCHAR(50)')

	SELECT
		@parmOrderBy = SUBSTRING(SortBy, 1, CHARINDEX('~',SortBy)-1),
		@parmOrderDirection = CASE SUBSTRING(SortBy, CHARINDEX('~',SortBy)+1, (LEN(SortBy)-CHARINDEX('~',SortBy)))
			WHEN 'ASCENDING' THEN 'ASC'			
			ELSE 'DESC'
		END,
		@parmRecordsPerPage = RecordsPerPage,
		@parmStartRecord = StartRecord
	FROM 
		@SearchRequest;

	SELECT
		SessionID,
		DepositDateFrom,
		DepositDateTo,
		DepositDateFromKey,
		DepositDateToKey,
		SortBy,
		RecordsPerPage,
		StartRecord,
		BatchSourceKey,
		BatchPaymentTypeKey,
		UserName,
		EntityName
	FROM 
		@SearchRequest;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH