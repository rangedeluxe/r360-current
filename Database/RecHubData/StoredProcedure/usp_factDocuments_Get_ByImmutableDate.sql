--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_ByImmutableDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_ByImmutableDate') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factDocuments_Get_ByImmutableDate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_ByImmutableDate
(
	@parmSessionID UNIQUEIDENTIFIER,  --WI 142848
	@parmSiteBankID INT,
	@parmSiteClientAccountID INT,
	@parmBatchID BIGINT,  --WI 142848
	@parmImmutableDateKey INT,
	@parmBatchSequence INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved.
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103852 JMC   Initial Version
* 05/27/2014 WI 142848 DLD  Batch Collision/RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BankKey			INT,
		@ClientAccountKey	INT,
		@DepositDateKey		INT,
		@DepositDate		DATETIME,
		@StartDateKey		INT,  --WI 142848
        @EndDateKey			INT;  --WI 142848

BEGIN TRY
	
	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

	SET @DepositDate = CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10), @DepositDateKey), 112), 101);

	/* Ensure the deposit date is not beyond the max viewing days */  --WI 142848
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @DepositDate,
		@parmDepositDateEnd = @DepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 
		RecHubData.factDocuments.BatchID,  --WI 142848
		RecHubData.factDocuments.SourceBatchID,  --WI 142848
		RecHubData.factDocuments.DepositDateKey,
		@DepositDate AS DepositDate,
		RecHubUser.SessionClientAccountEntitlements.SiteBankID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
		RecHubData.factDocuments.TransactionID
	FROM RecHubData.factDocuments
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factDocuments.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey  --WI 142848
	WHERE  	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142848
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142848
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID  --WI 142848
			AND RecHubData.factDocuments.DepositDateKey = @StartDateKey
			AND RecHubData.factDocuments.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factDocuments.BatchID = @parmBatchID
			AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
			AND RecHubData.factDocuments.IsDeleted = 0

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
