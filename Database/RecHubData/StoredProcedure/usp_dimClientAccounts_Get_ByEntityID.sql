--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_ByEntityID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_ByEntityID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByEntityID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByEntityID
(
	@parmEntityID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/17/2014
*
* Purpose: Query dimClientAccounts for records that have corresponding
*			RecHubUser.OLWorkgroups tying them to the RAAM Entity ID
*
* Modification History
* 06/17/2014 WI 151432 KLC	Created
* 03/19/2015 WI 196469 BLR  No longer using session client account entitlements
*                           for this SP. We'll be filtering after this call
*                           is made via RAAM.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT DISTINCT --Make it distinct as the entitlements will have all historical versions of a workgroup
			RecHubData.dimClientAccountsView.ClientAccountKey,
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.IsActive,
			RecHubData.dimClientAccountsView.ShortName,
			RecHubData.dimClientAccountsView.LongName
	FROM	RecHubData.dimClientAccountsView
		JOIN RecHubUser.OLWorkGroups
			ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
				AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID
	WHERE	RecHubUser.OLWorkGroups.EntityID = @parmEntityID;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

