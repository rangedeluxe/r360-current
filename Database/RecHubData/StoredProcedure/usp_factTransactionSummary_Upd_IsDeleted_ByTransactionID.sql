--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Upd_IsDeleted_ByTransactionID 
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Upd_IsDeleted_ByTransactionID ') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Upd_IsDeleted_ByTransactionID 
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Upd_IsDeleted_ByTransactionID
(
	@parmDepositDateKey		INT,
	@parmBatchID			BIGINT,
	@parmTransactionID		INT,
	@parmModificationDate	DATETIME,
	@parmRowsDeleted		BIT  OUT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 08/29/2013
*
* Purpose: Mark a factTransactionSummary record as deleted by TransactionID.
*
* Modification History
* 08/29/2013 WI 112906 JBS	Created
* 02/25/2015 WI 192465 JPB	FP from OLTA.usp_factTransactionSummaryDeleteTransaction.
*							Replaced param list with R360 parameters.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	UPDATE 	
		RecHubData.factTransactionSummary
	SET
		RecHubData.factTransactionSummary.IsDeleted = 1,
		RecHubData.factTransactionSummary.ModificationDate = @parmModificationDate
	WHERE	
		RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.TransactionID = @parmTransactionID
		AND RecHubData.factTransactionSummary.IsDeleted = 0
	OPTION (RECOMPILE);

	IF( @@ROWCOUNT > 0) 
		SET @parmRowsDeleted = 1;
	ELSE 
		SET @parmRowsDeleted = 0;

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorSeverity   INT,
			@ErrorState      INT;
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH