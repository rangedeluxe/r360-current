--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_API_dimBanksView_Get_BanksDropDown
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_API_dimBanksView_Get_BanksDropDown') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_API_dimBanksView_Get_BanksDropDown
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_API_dimBanksView_Get_BanksDropDown 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/26/2012
*
* Purpose: Create a drop down with bank information
*
* Modification History
* 12/26/2012 WI 86242 TWE   Created by converting embedded SQL
* 03/21/2013		  JBS	Updated to 2.0 release. Change schema name ot RecHubData.
*							Commented out ADDRESS information until source defined.	 
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT 
        RecHubData.dimBanksView.SiteBankID AS BankID, 
        RecHubData.dimBanksView.BankName

    FROM 
        RecHubData.dimBanksView 
    ORDER BY 
        RecHubData.dimBanksView.SiteBankID;       

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
