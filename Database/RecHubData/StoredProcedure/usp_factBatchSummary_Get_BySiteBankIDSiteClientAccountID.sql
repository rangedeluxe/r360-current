--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_BySiteBankIDSiteClientAccountID 
(
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID INT,
       @parmImmutableDateKey	INT,
       @parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: Pull Key data for Batch, per parameter list (SiteBankID, SiteClientAccountID,
*														ImmutableDateKey, BatchID)
*
* Modification History
* 04/08/2011 CR 33796 JMC	Created
* 08/01/2012 CR 54705 WJS	Add SourceProcessingDateKey to return set
* 04/01/2013 WI 90662 JBS	Update to 2.0 release. Change Schema Name
*							Rename proc from usp_GetBatch.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID
*							@parmProcessingDateKey to @parmImmutableDateKey
*							Change all references: Lockbox to ClientAccount
*							Processing to Immutable, Customer to Organization.
* 06/26/2014 WI 142818 JBS	Changing BatchID to BIGINT.  SessionID not required
* 10/10/2014 WI 171122 TWE  Return SourceBatchID, BatchNumber in result set
* 07/17/2015 WI 224806 TWE  Return BatchSourceKey
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BankKey			INT;
DECLARE @ClientAccountKey	INT;
DECLARE @DepositDateKey		INT;

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SELECT	RecHubData.factBatchSummary.BankKey,
			RecHubData.factBatchSummary.OrganizationKey,
			RecHubData.factBatchSummary.ClientAccountKey,
			RecHubData.factBatchSummary.DepositDateKey,
			RecHubData.factBatchSummary.ImmutableDateKey,
			RecHubData.factBatchSummary.BatchID,
			RecHubData.factBatchSummary.SourceBatchID,
			RecHubData.factBatchSummary.BatchNumber,
			RecHubData.factBatchSummary.BatchSourceKey,
			RecHubData.factBatchSummary.SourceProcessingDateKey
	FROM	RecHubData.factBatchSummary
	WHERE  	RecHubData.factBatchSummary.ClientAccountKey = @ClientAccountKey
			AND RecHubData.factBatchSummary.DepositDateKey = @DepositDateKey
			AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID;
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH

