--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factExtractTraces_UpSert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_factExtractTraces_UpSert') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factExtractTraces_UpSert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factExtractTraces_UpSert
(
	@parmExtractDateTime		DATETIME,
	@parmBatchID				BIGINT,
	@parmExtractDescriptor		VARCHAR(255)
)	
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 05/30/2013
*
* Purpose: Request Batch Setup Fields using a System SP.  
*
* Modification History
* 06/03/2013 WI 103854 JMC  Initial Version
*							Adding ModificationDate logic
* 06/04/2014 WI 142855 PKW  Batch Collision.
* 12/10/2014 WI 181135 BLR  Fixed up Batch Collisions - Removed clauses in
*                           the wheres that still used CAKey,BankKey,ImmutableDateKey.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE														--WI 142855
		@StartDateKey				INT,					--WI 142855
		@EndDateKey					INT,					--WI 142855
		@BankKey					INT,					--WI 142855;
		@OrganizationKey			INT,					
		@ClientAccountKey			INT,
		@DepositDateKey				INT,
		@SourceProcessingDateKey	INT,
		@ExtractDescriptorCount		INT,
		@ImmutableDateKey			INT,
		@SourceBatchID				BIGINT;					--WI 142855;

BEGIN TRY

	SELECT 
		@ExtractDescriptorCount = COUNT(*)
	FROM RecHubData.dimExtractDescriptor
	WHERE RecHubData.dimExtractDescriptor.ExtractDescriptor = @parmExtractDescriptor;

	IF @ExtractDescriptorCount = 0
	BEGIN
		INSERT INTO RecHubData.dimExtractDescriptor (ExtractDescriptor)
		VALUES(@parmExtractDescriptor);
	END

	SELECT 
		@BankKey = RecHubData.dimBanks.BankKey,
		@SourceBatchID = RecHubData.factBatchSummary.SourceBatchID,		--WI 142855
		@OrganizationKey = RecHubData.dimOrganizations.OrganizationKey,
		@ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey,
		@DepositDateKey = RecHubData.factBatchSummary.DepositDateKey,
		@SourceProcessingDateKey = RecHubData.factBatchSummary.SourceProcessingDateKey,
		@ImmutableDateKey = RecHubData.factBatchSummary.ImmutableDateKey
	FROM
		RecHubData.factBatchSummary 
		INNER JOIN RecHubData.dimBanks ON 
			RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimOrganizations ON 
			RecHubData.factBatchSummary.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
		INNER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
		RecHubData.factBatchSummary.BatchID = @parmBatchID;

	IF @@ROWCOUNT = 0
	BEGIN
		RETURN;
	END

	UPDATE RecHubData.factExtractTraces
	SET 
		RecHubData.factExtractTraces.OrganizationKey = @OrganizationKey,
		RecHubData.factExtractTraces.SourceBatchID = @SourceBatchID,
		RecHubData.factExtractTraces.ClientAccountKey = @ClientAccountKey,
		RecHubData.factExtractTraces.DepositDateKey = @DepositDateKey,
		RecHubData.factExtractTraces.SourceProcessingDateKey = @SourceProcessingDateKey,
		RecHubData.factExtractTraces.ExtractDateTime = @parmExtractDateTime,
		RecHubData.factExtractTraces.ModificationDate = GETDATE()
	FROM 
		RecHubData.factExtractTraces 
		INNER JOIN RecHubData.dimExtractDescriptor ON 
			RecHubData.factExtractTraces.ExtractDescriptorKey = RecHubData.dimExtractDescriptor.ExtractDescriptorKey
		INNER JOIN RecHubData.dimClientAccounts ON 
			RecHubData.factExtractTraces.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
		RecHubData.factExtractTraces.BatchID = @parmBatchID
		AND RecHubData.dimExtractDescriptor.ExtractDescriptor = @parmExtractDescriptor;

	--WI 142855 - Add code to check for @@ROWCOUNT = 0 and INSERT appropriately into RecHubData.factExtractTraces.
	IF @@ROWCOUNT = 0						
	BEGIN
	INSERT INTO RecHubData.factExtractTraces 
	(
		RecHubData.factExtractTraces.IsDeleted,
		RecHubData.factExtractTraces.BankKey,
		RecHubData.factExtractTraces.OrganizationKey,
		RecHubData.factExtractTraces.ClientAccountKey,
		RecHubData.factExtractTraces.DepositDateKey,
		RecHubData.factExtractTraces.ImmutableDateKey,
		RecHubData.factExtractTraces.SourceProcessingDateKey,
		RecHubData.factExtractTraces.BatchID,
			RecHubData.factExtractTraces.SourceBatchID,					--WI 142855
		RecHubData.factExtractTraces.ExtractDescriptorKey,
		RecHubData.factExtractTraces.ExtractDateTime,
		RecHubData.factExtractTraces.ModificationDate
	)
	SELECT
		0,
		@BankKey,
		@OrganizationKey,
		@ClientAccountKey,
		@DepositDateKey,
		@ImmutableDateKey,
		@SourceProcessingDateKey,
		@parmBatchID,
			@SourceBatchID,											  --WI 142855		
		RecHubData.dimExtractDescriptor.ExtractDescriptorKey,
		@parmExtractDateTime,
		GETDATE()
		FROM 
			RecHubData.dimExtractDescriptor
		WHERE 
			RecHubData.dimExtractDescriptor.ExtractDescriptor = @parmExtractDescriptor;
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
