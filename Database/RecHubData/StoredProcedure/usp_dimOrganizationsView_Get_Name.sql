--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimOrganizationsView_Get_Name
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimOrganizationsView_Get_Name') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimOrganizationsView_Get_Name
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimOrganizationsView_Get_Name 
(
       @parmSiteBankID			INT, 
       @parmSiteOrganizationID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Retreive the Organization Name from the dimOrganization table.
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 03/27/2013 WI 90760 JBS	Update to 2.0 release.  Change schema name 
*							Rename proc from usp_LookupCustomerName.
*							Change to dimOrganizationsView from the table.
*							Change Parameter: @parmSiteCustomerID to @parmSiteOrganizationID
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT COALESCE(RTRIM(RecHubData.dimOrganizationsView.Name),'') AS OrganizationName
	FROM	RecHubData.dimOrganizationsView
	WHERE	RecHubData.dimOrganizationsView.SiteBankID = @parmSiteBankID
			AND RecHubData.dimOrganizationsView.SiteOrganizationID = @parmSiteOrganizationID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
