--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_ByDepositDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_ByDepositDate') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get_ByDepositDate
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_ByDepositDate 
(
    @parmSessionID				UNIQUEIDENTIFIER,			--WI 142847
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Retrieve data from RecHubData.factDocuments by Deposit Date
*
* Modification History
* 01/15/2010 CR 28707 JMC	Created
* 04/12/2013 WI 90647 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_factDocuments_GetByBatch
*							Change references: Processing to Immutable,
*							Lockbox to ClientAccount, Customer to Organization.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Removed: factDocuments.GlobalDocumentID
* 06/17/2014 WI 142847 PKW  Batch Collision.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE																
		@DepositeDate	INT,									--WI 142847
		@StartDateKey	INT,									--WI 142847
		@EndDateKey		INT;									--WI 142847

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,				--WI 142847
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;
		
	SELECT 
		CONVERT(CHAR(8), RecHubData.factDocuments.ImmutableDateKey, 112) AS PICSDate,
		RecHubData.factBatchSummary.BatchID,
		RecHubData.factBatchSummary.SourceBatchID,				--WI 142847
		RecHubData.factDocuments.DocumentSequence,
		RTRIM(RecHubData.dimDocumentTypes.FileDescriptor) AS FileDescriptor,
		RecHubData.factDocuments.BatchSequence,
		ISNULL(RTRIM(RecHubData.dimDocumentTypes.DocumentTypeDescription), 'Unknown') AS Description
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON	--WI 142847
					RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN RecHubData.factDocuments 
			ON RecHubData.factDocuments.BankKey = RecHubData.factBatchSummary.BankKey 
			AND RecHubData.factDocuments.OrganizationKey = RecHubData.factBatchSummary.OrganizationKey 
			AND RecHubData.factDocuments.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey 
			AND RecHubData.factDocuments.ImmutableDateKey = RecHubData.factBatchSummary.ImmutableDateKey 
			AND RecHubData.factDocuments.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey 
		INNER JOIN RecHubData.dimDocumentTypes 
			ON RecHubData.dimDocumentTypes.DocumentTypeKey = RecHubData.factDocuments.DocumentTypeKey 
	WHERE 
		RecHubData.dimDocumentTypes.FileDescriptor <> 'C' 
		AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID	--WI 142847
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factBatchSummary.DepositDateKey = @StartDateKey				--WI 142847
	ORDER BY 
		RecHubData.factDocuments.TxnSequence ASC; 

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH