--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataEntryTemplates_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_DataEntryTemplates_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_DataEntryTemplates_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_DataEntryTemplates_Get
(
	@parmActiveOnly AS BIT = 1
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/02/2014
*
* Purpose: Get all the current data entry template records
*
* Modification History
* 06/02/2014 WI 143428 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 
	
BEGIN TRY

	SELECT
		TemplateShortName,
		TemplateDescription
	FROM 
		RecHubData.DataEntryTemplates
	WHERE 
		RecHubData.DataEntryTemplates.IsActive = 
			CASE @parmActiveOnly 
				WHEN 1 THEN 1 
				ELSE RecHubData.DataEntryTemplates.IsActive
			END
	ORDER BY
		TemplateShortName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
