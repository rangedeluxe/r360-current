--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get') IS NOT NULL
	   DROP PROCEDURE RecHubData.usp_factBatchSummary_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get
(
	   @parmSessionID UNIQUEIDENTIFIER,  --WI 142816
	   @parmSiteBankID INT, 
	   @parmSiteClientAccountID INT,
	   @parmDepositDateStart DATETIME,
	   @parmDepositDateEnd DATETIME,
	   @parmPaymentTypeID INT = null,
	   @parmPaymentSourceID INT = null,
	   @parmDisplayScannedCheck INT,
	   @parmStart INT,
	   @parmLength INT,
	   @parmOrderBy VarChar(128),
	   @parmOrderDirection VarChar(4),
	   @parmSearch VarChar(MAX),
	   @parmRecordsTotal INT OUTPUT,
	   @parmRecordsFiltered INT OUTPUT,
	   @parmTransactionRecords INT OUTPUT,
	   @parmCheckRecords INT OUTPUT,
	   @parmDocumentRecords INT OUTPUT,
	   @parmCheckAmount MONEY OUTPUT
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright © 2008-2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2008-2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 03/13/2009
*
* Purpose: Query Batch Summary fact table
*
* Modification History
* 03/20/2009 CR 25817 JMC	Created
* 03/15/2012 CR 49161 JNE	Add BatchSiteCode to result set.
* 06/21/2012 CR 53609 JNE	Add BatchCueID to result set.
* 07/20/2012 CR 54151 JNE	Add BatchNumber to result set.
* 03/20/2013 WI 90671 JBS	Update to 2.0 release. Change Schema Name.
*							Change proc name from usp_GetBatchSummary.
*							Rename Input variable: @parmSiteLockboxID to 
*                           @parmSiteClientAccountID.
*							Removed reference to dimDates and converted all 
*                           dates.
* 09/09/2013 WI 113596 EAS  Add Payment Type and Payment Source fields to 
*                           resultset.
* 05/19/2014 WI 142816 DLD  Batch Collision/RAAM Integration.
* 11/17/2014 WI 178299 LA   Add PaymentSourceId as a input Parameter
* 09/25/2015 WI 233992 BLR	Added paging and searching func.
* 05/18/2016 WI 280384 BLR  Added a default sorter for DepositDateKey, 
*                           BatchNumber
* 05/16/2018 #156657194 BLR Added DDA Filtering.
* 05/09/2019 R360-16487 MGE Fixed the where clause for DDA:None
* 05/22/2019 R360-16487 MGE Fixed the where clause for DDA:None in 2.03.08.01
* 06/24/2019 R360-15891 JPB	Return additional count information
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDateKey INT,
        @EndDateKey INT;

DECLARE @search VARCHAR(MAX) = '%' + @parmSearch + '%'

BEGIN TRY

	-- Ensure the start date is not beyond the max viewing days 
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	-- Gather our results.
	CREATE TABLE #results
	(
		BatchID BIGINT,  
		SourceBatchID BIGINT,
		CheckAmount MONEY,
		Deposit_Date DATETIME,
		DepositDate VARCHAR(20),
		DepositDateKey INT,
		TransactionCount INT,
		CheckCount INT,
		DocumentCount INT,
		BatchSiteCode INT,
		BatchCueID INT,
		BatchNumber INT,
		PaymentType VARCHAR(128),
		PaymentSource VARCHAR(128),
		BatchSourceKey SMALLINT,
		BatchPaymentTypeKey TINYINT,
		DDA VARCHAR(35)
	);

	-- Get our Counts.
	CREATE TABLE #counts
	(
		BatchID BIGINT,  
		DDAKey INT
	);

	INSERT INTO #results (BatchID, SourceBatchID, CheckAmount, Deposit_Date, DepositDate, DepositDateKey, TransactionCount,
		CheckCount, DocumentCount, BatchSiteCode, BatchCueID, BatchNumber, PaymentType, PaymentSource, BatchSourceKey, BatchPaymentTypeKey, DDA)
	SELECT DISTINCT
    	RecHubData.factBatchSummary.BatchID,
    	RecHubData.factBatchSummary.SourceBatchID,
    	RecHubData.factBatchSummary.CheckTotal,
    	CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factBatchSummary.DepositDateKey), 112),
    	CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factBatchSummary.DepositDateKey), 112),101),
    	RecHubData.factBatchSummary.DepositDateKey,
    	RecHubData.factBatchSummary.TransactionCount,
    	RecHubData.factBatchSummary.CheckCount,
    	-- Determine if scanned check should be included in the document count
    	CASE 
      		WHEN @parmDisplayScannedCheck > 0 THEN RecHubData.factBatchSummary.DocumentCount
      		ELSE RecHubData.factBatchSummary.DocumentCount - RecHubData.factBatchSummary.ScannedCheckCount
    	END AS DocumentCount,
    	RecHubData.factBatchSummary.BatchSiteCode,
    	RecHubData.factBatchSummary.BatchCueID,
    	RecHubData.factBatchSummary.BatchNumber,
    	RecHubData.dimBatchPaymentTypes.LongName,
    	RecHubData.dimBatchSources.LongName,
    	RecHubData.factBatchSummary.BatchSourceKey,
    	RecHubData.factBatchSummary.BatchPaymentTypeKey,
    	RecHubData.dimDDAs.DDA
  	FROM 
    	RecHubData.factBatchSummary
    	INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
    	INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
    	INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
    	LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.BatchID = RecHubData.factBatchSummary.BatchID
      		AND RecHubData.factChecks.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey
      		AND RecHubData.factChecks.IsDeleted = 0
    	LEFT JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey 
  	WHERE
    	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
    	AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
    	AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
    	AND RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey
    	AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey
    	AND RecHubData.factBatchSummary.DepositStatus >= 850
    	AND RecHubData.factBatchSummary.IsDeleted = 0
    	AND RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeID, RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey)
    	AND RecHubData.dimBatchSources.BatchSourceKey = ISNULL(@parmPaymentSourceID, RecHubData.dimBatchSources.BatchSourceKey)
    	AND (
      		(@parmSearch IN ('DDA:None', 'DDA:') AND RecHubData.dimDDAs.DDA is NULL)
      			OR
			(@parmSearch IN ('DDA:None', 'DDA:') AND RecHubData.dimDDAs.DDA = '')
				OR
			(@parmSearch LIKE 'DDA:%' AND RecHubData.dimDDAs.DDA = SUBSTRING(@parmSearch, 5, IIF(LEN(@parmSearch) > 4, (LEN(@parmSearch) - 4), 0)) )
				OR
      		(@parmSearch <> 'DDA:None' 
        		AND (
        			RecHubData.factBatchSummary.SourceBatchID LIKE @search
        			OR RecHubData.factBatchSummary.BatchNumber LIKE @search
        			OR RecHubData.factBatchSummary.DepositDateKey LIKE @search
        			OR RecHubData.dimBatchSources.LongName LIKE @search
        			OR RecHubData.dimBatchPaymentTypes.LongName LIKE @search
        			OR RecHubData.factBatchSummary.TransactionCount LIKE @search
        			OR RecHubData.factBatchSummary.CheckCount LIKE @search
        			OR RecHubData.factBatchSummary.DocumentCount LIKE @search
        			OR RecHubData.factBatchSummary.CheckTotal LIKE @search
        			OR RecHubData.dimDDAs.DDA LIKE @search
				)
      		)
    	)

    -- Return the result set.
	SELECT #results.*
	FROM #results
	ORDER BY
		-- Default order by. 2 case statements needed to trigger a 2-dimensional sort.
		CASE WHEN @parmOrderBy = ''
			THEN #results.DepositDateKey
		END,
		CASE WHEN @parmOrderBy = ''
			THEN #results.BatchNumber
		END,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'SourceBatchID' THEN #results.SourceBatchID END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'BatchNumber' THEN #results.BatchNumber END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'DepositDateString' THEN #results.DepositDateKey END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'PaymentSource' THEN #results.BatchSourceKey END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'PaymentType' THEN #results.BatchPaymentTypeKey END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'TransactionCount' THEN TransactionCount END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'PaymentCount' THEN CheckCount END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'DocumentCount' THEN DocumentCount END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'TotalAmount' THEN CheckAmount END ASC,
		CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'DDA' THEN DDA END ASC,
		
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'SourceBatchID' THEN #results.SourceBatchID END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'BatchNumber' THEN #results.BatchNumber END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'DepositDateString' THEN #results.DepositDateKey END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'PaymentSource' THEN #results.BatchSourceKey END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'PaymentType' THEN #results.BatchPaymentTypeKey END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'TransactionCount' THEN TransactionCount END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'PaymentCount' THEN CheckCount END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'DocumentCount' THEN DocumentCount END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'TotalAmount' THEN CheckAmount END DESC,
		CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'DDA' THEN DDA END DESC
		OFFSET @parmStart ROWS
		FETCH FIRST @parmLength ROWS ONLY;


	INSERT INTO #counts (BatchID, DDAKey)
	SELECT
		DISTINCT 
			RecHubData.factBatchSummary.BatchID, 
			RecHubData.factChecks.DDAKey
	FROM 
    	RecHubData.factBatchSummary
    	INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
    	INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
    	INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
    	LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.BatchID = RecHubData.factBatchSummary.BatchID
      		AND RecHubData.factChecks.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey
      		AND RecHubData.factChecks.IsDeleted = 0
    	LEFT JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey 
  	WHERE
    	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
    	AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
    	AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
    	AND RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey
    	AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey
    	AND RecHubData.factBatchSummary.DepositStatus >= 850
    	AND RecHubData.factBatchSummary.IsDeleted = 0
    	AND RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeID, RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey)
    	AND RecHubData.dimBatchSources.BatchSourceKey = ISNULL(@parmPaymentSourceID, RecHubData.dimBatchSources.BatchSourceKey);

	SELECT 
		@parmRecordsTotal = COUNT(BatchID)
	FROM
		#counts;

	SELECT 
		@parmTransactionRecords = COALESCE(SUM(TransactionCount),0),
		@parmCheckRecords = COALESCE(SUM(CheckCount),0),
		@parmDocumentRecords = COALESCE(SUM(DocumentCount),0),
		@parmCheckAmount = COALESCE(SUM(CheckTotal),0)
	FROM
		#counts
		INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.BatchID = #counts.BatchID
	WHERE 
		RecHubData.factBatchSummary.IsDeleted = 0;

	SELECT
	    @parmRecordsFiltered = COUNT(BatchID)
	FROM 
		#results;
			

	-- Clean up temp tables
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#results')) 
		DROP TABLE #results;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#counts')) 
		DROP TABLE #counts;

END TRY
BEGIN CATCH
	   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
