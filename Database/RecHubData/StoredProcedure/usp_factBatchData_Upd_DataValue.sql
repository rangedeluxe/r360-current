--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchData_Upd_DataValue
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchData_Upd_DataValue') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchData_Upd_DataValue
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchData_Upd_DataValue
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT, 
	@parmImmutableDateKey		INT,
	@parmBatchID				BIGINT,	--WI 142812
	@parmBatchDataSetupFieldKey	INT,
	@parmDataValue				VARCHAR(256),
	@parmDataValueDateTime		DATETIME = NULL,
	@parmDataValueMoney 		MONEY    = NULL,
	@parmDataValueInteger		INT      = NULL,
	@parmDataValueBit			BIT      = NULL
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR  33803 JMC	Created
* 06/24/2013 WI  90632 JBS	Update to 2.0 release.  Change schema to RecHubData
*							Rename proc from usp_factBatchDataAppend
*							Change references of Lockbox to ClientAccount,
*							Processing to Immutable.
* 06/02/2014 WI 142812 DLD  Batch Collision. 
* 07/24/2014 WI 142812 CMC  Add RecHubSystem execute permission
* 10/14/2014 WI 171993 TWE  Add back fields lost during translation.
*                           DataValue: DateTime,Money,Integer,Bit
* 07/17/2014 WI 224681 JBS	Add BatchSourceKey to insert using existing value
******************************************************************************/
SET NOCOUNT ON; --WI 142812

DECLARE @BankKey			INT,
        @ClientAccountKey	INT,
        @DepositDateKey		INT,
        @factBatchDataKey   BIGINT,
		@ModificationDate   DateTime,
		@ModifiedBy         VARCHAR(128);

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SET @ModificationDate = GETDATE();
	SET @ModifiedBy = SUSER_SNAME();

	/* Find out what the key of the record is that will be replaced */
	SELECT 	
	    @factBatchDataKey = RecHubData.factBatchData.factBatchDataKey	
	FROM 	
	    RecHubData.factBatchData 
	WHERE	
	    RecHubData.factBatchData.DepositDateKey = @DepositDateKey
		AND RecHubData.factBatchData.ClientAccountKey = @ClientAccountKey
		AND RecHubData.factBatchData.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factBatchData.BatchID = @parmBatchID
		AND RecHubData.factBatchData.BatchDataSetupFieldKey = @parmBatchDataSetupFieldKey
		AND RecHubData.factBatchData.IsDeleted = 0
	OPTION (RECOMPILE);

	IF @factBatchDataKey IS NULL 
	BEGIN
		RAISERROR('Batch Data Not Found',16,1);
	END

	/*  Now insert a new record with the updated value  */
	INSERT INTO RecHubData.factBatchData
	(
	     IsDeleted
		,BankKey
		,OrganizationKey
		,ClientAccountKey
		,DepositDateKey
		,ImmutableDateKey
		,SourceProcessingDateKey
		,BatchID
		,SourceBatchID
		,BatchNumber
		,BatchSourceKey
		,BatchDataSetupFieldKey
		,CreationDate
		,ModificationDate
		,DataValueDateTime
		,DataValueMoney
		,DataValueInteger
		,DataValueBit
		,ModifiedBy
		,DataValue 
	)
	SELECT	
		 RecHubData.factBatchData.IsDeleted
		,RecHubData.factBatchData.BankKey
		,RecHubData.factBatchData.OrganizationKey
		,RecHubData.factBatchData.ClientAccountKey
		,RecHubData.factBatchData.DepositDateKey
		,RecHubData.factBatchData.ImmutableDateKey
		,RecHubData.factBatchData.SourceProcessingDateKey
		,RecHubData.factBatchData.BatchID
		,RecHubData.factBatchData.SourceBatchID
		,RecHubData.factBatchData.BatchNumber
		,RecHubData.factBatchData.BatchSourceKey
		,RecHubData.factBatchData.BatchDataSetupFieldKey
		,RecHubData.factBatchData.CreationDate
		,@ModificationDate                         AS ModificationDate
		,@parmDataValueDateTime                    AS DataValueDateTime
		,@parmDataValueMoney                       AS DataValueMoney
		,@parmDataValueInteger                     AS DataValueInteger
		,@parmDataValueBit                         AS DataValueBit
		,@ModifiedBy                               AS ModifiedBy
		,@parmDataValue                            AS DataValue
	FROM 	
	    RecHubData.factBatchData 
	WHERE	
	     RecHubData.factBatchData.factBatchDataKey = @factBatchDataKey;
		
	/*  Now flag the original record as logically deleted */
	UPDATE
	    RecHubData.factBatchData
	SET 
	    IsDeleted = 1,
		ModificationDate = @ModificationDate,
		ModifiedBy = @ModifiedBy
	FROM 	
	    RecHubData.factBatchData 
	WHERE	
	    RecHubData.factBatchData.factBatchDataKey = @factBatchDataKey;

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH