--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimBatchSources_Upsert
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchSources_Upsert') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchSources_Upsert
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchSources_Upsert 
(
	@parmUserID			INT,
	@parmBatchSourceKey SMALLINT = NULL,
	@parmShortName		VARCHAR(30),
	@parmLongName		VARCHAR(128),
	@parmIsActive		BIT = 1,
	@parmEntityID		INT = NULL,
	@parmSystemType		VARCHAR(30),
	@outBatchSourceKey SMALLINT = NULL OUTPUT,
	@parmErrorCode		INT = NULL OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/24/2012
*
* Purpose: Update dimBatchSources (Or Insert if it doesn't exist)
*
* Modification History
* 01/24/2012 CR 49195  JPB	Created
* 04/15/2013 WI 90621  JBS	Update to 2.0 release. Change schema to RecHubData
* 05/29/2014 WI 143286 EAS	Allow User Defined Batch Payment Sources
* 07/09/2014 WI 153009 RDS	Include Auditing
* 10/13/2014 WI 171616 KLC	Updated to return error code when duplicate short
*							 name is used on an insert
* 06/05/2015 WI 217193 LA   Update the Audit messages to include Description and not the IDs
******************************************************************************/
SET NOCOUNT ON; 

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0,
        @ImportType VARCHAR(30),
		@prevImportType VARCHAR(30),
	    @AuditColumns RecHubCommon.AuditValueChangeTable,
		@UserLogonName					VARCHAR(24);

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	DECLARE @auditMessage			VARCHAR(1024);

	IF( @parmLongName = '' )
		RAISERROR('Long Name must be provided',16,1) WITH NOWAIT;

	DECLARE @ImportTypeKey TINYINT;
	SELECT 
		@ImportTypeKey = ImportTypeKey 
	FROM 
		RecHubData.dimImportTypes 
	WHERE
		RecHubData.dimImportTypes.ShortName = @parmSystemType;

	-- For Auditing
	SELECT @ImportType = RecHubData.dimImportTypes.ShortName FROM  RecHubData.dimImportTypes WHERE RecHubData.dimImportTypes.ImportTypeKey = @ImportTypeKey;
	SELECT @UserLogonName = LogonName FROM RecHubUser.Users Where UserID = @parmUserID;

	IF EXISTS( SELECT 1 FROM RecHubData.dimBatchSources WHERE RecHubData.dimBatchSources.ShortName = @parmShortName )
		BEGIN
			IF @parmBatchSourceKey IS NULL -- Trying to add an existing ShortName record.
			BEGIN
				SET @parmErrorCode = 1; -- 1 = Duplicate
				GOTO Cleanup;
			END

			DECLARE @prevIsActive			BIT,
					@prevLongName			VARCHAR(128),
					@prevEntityID			INT,
					@prevImportTypeKey		TINYINT;

			UPDATE	RecHubData.dimBatchSources
			SET		@prevIsActive = IsActive,
					@prevLongName = LongName,
					@prevEntityID = EntityID,
					@prevImportTypeKey = ImportTypeKey,
			
					RecHubData.dimBatchSources.IsActive = @parmIsActive,
					RecHubData.dimBatchSources.LongName = @parmLongName,
					RecHubData.dimBatchSources.ModificationDate = GETDATE(),
					RecHubData.dimBatchSources.ModifiedBy = SUSER_SNAME(),
					RecHubData.dimBatchSources.EntityID = @parmEntityID,
					RecHubData.dimBatchSources.ImportTypeKey = @ImportTypeKey
			WHERE	RecHubData.dimBatchSources.BatchSourceKey = @parmBatchSourceKey
			AND		RecHubData.dimBatchSources.ShortName = @parmShortName
			AND		(	-- Check if anything is changing
					IsActive <> @parmIsActive OR
					CAST(LongName AS VARBINARY(128)) <> CAST(@parmLongName AS VARBINARY(128)) OR
					ISNULL(EntityID, -1) <> ISNULL(@parmEntityID, -1) OR
					ISNULL(ImportTypeKey, -1) <> ISNULL(@ImportTypeKey, -1)
					);

			IF @@ROWCOUNT > 0
			BEGIN

			SELECT @prevImportType = RecHubData.dimImportTypes.ShortName FROM RecHubData.dimImportTypes WHERE RecHubData.dimImportTypes.ImportTypeKey = @prevImportTypeKey;
			
			
				-- Audit the Update (changes only)
				IF @prevIsActive <> @parmIsActive
					INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsActive', CAST(@prevIsActive AS VARCHAR(20)), CAST(@parmIsActive AS VARCHAR(20)));
				IF CAST(@prevLongName AS VARBINARY(128)) <> CAST(@parmLongName AS VARBINARY(128))
					INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('LongName', @prevLongName, @parmLongName);
				IF ISNULL(@prevEntityID, -1) <> ISNULL(@parmEntityID, -1)
					INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EntityID', ISNULL(CAST(@prevEntityID AS VARCHAR(20)), 'NULL'), ISNULL(CAST(@parmEntityID AS VARCHAR(20)), 'NULL'));
				IF ISNULL(@prevImportTypeKey, -1) <> ISNULL(@ImportTypeKey, -1) 
					INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ImportType', @prevImportType, @ImportType);
		
				SET @auditMessage = @UserLogonName + ' modified Payment Sources for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
									+ ', ShortName: ' + @parmShortName + '.'
									+ ', LongName: ' + @parmLongName
									+ ', System = ' + @prevImportType
									+ ', IsActive = ' + CAST(@prevIsActive AS VARCHAR(2))
									+ '.';

				EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubData.usp_dimBatchSources_Upsert',
					@parmSchemaName			= 'RecHubData',
					@parmTableName			= 'dimBatchSources',
					@parmColumnName			= 'BatchSourceKey',
					@parmAuditValue			= @parmBatchSourceKey,
					@parmAuditType			= 'UPD',
					@parmAuditColumns		= @AuditColumns,
					@parmAuditMessage		= @auditMessage;
			END
		END
	ELSE
		BEGIN
			IF( @parmShortName = '' )
				RAISERROR('Short Name must be provided',16,1) WITH NOWAIT;
			
			INSERT INTO RecHubData.dimBatchSources
			(
				RecHubData.dimBatchSources.IsActive,
				RecHubData.dimBatchSources.CreationDate,
				RecHubData.dimBatchSources.CreatedBy,
				RecHubData.dimBatchSources.ModificationDate,
				RecHubData.dimBatchSources.ModifiedBy,
				RecHubData.dimBatchSources.EntityID,
				RecHubData.dimBatchSources.ShortName,
				RecHubData.dimBatchSources.LongName,
				RecHubData.dimBatchSources.ImportTypeKey
			)
			VALUES
			(
				@parmIsActive,
				GETDATE(),
				SUSER_SNAME(),
				GETDATE(),
				SUSER_SNAME(),
				@parmEntityID,
				@parmShortName,
				@parmLongName,
				@ImportTypeKey
			);

			SET @outBatchSourceKey = SCOPE_IDENTITY();

			-- Audit the insert
		
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ShortName', NULL,@parmShortName);
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('LongName', NULL,@parmLongName);
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsActive', NULL,CAST(@parmIsActive AS VARCHAR(20)));
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ImportType', NULL, @ImportType);


			SET @auditMessage = @UserLogonName + ' added Payment Sources for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
				+ ': ShortName = ' + @parmShortName
				+ ', LongName = ' + @parmLongName
				+ ', System = ' + @ImportType
				+ ', IsActive = ' + CAST(@parmIsActive AS VARCHAR(2))
				+ '.';

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubData.usp_dimBatchSources_Upsert',
					@parmSchemaName			= 'RecHubData',
					@parmTableName			= 'dimBatchSources',
					@parmColumnName			= 'BatchSourceKey',
					@parmAuditValue			= @outBatchSourceKey,
					@parmAuditType			= 'INS',
					@parmAuditMessage		= @auditMessage;

		END

	Cleanup:

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
