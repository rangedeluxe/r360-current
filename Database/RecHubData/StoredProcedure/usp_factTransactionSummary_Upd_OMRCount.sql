--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Upd_OMRCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Upd_OMRCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Upd_OMRCount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Upd_OMRCount
(
	@parmBankKey					INT,
	@parmOrganizationKey			INT,
	@parmClientAccountKey			INT, 
	@parmImmutableDateKey			INT,
	@parmDepositDateKey				INT,
	@parmSourceProcessingDateKey	INT,
	@parmBatchID					BIGINT,		--WI 142867
	@parmModificationDate			DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/22/2011
*
* Purpose: Updates factTransactionSummary.OMRCount based on 
*	RecHubData.dimDataEntryColumns.MarkSense = 1
*
* Modification History
* 03/22/2011 CR 33483 JPB	Created.
* 01/23/2012 CR 49585 JPB	Added SourceProcessingDate.
* 04/18/2013 WI 90659 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc from usp_factTransactionSummary_UpdateOMRCount
*							Add table join of RecHubData.factStubs
*							Change parameters: @parmCustomerKey	to  @parmOrganizationKey,
*							@parmLockboxKey	to @parmClientAccountKey,
*							@parmProcessingDateKey to @parmImmutableDateKey
*							Change references: Customer to Organization,
*							Lockbox to ClientAccount, Processing to Immutable.	
*							Added IsDeleted = 0 for all Tables	
* 06/05/2014 WI 142867 DLD  Batch Collision.					
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @bLocalTransaction	BIT;

BEGIN TRY

	IF @@TRANCOUNT = 0
		BEGIN
			BEGIN TRANSACTION  
			SET @bLocalTransaction = 1
		END
	-- Combining the Rows from the 2 detail sources. This will then Join to the summary table to create the full count.

	CREATE TABLE #CombinedDetail
	(
		DepositDateKey			INT,
		BankKey					INT,
		OrganizationKey			INT,
		ClientAccountKey		INT,
		ImmutableDateKey		INT,
		SourceProcessingDateKey	INT,
		BatchID					BIGINT,	 --WI 142867
		TransactionID			INT		
	);

	INSERT INTO #CombinedDetail
	(
		DepositDateKey			,
		BankKey					,
		OrganizationKey			,
		ClientAccountKey		,
		ImmutableDateKey		,
		SourceProcessingDateKey	,
		BatchID					,
		TransactionID					
	)
	(Select 	
		DepositDateKey			,
		BankKey					,
		OrganizationKey			,
		ClientAccountKey		,
		ImmutableDateKey		,
		SourceProcessingDateKey	,
		BatchID					,
		TransactionID					
	FROM 
		RecHubData.factDataEntryDetails 
		INNER JOIN RecHubData.dimDataEntryColumns 
			ON RecHubData.factDataEntryDetails.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
				AND RecHubData.dimDataEntryColumns.MarkSense = 1
	WHERE
		RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factDataEntryDetails.BankKey = @parmBankKey
		AND RecHubData.factDataEntryDetails.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factDataEntryDetails.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factDataEntryDetails.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factDataEntryDetails.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
		AND RecHubData.factDataEntryDetails.IsDeleted = 0
	)
	UNION ALL
	(Select 	
		DepositDateKey			,
		BankKey					,
		OrganizationKey			,
		ClientAccountKey		,
		ImmutableDateKey		,
		SourceProcessingDateKey	,
		BatchID					,
		TransactionID					
	FROM 
		RecHubData.factStubs
	WHERE
		RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.BankKey = @parmBankKey
		AND RecHubData.factStubs.OrganizationKey = @parmOrganizationKey
		AND RecHubData.factStubs.ClientAccountKey = @parmClientAccountKey
		AND RecHubData.factStubs.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factStubs.SourceProcessingDateKey = @parmSourceProcessingDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.IsOMRDetected = 1
		AND RecHubData.factStubs.IsDeleted = 0
	);

	-- Now join to the summary table and count up total rows
	;WITH TransactionSummaryData AS
	(
		SELECT 	RecHubData.factTransactionSummary.DepositDateKey,
				RecHubData.factTransactionSummary.BankKey,
				RecHubData.factTransactionSummary.OrganizationKey,
				RecHubData.factTransactionSummary.ClientAccountKey,
				RecHubData.factTransactionSummary.ImmutableDateKey,
				RecHubData.factTransactionSummary.SourceProcessingDateKey,
				RecHubData.factTransactionSummary.BatchID,
				RecHubData.factTransactionSummary.TransactionID,
				COUNT(RecHubData.factTransactionSummary.TransactionID) AS OMRCount
		FROM 	RecHubData.factTransactionSummary
				INNER JOIN #CombinedDetail CD
					ON CD.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
					AND CD.BankKey = RecHubData.factTransactionSummary.BankKey
					AND CD.OrganizationKey = RecHubData.factTransactionSummary.OrganizationKey
					AND CD.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
					AND CD.ImmutableDateKey = RecHubData.factTransactionSummary.ImmutableDateKey
					AND CD.BatchID = RecHubData.factTransactionSummary.BatchID
					AND CD.TransactionID = RecHubData.factTransactionSummary.TransactionID
		WHERE 	
			RecHubData.factTransactionSummary.IsDeleted = 0
		GROUP BY 
			RecHubData.factTransactionSummary.DepositDateKey,
			RecHubData.factTransactionSummary.BankKey,
			RecHubData.factTransactionSummary.OrganizationKey,
			RecHubData.factTransactionSummary.ClientAccountKey,
			RecHubData.factTransactionSummary.ImmutableDateKey,
			RecHubData.factTransactionSummary.SourceProcessingDateKey,
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.TransactionID
	)
	UPDATE 	RecHubData.factTransactionSummary
	SET 	RecHubData.factTransactionSummary.OMRCount = TransactionSummaryData.OMRCount,
			RecHubData.factTransactionSummary.ModificationDate = @parmModificationDate	
	FROM 	RecHubData.factTransactionSummary
			INNER JOIN TransactionSummaryData 
				ON TransactionSummaryData.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
				AND TransactionSummaryData.BankKey = RecHubData.factTransactionSummary.BankKey
				AND TransactionSummaryData.OrganizationKey = RecHubData.factTransactionSummary.OrganizationKey
				AND TransactionSummaryData.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
				AND TransactionSummaryData.ImmutableDateKey = RecHubData.factTransactionSummary.ImmutableDateKey
				AND TransactionSummaryData.SourceProcessingDateKey = RecHubData.factTransactionSummary.SourceProcessingDateKey
				AND TransactionSummaryData.BatchID = RecHubData.factTransactionSummary.BatchID
				AND TransactionSummaryData.TransactionID = RecHubData.factTransactionSummary.TransactionID
				AND RecHubData.factTransactionSummary.IsDeleted = 0;
		
	IF @bLocalTransaction = 1 
		BEGIN
			COMMIT TRANSACTION 
			SET @bLocalTransaction = 0
		END

END TRY

BEGIN CATCH
	IF @bLocalTransaction = 1
		BEGIN --local transaction, handle the error
			IF (XACT_STATE()) = -1 --transaction is uncommittable
				ROLLBACK TRANSACTION
			IF (XACT_STATE()) = 1 --transaction is active and valid
				COMMIT TRANSACTION
			SET @bLocalTransaction = 0
		END
	ELSE
		BEGIN
			DECLARE @ErrorMessage    NVARCHAR(4000),
					@ErrorProcedure	 NVARCHAR(200),
					@ErrorSeverity   INT,
					@ErrorState      INT,
					@ErrorLine		 INT;
		
			SELECT	@ErrorMessage = ERROR_MESSAGE(),
					@ErrorSeverity = ERROR_SEVERITY(),
					@ErrorState = ERROR_STATE(),
					@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
					@ErrorLine = ERROR_LINE();

			SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

			RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
		END

		EXEC RecHubCommon.usp_WfsRethrowException;
		 
END CATCH