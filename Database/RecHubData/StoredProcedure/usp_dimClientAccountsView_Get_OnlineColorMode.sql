--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccountsView_Get_OnlineColorMode
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccountsView_Get_OnlineColorMode') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccountsView_Get_OnlineColorMode
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccountsView_Get_OnlineColorMode
(
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 07/26/2010 CR 30248 JMC	Modified stored procedure to always return values
*                           greater than zero.
* 03/30/2010 CR 33571 JMC	Modify proc to return OnlineColorMode as a TINYINT
* 03/26/2013 WI 90705 JPB	Update to 2.0 release. Change schema name.
*							Changed all references to dimLockBox to dimClientAccountsView,
*							SiteLockboxID to SiteClientAccountID.	
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	
		CASE 
			WHEN RecHubData.dimClientAccountsView.OnlineColorMode > 0 
				THEN RecHubData.dimClientAccountsView.OnlineColorMode 
				ELSE CAST(1 AS TINYINT) 
			END AS OnlineColorMode
	FROM	
		RecHubData.dimClientAccountsView
	WHERE  	
		RecHubData.dimClientAccountsView.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccountsView.SiteClientAccountID = @parmSiteClientAccountID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
