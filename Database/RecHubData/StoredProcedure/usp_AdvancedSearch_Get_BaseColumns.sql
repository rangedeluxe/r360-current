--use IntegrationTests_DDXW2JR2_AdvancedSearchStubDataEntry_20191111153644
--GO



IF OBJECT_ID('RecHubData.usp_AdvancedSearch_Get_BaseColumns') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_Get_BaseColumns
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_Get_BaseColumns
(
	@parmAdvancedSearchBaseKeysTable RecHubData.AdvancedSearchBaseKeysTable READONLY,
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmSearchTotals XML OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Query fact tables for matching records
*
* Modification History
* 08/16/2019 R360-30221 JPB	Created - based on RecHubData.usp_ClientAccount_Search
* 08/24/2019 R360-30227 JPB	Fixed WHERE use case, document count
*******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE
	@AmountFrom				MONEY,
	@AmountTo				MONEY,
	@BatchNumberFrom		INT,
	@BatchNumberTo			INT,
	@BatchPaymentTypeKey	TINYINT,
	@BatchSourceKey			SMALLINT,
	@DepositDateKeyFrom		INT,
	@DepositDateKeyTo		INT,
	@DisplayCOTSOnly		BIT,
	@DisplayScannedChecks	BIT,
	@MarkSense				BIT,
	@PageRecordSet			BIT,
	@RecordsPerPage			INT,
	@SessionID				UNIQUEIDENTIFIER,
	@SiteBankID				INT,
	@SiteClientAccountID	INT,
	@SortBy					VARCHAR(32),
	@SortByDirection		VARCHAR(4),
	@SourceBatchIDFrom		BIGINT,
	@SourceBatchIDTo		BIGINT,
	@StartRecord			INT,
	@TotalCheckAmount		MONEY,
	@TotalCheckCount		BIGINT,
	@TotalDocumentCount		BIGINT,
	@TotalRecords			BIGINT;

/* used to determine 'base' column names */
DECLARE 
	@CheckAmountCN			VARCHAR(30),
	@CheckSerialNumberCN	VARCHAR(30),
	@CheckAccountNumberCN	VARCHAR(30),
	@CheckRTCN				VARCHAR(30),
	@CheckPayerCN			VARCHAR(30);

/* vars for dynamic SQL */
DECLARE 
	@SelectColumns			NVARCHAR(MAX) = N'',
	@SortString				NVARCHAR(2048),
	@SQLCommand				NVARCHAR(MAX),
	@SQLParams				NVARCHAR(MAX),
	@WhereClause			NVARCHAR(MAX);

DECLARE
	@IncludeStubs			BIT = 0;

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;

	/* Create memory table for batch sources */
	CREATE TABLE #tmpBatchSources
	(
		BatchSourceKey SMALLINT,
		ImportTypeKey TINYINT,
		ShortName VARCHAR(30),
		LongName VARCHAR(128)
	);

	/* Create memory table for batch sources */
	CREATE TABLE #tmpBatchPaymentTypes
	(
		BatchPaymentTypeKey SMALLINT,
		ShortName VARCHAR(30),
		LongName VARCHAR(128)
	);

	/* Create temp table to store dimensional data */
	CREATE TABLE #tmpDimensions
	(
		SiteBankID			INT,   
		EntityID			INT,
		SiteClientAccountID	INT, 
		ClientAccountKey	INT, 
		SiteCodeID			INT,
		LBMostRecent		BIT,
		StartDateKey		INT,
		SiteCurProcDateKey	INT,
		ViewAhead			BIT
	);

	/* Create temp table for fully formed filtered records needed for sorting and final return */
	CREATE TABLE #tmpMatchingResults
	(
		factTransactionSummaryKey	BIGINT,
		BatchID						BIGINT,
		SourceBatchID				BIGINT,
		BankKey						INT,
		ClientAccountKey			INT,
		DepositDateKey				INT,
		ImmutableDateKey			INT,
		BatchSourceKey				SMALLINT,
		BatchPaymentTypeKey			SMALLINT,
		BatchNumber					INT,
		TransactionID				INT,
		TxnSequence					INT,
		SiteBankID					INT,
		SiteClientAccountID			INT,
		CheckBatchSequence			INT,
		StubSequence				INT,
		CheckCount					BIGINT,
		DocumentCount				BIGINT,
		StubCount					INT,
		OMRCount					BIGINT,
		CheckAmount					MONEY,
		NumericRoutingNumber		BIGINT,
		NumericSerial				BIGINT,
		RoutingNumber				VARCHAR(30),
		CheckAccountNumber			VARCHAR(30),
		SerialNumber				VARCHAR(30),
		TransactionCode				VARCHAR(30),
		RemitterName				VARCHAR(60),
		DDAKey						INT,
		StubAmount					MONEY,
		StubAccountNumber			VARCHAR(80),
		ChecksCheckSequence			INT,
		StubsStubSequence			INT
	);

	/* Get the input parameters from the table */
	exec RecHubData.usp_AdvancedSearch_GetSearchParams
		@parmAdvancedSearchBaseKeysTable = @parmAdvancedSearchBaseKeysTable,
		@parmAmountFrom				= @AmountFrom OUT,
		@parmAmountTo				= @AmountTo OUT,
		@parmBatchNumberFrom		= @BatchNumberFrom OUT,
		@parmBatchNumberTo			= @BatchNumberTo OUT,
		@parmBatchPaymentTypeKey	= @BatchPaymentTypeKey OUT,
		@parmBatchSourceKey			= @BatchSourceKey OUT,
		@parmDepositDateKeyFrom		= @DepositDateKeyFrom OUT,
		@parmDepositDateKeyTo		= @DepositDateKeyTo OUT,
		@parmDisplayCOTSOnly		= @DisplayCOTSOnly OUT,
		@parmDisplayScannedChecks	= @DisplayScannedChecks OUT,
		@parmMarkSense				= @MarkSense OUT,
		@parmRecordsPerPage			= @RecordsPerPage OUT,
		@parmSessionID				= @SessionID OUT,
		@parmSiteBankID				= @SiteBankID OUT,
		@parmSortBy					= @SortBy OUT,
		@parmSortByDirection		= @SortByDirection OUT,
		@parmSiteClientAccountID	= @SiteClientAccountID OUT,
		@parmSourceBatchIDFrom		= @SourceBatchIDFrom OUT,
		@parmSourceBatchIDTo		= @SourceBatchIDTo OUT,
		@parmStartRecord			= @StartRecord OUT;

	/* Call help stored procedure to create additional dynamic SQL strings */
	/* Get the where clause */
	EXEC RecHubData.usp_AdvancedSearch_GetWhereString
		@parmAdvancedSearchWhereClauseTable=@parmAdvancedSearchWhereClauseTable,
		@parmIsCheck = NULL,
		@parmWhereClauseString = @WhereClause OUT;

	/* Get the sort string */
	EXEC RecHubData.usp_AdvancedSearch_GetSortString
		@parmSortBy = @SortBy,
		@pamSortByDirection = @SortByDirection,
		@parmSortString = @SortString OUT;

	/* Get the only Stub select columns */
	IF EXISTS(SELECT 1 FROM @parmAdvancedSearchSelectFieldsTable WHERE UPPER(TableName) = 'STUBS' AND UPPER(FieldName) IN ('STUBAMOUNT','STUBACCOUNTNUMBER'))
	BEGIN
		SET @IncludeStubs = 1;
		SELECT 
			@SelectColumns = @SelectColumns + N',' + FieldName
		FROM 
			@parmAdvancedSearchSelectFieldsTable 
		WHERE 
			UPPER(TableName) = 'STUBS' 
			AND UPPER(FieldName) IN ('STUBAMOUNT','STUBACCOUNTNUMBER');
	END
	/* Get the only Stub where columns */
	IF EXISTS(SELECT 1 FROM @parmAdvancedSearchWhereClauseTable WHERE UPPER(TableName) = 'STUBS' AND UPPER(FieldName) IN ('STUBAMOUNT','STUBACCOUNTNUMBER') AND @IncludeStubs = 0)
	BEGIN
		SET @IncludeStubs = 1;
		SELECT 
			@SelectColumns = @SelectColumns + N',' + FieldName
		FROM 
			@parmAdvancedSearchWhereClauseTable 
		WHERE 
			UPPER(TableName) = 'STUBS' 
			AND UPPER(FieldName) IN ('STUBAMOUNT','STUBACCOUNTNUMBER');
	END

	/* Set specific names for result set */
	EXEC RecHubData.usp_AdvancedSearch_GetSelectNames
		 @parmAdvancedSearchSelectFieldsTable =  @parmAdvancedSearchSelectFieldsTable,
		 @parmCheckAmountCN = @CheckAmountCN OUT,
		 @parmCheckSerialNumberCN = @CheckSerialNumberCN OUT,
		 @parmCheckAccountNumberCN = @CheckAccountNumberCN OUT,
		 @parmCheckRTCN = @CheckRTCN OUT,
		 @parmCheckPayerCN = @CheckPayerCN OUT;

	/* Load up a temp table with just the client IDs that are going to be searched, use help SP */
	INSERT into #tmpDimensions 
	(   
		SiteBankID, 
		EntityID,
		SiteClientAccountID, 
		ClientAccountKey, 
		SiteCodeID,
		LBMostRecent,
		StartDateKey,
		SiteCurProcDateKey,
		ViewAhead
	)
	EXEC RecHubData.usp_AdvancedSearch_GetSessionInfo
		@parmSessionID = @SessionID,
		@parmSiteBankID = @SiteBankID,
		@parmSiteClientAccountID = @SiteClientAccountID;

	/* Get the matching records so it can be paged */
	SET @SQLCommand = N'
	SELECT
		RecHubData.factTransactionSummary.factTransactionSummaryKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		#tmpDimensions.SiteBankID,
		#tmpDimensions.SiteClientAccountID,
		RecHubData.factChecks.BatchSequence AS CheckBatchSequence,
		NULL AS StubSequence,
		CheckCount,
		CASE CONVERT(char(1),COALESCE(@DisplayScannedChecks,0))
			WHEN 0 THEN DocumentCount - ScannedCheckCount
			ELSE DocumentCount
		END AS DocumentCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.OMRCount,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account AS CheckAccountNumber,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.DDAKey,
		RecHubData.factChecks.CheckSequence AS ChecksCheckSequence, 
		' + CASE @IncludeStubs WHEN 0 THEN N'NULL,NULL,NULL' ELSE N'RecHubData.factStubs.StubSequence AS StubsStubSequence,RecHubData.factStubs.Amount AS StubAmount,factStubs.AccountNumber AS StubAccountNumber' END + N'
	FROM	
		#tmpDimensions
		INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.ClientAccountKey = #tmpDimensions.ClientAccountKey
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factChecks.IsDeleted = 0 
			' +	CASE @IncludeStubs
			WHEN 0 THEN ''
			ELSE '
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factStubs.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factStubs.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factStubs.IsDeleted = 0 
			'
		END + '
	WHERE
		RecHubData.factTransactionSummary.DepositDateKey >= StartDateKey
		AND ((RecHubData.factTransactionSummary.DepositDateKey <= SiteCurProcDateKey AND ViewAhead = 0 ) OR (ViewAhead = 1))
		AND RecHubData.factTransactionSummary.DepositDateKey >= @DepositDateKeyFrom
		AND RecHubData.factTransactionSummary.DepositDateKey <= @DepositDateKeyTo
		AND RecHubData.factTransactionSummary.SourceBatchID >= @SourceBatchIDFrom
		AND RecHubData.factTransactionSummary.SourceBatchID <= @SourceBatchIDTo
		AND RecHubData.factTransactionSummary.BatchNumber >= @BatchNumberFrom
		AND RecHubData.factTransactionSummary.BatchNumber <= @BatchNumberTo
		AND COALESCE(RecHubData.factChecks.Amount,0) >= @AmountFrom
		AND COALESCE(RecHubData.factChecks.Amount,922337203685477.5807) <= @AmountTo
		AND RecHubData.factTransactionSummary.OMRCount >= @MarkSense
		AND RecHubData.factTransactionSummary.DepositStatus >= 850
		AND RecHubData.factTransactionSummary.BatchSourceKey = CASE WHEN @BatchSourceKey IS NULL THEN RecHubData.factTransactionSummary.BatchSourceKey ELSE @BatchSourceKey END
		AND RecHubData.factTransactionSummary.BatchPaymentTypeKey = CASE WHEN @BatchPaymentTypeKey IS NULL THEN RecHubData.factTransactionSummary.BatchPaymentTypeKey ELSE @BatchPaymentTypeKey END
		AND RecHubData.factTransactionSummary.IsDeleted = 0' +
		@WhereClause + N'
	OPTION (RECOMPILE);'

	SET @SQLParams = N'@DepositDateKeyFrom INT,
		@DepositDateKeyTo INT,
		@SourceBatchIDFrom BIGINT,
		@SourceBatchIDTo BIGINT,
		@BatchNumberFrom INT,
		@BatchNumberTo INT,
		@AmountFrom MONEY,
		@AmountTo MONEY,
		@MarkSense BIT,
		@DisplayCOTSOnly BIT,
		@BatchSourceKey SMALLINT,
		@BatchPaymentTypeKey TINYINT,
		@DisplayScannedChecks BIT';

	/* Get preliminary key set of batches that match the basic search criteria, just get the keys */
	INSERT INTO #tmpMatchingResults
	(
		factTransactionSummaryKey,
		BatchID,
		SourceBatchID,
		BankKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchNumber,
		TransactionID,
		TxnSequence,
		SiteBankID,
		SiteClientAccountID,
		CheckBatchSequence,
		StubSequence,
		CheckCount,
		DocumentCount,
		StubCount,
		OMRCount,
		CheckAmount,
		NumericRoutingNumber,
		NumericSerial,
		RoutingNumber,
		CheckAccountNumber,
		SerialNumber,
		TransactionCode,
		RemitterName,
		DDAKey,
		ChecksCheckSequence,
		StubsStubSequence,
		StubAmount,
		StubAccountNumber
	)
	EXECUTE sp_executesql 
		@SQLCommand, 
		@SQLParams,
		@DepositDateKeyFrom = @DepositDateKeyFrom,
		@DepositDateKeyTo = @DepositDateKeyTo,
		@SourceBatchIDFrom = @SourceBatchIDFrom,
		@SourceBatchIDTo = @SourceBatchIDTo,
		@BatchNumberFrom = @BatchNumberFrom,
		@BatchNumberTo = @BatchNumberTo,
		@AmountFrom = @AmountFrom,
		@AmountTo = @AmountTo,
		@BatchSourceKey = @BatchSourceKey,
		@BatchPaymentTypeKey = @BatchPaymentTypeKey,
		@MarkSense = @MarkSense,
		@DisplayCOTSOnly = @DisplayCOTSOnly,
		@DisplayScannedChecks = @DisplayScannedChecks;


--select '#tmpMatchingResults', * from #tmpMatchingResults order by transactionid,SiteBankID,SiteClientAccountID,DepositDateKey,ImmutableDateKey,SourceBatchID,TxnSequence,COALESCE(StubsStubSequence,ChecksCheckSequence)
	/************************************************************************************
	* Get the batch source/payment type info to join to the final result set			*
	************************************************************************************/
	/* Fill in batch sources */
	;WITH BatchSources AS
	(
		SELECT BatchSourceKey
		FROM #tmpMatchingResults
		GROUP BY BatchSourceKey
	)
	INSERT INTO #tmpBatchSources(BatchSourceKey, ImportTypeKey, ShortName, LongName)
	SELECT 
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchSources.ImportTypeKey,
		RecHubData.dimBatchSources.ShortName,
		RecHubData.dimBatchSources.LongName
	FROM
		BatchSources
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = BatchSources.BatchSourceKey ;

	/* Fill in batch sources */
	;WITH BatchPaymentTypes AS
	(
		SELECT BatchPaymentTypeKey
		FROM #tmpMatchingResults
		GROUP BY BatchPaymentTypeKey
	)
	INSERT INTO #tmpBatchPaymentTypes(BatchPaymentTypeKey, ShortName, LongName)
	SELECT 
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.ShortName,
		RecHubData.dimBatchPaymentTypes.LongName
	FROM
		BatchPaymentTypes
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = BatchPaymentTypes.BatchPaymentTypeKey;

	/************************************************************************************
	* Set the total values to return to the app											*
	************************************************************************************/
	;WITH DocumentCount AS
	(
		SELECT DISTINCT
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence,
			COALESCE(RecHubData.factTransactionSummary.DocumentCount,0) AS DocumentCount
		FROM	
			#tmpMatchingResults
			INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = #tmpMatchingResults.DepositDateKey
				AND RecHubData.factTransactionSummary.BatchID = #tmpMatchingResults.BatchID
				AND RecHubData.factTransactionSummary.TransactionID = #tmpMatchingResults.TransactionID
				AND RecHubData.factTransactionSummary.TxnSequence = #tmpMatchingResults.TxnSequence
		WHERE
			RecHubData.factTransactionSummary.IsDeleted = 0
	)
	SELECT @TotalDocumentCount = COALESCE(SUM(DocumentCount),0) FROM DocumentCount;

	;WITH CheckInfo AS
	(
		SELECT	DISTINCT 
			BankKey,
			ClientAccountKey,
			DepositDateKey,
			ImmutableDateKey,
			BatchID,
			TransactionID,
			CheckBatchSequence,
			COALESCE(CheckCount,0) AS CheckCount,
			COALESCE(CheckAmount,0.00) AS CheckAmount
		FROM	
			#tmpMatchingResults
		WHERE	
			CheckCount <> 0
	)
	SELECT	
		@TotalCheckCount = COALESCE(COUNT(*),0),
		@TotalCheckAmount = COALESCE(SUM(CheckAmount),0.00)
	FROM
		CheckInfo;

	--COTS
	IF @DisplayCOTSOnly = 1
	BEGIN
		DELETE 
		FROM 
			#tmpMatchingResults 
		WHERE 
			CheckCount <> 0;
	END

	SELECT 
		@TotalRecords = COUNT(*)
	FROM 
		#tmpMatchingResults;

	/* Create the return XML */
	EXEC RecHubData.usp_AdvancedSearch_GetResultsXML
		@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
		@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable,
		@parmTotalCheckAmount = @TotalCheckAmount,
		@parmTotalCheckCount = @TotalCheckCount,
		@parmTotalDocumentCount = @TotalDocumentCount,
		@parmTotalRecords = @TotalRecords,
		@parmResultsXML = @parmSearchTotals OUTPUT;

	/************************************************************************************
	* Create the dynamic SQL to return all the columns for a page of data 				*
	************************************************************************************/
	SET @SQLCommand = N'
	;WITH ReturnList AS
	(
		SELECT
			ROW_NUMBER() OVER (ORDER BY ' + @SortString + N') AS RecID,
			#tmpDimensions.SiteBankID AS BankID,
			#tmpDimensions.SiteClientAccountID AS ClientAccountID,
			CONVERT(VARCHAR(10),CAST(CONVERT(VARCHAR(10),#tmpMatchingResults.DepositDateKey, 101) AS DATETIME),101) AS Deposit_Date,
			#tmpBatchSources.LongName AS PaymentSource,
			#tmpBatchPaymentTypes.LongName AS PaymentType,
			#tmpMatchingResults.BatchID,
			#tmpMatchingResults.SourceBatchID,
			#tmpMatchingResults.BatchNumber,
			#tmpMatchingResults.ImmutableDateKey AS PICSDate,
			#tmpMatchingResults.TransactionID,
			#tmpMatchingResults.TxnSequence,
			#tmpMatchingResults.CheckAmount AS ' + @CheckAmountCN + N',
			#tmpMatchingResults.SerialNumber AS ' + @CheckSerialNumberCN + N',
			#tmpMatchingResults.TransactionCode AS ChecksTransactionCode,
			#tmpMatchingResults.NumericSerial,
			#tmpMatchingResults.CheckAccountNumber AS ' + @CheckAccountNumberCN + N',
			#tmpMatchingResults.RoutingNumber AS ' + @CheckRTCN + N',
			#tmpMatchingResults.NumericRoutingNumber AS numeric_rt,
			#tmpMatchingResults.DDAKey,
			#tmpMatchingResults.RemitterName AS ' + @CheckPayerCN + N',
			#tmpBatchSources.ShortName AS BatchSourceShortName,
			#tmpMatchingResults.CheckBatchSequence,
			#tmpMatchingResults.CheckBatchSequence AS BatchSequence,
			#tmpMatchingResults.StubsStubSequence,
			#tmpMatchingResults.CheckCount,
			#tmpMatchingResults.DocumentCount,
			#tmpMatchingResults.StubCount,
			#tmpMatchingResults.OMRCount AS MarkSenseDocumentCount,
			#tmpMatchingResults.ChecksCheckSequence,
			#tmpBatchSources.ImportTypeKey 
			' + @SelectColumns + N'
		FROM
			#tmpMatchingResults
			INNER JOIN #tmpDimensions ON #tmpDimensions.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
			INNER JOIN #tmpBatchSources ON #tmpBatchSources.BatchSourceKey = #tmpMatchingResults.BatchSourceKey
			INNER JOIN #tmpBatchPaymentTypes ON #tmpBatchPaymentTypes.BatchPaymentTypeKey = #tmpMatchingResults.BatchPaymentTypeKey
		ORDER BY 
			RecID
		OFFSET ' + CAST(@StartRecord-1 AS NVARCHAR(10)) + N' ROWS
		FETCH FIRST ' + CAST(@RecordsPerPage AS NVARCHAR(10)) + N' ROWS ONLY
	)
	SELECT 
		BankID,
		ClientAccountID,
		Deposit_Date,
		PaymentSource,
		PaymentType,
		BatchID,
		SourceBatchID,
		BatchNumber,
		PICSDate,
		TransactionID,
		TxnSequence,
		' + @CheckAmountCN + N',
		' + @CheckSerialNumberCN + N',
		ChecksTransactionCode,
		NumericSerial,
		' + @CheckAccountNumberCN + N',
		' + @CheckRTCN + N',
		numeric_rt,
		RecHubData.dimDDAs.DDA,
		' + @CheckPayerCN + N',
		BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
		CheckBatchSequence,
		BatchSequence,
		StubsStubSequence,
		CheckCount,
		DocumentCount,
		StubCount,
		MarkSenseDocumentCount,
		ChecksCheckSequence '
		+ @SelectColumns + N'
	FROM 
		ReturnList
		INNER JOIN RecHubData.dimImportTypes ON ReturnList.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
		LEFT JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = ReturnList.DDAKey
	ORDER BY RecID
	OPTION (RECOMPILE);
	';
--print @SQLCommand
	EXEC(@SQLCommand);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH




--go
--use master
--go
