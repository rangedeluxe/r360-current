--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factItemData_Ins
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_factItemData_Ins') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factItemData_Ins
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factItemData_Ins
(
       @parmBankKey                 INT,
	   @parmOrganizationKey         INT,
	   @parmClientAccountKey        INT,
	   @parmDepositDateKey          INT,
	   @parmImmutableDateKey        INT,
	   @parmSourceProcessingDateKey INT,
	   @parmBatchNumber             INT,
	   @parmBatchSourceKey          SMALLINT,
	   @parmBatchID                 BIGINT,
	   @parmSourceBatchID           BIGINT,
	   @parmTransactionID           INT,
	   @parmBatchSequence           INT,	
	   @parmItemData                xml
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 10/08/2014
*
* Purpose: Used by ICON Service to insert data into the table
*
* Modification History
* 10/09/2014 WI 170757 TWE Created
* 07/17/2015 WI 224694 TWE insert BatchSourceKey
******************************************************************************/
SET NOCOUNT ON; 

Declare @CurrentDateTime DATETIME = getdate();

BEGIN TRY
	
	--Create temp file to hold the variable information contained in the xml input
	IF OBJECT_ID('tempdb..#tmpItemDataList') IS NOT NULL
        DROP TABLE #tmpItemDataList;

	CREATE TABLE #tmpItemDataList
	(
        DataSetupFieldKey       INT,
	    DataValue               varchar(256),  
	    DataValueDateTime       DATETIME,
	    DataValueMoney          MONEY,
	    DataValueInteger        INT,
	    DataValueBit            BIT
	);

	INSERT INTO #tmpItemDataList
	(
	    DataSetupFieldKey,    
	    DataValue,            
	    DataValueDateTime,    
	    DataValueMoney,       
	    DataValueInteger,     
	    DataValueBit         
	)
	SELECT
		ItemData.att.value('@DataSetupFieldKey', 'int')      AS DataSetupFieldKey,
		ItemData.att.value('@DataValue', 'varchar(256)')     AS DataValue,
		ItemData.att.value('@DataValueDateTime', 'datetime') AS DataValueDateTime,
		ItemData.att.value('@DataValueMoney', 'money')       AS DataValueMoney,
		ItemData.att.value('@DataValueInteger', 'int')       AS DataValueInteger,
		ItemData.att.value('@DataValueBit', 'bit')           AS DataValueBit
	FROM
	    @parmItemData.nodes('/Root/ItemDataRows/ItemDataRow') ItemData(att);

	--Now insert the data into the actual table
	INSERT INTO RecHubData.factItemData 
	(
	     IsDeleted,
		 BankKey, 
		 OrganizationKey, 
	     ClientAccountKey,
		 DepositDateKey,
	     ImmutableDateKey,
		 SourceProcessingDateKey, 
	     BatchID, 
		 SourceBatchID, 
	     BatchNumber, 
		 BatchSourceKey, 
		 TransactionID,
	     BatchSequence, 
		 ItemDataSetupFieldKey,
	     CreationDate, 
		 ModificationDate, 
	     DataValueDateTime, 
		 DataValueMoney, 
		 DataValueInteger, 
		 DataValueBit,
	     ModifiedBy, 
	     DataValue
    )
	SELECT 
	     0                            AS IsDeleted,
		 @parmBankKey                 AS BankKey,
		 @parmOrganizationKey         AS OrganizationKey ,  
		 @parmClientAccountKey        AS ClientAccountKey,   
		 @parmDepositDateKey          AS DepositDateKey,  
		 @parmImmutableDateKey        AS ImmutableDateKey, 
		 @parmSourceProcessingDateKey AS SourceProcessingDateKey,
		 @parmBatchID                 AS BatchID,
		 @parmSourceBatchID           AS SourceBatchID,
		 @parmBatchNumber             AS BatchNumber,
		 @parmBatchSourceKey          AS BatchSourceKey,
		 @parmTransactionID           AS TransactionID,
		 @parmBatchSequence           AS BatchSequence,
		 DataSetupFieldKey ,
		 @CurrentDateTime             AS CreationDate,
		 @CurrentDateTime             AS ModificationDate,
		 DataValueDateTime ,
		 DataValueMoney    ,
		 DataValueInteger  ,
		 DataValueBit      ,
		 SUSER_SNAME()                AS ModifiedBy,
		 DataValue         
	FROM #tmpItemDataList;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
