--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_CheckDetails
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_CheckDetails') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_CheckDetails
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_CheckDetails
(	
	@parmDepositDate		DATETIME,
	@parmBatchID			BIGINT,
	@parmBatchSequence		INT,
	@parmIsCheck			BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: SAS
* Date: 08/20/2014
*
* Purpose: To pull out the Check details for ACH and Wire Transfer images
*
* Modification History
* 08/20/2014 CR 160198, WI 160203  SAS	Created.
* 09/09/2014 WI 164608 SAS Changes done to add DDAKey.
* 09/09/2014 WI 169961 SAS Changes done to pull data using Deposit Data, BatchID and TransactionID.
* 10/23/2014 WI 173869 SAS Changes done to pull data using Deposit Data, BatchID and BatchSequence.
* 10/31/2014 WI 174742 SAS Changes done to pull data using factDocuments batch sequence / factcheck batch sequence.
					If @parmIsCheck is true then the batchsequence will match from factCheck table
					If @parmIsCheck is false then the batchsequence will match from factDocument table
* 11/29/2017 PT 127613853 MGE Always use most recent workgroup name					
******************************************************************************/
SET NOCOUNT ON;

DECLARE @TransactionId INT;
DECLARE	@DepositDateKey INT;

BEGIN TRY
	
	SET @DepositDateKey=CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);
	IF @parmIsCheck=1
		BEGIN
			SELECT							
				RTRIM(RecHubData.factChecks.RemitterName)	AS RemitterName,
				RecHubData.factChecks.Amount,
				RecHubData.factChecks.SourceProcessingDateKey,
				RecHubData.dimClientAccountsView.LongName AS WorkGroupLongName,
				RecHubData.dimDDAs.DDA
			FROM	
				RecHubData.factChecks       	
				INNER JOIN RecHubData.dimClientAccounts ON 
					RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN RecHubData.dimClientAccountsView ON
					RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
				INNER JOIN RecHubData.dimDDAs ON 
				  RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey
			WHERE  			
					RecHubData.factChecks.DepositDateKey =@DepositDateKey
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
				AND RecHubData.factChecks.IsDeleted=0;
		END 
	ELSE 
		--BatchSequence is requested from factDocumentTable, hence pull the check information 
		--based on the TransactionID that matched with the factdocument table
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factDocuments.TransactionID 
			FROM
				RecHubData.factDocuments
			WHERE 
					RecHubData.factDocuments.DepositDateKey =@DepositDateKey
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
				AND RecHubData.factDocuments.IsDeleted=0;
			

			SELECT							
				RTRIM(RecHubData.factChecks.RemitterName)	AS RemitterName,
				RecHubData.factChecks.Amount,
				RecHubData.factChecks.SourceProcessingDateKey,
				RecHubData.dimClientAccountsView.LongName AS WorkGroupLongName,
				RecHubData.dimDDAs.DDA
			FROM	
				RecHubData.factChecks       	
				INNER JOIN RecHubData.dimClientAccounts ON 
					RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN RecHubData.dimClientAccountsView ON
					RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID		
				INNER JOIN RecHubData.dimDDAs ON 
				  RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey
			WHERE  			
					RecHubData.factChecks.DepositDateKey=@DepositDateKey				
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.TransactionID=@TransactionID
				AND RecHubData.factChecks.IsDeleted=0;
		END
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


