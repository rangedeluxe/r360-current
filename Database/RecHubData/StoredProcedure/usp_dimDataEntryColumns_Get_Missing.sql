--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_Missing
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_Missing') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_Missing
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_Missing
(
	@XBatch					XML,
	@parmClientAccountKey	INT,
	@parmSourceIdentifier	INT,
	@parmMessage			VARCHAR(MAX) OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/19/2011
*
* Purpose: Create a descriptive error message listing table.columnname of items
*		missing from RecHubData.dimDataEntryColumns or RecHubData.ClientAccountsDataEntryColumns
*		tables.
*
* Modification History
* 04/19/2011 CR 33925 JPB	Created
* 04/12/2013 WI 90598 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_CreateMissingDEColumnsErrorMessage
*							Change references: Lockbox to ClientAccount.
*							Change Parameters: @parmLockboxKey to @parmClientAccountKey
*							Change loading of message field to a While Loop to load each row.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY
	DECLARE @MaxCount INT;
	DECLARE @LoopCount INT;

	DECLARE @MissingDEInfo TABLE
	(
		RowID		INT IDENTITY(1,1),
		TableName	VARCHAR(36),
		FldName		VARCHAR(36)
	)

	INSERT INTO @MissingDEInfo(TableName,FldName)
	SELECT	DISTINCT 
			DE.TableName,
			DE.FldName
	FROM (
			SELECT	
					DataEntry.att.value('@FldName', 'varchar(32)') as FldName, 
					DataEntry.att.value('@ReportTitle', 'varchar(32)') as ReportTitle,
					DataEntry.att.value('@TableName', 'varchar(36)') as TableName,
					DataEntry.att.value('@FldDataTypeEnum', 'smallint') as DataType,
					DataEntry.att.value('@FldLength', 'tinyint') as FldLength,
					DataEntry.att.value('@ScreenOrder', 'tinyint') as ScreenOrder
			FROM	@XBatch.nodes('/Batches/Batch/DataEntry/DataEntry') DataEntry(att)
		) DE
		LEFT JOIN RecHubData.fn_dimDataEntryColumns_Get_ByClientAccountKey(@parmClientAccountKey) dimDEColumns 
			ON DE.TableName = dimDEColumns.TableName 
				AND DE.FldName = dimDEColumns.FldName 
				AND DE.ReportTitle = dimDEColumns.DisplayName 
				AND DE.ScreenOrder = CASE WHEN @parmSourceIdentifier = 1 THEN DE.ScreenOrder ELSE dimDEColumns.ScreenOrder END 
				AND DE.FldLength = CASE WHEN @parmSourceIdentifier = 1 THEN DE.FldLength ELSE dimDEColumns.FldLength END
				AND DE.DataType = CASE WHEN @parmSourceIdentifier = 1 THEN DE.DataType ELSE dimDEColumns.DataType END
	WHERE dimDEColumns.DataEntryColumnKey IS NULL;
	
	SET @MaxCount = (SELECT MAX(RowID) FROM @MissingDEInfo);

	IF @MaxCount > 0
	BEGIN
		SET @parmMessage = 'Could not find ';
		SET @LoopCount = 1;

		WHILE @LoopCount <= @MaxCount
			BEGIN
				SELECT @parmMessage = @parmMessage + TableName + '.' + FldName + ','
				FROM @MissingDEInfo
				WHERE RowID = @LoopCount;

				SET @LoopCount = @LoopCount + 1;
				CONTINUE
			END
		SET @parmMessage = SUBSTRING(@parmMessage,1,LEN(@parmMessage)-1);  -- Formatting to remove last comma
		SET @parmMessage = @parmMessage + ' in RecHubData.dimDataEntryColumns or RecHubData.ClientAccountsDataEntryColumns for ClientAccountKey ' 
			+ CAST(@parmClientAccountKey AS VARCHAR) + ' SourceIdentifier ' + CAST(@parmSourceIdentifier AS VARCHAR);
	END
END TRY
BEGIN CATCH
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorProcedure	 NVARCHAR(200),
			@ErrorSeverity   INT,
			@ErrorState      INT,
			@ErrorLine		 INT
	
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE(),
			@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
			@ErrorLine = ERROR_LINE()

	SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
END CATCH
