--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_API_dimOrganizationsView_Get_OrganizationDropDown
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_API_dimOrganizationsView_Get_OrganizationDropDown') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_API_dimOrganizationsView_Get_OrganizationDropDown
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_API_dimOrganizationsView_Get_OrganizationDropDown
(
    @parmBankID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/26/2012
*
* Purpose: Create the Organization drop down
*
* Modification History
* 12/26/2012 WI 86243 TWE   Created by converting embedded SQL
* 03/21/2013 WI 86243 JBS	Update to 2.0 release. Change Schema to RecHubData
*							Rename View:  dimCustomersView to dimOrganizationsView
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT 
        RecHubData.dimOrganizationsView.SiteOrganizationID AS CustomerID, 
        RecHubData.dimOrganizationsView.Name, 
        RecHubData.dimOrganizationsView.[Description] 
    FROM 
        RecHubData.dimOrganizationsView 
    WHERE 
        RecHubData.dimOrganizationsView.SiteBankID =  @parmBankID
    ORDER BY 
        RecHubData.dimOrganizationsView.SiteOrganizationID;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

