IF OBJECT_ID('RecHubData.usp_AdvancedSearch_Get_PrelimResults') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_Get_PrelimResults
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_Get_PrelimResults
(
	@parmBatchNumberFrom		INT,
	@parmBatchNumberTo			INT,
	@parmBatchPaymentTypeKey	TINYINT,
	@parmBatchSourceKey			SMALLINT,
	@parmDepositDateKeyFrom		INT,
	@parmDepositDateKeyTo		INT,
	@parmMarkSense				BIT,
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmSourceBatchIDFrom		BIGINT,
	@parmSourceBatchIDTo		BIGINT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Query Data Advanced Search fact table for matching records
*
* Modification History
* 08/16/2019 R360-30221 JPB	Created - based on RecHubData.usp_ClientAccount_Search
* 08/20/2019 R360-30227 JPB Added support for filtering
*******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY

	SELECT
		RecHubData.factTransactionSummary.factTransactionSummaryKey,		
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubUser.SessionClientAccountEntitlements.SiteBankID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
	FROM
		RecHubUser.SessionClientAccountEntitlements
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			AND RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID =
			CASE 
			WHEN @parmSiteClientAccountID > 0 THEN @parmSiteClientAccountID
			ELSE RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			END
		AND RecHubData.factTransactionSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey
		AND ((RecHubData.factTransactionSummary.DepositDateKey <= RecHubUser.SessionClientAccountEntitlements.EndDateKey AND RecHubUser.SessionClientAccountEntitlements.ViewAhead = 0 ) OR (RecHubUser.SessionClientAccountEntitlements.ViewAhead = 1))
		AND RecHubData.factTransactionSummary.DepositDateKey >= @parmDepositDateKeyFrom
		AND RecHubData.factTransactionSummary.DepositDateKey <= @parmDepositDateKeyTo
		AND RecHubData.factTransactionSummary.SourceBatchID >= @parmSourceBatchIDFrom
		AND RecHubData.factTransactionSummary.SourceBatchID <= @parmSourceBatchIDTo
		AND RecHubData.factTransactionSummary.BatchNumber >= @parmBatchNumberFrom
		AND RecHubData.factTransactionSummary.BatchNumber <= @parmBatchNumberTo
		AND RecHubData.factTransactionSummary.OMRCount >= @parmMarkSense
		AND RecHubData.factTransactionSummary.DepositStatus >= 850
		AND RecHubData.factTransactionSummary.BatchSourceKey = CASE WHEN @parmBatchSourceKey IS NULL THEN RecHubData.factTransactionSummary.BatchSourceKey ELSE @parmBatchSourceKey END
		AND RecHubData.factTransactionSummary.BatchPaymentTypeKey = CASE WHEN @parmBatchPaymentTypeKey IS NULL THEN RecHubData.factTransactionSummary.BatchPaymentTypeKey ELSE @parmBatchPaymentTypeKey END
		AND RecHubData.factTransactionSummary.IsDeleted = 0
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
