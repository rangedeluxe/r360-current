--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_WorkgroupPayers_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_WorkgroupPayers_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_WorkgroupPayers_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_WorkgroupPayers_Get
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmRoutingNumber			NVARCHAR(30) = NULL,
	@parmAccount				NVARCHAR(30) = NULL
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 04/25/2019
*
* Purpose: Select the WorkgroupPayers for a given RT/Account that belong to a given workgroup 
*
* Modification History
* 04/25/2019 R360-15311 MGE	Created
* 05/14/2019 CES-4331   MSV Modified to allow NULL for RT and Account parameters,
*                            and to return additional columns from the table.
*
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT
		RecHubData.WorkgroupPayers.RoutingNumber,
		RecHubData.WorkgroupPayers.Account,
		RecHubData.WorkgroupPayers.PayerName,
		RecHubData.WorkgroupPayers.IsDefault,
		RecHubData.WorkgroupPayers.CreatedBy,
		RecHubData.WorkgroupPayers.CreationDate,
		RecHubData.WorkgroupPayers.ModifiedBy,
		RecHubData.WorkgroupPayers.ModificationDate
	FROM 
		RecHubData.WorkgroupPayers
	WHERE	
		RecHubData.WorkgroupPayers.SiteBankID = @parmSiteBankID
		AND	RecHubData.WorkgroupPayers.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.WorkgroupPayers.RoutingNumber = CASE WHEN @parmRoutingNumber IS NULL THEN RecHubData.WorkgroupPayers.RoutingNumber ELSE @parmRoutingNumber END
		AND RecHubData.WorkgroupPayers.Account = CASE WHEN @parmAccount IS NULL THEN RecHubData.WorkgroupPayers.Account ELSE @parmAccount END
	ORDER BY IsDefault DESC, PayerName
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH



