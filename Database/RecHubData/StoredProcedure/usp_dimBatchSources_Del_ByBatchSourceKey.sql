--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimBatchSources_Del_ByBatchSourceKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchSources_Del_ByBatchSourceKey') IS NOT NULL
	   DROP PROCEDURE RecHubData.usp_dimBatchSources_Del_ByBatchSourceKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchSources_Del_ByBatchSourceKey
(
	@parmUserID			INT,
	@parmBatchSourceKey	SMALLINT,
	@parmRowsReturned	INT = 0			    OUTPUT,
	@parmErrorMessage   VARCHAR(140) = NULL OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/03/2014
*
* Purpose: Delete dimBatchSources
*
* Modification History
* 06/03/2014 WI 143294  EAS	Created
* 07/09/2014 WI 153006  RDS	Include Auditing
* 06/05/2015 WI 217200  LS  Update Audit Messages
* 08/21/2017 PT 127612941	MGE	Fix NULL ImportType that caused audit message to be NULL
****************************************************************************************/
SET NOCOUNT ON; 

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	DECLARE @auditMessage			VARCHAR(1024);

	-- Save Previous Values before attempting to delete
	DECLARE @prevShortName					VARCHAR(30),
			@prevLongName					VARCHAR(128),
			@prevEntityID					INT,
			@prevImportTypeKey				TINYINT,
			@prevIsActive					BIT,
			@prevImportType                 VARCHAR(128),
			@UserLogonName					VARCHAR(24);

	SELECT @prevShortName = ShortName, @prevLongName = LongName, @prevEntityID = EntityID, @prevImportTypeKey = ImportTypeKey, @prevIsActive = IsActive
	FROM 
		RecHubData.dimBatchSources
	WHERE
		RecHubData.dimBatchSources.BatchSourceKey = @parmBatchSourceKey;

	SELECT @prevImportType = RecHubData.dimImportTypes.ShortName FROM RecHubData.dimImportTypes WHERE RecHubData.dimImportTypes.ImportTypeKey = @prevImportTypeKey;

	SELECT @UserLogonName = LogonName FROM RecHubUser.Users Where UserID = @parmUserID;

	DELETE FROM	
		RecHubData.dimBatchSources
	WHERE	
		RecHubData.dimBatchSources.BatchSourceKey = @parmBatchSourceKey;

	SELECT	
		@parmRowsReturned = @@ROWCOUNT; 

	IF (@parmRowsReturned > 0)
	BEGIN
		-- Audit the delete
		SELECT @auditMessage = @UserLogonName + ' deleted Payment Source for EntityID: ' + ISNULL(CAST(@prevEntityID AS VARCHAR(20)), 'NULL')
			+ ', ShortName = ' + @prevShortName
			+ ', LongName = ' + @prevLongName
			+ ', System = ' + @prevImportType
			+ ', IsActive = ' + CAST(@prevIsActive AS VARCHAR(2))
			+ '.';

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubData.usp_dimBatchSources_Del_ByBatchSourceKey',
				@parmSchemaName			= 'RecHubData',
				@parmTableName			= 'dimBatchSources',
				@parmColumnName			= 'BatchSourceKey',
				@parmAuditValue			= @parmBatchSourceKey,
				@parmAuditType			= 'DEL',
				@parmAuditMessage		= @auditMessage;
	END
	
	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY

BEGIN CATCH

	IF ERROR_NUMBER() = 547
	 BEGIN
		SET @parmErrorMessage = 'Delete Failed.<br />You are not able to delete this Payment Source because it has been tied to a Batch.'
	 END

	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

	EXEC RecHubCommon.usp_WfsRethrowException;

END CATCH