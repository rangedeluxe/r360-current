--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Upd') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Upd
(
	@parmUserId			INT,
	@parmFIEntityID		INT,
	@parmBankName		VARCHAR(128),
	@parmBankKey		INT	OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 06/03/2014
*
* Purpose: Updates a dimBanks record (dimensional table tracks historical data,
*			so this is really an update / insert)
*
* Modification History
* 06/03/2014 WI 145890 RDS	Created
* 05/18/2015 WI 213651 JPB	Updated audit message.
* 07/05/2016 WI 288733 TWE  Carry ABA from previous record to new record
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction bit = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	DECLARE @auditMessage			VARCHAR(1024),
			@auditColumns			RecHubCommon.AuditValueChangeTable,
			@errorDescription		VARCHAR(1024);

	DECLARE @curTime DATETIME = GETDATE();
	DECLARE @oldBankKey INT = @parmBankKey;

	-- Verify User ID
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimBanks record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Check if there is an actual update
	DECLARE @isHistoricalChange BIT,
			@isMostRecent BIT;

	SELECT	@isHistoricalChange		= CASE WHEN CAST(@parmBankName AS VARBINARY(128)) <> CAST(BankName AS VARBINARY(128)) -- CAST to varbinary for case-sensitive comparison...
											OR (EntityID IS NULL AND @parmFIEntityID IS NOT NULL)
										THEN 1
										ELSE 0
										END,
			@isMostRecent			= MostRecent
	FROM	RecHubData.dimBanks
	WHERE	RecHubData.dimBanks.BankKey = @parmBankKey;

	-- Verify the row exists to update
	IF @isMostRecent IS NULL
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimBanks record, BankKey (' + CAST(@parmBankKey AS VARCHAR(30)) + ') does not exist.';
		RAISERROR(@errorDescription, 16, 1);
	END
	ELSE IF @isMostRecent <> 1
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimBanks record, BankKey (' + CAST(@parmBankKey AS VARCHAR(30)) + ' is no longer the most recent.';
		RAISERROR(@errorDescription, 16, 1);
	END

	IF @isHistoricalChange = 1
	BEGIN
		-- Get Previous Values and clear the "MostRecent" flag
		DECLARE @prevBankName VARCHAR(128),
				@prevSiteBankID INT,
				@prevEntityID INT,
				@prevABA VARCHAR(10);

		UPDATE	RecHubData.dimBanks
		SET		@prevBankName				= RecHubData.dimBanks.BankName,
				@prevEntityID				= RecHubData.dimBanks.EntityID,
				@prevSiteBankID				= RecHubData.dimBanks.SiteBankID,
				@prevABA                    = RecHubData.dimBanks.ABA,

				RecHubData.dimBanks.MostRecent			= 0,
				RecHubData.dimBanks.ModificationDate	= GETDATE()
		WHERE	RecHubData.dimBanks.BankKey = @parmBankKey;
		
		-- Create a new row with the new values (bringing forward any non-updated columns)
		INSERT INTO RecHubData.dimBanks
			(
				SiteBankID,
				BankName,
				EntityID,
				ABA,
				CreationDate,
				ModificationDate,
				MostRecent
			)
		SELECT
				@prevSiteBankID,
				@parmBankName,
				ISNULL(@prevEntityID, @parmFIEntityID),	-- Only update this column if it was previously null
				@prevABA,
				@curTime,
				@curTime,
				1;

		SET @parmBankKey = SCOPE_IDENTITY();

		-- Audit the RecHubData.dimBanks update (changes only)
		IF CAST(@prevBankName AS VARBINARY(128)) <> CAST(@parmBankName AS VARBINARY(128))
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('BankName', @prevBankName, @parmBankName);
		IF @prevEntityID IS NULL AND @parmFIEntityID IS NOT NULL
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EntityID', NULL, CAST(@parmFIEntityID AS VARCHAR(10)));

		SET @auditMessage = 'Modified dimBanks for EntityID: ' + CAST(@parmFIEntityID AS VARCHAR(10))
			+ ', SiteBankID: ' + CAST(@prevSiteBankID AS VARCHAR(10))
			+ ', BankName: ' + @parmBankName
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_dimBanks_Upd',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimBanks',
			@parmColumnName			= 'BankKey',
			@parmAuditValue			= @parmBankKey,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END

	Cleanup:

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

