--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Get_ClientAccountList
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Get_ClientAccountList') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Get_ClientAccountList
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Get_ClientAccountList
(
	@parmSearchRequest	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/31/2017
*
* Purpose: Retrieve a list client accounts authorized for an invoice search.
*
* Modification History
* 01/31/2017 #134363645 JPB Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY 

	DECLARE @ClientAccountList TABLE
	(
		SessionID UNIQUEIDENTIFIER,
		SiteBankID INT,
		SiteClientAccountID INT
	);

	INSERT INTO @ClientAccountList(SessionID,SiteBankID,SiteClientAccountID)
	SELECT
		ClientAccount.att.query('../../SessionID').value('.','UNIQUEIDENTIFIER') AS SessionID,
		ClientAccount.att.value('@BankID','INT') AS SiteBankID,
		ClientAccount.att.value('@ClientAccountID','INT') AS SiteClientAccountID
	FROM	
		@parmSearchRequest.nodes('/Root/ClientAccounts/ClientAccount') ClientAccount(att);

	SELECT 
		ClientAccountList.SessionID,
		RecHubUser.SessionClientAccountEntitlements.SiteBankID, 
		RecHubUser.SessionClientAccountEntitlements.EntityID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID, 
		RecHubUser.SessionClientAccountEntitlements.ClientAccountKey, 
		RecHubUser.SessionClientAccountEntitlements.StartDateKey,
		RecHubUser.SessionClientAccountEntitlements.EndDateKey,
		RecHubUser.SessionClientAccountEntitlements.ViewAhead
	FROM 
		@ClientAccountList ClientAccountList
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubUser.SessionClientAccountEntitlements.SessionID = ClientAccountList.SessionID
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = ClientAccountList.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = ClientAccountList.SiteClientAccountID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH