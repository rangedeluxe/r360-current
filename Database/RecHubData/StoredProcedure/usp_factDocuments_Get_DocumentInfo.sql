--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_DocumentInfo
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_DocumentInfo') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get_DocumentInfo
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_DocumentInfo 
(
    @parmSessionID				UNIQUEIDENTIFIER,	--WI 142852
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmImmutableDateKey		INT,
    @parmBatchID				BIGINT,				--WI 142852
    @parmBatchSequence			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Get document information by bank
*
* Modification History
* 01/24/2013 WI 86304 TWE   Created by converting embedded SQL
*					  JPB	Moved to RecHubData schema.
*							Renamed from usp_factDocuments_Get_Document_BySiteBank
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
* 05/29/2014 WI 142852 DLD  Batch Collision/RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BankKey			INT,		--WI 142852
		@ClientAccountKey	INT,		--WI 142852
		@DepositDate		DATETIME,	--WI 142852
		@DepositDateKey		INT, 		--WI 142852
        @EndDateKey			INT, 		--WI 142852 
		@StartDateKey		INT; 		--WI 142852
		
BEGIN TRY

	/* Get Deposit Date Key for Deposit Date */							--WI 142852
	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SET @DepositDate = CAST(CAST(@DepositDateKey AS CHAR) AS DATETIME);	--WI 142852

	/* Ensure the start date is not beyond the max viewing days */		--WI 142852
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @DepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

    SELECT 
        RecHubUser.SessionClientAccountEntitlements.SiteBankID,															--WI 142852
        RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,												--WI 142852
        RecHubData.dimClientAccounts.OnlineColorMode,
        RecHubData.factDocuments.ImmutableDateKey,
        RecHubData.factDocuments.DepositDateKey,
        RecHubData.factDocuments.BatchID,
        RecHubData.factDocuments.SourceBatchID,																			--WI 142852
        RecHubData.factDocuments.TransactionID,
        RecHubData.factDocuments.TxnSequence,
        RecHubData.factDocuments.BatchSequence,
        RecHubData.dimDocumentTypes.FileDescriptor
    FROM 
        RecHubData.factDocuments
        INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
            RecHubData.factDocuments.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey	--WI 142852
        INNER JOIN RecHubData.dimClientAccounts ON
            RecHubData.factDocuments.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubData.dimDocumentTypes ON
            RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
    WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID										--WI 142852
        AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID									--WI 142852
        AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID					--WI 142852
        AND RecHubData.factDocuments.DepositDateKey = @StartDateKey														--WI 142852
        AND RecHubData.factDocuments.ImmutableDateKey = @parmImmutableDateKey
        AND RecHubData.factDocuments.BatchID = @parmBatchID
        AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
		AND RecHubData.factDocuments.IsDeleted = 0;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH