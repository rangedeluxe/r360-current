--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_DataEntryDefinitions
--WFSScriptProcessorStoredProcedureDrop

IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_DataEntryDefinitions
(
	@parmIsPayment BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/03/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/03/2013 WI 103848 JMC   Initial Version
* 06/26/2014 WI 150467 BLR   Added DisplayName to the Select Queries
* 12/11/2015 WI 252332 BLR   Updated to point to the new DE structure, added
*                            more data.  Adding Permission for User account.
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	IF(@parmIsPayment > 0)
	BEGIN
		SELECT
			'RecHub'  AS [Schema],
			'factChecks' AS 'TableName',
			FieldName   AS ColumnName,
			UILabel AS DisplayName,
			RecHubData.dimWorkgroupDataEntryColumns.SiteBankID,
			RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID,
			dimBatchSources.LongName as 'PaymentSource',
			dimClientAccounts.LongName as 'WorkgroupName',
			dimBanks.BankName as 'BankName',
			RecHubData.dimWorkgroupDataEntryColumns.IsActive,
			CASE
				WHEN [DataType]=1 THEN 'varchar'
				WHEN [DataType]=6 THEN 'int'
				WHEN [DataType]=7 THEN 'float'
				WHEN [DataType]=11 THEN 'datetime'
			END       AS [Type]
		FROM RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN RecHubData.dimBatchSources 
			ON RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimClientAccounts 
			ON RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND RecHubData.dimClientAccounts.MostRecent = 1
		INNER JOIN RecHubData.dimBanks
			ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimBanks.SiteBankID
			AND RecHubData.dimBanks.MostRecent = 1
		WHERE IsCheck = 1;
	END
	ELSE
	BEGIN
		SELECT
			'RecHub'  AS [Schema],
			'factStubs' AS 'TableName',
			FieldName   AS ColumnName,
			UILabel AS DisplayName,
			RecHubData.dimWorkgroupDataEntryColumns.SiteBankID,
			RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID,
			dimBatchSources.LongName as 'PaymentSource',
			dimClientAccounts.LongName as 'WorkgroupName',
			dimBanks.BankName as 'BankName',
			RecHubData.dimWorkgroupDataEntryColumns.IsActive,
			CASE
				WHEN [DataType]=1 THEN 'varchar'
				WHEN [DataType]=6 THEN 'int'
				WHEN [DataType]=7 THEN 'float'
				WHEN [DataType]=11 THEN 'datetime'
			END       AS [Type]
		FROM RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN RecHubData.dimBatchSources 
			ON RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimClientAccounts 
			ON RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND RecHubData.dimClientAccounts.MostRecent = 1
		INNER JOIN RecHubData.dimBanks
			ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimBanks.SiteBankID
			AND RecHubData.dimBanks.MostRecent = 1
		WHERE IsCheck = 0;
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH