--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factStubs_Get_ByTransactionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factStubs_Get_ByTransactionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factStubs_Get_ByTransactionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factStubs_Get_ByTransactionID
( 
    @parmSessionID				UNIQUEIDENTIFIER,	--WI 142859
	@parmSiteBankID				INT, 
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
    @parmTransactionID			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: 
*
* Modification History
* 03/23/2009 CR 25817  JMC	Created.
* 11/09/2009 CR 28083  JPB	Updated to check the data entry details table if 
*							there is not a fact stubs record.
* 04/05/2010 CR 29307  JPB  Account renamed to AccountNumber.
* 09/15/2010 CR 30980  JPB	Account for multiple rows in factDataEntryDetails 
*							on the ELSE by selecting distinct.
* 01/21/2011 CR 32402  JPB	Changed from dimLockboxes to dimLockboxesView.
* 01/19/2012 CR 49143  JNE  Add StubSequence to result set.
* 04/30/2012 CR 52388  JPB  FP:51671 
* 04/02/2013 WI 90724  JPB	Updated for 2.0.
*							FP:WI 85070 Performance tuning.
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionStubs.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Changed dimLockboxes refs to dimClientAccounts.
*							Removed check of factDataEntryDetails.
* 06/02/2014 WI 142859 PKW  Batch Collision.
* 03/24/2014 WI 197814 MAA  Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead 
* 05/27/2015 WI 215274 JBS  (FP 215003) Add BatchSequence to Order By
* 02/29/2016 WI 266938 MAA Added SourceProcessingDateKey for image retreival 
* 11/14/2016 #133519023 BLR Added TransactionID to the select fields.
******************************************************************************/
SET NOCOUNT ON;

DECLARE														--WI 142859
		@StartDateKey	INT,								--WI 142859
		@EndDateKey		INT;								--WI 142859

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ 
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays		--WI 142859
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	
		RecHubData.factStubs.BatchSequence,
		RecHubData.factStubs.Amount,
		RecHubData.factStubs.AccountNumber,
		RecHubData.factStubs.IsOMRDetected,
		RecHubData.factStubs.DocumentBatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.SourceProcessingDateKey,
		RecHubData.factStubs.TransactionID
	FROM	
		RecHubData.factStubs
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON					--WI 142859
			RecHubData.factStubs.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID	--WI 142859
		AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID	--WI 142859
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID --WI 142859
		AND RecHubData.factStubs.DepositDateKey = @EndDateKey						--WI 142859
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.TransactionID = @parmTransactionID
		AND RecHubData.factStubs.IsDeleted = 0
	ORDER BY 
		RecHubData.factStubs.BatchID ASC,
		RecHubData.factStubs.BatchSequence ASC,
		RecHubData.factStubs.TxnSequence ASC,
		RecHubData.factStubs.SequenceWithinTransaction ASC
	OPTION ( RECOMPILE );

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
