--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Del_BankClientAccountDelete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Del_BankClientAccountDelete') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Del_BankClientAccountDelete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Del_BankClientAccountDelete
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	NVARCHAR(12),
	@parmUpdateSuccess			BIT = 0 OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 09/22/2016
*
* Purpose: Delete all set up data for given Workgroups and Bank.  
*			Only delete Bank info if @parmSiteClientAccountID = 'ALL'
*
*
* List of Tables to remove BankID and ClientAccountID
* REchubData.dimBanks
* REchubData.dimClientAccounts
* REchubData.dimWorkgroupDataEntryColumns
* REchubData.ElectronicAccounts
* REchubData.dimOrganizations
* REchubData.ClientAccountBatchSource
* REchubData.ClientAccountBatchPaymentTypes
* REchubData.dimImageRPSAliasMappings
* REchubUser.OLClientAccountUsers
* REchubUser.OLClientAccounts
* REchubUser.OLWorkgroups
* REchubUser.SessionClientAccountEntitlementID
*
*
* Modification History
* 09/21/2016 PT #129555329 JBS	Created
******************************************************************************/
SET NOCOUNT ON;	
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY
	
	SET @parmUpdateSuccess = 0;

	CREATE TABLE #TempClientAccountKeys
	(
		ClientAccountKey	INT
	);
	
	/* Find the ClientAccountKeys associated to the SiteClientAccountIDs passed in. */
	INSERT INTO #TempClientAccountKeys
	(
		ClientAccountKey 
	)
	SELECT	
		RecHubData.dimClientAccounts.ClientAccountKey
	FROM	
		RecHubData.dimClientAccounts
	WHERE	
		RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubData.dimClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END;
	
	BEGIN TRANSACTION;
	
	/* Delete records from the Dimension tables */
	-- dimWorkgroupDataEntryColumns
	DELETE	
		RecHubData.dimWorkgroupDataEntryColumns
	FROM 
		RecHubData.dimWorkgroupDataEntryColumns 
	WHERE	
		RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = @parmSiteBankID
		AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );
	 
	-- ElectronicAccounts
	DELETE	
		RecHubData.ElectronicAccounts
	FROM 
		RecHubData.ElectronicAccounts 
	WHERE	
		RecHubData.ElectronicAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.ElectronicAccounts.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubData.ElectronicAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );
	
	-- ClientAccountBatchSources
	DELETE	
		RecHubData.ClientAccountBatchSources
	FROM 
		RecHubData.ClientAccountBatchSources 
		INNER JOIN #TempClientAccountKeys ON 
				RecHubData.ClientAccountBatchSources.ClientAccountKey = #TempClientAccountKeys.ClientAccountKey
	OPTION( RECOMPILE );
	
	-- ClientAccountBatchPaymentTypes
	DELETE	
		RecHubData.ClientAccountBatchPaymentTypes
	FROM 
		RecHubData.ClientAccountBatchPaymentTypes 
		INNER JOIN #TempClientAccountKeys ON 
				RecHubData.ClientAccountBatchPaymentTypes.ClientAccountKey = #TempClientAccountKeys.ClientAccountKey
	OPTION( RECOMPILE );

	-- dimImageRPSAliasMappings
	DELETE	
		RecHubData.dimImageRPSAliasMappings
	FROM 
		RecHubData.dimImageRPSAliasMappings 
	WHERE	
		RecHubData.dimImageRPSAliasMappings.SiteBankID = @parmSiteBankID
		AND RecHubData.dimImageRPSAliasMappings.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubData.dimImageRPSAliasMappings.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );

	-- SessionClientAccountEntitlements
	DELETE	
		RecHubUser.SessionClientAccountEntitlements
	FROM 
		RecHubUser.SessionClientAccountEntitlements 
		INNER JOIN #TempClientAccountKeys ON 
			RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = #TempClientAccountKeys.ClientAccountKey
	OPTION( RECOMPILE );

	-- OLWorkgroups
	DELETE	
		RecHubUser.OLWorkgroups
	FROM 
		RecHubUser.OLWorkgroups 
	WHERE	
		RecHubUser.OLWorkgroups.SiteBankID = @parmSiteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubUser.OLWorkgroups.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );

	-- OLClientAccountUsers
	DELETE	
		RecHubUser.OLClientAccountUsers
	FROM 
		RecHubUser.OLClientAccountUsers 
		INNER JOIN Rechubuser.OLClientAccounts ON
				RecHubUser.OLClientAccountUsers.OLClientAccountID = RecHubUser.OLClientAccounts.OLClientAccountID
	WHERE	
		RecHubUser.OLClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubUser.OLClientAccounts.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubUser.OLClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );

	-- OLClientAccounts
	DELETE	
		RecHubUser.OLClientAccounts
	FROM 
		RecHubUser.OLClientAccounts 
	WHERE	
		RecHubUser.OLClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubUser.OLClientAccounts.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubUser.OLClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END

	OPTION( RECOMPILE );

	-- dimClientAccounts
	DELETE	
		RecHubData.dimClientAccounts
	FROM 
		RecHubData.dimClientAccounts 
	WHERE	
		RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = 
		CASE @parmSiteClientAccountID
			WHEN 'ALL' 
				THEN RecHubData.dimClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END
	OPTION( RECOMPILE );

	-- dimBanks
	-- Only delete the bank record if ALL is chosen for ClientAccounts
	IF @parmSiteClientAccountID = 'ALL'
	BEGIN
		-- dimOrganizations  Only deleting this when we are finally delete the BankID
		DELETE	
			RecHubData.dimOrganizations
		FROM 
			RecHubData.dimOrganizations 
		WHERE	
			RecHubData.dimOrganizations.SiteBankID = @parmSiteBankID
		OPTION( RECOMPILE );

		DELETE	
			RecHubData.dimBanks
		FROM 
			RecHubData.dimBanks 
		WHERE	
			RecHubData.dimBanks.SiteBankID = @parmSiteBankID
		OPTION( RECOMPILE );
	END

 	COMMIT TRANSACTION;
	SET @parmUpdateSuccess = 1;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TempClientAccountKeys')) 
		DROP TABLE #TempClientAccountKeys; 

END TRY
BEGIN CATCH
	
	IF @@TRANCOUNT > 0
		ROLLBACK TRANSACTION;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TempClientAccountKeys')) 
		DROP TABLE #TempClientAccountKeys; 

 	EXEC RecHubCommon.usp_WfsRethrowException;
	
END CATCH
