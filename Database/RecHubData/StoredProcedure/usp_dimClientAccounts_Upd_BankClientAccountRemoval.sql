--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Upd_BankClientAccountRemoval
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Upd_BankClientAccountRemoval') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Upd_BankClientAccountRemoval
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Upd_BankClientAccountRemoval
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	NVARCHAR(12),
	@parmRetentionDays			INT,
	@parmUpdateSuccess			BIT = 0 OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 09/09/2013
*
* Purpose: Update Retention days in CLientaccount rows so Purge of data and images are quickened
*			so we can remove ClientAccount and Bank
*
* Modification History
* 09/21/2016 PT #129555329 JBS	Created
******************************************************************************/
SET NOCOUNT ON;	
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE @ClientAccountID			INT,
		@ModificationDate			DATE,
		@errorDescription			NVARCHAR(400)

BEGIN TRY

	SET @parmUpdateSuccess = 0;
	SET @ModificationDate = GETDATE();

	IF @parmSiteClientAccountID <> 'ALL' AND ISNumeric(@parmSiteClientAccountID) = 1
		SET @ClientAccountID = CAST(@parmSiteClientAccountID AS INT);


	-- Verify good data
	IF @ClientAccountID IS NULL AND @parmSiteClientAccountID <> 'ALL'
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, ClientAccountID (' + ISNULL(CAST(@parmSiteClientAccountID AS VARCHAR(12)), 'NULL') + ') must be numeric or ALL';
		RAISERROR(@errorDescription, 16, 1);
	END
	IF ISNUMERIC(@parmRetentionDays  ) = 0 OR @parmRetentionDays !> 0
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, RetentionDays (' + ISNULL(CAST(@parmRetentionDays AS VARCHAR(12)), 'NULL') + ') must be numeric and greater than 0.';
		RAISERROR(@errorDescription, 16, 1);
	END


	IF @parmSiteClientAccountID = 'ALL'
		BEGIN
			UPDATE RecHubData.dimClientAccounts
				SET DataRetentionDays = COALESCE(@parmRetentionDays,DataRetentionDays),
					ImageRetentionDays = COALESCE(@parmRetentionDays,ImageRetentionDays),
					IsActive = 0,
					ModificationDate = @ModificationDate
				WHERE
					SiteBankID = @parmSiteBankID AND 
					MostRecent = 1;
		END
	ELSE
		BEGIN
			UPDATE RecHubData.dimClientAccounts
				SET DataRetentionDays = COALESCE(@parmRetentionDays,DataRetentionDays),
					ImageRetentionDays = COALESCE(@parmRetentionDays,ImageRetentionDays),
					IsActive = 0,
					ModificationDate = @ModificationDate
				WHERE
					SiteBankID = @parmSiteBankID AND
					SiteClientAccountID = @ClientAccountID AND
					MostRecent = 1;
		END 

	IF @@ERROR = 0
		SET @parmUpdateSuccess = 1;


END TRY
BEGIN CATCH
	
	BEGIN
		EXEC RecHubCommon.usp_WfsRethrowException;
	END

END CATCH
