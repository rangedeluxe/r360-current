--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_factNotificationFiles_Get_ByNotificationMessageGroup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotificationFiles_Get_ByNotificationMessageGroup
	@parmNotificationMessageGroup INT,
	@parmSessionID UNIQUEIDENTIFIER
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: Returns a list of Notification Files.
*
* Modification History
* 09/04/2012 CR  55380 JMC	Created
* 10/14/2013 WI 117127 JMC	Update to 2.0 release. Change schema to RecHubData
*							Change references: Lockbox to ClientAccount
*							Customer to Organization
* 10/30/2013 WI 119339 MLH  Added IsDeleted = 0
* 11/15/2017 PT 152447729 JPB	Removed dimOrganization
* 03/09/2018 PT 154252232 MGE Added call to check whether user has access to all attachments.
*********************************************************************************************/

SET NOCOUNT ON

BEGIN TRY

DECLARE @NoAccessCount INT;
EXEC RecHubData.usp_factNotifications_MessageGroupNoAccessCount @parmNotificationMessageGroup, @parmSessionID, @NoAccessCount OUT;

SELECT 
	RecHubData.factNotificationFiles.factNotificationFileKey,
	RecHubData.factNotificationFiles.NotificationMessageGroup,
	RecHubData.factNotificationFiles.BankKey,
	RecHubData.factNotificationFiles.OrganizationKey,
	RecHubData.factNotificationFiles.ClientAccountKey,
	RecHubData.factNotificationFiles.NotificationDateKey,
	RecHubData.factNotificationFiles.NotificationSourceKey,
	RecHubData.factNotificationFiles.CreationDate,
	RecHubData.factNotificationFiles.ModificationDate,
	RecHubData.factNotificationFiles.CreatedBy,
	RecHubData.factNotificationFiles.ModifiedBy,
	RecHubData.factNotificationFiles.FileIdentifier,
	RecHubData.factNotificationFiles.UserFileName,
	RecHubData.factNotificationFiles.FileExtension,
	RecHubData.dimNotificationFileTypes.FileTypeDescription,
	CAST(RecHubData.factNotificationFiles.NotificationDateKey AS VARCHAR(8)) + '\' +
	CAST(RecHubData.dimBanks.SiteBankID AS VARCHAR(16)) + '\' +
	'Lbx' + '\' +
	CASE WHEN RecHubData.dimClientAccounts.SiteClientAccountID IS NULL THEN '' ELSE CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(16)) + '\' END +
	CAST(RecHubData.factNotificationFiles.FileIdentifier AS VARCHAR(36)) + 
	CASE WHEN LEN(RecHubData.factNotificationFiles.FileExtension) > 0 AND SUBSTRING(RecHubData.factNotificationFiles.FileExtension, 1, 1) <> '.' THEN '.' ELSE '' END +
	RecHubData.factNotificationFiles.FileExtension AS FilePath
FROM
	RecHubData.factNotificationFiles
		LEFT OUTER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factNotificationFiles.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		LEFT OUTER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factNotificationFiles.BankKey
		INNER JOIN RecHubData.dimNotificationFileTypes ON
			RecHubData.factNotificationFiles.NotificationFileTypeKey = RecHubData.dimNotificationFileTypes.NotificationFileTypeKey
WHERE
	RecHubData.factNotificationFiles.NotificationMessageGroup = @parmNotificationMessageGroup
	AND RecHubData.factNotificationFiles.IsDeleted = 0
	AND @NoAccessCount = 0				--Must have access to all workgroups in file

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
