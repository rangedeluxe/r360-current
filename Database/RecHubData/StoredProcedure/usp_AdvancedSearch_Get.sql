use IntegrationTests_DDXW2JR2_AdvancedSearchCheckAndStubDataEntry_20191111154151
GO




IF OBJECT_ID('RecHubData.usp_AdvancedSearch_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_Get
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_Get
(
	@parmAdvancedSearchBaseKeysTable RecHubData.AdvancedSearchBaseKeysTable READONLY,
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmSearchTotals XML OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Query Data Advanced Search fact table for matching records
*
* Modification History
* 08/16/2019 R360-16301 JPB	Created - based on RecHubData.usp_ClientAccount_Search
*******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE 
	@ChecksOnly INT = 0,
	@DEColumns	INT = 0;

BEGIN TRY

	;WITH WithDEColumns AS
	(
		SELECT 
			TableName,
			FieldName
		FROM 
			@parmAdvancedSearchWhereClauseTable
		WHERE
			(UPPER(TableName) = 'CHECKS' AND UPPER(FieldName) NOT IN ('AMOUNT', 'ACCOUNT','RT','SERIAL')) OR
			(UPPER(TableName) = 'STUBS' AND UPPER(FieldName) NOT IN ('STUBAMOUNT','STUBACCOUNTNUMBER'))
		UNION ALL
		SELECT 
			TableName,
			FieldName
		FROM 
			@parmAdvancedSearchSelectFieldsTable
		WHERE
			(UPPER(TableName) = 'CHECKS' AND UPPER(FieldName) NOT IN ('AMOUNT', 'ACCOUNT','RT','SERIAL')) OR
			(UPPER(TableName) = 'STUBS' AND UPPER(FieldName) NOT IN ('STUBAMOUNT','STUBACCOUNTNUMBER'))
	)
	SELECT 
		@DEColumns = COUNT(*) 
	FROM 
		WithDEColumns;

	IF( @DEColumns = 0 )
	BEGIN
PRINT 'BASE'
		EXEC RecHubData.usp_AdvancedSearch_Get_BaseColumns
				@parmAdvancedSearchBaseKeysTable = @parmAdvancedSearchBaseKeysTable,
				@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
				@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable,
				@parmSearchTotals = @parmSearchTotals OUT;
	END
	ELSE
	BEGIN
PRINT 'DE'
		;WITH ChecksOnly AS
		(
			SELECT 
				TableName,
				FieldName
			FROM 
				@parmAdvancedSearchWhereClauseTable
			WHERE
				UPPER(TableName) = 'STUBS'
			UNION ALL
			SELECT 
				TableName,
				FieldName
			FROM 
				@parmAdvancedSearchSelectFieldsTable
			WHERE
				UPPER(TableName) = 'STUBS'
		)
		SELECT 
			@ChecksOnly = COUNT(*) 
		FROM 
			ChecksOnly;

		IF( @ChecksOnly = 0 )
		BEGIN
PRINT 'CHECKS ONLY'
			EXEC RecHubData.usp_AdvancedSearch_Get_DataEntryColumns_ChecksOnly
					@parmAdvancedSearchBaseKeysTable = @parmAdvancedSearchBaseKeysTable,
					@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
					@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable,
					@parmSearchTotals = @parmSearchTotals OUT;
		END
		ELSE
		BEGIN
PRINT 'CHEKCS/STUBS'
			EXEC RecHubData.usp_AdvancedSearch_Get_DataEntryColumns
					@parmAdvancedSearchBaseKeysTable = @parmAdvancedSearchBaseKeysTable,
					@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
					@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable,
					@parmSearchTotals = @parmSearchTotals OUT;
		END
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

GRANT EXECUTE ON OBJECT::RecHubData.usp_AdvancedSearch_Get TO dbRole_RecHubData;





go 
use master
go
