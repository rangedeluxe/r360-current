--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_GetBatchCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_GetBatchCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_GetBatchCount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_GetBatchCount 
(
       @parmSessionID			UNIQUEIDENTIFIER, --WI 142827
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID 	INT,
       @parmDepositDateStart		DATETIME,
       @parmDepositDateEnd		DATETIME,
       @parmMinDepositStatus		INT = 850,
       @parmPaymentTypeKey		INT = NULL,
       @parmPaymentSourceKey		INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/13/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/13/2009 CR 22817 JPB	Created.
* 02/15/2009 CR 28728       Add new parameter to usp_GetBatchCount stored procedure in support of CSR Research.
* 03/20/2013 WI 90666 JBS	Update to 2.0 Release. Change Schema Name.
*							Change proc name from usp_GetBatchCount.
*							Rename Input variable: @parmSiteLockboxID to @parmSiteClientAccountID.
* 10/10/2013 WI 116679 EAS  Add Payment Type Filter to Batch Count.
* 05/22/2014 WI 142827 PKW  Batch Collision - RAAM Integration.
* 12/09/2014 WI 181267 LA   Added Batch Payment Source Parameter
******************************************************************************/
SET NOCOUNT ON; 

DECLARE																		--WI 142827
		@StartDateKey INT,
		@EndDateKey INT;

BEGIN TRY
	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 
		COUNT(RecHubData.factBatchSummary.DepositDateKey) AS BatchCount
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142827
			RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE	
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142827		
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142827
		AND RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey
		AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey
		AND RecHubData.factBatchSummary.DepositStatus  >= @parmMinDepositStatus
		AND RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.factBatchSummary.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeKey, RecHubData.factBatchSummary.BatchPaymentTypeKey)
		AND RecHubData.factBatchSummary.BatchSourceKey = ISNULL(@parmPaymentSourceKey, RecHubData.factBatchSummary.BatchSourceKey)

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
