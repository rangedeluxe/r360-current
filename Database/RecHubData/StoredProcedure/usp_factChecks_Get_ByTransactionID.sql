--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ByTransactionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_ByTransactionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_ByTransactionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_ByTransactionID 
(
	@parmSessionID				UNIQUEIDENTIFIER,			--WI 142835
	@parmSiteBankID				INT, 
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,
    @parmTransactionID			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: Retrieve factCheck detail based on parameters (SiteBankID,  @parmSiteClientAccountID,
*							@parmDepositDate,  @parmBatchID, @parmTransactionID).
*
* Modification History
* 03/23/2009 CR 25817 JMC	Created.
* 11/16/2009 CR 28264 JMC	Added ProcessingDate to the returned dataset.
* 11/19/2009 CR 28333 JMC	Added OnlineColorMode AS LockboxColorMode to the returned dataset.
* 03/18/2011 CR 33438 JNE	Added BatchSourceKey to the returned dataset.
* 04/09/2012 CR 51911 JNE	Added BatchSiteCode to the returned dataset.
* 09/21/2012 CR 55875 JNE	Added BatchNumber to the returned dataset.
* 11/20/2012 WI 70685 JPB	Added index hint/removed dimDates join.
* 04/02/2013 WI 90716 JBS	Update to 2.0 release.  Change schema.
*							Rename proc from usp_GetTransactionChecks.
*							Rename SiteCode to SiteCodeID.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Change all references: Lockbox to ClientAccount, Processing to Immutable
*							Remove: GlobalBatchID, GlobalCheckID columns
*									dimRemitters, dimDates  tables.
*							Adding: IsDeleted = 0.
* 02/10/2014 WI 129043 EAS	Add Payment Type and Payment Source to result set.
* 04/17/2014 WI 137325 EAS  Add DDA to Transaction Check return set.
* 05/29/2014 WI 142835 PKW  Batch Collision - RAAM Integration.
* 07/18/2014 WI 154685 CEJ	Limit the results from RecHubData.usp_factChecks_Get_ByTransactionID based on the SessionID
* 09/17/2014 WI	166556 SAS  Added ImportTypeShortName Field
* 10/24/2014 WI	174087 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175119 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176329 SAS  Add BatchSourceShortName  and ImportTypeShortName to resultset.  		
*							Removed BatchSourceName
* 3/24/2015  WI 197810 MAA  Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead  
* 05/27/2015 WI 214216 JBS  (FP 215001) Add BatchSequence to Order By
* 02/29/2016 WI 266936 MAA Added SourceProcessingDateKey for image retreival 
******************************************************************************/
SET NOCOUNT ON; 

DECLARE																		--WI 142835
		@StartDateKey	INT,
		@EndDateKey		INT;
		
BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */			--WI 142835
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,		
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	
			RecHubData.factChecks.BatchID,
			RecHubData.factChecks.SourceBatchID,
			RecHubData.dimClientAccounts.SiteBankID				AS BankID,
			RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.DepositDateKey), 112),101)	AS DepositDate,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.ImmutableDateKey), 112),101)	AS ImmutableDate,
			RecHubData.factChecks.ImmutableDateKey AS PICSDate,
			RecHubData.factChecks.TransactionID,
			RecHubData.factChecks.TxnSequence,
			RecHubData.factChecks.BatchSequence,
			RTRIM(RecHubData.factChecks.RoutingNumber)			AS RT,
			RTRIM(RecHubData.factChecks.Account)				AS Account,
			RTRIM(RecHubData.factChecks.Serial)					AS Serial,
			RTRIM(RecHubData.factChecks.RemitterName)			AS RemitterName,
			RecHubData.factChecks.Amount,
			RecHubData.factChecks.CheckSequence,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode		AS ClientAccountColorMode,
		    RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchSiteCode,
			RecHubData.factChecks.BatchNumber,
			RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType,
			RecHubData.dimBatchSources.ShortName AS PaymentSource,
			RecHubData.dimDDAs.DDA,
			RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
			RecHubData.factChecks.SourceProcessingDateKey
	FROM	
			RecHubData.dimClientAccounts WITH (INDEX(IDX_dimClientAccounts_SiteBankClientAccountID))
			INNER JOIN RecHubData.factChecks ON 
				RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON 
				RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factChecks.BatchPaymentTypeKey
			INNER JOIN RecHubData.dimBatchSources ON 
				RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factChecks.BatchSourceKey			
			INNER JOIN RecHubData.dimImportTypes ON
					RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON		--WI 142847
				RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey    
			LEFT OUTER JOIN RecHubData.dimDDAs ON 
				RecHubData.dimDDAs.DDAKey = RecHubData.factchecks.DDAKey
	WHERE  	
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND  RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.factChecks.DepositDateKey = @EndDateKey	--WI 142835 WI 197810 
			AND RecHubData.factChecks.BatchID = @parmBatchID
			AND RecHubData.factChecks.TransactionID = @parmTransactionID
			AND RecHubData.factChecks.IsDeleted = 0 
	ORDER BY 
			RecHubData.factChecks.BatchID ASC,
			RecHubData.factChecks.BatchSequence ASC,
			RecHubData.factChecks.TxnSequence ASC,
			RecHubData.factChecks.SequenceWithinTransaction ASC
	OPTION ( RECOMPILE );

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH