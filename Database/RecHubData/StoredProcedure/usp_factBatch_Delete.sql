--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatch_Delete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatch_Delete') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatch_Delete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatch_Delete
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmSourceBatchID			BIGINT,	
	@parmImmutableDateKey		INT,
	@parmDepositDateKey         INT,
	@parmBatchSource            VARCHAR(30),
	@parmRowDeleted				BIT = 0 OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 09/09/2013
*
* Purpose: Delete a batch from RecHubData. Please note @parmBatchID is 
*          equivalent to SourceBatchID; i.e. the @parmBatchID value 
*          must equal SourceBatchID value of the batch to be deleted. 
*
* Modification History
* 09/09/2013 WI 112906 JPB	Created
* 04/22/2014 WI 137972 CMC	Update to 2.x release.  Change schema to RecHubData.
* 06/02/2014 WI 142810 DLD  Batch Collision. 
* 02/24/2015 WI 191940 TWE  add @BatchSourceKey lookup, @parmDepositDateKey
* 01/12/2016 WI 252125 JPB	Remove call to deprecated factDataEntrySummary SP.
* 08/04/2017 PT 149496367 JPB	Delete record from exceptions batch table.
* 08/23/2017 PT 150231734 JPB	Delete records from exceptions transaction table.
* 10/23/2017 PT 151667885 MGE	Removed DepositDateKey from where clause because RPS does not pass it.
******************************************************************************************************/
SET NOCOUNT ON;	--WI 142810
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE @BatchID					BIGINT, 
		@BatchSourceKey             SMALLINT,
		@LocalTransaction			BIT, 
		@ModificationDate			DATE,
		/* Error Handling */
		@ErrorMessage				NVARCHAR(4000),
		@ErrorProcedure				NVARCHAR(200),
		@ErrorSeverity				INT,
		@ErrorState					INT,
		@ErrorLine					INT;

BEGIN TRY

	SET @parmRowDeleted = 0;
    /* go find out what the batch source key is */
	SELECT
	   @BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	FROM 
	    RecHubData.dimBatchSources
	WHERE
	    RecHubData.dimBatchSources.ShortName = @parmBatchSource
		AND RecHubData.dimBatchSources.IsActive = 1;

	IF @BatchSourceKey IS NULL 
	BEGIN
		RAISERROR('Batch Source Not Found',16,1);
	END

	/* Find the R360 Batch Id needed to delete a batch from the RecHubData tables. */
	SELECT	
		@BatchID = RecHubData.factBatchSummary.BatchID,	 
		@ModificationDate = GETDATE(),
		@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey
	FROM	
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE	
		RecHubData.dimBanks.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factBatchSummary.DepositDateKey = 
		CASE 
			WHEN @parmDepositDateKey = -1 THEN RecHubData.factBatchSummary.DepositDateKey
			ELSE @parmDepositDateKey
		END
		AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factBatchSummary.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factBatchSummary.BatchSourceKey = @BatchSourceKey; 


	IF @BatchID IS NULL --WI 142810
	BEGIN
		RAISERROR('Batch Not Found',16,1);
	END
	
	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	/* Delete records from the fact tables */
	EXEC RecHubData.usp_factBatchSummary_Upd_IsDeleted 
			@parmDepositDateKey, @BatchID, @ModificationDate, @parmRowDeleted OUT; --WI 142810
	EXEC RecHubData.usp_factTransactionSummary_Upd_IsDeleted 
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factChecks_Upd_IsDeleted
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factDocuments_Upd_IsDeleted
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factStubs_Upd_IsDeleted
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factDataEntryDetails_Upd_IsDeleted
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factBatchData_Upd_IsDeleted
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810
	EXEC RecHubData.usp_factItemData_Upd_IsDeleted 
			@parmDepositDateKey, @BatchID, @ModificationDate;				--WI 142810

	--PT 149496367 no real need for a stored procedure since it is a locking table and always do a hard delete
	DELETE FROM RecHubException.PostDepositBatchExceptions WHERE DepositDateKey = @parmDepositDateKey AND BatchID = @BatchID;
	--PT 150231734 no real need for a stored procedure since it is a locking table and always do a hard delete
	DELETE FROM RecHubException.PostDepositTransactionExceptions WHERE DepositDateKey = @parmDepositDateKey AND BatchID = @BatchID;

	IF @LocalTransaction = 1 
		BEGIN
		COMMIT TRANSACTION;
		SET @parmRowDeleted = 1;
		END

END TRY
BEGIN CATCH
	IF XACT_STATE() != 0 AND @LocalTransaction = 1 
		ROLLBACK TRANSACTION;
	/* Set to false if something bad happened */
	SET @parmRowDeleted = 0;
 	IF @LocalTransaction = 1 
	BEGIN
		EXEC RecHubCommon.usp_WfsRethrowException;
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();
				
		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
END CATCH