--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDataEntrySummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntrySummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntrySummary_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntrySummary_Upd_IsDeleted
(
	@parmDepositDateKey		INT,
	@parmBatchID			BIGINT, --WI 142844
	@parmModificationDate	DATETIME
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/26/2009
*
* Purpose: Deletes factDataEntryDetails.
*
* Modification History
* 03/26/2009 CR 25817 JPB	Created
* 11/02/2008 CR 28042 JPB	Fixed problem with reconsolidation of batches. 
* 12/31/2009 CR 28567 JPB	Performance improvements:
*								Keys passed in
* 01/23/2012 CR 49589 JPB	Added SourceProcessingDateKey.
* 04/24/2013 WI 90645 JPB	Updates for 2.0. Moved to RecHubData schema.
*							Renamed from usp_factDataEntrySummaryDelete.
*							Combined usp_DataImportIntegrationServices_Delete_factDataEntrySummary.
*							Renamed @parmCustomerKey to @parmOrganizationKey.
*							Renamed @parmLockboxKey	to @parmClientAccountKey.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
*							Added @parmModificationDate.
* 06/04/2014 WI 142844 DLD	Batch Collision.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factDataEntrySummary
	SET
		RecHubData.factDataEntrySummary.IsDeleted = 1,
		RecHubData.factDataEntrySummary.ModificationDate = @parmModificationDate		
	WHERE	
		RecHubData.factDataEntrySummary.DepositDateKey	= @parmDepositDateKey
	AND RecHubData.factDataEntrySummary.BatchID			= @parmBatchID
	AND RecHubData.factDataEntrySummary.IsDeleted		= 0
	OPTION( RECOMPILE );
END TRY
BEGIN CATCH
	IF( @@NESTLEVEL > 1 )
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH