--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ClientAccount_Search_ByClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ClientAccount_Search_ByClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ClientAccount_Search_ByClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ClientAccount_Search_ByClientAccountID
(
	@parmSearchRequest	XML,
	@parmSearchTotals	XML OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/22/2010
*
* Purpose: Query Data Entry Details fact table for matching records. Adapted from
*		usp_ClientAccountSearch.
*
* Modification History
* 02/22/2010 CR 29076 JMC	Created
* 03/11/2010 CR 29184 JPB	Added Customer ID to the result set.
* 03/22/2010 CR 29241 JPB	Updated to fix ambiguous column name error.
* 03/26/2010 CR 29256 JNE	Added MinDepositStatus parameter & to where statement.
* 07/06/2010 CR 29963 JPB	Corrected CheckTotal in return XML.
* 07/21/2010 CR 30214 JPB	Removed INNER JOIN to OLTA.OLLockboxes.
* 08/17/2010 CR 30492 JMC   Changed query for bit parameters to look for elements 
*                           instead of attributes.
* 11/04/2010 CR 31770 JNE   Added MarkSense parameter and filter.
* 10/28/2011 CR	47105 JPB	Speed improvements.
* 10/28/2011 CR 47746 JPB	Add WHERE items to result set.
* 09/10/2012 CR 54163 JPB	Added BatchNumber.
* 09/25/2012 CR 55996 JPB	FP lockbox search improvements.
* 12/05/2012 WI 70676 JPB	Too many records inserted into temp dim table.
* 05/23/2013 WI 90759 JBS	Update to 2.0 release. Move to RecHubData schema
*							Renamed proc from usp_LockboxSearchByLockboxID
*							Change references: LockBox to ClientAccount,
*							Customer to Organization, Processing to Immutable
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @CheckSearch			VARCHAR(MAX),
		@StubSearch				VARCHAR(MAX),
		@SelectColumns			VARCHAR(MAX),
		@SQLSearchCommand		VARCHAR(MAX),
		@SQLCommand				VARCHAR(MAX),
		@TransactionTable		VARCHAR(35),
		@iDEColumnCount			INT,
		@bClientAccountCutOff	BIT,
		@dtCurrentProcessingDate DATETIME,
		@iSiteOrganizationID	INT,
		@iMinDepositStatus		INT, -- CR 29256 3/26/2010 JNE
		--options from XML
		@vcDepositDateStart		VARCHAR(32),
		@vcDepositDateEnd		VARCHAR(32),
		@dtDepositDateStart		DATETIME,
		@dtDepositDateEnd		DATETIME,
		@dtDepositDateKeyStart	INT,			
		@dtDepositDateKeyStop	INT,			
		@iBatchIDFrom			INT,
		@iBatchIDTo				INT,
		@iBatchNumberFrom		INT,
		@iBatchNumberTo			INT,
		@vcAmountFrom			VARCHAR(100),
		@vcAmountTo				VARCHAR(100),
		@vcBankID				VARCHAR(15),
		@vcClientAccountID		VARCHAR(15),
		@vcSerialNumber			VARCHAR(30),
		@vcPaginateRS			VARCHAR(5),
		@iPaginateRS			SMALLINT,
		@iRecordsPerPage		INT,
		@iStartRecord			INT,
		@vcSortBy				VARCHAR(512),
		@vcSortDirection		VARCHAR(4),
		@iDisplayScannedChecks	BIT,
		@iDisplayCOTSOnly		BIT,
		@vcDEColumnNames		VARCHAR(MAX),
		@vcDBColumnsWithCast	VARCHAR(MAX), /* new for fixed data typing */
		@DEInSort				BIT, /* CR 55996 JPB 09/25/12 */
		@bUseCutoff				BIT,
		@bViewProcessAhead		BIT,
		--used for pivoting/merging records
		@iProcessDEData			INT,
		@iCurrentCheckRecordID	INT,
		@iCurrentBankKey		INT,
		@iCurrentOrganizationKey	INT,
		@iCurrentClientAccountKey	INT,
		@iCurrentImmutableDateKey	INT,
		@iCurrentDepositDateKey		INT,
		@iCurrentBatchID			INT,
		@iCurrentTransactionID		INT,
		@iDETransactionCounter		INT,
		@iMaxDEInfoItems			INT,
		@iSRRecordCount				INT,
		@iSRDocumentCount			INT,
		@iSRCheckCount				INT,
		@mSRCheckTotal				MONEY,
		@SQLWhereCommand			VARCHAR(MAX),
		@SQLFillTmpDims				VARCHAR(MAX),	-- CR 55996
		@Loop						INT,
		@DEWhereCount				INT,
		@TooManyRows				BIT,
		@factDataEntryDetailsMaxRowCount INT,
		@totalMatchRowCount			BIGINT,
		@totalMatchMaxRowCount		BIGINT,
		@totalMatchWithDEMaxRowCount BIGINT,
		--used to determine 'base' column names
		@vcCheckAmountCN			VARCHAR(30),
		@vcCheckSerialNumberCN		VARCHAR(30),
		@vcCheckAccountNumberCN		VARCHAR(30),
		@vcCheckRTCN				VARCHAR(30),
		@bMarkSense					BIT,			-- CR 31770 11/4/2010 JNE
		@Today						VARCHAR(10)		-- CR 55996

BEGIN TRY

	SET @Today = CONVERT(VARCHAR,GETDATE(),101); -- Function is nondeterministic so save as variable and use variable.
	/* max number rows that will be processed */
	SET @factDataEntryDetailsMaxRowCount = 480000;	--Value has been set to 80 thousand
	SET @totalMatchMaxRowCount = 200000;
	SET @totalMatchWithDEMaxRowCount = 2560000000;  -- Value has been set to 560 million

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResults')) 
		DROP TABLE #tmpDEResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResultsPivot')) 
		DROP TABLE #tmpDEResultsPivot;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotCheckInfo')) 
		DROP TABLE #pivotCheckInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotStubInfo')) 
		DROP TABLE #pivotStubInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFields')) 
		DROP TABLE #DEInfoFields;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEWhere')) 
		DROP TABLE #DEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
		
	--Create a temp table to hold the identifing information for the records that match
	CREATE TABLE #tmpMatchingResults
	(
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		BatchNumber			INT,
		TransactionID		INT,
		TxnSequence			INT,
		CheckCount			INT,
		DocumentCount		INT,
		StubCount			INT,
		OMRCount			INT,
		CheckAmount			MONEY,
		BankID				INT,
		OrganizationID		INT,
		ClientAccountID		INT,
		SerialNumber		VARCHAR(30),
		numeric_serial		BIGINT,
		BatchSequence		INT,
		ChecksCheckSequence INT,
		RT					VARCHAR(30),
		numeric_rt			INT,
		AccountNumber		VARCHAR(30),
--  does this Remitter stuff need to be conditional	?	RemitterKey			INT,
		RemitterName		VARCHAR(60),
		Account				VARCHAR(30),
		RoutingNumber		VARCHAR(30)
	);
		
	CREATE CLUSTERED INDEX IDX_#tmpMatchingResults ON #tmpMatchingResults
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID);

	--Create a temp table to hold the matched data entry records
	CREATE TABLE #tmpDEResults
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT,
		TableName			VARCHAR(36),
		FldName				VARCHAR(32),
		DataType			INT,
		StubsAmountOccured	BIT,
		FldLength			INT,
		DataEntryValue		VARCHAR(80),
		DataEntryColumnKey	INT,
		DataEntryValueMoney MONEY,
		DataEntryValueDateTime DATETIME
	);

	SET @SQLCommand = 'ALTER TABLE #tmpDEResults ADD CONSTRAINT PK_#tmpDEResults_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)';
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#tmpDEResults ON #tmpDEResults
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID);

	--Create a temp table to hold the matched data entry records
	CREATE TABLE #tmpDEResultsPivot
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT
	);

	SET @SQLCommand = 'ALTER TABLE #tmpDEResultsPivot ADD CONSTRAINT PK_#tmpDEResultsPivot_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)';
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#tmpDEResultsPivot ON #tmpDEResultsPivot
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence);

	CREATE TABLE #pivotCheckInfo
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT
	);

	SET @SQLCommand = 'ALTER TABLE #pivotCheckInfo ADD CONSTRAINT PK_#pivotCheckInfo_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)';
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#pivotCheckInfo ON #pivotCheckInfo
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence);

	CREATE TABLE #pivotStubInfo
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		BankKey				INT,
		OrganizationKey		INT,
		ClientAccountKey	INT,
		ImmutableDateKey	INT,
		DepositDateKey		INT,
		BatchID				INT,
		TransactionID		INT,
		BatchSequence		INT
	);

	SET @SQLCommand = 'ALTER TABLE #pivotStubInfo ADD CONSTRAINT PK_#pivotStubInfo_' + REPLACE(CAST(NEWID() AS VARCHAR(36)),'-','_') + ' PRIMARY KEY NONCLUSTERED (RowID)';
	EXEC (@SQLCommand);

	CREATE CLUSTERED INDEX IDX_#pivotStubInfo ON #pivotStubInfo
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence);

	CREATE TABLE #DEInfoFields 
	(
		RowID				INT IDENTITY(1,1) NOT NULL,			
		TableName			VARCHAR(36),
		FldName				VARCHAR(32),
		FinalName			VARCHAR(64),
		DisplayFinal		BIT DEFAULT(1),
		StubsAmountOccured	BIT,
		DataType			SMALLINT,
		TableType			SMALLINT,
		FldLength			SMALLINT,
		DataEntryColumnKey	INT
	);

	CREATE CLUSTERED INDEX IDX_#DEInfoFields ON #DEInfoFields
	(TableName,FldName);
	
	CREATE TABLE #DEInfoFieldsExpanded
	(
		RowID			INT IDENTITY(1,1) NOT NULL,			
		TableName		VARCHAR(36),
		FldName			VARCHAR(32),
		FinalName		VARCHAR(64),
		DisplayFinal	BIT DEFAULT(1),
		DataType		SMALLINT,
		TableType		SMALLINT,
		FldLength		SMALLINT,
		DataEntryColumnKey	INT,
		ClientAccountKey	INT,
		StubsAmountOccured	BIT
	);
	
	CREATE CLUSTERED INDEX IDX_#DEInfoFieldsExpanded ON #DEInfoFieldsExpanded
	(TableName,FldName,DataEntryColumnKey);
	
	CREATE TABLE #DEWhere
	(
		RowID				INT IDENTITY(1,1) NOT NULL,
		TableName			VARCHAR(128),
		ColumnName			VARCHAR(128),
		CombinedName		VARCHAR(256),
		Operator			VARCHAR(32),
		StubsAmountOccured	BIT,
		DataType			INT,
		DataValue			VARCHAR(80)
	);

	CREATE CLUSTERED INDEX IDX_#DEWhere ON #DEWhere
	(CombinedName);

	--Create temp table to store dimenstional data
	CREATE TABLE #tmpDimensions
	(
		SiteBankID			INT,   
		OLClientAccountID	UNIQUEIDENTIFIER,
		OLOrganizationID	UNIQUEIDENTIFIER,
		SiteClientAccountID INT, 
		ClientAccountKey	INT, 
		SiteCodeID			INT,
		LBMostRecent		BIT,
		LBViewingDays		INT,
		CustViewingDays		INT,
		StartDate			DATETIME,
		SiteCurProcDate		DATETIME,
		SiteOrganizationID	INT
	);
		
	SET @vcDEColumnNames = '';
	SET @vcDBColumnsWithCast = '';

	/* CR 55996 JPB 09/25/12 Set DEInSort if de fields in sort clause */
	IF EXISTS( 
				SELECT	1 
				FROM	@parmSearchRequest.nodes('/Root/SortBy') SearchRequest(att) 
				WHERE	SearchRequest.att.value('.','varchar(512)') LIKE 'ChecksDataEntry%' 
						OR SearchRequest.att.value('.','varchar(512)') LIKE 'StubsDataEntry%'
			)
		SET @DEInSort = 1
	ELSE SET @DEInSort = 0;
	/* CR 55996 JPB 09/25/12 Set DEInSort if de fields in sort clause */
	--Get the columns to return, this will be used in the final select statement
	INSERT INTO #DEInfoFields (TableName,FldName,StubsAmountOccured, DataType,TableType)
	SELECT	DISTINCT SearchRequest.att.value('@tablename', 'varchar(128)') AS TableName,
			SearchRequest.att.value('@fieldname', 'varchar(128)') AS FldName,
			CASE 
				WHEN SearchRequest.att.value('@tablename', 'varchar(128)') = 'Stubs' AND SearchRequest.att.value('@fieldname', 'varchar(128)') = 'Amount' THEN 1
				ELSE 0
			END AS StubsAmountOccured,
			SearchRequest.att.value('@datatype', 'int') AS DataType,
			CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(128)'))
				WHEN 'CHECKS' THEN 0
				WHEN 'CHECKSDATAENTRY' THEN 1
				WHEN 'STUBS' THEN 2
				WHEN 'STUBSDATAENTRY' THEN 3
			END AS TableType
	FROM 	@parmSearchRequest.nodes('/Root/SelectFields/field') SearchRequest(att)
	WHERE	UPPER(SearchRequest.att.value('@tablename', 'varchar(128)')) <> 'CHECKS' 
	SET @iDEColumnCount = @@ROWCOUNT;
	
	--add the remitter name from select if it is there
	INSERT INTO #DEInfoFields (TableName,FldName,StubsAmountOccured,DataType,TableType)
	SELECT	DISTINCT SearchRequest.att.value('@tablename', 'varchar(128)') AS TableName,
			SearchRequest.att.value('@fieldname', 'varchar(128)') AS FldName,
			0 AS StubsAmountOccured,
			SearchRequest.att.value('@datatype', 'int') AS DataType,
			CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(128)'))
				WHEN 'CHECKS' THEN 0
				WHEN 'CHECKSDATAENTRY' THEN 1
				WHEN 'STUBS' THEN 2
				WHEN 'STUBSDATAENTRY' THEN 3
			END AS TableType
	FROM 	@parmSearchRequest.nodes('/Root/SelectFields/field') SearchRequest(att)
	WHERE	UPPER(SearchRequest.att.value('@tablename', 'varchar(128)')) = 'CHECKS' 
			--CR 55996 Allow other checks fields the only want you want to exclude is checksequence
			AND UPPER(SearchRequest.att.value('@fieldname', 'varchar(128)')) <> 'CHECKSEQUENCE';
	SET @iDEColumnCount = @iDEColumnCount + @@ROWCOUNT;

	--finally get items in the where clause that are not in the selected fields list, but make sure we do not add any check columns
	;WITH DEFI AS
	(
		SELECT	DISTINCT SearchRequest.att.value('@tablename', 'varchar(128)') AS TableName,
				SearchRequest.att.value('@fieldname', 'varchar(128)') AS FldName,
				CASE 
					WHEN SearchRequest.att.value('@tablename', 'varchar(128)') = 'Stubs' AND SearchRequest.att.value('@fieldname', 'varchar(128)') = 'Amount' THEN 1
					ELSE 0
				END AS StubsAmountOccured,
				SearchRequest.att.value('@datatype', 'int') AS DataType,
				CASE UPPER(SearchRequest.att.value('@tablename', 'varchar(128)'))
					WHEN 'CHECKS' THEN 0
					WHEN 'CHECKSDATAENTRY' THEN 1
					WHEN 'STUBS' THEN 2
					WHEN 'STUBSDATAENTRY' THEN 3
				END AS TableType,
				1 AS DisplayFinal
		FROM 	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
		WHERE	UPPER(SearchRequest.att.value('@tablename', 'varchar(128)')) <> 'CHECKS' 
	)
	INSERT INTO #DEInfoFields (TableName,FldName,DataType,TableType,DisplayFinal)
	SELECT TableName,FldName,DataType,TableType,DisplayFinal 
	FROM DEFI 
	WHERE	NOT EXISTS
			(	
				SELECT	DEInfo.TableName,
						DEInfo.FldName
				FROM	#DEInfoFields DEInfo
				WHERE	DEInfo.TableName = DEFI.TableName
						AND DEInfo.FldName = DEFI.FldName
			);
	SET @iDEColumnCount = @iDEColumnCount + @@ROWCOUNT;

	--setup the where clause table for de fields
	INSERT INTO #DEWhere (TableName,ColumnName,CombinedName,Operator,DataValue,DataType,StubsAmountOccured)
	SELECT SR.TableName,SR.FieldName,SR.TableName+SR.FieldName,SR.Operator,SR.DEValue,SR.DataType, sr.StubsAmountOccured
	FROM	(
			SELECT	SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
					SearchRequest.att.value('@fieldname', 'varchar(128)') AS FieldName,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					CASE 
						WHEN SearchRequest.att.value('@tablename', 'varchar(128)') = 'Stubs' AND SearchRequest.att.value('@fieldname', 'varchar(128)') = 'Amount' THEN 1
						ELSE 0
					END AS StubsAmountOccured
			FROM	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE	SearchRequest.att.value('@tablename', 'varchar(20)') = 'ChecksDataEntry' OR
					SearchRequest.att.value('@tablename', 'varchar(20)') = 'Stubs' OR
					SearchRequest.att.value('@tablename', 'varchar(20)') = 'StubsDataEntry'
		) SR;

	SET @DEWhereCount = @@ROWCOUNT;
	--set the column names that we will use going forward
	IF EXISTS(SELECT 1 FROM @parmSearchRequest.nodes('/Root/SelectFields/field') SearchRequest(att) )
	BEGIN
		SELECT	@vcCheckAmountCN = 'ChecksAmount',
				@vcCheckSerialNumberCN = 'ChecksSerial',
				@vcCheckAccountNumberCN = 'ChecksAccount',
				@vcCheckRTCN = 'ChecksRT';
	END
	ELSE
	BEGIN
		SELECT	@vcCheckAmountCN = 'Amount',
				@vcCheckSerialNumberCN = 'SerialNumber',
				@vcCheckAccountNumberCN = 'AccountNumber',
				@vcCheckRTCN = 'RT';
	END
	
	--Get the filter parameters, this will be used in the search WHERE clause
	SELECT	@vcDepositDateStart = SR.VCDepositDateStart,
			@vcDepositDateEnd = SR.VCDepositDateEnd,
			@dtDepositDateStart = SR.DepositDateStart,
			@dtDepositDateEnd = SR.DepositDateEnd,
			@iBatchIDFrom = SR.BatchIDFrom,
			@iBatchIDTo = SR.BatchIDTo,
			@iBatchNumberFrom = SR.BatchNumberFrom,
			@iBatchNumberTo = SR.BatchNumberTo,
			@vcAmountFrom = SR.AmountFrom,
			@vcAmountTo = SR.AmountTo,
			@vcBankID = SR.BankID,
			@vcClientAccountID = SR.ClientAccountID,
			@vcSerialNumber = SR.SerialNumber,
			@iRecordsPerPage = SR.RecordsPerPage,
			@vcPaginateRS = SR.PaginateRS,
			@iStartRecord = StartRecord,
			@vcSortBy = SortBy,
			@vcSortDirection = SortDirection,
			@iDisplayScannedChecks = DisplayScannedChecks,
			@iDisplayCOTSOnly = DisplayCOTSOnly,
			@bUseCutoff = UseCutoff,
			@bViewProcessAhead = ViewProcessAhead,
			@iMinDepositStatus = MinDepositStatus, -- CR 29256 03/26/10 JNE
			@bMarkSense = MarkSense -- CR 30931 11/4/2010 JNE
	FROM	(
				SELECT	SearchRequest.att.query('DateFrom').value('.', 'varchar(32)') AS VCDepositDateStart,
						SearchRequest.att.query('DateTo').value('.', 'varchar(32)') AS VCDepositDateEnd,
						SearchRequest.att.query('DateFrom').value('.', 'datetime') AS DepositDateStart,
						SearchRequest.att.query('DateTo').value('.', 'datetime') AS DepositDateEnd,
						SearchRequest.att.query('BatchIDFrom').value('.', 'int') AS BatchIDFrom,
						SearchRequest.att.query('BatchIDTo').value('.', 'int') AS BatchIDTo,
						SearchRequest.att.query('BatchNumberFrom').value('.', 'int') AS BatchNumberFrom,
						SearchRequest.att.query('BatchNumberTo').value('.', 'int') AS BatchNumberTo,
						SearchRequest.att.query('AmountFrom').value('.', 'varchar(100)') AS AmountFrom,
						SearchRequest.att.query('AmountTo').value('.', 'varchar(100)') AS AmountTo,
						SearchRequest.att.query('BankID').value('.', 'varchar(15)') AS BankID,
						SearchRequest.att.query('ClientAccountID').value('.', 'varchar(15)') AS ClientAccountID,
						SearchRequest.att.query('Serial').value('.', 'varchar(40)') AS SerialNumber,
						SearchRequest.att.query('RecordsPerPage').value('.','int') AS RecordsPerPage,
						SearchRequest.att.query('PaginateRS').value('.','varchar(5)') AS PaginateRS,
						SearchRequest.att.query('StartRecord').value('.','int') AS StartRecord,
						SearchRequest.att.query('SortBy').value('.', 'varchar(512)') AS SortBy,
						SearchRequest.att.query('SortByDir').value('.','varchar(4)') AS SortDirection, --CR 27954 10/16/09 JPB
						COALESCE(SearchRequest.att.query('DisplayScannedChecks').value('.', 'bit'),1) AS DisplayScannedChecks,
						COALESCE(SearchRequest.att.query('UseCutoff').value('.', 'bit'),0) AS UseCutoff,
						COALESCE(SearchRequest.att.query('ViewProcessAhead').value('.', 'bit'),0) AS ViewProcessAhead,
						CASE
							WHEN UPPER(SearchRequest.att.query('COTSOnly').value('.','VARCHAR(5)')) = 'FALSE' THEN 0
							ELSE 1
						END AS DisplayCOTSOnly,
						SearchRequest.att.query('MinDepositStatus').value('.','int') AS MinDepositStatus, -- CR 29256 03/26/2010 JNE
						COALESCE(SearchRequest.att.query('MarkSense').value('.','bit'),0) AS MarkSense -- CR 31770 11/4/2010 JNE
				FROM	@parmSearchRequest.nodes('/Root') SearchRequest(att)
			) SR;

	--Clean up Batch IDs
	IF @iBatchIDFrom = 0
		SET @iBatchIDFrom = -1;
	IF @iBatchIDTo = 0
		SET @iBatchIDTo = 999999999; 

	--Clean up Batch Numberss
	IF @iBatchNumberFrom = 0
		SET @iBatchNumberFrom = -1;
	IF @iBatchNumberTo = 0
		SET @iBatchNumberTo = 999999999 ;
	
	--Clean up pagition information
	IF @iRecordsPerPage <= 0
		SET @iRecordsPerPage = 999999999
	IF @iStartRecord <= 0
		SET @iStartRecord = 1
	
	--Clean up sort by
	IF CHARINDEX('.',@vcSortBy) > 0
		SET @vcSortBy = REPLACE(@vcSortBy,'.','');
	
	--Cleanup sort direction
	IF LEN(@vcSortDirection) = 0
		SET @vcSortDirection = 'ASC';
		
	--Set pagation information
	IF UPPER(@vcPaginateRS) = 'TRUE' OR @vcPaginateRS = '1'
		SET @iPaginateRS = 1;
	ELSE
		SET @iPaginateRS = 0;
		
	Set @dtDepositDateKeyStart = CONVERT(VARCHAR,(@dtDepositDateStart),112);	--CR 49904
	Set @dtDepositDateKeyStop = CONVERT(VARCHAR,(@dtDepositDateEnd),112);		--CR 49904

	BEGIN TRY

		INSERT into #tmpDimensions 
		(   
			SiteBankID, 
			SiteOrganizationID,
			OLClientAccountID,
			OLOrganizationID,
			SiteClientAccountID, 
			ClientAccountKey, 
			SiteCodeID,
			LBMostRecent,
			LBViewingDays,
			CustViewingDays,
			StartDate,
			SiteCurProcDate
		)
		SELECT OLCA.SiteBankID,
			dimCA.SiteOrganizationID, 
			OLCA.OLClientAccountID,
			OLCA.OLOrganizationID, 
			OLCA.SiteClientAccountID, 
			dimCA.ClientAccountKey, 
			dimCA.SiteCodeID,
			dimCA.MostRecent,
			OLCA.ViewingDays,
			OLOrg.ViewingDays,
			DATEADD(DAY,(-1*(COALESCE(OLCA.ViewingDays,OLOrg.ViewingDays,2557))), @Today),
			CurrentProcessingDate
		FROM 
			RecHubUser.OLClientAccounts OLCA 
			INNER JOIN RechubData.dimClientAccounts AS dimCA 
				ON dimCA.SiteBankID = OLCA.SiteBankID
				AND dimCA.SiteClientAccountID = OLCA.SiteClientAccountID
			INNER JOIN  RecHubUser.OLOrganizations AS OLOrg
				ON OLCA.OLOrganizationID = OLOrg.OLOrganizationID
			INNER JOIN  RecHubData.dimSiteCodes
				ON dimCA.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID 
		WHERE 
			dimCA.SiteBankID = @vcBankID 
			AND dimCA.SiteClientAccountID = @vcClientAccountID;

	END TRY
	BEGIN CATCH
		--error inserting into temp table, stop the process here
		--get the error information so it can be passed to RAISERROR
		--this will pass control to main CATCH block
		DECLARE @ErrorMessageTmpDims    NVARCHAR(4000),
				@ErrorSeverityTmpDims   INT,
				@ErrorStateTmpDims      INT;
		SELECT	@ErrorMessageTmpDims = ERROR_MESSAGE()+ 'Inserting into #tmpDimensions table',
				@ErrorSeverityTmpDims = ERROR_SEVERITY(),
				@ErrorStateTmpDims = ERROR_STATE();
		RAISERROR(@ErrorMessageTmpDims,@ErrorSeverityTmpDims,@ErrorStateTmpDims);
	END CATCH			

	--BEGIN Fix processing dates based on cutoff and view ahead parameters
	SELECT	@bClientAccountCutoff = Cutoff,
			@dtCurrentProcessingDate = CurrentProcessingDate,
			@iSiteOrganizationID = SiteOrganizationID
	FROM	RecHubData.dimClientAccounts
			INNER JOIN RecHubData.dimSiteCodes ON RecHubData.dimSiteCodes.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
	WHERE	RecHubData.dimClientAccounts.SiteBankID = @vcBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @vcClientAccountID;

	IF LEN(@vcDepositDateEnd) = 0
		SET @dtDepositDateEnd = @dtCurrentProcessingDate;
		
	IF @bUseCutoff = 1 AND @bClientAccountCutoff = 1 AND @dtDepositDateEnd > @dtCurrentProcessingDate
		SET @dtDepositDateEnd = @dtCurrentProcessingDate
	ELSE IF @bUseCutoff = 1 AND @bClientAccountCutoff = 0 AND @dtDepositDateEnd >= @dtCurrentProcessingDate
		SET @dtDepositDateEnd = DATEADD(d,-1,@dtCurrentProcessingDate)
	ELSE IF @bViewProcessAhead = 0 AND @dtDepositDateEnd > @dtCurrentProcessingDate
			SET @dtDepositDateEnd = @dtCurrentProcessingDate;
	--END Fix processing dates based on cutoff and view ahead parameters

	--update #DEInfoFields with FldLength									--MGE 3/5/11
	INSERT INTO #DEInfoFieldsExpanded 
	(TableName, FldName, DisplayFinal, DataType, TableType, FldLength, DataEntryColumnKey, StubsAmountOccured)
	SELECT DISTINCT
		DEIF.TableName,
		DEIF.FldName,
		DEIF.DisplayFinal,
		DEIF.DataType,
		DEIF.TableType,
		RecHubData.dimDataEntryColumns.FldLength,
		RecHubData.dimDataEntryColumns.DataEntryColumnKey,
		DEIF.StubsAmountOccured
	FROM
		RecHubData.ClientAccountsDataEntryColumns							--MGE 3/5/12
		INNER JOIN RecHubData.dimDataEntryColumns							--MGE 3/5/12
			ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
		INNER JOIN #DEInfoFields DEIF 
			ON RecHubData.dimDataEntryColumns.TableName = DEIF.TableName	--MGE
			AND RecHubData.dimDataEntryColumns.FldName = DEIF.FldName		--MGE	
			AND RecHubData.dimDataEntryColumns.DataType = DEIF.DataType		--MGE
	WHERE 
		RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey in		--MGE 3/5/12
			(Select ClientAccountKey FROM #tmpDimensions);					--MGE 3/5/12
		 
		 
	--Get check search fields
	SET @CheckSearch = '';
	SELECT @CheckSearch=@CheckSearch + ' RecHubData.factChecks.' + FieldName
			+CASE Operator 
				WHEN 'Equals' THEN '=' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Is Greater Than' THEN ' > ' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Is Less Than' THEN ' < ' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE QUOTENAME(SR.DEValue,CHAR(39)) END
				WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(SR.DEValue+'%',CHAR(39))
				WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue+'%',CHAR(39))
				WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+SR.DEValue,CHAR(39))
				ELSE Operator       
			END+' AND'
	FROM ( 
			SELECT	SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
					SearchRequest.att.value('@fieldname', 'varchar(128)') AS FieldName,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue
			FROM	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE	SearchRequest.att.value('@tablename', 'varchar(20)') = 'Checks'
		   ) SR;
	--Add amounts if defined
	--CR 27952 JPB 10/16/09 Added = to each statement
	IF LEN(@vcAmountFrom) > 0
	BEGIN
		IF LEN(@CheckSearch) > 0
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Amount >= ' + @vcAmountFrom + ' AND'
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Amount >= ' + @vcAmountFrom + ' AND'
	END
	IF LEN(@vcAmountTo) > 0
	BEGIN
		IF LEN(@CheckSearch) > 0
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Amount <= ' + @vcAmountTo + ' AND'
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Amount <= ' + @vcAmountTo + ' AND'
	END
	--Add Check Serial Number if defined
	--Serial Number CR 27953 10/16/09 JPB
	IF LEN(@vcSerialNumber) > 0 
	BEGIN
		IF LEN(@CheckSearch) > 0 --SerialNumber NOT LIKE @vcSerialNumber OR SerialNumber IS NULL
			SET @CheckSearch = @CheckSearch + ' RecHubData.factChecks.Serial LIKE ' + CHAR(39) + '%' + @vcSerialNumber + '%' + CHAR(39) + ' AND'
		ELSE SET @CheckSearch = 'RecHubData.factChecks.Serial LIKE ' + CHAR(39) + '%' + @vcSerialNumber + '%' + CHAR(39) + ' AND'
	END

	--Get stub search fields
	SET @StubSearch = ''
	SELECT @StubSearch=@StubSearch + ' RecHubData.factStubs.' + FieldName
			+CASE Operator 
				WHEN 'Equals' THEN '=' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE CHAR(39)+SR.DEValue+CHAR(39) END
				WHEN 'Is Greater Than' THEN ' > ' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE CHAR(39)+SR.DEValue+CHAR(39) END
				WHEN 'Is Less Than' THEN ' < ' +CASE WHEN FieldName = 'Amount' THEN SR.DEValue ELSE CHAR(39)+SR.DEValue+CHAR(39) END
				WHEN 'Begins With' THEN ' LIKE '+CHAR(39)+SR.DEValue+'%'+CHAR(39)
				WHEN 'Contains' THEN ' LIKE '+CHAR(39)+'%'+SR.DEValue+'%'+CHAR(39)
				WHEN 'Ends With' THEN ' LIKE '+CHAR(39)+'%'+SR.DEValue+CHAR(39)
				ELSE Operator       
			END+' AND'
	FROM ( 
			SELECT	SearchRequest.att.value('@tablename', 'varchar(20)') AS TableName,
					SearchRequest.att.value('@fieldname', 'varchar(128)') AS FieldName,
					SearchRequest.att.value('@datatype', 'int') AS DataType,
					SearchRequest.att.value('@operator', 'varchar(20)') as Operator,
					SearchRequest.att.value('@value', 'varchar(128)') as DEValue
			FROM	@parmSearchRequest.nodes('/Root/WhereClause/field') SearchRequest(att)
			WHERE	SearchRequest.att.value('@tablename', 'varchar(20)') = 'Stubs'
		   ) SR;
	
	--Removing trailing 'AND'
	IF LEN(@CheckSearch) > 0
		SET @CheckSearch = SUBSTRING(@CheckSearch,1,LEN(@CheckSearch)-3);
	IF LEN(@StubSearch) > 0
		SET @StubSearch = SUBSTRING(@StubSearch,1,LEN(@StubSearch)-3);

	--Create base transaction search commnad
	SET @SQLSearchCommand = '
	INSERT INTO #tmpMatchingResults
	(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,BatchNumber,TransactionID,TxnSequence,CheckCount,DocumentCount,StubCount,OMRCount,CheckAmount,
	BankID,OrganizationID,ClientAccountID,SerialNumber,numeric_serial,BatchSequence,ChecksCheckSequence,RemitterName,Account,RoutingNumber)	
			SELECT	DISTINCT RecHubData.factTransactionSummary.BankKey,
					RecHubData.factTransactionSummary.OrganizationKey,
					RecHubData.factTransactionSummary.ClientAccountKey,
					RecHubData.factTransactionSummary.ImmutableDateKey,
					RecHubData.factTransactionSummary.DepositDateKey,
					RecHubData.factTransactionSummary.BatchID,
					RecHubData.factTransactionSummary.BatchNumber,
					RecHubData.factTransactionSummary.TransactionID,
					RecHubData.factTransactionSummary.TxnSequence,
					CheckCount,
					CASE ' + CONVERT(char(1),@iDisplayScannedChecks) + '
						WHEN 0 THEN DocumentCount - ScannedCheckCount
						ELSE DocumentCount
					END AS DocumentCount,
					StubCount,
					OMRCount,
					RecHubData.factChecks.Amount,
					SiteBankID,
					SiteClientAccountID,
					Serial,
					NumericSerial,
					BatchSequence,
					CheckSequence,
					RemitterName,
					Account,
					RoutingNumber
					
			FROM	#tmpDimensions
			
					INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.ClientAccountKey = #tmpDimensions.ClientAccountKey'

					SET @SQLSearchCommand = @SQLSearchCommand + '
					LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.BankKey = RecHubData.factTransactionSummary.BankKey
							AND RecHubData.factChecks.OrganizationKey = RecHubData.factTransactionSummary.OrganizationKey
							AND RecHubData.factChecks.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
							AND RecHubData.factChecks.ImmutableDateKey = RecHubData.factTransactionSummary.ImmutableDateKey
							AND RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
							AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
							AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID';
							
			SET @SQLSearchCommand = @SQLSearchCommand + '
			WHERE	#tmpDimensions.SiteBankID = ' + @vcBankID + '
				AND #tmpDimensions.SiteClientAccountID = ' + @vcClientAccountID + '
				AND RecHubData.factTransactionSummary.DepositDateKey >= ' + CONVERT(VARCHAR,(@dtDepositDateStart),112) + '
				AND RecHubData.factTransactionSummary.DepositDateKey <= ' + CONVERT(VARCHAR,(@dtDepositDateEnd),112) + '
				AND RecHubData.factTransactionSummary.BatchID >= ' + CONVERT(VARCHAR,@iBatchIDFrom) + '
				AND RecHubData.factTransactionSummary.BatchID <= ' + CONVERT(VARCHAR,@iBatchIDTo) + '
				AND RecHubData.factTransactionSummary.DepositStatus >= ' + CONVERT(VARCHAR,@iMinDepositStatus) +	-- CR 29256 3/26/2010 JNE
				' AND RecHubData.factTransactionSummary.OMRCount >= '+ CONVERT(VARCHAR,@bMarkSense);				-- CR 31770 11/4/2010 JNE
		
		IF LEN(@CheckSearch) > 0
			SET @SQLSearchCommand = @SQLSearchCommand + '
			AND ' + @CheckSearch;
		
		SET @SQLSearchCommand = @SQLSearchCommand + '
		OPTION (MAXDOP 1)';

	
	EXEC(@SQLSearchCommand);
	SET @totalMatchRowCount = @@ROWCOUNT;
	SET @iProcessDEData = 0	;				--MGE 3/20/2012 for case where there is no filter
	--If matching records where found, build the final select statement
	IF @totalMatchRowCount > 0 
	BEGIN /* Records found */
		--Get data entry columns if they were requested
		IF @iDEColumnCount > 0
		BEGIN /* @iDEColumnCount > 0 */
			--Store the DE Results so we can join them to the final result set
			--I.E. get all of the de column data, still in the vertical view
			IF( @DEWhereCount > 0 )
			BEGIN
				SET @SQLCommand = ''
				BEGIN
					SELECT	@SQLCommand=@SQLCommand + '			
					SELECT	RecHubData.factDataEntryDetails.BankKey,
							RecHubData.factDataEntryDetails.OrganizationKey,
							RecHubData.factDataEntryDetails.ClientAccountKey,
							RecHubData.factDataEntryDetails.ImmutableDateKey,
							RecHubData.factDataEntryDetails.DepositDateKey,
							RecHubData.factDataEntryDetails.BatchID,
							RecHubData.factDataEntryDetails.TransactionID,
							RecHubData.factDataEntryDetails.BatchSequence,
							DEIF.TableName,
							DEIF.FldName,
							DEIF.DataType,
							DEIF.StubsAmountOccured,
							DEIF.FldLength,
							RecHubData.factDataEntryDetails.DataEntryValue,
							RecHubData.factDataEntryDetails.DataEntryColumnKey,
							RecHubData.factDataEntryDetails.DataEntryValueMoney,
							RecHubData.factDataEntryDetails.DataEntryValueDateTime
					FROM	#DEInfoFieldsExpanded DEIF
							INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.DataEntryColumnKey = DEIF.DataEntryColumnKey
							INNER JOIN #tmpMatchingResults  SR
									ON RecHubData.factDataEntryDetails.BankKey = SR.BankKey 
									AND RecHubData.factDataEntryDetails.OrganizationKey = SR.OrganizationKey
									AND RecHubData.factDataEntryDetails.ClientAccountKey = SR.ClientAccountKey
									AND RecHubData.factDataEntryDetails.ImmutableDateKey = SR.ImmutableDateKey
									AND RecHubData.factDataEntryDetails.DepositDateKey = SR.DepositDateKey
									AND RecHubData.factDataEntryDetails.BatchID = SR.BatchID
									AND RecHubData.factDataEntryDetails.TransactionID = SR.TransactionID
									AND RecHubData.factDataEntryDetails.DepositDateKey >= ' + CAST(@dtDepositDateKeyStart AS VARCHAR(8)) +'
									AND RecHubData.factDataEntryDetails.DepositDateKey <= ' + CAST(@dtDepositDateKeyStop AS VARCHAR(8)) +'
							INNER JOIN #DEWhere ON DEIF.TableName = #DEWhere.TableName
									AND DEIF.FldName = #DEWhere.ColumnName
									AND RecHubData.factDataEntryDetails.'
							+CASE Operator
                						WHEN 'Equals' THEN 
											+CASE DataType 
												WHEN 1 THEN 'DataEntryValue ' 
												WHEN 6 THEN 
													CASE StubsAmountOccured 
														WHEN 1 THEN 'DataEntryValueMoney '
														ELSE  'DataEntryValueFloat '
													END
												WHEN 7 THEN 'DataEntryValueMoney ' 
												WHEN 11 THEN 'DataEntryValueDateTime' 
												ELSE 'DataEntryValue '
											END + ' = ' 
                							+CASE DataType 
                								WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
                								WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
                								ELSE DataValue 
                							END /* Can be numeric data types, but alpha/date still need ticks */
										WHEN 'Is Greater Than' THEN 
											+CASE DataType 
												WHEN 1 THEN 'DataEntryValue ' 
												WHEN 6 THEN 
													CASE StubsAmountOccured 
														WHEN 1  THEN 'DataEntryValueMoney '
														ELSE  'DataEntryValueFloat '
													END
												WHEN 7 THEN 'DataEntryValueMoney ' 
												WHEN 11 THEN 'DataEntryValueDateTime' 
												ELSE 'DataEntryValue '
											END + ' > ' 
											+CASE DataType 
												WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
												WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
												ELSE DataValue 
											END /* Can be numeric data types, but alpha/date still need ticks */
										WHEN 'Is Less Than' THEN 
											+CASE DataType 
												WHEN 1 THEN 'DataEntryValue ' 
												WHEN 6 THEN 
													CASE StubsAmountOccured 
														WHEN 1 THEN 'DataEntryValueMoney '
														ELSE  'DataEntryValueFloat '
													END
												WHEN 7 THEN 'DataEntryValueMoney ' 
												WHEN 11 THEN 'DataEntryValueDateTime' 
												ELSE 'DataEntryValue '
											END + ' < ' 
											+CASE DataType 
												WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
												WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
												ELSE DataValue 
											END /* Can be numeric data types, but alpha/date still need ticks */
										WHEN 'Begins With' THEN 'DataEntryValue LIKE '+QUOTENAME(DataValue+'%',CHAR(39))
										WHEN 'Contains' THEN 'DataEntryValue LIKE '+QUOTENAME('%'+DataValue+'%',CHAR(39))
										WHEN 'Ends With' THEN 'DataEntryValue LIKE '+QUOTENAME('%'+DataValue,CHAR(39))
								END
						FROM	#DEWhere;
				END

				SET @SQLCommand = @SQLCommand + '
				 ORDER BY RecHubData.factDataEntryDetails.BankKey,
						RecHubData.factDataEntryDetails.OrganizationKey,
						RecHubData.factDataEntryDetails.ClientAccountKey,
						RecHubData.factDataEntryDetails.DepositDateKey,
						RecHubData.factDataEntryDetails.BatchID,
						RecHubData.factDataEntryDetails.TransactionID';


				INSERT INTO #tmpDEResults	
				(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,
				BatchID,TransactionID,BatchSequence,TableName,FldName,DataType,FldLength, StubsAmountOccured,
				DataEntryValue,OLTA.factDataEntryDetails.DataEntryColumnKey, DataEntryValueMoney,	DataEntryValueDateTime)
				EXEC(@SQLCommand);					
				SET @iProcessDEData = @@ROWCOUNT;

				;with tmpDistinctRows AS
				(select Distinct 
				#tmpDEResults.BankKey,
				#tmpDEResults.OrganizationKey,
				#tmpDEResults.ClientAccountKey,
				#tmpDEResults.ImmutableDateKey,
				#tmpDEResults.DepositDateKey,
				#tmpDEResults.BatchID,
				#tmpDEResults.TransactionID
				--#tmpDEResults.BatchSequence
				From #tmpDEResults
				)
				Insert into #tmpDEResults
				(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence,
				TableName,FldName,DataType,FldLength,
				DataEntryValue, DataEntryColumnKey, DataEntryValueMoney, DataEntryValueDateTime)
				SELECT tDR.BankKey,
					tDR.OrganizationKey,
					tDR.ClientAccountKey,
					tDR.ImmutableDateKey,
					tDR.DepositDateKey,
					tDR.BatchID,
					tDR.TransactionID,
					RecHubData.factDataEntryDetails.BatchSequence,
					DEIF.TableName,
					DEIF.FldName,
					DEIF.DataType,
					DEIF.FldLength,
					RecHubData.factDataEntryDetails.DataEntryValue,
					RecHubData.factDataEntryDetails.DataEntryColumnKey,
					RecHubData.factDataEntryDetails.DataEntryValueMoney,
					RecHubData.factDataEntryDetails.DataEntryValueDateTime 
				FROM 
					RecHubData.factDataEntryDetails
					INNER JOIN   tmpDistinctRows tDR 
						ON RecHubData.factDataEntryDetails.BankKey = tDR.BankKey 
						AND RecHubData.factDataEntryDetails.OrganizationKey = tDR.OrganizationKey
						AND RecHubData.factDataEntryDetails.ClientAccountKey = tDR.ClientAccountKey
						AND RecHubData.factDataEntryDetails.ImmutableDateKey = tDR.ImmutableDateKey
						AND RecHubData.factDataEntryDetails.DepositDateKey = tDR.DepositDateKey
						AND RecHubData.factDataEntryDetails.BatchID = tDR.BatchID
						AND RecHubData.factDataEntryDetails.TransactionID = tDR.TransactionID
						AND RecHubData.factDataEntryDetails.DepositDateKey >= CAST(@dtDepositDateKeyStart AS varchar(8))
						AND RecHubData.factDataEntryDetails.DepositDateKey <= CAST(@dtDepositDateKeyStop AS varchar(8))
					--	AND RecHubData.factDataEntryDetails.BatchSequence = tDR.BatchSequence						
					Inner Join #DEInfoFieldsExpanded DEIF 
						ON RecHubData.factDataEntryDetails.DataEntryColumnKey = DEIF.DataEntryColumnKey
				WHERE DEIF.FldName not in (SELECT Distinct ColumnName FROM #DEWhere);
--select * from #tmpDEResults									--Debug ONLY						
			END
			ELSE  --@DEWhereCount
			BEGIN
				SELECT @totalMatchRowCount = @totalMatchRowCount * SUM(StubCount) FROM #tmpMatchingResults;
				SELECT @totalMatchRowCount = @totalMatchRowCount * COUNT(*) FROM #DEInfoFields;
				IF( (SELECT COUNT(*) FROM #DEInfoFields) > 1 )
					SET @totalMatchRowCount = (@totalMatchRowCount * 10) * 1.9;
				IF( @totalMatchRowCount < @totalMatchWithDEMaxRowCount )
				BEGIN
					SET @SQLCommand = ''
					SELECT	@SQLCommand=@SQLCommand + '	
					SELECT	RecHubData.factDataEntryDetails.BankKey,
							RecHubData.factDataEntryDetails.OrganizationKey,
							RecHubData.factDataEntryDetails.ClientAccountKey,
							RecHubData.factDataEntryDetails.ImmutableDateKey,
							RecHubData.factDataEntryDetails.DepositDateKey,
							RecHubData.factDataEntryDetails.BatchID,
							RecHubData.factDataEntryDetails.TransactionID,
							RecHubData.factDataEntryDetails.BatchSequence,
							DEIF.TableName,
							DEIF.FldName,
							DEIF.DataType,
							DEIF.FldLength,
							RecHubData.factDataEntryDetails.DataEntryValue,
							RecHubData.factDataEntryDetails.DataEntryColumnKey,
							RecHubData.factDataEntryDetails.DataEntryValueMoney,
							RecHubData.factDataEntryDetails.DataEntryValueDateTime
					FROM	#tmpMatchingResults
							INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.BankKey = #tmpMatchingResults.BankKey 
									AND RecHubData.factDataEntryDetails.OrganizationKey = #tmpMatchingResults.OrganizationKey
									AND RecHubData.factDataEntryDetails.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
									AND RecHubData.factDataEntryDetails.ImmutableDateKey = #tmpMatchingResults.ImmutableDateKey
									AND RecHubData.factDataEntryDetails.DepositDateKey = #tmpMatchingResults.DepositDateKey
									AND RecHubData.factDataEntryDetails.BatchID = #tmpMatchingResults.BatchID
									AND RecHubData.factDataEntryDetails.TransactionID = #tmpMatchingResults.TransactionID
									AND RecHubData.factDataEntryDetails.DepositDateKey >= ' + CAST(@dtDepositDateKeyStart AS VARCHAR(8)) +'
									AND RecHubData.factDataEntryDetails.DepositDateKey <= ' + CAST(@dtDepositDateKeyStop AS VARCHAR(8)) +'		 
							INNER JOIN #DEInfoFieldsExpanded DEIF ON RecHubData.factDataEntryDetails.DataEntryColumnKey = DEIF.DataEntryColumnKey';
					SET @SQLSearchCommand = @SQLSearchCommand + '
						OPTION (RECOMPILE, MAXDOP 1)';	
								
					INSERT INTO #tmpDEResults	
					(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,BatchSequence,TableName,FldName,DataType,FldLength,DataEntryValue,
					DataEntryColumnKey,DataEntryValueMoney,DataEntryValueDateTime)
					EXEC (@SQLCommand);
					SET @iProcessDEData = @@ROWCOUNT;
				END
				ELSE /* Total number of rows to process is too large, so set the count greater then max + 1 */ 
					SET @iProcessDEData = @factDataEntryDetailsMaxRowCount + 1;
			END

			/* continue on only if we ended up with data entry rows to handle */
			
			IF( (@iProcessDEData > 0 AND @iProcessDEData < @factDataEntryDetailsMaxRowCount) OR @DEInSort = 1 OR @DEWhereCount > 0 ) 
			BEGIN /* IF( @iProcessDEData > 0 AND @iProcessDEData < @factDataEntryDetailsMaxRowCount ) */
				SET @TooManyRows = 0;
				--create the alter statement and get a list of the columns for the final select statement
				SET @SQLCommand = 'ALTER TABLE #tmpDEResultsPivot Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.TableName + SDEI.FldName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(' + CAST(SDEI.FldLength AS VARCHAR(16)) + ') NULL'
							WHEN 6 THEN
								CASE StubsAmountOccured 
									WHEN 1 THEN ' MONEY NULL'
									ELSE  ' FLOAT NULL'
								END 
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName, 
									MAX(RecHubData.dimDataEntryColumns.DataType) AS DataType, 
									MAX(RecHubData.dimDataEntryColumns.FldLength) AS FldLength,
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFields DEI
									INNER JOIN RecHubData.dimDataEntryColumns 
										ON RecHubData.dimDataEntryColumns.TableName = DEI.TableName AND RecHubData.dimDataEntryColumns.FldName = DEI.FldName
							WHERE	UPPER(RecHubData.dimDataEntryColumns.FldName) <> 'CHECKSEQUENCE'
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.StubsAmountOccured
						) SDEI;
						
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);

					--now alter the temp table
					EXEC(@SQLCommand);

					--set the column names ordered by table, column name for the final result set
					SET @vcDEColumnNames = '';
					SET @vcDBColumnsWithCast = '';
					SELECT	@vcDEColumnNames = @vcDEColumnNames + TableName + FldName + ', ',
							@vcDBColumnsWithCast = @vcDBColumnsWithCast 
							+ CASE DataType 
								WHEN 6 THEN 
									CASE StubsAmountOccured 
									WHEN 1 THEN  'CAST(' + TableName + FldName + ' AS MONEY)' 
									ELSE  'CAST(' + TableName + FldName + ' AS FLOAT)' 
								END  
								WHEN 7 THEN 'CAST(' + TableName + FldName + ' AS MONEY)' 
								WHEN 11 THEN 'CONVERT(VARCHAR(80),CAST(' + TableName + FldName +' AS DATETIME),101)'
								ELSE TableName + FldName
							END
							+ ' AS ' + TableName + FldName + ','
					FROM	#DEInfoFields
					WHERE	TableType IN (1,2,3)
							AND DisplayFinal = 1
							OR (TableType = 0 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
					ORDER BY TableName,FldName;

					--clean up the DE column list
					IF LEN(@vcDEColumnNames) > 0
						SET @vcDEColumnNames = SUBSTRING(@vcDEColumnNames,1,LEN(@vcDEColumnNames)-1);
					IF LEN(@vcDBColumnsWithCast) > 0
						SET @vcDBColumnsWithCast = SUBSTRING(@vcDBColumnsWithCast,1,LEN(@vcDBColumnsWithCast)-1);
				END	

				/* Alter the Check/Stub pivot tables to match the final restults */
				SET @SQLCommand = 'ALTER TABLE #pivotCheckInfo Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.TableName + SDEI.FldName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(' + CAST(SDEI.FldLength AS VARCHAR(16)) + ') NULL'
							WHEN 6 THEN 
									CASE SDEI.StubsAmountOccured 
												WHEN 1 THEN 'MONEY NULL '
												ELSE  ' FLOAT NULL '
									END
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName, 
									MAX(RecHubData.dimDataEntryColumns.DataType) AS DataType, 
									MAX(RecHubData.dimDataEntryColumns.FldLength) AS FldLength,
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFields DEI
									INNER JOIN RecHubData.dimDataEntryColumns 
										ON RecHubData.dimDataEntryColumns.TableName = DEI.TableName AND RecHubData.dimDataEntryColumns.FldName = DEI.FldName
							WHERE	RecHubData.dimDataEntryColumns.TableType IN (0,1)
									AND UPPER(RecHubData.dimDataEntryColumns.FldName) <> 'CHECKSEQUENCE'
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.StubsAmountOccured
						) SDEI;
						
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
					--now alter the temp table
					EXEC(@SQLCommand) 
				END
				
				SET @SQLCommand = 'ALTER TABLE #pivotStubInfo Add ';
				SELECT	@SQLCommand = @SQLCommand + SDEI.TableName + SDEI.FldName 
						+ CASE DataType
							WHEN 1 THEN ' VARCHAR(' + CAST(SDEI.FldLength AS VARCHAR(16)) + ') NULL'
							WHEN 6 THEN 
									CASE SDEI.StubsAmountOccured 
												WHEN 1 THEN 'MONEY NULL '
												ELSE  ' FLOAT NULL '
									END
							WHEN 7 THEN ' MONEY NULL'
							WHEN 11 THEN ' DATETIME'
						END
						+ ','
				FROM	(
							SELECT	DISTINCT DEI.TableName AS TableName, 
									DEI.FldName AS FldName, 
									MAX(RecHubData.dimDataEntryColumns.DataType) AS DataType, 
									MAX(RecHubData.dimDataEntryColumns.FldLength) AS FldLength,
									DEI.StubsAmountOccured AS StubsAmountOccured
							FROM	#DEInfoFields DEI
									INNER JOIN RecHubData.dimDataEntryColumns 
										ON RecHubData.dimDataEntryColumns.TableName = DEI.TableName AND RecHubData.dimDataEntryColumns.FldName = DEI.FldName
							WHERE	RecHubData.dimDataEntryColumns.TableType IN (2,3)
							GROUP BY 
									DEI.TableName,
									DEI.FldName,
									DEI.StubsAmountOccured
						) SDEI;
						
				IF @@ROWCOUNT > 0
				BEGIN
					--clean up the SQL command
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
					--now alter the temp table
					EXEC(@SQLCommand);
				END
				/* Now pivot the data entry information into the correct temp table. */
				SET @SQLCommand = 'INSERT INTO #pivotCheckInfo(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (0,1)
							AND UPPER(DEI.FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL')
				)
				SELECT	@SQLCommand = @SQLCommand + TableName+FldName + ','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + '
						BatchSequence)';
				
				SET @SQLCommand = @SQLCommand + '
				SELECT 	#tmpDEResults.BankKey,
						#tmpDEResults.OrganizationKey,
						#tmpDEResults.ClientAccountKey,
						#tmpDEResults.ImmutableDateKey,
						#tmpDEResults.DepositDateKey,
						#tmpDEResults.BatchID,
						#tmpDEResults.TransactionID,';

				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (0,1)
							AND UPPER(DEI.FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL')
				)
				SELECT  @SQLCommand = @SQLCommand + 
						' MIN(CASE WHEN RecHubData.dimDataEntryColumns.TableName='+QUOTENAME(TableName,CHAR(39))+' AND RecHubData.dimDataEntryColumns.FldName='+QUOTENAME(FldName,CHAR(39))+' THEN '
							 +CASE DataType
								WHEN 1 THEN '#tmpDEResults.DataEntryValue '
								WHEN 6 THEN 
									CASE StubsAmountOccured 
												WHEN 1 THEN '#tmpDEResults.DataEntryValueMoney '
												ELSE  'CAST(#tmpDEResults.DataEntryValue AS FLOAT) '
									END
								
								WHEN 7 THEN '#tmpDEResults.DataEntryValueMoney '
								WHEN 11 THEN '#tmpDEResults.DataEntryValueDateTime '
							END+'ELSE NULL END) AS  '+TableName+FldName+','
				FROM DBColumns;
				--IF LEN(@SQLCommand) > 0				--MGE 3/5/2011	
				--	SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)	--MGE 3/5/2011
				--Added DepositDateKey filter below									--MGE 3/9/2011
				SELECT  @SQLCommand = @SQLCommand +  'MIN(#tmpDEResults.BatchSequence)
				 FROM	#tmpDEResults
						
						INNER JOIN RecHubData.dimDataEntryColumns ON #tmpDEResults.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
							AND RecHubData.dimDataEntryColumns.TableType IN (0,1)
				GROUP BY #tmpDEResults.BankKey,#tmpDEResults.OrganizationKey,#tmpDEResults.ClientAccountKey,#tmpDEResults.ImmutableDateKey,#tmpDEResults.DepositDateKey,#tmpDEResults.BatchID,#tmpDEResults.TransactionID
				';		
				SET @SQLSearchCommand = @SQLSearchCommand + '				
				OPTION (RECOMPILE, MAXDOP 1)';											--MGE Prevent Parallelism 3/19/12

				EXEC(@SQLCommand);

				SET @SQLCommand = 'INSERT INTO #pivotStubInfo(BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.DataType AS DataType
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (2,3)
				)
				SELECT	@SQLCommand = @SQLCommand + TableName+FldName + ','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + '
						BatchSequence)';
				
				SET @SQLCommand = @SQLCommand + '
				SELECT 	#tmpDEResults.BankKey,
						#tmpDEResults.OrganizationKey,
						#tmpDEResults.ClientAccountKey,
						#tmpDEResults.ImmutableDateKey,
						#tmpDEResults.DepositDateKey,
						#tmpDEResults.BatchID,
						#tmpDEResults.TransactionID,';

				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName,
							DEI.DataType AS DataType,
							DEI.StubsAmountOccured AS StubsAmountOccured
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (2,3)
				)
				SELECT  @SQLCommand = @SQLCommand + 
						'MIN(CASE WHEN RecHubData.dimDataEntryColumns.TableName='+QUOTENAME(TableName,CHAR(39))+' AND RecHubData.dimDataEntryColumns.FldName='+QUOTENAME(FldName,CHAR(39))+' THEN '
							 +CASE DataType
								WHEN 1 THEN '#tmpDEResults.DataEntryValue '
								WHEN 6 THEN 
									CASE StubsAmountOccured 
												WHEN 1 THEN '#tmpDEResults.DataEntryValueMoney '
												ELSE  'CAST(#tmpDEResults.DataEntryValue AS FLOAT) '
									END
								WHEN 7 THEN '#tmpDEResults.DataEntryValueMoney '
								WHEN 11 THEN '#tmpDEResults.DataEntryValueDateTime '
							END+'ELSE NULL END) AS  '+TableName+FldName+','
				FROM DBColumns;
							
				SELECT  @SQLCommand = @SQLCommand +  '
						#tmpDEResults.BatchSequence
				FROM	#tmpDEResults
						
						INNER JOIN RecHubData.dimDataEntryColumns ON #tmpDEResults.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
							AND RecHubData.dimDataEntryColumns.TableType IN (2,3)
				GROUP BY #tmpDEResults.BankKey,#tmpDEResults.OrganizationKey,#tmpDEResults.ClientAccountKey,#tmpDEResults.ImmutableDateKey,#tmpDEResults.DepositDateKey,#tmpDEResults.BatchID,#tmpDEResults.TransactionID,#tmpDEResults.BatchSequence
				';
				
				SET @SQLSearchCommand = @SQLSearchCommand + '				
				OPTION (MAXDOP 1)';											--MGE Prevent Parallelism 3/19/12
				
				EXEC(@SQLCommand);
				SET @iDETransactionCounter = @iDETransactionCounter + 1;

				SET @SQLCommand = '
				INSERT INTO #tmpDEResultsPivot (BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID,';
				
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (1,2,3)
							OR (TableType = 0 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
				)
				SELECT @SQLCommand = @SQLCommand + TableName+FldName+','
				FROM DBColumns;
				
				SET @SQLCommand = @SQLCommand + 'BatchSequence)
				SELECT	COALESCE(#pivotCheckInfo.BankKey,#pivotStubInfo.BankKey) AS BankKey,
						COALESCE(#pivotCheckInfo.OrganizationKey,#pivotStubInfo.OrganizationKey) AS OrganizationKey,
						COALESCE(#pivotCheckInfo.ClientAccountKey,#pivotStubInfo.ClientAccountKey) AS ClientAccountKey,
						COALESCE(#pivotCheckInfo.ImmutableDateKey,#pivotStubInfo.ImmutableDateKey) AS ImmutableDateKey,
						COALESCE(#pivotCheckInfo.DepositDateKey,#pivotStubInfo.DepositDateKey) AS DepositDateKey,
						COALESCE(#pivotCheckInfo.BatchID,#pivotStubInfo.BatchID) AS BatchID,
						COALESCE(#pivotCheckInfo.TransactionID,#pivotStubInfo.TransactionID) AS TransactionID,';
						
				;WITH DBColumns AS
				(
					SELECT	DISTINCT DEI.TableName AS TableName, 
							DEI.FldName AS FldName
					FROM	#DEInfoFields DEI
					WHERE	TableType IN (1,2,3)
							OR (TableType = 0 AND UPPER(FldName) NOT IN ('AMOUNT','ACCOUNT','RT','SERIAL'))
				)
				SELECT @SQLCommand = @SQLCommand + 
					+CASE UPPER(TableName)
						WHEN 'CHECKS' THEN '#pivotCheckInfo.'
						WHEN 'CHECKSDATAENTRY' THEN '#pivotCheckInfo.'
						WHEN 'STUBS' THEN '#pivotStubInfo.'
						WHEN 'STUBSDATAENTRY' THEN '#pivotStubInfo.'
					END+TableName+FldName+','
				FROM DBColumns;
						
				SET @SQLCommand = @SQLCommand + '
						COALESCE(#pivotCheckInfo.TransactionID,#pivotStubInfo.BatchSequence) AS BatchSequence
				FROM #pivotCheckInfo
					FULL OUTER JOIN #pivotStubInfo ON #pivotStubInfo.BankKey = #pivotCheckInfo.BankKey
						AND #pivotStubInfo.OrganizationKey = #pivotCheckInfo.OrganizationKey
						AND #pivotStubInfo.ClientAccountKey = #pivotCheckInfo.ClientAccountKey
						AND #pivotStubInfo.ImmutableDateKey = #pivotCheckInfo.ImmutableDateKey
						AND #pivotStubInfo.DepositDateKey = #pivotCheckInfo.DepositDateKey
						AND #pivotStubInfo.BatchID = #pivotCheckInfo.BatchID
						AND #pivotStubInfo.TransactionID = #pivotCheckInfo.TransactionID';
				SET @SQLSearchCommand = @SQLSearchCommand + '				
				OPTION (MAXDOP 1)';											--MGE Prevent Parallelism 3/19/12
				EXEC(@SQLCommand);
				
				/*	When muliple items are in the where, we have to do a AND. The process of creating the temp
					table is an OR. Delete all records that do not match the actual request. Only do the next step
					if there are more then 1 item in the where. 
				*/
				IF( @DEWhereCount > 1 )
				BEGIN
					SET @SQLCommand = '';
					SELECT	@SQLCommand=@SQLCommand + CombinedName
							+CASE Operator
								WHEN 'Equals' THEN ' = ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Is Greater Than' THEN ' > ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Is Less Than' THEN ' < ' 
									+CASE DataType 
										WHEN 1 THEN QUOTENAME(DataValue,CHAR(39)) 
										WHEN 11 THEN QUOTENAME(CONVERT(VARCHAR(80),CAST(DataValue AS DATETIME),112),CHAR(39))
										ELSE DataValue 
									END /* Can be numeric data types, but alpha/date still need ticks */
								WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(DataValue+'%',CHAR(39))
								WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+DataValue+'%',CHAR(39))
								WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+DataValue,CHAR(39))
							END+ ' AND '
					FROM #DEWhere;
					IF LEN(@SQLCommand) > 0
						SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4);

					SET @SQLCommand = 'DELETE FROM #tmpDEResultsPivot WHERE RowID NOT IN (SELECT RowID FROM #tmpDEResultsPivot WHERE ' + @SQLCommand + ')';
					EXEC(@SQLCommand);
				END
				--remove any none matching records from the #tmpMatchingResults table since we do a left join to this table later
				/* only remove if there were DE items included in the WHERE clause */
				IF( @DEWhereCount > 0 ) 
					DELETE 
					FROM	#tmpMatchingResults 
					WHERE	NOT EXISTS
							(
								SELECT	BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID
								FROM	#tmpDEResultsPivot
								WHERE	#tmpMatchingResults.BankKey = #tmpDEResultsPivot.BankKey
										AND #tmpMatchingResults.OrganizationKey = #tmpDEResultsPivot.OrganizationKey
										AND #tmpMatchingResults.ClientAccountKey = #tmpDEResultsPivot.ClientAccountKey
										AND #tmpMatchingResults.ImmutableDateKey = #tmpDEResultsPivot.ImmutableDateKey
										AND #tmpMatchingResults.DepositDateKey = #tmpDEResultsPivot.DepositDateKey
										AND #tmpMatchingResults.BatchID = #tmpDEResultsPivot.BatchID
										AND #tmpMatchingResults.TransactionID = #tmpDEResultsPivot.TransactionID
							);
			END	/* IF( @iProcessDEData > 0 AND @iProcessDEData < @factDataEntryDetailsMaxRowCount ) */
			ELSE
			BEGIN 
				IF( @iProcessDEData >= @factDataEntryDetailsMaxRowCount )
					SET @TooManyRows = 1;
				ELSE
				BEGIN
					/* remove any none matching records from the #tmpMatchingResults table since we do a left join to this table later */
					/* but not if there are no rows in #tmpMatchingResults                                       */
					IF @iProcessDEData > 0
					BEGIN
					DELETE 
					FROM	#tmpMatchingResults 
					WHERE	NOT EXISTS
							(
								SELECT	BankKey,OrganizationKey,ClientAccountKey,ImmutableDateKey,DepositDateKey,BatchID,TransactionID
								FROM	#tmpDEResultsPivot
								WHERE	#tmpMatchingResults.BankKey = #tmpDEResultsPivot.BankKey
										AND #tmpMatchingResults.OrganizationKey = #tmpDEResultsPivot.OrganizationKey
										AND #tmpMatchingResults.ClientAccountKey = #tmpDEResultsPivot.ClientAccountKey
										AND #tmpMatchingResults.ImmutableDateKey = #tmpDEResultsPivot.ImmutableDateKey
										AND #tmpMatchingResults.DepositDateKey = #tmpDEResultsPivot.DepositDateKey
										AND #tmpMatchingResults.BatchID = #tmpDEResultsPivot.BatchID
										AND #tmpMatchingResults.TransactionID = #tmpDEResultsPivot.TransactionID
							);
					END
				END
			END 
		END /* @iDEColumnCount > 0 */
		/* Check the total number of rows that are in the matching result set */
		IF( (SELECT COUNT(*) FROM #tmpMatchingResults) > @totalMatchMaxRowCount )
			SET @TooManyRows = 1;
		
		IF( @TooManyRows = 1 )
		BEGIN
			SELECT @iSRRecordCount = -99, @iSRDocumentCount = -99, @iSRCheckCount = -99, @mSRCheckTotal = 0.00;
		END
		ELSE
		BEGIN		
			--This is the total information before the result sets are joined
			SELECT	@iSRDocumentCount = SUM(COALESCE(DocumentCount,0)),
					@iSRCheckCount = SUM(COALESCE(CheckCount,0)),
					@mSRCheckTotal = SUM(COALESCE(CheckAmount,0.00)) --CR 29963
			FROM	#tmpMatchingResults;

			--the 'filters' need applied here so we get the correct counts.
			--COTS
			IF @iDisplayCOTSOnly = 1
				DELETE FROM #tmpMatchingResults WHERE CheckCount <> 0;
				
			--this is the total number of available to the user
			SELECT	@iSRRecordCount = COUNT(*) 
			FROM	#tmpMatchingResults
					LEFT JOIN #tmpDEResultsPivot ON 
							#tmpDEResultsPivot.BankKey = #tmpMatchingResults.BankKey
							AND #tmpDEResultsPivot.OrganizationKey = #tmpMatchingResults.OrganizationKey
							AND #tmpDEResultsPivot.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
							AND #tmpDEResultsPivot.ImmutableDateKey = #tmpMatchingResults.ImmutableDateKey
							AND #tmpDEResultsPivot.DepositDateKey = #tmpMatchingResults.DepositDateKey
							AND #tmpDEResultsPivot.BatchID = #tmpMatchingResults.BatchID
							AND #tmpDEResultsPivot.TransactionID = #tmpMatchingResults.TransactionID;

			--Use a CTE to create a sorted record set with server side paging
			--Adapted from http://www.sqlservercentral.com/articles/Advanced+Querying/3181/

			SET @SQLCommand = ';with FinalResultSet AS ( SELECT ';
			IF LEN(@vcSortBy) > 0
			BEGIN 
				IF UPPER(@vcSortBy) = 'BATCHDEPOSITDATE' -- some of the columns will be 'special' and have to be accounted for
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.DepositDateKey '+@vcSortDirection+' ) AS RecID,';
				ELSE IF @vcSortBy = 'BATCHBATCHID'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.BatchID '+@vcSortDirection+') AS RecID,';
				ELSE IF @vcSortBy = 'BATCHBATCHNUMBER'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.BatchNumber '+@vcSortDirection+') AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'TRANSACTIONSTRANSACTIONID'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.TransactionID '+@vcSortDirection+' ) AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'TRANSACTIONSTXNSEQUENCE'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY TxnSequence '+@vcSortDirection+' ) AS RecID,';
				ELSE IF UPPER(@vcSortBy) = 'CHECKSAMOUNT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY CheckAmount ' + @vcSortDirection+' ) AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSSERIAL'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY numeric_serial '+@vcSortDirection+') AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSRT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY numeric_rt '+@vcSortDirection+') AS RecID,';
				ELSE IF @vcSortBy = 'CHECKSACCOUNT'
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY AccountNumber ' + @vcSortDirection+') AS RecID,';
				ELSE
					SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY '+@vcSortBy+' '+@vcSortDirection+' ) AS RecID,';
			END
			ELSE 
			BEGIN
				SET @SQLCommand = @SQLCommand + 'ROW_NUMBER() OVER (ORDER BY #tmpMatchingResults.BankKey,
														#tmpMatchingResults.OrganizationKey,
														#tmpMatchingResults.ClientAccountKey,
														#tmpMatchingResults.DepositDateKey,
														#tmpMatchingResults.ImmutableDateKey,
														#tmpMatchingResults.BatchID,
														#tmpMatchingResults.TransactionID '+@vcSortDirection+') AS RecID,';
			END

			SET @SQLCommand = @SQLCommand + 
				'	BankID,
					OrganizationID,
					ClientAccountID,		
					CONVERT(varchar(10),#tmpMatchingResults.DepositDateKey, 101) AS Deposit_Date,
					#tmpMatchingResults.BatchID,
					#tmpMatchingResults.BatchNumber,
					#tmpMatchingResults.ImmutableDateKey AS PICSDate,
					#tmpMatchingResults.TransactionID,
					#tmpMatchingResults.TxnSequence,
					CheckAmount,
					SerialNumber,
					numeric_serial,
					AccountNumber,
					RT,
					numeric_rt,
					#tmpMatchingResults.BatchSequence,
					CheckCount,
					DocumentCount,
					StubCount,
					#tmpMatchingResults.OMRCount AS MarkSenseDocumentCount,
					ChecksCheckSequence,
					#tmpMatchingResults.BankKey,
					#tmpMatchingResults.OrganizationKey,
					#tmpMatchingResults.ClientAccountKey,
					#tmpMatchingResults.ImmutableDateKey,
					#tmpMatchingResults.DepositDateKey ';
			
			IF LEN(@vcDEColumnNames) > 0
				SET @SQLCommand = @SQLCommand + ', ' + @vcDEColumnNames;
				
			SET @SQLCommand = @SQLCommand + ' FROM #tmpMatchingResults ';
			
			SET @SQLCommand = @SQLCommand + 'LEFT JOIN #tmpDEResultsPivot ON #tmpDEResultsPivot.BankKey = #tmpMatchingResults.BankKey
					AND #tmpDEResultsPivot.OrganizationKey = #tmpMatchingResults.OrganizationKey
					AND #tmpDEResultsPivot.ClientAccountKey = #tmpMatchingResults.ClientAccountKey
					AND #tmpDEResultsPivot.ImmutableDateKey = #tmpMatchingResults.ImmutableDateKey
					AND #tmpDEResultsPivot.DepositDateKey = #tmpMatchingResults.DepositDateKey
					AND #tmpDEResultsPivot.BatchID = #tmpMatchingResults.BatchID
					AND #tmpDEResultsPivot.TransactionID = #tmpMatchingResults.TransactionID';
			
			SET @SQLCommand = @SQLCommand + ') SELECT BankID,
					OrganizationID,
					ClientAccountID,		
					CONVERT(varchar(10),CAST(CONVERT(varchar(10),Deposit_Date, 101) AS DATETIME),101) AS Deposit_Date, 	
					FinalResultSet.BatchID,
					FinalResultSet.BatchNumber,
					PICSDate,
					FinalResultSet.TransactionID,
					FinalResultSet.TxnSequence,
					CheckAmount AS ' + @vcCheckAmountCN + ',
					SerialNumber AS ' + @vcCheckSerialNumberCN + ',
					numeric_serial,
					RecHubData.factChecks.Account AS ' + @vcCheckAccountNumberCN + ',
					RecHubData.factChecks.RoutingNumber AS ' + @vcCheckRTCN + ',
					RecHubData.factChecks.NumericRoutingNumber AS numeric_rt,
					FinalResultSet.BatchSequence,
					CheckCount,
					DocumentCount,
					StubCount,
					MarkSenseDocumentCount,
					ChecksCheckSequence,
					ImageInfoXML.value(' + CHAR(39) + '(Images/Image/@FileSize)[1]' + CHAR(39) + ', ' + CHAR(39) + 'int' + CHAR(39) + ') AS ImageSize ';
			
			IF LEN(@vcDBColumnsWithCast) > 0
				SET @SQLCommand = @SQLCommand + ', ' + @vcDBColumnsWithCast;
			SET @SQLCommand = @SQLCommand + ' FROM FinalResultSet ';
			SET @SQLCommand = @SQLCommand + ' LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.BankKey = FinalResultSet.BankKey
								AND RecHubData.factChecks.OrganizationKey = FinalResultSet.OrganizationKey
								AND RecHubData.factChecks.ClientAccountKey = FinalResultSet.ClientAccountKey
								AND RecHubData.factChecks.ImmutableDateKey = FinalResultSet.ImmutableDateKey
								AND RecHubData.factChecks.DepositDateKey = FinalResultSet.DepositDateKey
								AND RecHubData.factChecks.BatchID = FinalResultSet.BatchID
								AND RecHubData.factChecks.TransactionID = FinalResultSet.TransactionID 
								AND (RecHubData.factChecks.CheckSequence = FinalResultSet.ChecksCheckSequence 
									OR FinalResultSet.ChecksCheckSequence is Null)';
		
			IF @iPaginateRS = 0
				SET @SQLCommand = @SQLCommand + 'WHERE RecID >= ' + CAST(@iStartRecord AS VARCHAR(10));
			ELSE SET @SQLCommand = @SQLCommand + 'WHERE RecID BETWEEN ' + CAST(@iStartRecord AS VARCHAR(10)) + ' AND ' + CAST((@iStartRecord+@iRecordsPerPage-1) AS VARCHAR(10));
			SET @SQLCommand = @SQLCommand + ' ORDER BY RecID';
			EXEC(@SQLCommand);
			IF @@ROWCOUNT = 0 -- no rows are actually returned, so 0 out the values
				SELECT @iSRRecordCount = 0, @iSRDocumentCount = 0, @iSRCheckCount = 0, @mSRCheckTotal = 0.00;
		END
		--final info needed for the return XML
		SET @parmSearchTotals = (SELECT	'Results' AS "@Name",
				@iSRRecordCount AS "@TotalRecords",
				@iSRDocumentCount AS "@DocumentCount",
				@iSRCheckCount AS "@CheckCount",
				@mSRCheckTotal AS "@CheckTotal",
				@iSiteOrganizationID AS "OrganizationID"
		FOR XML PATH('RecordSet'), ROOT('Page') );
	END	/* Records found */
	ELSE
	BEGIN /* No records found */
		SELECT @iSRRecordCount = 0, @iSRDocumentCount = 0, @iSRCheckCount = 0, @mSRCheckTotal = 0.00;
		SET @parmSearchTotals = (SELECT	'Results' AS "@Name",
				@iSRRecordCount AS "@TotalRecords",
				@iSRDocumentCount AS "@DocumentCount",
				@iSRCheckCount AS "@CheckCount",
				@mSRCheckTotal AS "@CheckTotal",
				@iSiteOrganizationID AS "OrganizationID"
		FOR XML PATH('RecordSet'), ROOT('Page') );
	END
	--Drop the temp tables
	DROP TABLE #tmpMatchingResults;
	DROP TABLE #tmpDEResults;
	DROP TABLE #tmpDEResultsPivot;
	DROP TABLE #pivotCheckInfo;
	DROP TABLE #pivotStubInfo;
	DROP TABLE #DEInfoFields;
	DROP TABLE #DEWhere;
	DROP TABLE #tmpDimensions;
END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDEResults')) 
		DROP TABLE #tmpDEResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotCheckInfo')) 
		DROP TABLE #pivotCheckInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#pivotStubInfo')) 
		DROP TABLE #pivotStubInfo;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEInfoFields')) 
		DROP TABLE #DEInfoFields;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEWhere')) 
		DROP TABLE #DEWhere;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
       
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
