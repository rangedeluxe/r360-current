--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_ByClientAccountKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_ByClientAccountKey') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByClientAccountKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByClientAccountKey
(
	@parmClientAccountKey	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/14/2014
*
* Purpose: Select the Data Entry Columns associated with a given workgroup
*
* Modification History
* 04/14/2014 WI 135761 KLC	Created
* 03/19/2015 WI 196673 BLR  No longer filtering by Session Entitlements,
*                           we'll be doing this after the call.
* 08/18/2015 WI 230198 MAA  Updated to support dimWorkgroupDataEntryColumns
* 08/20/2015 WI 231287 MAA  Added UI Label, Active, tableType and paymentsource fields
* 08/26/2015 WI 232090 MAA  now returns batchsourceKey
* 12/04/2015 WI 250566 BLR  Added batch source short name.
* 12/07/2016 #127604133 JAW Added IsRequired column.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @siteBankID				INT = NULL,
			@siteClientAccountID	INT = NULL;

	SELECT	@siteBankID = RecHubData.dimClientAccounts.SiteBankID,
			@siteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
	FROM RecHubData.dimClientAccounts
	WHERE RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;

	IF @@ROWCOUNT > 0
	BEGIN
		WITH CTE_AssignedDataEntryColumns AS
		(
			SELECT	ROW_NUMBER() OVER ( PARTITION BY RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey ORDER BY RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID DESC ) AS RowNumber,
					RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey

			FROM  RecHubData.dimWorkgroupDataEntryColumns
			WHERE RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = @siteBankID
				AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = @siteClientAccountID
		)	
		SELECT  RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey as DataEntryColumnKey,
				RecHubData.dimWorkgroupDataEntryColumns.DataType,			
				RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder,
				CASE
				WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 THEN 'Checks'
				ELSE 'Stubs'
				END AS 'TableName',
				RecHubData.dimWorkgroupDataEntryColumns.FieldName as FldName,
				RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName as  DisplayName,
				RecHubData.dimWorkgroupDataEntryColumns.MarkSense,
				RechubData.dimWorkgroupDataEntryColumns.IsActive,
				RechubData.dimWorkgroupDataEntryColumns.IsRequired,
				RechubData.dimWorkgroupDataEntryColumns.UILabel,
				RechubData.dimBatchSources.LongName as PaymentSource,
				RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
				RecHubData.dimWorkgroupDataEntryColumns.IsCheck as TableType,
				RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey
		FROM   CTE_AssignedDataEntryColumns
				INNER JOIN RecHubData.dimWorkgroupDataEntryColumns 
		ON CTE_AssignedDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
				INNER JOIN RecHubData.dimBatchSources 
		ON dimWorkgroupDataEntryColumns.BatchSourceKey = dimBatchSources.BatchSourceKey
		WHERE CTE_AssignedDataEntryColumns.RowNumber = 1
		ORDER BY RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder ASC;
	END

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

