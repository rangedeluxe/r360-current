--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_API_dimClientAccounts_Get_BySiteClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_API_dimClientAccounts_Get_BySiteClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_API_dimClientAccounts_Get_BySiteClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_API_dimClientAccounts_Get_BySiteClientAccountID
(
	@parmSiteBankID			INT,
	@parmSiteClientAccount	INT,
	@parmRowsReturned		INT OUTPUT,
	@parmErrorDescription	VARCHAR(2000) OUTPUT,
	@parmSQLErrorNumber		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/22/2009
*
* Purpose: Get Client Accounts
*		   
*
* Modification History
* 04/22/2009 CR 25817 JNE	Created
* 03/26/2013 WI 90549 JPB	Updated for 2.0.
*							Renamed and moved to RecHubData schema.
*							Renamed @parmBankID to @parmSiteBankID.
*							Renamed @parmLockboxID to @parmSiteClientAccountID.
******************************************************************************/

SET NOCOUNT ON;

SET @parmRowsReturned = 0;
SET @parmErrorDescription = '';
SET @parmSQLErrorNumber = 0;

BEGIN TRY
	SELECT	
		RecHubData.dimClientAccounts.SiteBankID AS BankID,
		RecHubData.dimClientAccounts.SiteClientAccountID AS ClientAccountID,
		RecHubData.dimClientAccounts.SiteOrganizationID AS OrganizationID,
		RecHubData.dimClientAccounts.LongName,
		RecHubData.dimClientAccounts.ShortName,
		RecHubData.dimClientAccounts.IsActive
	FROM	
		RecHubData.dimClientAccounts
	WHERE	
		RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccount
	ORDER BY 
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.dimClientAccounts.SiteOrganizationID,
		RecHubData.dimClientAccounts.LongName,
		RecHubData.dimClientAccounts.ShortName;

	SELECT @parmRowsReturned = @@ROWCOUNT;
	RETURN 0;
END TRY
BEGIN CATCH
	SELECT @parmSQLErrorNumber = @@ERROR;
	SET @parmErrorDescription = 'An error occured in the execution of usp_API_dimClientAccounts_Get_BySiteClientAccountID';
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 1;
END CATCH
