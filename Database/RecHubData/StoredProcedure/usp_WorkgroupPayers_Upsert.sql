--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_WorkgroupPayers_UpSert
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_WorkgroupPayers_UpSert') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_WorkgroupPayers_UpSert
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_WorkgroupPayers_UpSert
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,	
	@parmUserId					INT,
	@parmUserName				NVARCHAR(128),
	@parmRoutingNumber			NVARCHAR(30),
	@parmAccount				NVARCHAR(30),
	@parmPayerName				NVARCHAR(128),
	@parmIsDefault				BIT,					-- or DefaultRemitter
	@parmWorkgroupPayerKey		INT	OUTPUT
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 04/24/2019
*
* Purpose: Upserts a new WorkgroupRemitters record
*
* Modification History
* 04/24/2016 R360-15309 MGE	Created
* 05/06/2019 R360-15311 CMC Changed to upsert
* 05/24/2019 CES-4331   MSV Corrected to update "IsDefault" on update
* 05/29/2019 R360-16502 Removed the error code
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	

	DECLARE @errorDescription VARCHAR(1024);
	DECLARE @auditMessage VARCHAR(MAX);
	DECLARE @curTime DATETIME = GETDATE();
    DECLARE @oldWorkgroupPayerName VARCHAR(128);

	-- Verify User ID
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert RecHubData.WorkgroupPayers record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Verify the WorkgroupRemitters row doesn't already exist, if it does update it
	IF EXISTS(SELECT 1 FROM RecHubData.WorkgroupPayers WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID 
			AND RoutingNumber = @parmRoutingNumber AND Account = @parmAccount)
	BEGIN
		IF @@TRANCOUNT = 0 
			BEGIN 
				BEGIN TRANSACTION;
				SET @LocalTransaction = 1;
			END

			SELECT 	@parmWorkgroupPayerKey = WorkgroupPayerKey,
                    @oldWorkgroupPayerName = PayerName
			FROM 	RecHubData.WorkgroupPayers 
			WHERE 	SiteBankID = @parmSiteBankID 
			AND 	SiteClientAccountID = @parmSiteClientAccountID 
			AND 	RoutingNumber = @parmRoutingNumber 
			AND 	Account = @parmAccount

			UPDATE 	RecHubData.WorkgroupPayers
			SET 	PayerName = LTRIM(RTRIM(@parmPayerName)),
					IsDefault = @parmIsDefault,
					ModificationDate = @curTime,
					ModifiedBy = LTRIM(RTRIM(@parmUserName))
			WHERE 	WorkgroupPayerKey = @parmWorkgroupPayerKey

			-- Audit the RecHubData.WorkgroupRemitters update
			SET @auditMessage = 'Updated workgroup payer name for BankId=' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ', WorkgroupId=' + CAST(@parmSiteClientAccountID AS VARCHAR(30))
                + ', Routing Number=' + @parmRoutingNumber
                + ', Account=' + @parmAccount
				+ ' from "' + @oldWorkgroupPayerName + '"'
                + ' to "' + @parmPayerName + '"'
				+ '.';

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubData.usp_WorkgroupPayers_UpSert',
					@parmSchemaName			= 'RecHubData',
					@parmTableName			= 'WorkgroupPayers',
					@parmColumnName			= 'WorkgroupPayerKey',
					@parmAuditValue			= @parmWorkgroupPayerKey,
					@parmAuditType			= 'UPD',
					@parmAuditMessage		= @auditMessage;

			IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
	ELSE
	-- Insert the WorkgroupRemitters record
		BEGIN
			
			IF @@TRANCOUNT = 0 
				BEGIN 
					BEGIN TRANSACTION;
					SET @LocalTransaction = 1;
				END
			INSERT INTO RecHubData.WorkgroupPayers(SiteBankID, SiteClientAccountID, RoutingNumber, Account, PayerName, IsDefault, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
			VALUES (@parmSiteBankID, @parmSiteClientAccountID, LTRIM(RTRIM(@parmRoutingNumber)), LTRIM(RTRIM(@parmAccount)), LTRIM(RTRIM(@parmPayerName)), @parmIsDefault, @curTime, @curTime, LTRIM(RTRIM(@parmUserName)), @parmUserName);
			SET @parmWorkgroupPayerKey = SCOPE_IDENTITY();

			-- Audit the RecHubData.WorkgroupRemitters insert
			SET @auditMessage = 'Added workgroup payer for BankId=' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ', WorkgroupId=' + CAST(@parmSiteClientAccountID AS VARCHAR(30))
                + ', Routing Number=' + @parmRoutingNumber
                + ', Account=' + @parmAccount
				+ ', Payer Name=' + @parmPayerName
				+ '.';

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubData.usp_WorkgroupPayers_UpSert',
					@parmSchemaName			= 'RecHubData',
					@parmTableName			= 'WorkgroupPayers',
					@parmColumnName			= 'WorkgroupPayerKey',
					@parmAuditValue			= @parmWorkgroupPayerKey,
					@parmAuditType			= 'INS',
					@parmAuditMessage		= @auditMessage;

			IF @LocalTransaction = 1 COMMIT TRANSACTION;
		END
END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH