--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimSiteCodes_Upd_CurrentProcessingDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 07/15/2010
*
* Purpose: In an RPS-only environment, since we don't use Consolidation, this 
*          script is required to advance the Current Processing Date.  This 
*          custom script should be run using SQL Agent at exactly midnight 
*          every morning to ensure that the CurrentProcessingDate is always 
*          up-to-date.  Without this, new data entering the system will not be 
*          viewable Online.
*		   
*
* Modification History
* 07/15/2010 CR 30242 JMC	Created
* 02/29/2012 CR 50465 JPB	Only update OLTA records when 
*							AutoUpdateCurrentProcessingDate is 1.
* 04/17/2013 WI 90798 JBS	Update to 2.0 relesase. Change schema to RecHubData
*							Rename proc from usp_UpdateCurrentProcessingDate.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;
DECLARE @bLocalTransaction	BIT,
		@ErrorMessage		NVARCHAR(4000),
		@ErrorSeverity		INT,
		@ErrorState			INT,
		@ErrorLine			INT;
		
BEGIN TRY

	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1
	END;

	--BEGIN TRY
	--	UPDATE 	dbo.SiteCodes 
	--	SET 	CurrentProcessingDate = CAST(CONVERT(varchar, GetDate(), 101) AS DateTime),
	--			ModifiedBy = SUSER_SNAME(),
	--			ModificationDate = GETDATE()
	--END TRY
	--BEGIN CATCH
	--	SELECT	@ErrorMessage = ERROR_MESSAGE(),
	--			@ErrorSeverity = ERROR_SEVERITY(),
	--			@ErrorState = ERROR_STATE(),
	--			@ErrorLine = ERROR_LINE()
	--	SET @ErrorMessage = 'usp_UpdateCurrentProcessingDate (Line: %d)-> ' + @ErrorMessage
	--	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	--END CATCH
	BEGIN TRY
		UPDATE 	RecHubData.dimSiteCodes 
		SET 	RecHubData.dimSiteCodes.CurrentProcessingDate = CAST(CONVERT(VARCHAR, GETDATE(), 101) AS DATETIME),
				RecHubData.dimSiteCodes.ModificationDate = GETDATE()
		WHERE	RecHubData.dimSiteCodes.AutoUpdateCurrentProcessingDate = 1;
	END TRY
	BEGIN CATCH
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE()
		SET @ErrorMessage = 'usp_UpdateCurrentProcessingDate (Line: %d)-> ' + @ErrorMessage
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END CATCH
	
	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0
	END
END TRY

BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0
	END
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

