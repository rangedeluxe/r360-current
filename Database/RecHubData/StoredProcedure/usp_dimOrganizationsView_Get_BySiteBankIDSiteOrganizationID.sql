--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimOrganizationsView_Get_BySiteBankIDSiteOrganizationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimOrganizationsView_Get_BySiteBankIDSiteOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimOrganizationsView_Get_BySiteBankIDSiteOrganizationID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimOrganizationsView_Get_BySiteBankIDSiteOrganizationID 
(
	@parmSiteBankID			INT,
	@parmSiteOrganizationID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: Retrieve rows from RecHubData.dimOrganizationsView
*
* Modification History
* 01/15/2010 CR 28715 JMC	Created
* 08/32/2011 CR 46387 JMC	Joined dimBanks and Added BankName to recordset.
* 04/12/2013 WI 90622 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_dimCustomers_GetByID
*							Change references: Customer to Organization.
*							Change Parameters: @parmSiteCustomerID to @parmSiteOrganizationID.
*							Removed: factDocuments.GlobalDocumentID
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		RecHubData.dimBanksView.SiteBankID					AS BankID,
		RecHubData.dimBanksView.BankName, 
		RecHubData.dimOrganizationsView.SiteOrganizationID	AS OrganizationID, 
		RTRIM(RecHubData.dimOrganizationsView.Name)			AS Name, 
		RecHubData.dimOrganizationsView.[Description]
	FROM 
		RecHubData.dimOrganizationsView
		INNER JOIN RecHubData.dimBanksView 
			ON RecHubData.dimOrganizationsView.SiteBankID = RecHubData.dimBanksView.SiteBankID
	WHERE 
		RecHubData.dimOrganizationsView.SiteBankID = @parmSiteBankID 
		AND RecHubData.dimOrganizationsView.SiteOrganizationID = @parmSiteOrganizationID;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

