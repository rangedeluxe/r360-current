--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_DocumentCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_DocumentCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_DocumentCount
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_DocumentCount 
(
		@parmSessionID			UNIQUEIDENTIFIER,
		@parmSiteBankID			INT,
		@parmSiteClientAccountID	INT,
		@parmDepositDateStart		DATETIME,
		@parmDepositDateEnd		DATETIME,
		@parmDisplayScannedCheck	BIT,
		@parmPaymentTypeKey		INT = NULL,
		@parmPaymentSourceKey		INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table for Check Count for a specified 
*		   DepositDate.
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created.
* 03/28/2013 WI 90681 JBS	Updated to 2.0 release.  Change Schema name.
*							Rename proc from usp_GetDocumentCount.
*							Change references from Lockbox to ClientAccount.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID
*							Pulled date conversion out of where clause.
* 10/10/2013 WI	116708 EAS  Add Payment Type Filter to Document Count.
* 05/22/2014 WI 142823 PKW  Update for RAAM Integration.
* 12/09/2014 WI 181269 LA   Added Batch Payment Source Parameter
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
		@StartDate	INT,												--WI 142823
		@EndDate	INT;

BEGIN TRY
	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDate OUT,							--WI 142823		
		@parmEndDateKey = @EndDate OUT;								--WI 142823

	SELECT 
		CASE 
			WHEN @parmDisplayScannedCheck > 0 THEN SUM(RecHubData.factBatchSummary.DocumentCount)
			ELSE SUM(RecHubData.factBatchSummary.DocumentCount - RecHubData.factBatchSummary.ScannedCheckCount)
		END AS DocumentCount
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON	--WI 142823
			RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE  	
			RecHubUser.SessionClientAccountEntitlements.SessionID  = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
        AND RecHubData.factBatchSummary.DepositDateKey >= @StartDate --WI 142823
		AND RecHubData.factBatchSummary.DepositDateKey <= @EndDate	 --WI 142823
		AND RecHubData.factBatchSummary.DepositStatus >= 850
		AND RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.factBatchSummary.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeKey, RecHubData.factBatchSummary.BatchPaymentTypeKey)
		AND RecHubData.factBatchSummary.BatchSourceKey = ISNULL(@parmPaymentSourceKey, RecHubData.factBatchSummary.BatchSourceKey)
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
