--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Get_ByFI
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Get_ByFI') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Get_ByFI
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Get_ByFI(
	@parmFIEntityID		INT
	)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/26/2014
*
* Purpose: Reads all records from the RecHubData.dimBanks table that match the
*			provided FI Entity ID
*
* Modification History
* 06/26/2014 WI 151427 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	RecHubData.dimBanksView.BankKey,
			RecHubData.dimBanksView.SiteBankID,
			RecHubData.dimBanksView.BankName,
			RecHubData.dimBanksView.EntityID
	FROM	RecHubData.dimBanksView
	WHERE
		RecHubData.dimBanksView.EntityID = @parmFIEntityID;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

