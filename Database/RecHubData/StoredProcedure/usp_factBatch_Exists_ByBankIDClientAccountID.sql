--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatch_Exists_ByBankIDClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatch_Exists_ByBankIDClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatch_Exists_ByBankIDClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatch_Exists_ByBankIDClientAccountID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	NVARCHAR(12),
	@parmBatchExists			BIT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 09/23/2016
*
* Purpose: Test for the existance of a batch to know if we can delete setup data
*
* Modification History
* 09/23/2016 PT #129555329 JBS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/* Set the default value to not exists */
	SET @parmBatchExists = 0;

---- Checking the Batchsummary table and the factnotifications tables to determine if any related data exists for 
---- the BankID and ClientAccountID  so we can delete the Dimension data associated to these. 

	SELECT TOP 1
		@parmBatchExists = 1
	FROM
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
	WHERE
		RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = 
			CASE @parmSiteClientAccountID
				WHEN 'ALL'  THEN RecHubData.dimClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END;

	IF @parmBatchExists = 0
		SELECT TOP 1
			@parmBatchExists = 1
		FROM
			RecHubData.factNotifications
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotifications.ClientAccountKey
		WHERE
			RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = 
			CASE @parmSiteClientAccountID
				WHEN 'ALL'  THEN RecHubData.dimClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END;

	IF @parmBatchExists = 0
		SELECT TOP 1
			@parmBatchExists = 1
		FROM
			RecHubData.factNotificationFiles
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey
		WHERE
			RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = 
			CASE @parmSiteClientAccountID
				WHEN 'ALL'  THEN RecHubData.dimClientAccounts.SiteClientAccountID
			ELSE 
				@parmSiteClientAccountID
			END;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH