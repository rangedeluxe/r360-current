--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factItemData_CheckExists
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factItemData_CheckExists') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factItemData_CheckExists
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factItemData_CheckExists
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT, 
	@parmImmutableDateKey		INT,
	@parmBatchID				INT,
	@parmBatchSequence			INT,
	@parmItemFound				BIT = 0 OUT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/10/2011
*
* Purpose: Checks for the existance of a factItemData.
*
* Modification History
* 03/10/2011 CR 33232 WJS	Created
* 04/14/2011 CR 33232 WJS	Change to use Id instead of keys
* 05/17/2011 CR 34373 WJS	Added bankKey
* 03/08/2012 CR 51051 WJS	Removed join from dimLockboxes, instead use lockboxkey and bankKey
* 04/08/2013 WI 90653 JBS	Update to 2.0 release.  Change Schema name to RecHubData
*							Rename proc from usp_factItemDataExists
*							Change all references from Lockbox to ClientAccount,
*							Processing to Immutable.
*							Change parameters: @parmSiteLockBoxID to @parmSiteClientAccountID
*							@parmProcessingDateKey to @parmImmutableDateKey
*							Added:  IsDeleted = 0
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @BankKey			INT;
DECLARE @ClientAccountKey	INT;
DECLARE @DepositDateKey		INT;

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
            @parmSiteBankID = @parmSiteBankID,
            @parmSiteClientAccountID = @parmSiteClientAccountID,
            @parmBatchID = @parmBatchID,
            @parmImmutableDateKey = @parmImmutableDateKey,
            @parmBankKey = @BankKey						OUTPUT,
            @parmClientAccountKey = @ClientAccountKey	OUTPUT,
            @parmDepositDateKey = @DepositDateKey		OUTPUT;	

	IF EXISTS( SELECT 1	
				FROM	RecHubData.factItemData
				WHERE   RecHubData.factItemData.ClientAccountKey = @ClientAccountKey
						AND RecHubData.factItemData.BankKey = @BankKey
						AND RecHubData.factItemData.ImmutableDateKey = @parmImmutableDateKey
						AND RecHubData.factItemData.DepositDateKey = @DepositDateKey
						AND RecHubData.factItemData.BatchID = @parmBatchID
						AND RecHubData.factItemData.BatchSequence = @parmBatchSequence
						AND RecHubData.factItemData.IsDeleted = 0)
		BEGIN
			SET @parmItemFound = 1;
		END	
	ELSE
		BEGIN
			SET @parmItemFound = 0;	
		END
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
