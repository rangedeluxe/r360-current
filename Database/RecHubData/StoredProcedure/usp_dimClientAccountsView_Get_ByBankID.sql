--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccountsView_Get_ByBankID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccountsView_Get_ByBankID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByBankID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByBankID 
(
    @parmBankID				INT,
    @parmOLOrganizationID   UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/27/2012
*
* Purpose: Retrieve ClientAccount information for a bank
*
* Modification History
* 12/27/2012 WI 86239 TWE   Created by converting embedded SQL
*					  JBS	Update to 2.0 release. Change schema name.
*							Changed all references to LockBox to ClientAccount,
*							Customer to Organization.							
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    ;WITH cte_AssignedClientAccounts AS
    (
    SELECT 
        RecHubUser.OLClientAccounts.SiteClientAccountID,
        1      AS Assigned
    FROM 
        RecHubUser.OLClientAccounts 
    WHERE 
        RecHubUser.OLClientAccounts.SiteBankID = @parmBankID
        AND RecHubUser.OLClientAccounts.OLOrganizationID = @parmOLOrganizationID
    )
    SELECT 
        RecHubData.dimClientAccountsView.SiteBankID            AS BankID, 
        RecHubData.dimClientAccountsView.SiteOrganizationID    AS OrganizationID, 
        RecHubData.dimClientAccountsView.SiteClientAccountID   AS ClientAccountID, 
        RecHubData.dimClientAccountsView.ShortName,
        RecHubData.dimClientAccountsView.LongName, 
        RecHubData.dimClientAccountsView.CutOff, 
        RecHubData.dimClientAccountsView.SiteCodeID,
        COALESCE(cte_AssignedClientAccounts.Assigned,0)  AS Assigned
    FROM 
        RecHubData.dimClientAccountsView 
        LEFT OUTER JOIN cte_AssignedClientAccounts   ON 
            RecHubData.dimClientAccountsView.SiteClientAccountID = cte_AssignedClientAccounts.SiteClientAccountID
    WHERE 
        RecHubData.dimClientAccountsView.SiteBankID = @parmBankID
    ORDER BY 
        RecHubData.dimClientAccountsView.SiteBankID, 
        RecHubData.dimClientAccountsView.SiteOrganizationID, 
        RecHubData.dimClientAccountsView.SiteClientAccountID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

