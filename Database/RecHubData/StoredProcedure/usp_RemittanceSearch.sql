--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_RemittanceSearch
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_RemittanceSearch') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_RemittanceSearch
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_RemittanceSearch
(
	@parmSearchRequest	XML,
	@parmSearchTotals	XML OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 04/08/2010
*
* Purpose: Query fact table for matching records
*
* Modification History
* 04/08/2010 CR 29399 JNE	Created
* 06/25/2010 CR 29818 JNE	Added OLCustomers.ViewingDays
* 07/01/2010 CR 29904 JNE	Update Cutoff and ViewProcessAhead
* 07/01/2010 CR 29902 JNE	Added SiteLockboxID to where statement when applicable.
* 07/01/2010 CR 30068 JNE	Added parsing logic to determine proper Sort By Column
* 07/05/2010 CR 29901 JNE	Added logic to use startrecord, recordsperpage, and paginateRS(optional).
* 07/06/2010 CR 30124 JNE	Added logic to handle -1 for lockboxid
* 07/22/2010 CR 29901 JNE	Added missing BatchSequence in second SQL statement.
* 01/14/2011 CR 32361 JNE	Added LockboxUser Table to join and limit by UserID passed for Online
* 12/01/2011 CR 48496 JPB	Speed improvements. (FP 48494)
* 01/12/2012 CR 49204 JPB	Corrected error when OLCustomerID is not included in the search request.
*							Added SiteCustomerID to #tmpDimensions.
* 03/09/2012 CR 51092 JPB	Remove dup rows from #tmpDimensions if Research Only request.
* 03/19/2012 CR 51357 JPB	Speed improvements. (FP 50106)
* 03/26/2012 CR 51092 JNE       RemoveList was not capturing non-duplicated rows, changed > to >=
* 06/11/2012 CR 53346 JNE	Modified tmpDimensions insert to use dimlockboxes as base then
*				Left joined the OLLockboxes and OLCustomers for online specific info.
* 06/19/2012 CR 53477 JNE	Modified if statement - removed if lockboxid>=0 check, we want to remove
*				duplicates if searching by customer as well and lockboxid = -1 in that case.
* 06/19/2012 CR 53799 JNE	Added cuttoff information in tmpDimension.
* 08/30/2012 CR 55213 JNE	Added where clause to insert into tmpDimensions statement for research.
*							Added OLLockboxID to where statement if exists for online.
*							Added siteLockboxID to where statement if it exists and not -1 for research. 
* 09/09/2012 CR 54158 JPB	Added BatchNumber.
* 10/09/2012 CR 56125 JPB	OLTA missing from dimSiteCodes.CurrentProcessingDate on temp dmin table.
* 05/01/2013 WI 90780 JBS	Update to 2.0 release. Change schema to RecHubData.
*							Change all references: LockBox to ClientAccount,
*							Customer to Organization, SiteCode to SiteCodeID. 
* 06/12/2013 WI 93457 JPB	Added PaymentSource/Type to result set/sort.
* 08/27/2013 WI 112729 CEJ	Update usp_RemittanceSearch so the Payment Type and Payment Source Long Names are returned
* 12/12/2013 WI 125155 JBS	Adding logic to handle Multi-org relationships in search.  Also enhanced viewing days logic default.
* 12/17/2013 WI 125555 JBS	Add in Check on FactChecks.IsDeleted
* 03/26/2014 WI 133285 JBS	Add DDA to Filter and output. Also added RT and Account columns.
* 06/20/2014 WI 142871 JBS	Changes to handle New RAAM logic.  Add SessionID to input. 
*							SessionClientAccountEntitlements table will determine Workgroup access
* 07/09/2014 WI 142871 KLC	Changed from LogonEntityID to workgroup's EntityID
* 08/09/2014 WI 159378 JMC	Changed ORDER BY from BatchID to SourceBatchID
* 09/09/2014 WI	164313 JBS	Instead of search across ALL WorkGroups when none selected, we will change the 
*							ClientAccountIDs to be a list of 1 or Many.
* 09/23/2014 WI	167525 SAS	Added ImportTypeShortName column
* 09/30/2014 WI	167525 SAS	Added missing comma before ImportTypeShortName
* 09/30/2014 WI	167525 SAS	Added ImportTypeShortName is pagination field list
* 10/24/2014 WI	174075 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175110 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/03/2014 WI	174867 JMC	Modified join on SessionClientAccountEntitlements to use Key rather than IDs
* 11/07/2014 WI 176300 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
* 11/12/2014 WI	177505 SAS  Added a missing comma
* 03/25/2015 WI 196265 JBS  Adding view ahead permission logic
* 04/06/2016 WI 273967 MAA Allowed zeros to be the floor of check amounts
* 08/26/2016 #128178099 JBS Expand BatchSourceKey datatype to SMALLINT
* 11/20/2017 #127603923 MGE	Fix search for <= 0 so it doesnt return all rows.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @vcRemitterName			VARCHAR(30),
        @dtDepositDateStart		DATETIME,
        @dtDepositDateStop		DATETIME,
		@dtDepositDateKeyStart	INT,			--CR 48357
		@dtDepositDateKeyStop	INT,			--CR 48357
		@BatchPaymentTypeKey	TINYINT,
		@BatchSourceKey			SMALLINT,
        @vcAmountFrom			VARCHAR(100),
        @vcAmountTo				VARCHAR(100),
        @vcSerialNumber			VARCHAR(40),
        @vcSortBy				VARCHAR(512),
		@vcSortByColumn			VARCHAR(512),
        @vcSortDirection		VARCHAR(20),
        @vcSessionID			VARCHAR(40),
        @vcSiteBankID			VARCHAR(10),
		@vcSiteClientAccountID	VARCHAR(10),
		@vcOnlineViewingDays	VARCHAR(10),
        @vcViewAheadResearch	VARCHAR(5),
		@vcDDA					VARCHAR(35),	-- WI 133285
		@vcRoutingNumber		VARCHAR(30),	-- WI 133285
		@vcAccount				VARCHAR(30),	-- WI 133285
        @iUseCutoff				INT,
        @iOnlineViewingDays		INT,
        @iMinDepositStatus		INT,
        @SQLSelect				VARCHAR(MAX),
        @SQLFrom				VARCHAR(MAX),
        @SQLWhere				VARCHAR(MAX),
        @SQLOrderby				VARCHAR(MAX),
        @Today					VARCHAR(10),
		@iRecordsPerPage		INT,
		@iStartRecord			INT,
		@SQLPagination			VARCHAR(MAX),
		@vcPaginateRS			VARCHAR(5),
		@iPaginateRS			SMALLINT,
		@iSRRecordCount			INT,
		@SQLInsert				VARCHAR(MAX),
		@SQLSelectP				VARCHAR(MAX),
		@SQLCmd1				VARCHAR(MAX),
		@SQLFillTmpDims			VARCHAR(MAX);

BEGIN TRY 
	SET @Today = CONVERT(VARCHAR,GETDATE(),101); -- Function is nondeterministic so save as variable and use variable.
	SELECT	@vcRemitterName = RemitterName,
			@dtDepositDateStart = DepositDateStart,
			@dtDepositDateStop = DepositDateStop,
			@vcAmountFrom = AmountFrom,
			@vcAmountTo = AmountTo,
			@vcSessionID = SessionID,
			@vcSiteBankID = SiteBankID,
	--		@vcSiteClientAccountID = SiteClientAccountID,  -- remove WI	164313
			@vcViewAheadResearch = ViewAheadResearch,
			@vcSerialNumber = SerialNumber,
			@vcSortBy = SortBy,
			@iUseCutoff = UseCutoff,
	--		@iOnlineViewingDays = OnlineViewingDays,
			@iMinDepositStatus = MinDepositStatus,
			@iRecordsPerPage = RecordsPerPage,
			@iStartRecord = StartRecord,
			@vcPaginateRS = PaginateRS,
			@BatchPaymentTypeKey = 
				CASE 
					WHEN LEN(SR.BatchPaymentTypeKey) > 0 THEN CAST(SR.BatchPaymentTypeKey AS TINYINT)
					ELSE 255
			END,
			@BatchSourceKey = 
				CASE 
					WHEN LEN(SR.BatchSourceKey) > 0 THEN CAST(SR.BatchSourceKey AS SMALLINT)
					ELSE -1			-- #128178099 Changed value for larger datatype
			END,
			@vcDDA	= DDA,      	-- WI 133285
			@vcRoutingNumber = RT,  -- WI 133285
			@vcAccount = Account	-- WI 133285
	FROM	(
				SELECT	SearchRequest.att.query('RemitterName').value('.', 'varchar(40)') AS RemitterName,
						SearchRequest.att.query('DepositDateFrom').value('.', 'datetime') AS DepositDateStart,
						SearchRequest.att.query('DepositDateTo').value('.', 'datetime') AS DepositDateStop,
						SearchRequest.att.query('AmountFrom').value('.', 'varchar(100)') AS AmountFrom,
						SearchRequest.att.query('AmountTo').value('.', 'varchar(100)') AS AmountTo,
						SearchRequest.att.query('SessionID').value('.', 'varchar(40)') AS SessionID,
						SearchRequest.att.query('SiteBankID').value('.', 'varchar(10)') AS SiteBankID,
				--		SearchRequest.att.query('ClientAccountID').value('.', 'varchar(10)') AS SiteClientAccountID,  -- Remove WI 164313
						SearchRequest.att.query('ViewAheadResearch').value('.', 'varchar(5)') AS ViewAheadResearch,
						SearchRequest.att.query('Serial').value('.', 'varchar(40)') AS SerialNumber,
						SearchRequest.att.query('SortBy').value('.', 'varchar(512)') AS SortBy,
						COALESCE(SearchRequest.att.query('UseCutoff').value('.', 'int'),1) AS UseCutoff,
						COALESCE(SearchRequest.att.query('MinDepositStatus').value('.','int'),850) AS MinDepositStatus,
						COALESCE(SearchRequest.att.query('RecordsPerPage').value('.','int'),99999999) AS RecordsPerPage,
						COALESCE(SearchRequest.att.query('StartRecord').value('.','int'),1) AS StartRecord,
						SearchRequest.att.query('PaginateRS').value('.','varchar(5)') AS PaginateRS,
						SearchRequest.att.query('BatchPaymentTypeKey').value('.', 'VARCHAR(3)') AS BatchPaymentTypeKey,
						SearchRequest.att.query('BatchSourceKey').value('.', 'VARCHAR(5)') AS BatchSourceKey,
						SearchRequest.att.query('DDA').value('.', 'VARCHAR(35)') AS DDA,
						SearchRequest.att.query('RT').value('.', 'VARCHAR(30)') AS RT,
						SearchRequest.att.query('Account').value('.', 'VARCHAR(30)') AS ACCOUNT
				FROM	@parmSearchRequest.nodes('/Root') SearchRequest(att)
			) SR;

	--Set Default sort direction
	IF LEN(@vcSortDirection) = 0
		SET @vcSortDirection = 'ASC';
	--WI 125155
	--Set variable for use  IF Viewing days is NULL replace with 0, IF we hit the defaults we will not display by default
-- TODO not sure if this will still be used
--	SET @vcOnlineViewingDays = CAST(COALESCE(@iOnlineViewingDays,0) AS VARCHAR(10));
	
	--Set RecordsPerPage if not in XML
	IF @iRecordsPerPage <= 0
		SET @iRecordsPerPage = 999999999;
	-- Set StartRecord if not in XML
	IF @iStartRecord <= 0
		SET @iStartRecord = 1;
	-- Set Date Keys to use
	Set @dtDepositDateKeyStart = CONVERT(VARCHAR,(@dtDepositDateStart),112);	--CR 48357
	Set @dtDepositDateKeyStop = CONVERT(VARCHAR,(@dtDepositDateStop),112);	--CR 48357
	--Set pagination information
	-- If PaginateRS is not passed then use the recordsperpage variable
    -- If PaginateRS is passed then check flag whether to use recordsperpage variable.
	IF LEN(RTRIM(@vcPaginateRS))>0
		BEGIN
			IF UPPER(@vcPaginateRS) = 'TRUE' OR @vcPaginateRS = '1'
				SET @iPaginateRS = 1
			ELSE
				SET @iPaginateRS = 0
		END
	ELSE
		BEGIN
			SET @iPaginateRS = 1
		END;
	--Create temp table
	IF OBJECT_ID('tempdb..#tmpFinalResults')IS NOT NULL
			DROP TABLE #tmpFinalResults;

	CREATE TABLE #tmpFinalResults
    (
        BankID				INT,
        OrganizationID		INT,
        ClientAccountID		INT,
		ClientAccountKey	INT,
		BatchID				BIGINT,
		SourceBatchID		BIGINT,
		BatchNumber			INT,		--CR 54158 
		DepositDate			DATETIME,
		Deposit_Date		DATETIME,
		PICSDate			INT,		--48357  changed to int
		TransactionID		INT,
		TxnSequence			INT,
		Amount				MONEY,
		DDA					VARCHAR(35),	-- 133285
		RT					VARCHAR(30),
		Account				VARCHAR(30),
		BatchSequence		INT,
		Serial				VARCHAR(30),
		RemitterName		VARCHAR(60),
		DepositStatusDisplay VARCHAR(80),
		TotalCount			INT,
		BatchPaymentTypeKey	INT,
		BatchSourceKey		INT,
		RECID				INT,
		BatchSourceShortName	VARCHAR(30),
		ImportTypeShortName		VARCHAR(30)
    );
    
	-- WI 164313 new work table used to narrow WorkGroup selection
	IF OBJECT_ID('tempdb..#tmpClientAccountList') IS NOT NULL
        DROP TABLE #tmpClientAccountList;

	CREATE TABLE #tmpClientAccountList
	(
		BankID				INT,
		ClientAccountID		INT
	);

	IF OBJECT_ID('tempdb..#tmpDimensions') IS NOT NULL
            DROP TABLE #tmpDimensions;

	CREATE TABLE #tmpDimensions
	(
		RecID				INT IDENTITY(1,1),
		SiteBankID			INT,   
		EntityID			INT,			-- NEW
		SiteClientAccountID INT, 
		ClientAccountKey	INT, 
		SiteCodeID			INT,
		CutOff				INT,
		StartDateKey		INT,
		SiteCurProcDateKey	INT,
		SiteOrganizationID	INT,
		ViewAhead			BIT
	);

	-- OUT we will determine single clientAccount search or multi client account search based on value or no value in 
	-- vcSiteClientAccountID as we build the statement to load the #tmpDimensions

	-- WI 164313 This loads up list of ClientAccountIDs chosen to search against.  This should be 1 or many
	INSERT INTO #tmpClientAccountList
	(
		BankID,
		ClientAccountID		
	)
	SELECT	
		SearchRequest.att.value('@BankID', 'int') AS BankID,
		SearchRequest.att.value('@ClientAccountID', 'int') AS ClientAccountID	 
	FROM 	
		@parmSearchRequest.nodes('/Root/ClientAccounts/ClientAccount') SearchRequest(att)

	-- debug only
	--Select * from #tmpClientAccountList

	BEGIN TRY
--		EXEC(@SQLFillTmpDims);
-- eliminated the dynamic SQL build to populate the #tmpDimensions table		
	INSERT into #tmpDimensions 
	(   SiteBankID, 
		EntityID,		-- NEW
		SiteClientAccountID, 
		ClientAccountKey, 
		SiteCodeID,
		CutOff,
		StartDateKey,
		SiteCurProcDateKey,
		ViewAhead
	)
	SELECT RecHubData.dimClientAccounts.SiteBankID, 
		RecHubUser.SessionClientAccountEntitlements.EntityID,
		RecHubData.dimClientAccounts.SiteClientAccountID, 
		RecHubData.dimClientAccounts.ClientAccountKey, 
		RecHubData.dimClientAccounts.SiteCodeID,
		RecHubData.dimClientAccounts.CutOff,
		RecHubUser.SessionClientAccountEntitlements.StartDateKey,
		RecHubUser.SessionClientAccountEntitlements.EndDateKey,
		RecHubUser.SessionClientAccountEntitlements.ViewAhead
	FROM RecHubData.dimClientAccounts 
		INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN #tmpClientAccountList										-- WI 164313  added join to work table to refine search
			ON #tmpClientAccountList.BankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID 
			AND #tmpClientAccountList.ClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @vcSessionID;

	END TRY
	BEGIN CATCH
		--error inserting into temp table, stop the process here
		--get the error information so it can be passed to RAISERROR
		--this will pass control to main CATCH block
		DECLARE @ErrorMessageTmpDims    NVARCHAR(4000),
				@ErrorSeverityTmpDims   INT,
				@ErrorStateTmpDims      INT;
		SELECT	@ErrorMessageTmpDims = ERROR_MESSAGE()+ 'Inserting into #tmpDimensions table',
				@ErrorSeverityTmpDims = ERROR_SEVERITY(),
				@ErrorStateTmpDims = ERROR_STATE();
		RAISERROR(@ErrorMessageTmpDims,@ErrorSeverityTmpDims,@ErrorStateTmpDims);
	END CATCH	

 
-- DEBUG
--SELECT * FROM #tmpDimensions

	SET @SQLPagination = 'SELECT BankID, OrganizationID, ClientAccountID, ClientAccountKey';
	SET @SQLInsert = 'INSERT INTO #tmpFinalResults 
		(
		BankID,
        OrganizationID,
        ClientAccountID,
		ClientAccountKey';
	
	-- SELECT
	SET @SQLSelect = 'SELECT  
		tmpD.SiteBankID AS BankID,
		tmpD.SiteOrganizationID AS OrganizationID,
		tmpD.SiteClientAccountID AS ClientAccountID,
		tmpD.ClientAccountKey AS ClientAccountKey, ';
	
	--PRINT @SQLSelect
	--48357 MAY NEED TO CONVERT TYPE FOR THE DEPOSIT DATE FIELDS  
	--WI 133285  in all 3 scenarios added RecHubData.dimDDAs.DDA column
	SET @SQLInsert = @SQLInsert + ',BatchID,
		SourceBatchID,
		BatchNumber,
		DepositDate,
		Deposit_Date,
		PICSDate,
		TransactionID,
		TxnSequence,
		Amount,
		RecHubData.dimDDAS.DDA,
		RT,
		Account,
		BatchSequence,
		Serial,
		RemitterName,
		BatchPaymentTypeKey,
		BatchSourceKey,
		BatchSourceShortName,
		ImportTypeShortName'

	SET @SQLPagination = @SQLPagination + ',BatchID,
		SourceBatchID,
		BatchNumber,
		DepositDate,
		Deposit_Date,
		PICSDate,
		TransactionID,
		TxnSequence,
		Amount,
		DDA,
		RT,
		Account,
		BatchSequence,
		Serial,
		RemitterName,
		RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
		RecHubData.dimBatchSources.LongName AS PaymentSource,
		BatchSourceShortName,
		ImportTypeShortName '
--CR 48357 changed Convert(VARCHAR(8), dimProcessingDates.CalendarDate,112) to OLTA.factChecks.ProcessingDateKey

	SET @SQLSelect = @SQLSelect + 'RecHubData.factChecks.BatchID, 
		RecHubData.factChecks.SourceBatchID,
		RecHubData.factChecks.BatchNumber,
		CAST(CONVERT(VARCHAR, RecHubData.factChecks.DepositDateKey, 101) AS DateTime) AS DepositDate,
		CAST(CONVERT(VARCHAR, RecHubData.factChecks.DepositDateKey, 101) AS DateTime) AS Deposit_Date,
		RecHubData.factChecks.ImmutableDateKey AS PICSDate,       
		RecHubData.factChecks.TransactionID,
		RecHubData.factChecks.TxnSequence,
		RecHubData.factChecks.Amount,
		RecHubData.dimDDAS.DDA,
		RTrim(RecHubData.factChecks.RoutingNumber)                       AS RT,
		RTrim(RecHubData.factChecks.Account)                             AS Account,
		RecHubData.factChecks.BatchSequence,
		RTrim(RecHubData.factChecks.Serial)                              AS Serial,
		RTrim(RecHubData.factChecks.RemitterName)                        AS RemitterName,
		RecHubData.factChecks.BatchPaymentTypeKey,
		RecHubData.factChecks.BatchSourceKey,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName'	
		

-- OUT Do not see where DepositStatusDisplay and StatusDisplayName is coming from
-- TODO how to determine how this was used
	--IF LEN(@vcSiteBankID) > 0
	--	BEGIN
	--		SET @SQLSelect = @SQLSelect + ',  AS DepositStatusDisplay'-- Research Only
	--		SET @SQLInsert = @SQLInsert + ',DepositStatusDisplay'
	--		SET @SQLPagination = @SQLPagination + ',DepositStatusDisplay'
	--	END;

	-- Add total count
	SET @SQLSelect = @SQLSelect + ', COUNT(*) over () AS TotalCount';
	SET @SQLInsert = @SQLInsert +',TotalCount';

	-- FROM
	SET @SQLFrom =' 
		FROM #tmpDimensions tmpD 
            INNER JOIN RecHubData.factChecks on RecHubData.factChecks.ClientAccountKey = tmpD.ClientAccountKey 
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factChecks.BatchPaymentTypeKey 
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factChecks.BatchSourceKey 
			INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	 ';
	
	--	WI 133285
	SET @SQLFrom = @SQLFrom + '
			LEFT OUTER JOIN RecHubData.dimDDAs ON RecHubData.factChecks.DDAKey = RecHubData.dimDDAs.DDAKey';
 
	-- WHERE CLAUSE
	-- Used both in Online and Research
	SET @SQLWhere = ' WHERE RecHubData.factChecks.DepositStatus >= '+CAST(@iMinDepositStatus AS VARCHAR(10))
	
	IF @dtDepositDateStart > '12/30/1899 00:00:00'
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.DepositDateKey >= '+ CAST(@dtDepositDateKeyStart AS VARCHAR(8))  --48357
		END

	IF @dtDepositDateStop > '12/30/1899 00:00:00'
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.DepositDateKey <= '+ CAST(@dtDepositDateKeyStop AS VARCHAR(8))  --48357
		END

	IF LEN(@vcRemitterName)>0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.RemitterName LIKE '+CHAR(39)+'%'+@vcRemitterName + '%'+CHAR(39)
		END

	IF LEN(@vcSerialNumber)>0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.Serial LIKE '+CHAR(39)+'%' + @vcSerialNumber + '%'+CHAR(39)
		END

	IF @vcAmountFrom IS NOT NULL AND LTRIM(RTRIM(@vcAmountFrom)) <> ''  
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.Amount >= '+ @vcAmountFrom
		END

	IF @vcAmountTo IS NOT NULL AND LTRIM(RTRIM(@vcAmountTo)) <> ''
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.Amount <= '+ @vcAmountTo 
		END
	-- WI 133285 adding DDA,RT and Account filters
	IF LEN(@vcDDA)>0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.dimDDAs.DDA LIKE '+CHAR(39)+'%' + @vcDDA + '%'+CHAR(39);
		END
	IF LEN(@vcRoutingNumber)>0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.RoutingNumber LIKE '+CHAR(39)+'%' + @vcRoutingNumber + '%'+CHAR(39);
		END
	IF LEN(@vcAccount)>0
		BEGIN
			SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.Account LIKE '+CHAR(39)+'%' + @vcAccount + '%'+CHAR(39);
		END

	IF( @BatchPaymentTypeKey <> 255 )
		SET @SQLWhere = @SQLWhere +  '	AND RecHubData.factChecks.BatchPaymentTypeKey = ' + CAST(@BatchPaymentTypeKey AS VARCHAR);
	IF( @BatchSourceKey <> -1 )		-- #128178099 Changed value for larger datatype
		SET @SQLWhere = @SQLWhere +  '	AND RecHubData.factChecks.BatchSourceKey = ' + CAST(@BatchSourceKey AS VARCHAR);

	-- USE for all situations  WI 125555
	SET @SQLWhere = @SQLWhere +  'AND ((RecHubData.factChecks.DepositDateKey <= tmpD.SiteCurProcDateKey AND tmpD.ViewAhead = 0) OR (tmpD.ViewAhead = 1))
			AND RecHubData.factChecks.DepositDateKey >= tmpD.StartDateKey
			AND RecHubData.factChecks.IsDeleted = 0';


-- do I need this or will we just use the StartDates in the SessionClientAccountEntitlements  table
-- TODO 
--	IF @iOnlineViewingDays > -1 --  AND (LEN(@vcEntityID)>0 and @vcEntityID != '00000000-0000-0000-0000-000000000000')
	BEGIN
		SET @SQLWhere = @SQLWhere + ' AND RecHubData.factChecks.DepositDateKey >= tmpD.StartDateKey'
	END

	-- ORDER BY
	IF LEN(@vcSortBy) = 0
		BEGIN
			SET @SQLOrderby = ' ORDER BY
							tmpD.SiteClientAccountID ASC,
							RecHubData.factChecks.DepositDateKey ASC,
							RecHubData.factChecks.BatchID ASC,
							RecHubData.factChecks.TxnSequence ASC'
		END
	ELSE
		BEGIN
			SET @vcSortDirection = SUBSTRING(@vcSortBy, CHARINDEX('~',@vcSortBy)+1, (LEN(@vcSortBy)-CHARINDEX('~',@vcSortBy)));
			IF @vcSortDirection = 'DESCENDING'
				BEGIN
					SET @vcSortDirection = 'DESC'
				END
			ELSE
				BEGIN
					SET @vcSortDirection = 'ASC'
				END;
			IF CHARINDEX('~',@vcSortBy)>0
				BEGIN
					SET @vcSortByColumn = SUBSTRING(@vcSortBy, 1, CHARINDEX('~',@vcSortBy)-1)
				END
			ELSE
				BEGIN
					SET @vcSortByColumn = @vcSortBy
				END;

			SET @SQLOrderby =  CASE UPPER(@vcSortByColumn)
				WHEN 'CLIENTACCOUNTID' THEN ' ORDER BY
										tmpD.SiteClientAccountID ' + @vcSortDirection +',
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'DEPOSITDATE' THEN ' ORDER BY
										RecHubData.factChecks.DepositDateKey ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'SOURCEBATCHID' THEN ' ORDER BY
										RecHubData.factChecks.SourceBatchID ' + @vcSortDirection +',
										RecHubData.factChecks.DepositDateKey ASC,
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'BATCHNUMBER' THEN ' ORDER BY
										RecHubData.factChecks.BatchNumber ' + @vcSortDirection +',
										RecHubData.factChecks.DepositDateKey ASC,
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'TXNSEQUENCE' THEN ' ORDER BY
										RecHubData.factChecks.TxnSequence ' + @vcSortDirection +',
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										tmpD.SiteClientAccountID ASC'
				WHEN 'AMOUNT' THEN ' ORDER BY
										RecHubData.factChecks.Amount ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'RT' THEN ' ORDER BY
										RTrim(RecHubData.factChecks.RoutingNumber) ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'ACCOUNT' THEN ' ORDER BY
										RecHubData.factChecks.Account ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC, 
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'SERIAL' THEN ' ORDER BY
										RecHubData.factChecks.Serial ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'REMITTER' THEN ' ORDER BY
										RecHubData.factChecks.RemitterName ' + @vcSortDirection +',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'PAYMENTSOURCE' THEN ' ORDER BY
										RecHubData.dimBatchSources.LongName '+@vcSortDirection+',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'PAYMENTTYPE' THEN ' ORDER BY 
										RecHubData.dimBatchPaymentTypes.LongName '+@vcSortDirection+',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				WHEN 'DDA' THEN ' ORDER BY 
										RecHubData.dimDDAS.DDA '+@vcSortDirection+',
										tmpD.SiteClientAccountID ASC,
										RecHubData.factChecks.DepositDateKey ASC,
										RecHubData.factChecks.BatchID ASC,
										RecHubData.factChecks.TxnSequence ASC'
				ELSE
					' ORDER BY
						tmpD.SiteClientAccountID ASC,
						RecHubData.factChecks.DepositDateKey ASC,
						RecHubData.factChecks.BatchID ASC,
						RecHubData.factChecks.TxnSequence ASC'
			END;

			IF CHARINDEX('.',@vcSortBy)>0
				BEGIN
					 SET @SQLOrderby =' ORDER BY '+@vcSortBy+' '+@vcSortDirection +',
							RecHubData.dimClientAccountsView.SiteClientAccountID ASC,
							RecHubData.factChecks.DepositDateKey ASC,
							RecHubData.factChecks.BatchID ASC,
							RecHubData.factChecks.TxnSequence ASC'
				END;
		END

		-- ADD Pagination ability
	SET @SQLSelectP = @SQLSelect + ', ROW_NUMBER() OVER ('+@SQLOrderby+') AS RecID'	;	
	SET @SQLInsert = @SQLInsert + ',RECID)' ;

	SET @SQLcmd1 = @SQLSelectP + @SQLFrom + @SQLWhere + @SQLOrderby;
	
	-- Add insert statement to SQLSelect
	SET @SQLCmd1 = @SQLInsert + @SQLCmd1;
-- DEBUG 
--PRINT @SQLCmd1

	-- Run the select statement
	BEGIN TRY
		EXEC(@SQLCmd1);
	END TRY
	BEGIN CATCH
		--error inserting into temp table, stop the process here
		--get the error information so it can be passed to RAISERROR
		--this will pass control to main CATCH block
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorSeverity   INT,
				@ErrorState      INT;
		SELECT	@ErrorMessage = ERROR_MESSAGE()+ 'Inserting into temp table',
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE();
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
	END CATCH
	--select @SQLCmd1

	SET @iSRRecordCount = (SELECT TOP 1 TotalCount FROM #tmpFinalResults);
	SET @parmSearchTotals = (SELECT	'Results' AS "@Name",
					@iSRRecordCount AS "@TotalRecords"
			FOR XML PATH('RecordSet'), ROOT('Page') );

	-- Now limit it for pagination if required...
	SET @SQLPagination = @SQLPagination + ' FROM #tmpFinalResults 
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #tmpFinalResults.BatchPaymentTypeKey 
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = #tmpFinalResults.BatchSourceKey 
		'
	IF @iPaginateRS = 0
		BEGIN
			SET @SQLPagination = @SQLPagination + ' WHERE RecID >= ' + CAST(@iStartRecord AS VARCHAR(10));
		END
	ELSE
		BEGIN
			SET @SQLPagination = @SQLPagination + ' WHERE RecID BETWEEN ' + CAST(@iStartRecord AS VARCHAR(10)) + ' AND ' + CAST((@iStartRecord+@iRecordsPerPage-1) AS VARCHAR(10));
		END
	SET @SQLPagination = @SQLPagination + ' ORDER BY RecID ASC ;';	

	EXEC(@SQLPagination);
	--PRINT @SQLPagination

	IF OBJECT_ID('tempdb..#tmpClientAccountList') IS NOT NULL
        DROP TABLE #tmpClientAccountList;
	IF OBJECT_ID('tempdb..#tmpFinalResults')IS NOT NULL
		DROP TABLE #tmpFinalResults;
	IF OBJECT_ID('tempdb..#tmpDimensions') IS NOT NULL
		DROP TABLE #tmpDimensions;
	IF OBJECT_ID('tempdb..#tmpMultiOrgs') IS NOT NULL
		DROP TABLE #tmpMultiOrgs;
END TRY

BEGIN CATCH
	IF OBJECT_ID('tempdb..#tmpClientAccountList') IS NOT NULL
        DROP TABLE #tmpClientAccountList;
	IF OBJECT_ID('tempdb..#tmpFinalResults')IS NOT NULL
		DROP TABLE #tmpFinalResults;
	IF OBJECT_ID('tempdb..#tmpDimensions') IS NOT NULL
		DROP TABLE #tmpDimensions;
	IF OBJECT_ID('tempdb..#tmpMultiOrgs') IS NOT NULL
		DROP TABLE #tmpMultiOrgs;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

	