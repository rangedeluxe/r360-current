--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubConfig_Admin">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_dimNotificationFileTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimNotificationFileTypes_Get') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimNotificationFileTypes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimNotificationFileTypes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Charlie Johnson
* Date:     08/09/2012
*
* Purpose:  Gets the Notification File Types
*
* Modification History
* 09/08/2012 CR 54927 CEJ	Created.
* 04/24/2013 WI 98672 JBS	Update to 2.0 release. Change schema to RecHubData
* 04/29/2015 WI 204050 JBS	Removed FileType from Order By
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

SELECT 
	RecHubData.dimNotificationFileTypes.NotificationFileTypeKey,
	RecHubData.dimNotificationFileTypes.FileType,
	RecHubData.dimNotificationFileTypes.FileTypeDescription,
	RecHubData.dimNotificationFileTypes.CreationDate,
	RecHubData.dimNotificationFileTypes.ModificationDate,
	RecHubData.dimNotificationFileTypes.CreatedBy,
	RecHubData.dimNotificationFileTypes.ModifiedBy
FROM
	RecHubData.dimNotificationFileTypes
ORDER BY 
	RecHubData.dimNotificationFileTypes.FileTypeDescription;

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
