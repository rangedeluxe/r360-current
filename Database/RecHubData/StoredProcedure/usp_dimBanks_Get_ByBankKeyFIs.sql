--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Get_ByBankKeyFIs
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Get_ByBankKeyFIs') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Get_ByBankKeyFIs
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Get_ByBankKeyFIs(
	@parmUserId			INT,
	@parmFIs			XML,	-- <FIs><ID>1</ID><ID>2</ID>...</FIs>
	@parmBankKey		INT
	)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 06/03/2014
*
* Purpose: Reads the corresponding record from the RecHubData.dimBanks table 
*			(must match the given user's FI(s))
*
* Modification History
* 06/03/2014 WI 145888 RDS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	WITH CTE_FIs AS
	(
		SELECT FI.x.value('.', 'int') ID FROM @parmFIs.nodes('/FIs/ID') FI(x)
	)
	SELECT	RecHubData.dimBanksView.BankKey,
			RecHubData.dimBanksView.SiteBankID,
			RecHubData.dimBanksView.BankName,
			RecHubData.dimBanksView.EntityID
	FROM	RecHubData.dimBanksView
		LEFT JOIN CTE_FIs
			ON RecHubData.dimBanksView.EntityID = CTE_FIs.ID
	WHERE
		RecHubData.dimBanksView.BankKey = @parmBankKey
		AND (	CTE_FIs.ID IS NOT NULL							-- Must match a user FI
				OR RecHubData.dimBanksView.EntityID IS NULL)	-- Unless it has not been set yet (only occurs immediately post upgrade...)

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

