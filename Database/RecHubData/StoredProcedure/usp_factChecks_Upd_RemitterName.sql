--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Upd_RemitterName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Upd_RemitterName') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Upd_RemitterName
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Upd_RemitterName 
(
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID	INT,
       @parmDepositDate			DATETIME,
       @parmBatchID				BIGINT,		--WI 142839
       @parmTransactionID		INT,
       @parmBatchSequence		INT,
       @parmRemitterName		VARCHAR(60)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/25/2009
*
* Purpose: Update all RemitterNames based on inputs
*
* Modification History
* 03/25/2009 CR 25817 JMC	Created
* 04/24/2013 WI 90797 JBS	Update to 2.0 release.  Change schema to RecHubData
*							Rename proc from usp_UpdateChecksRemitter
*							Change update to RecHubData.factChecks
*							Change references: Lockbox to ClientAccount
*							Rename Parameter:  @parmSiteLockboxID to 
*							@parmSiteClientAccountID.
* 06/02/2014 WI 142839 DLD 	Batch Collision.
******************************************************************************/
--SET NOCOUNT ON 

DECLARE @DepositDateKey INT;

BEGIN TRY

	SELECT @DepositDateKey = CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);

	UPDATE	
		RecHubData.factChecks
	SET	
		RecHubData.factChecks.RemitterName = @parmRemitterName
	FROM
		RecHubData.factChecks
		INNER JOIN RecHubData.dimClientAccounts 
			ON RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
	  	RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factChecks.DepositDateKey = @DepositDateKey
		AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.TransactionID = @parmTransactionID
		AND RecHubData.factChecks.BatchSequence = @parmBatchSequence

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH