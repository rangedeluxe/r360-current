--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimBatchSources_Get_ByEntityID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchSources_Get_ByEntityID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchSources_Get_ByEntityID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchSources_Get_ByEntityID
(
	@parmEntityID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 10/30/2014
*
* Purpose: Get the current source records for the given entity.
*
* Modification History
* 10/30/2014 WI 175100 KLC	Created
* 03/31/2015 WI 198857 JPB	Added ORDER BY LongName
******************************************************************************/
SET NOCOUNT ON; 
	
BEGIN TRY

	SELECT 
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchSources.ShortName,
		RecHubData.dimBatchSources.LongName,
		RecHubData.dimBatchSources.EntityID,
		RecHubData.dimImportTypes.ShortName AS SystemType,
		RecHubData.dimBatchSources.IsActive
	FROM  
		RecHubData.dimBatchSources
		INNER JOIN RecHubData.dimImportTypes ON
		RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	WHERE 
		RecHubData.dimBatchSources.BatchSourceKey > 0
		AND RecHubData.dimBatchSources.EntityID = @parmEntityID
	ORDER BY
		RecHubData.dimBatchSources.LongName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

