--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_BatchID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_BatchID') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_BatchID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_BatchID	
(
	@parmImmutableDateKey	   INT,
	@parmSiteBankID			   INT,
	@parmSiteOrganizationID    INT,
	@parmSiteClientAccountID   INT,
	@parmSiteCodeID			   INT,	
	@parmSourceBatchID		   BIGINT,
	@parmBatchSource		   VARCHAR(30),
	@parmBatchID			   BIGINT       OUTPUT,
	@parmDepositDateKey		   INT	        OUTPUT,
	@parmBatchNumber           INT          OUTPUT,
	@parmBatchPaymentTypeKey   TINYINT      OUTPUT,
	@parmDepositStatus         INT          OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/09/2014
*
* Purpose: Get BatchID.  Given Source Batch ID and Source.
*
* Modification History
* 10/09/2014 WI 170735 JBS  Created
* 03/10/2015 WI 195092 TWE  Output BatchNumber, BatchPaymentTypeKey, DepositStatus
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY

    SET @parmBatchID = -1;
    SET @parmDepositDateKey = -1;
    SET @parmBatchNumber = -1;
    SET @parmBatchPaymentTypeKey = 0;
    SET @parmDepositStatus = -1;

	SELECT 
		@parmBatchID = RecHubData.factBatchSummary.BatchID,
		@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey,
		@parmBatchNumber         = RecHubData.factBatchSummary.BatchNumber,
		@parmBatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey,
		@parmDepositStatus       = RecHubData.factBatchSummary.DepositStatus
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimClientAccounts 
			ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubData.dimBanks 
			ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimOrganizations 
			ON RecHubData.factBatchSummary.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
		INNER JOIN RecHubData.dimBatchSources
			ON RecHubData.factBatchSummary.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey  
	WHERE
	    RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimOrganizations.SiteOrganizationID = @parmSiteOrganizationID	
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.dimClientAccounts.SiteCodeID = @parmSiteCodeID
		AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factBatchSummary.SourceBatchID = @parmSourceBatchID	
		AND RecHubData.dimBatchSources.ShortName = @parmBatchSource;		

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH