--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetDocImage
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetDocImage') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetDocImage
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubData].[usp_GetDocImage] 
(
	@parmBatchID AS BIGINT,
	@parmBatchSequence AS INT,
	@parmDepositDate as INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 07/10/2017
*
* Purpose: Get Information neccesary to get a document's image
*
* Modification History
* 09/21/2017 #150692361 MAA Created
************************************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT RecHubData.factDocuments.BatchSiteCode,
	RecHubData.factDocuments.BatchID, 
	RecHubData.dimClientAccounts.SiteBankID BankID,
	RecHubData.dimClientAccounts.SiteClientAccountID as LockboxID,
	RecHubData.factDocuments.SourceProcessingDateKey AS ProcessingDateKey,
	RecHubData.factDocuments.ImmutableDateKey AS PICSDate,
	RecHubData.factDocuments.SourceBatchID, 
	RecHubData.factDocuments.TransactionID,
	RecHubData.dimDocumentTypes.DocumentTypeDescription,
	RecHubData.dimClientAccounts.OnlineColorMode AS DefaultColorMode,
	RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
	RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
	RecHubData.factDocuments.BatchPaymentTypeKey as PaymentType
	FROM RecHubData.factDocuments 
	INNER JOIN RecHubData.dimClientAccounts 
		ON 	RecHubData.factDocuments.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey 
	LEFT OUTER JOIN  RecHubData.dimDocumentTypes
		ON RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
	INNER JOIN RecHubData.dimBatchSources 
		ON RecHubData.factDocuments.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	INNER JOIN RecHubData.dimImportTypes
		ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	WHERE RecHubData.factDocuments.BatchID = @parmBatchID 
	AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence 
	AND RecHubData.factDocuments.DepositDateKey = @parmDepositDate
	AND RecHubData.factDocuments.IsDeleted = 0
END TRY
BEGIN CATCH
	   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
