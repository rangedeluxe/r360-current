--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_Keys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_Keys') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_Keys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_Keys 
(
    @parmSessionID				UNIQUEIDENTIFIER,	--WI 142824
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmImmutableDate			DATETIME,
    @parmBatchID				BIGINT				--WI 142824
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Locate summary keys for a batch
*
* Modification History
* 01/24/2013 WI 86319 TWE   Created by converting embedded SQL
*							Moved to RecHubData schema.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountKey.
*							Renamed @parmProcessingDate to @parmImmutableDate.
* 05/30/2014 WI 142824 DLD  Batch Collision/RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @ImmutableDateKey	INT,
		@BankKey			INT,		--WI 142824
		@ClientAccountKey	INT,		--WI 142824
		@DepositDateKey		INT,		--WI 142824
		@DepositDateStart	DATETIME,	--WI 142824
        @EndDateKey			INT,		--WI 142824
		@StartDateKey		INT;		--WI 142824

BEGIN TRY
	SET @ImmutableDateKey = CAST(CONVERT(VARCHAR,@parmImmutableDate,112) AS INT);

	/* Get DepositDateKey using ImmutatableDateKey */ --WI 142824
	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @ImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SET @DepositDateStart = CAST(CAST(@DepositDateKey AS CHAR) AS DATETIME);  --WI 142824
	
	/* Ensure the start date is not beyond the max viewing days */  --WI 142824
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @DepositDateStart,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT
		RecHubData.factBatchSummary.BankKey,
		RecHubData.factBatchSummary.ClientAccountKey,
		RecHubData.factBatchSummary.BatchSourceKey
	FROM
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON														--WI 142824
			RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey	--WI 142824
	WHERE
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID						--WI 142824
	AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142824
	AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID  --WI 142824
	AND RecHubData.factBatchSummary.BatchID = @parmBatchID
	AND RecHubData.factBatchSummary.DepositDateKey = @StartDateKey									--WI 142824
	AND RecHubData.factBatchSummary.ImmutableDateKey = @ImmutableDateKey							--WI 142824
	AND IsDeleted = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH