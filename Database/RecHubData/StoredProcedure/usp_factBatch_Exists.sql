--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatch_Exists
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatch_Exists') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatch_Exists
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatch_Exists
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmSourceBatchID			BIGINT,
	@parmDepositDateKey			INT,
	@parmImmutableDateKey		INT,
	@parmBatchSource			VARCHAR(30),
	@parmBatchExists			BIT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2015
*
* Purpose: Test for the existance of a batch. 
*
* Modification History
* 02/24/2015 WI 191796 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/* Set the default value to not exists */
	SET @parmBatchExists = 0;

	SELECT
		@parmBatchExists = 1
	FROM
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
	WHERE
		SiteBankID = @parmSiteBankID
		AND SiteClientAccountID = @parmSiteClientAccountID
		AND DepositDateKey = @parmDepositDateKey
		AND ImmutableDateKey = @parmImmutableDateKey
		AND SourceBatchID = @parmSourceBatchID
		AND UPPER(RecHubData.dimBatchSources.ShortName) = UPPER(@parmBatchSource)
		AND IsDeleted = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH