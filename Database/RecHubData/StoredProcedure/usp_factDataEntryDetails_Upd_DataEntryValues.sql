--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Upd_DataEntryValues
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Upd_DataEntryValues') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Upd_DataEntryValues
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Upd_DataEntryValues
		@XBatch					        XML,
		@parmBankKey			        INT,
		@parmOrganizationKey	        INT,
		@parmClientAccountKey           INT, 
		@parmSourceProcessingDateKey	INT,
		@parmDepositDateKey		        INT,
		@parmImmutableDateKey	        INT,
		@parmBatchID			        BIGINT,
		@parmSourceBatchID              BIGINT,
		@parmBatchNumber                INT,     
		@parmBatchSourceKey             SMALLINT,
		@parmBatchPaymentTypeKey        TINYINT,
		@parmDepositStatus              INT,
		@parmSourceIdentifier       	SMALLINT,
		@parmModificationDate	        DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/09/2010
*
* Purpose: Updates factDataEntryDetails row(s) from XBatch Data Entry XML segment
*
* Modification History
* 02/09/2010 CR 28955 JPB	Created.
* 05/08/2010 CR 29510 JPB 	Account for changing ICON data entry setups.
* 06/09/2010 CR 29883 JPB	When the ICON de record is Checks/ChecksDataEntry,
* 							take the documentsequencenumber from the XML not
*							factStubs. (No fact stubs record exists.)
* 11/12/2010 CR 31050 JPB	Account for " in text fields.
* 01/07/2011 CR 32235 JPB	Store data in native data type columns (keep storing
*							existing DataEntryValue column. Native data types are
*							in addition to the DataEntryValue column.)
* 04/14/2011 CR 33871 JMC   Modified ICON logic to always expect the correct
*                           BatchSequence to avoid a lookup.
* 04/22/2011 CR 33984 JPB	Use new fn_GetLockboxDEColumns to determine 
*							DataEntryColumnKey.
* 10/24/2014 WI 173988 TWE  Converted from 1.05 to 2.01
* 07/16/2015 WI 221853 MAA  Associate data entry records with new data entry column key
* 08/10/2015 WI 228769 TWE  Compare batchsourcekey to match workgroupsDataEntryColumns
* 08/12/2015 WI 229313 TWE  Set IsCheck based on tabletype set in the XML
* 08/18/2015 WI 224995 TWE  Remove dimDataEntryColumns
* 12/09/2015 WI 251690 CEJ  use UILabel rather than SourceDisplayName to join to XML
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

DECLARE @bLocalTransaction	BIT;

BEGIN TRY

	IF @@TRANCOUNT = 0
	BEGIN
		BEGIN TRANSACTION  
		SET @bLocalTransaction = 1
	END
	
	--Create temp file to hold the variable information contained in the xml input parameter
	IF OBJECT_ID('tempdb..#DEUpdate') IS NOT NULL
        DROP TABLE #DEUpdate;
	
	IF OBJECT_ID('tempdb..#WorkgroupDEColumns') IS NOT NULL
        DROP TABLE #WorkgroupDEColumns;

	CREATE TABLE #DEUpdate
	(
	    --IsDeleted
		BankKey                 INT          NOT NULL,
		OrganizationKey         INT          NOT NULL,
		ClientAccountKey        INT          NOT NULL,
		DepositDateKey          INT          NOT NULL,
		ImmutableDateKey        INT          NOT NULL,
		SourceProcessingDateKey INT          NOT NULL, 
		BatchID                 BIGINT       NOT NULL,
		SourceBatchID           BIGINT       NOT NULL,
		BatchNumber             INT          NOT NULL,
	    BatchSourceKey          SMALLINT     NOT NULL,
	    BatchPaymentTypeKey     TINYINT      NOT NULL,
	    DepositStatus           INT          NOT NULL,
		WorkgroupDataEntryColumnKey      BIGINT          NULL,
		TransactionID           INT          NOT NULL,
		BatchSequence           INT          NOT NULL,
		--CreationDate
		--ModificationDate
		DataEntryValueDateTime  DATETIME     NULL,
		DataEntryValueFloat     FLOAT        NULL,
		DataEntryValueMoney     MONEY        NULL,
		DataEntryValue          VARCHAR(256) NOT NULL
	);
	
	CREATE TABLE #WorkgroupDEColumns 
	(
		RowID				        INT IDENTITY(1,1) NOT NULL,
		SiteBankID                  INT               NOT NULL,
		SiteClientAccountID         INT               NOT NULL,
		BatchSourceKey              SMALLINT          NOT NULL,
		IsCheck                     BIT               NOT NULL,
		WorkgroupDataEntryColumnKey	BIGINT            NOT NULL,
		DataType			        SMALLINT          NOT NULL,  --converted for compare
		ScreenOrder			        TINYINT           NOT NULL,
		FieldName	    	        NVARCHAR(256)     NOT NULL,
		UILabel						NVARCHAR(256)     NOT NULL
	);

	/* now fill the #WorkgroupDEColumns table*/
	;WITH SiteInfo AS
	(
		SELECT	RecHubData.dimClientAccounts.SiteBankID,
				RecHubData.dimClientAccounts.SiteClientAccountID
		FROM	RecHubData.dimClientAccounts
		WHERE	ClientAccountKey = @parmClientAccountKey
	)
	INSERT INTO #WorkgroupDEColumns
	(
	    SiteBankID,      
	    SiteClientAccountID,
		BatchSourceKey,
		IsCheck,
		WorkgroupDataEntryColumnKey,
		DataType,
		ScreenOrder,
		FieldName,
		UILabel
	)
	SELECT 	
	        RecHubData.dimWorkgroupDataEntryColumns.SiteBankID,
			RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID,
			RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
	        RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
			RecHubData.dimWorkgroupDataEntryColumns.DataType,
			RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName,
			RecHubData.dimWorkgroupDataEntryColumns.UILabel
	FROM	RecHubData.dimWorkgroupDataEntryColumns
			INNER JOIN SiteInfo 
				ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = SIteInfo.SiteBankID 
				AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = SiteInfo.SiteClientAccountID
	WHERE 
	    @parmBatchSourceKey = dimWorkgroupDataEntryColumns.BatchSourceKey
	ORDER BY RecHubData.dimWorkgroupDataEntryColumns.CreationDate;


	/* Remove duplicates based on the CreationDate (from the above query). */
	DELETE FROM #WorkgroupDEColumns
	WHERE RowID NOT IN
	(
		SELECT MAX(RowID)
		FROM	#WorkgroupDEColumns
		GROUP BY FieldName,UILabel
	);

	--parse the XML data into the temp table
	INSERT INTO #DEUpdate
	(
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber     ,     
		BatchSourceKey   ,    
		BatchPaymentTypeKey  ,
		DepositStatus      ,  
		WorkgroupDataEntryColumnKey,
		TransactionID,
		BatchSequence,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue
	)
	SELECT	
	    @parmBankKey                 AS BankKey,
		@parmOrganizationKey         AS OrganizationKey,
		@parmClientAccountKey        AS ClientAccountKey,
		@parmDepositDateKey          AS DepositDateKey,
		@parmImmutableDateKey        AS ImmutableDateKey,
		@parmSourceProcessingDateKey AS SourceProcessingDateKey,
		@parmBatchID                 AS BatchID,
		@parmSourceBatchID           AS SourceBatchID,
		@parmBatchNumber             AS BatchNumber,
		@parmBatchSourceKey          AS BatchSourceKey,  
		@parmBatchPaymentTypeKey     AS BatchPaymentTypeKey,
		@parmDepositStatus           AS DepositStatus, 
		dimWorkgroupDEColumns.WorkgroupDataEntryColumnKey,
		DE.TransactionID,
		DE.BatchSequence,
		CASE 
			WHEN dimWorkgroupDEColumns.DataType = 11 THEN CAST(DataEntryValue AS DATETIME)
			ELSE NULL
		END AS DataEntryValueDateTime,
		CASE 
			WHEN dimWorkgroupDEColumns.DataType = 6 
			     AND dimWorkgroupDEColumns.IsCheck = 'true' 
				 AND UPPER(dimWorkgroupDEColumns.FieldName) <> 'AMOUNT' THEN CAST(DataEntryValue AS FLOAT)
			ELSE NULL
		END AS DataEntryValueFloat,
		CASE 
			WHEN dimWorkgroupDEColumns.DataType = 7 THEN CAST(DataEntryValue AS MONEY)
			WHEN dimWorkgroupDEColumns.DataType = 6 
			     AND dimWorkgroupDEColumns.IsCheck = 'false'
				 AND UPPER(dimWorkgroupDEColumns.FieldName) = 'AMOUNT' THEN CAST(DataEntryValue AS MONEY)			
			ELSE NULL
		END AS DataEntryValueMoney,
		CASE
			WHEN dimWorkgroupDEColumns.DataType = 11 THEN CAST(DATEPART(M,CAST(DE.DataEntryValue AS DATETIME)) AS VARCHAR(2)) + '/' + CAST(DATEPART(D,CAST(DE.DataEntryValue AS DATETIME)) AS VARCHAR(2)) + '/' + CAST(DATEPART(YYYY,CAST(DE.DataEntryValue AS DATETIME)) AS VARCHAR(4))
			ELSE REPLACE(REPLACE(DE.DataEntryValue,'&amp;','&'),'&quot;','"')
		END AS DataEntryValue   
	FROM (
			SELECT	DataEntry.att.value('@TransactionID', 'int')                    AS TransactionID,
					DataEntry.att.value('@BatchSequence', 'int')                    AS BatchSequence,
					DataEntry.att.value('@DocumentBatchSequence','int')             AS DocumentBatchSequence,
					DataEntry.att.value('@IsCheck','bit')                           AS IsCheck,
					DataEntry.att.value('@FieldName', 'nvarchar(256)')              AS FieldName, 
					DataEntry.att.value('@SourceDisplayName', 'nvarchar(256)')      AS SourceDisplayName,
					DataEntry.att.value('@DataEntryValue', 'varchar(256)')          AS DataEntryValue
			FROM	@XBatch.nodes('/Batches/Batch/DataEntry/DataEntry') DataEntry(att)
		) DE
		INNER JOIN #WorkgroupDEColumns AS dimWorkgroupDEColumns
		  ON DE.FieldName = dimWorkgroupDEColumns.FieldName 
			AND DE.SourceDisplayName = dimWorkgroupDEColumns.UILabel 
			AND DE.IsCheck = dimWorkgroupDEColumns.IsCheck
	WHERE NULLIF(DataEntryValue,'') IS NOT NULL;

	---------------------------------------------------------------------------------------------------------
	--We have all the information in a temp table
	--First flag the current records as deleted
	UPDATE	
		RecHubData.factDataEntryDetails
	SET
		RecHubData.factDataEntryDetails.IsDeleted = 1,
		RecHubData.factDataEntryDetails.ModificationDate = @parmModificationDate		
	FROM
	  	RecHubData.factDataEntryDetails
		INNER JOIN #DEUpdate UpdateTable 
		  ON RecHubData.factDataEntryDetails.BankKey = UpdateTable.BankKey
			AND RecHubData.factDataEntryDetails.OrganizationKey = UpdateTable.OrganizationKey
			AND RecHubData.factDataEntryDetails.ClientAccountKey = UpdateTable.ClientAccountKey
			AND RecHubData.factDataEntryDetails.SourceProcessingDateKey = UpdateTable.SourceProcessingDateKey
			AND RecHubData.factDataEntryDetails.DepositDateKey = UpdateTable.DepositDateKey
			AND RecHubData.factDataEntryDetails.BatchID = UpdateTable.BatchID
			AND RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = UpdateTable.WorkgroupDataEntryColumnKey
			AND RecHubData.factDataEntryDetails.TransactionID = UpdateTable.TransactionID
			AND RecHubData.factDataEntryDetails.BatchSequence = UpdateTable.BatchSequence
		    AND RecHubData.factDataEntryDetails.IsDeleted = 0
	OPTION( RECOMPILE );

	--Now insert the new records

	INSERT INTO RecHubData.factDataEntryDetails
    (
	    IsDeleted
        ,BankKey
        ,OrganizationKey
        ,ClientAccountKey
        ,DepositDateKey
        ,ImmutableDateKey
        ,SourceProcessingDateKey
        ,BatchID
        ,SourceBatchID
        ,BatchNumber
        ,BatchSourceKey
        ,BatchPaymentTypeKey
        ,DepositStatus
        ,WorkgroupDataEntryColumnKey
        ,TransactionID
        ,BatchSequence
        ,CreationDate
        ,ModificationDate
        ,DataEntryValueDateTime
        ,DataEntryValueFloat
        ,DataEntryValueMoney
        ,DataEntryValue
    )
	SELECT
	    0,
		BankKey,                
		OrganizationKey,        
		ClientAccountKey,       
		DepositDateKey,         
		ImmutableDateKey,
		SourceProcessingDateKey,    
		BatchID,                
		SourceBatchID,
		batchNumber,
		batchSourceKey,
		batchPaymentTypeKey,
		DepositStatus,     
		WorkgroupDataEntryColumnKey,       
		TransactionID  ,        
		BatchSequence ,
		@parmModificationDate        as CreationDate,
		@parmModificationDate        as ModificationDate,         
		DataEntryValueDateTime ,
		DataEntryValueFloat    ,
		DataEntryValueMoney,    
		DataEntryValue
    FROM #DEUpdate;

	IF @bLocalTransaction = 1 
	BEGIN
		COMMIT TRANSACTION 
		SET @bLocalTransaction = 0
	END

END TRY
BEGIN CATCH
	IF @bLocalTransaction = 1
	BEGIN --local transaction, handle the error
		IF (XACT_STATE()) = -1 --transaction is uncommittable
			ROLLBACK TRANSACTION
		IF (XACT_STATE()) = 1 --transaction is active and valid
			COMMIT TRANSACTION
		SET @bLocalTransaction = 0
	END
	ELSE
	BEGIN
		-- the transaction did not start here, so pass the error information up to the calling SP.
		DECLARE @ErrorMessage	NVARCHAR(4000),
				@ErrorSeverity	INT,
				@ErrorState		INT,
				@ErrorLine		INT
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorLine = ERROR_LINE()

		IF @@NESTLEVEL > 3 --update only if we got here by two service broker calls
			SET @ErrorMessage = 'usp_factDataEntryDetails_Upd_DataEntryValues (Line: %d)-> ' + @ErrorMessage
		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
END CATCH


