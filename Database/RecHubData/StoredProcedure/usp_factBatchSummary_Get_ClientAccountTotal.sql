--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_ClientAccountTotal
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_ClientAccountTotal') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountTotal
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountTotal 
(
       @parmSessionID			UNIQUEIDENTIFIER,	   
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID 	INT,
       @parmDepositDateStart		DATETIME,
       @parmDepositDateEnd		DATETIME,
       @parmMinDepositStatus		INT = 850,
       @parmPaymentTypeKey		INT = NULL,
       @parmPaymentSourceKey		INT = NULL
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/13/2009
*
* Purpose: Get lockbox DepositTotal based on the payment Type and
*		   deposit date.
*
* Modification History
* 03/13/2009 CR 25817 JMC	Created.
* 02/15/2009 CR 28727       Add new parameter to usp_GetLockboxTotal stored procedure in support of CSR Research.
* 03/29/2013 WI 90707 JBS	Update to 2.0 release. Change schema name.
*							Rename proc from usp_GetLockboxTotal.
*							Change parameter: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Change all references of Lockbox to ClientAccount.
*							Added IsDeleted = 0.
* 10/10/2013 WI 116701 EAS  Add Payment Type Filter to Client Account Total.
* 05/22/2014 WI 142821 PKW  Update for RAAM Integration.
* 12/09/2014 WI 181266 LA   Added Batch Payment Source Parameter
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDate	INT,
		@EndDate	INT;

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDate OUT,
		@parmEndDateKey = @EndDate OUT;

	SELECT 
		COALESCE(SUM(RecHubData.factBatchSummary.CheckTotal), 0) AS DepositTotal
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON						--WI 142821
			RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factBatchSummary.DepositDateKey >= @StartDate				--WI 142821
		AND RecHubData.factBatchSummary.DepositDateKey <= @EndDate					--WI 142821
		AND RecHubData.factBatchSummary.DepositStatus  >= @parmMinDepositStatus
		AND RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.factBatchSummary.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeKey, RecHubData.factBatchSummary.BatchPaymentTypeKey)
		AND RecHubData.factBatchSummary.BatchSourceKey = ISNULL(@parmPaymentSourceKey, RecHubData.factBatchSummary.BatchSourceKey)

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
