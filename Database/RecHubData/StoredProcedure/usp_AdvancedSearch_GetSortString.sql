IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSortString') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSortString
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSortString
(
	@parmSortBy			VARCHAR(32),
	@pamSortByDirection	VARCHAR(4),
	@parmSortString		NVARCHAR(2048) OUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/17/2019
*
* Purpose: Build the sort clause used by Adv Search
*
* Modification History
* 04/17/2019 R360-16301 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SortCommand NVARCHAR(2048);

BEGIN TRY

	DECLARE @ColumnList TABLE
	(
		RowID INT IDENTITY(1,1),
		ColumnName VARCHAR(64)
	);

	IF LEN(@parmSortBy) > 0
	BEGIN
		IF (UPPER(@parmSortBy) = 'BATCHDEPOSITDATE')
			INSERT INTO @ColumnList(ColumnName) VALUES('DepositDateKey'),('SiteBankID'),('SiteClientAccountID'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'BATCHBATCHID') OR (UPPER(@parmSortBy) = 'BATCHSOURCEBATCHID')
			INSERT INTO @ColumnList(ColumnName) VALUES('SourceBatchID'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'BATCHBATCHNUMBER')
			INSERT INTO @ColumnList(ColumnName) VALUES('BatchNumber'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'BATCHPAYMENTSOURCE')
			INSERT INTO @ColumnList(ColumnName) VALUES('#tmpBatchSources.LongName'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'BATCHPAYMENTTYPE')
			INSERT INTO @ColumnList(ColumnName) VALUES('#tmpBatchPaymentTypes.LongName'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSACCOUNT')
			INSERT INTO @ColumnList(ColumnName) VALUES('CheckAccountNumber'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSAMOUNT')
			INSERT INTO @ColumnList(ColumnName) VALUES('CheckAmount'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSBATCHSEQUENCE')
			INSERT INTO @ColumnList(ColumnName) VALUES('CheckBatchSequence'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSREMITTERNAME') OR (UPPER(@parmSortBy) = 'CHECKSPAYER')
			INSERT INTO @ColumnList(ColumnName) VALUES('RemitterName'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSRT')
			INSERT INTO @ColumnList(ColumnName) VALUES('NumericRoutingNumber'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSSERIAL')
			INSERT INTO @ColumnList(ColumnName) VALUES('NumericSerial'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'CHECKSTRANSACTIONCODE')
			INSERT INTO @ColumnList(ColumnName) VALUES('TransactionCode'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'TRANSACTIONSTRANSACTIONID')
			INSERT INTO @ColumnList(ColumnName) VALUES('TransactionID'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE IF (UPPER(@parmSortBy) = 'TRANSACTIONSTXNSEQUENCE')
			INSERT INTO @ColumnList(ColumnName) VALUES('TxnSequence'),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		ELSE
		BEGIN
			INSERT INTO @ColumnList(ColumnName) VALUES(@parmSortBy),('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		END
	END
	ELSE
		INSERT INTO @ColumnList(ColumnName) VALUES('SiteBankID'),('SiteClientAccountID'),('DepositDateKey'),('ImmutableDateKey'),('SourceBatchID'),('TransactionID'),('TxnSequence'),('COALESCE(StubsStubSequence,ChecksCheckSequence)');
		
	SET @parmSortString = '';

	SELECT @parmSortString += CASE WHEN LEFT(ColumnName,4) = '#tmp' OR LEFT(ColumnName,8) = 'COALESCE' THEN ColumnName ELSE '#tmpMatchingResults.' + ColumnName END + CASE RowID WHEN 1 THEN ' '+ @pamSortByDirection ELSE '' END + ',' FROM @ColumnList

	SET @parmSortString = SUBSTRING(@parmSortString,1,LEN(@parmSortString)-1);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH