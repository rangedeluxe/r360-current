--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_BySiteBankIDSiteClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_BySiteBankIDSiteClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_BySiteBankIDSiteClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_BySiteBankIDSiteClientAccountID 
(
      @parmSiteBankID			INT,
      @parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/19/2010
*
* Purpose: Retrieve all of the data entry fields for a given ClientAccount
*
* Modification History
* 02/19/2010 CR 28743 JMC	Created
* 11/07/2011 CR 47949 JNE	Removed 'Most Recent' flag from where statement.
* 04/18/2013 WI 90704 JBS	Update to 2.0 release.  Change schema to RecHubData
*							Rename proc From usp_GetLockboxDataEntryFieldsByLockbox
*							Change parameter: @parmSiteLockboxID to @parmSiteCLientAccountID
*							Change references: Lockbox to ClientAccount
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	DISTINCT RTRIM(RecHubData.dimDataEntryColumns.FldName) AS FldName,
			RTRIM(RecHubData.dimDataEntryColumns.TableName) AS TableName,
			CASE
				WHEN ((RecHubData.dimDataEntryColumns.TableType = 0 OR RecHubData.dimDataEntryColumns.TableType = 2) 
					AND RTRIM(RecHubData.dimDataEntryColumns.FldName) = 'Amount') 
					THEN 7
				ELSE RecHubData.dimDataEntryColumns.DataType
			END AS FldDataTypeEnum,
			COALESCE(RecHubData.dimDataEntryColumns.DisplayName, RecHubData.dimDataEntryColumns.FldName) AS Title
	FROM	RecHubData.dimDataEntryColumns
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns ON 
				RecHubData.dimDataEntryColumns.DataEntryColumnKey = RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey
			INNER JOIN RecHubData.dimClientAccounts ON 
				RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
	WHERE	RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
	ORDER BY Title;		

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
