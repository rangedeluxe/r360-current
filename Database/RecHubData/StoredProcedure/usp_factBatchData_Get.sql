--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchData_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchData_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchData_Get
(
	@parmBankID				INT,
	@parmClientAccountID	INT, 
	@parmImmutableDateKey	INT,
	@parmBatchID			BIGINT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/01/2011
*
* Purpose: Retrieve the data for factBatchData
*
* Modification History
* 04/01/2011 CR 33628  WJS	Created
* 04/12/2013 WI 90684  JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_GetFactBatchData
*							Change references: Lockbox to ClientAccount
*							Processing to Immutable.
*							Renamed Parameters: @parmLockboxID to @parmClientAccountID,
*							@parmProcessingDateKey to @parmImmutableDateKey.
* 05/19/2014 WI 142811 PKW  Update for RAAM Integration.
* 07/23/2014 WI 155466 CMC  Remove RAAM Integration.
* 03/02/2015 WI 193394 TWE  Only read valid records
*                           Return processingDateKey, BatchNumber, batchpaymenttype
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE 
	@BankKey			INT,
	@ClientAccountKey	INT,
	@DepositDateKey		INT,
	@BatchPaymentType   VARCHAR(30);

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmBankID,
		@parmSiteClientAccountID = @parmClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

		SELECT
			@BatchPaymentType = RecHubData.dimBatchPaymentTypes.ShortName			
		FROM 
			RecHubData.factBatchSummary	
			INNER JOIN RecHubData.dimBatchPaymentTypes
			    on RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey		
		WHERE
				RecHubData.factBatchSummary.ClientAccountKey = @ClientAccountKey
			AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		    AND RecHubData.factBatchSummary.DepositDateKey = @DepositDateKey
			AND RecHubData.factBatchSummary.IsDeleted = 0;
		
		SELECT
		    RecHubData.factBatchData.SourceProcessingDateKey,
			RecHubData.factBatchData.BatchNumber,
			@BatchPaymentType                     AS BatchPaymentType,
			RecHubData.dimBatchDataSetupFields.Keyword,
			RecHubData.factBatchData.DataValue
		FROM 
			RecHubData.factBatchData
			INNER JOIN RecHubData.dimBatchDataSetupFields ON
				RecHubData.dimBatchDataSetupFields.BatchDataSetupFieldKey = RecHubData.factBatchData.BatchDataSetupFieldKey
		WHERE
				RecHubData.factBatchData.ClientAccountKey = @ClientAccountKey
			AND RecHubData.factBatchData.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factBatchData.BatchID = @parmBatchID
		    AND RecHubData.factBatchData.DepositDateKey = @DepositDateKey
			AND RecHubData.factBatchData.IsDeleted = 0
		ORDER BY 
			RecHubData.dimBatchDataSetupFields.Keyword, 
			RecHubData.factBatchData.DataValue
		OPTION ( RECOMPILE );

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage	= ERROR_MESSAGE(),
				@ErrorSeverity	= ERROR_SEVERITY(),
				@ErrorState		= ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine		= ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH