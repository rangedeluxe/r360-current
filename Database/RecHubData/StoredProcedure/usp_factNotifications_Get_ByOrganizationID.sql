--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Get_ByOrganizationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_Get_ByOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_Get_ByOrganizationID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_Get_ByOrganizationID
(
	@parmBankID						INT,
	@parmOrganizationID				INT,
	@parmStartDate					DATETIME,
	@parmEndDate					DATETIME,
	@parmNotificationFileTypeKey	INT			= 0,
	@parmOrderBy					VARCHAR(20) = 'date',
	@parmOrderDir					VARCHAR(4)	= 'asc',
	@parmStartRecord				INT			= 1,
	@parmRecordsToReturn			INT			= 20,
	@parmTotalRecords				INT OUTPUT,
	@parmTimeZoneBias               INT         = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/07/2012
*
* Purpose: 
*
* Modification History
* 09/07/2012 CR 55457 JMC	Created
* 04/25/2013 WI 99084 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc From usp_factNotifications_GetByCustomerID
*							Rename parameter: @parmCustomerID to @parmOrganizationID
*							Change references: Lockbox to ClientAccount,
*							Customer to Organization
*							Forward Patch: WI 91652
* 10/22/2013 WI 118376 JMC	Modified to return and filter Notification Dates as UTC
* 11/08/2013 WI 121650 MLH  Added IsDeleted = 0 to filter RecHubData.factNotifications
******************************************************************************/

SET NOCOUNT ON;

BEGIN TRY

	-- FP: WI 91652
	/****************************************************************************
	*   Set the dates up for search
	****************************************************************************/
	DECLARE @StartDate DATETIME; 
	DECLARE @EndDate DATETIME;

	--adjust search Date Time by timecode bias.
	SET @StartDate = DATEADD(minute, -DATEDIFF(minute, GetDate(), GETUTCDATE()), DATEADD(dd, 0, DATEDIFF(dd, 0, DATEADD(minute, DATEDIFF(minute, GetDate(), GETUTCDATE()), @parmStartDate))))
	SET @EndDate = DATEADD(second, -1, DATEADD(minute, -DATEDIFF(minute, GetDate(), GETUTCDATE()), DATEADD(dd, 1, DATEDIFF(dd, 0, DATEADD(minute, DATEDIFF(minute, GetDate(), GETUTCDATE()), @parmEndDate)))))

	/****************************************************************************
	* We have to use a temp table because the actual query against the database 
	* needs to be dynamic so we can accomodate a dynamic ORDER BY clause.
	****************************************************************************/
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#FinalResults')) 
		DROP TABLE #FinalResults;

	CREATE TABLE #Notifications (
		factNotificationKey			INT,
		NotificationDateKey			INT,
		NotificationMessageGroup	INT,
		integraPAYNotificationID	UNIQUEIDENTIFIER,
		NotificationDate			DATETIME,
		NotificationFileCount		INT,
		MessageText					VARCHAR(MAX),
		NotificationMessagePart		INT,
		BankID						INT,
		OrganizationID				INT,
		OrganizationName			VARCHAR(20),
		ClientAccountID				INT,
		ClientAccountName			VARCHAR(40)
	);

	CREATE TABLE #FinalResults (
		ROW_ID						INT IDENTITY,
		NotificationDateKey			INT,
		NotificationMessageGroup	INT,
		NotificationDate			DATETIME,
		NotificationFileCount		INT,
		MessageText					VARCHAR(MAX),
		BankID						INT,
		OrganizationID				INT,
		OrganizationName			VARCHAR(20),
		ClientAccountID				INT,
		ClientAccountName			VARCHAR(40)
	);

	/****************************************************************************
	* The first thing we need to do is to populate the #Notifications temp 
	* table with all of the rows that fit the criteria for the data being 
	* requested.  We have to use a #temp table because we will be selecting
	* from this table from a dynamic query (which is required for the dynamic 
	* ORDER BY clause.
	****************************************************************************/

	-- Retrieving Lockbox Level Notifications
	-- SuperUser has access to all Lockboxes within an OLCustomer
	INSERT INTO #Notifications
	SELECT DISTINCT
		RecHubData.factNotifications.factNotificationKey,
		RecHubData.factNotifications.NotificationDateKey,
		RecHubData.factNotifications.NotificationMessageGroup,
		RecHubData.factNotifications.SourceNotificationID,
		RecHubData.factNotifications.NotificationDateTime			AS NotificationDate,
		RecHubData.factNotifications.NotificationFileCount,
		RecHubData.factNotifications.MessageText					AS MessageText,
		RecHubData.factNotifications.NotificationMessagePart,
		RecHubData.dimOrganizations.SiteBankID,
		RecHubData.dimOrganizations.SiteOrganizationID,
		RecHubData.dimOrganizations.Name							AS OrganizationName,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.dimClientAccounts.LongName						AS ClientAccountName
	FROM RecHubData.factNotifications
		INNER JOIN RecHubData.dimOrganizations ON 
			RecHubData.factNotifications.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
        LEFT OUTER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE 
		RecHubData.dimOrganizations.SiteBankID = @parmBankID
		AND RecHubData.dimOrganizations.SiteOrganizationID = @parmOrganizationID
		-- FP: WI 91652
		AND RecHubData.factNotifications.NotificationDateTime BETWEEN @StartDate and @EndDate
		AND RecHubData.factNotifications.NotificationMessagePart = 1
		AND RecHubData.factNotifications.IsDeleted = 0;

	IF @parmNotificationFileTypeKey > 0 
	BEGIN		
	
		DELETE FROM #Notifications
		WHERE #Notifications.NotificationMessageGroup
		NOT IN(
			SELECT #Notifications.NotificationMessageGroup
			FROM #Notifications
				INNER JOIN RecHubData.factNotificationFiles ON
					#Notifications.NotificationMessageGroup = RecHubData.factNotificationFiles.NotificationMessageGroup
			WHERE RecHubData.factNotificationFiles.NotificationFileTypeKey = @parmNotificationFileTypeKey);
	END

	/****************************************************************************
	* Now that the #temp table has been populated, we can re-run the query with
	* an ORDER BY clause applied.  This query will be loaded into a @TABLE 
	* variable which will then be paginated.
	****************************************************************************/
	;WITH PrelimResults AS
	(
		SELECT	factNotificationKey,
				CASE WHEN ClientAccountID > 0 THEN ClientAccountID ELSE OrganizationID END AS SourceID
		FROM #Notifications
	),
	SortedResults AS
	(
		SELECT	CASE 
					WHEN UPPER(@parmOrderBy)='DATE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationDate DESC, ClientAccountID, OrganizationID)
							ELSE ROW_NUMBER() OVER (ORDER BY NotificationDate ASC, ClientAccountID, OrganizationID)
						END
					WHEN UPPER(@parmOrderBy)='NOTIFICATIONSOURCE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY SourceID DESC, NotificationDate, OrganizationName, ClientAccountID)
							ELSE ROW_NUMBER() OVER (ORDER BY SourceID ASC, NotificationDate, CustomerName, ClientAccountID)
						END
					WHEN UPPER(@parmOrderBy)='MESSAGE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY MessageText DESC, NotificationDate, ClientAccountID, OrganizationName)
							ELSE ROW_NUMBER() OVER (ORDER BY MessageText ASC, NotificationDate, ClientAccountID, CustomerName)
						END
					WHEN UPPER(@parmOrderBy)='FILE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationFileCount DESC, NotificationDate, ClientAccountID, OrganizationName)
							ELSE ROW_NUMBER() OVER (ORDER BY NotificationFileCount ASC, NotificationDate, ClientAccountID, OrganizationName)
						END
					ELSE CASE WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationDate DESC, ClientAccountID, OrganizationName)
						ELSE ROW_NUMBER() OVER (ORDER BY NotificationDate ASC, ClientAccountID, OrganizationName)
					END
				END AS RecID,
				#Notifications.factNotificationKey,
				NotificationDateKey,
				NotificationMessageGroup,
				integraPAYNotificationID,
				NotificationDate,
				NotificationFileCount,
				MessageText,
				NotificationMessagePart,
				BankID,
				OrganizationID,
				OrganizationName,
				ClientAccountID,
				ClientAccountName,
				SourceID
		FROM	#Notifications
				INNER JOIN PrelimResults ON PrelimResults.factNotificationKey = #Notifications.factNotificationKey
	)
	INSERT INTO #FinalResults
	SELECT
		NotificationDateKey,
		NotificationMessageGroup,
		NotificationDate,
		NotificationFileCount,
		MessageText,
		BankID,
		OrganizationID,
		OrganizationName,
		ClientAccountID,
		ClientAccountName
	FROM	SortedResults
	WHERE RecID BETWEEN @parmStartRecord AND @parmStartRecord + @parmRecordsToReturn - 1
	ORDER BY RecID;

	/****************************************************************************
	* With the full recordset, we can set the variable that will contain the 
	* total number of rows selected by the query.
	****************************************************************************/
	SELECT @parmTotalRecords = COUNT(*) FROM #Notifications;

	/****************************************************************************
	* We won't need this anymore now that the @TABLE variable has been populated.
	****************************************************************************/
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;


	/****************************************************************************
	* Now we're going to loop through the final paginated recordset, and append
	* all of the additional message parts to the MessageParts column.
	****************************************************************************/
	DECLARE @ROW_COUNT INT;
	SELECT @ROW_COUNT = COUNT(*) FROM #FinalResults;

	IF @ROW_COUNT > 0
	BEGIN

		DECLARE @CURRENT_ROW INT;

		/* Used to compile all of the message parts for a Notification into a single 
		 * column which will be returned by the query.	*/
		DECLARE @MessageText VARCHAR(MAX);
		DECLARE @MessageGroup INT;

		-- Loop through each row in the final recordset so we can 
		-- process a row at a time.
		SET @CURRENT_ROW = 0
		WHILE( @parmTotalRecords > @CURRENT_ROW )
		BEGIN
			SELECT @MessageGroup = NotificationMessageGroup
			FROM #FinalResults
			WHERE ROW_ID = @CURRENT_ROW;
		
			SET @MessageText = ''
			SELECT	@MessageText = @MessageText + RecHubData.factNotifications.MessageText
			FROM	RecHubData.factNotifications
					INNER JOIN #FinalResults AS Notifications_Table 
						ON RecHubData.factNotifications.NotificationDateKey = Notifications_Table.NotificationDateKey
						AND RecHubData.factNotifications.NotificationMessageGroup = Notifications_Table.NotificationMessageGroup
			WHERE	Notifications_Table.NotificationMessageGroup = @MessageGroup
			ORDER BY RecHubData.factNotifications.NotificationMessagePart;
		
			-- Now that each MessagePart has been concatenated together, 
			-- we can update the final Recordset.
			UPDATE #FinalResults 
			SET MessageText = @MessageText 
			WHERE ROW_ID = @CURRENT_ROW;

			SET @CURRENT_ROW = @CURRENT_ROW + 1;
	
		END
	END

	/****************************************************************************
	* Everything's been selected, the recordset has been paginated, 
	* and each of the messageparts have been compiled.  
	****************************************************************************/
	-- FP: WI 91652
	SELECT
		NotificationMessageGroup,
		DATEADD(minute, DATEDIFF(minute, GetDate(), GETUTCDATE()), NotificationDate) AS NotificationDate,
		NotificationFileCount,
		MessageText,
		BankID,
		OrganizationID,
		OrganizationName,
		ClientAccountID,
		ClientAccountName
	FROM #FinalResults;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#FinalResults')) 
		DROP TABLE #FinalResults;

	-- Let's get out of here!

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#FinalResults')) 
		DROP TABLE #FinalResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH