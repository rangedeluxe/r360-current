--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_ByEntities_Active
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_ByEntities_Active') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByEntities_Active
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByEntities_Active
(
	@parmSessionID	UNIQUEIDENTIFIER,
	@parmEntities	XML,
	@parmBatchSource VARCHAR(30) = NULL --This is actually the import type
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/17/2014
*
* Purpose: Query dimClientAccounts for records that have corresponding
*			RecHubUser.OLWorkgroups tying them to the RAAM Entity ID
*
* Modification History
* 08/04/2014 WI 156972 KLC	Created
* 09/15/2014 WI 165728 EAS  Allow Batch Source filter
* 09/17/2014 WI 165728 KLC	Fixed stuff
* 10/22/2014 WI	171734 EAS	CTE filter on ImportTypeKey
* 11/06/2014 WI	165728 KLC	Updated sorting of workgroups to be by ID first
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	WITH CTE_OrderedEntities AS
	(	SELECT ROW_NUMBER() OVER (ORDER BY ENTS.E) EntityOrder,
				ENTS.E.value('.', 'INT') EntityID
		FROM @parmEntities.nodes('ENTS/ENT') as ENTS(E)
	),
	CTE_SessionsClientAccounts AS
	(
		SELECT DISTINCT CTE_OrderedEntities.EntityOrder,
				CTE_OrderedEntities.EntityID,
				RecHubUser.SessionClientAccountEntitlements.SiteBankID,
				RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
		FROM CTE_OrderedEntities
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON	CTE_OrderedEntities.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID
			LEFT OUTER JOIN RecHubData.ClientAccountBatchSources
				ON RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = RecHubData.ClientAccountBatchSources.ClientAccountKey
			LEFT OUTER JOIN RecHubData.dimBatchSources
				ON RecHubData.ClientAccountBatchSources.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
			LEFT OUTER JOIN RecHubData.dimImportTypes
				ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
					AND RecHubData.dimImportTypes.ShortName = @parmBatchSource--this really is the import type
		WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND (RecHubData.dimImportTypes.ImportTypeKey IS NOT NULL OR @parmBatchSource IS NULL)
	)
	SELECT	Entities.EntityOrder,
			Entities.EntityID,
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel
	FROM	CTE_SessionsClientAccounts Entities
		INNER JOIN RecHubUser.OLWorkgroups
			ON	Entities.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
				AND Entities.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
				AND Entities.EntityID = RecHubUser.OLWorkgroups.EntityID
		INNER JOIN RecHubData.dimClientAccountsView
			ON	RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
				AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
	WHERE	RecHubData.dimClientAccountsView.IsActive = 1
	ORDER BY Entities.EntityOrder ASC,
			RecHubData.dimClientAccountsView.SiteClientAccountID ASC,
			RecHubData.dimClientAccountsView.LongName ASC;
		
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

