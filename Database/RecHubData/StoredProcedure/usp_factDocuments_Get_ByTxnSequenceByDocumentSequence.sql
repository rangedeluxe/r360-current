--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_ByTxnSequenceByDocumentSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_ByTxnSequenceByDocumentSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get_ByTxnSequenceByDocumentSequence
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_ByTxnSequenceByDocumentSequence 
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142851
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,				--WI 142851
	@parmTransactionID			INT,
	@parmScannedChecks			INT 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JNE
* Date: 1/22/2010
*
* Purpose: 
*
* Modification History
* 01/22/2010 CR 28962 JNE	Created
* 07/12/2010 CR 29976 JMC	Add field to dataset in support of CRs 29951
* 03/18/2011 CR 33439 JNE	Add BatchSourceKey and ProcessingDate to return set.
* 03/01/2012 CR 50746 JNE	Add parmScannedChecks as parameter and use to
*				prevent Scanned checks documents if 0.
* 04/15/2013 WI 90648 JBS	Updates for 2.0. Moved to RecHubData schema.
*							Renamed from usp_factDocuments_GetByTransaction.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed all references: Lockbox to ClientAccount, 
*								Customer to Organization
*							Removed columns: factDocuments.GlobalDocumentID.
*							Removed ISNULL function from DocumentTypeDescription.
*							Moved recompile from SP to SELECT statement.
* 06/03/2014 WI 142851 DLD  Batch Collision/RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DepositDateKey INT,
		@StartDateKey INT,	   --WI 142851
        @EndDateKey INT;	   --WI 142851

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;
	
	/* Create the memory table with the column names used by the application. Keeps from using AS xxx in the final SELECT */
	DECLARE @ClientAccounts TABLE
	(
		ClientAccountKey INT,
		SiteBankID INT,
		SiteClientAccountID INT,
		SiteOrganizationID INT
	);

	INSERT INTO @ClientAccounts(ClientAccountKey,SiteBankID,SiteClientAccountID,SiteOrganizationID)
	SELECT	RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccounts.SiteOrganizationID
	FROM	RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.SessionClientAccountEntitlements																	--WI 142851
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey		--WI 142851
	WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID													--WI 142851
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID											--WI 142851
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID;							--WI 142851

 	SELECT 	CONVERT(CHAR(8), RecHubData.factDocuments.ImmutableDateKey, 112)	AS PICSDate,
			SiteBankID															AS BankID,
			SiteOrganizationID													AS OrganizationID,
			SiteClientAccountID													AS ClientAccountID,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.DepositDateKey), 112),101)	AS DepositDate,
			RecHubData.factDocuments.BatchID,
			RecHubData.factDocuments.SourceBatchID, --WI 142851
			RecHubData.factDocuments.TransactionID,
			RecHubData.factDocuments.SequenceWithinTransaction					AS TransactionSequence,
			RecHubData.factDocuments.DocumentSequence,
			RTRIM(RecHubData.dimDocumentTypes.FileDescriptor)					AS FileDescriptor,
			RecHubData.factDocuments.BatchSequence,
			RTRIM(RecHubData.dimDocumentTypes.DocumentTypeDescription)			AS [Description],
			RecHubData.factDocuments.BatchSourceKey,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.ImmutableDateKey), 112),101) AS ProcessingDate
	FROM 	RecHubData.factDocuments
			INNER JOIN @ClientAccounts CA
				ON CA.ClientAccountKey = RecHubData.factDocuments.ClientAccountKey
			INNER JOIN RecHubData.dimDocumentTypes 
				ON RecHubData.dimDocumentTypes.DocumentTypeKey = RecHubData.factDocuments.DocumentTypeKey 
	WHERE 	RecHubData.factDocuments.DepositDateKey = @StartDateKey							    --WI 142851
		AND RecHubData.dimDocumentTypes.FileDescriptor <> 'C'									--WI 142851
		AND NOT(RecHubData.dimDocumentTypes.FileDescriptor = 'SC' AND @parmScannedChecks = 0)  	--WI 142851
		AND RecHubData.factDocuments.BatchID = @parmBatchID
		AND RecHubData.factDocuments.TransactionID = @parmTransactionID
		AND RecHubData.factDocuments.IsDeleted = 0
	ORDER BY 
			RecHubData.factDocuments.TxnSequence,
			RecHubData.factDocuments.DocumentSequence ASC 
	OPTION (RECOMPILE);

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH