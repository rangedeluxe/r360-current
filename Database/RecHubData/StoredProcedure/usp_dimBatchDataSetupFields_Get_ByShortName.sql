--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBatchDataSetupFields_Get_ByShortName
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchDataSetupFields_Get_ByShortName
(
	@parmImportShortName VARCHAR(30),
	@parmModificationDate DATETIME = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/8/2011
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 03/8/2011 CR  33258 WJS	Created
* 05/11/2012 CR 52654 WJS   Add Modification date
* 02/10/2014 WI 129075 JBS	Update to 2.01 Schema. Renamed from usp_dimBatchDataSetupFields_GetKeyWords 
* 04/17/2014 WI 137498 CMC	Adding permission tags. 
* 06/20/2014 WI 148860 JPB	Replace BatchSource with ImportType
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT	BatchDataSetupFieldKey,
			Keyword, 
			DataType 
	FROM 	RecHubData.dimBatchDataSetupFields 
			INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimImportTypes.ImportTypeKey = RecHubData.dimBatchDataSetupFields.ImportTypeKey
	WHERE 	ShortName=@parmImportShortName
		     AND (
                        (@parmModificationDate IS NULL AND RecHubData.dimBatchDataSetupFields.ModificationDate = RecHubData.dimBatchDataSetupFields.ModificationDate) OR  
                        (@parmModificationDate IS NOT NULL AND RecHubData.dimBatchDataSetupFields.ModificationDate > @parmModificationDate)
                  )

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
