--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_Document
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_Document') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_Document
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_Document 
(
		@parmSessionID				UNIQUEIDENTIFIER,	--WI 142822
		@parmSiteBankID				INT, 
		@parmSiteClientAccountID	INT,
		@parmDepositDate			DATETIME,
		@parmBatchID				BIGINT,				--WI 142822
		@parmTransactionID			INT,
		@parmBatchSequence			INT,
		@parmDisplayScannedCheck	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, ClientAccount, 
*          Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 11/16/2009 CR 28263 JMC	Added ProcessingDate to the returned dataset
* 11/16/2009 CR 28314 JMC	Added OnlineColorMode AS LockboxColorMode to the 
*                           returned dataset
* 03/14/2012 CR 51243 WJS   Add BatchSiteCode
* 04/09/2012 CR 51906 JNE	Add BatchSiteCode to SQL in else statement
* 06/21/2012 CR 53615 JNE	Add BatchCueID to result set.
* 07/20/2012 CR 54157 JNE	Add BatchNumber to result set.
* 03/28/2013 WI 90680 JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_GetDocument.
*							Rename SiteCode to SiteCodeID.
*							Change parameter: 
*                             @parmSiteLockboxID to @parmSiteClientAccountID
*							Change all references from Lockbox to ClientAccount.
*							Pulled date conversion from where clause and also 
*                             eliminated dimDates reference.
*							Removed GlobalBatchID and GlobalDocumentID.
*							Added performance tuning logic and changed 
*                             FileDescriptor split. (similar to 
*                             usp_GetBatchDocuments
*                             (usp_factBatchSummary_Get_BatchDocuments)
* 10/17/2013 WI 117652 EAS  Do not convert PICSDate to DateTime.
* 05/28/2014 WI 142822 DLD  Batch Collision/RAAM Integration.
* 12/05/2014 WI 180786 LA  Added BatchShortName and ImageTypeShortName to output 	
* 12/12/2014 WI 181673 LA  Stored Procedure - Document images - Batch: and Batch ID: values are incorrect 	
* 03/24/2014 WI 197816 MAA Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead
* 06/09/2015 WI 217489 MAA Excluded deleted documents
******************************************************************************/
SET NOCOUNT ON;

DECLARE @CheckDocumentKey INT,
		@ScannedCheckDocumentKey INT,
		@StartDateKey INT,  --WI 142822
        @EndDateKey INT;    --WI 142822

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ --WI 142822
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	/* Get the document keys here to be used in the CTE */
	SELECT @CheckDocumentKey = DocumentTypeKey
	FROM	RecHubData.dimDocumentTypes
	WHERE	RecHubData.dimDocumentTypes.FileDescriptor = 'C';

	SELECT @ScannedCheckDocumentKey = DocumentTypeKey
	FROM	RecHubData.dimDocumentTypes
	WHERE	RecHubData.dimDocumentTypes.FileDescriptor = 'SC';

	/* Create the memory table with the column names used by the application. Keeps from using AS xxx in the final SELECT */
	DECLARE @ClientAccounts TABLE
	(
		ClientAccountKey	INT,
		BankID				INT,
		ClientAccountID		INT,
		SiteCodeID			INT,
		ClientAccountColorMode TINYINT
	);

	INSERT INTO @ClientAccounts
		(ClientAccountKey,
		 BankID,
		 ClientAccountID,
		 SiteCodeID,
		 ClientAccountColorMode
		 )
	SELECT	RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode
	FROM	RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.SessionClientAccountEntitlements																--WI 142822
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142822
	WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID												--WI 142822
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID										--WI 142822
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID;						--WI 142822

	;WITH Documents AS
	(
		SELECT	RecHubData.factDocuments.BatchID,
				RecHubData.factDocuments.SourceBatchID, --WI 142822
				CLA.BankID,
				CLA.ClientAccountID,
				CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factDocuments.DepositDateKey), 112),101)		AS DepositDate,
				CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factDocuments.ImmutableDateKey), 112),101)	AS ImmutableDate,
				CONVERT(VARCHAR(10),RecHubData.factDocuments.ImmutableDateKey)                                      AS PICSDate,
				RecHubData.factDocuments.TransactionID,
				RecHubData.factDocuments.TxnSequence,
				RecHubData.factDocuments.DocumentTypeKey,
				RecHubData.factDocuments.BatchSequence,
				RecHubData.factDocuments.DocumentSequence,
				RecHubData.factDocuments.BatchSiteCode,
				CLA.SiteCodeID,
				CLA.ClientAccountColorMode,
				RecHubData.factDocuments.BatchCueID,
				RecHubData.factDocuments.BatchNumber,
				RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
				RecHubData.dimImportTypes.ShortName AS ImportTypeShortName			
		FROM	RecHubData.factDocuments
				INNER JOIN @ClientAccounts CLA ON RecHubData.factDocuments.ClientAccountKey = CLA.ClientAccountKey
				INNER JOIN RecHubData.dimBatchSources ON RecHubData.factDocuments.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey				
				INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
		WHERE  	RecHubData.factDocuments.DepositDateKey = @EndDateKey
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.TransactionID = @parmTransactionID				
				AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
				AND RecHubData.factDocuments.IsDeleted  = 0
				AND ( 
						(	@parmDisplayScannedCheck > 0 
							AND RecHubData.factDocuments.DocumentTypeKey <> @CheckDocumentKey
						)	 
						OR 
						(
								@parmDisplayScannedCheck = 0 
								AND RecHubData.factDocuments.DocumentTypeKey <> @CheckDocumentKey
								AND RecHubData.factDocuments.DocumentTypeKey <> @ScannedCheckDocumentKey
						) 
					)
	)
	SELECT	BatchID,
			SourceBatchID, --WI 142822
			BatchNumber,
			BankID,
			ClientAccountID,
			DepositDate,
			ImmutableDate,
			PICSDate,
			TransactionID,
			TxnSequence,
			RTRIM(RecHubData.dimDocumentTypes.FileDescriptor) AS FileDescriptor,
			BatchSequence,
			DocumentSequence,
			SiteCodeID,
			ClientAccountColorMode ,
			BatchSourceShortName,
			ImportTypeShortName
	FROM	Documents
			INNER JOIN RecHubData.dimDocumentTypes ON Documents.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
