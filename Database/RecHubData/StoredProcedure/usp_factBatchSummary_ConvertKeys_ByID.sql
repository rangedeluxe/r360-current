--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_ConvertKeys_ByID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_ConvertKeys_ByID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_ConvertKeys_ByID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_ConvertKeys_ByID
       @parmSiteBankID	INT,
	   @parmSiteOrganizationID INT,
	   @parmSiteClientAccountID INT,
	   @parmBatchID INT,
	   @parmImmutableDate varchar(32),
	   @parmDepositDate varchar(32),
	   @parmBatchSourceKey varchar(32)
      
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/09/2011
*
* Purpose: Convert the DBO site value to the OLTA key values
*
* Modification History
* 03/09/2011 CR 33278 WJS	Created.
* 12/01/2011 CR 48538 JPB	Speed Improvements.
* 03/19/2013 WI 90596 JBS	Update to 2.0 release. Change Schema Name. 
*							Change proc name from OLTA.usp_ConvertDBOKeysToOLTAKeys_byID
*							Changed Variables:  @iCustomerKey to @iOrganizationKey,
*							@iLockboxKey to @iClientAccountKey, @iProcessingDateKey to @iImmutableDateKey.
*							Changed references to tables and views:  dimCustomersView to dimOrganizationsView,
*							dimLockboxesView to dimClientAccountsView,  dimLockboxes to dimClientAccounts.
*							Changed references to columns:  CustomerKey to OrganizationKey,
*							SiteCustomerKey to SiteOrganizationKey,  LockboxKey to ClientAccountKey,
*							SiteLockboxID to ClientAccountID,  ProcessingDateKey to ImmutableDateKey,
*							@parmSiteCustomerID to @parmSiteOrganizationID, @parmSiteLockboxID to @parmSiteClientAccountID,
*							@parmProcessingDate to @parmImmutableDate.					
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON
SET XACT_ABORT ON

DECLARE @iBankKey INT,
		@iOrganizationKey INT,
		@iClientAccountKey INT,
		@iImmutableDateKey INT,
		@iDepositDateKey INT ,
		@iBatchSourceKey TINYINT

BEGIN TRY
	
	 SELECT	@iBankKey = RecHubData.dimBanksView.BankKey,
			@iOrganizationKey = RecHubData.dimOrganizationsView.OrganizationKey,
			@iClientAccountKey = RecHubData.dimClientAccountsView.ClientAccountKey,
			@iBatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	 FROM  RecHubData.dimBanksView 
			LEFT JOIN RecHubData.dimOrganizationsView ON RecHubData.dimOrganizationsView.SiteBankID	= @parmSiteBankID
				AND RecHubData.dimOrganizationsView.SiteOrganizationID = @parmSiteOrganizationID
			INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = @parmSiteBankID 
				AND RecHubData.dimClientAccountsView.SiteClientAccountID	= @parmSiteClientAccountID
			LEFT JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.ShortName = @parmBatchSourceKey

	--Select the Date Key for the ProcessingDate  CR 48538
	SELECT	@iImmutableDateKey = RecHubData.dimDates.DateKey
	FROM	RecHubData.dimDates
	WHERE	RecHubData.dimDates.CalendarDate = CAST(CONVERT(VARCHAR,@parmImmutableDate,101) AS DATETIME)
	--Select the Date Key for the DepositDate CR 48538
	SELECT	@iDepositDateKey = RecHubData.dimDates.DateKey
	FROM	RecHubData.dimDates
	WHERE	RecHubData.dimDates.CalendarDate = CAST(CONVERT(VARCHAR,@parmDepositDate,101) AS DATETIME)

	IF @iBatchSourceKey IS NULL
		SELECT @iBatchSourceKey = BatchSourceKey FROM RecHubData.dimBatchSources WHERE ShortName = 'Unknown'

	--CR 29954 deposit date not provided, so look it up accounting for changing keys
	IF @iDepositDateKey IS NULL
		SELECT	@iDepositDateKey = DepositDateKey
		FROM	RecHubData.factBatchSummary
				INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
				INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE	RecHubData.dimBanks.SiteBankID = @parmSiteBankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
				AND RecHubData.factBatchSummary.ImmutableDateKey = @iImmutableDateKey
				AND RecHubData.factBatchSummary.BatchID = @parmBatchID
				
	--if this batch exists, use the keys from batch	
	SELECT	@iBankKey = COALESCE(RecHubData.dimBanks.BankKey,@iBankKey),
			@iOrganizationKey = COALESCE(RecHubData.dimOrganizations.OrganizationKey,@iOrganizationKey),
			@iClientAccountKey = COALESCE(RecHubData.dimClientAccounts.ClientAccountKey,@iClientAccountKey)
	FROM	RecHubData.factBatchSummary
			INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
			INNER JOIN RecHubData.dimOrganizations ON RecHubData.factBatchSummary.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE	RecHubData.dimBanks.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.factBatchSummary.ImmutableDateKey = @iImmutableDateKey
			AND RecHubData.factBatchSummary.DepositDateKey = @iDepositDateKey
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
	
	SELECT @iBankKey, @iOrganizationKey, @iClientAccountKey, @iImmutableDateKey, @iDepositDateKey, @iBatchSourceKey
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH