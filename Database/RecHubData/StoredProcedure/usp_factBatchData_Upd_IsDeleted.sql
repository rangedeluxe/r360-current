--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchData_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchData_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchData_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchData_Upd_IsDeleted
(
	@parmDepositDateKey				INT,
	@parmBatchID					BIGINT,					--WI 142813
	@parmModificationDate			DATETIME
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/10/2011
*
* Purpose: Deletes factBatchData.
*
* Modification History
* 03/10/2011 CR 33233  WJS	Created.
* 01/19/2012 CR 49137  JPB	Lookup DepositDateKey before delete.
* 04/21/2014 WI 137833 CMC	Update to 2.0 release.  Change schema to RecHubData.
* 06/05/2014 WI 142813 PKW  Batch Collision.
*							Removed @parmBankKey,@parmBankKey,@parmOrganizationKey,
*									@parmClientAccountKey, @parmImmutableDateKey,
*									@parmSourceProcessingDateKey.
*							Removed tag WFSScriptProcessorQuotedIdentifierOn.
******************************************************************************/

BEGIN TRY

	UPDATE	RecHubData.factBatchData
	SET		RecHubData.factBatchData.IsDeleted = 1,
			RecHubData.factBatchData.ModificationDate = @parmModificationDate

	WHERE	
		RecHubData.factBatchData.DepositDateKey = @parmDepositDateKey		--WI 142813
		AND RecHubData.factBatchData.BatchID = @parmBatchID					--WI 142813
		AND RecHubData.factBatchData.IsDeleted = 0							--WI 142813

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException 
END CATCH