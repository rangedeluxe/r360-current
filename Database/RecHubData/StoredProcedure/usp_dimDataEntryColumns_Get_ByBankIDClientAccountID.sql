--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_ByBankIDClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_ByBankIDClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByBankIDClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByBankIDClientAccountID
(
    @parmSiteBankID			INT,
    @parmSiteClientAccountID INT,
    @parmSessionID			UNIQUEIDENTIFIER,
    @parmBatchSourceName	VARCHAR(30) = 'All'
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: CEJ
* Date: 08/12/2014
*
* Purpose: Select the Data Entry Columns associated with a given Site Bank ID and Site Client Account ID
*            This is only used by the Advanced Search page as of this time,  If this changes please upate this comment
*            to include the other uses.
*
* Modification History
* 08/12/2014 WI 158219 CEJ	Create new stored proc that takes Session ID, Bank ID and Client Account ID and returns the same result set as RecHubData.usp_dimDataEntryColumns_Get_ByOLClientAccountID
*					   JBS	Restoring to original Distinct result set and sort order.
* 11/20/2104 WI	179114 JBS	Add check to determine if BracketTitles will be used.
* 04/01/2015 WI 199088 JPB	Replace OLTA with R360 as section name in SystemSetup lookup.
* 07/22/2015 WI 224353 BLR  Updated for new workgroupdataentrycolumns table.
* 08/10/2015 WI 228861 MGE  Updated to return UILable preferentially over sourcedisplayname
* 08/12/2015 WI 227841 JAW  Removed duplication; only return rows where IsActive=1
******************************************************************************/
SET NOCOUNT ON;

DECLARE @BracketTitle SMALLINT;
SET @BracketTitle = 1;		-- Set to 1 as Default
BEGIN TRY

    SELECT
        @BracketTitle = COALESCE(CAST(Value AS SMALLINT),1)   -- Defaults to 1 if System record is NULL
    FROM
        RecHubConfig.SystemSetup
    WHERE
        Section = 'R360' AND
        SetupKey = 'BracketTitle';

    IF EXISTS(
        SELECT RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
        FROM RecHubUser.SessionClientAccountEntitlements
        WHERE RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
            AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
            AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID)
    BEGIN
        WITH CTE_AssignedDataEntryColumns AS
        (
            SELECT	ROW_NUMBER() OVER ( PARTITION BY RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
                ORDER BY RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey DESC ) AS RowNumber,
                    RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
            FROM RecHubData.dimWorkgroupDataEntryColumns
            WHERE RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = @parmSiteBankID
                AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = @parmSiteClientAccountID
        )
        SELECT	DISTINCT
                RecHubData.dimWorkgroupDataEntryColumns.DataType,
                RecHubData.dimWorkgroupDataEntryColumns.FieldName AS FldName,
                CASE @BracketTitle
                    WHEN 0 THEN ''
                    ELSE '[' + CASE
                        WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 THEN 'PMT'
                        ELSE 'INV'
                    END + '].'
                END + COALESCE(RecHubData.dimWorkgroupDataEntryColumns.UILabel, RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName) AS DisplayName,
                RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey,
                CASE RecHubData.dimWorkgroupDataEntryColumns.IsCheck
                    WHEN 1 THEN 'Checks'
                    ELSE 'Stubs'
                END AS 'TableName'
        FROM CTE_AssignedDataEntryColumns
            INNER JOIN RecHubData.dimWorkgroupDataEntryColumns
                ON CTE_AssignedDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
        WHERE CTE_AssignedDataEntryColumns.RowNumber = 1
            AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
        ORDER BY DisplayName ASC;
    END

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

