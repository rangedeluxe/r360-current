﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Upd') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Upd
(
	@parmClientAccountKey		INT,
	@parmIsActive				SMALLINT,
	@parmDataRetentionDays		SMALLINT,
	@parmImageRetentionDays		SMALLINT,
	@parmShortName				VARCHAR(20),
	@parmLongName				VARCHAR(40),
	@parmFileGroup				VARCHAR(256),
	@parmEntityID				INT,
	@parmViewingDays			INT,
	@parmMaximumSearchDays		INT,
	@parmCheckImageDisplayMode	TINYINT,
	@parmDocumentImageDisplayMode	TINYINT,
	@parmHOA					BIT,
	@parmDisplayBatchID			BIT,
	@parmInvoiceBalancingOption	TINYINT,
	@parmUserID					INT,
	@parmUserDisplayName		VARCHAR(256) = NULL,
	@parmDDAs					XML,
	@parmDataEntryColumns		XML,
	@parmDeadLineDay			SMALLINT,
	@parmRequiresInvoice		BIT,
	@parmBusinessRules			XML,
	@parmFIs					XML,		-- <FIs><ID>1</ID><ID>2</ID>...</FIs>
	@parmNewClientAccountKey	INT				OUTPUT,
	@parmRAAMNotificationRequried	BIT			OUTPUT,
	@parmBillingAccount				VARCHAR(20) = NULL,
	@parmBillingField1				VARCHAR(20) = NULL,
	@parmBillingField2				VARCHAR(20) = NULL,
	@parmEntityName					VARCHAR(50) = NULL,
	@parmBusinessRulesInvoiceRequired		BIT = NULL,
	@parmBusinessRulesBalancingRequired		BIT = NULL,
	@parmBusinessRulesPDBalancingRequired	BIT = NULL,
	@parmBusinessRulesPDPayerRequired		BIT = NULL

)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2014-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 03/21/2014
*
* Purpose: Update dimClientAccounts table
*
* Modification History
* 03/21/2014 WI 130465 KLC	Created
* 10/24/2014 WI 174083 KLC	Updated to pass through requires invoice
* 01/20/2015 WI 185218 RDS	Updated to include billing fields
* 03/19/2015 WI 196680 BLR  No longer filtering by Session Entitlements, we'll
*                           be doing this after the call.
* 04/28/2015 WI 203758 JPB	DataRetentionDays/ImageRetentionDays changing attributes
*							Correct IsActive for historical change.
* 06/18/2015 WI 218963 BLR  Commented out the call to RecHubException.usp_DataEntrySetups_Merge
*                           for 2.01.
* 09/01/2015 WI 233200 MAA Added @parmEntityName to be passed to usp_OLWorkgroup_Merge for auditing
* 12/16/2016 #127604133  JAW  Added parameter @parmUserDisplayName to include in IsRequired audit
* 01/12/2017 #132047521  JAW  Added parameter @parmBusinessRulesPaymentsOnlyTransaction
* 02/03/2017 PT 138212537 JBS  Add parameter @parmBusinessRulesBalancingRequired
* 03/28/2017 PT#142143773 MAA  Changed PaymentsOnlyTransaction to InvoiceRequired
* 07/14/2017 PT#141767379 MAA  Added PostDepositBalancingRequired
* 01/18/2019 R360-15291	  JPB  PostDepositPayerRequired
******************************************************************************/
SET NOCOUNT ON;

DECLARE @auditMessage			VARCHAR(1024),
		@auditColumns			RecHubCommon.AuditValueChangeTable,
		@errorDescription		VARCHAR(1024),
		@prevSiteCodeID			INT,
		@prevSiteOrganizationID INT,
		@prevCutOff				TINYINT,
		@prevOnlineColorMode	TINYINT,
		@prevIsCommingled		BIT,
		@prevDataRetentionDays	INT,
		@prevImageRetentionDays INT,
		@prevSiteClientAccountKey UNIQUEIDENTIFIER,
		@prevShortName			VARCHAR(20),
		@prevLongName			VARCHAR(40),
		@prevPOBox				VARCHAR(32),
		@prevDDA				VARCHAR(40),
		@prevFileGroup			VARCHAR(256),
		@LocalTransaction		BIT = 0;

BEGIN TRY
	SET @parmNewClientAccountKey = NULL;
	SET @parmRAAMNotificationRequried = 0;

	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	DECLARE @curTime					DATETIME	= GETDATE(),
			@isHistoricalChange			BIT			= NULL,
			@isMostRecent				BIT			= NULL,
			@isChanging					BIT			= NULL,
			@prevIsActive				TINYINT,
			@siteBankID					INT,
			@siteClientAccountID		INT;

	-- Normalize Billing Fields; do not store blank values
	IF LEN(@parmBillingAccount) = 0	SET @parmBillingAccount = NULL;
	IF LEN(@parmBillingField1) = 0	SET @parmBillingField1 = NULL;
	IF LEN(@parmBillingField2) = 0	SET @parmBillingField2 = NULL;

	SELECT TOP 1
			@isHistoricalChange		= CASE WHEN CAST(ISNULL(@parmLongName, '') AS VARBINARY(40)) <> CAST(ISNULL(LongName, '') AS VARBINARY(40))
											OR CAST(@parmShortName AS VARBINARY(20)) <> CAST(ShortName AS VARBINARY(20))
											OR ISNULL(@parmFileGroup, '') <> ISNULL([FileGroup], '')
										THEN 1
										ELSE 0
										END,
			@isMostRecent			= MostRecent,
			@isChanging				= CASE WHEN @parmDataRetentionDays <> DataRetentionDays
											OR @parmImageRetentionDays <> ImageRetentionDays
											OR @parmIsActive <> IsActive 
										THEN 1 
										ELSE 0 
										END,
			@prevIsActive			= RecHubData.dimClientAccounts.IsActive,
			@siteBankID				= RecHubData.dimClientAccounts.SiteBankID,
			@siteClientAccountID	= RecHubData.dimClientAccounts.SiteClientAccountID
	FROM	
		RecHubData.dimClientAccounts
	WHERE	
		RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;

	IF @isMostRecent IS NULL
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, ClientAccountKey (' + CAST(@parmClientAccountKey AS VARCHAR) + ') does not exist.';
		RAISERROR(@errorDescription, 16, 1);
	END
	
	--Verify the bank belongs to one of the user's provided entities
	IF NOT EXISTS(	SELECT	RecHubData.dimBanksView.BankKey
					FROM	RecHubData.dimBanksView
						INNER JOIN @parmFIs.nodes('/FIs/ID') FI(x) ON RecHubData.dimBanksView.EntityID = FI.x.value('.', 'INT')
					WHERE	RecHubData.dimBanksView.SiteBankID = @siteBankID )
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, bank (' + ISNULL(CAST(@siteBankID AS VARCHAR(10)), 'NULL') + ') does not exist or user does not have permissions';
		RAISERROR(@errorDescription, 16, 2);
	END

	IF @isMostRecent <> 1
	BEGIN
		SET @errorDescription = 'Unable to update RecHubData.dimClientAccounts record, ClientAccountKey (' + CAST(@parmClientAccountKey AS VARCHAR) + ') is no longer the most recent.';
		RAISERROR(@errorDescription, 16, 1);
	END

	IF @isHistoricalChange = 1
	BEGIN
		UPDATE	
			RecHubData.dimClientAccounts
		SET		
			@prevSiteCodeID				= RecHubData.dimClientAccounts.SiteCodeID,
			@prevSiteOrganizationID		= RecHubData.dimClientAccounts.SiteOrganizationID,
			@prevCutOff					= RecHubData.dimClientAccounts.CutOff,
			@prevOnlineColorMode		= RecHubData.dimClientAccounts.OnlineColorMode,
			@prevIsCommingled			= RecHubData.dimClientAccounts.IsCommingled,
			@prevDataRetentionDays		= RecHubData.dimClientAccounts.DataRetentionDays,
			@prevImageRetentionDays		= RecHubData.dimClientAccounts.ImageRetentionDays,
			@prevSiteClientAccountKey	= RecHubData.dimClientAccounts.SiteClientAccountKey,
			@prevShortName				= RecHubData.dimClientAccounts.ShortName,
			@prevLongName				= RecHubData.dimClientAccounts.LongName,
			@prevPOBox					= RecHubData.dimClientAccounts.POBox,
			@prevDDA					= RecHubData.dimClientAccounts.DDA,
			@prevFileGroup				= RecHubData.dimClientAccounts.[FileGroup],
			RecHubData.dimClientAccounts.MostRecent			= 0,
			RecHubData.dimClientAccounts.IsActive			= RecHubData.dimClientAccounts.IsActive,
			RecHubData.dimClientAccounts.ModificationDate	= GETDATE()
		WHERE	
			RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;

		INSERT INTO RecHubData.dimClientAccounts
		(
			SiteCodeID,
			SiteBankID,
			SiteOrganizationID,
			SiteClientAccountID,
			MostRecent,
			IsActive,
			CutOff,
			OnlineColorMode,
			IsCommingled,
			DataRetentionDays,
			ImageRetentionDays,
			CreationDate,
			ModificationDate,
			SiteClientAccountKey,
			ShortName,
			LongName,
			POBox,
			DDA,
			[FileGroup]
		)
		SELECT
			@prevSiteCodeID,
			@siteBankID,
			@prevSiteOrganizationID,
			@siteClientAccountID,
			1,
			@parmIsActive,
			@prevCutOff,
			@prevOnlineColorMode,
			@prevIsCommingled,
			@parmDataRetentionDays,
			@parmImageRetentionDays,
			@curTime,
			@curTime,
			@prevSiteClientAccountKey,
			@parmShortName,
			@parmLongName,
			@prevPOBox,
			@prevDDA,
			@parmFileGroup;

		SET @parmNewClientAccountKey = SCOPE_IDENTITY();

		IF @isChanging = 1
		BEGIN
			/*
				It is possible to have histical and changing attributes updated at the same time. If that is
				the case, the @isHistoricalChange branch is taken. So the non changing attributes have to 
				be updated on the previous records to ensure they are the same.
			*/
			UPDATE 
				RecHubData.dimClientAccounts
			SET 
				RecHubData.dimClientAccounts.DataRetentionDays = RecHubData.dimClientAccountsView.DataRetentionDays,
				RecHubData.dimClientAccounts.ImageRetentionDays = RecHubData.dimClientAccountsView.ImageRetentionDays
			FROM
				RecHubData.dimClientAccounts
				INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
					AND  RecHubData.dimClientAccountsView.SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID
					AND  RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			WHERE
				RecHubData.dimClientAccountsView.ClientAccountKey = @parmNewClientAccountKey
				AND RecHubData.dimClientAccounts.MostRecent = 0;
		END

		INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ClientAccountKey', CAST(@parmClientAccountKey AS VARCHAR(10)), CAST(@parmNewClientAccountKey AS VARCHAR(10)));
		IF @prevDataRetentionDays <> @parmDataRetentionDays
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DataRetentionDays', CAST(@prevDataRetentionDays AS VARCHAR(10)), CAST(@parmDataRetentionDays AS VARCHAR(10)));
		IF @prevImageRetentionDays <> @parmImageRetentionDays
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ImageRetentionDays', CAST(@prevImageRetentionDays AS VARCHAR(10)), CAST(@parmImageRetentionDays AS VARCHAR(10)));
		IF @parmIsActive <> @prevIsActive
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsActive', CAST(@prevIsActive AS VARCHAR(1)), CAST(@parmIsActive AS VARCHAR(1)));
		IF CAST(@parmShortName AS VARBINARY(20)) <> CAST(@prevShortName AS VARBINARY(20))
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ShortName', @prevShortName, @parmShortName);
		IF CAST(ISNULL(@parmLongName, '') AS VARBINARY(40)) <> CAST(ISNULL(@prevLongName, '') AS VARBINARY(40))
		BEGIN
			SET @parmRAAMNotificationRequried = 1;
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('LongName', ISNULL(@prevLongName, ''), ISNULL(@parmLongName, ''));
		END
		IF CAST(ISNULL(@prevFileGroup, '') AS VARBINARY(256)) <> CAST(ISNULL(@parmFileGroup, '') AS VARBINARY(256))
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FileGroup', ISNULL(@prevFileGroup, 'NULL'), ISNULL(@parmFileGroup, 'NULL'))

		-- Audit the RecHubData.dimClientAccounts insert
		SET @auditMessage = 'Modified dimClientAccounts for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' SiteBankID: ' + CAST(@siteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@siteClientAccountID AS VARCHAR(10))
			+ ' new ClientAccountKey: ' + CAST(@parmNewClientAccountKey AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_dimClientAccounts_Upd',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimClientAccounts',
			@parmColumnName			= 'ClientAccountKey',
			@parmAuditValue			= @parmNewClientAccountKey,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END
	ELSE IF @isChanging = 1
	BEGIN
		SELECT
			@prevDataRetentionDays = DataRetentionDays,
			@prevImageRetentionDays = ImageRetentionDays,
			@prevIsActive = IsActive
		FROM
			RecHubData.dimClientAccounts
		WHERE
			RecHubData.dimClientAccounts.ClientAccountKey	= @parmClientAccountKey;

		/*	First update the current record with the new values. */
		UPDATE	
			RecHubData.dimClientAccounts
		SET		
			RecHubData.dimClientAccounts.DataRetentionDays	= @parmDataRetentionDays,
			RecHubData.dimClientAccounts.ImageRetentionDays = @parmImageRetentionDays,
			RecHubData.dimClientAccounts.IsActive			= @parmIsActive,
			RecHubData.dimClientAccounts.ModificationDate	= @curTime
		WHERE	
			RecHubData.dimClientAccounts.ClientAccountKey	= @parmClientAccountKey;

		/* Now update all the existing records with the new retention values if they changed */
		IF( @prevDataRetentionDays <> @parmDataRetentionDays OR @prevImageRetentionDays <> @parmImageRetentionDays )
		BEGIN
			UPDATE 
				RecHubData.dimClientAccounts
			SET 
				RecHubData.dimClientAccounts.DataRetentionDays = RecHubData.dimClientAccountsView.DataRetentionDays,
				RecHubData.dimClientAccounts.ImageRetentionDays = RecHubData.dimClientAccountsView.ImageRetentionDays
			FROM
				RecHubData.dimClientAccounts
				INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
					AND  RecHubData.dimClientAccountsView.SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID
					AND  RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			WHERE
				RecHubData.dimClientAccountsView.ClientAccountKey = @parmClientAccountKey
				AND RecHubData.dimClientAccounts.MostRecent = 0;
		END


		/* Setup audit table */
		IF @prevDataRetentionDays <> @parmDataRetentionDays
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DataRetentionDays', CAST(@prevDataRetentionDays AS VARCHAR(10)), CAST(@parmDataRetentionDays AS VARCHAR(10)));
		IF @prevImageRetentionDays <> @parmImageRetentionDays
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ImageRetentionDays', CAST(@prevImageRetentionDays AS VARCHAR(10)), CAST(@parmImageRetentionDays AS VARCHAR(10)));
		IF @parmIsActive <> @prevIsActive
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsActive', CAST(@prevIsActive AS VARCHAR(1)), CAST(@parmIsActive AS VARCHAR(1)));

		-- Audit the RecHubData.dimClientAccounts insert
		SET @auditMessage = 'Modified dimClientAccounts for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' SiteBankID: ' + CAST(@siteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@siteClientAccountID AS VARCHAR(10))
			+ ' ClientAccountKey: ' + CAST(@parmClientAccountKey AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_dimClientAccounts_Upd',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimClientAccounts',
			@parmColumnName			= 'ClientAccountKey',
			@parmAuditValue			= @parmClientAccountKey,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;
	END


	/*
		Update our @parmClientAccountKey to be the new key if change was historical.  This won't go back to the client we just need it to send into
		the various merge procedures we'll be calling for accounts, columns and business rules
	*/
	SET @parmClientAccountKey = ISNULL(@parmNewClientAccountKey, @parmClientAccountKey);

	EXEC RecHubUser.usp_OLWorkgroups_Merge
		@parmSiteBankID					= @siteBankID,
		@parmSiteClientAccountID		= @siteClientAccountID,
		@parmEntityID					= @parmEntityID,
		@parmViewingDays				= @parmViewingDays,
		@parmMaximumSearchDays			= @parmMaximumSearchDays,
		@parmCheckImageDisplayMode		= @parmCheckImageDisplayMode,
		@parmDocumentImageDisplayMode	= @parmDocumentImageDisplayMode,
		@parmHOA						= @parmHOA,
		@parmDisplayBatchID				= @parmDisplayBatchID,
		@parmInvoiceBalancingOption		= @parmInvoiceBalancingOption,
		@parmUserID						= @parmUserID,
		@parmBillingAccount				= @parmBillingAccount,
		@parmBillingField1				= @parmBillingField1,
		@parmBillingField2				= @parmBillingField2,
		@parmEntityName					= @parmEntityName;
	EXEC RecHubData.usp_ElectronicAccounts_Merge
		@parmEntityID				= @parmEntityID,
		@parmSiteBankID				= @siteBankID,
		@parmSiteClientAccountID	= @siteClientAccountID,
		@parmDDAs					= @parmDDAs,
		@parmUserID					= @parmUserID;

	EXEC RecHubData.usp_ClientAccountsDataEntryColumns_Merge
		@parmEntityID				= @parmEntityID,
		@parmClientAccountKey		= @parmClientAccountKey,
		@parmDataEntryColumns		= @parmDataEntryColumns,
		@parmUserID					= @parmUserID,
		@parmUserDisplayName		= @parmUserDisplayName;

	IF (@parmBusinessRulesInvoiceRequired IS NOT NULL OR @parmBusinessRulesBalancingRequired IS NOT NULL OR 
		@parmBusinessRulesPDBalancingRequired IS NOT NULL OR @parmBusinessRulesPDPayerRequired IS NOT NULL)
	BEGIN
		EXEC RecHubData.usp_dimWorkgroupBusinessRules_Merge
			@paramSiteBankId              = @siteBankID,
			@paramSiteClientAccountId     = @siteClientAccountID,
			@paramInvoiceRequired		  = @parmBusinessRulesInvoiceRequired,
			@paramBalancingRequired		  = @parmBusinessRulesBalancingRequired,
			@paramPDBalancingRequired	  = @parmBusinessRulesPDBalancingRequired,
			@paramPDPayerRequired		  = @parmBusinessRulesPDPayerRequired;
	END;

	--EXEC RecHubException.usp_DataEntrySetups_Merge
		--@parmEntityID				= @parmEntityID,
		--@parmSiteBankID				= @siteBankID,
		--@parmWorkgroupID			= @siteClientAccountID,
		--@parmDeadLineDay			= @parmDeadLineDay,
		--@parmRequiresInvoice		= @parmRequiresInvoice,
		--@parmBusinessRules			= @parmBusinessRules,
		--@parmUserID					= @parmUserID;

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
