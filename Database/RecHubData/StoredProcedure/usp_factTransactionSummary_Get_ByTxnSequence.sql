--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Get_ByTxnSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Get_ByTxnSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Get_ByTxnSequence
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Get_ByTxnSequence 
(
	@parmSessionID				UNIQUEIDENTIFIER,		--WI 142865
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT, 
	@parmBatchID				BIGINT, 
	@parmDepositDate			DATETIME, 
	@parmTxnSequence			INT,
	@parmDisplayScannedCheck	BIT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: 
*
* Modification History
* 03/23/2009 CR 25817  JMC	Created.
* 12/31/2009 CR 28580  CS   Updated to use olta.factTransactionSummary.
* 08/20/2010 CR 30591  JMC  Fixed join to dimLockboxes table.
* 03/15/2012 CR 49164  JNE  Add SiteCode & BatchSiteCode to result set. 
* 04/02/2013 WI 90721  JBS	Update to 2.0 release. Change Schema Name.
*							Change proc name from usp_GetTransactionDetailBySequence.
*							Change all references from Lockbox to ClientAccount and 
*							Customer to Organization.
*							Changed parameters: @parmSiteLockboxID to @parmSiteClientAccountID,
*							@parmOLCustomerID to @parmOLOrganizationID.
*							Added:  factTransactionSummary.IsDeleted = 0 to where clause.
* 06/04/2014 WI 142865 PKW  Batch Collision.
*							Removed table RecHubUser.OLClientAccounts INNER JOIN.
*							Replaced @parmOLOrganizationID with @parmSessionID.
* 07/09/2014 WI 142865 KLC	Changed from LogonEntityID to workgroup's EntityID
* 07/11/2016 WI 291433 BLR  Added another join to the dimClientAccounts table to get the MostRecent Row.
* 09/27/2016 WI R360-1087  MR   Updated to return same columns as
*                               usp_factTransactionSummary_Get_ByOLOrganizationID
* 03/30/2018 PT 154101786	JPB	Added BatchSourceShortName, PaymentTypeShortName to results
* 10/30/2018 PT 161348262	MGE Added FileGroup to result set
******************************************************************************/
SET NOCOUNT ON 

	DECLARE													--WI 142865
		@StartDateKey	INT,								--WI 142865
		@EndDateKey		INT;								--WI 142865
	
BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays		--WI 142865
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
		RecHubUser.SessionClientAccountEntitlements.EntityID,										--WI 142865			
		RecHubUser.SessionClientAccountEntitlements.SiteBankID					AS BankID,			--WI 142865
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID			AS ClientAccountID,	--WI 142865
		RTRIM(dimClientAccounts.LongName)						AS LongName,
		CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.DepositDateKey), 112) AS DepositDate,
		CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.ImmutableDateKey), 112) AS PICSDate,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.dimClientAccounts.SiteCodeID,
		RecHubData.factTransactionSummary.BatchSiteCode,
		RecHubData.factTransactionSummary.BatchCueID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.dimBatchSources.ImportTypeKey,		
		CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.SourceProcessingDateKey), 112),101)	AS ProcessingDate,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimBatchPaymentTypes.ShortName AS PaymentTypeShortName,
		CA.FileGroup
	FROM 
		RecHubData.dimClientAccounts
		INNER JOIN RecHubData.factTransactionSummary 
			ON RecHubData.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON					--WI 142865
			RecHubData.factTransactionSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources ON 
			RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
		INNER JOIN RecHubData.dimImportTypes ON 
			RecHubData.dimImportTypes.ImportTypeKey = RecHubData.dimBatchSources.ImportTypeKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		INNER JOIN RecHubData.dimClientAccounts CA ON
			CA.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND CA.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND CA.MostRecent = 1
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142865		
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142865
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142865
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.DepositDateKey = @StartDateKey							--WI 142865
		AND RecHubData.factTransactionSummary.TxnSequence = @parmTxnSequence
		AND RecHubData.factTransactionSummary.IsDeleted = 0;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
