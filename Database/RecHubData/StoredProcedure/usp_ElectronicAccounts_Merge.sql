--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ElectronicAccounts_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ElectronicAccounts_Merge') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ElectronicAccounts_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ElectronicAccounts_Merge
(
	@parmEntityID				INT,
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmDDAs					XML,
	@parmUserID					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/08/2014
*
* Purpose: Performa merge operation on the ElectronicAccounts and dimDDAs tables.
*
* Modification History
* 04/08/2014 WI 133597 KLC	Created
******************************************************************************/
DECLARE @errorDescription			VARCHAR(1024),
		@auditMessage				VARCHAR(1024),
		@auditDDAKey				INT,
		@auditABA					VARCHAR(10),
		@auditDDA					VARCHAR(80),
		@auditElectronicAccountKey	INT,
		@auditType					VARCHAR(3);

DECLARE @dimDDAInserts				TABLE(DDAKey INT, ABA VARCHAR(10), DDA VARCHAR(35));
DECLARE @ElectronicAccountChanges	TABLE(ElectronicAccountKey INT, AuditType VARCHAR(3), DDAKey INT, ABA VARCHAR(10), DDA VARCHAR(35));

SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1 
	END

	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to merge DDAs with RecHubData.ElectronicAccounts, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	DECLARE @curTime datetime = GETDATE();

	--Validate that any DDAs passed in are not assigned to a different workgroup and fail if there are some
	IF EXISTS(	SELECT	RecHubData.ElectronicAccounts.DDAKey
				FROM	@parmDDAs.nodes('/ddas/dda') DDAs(dda)
					INNER JOIN RecHubData.dimDDAs
						ON	DDAs.dda.value('@rt', 'VARCHAR(10)') = RecHubData.dimDDAs.ABA
						AND	DDAs.dda.value('@dda', 'VARCHAR(35)') = RecHubData.dimDDAs.DDA
					INNER JOIN RecHubData.ElectronicAccounts
						on	RecHubData.dimDDAs.DDAKey = RecHubData.ElectronicAccounts.DDAKey
				WHERE	RecHubData.ElectronicAccounts.SiteBankID <> @parmSiteBankID
					OR	RecHubData.ElectronicAccounts.SiteClientAccountID <> @parmSiteClientAccountID )
	BEGIN
		SET @errorDescription = 'Unable to merge DDAs with RecHubData.ElectronicAccounts, one or more DDAs is already assigned to a different workgroup';
		RAISERROR(@errorDescription, 16, 2);
	END

	INSERT INTO RecHubData.dimDDAs(ABA, DDA, CreationDate)
	OUTPUT INSERTED.DDAKey, INSERTED.ABA, INSERTED.DDA
	INTO @dimDDAInserts
	SELECT	DDAs.dda.value('@rt', 'VARCHAR(10)'),
			DDAs.dda.value('@dda', 'VARCHAR(35)'),
			@curTime
	FROM @parmDDAs.nodes('/ddas/dda') DDAs(dda)
		LEFT JOIN RecHubData.dimDDAs
			ON DDAs.dda.value('@rt', 'VARCHAR(10)') = RecHubData.dimDDAs.ABA
				AND DDAs.dda.value('@dda', 'VARCHAR(35)') = RecHubData.dimDDAs.DDA
	WHERE RecHubData.dimDDAs.DDAKey is null;

	--Audit insert of dimDDAs (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditDDAKey = NULL;
	SELECT TOP 1 @auditDDAKey = DDAKey, @auditABA = ABA, @auditDDA = DDA FROM @dimDDAInserts;
	WHILE @auditDDAKey IS NOT NULL
	BEGIN
		SET @auditMessage = 'Added dimDDAs for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' DDAKey: ' + CAST(@auditDDAKey AS VARCHAR(10))
			+ ': ABA = ' + @auditABA
			+ ', DDA = ' + @auditDDA
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_ElectronicAccounts_Merge',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimDDAs',
			@parmColumnName			= 'DDAKey',
			@parmAuditValue			= @auditDDAKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;

		DELETE FROM @dimDDAInserts WHERE DDAKey = @auditDDAKey;

		SET @auditDDAKey = NULL;
		SELECT TOP 1 @auditDDAKey = DDAKey, @auditABA = ABA, @auditDDA = DDA FROM @dimDDAInserts;
	END

	--Now we merge the provided DDAs with the ElectronicAccounts table
	MERGE RecHubData.ElectronicAccounts
	USING
	(
		SELECT RecHubData.dimDDAs.DDAKey,
				RecHubData.dimDDAs.ABA,
				RecHubData.dimDDAs.DDA
		FROM @parmDDAs.nodes('/ddas/dda') DDAs(dda)
			INNER JOIN RecHubData.dimDDAs
				ON	DDAs.dda.value('@rt', 'VARCHAR(10)') = RecHubData.dimDDAs.ABA
				AND DDAs.dda.value('@dda', 'VARCHAR(35)') = RecHubData.dimDDAs.DDA
	) S
	ON RecHubData.ElectronicAccounts.DDAKey = S.DDAKey
		AND	RecHubData.ElectronicAccounts.SiteBankID = @parmSiteBankID
		AND	RecHubData.ElectronicAccounts.SiteClientAccountID = @parmSiteClientAccountID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (DDAKey, SiteCodeID, SiteBankID, SiteClientAccountID, CreationDate)
		VALUES (S.DDAKey, -1, @parmSiteBankID, @parmSiteClientAccountID, @curTime)
	WHEN NOT MATCHED BY SOURCE
		AND RecHubData.ElectronicAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.ElectronicAccounts.SiteClientAccountID = @parmSiteClientAccountID
			THEN DELETE
	OUTPUT	CASE WHEN $action = 'DELETE' THEN DELETED.ElectronicAccountKey ELSE INSERTED.ElectronicAccountKey END,
			CASE WHEN $action = 'DELETE' THEN 'DEL' ELSE 'INS' END,
			CASE WHEN $action = 'DELETE' THEN DELETED.DDAKey ELSE INSERTED.DDAKey END,
			S.ABA,
			S.DDA
	INTO @ElectronicAccountChanges;

	--Audit insert/deletes of ElectronicAccounts (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditElectronicAccountKey = NULL;
	SELECT TOP 1 @auditElectronicAccountKey = ElectronicAccountKey, @auditType = AuditType, @auditDDAKey = DDAKey, @auditABA = ABA, @auditDDA = DDA FROM @ElectronicAccountChanges;
	WHILE @auditElectronicAccountKey IS NOT NULL
	BEGIN
		IF @auditType = 'DEL'
		BEGIN
			SELECT TOP 1 @auditABA = ABA, @auditDDA = DDA FROM RecHubData.dimDDAs WHERE DDAKey = @auditDDAKey;
		END

		SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' ElectronicAccounts for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ': DDAKey = ' + CAST(@auditDDAKey AS VARCHAR(10))
			+ ', ABA = ' + ISNULL(@auditABA, 'NULL')
			+ ', DDA = ' + ISNULL(@auditDDA, 'NULL')
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_ElectronicAccounts_Merge',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'ElectronicAccounts',
			@parmColumnName			= 'ElectronicAccountKey',
			@parmAuditValue			= @auditElectronicAccountKey,
			@parmAuditType			= @auditType,
			@parmAuditMessage		= @auditMessage;

		DELETE FROM @ElectronicAccountChanges WHERE ElectronicAccountKey = @auditElectronicAccountKey;

		SET @auditElectronicAccountKey = NULL;
		SELECT TOP 1 @auditElectronicAccountKey = ElectronicAccountKey, @auditType = AuditType, @auditDDAKey = DDAKey FROM @ElectronicAccountChanges;
	END

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

