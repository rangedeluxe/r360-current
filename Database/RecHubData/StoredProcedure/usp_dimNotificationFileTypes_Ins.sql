--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_dimNotificationFileTypes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimNotificationFileTypes_Ins') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimNotificationFileTypes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimNotificationFileTypes_Ins 
(
      @parmFileType		VARCHAR(32), 
      @parmFileTypeDesc VARCHAR(255),
	  @parmUserId	    INT
)
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Charlie Johnson
* Date:     08/09/2012
*
* Purpose:  Inserts new Notification File Type database entries
*
* Modification History
* Created
* 09/08/2012 CR 54926 CEJ	Created.
* 04/24/2013 WI 98693 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc from usp_dimNotificationFileTypes_Insert	
* 09/15/2014 WI 165781 BLR  Added data auditing, UserID for param input.
* 06/03/2015 WI 216762 LA   Removed ID from Audit Message; added changes to AuditColumns table
****************************************************************************** */
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE
	@AuditMessage	VARCHAR(1024),
	@NotificationId INT,
	@AuditColumns RecHubCommon.AuditValueChangeTable;

BEGIN TRY   

	BEGIN TRANSACTION NotificationTypes_Add

	IF EXISTS(SELECT 1 FROM RecHubData.dimNotificationFileTypes 
				WHERE RecHubData.dimNotificationFileTypes.FileType = @parmFileType )
		BEGIN
		RAISERROR('There already is a database entry for FileType: %s.',16, 1,@parmFileType);
		END

	-- Insert Notification
	INSERT INTO RecHubData.dimNotificationFileTypes(FileType, FileTypeDescription)
	VALUES(@parmFileType, @parmFileTypeDesc);

	-- Data Audit

	INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FileTypeDescription', NULL,@parmFileTypeDesc);

	SELECT @NotificationId = SCOPE_IDENTITY() 
	SET @AuditMessage = 'Added NotificationType' 
		+ ', FileType: ' + @parmFileType
		+ ', Description: ' + @parmFileTypeDesc;
		
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 	
		@parmUserID					= @parmUserId,
		@parmApplicationName		= 'RecHubData.usp_dimNotificationFileTypes_Ins',
		@parmSchemaName				= 'RecHubData',
		@parmTableName				= 'dimNotificationTypes',
		@parmColumnName				= 'NotificationTypeKey',
		@parmAuditValue				= @NotificationId,
		@parmAuditType				= 'INS',
		@parmAuditMessage			= @AuditMessage

	COMMIT TRANSACTION NotificationTypes_Add;
END TRY

BEGIN CATCH
	  ROLLBACK TRANSACTION NotificationTypes_Add;
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
