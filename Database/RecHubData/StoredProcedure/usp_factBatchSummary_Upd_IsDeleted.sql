--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Upd_IsDeleted
(
	@parmDepositDateKey				INT,
	@parmBatchID					BIGINT,		   --WI 142830
	@parmModificationDate			DATETIME,
	@parmRowsDeleted				BIT  OUT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 02/16/2009
*
* Purpose: Mark RecHubData.factBatchSummary as deleted.
*
* Modification History
* 02/16/2009 CR 25817 JJR	Created
* 11/02/2008 CR 28039 JPB	Fixed problem with reconsolidation of batches. 
* 12/31/2009 CR 28564 JPB	Performance improvements:
*								Keys passed in
* 01/23/2012 CR 49583 JPB	Added SourceProcessingDateKey.
* 04/23/2013 WI 90637 JPB	Updates for 2.0. Moved to RecHubData schema.
*							Renamed from usp_factBatchSummaryDelete.
*							Combined usp_DataImportIntegrationServices_Delete_factBatchSummary.
*							Renamed @parmCustomerKey to @parmOrganizationKey.
*							Renamed @parmLockboxKey	to @parmClientAccountKey.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
*							Added @parmModificationDate.
*							Added @parmDeletedRecords.
* 06/18/2014 WI 142830 DLD  Batch Collision. 
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factBatchSummary
	SET
		RecHubData.factBatchSummary.IsDeleted = 1,
		RecHubData.factBatchSummary.ModificationDate = @parmModificationDate		
	WHERE	
		RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		AND RecHubData.factBatchSummary.IsDeleted = 0
	OPTION( RECOMPILE );

	IF( @@ROWCOUNT > 0) 
		SET @parmRowsDeleted = 1;
	ELSE 
		SET @parmRowsDeleted = 0;

END TRY
BEGIN CATCH
	IF( @@NESTLEVEL > 1 )
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
