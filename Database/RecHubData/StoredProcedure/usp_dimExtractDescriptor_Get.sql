--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimExtractDescriptor_Get
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_dimExtractDescriptor_Get') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimExtractDescriptor_Get;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimExtractDescriptor_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103849 JMC   Initial Version
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT
		RecHubData.dimExtractDescriptor.ExtractDescriptorKey,
		RecHubData.dimExtractDescriptor.ExtractDescriptor
	FROM RecHubData.dimExtractDescriptor
	ORDER BY RecHubData.dimExtractDescriptor.ExtractDescriptor;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
