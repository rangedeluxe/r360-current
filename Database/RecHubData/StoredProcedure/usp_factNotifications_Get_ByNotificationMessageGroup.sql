--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Get_ByNotificationMessageGroup
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup
(
	@parmNotificationMessageGroup	INT,
    @parmTimeZoneBias               INT = 0,
	@parmSessionID					UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: 
*
* Modification History
* 09/04/2012 CR 55381 JMC	Created
* 05/02/2103 WI 99083 JBS	Update to 2.0 release. Change schema to RecHubData
*							Forward Patch: WI 88732
*							Change references: Lockbox to ClientAccount,
*							Customer to Organization
* 10/22/2013 WI 118375 JMC	Modified to return Notification Dates as UTC
* 11/26/2013 WI 123421 TWE  Modified to return Notification Date adjusted by TimeBias
* 04/03/2015 WI 199541 CMC  Adding Permissions and forward patch of WI 123421
* 03/09/2018 PT 154252232 MGE Added call to check whether user has access to all attachments.
*********************************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/****************************************************************************
	* Used to compile all of the message parts for a Notification into a single 
	* column which will be returned by the query.
	****************************************************************************/
	DECLARE @MessageText VARCHAR(MAX),
			@NoAccessCount INT;

	EXEC RecHubData.usp_factNotifications_MessageGroupNoAccessCount @parmNotificationMessageGroup, @parmSessionID, @NoAccessCount OUT;

	SET @MessageText = '';

	SELECT 
		@MessageText = @MessageText + MessageText
	FROM 
		RecHubData.factNotifications 
	WHERE 
		RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
	ORDER BY 
		RecHubData.factNotifications.NotificationMessagePart;

	SELECT 
		RecHubData.factNotifications.factNotificationKey,
		RecHubData.factNotifications.NotificationMessageGroup,
		RecHubData.factNotifications.SourceNotificationID,
		DATEADD(minute, @parmTimeZoneBias, RecHubData.factNotifications.NotificationDateTime) AS NotificationDate,
		(CASE  
			WHEN @NoAccessCount = 0 THEN RecHubData.factNotifications.NotificationFileCount
			ELSE
			0
		END) AS NotificationFileCount,
		@MessageText										AS MessageText,
		RecHubData.factNotifications.NotificationMessagePart,
		RecHubData.dimOrganizations.SiteBankID				AS BankID,
		RecHubData.dimOrganizations.SiteOrganizationID		AS OrganizationID,
		RecHubData.dimOrganizations.Name					AS OrganizationName,
		RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
		RecHubData.dimClientAccounts.LongName				AS ClientAccountName
	FROM
		RecHubData.factNotifications
		INNER JOIN RecHubData.dimOrganizations ON
			RecHubData.factNotifications.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
		LEFT OUTER JOIN RecHubData.dimClientAccounts ON
			RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
		RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
		AND RecHubData.factNotifications.NotificationMessagePart = 1
		AND RecHubData.factNotifications.IsDeleted = 0;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH