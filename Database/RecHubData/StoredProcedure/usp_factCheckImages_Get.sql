--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factCheckImages_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factCheckImages_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factCheckImages_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factCheckImages_Get
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142831
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmImmutableDateKey		INT,
	@parmBatchID				BIGINT,				--WI 142831
	@parmBatchSequence			INT,
	@parmDepositDateKey			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/26/2009
*
* Purpose: Get information about a check image.
*
* Modification History
* 08/26/2009 CR 25817  JPB	Created.
* 11/14/2009 CR 28236  JPB	Added commands to set quoted identiifer and Arith 
*							Abort on.
* 11/18/2009 CR 28316  JPB	SP was not returning all rows from the XML column.
* 11/15/2011 CR 48106  WJS	Allow batch sequence to be passed it as negative 1. 
*							Passing in deposit key.
* 11/17/2011 CR 48410  WJS  Return batch Sequence and order by batchsequence.
* 11/20/2012 WI 69811  JPB	Added index hint.
* 11/20/2012 WI 70678  JPB	Added DepositDateKey and ProcessingDateKey to both WHERE
* 03/29/2013 WI 90676  JPB	Updated to 2.0.
*							Renamed proc from usp_GetCheckImageInfo.
*							Moved to RecHubData schema.
*							Renamed to usp_factCheckImages_Get.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
* 05/23/2014 WI 142831 PKW  Batch Collision - RAAM Integration.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE 
		@ImageSourceKey INT,
		@StartDateKey INT,							--WI 142831
		@EndDateKey INT;							--WI 142831

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateKey, --WI 142831
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT
		RecHubData.factCheckImages.FileSize,
		RecHubData.factCheckImages.Side,
		RecHubData.factCheckImages.PageNumber,
		RecHubData.dimImageTypes.ShortName,
		RecHubData.factCheckImages.ExternalDocumentID,
		RecHubData.factCheckImages.BatchSequence
	FROM
		RecHubData.factCheckImages
		INNER JOIN RecHubData.dimImageTypes ON
			RecHubData.dimImageTypes.ImageTypeKey = RecHubData.factCheckImages.ImageTypeKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142831
			RecHubData.factCheckImages.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE
		RecHubData.factCheckImages.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factCheckImages.DepositDateKey >= @StartDateKey
		AND RecHubData.factCheckImages.IsDeleted = 0
		AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID						--WI 142831
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142831
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142831
		AND RecHubData.factCheckImages.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factCheckImages.BatchID = @parmBatchID
		AND RecHubData.factCheckImages.BatchSequence = CASE WHEN @parmBatchSequence <> -1 THEN @parmBatchSequence ELSE RecHubData.factCheckImages.BatchSequence END
	ORDER BY RecHubData.factCheckImages.BatchSequence;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH