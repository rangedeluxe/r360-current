--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_FactItemData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_FactItemData_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_FactItemData_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_FactItemData_Get
(
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmImmutableDateKey		INT,
    @parmBatchID				BIGINT,
	@parmBatchSequence			INT
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/03/2014
*
* Purpose: Used by ICON to get factItemData 
*
* Modification History
* 10/01/2014 WI 169440  JBS Created. 
* 05/26/2015 WI 169440  JBS	Changed Input parm from DepositDate to ImmutableDateKey
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
	@BankKey			INT,
	@ClientAccountKey	INT,
	@DepositDateKey		INT;			

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

	SELECT
		RecHubData.factItemData.BatchSequence,
		RecHubData.dimItemDataSetupFields.Keyword,
		RecHubData.factItemData.DataValue
	FROM RecHubData.factItemData
		INNER JOIN RecHubData.dimItemDataSetupFields
			ON RecHubData.dimItemDataSetupFields.ItemDataSetupFieldKey = RecHubData.factItemData.ItemDataSetupFieldKey
	WHERE
		RecHubData.factItemData.ClientAccountKey = @ClientAccountKey
		AND RecHubData.factItemData.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factItemData.BatchID = @parmBatchID
		AND	RecHubData.factItemData.DepositDateKey = @DepositDateKey
		AND RecHubData.factItemData.BatchSequence = @parmBatchSequence
		AND RecHubData.factItemData.IsDeleted = 0;
		
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


