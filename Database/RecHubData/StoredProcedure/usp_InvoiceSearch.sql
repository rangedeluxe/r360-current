--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch
(
	@parmSearchRequest	XML,
	@parmSearchTotals	XML OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 12/08/2016
*
* Purpose: Query fact table for matching records
*
* Modification History
* 12/08/2016 #134732987 TWE Created by copying RemittanceSearch
* 01/27/2017 #134363645 JPB	Query from factStubs
* 03/03/2017 #136913453 JPB Added Amount and AccountNumber filtering
* 03/10/2017 #136915689 JPB	Added auditing
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @OrderBy VARCHAR(64),
		@OrderDirection VARCHAR(4),
		@RecordCount INT,
		@RecordsPerPage INT,
		@StartRecord INT;

--Use User Defined Table types to pass data to the helper SPs.
DECLARE @ClientAccounts RecHubData.InvoiceSearchClientAccountsTable;
DECLARE @SearchRequest RecHubData.InvoiceSearchSearchRequestTable;
DECLARE @BaseKeys RecHubData.InvoiceSearchBaseKeysTable;
DECLARE @QueryBuilder RecHubData.InvoiceSearchQueryBuilderTable;
DECLARE @AmountAccountNumberKeys RecHubData.InvoiceSearchBaseKeysTable;

BEGIN TRY 
	IF OBJECT_ID('tempdb..#BaseResults') IS NOT NULL
		DROP TABLE #BaseResults;
		
	CREATE TABLE #BaseResults
	(
		factStubKey BIGINT,
		BatchID BIGINT,
		ClientAccountKey INT,
		EntityID INT,
		SiteBankID INT,
		SiteClientAccountID INT,
		DepositDateKey INT,
		ImmutableDateKey INT,
		SourceBatchID BIGINT,
		BatchNumber INT,
		BatchSourceKey SMALLINT,
		BatchPaymentTypeKey TINYINT,
		TransactionID INT,
		TxnSequence INT,
		BatchSequence INT,
		DocumentBatchSequence INT,
		Amount MONEY,
		AccountNumber VARCHAR(80)
	);

	--Call a helper SP that will parse the XML data needed for the rest of the SP.
	INSERT INTO @SearchRequest
	(
		SessionID,
		DepositDateFrom,
		DepositDateTo,
		DepositDateFromKey,
		DepositDateToKey,
		SortBy,
		RecordsPerPage,
		StartRecord,
		BatchSourceKey,
		BatchPaymentTypeKey,
		UserName,
		EntityName
	)
	EXEC RecHubData.usp_InvoiceSearch_Get_SearchRequest
		@parmSearchRequest = @parmSearchRequest,
		@parmOrderBy = @OrderBy OUTPUT,
		@parmOrderDirection = @OrderDirection OUTPUT,
		@parmRecordsPerPage = @RecordsPerPage OUTPUT,
		@parmStartRecord = @StartRecord OUTPUT;

	--Call a help SP to parse the query builder part of the search request XML
	INSERT INTO @QueryBuilder
	(
		FieldName,
		Operator,
		Value
	)
	EXEC RecHubData.usp_InvoiceSearch_Get_QueryBuilder 
		@parmSearchRequest = @parmSearchRequest;

	--Call a helper SP to get the valid account list
	INSERT INTO @ClientAccounts
	(
		SessionID,
		SiteBankID,
		EntityID,
		SiteClientAccountID,
		ClientAccountKey,
		StartDateKey,
		EndDateKey,
		ViewAhead
	)
	EXEC RecHubData.usp_InvoiceSearch_Get_ClientAccountList 
		@parmSearchRequest = @parmSearchRequest;

	--Call a helper SP to get the intial list of basic keys
	INSERT INTO @BaseKeys
	(
		factStubKey,
		DepositDateKey,
		BatchSourceKey,
		BatchPaymentTypeKey,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		SessionID
	)
	EXEC RecHubData.usp_InvoiceSearch_Get_BaseKeys 
		@parmSearchRequest = @SearchRequest,
		@parmClientAccounts = @ClientAccounts;

	--If the query builder included Amount or Account Number, filter the basic key
	IF EXISTS (SELECT 1 FROM @QueryBuilder WHERE UPPER(FieldName) = 'AMOUNT' OR UPPER(FieldName) = 'ACCOUNTNUMBER' )
	BEGIN
		INSERT INTO @AmountAccountNumberKeys
		(
			factStubKey,
			DepositDateKey,
			BatchSourceKey,
			BatchPaymentTypeKey,
			EntityID,
			SiteBankID,
			SiteClientAccountID,
			SessionID
		)
		EXEC RecHubData.usp_InvoiceSearch_Get_BaseKeysByAmountAccountNumber
			@parmQueryBuilder = @QueryBuilder,
			@parmBaseKeys = @BaseKeys;

		--Use a merge statement to delete rows from the @BaseKeys table that are not in the @AmountAccountNumberKeys
		MERGE @BaseKeys AS TARGET
		USING @AmountAccountNumberKeys AS SOURCE ON (TARGET.factStubKey = SOURCE.factStubKey)
		WHEN NOT MATCHED BY SOURCE THEN DELETE; 
	END

	--In the next iteration, look at moving this to a helper SP. It will need to be dynamic SQL once we add sort options.
	INSERT INTO #BaseResults
	(
		factStubKey,
		BatchID,
		ClientAccountKey,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		DepositDateKey,
		ImmutableDateKey,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		TransactionID,
		TxnSequence,
		BatchSequence,
		DocumentBatchSequence,
		Amount,
		AccountNumber
	)
	SELECT
		RecHubData.factStubs.factStubKey,
		RecHubData.factStubs.BatchID,
		RecHubData.factStubs.ClientAccountKey,
		BaseKeys.EntityID,
		BaseKeys.SiteBankID,
		BaseKeys.SiteClientAccountID,
		RecHubData.factStubs.DepositDateKey,
		RecHubData.factStubs.ImmutableDateKey,
		RecHubData.factStubs.SourceBatchID,
		RecHubData.factStubs.BatchNumber,
		RecHubData.factStubs.BatchSourceKey,
		RecHubData.factStubs.BatchPaymentTypeKey,
		RecHubData.factStubs.TransactionID,
		RecHubData.factStubs.TxnSequence,
		RecHubData.factStubs.BatchSequence,
		RecHubData.factStubs.DocumentBatchSequence,
		COALESCE(RecHubData.factStubs.Amount,0.00),
		RecHubData.factStubs.AccountNumber
	FROM 
		@BaseKeys AS BaseKeys
		INNER JOIN @SearchRequest SearchRequest ON SearchRequest.SessionID = BaseKeys.SessionID
		INNER JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = BaseKeys.DepositDateKey
			AND RecHubData.factStubs.factStubKey = BaseKeys.factStubKey
	ORDER BY 
		SiteBankID,
		SiteClientAccountID,
		SourceBatchID,
		BatchSequence
	OFFSET @StartRecord-1 ROWS
	FETCH NEXT @RecordsPerPage ROWS ONLY;

	SELECT 
		#BaseResults.SiteBankID AS BankID,
		#BaseResults.SiteClientAccountID AS ClientAccountID,
		CAST(CONVERT(VARCHAR,#BaseResults.DepositDateKey,101) AS DateTime) AS DepositDate,
		#BaseResults.ImmutableDateKey AS PICSDate,
		#BaseResults.SourceBatchID,
		#BaseResults.BatchNumber,
		RecHubData.dimBatchSources.LongName AS BatchSourceLongName,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimBatchPaymentTypes.LongName AS ImportTypeLongName,
		RecHubData.dimBatchPaymentTypes.ShortName AS ImportTypeShortName,
		#BaseResults.TransactionID,
		#BaseResults.TxnSequence,
		#BaseResults.BatchSequence,
		#BaseResults.DocumentBatchSequence,
		ISNULL(#BaseResults.Amount,0.00) AS Amount,
		#BaseResults.AccountNumber AS Account,
		#BaseResults.BatchID,
		#BaseResults.ClientAccountKey,
		#BaseResults.BatchSourceKey AS PaymentSource,
		#BaseResults.BatchPaymentTypeKey AS PaymentType
	FROM 
		#BaseResults
		INNER JOIN RecHubData.dimBatchSources ON /*RecHubData.dimBatchSources.EntityID = #BaseResults.EntityID
			AND*/ RecHubData.dimBatchSources.BatchSourceKey = #BaseResults.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #BaseResults.BatchPaymentTypeKey;
/* Dev Note --- The Entity ID check was commented out. It was added for performance reasons but is cause some retrival issues. Further investigation is needed. */
	SELECT @RecordCount = COUNT(*) FROM @BaseKeys
	SET @parmSearchTotals = 
		(
			SELECT	
				'Results' AS "@Name",
				@RecordCount AS "@TotalRecords"
			FOR XML PATH('RecordSet'), ROOT('Page') 
		);

	IF OBJECT_ID('tempdb..#BaseResults') IS NOT NULL
		DROP TABLE #BaseResults;

	EXEC RecHubData.usp_InvoiceSearch_Ins_Audit
		@parmSearchRequest = @SearchRequest,
		@parmQueryBuilder = @QueryBuilder,
		@parmClientAccounts = @ClientAccounts;

END TRY

BEGIN CATCH
	IF OBJECT_ID('tempdb..#BaseResults') IS NOT NULL
		DROP TABLE #BaseResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

	