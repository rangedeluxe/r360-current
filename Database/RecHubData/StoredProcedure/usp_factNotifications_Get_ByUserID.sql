--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_Get_ByUserID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_Get_ByUserID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_Get_ByUserID
(
	@parmSessionID					UNIQUEIDENTIFIER,
	@parmStartDate					DATETIME,
	@parmEndDate					DATETIME,
	@parmNotificationFileTypeKey	INT			= 0,
	@parmOrderBy					VARCHAR(20) = 'date',
	@parmOrderDir					VARCHAR(4)	= 'asc',
	@parmStartRecord				INT			= 1,
	@parmRecordsToReturn			INT			= 20,
	@parmTotalRecords				INT OUTPUT,
	@parmFilteredRecords            INT OUTPUT,
	@parmSearch                     VARCHAR(MAX)	= '',
	@parmTimeZoneBias               INT         = 0,
	@parmSearchRequest				XML,
	@parmFileName					VARCHAR(255) = NULL
)
AS
/******************************************************************************
** Deluxe Corp. (DLX)
** Copyright � 2012-2018 DELUXE Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: 
*
* Modification History
* 09/04/2012 CR 55378 JMC	Created
* 04/25/2013 WI 99098 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc From usp_factNotifications_GetByUserID
*							Change references: Lockbox to ClientAccount,
*							Customer to Organization
*							Forward Patch: WI 91651
* 10/22/2013 WI 118377 JMC	Modified to return and filter Notification Dates as UTC
* 10/30/2013 WI 119185 MLH  Added IsDeleted = 0 
* 11/26/2013 WI 123428 TWE  Modified to return Notification date adjuste by timebias
* 09/11/2014 WI 139599 JBS  Add RAAM integration. Change to return only Notifications
*							related to SessionUser. 
*							Add optional Parm list of BankID/ClientAccountID to filter @parmSearchRequest.  
*							Add optional Parm to filter on File Name @parmFileName 
* 01/15/2015 WI 184600 TWE  Only filter on FileName if a real name was input (not empty or null)
* 01/27/2015 WI 186739 TWE  Pick up alerts by UserID
* 08/30/2016 WI 127604153 BLR Added functionality for pagination and filtering and got rid of organization
* 10/03/2016 WI 131190743 TWE Add back in select to pick up fatal alerts
* 03/20/2018 PT 154252232 MGE Added call to check whether user has access to all attachments.
******************************************************************************/

SET NOCOUNT ON;

BEGIN TRY

	SET @parmSearch = '%' + @parmSearch + '%'

	-- FP: WI 91651
	/****************************************************************************
	*   Set the dates up for search
	****************************************************************************/
	DECLARE @StartDate DATETIME; 
	DECLARE @EndDate DATETIME;
	DECLARE @justDate DATE;
	DECLARE @RowNumber INT;
	DECLARE @MessageGroup BIGINT;
	DECLARE @NoAccessCount INT; 

	--Remove any time component
    SET @justDate = @parmStartDate;
    SET @parmStartDate = @justDate;
    SET @justDate = @parmEndDate;
    SET @parmEndDate = @justDate;

    --adjust search Date Time by timecode bias.
    SET @StartDate = DATEADD(minute, - @parmTimeZoneBias, @parmStartDate);
    SET @EndDate = DATEADD(minute, - @parmTimeZoneBias , @parmEndDate);
    SET @EndDate = DATEADD(second, -1, DATEADD(day, 1, @EndDate));

	/****************************************************************************
	* We have to use a temp table because the actual query against the database 
	* needs to be dynamic so we can accomodate a dynamic ORDER BY clause.
	****************************************************************************/
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;

	CREATE TABLE #Notifications (
		factNotificationKey			INT,
		NotificationMessageGroup	INT,
		integraPAYNotificationID	UNIQUEIDENTIFIER,
		NotificationDate			DATETIME,
		NotificationFileCount		INT,
		MessageText					VARCHAR(MAX),
		NotificationMessagePart		INT,
		BankID						INT,
		OrganizationID				INT,
		OrganizationName			VARCHAR(20),
		ClientAccountID				INT,
		ClientAccountName			VARCHAR(40)
	);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#FinalNotifications')) 
		DROP TABLE #FinalNotifications;

	CREATE TABLE #FinalNotifications (
		RowNumber					INT IDENTITY(1,1),
		NotificationMessageGroup	INT,
		NotificationDate			DATETIME,
		NotificationFileCount		INT,
		MessageText					VARCHAR(MAX),
		AdditionalMessageParts		INT,
		BankID						INT,
		OrganizationID				INT,
		OrganizationName			VARCHAR(20),
		ClientAccountID				INT,
		ClientAccountName			VARCHAR(40)
	);

	DECLARE @NotificationsFirstFilter TABLE (
		NotificationMessageGroup	int
	)

	-- WI 139599 new work table used to narrow WorkGroup selection
	IF OBJECT_ID('tempdb..#tmpClientAccountListInput') IS NOT NULL
        DROP TABLE #tmpClientAccountListInput;

	CREATE TABLE #tmpClientAccountListInput
	(
		BankID				INT,
		ClientAccountID		INT
	);
	-- WI 139599 new work table used to narrow WorkGroup selection
	IF OBJECT_ID('tempdb..#tmpSessionClientAccountList') IS NOT NULL
        DROP TABLE #tmpSessionClientAccountList;

	CREATE TABLE #tmpSessionClientAccountList
	(
		SessionID			UNIQUEIDENTIFIER,
		ClientAccountKey	INT,
		BankID				INT,
		ClientAccountID		INT
	);

	-- WI 139599 This loads up list of ClientAccountIDs chosen to search against.  This should be 0 or many
	INSERT INTO #tmpClientAccountListInput
	(
		BankID,
		ClientAccountID		
	)
	SELECT	
		SearchRequest.att.value('@BankID', 'int') AS BankID,
		SearchRequest.att.value('@ClientAccountID', 'int') AS ClientAccountID	 
	FROM 	
		@parmSearchRequest.nodes('/Root/ClientAccounts/ClientAccount') SearchRequest(att)
	IF EXISTS (SELECT 1 FROM #tmpClientAccountListInput)
		BEGIN			--  IF ClientAccounts sent in we will build our temp table to only include them
			INSERT INTO #tmpSessionClientAccountList
			(
				SessionID,
				ClientAccountKey,
				BankID,
				ClientAccountID		
			)
			SELECT DISTINCT
				SessionID,
				ClientAccountKey,
				SiteBankID,	
				SiteClientAccountID
			FROM 
				RecHubUser.SessionClientAccountEntitlements
				INNER JOIN #tmpClientAccountListInput ON
					#tmpClientAccountListInput.BankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID AND
					#tmpClientAccountListInput.ClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			WHERE	
				RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;
		END
	ELSE
		BEGIN		--  IF NO ClientAccounts sent in we will build with ALL rows matching SessionID
			INSERT INTO #tmpSessionClientAccountList
			(
				SessionID,
				ClientAccountKey,
				BankID,
				ClientAccountID		
			)
			SELECT DISTINCT
				SessionID,
				ClientAccountKey,
				SiteBankID,	
				SiteClientAccountID
			FROM 
				RecHubUser.SessionClientAccountEntitlements
			WHERE	
				RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;
		END

	/****************************************************************************
	* The first thing we need to do is to populate the #Notifications temp 
	* table with all of the rows that fit the criteria for the data being 
	* requested.  We have to use a #temp table because we will be selecting
	* from this table from a dynamic query (which is required for the dynamic 
	* ORDER BY clause.
	****************************************************************************/

		/* Retrieving ClientAccount Level Notifications Assigned to User 
		   and all others unassigned */
			INSERT INTO #Notifications
			SELECT DISTINCT
				RecHubData.factNotifications.factNotificationKey,
				RecHubData.factNotifications.NotificationMessageGroup,
				RecHubData.factNotifications.SourceNotificationID,
				RecHubData.factNotifications.NotificationDateTime			AS NotificationDate,
				RecHubData.factNotifications.NotificationFileCount,
				RecHubData.factNotifications.MessageText					AS MessageText,
				RecHubData.factNotifications.NotificationMessagePart,
				RecHubData.dimOrganizations.SiteBankID,
				RecHubData.dimOrganizations.SiteOrganizationID,
				RecHubData.dimOrganizations.Name							AS OrganizationName,
				RecHubData.dimClientAccounts.SiteClientAccountID,
				RecHubData.dimClientAccounts.LongName						AS ClientAccountName
			FROM 
				RecHubData.factNotifications
				INNER JOIN RecHubData.dimOrganizations ON 
					RecHubData.factNotifications.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
				INNER JOIN RecHubData.dimClientAccounts 
					INNER JOIN RecHubUser.OLWorkgroups ON      
						RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID 
						AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID ON 
						RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN #tmpSessionClientAccountList	ON							--WI 139599
						RecHubData.dimClientAccounts.ClientAccountKey = #tmpSessionClientAccountList.ClientAccountKey 
				INNER JOIN RecHubUser.[Session] ON 
						#tmpSessionClientAccountList.SessionID = RecHubUser.[Session].SessionID
			WHERE	#tmpSessionClientAccountList.SessionID = @parmSessionID	
				AND ((RecHubData.factNotifications.UserID = RecHubUser.[Session].UserID AND
					RecHubData.factNotifications.UserNotification = 1 ) OR
					(RecHubData.factNotifications.UserNotification = 0))
				--AND RecHubData.factNotifications.ClientAccountKey IS NOT NULL 
				--AND RecHubData.factNotifications.ClientAccountKey >= 1
				---- FP: WI 91651
				AND RecHubData.factNotifications.NotificationDateTime BETWEEN @StartDate and @EndDate
				AND RecHubData.factNotifications.NotificationMessagePart = 1
				AND RecHubData.factNotifications.IsDeleted = 0;

			------------------------------------------------------------------
			-- Now go insert the notifications assigned to the actual user
			------------------------------------------------------------------
			INSERT INTO #Notifications 
			SELECT DISTINCT
				RecHubData.factNotifications.factNotificationKey,
				RecHubData.factNotifications.NotificationMessageGroup,
				RecHubData.factNotifications.SourceNotificationID,
				RecHubData.factNotifications.NotificationDateTime			AS NotificationDate,
				RecHubData.factNotifications.NotificationFileCount,
				RecHubData.factNotifications.MessageText					AS MessageText,
				RecHubData.factNotifications.NotificationMessagePart,
				-1                                                          AS SiteBankID,
				-1                                                          AS SiteOrganizationID,
				'Alerts'	    						                    AS OrganizationName,
				NULL														AS SiteClientAccountID,
				NULL														AS ClientAccountName
			FROM 
				RecHubData.factNotifications
				INNER JOIN RecHubUser.[Session]
					on RecHubUser.[Session].UserID = RecHubData.factNotifications.UserID
			WHERE 
				RecHubData.factNotifications.ClientAccountKey IS NULL OR RecHubData.factNotifications.ClientAccountKey < 1
				AND RecHubData.factNotifications.NotificationDateTime BETWEEN @StartDate AND @EndDate
				AND RecHubData.factNotifications.NotificationMessagePart = 1
				AND RecHubData.factNotifications.IsDeleted = 0
				AND RecHubUser.[Session].SessionID = @parmSessionID;

	IF @parmNotificationFileTypeKey > 0 
	BEGIN		
	
		DELETE FROM #Notifications
		WHERE #Notifications.NotificationMessageGroup
		NOT IN(
			SELECT #Notifications.NotificationMessageGroup
			FROM #Notifications
				INNER JOIN RecHubData.factNotificationFiles ON
					#Notifications.NotificationMessageGroup = RecHubData.factNotificationFiles.NotificationMessageGroup
			WHERE RecHubData.factNotificationFiles.NotificationFileTypeKey = @parmNotificationFileTypeKey);
	END

	--	WI 139599 Filter out ALL not equal to input file name
	IF (@parmFileName IS NOT NULL)   AND 
	   (LEN(@parmFileName) > 0)
	BEGIN		
		DECLARE @FileNameWildCard VARCHAR(255); 
		SET @FileNameWildCard = '%'+@parmFileName+'%';

		DELETE FROM #Notifications
		WHERE #Notifications.NotificationMessageGroup
		NOT IN(
			SELECT #Notifications.NotificationMessageGroup
			FROM #Notifications
				INNER JOIN RecHubData.factNotificationFiles ON
					#Notifications.NotificationMessageGroup = RecHubData.factNotificationFiles.NotificationMessageGroup
			WHERE RecHubData.factNotificationFiles.UserFileName LIKE @FileNameWildCard);
	END

	/****************************************************************************
	* Now that the #temp table has been populated, we can re-run the query with
	* an ORDER BY clause applied.  This query will be loaded into a @TABLE 
	* variable which will then be paginated.
	****************************************************************************/
	;WITH PrelimResults AS
	(
		SELECT	factNotificationKey,
				CASE WHEN ClientAccountID > 0 THEN ClientAccountID ELSE OrganizationID END AS SourceID
		FROM #Notifications
	),
	FinalResults AS
	(
		SELECT	CASE 
					WHEN UPPER(@parmOrderBy)='DATE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationDate DESC, ClientAccountID, OrganizationID)
							ELSE ROW_NUMBER() OVER (ORDER BY NotificationDate ASC, ClientAccountID, OrganizationID)
						END
					WHEN UPPER(@parmOrderBy)='NOTIFICATIONSOURCE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY SourceID DESC, NotificationDate, OrganizationName, ClientAccountID)
							ELSE ROW_NUMBER() OVER (ORDER BY SourceID ASC, NotificationDate, OrganizationName, ClientAccountID)
						END
					WHEN UPPER(@parmOrderBy)='MESSAGE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY MessageText DESC, NotificationDate, ClientAccountID, OrganizationName)
							ELSE ROW_NUMBER() OVER (ORDER BY MessageText ASC, NotificationDate, ClientAccountID, OrganizationName)
						END
					WHEN UPPER(@parmOrderBy)='FILE' THEN
						CASE 
							WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationFileCount DESC, NotificationDate, ClientAccountID, OrganizationName)
							ELSE ROW_NUMBER() OVER (ORDER BY NotificationFileCount ASC, NotificationDate, ClientAccountID, OrganizationName)
						END
					ELSE CASE WHEN UPPER(@parmOrderDir) = 'DESC' THEN ROW_NUMBER() OVER (ORDER BY NotificationDate DESC, ClientAccountID, OrganizationName)
						ELSE ROW_NUMBER() OVER (ORDER BY NotificationDate ASC, ClientAccountID, OrganizationName)
					END
				END AS RecID,
				#Notifications.factNotificationKey,
				NotificationMessageGroup,
				integraPAYNotificationID,
				NotificationDate,
				NotificationFileCount,
				MessageText,
				NotificationMessagePart,
				BankID,
				OrganizationID,
				OrganizationName,
				ClientAccountID,
				ClientAccountName,
				SourceID
		FROM	#Notifications
				INNER JOIN PrelimResults ON PrelimResults.factNotificationKey = #Notifications.factNotificationKey
	)

	/****************************************************************************
	* This is the ending result select.
	****************************************************************************/
	INSERT INTO #FinalNotifications
	SELECT
		NotificationMessageGroup,
		DATEADD(minute,@parmTimeZoneBias,NotificationDate)     as NotificationDate,
		NotificationFileCount,
		MessageText,
		(SELECT COUNT(RecHubData.factNotifications.factNotificationKey) 
		 FROM RecHubData.factNotifications 
		 WHERE 
			RecHubData.factNotifications.NotificationMessageGroup = FinalResults.NotificationMessageGroup 
			AND RecHubData.factNotifications.NotificationMessagePart > 1  AND RecHubData.factNotifications.IsDeleted = 0) AS AdditionalMessageParts,
		BankID,
		OrganizationID,
		OrganizationName,
		ClientAccountID,
		ClientAccountName
	FROM	FinalResults
	WHERE (OrganizationName like @parmSearch 
		OR ClientAccountID like @parmSearch
		OR ClientAccountName like @parmSearch
		OR MessageText like @parmSearch 
		OR MessageText like @parmSearch)
	ORDER BY RecID
	OFFSET @parmStartRecord ROWS
	FETCH NEXT @parmRecordsToReturn ROWS ONLY

	
	/* Now Validate Access to the Files */
	Set @RowNumber = 1;

	WHILE (@RowNumber <= @parmRecordsToReturn)
	BEGIN
		SELECT @MessageGroup = NotificationMessageGroup FROM #FinalNotifications WHERE RowNumber = @RowNumber; 
		EXEC RecHubData.usp_factNotifications_MessageGroupNoAccessCount @MessageGroup, @parmSessionID, @NoAccessCount OUT;
		IF (@NoAccessCount > 0)
			UPDATE #FinalNotifications SET NotificationFileCount = 0 WHERE RowNumber = @RowNumber;
		SET @RowNumber = @RowNumber + 1;
	END;

	SELECT 
		NotificationMessageGroup,
		NotificationDate,
		NotificationFileCount,
		MessageText,
		AdditionalMessageParts,
		BankID,
		OrganizationID,
		OrganizationName,
		ClientAccountID,
		ClientAccountName
	FROM #FinalNotifications;

	/****************************************************************************
	* With the full recordset, we can set the variable that will contain the 
	* total number of rows selected by the query.
	****************************************************************************/
	SELECT @parmTotalRecords = COUNT(*) FROM #Notifications;
	SELECT @parmFilteredRecords = COUNT(*)
	FROM #Notifications
	WHERE (OrganizationName like @parmSearch 
		OR ClientAccountID like @parmSearch
		OR ClientAccountName like @parmSearch
		OR MessageText like @parmSearch 
		OR MessageText like @parmSearch)

	/****************************************************************************
	* We won't need this anymore now that the @TABLE variable has been populated.
	****************************************************************************/
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#FinalNotifications')) 
		DROP TABLE #FinalNotifications;
	IF OBJECT_ID('tempdb..#tmpClientAccountListInput') IS NOT NULL
        DROP TABLE #tmpClientAccountListInput;
	IF OBJECT_ID('tempdb..#tmpSessionClientAccountList') IS NOT NULL
        DROP TABLE #tmpSessionClientAccountList;

	-- Let's get out of here!

END TRY

BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Notifications')) 
		DROP TABLE #Notifications;
	IF OBJECT_ID('tempdb..#tmpClientAccountListInput') IS NOT NULL
        DROP TABLE #tmpClientAccountListInput;
	IF OBJECT_ID('tempdb..#tmpSessionClientAccountList') IS NOT NULL
        DROP TABLE #tmpSessionClientAccountList;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH