--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Ins') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Ins
(
	@parmUserId			INT,
	@parmFIEntityID		INT,
	@parmSiteBankID		INT,
	@parmBankName		VARCHAR(128),
	@parmBankKey		INT	OUTPUT,
	@parmErrorCode		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 06/03/2014
*
* Purpose: Inserts a new dimBanks record
*
* Modification History
* 06/03/2014 WI 145889 RDS	Created
* 06/06/2014 WI 146180 RDS	Return error message for duplicate key
* 06/24/2014 WI 149754 JPB	Add default dimOrganization record.
* 05/29/2015 WI 215941 JPB	Removed unnecessary information from audit messages.
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction BIT = 0;
DECLARE @OrganizationKey INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	DECLARE @errorDescription VARCHAR(1024)
	DECLARE @auditMessage VARCHAR(MAX)
	DECLARE @curTime DATETIME = GETDATE();

	-- Verify User ID
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert RecHubData.dimBanks record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Verify the SiteBankID doesn't already exist
	IF EXISTS(SELECT * FROM RecHubData.dimBanksView WHERE SiteBankID = @parmSiteBankID)
	BEGIN
		SET @parmErrorCode = 1; -- 1 = Duplicate
		GOTO Cleanup;
	END

	-- Insert the Bank record
	INSERT INTO RecHubData.dimBanks(SiteBankID, MostRecent, CreationDate, ModificationDate, BankName, EntityID)
	VALUES (@parmSiteBankID, 1, @curTime, @curTime, @parmBankName, @parmFIEntityID);
	SET @parmBankKey = SCOPE_IDENTITY();

	-- Audit the RecHubData.dimBanks insert
	SET @auditMessage = 'Added dimBanks for EntityID: ' + CAST(@parmFIEntityID AS VARCHAR(10))
		+ ': SiteBankID = ' + CAST(@parmSiteBankID AS VARCHAR(30))
		+ ', BankName = ' + @parmBankName
		+ '.';

	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_dimBanks_Ins',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimBanks',
			@parmColumnName			= 'BankKey',
			@parmAuditValue			= @parmBankKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;

	INSERT INTO RecHubData.dimOrganizations(SiteOrganizationID,SiteBankID,MostRecent,CreationDate,ModificationDate,Name,[Description])
	VALUES(-1,@parmSiteBankID,1,@curTime,@curTime,'Default','Default for ' + @parmBankName);
	SET @OrganizationKey = SCOPE_IDENTITY();

	-- Audit the RecHubData.dimBanks insert
	SET @auditMessage = 'Added dimOrganizations for EntityID: ' + CAST(@parmFIEntityID AS VARCHAR(10))
		+ ': SiteBankID = ' + CAST(@parmSiteBankID AS VARCHAR(30))
		+ ', Name = Default'
		+ ', Description = Default for ' + @parmBankName
		+ '.';

	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_dimBanks_Ins',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimOrganizations',
			@parmColumnName			= 'OrganizationKey',
			@parmAuditValue			= @OrganizationKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;


	Cleanup:

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

