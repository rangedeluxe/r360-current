--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimBatchSources_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchSources_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchSources_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchSources_Get
(
	@parmActiveOnly AS BIT = 1
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 06/13/2013
*
* Purpose: Get all the current Source records
*
* Modification History
* 06/13/2013 WI 106021 DRP	Created
* 11/22/2013 WI 119917 MLH	Restricted results to return only Lockbox and ACH
*					This change is ONLY for v2.00.01 Comerica release.  There 
*					is a related 2.01 , requirement (WI 122141) to create an 
*					editor in Online Admin where the user can control which 
*					Payment Sources will display in the Online Account Search Options.
*					BatchSourceKey:
*						1 - Lockbox
*						4 - ACH
* 05/29/2014 WI 143282 EAS	Remove filter for 2.0.  Add new columns to resultset.
* 03/31/2015 WI 198856 JPB	Added ORDER BY LongName
******************************************************************************/
SET NOCOUNT ON; 
	
BEGIN TRY

	SELECT 
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchSources.ShortName,
		RecHubData.dimBatchSources.LongName,
		RecHubData.dimBatchSources.EntityID,
		RecHubData.dimImportTypes.ShortName AS SystemType,
		RecHubData.dimBatchSources.IsActive
	FROM  
		RecHubData.dimBatchSources
		INNER JOIN RecHubData.dimImportTypes ON	RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	WHERE 
		RecHubData.dimBatchSources.IsActive = 
	    CASE @parmActiveOnly 
			WHEN 1 THEN 1 
			ELSE RecHubData.dimBatchSources.IsActive
		END
		AND   RecHubData.dimBatchSources.BatchSourceKey > 0
	ORDER BY
		RecHubData.dimBatchSources.LongName;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
