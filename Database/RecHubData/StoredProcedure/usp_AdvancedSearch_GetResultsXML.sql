IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetResultsXML') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetResultsXML
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetResultsXML
(
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmTotalCheckAmount		MONEY,
	@parmTotalCheckCount		BIGINT,
	@parmTotalDocumentCount		BIGINT,
	@parmTotalRecords			BIGINT,
	@parmResultsXML				XML OUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 09/10/2019
*
* Purpose: Create the return XML.
*
* Modification History
* 09/10/2019 R360-30221 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @SelectFields TABLE
	(
		IsCheck BIT,
		DataType INT,
		BatchSourceKey SMALLINT,
		FieldName VARCHAR(256),
		ReportTitle VARCHAR(256),
		CompositeFieldName VARCHAR(256)
	)

	INSERT INTO @SelectFields
	(
		IsCheck,
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle,
		CompositeFieldName
	)
	EXEC RecHubData.usp_AdvancedSearch_GetSelectColumnList
		@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
		@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable;

	SELECT @parmResultsXML = 
	(
		SELECT	
			'Results' AS "@Name",
			@parmTotalRecords AS "@TotalRecords",
			@parmTotalDocumentCount AS "@DocumentCount",
			@parmTotalCheckCount AS "@CheckCount",
			@parmTotalCheckAmount AS "@CheckTotal",
			(
				SELECT 
					CASE IsCheck WHEN 1 THEN 'Checks' ELSE 'Stubs' END AS '@tablename',
					CompositeFieldName AS '@fieldname',
					DataType AS '@datatype',
					ReportTitle AS '@reporttitle'
				FROM
					@SelectFields
				WHERE
					((IsCheck = 0 AND UPPER(FieldName) NOT IN ('BATCHSEQUENCE')) OR
								(IsCheck = 1 AND BatchSourceKey <> 255))
				FOR XML PATH('field'), ROOT('SelectFields'), TYPE
			)
		FOR XML PATH('RecordSet'), ROOT('Page')
	);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
