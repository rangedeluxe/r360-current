--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_ByUserID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_ByUserID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByUserID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_ByUserID
(
	@UserID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 03/05/2014
*
* Purpose: Return Bank ID and Client Account ID Client Accounts open to the User ID.
*
* Modification History
* 03/05/2014	WI	131935	EAS	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	CREATE TABLE #BankIDClientAccountID
	(
		SiteBankID INT ,
		SiteOrganizationID INT ,
		SiteClientAccountID INT ,
		ShortName VARCHAR(20),
		SiteCodeID INT,
		CutOff TINYINT ,
		SiteClientAccountKey UNIQUEIDENTIFIER,
		IsCommingled BIT,
		OLOrganizationID UNIQUEIDENTIFIER,
		OLClientAccountID UNIQUEIDENTIFIER ,
		IsActive BIT,
		DisplayName VARCHAR(50),
		DocumentImageDisplayMode TINYINT,
		CheckImageDisplayMode TINYINT ,
		CurrentProcessingDate DATETIME,  
		MaximumSearchDays INT ,
		ViewingDays INT ,
		LongName VARCHAR(40) ,
		HOA BIT
	)

	INSERT INTO #BankIDClientAccountID
		EXEC RecHubUser.usp_API_OLClientAccounts_Get_ByUserID @UserID
		
	SELECT DISTINCT SiteBankID, SiteClientAccountID 
	FROM #BankIDClientAccountID
	
	DROP TABLE #BankIDClientAccountID 
	
END TRY
BEGIN CATCH
	DROP TABLE #BankIDClientAccountID 
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
