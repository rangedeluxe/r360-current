--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_Report_ImportReconciliation_Filter
--WFSScriptProcessorStoredProcedureDrop
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_Report_ImportReconciliation_Filter
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*                                                                           	
* Author: PKW									                                
* Date: 05/08/2014																
*																				
* Purpose: Filter for the Import Reconciliation SSRS report.
*																			    
* Modification History														    	
* 05/08/2014 WI 140801  PKW  Created.  
* 06/18/2014 WI 140801	PKW  Move the SP from RecHubAlert to RecHubData schema.
*							 Granted on SP the 'RecHubUser_User' with EXECUTE Permission.
* 09/22/2014 WI 140801  PKW  Added to enable the selection for '-- ALL --' in the filter - bug 160764.                       
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT DISTINCT
		RecHubData.dimBatchSources.BatchSourceKey,
		COALESCE(RecHubData.dimBatchSources.LongName, RecHubData.dimBatchSources.ShortName) AS LongName
	FROM
		RecHubData.dimBatchSources
	WHERE 
		LongName NOT LIKE 'Reserved%'
		AND RecHubData.dimBatchSources.IsActive = 1
	UNION ALL
	SELECT 
		NULL, '-- ALL --'
	ORDER BY LongName ASC;
	END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH