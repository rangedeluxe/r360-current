--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ClientAccountBatchPaymentTypes_Get_BySessionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ClientAccountBatchPaymentTypes_Get_BySessionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ClientAccountBatchPaymentTypes_Get_BySessionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ClientAccountBatchPaymentTypes_Get_BySessionID
(
	@parmSessionID			UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 04/14/2014
*
* Purpose: Get current Payment Types based on ClientAccountID passed or All if ClientAccountId is null
*
* Modification History
* 04/22/2014 WI 135936 JBS	Created. Enhanced to use RAAM. 
* 04/16/2015 WI 202100 MAA  Added order by ShortName
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		DISTINCT
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.ShortName,
		RecHubData.dimBatchPaymentTypes.LongName
	FROM
	    RecHubData.dimBatchPaymentTypes
		INNER JOIN RecHubData.ClientAccountBatchPaymentTypes ON
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.ClientAccountBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
			RecHubData.ClientAccountBatchPaymentTypes.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	ORDER BY RecHubData.dimBatchPaymentTypes.LongName;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH