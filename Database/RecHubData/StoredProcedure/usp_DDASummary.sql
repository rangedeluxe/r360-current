--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_DDASummary
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_DDASummary') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_DDASummary
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_DDASummary
(
	@parmDepositDateKey INT,
	@parmSessionID		UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 09/15/2014
*
* Purpose: 
*		   
*
* Modification History
* 09/15/2014 WI 166670 JPB	Created
* 10/10/2014 WI 167460 EAS  Added Entity ID to resultset.
* 11/21/2014 WI	167460 KLC	Updated the following
*							 - removed entity name selection, breadcrumb built by client
*							 - updated grouping to be by workgroup not by historical workgroup update
*							 - updated to return workgroup display label of most recent workgroup
*							 - updated to obey IsActive of most recent workgroup
*							 - updated to obey site processing date and days to view setting
* 03/09/2015 WI 194899 JBS	Add View Ahead Permission
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;

	DECLARE @depositDate	DATETIME;
	SET @depositDate = CONVERT(DATETIME, CAST(@parmDepositDateKey AS VARCHAR(8)), 112)

	CREATE TABLE #tmpResults
	(
		EntityID INT,
		SiteBankID INT,
		SiteClientAccountID INT,
		DisplayLabel VARCHAR(80),
		BatchSourceKey INT,
		BatchPaymentTypeKey INT,
		DDAKey INT,
		PaymentCount INT,
		PaymentTotal MONEY
	);
	
	/* Use a cte to get the batch IDs that the user can see and a cte to get the check data "rolled" up for display */
	;WITH factBatchSummary_cte AS
	(
		SELECT
			RecHubData.factBatchSummary.BatchID,
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel
		FROM
			RecHubUser.SessionClientAccountEntitlements
			INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
			INNER JOIN RecHubUser.OLWorkgroups /*Join through OLWorkgroups to ensure WG association still exist*/
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
					AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
			INNER JOIN RechubData.dimClientAccountsView
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		WHERE
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			AND RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey 
			AND ((RecHubData.factBatchSummary.DepositDateKey <= RecHubUser.SessionClientAccountEntitlements.EndDateKey AND RecHubUser.SessionClientAccountEntitlements.ViewAhead = 0) -- WI 194899
				 OR (RecHubUser.SessionClientAccountEntitlements.ViewAhead = 1))  -- WI 194899
			AND RecHubData.factBatchSummary.DepositStatus >= 850/*Deposit Complete*/
			AND RecHubData.factBatchSummary.IsDeleted = 0
			AND RecHubData.dimClientAccountsView.IsActive = 1
	),
	factCheckData_cte AS
	(
		SELECT
			factBatchSummary_cte.EntityID,
			factBatchSummary_cte.SiteBankID,
			factBatchSummary_cte.SiteClientAccountID,
			factBatchSummary_cte.DisplayLabel,
			RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchPaymentTypeKey,
			RecHubData.factChecks.DDAKey,
			COUNT(RecHubData.factChecks.ClientAccountKey) AS PaymentCount,
			SUM(RecHubData.factChecks.Amount) AS PaymentTotal
		FROM
			factBatchSummary_cte
			INNER JOIN RecHubData.factChecks ON RecHubData.factChecks.BatchID = factBatchSummary_cte.BatchID
		WHERE 
			RecHubData.factChecks.IsDeleted = 0
			AND RecHubData.factChecks.DepositDateKey = @parmDepositDateKey
		GROUP BY
			factBatchSummary_cte.EntityID,
			factBatchSummary_cte.SiteBankID,
			factBatchSummary_cte.SiteClientAccountID,
			factBatchSummary_cte.DisplayLabel,
			RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchPaymentTypeKey,
			RecHubData.factChecks.DDAKey
	)
	INSERT INTO #tmpResults
	(
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		DisplayLabel,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DDAKey,
		PaymentCount,
		PaymentTotal
	)
	SELECT
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		DisplayLabel,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DDAKey,
		PaymentCount,
		PaymentTotal
	FROM factCheckData_cte;

	/* Select the final results joining the friendly names and values for the UI */
	SELECT
		#tmpResults.EntityID,
		#tmpResults.SiteBankID,
		#tmpResults.SiteClientAccountID,
		#tmpResults.DisplayLabel,
		RecHubData.dimBatchSources.LongName AS PaymentSource,
		RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
		RecHubData.dimDDAs.DDA,
		#tmpResults.PaymentCount,
		#tmpResults.PaymentTotal
	FROM 
		#tmpResults
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = #tmpResults.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = #tmpResults.BatchPaymentTypeKey
		INNER JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = #tmpResults.DDAKey

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpResults')) 
		DROP TABLE #tmpResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH