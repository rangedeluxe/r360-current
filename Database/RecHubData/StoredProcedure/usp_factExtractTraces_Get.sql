--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factExtractTraces_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factExtractTraces_Get') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factExtractTraces_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factExtractTraces_Get
(
	@parmTraceColumnName VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103853	JMC	Initial Version
* 06/03/2014 WI 142854	DLD	RAAM Integration.
* 12/09/2014 WI 142854  JBS Removing RAAM integration(reverting to previous logic), 
*						but leaving permission tags and	keeping DepositDateKey for join criteria.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	;WITH ExtractTrace (RunDate, ClientAccountKey, BatchID, DepositDateKey)													--WI 142854
	AS
	(
		SELECT
			RecHubData.factExtractTraces.ExtractDateTime AS RunDate,
			RecHubData.factExtractTraces.ClientAccountKey,
			RecHubData.factExtractTraces.BatchID,
			RecHubData.factExtractTraces.DepositDateKey																		--WI 142854
		FROM RecHubData.factExtractTraces
			INNER JOIN RecHubData.dimExtractDescriptor 
				ON RecHubData.factExtractTraces.ExtractDescriptorKey = RecHubData.dimExtractDescriptor.ExtractDescriptorKey
		WHERE 
			RecHubData.dimExtractDescriptor.ExtractDescriptor = @parmTraceColumnName
			AND RecHubData.factExtractTraces.IsDeleted = 0
	)
	SELECT
		ExtractTrace.RunDate, 
		RecHubData.dimClientAccounts.SiteBankID, 
		RecHubData.dimClientAccounts.SiteClientAccountID, 
		ExtractTrace.BatchID, 
		RecHubData.factBatchSummary.ImmutableDateKey,
		SUM(RecHubData.factBatchSummary.CheckCount) AS CheckCount,
		SUM(RecHubData.factBatchSummary.CheckTotal) AS CheckAmount
	FROM RecHubData.factBatchSummary
		INNER JOIN ExtractTrace ON 
			RecHubData.factBatchSummary.ClientAccountKey = ExtractTrace.ClientAccountKey
			AND RecHubData.factBatchSummary.BatchID = ExtractTrace.BatchID
			AND RecHubData.factBatchSummary.DepositDateKey = ExtractTrace.DepositDateKey
		INNER JOIN RecHubData.dimClientAccounts ON 
			RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE
		RecHubData.factBatchSummary.IsDeleted = 0
	GROUP BY
		ExtractTrace.RunDate, 
		RecHubData.dimClientAccounts.SiteBankID, 
		RecHubData.dimClientAccounts.SiteClientAccountID, 
		ExtractTrace.BatchID, 
		RecHubData.factBatchSummary.ImmutableDateKey;																						--WI 142854

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH