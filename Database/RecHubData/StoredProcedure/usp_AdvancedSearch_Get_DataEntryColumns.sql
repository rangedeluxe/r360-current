use IntegrationTests_DDXW2JR2_AdvancedSearchCheckAndStubDataEntry_20191111154151
GO




IF OBJECT_ID('RecHubData.usp_AdvancedSearch_Get_DataEntryColumns') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_Get_DataEntryColumns
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_Get_DataEntryColumns
(
	@parmAdvancedSearchBaseKeysTable RecHubData.AdvancedSearchBaseKeysTable READONLY,
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmSearchTotals XML OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Query Data Advanced Search fact table for matching records
*
* Modification History
* 08/16/2019 R360-30221 JPB	Created - based on RecHubData.usp_ClientAccount_Search
* 08/20/2019 R360-30227 JPB Added support for filtering
*******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;
DECLARE
	@AmountFrom				MONEY,
	@AmountTo				MONEY,
	@BatchNumberFrom		INT,
	@BatchNumberTo			INT,
	@BatchPaymentTypeKey	TINYINT,
	@BatchSourceKey			SMALLINT,
	@DepositDateKeyFrom		INT,
	@DepositDateKeyTo		INT,
	@DisplayCOTSOnly		BIT,
	@DisplayScannedChecks	BIT,
	@MarkSense				BIT,
	@RecordsPerPage			INT,
	@SessionID				UNIQUEIDENTIFIER,
	@SiteBankID				INT,
	@SiteClientAccountID	INT,
	@SortBy					VARCHAR(32),
	@SortByDirection		VARCHAR(4),
	@SourceBatchIDFrom		BIGINT,
	@SourceBatchIDTo		BIGINT,
	@StartRecord			INT,
	@TotalCheckAmount		MONEY,
	@TotalCheckCount		BIGINT,
	@TotalDocumentCount		BIGINT,
	@TotalRecords			BIGINT;

/* used to determine 'base' column names */
DECLARE 
	@CheckAmountCN			VARCHAR(30),
	@CheckSerialNumberCN	VARCHAR(30),
	@CheckAccountNumberCN	VARCHAR(30),
	@CheckRTCN				VARCHAR(30),
	@CheckPayerCN			VARCHAR(30);

/* vars for dynamic SQL */
DECLARE 
	@SelectColumns			NVARCHAR(MAX),
	@SortString				NVARCHAR(2048),
	@SQLCommand				NVARCHAR(MAX),
	@SQLParams				NVARCHAR(MAX),
	@WhereClause			NVARCHAR(MAX);

/* Determine if stubs should be filtered out */
DECLARE
	@IncludeChecks			BIT = 0,
	@IncludeStubs			BIT = 0;

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingChecks')) 
		DROP TABLE #tmpMatchingChecks;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingStubs')) 
		DROP TABLE #tmpMatchingStubs;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;

	/* Create memory table for batch sources */
	CREATE TABLE #tmpBatchSources
	(
		BatchSourceKey SMALLINT,
		ImportTypeKey TINYINT,
		ShortName VARCHAR(30),
		LongName VARCHAR(128)
	);

	/* Create memory table for batch sources */
	CREATE TABLE #tmpBatchPaymentTypes
	(
		BatchPaymentTypeKey SMALLINT,
		ShortName VARCHAR(30),
		LongName VARCHAR(128)
	);

	/* Create temp table for initial filter of records, min columns needed to get a full filtered record set */
	CREATE TABLE #tmpPreliminaryMatchingResults
	(
		factTransactionSummaryKey BIGINT,
		DepositDateKey		INT,
		BatchID				BIGINT,
		SiteBankID			INT,
		SiteClientAccountID	INT,
		TransactionID		INT,
		TxnSequence			INT
	);

	CREATE TABLE #tmpMatchingChecks
	(
		factTransactionSummaryKey BIGINT,
		factCheckKey			BIGINT,
		DepositDateKey			INT,
		BatchID					BIGINT,
		TransactionID			INT
	);

	CREATE TABLE #tmpMatchingStubs
	(
		factTransactionSummaryKey BIGINT,
		factStubKey				BIGINT,
		DepositDateKey			INT,
		BatchID					BIGINT,
		TransactionID			INT
	);

	/* Create temp table for fully formed filtered records needed for sorting and final return */
	CREATE TABLE #tmpMatchingResults
	(
		RowID					BIGINT IDENTITY(1,1),
		factTransactionSummaryKey BIGINT,
		factCheckKey			BIGINT,
		factStubKey				BIGINT,
		BatchID					BIGINT,
		SourceBatchID			BIGINT,
		BankKey					INT,
		ClientAccountKey		INT,
		DepositDateKey			INT,
		ImmutableDateKey		INT,
		BatchSourceKey			SMALLINT,
		BatchPaymentTypeKey		SMALLINT,
		BatchNumber				INT,
		TransactionID			INT,
		TxnSequence				INT,
		SiteBankID				INT,
		SiteClientAccountID		INT,
		CheckBatchSequence		INT,
		ChecksCheckSequence		INT,
		StubsStubSequence		INT,
		CheckCount				BIGINT,
		DocumentCount			BIGINT,
		CheckAmount				MONEY,
		NumericRoutingNumber	BIGINT,
		NumericSerial			BIGINT,
		CheckAccountNumber		VARCHAR(30),
		SerialNumber			VARCHAR(30),
		TransactionCode			VARCHAR(30),
		RemitterName			VARCHAR(60)
	);

	/* Get the input parameters from the table */
	exec RecHubData.usp_AdvancedSearch_GetSearchParams
		@parmAdvancedSearchBaseKeysTable = @parmAdvancedSearchBaseKeysTable,
		@parmAmountFrom				= @AmountFrom OUT,
		@parmAmountTo				= @AmountTo OUT,
		@parmBatchNumberFrom		= @BatchNumberFrom OUT,
		@parmBatchNumberTo			= @BatchNumberTo OUT,
		@parmBatchPaymentTypeKey	= @BatchPaymentTypeKey OUT,
		@parmBatchSourceKey			= @BatchSourceKey OUT,
		@parmDepositDateKeyFrom		= @DepositDateKeyFrom OUT,
		@parmDepositDateKeyTo		= @DepositDateKeyTo OUT,
		@parmDisplayCOTSOnly		= @DisplayCOTSOnly OUT,
		@parmDisplayScannedChecks	= @DisplayScannedChecks OUT,
		@parmMarkSense				= @MarkSense OUT,
		@parmRecordsPerPage			= @RecordsPerPage OUT,
		@parmSessionID				= @SessionID OUT,
		@parmSiteBankID				= @SiteBankID OUT,
		@parmSortBy					= @SortBy OUT,
		@parmSortByDirection		= @SortByDirection OUT,
		@parmSiteClientAccountID	= @SiteClientAccountID OUT,
		@parmSourceBatchIDFrom		= @SourceBatchIDFrom OUT,
		@parmSourceBatchIDTo		= @SourceBatchIDTo OUT,
		@parmStartRecord			= @StartRecord OUT;

	/* Call helper stored procedure to create additional dynamic SQL strings */
	/* Get the sort string */
	EXEC RecHubData.usp_AdvancedSearch_GetSortString
		@parmSortBy = @SortBy,
		@pamSortByDirection = @SortByDirection,
		@parmSortString = @SortString OUT;

	/* Get the select columns */
	EXEC RecHubData.usp_AdvancedSearch_GetSelectColumnsString
		@parmAdvancedSearchWhereClauseTable=@parmAdvancedSearchWhereClauseTable,
		@parmAdvancedSearchSelectFieldsTable=@parmAdvancedSearchSelectFieldsTable,
		@parmSelectColumnsString=@SelectColumns OUT;

	/* Set specific names for result set */
	EXEC RecHubData.usp_AdvancedSearch_GetSelectNames
		 @parmAdvancedSearchSelectFieldsTable =  @parmAdvancedSearchSelectFieldsTable,
		 @parmCheckAmountCN = @CheckAmountCN OUT,
		 @parmCheckSerialNumberCN = @CheckSerialNumberCN OUT,
		 @parmCheckAccountNumberCN = @CheckAccountNumberCN OUT,
		 @parmCheckRTCN = @CheckRTCN OUT,
		 @parmCheckPayerCN = @CheckPayerCN OUT;

	/* Get preliminary key set of batches that match the basic search criteria, just get the keys */
	INSERT INTO #tmpPreliminaryMatchingResults(factTransactionSummaryKey, DepositDateKey, BatchID, TransactionID, TxnSequence, SiteBankID, SiteClientAccountID)
	EXEC RecHubData.usp_AdvancedSearch_Get_PrelimResults
		@parmBatchNumberFrom		= @BatchNumberFrom,
		@parmBatchNumberTo			= @BatchNumberTo,
		@parmBatchPaymentTypeKey	= @BatchPaymentTypeKey,
		@parmBatchSourceKey			= @BatchSourceKey,
		@parmDepositDateKeyFrom		= @DepositDateKeyFrom,
		@parmDepositDateKeyTo		= @DepositDateKeyTo,
		@parmMarkSense				= @MarkSense,
		@parmSessionID				= @SessionID,
		@parmSiteBankID				= @SiteBankID,
		@parmSiteClientAccountID	= @SiteClientAccountID,
		@parmSourceBatchIDFrom		= @SourceBatchIDFrom,
		@parmSourceBatchIDTo		= @SourceBatchIDTo;

SELECT '#tmpPreliminaryMatchingResults', * FROM #tmpPreliminaryMatchingResults  ORDER BY batchid, TransactionID, TxnSequence

	/* Now get the actual filter results account for checks and stubs with WHERE clauses */
	/* Setup the checks request */
	/* Get the where clause */
	EXEC RecHubData.usp_AdvancedSearch_GetWhereString
		@parmAdvancedSearchWhereClauseTable=@parmAdvancedSearchWhereClauseTable,
		@parmIsCheck = 1,
		@parmWhereClauseString = @WhereClause OUT;

	SET @SQLCommand = N'
	SELECT 
		#tmpPreliminaryMatchingResults.factTransactionSummaryKey,
		RecHubData.factChecks.factCheckKey,
		#tmpPreliminaryMatchingResults.DepositDateKey,
		#tmpPreliminaryMatchingResults.BatchID,
		#tmpPreliminaryMatchingResults.TransactionID
	FROM
		#tmpPreliminaryMatchingResults
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = #tmpPreliminaryMatchingResults.DepositDateKey
			AND RecHubData.factChecks.BatchID = #tmpPreliminaryMatchingResults.BatchID
			AND RecHubData.factChecks.TransactionID = #tmpPreliminaryMatchingResults.TransactionID
	WHERE
		COALESCE(RecHubData.factChecks.Amount,0) >= @AmountFrom
		AND COALESCE(RecHubData.factChecks.Amount,922337203685477.5807) <= @AmountTo
		AND RecHubData.factChecks.IsDeleted = 0
		' + @WhereClause + N';';

	SET @SQLParams = N'	@AmountFrom MONEY,
		@AmountTo MONEY';

	INSERT INTO #tmpMatchingChecks
	(
		factTransactionSummaryKey,
		factCheckKey, 
		DepositDateKey,
		BatchID,
		TransactionID
	)
	EXECUTE sp_executesql 
		@SQLCommand, 
		@SQLParams,
		@AmountFrom = @AmountFrom,
		@AmountTo = @AmountTo;

	/* Setup the checks request */
	/* Get the where clause */
	EXEC RecHubData.usp_AdvancedSearch_GetWhereString
		@parmAdvancedSearchWhereClauseTable=@parmAdvancedSearchWhereClauseTable,
		@parmIsCheck = 0,
		@parmWhereClauseString = @WhereClause OUT;

	SET @SQLCommand = N'
	SELECT 
		#tmpPreliminaryMatchingResults.factTransactionSummaryKey,
		RecHubData.factStubs.factStubKey,
		#tmpPreliminaryMatchingResults.DepositDateKey,
		#tmpPreliminaryMatchingResults.BatchID,
		#tmpPreliminaryMatchingResults.TransactionID
	FROM
		#tmpPreliminaryMatchingResults
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = #tmpPreliminaryMatchingResults.DepositDateKey
			AND RecHubData.factStubs.BatchID = #tmpPreliminaryMatchingResults.BatchID
			AND RecHubData.factStubs.TransactionID = #tmpPreliminaryMatchingResults.TransactionID
		' + @WhereClause + N';';

	INSERT INTO #tmpMatchingStubs
	(
		factTransactionSummaryKey,
		factStubKey, 
		DepositDateKey,
		BatchID,
		TransactionID
	)
	EXECUTE sp_executesql 
		@SQLCommand;

--SELECT '#tmpMatchingStubs',* FROM #tmpMatchingStubs
	/************************************************************************************
	* Set the total values to return to the app											*
	************************************************************************************/
	;WITH DocumentCount AS
	(
		SELECT 
			COALESCE(SUM(RecHubData.factTransactionSummary.DocumentCount),0) AS DocumentCount
		FROM	
			#tmpPreliminaryMatchingResults
			INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = #tmpPreliminaryMatchingResults.DepositDateKey
				AND RecHubData.factTransactionSummary.factTransactionSummaryKey = #tmpPreliminaryMatchingResults.factTransactionSummaryKey
		WHERE
			RecHubData.factTransactionSummary.IsDeleted = 0
		GROUP BY
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence
	)
	SELECT @TotalDocumentCount = COALESCE(SUM(DocumentCount),0) FROM DocumentCount;

	;WITH CheckInfo AS
	(
		SELECT 
			SUM(COALESCE(RecHubData.factTransactionSummary.CheckCount,0)) AS CheckCount,
			SUM(COALESCE(RecHubData.factTransactionSummary.CheckTotal,0.00)) AS CheckAmount
		FROM	
				#tmpPreliminaryMatchingResults
				INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = #tmpPreliminaryMatchingResults.DepositDateKey
					AND RecHubData.factTransactionSummary.factTransactionSummaryKey = #tmpPreliminaryMatchingResults.factTransactionSummaryKey
		WHERE
			RecHubData.factTransactionSummary.IsDeleted = 0
			AND RecHubData.factTransactionSummary.CheckCount <> 0
		GROUP BY
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence
	)
	SELECT	
		@TotalCheckCount = COALESCE(SUM(CheckCount),0),
		@TotalCheckAmount = COALESCE(SUM(CheckAmount),0.00)
	FROM
		CheckInfo;

	/* COTS - remove records that have a check */
	IF @DisplayCOTSOnly = 1
	BEGIN
		DELETE #tmpPreliminaryMatchingResults
		FROM 
			#tmpPreliminaryMatchingResults 
			INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = #tmpPreliminaryMatchingResults.DepositDateKey
				AND RecHubData.factTransactionSummary.BatchID = #tmpPreliminaryMatchingResults.BatchID
				AND RecHubData.factTransactionSummary.TransactionID = #tmpPreliminaryMatchingResults.TransactionID
				AND RecHubData.factTransactionSummary.TxnSequence = #tmpPreliminaryMatchingResults.TxnSequence
		WHERE 
			CheckCount <> 0;
	END

	SELECT 
		@TotalRecords = COUNT(*)
	FROM 
		#tmpMatchingChecks 
		FULL OUTER JOIN #tmpMatchingStubs ON #tmpMatchingStubs.DepositDateKey = #tmpMatchingChecks.DepositDateKey 
			AND #tmpMatchingStubs.BatchID = #tmpMatchingChecks.BatchID 
			AND #tmpMatchingStubs.TransactionID = #tmpMatchingChecks.TransactionID

	/* Create the full record set so it can be paged and returned */
	;WITH factKeys AS
	(
		SELECT 
			COALESCE(#tmpMatchingChecks.factTransactionSummaryKey, #tmpMatchingStubs.factTransactionSummaryKey) AS factTransactionSummaryKey,
			#tmpMatchingChecks.DepositDateKey,
			#tmpMatchingChecks.BatchID,
			#tmpMatchingChecks.factCheckKey,
			#tmpMatchingStubs.factStubKey
		FROM 
			#tmpMatchingChecks 
			FULL OUTER JOIN #tmpMatchingStubs ON #tmpMatchingStubs.DepositDateKey = #tmpMatchingChecks.DepositDateKey 
				AND #tmpMatchingStubs.BatchID = #tmpMatchingChecks.BatchID 
				AND #tmpMatchingStubs.TransactionID = #tmpMatchingChecks.TransactionID 
				AND #tmpMatchingStubs.factStubKey IS NOT NULL
	)
	select * FROM factKeys

	select * from #tmpMatchingStubs where factStubKey is not null
return
	INSERT INTO #tmpMatchingResults
	(
		factTransactionSummaryKey,
		factCheckKey,
		factStubKey,
		BatchID,
		SourceBatchID,
		DepositDateKey,
		ImmutableDateKey,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchNumber,
		TransactionID,
		TxnSequence,
		SiteBankID,
		SiteClientAccountID,
		CheckBatchSequence,
		ChecksCheckSequence,
		StubsStubSequence,
		CheckCount,
		DocumentCount,
		CheckAmount,
		NumericRoutingNumber,
		NumericSerial,
		CheckAccountNumber,
		SerialNumber,
		TransactionCode,
		RemitterName
	)
	SELECT
		factKeys.factTransactionSummaryKey,
		factKeys.factCheckKey,
		factKeys.factStubKey,
		factKeys.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		@SiteBankID AS SiteBankID,
		@SiteClientAccountID AS SiteClientAccountID,
		RecHubData.factChecks.BatchSequence AS CheckBatchSequence,
		RecHubData.factChecks.CheckSequence AS ChecksCheckSequence,
		RecHubData.factStubs.StubSequence AS StubsStubSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factChecks.Account,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.RemitterName
	FROM
		factKeys
		INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = factKeys.DepositDateKey
			AND RecHubData.factTransactionSummary.factTransactionSummaryKey = factKeys.factTransactionSummaryKey
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = factKeys.DepositDateKey
			AND RecHubData.factChecks.factCheckKey = factKeys.factCheckKey
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = factKeys.DepositDateKey
			AND RecHubData.factStubs.factStubKey = factKeys.factStubKey
			AND factKeys.factStubKey IS NOT NULL;

SELECT '#tmpMatchingResults', * FROM #tmpMatchingResults order by batchid, TransactionID, TxnSequence

	/************************************************************************************
	* Get the batch source/payment type info to join to the final result set			*
	************************************************************************************/
	/* Fill in batch sources */
	;WITH BatchSources AS
	(
		SELECT BatchSourceKey
		FROM #tmpMatchingResults
		GROUP BY BatchSourceKey
	)
	INSERT INTO #tmpBatchSources(BatchSourceKey, ImportTypeKey, ShortName, LongName)
	SELECT 
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchSources.ImportTypeKey,
		RecHubData.dimBatchSources.ShortName,
		RecHubData.dimBatchSources.LongName
	FROM
		BatchSources
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = BatchSources.BatchSourceKey ;

	/* Fill in batch sources */
	;WITH BatchPaymentTypes AS
	(
		SELECT BatchPaymentTypeKey
		FROM #tmpMatchingResults
		GROUP BY BatchPaymentTypeKey
	)
	INSERT INTO #tmpBatchPaymentTypes(BatchPaymentTypeKey, ShortName, LongName)
	SELECT 
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.ShortName,
		RecHubData.dimBatchPaymentTypes.LongName
	FROM
		BatchPaymentTypes
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = BatchPaymentTypes.BatchPaymentTypeKey;

	/* Create the return XML */
	EXEC RecHubData.usp_AdvancedSearch_GetResultsXML
		@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
		@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable,
		@parmTotalCheckAmount = @TotalCheckAmount,
		@parmTotalCheckCount = @TotalCheckCount,
		@parmTotalDocumentCount = @TotalDocumentCount,
		@parmTotalRecords = @TotalRecords,
		@parmResultsXML = @parmSearchTotals OUTPUT;

	/************************************************************************************
	* Create the dynamic SQL to return all the columns for a page of data 				*
	************************************************************************************/
	SET @SQLCommand = N'
	;WITH ReturnList AS
	(
		SELECT
			ROW_NUMBER() OVER (ORDER BY ' + @SortString + N') AS RecID,
			DepositDateKey,
			factTransactionSummaryKey,
			factCheckKey,
			factStubKey,
			SiteBankID,
			SiteClientAccountID
		FROM
			#tmpMatchingResults
			INNER JOIN #tmpBatchSources ON #tmpBatchSources.BatchSourceKey = #tmpMatchingResults.BatchSourceKey
			INNER JOIN #tmpBatchPaymentTypes ON #tmpBatchPaymentTypes.BatchPaymentTypeKey = #tmpMatchingResults.BatchPaymentTypeKey
		ORDER BY 
			RecID
		OFFSET ' + CAST(@StartRecord-1 AS NVARCHAR(10)) + N' ROWS
		FETCH FIRST ' + CAST(@RecordsPerPage AS NVARCHAR(10)) + N' ROWS ONLY
	)
	SELECT
		--#tmpDimensions.SiteBankID AS BankID,
		--#tmpDimensions.SiteClientAccountID AS ClientAccountID,
		CONVERT(VARCHAR(10),CAST(CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.DepositDateKey, 101) AS DATETIME),101) AS Deposit_Date,
		#tmpBatchSources.LongName AS PaymentSource,
		#tmpBatchPaymentTypes.LongName AS PaymentType,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.ImmutableDateKey AS PICSDate,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factChecks.Amount AS ' + @CheckAmountCN + N',
		RecHubData.factChecks.Serial AS ' + @CheckSerialNumberCN + N',
		RecHubData.factChecks.TransactionCode AS ChecksTransactionCode,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factChecks.Account AS ' + @CheckAccountNumberCN + N',
		RecHubData.factChecks.RoutingNumber AS ' + @CheckRTCN + N',
		RecHubData.factChecks.NumericRoutingNumber AS numeric_rt,
		RecHubData.dimDDAs.DDA,
		RecHubData.factChecks.RemitterName AS ' + @CheckPayerCN + N',
		#tmpBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
		RecHubData.factChecks.BatchSequence AS CheckBatchSequence,
		RecHubData.factStubs.BatchSequence AS StubBatchSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.OMRCount AS MarkSenseDocumentCount,
		RecHubData.factChecks.CheckSequence AS ChecksCheckSequence
	' + @SelectColumns + N'
	FROM 
		ReturnList
		INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = ReturnList.DepositDateKey
			AND RecHubData.factTransactionSummary.factTransactionSummaryKey = ReturnList.factTransactionSummaryKey
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = ReturnList.DepositDateKey
			AND RecHubData.factChecks.factCheckKey = ReturnList.factCheckKey
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = ReturnList.DepositDateKey
			AND RecHubData.factStubs.factStubKey = ReturnList.factStubKey
		INNER JOIN #tmpBatchSources ON #tmpBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
		INNER JOIN #tmpBatchPaymentTypes ON #tmpBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		LEFT JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = RecHubData.factChecks.DDAKey
		INNER JOIN RecHubData.dimImportTypes ON #tmpBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	ORDER BY RecID
	OPTION (RECOMPILE);';

	EXEC(@SQLCommand);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingChecks')) 
		DROP TABLE #tmpMatchingChecks;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingStubs')) 
		DROP TABLE #tmpMatchingStubs;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;
END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingChecks')) 
		DROP TABLE #tmpMatchingChecks;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingStubs')) 
		DROP TABLE #tmpMatchingStubs;	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpMatchingResults')) 
		DROP TABLE #tmpMatchingResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchSources')) 
		DROP TABLE #tmpBatchSources;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchPaymentTypes')) 
		DROP TABLE #tmpBatchPaymentTypes;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpPreliminaryMatchingResults')) 
		DROP TABLE #tmpPreliminaryMatchingResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH







go
use master
go
