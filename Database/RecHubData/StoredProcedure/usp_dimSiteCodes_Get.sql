--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimSiteCodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimSiteCodes_Get') IS NOT NULL
	   DROP PROCEDURE RecHubData.usp_dimSiteCodes_Get
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimSiteCodes_Get

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/27/2013
*
* Purpose: Get All Site Code IDs
*
* Modification History
* 06/27/2013  WI  107512	EAS	Created
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

	SELECT   RecHubData.dimSiteCodes.SiteCodeID,
	         RecHubData.dimSiteCodes.ShortName
	FROM     RecHubData.dimSiteCodes
	ORDER BY RecHubData.dimSiteCodes.SiteCodeID

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH