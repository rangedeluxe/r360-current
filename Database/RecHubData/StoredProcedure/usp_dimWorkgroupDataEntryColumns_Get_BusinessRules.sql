--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimWorkgroupDataEntryColumns_Get_BusinessRules
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimWorkgroupDataEntryColumns_Get_BusinessRules
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmBatchSourceKey			SMALLINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/23/2017
*
* Purpose: Return information needed by the business rules engine.
*
* Modification History
* 03/23/2017 PT #135147147 JPB  Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		IsCheck,
		FieldName,
		IsRequired
	FROM 
		RecHubData.dimWorkgroupDataEntryColumns
	WHERE
		SiteBankID = @parmSiteBankID
		AND SiteClientAccountID = @parmSiteClientAccountID
		AND BatchSourceKey = @parmBatchSourceKey
		AND IsRequired = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
