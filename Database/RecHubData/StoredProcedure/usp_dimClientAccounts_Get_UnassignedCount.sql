--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_UnassignedCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_UnassignedCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_UnassignedCount
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_UnassignedCount
(
	@parmFIEntityID			INT,
	@parmUnassignedCount	INT OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2010-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 07/01/2014
*
* Purpose: Query dimClientAccounts for a count of records for a given FI that do not have 
*			a corresponding RecHubUser.OLWorkgroups tying them to RAAM
*
* Modification History
* 07/01/2014 WI 151234 KLC	Created
* 02/14/2018 PT 154804395 MGE Only Count Active Workgroups
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	@parmUnassignedCount = COUNT(*)
	FROM	RecHubData.dimClientAccountsView
		JOIN RecHubData.dimBanksView
			ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimBanksView.SiteBankID
		LEFT JOIN RecHubUser.OLWorkGroups
			ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
			AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID
	WHERE	RecHubData.dimBanksView.EntityID = @parmFIEntityID
		AND RecHubUser.OLWorkGroups.EntityID IS NULL
		AND RecHubData.dimClientAccountsView.IsActive = 1;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

