IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP;
GO

CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP
(
	@parmSessionID			UNIQUEIDENTIFIER,	   --WI 142820
	@parmDepositDateKey		INT,
	@parmMinDepositStatus	INT = 850,
	@parmWorkgroups			RecHubData.WorkgroupListTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CEJ
* Date: 05/13/2013
*
* Purpose: Gets the summary data for all the ClientAccounts that satisfy the 
*          given parameters.
*
* Modification History
* 05/13/2013 WI 106151 CEJ   Created 
* 10/08/2013 WI 116336 ASG	Add BatchSourceKey and BatchTypeKey to result set.
* 10/22/2013 WI 116336 KLC	Updated to use RecHubData.dimClientAccountsView
* 10/29/2013 WI 119117 KLC	Updated to use new procedures that select the pruned
*								org - client/account tree
* 10/31/2013 WI 119612 CMC  Updated to use RecHubUser.OLOrganizationTree.
* 11/08/2013 WI 122097 CMC  Use new view for Account Name.
* 11/11/2013 WI 122178 CMC  Putting ExceedsViewingDays back in
* 06/19/2014 WI 142820 DLD  RAAM Integration. 
* 07/07/2014 WI	142820 KLC	Removed checking of cutoff,
*								added site bank id to grouping thus preventing two
*								workgroups with the same site client account id
*								from grouping together when they should not
*								and changed to always load the latest workgroup name
*								thus preventing a workgroup with a name change
*								from splitting that workgroup into two groupings
* 07/25/2014 WI 155796 CEJ	Modify usp_factBatchSummary_Get_ClientAccountSummary_by_SP to include OLWorkgroupID in its resultset
* 07/30/2014 WI 142820 KLC	Updated to only return active workgroups.
* 08/14/2014 WI 142820 KLC	Updated to add back current processing date check
* 08/20/2014 WI 159911 JMC	Updated to retrieve dimClientAccountsView.DisplayLabel instead of dimClientAccountsView.LongName
* 10/09/2014 WI 170841 BLR	Just added EntityID to the select clause.
* 03/09/2015 WI 194850 JBS	Add in View Ahead Permission 
* 10/03/2017 #151440339 BLR Added ability to filter by workgroups.
* 10/13/2017 #151445364 JB/BR Added DDA to the result set.
* 10/23/2017 #151884042	MGE Corrected to get the right transaction count
* 11/07/2017 #147275517 JPB	Corrected temp table/dimDDA reference.
* 09/27/2018 #160347603 MGE Updated to improve performance of Dashboard
* 03/27/2019 R360-15940 MGE Updated to improve performance of Dashboard
* 05/09/2019 R360-16487 MGE Updated so that when DDA is empty string OR NULL they return ''
* 05/22/2019 R360-16487 MGE Updated 2.03.08.01 so that when DDA is empty string OR NULL they return ''
******************************************************************************/
SET NOCOUNT ON;

DECLARE @parmDepositDate DATETIME;
SET @parmDepositDate = CONVERT(DATETIME, CONVERT(VARCHAR(8), @parmDepositDateKey));

DECLARE @varSessionID UNIQUEIDENTIFIER;
SET @varSessionID = @parmSessionID;

BEGIN TRY

	--<<<<DROP TEMP TABLES IF THEY EXIST >>>>>
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpWorkgroup')) 
		DROP TABLE #tmpWorkgroup;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#factBatchSummary')) 
		DROP TABLE #factBatchSummary;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#factCheckSummary')) 
		DROP TABLE #factCheckSummary;

	CREATE TABLE #tmpWorkgroup
	(
	EntityName VARCHAR(128),
	SiteBankID INT,
	SiteClientAccountID INT,
	EntityID INT,
	WorkgroupName VARCHAR(73),
	OLWorkgroupID INT,
	MostRecent BIT,
	ClientAccountKey INT,
	StartDateKey INT,
	EndDateKey INT,
	ViewAhead BIT
	);

	CREATE TABLE #factBatchSummary
	(
	BatchID BIGINT,
	EntityName VARCHAR(128),
	SiteBankID INT,
	SiteClientAccountID INT,
	EntityID INT,
	WorkgroupName VARCHAR(73),
	BatchSource VARCHAR(128),
	OLWorkgroupID INT,
	BatchSourceKey SMALLINT,
	PaymentType VARCHAR(128),
	BatchPaymentTypeKey TINYINT,
	TransactionCount INT,
	CheckTotal MONEY,
	DepositDateKey INT
	);

	CREATE TABLE #factCheckSummary
	(
	BatchID BIGINT,
	DepositDateKey INT,
	DDAKey INT,
	DDA VARCHAR(35),
	ClientAccountKey INT,
	TransactionCount INT,
	ChecksTotalAmount MONEY
	);

	INSERT INTO #tmpWorkgroup
	(EntityName,SiteBankID,SiteClientAccountID,EntityID,WorkgroupName,OLWorkgroupID,MostRecent,ClientAccountKey,StartDateKey,EndDateKey,ViewAhead)
	SELECT RecHubUser.SessionClientAccountEntitlements.EntityName,
		RecHubUser.SessionClientAccountEntitlements.SiteBankID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,	--WI 142820
		RecHubUser.SessionClientAccountEntitlements.EntityID,
		CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR) + 
		CASE
			WHEN RecHubData.dimClientAccounts.LongName IS NULL OR RecHubData.dimClientAccounts.LongName = ''
			THEN ''
			ELSE
				' - ' + RecHubData.dimClientAccounts.LongName
		END	AS WorkgroupName, 
		RecHubUser.OLWorkgroups.OLWorkgroupID,
		RecHubData.dimClientAccounts.MostRecent,
		RecHubData.dimClientAccounts.ClientAccountKey,
		RecHubUser.SessionClientAccountEntitlements.StartDateKey,
		RecHubUser.SessionClientAccountEntitlements.EndDateKey,
		RecHubUser.SessionClientAccountEntitlements.ViewAhead
	FROM
		@parmWorkgroups AS parmWorkgroups
		INNER JOIN RecHubUser.OLWorkgroups
			ON parmWorkgroups.BankId = RecHubUser.OLWorkgroups.SiteBankID
			AND	parmWorkgroups.WorkgroupId = RecHubUser.OLWorkgroups.SiteClientAccountID
		INNER JOIN RecHubUser.SessionClientAccountEntitlements
			ON parmWorkgroups.BankId = RecHubUser.SessionClientAccountEntitlements.SiteBankID
			AND parmWorkgroups.WorkgroupId = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
			AND RecHubUser.SessionClientAccountEntitlements.SessionID = @varSessionID
		INNER JOIN RechubData.dimClientAccounts 
			ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @varSessionID												--WI 142820
		AND RecHubData.dimClientAccounts.IsActive = 1
	OPTION( RECOMPILE );

	;WITH MostRecents AS
	(SELECT SiteBankID, SiteClientAccountID, WorkgroupName, MostRecent
	FROM #tmpWorkgroup
	WHERE MostRecent = 1)
	UPDATE #tmpWorkgroup SET #tmpWorkgroup.WorkgroupName = MostRecents.WorkgroupName
	FROM #tmpWorkgroup
	INNER JOIN MostRecents
		ON #tmpWorkgroup.SiteBankID = MostRecents.SiteBankID
		AND #tmpWorkgroup.SiteClientAccountID = MostRecents.SiteClientAccountID
	WHERE #tmpWorkgroup.MostRecent = 0
	AND	MostRecents.MostRecent = 1
	AND MostRecents.WorkgroupName <> #tmpWorkgroup.WorkgroupName;


	INSERT INTO #factBatchSummary
	(BatchID,EntityName,SiteBankID,SiteClientAccountID,EntityID,WorkgroupName,BatchSource,OLWorkgroupID,BatchSourceKey, PaymentType, BatchPaymentTypeKey,TransactionCount,CheckTotal,DepositDateKey)

	SELECT
		RecHubData.factBatchSummary.BatchID,
		#tmpWorkgroup.EntityName,
		#tmpWorkgroup.SiteBankID,
		#tmpWorkgroup.SiteClientAccountID,	
		#tmpWorkgroup.EntityID,
		#tmpWorkgroup.WorkgroupName,
		RecHubData.dimBatchSources.LongName AS BatchSource,
		#tmpWorkgroup.OLWorkgroupID,
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey,
		RecHubData.factBatchSummary.TransactionCount,
		RecHubData.factBatchSummary.CheckTotal,
		RecHubData.factBatchSummary.DepositDateKey
	FROM 
		#tmpWorkgroup
		INNER JOIN RecHubData.factBatchSummary 
			ON #tmpWorkgroup.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources 
			ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes 
			ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
	WHERE 
		RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factBatchSummary.DepositDateKey >= #tmpWorkgroup.StartDateKey
		AND ((RecHubData.factBatchSummary.DepositDateKey <= #tmpWorkgroup.EndDateKey AND #tmpWorkgroup.ViewAhead = 0 ) OR (#tmpWorkgroup.ViewAhead = 1))
		AND RecHubData.factBatchSummary.DepositStatus >= @parmMinDepositStatus
		AND RecHubData.factBatchSummary.IsDeleted = 0
	OPTION( RECOMPILE );

	INSERT INTO #factCheckSummary
	(BatchID, DepositDateKey, DDAKey, DDA, ClientAccountKey, TransactionCount, ChecksTotalAmount)

	SELECT
		#factBatchSummary.BatchID,
		#factBatchSummary.DepositDateKey,
		COALESCE(RecHubData.factChecks.DDAKey,0) AS DDAKey,
		COALESCE(RecHubData.dimDDAs.DDA,'') AS DDA,
		COUNT(RecHubData.factChecks.ClientAccountKey) AS ClientAccountKey,
		COUNT(RecHubData.factTransactionSummary.factTransactionSummaryKey) AS TransactionCount,
		SUM(RecHubData.factChecks.Amount) AS ChecksTotalAmount
	FROM
		#factBatchSummary
		INNER JOIN RecHubData.factTransactionSummary ON RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factTransactionSummary.BatchID = #factBatchSummary.BatchID
			AND RecHubData.factTransactionSummary.IsDeleted = 0
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factChecks.BatchID = #factBatchSummary.BatchID
			AND RecHubData.factTransactionSummary.TransactionID = RecHubData.factChecks.TransactionID
			AND RecHubData.factChecks.IsDeleted = 0
		LEFT JOIN RecHubData.dimDDAs ON RecHubData.factChecks.DDAKey = RecHubData.dimDDAs.DDAKey
	GROUP BY 
		#factBatchSummary.BatchID, 
		#factBatchSummary.DepositDateKey,
		RecHubData.factChecks.DDAKey,
		DDA
	OPTION( RECOMPILE );
	
	SELECT
		#factBatchSummary.EntityName,
		#factBatchSummary.SiteBankID,
		#factBatchSummary.SiteClientAccountID,
		#factBatchSummary.EntityID,
		#factBatchSummary.WorkgroupName,
		#factBatchSummary.BatchSource,
		#factBatchSummary.OLWorkgroupID,
		#factBatchSummary.BatchSourceKey AS BatchSourceID,
		#factBatchSummary.PaymentType,
		#factBatchSummary.BatchPaymentTypeKey AS PaymentTypeID,
		COUNT(*) AS BatchCount,
		MAX(DDA) AS DDA,
		SUM(COALESCE(#factCheckSummary.TransactionCount, 0)) AS TransactionCount,
		SUM(#factCheckSummary.ClientAccountKey) AS PaymentCount,
		SUM(COALESCE(ChecksTotalAmount,0)) AS PaymentTotal
	FROM
		#factBatchSummary
		INNER JOIN #factCheckSummary on #factBatchSummary.BatchID = #factCheckSummary.BatchID
	GROUP BY
		EntityID,
		EntityName,
		SiteBankID,
		SiteClientAccountID,
		WorkgroupName,
		BatchSource,
		PaymentType,
		BatchSourceKey,
		BatchPaymentTypeKey,
		OLWorkgroupID,
		DDAKey
	OPTION( RECOMPILE );
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpWorkgroup')) 
		DROP TABLE #tmpWorkgroup;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#factBatchSummary')) 
		DROP TABLE #factBatchSummary;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#factCheckSummary')) 
		DROP TABLE #factCheckSummary;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

/* Set object permission */
GRANT EXECUTE ON OBJECT::RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP TO dbRole_RecHubData;

