--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetStubImage
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetStubImage') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetStubImage
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubData].[usp_GetStubImage] 
(
	@parmBatchID AS BIGINT,
	@parmBatchSequence AS INT,
	@parmDepositDate as INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 07/10/2017
*
* Purpose: Get Information neccesary to get a stub's image
*
* Modification History
* 07/10/2017 #144853381 MAA Created
************************************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT RecHubData.factStubs.BatchSiteCode,
	RecHubData.factStubs.BatchID, 
	RecHubData.dimClientAccounts.SiteBankID BankID,
	RecHubData.dimClientAccounts.SiteClientAccountID as LockboxID,
	RecHubData.factStubs.SourceProcessingDateKey AS ProcessingDateKey,
	RecHubData.factStubs.ImmutableDateKey AS PICSDate,
	RecHubData.factStubs.SourceBatchID, 
	RecHubData.factStubs.TransactionID,
	RecHubData.dimDocumentTypes.DocumentTypeDescription,
	RecHubData.dimClientAccounts.OnlineColorMode AS DefaultColorMode,
	RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
	RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
	RecHubData.factStubs.BatchPaymentTypeKey as PaymentType
	FROM RecHubData.factStubs 
	INNER JOIN RecHubData.dimClientAccounts 
		ON 	RecHubData.factStubs.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey 
	LEFT OUTER JOIN RecHubData.factDocuments
		ON RecHubData.factStubs.DepositDateKey = RecHubData.factDocuments.DepositDateKey
		AND RecHubData.factStubs.BatchID = RecHubData.factDocuments.BatchID
		AND RecHubData.factStubs.DocumentBatchSequence = RecHubData.factDocuments.BatchSequence
	LEFT OUTER JOIN  RecHubData.dimDocumentTypes
		ON RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
	INNER JOIN RecHubData.dimBatchSources 
		ON RecHubData.factStubs.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	INNER JOIN RecHubData.dimImportTypes
		ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	WHERE RecHubData.factStubs.BatchID = @parmBatchID 
	AND RecHubData.factStubs.BatchSequence = @parmBatchSequence 
	AND RecHubData.factStubs.DepositDateKey = @parmDepositDate
	AND RecHubData.factStubs.IsDeleted = 0
END TRY
BEGIN CATCH
	   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH