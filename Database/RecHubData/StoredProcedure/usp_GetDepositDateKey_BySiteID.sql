--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetDepositDateKey_BySiteID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetDepositDateKey_BySiteID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetDepositDateKey_BySiteID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_GetDepositDateKey_BySiteID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmBatchID				BIGINT,				--WI 142868
	@parmImmutableDateKey		INT,
	@parmBankKey				INT = 0 OUT,
	@parmClientAccountKey		INT = 0 OUT,
	@parmDepositDateKey			INT = 0 OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/10/2011
*
* Purpose: Retrieve a deposit date key based on Bank Key, Lockbox Key,
*	Processing Date Key and Batch ID.
*
*
*	The basis of this SP is the fact that most batches have the same processing
*	date and deposit date.
*	Step 1: Look for the batch deposit date key based on the input parms using
*		DepositDateKey = @parmProcessingDateKey
*	Step 2: If step 1 returns 0 rows, look for the deposit date key based on
*		the input parms using DepositDateKey BETWEEN @parmProcessingDateKey - 1
*		AND @parmProcessingDateKey + 7 
*		(I.e. look between 1 before and 7 days after the given processing date.)
*	Step 3: If both Step 1 and Step 2 fail to return a row, give up and search
*		for the batch based on the processing date. Note that this is the slowest 
*		method since the partitions cannot be used to narrow down the data being
*		looked at.
*
* Modification History
* 03/10/2011 CR 33332 JPB	Created
* 05/18/2011 CR	34378  WJS 	Need to add processingDate to guarantee uniqueness
* 03/18/2013 WI	90679  CRG 	Update Stored Procedure OLTA.usp_GetDepositDateKey_BySiteID for 2.0
*							Forward Patch: WI 85072 
* 05/30/2014 WI 142868 DLD  Batch Collision.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		@parmBankKey = -1, 
		@parmClientAccountKey = -1, 
		@parmDepositDateKey = NULL; --WI 142868

	-- FP: WI 85072 for all references to this table.
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CAKey')) 
              DROP TABLE #CAKey

	/* WI 85072  Create table and use table in following statements  */  
	CREATE TABLE #CAKey
	(
		ClientAccountKey INT
	);
	
	INSERT INTO #CAKey (ClientAccountKey)
	SELECT	ClientAccountKey
	FROM	RecHubData.dimClientAccounts
	WHERE  	RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID;

	/* Look for the deposit date = processing date */
	/* WI 85072  removed extra search parameters and join to temp table */
	SELECT 	
		@parmBankKey = RecHubData.factBatchSummary.BankKey,
		@parmClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey,
		@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey
	FROM	
		RecHubData.factBatchSummary
	INNER JOIN 
		#CAKey 
		ON RecHubData.factBatchSummary.ClientAccountKey = #CAKey.ClientAccountKey
	WHERE	
		RecHubData.factBatchSummary.DepositDateKey = @parmImmutableDateKey
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
			
	IF( @@ROWCOUNT = 0 )
	BEGIN /* Look for the deposit date between 1 day before and 7 days after the give processing date */
		SELECT 	
			@parmBankKey = RecHubData.factBatchSummary.BankKey,
			@parmClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey,
			@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey
		FROM	
			RecHubData.factBatchSummary
		INNER JOIN 
			#CAKey 
			ON RecHubData.factBatchSummary.ClientAccountKey = #CAKey.ClientAccountKey
		WHERE	
			RecHubData.factBatchSummary.DepositDateKey 
				BETWEEN CAST(CONVERT(VARCHAR, CONVERT(DATETIME ,CONVERT(VARCHAR, @parmImmutableDateKey, 112), 112) - 1 , 112) as INT) 
				AND CAST(CONVERT(VARCHAR, CONVERT(DATETIME ,CONVERT(VARCHAR, @parmImmutableDateKey, 112), 112) + 7 , 112) as INT)
			AND RecHubData.factBatchSummary.BatchID = @parmBatchID
			AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey

		IF( @@ROWCOUNT = 0 )
		BEGIN /* give up, just scan the table. Hopefully we never get here */
			SELECT 	
				@parmBankKey = RecHubData.factBatchSummary.BankKey,
				@parmClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey,
				@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey
			FROM	
				RecHubData.factBatchSummary
			INNER JOIN 
				#CAKey 
				ON RecHubData.factBatchSummary.ClientAccountKey = #CAKey.ClientAccountKey
			WHERE	
				RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
				AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		END
	END 
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CAKey')) 
		DROP TABLE #CAKey
	END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CAKey')) 
		DROP TABLE #CAKey
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH