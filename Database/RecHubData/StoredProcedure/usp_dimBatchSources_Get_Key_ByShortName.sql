--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBatchSources_Get_Key_ByShortName
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchSources_Get_Key_ByShortName') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchSources_Get_Key_ByShortName
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchSources_Get_Key_ByShortName
(
	@parmShortName			VARCHAR(40),
	@parmBatchSourceKey		SMALLINT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/04/2014
*
* Purpose: Retreived the batch source key for a given short name
*
* Modification History
* 06/04/2014 WI 145750 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	@parmBatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	FROM	RecHubData.dimBatchSources
	WHERE	RecHubData.dimBatchSources.ShortName = @parmShortName;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

