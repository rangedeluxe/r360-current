--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Get_BaseKeysByAmountAccountNumber
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Get_BaseKeysByAmountAccountNumber') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Get_BaseKeysByAmountAccountNumber
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Get_BaseKeysByAmountAccountNumber
(
	@parmQueryBuilder RecHubData.InvoiceSearchQueryBuilderTable READONLY,
	@parmBaseKeys RecHubData.InvoiceSearchBaseKeysTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2017
*
* Purpose: Filter the base keys by Amount and/or AccountNumber
*
* Modification History
* 03/06/2017 #136913453 JPB Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @SQL NVARCHAR(MAX);

BEGIN TRY 

	SET @SQL = N'
	SELECT
		BaseKeys.factStubKey,
		BaseKeys.DepositDateKey,
		BaseKeys.BatchSourceKey,
		BaseKeys.BatchPaymentTypeKey,
		BaseKeys.EntityID,
		BaseKeys.SiteBankID,
		BaseKeys.SiteClientAccountID,
		BaseKeys.SessionID
	FROM 
		@parmBaseKeys AS BaseKeys
		INNER JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = BaseKeys.DepositDateKey
			AND RecHubData.factStubs.factStubKey = BaseKeys.factStubKey
	WHERE
		';

	SELECT  
		@SQL += 
		FieldName + N' ' +
		Operator + N' ' +
		CASE Operator
			WHEN 'LIKE' THEN QUOTENAME(Value,CHAR(39))
			ELSE Value
		END + ' AND '
	FROM 
		@parmQueryBuilder
	WHERE
		UPPER(FieldName) = 'AMOUNT' 
		OR UPPER(FieldName) = 'ACCOUNTNUMBER'

	--Remove AND
	SET @SQL = SUBSTRING(@SQL,1,LEN(@SQL)-4);

	SET @SQL += N'
	OPTION (RECOMPILE);'

	EXEC SP_EXECUTESQL @SQL, N'@parmBaseKeys RecHubData.InvoiceSearchBaseKeysTable READONLY', @parmBaseKeys;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH