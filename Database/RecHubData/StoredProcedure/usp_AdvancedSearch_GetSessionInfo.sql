IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSessionInfo') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSessionInfo
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSessionInfo
(
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT = -1,
	@parmSiteClientAccountID	INT = -1
)
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Support SP to get session information for advanced search
*
* Modification History
* 08/19/2019 R360-16301 JPB	Created
***********************************************************************************************************************************************/
AS
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY

	SELECT 
		RecHubUser.SessionClientAccountEntitlements.SiteBankID, 
		RecHubUser.SessionClientAccountEntitlements.EntityID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID, 
		RecHubData.dimClientAccounts.ClientAccountKey, 
		RecHubData.dimClientAccounts.SiteCodeID,
		RecHubData.dimClientAccounts.MostRecent,
		RecHubUser.SessionClientAccountEntitlements.StartDateKey,
		RecHubUser.SessionClientAccountEntitlements.EndDateKey,
		RecHubUser.SessionClientAccountEntitlements.ViewAhead
	FROM RecHubUser.SessionClientAccountEntitlements
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			AND RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID =
			CASE 
			WHEN @parmSiteClientAccountID > 0 THEN @parmSiteClientAccountID
			ELSE RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
			END;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpDimensions')) 
		DROP TABLE #tmpDimensions;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
