--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Get_BySiteCodeID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Get_BySiteCodeID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Get_BySiteCodeID
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Get_BySiteCodeID
(
	@parmSiteCodeID INT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/28/2013
*
* Purpose: Get Site Bank IDs related to Site Code IDs
*
* Modification History
* 06/28/2013  WI  107529	EAS	Created
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

	SELECT DISTINCT RecHubData.dimBanksView.SiteBankID,
	                RecHubData.dimBanksView.BankName
	FROM       RecHubData.dimBanksView
	INNER JOIN RecHubData.dimClientAccounts ON (RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimBanksView.SiteBankID)
	WHERE      RecHubData.dimClientAccounts.SiteCodeID = @parmSiteCodeID
	ORDER BY   RecHubData.dimBanksView.SiteBankID

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH