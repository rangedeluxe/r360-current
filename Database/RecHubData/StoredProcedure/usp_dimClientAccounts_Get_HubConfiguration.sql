--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_HubConfiguration
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_HubConfiguration') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_HubConfiguration
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_HubConfiguration 
(
	@parmClientAccountKey	INT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 04/03/2014
*
* Purpose: Query dimClientAccounts table by ClientAccountKey, joining to
*			OLWorkGroups for maintenance in Hub Configuration
*
* Modification History
* 04/03/2014 WI 130464 	  KLC	Created
* 01/20/2015 WI 185216 	  RDS	Include Billing Fields
* 03/19/2015 WI 196656 	  BLR  	No longer joining on the session entitlements,
*                           	we'll be doing this after the call.
* 01/07/2017 #132047521   JAW  	Include dimWorkgroupBusinessRules.PaymentsOnlyTransaction
* 02/03/2017 PT 138212537 JBS 	Add BalancingRequired
* 03/28/2017 PT#142143773 MAA   Changed PaymentsOnlyTransaction to InvoiceRequired
* 07/14/2017 PT#141767379 MAA   Added PostDepositBalancingRequired
* 01/18/2019 R360-15289	  JPB	Added PostDepositPayerRequired
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT DISTINCT --Make it distinct as the entitlements will have all historical versions of a workgroup
			RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccounts.MostRecent,
			RecHubData.dimClientAccounts.IsActive,
			RecHubData.dimClientAccounts.DataRetentionDays,
			RecHubData.dimClientAccounts.ImageRetentionDays,
			RecHubData.dimClientAccounts.ShortName,
			RecHubData.dimClientAccounts.LongName,
			RecHubData.dimClientAccounts.[FileGroup],
			RecHubUser.OLWorkGroups.EntityID,
			RecHubUser.OLWorkGroups.ViewingDays,
			RecHubUser.OLWorkgroups.MaximumSearchDays,
			RecHubUser.OLWorkGroups.CheckImageDisplayMode,
			RecHubUser.OLWorkGroups.DocumentImageDisplayMode,
			RecHubUser.OLWorkGroups.HOA,
			RecHubUser.OLWorkGroups.DisplayBatchID,
			RecHubUser.OLWorkGroups.InvoiceBalancingOption,
			RecHubuser.OLWorkGroups.BillingAccount,
			RecHubUser.OLWorkGroups.BillingField1,
			RecHubUser.OLWorkGroups.BillingField2,
			dimWorkgroupBusinessRules.InvoiceRequired,
			dimWorkgroupBusinessRules.BalancingRequired,
			dimWorkgroupBusinessRules.PostDepositBalancingRequired,
			dimWorkgroupBusinessRules.PostDepositPayerRequired
	FROM	RecHubData.dimClientAccounts
		JOIN RecHubUser.OLWorkGroups
			ON	RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
				AND	RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID
		LEFT OUTER JOIN RecHubData.dimWorkgroupBusinessRules
			ON RecHubData.dimClientAccounts.SiteBankId = RecHubData.dimWorkgroupBusinessRules.SiteBankId
				AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimWorkgroupBusinessRules.SiteClientAccountID
	WHERE	RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

