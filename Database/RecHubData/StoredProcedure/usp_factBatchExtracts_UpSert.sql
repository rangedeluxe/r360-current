--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchExtracts_UpSert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_factBatchExtracts_UpSert') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factBatchExtracts_UpSert;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchExtracts_UpSert
(
	@parmBatchID               BIGINT, --WI 142814
	@parmExtractSequenceNumber BIGINT
)	
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103914 JMC  Initial Version
*							Added ModificationDate logic
*					   JBS	added Permissions tags
* 05/19/2014 WI 142814 DLD  Batch Collision.  Populate SourceBatchID.
* 11/11/2014 WI 177121 BLR  Batch Collision. We only need BatchID, Sequence Number.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	IF EXISTS 
	(
		SELECT RecHubData.factBatchExtracts.BatchID
		FROM RecHubData.factBatchExtracts
		WHERE RecHubData.factBatchExtracts.BatchID = @parmBatchID
	)
	BEGIN
		UPDATE RecHubData.factBatchExtracts
		SET
			IsDeleted = 0,
			ExtractSequenceNumber = @parmExtractSequenceNumber,
			ModificationDate = GETDATE()
		FROM RecHubData.factBatchExtracts
		WHERE RecHubData.factBatchExtracts.BatchID = @parmBatchID;
	END
	ELSE
	BEGIN
		INSERT INTO RecHubData.factBatchExtracts
		(
			IsDeleted,
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			DepositDateKey,
			ImmutableDateKey,
			BatchID,
			SourceBatchID,	--WI 142814
			ExtractSequenceNumber,
			ModificationDate 
		)
		SELECT
			0,
			RecHubData.factBatchSummary.BankKey,
			RecHubData.factBatchSummary.OrganizationKey,
			RecHubData.factBatchSummary.ClientAccountKey,
			RecHubData.factBatchSummary.DepositDateKey,
			RecHubData.factBatchSummary.ImmutableDateKey,
			@parmBatchID,
			RecHubData.factBatchSummary.SourceBatchID,	--WI 142814
			@parmExtractSequenceNumber,
			GETDATE()
		FROM RecHubData.factBatchSummary
		WHERE RecHubData.factBatchSummary.BatchID = @parmBatchID;
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
