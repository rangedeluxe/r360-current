--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetCheckImage
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetCheckImage') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetCheckImage
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE  PROCEDURE [RecHubData].[usp_GetCheckImage] 
(
	@parmBatchID AS BIGINT,
	@parmBatchSequence AS INT,
	@parmDepositDate as INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 07/10/2017
*
* Purpose: Get Information neccesary to get a check's image
*
* Modification History
* 07/10/2017 #144853381 MAA Created
************************************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT RecHubData.factChecks.BatchSiteCode, 
	RecHubData.factChecks.BatchID, 
	RecHubData.dimClientAccounts.SiteBankID BankID,
	RecHubData.dimClientAccounts.SiteClientAccountID as LockboxID, 
	RecHubData.factChecks.SourceProcessingDateKey AS ProcessingDateKey,
	RecHubData.factChecks.ImmutableDateKey AS PICSDate, 
	RecHubData.factChecks.SourceBatchID, 
	RecHubData.factChecks.TransactionID,
	'C' AS FileDescriptor, 
	RecHubData.dimClientAccounts.OnlineColorMode AS DefaultColorMode,
	RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
	RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
	RecHubData.factChecks.BatchPaymentTypeKey as PaymentType
	FROM RecHubData.factChecks 
	INNER JOIN RecHubData.dimClientAccounts 
		ON 	RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey 
	INNER JOIN RecHubData.dimBatchSources 
		ON RecHubData.factChecks.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	INNER JOIN RecHubData.dimImportTypes
		ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	WHERE BatchID = @parmBatchID 
	AND BatchSequence = @parmBatchSequence 
	AND DepositDateKey = @parmDepositDate
	AND IsDeleted = 0
END TRY
BEGIN CATCH
	   EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
