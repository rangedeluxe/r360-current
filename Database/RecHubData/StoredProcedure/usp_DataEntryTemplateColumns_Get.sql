--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_DataEntryTemplateColumns_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_DataEntryTemplateColumns_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_DataEntryTemplateColumns_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_DataEntryTemplateColumns_Get
(
	 @parmTemplateShortName VARCHAR(32)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/02/2014
*
* Purpose: Get the data entry columns for a given data entry template.
*
* Modification History
* 06/02/2014 WI 143429 JPB	Created
* 08/28/2015 WI 230302 MAA  Updated to return WorkgroupDataentryColumns fields
******************************************************************************/
SET NOCOUNT ON; 
	
BEGIN TRY
	SELECT 
		RecHubData.DataEntryTemplateColumns.DataType,
		RecHubData.DataEntryTemplateColumns.ScreenOrder,
		RecHubData.DataEntryTemplateColumns.FieldName,
		RecHubData.DataEntryTemplateColumns.UILabel,
		RecHubData.DataEntryTemplateColumns.IsCheck AS TableType,
		CASE
		WHEN RecHubData.DataEntryTemplateColumns.IsCheck = 1 THEN 'Checks'
		ELSE 'Stubs'
		END AS 'TableName'
	FROM
		RecHubData.DataEntryTemplateColumns
		INNER JOIN RecHubData.DataEntryTemplates ON RecHubData.DataEntryTemplates.DataEntryTemplateID = RecHubData.DataEntryTemplateColumns.DataEntryTemplateID
	WHERE
		RecHubData.DataEntryTemplates.TemplateShortName = @parmTemplateShortName
	ORDER BY
		RecHubData.DataEntryTemplateColumns.ScreenOrder;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
