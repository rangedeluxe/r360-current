--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factTables_Upd_DepositStatus
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTables_Upd_DepositStatus') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTables_Upd_DepositStatus
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTables_Upd_DepositStatus
(
    @BankID				INT, 
	@ClientAccountID	INT, 
	@BatchID			INT, 
	@ImmutableDate		DATETIME,
	@DepositDate		DATETIME
)	   
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CS
* Date: 11/12/2010
*
* Purpose: Update batch fact tables when dbo.Batch.DepositStatus changes.  
*		   Currently, this stored procedure is called by the tu_batch trigger ONLY when	
*          the DepositStatus changes from 825 to 850.
*
* Modification History
* 11/12/2010 CR 31611 CS	Created
* 01/07/2011 CR 32229 JPB	Removed factTransactionDetails update.
* 01/23/2012 CR 49591 JPB	Added SourceProcessingDate.
* 04/17/2013 WI 90802 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_UpdateFactTablesDepositStatus
*							Change references: Processing to Immutable,
*							Lockbox to ClientAccount, Customer to Organization.
*							Change Parameters: 	@LockboxID	TO 	@ClientAccountID, 
*								@ProcessingDate TO @ImmutableDate.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;
SET XACT_ABORT ON;

BEGIN TRY

	DECLARE @bLocalTransaction			BIT,
			@iBankKey					INT,
			@iOrganizationKey			INT,
			@iClientAccountKey			INT,
			@iImmutableDateKey			INT,
			@iSourceProcessingDateKey	INT,
			@iDepositDateKey			INT,
			@iBatchID					INT,
			@XBatch						XML;

	SELECT @XBatch = 
				(
					SELECT	@BankID AS '@BankID',
							@ClientAccountID AS '@ClientAccountID',
							@BatchID AS '@BatchID',
							@ImmutableDate AS '@ImmutableDate',
							@DepositDate as '@DepositDate'
					FOR XML PATH('Batch'), TYPE, ROOT('Batches')
				);

	EXECUTE RecHubData.usp_ConvertDBOKeysToOLTAKeys @XBatch, @iBankKey OUTPUT, @iOrganizationKey OUTPUT, @iClientAccountKey OUTPUT, @iImmutableDateKey OUTPUT, @iDepositDateKey OUTPUT, @iSourceProcessingDateKey OUTPUT;


	UPDATE	RecHubData.factBatchSummary 
	SET		RecHubData.factBatchSummary.DepositStatus = 850, 
			RecHubData.factBatchSummary.ModificationDate = GETDATE() 
	WHERE	RecHubData.factBatchSummary.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factBatchSummary.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factBatchSummary.BankKey = @iBankKey 
			AND RecHubData.factBatchSummary.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factBatchSummary.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factBatchSummary.BatchID = @BatchID 
			AND RecHubData.factBatchSummary.DepositStatus = 825
			AND RecHubData.factBatchSummary.IsDeleted = 0;  
		
	UPDATE	RecHubData.factDataEntryDetails 
	SET		RecHubData.factDataEntryDetails.DepositStatus = 850, 
			RecHubData.factDataEntryDetails.ModificationDate = GETDATE() 
	WHERE	RecHubData.factDataEntryDetails.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factDataEntryDetails.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factDataEntryDetails.BankKey = @iBankKey 
			AND RecHubData.factDataEntryDetails.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factDataEntryDetails.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factDataEntryDetails.BatchID = @BatchID 
			AND RecHubData.factDataEntryDetails.DepositStatus = 825
			AND RecHubData.factDataEntryDetails.IsDeleted = 0; 
		
	UPDATE RecHubData.factDataEntrySummary 
	SET		RecHubData.factDataEntrySummary.DepositStatus = 850 
	WHERE	RecHubData.factDataEntrySummary.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factDataEntrySummary.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factDataEntrySummary.BankKey = @iBankKey 
			AND RecHubData.factDataEntrySummary.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factDataEntrySummary.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factDataEntrySummary.BatchID = @BatchID 
			AND RecHubData.factDataEntrySummary.DepositStatus = 825
			AND RecHubData.factDataEntrySummary.IsDeleted = 0; 
		 
	UPDATE RecHubData.factTransactionSummary 
	SET		RecHubData.factTransactionSummary.DepositStatus = 850, 
			RecHubData.factTransactionSummary.ModificationDate = GETDATE() 
	WHERE	RecHubData.factTransactionSummary.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factTransactionSummary.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factTransactionSummary.BankKey = @iBankKey 
			AND RecHubData.factTransactionSummary.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factTransactionSummary.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factTransactionSummary.BatchID = @BatchID 
			AND RecHubData.factTransactionSummary.DepositStatus = 825
			AND RecHubData.factTransactionSummary.IsDeleted = 0;  

	UPDATE RecHubData.factChecks 
	SET		RecHubData.factChecks.DepositStatus = 850, 
			RecHubData.factChecks.ModificationDate = GETDATE() 
	WHERE	RecHubData.factChecks.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factChecks.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factChecks.BankKey = @iBankKey 
			AND RecHubData.factChecks.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factChecks.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factChecks.BatchID = @BatchID 
			AND RecHubData.factChecks.DepositStatus = 825
			AND RecHubData.factChecks.IsDeleted = 0;  

	UPDATE RecHubData.factDocuments 
	SET		RecHubData.factDocuments.DepositStatus = 850, 
			RecHubData.factDocuments.ModificationDate = GETDATE() 
	WHERE	RecHubData.factDocuments.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factDocuments.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factDocuments.BankKey = @iBankKey 
			AND RecHubData.factDocuments.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factDocuments.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factDocuments.BatchID = @BatchID 
			AND RecHubData.factDocuments.DepositStatus = 825
			AND RecHubData.factDocuments.IsDeleted = 0;  
		
	UPDATE RecHubData.factStubs 
	SET		RecHubData.factStubs.DepositStatus = 850, 
			RecHubData.factStubs.ModificationDate = GETDATE() 
	WHERE	RecHubData.factStubs.DepositDateKey = @iDepositDateKey 
			AND RecHubData.factStubs.ImmutableDateKey = @iImmutableDateKey 
			AND RecHubData.factStubs.BankKey = @iBankKey 
			AND RecHubData.factStubs.ClientAccountKey = @iClientAccountKey 
			AND RecHubData.factStubs.SourceProcessingDateKey = @iSourceProcessingDateKey 
			AND RecHubData.factStubs.BatchID = @BatchID 
			AND RecHubData.factStubs.DepositStatus = 825
			AND RecHubData.factStubs.IsDeleted = 0; 

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO

	
