--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_DataEntryFieldsOther
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_DataEntryFieldsOther') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_DataEntryFieldsOther
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_DataEntryFieldsOther 
(
	@parmSessionID				UNIQUEIDENTIFIER,						--WI 142809
    @parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Bring back data entry fields for other types
*
* Modification History
* 01/24/2013 WI 86291  TWE  Created by converting embedded SQL
*							Moved to RecHubData schema.
*							Reanamed @parmSiteLockboxID to @parmSiteClientAccountID.
* 05/19/2014 WI 142809 PKW  Update for RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE
		@StartDateKey INT,
		@EndDateKey INT;

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

    SELECT	
        COALESCE(RecHubData.dimDataEntryColumns.DisplayName, RecHubData.dimDataEntryColumns.FldName) AS FldTitle,
        RTRIM(RecHubData.dimDataEntryColumns.FldName) AS FldName,
        RecHubData.dimDataEntryColumns.TableName AS TableName,
        CASE
          WHEN (RecHubData.dimDataEntryColumns.TableType = 2 AND RTRIM(RecHubData.dimDataEntryColumns.FldName) = 'Amount')   
			THEN 7
			ELSE RecHubData.dimDataEntryColumns.DataType
        END AS FldDataTypeEnum,
        RecHubData.dimDataEntryColumns.FldLength AS FldLength
    FROM
        RecHubData.factDataEntrySummary
        INNER JOIN RecHubData.dimDataEntryColumns ON
            RecHubData.dimDataEntryColumns.DataEntryColumnKey = RecHubData.factDataEntrySummary.DataEntryColumnKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142809
			RecHubData.factDataEntrySummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
    WHERE  
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
        AND RecHubData.factDataEntrySummary.DepositDateKey = @StartDateKey  --WI 142809	
        AND RecHubData.factDataEntrySummary.BatchID = @parmBatchID
        AND RecHubData.dimDataEntryColumns.TableType IN(2, 3)
    ORDER BY 
        RecHubData.dimDataEntryColumns.ScreenOrder ASC;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH