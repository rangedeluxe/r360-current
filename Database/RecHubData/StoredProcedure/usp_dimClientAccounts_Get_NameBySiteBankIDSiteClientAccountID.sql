--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 09/19/2016
*
* Purpose: Retrieve the Name for given dimClientAccounts by SiteBankID
*
* Modification History
* 09/19/2016 PT #129555329 JBS	Created
******************************************************************************/

SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		 RTRIM(RecHubData.dimClientAccounts.LongName)		AS LongName
	FROM RecHubData.dimClientAccounts
	WHERE RecHubData.dimClientAccounts.MostRecent = 1
		 AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		 AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

