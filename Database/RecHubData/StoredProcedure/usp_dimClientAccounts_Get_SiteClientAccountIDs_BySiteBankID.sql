--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_SiteClientAccountIDs_BySiteBankID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_SiteClientAccountIDs_BySiteBankID') IS NOT NULL
	   DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_SiteClientAccountIDs_BySiteBankID
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_SiteClientAccountIDs_BySiteBankID
(
	@parmSiteBankID INT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/28/2013
*
* Purpose: Get Site Client Account IDs related to Site Bank IDs
*
* Modification History
* 06/28/2013  WI  107543	EAS	Created
* 11/26/2013  WI  123474    EAS Use LongName and not ShortName
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

	SELECT   RecHubData.dimClientAccountsView.SiteClientAccountID,
	         RecHubData.dimClientAccountsView.LongName
	FROM     RecHubData.dimClientAccountsView
	WHERE    RecHubData.dimClientAccountsView.SiteBankID = @parmSiteBankID
	AND      RecHubData.dimClientAccountsView.IsActive = 1
	ORDER BY RecHubData.dimClientAccountsView.SiteClientAccountID
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
