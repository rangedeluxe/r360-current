--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factItemData_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factItemData_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factItemData_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factItemData_Upd_IsDeleted
(
	@parmDepositDateKey				INT,
	@parmBatchID					BIGINT,		--WI 142857
	@parmModificationDate			DATETIME
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/10/2011
*
* Purpose: Deletes factItemData.
*
* Modification History
* 03/10/2011 CR  33292 WJS	Created
* 04/14/2011 CR  33292 WJS	Change to use Id instead of keys
* 01/19/2012 CR  49135 JPB	Lookup DepositDateKey before delete.
* 04/22/2014 WI 137861 CMC	Update to 2.0 release.  Change schema to RecHubData.
* 06/02/2014 WI 142857 DLD  Batch Collision. 
******************************************************************************/
SET NOCOUNT ON;	--WI 142857

BEGIN TRY
	
	UPDATE 	RecHubData.factItemData 
	SET		RecHubData.factItemData.IsDeleted = 1,
			RecHubData.factItemData.ModificationDate = @parmModificationDate
	WHERE	
		RecHubData.factItemData.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factItemData.BatchID = @parmBatchID
		AND RecHubData.factItemData.IsDeleted = 0

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException 
END CATCH