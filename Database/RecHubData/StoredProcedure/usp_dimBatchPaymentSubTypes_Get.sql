--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBatchPaymentSubTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchPaymentSubTypes_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchPaymentSubTypes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchPaymentSubTypes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/29/2014
*
* Purpose: Reads all records from the RecHubData.dimBatchPaymentSubTypes table
*
* Modification History
* 04/29/2014 WI 138862 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	RecHubData.dimBatchPaymentSubTypes.BatchPaymentSubTypeKey,
			RecHubData.dimBatchPaymentSubTypes.BatchPaymentTypeKey,
			RecHubData.dimBatchPaymentSubTypes.SubTypeDescription
	FROM	RecHubData.dimBatchPaymentSubTypes
	ORDER BY RecHubData.dimBatchPaymentSubTypes.BatchPaymentSubTypeKey ASC;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

