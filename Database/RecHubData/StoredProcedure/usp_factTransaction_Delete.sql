--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransaction_Delete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransaction_Delete') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransaction_Delete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransaction_Delete
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmSourceBatchID			BIGINT,	
	@parmDepositDateKey         INT,
	@parmImmutableDateKey		INT,
	@parmBatchSource            VARCHAR(30),
	@parmTransactionID			INT = NULL,
	@parmTxnSequence			INT = NULL,
	@parmRowsDeleted			BIT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 08/29/2013
*
* Purpose: Mark a transaction as deleted.
*
* Modification History
* 08/29/2013 WI 112904 JBS	Created
* 11/14/2013 WI 122499 JBS	Change now using TxnSequence to get TransactionID to pass to other procs
* 02/25/2015 WI 191797 JPB	FP from OLTA.usp_factTransactionDelete.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed @parmBatchID to @parmSourceBatchID.
*							Renamed @parmProcessingDate to @parmImmutableDateKey.
*							Renamed @parmProcessingDate to @parmImmutableDateKey.
*							Renamed @parmDepositDate to @parmDepositDateKey.
*							Added @parmBatchSource.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @BatchID			BIGINT, 
		@BatchSourceKey		SMALLINT,
		@ModificationDate	DATETIME,
		@BankKey			INT,
		@CustomerKey		INT,
		@LockboxKey			INT,
		@DepositDateKey		INT,
		@ProcessingDateKey	INT,
		@ActionCode			INT,
		@TransactionID		INT;

BEGIN TRY
	SET @parmRowsDeleted = 0;
    /* go find out what the batch source key is */
	SELECT
	   @BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
	FROM 
	    RecHubData.dimBatchSources
	WHERE
	    RecHubData.dimBatchSources.ShortName = @parmBatchSource
		AND RecHubData.dimBatchSources.IsActive = 1;

	IF @BatchSourceKey IS NULL 
	BEGIN
		RAISERROR('Batch Source Not Found',16,1);
	END

	/* Find the R360 BatchID needed to delete a batch from the RecHubData tables. */
	SELECT 	
		@BatchID = RecHubData.factTransactionSummary.BatchID,	 
		@TransactionID = RecHubData.factTransactionSummary.TransactionID,
		@ModificationDate = GETDATE()
	FROM	
		RecHubData.factTransactionSummary
		INNER JOIN RecHubData.dimBanks ON RecHubData.factTransactionSummary.BankKey = RecHubData.dimBanks.BankKey
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE	
		RecHubData.dimBanks.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factTransactionSummary.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factTransactionSummary.BatchSourceKey = @BatchSourceKey
		AND RecHubData.factTransactionSummary.TransactionID = CASE WHEN @parmTransactionID IS NULL THEN RecHubData.factTransactionSummary.TransactionID ELSE @parmTransactionID END
		AND RecHubData.factTransactionSummary.TxnSequence = CASE WHEN @parmTxnSequence IS NULL THEN RecHubData.factTransactionSummary.TxnSequence ELSE @parmTxnSequence END
		AND RecHubData.factTransactionSummary.IsDeleted = 0;

	IF @BatchID IS NULL --WI 142810
	BEGIN
		RAISERROR('Transaction Not Found',16,1);
	END

	BEGIN TRANSACTION;

	/* Delete records from the fact tables */
	EXEC RecHubData.usp_factTransactionSummary_Upd_IsDeleted_ByTransactionID 
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate, 
		@parmRowsDeleted=@parmRowsDeleted OUT;
	EXEC RecHubData.usp_factChecks_Upd_IsDeleted_ByTransactionID
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate;
	EXEC RecHubData.usp_factDocuments_Upd_IsDeleted_ByTransactionID
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate;
	EXEC RecHubData.usp_factStubs_Upd_IsDeleted_ByTransactionID
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate;
	EXEC RecHubData.usp_factDataEntryDetails_Upd_IsDeleted_ByTransactionID
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate;
	EXEC RecHubData.usp_factItemData_Upd_IsDeleted_ByTransactionID
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmTransactionID=@TransactionID,
		@parmModificationDate=@ModificationDate;
	EXEC RecHubData.usp_factBatchSummary_Upd_Transaction
		@parmDepositDateKey=@parmDepositDateKey, 
		@parmBatchID=@BatchID, 
		@parmModificationDate=@ModificationDate;

	COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF XACT_STATE() != 0 
		ROLLBACK TRANSACTION;
	SET @parmRowsDeleted = 0;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH