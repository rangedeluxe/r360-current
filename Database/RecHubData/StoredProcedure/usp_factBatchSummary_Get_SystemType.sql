--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_SystemType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_SystemType') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_SystemType
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_SystemType 
(
    @parmSessionID				UNIQUEIDENTIFIER,						--WI 142825
	@parmSiteBankID				INT,
    @parmSiteClientAccountID	INT,
    @parmImmutableDate			DATETIME,
    @parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 01/24/2013
*
* Purpose: Get summary information by system type.
*
* Modification History
* 01/24/2013 WI 86358  TWE  Created by converting embedded SQL.
*							Moved to RecHubData.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed @parmProcessingDate to @parmImmutableDate.
* 05/22/2014 WI 142825 PKW  Batch Collision/RAAM Integration.
******************************************************************************/
SET NOCOUNT ON; 
DECLARE 
	@BankKey			INT,
	@ClientAccountKey	INT,
	@DepositDateKey		INT,
	@DepositDate		DATETIME,
	@ImmutableDateKey	INT,												--WI 142825
	@StartDateKey		INT,												--WI 142825
	@EndDateKey			INT;												--WI 142825

BEGIN TRY

	SET @ImmutableDateKey = CAST(CONVERT(VARCHAR,@parmImmutableDate,112) AS INT);

	/* Ensure the ImmutableDateKey conversion into DepositDate */			--WI 142825
	EXEC RecHubData.usp_GetDepositDateKey_BySiteID							--WI 142825
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDate,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SET @DepositDate = CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10), @DepositDateKey), 112), 101);

	/* Ensure the start date is not beyond the max viewing days */			--WI 142834
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays						--WI 142825
		@parmSessionID = @parmSessionID,									--WI 142825
		@parmSiteBankID = @parmSiteBankID,									--WI 142825
		@parmSiteClientAccountID = @parmSiteClientAccountID,				--WI 142825
		@parmDepositDateStart = @DepositDate,								--WI 142825
		@parmStartDateKey = @StartDateKey OUT,								--WI 142825
		@parmEndDateKey = @EndDateKey OUT;									--WI 142825

	SELECT 
		RecHubData.factBatchSummary.SystemType 
	FROM  
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142825
			RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
  	WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factBatchSummary.DepositDateKey = @StartDateKey		--WI 142825	
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDate --@ImmutableDateKey --WI 142825
		AND IsDeleted = 0;    

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH