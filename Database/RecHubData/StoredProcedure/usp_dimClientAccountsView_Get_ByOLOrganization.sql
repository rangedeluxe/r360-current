--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccountsView_Get_ByOLOrganization
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByOLOrganization 
(
	@parmOLOrganizationID	UNIQUEIDENTIFIER,
	@parmOLClientAccountID	UNIQUEIDENTIFIER,
	@parmUserID				INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817 JMC - Created
* 04/26/2010 CR 29424 JMC - Reconcilled inconsistent column names returned 
*                           from SELECT.
* 06/12/2010 CR 29925 JMC - Change references from dimLockboxes to 
*                           dimLockboxesView.
* 03/04/2012 CR 50507 JPB	Removed reference to dbo.SiteCodes.
* 03/26/2013 WI 90700 JPB	Upgraded to 2.0.
*							Renamed and moved to RecHubData schema.
*							Renamed @parmOLCustomerID to @parmOLOrganizationID
*							Renamed @parmOLLockboxID to @parmOLClientAccountID
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @IsSuperUser BIT;

	SELECT	@IsSuperUser = SuperUser 
	FROM	RecHubUser.Users 
	WHERE	UserID = @parmUserID;

	IF @IsSuperUser > 0
	BEGIN
		SELECT	
			RecHubUser.OLClientAccounts.OLOrganizationID, 
			RecHubUser.OLClientAccounts.OLClientAccountID, 
			RecHubData.dimClientAccountsView.SiteBankID				AS BankID, 
			RecHubData.dimClientAccountsView.SiteOrganizationID		AS OrganizationID,
			RecHubData.dimClientAccountsView.SiteClientAccountID	AS ClientAccountID,
			RecHubData.dimClientAccountsView.ShortName,
			RecHubData.dimClientAccountsView.SiteCodeID,
			RecHubData.dimClientAccountsView.Cutoff, 
			RecHubUser.OLClientAccounts.IsActive,
			RecHubData.dimSiteCodes.CurrentProcessingDate,
			CASE 
				WHEN RecHubUser.OLClientAccounts.ViewingDays IS NULL OR RecHubUser.OLClientAccounts.ViewingDays<=0
					THEN ISNULL(RecHubUser.OLOrganizations.ViewingDays, 0)
					ELSE RecHubUser.OLClientAccounts.ViewingDays 
			END AS OnlineViewingDays,
			CASE 
				WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR Len(RecHubUser.OLClientAccounts.DisplayName)=0
					THEN RecHubData.dimClientAccountsView.LongName 
					ELSE RecHubUser.OLClientAccounts.DisplayName 
			END AS LongName
		FROM	
			RecHubData.dimClientAccountsView
			INNER JOIN RecHubUser.OLClientAccounts ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLClientAccounts.SiteBankID 
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLClientAccounts.SiteClientAccountID
			INNER JOIN RecHubUser.OLOrganizations ON RecHubUser.OLOrganizations.OLOrganizationID = RecHubUser.OLClientAccounts.OLOrganizationID
			INNER JOIN RecHubData.dimSiteCodes ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		WHERE	
			RecHubUser.OLClientAccounts.OLClientAccountID = @parmOLClientAccountID
			AND RecHubUser.OLClientAccounts.OLOrganizationID = @parmOLOrganizationID
			AND RecHubData.dimClientAccountsView.IsActive = 1
			AND RecHubUser.OLClientAccounts.IsActive = 1;
	END
	ELSE
	BEGIN
		SELECT	
			RecHubUser.OLClientAccounts.OLOrganizationID, 
			RecHubUser.OLClientAccounts.OLClientAccountID, 
			RecHubData.dimClientAccountsView.SiteBankID				AS BankID, 
			RecHubData.dimClientAccountsView.SiteOrganizationID		AS OrganizationID,
			RecHubData.dimClientAccountsView.SiteClientAccountID	AS ClientAccountID,
			RecHubData.dimClientAccountsView.ShortName,
			RecHubData.dimClientAccountsView.SiteCodeID,
			RecHubData.dimClientAccountsView.Cutoff, 
			RecHubUser.OLClientAccounts.IsActive,
			RecHubData.dimSiteCodes.CurrentProcessingDate,
			CASE 
				WHEN RecHubUser.OLClientAccounts.ViewingDays IS NULL OR RecHubUser.OLClientAccounts.ViewingDays<=0
					THEN ISNULL(RecHubUser.OLOrganizations.ViewingDays, 0)
					ELSE RecHubUser.OLClientAccounts.ViewingDays 
			END AS OnlineViewingDays,
			CASE 
				WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR Len(RecHubUser.OLClientAccounts.DisplayName)=0
					THEN RecHubData.dimClientAccountsView.LongName 
					ELSE RecHubUser.OLClientAccounts.DisplayName 
			END AS LongName
		FROM	
			RecHubData.dimClientAccountsView
			INNER JOIN RecHubUser.OLClientAccounts ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLClientAccounts.SiteBankID 
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLClientAccounts.SiteClientAccountID
			INNER JOIN RecHubUser.OLClientAccountUsers ON RecHubUser.OLClientAccountUsers.OLClientAccountID = RecHubUser.OLClientAccounts.OLClientAccountID
			INNER JOIN RecHubUser.Users ON RecHubUser.Users.UserID = RecHubUser.OLClientAccountUsers.UserID
			INNER JOIN RecHubUser.OLOrganizations ON RecHubUser.OLOrganizations.OLOrganizationID = RecHubUser.OLClientAccounts.OLOrganizationID
			INNER JOIN RecHubData.dimSiteCodes ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		WHERE	
			RecHubUser.OLClientAccounts.OLClientAccountID=@parmOLClientAccountID
			AND RecHubUser.OLClientAccounts.OLOrganizationID =@parmOLOrganizationID
			AND RecHubUser.OLClientAccountUsers.UserID = CASE WHEN @IsSuperUser > 0 THEN RecHubUser.OLClientAccountUsers.UserID ELSE @parmUserID END
			AND RecHubData.dimClientAccountsView.IsActive = 1
			AND RecHubUser.OLClientAccounts.IsActive = 1;
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

