--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_SourceBatchID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_SourceBatchID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_SourceBatchID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [RecHubData].[usp_factBatchSummary_Get_SourceBatchID]
(
	@parmBatchID			   BIGINT  
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 4/21/2015
*
* Purpose: Get SourceBatchID from Batch ID
*
* Modification History
* 04/22/2015 WI 203156 MAA	Created.
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY

	SELECT 
		RecHubData.factBatchSummary.SourceBatchID
	FROM 
		RecHubData.factBatchSummary
	WHERE
	    RecHubData.factBatchSummary.BatchID = @parmBatchID

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
