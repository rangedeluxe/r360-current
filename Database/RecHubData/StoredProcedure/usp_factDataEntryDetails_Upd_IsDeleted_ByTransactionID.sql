--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Upd_IsDeleted_ByTransactionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Upd_IsDeleted_ByTransactionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Upd_IsDeleted_ByTransactionID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Upd_IsDeleted_ByTransactionID
(
	@parmDepositDateKey		INT,
	@parmBatchID			BIGINT,
	@parmTransactionID		INT,
	@parmModificationDate	DATETIME
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 08/29/2013
*
* Purpose: Deletes factDataEntryDetails by transactionID.
*
* Modification History
* 08/29/2013 WI 112911 JBS	Created
* 02/26/2015 WI 192469 JPB	FP from OLTA.usp_factDataEntryDetailsDeleteTransaction.
*							Replaced param list with R360 parameters.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY

	UPDATE 	
		RecHubData.factDataEntryDetails
	SET
		RecHubData.factDataEntryDetails.IsDeleted = 1,
		RecHubData.factDataEntryDetails.ModificationDate = @parmModificationDate
	WHERE	
		RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
		AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
		AND RecHubData.factDataEntryDetails.IsDeleted = 0
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorSeverity   INT,
			@ErrorState      INT
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE()
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState)
END CATCH
