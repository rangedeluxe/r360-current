--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factRawPaymentData_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factRawPaymentData_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factRawPaymentData_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factRawPaymentData_Get
(	
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,	
	@parmBatchSequence			INT,
	@parmIsCheck				BIT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Santosh Sav
* Date: 06/19/2014
*
* Purpose: Get fact Raw Payment Data
*
* Modification History
* 06/19/2014 WI 146898 SAS 	Created for fetching fact Raw Payment Data
* 09/10/2014 WI 164984 SAS 	Added RawDataSequence column.
* 10/07/2014 WI 169850 SAS 	BatchSequence, BankID and ClientAccountID Removed
* 11/03/2014 WI 175368 SAS	Changes done to pull data using factDocuments batch sequence or factCheck BatchSequence.
							Updated The SP to take BatchSequence as an input paramater and IsCheck paramater
							If @parmIsCheck is true then the batchsequence will match from factCheck table
							If @parmIsCheck is false then the batchsequence will match from factDocument table
* 03/23/2016 WI 272256 JBS  Add order by clause to sort output
******************************************************************************/
SET NOCOUNT ON; 

DECLARE	@TransactionID	INT;
DECLARE @DepositDateKey INT;

BEGIN TRY                

	SET @DepositDateKey=CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);
	IF @parmIsCheck=1
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factChecks.TransactionID 
			FROM
				RecHubData.factChecks
			WHERE 
					RecHubData.factChecks.DepositDateKey =@DepositDateKey
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
				AND RecHubData.factChecks.IsDeleted=0;
		END 
	ELSE 
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factDocuments.TransactionID 
			FROM
				RecHubData.factDocuments
			WHERE 
					RecHubData.factDocuments.DepositDateKey =@DepositDateKey
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
				AND RecHubData.factDocuments.IsDeleted=0;
		END
	SELECT	
		RecHubData.factRawPaymentData.RawDataSequence,
		RecHubData.factRawPaymentData.RawData			
	FROM
		RecHubData.factRawPaymentData						
	WHERE 
			RecHubData.factRawPaymentData.DepositDateKey = @DepositDateKey
		AND RecHubData.factRawPaymentData.BatchID = @parmBatchID
		AND RecHubData.factRawPaymentData.TransactionID = @TransactionID			
		AND RecHubData.factRawPaymentData.IsDeleted = 0
	ORDER BY 
		RecHubData.factRawPaymentData.DepositDateKey,
		RecHubData.factRawPaymentData.BatchID,
		RecHubData.factRawPaymentData.TransactionID,
		RecHubData.factRawPaymentData.BatchSequence,
		RecHubData.factRawPaymentData.RawDataSequence
	OPTION ( RECOMPILE );
    
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
