IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSelectColumnList') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectColumnList
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectColumnList
(
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 09/10/2019
*
* Purpose: Get alist of columns the user selected to return in Adv Search.
*
* Modification History
* 09/10/2019 R360-30221 JPB	Created
* 09/23/2018 R360-30227 Fix WHERE EXCEPT to get the right records
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @SelectFields TABLE
	(
		IsCheck BIT,
		DataType INT,
		BatchSourceKey SMALLINT,
		FieldName VARCHAR(256),
		ReportTitle VARCHAR(256)
	)

	/* Select Fields */
	INSERT INTO @SelectFields(IsCheck, DataType, BatchSourceKey, FieldName, ReportTitle)
	SELECT 
		CASE UPPER(TableName)
			WHEN 'CHECKS' THEN 1 
			WHEN 'STUBS' THEN 0
			ELSE 1
		END,
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle
	FROM 
		@parmAdvancedSearchSelectFieldsTable;

	/* Add any WHERE fields that were not included in the SELECT fields */
	INSERT INTO @SelectFields(IsCheck, DataType, BatchSourceKey, FieldName, ReportTitle)
	SELECT 
		CASE UPPER(TableName)
			WHEN 'CHECKS' THEN 1 
			WHEN 'STUBS' THEN 0
			ELSE 1
		END AS IsCheck,
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle
	FROM 
		@parmAdvancedSearchWhereClauseTable
	EXCEPT 
	SELECT 
		IsCheck, 
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle
	FROM 
		@SelectFields;

	/* Account for special fields and create CompositeFieldName */
	SELECT 
		IsCheck,
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle,
		CASE
			WHEN IsCheck = 1 AND UPPER(FieldName) = 'AMOUNT' THEN 'CheckAmount'
			WHEN IsCheck = 1 AND UPPER(FieldName) = 'ACCOUNT' THEN 'CheckAccountNumber'
			WHEN IsCheck = 1 AND UPPER(FieldName) = 'SERIAL' THEN FieldName
			WHEN IsCheck = 1 AND UPPER(FieldName) = 'RT' THEN 'RoutingNumber'
			WHEN IsCheck = 1 AND UPPER(FieldName) = 'PAYER' THEN 'RemitterName'
			WHEN IsCheck = 1 AND UPPER(FieldName) IN ('SERIAL', 'DDA', 'PAYER') THEN FieldName
			WHEN IsCheck = 0 AND UPPER(FieldName) = 'STUBAMOUNT' THEN 'Amount'
			WHEN IsCheck = 0 AND UPPER(FieldName) = 'STUBACCOUNTNUMBER' THEN 'AccountNumber'
			ELSE FieldName + '_' + RTRIM(CAST(IsCheck AS CHAR)) + '_' + CAST(DataType AS VARCHAR(2))
		END AS CompositeFieldName
	FROM 
		@SelectFields;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
