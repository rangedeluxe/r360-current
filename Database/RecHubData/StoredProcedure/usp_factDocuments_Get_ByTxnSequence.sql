--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_ByTxnSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_ByTxnSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get_ByTxnSequence
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_ByTxnSequence 
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142850
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,				--WI 142850
	@parmTxnSequence			INT,
	@parmDisplayScannedCheck	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Detail fact table for Document records
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 11/16/2009 CR 28267 JMC	Added ProcessingDate to the returned dataset
* 11/19/2009 CR 28335 JMC	Added OnlineColorMode AS LockboxColorMode to the returned dataset
* 04/09/2012 CR 51908 JNE	Added BatchSiteCode to the returned dataset
* 04/03/2013 WI 90723 JBS	Updates for 2.0. Change to RecHubData schema.
*							Renamed from usp_GetTransactionDocumentsBySequence.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed all references Lockbox to ClientAccount,
*							Process to Immutable.
*							Removed columns GlobalBatchID, GlobalDocumentID.
*							Renamed SiteCode to SiteCodeID.
*							Added  RecHubData.factDocuments.IsDeleted = 0
*							Moved recompile from SP to SELECT statement.
* 05/28/2014 WI 142850 DLD  Batch Collision/RAAM Integration.
* 09/17/2014 WI	166574 SAS  Added ImportTypeShortName
* 10/24/2014 WI	174090 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175125 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176349 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
*							Removed BatchSourceName
* 05/27/2015 WI 215279 JBS  (FP 215004) Add BatchSequence to Order By
* 11/14/2016 #133519023 BLR Copied the file type descriptor logic from 
*                           [RecHubData].[usp_factDocuments_Get_ByTransactionID]
*                           as they were producing different results.
* 10/30/2018 #161488478 MAA  Match resulting set from 
*                           [RecHubData].[usp_factDocuments_Get_ByTransactionID]
*						    as they are pretty much used for the same purpose
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DepositDateKey INT,
		@CheckDocumentKey INT,
		@ScannedCheckDocumentKey INT,
		@StartDateKey INT,				--WI 142850
        @EndDateKey INT;				--WI 142850

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */	--WI 142850
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	/* Create the memory table with the column names used by the application. Keeps from using AS xxx in the final SELECT */
	DECLARE @ClientAccounts TABLE
	(
		ClientAccountKey	INT,
		BankID				INT,
		ClientAccountID		INT,
		SiteCodeID			INT,
		ClientAccountColorMode TINYINT
	);

	INSERT INTO @ClientAccounts
	(
		ClientAccountKey,
		BankID,
		ClientAccountID,
		SiteCodeID,
		ClientAccountColorMode
	)
	SELECT	RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode
	FROM	RecHubData.dimClientAccounts
			INNER JOIN RecHubUser.SessionClientAccountEntitlements																	--WI 142850
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey		--WI 142850
	WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID													--WI 142850
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID											--WI 142850
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID							--WI 142850
			AND RecHubData.dimClientAccounts.MostRecent = 1;

	;WITH BatchDocuments AS
	(
		SELECT	RecHubData.factDocuments.BatchID,
				RecHubdata.factdocuments.SourceBatchID, --WI 142850
				CLA.BankID,
				CLA.ClientAccountID,
				CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.DepositDateKey), 112),101)		AS DepositDate,
				CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.ImmutableDateKey), 112),101)		AS ImmutableDate,
				RecHubData.factDocuments.ImmutableDateKey AS PICSDate,
				RecHubData.factDocuments.TransactionID,
				RecHubData.factDocuments.TxnSequence,
				RecHubData.factDocuments.BatchSequence,
				RecHubData.factDocuments.DocumentSequence,
				CLA.SiteCodeID,
				CLA.ClientAccountColorMode,
				RecHubData.factDocuments.BatchSiteCode,
				RecHubData.factDocuments.DocumentTypeKey,
				RecHubData.factDocuments.BatchNumber,
				RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
				RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
				RecHubData.factDocuments.SourceProcessingDateKey,
				RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType,
				RecHubData.factDocuments.SequenceWithinTransaction
		FROM	RecHubData.factDocuments
				INNER JOIN @ClientAccounts CLA ON RecHubData.factDocuments.ClientAccountKey = CLA.ClientAccountKey
				INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factDocuments.BatchSourceKey				
				INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
				INNER JOIN RecHubData.dimDocumentTypes ON 
					RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
				INNER JOIN RecHubData.dimBatchPaymentTypes ON
					RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factDocuments.BatchPaymentTypeKey

		WHERE  	RecHubData.factDocuments.DepositDateKey = @StartDateKey	--WI 142850
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.TxnSequence = @parmTxnSequence
				AND RecHubData.factDocuments.DepositStatus >= 850
				AND RecHubData.factDocuments.IsDeleted = 0
				AND 
					( 
						(	@parmDisplayScannedCheck > 0 
							AND RecHubData.dimDocumentTypes.FileDescriptor <> 'C'
						)	 
						OR 
						(
							@parmDisplayScannedCheck = 0 
							AND RecHubData.dimDocumentTypes.FileDescriptor <> 'C'
							AND RecHubData.dimDocumentTypes.FileDescriptor <> 'SC'
						) 
					)
				)
	SELECT	BatchID,
			SourceBatchID,	--WI 142850
			BankID,
			ClientAccountID,
			DepositDate,
			ImmutableDate,
			PICSDate,
			TransactionID,
			TxnSequence,
			RTRIM(RecHubData.dimDocumentTypes.FileDescriptor) AS FileDescriptor,
			ISNULL(RTRIM(RecHubData.dimDocumentTypes.DocumentTypeDescription), 'Unknown')	AS [Description], 
			BatchSequence,
			DocumentSequence,
			SiteCodeID,
			ClientAccountColorMode,
			BatchSiteCode,
			BatchNumber,
			BatchSourceShortName,
			ImportTypeShortName,
			SourceProcessingDateKey,
			PaymentType,
			SequenceWithinTransaction
	FROM	BatchDocuments
			INNER JOIN RecHubData.dimDocumentTypes ON BatchDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
	ORDER BY
			BatchSequence ASC,
			DocumentSequence ASC
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
