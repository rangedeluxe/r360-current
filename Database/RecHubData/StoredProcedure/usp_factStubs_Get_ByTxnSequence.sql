--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factStubs_Get_ByTxnSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factStubs_Get_ByTxnSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factStubs_Get_ByTxnSequence
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factStubs_Get_ByTxnSequence
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142860
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,				--WI 142860
	@parmTxnSequence			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: 
*
* Modification History
* 03/23/2009 CR 25817 JMC	Created
* 01/05/2010 CR 28081  CS   Updated to use OLTA.factDataEntryDetails when there
*						    are no OLTA.factStubs records
* 04/05/2010 CR 29308 JPB   Renamed Account to AccountNumber.
* 04/03/2013 WI 90725 JPB	Updated for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionStubsBySequence.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Changed all dimLockboxes refs to dimClientAccounts.
*							Removed GlobalStubID from result set.
*							Removed factDataEntryDetails.
*							Moved RECOMPILE from stored procedure to SELECT.
* 06/03/2014 WI	142860	DLD	Batch Collision/RAAM Integration.
* 05/27/2015 WI 215264  JBS (FP 215002) Add BatchSequence to Order By
* 11/14/2016 #133519023 BLR Added TransactionID to the select fields.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @DepositDateKey INT,
		@StartDateKey INT,	--WI 142860
        @EndDateKey INT;	--WI 142860

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */	--WI 142860
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT @DepositDateKey = CAST(CONVERT(VARCHAR,@StartDateKey,112) AS INT);	--WI 142860

	SELECT	
		RecHubData.factStubs.BatchSequence,
		RecHubData.factStubs.Amount,
		RecHubData.factStubs.AccountNumber,
		RecHubData.factStubs.IsOMRDetected,
		RecHubData.factStubs.DocumentBatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.SourceProcessingDateKey,
		RecHubData.factStubs.TransactionID
	FROM	
		RecHubData.factStubs
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON													--WI 142860
			RecHubData.factStubs.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey	--WI 142860
	WHERE  	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID									--WI 142860
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID								--WI 142860
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID				--WI 142860
		AND RecHubData.factStubs.DepositDateKey = @DepositDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.TxnSequence = @parmTxnSequence
		AND RecHubData.factStubs.IsDeleted = 0
	ORDER BY 
		RecHubData.factStubs.BatchID ASC,
		RecHubData.factStubs.BatchSequence ASC,
		RecHubData.factStubs.TxnSequence ASC,
		RecHubData.factStubs.SequenceWithinTransaction ASC
	OPTION( RECOMPILE );

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
