--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ReceivablesSummaryByType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_Report_ReceivablesSummaryByType') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_Report_ReceivablesSummaryByType;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_Report_ReceivablesSummaryByType
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the data needed for part of the Receivables And Exceptions Summary by Type report.
*
* Modification History
* Created
* 05/06/2013 WI 107246 KLC	Created.
* 07/10/2013 WI 108503 KLC	Updated auditing to write individual parameters instead of the xml.
* 07/26/2013 WI 107246 JBS  Changing called proc from usp_OLClientAccounts_Get_ByUserID
*							to usp_OLClientAccounts_Get_ByUserID_Reporting
* 07/26/2013 WI 107246 KLC	Updated to use payment type's long name
* 07/30/2013 WI	107246 KLC  Updated to use transaction count for the count returned and just check amount for total amount.
* 10/22/2013 WI 107246 KLC	Updated to check the preference for respecting the cut off
* 10/31/2013 WI 119751 CMC	Updated to use new Org Hierarchy Tree
* 11/19/2013 WI 122364 CMC	Fix to match dashboard
* 07/10/2014 WI 151283 KLC	Updated for RAAM integration
* 07/30/2014 WI 151283 KLC	Refactored again to improve performance (?) and only return data for
*							 active workgroups
* 08/14/2014 WI 151283 KLC	Updated to add back current processing date check
* 08/28/2014 WI 162747 PKW	Modified to Filter out Session Audits on EventType.
* 10/16/2014 WI 172744	LA	Pass in Entities to Joing to Client Accounts
* 03/09/2015 WI 194996 JBS	Add View Ahead Permission
* 04/20/2015 WI 202869 BLR  Altered the audit message a bit; no functional changes.
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDateKey		INT,
			@filterDate			DATETIME,
			@filterWGExtRefID	VARCHAR(40),
			@filterEntities		XML,
			@rootEntityID		INT;
			
	SELECT	@filterDateKey = CASE WHEN Parms.val.value('(FD)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(FD)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@filterEntities = CASE WHEN Parms.val.exist('ENTs') = 1 THEN Parms.val.query('ENTs') ELSE NULL END,
			@filterWGExtRefID = CASE WHEN Parms.val.value('(WG)[1]', 'VARCHAR(40)') <> '' THEN REPLACE(Parms.val.value('(WG)[1]', 'VARCHAR(40)'), '|', '.') ELSE NULL END
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);

	SET @filterDate = CONVERT(DATETIME, CONVERT(VARCHAR(8), @filterDateKey));

	--Get the workgroup or list of all workgroups for the entities requested
	DECLARE @tblWorkgroups TABLE (ClientAccountKey INT PRIMARY KEY, StartDateKey INT, EndDateKey INT, ViewAhead BIT);
	IF @filterWGExtRefID IS NOT NULL
	BEGIN
		DECLARE @siteBankID INT,
				@siteClientAccountID INT;

		--parse the external reference id
		SET @siteBankID = PARSENAME(@filterWGExtRefID, 2);
		SET @siteClientAccountID = PARSENAME(@filterWGExtRefID, 1);

		SELECT @rootEntityID = RecHubUser.OLWorkgroups.EntityID
		FROM RecHubUser.OLWorkgroups
		WHERE RecHubUser.OLWorkgroups.SiteBankID = @siteBankID
			AND RecHubUser.OLWorkgroups.SiteClientAccountID = @siteClientAccountID;

		IF @rootEntityID IS NOT NULL
		BEGIN
			INSERT INTO @tblWorkgroups(ClientAccountKey, StartDateKey, EndDateKey, ViewAhead)
			SELECT	RecHubUser.SessionClientAccountEntitlements.ClientAccountKey,
					RecHubUser.SessionClientAccountEntitlements.StartDateKey,
					RecHubUser.SessionClientAccountEntitlements.EndDateKey,
					RecHubUser.SessionClientAccountEntitlements.ViewAhead
			FROM RecHubData.dimClientAccountsView
				INNER JOIN RecHubUser.OLWorkgroups
					ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
						AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
				INNER JOIN RecHubUser.SessionClientAccountEntitlements
					ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
						AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
						AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
						AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			WHERE RecHubData.dimClientAccountsView.SiteBankID = @siteBankID
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = @siteClientAccountID
				AND RecHubData.dimClientAccountsView.IsActive = 1;
		END
	END
	ELSE
	BEGIN
	
		-- Add the EntityBreadcrumb data to a temp table
		-- So we can Join to it for RAAM data
		IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
			DROP TABLE #tmpEntities;

		 CREATE TABLE #tmpEntities
		 (
			   EntityID              INT,
			   BreadCrumb	         VARCHAR(100)
		 );
     
		 INSERT INTO #tmpEntities
		 (
			   EntityID,
			   BreadCrumb       
		 )
		 SELECT     
			entityRequest.att.value('@EntityID', 'int') AS EntityID,
			entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
		 FROM 
			@parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

		--for all entities, get the workgroups
		INSERT INTO @tblWorkgroups(ClientAccountKey, StartDateKey, EndDateKey, ViewAhead)
		SELECT	RecHubUser.SessionClientAccountEntitlements.ClientAccountKey,
				RecHubUser.SessionClientAccountEntitlements.StartDateKey,
				RecHubUser.SessionClientAccountEntitlements.EndDateKey,
				RecHubUser.SessionClientAccountEntitlements.ViewAhead
		FROM #tmpEntities
			INNER JOIN RecHubUser.OLWorkgroups
				ON #tmpEntities.EntityID = RecHubUser.OLWorkgroups.EntityID
			INNER JOIN RecHubData.dimClientAccountsView
				ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
					AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
					AND RecHubData.dimClientAccountsView.IsActive = 1
			INNER JOIN RecHubUser.SessionClientAccountEntitlements
				ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
					AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
					AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
					AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;
	END
	
	SELECT
		RecHubData.dimBatchPaymentTypes.LongName AS BatchPaymentTypeName,
		SUM(COALESCE(RecHubData.factBatchSummary.TransactionCount, 0)) AS TotalCount,
		SUM(COALESCE(RecHubData.factBatchSummary.CheckTotal, 0)) AS TotalAmount
	FROM
		RecHubData.factBatchSummary
		INNER JOIN @tblWorkgroups tblWorkgroups
			ON RecHubData.factBatchSummary.ClientAccountKey = tblWorkgroups.ClientAccountKey
		INNER JOIN RecHubData.dimBatchPaymentTypes 
			ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
	WHERE 
		RecHubData.factBatchSummary.DepositDateKey = @filterDateKey
		AND RecHubData.factBatchSummary.DepositDateKey >= tblWorkgroups.StartDateKey
		AND ((RecHubData.factBatchSummary.DepositDateKey <= tblWorkgroups.EndDateKey AND tblWorkgroups.ViewAhead = 0) -- WI 194996
				 OR (tblWorkgroups.ViewAhead = 1))  -- WI 194996
		AND RecHubData.factBatchSummary.DepositStatus >= 850/*Deposit Complete*/
		AND RecHubData.factBatchSummary.IsDeleted = 0
	GROUP BY
		RecHubData.dimBatchPaymentTypes.LongName
	ORDER BY
		RecHubData.dimBatchPaymentTypes.LongName ASC;

	DECLARE @auditMessage VARCHAR(1024) = 'Receivables Summary By Type executed for Parameters -'
											+ ' Date: ' + CAST(@filterDateKey AS VARCHAR(8))
											+ ' Bank ID: ' + CASE WHEN @siteBankID IS NULL THEN 'All' ELSE CAST(@siteBankID as VARCHAR(10)) END
											+ ' Workgroup ID: ' + CASE WHEN @siteClientAccountID IS NULL THEN 'All' ELSE CAST(@siteClientAccountID as VARCHAR(10)) END;

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubData.usp_Report_ReceivablesSummaryByType', @parmEventName='Report Executed', @parmEventType='Reports', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
