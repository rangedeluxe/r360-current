--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factStubs_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factStubs_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factStubs_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factStubs_Upd_IsDeleted
(
	@parmDepositDateKey				INT,			--WI 142861
	@parmBatchID					BIGINT,			--WI 142861
	@parmModificationDate			DATETIME		--WI 142861
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/26/2009
*
* Purpose: Deletes factStubs.
*
* Modification History
* 03/26/2009 CR 25817 JPB	Created
* 11/02/2008 CR 28044 JPB	Fixed problem with reconsolidation of batches. 
* 12/31/2009 CR 28569 JPB	Performance improvements:
*								Keys passed in
* 01/23/2012 CR 49588 JPB	Added SourceProcessingDateKey.
* 04/24/2013 WI 90655 JPB	Updates for 2.0. Moved to RecHubData schema.
*							Renamed from usp_factStubsDelete.
*							Combined usp_DataImportIntegrationServices_Delete_factStubs.
*							Renamed @parmCustomerKey to @parmOrganizationKey.
*							Renamed @parmLockboxKey	to @parmClientAccountKey.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
*							Added @parmModificationDate.
* 06/04/2014 WI 142861 PKW  Batch Collision.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factStubs
	SET
		RecHubData.factStubs.IsDeleted = 1,
		RecHubData.factStubs.ModificationDate = @parmModificationDate	--WI 142861
	WHERE	
		RecHubData.factStubs.DepositDateKey = @parmDepositDateKey		--WI 142861
		AND RecHubData.factStubs.BatchID = @parmBatchID					--WI 142861
		AND RecHubData.factStubs.IsDeleted = 0
	OPTION( RECOMPILE );
END TRY
BEGIN CATCH
	IF( @@NESTLEVEL > 1 )
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH