--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_dimNotificationFileTypes_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimNotificationFileTypes_Upd') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimNotificationFileTypes_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimNotificationFileTypes_Upd
(
	@parmFileType		VARCHAR(32), 
	@parmFileTypeDesc	VARCHAR(255),
	@parmUserId			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Charlie Johnson
* Date:     08/09/2012
*
* Purpose:  Updates Notification File Type Descriptions
*
* Modification History
* Created
* 09/08/2012 CR 54925 CEJ	Created.
* 04/24/2013 WI 98695 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc from usp_dimNotificationFileTypes_Update_ByFileType
* 09/15/2014 WI 163478 BLR  Added data auditing, UserID for param input.
* 06/03/2015 WI 216751 LA   Removed ID from Audit Message; added changes to AuditColumns table
*******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE
	@AuditMessage	VARCHAR(1024),
	@NotificationId INT,
	@PrevFileTypeDesc VARCHAR(1024),
	@AuditColumns RecHubCommon.AuditValueChangeTable;

BEGIN TRY
	BEGIN TRANSACTION NotificationTypes_Upd;
	SET @NotificationId = -1;

	-- Grab the NotificationId of the old value.
	SELECT @NotificationId = RecHubData.dimNotificationFileTypes.NotificationFileTypeKey,
	       @PrevFileTypeDesc = RecHubData.dimNotificationFileTypes.FileTypeDescription
	FROM RecHubData.dimNotificationFileTypes
	WHERE RecHubData.dimNotificationFileTypes.FileType = @parmFileType;

	IF @NotificationId = -1
	BEGIN
		ROLLBACK TRANSACTION NotificationTypes_Upd;
		RAISERROR('There is not a database entry for FileType: %s.',16, 1, @parmFileType);
	END

	-- Update the Value
	UPDATE	RecHubData.dimNotificationFileTypes 
	SET		RecHubData.dimNotificationFileTypes.FileTypeDescription =  @parmFileTypeDesc,
			RecHubData.dimNotificationFileTypes.ModificationDate = GETDATE(),
			RecHubData.dimNotificationFileTypes.ModifiedBy = SUSER_SNAME()
	WHERE	RecHubData.dimNotificationFileTypes.FileType = @parmFileType;
	
	-- Data Audit

	IF @PrevFileTypeDesc <> @parmFileTypeDesc
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FileTypeDescription', @PrevFileTypeDesc,@parmFileTypeDesc);

	SET @AuditMessage = 'Updated NotificationType ' 
		+ ', FileType: ' + @parmFileType
		+ ', Description: ' + @parmFileTypeDesc;
		
	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 	
		@parmUserID					= @parmUserId,
		@parmApplicationName		= 'RecHubData.usp_dimNotificationFileTypes_Upd',
		@parmSchemaName				= 'RecHubData',
		@parmTableName				= 'dimNotificationTypes',
		@parmColumnName				= 'NotificationTypeKey',
		@parmAuditValue				= @NotificationId,
		@parmAuditType				= 'UPD',
		@parmAuditMessage			= @AuditMessage

	COMMIT TRANSACTION NotificationTypes_Upd;
END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION NotificationTypes_Upd;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
