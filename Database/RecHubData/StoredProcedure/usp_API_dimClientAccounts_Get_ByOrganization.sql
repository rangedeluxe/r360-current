--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_API_dimClientAccounts_Get_ByOrganization
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_API_dimClientAccounts_Get_ByOrganization') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_API_dimClientAccounts_Get_ByOrganization
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_API_dimClientAccounts_Get_ByOrganization 
(
	@parmSiteBankID			INT,
	@parmSiteOrganizationID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights res
.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28713 JMC	Created
* 04/15/2013 WI 90627 JBS	Update to 2.0 release. Change schema name
*							Rename proc from  usp_API_dimClientAccounts_Get_ByOrganization.
*							Change parameters: @parmSiteCustomerID to @parmSiteOrganizationID
*							Change all references:  Lockbox to ClientAccount
*													Customer to Organization
*													SiteCode to SiteCodeID
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		 RecHubData.dimClientAccounts.SiteCodeID,
		 RecHubData.dimClientAccounts.SiteBankID			AS BankID,
		 RecHubData.dimClientAccounts.SiteOrganizationID	AS OrganizationID,
		 RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
		 RTRIM(RecHubData.dimClientAccounts.ShortName)		AS ShortName,
		 RTRIM(RecHubData.dimClientAccounts.LongName)		AS LongName,
		 RecHubData.dimClientAccounts.DDA,
		 RecHubData.dimClientAccounts.OnlineColorMode,
		 RecHubData.dimClientAccounts.CutOff,
		 RecHubData.dimClientAccounts.IsActive,
		 RecHubData.dimClientAccounts.ClientAccountKey,
		 RecHubData.dimSiteCodes.CurrentProcessingDate,
		 RecHubData.dimClientAccounts.[FileGroup]
	FROM RecHubData.dimClientAccounts
		 INNER JOIN RecHubData.dimSiteCodes ON
			 RecHubData.dimClientAccounts.SiteCodeID = dimSiteCodes.SiteCodeID
	WHERE RecHubData.dimClientAccounts.MostRecent = 1
		 AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		 AND RecHubData.dimClientAccounts.SiteOrganizationID = @parmSiteOrganizationID
	ORDER BY
		 RecHubData.dimClientAccounts.SiteBankID ASC,
		 RecHubData.dimClientAccounts.SiteOrganizationID ASC,
		 RecHubData.dimClientAccounts.SiteClientAccountID ASC;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

