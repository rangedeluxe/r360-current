--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_GetCheckImagePathInfo
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_GetCheckImagePathInfo') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_GetCheckImagePathInfo
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_GetCheckImagePathInfo
(
    @parmBatchID           BIGINT,
    @parmBatchSequence     INT,
	@parmFileDescriptor	   VARCHAR(2)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 07/02/2014
*
* Purpose: Get Check Image Path information. (Join on the BatchSources table)
*
* Modification History
* 07/02/2014 WI 151519 BLR - Initial Version
* 07/31/2014 WI 156528 SAS - Added additonal parameter FileDescriptor to decide whi
							 if the data needs to be pulled from factchecks or factdocuments
							 table.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	IF @parmFileDescriptor='C' 
	BEGIN 
		SELECT
			RecHubData.factChecks.SourceBatchID,
			RecHubData.dimImportTypes.ShortName
		FROM 
			RecHubData.factChecks
			INNER JOIN RecHubData.dimBatchSources 
				ON RecHubData.factChecks.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
			INNER JOIN RecHubData.dimImportTypes
				ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
		
		WHERE 
			RecHubData.factChecks.BatchID = @parmBatchID
			AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
			AND RecHubData.factChecks.IsDeleted = 0;
	END
	ELSE
	BEGIN  
		SELECT
			RecHubData.factDocuments.SourceBatchID,
			RecHubData.dimImportTypes.ShortName
		FROM 
			RecHubData.factDocuments
			INNER JOIN RecHubData.dimBatchSources 
				ON RecHubData.factDocuments.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
			INNER JOIN RecHubData.dimImportTypes
				ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
		
		WHERE 
			RecHubData.factDocuments.BatchID = @parmBatchID
			AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
			AND RecHubData.factDocuments.IsDeleted = 0;
	END
    

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

