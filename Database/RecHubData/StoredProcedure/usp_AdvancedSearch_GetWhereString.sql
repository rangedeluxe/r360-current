IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetWhereString') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetWhereString
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetWhereString
(
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmIsCheck BIT,
	@parmWhereClauseString NVARCHAR(MAX) OUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/17/2019
*
* Purpose: Build the where clause used by Adv Search
*
* Modification History
* 04/17/2019 R360-16301 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @WhereClauseString NVARCHAR(MAX) = '';

BEGIN TRY

	SELECT @WhereClauseString = @WhereClauseString + N'
		AND RecHubData.' + 
		CASE UPPER(TableName) WHEN 'CHECKS' THEN 'factChecks' ELSE 'factStubs' END
		+ N'.' +
		CASE
			WHEN UPPER(TableName) = 'CHECKS' AND UPPER(FieldName) = 'AMOUNT' THEN N'Amount'
			WHEN UPPER(TableName) = 'CHECKS' AND (UPPER(FieldName) = 'ACCOUNT' OR UPPER(FieldName) = 'SERIAL') THEN FieldName
			WHEN UPPER(TableName) = 'CHECKS' AND UPPER(FieldName) = 'RT' THEN N'RoutingNumber'
			WHEN UPPER(TableName) = 'CHECKS' AND UPPER(FieldName) = 'SERIAL' THEN FieldName
			WHEN UPPER(TableName) = 'STUBS' AND UPPER(FieldName) = 'STUBAMOUNT' THEN 'Amount'
			WHEN UPPER(TableName) = 'STUBS' AND UPPER(FieldName) = 'STUBACCOUNTNUMBER' THEN 'AccountNumber'
			ELSE FieldName + N'_' + CASE UPPER(TableName) WHEN 'CHECKS' THEN N'1' ELSE N'0' END + '_' + CAST(DataType AS NVARCHAR(3))
		END +
		CASE Operator 
			WHEN 'Equals' THEN '=' + CASE WHEN UPPER(FieldName) IN ('AMOUNT','STUBAMOUNT') OR DataType = 6 OR DataType = 7 THEN DataValue ELSE QUOTENAME(DataValue,CHAR(39)) END 
			WHEN 'Is Greater Than' THEN ' > ' + CASE WHEN UPPER(FieldName) IN ('AMOUNT','STUBAMOUNT') OR DataType = 6 OR DataType = 7 THEN DataValue ELSE QUOTENAME(DataValue,CHAR(39)) END 
			WHEN 'Is Less Than' THEN ' < ' + CASE WHEN UPPER(FieldName) IN ('AMOUNT','STUBAMOUNT') OR DataType = 6 OR DataType = 7 THEN DataValue ELSE QUOTENAME(DataValue,CHAR(39)) END
			WHEN 'Begins With' THEN ' LIKE '+QUOTENAME(DataValue+'%',CHAR(39))
			WHEN 'Contains' THEN ' LIKE '+QUOTENAME('%'+DataValue+'%',CHAR(39))
			WHEN 'Ends With' THEN ' LIKE '+QUOTENAME('%'+DataValue,CHAR(39))
		END
	FROM
		@parmAdvancedSearchWhereClauseTable
	WHERE
		UPPER(TableName) = CASE @parmIsCheck WHEN NULL THEN TableName WHEN 1 THEN 'CHECKS' ELSE 'STUBS' END
		AND DataType <> -1 
		AND BatchSourceKey <> 255;

	SET @parmWhereClauseString = @WhereClauseString;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
