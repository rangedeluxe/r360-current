--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimItemDataSetupFields_Get_ByShortName
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_dimItemDataSetupFields_Get_ByShortName') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimItemDataSetupFields_Get_ByShortName
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimItemDataSetupFields_Get_ByShortName
(
	@parmImportShortName VARCHAR(30),
	@parmModificationDate dateTime = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2012  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 03/8/2011
*
* Purpose: Request Item Setup Fields 
*
* Modification History
* 03/8/2011 CR 33257  WJS	Created
* 05/11/2012 CR 52655 WJS	Add modificiation date
* 02/11/2014 WI 129235 CMC	Update to 2.01 Schema. Renamed from usp_dimItemDataSetupFields_GetKeyWords
* 06/20/2014 WI 148861 JPB	Replace BatchSource with ImportType.  Adding Permissions to script
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY
	SELECT 	ItemDataSetupFieldKey,
			Keyword,
			DataType 
	FROM 	RecHubData.dimItemDataSetupFields
			INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimImportTypes.ImportTypeKey = RecHubData.dimItemDataSetupFields.ImportTypeKey
	WHERE 	ShortName=@parmImportShortName
		            AND (
                        (@parmModificationDate IS NULL AND RecHubData.dimItemDataSetupFields.ModificationDate = RecHubData.dimItemDataSetupFields.ModificationDate) OR  
                        (@parmModificationDate IS NOT NULL AND RecHubData.dimItemDataSetupFields.ModificationDate > @parmModificationDate)
                  )

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
