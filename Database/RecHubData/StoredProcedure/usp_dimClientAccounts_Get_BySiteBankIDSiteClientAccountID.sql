--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID 
(	
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28712 JMC	Created
* 09/30/2012 CR 56133 JNE	Added FileGroup to result set
* 04/01/2013 WI 90628 JBS	Update to 2.0 release. Change schema name
*							Rename proc from  usp_dimLockboxes_GetByID.
*							Change paramaters: @parmSiteLockboxID to @parmSiteClientAccountID
*							Change all references:  Lockbox to ClientAccount
*													Customer to Organization
* 10/17/2014 WI 165465 EAS  Add Import Type to resultset
* 10/29/2014 WI 174959 EAS  Use dimClientAccountsView as anchor table
*                           Add Display Label to resultset
* 10/20/2014 WI 174187 EAS  Add Batch Source Short Name to resultset.  Batch
*                           Source Name is a calculated field.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

SELECT
	 RecHubData.dimClientAccountsView.ClientAccountkey,
     RecHubData.dimClientAccountsView.SiteCodeID,
     RecHubData.dimClientAccountsView.SiteBankID			AS BankID,
     RecHubData.dimClientAccountsView.SiteOrganizationID	AS OrganizationID,
     RecHubData.dimClientAccountsView.SiteClientAccountID	AS ClientAccountID,
     RTRIM(RecHubData.dimClientAccountsView.ShortName)		AS ShortName,
     RTRIM(RecHubData.dimClientAccountsView.LongName)		AS LongName,
     RecHubData.dimClientAccountsView.DDA,
     RecHubData.dimClientAccountsView.OnlineColorMode,
     RecHubData.dimClientAccountsView.CutOff,
     RecHubData.dimClientAccountsView.IsActive,
     RecHubData.dimClientAccountsView.ClientAccountKey,
     RecHubData.dimSiteCodes.CurrentProcessingDate,
     RecHubData.dimClientAccountsView.[FileGroup],
	 COALESCE(RecHubData.dimImportTypes.ShortName, 'None') AS ImportType,
	 RecHubData.dimClientAccountsView.DisplayLabel,
	 CASE RecHubData.dimImportTypes.ImportTypeKey
		WHEN 1 THEN RecHubData.dimImportTypes.ShortName
		ELSE RecHubData.dimBatchSources.ShortName
	 END AS BatchSourceName
FROM RecHubData.dimClientAccountsView
     INNER JOIN RecHubData.dimSiteCodes ON
         RecHubData.dimClientAccountsView.SiteCodeID = dimSiteCodes.SiteCodeID
	LEFT OUTER JOIN RecHubData.ClientAccountBatchSources ON 
		RecHubData.ClientAccountBatchSources.ClientAccountKey = RecHubData.dimClientAccountsView.ClientAccountKey
    LEFT OUTER JOIN RecHubData.dimBatchSources ON 
		RecHubData.dimBatchSources.BatchSourceKey = RecHubData.ClientAccountBatchSources.BatchSourceKey
    LEFT OUTER JOIN RecHubData.dimImportTypes ON 
		RecHubData.dimImportTypes.ImportTypeKey = LOWER(RecHubData.dimBatchSources.ImportTypeKey) AND RecHubData.dimImportTypes.ShortName = 'integrapay'
WHERE RecHubData.dimClientAccountsView.MostRecent = 1
     AND RecHubData.dimClientAccountsView.SiteBankID = @parmSiteBankID
     AND RecHubData.dimClientAccountsView.SiteClientAccountID = @parmSiteClientAccountID
ORDER BY
     RecHubData.dimClientAccountsView.SiteBankID ASC,
     RecHubData.dimClientAccountsView.SiteOrganizationID ASC,
     RecHubData.dimClientAccountsView.SiteClientAccountID ASC;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
