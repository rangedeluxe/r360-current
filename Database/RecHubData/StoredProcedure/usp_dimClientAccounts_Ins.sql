﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Ins') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimClientAccounts_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Ins
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmIsActive				SMALLINT,
	@parmDataRetentionDays		SMALLINT,
	@parmImageRetentionDays		SMALLINT,
	@parmShortName				VARCHAR(20),
	@parmLongName				VARCHAR(40),
	@parmFileGroup				VARCHAR(256),
	@parmEntityID				INT,
	@parmViewingDays			INT,
	@parmMaximumSearchDays		INT,
	@parmCheckImageDisplayMode	TINYINT,
	@parmDocumentImageDIsplayMode	TINYINT,
	@parmHOA					BIT,
	@parmDisplayBatchID			BIT,
	@parmInvoiceBalancingOption	TINYINT,
	@parmUserID					INT,
	@parmUserDisplayName		VARCHAR(256) = NULL,
	@parmDDAs					XML,
	@parmDataEntryColumns		XML,
	@parmDeadLineDay			SMALLINT,
	@parmRequiresInvoice		BIT,
	@parmBusinessRules			XML,
	@parmFIs					XML,	-- <FIs><ID>1</ID><ID>2</ID>...</FIs>
	@parmClientAccountKey		INT				OUTPUT,
	@parmErrorCode				INT				OUTPUT,
	@parmBillingAccount				VARCHAR(20) = NULL,
	@parmBillingField1				VARCHAR(20) = NULL,
	@parmBillingField2				VARCHAR(20) = NULL,
	@parmEntityName					VARCHAR(50) = NULL,
	@parmBusinessRulesInvoiceRequired		BIT = NULL,
	@parmBusinessRulesBalancingRequired		BIT = NULL,
	@parmBusinessRulesPDBalancingRequired	BIT = NULL,
	@parmBusinessRulesPDPayerRequired		BIT = NULL
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2014-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 03/21/2014
*
* Purpose: Insert into dimClientAccounts table
*
* Modification History
* 03/21/2014 WI 133745 	  KLC  Created
* 10/24/2014 WI 174080 	  KLC  Updated to pass through requires invoice
* 01/20/2015 WI 185217 	  RDS  Updated to include billing fields
* 06/19/2015 WI 219149 	  BLR  Commented out the call to the Exception Schema
*                              For 2.01.
* 08/31/2015 WI 220883    MAA  Added parameter @parmEntityName to include in audit
* 12/16/2016 #127604133   JAW  Added parameter @parmUserDisplayName to include in IsRequired audit
* 01/12/2017 #132047521   JAW  Added parameter @parmBusinessRulesPaymentsOnlyTransaction
* 02/03/2017 PT 138212537 JBS  Add parameter @parmBusinessRulesBalancingRequired
* 07/14/2017 PT#141767379 MAA  Added PostDepositBalancingRequired
* 01/18/2019 R360-15290	  JPB  PostDepositPayerRequired
******************************************************************************/
DECLARE @auditMessage	VARCHAR(1024);

SET NOCOUNT ON; 


DECLARE @LocalTransaction bit = 0;
DECLARE @errorDescription VARCHAR(1024);

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	--Verify the user exists
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert RecHubData.dimClientAccounts record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	DECLARE @curTime datetime = GETDATE();

	-- Normalize Billing Fields; do not store blank values
	IF LEN(@parmBillingAccount) = 0	SET @parmBillingAccount = NULL;
	IF LEN(@parmBillingField1) = 0	SET @parmBillingField1 = NULL;
	IF LEN(@parmBillingField2) = 0	SET @parmBillingField2 = NULL;

	--Verify the bank belongs to one of the user's provided entities
	IF NOT EXISTS(	SELECT	RecHubData.dimBanksView.BankKey
					FROM	RecHubData.dimBanksView
						JOIN @parmFIs.nodes('/FIs/ID') FI(x)
							ON RecHubData.dimBanksView.EntityID = FI.x.value('.', 'int')
					WHERE	RecHubData.dimBanksView.SiteBankID = @parmSiteBankID )
	BEGIN
		SET @errorDescription = 'Unable to insert RecHubData.dimClientAccounts record, bank (' + ISNULL(CAST(@parmSiteBankID AS VARCHAR(10)), 'NULL') + ' does not exist or user does not have permissions';
		RAISERROR(@errorDescription, 16, 2);
	END

	--Verify the workgroup ID is not already in use
	IF EXISTS( SELECT * FROM RecHubData.dimClientAccounts WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID )
	BEGIN
		SET @parmErrorCode = 1;
		GOTO Cleanup;
	END

	INSERT INTO RecHubData.dimClientAccounts
		(
			SiteCodeID,
			SiteBankID,
			SiteOrganizationID,
			SiteClientAccountID,
			MostRecent,
			IsActive,
			CutOff,
			OnlineColorMode,
			IsCommingled,
			DataRetentionDays,
			ImageRetentionDays,
			CreationDate,
			ModificationDate,
			SiteClientAccountKey,
			ShortName,
			LongName,
			POBox,
			DDA,
			[FileGroup]
		)
	VALUES
		(
			-1,--TODO: Figure out where this comes from, what it should be set to
			@parmSiteBankID,
			-1,--TODO: Figure out where this comes from, what it should be set to.  Is the RecHubData.dimOrganizations table going away with RAAM?
			@parmSiteClientAccountID,
			1,
			@parmIsActive,
			1,--Default to CutOff for now, may change in the future
			0,--Default to Black & White
			0,
			@parmDataRetentionDays,
			@parmImageRetentionDays,
			@curTime,
			@curTime,
			NEWID(),
			@parmShortName,
			@parmLongName,
			null,
			null,
			@parmFileGroup);

	SET @parmClientAccountKey = SCOPE_IDENTITY();

	-- Audit the RecHubData.dimClientAccounts insert
	SET @auditMessage = 'Added dimClientAccounts for EntityID: ' + ISNULL(CAST(@parmEntityID AS VARCHAR(10)), 'NULL')
		+ ' ClientAccountKey: ' + CAST(@parmClientAccountKey AS VARCHAR(10))
		+ ': SiteBankID = ' + CAST(@parmSiteBankID AS VARCHAR(20)) 
		+ ', WorkgroupID = ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
		+ ', IsActive = ' + CAST(@parmIsActive AS VARCHAR(5))
		+ ', DataRetentionDays = ' + CAST(@parmDataRetentionDays AS VARCHAR(5))
		+ ', ImageRetentionDays = ' + CAST(@parmImageRetentionDays AS VARCHAR(5))
		+ ', ShortName = ' + @parmShortName
		+ ', LongName = ' + ISNULL(@parmLongName, 'NULL')
		+ ', FileGroup = ' + ISNULL(@parmFileGroup, 'NULL')
		+ '.';
	EXEC RecHubCommon.usp_WFS_DataAudit_ins
		@parmUserID				= @parmUserID,
		@parmApplicationName	= 'RecHubData.usp_dimClientAccounts_Ins',
		@parmSchemaName			= 'RecHubData',
		@parmTableName			= 'dimClientAccounts',
		@parmColumnName			= 'ClientAccountKey',
		@parmAuditValue			= @parmClientAccountKey,
		@parmAuditType			= 'INS',
		@parmAuditMessage		= @auditMessage;

	EXEC RecHubUser.usp_OLWorkgroups_Merge
		@parmSiteBankID					= @parmSiteBankID,
		@parmSiteClientAccountID		= @parmSiteClientAccountID,
		@parmEntityID					= @parmEntityID,
		@parmViewingDays				= @parmViewingDays,
		@parmMaximumSearchDays			= @parmMaximumSearchDays,
		@parmCheckImageDisplayMode		= @parmCheckImageDisplayMode,
		@parmDocumentImageDIsplayMode	= @parmDocumentImageDIsplayMode,
		@parmHOA						= @parmHOA,
		@parmDisplayBatchID				= @parmDisplayBatchID,
		@parmInvoiceBalancingOption		= @parmInvoiceBalancingOption,
		@parmUserID						= @parmUserID,
		@parmBillingAccount				= @parmBillingAccount,
		@parmBillingField1				= @parmBillingField1,
		@parmBillingField2				= @parmBillingField2,
		@parmEntityName					= @parmEntityName;

	EXEC RecHubData.usp_ElectronicAccounts_Merge
		@parmEntityID				= @parmEntityID,
		@parmSiteBankID				= @parmSiteBankID,
		@parmSiteClientAccountID	= @parmSiteClientAccountID,
		@parmDDAs					= @parmDDAs,
		@parmUserID					= @parmUserID;

	EXEC RecHubData.usp_ClientAccountsDataEntryColumns_Merge
		@parmEntityID				= @parmEntityID,
		@parmClientAccountKey		= @parmClientAccountKey,
		@parmDataEntryColumns		= @parmDataEntryColumns,
		@parmUserID					= @parmUserID,
		@parmUserDisplayName		= @parmUserDisplayName;

	IF (@parmBusinessRulesInvoiceRequired IS NOT NULL OR @parmBusinessRulesBalancingRequired IS NOT NULL OR 
		@parmBusinessRulesPDBalancingRequired IS NOT NULL OR @parmBusinessRulesPDPayerRequired IS NOT NULL)
	BEGIN
		EXEC RecHubData.usp_dimWorkgroupBusinessRules_Merge
			@paramSiteBankId              = @parmSiteBankID,
			@paramSiteClientAccountId     = @parmSiteClientAccountID,
			@paramInvoiceRequired		  = @parmBusinessRulesInvoiceRequired,
			@paramBalancingRequired		  = @parmBusinessRulesBalancingRequired,
			@paramPDBalancingRequired     = @parmBusinessRulesPDBalancingRequired,
			@paramPDPayerRequired		  = @parmBusinessRulesPDPayerRequired;
	END;
	
	--EXEC RecHubException.usp_DataEntrySetups_Merge
		--@parmEntityID				= @parmEntityID,
		--@parmSiteBankID			= @parmSiteBankID,
		--@parmWorkgroupID			= @parmSiteClientAccountID,
		--@parmDeadLineDay			= @parmDeadLineDay,
		--@parmRequiresInvoice		= @parmRequiresInvoice,
		--@parmBusinessRules		= @parmBusinessRules,
		--@parmUserID				= @parmUserID;

	Cleanup:

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

