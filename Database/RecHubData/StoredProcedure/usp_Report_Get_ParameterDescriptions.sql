--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_Get_ParameterDescriptions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_Report_Get_ParameterDescriptions') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_Report_Get_ParameterDescriptions;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_Report_Get_ParameterDescriptions
(
	@parmUserID						INT,
	@parmSessionID					UNIQUEIDENTIFIER,
	@parmReportParameters			XML,
	@parmEntityNameFromBreadCrumb	BIT = 0 
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the description for the report parameters that were selected
*
* Modification History
* Created
* 05/06/2013 WI 107241 KLC	Created.
* 07/09/2013 WI	107241 KLC	Fixed issue with @filterDateKey and added additional
*								parameters.
* 07/11/2013 WI 108635 KLC	Added second filter date parameters 
* 07/26/2013 WI 107241 JBS	Changing called proc from RecHubUser.usp_OLClientAccounts_Get_ByUserID
*							to RecHubUser.usp_OLClientAccounts_Get_ByUserID_Reporting
* 07/26/2013 WI 107241 KLC	Updated to change grouping and sorting labels
* 07/30/2013 WI	107241 KLC	Updated to return current date for filter date 2 if nothing was provided
* 08/15/2013 WI 107241 KLC	Added user type to the returned name to ensure uniqueness
* 09/09/2013 WI 107241 KLC	Added status and deadline translations for group by and sort by
* 10/31/2013 WI 119708 CMC	Updated to use new Org Hierarchy Tree
* 11/13/2013 WI 122323 CMC  Use new view for Account Name.
* 11/13/2013 WI 122424 CMC  Show the correct account labels.
* 11/20/2013 WI 123073 CMC  Increase the size of the ClientAccountName  
							to a VARCHAR(MAX) to handle to variable size
							display label.
* 07/07/2014 WI 153405 KLC	Updates for version 2.01
*								- Orgs change to Entities
*								- ClientAccounts changed to Workgroups
* 07/17/2014 WI 146686 EAS	Add Payment Source type to dataset
* 10/20/2014 WI 173122 LA	Add Event Type and RAAM Event Type type to dataset
* 11/03/2014 WI 153405 KLC	Updated sort descriptions
* 12/08/2014 WI 153405 KLC	Updated sort descriptions
* 12/09/2014 WI 181226 LA	Added Filter for Show Summary and Show Exceptions
* 02/05/2015 WI 188433 MA   Increased Activity VARCHAR size
* 02/05/2015 WI 188470 MA   Added FullName field to return query
* 04/17/2015 WI 202445 JBS	Removed UserType Column.  It no longer exists 
* 05/04/2015 WI 211518 JPB	Added option to get root entity from the BreadCrumb
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDate			 VARCHAR(10),
			@filterDate2		 VARCHAR(10),
			@startDate			 VARCHAR(10),
			@endDate			 VARCHAR(10),
			@groupBy			 VARCHAR(20),
			@sortBy				 VARCHAR(20),
			@sortDir			 VARCHAR(10),
			@filterWGExtRefID	 VARCHAR(40),
			@rootEntityID		 INT,
			@rootEntityName		 VARCHAR(40),
			@workgroupName		 VARCHAR(60),
			@filterUserID		 INT,
			@filterUserName		 VARCHAR(80),
			@filterPaymentSource VARCHAR(30),
			@filterEventType	 VARCHAR(80),
			@filterRAAMEventType VARCHAR(80),
			@filterShowExceptions		 INT,
			@filterShowReceivables		 INT;

	--In the future, if reporting grows, we should tie this into the report configuration's paramters so we don't have to hard code the parameter identifiers.
	SELECT	@filterDate = CASE WHEN Parms.val.value('(FD)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(FD)[1]', 'DATETIME'), 101) ELSE NULL END,
			@filterDate2 = CASE WHEN Parms.val.value('(FD2)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(FD2)[1]', 'DATETIME'), 101) ELSE NULL END,
			@startDate = CASE WHEN Parms.val.value('(SD)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(SD)[1]', 'DATETIME'), 101) ELSE NULL END,
			@endDate = CASE WHEN Parms.val.value('(ED)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(ED)[1]', 'DATETIME'), 101) ELSE NULL END,
			@groupBy = COALESCE(LOWER(Parms.val.value('(GB)[1]', 'VARCHAR(20)')), ''),
			@sortBy = COALESCE(LOWER(Parms.val.value('(SB)[1]', 'VARCHAR(20)')), ''),
			@sortDir = COALESCE(LOWER(Parms.val.value('(SO)[1]', 'VARCHAR(10)')), ''),
			@rootEntityName = 
				CASE
					WHEN @parmEntityNameFromBreadCrumb = 0 THEN COALESCE(Parms.val.value('(REN)[1]', 'VARCHAR(40)'), '')
					ELSE Parms.val.value('(RAAM/Entity/@BreadCrumb)[1]','VARCHAR(40)')
				END,
			@filterWGExtRefID = CASE WHEN Parms.val.value('(WG)[1]', 'VARCHAR(40)') <> '' THEN REPLACE(Parms.val.value('(WG)[1]', 'VARCHAR(40)'), '|', '.') ELSE NULL END,
			@filterUserID = Parms.val.value('(USER)[1]', 'INT'),
			@filterPaymentSource = Parms.val.value('(PS)[1]', 'VARCHAR(30)'),
			@filterEventType = COALESCE(Parms.val.value('(EVT)[1]', 'VARCHAR(80)'), ''),
			@filterRAAMEventType = COALESCE(Parms.val.value('(REVT)[1]', 'VARCHAR(10)'), ''),
			@filterShowExceptions = COALESCE(Parms.val.value('(SEC)[1]', 'INT'), 1),
			@filterShowReceivables = COALESCE(Parms.val.value('(SRC)[1]', 'INT'), 1)
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);
	
	IF( @parmEntityNameFromBreadCrumb = 1 )
	BEGIN
		SELECT
			@rootEntityName =
				CASE
					WHEN CHARINDEX('\',@rootEntityName) = 0 THEN @rootEntityName
					ELSE SUBSTRING(@rootEntityName,CHARINDEX('\',@rootEntityName)+1,LEN(@rootEntityName)-CHARINDEX('\',@rootEntityName)+1)
				END
	END

	IF @filterWGExtRefID IS NOT NULL
	BEGIN
		DECLARE @siteBankID INT,
				@siteClientAccountID INT;

		--parse the external reference id
		SET @siteBankID = PARSENAME(@filterWGExtRefID, 2);
		SET @siteClientAccountID = PARSENAME(@filterWGExtRefID, 1);

		SET @workgroupName = '';

		--get the workgroup's entity name and validate the user has rights to it
		DECLARE @wgEntityName VARCHAR(40);
		SELECT TOP 1 @wgEntityName = EntityName
		FROM RecHubUser.SessionClientAccountEntitlements
		WHERE SiteBankID = @siteBankID AND SiteClientAccountID = @siteClientAccountID and SessionID = @parmSessionID;

		IF @wgEntityName IS NOT NULL
		BEGIN
			SELECT @workgroupName = RecHubData.dimClientAccountsView.DisplayLabel
			FROM RecHubData.dimClientAccountsView
			WHERE RecHubData.dimClientAccountsView.SiteBankID = @siteBankID
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = @siteClientAccountID;

			IF @rootEntityName = ''
			BEGIN
				SET @rootENtityName = @wgEntityName;
			END
		END
	END
	
	IF @filterUserID > 0
	BEGIN
		SELECT 
			@filterUserName = RecHubUser.Users.LogonName	-- WI 202445
		FROM 
			RecHubUser.Users
		WHERE 
			RecHubUser.Users.UserID = @filterUserID;
	END
	
	DECLARE @LogonName  VARCHAR(24);
	SELECT @LogonName = LogonName FROM RecHubUser.Users WHERE UserID = @parmUserID;

	SELECT	
		@LogonName AS UserName,
		@LogonName as FullName,
		@filterDate AS FilterDate,
		COALESCE(@filterDate2, CONVERT(VARCHAR(10), GETDATE(), 101)) AS FilterDate2,
		@startDate AS StartDate,
		@endDate AS EndDate,
		CASE WHEN @groupBy = '' THEN 'No Grouping Selected'
			ELSE (
				CASE @groupBy	WHEN 'workgroup'		THEN 'Workgroup'
								WHEN 'paymenttype'		THEN 'Payment Type'
								WHEN 'batchsource'		THEN 'Payment Source'
								WHEN 'status'			THEN 'Status'
								ELSE @groupBy END
				) END AS GroupBy,
		CASE WHEN @sortBy = '' THEN 'No Sorting Selected'
			ELSE (
				CASE @sortBy	WHEN 'workgroup'			THEN 'Workgroup'
								WHEN 'paymenttype'			THEN 'Payment Type'
								WHEN 'batchsource'			THEN 'Payment Source'
								WHEN 'batchcount'			THEN 'Batch Count'
								WHEN 'trancount'			THEN 'Transaction Count'
								WHEN 'totalamount'			THEN 'Total Amount'
								WHEN 'checkamount'			THEN 'Payment Amount'
								WHEN 'commonexceptionid'	THEN 'Exception Id'
								WHEN 'depositdate'			THEN 'Deposit Date'
								WHEN 'batchid'				THEN 'Batch ID'
								WHEN 'status'				THEN 'Status'
								WHEN 'deadline'				THEN 'Deadline'
								WHEN 'entity'				THEN 'Entity'
								WHEN 'tranid'				THEN 'Transaction Id'
								WHEN 'paymentcount'			THEN 'Payment Count'
								ELSE @sortBy END +
				' ' +
				CASE	WHEN @sortDir = 'descending'	THEN 'Descending'--replace it incase the casing isn't correct
						ELSE 'Ascending' END
				) END AS SortDescription,
		COALESCE(@rootEntityName, 'All Entities') AS EntityName,
		COALESCE(@workgroupName, 'All Workgroups') AS WorkgroupName,
		COALESCE(@filterUserName, 'All Users') AS FilterUserName,
		COALESCE(@filterEventType, 'All Event Types') AS FilterEventType,
		COALESCE(@filterRAAMEventType, 'All Event Types') AS FilterRAAMEventType,
		COALESCE(@filterPaymentSource, 'All Payment Sources') AS FilterPaymentSource,
		@filterShowExceptions AS FilterShowExceptions,
		@filterShowReceivables AS FilterShowReceivables

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
