--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ElectronicAccounts_Get_BySiteBankIDClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ElectronicAccounts_Get_BySiteBankIDClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ElectronicAccounts_Get_BySiteBankIDClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ElectronicAccounts_Get_BySiteBankIDClientAccountID
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 03/21/2014
*
* Purpose: Select the ElectronicAccounts that belong to a given workgroup
*
* Modification History
* 03/25/2014 WI 133596 KLC	Created
* 03/19/2015 WI 196668 BLR  No longer joining on session entitlements, we'll
*                           be doing this after the call.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT
		RecHubData.dimDDAs.DDAKey,
		RecHubData.dimDDAs.ABA,
		RecHubData.dimDDAs.DDA
	FROM RecHubData.ElectronicAccounts
		JOIN RecHubData.dimDDAs
			ON RecHubData.ElectronicAccounts.DDAKey = RecHubData.dimDDAs.DDAKey
	WHERE	RecHubData.ElectronicAccounts.SiteBankID = @parmSiteBankID
		AND	RecHubData.ElectronicAccounts.SiteClientAccountID = @parmSiteClientAccountID
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

