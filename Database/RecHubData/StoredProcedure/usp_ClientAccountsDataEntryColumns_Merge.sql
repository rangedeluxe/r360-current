--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ClientAccountsDataEntryColumns_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ClientAccountsDataEntryColumns_Merge') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ClientAccountsDataEntryColumns_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ClientAccountsDataEntryColumns_Merge
(
	@parmEntityID				INT,
	@parmClientAccountKey		INT,
	@parmDataEntryColumns		XML,
	@parmUserID					INT,
	@parmUserDisplayName		VARCHAR(256)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/14/2014
*
* Purpose: Perform a merge operation on the ClientAccountDataEntryColumns table and
*			 inserts into the dimDataEntryColumns table.
*
* Modification History
* 04/14/2014 WI 135762 KLC	Created
* 08/29/2014 WI 135762 KLC	Updated to insert MarkSense column and prevent removal of
*								association between workgroup and de column
* 08/23/2015 WI 231479 MAA  Updated to insert in dimWorkgroupDataEntryColumns 
* 08/26/2015 WI 232068 MAA  Updated to allow updates to DE Fields but only to IsActive,
							MarkSense, ScreenOrder and UILabel. Passing a 
							valid WorkgroupDataEntryColumnKey indicates an update
							passing a 0 indicates an insert. Only inserts unique
							Fields as determined by PBI 229527
* 12/04/2015 WI 240572 BLR  Updated to accept the source display name.
* 12/15/2016 #127604133  JAW  Added @ExceptionBusinessRuleAuditMessages logic.
******************************************************************************/
DECLARE @siteBankID						INT,
		@siteClientAccountID			INT,
		@workgroupLongName				VARCHAR(40),
		@errorDescription				VARCHAR(1024),
		@auditMessage					VARCHAR(1024),
		@auditClientAccountKey			INT,
		@auditDataEntryColumnKey		INT,
		@auditBatchSourceKey			SMALLINT,
		@auditIsCheck					BIT,
		@auditIsActive					BIT,
		@auditIsRequired				BIT,
		@auditMarkSense					BIT,
		@auditDataType					TINYINT,
		@auditScreenOrder				TINYINT,
		@auditUILabel					NVARCHAR(64),
		@auditFieldName					NVARCHAR(256),
		@auditDisplayName				NVARCHAR(256),
		@auditPaymentSourceLongName		VARCHAR(128),
		@auditType						VARCHAR(3),
		@userDisplayName				VARCHAR(256);

DECLARE @dimWorkgroupDataEntryColumnsInserts TABLE(WorkgroupDataEntryColumnKey BIGINT, SiteBankID INT, SiteClientAccountID INT, BatchSourceKey SMALLINT, 
												   IsCheck BIT, IsActive BIT, IsRequired BIT, MarkSense BIT, DataType TINYINT, ScreenOrder TINYINT, CreationDate DATETIME,
												   ModificationDate DATETIME, UILabel NVARCHAR(64), FieldName NVARCHAR(256), SourceDisplayName NVARCHAR(256))

DECLARE @dimWorkgroupDataEntryColumnsUpdates TABLE(WorkgroupDataEntryColumnKey BIGINT, SiteBankID INT, SiteClientAccountID INT, BatchSourceKey SMALLINT, 
												   IsCheck BIT, IsActive BIT, IsRequired BIT, OriginalIsRequired BIT, MarkSense BIT, DataType TINYINT, ScreenOrder TINYINT, CreationDate DATETIME,
												   ModificationDate DATETIME, UILabel NVARCHAR(64), FieldName NVARCHAR(256), SourceDisplayName NVARCHAR(256))

DECLARE @ExceptionBusinessRuleAuditMessages TABLE(
	WorkgroupDataEntryColumnKey BIGINT,
	FieldName NVARCHAR(256),
	UILabel NVARCHAR(64),
	PaymentSourceLongName VARCHAR(128),
	IsRequired BIT
);

SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END	
	
	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to merge data entry columns with RecHubData.ClientAccountsDataEntryColumns, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END
	
	SELECT
		@siteBankID = SiteBankID,
		@siteClientAccountID = SiteClientAccountID,
		@workgroupLongName = LongName
	FROM
		RecHubData.dimClientAccounts
	WHERE
		ClientAccountKey = @parmClientAccountKey;

	IF @siteBankID IS NULL OR @siteClientAccountID IS NULL
	BEGIN
		SET @errorDescription = 'Unable to merge data entry columns with RecHubData.ClientAccountsDataEntryColumns, the RecHubData.dimClientAccounts record does not exist for key ' + CAST(@parmClientAccountKey AS VARCHAR(10));
		RAISERROR(@errorDescription, 16, 2);
	END

	DECLARE @curTime datetime = GETDATE();

	/*First update  the DE fields   */
	UPDATE RecHubData.dimWorkgroupDataEntryColumns
	SET IsActive = DEColumns.DEColumn.value('@isactive', 'BIT'),
		IsRequired = DEColumns.DEColumn.value('@isrequired', 'BIT'),
		MarkSense = DEColumns.DEColumn.value('@marksense', 'BIT') ,
		ScreenOrder = DEColumns.DEColumn.value('@screenorder', 'TINYINT'), 
		ModificationDate = @curTime, 
		UILabel = DEColumns.DEColumn.value('@uilabel', 'NVARCHAR(64)'),
		SourceDisplayName = DEColumns.DEColumn.value('@displayname', 'NVARCHAR(256)')
	OUTPUT DEColumns.DEColumn.value('@dataentrycolumnkey', 'INT'), INSERTED.SiteBankID, INSERTED.SiteClientAccountID, INSERTED.BatchSourceKey, 
			INSERTED.IsCheck, INSERTED.IsActive, INSERTED.IsRequired, DELETED.IsRequired, INSERTED.MarkSense, INSERTED.DataType, INSERTED.ScreenOrder,
			INSERTED.CreationDate, INSERTED.ModificationDate, INSERTED.UILabel, INSERTED.FieldName, INSERTED.SourceDisplayName
	INTO   @dimWorkgroupDataEntryColumnsUpdates
	FROM @parmDataEntryColumns.nodes('/decolumns/decolumn') DEColumns(DEColumn)
	WHERE RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey  = DEColumns.DEColumn.value('@dataentrycolumnkey', 'INT');

	/*Now insert the new DE fields*/
	INSERT INTO RecHubData.dimWorkgroupDataEntryColumns(SiteBankID, SiteClientAccountID, BatchSourceKey, 
														IsCheck, IsActive, IsRequired, MarkSense, DataType, ScreenOrder, 
														CreationDate, ModificationDate, UILabel, FieldName, SourceDisplayName)
	OUTPUT INSERTED.WorkgroupDataEntryColumnKey, INSERTED.SiteBankID, INSERTED.SiteClientAccountID, INSERTED.BatchSourceKey, 
			INSERTED.IsCheck, INSERTED.IsActive, INSERTED.IsRequired, INSERTED.MarkSense, INSERTED.DataType, INSERTED.ScreenOrder, 
			INSERTED.CreationDate, INSERTED.ModificationDate, INSERTED.UILabel, INSERTED.FieldName, INSERTED.SourceDisplayName
	INTO @dimWorkgroupDataEntryColumnsInserts
	SELECT	@siteBankID,
			@siteClientAccountID,
			DEColumns.DEColumn.value('@batchsourcekey', 'SMALLINT'),
			DEColumns.DEColumn.value('@ischeck', 'BIT'),
			DEColumns.DEColumn.value('@isactive', 'BIT'),
			DEColumns.DEColumn.value('@isrequired', 'BIT'),
			DEColumns.DEColumn.value('@marksense', 'BIT'),
			DEColumns.DEColumn.value('@datatype', 'TINYINT'),
			DEColumns.DEColumn.value('@screenorder', 'TINYINT'),
			@curTime,
			@curTime,
			DEColumns.DEColumn.value('@uilabel', 'NVARCHAR(64)'),
			DEColumns.DEColumn.value('@fieldname', 'NVARCHAR(256)'),
			DEColumns.DEColumn.value('@displayname', 'NVARCHAR(256)')
	FROM @parmDataEntryColumns.nodes('/decolumns/decolumn') DEColumns(DEColumn)
		LEFT JOIN RecHubData.dimWorkgroupDataEntryColumns
			ON DEColumns.DEColumn.value('@ischeck', 'BIT') =RecHubData.dimWorkgroupDataEntryColumns.IsCheck
				AND DEColumns.DEColumn.value('@fieldname', 'NVARCHAR(256)') = RecHubData.dimWorkgroupDataEntryColumns.FieldName
				AND RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey is null 
	WHERE DEColumns.DEColumn.value('@dataentrycolumnkey', 'INT')  = 0  

	-- Prepare exception-rule audits
	-- All added DE fields should generate exception-rule audit messages
	INSERT INTO @ExceptionBusinessRuleAuditMessages (
		WorkgroupDataEntryColumnKey,
		FieldName,
		UILabel,
		PaymentSourceLongName,
		IsRequired
	) SELECT
		dimWorkgroupDataEntryColumnsInserts.WorkgroupDataEntryColumnKey,
		dimWorkgroupDataEntryColumnsInserts.FieldName,
		dimWorkgroupDataEntryColumnsInserts.UILabel,
		dimBatchSources.LongName,
		dimWorkgroupDataEntryColumnsInserts.IsRequired
	FROM
		@dimWorkgroupDataEntryColumnsInserts dimWorkgroupDataEntryColumnsInserts
		LEFT OUTER JOIN RecHubData.dimBatchSources ON
			dimWorkgroupDataEntryColumnsInserts.BatchSourceKey = dimBatchSources.BatchSourceKey;
	-- Modified DE fields should generate exception-rule audit messages if IsRequired changed
	INSERT INTO @ExceptionBusinessRuleAuditMessages (
		WorkgroupDataEntryColumnKey,
		FieldName,
		UILabel,
		PaymentSourceLongName,
		IsRequired
	) SELECT
		dimWorkgroupDataEntryColumnsUpdates.WorkgroupDataEntryColumnKey,
		dimWorkgroupDataEntryColumnsUpdates.FieldName,
		dimWorkgroupDataEntryColumnsUpdates.UILabel,
		dimBatchSources.LongName,
		dimWorkgroupDataEntryColumnsUpdates.IsRequired
	FROM
		@dimWorkgroupDataEntryColumnsUpdates dimWorkgroupDataEntryColumnsUpdates
		LEFT OUTER JOIN RecHubData.dimBatchSources ON
			dimWorkgroupDataEntryColumnsUpdates.BatchSourceKey = dimBatchSources.BatchSourceKey
	WHERE
		dimWorkgroupDataEntryColumnsUpdates.IsRequired <> dimWorkgroupDataEntryColumnsUpdates.OriginalIsRequired;

	--audit update
	SET @auditDataEntryColumnKey = NULL;
	SELECT TOP 1 @auditDataEntryColumnKey	= WorkgroupDataEntryColumnKey,
					@auditBatchSourceKey		= BatchSourceKey,
					@auditIsCheck				= IsCheck,
					@auditIsActive				= IsActive,
					@auditIsRequired			= IsRequired,
					@auditMarkSense				= MarkSense,
					@auditDataType				= DataType,
					@auditScreenOrder			= ScreenOrder,
					@auditUILabel				= UILabel,
					@auditDisplayName		    = SourceDisplayName,
					@auditFieldName				= FieldName
	FROM @dimWorkgroupDataEntryColumnsUpdates;
	WHILE @auditDataEntryColumnKey IS NOT NULL
	BEGIN
		SET @auditMessage = 'Updated dimWorkgroupDataEntryColumns' --don't audit entity id here, these belong to the system, not a specific entity
			+ ': BatchSourceKey = ' + CAST(@auditBatchSourceKey AS VARCHAR(5))
			+ ', IsCheck = ' + CAST(@auditIsCheck AS VARCHAR(1))
			+ ', IsActive = ' + CAST(@auditIsActive AS VARCHAR(1))
			+ ', IsRequired = ' + CAST(@auditIsRequired AS VARCHAR(1))
			+ ', MarkSense = ' + CAST(@auditMarkSense AS VARCHAR(3))
			+ ', DataType = ' + CAST(@auditDataType AS VARCHAR(3))
			+ ', ScreenOrder = ' + CAST(@auditScreenOrder AS VARCHAR(3))
			+ ', UILabel = ' + @auditUILabel
			+ ', SourceDisplayName = ' + @auditDisplayName
			+ ', FieldName = ' + @auditFieldName			
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_ClientAccountsDataEntryColumns_Merge',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimWorkgroupDataEntryColumns',
			@parmColumnName			= 'WorkgroupDataEntryColumnKey',
			@parmAuditValue			= @auditDataEntryColumnKey,
			@parmAuditType			= 'UPD',
			@parmAuditMessage		= @auditMessage;

		DELETE FROM @dimWorkgroupDataEntryColumnsInserts WHERE WorkgroupDataEntryColumnKey = @auditDataEntryColumnKey;

		SET @auditDataEntryColumnKey = NULL;
		SELECT TOP 1	@auditDataEntryColumnKey	= WorkgroupDataEntryColumnKey,
						@auditIsCheck				= IsCheck,
						@auditIsActive				= IsActive,
						@auditIsRequired			= IsRequired,
						@auditMarkSense				= MarkSense,
						@auditDataType				= DataType,
						@auditScreenOrder			= ScreenOrder,
						@auditUILabel				= UILabel,
						@auditDisplayName		    = SourceDisplayName,
						@auditFieldName				= FieldName						
		FROM @dimWorkgroupDataEntryColumnsInserts;
	END
	
	--end audit update


	--Audit insert of dimDataEntryColumns (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditDataEntryColumnKey = NULL;
	SELECT TOP 1	@auditDataEntryColumnKey	= WorkgroupDataEntryColumnKey,
					@auditBatchSourceKey		= BatchSourceKey,
					@auditIsCheck				= IsCheck,
					@auditIsActive				= IsActive,
					@auditIsRequired			= IsRequired,
					@auditMarkSense				= MarkSense,
					@auditDataType				= DataType,
					@auditScreenOrder			= ScreenOrder,
					@auditUILabel				= UILabel,
					@auditDisplayName		    = SourceDisplayName,
					@auditFieldName				= FieldName					
	FROM @dimWorkgroupDataEntryColumnsInserts;

	WHILE @auditDataEntryColumnKey IS NOT NULL
	BEGIN
		SET @auditMessage = 'Added dimWorkgroupDataEntryColumns' --don't audit entity id here, these belong to the system, not a specific entity
			+ ': BatchSourceKey = ' + CAST(@auditBatchSourceKey AS VARCHAR(5))
			+ ', IsCheck = ' + CAST(@auditIsCheck AS VARCHAR(1))
			+ ', IsActive = ' + CAST(@auditIsActive AS VARCHAR(1))
			+ ', IsRequired = ' + CAST(@auditIsRequired AS VARCHAR(1))
			+ ', MarkSense = ' + CAST(@auditMarkSense AS VARCHAR(3))
			+ ', DataType = ' + CAST(@auditDataType AS VARCHAR(3))
			+ ', ScreenOrder = ' + CAST(@auditScreenOrder AS VARCHAR(3))
			+ ', UILabel = ' + @auditUILabel
			+ ', SourceDisplayName = ' + @auditDisplayName
			+ ', FieldName = ' + @auditFieldName			
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_ClientAccountsDataEntryColumns_Merge',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimWorkgroupDataEntryColumns',
			@parmColumnName			= 'WorkgroupDataEntryColumnKey',
			@parmAuditValue			= @auditDataEntryColumnKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;

		DELETE FROM @dimWorkgroupDataEntryColumnsInserts WHERE WorkgroupDataEntryColumnKey = @auditDataEntryColumnKey;

		SET @auditDataEntryColumnKey = NULL;
		SELECT TOP 1	@auditDataEntryColumnKey	= WorkgroupDataEntryColumnKey,
						@auditIsCheck				= IsCheck,
						@auditIsActive				= IsActive,
						@auditIsRequired			= IsRequired,
						@auditMarkSense				= MarkSense,
						@auditDataType				= DataType,
						@auditScreenOrder			= ScreenOrder,
						@auditUILabel				= UILabel,
						@auditDisplayName		    = SourceDisplayName,
						@auditFieldName				= FieldName						
		FROM @dimWorkgroupDataEntryColumnsInserts;
	END

	-- Exception-rule auditing (written out after the regular "edit" audit messages)
	IF @parmUserDisplayName IS NOT NULL
	BEGIN
		SET @userDisplayName = @parmUserDisplayName;
	END
	ELSE
	BEGIN
		SET @userDisplayName = '(user not found)';
		SELECT
			@userDisplayName = LogonName
		FROM
			RecHubUser.Users
		WHERE
			UserID = @parmUserID;
	END

	WHILE (1 = 1)
	BEGIN
		SET @auditDataEntryColumnKey = NULL;
		SET @auditFieldName = '(unknown)';
		SET @auditUILabel = '(unknown)';
		SET @auditPaymentSourceLongName = '(unknown)';
		SET @auditIsRequired = 0;
		SELECT TOP 1
			@auditDataEntryColumnKey = WorkgroupDataEntryColumnKey,
			@auditFieldName = FieldName,
			@auditUILabel = UILabel,
			@auditPaymentSourceLongName = PaymentSourceLongName,
			@auditIsRequired = IsRequired
		FROM
			@ExceptionBusinessRuleAuditMessages;

		-- Exit the loop when there are no more messages in the table.
		IF (@auditDataEntryColumnKey IS NULL)
			BREAK;

		SET @auditMessage = 'Post-Deposit Exceptions'
			+ ': ' + @userDisplayName
			+ ' has turned ' + CASE WHEN @auditIsRequired = 1 THEN 'on' ELSE 'off' END
			+ ' the "Field Is Required" Exception business rule for '
			+ 'Bank ' + CAST(@siteBankID AS VARCHAR(11))
			+ ', Workgroup ' + CAST(@siteClientAccountID AS VARCHAR(11)) + '-' + @workgroupLongName
			+ ', Data entry field name ' + @auditFieldName
			+ ', Data entry UI Label ' + @auditUILabel
			+ ', payment source ' + @auditPaymentSourceLongName
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_ClientAccountsDataEntryColumns_Merge',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimWorkgroupDataEntryColumns',
			@parmColumnName			= 'WorkgroupDataEntryColumnKey',
			@parmAuditValue			= @auditDataEntryColumnKey,
			@parmAuditType			= 'UPD', -- Log message always refers to edits even if it's a new record
			@parmAuditMessage		= @auditMessage;

		DELETE FROM
			@ExceptionBusinessRuleAuditMessages
		WHERE
			WorkgroupDataEntryColumnKey = @auditDataEntryColumnKey;
	END;
	-- End of exception-rule auditing

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


