--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factStubs_Get_ByBatchSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factStubs_Get_ByBatchSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factStubs_Get_ByBatchSequence
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factStubs_Get_ByBatchSequence 
(
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID	INT,
       @parmImmutableDateKey	INT,
       @parmBatchID				BIGINT,
       @parmBatchSequence		INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR 33795 JMC	Created
* 03/29/2013 WI 90714 JBS	Update to 2.0 release, Change Schema name
*							Renamed name from usp_GetStub 
*							Changed Parameters:  @parmSiteLockboxID to @parmSiteClientAccountID	
*							@parmProcessingDateKey to @parmImmutableDateKey.
*							Changed all references: Lockbox to ClientAccount,
*							Processing to Immutable, Customer to Organization.
*							Added IsDeleted = 0 
* 06/26/2014 WI 142858 JBS	Changing @parmBatchID to BIGINT.  SessionID not needed
* 10/10/2014 WI 171346 TWE  return sourcebatchid,batchnumber,SourceProcessingDateKey
* 07/17/2015 WI 224775 TWE  return BatchSourceKey
* 07/28/2015 WI 224996 JPB	Updated for dimWorkgroupDataEntryColumns table.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BankKey			INT;
DECLARE @ClientAccountKey	INT;
DECLARE @DepositDateKey		INT;

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	IF EXISTS (
		SELECT	RecHubData.factStubs.BankKey,
				RecHubData.factStubs.OrganizationKey,
				RecHubData.factStubs.ClientAccountKey,
				RecHubData.factStubs.DepositDateKey,
				RecHubData.factStubs.ImmutableDateKey,
				RecHubData.factStubs.SourceProcessingDateKey,
				RecHubData.factStubs.BatchNumber,
				RecHubData.factStubs.BatchSourceKey,
				RecHubData.factStubs.BatchID,
				RecHubData.factStubs.SourceBatchID,
				RecHubData.factStubs.TransactionID,
				RecHubData.factStubs.BatchSequence,
				RecHubData.factStubs.Amount,
				RecHubData.factStubs.AccountNumber,
				RecHubData.factStubs.IsOMRDetected,
				RecHubData.factStubs.DocumentBatchSequence
		FROM	RecHubData.factStubs
		WHERE  	RecHubData.factStubs.ClientAccountKey = @ClientAccountKey
				AND RecHubData.factStubs.DepositDateKey = @DepositDateKey
				AND RecHubData.factStubs.IsDeleted = 0
				AND RecHubData.factStubs.ImmutableDateKey = @parmImmutableDateKey
				AND RecHubData.factStubs.BatchID = @parmBatchID
				AND RecHubData.factStubs.BatchSequence = @parmBatchSequence
	)
	BEGIN
		SELECT	RecHubData.factStubs.BankKey,
				RecHubData.factStubs.OrganizationKey,
				RecHubData.factStubs.ClientAccountKey,
				RecHubData.factStubs.DepositDateKey,
				RecHubData.factStubs.ImmutableDateKey,				
				RecHubData.factStubs.SourceProcessingDateKey,
				RecHubData.factStubs.BatchNumber,
				RecHubData.factStubs.BatchSourceKey,
				RecHubData.factStubs.BatchID,
				RecHubData.factStubs.SourceBatchID,
				RecHubData.factStubs.TransactionID,
				RecHubData.factStubs.BatchSequence,
				RecHubData.factStubs.Amount,
				RecHubData.factStubs.AccountNumber,
				RecHubData.factStubs.IsOMRDetected,
				RecHubData.factStubs.DocumentBatchSequence
		FROM	RecHubData.factStubs
		WHERE  	RecHubData.factStubs.ClientAccountKey = @ClientAccountKey
				AND RecHubData.factStubs.DepositDateKey = @DepositDateKey
				AND RecHubData.factStubs.IsDeleted = 0
				AND RecHubData.factStubs.ImmutableDateKey = @parmImmutableDateKey
				AND RecHubData.factStubs.BatchID = @parmBatchID
				AND RecHubData.factStubs.BatchSequence = @parmBatchSequence;
	END
	ELSE 
	BEGIN

		DECLARE @Amount			MONEY
		DECLARE @AccountNumber	VARCHAR(80)
		DECLARE @IsOMRDetected	BIT
		DECLARE @DocumentBatchSequence INT
	
		SET @Amount = 0
		SET @AccountNumber = NULL
		SET @IsOMRDetected = 0
		SET @DocumentBatchSequence = NULL

		SELECT	DISTINCT
			RecHubData.factDataEntryDetails.BankKey,
			RecHubData.factDataEntryDetails.OrganizationKey,
			RecHubData.factDataEntryDetails.ClientAccountKey,
			RecHubData.factDataEntryDetails.DepositDateKey,
			RecHubData.factDataEntryDetails.ImmutableDateKey,
			RecHubData.factDataEntryDetails.SourceProcessingDateKey,
			RecHubData.factDataEntryDetails.BatchNumber,
			RecHubData.factDataEntryDetails.BatchSourceKey,
			RecHubData.factDataEntryDetails.BatchID,
			RecHubData.factDataEntryDetails.SourceBatchID,
			RecHubData.factDataEntryDetails.TransactionID,
			RecHubData.factDataEntryDetails.BatchSequence,
			@Amount AS Amount,
			@AccountNumber AS AccountNumber,
			@IsOMRDetected AS IsOMRDetected,
			@DocumentBatchSequence AS DocumentBatchSequence
		FROM	
			RecHubData.factDataEntryDetails
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
		WHERE  	
			RecHubData.factDataEntryDetails.ClientAccountKey = @ClientAccountKey
			AND RecHubData.factDataEntryDetails.DepositDateKey = @DepositDateKey
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
			AND RecHubData.factDataEntryDetails.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.BatchSequence = @parmBatchSequence
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0;
	END

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
