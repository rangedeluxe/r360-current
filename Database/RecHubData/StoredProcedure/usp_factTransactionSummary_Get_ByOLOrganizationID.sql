--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Get_ByOLOrganizationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Get_ByOLOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Get_ByOLOrganizationID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Get_ByOLOrganizationID
(
	@parmSessionID				UNIQUEIDENTIFIER,			--WI 142864
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT, 
	@parmBatchID				BIGINT,						--WI 142864
	@parmDepositDate			DATETIME, 
	@parmTransactionID			INT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: 
*
* Modification History
* 03/23/2009 CR 25817 JMC	Created.
* 12/31/2009 CR 28575  CS   Updated to use olta.factTransactionSummary.
* 01/18/2010 CR 28488 JMC   Fixed: 
*							Name of stored procedure in proc header. 
*							Procedure was filtering for ScannedCheckCount > 0.  This is not correct. 
*							The IF Statement is no longer required. 
* 08/20/2010 CR 30591 JMC   Fixed join to dimLockboxes table.
* 01/19/2012 CR 49163 JNE   Add SiteCode & CaptureSiteCodeID.
* 03/14/2012 CR 49163 WJS   Remove CaptureSiteCodeID and add BatchSiteCode.
* 06/21/2012 CR 53611 JNE   Add BatchCueID to result set.
* 07/20/2012 CR 54153 JNE   Add BatchNumber to result set.
* 04/01/2013 WI 90720 JPB	Updates for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionDetail.
*							Renamed @parmOLCustomerID to @parmOLOrganizationID.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Removed @parmDisplayScannedCheck (not longer used CR 28488).
*							Changed all dimLockboxes refs to dimClientAccounts.
*							Changed all OLLockboxes refs to RecHubUser.OLClientAccounts.
*							Renamed SiteCode to SiteCodeID.
* 06/04/2014 WI 142864 PKW  Batch Collision.
*							Removed table RecHubUser.OLClientAccounts.
*							Replaced @parmOLOrganizationID with @parmSessionID.
* 08/18/2014 WI 159410 JMC  Include SourceBatchID in results.
* 08/29/2014 WI	162682 SAS  ImageRPSReport Added Import Type Key and Processing Date
* 03/24/2015 WI 197780 MAA  added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead
* 02/29/2016 WI 266821 MAA	Addded ImportTypeShortName for new mvc page
* 07/11/2016 WI 291432 BLR  Added another join to the dimClientAccounts table to get the MostRecent Row.
* 03/30/2018 PT 154101786	JPB	Added BatchSourceShortName, PaymentTypeShortName to results
* 10/30/2018 PT 161348262	MGE Added FileGroup to result set
******************************************************************************/
SET NOCOUNT ON;

DECLARE																					--WI 142864
		@StartDateKey INT,																--WI 142864
		@EndDateKey INT;																--WI 142864

BEGIN TRY
	
	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays									--WI 142864
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,										--WI 142864
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,					--WI 142864
		RecHubUser.SessionClientAccountEntitlements.SiteBankID				AS BankID,		--WI 142864
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID	AS ClientAccountID,	--WI 142864
		RTRIM(CA.LongName)		AS LongName,
		CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.DepositDateKey), 112) AS DepositDate,
		CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.ImmutableDateKey), 112) AS PICSDate,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.TxnSequence,
		CA.SiteCodeID,
		RecHubData.factTransactionSummary.BatchSiteCode,
		RecHubData.factTransactionSummary.BatchCueID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.dimBatchSources.ImportTypeKey,		
		CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),RecHubData.factTransactionSummary.SourceProcessingDateKey), 112),101)	AS ProcessingDate,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimBatchPaymentTypes.ShortName AS PaymentTypeShortName,
		CA.[FileGroup]
	FROM 
		RecHubData.dimClientAccounts
		INNER JOIN RecHubData.factTransactionSummary ON
			RecHubData.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON							--WI 142864
			RecHubData.factTransactionSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources ON 
			RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
		INNER JOIN RecHubData.dimImportTypes ON 
			RecHubData.dimImportTypes.ImportTypeKey = RecHubData.dimBatchSources.ImportTypeKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		INNER JOIN RecHubData.dimClientAccounts CA ON
			CA.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND CA.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND CA.MostRecent = 1
	WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID			--WI 142864
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND factTransactionSummary.DepositDateKey = @EndDateKey							--WI 142864 WI 197780
		AND factTransactionSummary.BatchID = @parmBatchID
		AND factTransactionSummary.TransactionID = @parmTransactionID
		AND RecHubData.factTransactionSummary.IsDeleted = 0
	OPTION( RECOMPILE );

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
