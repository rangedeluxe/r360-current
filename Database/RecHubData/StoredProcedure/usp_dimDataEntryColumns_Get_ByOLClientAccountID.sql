--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDataEntryColumns_Get_ByOLClientAccountID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDataEntryColumns_Get_ByOLClientAccountID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByOLClientAccountID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDataEntryColumns_Get_ByOLClientAccountID 
(
	@parmOLClientAccountID UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/15/2009
*
* Purpose: Retrieve all of the data entry fields for a given lockbox
*
* Modification History
* 04/15/2009 CR 25817 JPB	Created
* 04/01/2012 CR 51680 WJS   Do not conditionalize stubs amount, always just return the DataType
* 04/18/2013 WI 90703 JBS	Update to 2.0 release.  Change schema to RecHubData
*							Rename proc From usp_GetLockboxDataEntryFields
*							Change parameter: @parmOLLockboxID to @parmOLCLientAccountID
*							Change references: Lockbox to ClientAccount
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	
	SELECT	DISTINCT RTRIM(RecHubData.dimDataEntryColumns.FldName) AS FldName,
			RTRIM(RecHubData.dimDataEntryColumns.TableName) AS TableName,
			RecHubData.dimDataEntryColumns.DataType AS FldDataTypeEnum,
			COALESCE(RecHubData.dimDataEntryColumns.DisplayName,RecHubData.dimDataEntryColumns.FldName) AS Title
	FROM	RecHubData.dimDataEntryColumns
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns 
				ON RecHubData.dimDataEntryColumns.DataEntryColumnKey = RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey
			INNER JOIN RecHubData.dimClientAccounts 
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
			INNER JOIN RecHubUser.OLClientAccounts 
				ON RecHubUser.OLClientAccounts.SiteBankID = RecHubData.dimClientAccounts.SiteBankID 
				AND RecHubUser.OLClientAccounts.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
	WHERE	RecHubUser.OLClientAccounts.OLClientAccountID = @parmOLClientAccountID
	ORDER BY Title;
			
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
