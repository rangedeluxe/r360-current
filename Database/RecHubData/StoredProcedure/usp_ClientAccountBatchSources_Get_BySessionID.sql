--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ClientAccountBatchSources_Get_BySessionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ClientAccountBatchSources_Get_BySessionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ClientAccountBatchSources_Get_BySessionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ClientAccountBatchSources_Get_BySessionID
(
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmActiveOnly         BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 04/14/2014
*
* Purpose: Get current Batch Sources based on SessionID.  
*		 Used to populate drop down list on Search screens
*
* Modification History
* 07/30/2014 WI 155460 JBS	Created
* 04/15/2014 WI 201160 MAA  Added order by 
* 06/08/2015 WI 219073 BLR  Added the ActiveOnly parm & where clause.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		DISTINCT
		RecHubData.dimBatchSources.BatchSourceKey,
		RecHubData.dimBatchSources.ShortName,
		RecHubData.dimBatchSources.LongName
	FROM
	    RecHubData.dimBatchSources
		INNER JOIN RecHubData.ClientAccountBatchSources ON
			RecHubData.ClientAccountBatchSources.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
			RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = RecHubData.ClientAccountBatchSources.ClientAccountKey
	WHERE 
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND (@parmActiveOnly = 0 OR (@parmActiveOnly = 1 AND RecHubData.dimBatchSources.IsActive = 1))

	ORDER BY RecHubData.dimBatchSources.LongName;
		

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
