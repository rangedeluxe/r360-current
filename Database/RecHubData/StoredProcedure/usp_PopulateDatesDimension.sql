--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_PopulateDatesDimension
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_PopulateDatesDimension') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_PopulateDatesDimension
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_PopulateDatesDimension
(
	@parmStartDate	DATETIME,
	@parmEndDate	DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/10/2009
*
* Purpose: Populates Dates Dimension with values from Start - End dates.  
*      IMPORTANT!! - do not run this against a DB with production data.  Any
*             change in start/end dates will mess up the integrity of the fact
*             tables.
*
* Modification History
* 03/10/2009 CR 25817 JJR	Created
* 03/07/2013 WI 91301 JBS	Update to 2.0 release.  Change Schema Names.
******************************************************************************/
SET NOCOUNT ON 
DECLARE @id INT,
		@date DATETIME,
		@LocalTransaction BIT;
       
SELECT @id = 0,
       @date = DATEADD(dd, @id, @parmStartDate),
	   @LocalTransaction = 0;

BEGIN TRY
	IF( SELECT COUNT(*) AS RecCount FROM RecHubData.dimDates ) = 0 
	BEGIN
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
		WHILE @date <= @parmEndDate
		BEGIN
			INSERT INTO RecHubData.dimDates
			SELECT	CAST(LEFT(REPLACE(CONVERT(varchar,@date,120),'-',''),8) as int),
					@date CalendarDate,
					DATEPART(dd, @date) CalendarDayMonth,
					DATEPART(dy, @date) CalendarDayYear,
					DATEPART(dw, @date) CalendarDayWeek,
					DATENAME(dw, @date) CalendarDayName,
					DATEPART(ww, @date) CalendarWeek,
					DATEPART(mm, @date) CalendarMonth,
					DATENAME(mm, @date) CalendarMonthName,
					DATEPART(qq, @date) CalendarQuarter,
					'Q' + DATENAME(qq, @date) + ' ' + DATENAME(yy, @date) CalendarQuarterName,
					DATEPART(yy, @date) CalendarYear;

			SET @id = @id + 1;
			SET @date = DATEADD(dd, @id, @parmStartDate);
		END
		COMMIT TRANSACTION;
	END
END TRY
BEGIN CATCH
	IF( @LocalTransaction = 1 )
		ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
