--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Get_ByTransactionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocuments_Get_ByTransactionID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocuments_Get_ByTransactionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocuments_Get_ByTransactionID
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142849
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,
	@parmTransactionID			INT,
	@parmDisplayScannedCheck	INT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query fact table for Document records
*
* Modification History
* 03/17/2009 CR 25817  JMC	Created.
* 11/16/2009 CR 28266  JMC	Added ProcessingDate to the returned dataset.
* 11/19/2009 CR 28334  JMC	Added OnlineColorMode AS LockboxColorMode to the returned dataset.
* 04/09/2012 CR 51909  JNE	Added BatchSiteCode to the returned dataset.
* 09/21/2012 CR 55876  JNE	Added BatchNumber to the returned dataset.
* 11/20/2012 WI 70686  JPB	Added index hint/removed dimDates join.
* 04/01/2013 WI 90722  JPB	Updates for 2.0.
*							FP WI 85067 (Performance tuning).
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionDocuments.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Renamed all dimLockboxes refs to dimClientAccounts.
*							Moved recompile from SP to SELECT statement.
* 05/29/2014 WI 142849 PKW  Update for RAAM Integration.
* 09/17/2014 WI	166563 SAS  Added ImportTypeShortName
* 10/24/2014 WI	174089 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175122 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176347 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
* 12/12/2014 WI 176347 KLC	Added granting of rights to the RecHubException db role
* 03/24/2015 WI 197817 MAA  Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead 
* 05/27/2015 WI 215280 JBS  (FP 215005) Add BatchSequence to Order By
* 02/29/2016 WI 266939 MAA Added SourceProcessingDateKey for image retreival 
* 06/07/2016 WI 285290 BLR  Added PaymentType to the Select.
* 08/13/2018 PT 158216761	JPB	Added SequenceWithinTransaction
******************************************************************************/
SET NOCOUNT ON;

DECLARE 
		@StartDateKey	INT,												--WI 142849
		@EndDateKey		INT;												--WI 142849

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	/* Create the memory table with the column names used by the application. Keeps from using AS xxx in the final SELECT */
	DECLARE @ClientAccounts TABLE
	(
		ClientAccountKey INT,
		BankID INT,
		ClientAccountID INT,
		SiteCodeID INT,
		ClientAccountColorMode TINYINT
	);

	INSERT INTO @ClientAccounts(ClientAccountKey,BankID,ClientAccountID,SiteCodeID,ClientAccountColorMode)
	SELECT	
		RecHubData.dimClientAccounts.ClientAccountKey,
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.dimClientAccounts.SiteCodeID,
		RecHubData.dimClientAccounts.OnlineColorMode
	FROM
		RecHubData.dimClientAccounts
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142849
			RecHubData.dimClientAccounts.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE	
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID  --WI 142849
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID;

	SELECT	
		RecHubData.factDocuments.BatchID,
		RecHubData.factDocuments.SourceBatchID,								--WI 142849
		CA.BankID,
		CA.ClientAccountID,
		CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.DepositDateKey), 112),101) AS DepositDate,
		CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factDocuments.ImmutableDateKey), 112),101) AS ImmutableDate,
		RecHubData.factDocuments.ImmutableDateKey AS PICSDate,
		RecHubData.factDocuments.TransactionID,
		RecHubData.factDocuments.TxnSequence,
		RecHubData.factDocuments.SequenceWithinTransaction,
		RTRIM(RecHubData.dimDocumentTypes.FileDescriptor) AS FileDescriptor,
		ISNULL(RTRIM(RecHubData.dimDocumentTypes.DocumentTypeDescription), 'Unknown')	AS Description, 
		RecHubData.factDocuments.BatchSequence,
		RecHubData.factDocuments.DocumentSequence,
		CA.SiteCodeID,
		CA.ClientAccountColorMode,
		RecHubData.factDocuments.BatchSiteCode,
		RecHubData.factDocuments.BatchNumber,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
		RecHubData.factDocuments.SourceProcessingDateKey,
		RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType
		
	FROM	
		RecHubData.factDocuments
		INNER JOIN @ClientAccounts CA ON 
			RecHubData.factDocuments.ClientAccountKey = CA.ClientAccountKey
		INNER JOIN RecHubData.dimDocumentTypes ON 
			RecHubData.factDocuments.DocumentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey
		INNER JOIN RecHubData.dimBatchSources ON 
				RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factDocuments.BatchSourceKey			
		INNER JOIN RecHubData.dimImportTypes ON 
				RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
				RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factDocuments.BatchPaymentTypeKey
	WHERE 
		RecHubData.factDocuments.DepositDateKey	= @EndDateKey				--WI 142849  WI 197817
		AND RecHubData.factDocuments.BatchID = @parmBatchID
		AND RecHubData.factDocuments.TransactionID = @parmTransactionID
		AND RecHubData.factDocuments.DepositStatus >= 850
		AND RecHubData.factDocuments.IsDeleted = 0
		AND 
		( 
			(	@parmDisplayScannedCheck > 0 
				AND RecHubData.dimDocumentTypes.FileDescriptor <> 'C'
			)	 
			OR 
			(
				@parmDisplayScannedCheck = 0 
				AND RecHubData.dimDocumentTypes.FileDescriptor <> 'C'
				AND RecHubData.dimDocumentTypes.FileDescriptor <> 'SC'
			) 
		)
	ORDER BY
		RecHubData.factDocuments.BatchID ASC,
		RecHubData.factDocuments.BatchSequence ASC,
		RecHubData.factDocuments.TxnSequence ASC,
		RecHubData.factDocuments.SequenceWithinTransaction ASC
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH