--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_TransactionCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_TransactionCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_TransactionCount
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_TransactionCount 
(
	@parmSessionID			UNIQUEIDENTIFIER,  --WI 142826
	@parmSiteBankID			INT,
	@parmSiteClientAccountID	INT,
	@parmDepositDateStart		DATETIME,
	@parmDepositDateEnd		DATETIME,
	@parmMinDepositStatus		INT = 850,
	@parmPaymentTypeKey		INT = NULL,
	@parmPaymentSourceKey		INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 02/15/2009 CR 28726       Add new parameter to usp_GetTransactionCount stored 
*                           procedure in support of CSR Research
* 04/01/2013 WI 90719 JPB	Updates for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionCount.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Changed all dimLockboxes refs to dimClientAccounts.
*							Changes all LockboxKey refs to ClientAccountKey.
*							Changed all SiteLockboxID refs to SiteClientAccountID.
*							Changed recompile from SP to SELECT statement.
* 10/10/2013 WI 116687 EAS  Add Payment Type Filter to Transaction Count
* 05/20/2014 WI 142826 DLD  Batch Collision/RAAM Integration.
* 12/09/2014 WI 181270 LA   Added Batch Payment Source Parameter
* 03/02/2015 WI 191990 BLR  Added a where clause to use the mindepositstatus.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @StartDateKey INT,  --WI 142826
        @EndDateKey INT;    --WI 142826

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */  --WI 142826
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays 
		@parmSessionID = @parmSessionID,                            
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	COALESCE(SUM(RecHubData.factBatchSummary.TransactionCount), 0) AS 	TransactionCount
	FROM RecHubData.factBatchSummary
		 INNER JOIN RecHubUser.SessionClientAccountEntitlements 
		 ON RecHubData.factBatchSummary.ClientAccountKey = 		RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142826
	WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID                      --WI 142826
	  AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID                    --WI 142826
      AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID  --WI 142826
	  AND RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey                                 --WI 142826
	  AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey                                   --WI 142826
	  AND RecHubData.factBatchSummary.IsDeleted = 0
	  AND RecHubData.factBatchSummary.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeKey, RecHubData.factBatchSummary.BatchPaymentTypeKey)
	  AND RecHubData.factBatchSummary.BatchSourceKey = ISNULL(@parmPaymentSourceKey, RecHubData.factBatchSummary.BatchSourceKey)
	  AND RecHubData.factBatchSummary.DepositStatus >= @parmMinDepositStatus
	OPTION( RECOMPILE );

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
