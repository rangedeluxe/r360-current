--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_DEColumns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Get_DEColumns') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Get_DEColumns
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Get_DEColumns 
(
       @parmSessionID			UNIQUEIDENTIFIER,						--WI 142843
	   @parmSiteBankID			INT, 
       @parmSiteClientAccountID INT,
       @parmDepositDate			DATETIME,
       @parmBatchID				BIGINT									--WI 142843
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/13/2009
*
* Purpose: Query Data Entry Summary fact table for batch DE Setup information.
*
* Modification History
* 03/20/2009 CR 25817  JPB	Created.
* 04/02/2013 WI 90678  JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_GetDataEntryFields.
*							Change all references:  Lockbox to ClientAccount.
*							Change parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
* 05/22/2014 WI 142843 PKW  Batch Collision - RAAM Integration.
* 10/24/2014 WI 172317 CMC  Filter inactive ClientAccount/DE associations.
* 01/20/2015 WI 184272 JSF  Related Items Section on transaction detail.
* 03/24/2015 WI 197805 MAA  added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead  
* 07/22/2015 WI 224443 MAA  Changed name and updated to support WorkgroupDataEntryColumns
* 08/10/2015 WI 228486 MAA  No longer returns inactive DE columns
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
	@StartDateKey INT,												--WI 142843
	@EndDateKey INT;												--WI 142843
	
BEGIN TRY
	
	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	;WITH DEList AS
	(
		SELECT	DISTINCT
			COALESCE(RecHubData.dimWorkgroupDataEntryColumns.UILabel, RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldTitle,
			RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldName,
			CASE
			   WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 THEN 'Checks'
			   ELSE 'Stubs'
			END AS 'TableName',
			CASE
				WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount') 
				THEN 7
				ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
			END AS FldDataTypeEnum,
			RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder
		FROM	
			RecHubData.factDataEntryDetails
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON 
				RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey			
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142843
				RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	
		WHERE  	
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142843		
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142843
			AND RecHubData.factDataEntryDetails.DepositDateKey = @EndDateKey								--WI 142843	 WI 197805					
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
	)
	SELECT
		FldTitle,
		FldName,
		TableName,
		FldDataTypeEnum		
	FROM
		DEList
	ORDER BY 
		ScreenOrder ASC
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH