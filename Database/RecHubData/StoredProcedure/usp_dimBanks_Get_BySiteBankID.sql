--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBanks_Get_BySiteBankID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBanks_Get_BySiteBankID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBanks_Get_BySiteBankID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBanks_Get_BySiteBankID 
(
	@parmSiteBankID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 01/15/2010
*
* Purpose: 
*
* Modification History
* 01/15/2010 CR 28716 JMC	Created
* 04/12/2013 WI 90619 JBS	Update to 2.0 release. Change schema name to RecHubData
*							Rename proc FROM usp_dimBanks_GetByID
*							Removed: Address table and all it's columns
*							Address, City, State, Zip
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		RecHubData.dimBanks.SiteBankID		AS BankID,
		RTRIM(RecHubData.dimBanks.BankName) AS BankName
	FROM
		RecHubData.dimBanks
	WHERE RecHubData.dimBanks.SiteBankID = @parmSiteBankID
		AND RecHubData.dimBanks.MostRecent = 1;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

