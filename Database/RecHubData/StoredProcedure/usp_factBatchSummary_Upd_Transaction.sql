--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Upd_Transaction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Upd_Transaction') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Upd_Transaction
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Upd_Transaction
(
	@parmDepositDateKey		INT,
	@parmBatchID			BIGINT,
	@parmModificationDate	DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 09/06/2013
*
* Purpose: Updates factBatchSummary record with values from related tables
*
* Modification History
* 09/06/2013 WI 112905 JBS	Created.
* 02/26/2015 WI 192471 JPB	FP from OLTA.usp_factBatchSummaryUpdateTransaction.
*							Replaced param list with R360 parameters.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @CheckCount			INT,
		@ScannedCheckCount	INT,
		@DocumentCount		INT,
		@StubCount			INT,
		@TransactionCount	INT,
		@CheckAmountTotal	MONEY,
		@StubAmountTotal	MONEY;

BEGIN TRY

	--Get the Counts/totals
	SELECT	
		@CheckCount = COUNT(*),
		@CheckAmountTotal = SUM(RecHubData.factChecks.Amount)
	FROM	
		RecHubData.factChecks
	WHERE	
		RecHubData.factChecks.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.IsDeleted = 0
	OPTION (RECOMPILE);

	SELECT	
		@StubCount = COUNT(*),
		@StubAmountTotal = SUM(RecHubData.factStubs.Amount)
	FROM	
		RecHubData.factStubs
	WHERE	
		RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.IsDeleted = 0
	OPTION (RECOMPILE);

	SELECT	
		@DocumentCount = COUNT(*)
	FROM	
		RecHubData.factDocuments
	WHERE	
		RecHubData.factDocuments.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factDocuments.BatchID = @parmBatchID
		AND RecHubData.factDocuments.IsDeleted = 0
	OPTION (RECOMPILE);

	SELECT	
		@ScannedCheckCount = COUNT(*)
	FROM	
		RecHubData.factDocuments
		INNER JOIN RecHubData.dimDocumentTypes ON RecHubData.dimDocumentTypes.DocumentTypeKey = RecHubData.factDocuments.DocumentTypeKey
	WHERE	
		RecHubData.factDocuments.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factDocuments.BatchID = @parmBatchID
		AND RecHubData.factDocuments.IsDeleted = 0
		AND RecHubData.dimDocumentTypes.FileDescriptor = 'SC'
	OPTION (RECOMPILE);

	SELECT	
		@TransactionCount = COUNT(*)
	FROM	
		RecHubData.factTransactionSummary
	WHERE	
		RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.IsDeleted = 0
	OPTION (RECOMPILE);

	/* Update the fact batch summary record */
	UPDATE	
		RecHubData.factBatchSummary
	SET 	
		RecHubData.factBatchSummary.CheckTotal = COALESCE(@CheckAmountTotal, 0),
		RecHubData.factBatchSummary.CheckCount = COALESCE(@CheckCount, 0),
		RecHubData.factBatchSummary.StubTotal = COALESCE(@StubAmountTotal, 0),
		RecHubData.factBatchSummary.StubCount = COALESCE(@StubCount, 0),
		RecHubData.factBatchSummary.DocumentCount = COALESCE(@DocumentCount, 0),
		RecHubData.factBatchSummary.ScannedCheckCount = COALESCE(@ScannedCheckCount, 0),
		RecHubData.factBatchSummary.TransactionCount = COALESCE(@TransactionCount, 0),
		RecHubData.factBatchSummary.ModificationDate = GETDATE()
	FROM	
		RecHubData.factBatchSummary
	WHERE	
		RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factBatchSummary.BatchID = @parmBatchID
		AND RecHubData.factBatchSummary.IsDeleted = 0
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorSeverity   INT,
			@ErrorState      INT;
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH
