--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDocumentImages_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDocumentImages_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDocumentImages_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDocumentImages_Get
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142845
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmImmutableDateKey		INT,
	@parmBatchID				BIGINT,
	@parmBatchSequence			INT,
	@parmDepositDateKey			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/26/2009
*
* Purpose: Get information about a Document image.
*
* Modification History
* 08/26/2009 CR 25817 JPB	Created.
* 11/14/2009 CR 28237 JPB	Added commands to set quoted identiifer and Arith 
*							Abort on.
* 11/18/2009 CR 28317 JPB	SP was not returning all rows from the XML column.
* 11/8/2011  CR 48022 WJS   Allow batch sequence to be passed it as negative 1. Passing in deposit key.
* 11/17/2011 CR 48411 WJS   Return batch Sequence and order by batchsequence.
* 11/20/2012 WI 70677 JPB	Added DepositDateKey and ProcessingDateKey to both WHERE.
* 11/20/2012 WI 69810 JPB	Added index hint.
* 05/20/2013 WI 90682 JBS	Update to 2.0 release.  Moved to RecHubData schema.
*							Renamed SP from usp_GetDocumentImageInfo.
*							Renamed parameters: @parmSiteLockboxID to @parmSiteClientAccountID,
*							@parmProcessingDateKey to @parmImmutableDateKey.
* 06/02/2014 WI 142825 PKW  Batch Collision.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE 
		@StartDateKey	INT,											--WI 142845
		@EndDateKey		INT;											--WI 142845

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */		--WI 142845
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays					--WI 142845
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateKey,					--WI 142845
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT
		RecHubData.factDocumentImages.FileSize,
		RecHubData.factDocumentImages.Side,
		RecHubData.factDocumentImages.PageNumber,
		RecHubData.dimImageTypes.ShortName,
		RecHubData.factDocumentImages.ExternalDocumentID,
		RecHubData.factDocumentImages.BatchSequence
	FROM
		RecHubData.factDocumentImages
		INNER JOIN RecHubData.dimImageTypes ON
			RecHubData.dimImageTypes.ImageTypeKey = RecHubData.factDocumentImages.ImageTypeKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142827
			RecHubData.factDocumentImages.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE
		RecHubData.factDocumentImages.DepositDateKey = @StartDateKey	--WI 142845
		AND RecHubData.factDocumentImages.IsDeleted = 0
		AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factDocumentImages.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factDocumentImages.BatchID = @parmBatchID
		AND RecHubData.factDocumentImages.BatchSequence = CASE WHEN @parmBatchSequence <> -1 THEN @parmBatchSequence ELSE RecHubData.factDocumentImages.BatchSequence END
		AND RecHubData.factDocumentImages.IsDeleted = 0
	ORDER BY RecHubData.factDocumentImages.BatchSequence;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH