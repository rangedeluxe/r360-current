--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimWorkgroupBusinessRules_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimWorkgroupBusinessRules_Get') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimWorkgroupBusinessRules_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimWorkgroupBusinessRules_Get
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 02/04/2019
*
* Purpose: Return information needed by the business rules engine.
*
* Modification History
* 02/04/2019 R360-15456 JPB  Created.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		PostDepositPayerRequired,
		'RemitterName' AS FieldName
	FROM 
		RecHubData.dimWorkgroupBusinessRules
	WHERE
		SiteBankID = @parmSiteBankID
		AND SiteClientAccountID = @parmSiteClientAccountID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH;
