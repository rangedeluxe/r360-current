--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ByTxnSequence
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_ByTxnSequence') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_ByTxnSequence
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_ByTxnSequence 
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142836
	@parmSiteBankID				INT, 
    @parmSiteClientAccountID	INT,
    @parmDepositDate			DATETIME,
    @parmBatchID				BIGINT,				--WI 142836
    @parmTxnSequence			INT
) 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved.  
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: Retrieve factCheck detail based on parameters (SiteBankID,  @parmSiteClientAccountID,
*							@parmDepositDate,  @parmBatchID, @parmTxnSequence)
*
* Modification History
* 03/23/2009 CR 25817 JMC	Created
* 11/16/2009 CR 28265 JMC	Added ProcessingDate to the returned dataset
* 11/19/2009 CR 28330 JMC	Added OnlineColorMode AS LockboxColorMode to the returned dataset
* 04/09/2012 CR 51910 JNE	Added BatchSiteCode to the returned dataset
* 04/02/2013 WI 90717 JBS	Update to 2.0 release.  Change schema
*							Rename proc from usp_GetTransactionChecksBySequence
*							Rename SiteCode to SiteCodeID.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Change all references: Lockbox to ClientAccount, Processing to Immutable
*							Remove: GlobalBatchID, GlobalCheckID columns
*									dimRemitters, dimDates  tables.
*							Adding: IsDeleted = 0
* 02/10/2014 WI 129042 EAS	Add Payment Type and Payment Source to result set
* 05/27/2014 WI 142836 DLD  Batch Collision/RAAM Integration.
* 07/21/2014 WI 154684 CEJ	Limit the results from RecHubData.usp_factChecks_Get_ByTxnSequence based on the SessionID
* 07/21/2014 WI 154685 CEJ	Limit the results from RecHubData.usp_factChecks_Get_ByTransactionID based on the SessionID
* 09/22/2014 WI	167271 SAS  Added Import Type Key and Import Type Short Name
* 10/24/2014 WI	174079 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175120 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176338 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
*							Removed BatchSourceName
* 05/27/2015 WI 215281 JBS  (FP 215006) Add BatchSequence to Order By
* 09/27/2016 WI R360-1087  MR   Updated to return same columns as
*                               usp_factChecks_Get_ByTransactionID
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDateKey INT,  --WI 142836
        @EndDateKey   INT;  --WI 142836

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ --WI 142836
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	RecHubData.factChecks.BatchID,
			RecHubData.factChecks.SourceBatchID,						                            --WI 142836
			RecHubUser.SessionClientAccountEntitlements.SiteBankID				AS BankID,			--WI 142836
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID		AS ClientAccountID,	--WI 142836
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.DepositDateKey), 112),101)	AS DepositDate,
			CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.ImmutableDateKey), 112),101)	AS ImmutableDate,
			RecHubData.factChecks.ImmutableDateKey AS PICSDate,
			RecHubData.factChecks.TransactionID,
			RecHubData.factChecks.TxnSequence,
			RecHubData.factChecks.BatchSequence,
			RTRIM(RecHubData.factChecks.RoutingNumber)			AS RT,
			RTRIM(RecHubData.factChecks.Account)				AS Account,
			RTRIM(RecHubData.factChecks.Serial)					AS Serial,
			RTRIM(RecHubData.factChecks.RemitterName)			AS RemitterName,
			RecHubData.factChecks.Amount,
			RecHubData.factChecks.CheckSequence,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.OnlineColorMode		AS ClientAccountColorMode,
		    RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.BatchSiteCode,
			RecHubData.factChecks.BatchNumber,
			RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType,
			RecHubData.dimBatchSources.ShortName AS PaymentSource,
			RecHubData.dimImportTypes.ImportTypeKey,
			RecHubData.dimDDAs.DDA,
			RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName AS ImportTypeShortName,
			RecHubData.factChecks.SourceProcessingDateKey
	FROM	RecHubData.factChecks
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142836
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factChecks.BatchPaymentTypeKey
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factChecks.BatchSourceKey
			INNER JOIN RecHubData.dimImportTypes ON RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
			LEFT OUTER JOIN RecHubData.dimDDAs ON RecHubData.dimDDAs.DDAKey = RecHubData.factchecks.DDAKey

	WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID  	
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142836
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142836
			AND RecHubData.factChecks.DepositDateKey = @StartDateKey										--WI 142836
			AND RecHubData.factChecks.BatchID = @parmBatchID
			AND RecHubData.factChecks.TxnSequence = @parmTxnSequence
			AND RecHubData.factChecks.IsDeleted = 0 
	ORDER BY 
			RecHubData.factChecks.BatchID ASC,
			RecHubData.factChecks.BatchSequence ASC, 
			RecHubData.factChecks.TxnSequence ASC,
			RecHubData.factChecks.SequenceWithinTransaction ASC
	OPTION ( RECOMPILE );

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
