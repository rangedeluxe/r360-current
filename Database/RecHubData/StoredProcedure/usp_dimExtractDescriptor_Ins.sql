--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimExtractDescriptor_Ins
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_dimExtractDescriptor_Ins') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_dimExtractDescriptor_Ins;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimExtractDescriptor_Ins
(
	@parmExtractDescriptor VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 05/30/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/03/2013 WI 103850 JMC   Initial Version
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	DECLARE @ExtractDescriptorCount INT;

	SELECT @ExtractDescriptorCount = COUNT(*)
	FROM RecHubData.dimExtractDescriptor
	WHERE ExtractDescriptor = @parmExtractDescriptor;

	IF @ExtractDescriptorCount = 0
	BEGIN
		INSERT INTO RecHubData.dimExtractDescriptor (ExtractDescriptor)
		SELECT @parmExtractDescriptor;
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
