--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_ElectronicAccounts_Get_ByDDA
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ElectronicAccounts_Get_ByDDA') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ElectronicAccounts_Get_ByDDA
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ElectronicAccounts_Get_ByDDA
(
	@parmABA	VARCHAR(10),
	@parmDDA	VARCHAR(35)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/08/2014
*
* Purpose: Select the ElectronicAccounts associated with a given ABA/DDA
*
* Modification History
* 04/08/2014 WI 135813 KLC	Created
* 09/12/2016 WI R360-80 MR  Fixed bug in selecting items with leading 0's on 
                            the DDA.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT	RecHubData.ElectronicAccounts.ElectronicAccountKey,
			RecHubData.ElectronicAccounts.DDAKey,
			RecHubData.ElectronicAccounts.SiteBankID,
			RecHubData.ElectronicAccounts.SiteClientAccountID
	FROM RecHubData.dimDDAs
		INNER JOIN RecHubData.ElectronicAccounts
			on RecHubData.dimDDAs.DDAKey = RecHubData.ElectronicAccounts.DDAKey
	WHERE	RecHubData.dimDDAs.ABA = @parmABA
		AND	substring(RecHubData.dimDDAs.DDA, patindex('%[^0 ]%', RecHubData.dimDDAs.DDA + '.'), len(RecHubData.dimDDAs.DDA))
		    = substring(@parmDDA, patindex('%[^ 0]%', @parmDDA + '.'), len(@parmDDA))
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

