--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ByDepositDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factChecks_Get_ByDepositDate') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factChecks_Get_ByDepositDate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_ByDepositDate 
(
       @parmSessionID			UNIQUEIDENTIFIER,			--WI 142833
	   @parmSiteBankID			INT, 
       @parmSiteClientAccountID INT,
       @parmDepositDate			DATETIME,
       @parmBatchID				BIGINT,
       @parmTransactionID		INT,
       @parmBatchSequence		INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/25/2009
*
* Purpose:  Get Check information based on SiteBankID, ClientAccountID, DepositDateKey 
*											BatchID, TransactionID, BatchSequence.
*
* Modification History
* 03/25/2009 CR 25817  JMC	Created.
* 11/16/2009 CR 28241  JMC	Added ProcessingDate to the returned dataset.
* 11/16/2009 CR 28315  JMC	Added OnlineColorMode AS LockboxColorMode to the returned dataset.
* 03/14/2012 CR 51241  WJS  Add BatchSiteCode.
* 06/21/2012 CR 53613  JNE  Add BatchCueID to result set.
* 07/20/2012 CR 54155  JNE	Add BatchNumber to result set.
* 04/01/2013 WI 90673  JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_GetCheck.
*							Rename SiteCode to SiteCodeID.
*							Change all references:  Lockbox to ClientAccount, Processing to Immutable.
*							Change parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Removed: Columns (GlobalBatchId, GlobalCheckID)
*									 Table   (dimRemitters, dimDates).
* 10/09/2013 WI 116456 CEJ	Fix PICS Date format in RecHubData.usp_factChecks_Get_ByDepositDate.
* 06/02/2014 WI 142833 PKW  Batch Collision.
* 09/17/2014 WI	166536 SAS  Changes done to add ImportTypeShortName.
* 10/24/2014 WI	174086 SAS	Changes done to pull ShortName from dimBatchSources table
* 10/30/2014 WI	175117 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when import type is integraPAY
* 11/07/2014 WI 176303 SAS  Add BatchSourceShortName and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
* 11/23/2016 #133847043 BLR Added the 'IsDeleted=0' clause to prevent incorrect dataset
*                           Showing up in the UI.
******************************************************************************/
SET NOCOUNT ON;

DECLARE																
		@StartDateKey	INT,										--WI 142833
		@EndDateKey		INT;										--WI 142833

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,					--WI 142833
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;
		
	SELECT	
		RecHubData.factChecks.BatchID,
		RecHubData.factChecks.SourceBatchID,						--WI 142833
		RecHubUser.SessionClientAccountEntitlements.SiteBankID				AS BankID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID		AS ClientAccountID,
		CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.DepositDateKey), 112),101)	AS DepositDate,
		CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10),factChecks.ImmutableDateKey), 112),101)	AS ImmutableDate,
		CONVERT(VARCHAR, RecHubData.factChecks.ImmutableDateKey, 112)		AS PICSDate,
		RecHubData.factChecks.TransactionID,
		RecHubData.factChecks.TxnSequence,
		RecHubData.factChecks.BatchSequence,
		RTRIM(RecHubData.factChecks.RoutingNumber)				AS RT,
		RTRIM(RecHubData.factChecks.Account)					AS Account,
		RTRIM(RecHubData.factChecks.Serial)						AS Serial,
		RTRIM(RecHubData.factChecks.RemitterName)				AS RemitterName,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.BatchSiteCode,
		RecHubData.dimClientAccounts.SiteCodeID,
		RecHubData.dimClientAccounts.OnlineColorMode			AS ClientAccountColorMode,
		RecHubData.factChecks.BatchCueID,
		RecHubData.factChecks.BatchNumber,
		RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName AS ImportTypeShortName
	FROM	
		RecHubData.factChecks
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON	--WI 142833
			RecHubData.factChecks.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
       	INNER JOIN RecHubData.dimClientAccounts ON 
			RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey		
		INNER JOIN RecHubData.dimBatchSources  ON 
			RecHubData.factChecks.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey		
		INNER JOIN RecHubData.dimImportTypes ON
			RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
    WHERE  	
			RecHubUser.SessionClientAccountEntitlements.SessionID  = @parmSessionID		--WI 142833
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID	--WI 142833
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142833
		AND RecHubData.factChecks.DepositDateKey = @StartDateKey
		AND RecHubData.factChecks.BatchID = @parmBatchID
		AND RecHubData.factChecks.TransactionID = @parmTransactionID
		AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
		AND RecHubData.factChecks.IsDeleted = 0
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH