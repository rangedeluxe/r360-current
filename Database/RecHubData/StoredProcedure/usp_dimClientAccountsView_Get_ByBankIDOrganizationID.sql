--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccountsView_Get_ByBankIDOrganizationID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccountsView_Get_ByBankIDOrganizationID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByBankIDOrganizationID
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccountsView_Get_ByBankIDOrganizationID
(
    @parmBankID				INT,
    @parmOrganizationID		INT,
    @parmOLOrganizationID	UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/27/2012
*
* Purpose: Return ClientAccount information for a Organization and bank
*
* Modification History
* 12/27/2012 WI 86236 TWE   Created by converting embedded SQL
*					  JBS	Update to 2.0 release. Change Schema Name
*							Change proc name from usp_dimLockboxesView_GetLockboxes_ByBankIDCustomerID.
*							Change all references from Lockbox to ClientAccount and 
*							Customer to Organization.
* 11/26/2013 WI 123460 EAS  Get Proper Account Display Label for LongName - Org Accounts
* 12/20/2013 WI 125816 JBS	Change construction of LongName to default to dim ClientAccount if Ol ClientAccount not present
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	;WITH cte_AssignedClientAccounts AS
    (
    SELECT 
        RecHubUser.OLClientAccounts.SiteClientAccountID,
        1      AS Assigned
    FROM 
        RecHubUser.OLClientAccounts 
    WHERE 
        RecHubUser.OLClientAccounts.SiteBankID = @parmBankID
        AND RecHubUser.OLClientAccounts.OLOrganizationID = @parmOLOrganizationID 
    )
    SELECT DISTINCT
        RecHubData.dimClientAccountsView.SiteBankID            AS BankID, 
        RecHubData.dimClientAccountsView.SiteOrganizationID    AS OrganizationID, 
        RecHubData.dimClientAccountsView.SiteClientAccountID   AS ClientAccountID, 
        RecHubData.dimClientAccountsView.ShortName,
        RecHubData.dimClientAccountsView.CutOff, 
        RecHubData.dimClientAccountsView.SiteCodeID, 
        COALESCE(cte_AssignedClientAccounts.Assigned,0)  AS Assigned,
        COALESCE(
                  CAST(RecHubUser.OLClientAccounts.SiteClientAccountID AS VARCHAR) + 
                  COALESCE(' (' + RTRIM(RecHubData.dimClientAccountsView.DDA) + ')', ' (N/A)') +
                  CASE
                        WHEN RecHubUser.OLClientAccounts.DisplayName IS NULL OR RecHubUser.OLClientAccounts.DisplayName = ''
                              THEN 
                                    CASE
                                          WHEN RecHubData.dimClientAccountsView.LongName IS NULL OR RecHubData.dimClientAccountsView.LongName = ''
                                                THEN ''
                                          ELSE
                                                ' - ' + RecHubData.dimClientAccountsView.LongName
                                    END
                        ELSE 
                              ' - ' + RecHubUser.OLClientAccounts.DisplayName
                  END,
                  CAST(RecHubData.dimClientAccountsView.SiteClientAccountID AS VARCHAR) + 
                  COALESCE(' (' + RTRIM(RecHubData.dimClientAccountsView.DDA) + ')', ' (N/A)', '') +
                  COALESCE(RecHubData.dimClientAccountsView.LongName, '')
            ) AS LongName
    FROM 
        RecHubData.dimClientAccountsView 
        LEFT OUTER JOIN cte_AssignedClientAccounts   ON 
            RecHubData.dimClientAccountsView.SiteClientAccountID = cte_AssignedClientAccounts.SiteClientAccountID
        LEFT OUTER JOIN RecHubUser.OLClientAccounts ON
            RecHubUser.OLClientAccounts.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID AND
            RecHubUser.OLClientAccounts.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
    WHERE 
        RecHubData.dimClientAccountsView.SiteBankID = @parmBankID
        AND RecHubData.dimClientAccountsView.SiteOrganizationID = @parmOrganizationID
    ORDER BY 
        RecHubData.dimClientAccountsView.SiteBankID, 
        RecHubData.dimClientAccountsView.SiteOrganizationID, 
        RecHubData.dimClientAccountsView.SiteClientAccountID;
    
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH