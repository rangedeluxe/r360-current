--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Get_BaseKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Get_BaseKeys') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Get_BaseKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Get_BaseKeys
(
	@parmSearchRequest RecHubData.InvoiceSearchSearchRequestTable READONLY,
	@parmClientAccounts RecHubData.InvoiceSearchClientAccountsTable READONLY
)
WITH RECOMPILE
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2017
*
* Purpose: Get the basic keys that match the search request
*
* Modification History
* 03/03/2017 #136913453 JPB Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY 

	;WITH BaseKeys AS
	(
		SELECT 
			RecHubData.factStubs.factStubKey,
			RecHubData.factStubs.DepositDateKey,
			RecHubData.factStubs.BatchSourceKey,
			RecHubData.factStubs.BatchPaymentTypeKey,
			ClientAccounts.EntityID,
			ClientAccounts.SiteBankID,
			ClientAccounts.SiteClientAccountID,
			ClientAccounts.SessionID
		FROM 
			@parmClientAccounts AS ClientAccounts
			INNER JOIN @parmSearchRequest AS SearchRequest ON SearchRequest.SessionID = ClientAccounts.SessionID
			INNER JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey >= SearchRequest.DepositDateFromKey
				AND RecHubData.factStubs.DepositDateKey <= SearchRequest.DepositDateToKey
				AND RecHubData.factStubs.ClientAccountKey = ClientAccounts.ClientAccountKey
		WHERE
			RecHubData.factStubs.IsDeleted = 0
	)
	SELECT 
		factStubKey,
		DepositDateKey,
		BaseKeys.BatchSourceKey,
		BaseKeys.BatchPaymentTypeKey,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		BaseKeys.SessionID
	FROM
		BaseKeys
		INNER JOIN @parmSearchRequest AS SearchRequest ON SearchRequest.SessionID = BaseKeys.SessionID
			AND BaseKeys.BatchSourceKey = CASE WHEN SearchRequest.BatchSourceKey = -1 THEN BaseKeys.BatchSourceKey ELSE SearchRequest.BatchSourceKey END
			AND BaseKeys.BatchPaymentTypeKey = CASE WHEN SearchRequest.BatchPaymentTypeKey = -1 THEN BaseKeys.BatchPaymentTypeKey ELSE SearchRequest.BatchPaymentTypeKey END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH