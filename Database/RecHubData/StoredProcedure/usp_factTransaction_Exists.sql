--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransaction_Exists
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransaction_Exists') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransaction_Exists
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransaction_Exists
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmSourceBatchID			BIGINT,
	@parmDepositDateKey			INT,
	@parmImmutableDateKey		INT,
	@parmBatchSource			VARCHAR(30),
	@parmTransactionID			INT = NULL,
	@parmTxnSequence			INT = NULL,
	@parmTransactionExists		BIT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2015
*
* Purpose: Test for the existance of a transaction. 
*
* Modification History
* 02/24/2015 WI 192828 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/* Set the default value to not exists */
	SET @parmTransactionExists = 0;

	SELECT
		@parmTransactionExists = 1
	FROM
		RecHubData.factTransactionSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
	WHERE
		RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
		AND RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factTransactionSummary.SourceBatchID = @parmSourceBatchID
		AND RecHubData.factTransactionSummary.TransactionID = CASE WHEN @parmTransactionID IS NULL THEN RecHubData.factTransactionSummary.TransactionID ELSE @parmTransactionID END
		AND RecHubData.factTransactionSummary.TxnSequence = CASE WHEN @parmTxnSequence IS NULL THEN RecHubData.factTransactionSummary.TxnSequence ELSE @parmTxnSequence END
		AND RecHubData.factTransactionSummary.IsDeleted = 0
		AND UPPER(RecHubData.dimBatchSources.ShortName) = UPPER(@parmBatchSource);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH