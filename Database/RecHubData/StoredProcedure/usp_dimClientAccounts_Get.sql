--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccount_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccount_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccount_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccount_Get 
(
	@parmClientAccountKey INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 02/22/2010
*
* Purpose: Query dimClientAccounts table by ClientAccountKey
*
* Modification History
* 02/22/2010 CR 29075 JMC	Created
* 04/15/2013 WI 90702 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_GetLockboxByLockboxKey
*							Reanme SiteCode to SiteCodeID.
*							Change references: Lockbox to ClientAccount, Customer to Organization.
*							Change Parameters: @parmLockboxKey to @parmClientAccountKey.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	RecHubData.dimClientAccounts.SiteBankID				AS BankID,
			RecHubData.dimClientAccounts.SiteOrganizationID		AS OrganizationID,
			RecHubData.dimClientAccounts.SiteClientAccountID	AS ClientAccountID,
			RecHubData.dimClientAccounts.ShortName,
			RecHubData.dimClientAccounts.SiteCodeID,
			RecHubData.dimClientAccounts.Cutoff,
			RecHubData.dimClientAccounts.LongName
	FROM	RecHubData.dimClientAccounts
	WHERE	RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

