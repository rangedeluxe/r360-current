--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factNotifications_MessageGroupNoAccessCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_MessageGroupNoAccessCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_MessageGroupNoAccessCount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_MessageGroupNoAccessCount
(
	@parmMessageGroup			INT,
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmNoAccessCount			INT = 0 OUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2018 Deluxe Corp. All rights reserved.  
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 03/05/2018
*
* Purpose: Determine if there are Notification Files that contain Workgroup information
*	for which the User does not have access rights.
*
*
* Modification History
* 03/05/2018 PT 154252232	MGE		Created
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE 
		@MessageWorkgroups UNIQUEIDENTIFIER;

	SELECT @MessageWorkgroups = MessageWorkgroups 
	FROM RecHubData.factNotifications
	WHERE NotificationMessageGroup = @parmMessageGroup and NotificationMessagePart =1
			
	SELECT @parmNoAccessCount = COUNT(*) 
	FROM RecHubData.factNotificationFiles 
	INNER JOIN RecHubData.dimClientAccounts 
		on RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factNotificationFiles.ClientAccountKey
	LEFT JOIN RecHubUser.SessionClientAccountEntitlements 
			ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
			AND SessionClientAccountEntitlements.SessionID = @parmSessionID
	WHERE MessageWorkgroups = @MessageWorkgroups
	AND IsDeleted = 0
	AND SessionID IS NULL;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException
END CATCH