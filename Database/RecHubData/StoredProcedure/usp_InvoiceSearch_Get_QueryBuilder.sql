--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Get_QueryBuilder
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Get_QueryBuilder') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Get_QueryBuilder
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Get_QueryBuilder
(
	@parmSearchRequest	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2017
*
* Purpose: Parse Invoice Search search request XML, query builder info
*
* Modification History
* 03/06/2017 #136913453 JPB Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @AccountNumber VARCHAR(80),
		@AmountFrom VARCHAR(64),
		@AmountTo VARCHAR(64);

DECLARE @QueryBuilder RecHubData.InvoiceSearchQueryBuilderTable;

BEGIN TRY 

	SELECT
		@AccountNumber = @parmSearchRequest.value('(/Root/Account)[1]','VARCHAR(80)'),
		@AmountFrom = @parmSearchRequest.value('(/Root/AmountFrom)[1]','VARCHAR(64)'),
		@AmountTo = @parmSearchRequest.value('(/Root/AmountTo)[1]','VARCHAR(64)');

	IF( @AmountFrom IS NOT NULL AND @AmountTo IS NOT NULL )
		INSERT INTO @QueryBuilder(FieldName,Operator,Value)
		SELECT 'Amount','=',@AmountFrom;
	ELSE IF( @AmountFrom IS NOT NULL )
		INSERT INTO @QueryBuilder(FieldName,Operator,Value)
		SELECT 'Amount','>=',@AmountFrom;
	ELSE IF( @AmountTo IS NOT NULL )
		INSERT INTO @QueryBuilder(FieldName,Operator,Value)
		SELECT 'Amount','<=',@AmountTo;

	IF( @AccountNumber IS NOT NULL )
		INSERT INTO @QueryBuilder(FieldName,Operator,Value)
		SELECT 'AccountNumber','LIKE','%' + @AccountNumber + '%';


	SELECT
		FieldName,
		Operator,
		Value
	FROM
		@QueryBuilder;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH