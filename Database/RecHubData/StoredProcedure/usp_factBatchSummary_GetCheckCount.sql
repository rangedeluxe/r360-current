--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_GetCheckCount
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_GetCheckCount') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_GetCheckCount
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_GetCheckCount 
(
	@parmSessionID			UNIQUEIDENTIFIER,  --WI 142828
	@parmSiteBankID			INT,
	@parmSiteClientAccountID	INT,
	@parmDepositDateStart		DATETIME,
	@parmDepositDateEnd		DATETIME,
	@parmPaymentTypeKey		INT = NULL,
	@parmPaymentSourceKey		INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table for Check Count for a specified 
*		   DepositDate.
*
* Modification History
* 03/17/2009 CR 25817 JMC	Created
* 03/20/2013 WI 90675 JBS	Update to 2.0 Release. Change Schema Name.
*							Change proc name from usp_GetCheckCount.
*							Rename Input variable: @parmSiteLockboxID to 
*							@parmSiteClientAccountID.
* 10/10/2013 WI 116694 EAS  Add Payment Type Filter to Check Count
* 05/28/2014 WI 142828 DLD  Batch Collision/RAAM Integration.
* 12/09/2014 WI 181268 LA   Added Batch Payment Source Parameter
******************************************************************************/
SET NOCOUNT ON 

DECLARE @StartDateKey INT,	-- WI 142828
        @EndDateKey INT;	-- WI 142828

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ -- WI 142828
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDateStart,
		@parmDepositDateEnd = @parmDepositDateEnd,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	SUM(COALESCE(RecHubData.factBatchSummary.CheckCount,0)) AS CheckCount
	FROM	RecHubData.factBatchSummary
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142828
	WHERE   RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID						--WI 142828	
		AND	RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142828
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142828
		AND RecHubData.factBatchSummary.DepositDateKey >= @StartDateKey									--WI 142828
		AND RecHubData.factBatchSummary.DepositDateKey <= @EndDateKey									--WI 142828
		AND RecHubData.factBatchSummary.DepositStatus >= 850
		AND RecHubData.factBatchSummary.IsDeleted = 0
		AND RecHubData.factBatchSummary.BatchPaymentTypeKey = ISNULL(@parmPaymentTypeKey, RecHubData.factBatchSummary.BatchPaymentTypeKey)
		AND RecHubData.factBatchSummary.BatchSourceKey = ISNULL(@parmPaymentSourceKey, RecHubData.factBatchSummary.BatchSourceKey)

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH
