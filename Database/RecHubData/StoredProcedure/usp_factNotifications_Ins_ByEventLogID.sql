--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factNotifications_Ins_ByEventLogID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_Ins_ByEventLogID') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_Ins_ByEventLogID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_Ins_ByEventLogID
(
	@parmEventLogID		INT = NULL,
	@parmUserName		VARCHAR(128),
	@parmNextNotificationMessageGroup  BIGINT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/30/2013
*
* Purpose: Inserts factNotification row using RecHubAlert.EventLogWorkTemp and 
*			the record ID passed in.  This is used by the SSIS package RecHubAlertsEventLog
*			Setting DateDefault to 01/01/1950 which will be the indication that the Email
*				Has not been sent.  The notification process will update the value when sent.
*
* Modification History
* 06/05/2013 WI 99901 JBS	Created
* 09/12/2014 WI 139598 JBS	Add UserID to each factNotification record as they are related 
*							to RecHubAlert.Alerts
* 01/27/2015 WI 186617 JBS	Add IsActive = 1 for Alerts
* 03/20/2015 WI 197089 TWE  set NotificationDateTime to UTC create datetime from eventlog
* 04/02/2015 WI 198847 JBS  Adding in logic to use RecHubSystem.NotificationMessageGroup
* 04/13/2105 WI	200428 JBS	Remove IsActive column reference.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @EventMessage			VARCHAR(MAX),
		@TodayDate				DATETIME,
		@utcTimeOffset          INT,
		@TodayDateKey			INT,
		@InitGUID				UNIQUEIDENTIFIER,
		@NextNotificationMessageGroup	BIGINT;

SET @TodayDate = GETDATE();
SET @TodayDateKey = CAST(CONVERT(VARCHAR,@TodayDate,112) AS INT);
SET @InitGUID = '00000000-0000-0000-0000-000000000000';
SET @utcTimeOffset = DATEDIFF(MINUTE, GETDATE(), GETUTCDATE());

BEGIN TRY      

	-- Load the full message which to parse through later before loading the factNotifications table
	SELECT 
		@EventMessage = RecHubAlert.EventLogWorkTemp.EventMessage
	FROM
		RecHubAlert.EventLogWorkTemp
	WHERE
		RecHubAlert.EventLogWorkTemp.EventLogID = @parmEventLogID;
	
	-- WI 139598
	-- Load temp table of ALL UserIDs that are set up to receive this Notification
	CREATE TABLE #Users
	( 
		UserID	INT,
		EventLogID INT
	);

	INSERT INTO #Users
	(	
		UserID,
		EventLogID
	)
	SELECT 
		RecHubUser.Users.UserID,
		@parmEventLogID 
	FROM 
		RecHubUser.Users 
		INNER JOIN RecHubAlert.Alerts
			ON RecHubUser.Users.UserID = RecHubAlert.Alerts.UserID
		INNER JOIN RecHubAlert.EventLog
			ON RecHubAlert.EventLog.AlertID = RecHubAlert.Alerts.AlertID
		INNER JOIN RecHubAlert.EventRules
			ON RecHubAlert.EventRules.EventRuleID = RecHubAlert.EventLog.EventRuleID
		INNER JOIN RecHubAlert.[Events]
			ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
	WHERE 
		RecHubAlert.EventLog.EventLogID = @parmEventLogID AND
		RecHubAlert.Alerts.IsActive = 1 AND 
		RecHubAlert.[Events].IsActive = 1;

	-- Get value for next message Using the proper SEQUENCE
	SET @NextNotificationMessageGroup = NEXT VALUE FOR RecHubSystem.NotificationMessageGroup ;

	/* Parse @parmAuditMessage into 128 byte lengths */
	;WITH EventMessage_CTE(EventLogID, StartingPosition, EndingPosition, Occurence)
	AS
	(		
		SELECT	
			EventLogID = @parmEventLogID ,	
			StartingPosition = 1,
			EndingPosition = 128,
			1 AS Occurence
		UNION ALL
		SELECT 		
			EventLogID = @parmEventLogID ,
			StartingPosition = EndingPosition + 1,
			EndingPosition = 
				CASE 
					WHEN EndingPosition <= LEN(@EventMessage) + EndingPosition + 128 
						THEN EndingPosition + 128 
						ELSE EndingPosition + CAST(LEN(@EventMessage) AS INT) 
					END,
			Occurence + 1
		FROM 
			EventMessage_CTE
		WHERE 
			EndingPosition + 1 <= LEN(@EventMessage)
	)
	INSERT INTO RecHubData.factNotifications
		(
			IsDeleted,
			NotificationMessageGroup,
			UserNotification,
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			UserID,
			NotificationDateKey,
			NotificationSourceKey,
			NotificationFileCount,
			NotificationMessagePart,
			NotificationDateTime,
			CreationDate,
			ModificationDate,
			SourceNotificationID,
			CreatedBy,
			ModifiedBy,
			MessageText
		)
	SELECT	
		0, /* IsDelete set to 0 on insert */
		@NextNotificationMessageGroup,   
		1,
		RecHubAlert.EventLogWorkTemp.BankKey,	
		RecHubAlert.EventLogWorkTemp.OrganizationKey,	
		RecHubAlert.EventLogWorkTemp.ClientAccountKey,	
		UserID,	
		@TodayDateKey,	
		RecHubAlert.EventLogWorkTemp.EventSourceKey,
		0,			-- File Count is 0
		EventMessage_CTE.Occurence,	
		DATEADD(minute, @utcTimeOffset, RecHubAlert.EventLogWorkTemp.CreationDate) as NotificationDate,			
		@TodayDate,			
		@TodayDate,			
		@InitGUID,    
		@parmUserName,    
		@parmUserName,    
		SUBSTRING(@EventMessage, StartingPosition, EndingPosition-StartingPosition+1)
	FROM	
		EventMessage_CTE 
			INNER JOIN RecHubAlert.EventLogWorkTemp 
				ON EventMessage_CTE.EventLogID = RecHubAlert.EventLogWorkTemp.EventLogID
			INNER JOIN #Users
				ON EventMessage_CTE.EventLogID = #Users.EventLogID
	WHERE
			RecHubAlert.EventLogWorkTemp.EventLogID = @parmEventLogID
	OPTION (MaxRecursion 0);

	-- Set group number to use in the SSIS package
	SET @parmNextNotificationMessageGroup = @NextNotificationMessageGroup;

	IF OBJECT_ID('tempdb..#Users') IS NOT NULL
        DROP TABLE #Users;
END TRY

BEGIN CATCH
    IF OBJECT_ID('tempdb..#Users') IS NOT NULL
        DROP TABLE #Users;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
