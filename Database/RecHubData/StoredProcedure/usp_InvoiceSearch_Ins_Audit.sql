--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_InvoiceSearch_Ins_Audit
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_InvoiceSearch_Ins_Audit') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_InvoiceSearch_Ins_Audit
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_InvoiceSearch_Ins_Audit
(
	@parmSearchRequest RecHubData.InvoiceSearchSearchRequestTable READONLY,
	@parmQueryBuilder RecHubData.InvoiceSearchQueryBuilderTable READONLY,
	@parmClientAccounts RecHubData.InvoiceSearchClientAccountsTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2017
*
* Purpose: Audit search request
*
* Modification History
* 03/07/2017 #136915689 JPB Created
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @UserID INT,
		@AuditMessage VARCHAR(MAX),
		@QueryBuilderMessage VARCHAR(MAX);

BEGIN TRY 
	SELECT 
		@AuditMessage = UserName + ' viewed the Invoice Search results for '
	FROM 
		@parmSearchRequest SearchRequest;

	IF( (SELECT COUNT(*) FROM @parmSearchRequest WHERE EntityName IS NOT NULL) > 0 )
	BEGIN
		SELECT TOP(1)
			@AuditMessage += 'Entity: ' + EntityName
		FROM
			@parmSearchRequest SearchRequest
	END
	ELSE
	BEGIN
		SELECT TOP(1)
			@AuditMessage += 'Bank: ' + 
			CAST(RecHubData.dimBanksView.SiteBankID AS VARCHAR(10)) + ' - ' + RecHubData.dimBanksView.BankName + 
			', Workgroup: ' + CAST(RecHubData.dimClientAccountsView.SiteClientAccountID AS VARCHAR(10)) + ' ' + RecHubData.dimClientAccountsView.LongName
		FROM 
			@parmSearchRequest SearchRequest
			INNER JOIN @parmClientAccounts ClientAccounts ON ClientAccounts.SessionID = SearchRequest.SessionID
			INNER JOIN RecHubData.dimBanksView ON RecHubData.dimBanksView.SiteBankID = ClientAccounts.SiteBankID
			INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = ClientAccounts.SiteBankID
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = ClientAccounts.SiteClientAccountID
	END

	SELECT 
		@AuditMessage +=  
		' from ' + CONVERT(VARCHAR(10),DepositDateFrom,101) + 
		' to ' + CONVERT(VARCHAR(10),DepositDateTo,101) + 
		', for Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,'All') +
		', and Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,'All') +
		'.'
	FROM 
		@parmSearchRequest SearchRequest
		LEFT JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = SearchRequest.BatchSourceKey
		LEFT JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = SearchRequest.BatchPaymentTypeKey;


	IF EXISTS( SELECT 1 FROM @parmQueryBuilder )
	BEGIN
		SET @QueryBuilderMessage = ' Search criteria included: ';

		SELECT 
			@QueryBuilderMessage += 
			FieldName + ' ' +
			CASE FieldName
				WHEN 'Amount' THEN
					CASE Operator
						WHEN '=' THEN 'equals '
						WHEN '>=' THEN 'is greater then '
						WHEN '<=' THEN 'is less then '
					END
				WHEN 'AccountNumber' THEN 'containing '
			END +
			CASE Operator
				WHEN 'LIKE' THEN REPLACE(value,'%','')
				ELSE value
			END + ', and '
		FROM
			@parmQueryBuilder;
		SET @AuditMessage = @AuditMessage + SUBSTRING(@QueryBuilderMessage,1,LEN(@QueryBuilderMessage)-5)+'.';
	END		

	SELECT
		@UserID = UserID
	FROM
		@parmSearchRequest SearchRequest
		INNER JOIN RecHubUser.[Session] ON RecHubUser.[Session].SessionID = SearchRequest.SessionID; 

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins 
		@parmApplicationName='Invoice Search', 
		@parmEventName='Viewed Page', 
		@parmUserID = @UserID, 
		@parmAuditMessage = @AuditMessage;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH