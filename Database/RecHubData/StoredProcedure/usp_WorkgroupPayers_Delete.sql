--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_WorkgroupPayers_Delete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_WorkgroupPayers_Delete') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_WorkgroupPayers_Delete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_WorkgroupPayers_Delete
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,	
	@parmRoutingNumber			NVARCHAR(30),
	@parmAccount				NVARCHAR(30),
	@parmUserId					INT
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MSV
* Date: 05/20/2019
*
* Purpose: Deletes a WorkgroupPayers record
*
* Modification History
* 05/20/2019 CES-4331 MSV Created
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction BIT = 0,
		@RowsReturned INT  = 0;

BEGIN TRY

	DECLARE @errorDescription VARCHAR(1024);
	DECLARE @auditMessage VARCHAR(MAX);
	DECLARE @curTime DATETIME = GETDATE();
	DECLARE @WorkgroupPayerKey INT = 0;
    DECLARE @WorkgroupPayerName VARCHAR(128);

	-- Verify User ID
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to delete RecHubData.WorkgroupPayers record, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ') does not exist.';
		RAISERROR(@errorDescription, 16, 1);
	END

	IF @@TRANCOUNT = 0 
		BEGIN 
			BEGIN TRANSACTION;
			SET @LocalTransaction = 1;
		END

    SET @WorkgroupPayerName = '';

	SELECT TOP 1 @WorkgroupPayerKey = RecHubData.WorkgroupPayers.WorkgroupPayerKey, @WorkgroupPayerName = RecHubData.WorkgroupPayers.PayerName
	FROM RecHubData.WorkgroupPayers
	WHERE
		(RecHubData.WorkgroupPayers.SiteBankID = @parmSiteBankID)
		AND (RecHubData.WorkgroupPayers.SiteClientAccountID = @parmSiteClientAccountID)
		AND (RecHubData.WorkgroupPayers.RoutingNumber= @parmRoutingNumber)
		AND (RecHubData.WorkgroupPayers.Account = @parmAccount);

	DELETE FROM
		RecHubData.WorkgroupPayers
	WHERE
		(RecHubData.WorkgroupPayers.SiteBankID = @parmSiteBankID)
		AND (RecHubData.WorkgroupPayers.SiteClientAccountID = @parmSiteClientAccountID)
		AND (RecHubData.WorkgroupPayers.RoutingNumber= @parmRoutingNumber)
		AND (RecHubData.WorkgroupPayers.Account = @parmAccount);

	SELECT @RowsReturned = @@ROWCOUNT;

	-- Audit the RecHubData.WorkgroupPayers delete
	SET @auditMessage = 'Deleted workgroup payer for BankId=' + CAST(@parmSiteBankID AS VARCHAR(10))
		+ ', WorkgroupId=' + CAST(@parmSiteClientAccountID AS VARCHAR(30))
		+ ', Routing Number=' + @parmRoutingNumber
		+ ', Account=' + @parmAccount
        + ', Payer Name=' + @WorkgroupPayerName
		+ '.';

	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubData.usp_WorkgroupPayers_Delete',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'WorkgroupPayers',
			@parmColumnName			= 'WorkgroupPayerKey',
			@parmAuditValue			= @WorkgroupPayerKey,
			@parmAuditType			= 'DEL',
			@parmAuditMessage		= @auditMessage;

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH