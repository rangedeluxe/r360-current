--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factNotifications_ValidateUserAccess
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factNotifications_ValidateUserAccess') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factNotifications_ValidateUserAccess
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factNotifications_ValidateUserAccess
(
	@parmSessionID					UNIQUEIDENTIFIER,
	@parmNotificationMessageGroup	INT,
	@parmHasAccess					INT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/04/2012
*
* Purpose: 
*
* Modification History
* 09/04/2012 CR 55379 JMC	Created
* 05/02/2013 WI 99166 JBS	Update to 2.0 release. Change schema to RecHubData
*							Change references: Lockbox to ClientAccount
*							Customer to Organization
* 10/14/2013 WI 99166 JMC	Corrected schema: RecHubData.OLClientAccounts --> 
*                                             RecHubUser.OLClientAccounts
* 12/10/2013 WI 124781 JBS	Corrected Schema  RecHubData.OLClientAccountUsers --> 
*                                             RecHubUser.OLClientAccountUsers
* 11/19/2014 WI 178498 JBS	Change to use SessionID to verify access.
* 02/18/2015 WI 191065 CMC	Corrected to use a valid temporary table object.
* 03/26/2015 WI 191889 CMC	Retrieving the actual user notifications.
******************************************************************************/

SET NOCOUNT ON;

BEGIN TRY

	/****************************************************************************
	* A SuperUser has access to all ClientAccounts that belong to an OLOrganization.
	* Non-SuperUsers have access to only those rows for which OLUserClientAccount 
	* rows exist.  THIS is only preserved in case we need to come back in add Super User Logic
	****************************************************************************/
	DECLARE @IsSuperUser BIT;

	SET @IsSuperUser = 1;   -- we are going to default to 1 so all are until we get further definition
							-- how to differentiate this permission

	IF OBJECT_ID('tempdb..#tmpSessionClientAccountList') IS NOT NULL
        DROP TABLE #tmpSessionClientAccountList;

	CREATE TABLE #tmpSessionClientAccountList
	(
		SessionID			UNIQUEIDENTIFIER,
		ClientAccountKey	INT,
		BankID				INT,
		ClientAccountID		INT
	);

	--  We are not filtering on any ClientAccounts. We will build with ALL rows matching SessionID
	INSERT INTO #tmpSessionClientAccountList
	(
		SessionID,
		ClientAccountKey,
		BankID,
		ClientAccountID		
	)
	SELECT DISTINCT
		SessionID,
		ClientAccountKey,
		SiteBankID,	
		SiteClientAccountID
	FROM 
		RecHubUser.SessionClientAccountEntitlements
	WHERE	
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;


	/* Finding any ClientAccount Level Notifications Assigned to User and all others unassigned */
	
	BEGIN
		IF EXISTS(SELECT RecHubData.factNotifications.NotificationMessageGroup
					FROM 
						RecHubData.factNotifications
						INNER JOIN RecHubData.dimOrganizations ON 
							RecHubData.factNotifications.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
						INNER JOIN RecHubData.dimClientAccounts 
							INNER JOIN RecHubUser.OLWorkgroups ON      
								RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID 
								AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID ON 
								RecHubData.factNotifications.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
						INNER JOIN #tmpSessionClientAccountList	ON							
								RecHubData.dimClientAccounts.ClientAccountKey = #tmpSessionClientAccountList.ClientAccountKey 
						INNER JOIN RecHubUser.[Session] ON 
								#tmpSessionClientAccountList.SessionID = RecHubUser.[Session].SessionID
					WHERE	#tmpSessionClientAccountList.SessionID = @parmSessionID	
						AND ((RecHubData.factNotifications.UserID = RecHubUser.[Session].UserID AND
							RecHubData.factNotifications.UserNotification = 1 ) OR
							(RecHubData.factNotifications.UserNotification = 0))
						AND RecHubData.factNotifications.NotificationMessagePart = 1
						AND RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
						AND RecHubData.factNotifications.IsDeleted = 0)
			SET @parmHasAccess = 1
		ELSE 
			SET @parmHasAccess = 0;
	END
	
	IF @parmHasAccess = 0
		BEGIN
		/* Finding any Organization Level Notifications. Returning these used to be reserved for SuperUser access  */
			CREATE TABLE #TmpOrganizations
			(	OrganizationKey INT );

			INSERT INTO #TmpOrganizations
			(	
				OrganizationKey 
			)
			SELECT 
				RecHubData.dimOrganizations.OrganizationKey 
			FROM 
				RecHubData.dimOrganizations
				INNER JOIN RecHubData.dimClientAccounts 
				INNER JOIN RecHubUser.OLWorkgroups 
					ON  RecHubData.dimClientAccounts.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID 
						and RecHubData.dimClientAccounts.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID 
					ON 	RecHubData.dimOrganizations.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
						and RecHubData.dimOrganizations.SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID
				INNER JOIN #tmpSessionClientAccountList	ON							 
						RecHubData.dimClientAccounts.ClientAccountKey = #tmpSessionClientAccountList.ClientAccountKey 
				WHERE 
					#tmpSessionClientAccountList.SessionID = @parmSessionID;
			 
			IF EXISTS(SELECT RecHubData.factNotifications.NotificationMessageGroup
						FROM 
							RecHubData.factNotifications
							INNER JOIN #TmpOrganizations ON RecHubData.factNotifications.OrganizationKey = #TmpOrganizations.OrganizationKey
						WHERE 
							RecHubData.factNotifications.ClientAccountKey IS NULL OR RecHubData.factNotifications.ClientAccountKey < 1
							AND RecHubData.factNotifications.NotificationMessagePart = 1
							AND RecHubData.factNotifications.NotificationMessageGroup = @parmNotificationMessageGroup
							AND RecHubData.factNotifications.IsDeleted = 0)
				SET @parmHasAccess = 1;
			
		END

		IF @parmHasAccess = 0 --if still no access, let's look for alert notifications
		BEGIN
		
			IF EXISTS
				(		
					SELECT 
						RecHubData.factNotifications.factNotificationKey
					FROM 
						RecHubData.factNotifications
						INNER JOIN RecHubUser.[Session]
							ON RecHubUser.[Session].UserID = RecHubData.factNotifications.UserID
					WHERE 
						NotificationMessageGroup = @parmNotificationMessageGroup
						AND RecHubData.factNotifications.IsDeleted = 0
						AND RecHubUser.[Session].SessionID = @parmSessionID
						-- I don't like having to put the following filters on, because if we ever change the alerts so that they don't insert
						-- -1 values, then this proc is broken.  The specific session and notification message group should be enough here.
						AND (RecHubData.factNotifications.ClientAccountKey IS NULL OR RecHubData.factNotifications.ClientAccountKey < 1)
						AND (RecHubData.factNotifications.BankKey IS NULL OR RecHubData.factNotifications.BankKey < 1)
						AND (RecHubData.factNotifications.OrganizationKey IS NULL OR RecHubData.factNotifications.OrganizationKey < 1)
				)
				SET @parmHasAccess = 1;
		END
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH