--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factItemData_Upd_DataValue
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factItemData_Upd_DataValue') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factItemData_Upd_DataValue
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factItemData_Upd_DataValue
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT, 
	@parmImmutableDateKey		INT,
	@parmBatchID				BIGINT,
	@parmBatchSequence			INT,
	@parmItemDataSetupFieldKey	INT,
	@parmDataValue				VARCHAR(256),
	@parmDataValueDateTime		DATETIME = Null,
	@parmDataValueMoney 		MONEY    = Null,
	@parmDataValueInteger		INT      = Null,
	@parmDataValueBit			BIT      = Null
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR 33802 JMC	Created
* 07/24/2014 WI 155679 CMC	Update to 2.0 release.  Change schema to RecHubData
*							Rename proc from usp_factItemDataAppend
*							Change references of Lockbox to ClientAccount,
*							Processing to Immutable.
* 10/14/2014 WI 172006 TWE  Add back fields lost during translation.
*                           DataValue: DateTime,Money,Integer,Bit
* 07/17/2015 WI 224690 JBS  Add BatchSourceKey to insert using existing value
******************************************************************************/

DECLARE @BankKey			INT,
        @ClientAccountKey	INT,
        @DepositDateKey		INT,
		@factItemDataKey    BIGINT,
        @ModificationDate   DateTime,
		@ModifiedBy         VARCHAR(128);

BEGIN TRY

	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT;

	SET @ModificationDate = GETDATE();
	SET @ModifiedBy = SUSER_SNAME();

	/* Find out what the key of the record is that will be replaced */
	SELECT
	    @factItemDataKey = RecHubData.factItemData.factItemDataKey
	FROM
	    RecHubData.factItemData
	WHERE
	    RecHubData.factItemData.IsDeleted = 0
	    AND RecHubData.factItemData.DepositDateKey = @DepositDateKey
		AND RecHubData.factItemData.ClientAccountKey = @ClientAccountKey
		AND RecHubData.factItemData.ImmutableDateKey = @parmImmutableDateKey
		AND RecHubData.factItemData.BatchID = @parmBatchID
		AND RecHubData.factItemData.BatchSequence = @parmBatchSequence
		AND RecHubData.factItemData.ItemDataSetupFieldKey = @parmItemDataSetupFieldKey
	OPTION (RECOMPILE);

	if @factItemDataKey IS NULL
	BEGIN
		RAISERROR('Item Data Not Found',16,1);
	END

	/* Now copy over the original record modify new column data */
	INSERT INTO RecHubData.factItemData
	(
        IsDeleted
        ,BankKey
        ,OrganizationKey
        ,ClientAccountKey
        ,DepositDateKey
        ,ImmutableDateKey
        ,SourceProcessingDateKey
        ,BatchID
        ,SourceBatchID
        ,BatchNumber
		,BatchSourceKey
        ,TransactionID
        ,BatchSequence
        ,ItemDataSetupFieldKey
        ,CreationDate
        ,ModificationDate
        ,DataValueDateTime
        ,DataValueMoney
        ,DataValueInteger
        ,DataValueBit
        ,ModifiedBy
        ,DataValue
	)
	SELECT
        RecHubData.factItemData.IsDeleted
        ,RecHubData.factItemData.BankKey
        ,RecHubData.factItemData.OrganizationKey
        ,RecHubData.factItemData.ClientAccountKey
        ,RecHubData.factItemData.DepositDateKey
        ,RecHubData.factItemData.ImmutableDateKey
        ,RecHubData.factItemData.SourceProcessingDateKey
        ,RecHubData.factItemData.BatchID
        ,RecHubData.factItemData.SourceBatchID
        ,RecHubData.factItemData.BatchNumber
		,RecHubData.factItemData.BatchSourceKey
        ,RecHubData.factItemData.TransactionID
        ,RecHubData.factItemData.BatchSequence
        ,RecHubData.factItemData.ItemDataSetupFieldKey
        ,RecHubData.factItemData.CreationDate
        ,@ModificationDate              AS ModificationDate
        ,@parmDataValueDateTime         AS DataValueDateTime
        ,@parmDataValueMoney            AS DataValueMoney
        ,@parmDataValueInteger          AS DataValueInteger
        ,@parmDataValueBit              AS DataValueBit
        ,@ModifiedBy                    AS ModifiedBy
        ,@parmDataValue                 AS DataValue
	FROM
	    RecHubData.factItemData
	WHERE
	    RecHubData.factItemData.factItemDataKey = @factItemDataKey;

    /* Now flag the original record as deleted  */
	UPDATE 	
	    RecHubData.factItemData
	SET 
		IsDeleted = 1,
		ModifiedBy = @ModifiedBy,
		ModificationDate = @ModificationDate
	FROM
	 	RecHubData.factItemData 
	WHERE	
	    RecHubData.factItemData.factItemDataKey = @factItemDataKey;
	
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE()

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine)
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException 
END CATCH
