--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ConvertSourceKeysToRecHubKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_ConvertSourceKeysToRecHubKeys') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_ConvertSourceKeysToRecHubKeys
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_ConvertSourceKeysToRecHubKeys
(
       @XBatch						XML,
       @parmBankKey					INT OUT,
       @parmOrganizationKey			INT OUT,
       @parmClientAccountKey		INT OUT,
       @parmImmutableDateKey		INT OUT,
       @parmDepositDateKey			INT OUT,
       @parmSourceProcessingDateKey INT OUT,
	   @parmBatchSourceKey			SMALLINT = 0 OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/01/2009
*
* Purpose: Convert the DBO site value to the OLTA key values
*
* Modification History
* 07/01/2009 CR 25817 JPB	Created
* 02/08/2010 CR 28941 JPB	Updated to left joins on the dates table to support
*								missing proc date or missing desposit date
* 02/26/2010 CR 28823 JPB	Added a check against the factBatchSummary table to 
*								make sure the correct keys are returned for existing
*								batches.
* 04/14/2010 CR 29366 JPB	Updated to account for ICON batches. (ICON batches 
*								do not have a deposit date included.)
* 06/16/2010 CR 29954 JPB	Account for missing DepositDate from any input time.
* 09/30/2010 CR 31208 JPB	Updated to account for missing SiteCode, use views
*								instead of dim tables directly.
* 01/11/2011 CR 32313 JPB	Added BatchSourceKey lookup.
* 12/01/2011 CR 48535 JPB	Removed join to dimDates to improve performance.
*							(FP 48147)
* 01/23/2012 CR 49568 JPB	Added SourceProcessingDate.
* 08/02/2012 CR 54731 JPB	Account for a NULL SourceProcessingDate.
* 04/17/2013 WI 90595 JBS	Update to 2.0 release. Change schema to RecHubData
*							Rename proc FROM usp_ConvertDBOKeysToOLTAKeys	
*							Change Parameters: @parmCustomerKey TO @parmOrganizationKey,
*						      @parmLockboxKey TO @parmClientAccountKey,
*						      @parmProcessingDateKey TO @parmImmutableDateKey.
*							Change references: Processing to Immutable,
*							Lockbox to ClientAccount, Customer to Organization.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Removed: GlobalBatchID  references
*							Foward Patch: WI 85063
* 08/22/2016 PT 128448991	JPB	Changed @parmBatchSourceKey to SMALLINT. 
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
SET XACT_ABORT ON;

DECLARE 
		--@iGlobalBatchID INT,
		@iBankID				INT,
		@iOrganizationID		INT,
		@iClientAccountID		INT,
		@iBatchID				INT,
		@DepositDate			DATETIME,	--CR 48535
		@ImmutableDate			DATETIME,	--CR 48535
		@SourceProcessingDate	DATETIME;

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Batch')) 
              DROP TABLE #Batch;

	--FP: WI 85063  Create table prior to main process so the indexes can optimize not based on the Variables.
	CREATE TABLE #Batch
	(
		BankID				INT,
		ClientAccountID		INT,
		SiteCodeID			INT,
		OrganizationID		INT,
		ImmutableDate		DATETIME,
		DepositDate			DATETIME,
		SourceProcessingDate DATETIME,
		BatchSource			VARCHAR(30),
		BatchID				INT
	);

	INSERT INTO #Batch 
	SELECT	Batch.att.value('@BankID','int'),
			Batch.att.value('@ClientAccountID','int'),
			Batch.att.value('@SiteID','int'),
			Batch.att.value('@OrganizationID','int'),
			Batch.att.value('@ImmutableDate','datetime'),
			Batch.att.value('@DepositDate','datetime'),
			Batch.att.value('@SourceProcessingDate','datetime'),
			Batch.att.value('@BatchSource','varchar(30)'),
			Batch.att.value('@BatchID','int')
	FROM	@XBatch.nodes('/Batches/Batch') AS Batch(att);

	SELECT	@parmBankKey = RecHubData.dimBanksView.BankKey,
			@parmOrganizationKey = RecHubData.dimOrganizationsView.OrganizationKey,
			@parmClientAccountKey = RecHubData.dimClientAccountsView.ClientAccountKey,
			@ImmutableDate = #Batch.ImmutableDate,
			@DepositDate = #Batch.DepositDate,
			@SourceProcessingDate = #Batch.SourceProcessingDate,
			@parmBatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey,
			@iBankID = #Batch.BankID,
			@iOrganizationID = #Batch.OrganizationID,
			@iClientAccountID = #Batch.ClientAccountID,
			@iBatchID = #Batch.BatchID
	FROM	#Batch 
			INNER JOIN RecHubData.dimBanksView ON RecHubData.dimBanksView.SiteBankID = #Batch.BankID
			LEFT JOIN RecHubData.dimOrganizationsView ON RecHubData.dimOrganizationsView.SiteBankID = #Batch.BankID
							AND RecHubData.dimOrganizationsView.SiteOrganizationID = #Batch.OrganizationID
			INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.SiteBankID = #Batch.BankID 
							AND RecHubData.dimClientAccountsView.SiteClientAccountID = #Batch.ClientAccountID
			LEFT JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.ShortName = #Batch.BatchSource; 

	--FP WI 85063 This assigns variables from input values without table lookup
	SELECT @parmImmutableDateKey = ISNULL(CAST(CONVERT(VARCHAR,@ImmutableDate,112) AS INT), 0),
			@parmDepositDateKey  = CAST(CONVERT(VARCHAR,@DepositDate,112) AS INT);

	IF @SourceProcessingDate IS NOT NULL
		SELECT @parmSourceProcessingDateKey = CAST(CONVERT(VARCHAR,@SourceProcessingDate,112) AS INT)
	ELSE SET @parmSourceProcessingDateKey = NULL;
	
	IF @parmBatchSourceKey IS NULL
		SELECT @parmBatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey FROM RecHubData.dimBatchSources WHERE ShortName = 'Unknown';

	--CR 29954 deposit date not provided, so look it up accounting for changing keys
	--FP WI 85063 Added #Batch table joining to use columns(replacing previous variables) for joining logic.
	IF @parmDepositDateKey IS NULL
		SELECT	@parmDepositDateKey = RecHubData.factBatchSummary.DepositDateKey,
				@parmSourceProcessingDateKey = COALESCE(@parmSourceProcessingDateKey,RecHubData.factBatchSummary.SourceProcessingDateKey)
		FROM	RecHubData.factBatchSummary
				INNER JOIN #Batch ON RecHubData.factBatchSummary.BatchID = #Batch.BatchID
				INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
				INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		WHERE	RecHubData.dimBanks.SiteBankID = #Batch.BankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = #Batch.ClientAccountID
				AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
				AND RecHubData.factBatchSummary.SourceProcessingDateKey = CASE WHEN @parmSourceProcessingDateKey IS NULL THEN SourceProcessingDateKey ELSE @parmSourceProcessingDateKey END;
				-- AND OLTA.factBatchSummary.BatchID = @iBatchID  
		
	--if this batch exists, use the keys from batch	
	SELECT	@parmBankKey = COALESCE(RecHubData.dimBanks.BankKey,@parmBankKey),
			@parmOrganizationKey = COALESCE(RecHubData.dimOrganizations.OrganizationKey,@parmOrganizationKey),
			@parmClientAccountKey = COALESCE(RecHubData.dimClientAccounts.ClientAccountKey,@parmClientAccountKey)
	FROM	RecHubData.factBatchSummary
			INNER JOIN RecHubData.dimBanks ON RecHubData.factBatchSummary.BankKey = RecHubData.dimBanks.BankKey
			INNER JOIN RecHubData.dimOrganizations ON RecHubData.factBatchSummary.OrganizationKey = RecHubData.dimOrganizations.OrganizationKey
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE	RecHubData.dimBanks.SiteBankID = @iBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @iClientAccountID
			AND RecHubData.factBatchSummary.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factBatchSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factBatchSummary.SourceProcessingDateKey = @parmSourceProcessingDateKey
			AND RecHubData.factBatchSummary.BatchID = @iBatchID;
	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Batch')) 
            DROP TABLE #Batch;

END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Batch')) 
            DROP TABLE #Batch;

	EXEC RecHubCommon.usp_WfsRethrowException;

END CATCH
