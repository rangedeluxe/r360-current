IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSelectColumnsString') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectColumnsString
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSelectColumnsString
(
	@parmAdvancedSearchWhereClauseTable RecHubData.AdvancedSearchWhereClauseTable READONLY,
	@parmAdvancedSearchSelectFieldsTable RecHubData.AdvancedSearchSelectFieldsTable READONLY,
	@parmSelectColumnsString NVARCHAR(MAX) OUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/17/2019
*
* Purpose: Build a list of columns the user selected to return in Adv Search.
*
* Modification History
* 04/17/2019 R360-16301 JPB	Created
* 08/29/2019 R360-30221 JPB	Add support for data entry columns
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @SelectColumns NVARCHAR(MAX) = '';

BEGIN TRY

	DECLARE @SelectFields TABLE
	(
		IsCheck BIT,
		DataType INT,
		BatchSourceKey SMALLINT,
		FieldName VARCHAR(256),
		ReportTitle VARCHAR(256),
		CompositeFieldName VARCHAR(256)
	)

	INSERT INTO @SelectFields
	(
		IsCheck,
		DataType,
		BatchSourceKey,
		FieldName,
		ReportTitle,
		CompositeFieldName
	)
	EXEC RecHubData.usp_AdvancedSearch_GetSelectColumnList
		@parmAdvancedSearchWhereClauseTable = @parmAdvancedSearchWhereClauseTable,
		@parmAdvancedSearchSelectFieldsTable = @parmAdvancedSearchSelectFieldsTable;

	SELECT 
		@SelectColumns = @SelectColumns + 
		CASE DataType
			WHEN 11 THEN N',CONVERT(VARCHAR(10),'
			ELSE N','
		END +
		N'RecHubData.' +
		CASE IsCheck WHEN 1 THEN 'factChecks.' ELSE 'factStubs.' END 
		+ CompositeFieldName + 
		CASE DataType
			WHEN 11 THEN N',101) AS ' + CompositeFieldName
			ELSE N''
		END
	FROM 
		@SelectFields;

	SET @parmSelectColumnsString = @SelectColumns;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
