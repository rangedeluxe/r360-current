--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDocumentTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDocumentTypes_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDocumentTypes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDocumentTypes_Get
(
	@parmMostRecent BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/04/2011
*
* Purpose: Retrieve Document Type information
*
*
* Parameter Information
*	@parmMostRecent - Determines if only the most recent document types are returned
*		0 - All records returned
*		1 - Only MOSTRECENT = 1 are returned
*
*
* Modification History
* 10/04/2011 CR 47150 WJS	Created
* 01/25/2013 WI 86336 TWE   Converting embedded SQL
* 04/04/2013 WI 90789 JPB	Updated for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_SelDocumentTypes.
*							Merged 86336 code.
*							Added @parmMostRecent.
* 10/27/2014 WI 174450 BLR	Added IsReadOnly to the Select clause.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

    SELECT 
        RecHubData.dimDocumentTypes.DocumentTypeKey, 
        RecHubData.dimDocumentTypes.FileDescriptor, 
        RecHubData.dimDocumentTypes.DocumentTypeDescription AS [Description], 
        RecHubData.dimDocumentTypes.IMSDocumentType,
		RecHubData.dimDocumentTypes.IsReadOnly
    FROM 
        RecHubData.dimDocumentTypes
	WHERE 
		RecHubData.dimDocumentTypes.MostRecent = CASE WHEN @parmMostRecent = 1 THEN 1 ELSE RecHubData.dimDocumentTypes.MostRecent END
	ORDER BY 
		RecHubData.dimDocumentTypes.FileDescriptor;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
