--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDocumentTypes_Get_ByFileDescriptor_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDocumentTypes_Get_ByFileDescriptor_Ins') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDocumentTypes_Get_ByFileDescriptor_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDocumentTypes_Get_ByFileDescriptor_Ins
(
	@paramFileDescriptor				VARCHAR(30),
	@paramDocumentType					VARCHAR(30) = 'Invoice',
	@paramDocumentTypeDescription		VARCHAR(64) = '',
	@paramUserId						INT = 0

)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CMC
* Date: 10/11/2014
*
* Purpose: Look up Document Type By File Descriptor.  If not found insert new document type.
*
* Modification History
* 10/11/2014 WI 171556 CMC	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @documentTypeKey				INT;
DECLARE @errorDescription				VARCHAR(1024);
DECLARE @auditMessage					VARCHAR(1024);
DECLARE @currentdateTime				DATETIME = GETDATE();	

BEGIN TRY

	IF @paramFileDescriptor IS NULL OR @paramFileDescriptor = ''
	BEGIN
		SET @errorDescription = 'Must provide a file descriptor.';
		RAISERROR(@errorDescription, 16, 1);
	END

    SELECT 
        @documentTypeKey = RecHubData.dimDocumentTypes.DocumentTypeKey 
    FROM 
        RecHubData.dimDocumentTypes
	WHERE 
		RecHubData.dimDocumentTypes.FileDescriptor = @paramFileDescriptor

	IF @documentTypeKey IS NULL
	BEGIN
		INSERT INTO RecHubData.dimDocumentTypes
			(
				 MostRecent
				,CreationDate
				,ModificationDate
				,FileDescriptor
				,DocumentTypeDescription
				,IMSDocumentType
			)
		VALUES
			(
				 1
				,@currentDateTime
				,@currentDateTime
				,@paramFileDescriptor
				,@paramDocumentTypeDescription
				,@paramDocumentType
			)

		SET @documentTypeKey = SCOPE_IDENTITY();

		-- Audit the RecHubData.dimClientAccounts insert
		SET @auditMessage = 'Added dimDocumentTypes record for FileDescriptor: ' + @paramFileDescriptor	+ '.';

		/*
			Commenting out the call to auditing for right now.  The calls don't validate when using from SSIS.  Reason being, this 
			auditing stored proc calls two other stored procs (RecHubAudit.usp_dimAuditApplications_Get, and RecHubAudit.usp_dimAuditColumns_Get)
			that in turn call sp_getapplock for an exclusive lock.
		*/
		/*
		  EXEC RecHubCommon.usp_WFS_DataAudit_ins
			@parmUserID				= @paramUserId,
			@parmApplicationName	= 'RecHubData.usp_dimDocumentTypes_Get_ByFileDescriptor_Or_Insert',
			@parmSchemaName			= 'RecHubData',
			@parmTableName			= 'dimDocumentTypes',
			@parmColumnName			= 'DocumentTypeKey',
			@parmAuditValue			= @documentTypeKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;
		
		*/
	END

	SELECT
		*
	FROM
		RecHubData.dimDocumentTypes
	WHERE
		DocumentTypeKey = @documentTypeKey;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

