--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Upd_IsDeleted
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Upd_IsDeleted
(
	@parmDepositDateKey		INT,
	@parmBatchID			BIGINT,	 --WI 142866 
	@parmModificationDate	DATETIME
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/25/2009
*
* Purpose: Deletes factTransactionSummary.
*
* Modification History
* 04/25/2009 CR 25817 JPB	Created
* 11/02/2008 CR 28045 JPB	Fixed problem with reconsolidation of batches. 
* 12/31/2009 CR 28570 JPB	Performance improvements:
*								Keys passed in
* 04/23/2013 WI 90660 JPB	Updates for 2.0. Moved to RecHubData schema.
*							Renamed from usp_factTransactionSummaryDelete.
*							Combined usp_DataImportIntegrationServices_Delete_factTransactionSummary.
*							Renamed @parmCustomerKey to @parmOrganizationKey.
*							Renamed @parmLockboxKey	to @parmClientAccountKey.
*							Renamed @parmProcessingDateKey to @parmImmutableDateKey.
*							Added @parmModificationDate. 
* 06/03/2014 WI 142866 DLD  Batch Collision.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factTransactionSummary
	SET
		RecHubData.factTransactionSummary.IsDeleted = 1,
		RecHubData.factTransactionSummary.ModificationDate = @parmModificationDate		
	WHERE	
		RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.IsDeleted = 0
	OPTION( RECOMPILE );
END TRY
BEGIN CATCH
	IF( @@NESTLEVEL > 1 )
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
	
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
		EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH