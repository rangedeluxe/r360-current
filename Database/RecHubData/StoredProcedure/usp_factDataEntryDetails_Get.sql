--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Get
(
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,
	@parmBatchSequence			INT,
	@parmIsCheck				BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Santosh Sav
* Date: 06/27/2014
*
* Purpose: Get fact Data entry details for a check 
*
* Modification History
* WI 150840 SAS 06/27/2014
*    -Created for fetching Data Entry Details.
* WI 162632 BLR 09/02/2014
*    - Removed BankID, ClientAccountID, TransactionID.  Added BatchSequence.
* WI 169849 SAS  10/07/2014
*    - Changes done to pull data using TransactionID
* 11/03/2014 WI 175367 SAS Changes done to pull data using factDocuments batch sequence or factCheck BatchSequence.
					Updated The SP to take BatchSequence as an input paramater and IsCheck paramater
					If @parmIsCheck is true then the batchsequence will match from factCheck table
					If @parmIsCheck is false then the batchsequence will match from factDocument table
* 07/22/2015 WI 224526	MGE	Update for dimWorkgroupDataEntryColumns changes  (ver 2.02 sprint 26)
* 10/04/2018 PT 160928069 CMC	Returning DataType in result
**********************************************************************************************************************/
SET NOCOUNT ON; 

DECLARE	@TransactionID	INT;
DECLARE @DepositDateKey INT;

BEGIN TRY 
    
	SET @DepositDateKey=CAST(CONVERT(VARCHAR,@parmDepositDate,112) AS INT);
	IF @parmIsCheck=1
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factChecks.TransactionID 
			FROM
				RecHubData.factChecks
			WHERE 
					RecHubData.factChecks.DepositDateKey = @DepositDateKey
				AND RecHubData.factChecks.BatchID = @parmBatchID
				AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
				AND RecHubData.factChecks.IsDeleted = 0;
		END 
	ELSE 
		BEGIN
			SELECT 
				@TransactionID =RecHubData.factDocuments.TransactionID 
			FROM
				RecHubData.factDocuments
			WHERE 
					RecHubData.factDocuments.DepositDateKey = @DepositDateKey
				AND RecHubData.factDocuments.BatchID = @parmBatchID
				AND RecHubData.factDocuments.BatchSequence = @parmBatchSequence
				AND RecHubData.factDocuments.IsDeleted = 0;
		END

	SELECT	CASE RecHubData.dimWorkgroupDataEntryColumns.IsCheck
				WHEN 1 THEN 'Checks'
				ELSE 'Stubs'
			END AS TableName,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName AS FldName,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName,
			RecHubData.dimWorkgroupDataEntryColumns.UILabel AS DisplayName,
			RecHubData.dimWorkgroupDataEntryColumns.UILabel,
			RecHubData.factDataEntryDetails.DataEntryValue,
			RecHubData.factDataEntryDetails.BatchSequence,
			RecHubData.dimWorkgroupDataEntryColumns.DataType			
	FROM	RecHubData.dimWorkgroupDataEntryColumns
			INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
			--INNER JOIN RecHubData.dimClientAccounts ON RecHubData.factDataEntryDetails.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE	RecHubData.factDataEntryDetails.DepositDateKey = @DepositDateKey
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID=@TransactionID
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
	ORDER BY RecHubData.factDataEntryDetails.BatchSequence
	OPTION ( RECOMPILE );
    
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
