IF OBJECT_ID('RecHubData.usp_AdvancedSearch_GetSearchParams') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_AdvancedSearch_GetSearchParams
GO

CREATE PROCEDURE RecHubData.usp_AdvancedSearch_GetSearchParams
(
	@parmAdvancedSearchBaseKeysTable RecHubData.AdvancedSearchBaseKeysTable READONLY,
	@parmAmountFrom				MONEY OUTPUT,
	@parmAmountTo				MONEY OUTPUT,
	@parmBatchNumberFrom		BIGINT OUTPUT,
	@parmBatchNumberTo			BIGINT OUTPUT,
	@parmBatchPaymentTypeKey	TINYINT OUTPUT,
	@parmBatchSourceKey			SMALLINT OUTPUT,
	@parmDepositDateKeyFrom		INT OUTPUT,
	@parmDepositDateKeyTo		INT OUTPUT,
	@parmDisplayCOTSOnly		BIT OUTPUT,
	@parmDisplayScannedChecks	BIT OUTPUT,
	@parmMarkSense				BIT OUTPUT,
	@parmRecordsPerPage			INT OUTPUT,
	@parmSessionID				UNIQUEIDENTIFIER OUTPUT,
	@parmSiteBankID				INT OUTPUT,
	@parmSortBy					VARCHAR(32) OUTPUT,
	@parmSortByDirection		VARCHAR(4) OUTPUT,
	@parmSiteClientAccountID	INT OUTPUT,
	@parmSourceBatchIDFrom		BIGINT OUTPUT,
	@parmSourceBatchIDTo		BIGINT OUTPUT,
	@parmStartRecord			INT OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/16/2019
*
* Purpose: Query Data Advanced Search fact table for matching records
*
* Modification History
* 08/16/2019 R360-30221 JPB	Created - based on RecHubData.usp_ClientAccount_Search
*******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	/* Get the input parameters from the table */
	SELECT
		@parmAmountFrom	= COALESCE(AmountFrom,0),
		@parmAmountTo = COALESCE(AmountTo,922337203685477.5807), /* Max MONEY if the value was not passed in (NULL) */
		@parmBatchNumberFrom = COALESCE(BatchNumberFrom,0),
		@parmBatchNumberTo = COALESCE(BatchNumberTo,2147483647), /* Max INT if the value was not passed in (NULL) */
		@parmBatchPaymentTypeKey = BatchPaymentTypeKey,
		@parmBatchSourceKey = BatchSourceKey,
		@parmDepositDateKeyFrom = CONVERT(VARCHAR,(DateFrom),112),
		@parmDepositDateKeyTo = CONVERT(VARCHAR,(DateTo),112),
		@parmDisplayCOTSOnly = CONVERT(VARCHAR,COTSOnly),
		@parmDisplayScannedChecks = COALESCE(DisplayScannedCheck,0),
		@parmMarkSense = CONVERT(VARCHAR,MarkSenseOnly),
		@parmRecordsPerPage = CASE
			WHEN COALESCE(PaginateRS,0) = 0 THEN 2147483647 /* Max INT if not paged */
			ELSE COALESCE(RecordsPerPage,10)
		END,
		@parmSessionID = SessionID,
		@parmSiteBankID = BankID,
		@parmSortBy = SortBy,
		@parmSortByDirection = 
			CASE
				WHEN LEN(SortByDir) = 0 OR SortByDir IS NULL
					THEN 'ASC'
					ELSE SortByDir
			END,
		@parmSiteClientAccountID = ClientAccountID,
		@parmSourceBatchIDFrom = COALESCE(BatchIDFrom,0),
		@parmSourceBatchIDTo = COALESCE(BatchIDTo,9223372036854775807), /* Max BIGINT if the value was not passed in (NULL) */
		@parmStartRecord = COALESCE(StartRecord,1)
	FROM
		@parmAdvancedSearchBaseKeysTable;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH