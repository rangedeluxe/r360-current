--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_BatchDetail
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factBatchSummary_Get_BatchDetail') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factBatchSummary_Get_BatchDetail
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factBatchSummary_Get_BatchDetail
(
	@parmSessionID				UNIQUEIDENTIFIER,	-- WI 142817
	@parmBatchID				BIGINT,				-- WI 142817
	@parmDepositDate			DATETIME,		
	@parmDisplayScannedCheck	BIT,
	@parmStartRecord			INT,
	@parmRecordsPerPage			INT = 25,
	@parmOrderBy				VarChar(128) = '',
	@parmOrderDirection			VarChar(4) = '',
	@parmSearch					VARCHAR(MAX) = '',
	@parmTotalRecords			INT OUTPUT,
	@parmFilteredRecords		INT = 0 OUTPUT
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2009-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JMC
* Date: 03/23/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range.
*
* Modification History
* 03/23/2009 CR 25817 JMC	Created.
* 11/18/2009 CR 28313 JMC   Modified Order by to use Transactions.TxnSequence.
* 01/26/2011 CR 32463 JPB	Redone to improve performance.
* 04/14/2011 CR 33875 JPB	Changed inner join on factChecks/dimRemitters to
*							left join.
* 10/28/2011 CR 47512 JPB	Speed improvements:
* 01/19/2012 CR 49162 JNE       Add CaptureSiteCodeID to result set.
* 03/14/2012 CR 49162 WJS   Remove CaptureSiteCodeID, add BatchSiteCode to result set.
* 06/21/2012 CR 53610 JNE	Add BatchCueID to result set.
* 07/20/2012 CR 54152 JNE	Add BatchNumber to result set.
* 11/21/2012 WI 70671 JPB	Added paging (FP:CR 55430) and removed INNER JOIN to dimDates
* 11/21/2012 WI 70671 JPB	Changed sort order (FP:CR 56514).
* 11/21/2012 WI 70679 JPB	Added RecID to final result set, final sort was not correct. (FP:CR 56767).
* 04/01/2013 WI 90668 JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_GetBatchDetail.
*							Change parameters: @parmOLLockboxID to @parmOLClientAccountID
*							Changed all references:  Lockbox to ClientAccount, Processing to Immutable,
*							Customer to Organization.
*							Removed table dimRemitters.
*							Added ISNULL check for Total records to correct if no rows in table.
* 11/13/2013 WI 122324 JBS	Changed Join/Where clause for Bug#122285. 
* 02/10/2014 WI 129044 EAS	Add Payment Type and Payment Source to result set.
* 04/23/2014 WI 138110 JBS	Add DDA to Result set.
* 06/04/2014 WI 142817 PKW  Batch Collision.
*							Replaced @parmOLClientAccountID with @parmSessionID.
*							Removed table RecHubUser.OLClientAccounts INNER JOIN from #PageRecords.
*							Removed from WHERE clause:
*								RecHubData.factTransactionSummary.ImmutableDateKey,
*								RecHubData.factTransactionSummary.OrganizationKey,
*								RecHubData.factTransactionSummary.BankKey,
*								RecHubData.factTransactionSummary.DepositStatus,
*								RecHubData.factChecks.ClientAccountKey,
*								RecHubData.factChecks.ImmutableDateKey,
*								RecHubData.factChecks.BankKey,
*								RecHubData.factChecks.OrganizationKey.
* 08/26/2014 WI 161355 JMC	Removed MostRecent = 1
* 08/27/2014 WI 159754 SAS  ImageRPSReport.
*							Added ImportTypeKey Field
* 08/28/2014 WI 162056 SAS  ImageRPSReport.							
*							Added Processing Date Key
* 09/22/2014 WI 167267 SAS  ImportTypeShortName field added.	
* 10/24/2014 WI	174077 SAS	Changes done to pull ShortName from dimBatchSources table		
* 10/30/2014 WI	175114 SAS	Changes done to update ShortName to BatchSourceName 
*							Change done to set the BatchSourceName to blank when importtype is integraPAY	
* 11/07/2014 WI 176301 SAS  Add BatchSourceShortName  and ImportTypeShortName to resultset.  		
							Removed BatchSourceName
* 03/09/2015 WI 194871 JBS	Add View Ahead permission
* 01/26/2016 WI 260174 LA   Add dimClientAccounts.SiteCodeId to output
* 02/03/2016 WI 261564 LA   Return zero when Amount is null
* 07/11/2016 WI 290948 BLR  Added another join to get the MostRecent ClientAccount row.
* 04/11/2018 PT 154101786 JPB	Added DocumentBatchSequence to final result.
* 05/16/2018 PT 154338586 TDM Added search parameter
* 05/16/2018 PT 154338586 TDM Added OrderBy and OrderDirection
* 10/30/2018 PT 161348262 MGE Added FileGroup to result set
* 11/02/2018 PT 161346424	CMC Rework for bug R360-9365
* 01/02/2019 R360-14601		MGE Tune for join of Documents
******************************************************************************/
SET NOCOUNT ON; 

DECLARE																
	@DepositDateKey	INT;
	IF (@parmSearch is null)
	BEGIN
		SET @parmSearch = ''
	END
											--WI 142817
	SET @parmSearch = '%' + @parmSearch + '%'

BEGIN TRY

	SELECT @DepositDateKey = CAST(CONVERT(VARCHAR, @parmDepositDate, 112) AS INT);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#TempTxnPmtResults')) 
		DROP TABLE #TempResults;

	CREATE TABLE #TempTxnPmtResults
	(
		RecID				INT,
		--LongName			VARCHAR(40),
		BankID				INT,
		LockboxID			INT,
		BatchID				BIGINT,
		DepositDateKey		INT,
		SourceBatchID		BIGINT,
		DepositDate			DATETIME,
		PICSDATE			INT,
		ProcessingDate		DATETIME,
		TransactionID		INT,
		TxnSequence			INT,
		PaymentBatchSequence	INT,
		DocumentBatchSequence	INT,
		Serial				VARCHAR(30),
		Amount				MONEY,
		RT					VARCHAR(30),
		Account				VARCHAR(30),
		RemitterName		VARCHAR(60),
		BatchTotal			MONEY,
		CheckCount			INT,
		StubCount			INT,
		DocumentCount		INT,
		BatchSiteCode		INT,
		BatchCueID			INT,
		BatchNumber			INT,
		PaymentType			VARCHAR(30),
		PaymentSource		VARCHAR(30),
		ImportTypeKey		TINYINT,
		ImportTypeShortName	VARCHAR(30),
		DDA					VARCHAR(35),
		BatchSourceShortName VARCHAR(30)
	)

	CREATE TABLE #PagedResults
	(
		RecID				INT,
		TotalRecords		INT,
		LongName			VARCHAR(40),
		BankID				INT,
		ClientAccountID		INT,
		BatchID				BIGINT,			--WI 142817		
		DepositDateKey		INT,			
		SourceBatchID		BIGINT,			--WI 142817
		DepositDate			DATETIME,
		PICSDate			INT,
		ProcessingDate		DATETIME,
		TransactionID		INT,
		TxnSequence			INT,
		BatchTotal			MONEY,
		BatchSequence		INT,
		RT					VARCHAR(30),
		Account				VARCHAR(30),
		RemitterName		VARCHAR(60),
		Serial				VARCHAR(30),
		Amount				MONEY,
		CheckCount			INT,
		StubCount			INT,
		DocumentCount		INT,
		BatchSiteCode		INT,
		BatchCueID			INT,
		BatchNumber			INT,
		PaymentType			VARCHAR(30),
		PaymentSource		VARCHAR(30),
		DDA					VARCHAR(35),
		ImportTypeKey		TINYINT,
		BatchSourceShortName VARCHAR(30),
		ImportTypeShortName  VARCHAR(30),
		AccountSiteCode		INT,
		FileGroup			VARCHAR(256),
		DocumentBatchSequence INT
	)

	;WITH TransactionRecords AS -- grab the transaction records for this batch
	(
		SELECT	
			--RTRIM(RecHubData.dimClientAccounts.LongName) AS LongName,
			RecHubUser.SessionClientAccountEntitlements.SiteBankID AS BankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID	AS LockboxID,
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factBatchSummary.DepositDateKey,
			RecHubData.factTransactionSummary.SourceBatchID,					-- WI 142817
			CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factBatchSummary.DepositDateKey), 112),101) AS DepositDate,
			RecHubData.factTransactionSummary.ImmutableDateKey AS PICSDate,
			CONVERT(varchar, CONVERT(DATETIME, CONVERT(VARCHAR(8),RecHubData.factBatchSummary.SourceProcessingDateKey), 112),101) AS ProcessingDate,				
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence,
			RecHubData.factBatchSummary.CheckTotal AS BatchTotal,
			RecHubData.factTransactionSummary.CheckCount,
			RecHubData.factTransactionSummary.StubCount,
			CASE WHEN @parmDisplayScannedCheck > 0 THEN
				RecHubData.factTransactionSummary.DocumentCount 
			ELSE
				RecHubData.factTransactionSummary.DocumentCount - RecHubData.factTransactionSummary.ScannedCheckCount
			END	AS DocumentCount,
			RecHubData.factBatchSummary.BatchSiteCode,
			RecHubData.factBatchSummary.BatchCueID,
			RecHubData.factBatchSummary.BatchNumber,
			RecHubData.dimBatchPaymentTypes.ShortName AS PaymentType,
			RecHubData.dimBatchSources.ShortName AS PaymentSource,
			RecHubData.dimImportTypes.ImportTypeKey,
			RecHubData.dimBatchSources.ShortName AS BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName AS ImportTypeShortName
		FROM 	
			RecHubData.factBatchSummary
			INNER JOIN RecHubData.dimClientAccounts ON
				RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON	--WI 142817
				RecHubData.factBatchSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
			INNER JOIN RecHubData.factTransactionSummary ON
				RecHubData.factTransactionSummary.BatchID = RecHubData.factBatchSummary.BatchID
				AND RecHubData.factTransactionSummary.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON
				RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factBatchSummary.BatchPaymentTypeKey
			INNER JOIN RecHubData.dimBatchSources ON
				RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
			INNER JOIN RecHubData.dimImportTypes ON
				RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey	
		WHERE	
			RecHubData.factBatchSummary.DepositDateKey = @DepositDateKey				--WI 142817
			AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID	--WI 142817
			AND RecHubData.factBatchSummary.DepositDateKey >= RecHubUser.SessionClientAccountEntitlements.StartDateKey -- WI 142817
			AND ((RecHubData.factBatchSummary.DepositDateKey <= RecHubUser.SessionClientAccountEntitlements.EndDateKey AND RecHubUser.SessionClientAccountEntitlements.ViewAhead = 0) -- WI 194871
					OR (RecHubUser.SessionClientAccountEntitlements.ViewAhead = 1))  -- WI 194871
			AND RecHubData.factBatchSummary.BatchID =	CASE WHEN @parmBatchID >= 0 THEN 
															@parmBatchID 
														ELSE 
															RecHubData.factBatchSummary.BatchID 
														END
			AND RecHubData.factBatchSummary.DepositStatus >= 850
			AND RecHubData.factBatchSummary.IsDeleted = 0
			AND RecHubData.factTransactionSummary.IsDeleted = 0
	),
	PaymentRecords AS -- add the payment records if there are any
	(
		SELECT	
			TransactionRecords.*,
			RecHubData.factChecks.BatchSequence,
			RTRIM(RecHubData.factChecks.Serial) AS Serial,
			RecHubData.factChecks.Amount,
			RTRIM(RecHubData.factChecks.RoutingNumber) AS RT,
			RTRIM(RecHubData.factChecks.Account) AS Account,
			RTRIM(RecHubData.factChecks.RemitterName) AS RemitterName,
			RecHubData.dimDDAs.DDA AS DDA,
			RecHubData.factChecks.SequenceWithinTransaction AS SequenceWithinTransaction
		FROM 	
			TransactionRecords 
			LEFT JOIN RecHubData.factChecks ON
				RecHubData.factChecks.DepositDateKey = TransactionRecords.DepositDateKey
				AND RecHubData.factChecks.BatchID = TransactionRecords.BatchID
				AND RecHubData.factChecks.TransactionID = TransactionRecords.TransactionID
				AND RecHubData.factChecks.TxnSequence = TransactionRecords.TxnSequence
				AND RecHubData.factChecks.IsDeleted = 0		-- WI 122324
			LEFT JOIN RecHubData.dimDDAs ON
				RecHubData.dimDDAs.DDAKey = RecHubData.factchecks.DDAKey
	) 

	INSERT INTO #TempTxnPmtResults
	(
		
		RecID,
		BankID			,
		LockboxID		,
		BatchID			,
		DepositDateKey	,
		SourceBatchID	,
		DepositDate		,
		PICSDATE		,
		ProcessingDate	,
		TransactionID	,
		TxnSequence		,
		PaymentBatchSequence,
		DocumentBatchSequence,
		BatchTotal,
		CheckCount,
		StubCount,
		DocumentCount,
		PaymentSource,
		BatchSourceShortName,
		Serial,
		Amount,
		RT,
		Account,
		RemitterName,
		DDA,
		PaymentType,
		ImportTypeKey,
		ImportTypeShortName,
		BatchNumber,
		BatchCueID,
		BatchSiteCode
	)
	SELECT  
		ROW_NUMBER() OVER 
		(
			ORDER BY TransactionRecords.BatchID,
					TransactionRecords.DepositDateKey,
					TransactionRecords.TransactionID,
					TransactionRecords.TxnSequence,
					PaymentRecords.BatchSequence ASC
		) AS RecID,
		TransactionRecords.BankID,
		TransactionRecords.LockboxID,
		TransactionRecords.BatchID,
		TransactionRecords.DepositDateKey,
		TransactionRecords.SourceBatchID,				
		TransactionRecords.DepositDate,
		TransactionRecords.PICSDate,
		TransactionRecords.ProcessingDate,
		TransactionRecords.TransactionID,
		TransactionRecords.TxnSequence,
		PaymentRecords.BatchSequence AS PaymentBatchSequence,
		factDocuments.BatchSequence AS DocumentBatchSequence,
		TransactionRecords.BatchTotal,
		TransactionRecords.CheckCount,
		TransactionRecords.StubCount,
		TransactionRecords.DocumentCount,
		TransactionRecords.PaymentSource,
		TransactionRecords.BatchSourceShortName,
		Serial,
		Amount,
		RT,
		Account,
		RemitterName,
		PaymentRecords.DDA,
		TransactionRecords.PaymentType,
		TransactionRecords.ImportTypeKey,
		TransactionRecords.ImportTypeShortName,
		TransactionRecords.BatchNumber,
		TransactionRecords.BatchCueID,
		TransactionRecords.BatchSiteCode
	FROM	
			TransactionRecords 
			LEFT JOIN PaymentRecords ON
				PaymentRecords.DepositDateKey = TransactionRecords.DepositDateKey
				AND PaymentRecords.BatchID = TransactionRecords.BatchID
				AND PaymentRecords.TransactionID = TransactionRecords.TransactionID
				AND PaymentRecords.TxnSequence = TransactionRecords.TxnSequence
			LEFT JOIN 
				(
				Select DepositDateKey, BatchID, TransactionID, Min(BatchSequence) AS BatchSequence
				FROM 	RecHubData.factDocuments
				WHERE 
					 RecHubData.factDocuments.IsDeleted = 0
				Group By DepositDateKey, BatchID, TransactionID
				) as factDocuments
			ON factDocuments.DepositDateKey = TransactionRecords.DepositDateKey
			AND factDocuments.BatchID = TransactionRecords.BatchID
			AND factDocuments.TransactionID = TransactionRecords.TransactionID
	WHERE 
		(TransactionRecords.TransactionID LIKE @parmSearch   
				OR CAST(ISNULL(Amount, 0) AS VARCHAR) LIKE @parmSearch
				OR RTRIM(RemitterName)  LIKE @parmSearch
				OR RTRIM(RT) LIKE @parmSearch
				OR RTRIM(Account) LIKE @parmSearch
				OR RTRIM(Serial) LIKE @parmSearch
				OR RTRIM(TransactionRecords.BatchSourceShortName) LIKE @parmSearch
				OR PaymentRecords.DDA LIKE @parmSearch)	
	OPTION(RECOMPILE);			

	INSERT INTO #PagedResults -- insert the filtered result set of batch detail records into the temp table
	(	
			RecID,
			TotalRecords,
			LongName,
			BankID,
			ClientAccountID,
			BatchID,
			DepositDateKey,
			SourceBatchID,				--WI 142817
			DepositDate,
			PICSDate,
			ProcessingDate,
			TransactionID,
			TxnSequence,
			BatchTotal,
			BatchSequence,
			Serial,
			Amount, 
			RT,
			Account,
			RemitterName,
			CheckCount,
			StubCount,
			DocumentCount,
			BatchSiteCode,
			BatchCueID,
			BatchNumber,
			PaymentType,
			PaymentSource,
			DDA,
			ImportTypeKey,
			BatchSourceShortName,
			ImportTypeShortName,
			AccountSiteCode,
			[FileGroup],
			DocumentBatchSequence
	)

	SELECT	
			RecID,
			TotalRecords = (SELECT ISNULL(MAX(RecID), 0) FROM #TempTxnPmtResults),
			RecHubData.dimClientAccounts.LongName,
			BankID,
			LockboxID,
			BatchID,
			DepositDateKey,
			SourceBatchID,				
			DepositDate,
			PICSDate,
			ProcessingDate,
			TransactionID,
			TxnSequence,
			BatchTotal,
			PaymentBatchSequence,
			Serial,
			Amount, 
			RT,
			Account,
			RemitterName,
			CheckCount,
			StubCount,
			DocumentCount,
			BatchSiteCode,
			BatchCueID,
			BatchNumber,
			PaymentType,
			PaymentSource,
			#TempTxnPmtResults.DDA,
			ImportTypeKey,
			BatchSourceShortName,
			ImportTypeShortName,
			RecHubData.dimClientAccounts.SiteCodeID AS AccountSiteCode,
			RecHubData.dimClientAccounts.[FileGroup],
			DocumentBatchSequence
	FROM	
			#TempTxnPmtResults
			INNER JOIN RecHubData.dimClientAccounts 
				ON #TempTxnPmtResults.BankID = RecHubData.dimClientAccounts.SiteBankID
				AND #TempTxnPmtResults.LockboxID = RecHubData.dimClientAccounts.SiteClientAccountID
				AND RecHubData.dimClientAccounts.MostRecent = 1
	

	-- Get filtered record count
	SELECT	
		@parmFilteredRecords = COUNT(*)	
	FROM 	
		#PagedResults

	-- Get total record count
	SELECT	
		@parmTotalRecords = TotalRecords	
	FROM 	
		#PagedResults

	IF @parmTotalRecords > 0
	BEGIN
		-- Are we returning a page or the full set
		IF @parmRecordsPerPage > 0
			SELECT	
				LongName,
				BankID,
				ClientAccountID,
				BatchID,
				SourceBatchID,				--WI 142817
				DepositDate,
				PICSDate,
				ProcessingDate,
				TransactionID,
				TxnSequence,
				BatchTotal,
				BatchSequence,
				Serial,
				ISNULL(Amount, 0) AS Amount,
				RT,
				Account,
				RemitterName,
				CheckCount,
				StubCount,
				DocumentCount,
				BatchSiteCode,
				BatchCueID,
				BatchNumber,
				PaymentType,
				PaymentSource,
				DDA,
				ImportTypeKey,
				BatchSourceShortName,
				ImportTypeShortName,
				AccountSiteCode,
				[FileGroup],
				DocumentBatchSequence
			FROM	
				#PagedResults
			ORDER BY 
			CASE WHEN @parmOrderBy = ''
				THEN RecID
			END,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'TransactionId' THEN #PagedResults.TransactionId END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'TxnSequence' THEN #PagedResults.TxnSequence END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'Amount' THEN #PagedResults.Amount END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'RT' THEN #PagedResults.RT END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'Account' THEN #PagedResults.Account END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'Serial' THEN #PagedResults.Serial END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'PaymentSource' THEN #PagedResults.PaymentSource END ASC,
			CASE WHEN @parmOrderDirection = 'asc' AND @parmOrderBy = 'DDA' THEN #PagedResults.DDA END ASC,

			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'TransactionId' THEN #PagedResults.TransactionId END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'TxnSequence' THEN #PagedResults.TxnSequence END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'Amount' THEN #PagedResults.Amount END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'RT' THEN #PagedResults.RT END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'Account' THEN #PagedResults.Account END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'Serial' THEN #PagedResults.Serial END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'PaymentSource' THEN #PagedResults.PaymentSource END DESC,
			CASE WHEN @parmOrderDirection = 'desc' AND @parmOrderBy = 'DDA' THEN #PagedResults.DDA END DESC
			OFFSET @parmStartRecord ROWS
			FETCH FIRST @parmRecordsPerPage ROWS ONLY;
		ELSE
			SELECT	
				LongName,
				BankID,
				ClientAccountID,
				BatchID,
				SourceBatchID,			
				DepositDate,
				PICSDate,
				ProcessingDate,
				TransactionID,
				TxnSequence,
				BatchTotal,
				BatchSequence,
				DocumentBatchSequence,
				Serial,
				ISNULL(Amount, 0) AS Amount,
				RT,
				Account,
				RemitterName,
				CheckCount,
				StubCount,
				DocumentCount,
				BatchSiteCode,
				BatchCueID,
				BatchNumber,
				PaymentType,
				PaymentSource,
				DDA,
				ImportTypeKey,
				BatchSourceShortName,
				ImportTypeShortName,
				AccountSiteCode,
				[FileGroup],
				DocumentBatchSequence
			FROM	
				#PagedResults
		END
	ELSE
		SET @parmTotalRecords = 0;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#PagedResults')) 
		DROP TABLE #PagedResults;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH