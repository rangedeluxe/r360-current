--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_ChecksDE
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Get_ChecksDE') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Get_ChecksDE
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Get_ChecksDE 
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142840
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT,				--WI 142840
	@parmTransactionID			INT,
	@parmBatchSequence			INT,
	@parmConvertDataType		BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2008-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/30/2009
*
* Purpose: Query Data Entry Detail fact table for batch, transaction checks 
*			data entry information.
* Condition: @parmConvertDataType = 1 Convert datatype for FldDataTypeEnum
*									0 Do NOT Convert datatype for FldDataTypeEnum
*	TableType
*	0 - Checks
*	1 - ChecksDataEntry
*	2 - Stubs
*	3 - StubsDE
*	
*	DataType
*	7 - Money	
* Modification History
* 03/30/2009 CR 25817 JPB	Created
* 04/02/2013 WI 90718 JBS	Update to 2.0 release.  Change schema
*							Rename proc from usp_GetTransactionChecksDE
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID
*							Added Parameter: @parmConvertDataType BIT
*							Change all references: Lockbox to ClientAccount, 
*                           Processing to Immutable.
*							Adding: factDataEntryDetails.IsDeleted = 0
*							Changed dimDataEntryColumns.TableType = 1 in CASE 
*                           statement to match Where clause.
* 12/03/2013 WI 124078 MLH	Changed TableType in CASE from 2 to 0.  The rule is 
*                           if TableType is 'Stubs' or 'Checks' and 
*                           FldName = 'Amount', then the DataTypeEnum conversion 
*                           is currency.
* 05/28/2014 WI 142840 DLD  Batch Collision/RAAM Integration.
* 03/24/2015 WI 197813 MAA  Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead 
* 07/22/2015 WI 224456 MAA  Updated to support WorkgroupDataEntryColumns
* 08/10/2015 WI 228479 MAA  No longer returns inactive DE columns
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartDateKey INT,	--WI 142840
		@EndDateKey INT;	--WI 142840

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */ --WI 142840
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;
                
	SELECT  'Checks' AS 'TableName',
			RecHubData.dimWorkgroupDataEntryColumns.FieldName as FldName,
			CASE @parmConvertDataType
				WHEN 0 
					THEN RecHubData.dimWorkgroupDataEntryColumns.DataType
				WHEN 1 
					THEN 
						CASE
							WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount')
								THEN 7
							ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
						END
			END AS FldDataTypeEnum,
			RecHubData.factDataEntryDetails.DataEntryValue,
			RTRIM(COALESCE(RecHubData.dimWorkgroupDataEntryColumns.UILabel,RecHubData.dimWorkgroupDataEntryColumns.FieldName)) AS FldTitle
	FROM	RecHubData.dimWorkgroupDataEntryColumns
			INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey	--WI 142840
	WHERE	RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142840
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142840
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142840
			AND RecHubData.factDataEntryDetails.DepositDateKey = @EndDateKey								--WI 142840
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
			AND RecHubData.factDataEntryDetails.BatchSequence = @parmBatchSequence
			AND RecHubData.factDataEntryDetails.IsDeleted = 0		
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
	ORDER BY 
			RecHubData.factDataEntryDetails.TransactionID,
			RecHubData.factDataEntryDetails.BatchSequence,
			RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder ASC
	OPTION ( RECOMPILE );

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
