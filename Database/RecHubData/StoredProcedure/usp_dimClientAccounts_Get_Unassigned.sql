--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimClientAccounts_Get_Unassigned
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimClientAccounts_Get_Unassigned') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimClientAccounts_Get_Unassigned
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimClientAccounts_Get_Unassigned
(
	@parmFIEntityID		INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/17/2014
*
* Purpose: Query dimClientAccounts for records for a given FI that do not have 
*			a corresponding RecHubUser.OLWorkgroups tying them to RAAM
*
* Modification History
* 06/17/2014 WI 148383 KLC	Created
* 02/08/2018 PT 154804395	MGE	Only select Active Workgroups
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT	RecHubData.dimClientAccountsView.ClientAccountKey,
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.IsActive,
			RecHubData.dimClientAccountsView.ShortName,
			RecHubData.dimClientAccountsView.LongName
	FROM	RecHubData.dimClientAccountsView
		JOIN RecHubData.dimBanksView
			ON RecHubData.dimClientAccountsView.SiteBankID = RecHubData.dimBanksView.SiteBankID
		LEFT JOIN RecHubUser.OLWorkGroups
			ON	RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkGroups.SiteBankID
			AND	RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkGroups.SiteClientAccountID
	WHERE	RecHubData.dimBanksView.EntityID = @parmFIEntityID
		AND RecHubUser.OLWorkGroups.EntityID IS NULL
		AND RecHubData.dimClientAccountsView.IsActive = 1;
                 
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

