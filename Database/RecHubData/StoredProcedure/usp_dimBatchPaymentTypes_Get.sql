--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimBatchPaymentTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimBatchPaymentTypes_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimBatchPaymentTypes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimBatchPaymentTypes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 06/13/2013
*
* Purpose: Get current Payment Type definitions
*
* Modification History
* 06/13/2013 WI 106026 DRP	Created
* 07/10/2013 WI 106026 DRP	Fixed typo in comment/heading - wrong usp name.
* 03/31/2015 WI 198858 JPB	Added ORDER BY LongName
* 04/10/2015 WI 198858 JBS	Added permissions 
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.ShortName,
		RecHubData.dimBatchPaymentTypes.LongName
	FROM   
		RecHubData.dimBatchPaymentTypes
	ORDER BY
		RecHubData.dimBatchPaymentTypes.LongName;
	
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
