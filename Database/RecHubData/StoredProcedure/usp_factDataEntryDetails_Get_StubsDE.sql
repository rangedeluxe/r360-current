--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_StubsDE
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factDataEntryDetails_Get_StubsDE') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factDataEntryDetails_Get_StubsDE
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factDataEntryDetails_Get_StubsDE
(
	@parmSessionID				UNIQUEIDENTIFIER,	--WI 142841
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				INT,
	@parmTransactionID			INT,
	@parmBatchSequence			INT,
	@parmConvertDataType		BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/30/2009
*
* Purpose: Query Data Entry Detail fact table for batch, transaction stub 
*			data entry information
*
*
* Parameter Information
*	@parmConvertDataType - 0 means to not convert, 1 means to convert.
*
*
* Modification History
* 03/30/2009 CR 25817  JPB	Created.
* 04/03/2013 WI 90726  JPB	Updated for 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_GetTransactionStubsDE.
*							Renamed @parmSiteLockboxID to @parmSiteClientAccountID.
*							Changed all dimLockboxes refs to dimClientAccounts. 
*							Add @parmConvertDataType.
*							Moved Recompile from SP to SELECT.
* 06/02/2014 WI 142841 PKW  Batch Collision.
* 03/24/2015 WI 197815 MAA  Added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead 
* 07/22/2015 WI 224457 MAA  Updated to support WorkgroupDataEntryColumns
* 08/10/2015 WI 227843 MAA  No longer returns inactive DE columns
******************************************************************************/
SET NOCOUNT ON;

DECLARE														--WI 142841
		@StartDateKey	INT,								--WI 142841
		@EndDateKey		INT;								--WI 142841
		
BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,			--WI 142841
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT	'Stubs' AS 'TableName',
		RecHubData.dimWorkgroupDataEntryColumns.FieldName as FldName,
		CASE @parmConvertDataType
			WHEN 0 
				THEN RecHubData.dimWorkgroupDataEntryColumns.DataType
			WHEN 1 
				THEN 
					CASE
						WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount')
							THEN 7
						ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
					END
		END AS FldDataTypeEnum,
		RecHubData.factDataEntryDetails.DataEntryValue
	FROM
		RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN RecHubData.factDataEntryDetails ON
			RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142841
			RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	WHERE	
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID						--WI 142841
		AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142841
		AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID  --WI 142841
		AND RecHubData.factDataEntryDetails.DepositDateKey = @EndDateKey								--WI 142841
		AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
		AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
		AND RecHubData.factDataEntryDetails.BatchSequence = @parmBatchSequence
		AND RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
	ORDER BY 
		RecHubData.factDataEntryDetails.TransactionID,
		RecHubData.factDataEntryDetails.BatchSequence,
		RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder ASC
		OPTION( RECOMPILE );

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
