--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ImportReconciliation
--WFSScriptProcessorStoredProcedureDrop
--WFSScriptProcessorStoredProcedureCreate
IF OBJECT_ID('RecHubData.usp_Report_ImportReconciliation') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_Report_ImportReconciliation;
GO
CREATE PROCEDURE RecHubData.usp_Report_ImportReconciliation
(
		@parmUserID				INT,
		@parmSessionID			UNIQUEIDENTIFIER,		  -- WI 139057
		@parmReportParameters   XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*                                                                           	
* Author: PKW									                                
* Date: 04/03/2014																
*																				
* Purpose: Render information to the Import Reconciliation SSRS report.																
*																			    
* Modification History														    	
* 04/03/2014 WI 135930  PKW  Created.  
* 05/07/2014 WI 139561  PKW  Added Payment Source criteria on report. 
* 06/18/2014 WI 135930	PKW  Move the SP from RecHubAlert to RecHubData schema.
*							 Granted on SP the 'RecHubUser_User' with EXECUTE Permission.
* 07/02/2014 WI 139057	DLD	 Add RAAM resource.	
* 08/01/2014 WI 151507  PKW  Add EntityName to SP and updated ImportReconciliation Report RDL.
* 08/11/2014 WI 157984  PKW  Add @parmUserID,@parmSessionID,@parmReportParameters to SP
*							 Updated ImportReconciliation Report RDL.
* 08/19/2014 WI 157984	PKW  Add WFSScriptProcessorQuotedIdentifierOn tags to SP.
*							 Renamed @StartDate to @parmStartDate, 
*							   @EndDate to @parmEndDate, 
*							   @PaymentSrc to @parmPaymentSrc.
* 08/26/2014 WI 157984  PKW  Removed columns from SP
*							  1. RecHubData.factBatchSummary.ScannedCheckCount,
*							  2. RecHubData.factBatchSummary.DocumentCount
*							  3. Replaced INNER JOIN RecHubData.dimBatchSources.BatchSourceKey
*							     with RecHubData.factBatchSummary.BatchPaymentTypeKey.
*							  4. Changed date format for RecHubData.factBatchSummary.CreationDate.
*							  5. Removed RecHubUser.SessionClientAccountEntitlements.ClientAccountKey.
*							  6. Added table join and column RecHubData.dimClientAccounts.ShortName.
* 08/29/2014 WI 157984 PKW   Removed per bugs 160813,160817,160764,160773,160801,160813,160830.
*							 1.RecHubData.factBatchSummary.IsDeleted = 0 from the Where clause.
*							 2.Changed RecHubData.factBatchSummary.CreationDate >= @parmStartDate
*							 3.RecHubData.factBatchSummary.CreationDate <= @parmEndDate
*							 4.Updated SP with @parmEventType='Reports' for the RAAM Session Audits
*							 5.Changed date format for RecHubData.factBatchSummary.CreationDate.
* 09/03/2014 WI 161088	LA   Added Payment Source to Audit Record.
* 09/10/2014 WI 157984 PKW   Added per bug 160813 - RecHubData.factBatchSummary.IsDeleted = 0 to Where clause.
*							 Added per bug 160797 - SiteClientAccountID and LongName for the report Workgroup column.
*							 Added per bug 160817 - Return results for selected date when both start and end date are same.
* 09/18/2014 WI 157984 PKW   Add @tmpReportParameters TABLE
*							 Changed WHERE clause to enable single or 'All Payment Sources' selections.
* 09/22/2014 WI 157984 PKW   Added to the WHERE clause the selection for '-- ALL --' per bug 160764
*						     Changed to LongName length per bug 167016 to display entire name.
*							 Added the sort order by DepositDateKey,EntityID, SiteClientAccountID & BatchNumber.
* 09/26/2014 WI 157984 PKW	 Updated sort order to DepositDateKey,EntityID, SiteClientAccountID, 
*							 Payment Source, Payment Type & BatchNumber.
* 01/14/2014 WI 172062 LA	 Updated Sort order to DepositDateKey DESC
* 01/29/2014 WI 174889 LA	 Fix Entity BreadCrumb parameters
* 12/15/2014 WI 174889 KLC   Updated to handle null workgroup long names though it should probably 
*								use the RecHubData.dimClientAccountsView for the display label
*								as is the standard with most other things.
* 03/17/2015 WI 196347 MAA   Added stub count 
* 03/18/2015 WI 196441 MAA   Added SourceBatchID
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DECLARE 
        @parmStartDate		DATETIME,                                                
	    @parmEndDate		DATETIME,
		@parmPaymentSrc		VARCHAR(30);			  -- WI 139057 Increased length to accommodate long strings.

	SELECT	@parmStartDate = CASE WHEN Parms.val.value('(SD)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(SD)[1]', 'DATETIME'), 101) ELSE NULL END,
			@parmEndDate = CASE WHEN Parms.val.value('(ED)[1]', 'VARCHAR(10)') <> '' THEN CONVERT(VARCHAR, Parms.val.value('(ED)[1]', 'DATETIME'), 101) ELSE NULL END,
			@parmPaymentSrc = Parms.val.value('(PS)[1]', 'VARCHAR(30)')
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);


	-- WI172396 Add the EntityBreadcrumb data to a temp table
	-- So we can Join to it for RAAM data
	IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
		DROP TABLE #tmpEntities;

	CREATE TABLE #tmpEntities
	(
           EntityID              INT,
           BreadCrumb	         VARCHAR(100)
     );
     
     INSERT INTO #tmpEntities
     (
           EntityID,
           BreadCrumb       
	)

     SELECT     
           entityRequest.att.value('@EntityID', 'int') AS EntityID,
           entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
       FROM  
          @parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

	SELECT 
		  CONVERT(VARCHAR(20), (RecHubData.factBatchSummary.CreationDate), 22) AS ImportDateTime,
		  RecHubData.factBatchSummary.BatchSourceKey,
		  RecHubData.factBatchSummary.factBatchSummaryKey,
		  CONVERT(VARCHAR(10), (CAST(CAST(RecHubData.factBatchSummary.DepositDateKey AS VARCHAR) AS DATETIME)), 101) AS DepositDateKey,
		  RecHubData.factBatchSummary.BatchNumber,
		  RecHubData.factBatchSummary.BatchPaymentTypeKey,
		  RecHubData.factBatchSummary.TransactionCount,
		  RecHubData.factBatchSummary.CheckCount,
		  RecHubData.factBatchSummary.CheckTotal,
		  #tmpEntities.BreadCrumb AS EntityName,																-- WI 151507
		  CONVERT(VARCHAR(20),(RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID)) + ISNULL(' - ' + CONVERT(VARCHAR(60),(RecHubData.dimClientAccounts.LongName)), '') AS Workgroup,  -- WI 157984	   
		  COALESCE(RecHubData.dimBatchSources.LongName, RecHubData.dimBatchSources.ShortName) AS LongName,
		  RecHubData.dimBatchPaymentTypes.LongName AS PaymentType,
		  CONVERT(VARCHAR(10),RecHubData.factBatchSummary.DepositDateKey) AS PaymentDate,										-- WI 139057 
		  RecHubData.dimClientAccounts.ShortName,
		  RecHubUser.SessionClientAccountEntitlements.EntityID,
		  RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
		  RecHubData.factBatchSummary.StubCount,																--WI 196347
		RechubData.factBatchSummary.SourceBatchID																--WI 196441
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubUser.SessionClientAccountEntitlements																	-- WI 139057 
				ON RecHubData.factBatchSummary.ClientAccountKey = RecHubuser.SessionClientAccountEntitlements.ClientAccountKey	-- WI 139057 
		INNER JOIN RecHubData.dimBatchSources
				ON RecHubData.factBatchSummary.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes 
				ON RecHubData.factBatchSummary.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN RecHubData.dimClientAccounts
				ON RecHubuser.SessionClientAccountEntitlements.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN #tmpEntities
				ON RecHubUser.SessionClientAccountEntitlements.EntityID = #tmpEntities.EntityID
	WHERE
		RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID		 -- WI 139057
		AND CAST(RecHubData.factBatchSummary.CreationDate AS DATE) >= @parmStartDate -- WI 160817
		AND CAST(RecHubData.factBatchSummary.CreationDate AS DATE) <= @parmEndDate	 -- WI 160817
		AND @parmPaymentSrc = CASE 
							    WHEN @parmPaymentSrc = '-- All --' THEN '-- All --'	 -- WI 157984
							    ELSE (COALESCE(RecHubData.dimBatchSources.LongName, RecHubData.dimBatchSources.ShortName))
							  END								
		AND RecHubData.factBatchSummary.IsDeleted = 0								 -- WI 160813
	ORDER BY
		RecHubData.factBatchSummary.DepositDateKey DESC,
		RecHubUser.SessionClientAccountEntitlements.EntityID,
		RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
		COALESCE(RecHubData.dimBatchSources.LongName, RecHubData.dimBatchSources.ShortName),
		RecHubData.dimBatchPaymentTypes.LongName,
		RecHubData.factBatchSummary.BatchNumber;

       DECLARE @auditMessage VARCHAR(1024) = 'Import Reconciliation executed for parameters -'
                                                                           + ' Date Range: ' + CONVERT(VARCHAR(8), @parmStartDate, 112) + ' - ' + CONVERT(VARCHAR(8), @parmEndDate, 112)
									   + ' Payment Source: ' + @parmPaymentSrc;

       EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName= 'RecHubAudit.usp_Report_ImportReconciliation' , @parmEventName='Report Executed', @parmEventType='Reports', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

	END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH