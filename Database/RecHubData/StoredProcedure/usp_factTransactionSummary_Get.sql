--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_factTransactionSummary_Get') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_factTransactionSummary_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factTransactionSummary_Get 
(
       @parmSessionID			UNIQUEIDENTIFIER,
	   @parmSiteBankID			INT, 
       @parmSiteClientAccountID	INT,
       @parmDepositDate			DATETIME,
       @parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 03/17/2009
*
* Purpose: Query Batch Summary fact table by Bank, Box, Deposit Date Range
*
* Modification History
* 03/17/2009 CR 25817  JMC	Created.
* 12/30/2009 CR 28080  CS   Modified to use OLTA.factTransactionSummary instead of the UNION syntax
*                           and removed factChecks.GlobalBatchID from the SELECT.
* 07/30/2012 CR 54610  JNE  Add BatchNumber to result set.
* 04/01/2013 WI 90672  JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_GetBatchTransactions.
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID.
*							Change All references of Lockbox to ClientAccount.
*							Foward Patch: WI 85065.
* 05/23/2014 WI 142863 PKW  Batch Collision - RAAM Integration.
* 08/19/2014 WI 159411 JMC  Because of multiple Key values, dupes are likely.  Need DISTINCT.
* 12/24/2014 WI 182727 TWE  Add SessionID to join
* 03/24/2015 WI 197793 MAA  added @parmDepositDateEnd to usp_AdjustStartDateForViewingDays
*							so it respects the viewahead  
************************************************************************************************/
SET NOCOUNT ON; 

DECLARE 
		@EndDateKey			INT,
		@StartDateKey		INT; 

BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	-- FP WI85065
	SELECT DISTINCT
			RecHubUser.SessionClientAccountEntitlements.SiteBankID AS BankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID AS ClientAccountID,
			@parmDepositDate AS DepositDate,
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.SourceBatchID, 
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence,
			RecHubData.factTransactionSummary.BatchNumber
	FROM	
			RecHubData.factTransactionSummary
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142863
			    RecHubuser.SessionClientAccountEntitlements.sessionid = @parmSessionID                      --WI 182727
				AND RecHubData.factTransactionSummary.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey --WI 142863
	WHERE  	
			RecHubData.factTransactionSummary.DepositDateKey = @EndDateKey   								--WI 142863  WI 197793
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142863
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142863
			AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
			AND RecHubData.factTransactionSummary.DepositStatus >= 850
			AND RecHubData.factTransactionSummary.IsDeleted = 0	
	ORDER BY TxnSequence
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH