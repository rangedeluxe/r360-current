--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ByImmutableDate
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubData.usp_factChecks_Get_ByImmutableDate') IS NOT NULL
	DROP PROCEDURE RecHubData.usp_factChecks_Get_ByImmutableDate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_factChecks_Get_ByImmutableDate
(
       @parmSiteBankID			INT, 
       @parmSiteClientAccountID	INT,
       @parmImmutableDateKey	INT,
       @parmBatchID				BIGINT,
       @parmBatchSequence		INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 04/08/2011
*
* Purpose: 
*
* Modification History
* 04/08/2011 CR 33798 JMC	Created
* 04/02/2013 WI 90674 JBS	Update to 2.0 release. Change schema name
*							Rename proc from usp_GetCheck2
*							Change Parameters: @parmSiteLockboxID to @parmSiteClientAccountID,
*							@parmProcessingDateKey  to  @parmImmutableDateKey
*							Change References: Lockbox to ClientAccount,  Processing to Immutable.
*							Customer to Organization.
* 06/04/2013 WI 103851 JMC	Added additional output data fields for Extract Wizard.
* 06/25/2014 WI 142834 JBS	Only adding change for BatchID to BIGINT.  SessionID not required
* 10/09/2014 WI 170773 TWE  Return SourceProcessingDateKey,sourcebatchid,batchnumber in result set
* 07/17/2015 WI 224756 TWE  Return the BatchSourceKey
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BankKey			INT;
DECLARE @ClientAccountKey	INT;
DECLARE @DepositDateKey		INT;
DECLARE @DepositDate		DATETIME;

BEGIN TRY
	
	EXEC RecHubData.usp_GetDepositDateKey_BySiteID
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmBatchID = @parmBatchID,
		@parmImmutableDateKey = @parmImmutableDateKey,
		@parmBankKey = @BankKey OUTPUT,
		@parmClientAccountKey = @ClientAccountKey OUTPUT,
		@parmDepositDateKey = @DepositDateKey OUTPUT

	SET @DepositDate = CONVERT(VARCHAR, CONVERT(DATETIME, CONVERT(VARCHAR(10), @DepositDateKey), 112), 101);

	SELECT	RecHubData.factChecks.BankKey,
			RecHubData.factChecks.OrganizationKey,
			RecHubData.factChecks.ClientAccountKey,
			RecHubData.factChecks.DepositDateKey,
			@DepositDate AS DepositDate,
			RecHubData.factChecks.ImmutableDateKey,
			RecHubData.factChecks.SourceProcessingDateKey,
			RecHubData.dimClientAccounts.SiteBankID,
			RecHubData.dimClientAccounts.SiteClientAccountID,
			RecHubData.factChecks.BatchID,
			RecHubData.factChecks.SourceBatchID,
			RecHubData.factChecks.BatchNumber,
			RecHubData.factChecks.BatchSourceKey,
			RecHubData.factChecks.TransactionID,
			RecHubData.factChecks.BatchSequence,
			RecHubData.factChecks.RoutingNumber,
			RecHubData.factChecks.Account,
			RecHubData.factChecks.Serial,
			RecHubData.factChecks.Amount
	FROM RecHubData.factChecks
		INNER JOIN RecHubData.dimClientAccounts on RecHubData.factChecks.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	WHERE  	RecHubData.factChecks.ClientAccountKey = @ClientAccountKey
			AND RecHubData.factChecks.DepositDateKey = @DepositDateKey
			AND RecHubData.factChecks.ImmutableDateKey = @parmImmutableDateKey
			AND RecHubData.factChecks.BatchID = @parmBatchID
			AND RecHubData.factChecks.BatchSequence = @parmBatchSequence
			AND RecHubData.factChecks.IsDeleted = 0
	OPTION (RECOMPILE);

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO
