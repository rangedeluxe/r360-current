--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorStoredProcedureName usp_dimDocumentTypes_Upd_DocumentTypeDescription
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription') IS NOT NULL
       DROP PROCEDURE RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription
(
	@parmFileDescriptor	 VARCHAR(30),
	@parmDescription	 VARCHAR(64),
	@parmUserId			 INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 10/4/2011
*
* Purpose: Update document Types
*
* Modification History
* 10/04/2011 CR 47152 WJS	Created
* 02/23/2012 CR 50526 WJS 	Moved to OLTA schema.  For FileDescriptors that
*                           exist in dbo, we update dbo only and allow OTIS to
*                           replicate the change into OLTA.  For FileDescriptors
*                           that exist only in OLTA, we update them directly.
* 03/29/2013 WI 90799 JPB	Upgraded to 2.0.
*							Moved to RecHubData schema.
*							Renamed from usp_UpdateDocumentTypesDescription.
* 09/15/2014 WI 163470 BLR  Added data auditing, UserID for param input.
* 10/28/2014 WI 174479 BLR  Altered to persist column IsReadOnly.
* 05/12/2015 WI 212696 JPB	Updated auditing with before and after values.
* 06/03/2015 WI 216746 LA   Removed ID from Audit Message
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @AuditMessage VARCHAR(MAX),
		@RowsUpdated INT,
		@SyncDocumentTypes BIT = 0,
		@DocumentTypeId INT,
		@prevDescription VARCHAR(64),
		@AuditColumns RecHubCommon.AuditValueChangeTable;

BEGIN TRY

	/* 
		If the DocumentTypeDescription is NULL or blank, then update the DocumentTypeDescription with the new value. Else, update the current records to MostActive to 1 and
		create a new record.
	*/

	--Look in system setup to determine if this is being synced to integraPAY
	SELECT 
		@SyncDocumentTypes = CAST(Value AS BIT)
	FROM
		RecHubConfig.SystemSetup
	WHERE
		Section = 'integraPAY'
		AND SetupKey = 'SyncDocumentTypes'

	/* If not syncing to integraPAY, then update R360Hub Directly. (This would be only be true at non integraPAY sites.) */
	IF( @SyncDocumentTypes = 0 )
	BEGIN
		IF EXISTS( SELECT 1 FROM RecHubData.dimDocumentTypes WHERE FileDescriptor = @parmFileDescriptor AND (DocumentTypeDescription IS NULL OR DocumentTypeDescription = '' AND MostRecent = 1) )
		BEGIN

			BEGIN TRANSACTION dimDocumentTypes_Upd;
			
			-- Grab the ID for auditing
			SELECT 
				@DocumentTypeId = RecHubData.dimDocumentTypes.DocumentTypeKey,
				@prevDescription =  RecHubData.dimDocumentTypes.DocumentTypeDescription
			FROM 
				RecHubData.dimDocumentTypes
			WHERE 
				FileDescriptor = @parmFileDescriptor
				AND MostRecent = 1;

			-- Update our Fields
			UPDATE 
				RecHubData.dimDocumentTypes
			SET 
				DocumentTypeDescription = @parmDescription
			WHERE 
				FileDescriptor = @parmFileDescriptor
				AND MostRecent = 1;

			IF @prevDescription <> @parmDescription
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Description', @prevDescription,@parmDescription);

			-- Perform audit
			SET @AuditMessage = 'Updated DocumentType, Id: ' + CAST(@DocumentTypeId AS VARCHAR(10)) 
				+ ', FileDescriptor: ' + @parmFileDescriptor
				+ ', Description: ' + @parmDescription;

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserId,
				@parmApplicationName	= 'RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription',
				@parmSchemaName			= 'RecHubData',
				@parmTableName			= 'dimDocumentTypes',
				@parmColumnName			= 'DocumentTypeKey',
				@parmAuditValue			= @DocumentTypeId,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @AuditMessage;

			COMMIT TRANSACTION dimDocumentTypes_Upd;
		END
		ELSE
		BEGIN
			/* Store the existing columns */
			DECLARE @OldData TABLE
			(
				IMSDocumentType VARCHAR(30),
				CreationDate DATETIME,
				IsReadOnly BIT,
				PrevDescription VARCHAR(64)
			);

			BEGIN TRAN;
			/* Update the most recent to 0, saving the current columns to the temp table */
			UPDATE 
				RecHubData.dimDocumentTypes
			SET 
				MostRecent = 0,
				ModifiedBy = SUSER_SNAME(),
				ModificationDate = GETDATE()
			OUTPUT	
				deleted.IMSDocumentType,
				deleted.CreationDate,
				deleted.IsReadOnly,
				deleted.DocumentTypeDescription
			INTO 
				@OldData
			WHERE 
				FileDescriptor = @parmFileDescriptor and MostRecent = 1;

			/* Set number of rows updated */
			SET @RowsUpdated = @@ROWCOUNT;

			/* Insert the new row, using the old columns as needed */
			INSERT INTO RecHubData.dimDocumentTypes
			(
				FileDescriptor,
				DocumentTypeDescription,
				IMSDocumentType,
				MostRecent,
				CreationDate,
				ModificationDate,
				ModifiedBy,
				IsReadOnly
			)
			SELECT TOP 1
				@parmFileDescriptor,
				@parmDescription,
				IMSDocumentType,
				1,
				GETDATE(),
				GETDATE(),
				SUSER_SNAME(),
				IsReadOnly
			FROM
				@OldData
			ORDER BY
				CreationDate DESC;
			
			-- Perform Data Audit
			SET @DocumentTypeId = SCOPE_IDENTITY();

			SELECT TOP 1
				@prevDescription = PrevDescription
			FROM 
				@OldData;

			IF @prevDescription <> @parmDescription
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Description', @prevDescription,@parmDescription);
			SET @AuditMessage = 'Updated  FileDescriptor: ' + @parmFileDescriptor
				+ ', Description: ' +  @parmDescription;
		
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserId,
				@parmApplicationName	= 'RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription',
				@parmSchemaName			= 'RecHubData',
				@parmTableName			= 'dimDocumentTypes',
				@parmColumnName			= 'DocumentTypeKey',
				@parmAuditValue			= @DocumentTypeId,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @AuditMessage;

			SET @AuditMessage = 'There were ' + CAST(@RowsUpdated AS VARCHAR) + ' "' + @parmFileDescriptor + '" records set to MostRecent=1';
			IF( @RowsUpdated > 1 ) /* There was more then one row with MostRecent = 1, log the error */
				EXEC RecHubCommon.usp_WFS_DataAudit_Ins
					@parmApplicationName		= 'RecHubData.usp_dimDocumentTypes_Upd_DocumentTypeDescription',
					@parmSchemaName				= 'RecHubData',
					@parmTableName				= 'dimDocumentTypes',
					@parmColumnName				= 'FileDescriptor',
					@parmAuditType				= 'SYSERROR',
					@parmAuditValue				= @parmFileDescriptor,
					@parmAuditMessage			= @AuditMessage;

			COMMIT TRAN;
		END
	END
	ELSE
		EXEC RecHubSystem.usp_CDSQueue_Ins_DocumentType @parmFileDescriptor=@parmFileDescriptor, @parmFileDescription=@parmDescription, @parmActionCode=1;

END TRY
BEGIN CATCH
	IF (XACT_STATE()) = -1 --transaction is uncommittable
		ROLLBACK TRANSACTION
	IF (XACT_STATE()) = 1 --transaction is active and valid
		COMMIT TRANSACTION
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
