--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName InvoiceSearchBaseKeysTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.InvoiceSearchBaseKeysTable AS TABLE
(
	factStubKey BIGINT,
	DepositDateKey INT,
	BatchSourceKey SMALLINT,
	BatchPaymentTypeKey TINYINT,
	EntityID INT,
	SiteBankID INT,
	SiteClientAccountID INT,
	SessionID UNIQUEIDENTIFIER
);
