--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName EntityBreadcrumbsRequestTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.EntityBreadcrumbsRequestTable AS TABLE
(
	EntityID INT,
	EntityBreadcrumb NVARCHAR(256)
);