--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName InvoiceSearchQueryBuilderTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.InvoiceSearchQueryBuilderTable AS TABLE
(
	FieldName VARCHAR(128),
	Operator VARCHAR(36),
	Value VARCHAR(128)
);
