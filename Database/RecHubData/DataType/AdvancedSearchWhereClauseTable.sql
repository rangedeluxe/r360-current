--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorDataTypeName AdvancedSearchWhereClauseTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.AdvancedSearchWhereClauseTable AS TABLE
(
	TableName VARCHAR(64),
	FieldName VARCHAR(256),
	DataType INT,
	ReportTitle VARCHAR(256),
	Operator VARCHAR(32),
	BatchSourceKey SMALLINT,
	DataValue VARCHAR(80)
);