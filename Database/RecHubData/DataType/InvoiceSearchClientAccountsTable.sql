--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName InvoiceSearchClientAccountsTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.InvoiceSearchClientAccountsTable AS TABLE
(
	SessionID UNIQUEIDENTIFIER,
	SiteBankID INT,
	SiteClientAccountID INT,
	EntityID INT,
	ClientAccountKey INT,
	StartDateKey INT,
	EndDateKey INT,
	ViewAhead BIT
);

