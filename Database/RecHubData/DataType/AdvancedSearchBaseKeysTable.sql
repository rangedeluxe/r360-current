--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName AdvancedSearchBaseKeysTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.AdvancedSearchBaseKeysTable AS TABLE
(
	BankID INT,
	ClientAccountID INT,
	DateFrom DATE,
	DateTo DATE,
	BatchIDFrom BIGINT NULL,
	BatchIDTo BIGINT NULL,
	BatchNumberFrom BIGINT NULL,
	BatchNumberTo BIGINT NULL,
	AmountFrom MONEY NULL,
	AmountTo MONEY NULL,
	SessionID UNIQUEIDENTIFIER,
	COTSOnly VARCHAR(8),
	MarkSenseOnly VARCHAR(8),
	BatchPaymentTypeKey TINYINT,
	BatchSourceKey TINYINT,
	PaginateRS INT,
	StartRecord INT,
	RecordsPerPage TINYINT,
	SortBy VARCHAR(32),
	SortByDir VARCHAR(4),
	SortByDisplayName VARCHAR(32),
	Serial VARCHAR(30) NULL,
	DisplayScannedCheck VARCHAR(8)
);
