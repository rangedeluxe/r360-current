--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorDataTypeName AdvancedSearchSelectFieldsTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.AdvancedSearchSelectFieldsTable AS TABLE
(
	TableName VARCHAR(64),
	FieldName VARCHAR(256),
	DataType INT,
	ReportTitle VARCHAR(256),
	BatchSourceKey SMALLINT
);
