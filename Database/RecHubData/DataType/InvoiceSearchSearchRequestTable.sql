--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubData
--WFSScriptProcessorDataTypeName InvoiceSearchSearchRequestTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubData.InvoiceSearchSearchRequestTable AS TABLE
(
	SessionID UNIQUEIDENTIFIER,
	DepositDateFrom DATE,
	DepositDateTo DATE,
	DepositDateFromKey INT,
	DepositDateToKey INT,
	RecordsPerPage INT,
	StartRecord INT,
	BatchSourceKey SMALLINT,
	BatchPaymentTypeKey SMALLINT,
	EntityName NVARCHAR(50),
	OrderDirection VARCHAR(4),
	OrderBy VARCHAR(64),
	SortBy VARCHAR(125),
	UserName VARCHAR(256)
);
