--WFSScriptProcessorPrint Populate Dates Dimension Static Data
--WFSScriptProcessorStaticDataName query_PopulateDatesDimension
--WFSScriptPRocessorStaticDataCreateBegin
--WI 91315  Update to 2.0 Release
IF OBJECT_ID('RecHubData.usp_PopulateDatesDimension') IS NOT NULL
BEGIN
	EXEC RecHubData.usp_PopulateDatesDimension '2000-01-01', '2050-12-31';
	DROP PROCEDURE RecHubData.usp_PopulateDatesDimension;
END
--WFSScriptPRocessorStaticDataCreateEnd