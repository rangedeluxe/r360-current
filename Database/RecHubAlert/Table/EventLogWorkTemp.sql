--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTable EventLogWorkTemp
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHubAlert.EventLogWorkTemp') IS NOT NULL
       DROP TABLE RecHubAlert.EventLogWorkTemp;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 06/20/2013
*
* Purpose: Working table for the Event Log processing for R360 Alerts SSIS pkg.
*			This table will remain between runs of pkg.
*
* EventRowID will be used as row counter in SSIS pkg.
*
* Check Constraint definitions:
* EventStatus
*	10 - Ready to Process
*   15 - Error, can be reprocessed
*   20 - In Process
*   30 - Error, cannot be reprocessed
*   99 - Complete
*	
* Creation Date = Date/Time record was inserted into Table
* Modification Date = Date/Time the record was changed
*
* Modification History
* 06/15/2013 WI 109188 JBS	Created
* 08/14/2014 WI 158581 JBS	Change EventSourceKey to SMALLINT
* 06/03/2015 WI 158581 JBS	Adding index IDX_EventLogWorkTemp_EventLogID
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.EventLogWorkTemp
(
	EventRowID			BIGINT NOT NULL IDENTITY(1,1),
	EventLogID			BIGINT NOT NULL,
	EventRuleID			BIGINT NOT NULL,
	AlertID				BIGINT NOT NULL,
	EventStatus			SMALLINT NOT NULL,
	NotificationMessageGroup BIGINT NULL,
	BankKey				INT NOT NULL,
	OrganizationKey		INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	EventSourceKey		SMALLINT NOT NULL,
	CreationDate		DATETIME NULL,
	ModificationDate	DATETIME NULL,
	EventMessage		VARCHAR(MAX) NULL,
	NewEventStatus		SMALLINT	NULL,
	NotificationDateInserted	DATETIME NULL,
	EmailAddress		VARCHAR(256) NULL,
	NotificationDateSent		DATETIME NULL
);
--WFSScriptProcessorTableProperties
-- 158581
--WFSScriptProcessorIndex RecHubAlert.EventLogWorkTemp.IDX_EventLogWorkTemp_EventLogID
CREATE INDEX IDX_EventLogWorkTemp_EventLogID ON RecHubAlert.EventLogWorkTemp
(
	EventLogID
) 
INCLUDE 
(
	BankKey, 
	OrganizationKey, 
	ClientAccountKey, 
	EventSourceKey
);