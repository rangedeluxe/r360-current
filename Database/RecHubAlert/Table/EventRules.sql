--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTable EventRules
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores Event Rules for R360 Alerts.
*
*
* Modification History
* 05/03/2013 WI 99838 JBS	Created.  Removing EventLevel Column
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.EventRules
(
	EventRuleID			BIGINT IDENTITY
		CONSTRAINT PK_EventRules PRIMARY KEY CLUSTERED,
	EventID				SMALLINT NOT NULL,
	SiteCodeID			INT NOT NULL,
	SiteBankID			INT NULL,
	SiteOrganizationID	INT NULL,
	SiteClientAccountID INT NULL,
	IsActive			BIT NOT NULL,
	EventRuleDescription VARCHAR(30) NULL,
	EventRuleOperator	 VARCHAR(10) NULL,
	EventRuleValue		 VARCHAR(80) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubAlert.EventRules.IDX_EventRules_SiteBankOrganizationClientAccountID
CREATE INDEX IDX_EventRules_SiteBankOrganizationClientAccountID ON RecHubAlert.EventRules 
(
	SiteBankID,
	SiteOrganizationID,
	SiteClientAccountID
);
--WFSScriptProcessorIndex RecHubAlert.EventRules.IDX_EventRules_EventRuleEventIDIsActive
CREATE INDEX IDX_EventRules_EventRuleEventIDIsActive ON RecHubAlert.EventRules (EventRuleID,EventID,IsActive);
