--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTable EventLog
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores Event Logs for R360 Alerts.
*
*
* Check Constraint definitions:
* EventStatus
*	10 - Ready to Process
*	15 - Error, can be reprocessed
*	20 - In Process
*	30 - Error, cannot be reprocessed
*	99 - Complete
*	
*
* Modification Date = Date/Time the record was changed
* Modification History
* 05/03/2013 WI 99840 JBS	Created
* 06/03/2014 WI 143688 JPB	Changed EventSourceKey to SMALLINT.
* 06/04/2015 WI 216999 JBS	Add index IDX_EventLog_AlertID
* 07/21/2017 PT 148855865	JPB	Added IDX_EventLog_EventStatus
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.EventLog
(
	EventLogID			BIGINT IDENTITY
		CONSTRAINT PK_EventLog PRIMARY KEY CLUSTERED,
	EventRuleID			BIGINT NOT NULL,
	AlertID				BIGINT NOT NULL,
	NotificationMessageGroup BIGINT NULL,
	EventStatus			SMALLINT NOT NULL
		CONSTRAINT CK_EventRules_EventStatus CHECK (EventStatus IN (10,15,20,30,99)),
	BankKey				INT NOT NULL,
	OrganizationKey		INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	EventSourceKey		SMALLINT NOT NULL,
	CreationDate		DATETIME NULL,
	ModificationDate	DATETIME NULL,
	EventMessage		VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAlert.EventLog ADD
	CONSTRAINT FK_EventLog_EventRules FOREIGN KEY(EventRuleID) REFERENCES RecHubAlert.EventRules(EventRuleID),
	CONSTRAINT FK_EventLog_Alerts FOREIGN KEY(AlertID) REFERENCES RecHubAlert.Alerts(AlertID),
	CONSTRAINT FK_EventLog_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_EventLog_dimOrganizations FOREIGN KEY(OrganizationKey) REFERENCES RecHubData.dimOrganizations(OrganizationKey),
	CONSTRAINT FK_EventLog_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey),
	CONSTRAINT FK_EventLog_dimBatchSources FOREIGN KEY(EventSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey);
--WFSScriptProcessorIndex RecHubAlert.EventLog.IDX_EventLog_AlertID
CREATE INDEX IDX_EventLog_AlertID ON RecHubAlert.EventLog 
(
	AlertID
) 
INCLUDE 
(
	EventLogID, 
	EventRuleID, 
	NotificationMessageGroup, 
	EventStatus,
	BankKey,
	OrganizationKey, 
	ClientAccountKey, 
	EventSourceKey, 
	CreationDate, 
	ModificationDate, 
	EventMessage
);
--PT 148855865
--WFSScriptProcessorIndex RecHubAlert.EventLog.IDX_EventLog_EventStatus
CREATE NONCLUSTERED INDEX IDX_EventLog_EventStatus ON RecHubAlert.EventLog
(
	EventStatus
)
INCLUDE 
(
	EventLogID,
	EventRuleID,
	AlertID
);
