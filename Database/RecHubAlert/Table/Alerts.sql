--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTable Alerts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores Alerts for R360 Alerts.
*
*
* Modification History
* 05/06/2013 WI 99839  JBS	Created
* 01/26/2015 WI 186370 JBS	Add IsActive, CreationDate and ModificationDate
* 05/27/2015 WI 204445 CMC  Add IsAssigned
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.Alerts
(
	AlertID				BIGINT IDENTITY
		CONSTRAINT PK_Alerts PRIMARY KEY CLUSTERED,
	UserID				INT NULL,
	DeliveryMethodID	SMALLINT NOT NULL,
	EventRuleID			BIGINT NOT NULL,
	IsActive			BIT	NOT NULL
		CONSTRAINT DF_Alerts_IsActive DEFAULT(1),
	IsAssigned			BIT NULL
		CONSTRAINT DF_Alerts_IsAssigned DEFAULT(1),
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_Alerts_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_Alerts_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_Alerts_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_Alerts_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAlert.Alerts ADD
	CONSTRAINT FK_Alerts_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_Alerts_EventRules FOREIGN KEY(EventRuleID) REFERENCES RecHubAlert.EventRules(EventRuleID),
	CONSTRAINT FK_Alerts_DeliveryMethods FOREIGN KEY(DeliveryMethodID) REFERENCES RecHubAlert.DeliveryMethods(DeliveryMethodID);
