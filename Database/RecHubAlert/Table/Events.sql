--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTableName Events
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/09/2009
*
* Purpose: Stores Events for R360 Alerts
*	
* Check Constraint definitions:
* EventLevel
*	0: System Level
*	1: Site
*	2: Bank
*	3: ClientAccount	   
*
* Modification History
* 05/03/2013 WI 99837 JBS	Created.  Adding EventLevel column
* 05/08/2014 WI 140814 JPB	Added EventSubject and Modification columns.
* 05/09/2014 WI 140890 JBS	Change Constraint CK_Events_EventLevel to allow value 0 for System level Events
* 02/06/2015 WI 188722 JPB	Added EventLongName column to contain user friendly event name.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.[Events]
(
	EventID				SMALLINT NOT NULL
		CONSTRAINT PK_Events PRIMARY KEY CLUSTERED,
	EventName			VARCHAR(30) NOT NULL,
	EventType			TINYINT NOT NULL
		CONSTRAINT CK_Events_EventType CHECK (EventType IN (0,1)),
	EventLevel			TINYINT NOT NULL
		CONSTRAINT CK_Events_EventLevel CHECK (EventLevel IN (0,1,2,3)),
	IsActive			BIT NOT NULL,
	EventLongName		VARCHAR(64) NOT NULL,
	MessageDefault		VARCHAR(1028) NOT NULL,
	MessageSubject		VARCHAR(64) NULL,
	EventSchema			VARCHAR(128) NULL,
	EventTable			VARCHAR(128) NULL,
	EventColumn			VARCHAR(128) NULL,
	EventOperators		VARCHAR(64) NULL,
	ModificationDate	DATETIME NOT NULL,
	ModifiedBy			VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties