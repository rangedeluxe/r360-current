--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorTable DeliveryMethods
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/03/2013
*
* Purpose: Stores DeliveryMethod Names for R360 Alerts.
*
*
* Modification History
* 05/06/2013 WI 99841 JBS	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAlert.DeliveryMethods
(
	DeliveryMethodID	SMALLINT NOT NULL	
		CONSTRAINT PK_DeliveryMethods PRIMARY KEY CLUSTERED,
	DeliveryMethodName	VARCHAR(30)
);
--WFSScriptProcessorTableProperties