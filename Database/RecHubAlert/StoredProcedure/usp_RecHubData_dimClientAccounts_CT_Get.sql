--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubData_dimClientAccounts_CT_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubData_dimClientAccounts_CT_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubData_dimClientAccounts_CT_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubData_dimClientAccounts_CT_Get
(
	@parmStartLSN NVARCHAR(42),
	@parmEndLSN NVARCHAR(42)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/28/2013
*
* Purpose: Retrieve dim client account CDC data by LSN range.
*
*
* Modification History
* 05/28/2013 WI 103921 JPB	Created
* 05/05/2014 WI 139621 JPB	Account for NULL EventRules.SiteOrganizationID. (FP:139612)
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY

	/* conver the string LSNs to hex */
	SELECT	
		@StartLSN = sys.fn_cdc_hexstrtobin(@parmStartLSN),
		@EndLSN = sys.fn_cdc_hexstrtobin(@parmEndLSN);

	;WITH CDCRecords AS
	(
		SELECT 
			MAX(__$start_lsn) AS __$start_lsn,
			MAX(__$seqval) AS __$seqval,
			CDCdimClientAccounts.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
		FROM 
			RecHubAlert.[Events] 	
			INNER JOIN RecHubAlert.EventRules ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
			INNER JOIN RecHubData.dimClientAccounts ON RecHubAlert.EventRules.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
				AND COALESCE(RecHubAlert.EventRules.SiteBankID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 2 THEN RecHubData.dimClientAccounts.SiteBankID ELSE COALESCE(RecHubAlert.EventRules.SiteBankID,-1) END
				AND COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) = 
					CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN
						CASE WHEN RecHubData.dimClientAccounts.SiteOrganizationID IS NOT NULL
							THEN RecHubData.dimClientAccounts.SiteOrganizationID 
							ELSE COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) 
						END
				END
				AND COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN RecHubData.dimClientAccounts.SiteClientAccountID ELSE COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) END
			INNER JOIN cdc.fn_cdc_get_all_changes_RecHubData_dimClientAccounts(@StartLSN,@EndLSN,'all update old') AS CDCdimClientAccounts
				ON RecHubData.dimClientAccounts.ClientAccountKey = CDCdimClientAccounts.ClientAccountKey
		WHERE 
			RecHubAlert.EventRules.IsActive = 1 
			AND RecHubAlert.[Events].IsActive = 1
			AND RecHubAlert.[Events].EventSchema = 'RecHubData'
			AND RecHubAlert.[Events].EventTable = 'dimClientAccounts'
		GROUP BY 
			CDCdimClientAccounts.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
	)
	SELECT 
		cdc.RecHubData_dimClientAccounts_CT.__$start_lsn,
		cdc.RecHubData_dimClientAccounts_CT.__$seqval,
		cdc.RecHubData_dimClientAccounts_CT.__$update_mask,
		cdc.RecHubData_dimClientAccounts_CT.__$operation,
		NULL AS BankKey,
		NULL AS OrganizationKey,
		ClientAccountKey,
		CDCRecords.SiteCodeID,
		CDCRecords.SiteBankID,
		CDCRecords.SiteOrganizationID,
		CDCRecords.SiteClientAccountID,
		cdc.RecHubData_dimClientAccounts_CT.Cutoff,
		cdc.RecHubData_dimClientAccounts_CT.ShortName,
		CDCRecords.EventLevel
	FROM cdc.RecHubData_dimClientAccounts_CT
		INNER JOIN CDCRecords ON CDCRecords.__$start_lsn = cdc.RecHubData_dimClientAccounts_CT.__$start_lsn
			AND COALESCE(CDCRecords.__$seqval,-1) = COALESCE(cdc.RecHubData_dimClientAccounts_CT.__$seqval,-1)
			AND CDCRecords.SiteCodeID = cdc.RecHubData_dimClientAccounts_CT.SiteCodeID
			AND COALESCE(CDCRecords.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN cdc.RecHubData_dimClientAccounts_CT.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND COALESCE(CDCRecords.SiteOrganizationID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN cdc.RecHubData_dimClientAccounts_CT.SiteOrganizationID ELSE COALESCE(CDCRecords.SiteOrganizationID,-1) END
			AND COALESCE(CDCRecords.SiteClientAccountID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN cdc.RecHubData_dimClientAccounts_CT.SiteClientAccountID ELSE COALESCE(CDCRecords.SiteClientAccountID,-1) END
			AND CDCRecords.__$operation = cdc.RecHubData_dimClientAccounts_CT.__$operation;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
