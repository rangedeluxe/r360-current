--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_Events_Get_Names
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_Events_Get_Names') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_Events_Get_Names
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_Events_Get_Names

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/10/2013
*
* Purpose: Get Events for Lookup
*
* Modification History
* 06/10/2013  WI  105399	EAS	Created
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY

	SELECT   RecHubAlert.[Events].EventID, RecHubAlert.[Events].EventName
	FROM     RecHubAlert.[Events]
	WHERE    RecHubAlert.[Events].IsActive = 1
	ORDER BY RecHubAlert.[Events].EventName

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH