--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_Events_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_Events_Upd') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_Events_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_Events_Upd
(
	@parmEventName		VARCHAR(30),
	@parmMessageSubject	VARCHAR(64)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/08/2014
*
* Purpose: Get All Events
*
* Modification History
* 05/08/2014 WI 140818 JPB	Created.
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY

	UPDATE 
		RecHubAlert.[Events]
	SET
		MessageSubject = @parmMessageSubject
	WHERE
		EventName = @parmEventName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH