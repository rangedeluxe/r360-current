--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_EventRules_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get
(	
	@parmSessionID				UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/16/2014
*
* Purpose: Get All Event Rules
*
* Modification History
* 06/16/2014 WI 148553 EAS	Created
* 08/21/2014 WI 160622 EAS    Add Client Account Key to resultset
* 11/26/2014 WI 160622 KLC	Updated to enforce workgroup permissions
*								 replaced client account key with entity id
*								 replaced WG LongName with DisplayLabel
* 02/09/2015 WI 188966 JPB	Added EventLongName to result set.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	;WITH CTE_UsersWorkgroups AS
	(
		SELECT DISTINCT 
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel,
			RecHubUser.SessionClientAccountEntitlements.EntityID
		FROM 
			RecHubUser.SessionClientAccountEntitlements
			INNER JOIN RecHubUser.OLWorkgroups /*joining through here in case the workgroup association is changed while the user is logged in and no longer belongs to the entity*/
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
				AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
				AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
			INNER JOIN RecHubData.dimClientAccountsView	ON RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
				AND RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
		WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	)
	SELECT	 
		RecHubAlert.EventRules.EventRuleID,
		RecHubAlert.EventRules.EventRuleDescription AS [Description],
		RecHubAlert.[Events].EventID, 
		RecHubAlert.[Events].EventName,
		RecHubAlert.[Events].EventLongName,
		RecHubAlert.[Events].EventType,
		RecHubAlert.EventRules.SiteCodeID,
		RecHubAlert.EventRules.SiteBankID,
		RecHubAlert.EventRules.SiteClientAccountID,
		RecHubAlert.EventRules.IsActive,
		CTE_UsersWorkgroups.EntityID,
		CTE_UsersWorkgroups.DisplayLabel
	FROM 
		RecHubAlert.EventRules
		INNER JOIN RecHubAlert.[Events] ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
		LEFT OUTER JOIN CTE_UsersWorkgroups	ON RecHubAlert.EventRules.SiteBankID = CTE_UsersWorkgroups.SiteBankID
			AND RecHubAlert.EventRules.SiteClientAccountID = CTE_UsersWorkgroups.SiteClientAccountID
	WHERE 
		RecHubAlert.EventRules.SiteClientAccountID = -1 OR CTE_UsersWorkgroups.SiteClientAccountID IS NOT NULL;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
