--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get_ClientAccountKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get_ClientAccountKeys') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventRules_Get_ClientAccountKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get_ClientAccountKeys
(
	@parmEventSchema	VARCHAR(128),
	@parmEventTable		VARCHAR(128)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/02/2013
*
* Purpose: Retrieve all EventRules.
*
*
* Modification History
* 06/02/2013 WI 104171 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT 
		RecHubAlert.EventRules.SiteCodeID,
		RecHubAlert.EventRules.SiteBankID,
		RecHubAlert.EventRules.SiteOrganizationID,
		RecHubAlert.EventRules.SiteClientAccountID,
		RecHubData.dimBanks.BankKey,
		RecHubData.dimOrganizations.OrganizationKey,
		RecHubData.dimClientAccounts.ClientAccountKey
	FROM
		RecHubAlert.EventRules
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteCodeID = RecHubAlert.EventRules.SiteCodeID
			AND RecHubData.dimClientAccounts.SiteBankID = RecHubAlert.EventRules.SiteBankID
			AND RecHubData.dimClientAccounts.SiteOrganizationID = RecHubAlert.EventRules.SiteOrganizationID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubAlert.EventRules.SiteClientAccountID
		INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
		INNER JOIN RecHubData.dimOrganizations ON RecHubData.dimOrganizations.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
			AND RecHubData.dimOrganizations.SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID
		INNER JOIN RecHubAlert.[Events] ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
	WHERE
		RecHubAlert.EventRules.IsActive = 1
		AND RecHubAlert.[Events].EventSchema = @parmEventSchema
		AND RecHubAlert.[Events].EventTable = @parmEventTable;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
