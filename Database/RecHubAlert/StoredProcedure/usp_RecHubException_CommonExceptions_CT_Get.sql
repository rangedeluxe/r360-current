--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubException_CommonExceptions_CT_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubException_CommonExceptions_CT_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubException_CommonExceptions_CT_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubException_CommonExceptions_CT_Get
(
	@parmStartLSN NVARCHAR(42),
	@parmEndLSN NVARCHAR(42)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/28/2013
*
* Purpose: Retrieve Common Exceptions CDC data by LSN range.
*
*
* Modification History
* 05/28/2013 WI 108889 JPB	Created.  changed reference for EventLevel column
* 11/17/2014 WI 178270 JPB	Updated for schema changes.
* 01/15/2015 WI 178270 JBS	Change Column name from CommonExceptionID to IntegraPAYExceptionKey
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY

	SELECT	
		@StartLSN = sys.fn_cdc_hexstrtobin(@parmStartLSN),
		@EndLSN = sys.fn_cdc_hexstrtobin(@parmEndLSN);

	;WITH CDCRecords AS
	(
		SELECT 
			MAX(__$start_lsn) AS __$start_lsn,
			MAX(__$seqval) AS __$seqval,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
		FROM 
			RecHubAlert.[Events] 
			INNER JOIN RecHubAlert.EventRules ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
			INNER JOIN cdc.fn_cdc_get_all_changes_RecHubException_IntegraPAYExceptions(@StartLSN,@EndLSN,'all update old') AS CDCCommonExceptions
				ON COALESCE(RecHubAlert.EventRules.SiteBankID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 2 THEN CDCCommonExceptions.SiteBankID ELSE COALESCE(RecHubAlert.EventRules.SiteBankID,-1) END
				AND COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN CDCCommonExceptions.SiteClientAccountID ELSE COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) END
		WHERE
			RecHubAlert.[Events].EventSchema = 'RecHubException'
			AND RecHubAlert.[Events].EventTable = 'CommonExceptions'
			AND RecHubAlert.EventRules.IsActive = 1
			AND RecHubAlert.[Events].IsActive = 1
		GROUP BY 
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
	)
	SELECT 
		cdc.RecHubException_IntegraPAYExceptions_CT.__$start_lsn,
		cdc.RecHubException_IntegraPAYExceptions_CT.__$seqval,
		cdc.RecHubException_IntegraPAYExceptions_CT.__$update_mask,
		cdc.RecHubException_IntegraPAYExceptions_CT.__$operation,
		RecHubData.dimBanks.BankKey AS BankKey,
		NULL AS OrganizationKey,
		RecHubData.dimClientAccounts.ClientAccountKey AS ClientAccountKey,
		NULL AS SiteCodeID,
		CDCRecords.SiteBankID,
		CDCRecords.SiteOrganizationID,
		CDCRecords.SiteClientAccountID,
		cdc.RecHubException_IntegraPAYExceptions_CT.BatchID,
		cdc.RecHubException_IntegraPAYExceptions_CT.IntegraPAYExceptionKey,
		cdc.RecHubException_IntegraPAYExceptions_CT.ImmutableDateKey,
		CDCRecords.EventLevel
	FROM 
		cdc.RecHubException_IntegraPAYExceptions_CT
		INNER JOIN CDCRecords ON CDCRecords.__$start_lsn = cdc.RecHubException_IntegraPAYExceptions_CT.__$start_lsn
			AND COALESCE(CDCRecords.__$seqval,-1) = COALESCE(cdc.RecHubException_IntegraPAYExceptions_CT.__$seqval,-1)
			AND COALESCE(CDCRecords.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN cdc.RecHubException_IntegraPAYExceptions_CT.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND COALESCE(CDCRecords.SiteClientAccountID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN cdc.RecHubException_IntegraPAYExceptions_CT.SiteClientAccountID ELSE COALESCE(CDCRecords.SiteClientAccountID,-1) END
			AND CDCRecords.__$operation = cdc.RecHubException_IntegraPAYExceptions_CT.__$operation
		INNER JOIN RecHubData.dimBanks ON  COALESCE(RecHubData.dimBanks.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN cdc.RecHubException_IntegraPAYExceptions_CT.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND RecHubData.dimBanks.MostRecent = 1
		INNER JOIN RecHubData.dimClientAccounts ON  COALESCE(RecHubData.dimClientAccounts.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN cdc.RecHubException_IntegraPAYExceptions_CT.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND COALESCE(RecHubData.dimClientAccounts.SiteClientAccountID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN cdc.RecHubException_IntegraPAYExceptions_CT.SiteClientAccountID ELSE COALESCE(CDCRecords.SiteClientAccountID,-1) END
			AND RecHubData.dimClientAccounts.MostRecent = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
