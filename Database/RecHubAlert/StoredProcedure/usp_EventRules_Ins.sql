--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_EventRules_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Ins') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_EventRules_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Ins
(	
	@parmUserID					INT,
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmEventID				INT,
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmIsActive				BIT,
	@parmDescription			VARCHAR(30) = NULL,
	@parmOperator				VARCHAR(10) = NULL,
	@parmValue					VARCHAR(80) = NULL,
	@parmUsers					XML,
	@parmEventRuleID			INT OUTPUT		
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/03/2013
*
* Purpose: Insert new Event Rule
*
* Modification History
* 07/03/2013 WI 108382	EAS	Created.  Removed EventLevel column
* 06/27/2014 WI 146687	EAS Changed for Portlet move.
* 08/08/2014 WI 146860	EAS Add Auditing
* 11/26/2014 WI 146860	KLC	Updated the following
*								 Added verification user has access to workgroup
*								 Fixed auditing so it audits the user
* 05/13/2015 WI 204101	JPB	Updated auditing messages. 
* 05/27/2015 WI 215504	CMC	Enhance to use IsAssigned.  Need to honor 
*							the IsActive parm passed in.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @SiteCodeID				INT;
DECLARE @errorDescription		VARCHAR(2000);
DECLARE @auditMessage			VARCHAR(1024);

BEGIN TRY

	BEGIN TRANSACTION
		
		IF @parmSiteBankID = -1 AND @parmSiteClientAccountID = -1
			BEGIN
				SET @SiteCodeID = -1
			END
		ELSE
			BEGIN
				-- Get the Site Code ID
				SELECT TOP 1 @SiteCodeID = RecHubData.dimClientAccountsView.SiteCodeID 
				FROM RecHubUser.SessionClientAccountEntitlements
					INNER JOIN RecHubUser.OLWorkgroups
						ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
							AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
							AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
					INNER JOIN RecHubData.dimClientAccountsView
						ON RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
							AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
							AND RecHubData.dimClientAccountsView.IsActive = 1
				WHERE RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
					AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID

				IF @SiteCodeID IS NULL
				BEGIN
					SET @errorDescription = 'Unable to insert RecHubAlert.EventRules record, user does not have access to workgroup.';
					RAISERROR(@errorDescription, 16, 1);
				END
			END

		-- Insert the Event Rule
		INSERT INTO RecHubAlert.EventRules
		(
			RecHubAlert.EventRules.EventID,
			RecHubAlert.EventRules.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.EventRules.IsActive,
			RecHubAlert.EventRules.EventRuleDescription,
			RecHubAlert.EventRules.EventRuleOperator,
			RecHubAlert.EventRules.EventRuleValue
		)
		VALUES
		(
			@parmEventID,
			@SiteCodeID,
			@parmSiteBankID,
			@parmSiteClientAccountID,
			@parmIsActive,
			@parmDescription,
			@parmOperator,
			@parmValue
		)
	
		SET @parmEventRuleID = @@IDENTITY;

		-- Audit the Event Rule insert
		SELECT @auditMessage = 'Added EventRules:'
			+ '  Event Rule = ' + EventLongName
			+ ', SiteCodeID = ' 
			+ CASE @SiteCodeID
				WHEN -1 THEN 'All'
				ELSE CAST(@SiteCodeID AS VARCHAR)
			END
			+ ', SiteBankID = '
			+ CASE @parmSiteBankID 
				WHEN -1 THEN 'All'
				ELSE CAST(@parmSiteBankID AS VARCHAR)
			END
			+ ', SiteClientAccountID = '
			+ CASE @parmSiteClientAccountID
				WHEN -1 THEN 'All'
				ELSE CAST(@parmSiteClientAccountID AS VARCHAR)
			END
			+ ', IsActive = '
			+ CASE @parmIsActive
				WHEN 0 THEN 'False'
				ELSE 'True'
			END
			+ ', Description = ' + ISNULL(CAST(@parmDescription AS VARCHAR), '<NULL>')
			+ CASE 
				WHEN @parmOperator IS NOT NULL AND LEN(@parmOperator) > 0 THEN ', Operator = ' + CAST(@parmOperator AS VARCHAR)
				ELSE ''
			END
			+ CASE
				WHEN @parmValue IS NOT NULL THEN ', Value = ' + CAST(@parmValue AS VARCHAR)
				ELSE ''
			END	
			+ '.'
		FROM
			RecHubAlert.[Events]
		WHERE 
			EventID = @parmEventID;

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubAlert.usp_EventRules_Ins',
				@parmSchemaName			= 'RecHubAlert',
				@parmTableName			= 'EventRules',
				@parmColumnName			= 'EventRuleID',
				@parmAuditValue			= @parmEventRuleID,
				@parmAuditType			= 'INS',
				@parmAuditMessage		= @auditMessage;

		-- Insert Alerts (User, Event Rule, and static Delivery Method, and IsActive)
		INSERT INTO RecHubAlert.Alerts
		(
			UserID,
			DeliveryMethodID,
			EventRuleID,
			IsActive
		)
		SELECT 
			Tbl.Col.value('@ID', 'INT') AS UserID,
			1 AS DeliveryMethodID,
			@parmEventRuleID AS EventRuleID,
			@parmIsActive AS IsActive
		FROM
			@parmUsers.nodes('//users/user') AS Tbl(Col);

		DECLARE @AlertsTableVar TABLE
		(
			AlertID          INT,
			UserID           INT,
			DeliveryMethodID INT,
			EventRuleID      INT
		);

		INSERT INTO @AlertsTableVar
		SELECT RecHubAlert.Alerts.AlertID,
		       RecHubAlert.Alerts.UserID,
			   RecHubAlert.Alerts.DeliveryMethodID,
			   RecHubAlert.Alerts.EventRuleID 
		FROM   RecHubAlert.Alerts 
		WHERE 
			RecHubAlert.Alerts.EventRuleID = @parmEventRuleID
			AND RecHubAlert.Alerts.IsActive = 1;

		IF EXISTS(SELECT * From @AlertsTableVar) BEGIN
			
			DECLARE @AlertID          INT,
			        @UserID           INT,
			        @DeliveryMethodID INT,
			        @EventRuleID      INT;

			WHILE EXISTS(SELECT * FROM @AlertsTableVar) BEGIN
			
				SELECT TOP 1 @AlertID = AlertID, @UserID = UserID, @DeliveryMethodID = DeliveryMethodID, @EventRuleID = EventRuleID  FROM @AlertsTableVar
					
					-- Audit the Alerts insert
				SELECT 
					@auditMessage = 'Added Alerts: Logon Name = ' + LogonName
				FROM
					RecHubUser.Users
				WHERE
					UserID = @UserID;

				SELECT
					@auditMessage = @auditMessage + ', Delivery Method = ' + DeliveryMethodName
				FROM
					RecHubAlert.DeliveryMethods
				WHERE
					DeliveryMethodID = @DeliveryMethodID;

				SELECT
					@auditMessage = @auditMessage + ', Event Rule = ' + EventRuleDescription + '.'
				FROM
					RecHubAlert.EventRules
				WHERE
					EventRuleID = @EventRuleID;

				EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
						@parmUserID				= @parmUserID,
						@parmApplicationName	= 'RecHubAlert.usp_EventRules_Ins',
						@parmSchemaName			= 'RecHubAlert',
						@parmTableName			= 'Alerts',
						@parmColumnName			= 'AlertID',
						@parmAuditValue			= @AlertID,
						@parmAuditType			= 'INS',
						@parmAuditMessage		= @auditMessage;
			
				DELETE FROM @AlertsTableVar WHERE AlertID = @AlertID;
			END
		END

	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	
	ROLLBACK TRANSACTION

	EXEC RecHubCommon.usp_WfsRethrowException;

END CATCH