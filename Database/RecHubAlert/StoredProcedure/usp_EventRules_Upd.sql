--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_EventRules_Upd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Upd') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_EventRules_Upd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Upd
(	
	@parmUserID INT,
	@parmSessionID UNIQUEIDENTIFIER,
	@parmEventRuleID INT,
	@parmEventID INT,
	@parmSiteBankID INT,
	@parmSiteClientAccountID INT,
	@parmIsActive BIT,
	@parmDescription VARCHAR(30) = NULL,
	@parmOperator VARCHAR(10) = NULL,
	@parmValue VARCHAR(80) = NULL,
	@parmUsers	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/09/2013
*
* Purpose: Update existing Event Rule
*
* Modification History
* 07/09/2013 WI 108371	EAS	Created.  Removed EventLEvel reference
* 07/11/2013 WI 108654	EAS Add Default NULL Values
* 06/27/2014 WI 146689	EAS Changed for Portlet move.
* 08/08/2014 WI 146862	EAS Added Auditing
* 12/08/2014 WI 146862	KLC	Updated the following
*								 Added verification user has access to workgroup
*								 Fixed auditing so it audits the user
* 01/27/2015 WI 186619	JBS	Add IsActive = 1 for Alerts
* 02/10/2015 WI 186619	JBS We were adding back rows even if we were inactivating
* 02/19/2015 WI 191218	MA  Added a disctinct on the users to be subscribed to alerts
* 05/11/2015 WI 204103	JPB	Updated auditing messages.
* 05/27/2015 WI 215503	CMC	Enhance to use IsAssigned.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @SiteCodeID				INT = NULL;
DECLARE @errorDescription		VARCHAR(2000);
DECLARE @auditMessage			VARCHAR(1024);
DECLARE @auditColumns			RecHubCommon.AuditValueChangeTable;

BEGIN TRY

	BEGIN TRANSACTION

		IF @parmSiteBankID = -1 AND @parmSiteClientAccountID = -1
			BEGIN
				SET @SiteCodeID = -1
			END
		ELSE
			BEGIN
				-- Get the Site Code ID
				SELECT TOP 1 @SiteCodeID = RecHubData.dimClientAccountsView.SiteCodeID 
				FROM RecHubUser.SessionClientAccountEntitlements
					INNER JOIN RecHubUser.OLWorkgroups
						ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
							AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
							AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
					INNER JOIN RecHubData.dimClientAccountsView
						ON RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
							AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
							AND RecHubData.dimClientAccountsView.IsActive = 1
				WHERE RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
					AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID
					AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID

				IF @SiteCodeID IS NULL
				BEGIN
					SET @errorDescription = 'Unable to update RecHubAlert.EventRules record, EventRuleID (' + CAST(@parmEventRuleID AS VARCHAR(20)) + ' user does not have access to workgroup.';
					RAISERROR(@errorDescription, 16, 1);
				END
			END

		DECLARE @AlertsToRemoveVar TABLE -- Table to hold the Alerts that we are going to remove 
		(
			AlertID          INT,
			UserID           INT,
			DeliveryMethodID INT,
			EventRuleID      INT,
			IsActive		 BIT
		);

		DECLARE @AlertsToAddVar TABLE -- Table to hold the Alerts that we are going to add  
		(
			AlertID          INT,
			UserID           INT,
			DeliveryMethodID INT,
			EventRuleID      INT,
			IsActive		 BIT
		);

		DECLARE @AlertsToUpdateVar TABLE -- Table to hold the Alerts that we are going to update
		(
			AlertID          INT,
			UserID           INT,
			DeliveryMethodID INT,
			EventRuleID      INT,
			IsActive		 BIT
		);

		DECLARE @SelectedUsers TABLE -- Table to hold the Selected Users
		(
			UserID           INT,
			EventRuleID      INT,
			IsActive		 BIT
		);

		INSERT INTO @SelectedUsers
		SELECT 
		DISTINCT
			Tbl.Col.value('@ID', 'INT') AS UserID,
			@parmEventRuleID AS EventRuleID,
			@parmIsActive AS IsActive
		FROM
			@parmUsers.nodes('//users/user') AS Tbl(Col)

		INSERT INTO @AlertsToRemoveVar -- Gather the existing user alerts that have not been selected and need to be removed
		SELECT 
			RecHubAlert.Alerts.AlertID,
			RecHubAlert.Alerts.UserID,
			RecHubAlert.Alerts.DeliveryMethodID,
			RecHubAlert.Alerts.EventRuleID,
			@parmIsActive  
		FROM RecHubAlert.Alerts 
		LEFT JOIN @SelectedUsers AS Users ON RecHubAlert.Alerts.UserID = Users.UserID AND RecHubAlert.Alerts.EventRuleID = Users.EventRuleID
		WHERE RecHubAlert.Alerts.EventRuleID = @parmEventRuleID
			AND RecHubAlert.Alerts.IsAssigned = 1
			AND Users.UserID IS NULL

		INSERT INTO @AlertsToUpdateVar -- Gather the existing user alerts that are still selected and need to be updated
		SELECT 
			RecHubAlert.Alerts.AlertID,
			RecHubAlert.Alerts.UserID,
			RecHubAlert.Alerts.DeliveryMethodID,
			@parmEventRuleID,
			@parmIsActive As IsActive  
		FROM 
			RecHubAlert.Alerts 
		INNER JOIN @SelectedUsers AS Users 
			ON RecHubAlert.Alerts.UserID = Users.UserID 
			AND RecHubAlert.Alerts.EventRuleID = Users.EventRuleID
		WHERE 
			RecHubAlert.Alerts.EventRuleID = @parmEventRuleID

		INSERT INTO @AlertsToAddVar -- Gather the new user alerts that have been selected and need to be inserted
		SELECT 
			NULL,
			Users.UserID,
			1,
			@parmEventRuleID,
			@parmIsActive  
		FROM 
			RecHubAlert.Alerts 
		RIGHT JOIN @SelectedUsers AS Users 
			ON RecHubAlert.Alerts.UserID = Users.UserID 
			AND RecHubAlert.Alerts.EventRuleID = Users.EventRuleID
		WHERE 
			RecHubAlert.Alerts.UserID IS NULL 
		
		UPDATE RecHubAlert.Alerts -- Remove User Alerts that are no longer selected
		SET  
			RecHubAlert.Alerts.IsActive = 0,
			RecHubAlert.Alerts.IsAssigned = 0
		FROM @AlertsToRemoveVar a
		INNER JOIN RecHubAlert.Alerts b ON a.AlertID = b.AlertID
		
		UPDATE RecHubAlert.Alerts --Update existing User Alerts that are still selected
		SET  
			RecHubAlert.Alerts.IsActive = a.IsActive,
			RecHubAlert.Alerts.IsAssigned = 1
		FROM @AlertsToUpdateVar a
		INNER JOIN RecHubAlert.Alerts b ON a.AlertID = b.AlertID

		INSERT INTO RecHubAlert.Alerts -- Insert new User Alerts that have been selected
		(	
			UserID,
			DeliveryMethodID,	
			EventRuleID,			
			IsActive,
			IsAssigned
		)
		SELECT 
			UserID,
			DeliveryMethodID,
			EventRuleID,
			IsActive,
			IsAssigned = 1
		FROM @AlertsToAddVar

		-- Now for the Auditing
		DECLARE @AlertsTableVar TABLE 
		(
			AlertID          INT,
			UserID           INT,
			DeliveryMethodID INT,
			EventRuleID      INT
		);

		-- First we will handle the auditing of the Deleted User Alerts
		INSERT INTO @AlertsTableVar
		SELECT 
			AlertID,
			UserID,
			DeliveryMethodID,
			EventRuleID 
		FROM @AlertsToRemoveVar 
		WHERE IsActive = 1; -- We will only gather the Active Alerts for Auditing

		IF EXISTS(SELECT TOP 1 AlertID From @AlertsTableVar)  
		BEGIN
			
			DECLARE @AlertID          INT,
			        @UserID           INT,
			        @DeliveryMethodID INT,
			        @EventRuleID      INT;

			WHILE EXISTS(SELECT TOP 1 AlertID FROM @AlertsTableVar) 
			BEGIN
				SELECT TOP 1 @AlertID = AlertID, @UserID = UserID, @DeliveryMethodID = DeliveryMethodID, @EventRuleID = EventRuleID  FROM @AlertsTableVar

				/* Audit the Alerts insert */
				SELECT 
					@auditMessage = 'Deleted Alerts: Logon Name = ' + LogonName
				FROM
					RecHubUser.Users
				WHERE
					UserID = @UserID;

				SELECT
					@auditMessage = @auditMessage + ', Delivery Method = ' + DeliveryMethodName
				FROM
					RecHubAlert.DeliveryMethods
				WHERE
					DeliveryMethodID = @DeliveryMethodID;

				SELECT
					@auditMessage = @auditMessage + ', Event Rule = ' + EventRuleDescription + '.'
				FROM
					RecHubAlert.EventRules
				WHERE
					EventRuleID = @EventRuleID;

				EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
					@parmUserID				= @parmUserID,
					@parmApplicationName	= 'RecHubAlert.usp_EventRules_Upd',
					@parmSchemaName			= 'RecHubAlert',
					@parmTableName			= 'Alerts',
					@parmColumnName			= 'AlertID',
					@parmAuditValue			= @AlertID,
					@parmAuditType			= 'DEL',
					@parmAuditMessage		= @auditMessage;
			
				DELETE FROM @AlertsTableVar WHERE AlertID = @AlertID;
			END
		END
		
		DECLARE @prevEventID		      INT,
			    @prevSiteCodeID	          INT,
				@prevSiteBankID			  INT,
			    @prevSiteClientAccountID  INT,
			    @prevIsActive			  BIT,
			    @prevEventRuleDescription VARCHAR(30),
				@prevEventRuleOperator    VARCHAR(10),
				@prevEventRuleValue       VARCHAR(80)

		-- Update the Event Rule
		UPDATE RecHubAlert.EventRules
		SET
		    @prevEventID =- RecHubAlert.EventRules.EventID,
			@prevSiteCodeID = RecHubAlert.EventRules.SiteCodeID,
			@prevSiteBankID = RecHubAlert.EventRules.SiteBankID,
			@prevSiteClientAccountID = RecHubAlert.EventRules.SiteClientAccountID,
			@prevIsActive = RecHubAlert.EventRules.IsActive,		 
			@prevEventRuleDescription = RecHubAlert.EventRules.EventRuleDescription,
			@prevEventRuleOperator = RecHubAlert.EventRules.EventRuleOperator, 
			@prevEventRuleValue = RecHubAlert.EventRules.EventRuleValue,    
			RecHubAlert.EventRules.EventID = @parmEventID,
			RecHubAlert.EventRules.SiteCodeID = @SiteCodeID,
			RecHubAlert.EventRules.SiteBankID = @parmSiteBankID,
			RecHubAlert.EventRules.SiteClientAccountID = @parmSiteClientAccountID,
			RecHubAlert.EventRules.IsActive = @parmIsActive,
			RecHubAlert.EventRules.EventRuleDescription = @parmDescription,
			RecHubAlert.EventRules.EventRuleOperator = @parmOperator,
			RecHubAlert.EventRules.EventRuleValue = @parmValue
		WHERE 
			RecHubAlert.EventRules.EventRuleID = @parmEventRuleID;


		-- Audit the Update (changes only)
		IF @prevEventID <> @parmEventID
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EventID', CAST(@prevEventID AS VARCHAR), CAST(@parmEventID AS VARCHAR));
		IF @prevSiteCodeID <> @SiteCodeID
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('SiteCodeID', CAST(@prevSiteCodeID AS VARCHAR), CAST(@SiteCodeID AS VARCHAR));
		IF @prevSiteBankID <> @parmSiteBankID
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('SiteBankID', CAST(@prevSiteBankID AS VARCHAR), CAST(@parmSiteBankID AS VARCHAR));
		IF @prevSiteClientAccountID <> @parmSiteClientAccountID
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('SiteClientAccountID', CAST(@prevSiteClientAccountID AS VARCHAR), CAST(@parmSiteClientAccountID AS VARCHAR));
		IF @prevIsActive <> @parmIsActive
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IsActive', CAST(@prevIsActive AS VARCHAR), CAST(@parmIsActive AS VARCHAR));
		IF @prevEventRuleDescription <> @parmDescription
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EventRuleDescription', CAST(@prevEventRuleDescription AS VARCHAR), CAST(@parmDescription AS VARCHAR));
		IF @prevEventRuleOperator <> @parmOperator
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('EventRuleOperator', CAST(@prevEventRuleOperator AS VARCHAR), CAST(@parmOperator AS VARCHAR));
		IF @prevEventRuleValue <> @parmValue
			INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('', CAST(@prevEventRuleValue AS VARCHAR), CAST(@parmValue AS VARCHAR));
		
		SET @auditMessage = 'Modified EventRules for EventRuleID: ' + CAST(@parmEventRuleID AS VARCHAR(10)) + '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubAlert.usp_EventRules_Upd',
			@parmSchemaName			= 'RecHubAlert',
			@parmTableName			= 'EventRules',
			@parmColumnName			= 'EventRuleID',
			@parmAuditValue			= @parmEventRuleID,
			@parmAuditType			= 'UPD',
			@parmAuditColumns		= @AuditColumns,
			@parmAuditMessage		= @auditMessage;

		-- Second, we will process the auditing of the new User Alerts
		INSERT INTO @AlertsTableVar
		SELECT 
			RecHubAlert.Alerts.AlertID,
			RecHubAlert.Alerts.UserID,
			RecHubAlert.Alerts.DeliveryMethodID,
			RecHubAlert.Alerts.EventRuleID 
		FROM
			@AlertsToAddVar a
		INNER JOIN RecHubAlert.Alerts ON a.EventRuleID = RecHubAlert.Alerts.EventRuleID AND a.UserID = RechubAlert.Alerts.UserID
		WHERE  
			RecHubAlert.Alerts.IsActive = 1;    -- We will only gather the Active Alerts for Auditing

		IF EXISTS(SELECT TOP 1 AlertID From @AlertsTableVar) 
		BEGIN

			WHILE EXISTS(SELECT TOP 1 AlertID FROM @AlertsTableVar) 
			BEGIN
			
				SELECT TOP 1 
					@AlertID = AlertID, 
					@UserID = UserID, 
					@DeliveryMethodID = DeliveryMethodID, 
					@EventRuleID = EventRuleID  
				FROM 
					@AlertsTableVar
					
				/* Audit the Alerts insert */
				SELECT 
					@auditMessage = 'Added Alerts: Logon Name = ' + LogonName
				FROM
					RecHubUser.Users
				WHERE
					UserID = @UserID;

				SELECT
					@auditMessage = @auditMessage + ', Delivery Method = ' + DeliveryMethodName
				FROM
					RecHubAlert.DeliveryMethods
				WHERE
					DeliveryMethodID = @DeliveryMethodID;

				SELECT
					@auditMessage = @auditMessage + ', Event Rule = ' + EventRuleDescription + '.'
				FROM
					RecHubAlert.EventRules
				WHERE
					EventRuleID = @EventRuleID;

				EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
						@parmUserID				= @parmUserID,
						@parmApplicationName	= 'RecHubAlert.usp_EventRules_Upd',
						@parmSchemaName			= 'RecHubAlert',
						@parmTableName			= 'Alerts',
						@parmColumnName			= 'AlertID',
						@parmAuditValue			= @AlertID,
						@parmAuditType			= 'INS',
						@parmAuditMessage		= @auditMessage;
			
				DELETE FROM @AlertsTableVar WHERE AlertID = @AlertID;
			END
		END
		
		-- Lastly, we will process the auditing of the updated User Alerts
		INSERT INTO @AlertsTableVar
		SELECT 
			AlertID,
			UserID,
			DeliveryMethodID,
			EventRuleID 
		FROM
			@AlertsToUpdateVar 
		WHERE  
			IsActive = 1;    -- We will only gather the Active Alerts for Auditing

		IF EXISTS(SELECT TOP 1 AlertID From @AlertsTableVar) 
		BEGIN

			WHILE EXISTS(SELECT TOP 1 AlertID FROM @AlertsTableVar) 
			BEGIN
			
				SELECT TOP 1 
					@AlertID = AlertID, 
					@UserID = UserID, 
					@DeliveryMethodID = DeliveryMethodID, 
					@EventRuleID = EventRuleID  
				FROM 
					@AlertsTableVar
					
				/* Audit the Alerts insert */
				SELECT 
					@auditMessage = 'Updated Alert: Logon Name = ' + LogonName
				FROM
					RecHubUser.Users
				WHERE
					UserID = @UserID;

				SELECT
					@auditMessage = @auditMessage + ', Delivery Method = ' + DeliveryMethodName
				FROM
					RecHubAlert.DeliveryMethods
				WHERE
					DeliveryMethodID = @DeliveryMethodID;

				SELECT
					@auditMessage = @auditMessage + ', Event Rule = ' + EventRuleDescription + '.'
				FROM
					RecHubAlert.EventRules
				WHERE
					EventRuleID = @EventRuleID;

				EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
						@parmUserID				= @parmUserID,
						@parmApplicationName	= 'RecHubAlert.usp_EventRules_Upd',
						@parmSchemaName			= 'RecHubAlert',
						@parmTableName			= 'Alerts',
						@parmColumnName			= 'AlertID',
						@parmAuditValue			= @AlertID,
						@parmAuditType			= 'UPD',
						@parmAuditMessage		= @auditMessage;
			
				DELETE FROM @AlertsTableVar WHERE AlertID = @AlertID;
			END
		END
	COMMIT TRANSACTION

END TRY

BEGIN CATCH
	ROLLBACK TRANSACTION
	
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH