--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventLog_Get_ByEventStatus
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventLog_Get_ByEventStatus') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventLog_Get_ByEventStatus
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventLog_Get_ByEventStatus
(
	@parmEventStatus  INT = 10
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/30/2013
*
* Purpose: Retrieve rows from EventLog By EventStatus
*
* Modification History
* 05/30/2013 WI 99898 JBS	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY      
 
	SELECT	
		RecHubAlert.EventLog.EventLogID,
		RecHubAlert.EventLog.EventRuleID,
		RecHubAlert.EventLog.AlertID,
		RecHubAlert.EventLog.EventStatus,
		RecHubAlert.EventLog.BankKey,
		RecHubAlert.EventLog.OrganizationKey,
		RecHubAlert.EventLog.ClientAccountKey,
		RecHubAlert.EventLog.EventSourceKey,
		RecHubAlert.EventLog.CreationDate,
		RecHubAlert.EventLog.ModificationDate,
		RecHubAlert.EventLog.EventMessage
	FROM
		RecHubAlert.EventLog
	WHERE
		RecHubAlert.EventLog.EventStatus = @parmEventStatus;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
