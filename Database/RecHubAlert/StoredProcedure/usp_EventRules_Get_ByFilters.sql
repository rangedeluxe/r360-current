--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get_ByFilters
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get_ByFilters') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_EventRules_Get_ByFilters
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get_ByFilters
(
	@parmStartRecord INT,
	@parmRecordsPerPage INT,
	@parmUserName VARCHAR(24) = NULL,
	@parmEventID INT = NULL, 
	@parmSiteCodeID INT = NULL, 
	@parmSiteBankID INT = NULL, 
	@parmSiteClientAccountID INT = NULL,
	@parmTotalRecords INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/10/2013
*
* Purpose: Get Event Rules by filter criteria
*
* Modification History
* 06/10/2013  WI  99852	 EAS	Created
* 07/09/2013  WI  108367 EAS	Correction to tie Event Rule ID
* 07/11/2013  WI  108662 EAS	Add Event Rule ID field for return
* 10/18/2013  WI  117819 EAS    Remove paging.  Need to calculate Total Records yet.
* 01/27/2015  WI  186531 JBS	Add IsActive = 1 for Alerts table
******************************************************************************/
SET NOCOUNT ON 
BEGIN TRY
IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Results')) 
		DROP TABLE #Results;

	CREATE TABLE #Results
	(
		RecID					INT,
		TotalRecords			INT,
		EventRuleID				INT,
		[Description]			VARCHAR(30),
		EventID					INT,
		EventName				VARCHAR(30),
		EventType				TINYINT,
		UserID					INT,
		UserName				VARCHAR(24),
		SiteCodeID				INT,
		SiteBankID				INT,
		SiteClientAccountID		INT
	)
	

	;WITH PageRecords AS
	(
		SELECT	ROW_NUMBER() OVER 
						(
							ORDER BY RecHubAlert.EventRules.EventRuleID ASC
						) AS RecID,
				RecHubAlert.EventRules.EventRuleID,
				RecHubAlert.EventRules.EventRuleDescription AS [Description],
				RecHubAlert.[Events].EventID AS EventID, 
				RecHubAlert.[Events].EventName AS EventName,
				RecHubAlert.[Events].EventType AS EventType,
				RecHubUser.Users.UserID AS UserID, 
				RecHubUser.Users.LogonName AS UserName,
				RecHubAlert.EventRules.SiteCodeID AS SiteCodeID,
				RecHubAlert.EventRules.SiteBankID AS SiteBankID,
				RecHubAlert.EventRules.SiteClientAccountID AS SiteClientAccountID
		FROM RecHubAlert.EventRules
			INNER JOIN RecHubAlert.[Events] ON (RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID)
			LEFT OUTER JOIN RecHubAlert.Alerts ON (RecHubAlert.Alerts.EventRuleID = RecHubAlert.EventRules.EventRuleID)
			LEFT OUTER JOIN RecHubUser.Users ON (RecHubUser.Users.UserID = RecHubAlert.Alerts.UserID)
			
			WHERE RecHubAlert.EventRules.EventID = COALESCE(@parmEventID, RecHubAlert.EventRules.EventID)
			AND   RecHubAlert.EventRules.SiteCodeID = COALESCE(@parmSiteCodeID, RecHubAlert.EventRules.SiteCodeID)
			AND   RecHubAlert.EventRules.SiteBankID = COALESCE(@parmSiteBankID, RecHubAlert.EventRules.SiteBankID)
			AND   RecHubAlert.EventRules.SiteClientAccountID = COALESCE(@parmSiteClientAccountID, RecHubAlert.EventRules.SiteClientAccountID)
			AND	  ISNULL(RecHubUser.Users.LogonName , '') LIKE COALESCE('%' + @parmUserName + '%', ISNULL(RecHubUser.Users.LogonName, ''))
			AND	  RecHubAlert.Alerts.IsActive = 1
	)
	INSERT INTO #Results
		(	
			RecID,
			TotalRecords,
			EventRuleID,
			[Description],
			EventID,
			EventName,
			EventType,
			UserID,
			UserName,
			SiteCodeID,
			SiteBankID,
			SiteClientAccountID
		)
	SELECT	RecID,
			TotalRecords = (SELECT ISNULL(MAX(RecID), 0) FROM PageRecords),
			EventRuleID,
			[Description],
			EventID,
			EventName,
			EventType,
			UserID,
			UserName,
			SiteCodeID,
			SiteBankID,
			SiteClientAccountID
	FROM	PageRecords
	ORDER BY RecID ASC;

	SELECT TOP(1) @parmTotalRecords = TotalRecords FROM #Results
	
	IF @parmTotalRecords > 0
		SELECT	RecID,
				EventRuleID,
				[Description],
				EventID,
				EventName,
				EventType,
				UserID,
				UserName,
				SiteCodeID,
				SiteBankID,
				SiteClientAccountID
		FROM	#Results
		ORDER BY RecID ASC
	ELSE
		SET @parmTotalRecords = 0;

	
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Results')) 
		DROP TABLE #Results;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#Results')) 
		DROP TABLE #Results;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH