--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventLog_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventLog_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventLog_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventLog_Ins
(
	@parmEventName				VARCHAR(30),
	@parmSiteCodeID				INT = NULL,
	@parmSiteBankID				INT = NULL,
	@parmSiteClientAccountID	INT = NULL,
	@parmMessage				VARCHAR(1028) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/06/2013
*
* Purpose: Insert rows to EventLog table.
*
* Modification History
* 05/06/2013 WI 99876 JBS	Created.  Changed the source reference for EventLevel Column.
* 04/17/2014 WI 137394 JBS	Added Permissions
* 04/24/2014 WI 138416 JBS	Correcting mismatch of columns used for comparison.  Also removing raising error
*							For EventRule not found.
* 12/09/2014 WI 180164 JBS	Adding IsActive checks. 
* 12/15/2014 WI 180164 JBS  Fix BUG 181656. Correct so ALL Alerts matching EventName are identified and built in EventLog
* 01/21/2015 WI 185583 JBS	Moving in Default message if nothing passed in
* 06/09/2016 WI 285783 JBS	Adding Top 1 to subselect to make assignment singluar until we can remove Organization Key from the process 
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @EventID				SMALLINT,
		@EventLevel				TINYINT,
		@EventSourceKey			TINYINT,
		@BankKey				INT = NULL,
		@OrganizationKey		INT = NULL,
		@ClientAccountKey		INT = NULL,
		@SiteOrganizationID		INT = NULL,
		@ErrorDescription		VARCHAR(2000);
		
SET @ErrorDescription = '';

BEGIN TRY      
 
	SELECT	
		@EventID = COALESCE(RecHubAlert.[Events].EventID, NULL),
		@EventLevel= COALESCE(RecHubAlert.[Events].EventLevel, NULL)
	FROM
		RecHubAlert.[Events]
	WHERE
		RecHubAlert.[Events].EventName = @parmEventName;

-- Check for success
	IF @EventID IS NULL
	BEGIN
	   SET @ErrorDescription = 'Alert Event Not found in Events Table for usp_EventLog_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END

-- Check for success  
	IF @EventLevel IS NULL
	BEGIN
	   SET @ErrorDescription = 'Alert Event Level not valid for usp_EventLog_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END

	SELECT 
		@EventSourceKey = COALESCE(RecHubData.DimBatchSources.BatchSourceKey, NULL)
	FROM 
		RecHubData.dimBatchSources
	WHERE
		RecHubData.dimBatchSources.ShortName = 'R360 Alerts'
		AND RecHubData.dimBatchSources.IsActive = 1;  -- WI180164

-- Check for success
	IF @EventSourceKey IS NULL
	BEGIN
	   SET @ErrorDescription = 'Event SourceKey not found in RecHubData.dimBatchSources table for usp_EventLog_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END	

	IF EXISTS (SELECT * FROM TEMPDB..SYSOBJECTS WHERE ID=OBJECT_ID('tempdb..#TMPEventRules')) 
		DROP TABLE #TMPEventRules;
	
	CREATE TABLE #TMPEventRules
	(
		EventRuleID			 BIGINT,
		EventRuleDescription VARCHAR(30)	
	);

	INSERT INTO #TMPEventRules			-- Gather all EventRules matching EventID
	(	
		EventRuleID,
		EventRuleDescription
	)
	SELECT 
		DISTINCT RecHubAlert.EventRules.EventRuleID, 
				COALESCE(RecHubAlert.EventRules.EventRuleDescription,' ') AS EventRuleDescription
	FROM 
		RecHubAlert.EventRules
	WHERE
		RecHubAlert.EventRules.EventID = @EventID
		AND RecHubAlert.EventRules.SiteCodeID = @parmSiteCodeID
		AND (@parmSiteBankID IS NULL OR RecHubAlert.EventRules.SiteBankID = @parmSiteBankID)
		AND (@parmSiteClientAccountID IS NULL OR RecHubAlert.EventRules.SiteClientAccountID = @parmSiteClientAccountID)
		AND RecHubAlert.EventRules.IsActive = 1;	-- WI180164

-- Check for success
	IF EXISTS (SELECT 1 FROM #TMPEventRules) -- @EventRuleID IS NOT NULL   -- WI138416 No EventRule Found. Do not want to proceed there is nothing to log based on inputs.  
	BEGIN
		-- Turn ID's into Keys
		-- IF @EventLevel = 1  set the variables all to -1
		SET @BankKey = -1;
		SET	@OrganizationKey = -1;
		SET	@ClientAccountKey = -1;
	
		IF @EventLevel > 1  -- do it is either a 2 or a 3
			BEGIN
				SELECT @BankKey = (SELECT RecHubData.dimBanks.BankKey FROM RecHubData.dimBanks WHERE RecHubData.dimBanks.SiteBankID = @parmSiteBankID and RecHubData.dimBanks.MostRecent = 1)
				IF @BankKey IS NULL
					BEGIN
						SET @ErrorDescription = 'Bank Key not found in RecHubData.dimBanks table for usp_EventLog_Ins '+@parmEventName;
						RAISERROR(@ErrorDescription,16,1);
					END		
			END
	
		IF @EventLevel > 2  -- is a 3
			BEGIN
				SELECT	@ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey,
						@SiteOrganizationID = RecHubData.dimClientAccounts.SiteOrganizationID 	
				FROM RecHubData.dimClientAccounts 
				WHERE RecHubData.dimClientAccounts.SiteCodeID = @parmSiteCodeID  
					AND RecHubData.dimClientAccounts.SiteBankID = @parmSiteBankID 
					AND RecHubData.dimClientAccounts.SiteClientAccountID = @parmSiteClientAccountID
					AND RecHubData.dimClientAccounts.MostRecent = 1
					AND RecHubData.dimClientAccounts.IsActive = 1;
				IF @ClientAccountKey IS NULL
					BEGIN
						SET @ErrorDescription = 'ClientAccountKey not found in RecHubData.dimClientAccounts table for usp_EventLog_Ins '+@parmEventName;
						RAISERROR(@ErrorDescription,16,1);
					END	
				-- WI138416, WI285783
				SELECT @OrganizationKey = 
					(SELECT TOP 1 RecHubData.dimOrganizations.OrganizationKey  
					FROM RecHubData.dimOrganizations 
					WHERE RecHubData.dimOrganizations.SiteOrganizationID = @SiteOrganizationID  
					AND RecHubData.dimOrganizations.SiteBankID = @parmSiteBankID 
					AND RecHubData.dimOrganizations.MostRecent = 1);
				IF @OrganizationKey IS NULL  -- WI138416
					BEGIN
						SET @ErrorDescription = 'OrganizationKey not found in RecHubData.dimClientAccounts table for usp_EventLog_Ins '+@parmEventName;
						RAISERROR(@ErrorDescription,16,1);
					END	
			END	
		
		BEGIN
			INSERT INTO RecHubAlert.EventLog
			(
				RecHubAlert.EventLog.EventRuleID,
				RecHubAlert.EventLog.EventStatus,
				RecHubAlert.EventLog.AlertID,
				RecHubAlert.EventLog.BankKey,
				RecHubAlert.EventLog.OrganizationKey,
				RecHubAlert.EventLog.ClientAccountKey,
				RecHubAlert.EventLog.EventSourceKey,
				RecHubAlert.EventLog.CreationDate,
				RecHubAlert.EventLog.ModificationDate,
				RecHubAlert.EventLog.EventMessage
			)
			SELECT		
				#TMPEventRules.EventRuleID,   
				10, 
				RecHubAlert.Alerts.AlertID,
				@BankKey,
				@OrganizationKey,
				@ClientAccountKey,
				@EventSourceKey,
				GETDATE(), 
				GETDATE(),
				COALESCE(@parmMessage, @parmEventName + ' - ' + #TMPEventRules.EventRuleDescription + ' - No Message Provided')
			FROM 
				RecHubAlert.Alerts 
				INNER JOIN #TMPEventRules ON 
					RecHubAlert.Alerts.EventRuleID = #TMPEventRules.EventRuleID;
		END
	END	-- WI138416

	IF EXISTS (SELECT * FROM TEMPDB..SYSOBJECTS WHERE ID=OBJECT_ID('tempdb..#TMPEventRules')) 
		DROP TABLE #TMPEventRules;

END TRY

BEGIN CATCH
	IF EXISTS (SELECT * FROM TEMPDB..SYSOBJECTS WHERE ID=OBJECT_ID('tempdb..#TMPEventRules')) 
		DROP TABLE #TMPEventRules;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH