--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get_ByEventRuleID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get_ByEventRuleID') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventRules_Get_ByEventRuleID
GO


--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get_ByEventRuleID
(
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmEventRuleID	INT
)

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/02/2013
*
* Purpose: Get Event Rule by its ID
*
* Modification History
* 07/02/2013 WI 107697	EAS	Created
* 06/27/2014 WI 146685	EAS Change Description name for resultset
* 11/26/2014 WI 146685	KLC	Updated to enforce workgroup permissions
* 12/10/2014 WI 146685	KLC	Fixed where condition parenthesis
* 02/09/2015 WI 188723	JPB	Added EventLongName to result set.
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	;WITH CTE_UsersWorkgroups AS
	(
		SELECT DISTINCT 
			RecHubData.dimClientAccountsView.SiteBankID,
			RecHubData.dimClientAccountsView.SiteClientAccountID
		FROM 
			RecHubUser.SessionClientAccountEntitlements
			INNER JOIN RecHubUser.OLWorkgroups /*joining through here in case the workgroup association is changed while the user is logged in and no longer belongs to the entity*/
				ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
			AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
			INNER JOIN RecHubData.dimClientAccountsView	ON RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
				AND RecHubUser.OLWorkgroups.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
		WHERE 
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	)
	SELECT 
		RecHubAlert.EventRules.EventRuleID,
		RecHubAlert.EventRules.EventRuleDescription AS [Description],
		RecHubAlert.EventRules.SiteCodeID,
		RecHubAlert.EventRules.SiteBankID,
		RecHubAlert.EventRules.SiteClientAccountID,
		RecHubAlert.EventRules.IsActive,
		RecHubAlert.EventRules.EventRuleOperator,
		RecHubAlert.EventRules.EventRuleValue,
		RecHubAlert.[Events].EventID,
		RecHubAlert.[Events].EventName,
		RecHubAlert.[Events].EventLongName,
		RecHubAlert.[Events].EventType,
		CASE RecHubAlert.[Events].EventType
			WHEN 0 THEN 'Data Change'
			ELSE 'Programmatic'
		END AS EventTypeName,
		RecHubAlert.[Events].EventTable,
		RecHubAlert.[Events].EventColumn,
		RecHubAlert.[Events].MessageDefault
	FROM   
		RecHubAlert.EventRules
		INNER JOIN RecHubAlert.[Events]	ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
		LEFT OUTER JOIN CTE_UsersWorkgroups	ON RecHubAlert.EventRules.SiteBankID = CTE_UsersWorkgroups.SiteBankID
			AND RecHubAlert.EventRules.SiteClientAccountID = CTE_UsersWorkgroups.SiteClientAccountID
	WHERE 
		RecHubAlert.EventRules.EventRuleID = @parmEventRuleID
		AND (RecHubAlert.EventRules.SiteClientAccountID = -1 OR CTE_UsersWorkgroups.SiteClientAccountID IS NOT NULL);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH