--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_Alerts_Del_ByEventRuleID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_Alerts_Del_ByEventRuleID') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_Alerts_Del_ByEventRuleID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_Alerts_Del_ByEventRuleID
(	
	@parmEventRuleID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/09/2013
*
* Purpose: Remove Alerts belonging to an Event Rule
*
* Modification History
* 07/09/2013 WI 108363	EAS	Created
* 01/27/2015 WI	186535	JBS Change to soft delete. Mark IsActive = 0
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	--DELETE RecHubAlert.Alerts
	UPDATE RecHubAlert.Alerts
		SET RecHubAlert.Alerts.IsActive = 0
	WHERE  
		RecHubAlert.Alerts.EventRuleID = @parmEventRuleID;
	
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
