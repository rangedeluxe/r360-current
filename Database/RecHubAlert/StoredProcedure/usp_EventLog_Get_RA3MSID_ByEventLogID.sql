--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventLog_Get_RA3MSID_ByEventLogID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventLog_Get_RA3MSID_ByEventLogID') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventLog_Get_RA3MSID_ByEventLogID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventLog_Get_RA3MSID_ByEventLogID
(
	@parmEventLogID  BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 07/12/2013
*
* Purpose: Retrieve Email address and message for sending email notifications.
*			This is used by RecHubAlertsEventLog.dtsx
*
* Modification History
* 07/12/2013 WI 99899 JBS	Created
* 05/08/2014 WI 140839 JPB	Return MessageSubject if available.
* 08/25/2014 WI 161119 JBS	Get RA3MSID instead of EmailAddress from Users Table
* 01/27/2015 WI 186616 JBS	Add IsActive = 1 for Alerts
* 04/13/2015 WI 200513 JBS	Renamed procedure from usp_EventLog_Get_EmailAddress_ByEventLogID
*							And removed User.IsActive column
* 06/20/2016 WI 287928 JPB	Add hard CRLF for \r\n in text string.
* 03/30/2017 PT #142257299 JPB	Added EventLongName to MessageSubject COALESCE.
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY      
 
	SELECT 
		RecHubUser.Users.RA3MSID  AS RA3MSID,
		COALESCE(RecHubAlert.[Events].MessageSubject,RecHubAlert.[Events].EventLongName,RecHubAlert.[Events].EventName) AS MessageSubject,
		CAST(REPLACE(RecHubAlert.EventLog.EventMessage,'\r\n',CHAR(13)+CHAR(10)) AS VARCHAR(8000)) AS EventMessage	
	FROM 
		RecHubUser.Users 
		INNER JOIN RecHubAlert.Alerts
			ON RecHubUser.Users.UserID = RecHubAlert.Alerts.UserID
		INNER JOIN RecHubAlert.EventLog
			ON RecHubAlert.EventLog.AlertID = RecHubAlert.Alerts.AlertID
		INNER JOIN RecHubAlert.EventRules
			ON RecHubAlert.EventRules.EventRuleID = RecHubAlert.EventLog.EventRuleID
		INNER JOIN RecHubAlert.[Events]
			ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
	WHERE 
		RecHubAlert.EventLog.EventLogID = @parmEventLogID AND
		RecHubAlert.Alerts.IsActive = 1 AND 
		RecHubAlert.[Events].IsActive = 1;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH