--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubData_dimSiteCodes_CT_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubData_dimSiteCodes_CT_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubData_dimSiteCodes_CT_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubData_dimSiteCodes_CT_Get
(
	@parmStartLSN NVARCHAR(42),
	@parmEndLSN NVARCHAR(42)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/03/2013
*
* Purpose: Retrieve dimSiteCode CDC data by LSN range.
*
*
* Modification History
* 06/03/2013 WI 104216 JPB	Created.  Change reference for EventLevel column
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY

	/* conver the string LSNs to hex */
	SELECT	
		@StartLSN = sys.fn_cdc_hexstrtobin(@parmStartLSN),
		@EndLSN = sys.fn_cdc_hexstrtobin(@parmEndLSN);

	;WITH CDCRecords AS
	(
		SELECT 
			MAX(__$start_lsn) AS __$start_lsn,
			MAX(__$seqval) AS __$seqval,
			CDCdimSiteCodes.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
		FROM 
			cdc.fn_cdc_get_all_changes_RecHubData_dimSiteCodes(@StartLSN,@EndLSN,'all update old') CDCdimSiteCodes
			INNER JOIN RecHubAlert.EventRules ON RecHubAlert.EventRules.SiteCodeID = CDCdimSiteCodes.SiteCodeID
			INNER JOIN RecHubAlert.[Events] ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID		
		WHERE
			RecHubAlert.[Events].EventSchema = 'RecHubData'
			AND RecHubAlert.[Events].EventTable = 'dimSiteCodes'
			AND RecHubAlert.EventRules.IsActive = 1
			AND RecHubAlert.[Events].IsActive = 1
		GROUP BY 
			CDCdimSiteCodes.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			RecHubAlert.[Events].EventLevel,
			__$operation
	)
	SELECT 
		cdc.RecHubData_dimSiteCodes_CT.__$start_lsn,
		cdc.RecHubData_dimSiteCodes_CT.__$seqval,
		cdc.RecHubData_dimSiteCodes_CT.__$update_mask,
		cdc.RecHubData_dimSiteCodes_CT.__$operation,
		NULL AS BankKey,
		NULL AS OrganizationKey,
		NULL AS ClientAccountKey,
		CDCRecords.SiteCodeID,
		CDCRecords.SiteBankID,
		CDCRecords.SiteOrganizationID,
		CDCRecords.SiteClientAccountID,
		cdc.RecHubData_dimSiteCodes_CT.CWDBActiveSite,
		cdc.RecHubData_dimSiteCodes_CT.LocalTimeZoneBias,
		cdc.RecHubData_dimSiteCodes_CT.CurrentProcessingDate,
		cdc.RecHubData_dimSiteCodes_CT.ShortName,
		CDCRecords.EventLevel
	FROM cdc.RecHubData_dimSiteCodes_CT
		INNER JOIN CDCRecords ON CDCRecords.__$start_lsn = cdc.RecHubData_dimSiteCodes_CT.__$start_lsn
			AND COALESCE(CDCRecords.__$seqval,-1) = COALESCE(cdc.RecHubData_dimSiteCodes_CT.__$seqval,-1)
			AND CDCRecords.SiteCodeID = cdc.RecHubData_dimSiteCodes_CT.SiteCodeID
			AND CDCRecords.__$operation = cdc.RecHubData_dimSiteCodes_CT.__$operation;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
