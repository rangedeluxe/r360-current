--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubException_CommonExceptions_CT_GetLSNRange
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubException_CommonExceptions_CT_GetLSNRange') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubException_CommonExceptions_CT_GetLSNRange;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubException_CommonExceptions_CT_GetLSNRange
(
	@parmStartLSN NVARCHAR(42) OUT,
	@parmEndLSN NVARCHAR(42) OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/15/2013
*
* Purpose: Get the CommonExceptions CDC LSN range.
*
*
* Modification History
* 07/15/2013 WI 108888 JPB	Created
* 12/11/2014 WI 181640 JPB	Updates for schema changes.
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DataToProcess BIT,
		@StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY
	SELECT 
		@StartLSN = CONVERT(BINARY(10),VariableString, 1)
	FROM	
		RecHubsystem.SSISWorkingValues
	WHERE
		RecHubSystem.SSISWorkingValues.PackageName = 'RecHubAlertsCDC'
		AND RecHubSystem.SSISWorkingValues.VariableName = 'RecHubException_CommonExceptions_LSN';

	IF( @StartLSN IS NULL )
		SELECT 
			@StartLSN = sys.fn_cdc_get_min_lsn('RecHubException_IntegraPAYExceptions');
	ELSE
		SELECT 
			@StartLSN = sys.fn_cdc_increment_lsn(@StartLSN);

	SELECT 
		@EndLSN = MAX(__$start_lsn)
	FROM
		cdc.RecHubException_IntegraPAYExceptions_CT;

	IF( @EndLSN IS NULL 
		OR @StartLSN = @EndLSN 
		OR @StartLSN > @EndLSN )	BEGIN
		SELECT 
			@DataToProcess = 0;
	END
	ELSE
	BEGIN
		SELECT
			@parmStartLSN = UPPER(sys.fn_varbintohexstr(@StartLSN)),
			@parmEndLSN = UPPER(sys.fn_varbintohexstr(@EndLSN)),
			@DataToProcess = 1;
	END
	RETURN @DataToProcess;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 0;
END CATCH
