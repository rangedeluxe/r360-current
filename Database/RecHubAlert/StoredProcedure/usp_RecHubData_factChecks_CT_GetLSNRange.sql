--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubData_factChecks_CT_GetLSNRange
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubData_factChecks_CT_GetLSNRange') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubData_factChecks_CT_GetLSNRange
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubData_factChecks_CT_GetLSNRange
(
	@parmStartLSN NVARCHAR(42) OUT,
	@parmEndLSN NVARCHAR(42) OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/07/2013
*
* Purpose: Get the factCheck CDC LSN range.
*
*
* Modification History
* 06/07/2013 WI 104816 JPB	Created
* 06/10/2016 WI 277698 JPB	Adjust StartLSN if it is out of range
* 06/16/2016 WI 287263 JPB	Return Start/End LSN when there is no data in order
*								to keep working table to date.
* 08/01/2016 WI 293773 JBS  Handle mismatched value in table to something we can convert to Binary
*							Also Replaced EndLSN value if none exists
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @DataToProcess BIT,
		@CurrentMinLSN BINARY(10),
		@StartLSN BINARY(10),
		@EndLSN BINARY(10),
		@TempString VARCHAR(MAX);

BEGIN TRY

	SELECT 
		@TempString = 
		CASE 
			WHEN VariableString IS NULL	 THEN '0x00000000000000000000'		--WI 293773
			WHEN VariableString = ''	 THEN '0x00000000000000000000'
			ELSE VariableString
		END
    FROM   
        RecHubsystem.SSISWorkingValues
    WHERE
        RecHubSystem.SSISWorkingValues.PackageName = 'RecHubAlertsCDC'
        AND RecHubSystem.SSISWorkingValues.VariableName = 'RecHubData_factChecks_LSN';

    SET @StartLSN = CONVERT(BINARY(10),@TempString, 1 )		--WI 293773

	SELECT @CurrentMinLSN = sys.fn_cdc_get_min_lsn('RecHubData_factChecks');

	IF( @StartLSN IS NULL ) OR (@StartLSN < @CurrentMinLSN) 
	BEGIN
		IF( @StartLSN IS NOT NULL )
		BEGIN
			EXEC RecHubSystem.usp_SystemAuditMessages_Ins 
				@parmAuditSource='RecHubAlert.usp_RecHubData_factChecks_CT_GetLSNRange',
				@parmAuditTypeCode='Information',
				@parmAuditMessage='The stored procedure detected that the starting LSN was out of bounds during execution. The LSN was reset and the operation continued normally.';
		END
		SELECT 
			@StartLSN = @CurrentMinLSN;
	END
	ELSE
		SELECT 
			@StartLSN = sys.fn_cdc_increment_lsn(@StartLSN);

	SELECT 
		@EndLSN = MAX(__$start_lsn)
	FROM
		cdc.RecHubData_factChecks_CT;

	IF( @EndLSN IS NULL 
		OR @StartLSN = @EndLSN 
		OR @StartLSN > @EndLSN )
	BEGIN
		/* WI 287263 - If they start is equal or greater then the end, set the start to the end */
		IF @EndLSN IS NULL and @StartLSN IS NOT NULL
			SET @EndLSN = @StartLSN							--WI 293773		
		ELSE
			IF(  @StartLSN = @EndLSN 
				OR @StartLSN > @EndLSN )
				SET @StartLSN = @EndLSN;

		SET @DataToProcess = 0;
	END
	ELSE
	BEGIN
		SET @DataToProcess = 1;
	END

	SELECT 
		@parmStartLSN = UPPER(sys.fn_varbintohexstr(@StartLSN)),
		@parmEndLSN = UPPER(sys.fn_varbintohexstr(@EndLSN));

	RETURN @DataToProcess;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
	RETURN 0;
END CATCH
