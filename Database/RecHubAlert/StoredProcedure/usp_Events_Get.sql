--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_Events_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_Events_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_Events_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_Events_Get

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 06/26/2013
*
* Purpose: Get All Events
*
* Modification History
* 06/10/2013 WI 107421	EAS	Created.  Added EventLevel column
* 06/27/2014 WI 146682	EAS Remove IntegraPAY related Events from resultset.
* 02/09/2015 WI 188723	JPB	Added EventLongName to result set.
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY

	SELECT 
		RecHubAlert.[Events].EventID, 
		RecHubAlert.[Events].EventName,
		RecHubAlert.[Events].EventLongName,
		RecHubAlert.[Events].EventType, 
		RecHubAlert.[Events].EventLevel,
		RecHubAlert.[Events].IsActive,
		RecHubAlert.[Events].MessageDefault, 
		RecHubAlert.[Events].EventSchema,
		RecHubAlert.[Events].EventTable, 
		RecHubAlert.[Events].EventColumn,
		RecHubAlert.[Events].EventOperators
	FROM   
		RecHubAlert.[Events]
	WHERE  
		RecHubAlert.[Events].IsActive = 1
		AND LOWER(RecHubAlert.[Events].EventName) NOT LIKE 'integrapay%';

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH