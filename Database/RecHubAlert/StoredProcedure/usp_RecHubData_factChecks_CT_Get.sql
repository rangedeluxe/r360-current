--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubData_factChecks_CT_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubData_factChecks_CT_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubData_factChecks_CT_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubData_factChecks_CT_Get
(
	@parmStartLSN NVARCHAR(42),
	@parmEndLSN NVARCHAR(42)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/27/2013
*
* Purpose: Retrieve fact check CDC data by LSN range.
*
*
* Modification History
* 05/27/2013 WI 103923 JPB	Created. Changing source of EventLevel Column
* 05/05/2014 WI 139623 JPB	Account for NULL EventRules.SiteOrganizationID. (FP:139614)
* 05/14/2014 WI 142258 JPB	Added RoutingNumber. (FP:142198)
* 01/19/2016 WI 258562 JPB	Added BatchID for increase uniqueness of record selection
* 06/15/2016 WI 287192 JPB	Added SourceBatchID, Serial
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY

	/* conver the string LSNs to hex */
	SELECT	
		@StartLSN = sys.fn_cdc_hexstrtobin(@parmStartLSN),
		@EndLSN = sys.fn_cdc_hexstrtobin(@parmEndLSN);

	;WITH CDCRecords AS
	(
		SELECT 
			MAX(__$start_lsn) AS __$start_lsn,
			MAX(__$seqval) AS __$seqval,
			RecHubAlert.EventRules.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			CDCfactChecks.BankKey,
			CDCfactChecks.ClientAccountKey,
			CDCfactChecks.DepositDateKey,
			CDCfactChecks.ImmutableDateKey,
			CDCfactChecks.SourceProcessingDateKey,
			CDCfactChecks.BatchID,
			CDCfactChecks.TransactionID,
			CDCfactChecks.TxnSequence,
			CDCfactChecks.SequenceWithinTransaction,
			RecHubAlert.[Events].EventLevel,
			__$operation
		FROM 
			RecHubAlert.[Events]
			INNER JOIN RecHubAlert.EventRules ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
			INNER JOIN RecHubData.dimClientAccounts ON RecHubAlert.EventRules.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
				AND COALESCE(RecHubAlert.EventRules.SiteBankID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 2 THEN RecHubData.dimClientAccounts.SiteBankID ELSE COALESCE(RecHubAlert.EventRules.SiteBankID,-1) END
				AND COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) = 
					CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN 
						CASE 
							WHEN RecHubAlert.EventRules.SiteOrganizationID IS NOT NULL THEN RecHubData.dimClientAccounts.SiteOrganizationID
							ELSE COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) 
						END
					END
				AND COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN RecHubData.dimClientAccounts.SiteClientAccountID ELSE COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) END
			INNER JOIN cdc.fn_cdc_get_all_changes_RecHubData_factChecks(@StartLSN,@EndLSN,'all update old') AS CDCfactChecks
				ON RecHubData.dimClientAccounts.ClientAccountKey = CDCfactChecks.ClientAccountKey
		WHERE
			RecHubAlert.[Events].EventSchema = 'RecHubData'
			AND RecHubAlert.[Events].EventTable = 'factChecks'
			AND RecHubAlert.EventRules.IsActive = 1
			AND RecHubAlert.[Events].IsActive = 1
		GROUP BY 
			RecHubAlert.EventRules.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			CDCfactChecks.BankKey,
			CDCfactChecks.ClientAccountKey,
			CDCfactChecks.DepositDateKey,
			CDCfactChecks.ImmutableDateKey,
			CDCfactChecks.SourceProcessingDateKey,
			CDCfactChecks.BatchID,
			CDCfactChecks.TransactionID,
			CDCfactChecks.TxnSequence,
			CDCfactChecks.SequenceWithinTransaction,
			RecHubAlert.[Events].EventLevel,
			__$operation
	)
	SELECT 
		cdc.RecHubData_factChecks_CT.__$start_lsn,
		cdc.RecHubData_factChecks_CT.__$seqval,
		cdc.RecHubData_factChecks_CT.__$update_mask,
		cdc.RecHubData_factChecks_CT.__$operation,
		cdc.RecHubData_factChecks_CT.BankKey,
		cdc.RecHubData_factChecks_CT.OrganizationKey,
		cdc.RecHubData_factChecks_CT.ClientAccountKey,
		CDCRecords.SiteCodeID,
		CDCRecords.SiteBankID,
		CDCRecords.SiteOrganizationID,
		CDCRecords.SiteClientAccountID,
		cdc.RecHubData_factChecks_CT.factCheckKey,
		cdc.RecHubData_factChecks_CT.IsDeleted,
		cdc.RecHubData_factChecks_CT.DepositDateKey,
		cdc.RecHubData_factChecks_CT.ImmutableDateKey,
		cdc.RecHubData_factChecks_CT.SourceProcessingDateKey,
		cdc.RecHubData_factChecks_CT.BatchID,
		cdc.RecHubData_factChecks_CT.SourceBatchID,
		cdc.RecHubData_factChecks_CT.TransactionID,
		cdc.RecHubData_factChecks_CT.TxnSequence,
		cdc.RecHubData_factChecks_CT.SequenceWithinTransaction,
		cdc.RecHubData_factChecks_CT.NumericRoutingNumber,
		cdc.RecHubData_factChecks_CT.NumericSerial,
		cdc.RecHubData_factChecks_CT.Amount,
		cdc.RecHubData_factChecks_CT.RoutingNumber,
		cdc.RecHubData_factChecks_CT.Account,
		cdc.RecHubData_factChecks_CT.Serial,
		cdc.RecHubData_factChecks_CT.RemitterName,
		CDCRecords.EventLevel
	FROM 
		cdc.RecHubData_factChecks_CT
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = cdc.RecHubData_factChecks_CT.ClientAccountKey
		INNER JOIN CDCRecords ON CDCRecords.__$start_lsn = cdc.RecHubData_factChecks_CT.__$start_lsn
			AND COALESCE(CDCRecords.__$seqval,-1) = COALESCE(cdc.RecHubData_factChecks_CT.__$seqval,-1)
			AND CDCRecords.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
			AND COALESCE(CDCRecords.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN RecHubData.dimClientAccounts.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND COALESCE(CDCRecords.SiteOrganizationID,-1) = 
				CASE WHEN CDCRecords.EventLevel >= 3 THEN 
					CASE 
						WHEN CDCRecords.SiteOrganizationID IS NOT NULL THEN RecHubData.dimClientAccounts.SiteOrganizationID
						ELSE COALESCE(CDCRecords.SiteOrganizationID,-1) 
					END
				END
			AND COALESCE(CDCRecords.SiteClientAccountID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN RecHubData.dimClientAccounts.SiteClientAccountID ELSE COALESCE(CDCRecords.SiteClientAccountID,-1) END
			AND CDCRecords.__$operation = cdc.RecHubData_factChecks_CT.__$operation;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
