--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_Alerts_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_Alerts_Ins') IS NOT NULL
	   DROP PROCEDURE RecHubAlert.usp_Alerts_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_Alerts_Ins
(	
	@parmUserID INT = NULL,
	@parmEventRuleID INT,
	@parmAlertID INT OUTPUT	
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: EAS
* Date: 07/08/2013
*
* Purpose: Insert new Alerts record
*
* Modification History
* 07/08/2013  WI  108386	EAS	Created
* 01/27/2015  WI  186536	JBS	Add New column
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY

	DECLARE @ModDate	DATETIME;
	SET @ModDate = GETDATE();

	BEGIN
		INSERT INTO RecHubAlert.Alerts
		(
			RecHubAlert.Alerts.DeliveryMethodID,
			RecHubAlert.Alerts.EventRuleID,
			RecHubAlert.Alerts.UserID,
			RecHubAlert.Alerts.IsActive,
			RecHubAlert.Alerts.CreationDate,
			RecHubAlert.Alerts.CreatedBy,
			RecHubAlert.Alerts.ModificationDate,
			RecHubAlert.Alerts.ModifiedBy
		)
		VALUES
		(
			1,
			@parmEventRuleID,
			@parmUserID,
			1,
			@ModDate,
			SUSER_SNAME(),
			@ModDate,
			SUSER_SNAME()
		)
	END
	
	SELECT @parmAlertID = @@IDENTITY;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
