--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get_Events
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get_Events') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventRules_Get_Events
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get_Events
(
	@parmEventSchema	VARCHAR(128),
	@parmEventTable		VARCHAR(128)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/02/2013
*
* Purpose: Retrieve all EventRules.
*
*
* Modification History
* 06/02/2013 WI 104140 JPB	Created.  changed reference for EventLevel source
* 01/28/2015 WI 186949 JBS	Add check for IsActive = 1 for ALerts
* 12/18/2015 WI 253926 MGE	Rebuild
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT 
		RecHubAlert.Alerts.AlertID,
		RecHubAlert.EventRules.EventRuleID,
		RecHubAlert.EventRules.EventID,
		RecHubAlert.EventRules.SiteCodeID,
		RecHubAlert.EventRules.SiteBankID,
		RecHubAlert.EventRules.SiteOrganizationID,
		RecHubAlert.EventRules.SiteClientAccountID,
		RecHubAlert.[Events].EventLevel,
		RecHubAlert.[Events].EventColumn,
		RecHubAlert.EventRules.EventRuleOperator,
		RecHubAlert.EventRules.EventRuleValue,
		RecHubAlert.[Events].MessageDefault AS EventRuleMessage
	FROM
		RecHubAlert.EventRules
		INNER JOIN RecHubAlert.Alerts ON RecHubAlert.Alerts.EventRuleID = RecHubAlert.EventRules.EventRuleID
		INNER JOIN RecHubAlert.[Events] ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
	WHERE
		RecHubAlert.EventRules.IsActive = 1
		AND RecHubAlert.Alerts.IsActive = 1
		AND RecHubAlert.[Events].EventSchema = @parmEventSchema
		AND RecHubAlert.[Events].EventTable = @parmEventTable;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
