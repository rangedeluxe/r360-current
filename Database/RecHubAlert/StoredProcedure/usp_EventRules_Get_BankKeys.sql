--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventRules_Get_BankKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventRules_Get_BankKeys') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventRules_Get_BankKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventRules_Get_BankKeys
(
	@parmEventSchema	VARCHAR(128),
	@parmEventTable		VARCHAR(128)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/02/2013
*
* Purpose: Retrieve BankKeys associated with EventRules.
*
*
* Modification History
* 06/02/2013 WI 104164 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	SELECT 
		RecHubData.dimBanks.SiteBankID,
		RecHubData.dimBanks.BankKey
	FROM
		RecHubAlert.EventRules
		INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.SiteBankID = RecHubAlert.EventRules.SiteBankID
		INNER JOIN RecHubAlert.[Events] ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
	WHERE
		RecHubAlert.EventRules.IsActive = 1
		AND RecHubAlert.[Events].EventSchema = @parmEventSchema
		AND RecHubAlert.[Events].EventTable = @parmEventTable;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
