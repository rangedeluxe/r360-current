--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAlert">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubSystem_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_EventLog_SystemLevel_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_EventLog_SystemLevel_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_EventLog_SystemLevel_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_EventLog_SystemLevel_Ins
(
	@parmEventName				VARCHAR(30),
	@parmMessage				VARCHAR(1028) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Brandon Resheske
* Date: 05/08/2014
*
* Purpose: Creates a System-Level EventLog.  System-Level requires BankKey,
*          OrganizationKey, and WorkgroupKey to be -1.
*
*          The majority of the code came from usp_EventLog_Ins.
*
* Modification History
* 05/08/2014 WI 139594 BLR Initial Version.
* 01/19/2015 WI 185025 TWE insert all alert id's for given rule
* 01/21/2015 WI 185584 JBS	Moving in Default message if nothing passed in
* 02/05/2015 WI 188473 JBS Adding RecHubSystem_User to Exec permissions
* 02/10/2015 WI 189211 CMC Only insert for active alert records
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @EventID				SMALLINT,
		@EventLevel				TINYINT,
		@EventSourceKey			TINYINT,
		@ErrorDescription		VARCHAR(2000);
		
SET @ErrorDescription = '';

BEGIN TRY
 
 -- First grab out Event, kick out early if none exists.
	SELECT
		@EventID = COALESCE(RecHubAlert.[Events].EventID, NULL),
		@EventLevel= COALESCE(RecHubAlert.[Events].EventLevel, NULL)
	FROM
		RecHubAlert.[Events]
	WHERE
		RecHubAlert.[Events].EventName = @parmEventName;

	IF @EventID IS NULL
	BEGIN
	   SET @ErrorDescription = 'Alert Event Not found in Events Table for usp_EventLog_SystemLevel_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END

	IF @EventLevel IS NULL
	BEGIN
	   SET @ErrorDescription = 'Alert Event Level not valid for usp_EventLog_SystemLevel_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END

-- Now we grab the SourceKey.  This is a global variable, no input is required.
	SELECT 
		@EventSourceKey = COALESCE(RecHubData.DimBatchSources.BatchSourceKey, NULL)
	FROM 
		RecHubData.dimBatchSources
	WHERE
		RecHubData.dimBatchSources.ShortName = 'R360 Alerts';

	IF @EventSourceKey IS NULL
	BEGIN
	   SET @ErrorDescription = 'Event SourceKey not found in RecHubData.dimBatchSources table for usp_EventLog_SystemLevel_Ins '+@parmEventName;
	   RAISERROR(@ErrorDescription,16,1);
	END	

-- Finally Perform the Insert.
--    for every rule and alert

	INSERT INTO RecHubAlert.EventLog
       (
              RecHubAlert.EventLog.EventRuleID,
              RecHubAlert.EventLog.EventStatus,
              RecHubAlert.EventLog.AlertID,
              RecHubAlert.EventLog.BankKey,
              RecHubAlert.EventLog.OrganizationKey,
              RecHubAlert.EventLog.ClientAccountKey,
              RecHubAlert.EventLog.EventSourceKey,
              RecHubAlert.EventLog.CreationDate,
              RecHubAlert.EventLog.ModificationDate,
              RecHubAlert.EventLog.EventMessage
       )
       SELECT 
              RecHubAlert.EventRules.EventRuleID,
			  10, 
              RecHubAlert.Alerts.AlertID,
              -1,
              -1,
              -1,
              @EventSourceKey,
              GETDATE(), 
              GETDATE(),
			  COALESCE(@parmMessage, @parmEventName + ' - ' + COALESCE(RecHubAlert.EventRules.EventRuleDescription, ' ') + ' - No Message Provided')
       FROM 
              RecHubAlert.EventRules
			  INNER JOIN RecHubAlert.Alerts
			      ON RecHubAlert.Alerts.EventRuleID = RecHubAlert.EventRules.EventRuleID 
       WHERE
              RecHubAlert.EventRules.EventID = 2
              AND RecHubAlert.EventRules.IsActive = 1
              AND RecHubAlert.EventRules.SiteBankID = -1
              AND RecHubAlert.EventRules.SiteClientAccountID = -1
			  AND RecHubAlert.Alerts.IsActive = 1;
			  
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH