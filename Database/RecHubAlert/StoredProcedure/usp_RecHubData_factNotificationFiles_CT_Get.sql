--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAlert
--WFSScriptProcessorStoredProcedureName usp_RecHubData_factNotificationFiles_CT_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAlert.usp_RecHubData_factNotificationFiles_CT_Get') IS NOT NULL
       DROP PROCEDURE RecHubAlert.usp_RecHubData_factNotificationFiles_CT_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAlert.usp_RecHubData_factNotificationFiles_CT_Get
(
	@parmStartLSN NVARCHAR(42),
	@parmEndLSN NVARCHAR(42)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/04/2013
*
* Purpose: Retrieve fact check CDC data by LSN range.
*
*
* Modification History
* 06/04/2013 WI 104219 JPB	Created.  Change reference of EventLevel column to Events table
* 05/04/2014 WI 139624 JPB	Account for NULL EventRules.SiteOrganizationID. (FP:139614)
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @StartLSN BINARY(10),
		@EndLSN BINARY(10);

BEGIN TRY

	/* conver the string LSNs to hex */
	SELECT	
		@StartLSN = sys.fn_cdc_hexstrtobin(@parmStartLSN),
		@EndLSN = sys.fn_cdc_hexstrtobin(@parmEndLSN);


	;WITH CDCRecords AS
	(
		SELECT 
			MAX(__$start_lsn) AS __$start_lsn,
			MAX(__$seqval) AS __$seqval,
			RecHubAlert.EventRules.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			CDCFactNotificationFiles.NotificationMessageGroup,
			RecHubAlert.[Events].EventLevel,
			__$operation
		FROM 
			RecHubAlert.[Events] 
			INNER JOIN RecHubAlert.EventRules ON RecHubAlert.[Events].EventID = RecHubAlert.EventRules.EventID
			INNER JOIN RecHubData.dimClientAccounts ON RecHubAlert.EventRules.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
				AND COALESCE(RecHubAlert.EventRules.SiteBankID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 2 THEN RecHubData.dimClientAccounts.SiteBankID ELSE COALESCE(RecHubAlert.EventRules.SiteBankID,-1) END
				AND COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) = 
					CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN 
						CASE WHEN RecHubAlert.EventRules.SiteOrganizationID IS NOT NULL 
							THEN RecHubData.dimClientAccounts.SiteOrganizationID
							ELSE COALESCE(RecHubAlert.EventRules.SiteOrganizationID,-1) 
						END
					END
				AND COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) = CASE WHEN RecHubAlert.[Events].EventLevel >= 3 THEN RecHubData.dimClientAccounts.SiteClientAccountID ELSE COALESCE(RecHubAlert.EventRules.SiteClientAccountID,-1) END
			INNER JOIN cdc.fn_cdc_get_all_changes_RecHubData_factNotificationFiles(@StartLSN,@EndLSN,'all update old') AS CDCfactNotificationFiles
				ON RecHubData.dimClientAccounts.ClientAccountKey = CDCfactNotificationFiles.ClientAccountKey		
		WHERE
			RecHubAlert.[Events].EventSchema = 'RecHubData'
			AND RecHubAlert.[Events].EventTable = 'factNotificationFiles'
			AND RecHubAlert.EventRules.IsActive = 1
			AND RecHubAlert.[Events].IsActive = 1
		GROUP BY 
			RecHubAlert.EventRules.SiteCodeID,
			RecHubAlert.EventRules.SiteBankID,
			RecHubAlert.EventRules.SiteOrganizationID,
			RecHubAlert.EventRules.SiteClientAccountID,
			CDCfactNotificationFiles.NotificationMessageGroup,
			RecHubAlert.[Events].EventLevel,
			__$operation
	)
	SELECT 
		cdc.RecHubData_factNotificationFiles_CT.__$start_lsn,
		cdc.RecHubData_factNotificationFiles_CT.__$seqval,
		cdc.RecHubData_factNotificationFiles_CT.__$update_mask,
		cdc.RecHubData_factNotificationFiles_CT.__$operation,
		cdc.RecHubData_factNotificationFiles_CT.factNotificationFileKey,
		cdc.RecHubData_factNotificationFiles_CT.IsDeleted,
		cdc.RecHubData_factNotificationFiles_CT.BankKey,
		cdc.RecHubData_factNotificationFiles_CT.OrganizationKey,
		cdc.RecHubData_factNotificationFiles_CT.ClientAccountKey,
		CDCRecords.SiteCodeID,
		CDCRecords.SiteBankID,
		CDCRecords.SiteOrganizationID,
		CDCRecords.SiteClientAccountID,
		cdc.RecHubData_factNotificationFiles_CT.NotificationDateKey,
		cdc.RecHubData_factNotificationFiles_CT.NotificationMessageGroup,
		cdc.RecHubData_factNotificationFiles_CT.NotificationFileTypeKey,
		cdc.RecHubData_factNotificationFiles_CT.NotificationSourceKey,
		cdc.RecHubData_factNotificationFiles_CT.UserFileName,
		CDCRecords.EventLevel
	FROM 
		cdc.RecHubData_factNotificationFiles_CT
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = cdc.RecHubData_factNotificationFiles_CT.ClientAccountKey
		INNER JOIN CDCRecords ON CDCRecords.__$start_lsn = cdc.RecHubData_factNotificationFiles_CT.__$start_lsn
			AND COALESCE(CDCRecords.__$seqval,-1) = COALESCE(cdc.RecHubData_factNotificationFiles_CT.__$seqval,-1)
			AND CDCRecords.SiteCodeID = RecHubData.dimClientAccounts.SiteCodeID
			AND COALESCE(CDCRecords.SiteBankID,-1) = CASE WHEN CDCRecords.EventLevel >= 2 THEN RecHubData.dimClientAccounts.SiteBankID ELSE COALESCE(CDCRecords.SiteBankID,-1) END
			AND COALESCE(CDCRecords.SiteOrganizationID,-1) = 
				CASE WHEN CDCRecords.EventLevel >= 3 THEN 
					CASE WHEN CDCRecords.SiteOrganizationID IS NOT NULL 
						THEN RecHubData.dimClientAccounts.SiteOrganizationID
						ELSE COALESCE(CDCRecords.SiteOrganizationID,-1) 
					END
				END
			AND COALESCE(CDCRecords.SiteClientAccountID,-1) = CASE WHEN CDCRecords.EventLevel >= 3 THEN RecHubData.dimClientAccounts.SiteClientAccountID ELSE COALESCE(CDCRecords.SiteClientAccountID,-1) END
			AND CDCRecords.__$operation = cdc.RecHubData_factNotificationFiles_CT.__$operation;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
