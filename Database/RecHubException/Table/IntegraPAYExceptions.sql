--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName IntegraPAYExceptions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/13/2013
*
* Purpose: Holds data for IntegraPay Common Exception.
* PaymentSource
*	0 - Unknown
*   1 - integraPAY
*   2 - WebDDL
*   3 - ImageRPS   
*		   
*
* Modification History
* 05/13/2013 WI 101865 JBS	Created 
* 06/04/2013 WI 101865 WJS	Added TransactionID
* 05/05/2014 WI 138814 JBS	Changed table name from CommonExceptions and PK, 
*							Change BatchSourceKey TO SMALLINT
* 11/25/2014 WI 138814 JBS	Change BatchID to BIGINT
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.IntegraPAYExceptions
(
	IntegraPAYExceptionKey   BIGINT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_IntegraPAYExceptions PRIMARY KEY CLUSTERED,
	DepositDateKey		INT NOT NULL,
	DecisioningStatus	TINYINT NOT NULL,
	InProcessException	BIT NOT NULL,
	BatchSourceKey		SMALLINT NOT NULL, 
	BatchPaymentTypeKey	TINYINT,
	Deadline			DATETIME NOT NULL,
	CheckAmount			MONEY,
	GlobalBatchID		INT,
	SiteBankID			INT,
	SiteClientAccountID	INT,
	ImmutableDateKey	INT,
	BatchID				BIGINT,
	TransactionID		INT,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_IntegraPAYExceptions_CreationDate DEFAULT GETDATE(),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_IntegraPAYExceptions_ModificationDate DEFAULT GETDATE()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.IntegraPAYExceptions ADD 
	CONSTRAINT FK_IntegraPAYExceptions_dimBatchSources FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_IntegraPAYExceptions_dimBatchPaymentTypes FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey);