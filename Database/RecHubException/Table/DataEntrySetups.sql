--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName DataEntrySetups
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/03/2014
*
* Purpose: Holds data for a Common Exception DataEntry Setup.
*
* Modification History
* 03/03/2014 WI 130763 JBS	Created 
* 04/24/2014 WI 130763 KLC	Fixed copy/paste issue and foreign key reference
* 04/29/2014 WI 130763 KLC	Removed EntityID and made SiteBankID and SiteWorkGroupID
*							not nullable.  Not supporting default setups at the entity
*							or bank level.
* 10/23/2014 WI	173970 KLC	Added RequiresInvoice bit
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.DataEntrySetups
(
	DataEntrySetupKey	BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_DataEntrySetups PRIMARY KEY CLUSTERED,
	SiteBankID			INT NOT NULL,
	SiteWorkgroupID		INT NOT NULL,
	DeadLineDay			SMALLINT NULL,
	RequiresInvoice		BIT	NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_DataEntrySetups_CreationDate DEFAULT(GETDATE()),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_DataEntrySetups_ModificationDate DEFAULT(GETDATE()),
	CreatedBy			VARCHAR(128)
		CONSTRAINT DF_DataEntrySetups_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy			VARCHAR(128)
		CONSTRAINT DF_DataEntrySetups_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubException.DataEntrySetups.IDX_DataEntrySetups_SiteBankIDSiteWorkgroupID
CREATE NONCLUSTERED INDEX IDX_DataEntrySetups_SiteBankIDSiteWorkgroupID ON RecHubException.DataEntrySetups
(
	SiteBankID,
	SiteWorkgroupID
);