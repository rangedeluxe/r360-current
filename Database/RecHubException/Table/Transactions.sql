--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName Transactions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/25/2014
*
* Purpose: Holds data for a Common Exception Transactions.
*		   
*
* Modification History
* 02/25/2014 WI 130758 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.Transactions
(
	TransactionKey		BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_Transactions PRIMARY KEY,
	ExceptionBatchKey	BIGINT NOT NULL,
	TransactionID		INT NOT NULL,
	TxnSequence			INT NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_Transactions_CreationDate DEFAULT(GETDATE()),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_Transactions_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy			VARCHAR(128)
		CONSTRAINT DF_Transactions_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.Transactions ADD 
	CONSTRAINT FK_Transactions_ExceptionBatches FOREIGN KEY(ExceptionBatchKey) REFERENCES RecHubException.ExceptionBatches(ExceptionBatchKey);

--WFSScriptProcessorIndex RecHubException.Transactions.IDX_Transactions_ExceptionBatchKey
CREATE NONCLUSTERED INDEX IDX_Transactions_ExceptionBatchKey ON RecHubException.Transactions 
(
	ExceptionBatchKey
);