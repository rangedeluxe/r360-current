--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName Payments
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
* Purpose: Holds data for Common Exception Payments.
*		   
*
* Modification History
* 03/04/2014 WI 130759 JBS	Created 
* 07/28/2014 WI 130759 KLC	Added TransactionCode, Serial, RemitterName column
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.Payments
(
	PaymentKey				BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_Payments PRIMARY KEY,
	TransactionKey			BIGINT NOT NULL,
	TxnSequence				INT NOT NULL,
	SequenceWithinTransaction	INT NOT NULL,
	BatchSequence			INT NOT NULL,
	PaymentSequence			INT NOT NULL,
	Amount					MONEY NOT NULL,
	RT						VARCHAR(30) NULL,
	Account					VARCHAR(30) NULL,
	TransactionCode			VARCHAR(30) NULL,
	Serial					VARCHAR(30) NULL,
	RemitterName			VARCHAR(60) NULL,
	CreationDate			DATETIME NOT NULL
		CONSTRAINT DF_Payments_CreationDate DEFAULT(GETDATE()),
	ModificationDate		DATETIME NOT NULL
		CONSTRAINT DF_Payments_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy				VARCHAR(128)
		CONSTRAINT DF_Payments_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.Payments ADD 
	CONSTRAINT FK_Payments_Transactions FOREIGN KEY(TransactionKey) REFERENCES RecHubException.Transactions(TransactionKey);

--WFSScriptProcessorIndex RecHubException.Payments.IDX_Payments_TransactionKey
CREATE NONCLUSTERED INDEX IDX_Payments_TransactionKey ON RecHubException.Payments 
(
	TransactionKey
);