--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName BusinessRules
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/27/2014
*
* Purpose: Holds data for a Common Exception Business Rules.
*		   
*
* Modification History
* 02/27/2014 WI 130765 JBS	Created
* 04/29/2014 WI 130765 KLC	Added Batch Source, Payment Type, Payment Sub Type
*							and UserCanEdit
* 04/30/2014 WI 130765 KLC	Added FormatType column
* 05/12/2014 WI 130765 KLC	Moved Batch Source, Payment Type, Payment Sub Type
*							to DataEntrySetupFields table.
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.BusinessRules
(
	BusinessRuleKey			BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_BusinessRules PRIMARY KEY CLUSTERED,
	BusinessRuleDescription	NVARCHAR(128),
	MinLength				SMALLINT NOT NULL,
	[MaxLength]				SMALLINT NOT NULL,
	[Required]				BIT NOT NULL,
	UserCanEdit				BIT	NOT NULL,
	CheckDigitRoutineKey	BIGINT,
	FieldValidationKey		BIGINT,
	FormatType				TINYINT NOT NULL,
	Mask					VARCHAR(80),
	RequiredMessage			NVARCHAR(120),
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_BusinessRules_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_BusinessRules_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_BusinessRules_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_BusinessRules_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.BusinessRules ADD 
	CONSTRAINT FK_BusinessRules_CheckDigitRoutineKey FOREIGN KEY(CheckDigitRoutineKey) REFERENCES RecHubException.CheckDigitRoutines(CheckDigitRoutineKey),
	CONSTRAINT FK_BusinessRules_FieldValidationKey FOREIGN KEY(FieldValidationKey) REFERENCES RecHubException.FieldValidations(FieldValidationKey);

--WFSScriptProcessorIndex RecHubException.BusinessRules.IDX_BusinessRules_CheckDigitRoutineKey
CREATE INDEX IDX_BusinessRules_CheckDigitRoutineKey ON RecHubException.BusinessRules 
(
	CheckDigitRoutineKey
);

--WFSScriptProcessorIndex RecHubException.BusinessRules.IDX_BusinessRules_FieldValidationKey
CREATE INDEX IDX_BusinessRules_FieldValidationKey ON RecHubException.BusinessRules 
(
	FieldValidationKey
);