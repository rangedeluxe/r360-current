--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName FieldValidations
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/27/2014
*
* Purpose: Holds data for a Common Exception Field Validations.
*		   
*
* Modification History
* 02/27/2014 WI 130768 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.FieldValidations
(
	FieldValidationKey			BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_FieldValidations PRIMARY KEY CLUSTERED,
	FieldValidationName			NVARCHAR(128) NOT NULL,	
	LookupRequired				BIT NOT NULL,
	FailureMessage				NVARCHAR(120),
	FieldValidationTypeKey		TINYINT NOT NULL,
	FieldValidationSortMethodKey	TINYINT,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_FieldValidations_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_FieldValidations_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_FieldValidations_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_FieldValidations_ModifiedBy DEFAULT(SUSER_SNAME())   
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.FieldValidations ADD 
	CONSTRAINT FK_FieldValidations_FieldValidationTypes FOREIGN KEY(FieldValidationTypeKey) REFERENCES RecHubException.FieldValidationTypes(FieldValidationTypeKey),
	CONSTRAINT FK_FieldValidations_FieldValidationSortMethods FOREIGN KEY(FieldValidationSortMethodKey) REFERENCES RecHubException.FieldValidationSortMethods(FieldValidationSortMethodKey);

--WFSScriptProcessorIndex RecHubException.FieldValidations.IDX_FieldValidations_FieldValidationTypeKey
CREATE INDEX IDX_FieldValidations_FieldValidationTypeKey ON RecHubException.FieldValidations 
(
	FieldValidationTypeKey
);

--WFSScriptProcessorIndex RecHubException.FieldValidations.IDX_FieldValidations_FieldValidationSortMethodKey
CREATE INDEX IDX_FieldValidations_FieldValidationSortMethodKey ON RecHubException.FieldValidations 
(
	FieldValidationSortMethodKey
);