--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName PostDepositTransactionExceptions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 07/11/2017
*
* Purpose: Holds a row for each locked Post Deposit Exception Transaction.
*		   
*
* Modification History
* 07/11/2017 PT 144805301	MGE	Created 
* 07/17/2017 PT 144805301	MGE	Added natural key columns
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.PostDepositTransactionExceptions
(
	TransactionKey		BIGINT NOT NULL	IDENTITY(1,1)			
		CONSTRAINT PK_PostDepositTransactionExceptions PRIMARY KEY CLUSTERED,
	BatchID				BIGINT NOT NULL,
	DepositDateKey		INT	NOT NULL,
	TransactionID		INT NOT NULL,
	LockedDate			DATETIME NOT NULL
		CONSTRAINT DF_PostDepositTransactionExceptions_LockedDate DEFAULT(GETDATE()),
	LockedByUserID		INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.PostDepositTransactionExceptions WITH NOCHECK ADD
	CONSTRAINT FK_PostDepositTransactionExceptions_LockedByUserID FOREIGN KEY(LockedByUserID) REFERENCES RecHubUser.Users(UserID)
--WFSScriptProcessorIndex RecHubException.Transactions.IDX_PostDepositTransactionsExceptions_LockedByUserID
CREATE NONCLUSTERED INDEX IDX_PostDepositTransactionExceptions_LockedByUserID ON RecHubException.PostDepositTransactionExceptions 
(
	LockedByUserID
);


