--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName DecisioningTransactions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/13/2013
*
* Purpose: Holds data Decisioning Transactions.
*
* Modification History
* 05/13/2013 WI 101864 JBS	Created 
* 05/06/2014 WI 139983 JBS  Add FK to Transactions, TransactionStatus and IntegraPAYExceptions tables
*							change Key field name to DecisioningTransactionKey, Add ModifiedBy column
*							Removing Unique index IDX_DecisioningTransactions_UserCommonExceptionID
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.DecisioningTransactions
(
	DecisioningTransactionKey  BIGINT NOT NULL IDENTITY(1,1) 
		CONSTRAINT PK_DecisioningTransactions PRIMARY KEY CLUSTERED,
	TransactionKey			BIGINT,
	IntegraPAYExceptionKey	BIGINT,
	UserID					INT,
	TransactionStatusKey	SMALLINT NOT NULL,
	TransactionBeginWork	DATETIME NOT NULL,
	TransactionReset		BIT	NOT NULL,
	TransactionEndWork		DATETIME,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_DecisioningTransactions_CreationDate DEFAULT GETDATE(),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_decisioningTransactions_ModificationDate DEFAULT GETDATE(),
	ModifiedBy			VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.DecisioningTransactions ADD 
	CONSTRAINT FK_DecisioningTransactions_Users FOREIGN KEY (UserID) REFERENCES RecHubUser.Users(UserID),
	CONSTRAINT FK_DecisioningTransactions_IntegraPAYExceptions FOREIGN KEY (IntegraPAYExceptionKey) REFERENCES RecHubException.IntegraPAYExceptions(IntegraPAYExceptionKey),
	CONSTRAINT FK_DecisioningTransactions_Transactions FOREIGN KEY (TransactionKey) REFERENCES RecHubException.Transactions(TransactionKey),
	CONSTRAINT FK_DecisioningTransactions_TransactionStatuses FOREIGN KEY (TransactionStatusKey) REFERENCES RecHubException.TransactionStatuses(TransactionStatusKey);
--WFSScriptProcessorIndex RecHubException.DecisioningTransactions.IDX_DecisioningTransactions_IntegraPAYExceptionKey
CREATE NONCLUSTERED INDEX IDX_DecisioningTransactions_IntegraPAYExceptionKey ON RecHubException.DecisioningTransactions
(
	IntegraPAYExceptionKey ASC
);
--WFSScriptProcessorIndex RecHubException.DecisioningTransactions.IDX_DecisioningTransactions_TransactionKey
CREATE NONCLUSTERED INDEX IDX_DecisioningTransactions_TransactionKey ON RecHubException.DecisioningTransactions
(
	TransactionKey ASC
);