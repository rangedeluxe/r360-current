--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName DataEntrySetupFields
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/03/2014
*
* Purpose: Holds data for a Common Exception DataEntry Setup Fields.
*
* Modification History
* 03/03/2014 WI 130764 JBS	Created 
* 05/12/2014 WI 130764 KLC	Added Batch Source, Payment Type, Payment Sub Type
* 10/01/2014 WI 168950 KLC	Added Exception Display Order
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.DataEntrySetupFields
(
	DataEntrySetupFieldKey	BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_DataEntrySetupFields PRIMARY KEY CLUSTERED,
	DataEntrySetupKey		BIGINT NOT NULL,
	BatchSourceKey			SMALLINT NULL,
	BatchPaymentTypeKey		TINYINT NULL,
	BatchPaymentSubTypeKey	TINYINT NULL,
	BusinessRuleKey			BIGINT NULL,
	TableType				TINYINT NOT NULL,
	FldName					VARCHAR(128) NOT NULL,
	ExceptionDisplayOrder	TINYINT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_DataEntrySetupFields_CreationDate DEFAULT(GETDATE()),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_DataEntrySetupFields_ModificationDate DEFAULT(GETDATE()),
	CreatedBy			VARCHAR(128)
		CONSTRAINT DF_DataEntrySetupFields_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy			VARCHAR(128)
		CONSTRAINT DF_DataEntrySetupFields_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.DataEntrySetupFields ADD 
	CONSTRAINT FK_DataEntrySetupFields_BusinessRules FOREIGN KEY(BusinessRuleKey) REFERENCES RecHubException.BusinessRules(BusinessRuleKey),
	CONSTRAINT FK_DataEntrySetupFields_DataEntrySetups FOREIGN KEY(DataEntrySetupKey) REFERENCES RecHubException.DataEntrySetups(DataEntrySetupKey),
	CONSTRAINT FK_DataEntrySetupFields_BatchSourceKey FOREIGN KEY(BatchSourceKey) REFERENCES RecHubData.dimBatchSources(BatchSourceKey),
	CONSTRAINT FK_DataEntrySetupFields_BatchPaymentTypeKey FOREIGN KEY(BatchPaymentTypeKey) REFERENCES RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey),
	CONSTRAINT FK_DataEntrySetupFields_BatchPaymentSubTypeKey FOREIGN KEY(BatchPaymentSubTypeKey) REFERENCES RecHubData.dimBatchPaymentSubTypes(BatchPaymentSubTypeKey);

--WFSScriptProcessorIndex RecHubException.DataEntrySetupFields.IDX_DataEntrySetupFields_DataEntrySetupKey
CREATE NONCLUSTERED INDEX IDX_DataEntrySetupFields_DataEntrySetupKey ON RecHubException.DataEntrySetupFields 
(
	DataEntrySetupKey
);
--WFSScriptProcessorIndex RecHubException.DataEntrySetupFields.IDX_DataEntrySetupFields_BusinessRuleKey
CREATE NONCLUSTERED INDEX IDX_DataEntrySetupFields_BusinessRuleKey ON RecHubException.DataEntrySetupFields 
(
	BusinessRuleKey
);