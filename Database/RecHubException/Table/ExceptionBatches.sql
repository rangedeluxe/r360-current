--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName ExceptionBatches
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/25/2014
*
* Purpose: Holds data for a Common Exception Batches.
*
* Modification History
* 02/25/2014 WI 130757 JBS	Created 
* 04/24/2014 WI 130757 KLC	Fixed foreign key reference table name
* 06/03/2014 WI 143494 JBS	Adding SourceBatchID.  Changing BatchID Data type for Batch collisions
*							Changing BatchSourceKey to SmallINT
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.ExceptionBatches
(
	ExceptionBatchKey	BIGINT NOT NULL IDENTITY(1,1),
	DataEntrySetupKey	BIGINT NOT NULL,
	SiteBankID			INT NOT NULL,
	SiteWorkgroupID		INT NOT NULL,
	BatchID				BIGINT NOT NULL,
	SourceBatchID		BIGINT NOT NULL,
	DepositDateKey		INT NOT NULL,
	ImmutableDateKey	INT NOT NULL,
	SourceProcessingDateKey	INT NOT NULL,
	BatchSourceKey		SMALLINT NOT NULL, 
	BatchPaymentTypeKey	TINYINT	NOT NULL,
	BatchStatusKey		SMALLINT NOT NULL,
	CreationDate		DATETIME NOT NULL
		CONSTRAINT DF_ExceptionBatches_CreationDate DEFAULT(GETDATE()),
	ModificationDate	DATETIME NOT NULL
		CONSTRAINT DF_ExceptionBatches_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy			VARCHAR(128)
		CONSTRAINT DF_ExceptionBatches_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.ExceptionBatches ADD 
	CONSTRAINT PK_ExceptionBatches PRIMARY KEY NONCLUSTERED (ExceptionBatchKey),
	CONSTRAINT FK_ExceptionBatches_DataEntrySetups FOREIGN KEY(DataEntrySetupKey) REFERENCES RecHubException.DataEntrySetups(DataEntrySetupKey),
	CONSTRAINT FK_ExceptionBatches_BatchStatuses FOREIGN KEY(BatchStatusKey) REFERENCES RecHubException.BatchStatuses(BatchStatusKey);
--WFSScriptProcessorIndex RecHubException.ExceptionBatches.IDX_ExceptionBatches_DepositDateBatchKey
CREATE CLUSTERED INDEX IDX_ExceptionBatches_DepositDateBatchKey ON RecHubException.ExceptionBatches 
(
	DepositDateKey,
	ExceptionBatchKey
);
--WFSScriptProcessorIndex RecHubException.ExceptionBatches.IDX_ExceptionBatches_DataEntrySetupKey
CREATE NONCLUSTERED INDEX IDX_ExceptionBatches_DataEntrySetupKey ON RecHubException.ExceptionBatches 
(
	DataEntrySetupKey
);
--WFSScriptProcessorIndex RecHubException.ExceptionBatches.IDX_ExceptionBatches_ImmutableDateKeyBatchID
CREATE NONCLUSTERED INDEX IDX_ExceptionBatches_ImmutableDateKeyBatchID ON RecHubException.ExceptionBatches 
(
	ImmutableDateKey,
	BatchID
);