--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName FieldValidationTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/27/2014
*
* Purpose: Holds data for Common Exceptions Field ValidationTypes.
*		   
*
* Modification History
* 02/27/2014 WI 130769 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.FieldValidationTypes
(
	FieldValidationTypeKey		TINYINT NOT NULL
		CONSTRAINT PK_FieldValidationTypes PRIMARY KEY CLUSTERED,
	TypeDescription	NVARCHAR(128) NOT NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_FieldValidationTypes_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_FieldValidationTypes_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_FieldValidationTypes_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_FieldValidationTypes_ModifiedBy DEFAULT(SUSER_SNAME()) 
);
--WFSScriptProcessorTableProperties