--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName FieldValidationValues
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
* Purpose: Holds data for a Common Exception Field Validation Values.
*		   
*
* Modification History
* 03/04/2014 WI 130771 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.FieldValidationValues
(
	FieldValidationValueKey		BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_FieldValidationValues PRIMARY KEY NONCLUSTERED,
	FieldValidationKey			BIGINT NOT NULL,	
	Value						SQL_VARIANT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.FieldValidationValues ADD 
	CONSTRAINT FK_FieldValidationValues_FieldValidations FOREIGN KEY(FieldValidationKey) REFERENCES RecHubException.FieldValidations(FieldValidationKey);

--WFSScriptProcessorIndex RecHubException.FieldValidationValues.IDX_FieldValidationValues_FieldValidationKey
CREATE CLUSTERED INDEX IDX_FieldValidationValues_FieldValidationKey ON RecHubException.FieldValidationValues 
(
	FieldValidationKey
);