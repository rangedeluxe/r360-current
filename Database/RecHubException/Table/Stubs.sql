--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName Stubs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
* Purpose: Holds data for Common Exception Stubs.
*		   
*
* Modification History
* 03/04/2014 WI 130760 JBS	Created 
* 07/28/2014 WI 130760 KLC	Added AccountNumber column
* 09/08/2014 WI 164106 CMC  Changed Amount to be nullable
* 10/22/2014 WI 173619 KLC	Added IsDeleted column
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.Stubs
(
	StubKey					BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_Stubs PRIMARY KEY,
	TransactionKey			BIGINT NOT NULL,
	TxnSequence				INT NOT NULL,
	SequenceWithinTransaction	INT NOT NULL,
	BatchSequence			INT NOT NULL,
	StubSequence			INT NOT NULL,
	Amount					MONEY NULL,
	AccountNumber			VARCHAR(80) NULL,
	IsDeleted				BIT NOT NULL DEFAULT(0),
	CreationDate			DATETIME NOT NULL
		CONSTRAINT DF_Stubs_CreationDate DEFAULT(GETDATE()),
	ModificationDate		DATETIME NOT NULL
		CONSTRAINT DF_Stubs_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy				VARCHAR(128)
		CONSTRAINT DF_Stubs_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.Stubs ADD 
	CONSTRAINT FK_Stubs_Transactions FOREIGN KEY(TransactionKey) REFERENCES RecHubException.Transactions(TransactionKey);

--WFSScriptProcessorIndex RecHubException.Stubs.IDX_Stubs_TransactionKey
CREATE NONCLUSTERED INDEX IDX_Stubs_TransactionKey ON RecHubException.Stubs 
(
	TransactionKey
);