--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName PostDepositBatchExceptions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/04/2017
*
* Purpose: Store basic information about a batch that has an exception. The table
*		will be used by the DIT SSIS package to determine if a batch can be 
*		reimported. If a batch exists in this table, the batch cannot be reimported.
*		   
*
* Modification History
* 08/04/2017 PT 149496367 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.PostDepositBatchExceptions
(
	PostDepositBatchExceptionKey BIGINT IDENTITY(1,1),
	DepositDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	CreationDate DATETIME2(0) NOT NULL
) $(OnDataPartition);
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.PostDepositBatchExceptions ADD 
	CONSTRAINT PK_PostDepositBatchExceptions PRIMARY KEY NONCLUSTERED (PostDepositBatchExceptionKey,DepositDateKey),
	CONSTRAINT FK_PostDepositBatchExceptions_DepositDate FOREIGN KEY(DepositDateKey) REFERENCES RecHubData.dimDates(DateKey);

--WFSScriptProcessorIndex RecHubException.PostDepositBatchExceptions.IDX_PostDepositBatchExceptions_DepositDatePostDepositBatchExceptionKey
CREATE CLUSTERED INDEX IDX_PostDepositBatchExceptions_DepositDatePostDepositBatchExceptionKey ON RecHubException.PostDepositBatchExceptions 
(
	DepositDateKey,
	PostDepositBatchExceptionKey
) $(OnDataPartition);

--WFSScriptProcessorIndex RecHubException.PostDepositBatchExceptions.IDX_PostDepositBatchExceptions_DepositDateKey_BatchID
CREATE UNIQUE NONCLUSTERED INDEX IDX_PostDepositBatchExceptions_DepositDateKey_BatchID ON RecHubException.PostDepositBatchExceptions
(
	DepositDateKey,
	BatchID
) $(OnDataPartition);