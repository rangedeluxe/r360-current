--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName PostDepositDataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Stores what fields for a given transaction/batch sequence that can 
*		be edited by the end user.
*
*
* Modification History
* 08/24/2017 PT 149441599 JPB	Created
* 09/25/2017 PT 147282137 MGE	Added IsUIAddedField (because only those can be deleted by UI) 
*************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.PostDepositDataEntryDetails
(
	PostDepositDataEntryDetailsKey BIGINT NOT NULL	IDENTITY(1,1)			
		CONSTRAINT PK_PostDepositDataEntryDetails PRIMARY KEY CLUSTERED,
	DepositDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	InputBatchSequence INT NOT NULL,
	StubSequence INT NOT NULL,
	IsCheck BIT NOT NULL,
	IsUIAddedField BIT NOT NULL
		CONSTRAINT DF_dimImportTypes_CreationDate DEFAULT 0,
	FieldName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
