--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName CheckDigitRoutines
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/27/2014
*
* Purpose: Holds data for a Common Exception Check Digit Routines.
*		   
*
* Modification History
* 02/27/2014 WI 130766 JBS	Created
* 04/30/2014 WI 130766 KLC	Changed WeightPatternValue from INT to NVARCHAR(40)
* 04/30/2014 WI 130766 KLC	Added Offset column and updated nullable settings
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.CheckDigitRoutines
(
	CheckDigitRoutineKey	BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_CheckDigitRoutines PRIMARY KEY CLUSTERED (CheckDigitRoutineKey),
	CheckDigitRoutineName	NVARCHAR(128) NULL,	
	Method					INT	NOT NULL,
	Offset					SMALLINT NOT NULL,
	Modulus					INT NOT NULL,
	Compliment				INT NOT NULL,
	WeightPatternDirection	INT NOT NULL,
	WeightPatternValue		NVARCHAR(40) NOT NULL,
	IgnoreSpaces			BIT NOT NULL,
	Remainder10Replacement	INT NOT NULL,
	Remainder11Replacement	INT NOT NULL,
	FailureMessage			NVARCHAR(120) NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_CheckDigitRoutines_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_CheckDigitRoutines_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_CheckDigitRoutines_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_CheckDigitRoutines_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties