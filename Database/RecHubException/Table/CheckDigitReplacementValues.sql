--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName CheckDigitReplacementValues
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 02/27/2014
*
* Purpose: Holds data for a Common Exception Check Digit Replacement Values.
*		   
*
* Modification History
* 02/27/2014 WI 130767 JBS	Created 
* 04/30/2014 WI 130767 KLC	Changed data types for value columns
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.CheckDigitReplacementValues
(
	CheckDigitReplacementValueKey	BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_CheckDigitReplacementValues PRIMARY KEY NONCLUSTERED,
	CheckDigitRoutineKey			BIGINT NOT NULL,
	OriginalValue					NCHAR NOT NULL,
	ReplacementValue				TINYINT NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_CheckDigitReplacementValues_CreationDate DEFAULT(GETDATE()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_CheckDigitReplacementValues_ModificationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_CheckDigitReplacementValues_CreatedBy DEFAULT(SUSER_SNAME()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_CheckDigitReplacementValues_ModifiedBy DEFAULT(SUSER_SNAME())       
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.CheckDigitReplacementValues ADD 
	CONSTRAINT FK_CheckDigitReplacementValues_CheckDigitRoutines FOREIGN KEY(CheckDigitRoutineKey) REFERENCES RecHubException.CheckDigitRoutines(CheckDigitRoutineKey);

--WFSScriptProcessorIndex RecHubException.CheckDigitReplacementValues.IDX_CheckDigitReplacementValues_CheckDigitRoutineKey
CREATE CLUSTERED INDEX IDX_CheckDigitReplacementValues_CheckDigitRoutineKey ON RecHubException.CheckDigitReplacementValues 
(
	CheckDigitRoutineKey
);