--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName IntegraPAYExceptionErrors
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: PS
* Date: 09/05/2013
*
* Purpose:  Used in the CommonException SSIS package
*               
*
* Modification History
* 09/05/2013 WI 113309 PS   Created 
* 05/05/2014 WI 138815 JBS	Change table name to IntegraPAYExceptionErrors and name of Primary Key
* 11/25/2014 WI 138815 JBS	Change BatchSourceKey to SMALLINT and BatchID to BIGINT
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.IntegraPAYExceptionErrors
(
    IntegraPAYExceptionErrorKey BIGINT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_IntegraPAYExceptionErrors PRIMARY KEY CLUSTERED,
    IsDeleted			BIT NOT NULL,
    DepositDateKey		INT NULL,
    DecisioningStatus	TINYINT NULL,
    InProcessException	BIT NULL,
    BatchSourceKey		SMALLINT NULL,
    BatchPaymentTypeKey	TINYINT NULL,
	Deadline			DATETIME NULL,
    CheckAmount			MONEY NULL,
    GlobalBatchID		INT NULL,
    SiteBankID			INT NULL,
    SiteClientAccountID	INT NULL,
    ImmutableDateKey	INT NULL,
    BatchID				BIGINT NULL,
    TransactionID		INT NULL,
    CreationDate		DATETIME NULL,
    ModificationDate	DATETIME NULL,
    CombinedError		NVARCHAR(250) NULL
);
--WFSScriptProcessorTableProperties
