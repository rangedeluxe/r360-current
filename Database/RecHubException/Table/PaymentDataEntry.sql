--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName PaymentDataEntry
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
* Purpose: Holds data for Common Exception Payment Data Entry.
*		   
*
* Modification History
* 03/04/2014 WI 130761 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.PaymentDataEntry
(
	PaymentDataEntryKey		BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_PaymentDataEntry PRIMARY KEY,
	PaymentKey				BIGINT NOT NULL,
	FldName					VARCHAR(128) NOT NULL,
	Value					SQL_VARIANT NOT NULL,
	CreationDate			DATETIME NOT NULL
		CONSTRAINT DF_PaymentDataEntry_CreationDate DEFAULT(GETDATE()),
	ModificationDate		DATETIME NOT NULL
		CONSTRAINT DF_PaymentDataEntry_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy				VARCHAR(128)
		CONSTRAINT DF_PaymentDataEntry_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.PaymentDataEntry ADD 
	CONSTRAINT FK_PaymentDataEntry_Payments FOREIGN KEY(PaymentKey) REFERENCES RecHubException.Payments(PaymentKey);

--WFSScriptProcessorIndex RecHubException.PaymentDataEntry.IDX_PaymentDataEntry_PaymentKey
CREATE NONCLUSTERED INDEX IDX_PaymentDataEntry_PaymentKey ON RecHubException.PaymentDataEntry 
(
	PaymentKey
);