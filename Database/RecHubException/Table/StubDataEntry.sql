--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName StubDataEntry
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
*
* Purpose: Holds data for Common Exception Stubs Data Entry.
*
* Modification History
* 03/04/2014 WI 130762 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.StubDataEntry
(
	StubDataEntryKey		BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_StubDataEntry PRIMARY KEY,
	StubKey					BIGINT NOT NULL,
	FldName					VARCHAR(128) NOT NULL,
	Value					SQL_VARIANT NOT NULL,
	CreationDate			DATETIME NOT NULL
		CONSTRAINT DF_StubDataEntry_CreationDate DEFAULT(GETDATE()),
	ModificationDate		DATETIME NOT NULL
		CONSTRAINT DF_StubDataEntry_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy				VARCHAR(128)
		CONSTRAINT DF_StubDataEntry_ModifiedBy DEFAULT(SUSER_SNAME())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubException.StubDataEntry ADD 
	CONSTRAINT FK_StubDataEntry_Stubs FOREIGN KEY(StubKey) REFERENCES RecHubException.Stubs(StubKey);

--WFSScriptProcessorIndex RecHubException.StubDataEntry.IDX_StubDataEntry_StubKey
CREATE NONCLUSTERED INDEX IDX_StubDataEntry_StubKey ON RecHubException.StubDataEntry 
(
	StubKey
);