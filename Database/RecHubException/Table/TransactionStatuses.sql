--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorTableName TransactionStatuses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/04/2014
*
* Purpose: Holds Statuses for the Common Exception Transaction.
*
* Modification History
* 03/04/2014 WI 130795 JBS	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubException.TransactionStatuses
(
	TransactionStatusKey			SMALLINT NOT NULL
		CONSTRAINT PK_TransactionStatuses PRIMARY KEY,
	TransactionStatusName			VARCHAR(36) NOT NULL,
	TransactionStatusDescription	NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties