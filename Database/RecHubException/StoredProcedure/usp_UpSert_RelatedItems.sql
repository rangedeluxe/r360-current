--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_UpSert_RelatedItems
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_UpSert_RelatedItems') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_UpSert_RelatedItems
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_UpSert_RelatedItems
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Update an existing factStubs/factDataEntryDatail records from 
*		PostDepositExceptions
*
* Modification History
* 08/24/2017 PT 149441599	JPB Created
******************************************************************************/
DEClARE @LocalTran BIT,
		@ModificationDate DATETIME = GETDATE();
BEGIN TRY

	BEGIN TRAN
	SET @LocalTran = 1;
	/* insert/update the fields that can be edited, PostDepositDataEntryDetails */
	EXEC RecHubException.usp_PostDepositDataEntryDetails_UpSert
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmDataEntryValues = @parmDataEntryValues;

	/* Update RecHubData.factStubs */
	EXEC RecHubException.usp_factStubs_UpSert
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmDataEntryValues = @parmDataEntryValues;

	/* Update RecHubData.factDataEntryDetails */
	EXEC RecHubException.usp_factDataEntryDetails_UpSert
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmDataEntryValues = @parmDataEntryValues;

	COMMIT;

END TRY
BEGIN CATCH
	IF( @LocalTran = 1 )
		ROLLBACK TRAN;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH