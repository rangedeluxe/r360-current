--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ReceivablesExceptionsByType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Report_ReceivablesExceptionsByType') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_Report_ReceivablesExceptionsByType;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Report_ReceivablesExceptionsByType
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the exception data needed for the Receivables Summary and Exceptions By Type report
*
* Modification History
* Created
* 05/24/2013 WI 107244 KLC	Created.
* 07/10/2013 WI	107244 KLC	Updated auditing to write individual parameters instead of the xml.
* 07/11/2013 WI 108627 KLC	Updated to make filter date optional, taking second date parameter and default to current date.
* 07/26/2013 WI 107244 JBS	Changing called proc from usp_OLClientAccounts_Get_ByUserID
*							to usp_OLClientAccounts_Get_ByUserID_Reporting
* 07/26/2013 WI 107244 KLC	Updated to use payment type's long name
* 09/09/2013 WI 107244 KLC	Updated to return 0's as unresolved anything else as resolved
* 09/13/2013 WI 107244 KLC	Updated to remove join to RecHubData.dimClientAccounts as unneeded and it was creating duplicate records
* 10/31/2013 WI 119762 CMC	Updated to use new Org Hierarchy Tree
* 11/13/2013 WI 122417 CMC	Fix to match dashboard
* 11/20/2013 WI 123166 CMC	Fix to match dashboard when deeper than two levels
* 11/26/2013 WI 123622 CMC	Apply Deadline Restrictions
* 07/10/2014 WI 151285 KLC	Updated for RAAM integration
* 07/30/2014 WI 151285 KLC	Updated to only return data for active workgroups
* 10/16/2014 WI 172743 LA	Pass in Entity so we can join on Client Account
* 06/16/2015 WI 218762 RDS	Short-circuit if exceptions are not being shown on the report
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDateKey		INT,
			@filterWGExtRefID	VARCHAR(40),
			@filterEntities		XML,
			@showExceptions		BIT,
			@rootEntityID		INT,
			@currentTime		VARCHAR(8) = CONVERT(VARCHAR(8), GETDATE(), 108);
			
	SELECT	@filterDateKey = CASE WHEN Parms.val.value('(FD2)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(FD2)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@filterEntities = CASE WHEN Parms.val.exist('ENTs') = 1 THEN Parms.val.query('ENTs') ELSE NULL END,
			@filterWGExtRefID = CASE WHEN Parms.val.value('(WG)[1]', 'VARCHAR(40)') <> '' THEN REPLACE(Parms.val.value('(WG)[1]', 'VARCHAR(40)'), '|', '.') ELSE NULL END,
			@showExceptions = CASE WHEN Parms.val.value('(SEC)[1]', 'VARCHAR(1)') <> '' THEN Parms.val.value('(SEC)[1]', 'BIT') ELSE 0 END
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);

	-- Short-circuit if the report isn't going to show the exception data
	IF @showExceptions = 0
	BEGIN
		RETURN;
	END;

	/*If no filter date was provided, then we need to default to the current date for filtering.  That is typically what the report will be run for
		except for development testing where providing a date may be useful*/
	IF @filterDateKey IS NULL
	BEGIN
		SET @filterDateKey = CONVERT(VARCHAR(8), GETDATE(), 112);
	END
	
	--Get the workgroup or list of all workgroups for the entities requested
	DECLARE @tblWorkgroups TABLE (SiteBankID INT, SiteClientAccountID INT, PRIMARY KEY(SiteBankID, SiteClientAccountID));
	IF @filterWGExtRefID IS NOT NULL
	BEGIN
		DECLARE @siteBankID INT,
				@siteClientAccountID INT;

		--parse the external reference id
		SET @siteBankID = PARSENAME(@filterWGExtRefID, 2);
		SET @siteClientAccountID = PARSENAME(@filterWGExtRefID, 1);

		SELECT @rootEntityID = RecHubUser.OLWorkgroups.EntityID
		FROM RecHubUser.OLWorkgroups
			JOIN RecHubUser.SessionClientAccountEntitlements
				ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
					AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
					AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
					AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
		WHERE RecHubUser.OLWorkgroups.SiteBankID = @siteBankID
			AND RecHubUser.OLWorkgroups.SiteClientAccountID = @siteClientAccountID;

		IF @rootEntityID IS NOT NULL
		BEGIN
			INSERT INTO @tblWorkgroups(SiteBankID, SiteClientAccountID)
			SELECT DISTINCT RecHubData.dimClientAccountsView.SiteBankID,
							RecHubData.dimClientAccountsView.SiteClientAccountID
			FROM RecHubData.dimClientAccountsView
				JOIN RecHubUser.OLWorkgroups
					ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
						AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
				JOIN RecHubUser.SessionClientAccountEntitlements
					ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
						AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
						AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
						AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
			WHERE RecHubData.dimClientAccountsView.SiteBankID = @siteBankID
				AND RecHubData.dimClientAccountsView.SiteClientAccountID = @siteClientAccountID
				AND RecHubData.dimClientAccountsView.IsActive = 1;
		END
	END
	ELSE
	BEGIN
		-- Add the EntityBreadcrumb data to a temp table
		-- So we can Join to it for RAAM data
		IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
			DROP TABLE #tmpEntities;

		 CREATE TABLE #tmpEntities
		 (
			   EntityID              INT,
			   BreadCrumb	         VARCHAR(100)
		 );
     
		 INSERT INTO #tmpEntities
		 (
			   EntityID,
			   BreadCrumb       
		 )
		 SELECT     
			entityRequest.att.value('@EntityID', 'int') AS EntityID,
			entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
		 FROM 
			@parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

		--for all entities, get the workgroups
		INSERT INTO @tblWorkgroups(SiteBankID, SiteClientAccountID)
		SELECT DISTINCT RecHubData.dimClientAccountsView.SiteBankID,
						RecHubData.dimClientAccountsView.SiteClientAccountID
		FROM #tmpEntities
			JOIN RecHubUser.OLWorkgroups
				ON #tmpEntities.EntityID = RecHubUser.OLWorkgroups.EntityID
			JOIN RecHubData.dimClientAccountsView
				ON RecHubData.dimClientAccountsView.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
					AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
					AND RecHubData.dimClientAccountsView.IsActive = 1
			JOIN RecHubUser.SessionClientAccountEntitlements
				ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
					AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
					AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
					AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID;
	END
	

	;WITH ExceptionRecords AS
	(
		SELECT 
			ISNULL(tblPaymentTypes.LongName, tblPaymentTypes2.LongName) AS BatchPaymentTypeName,
			CASE WHEN RecHubException.DecisioningTransactions.TransactionStatusKey in (2, 3)
					AND RecHubException.IntegraPAYExceptions.Deadline IS NOT NULL
					AND CONVERT(VARCHAR(8), RecHubException.IntegraPAYExceptions.Deadline, 108) < @CurrentTime
				THEN 0 -- deadline has passed, so it should go back to unresolved
				WHEN RecHubException.DecisioningTransactions.TransactionStatusKey = 1
				THEN 0 -- status is still Pending (1) so it is unresolved
				ELSE 1 -- status is not Pending and deadline hasn't passed to it is resolved
				END AS Resolved,
			ISNULL(RecHubException.IntegraPAYExceptions.CheckAmount, (SELECT SUM(Amount) FROM RecHubException.Payments WHERE TransactionKey = RecHubException.Transactions.TransactionKey)) AS CheckAmount
		FROM RecHubException.DecisioningTransactions
			LEFT JOIN RecHubException.IntegraPAYExceptions
				ON RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
				  -- This USP assumes the current date for CommonExceptions queries.
				AND RecHubException.IntegraPAYExceptions.ImmutableDateKey = @filterDateKey
			LEFT JOIN @tblWorkgroups AS tblClientAccounts
				ON RecHubException.IntegraPAYExceptions.SiteClientAccountID = tblClientAccounts.SiteClientAccountID	
				AND RecHubException.IntegraPAYExceptions.SiteBankID = tblClientAccounts.SiteBankID
			LEFT JOIN RecHubData.dimBatchPaymentTypes AS tblPaymentTypes
				ON RecHubException.IntegraPAYExceptions.BatchPaymentTypeKey = tblPaymentTypes.BatchPaymentTypeKey

			LEFT JOIN RecHubException.Transactions
				ON RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
			LEFT JOIN RecHubException.ExceptionBatches
				ON RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
				  -- This USP assumes the current date for CommonExceptions queries.
				AND RecHubException.ExceptionBatches.ImmutableDateKey = @filterDateKey
			LEFT JOIN @tblWorkgroups AS tblClientAccounts2
				ON RecHubException.ExceptionBatches.SiteWorkgroupID = tblClientAccounts2.SiteClientAccountID
				AND RecHubException.ExceptionBatches.SiteBankID = tblClientAccounts2.SiteBankID
			LEFT JOIN RecHubData.dimBatchPaymentTypes AS tblPaymentTypes2
				ON RecHubException.ExceptionBatches.BatchPaymentTypeKey = tblPaymentTypes2.BatchPaymentTypeKey
		WHERE 
			tblClientAccounts.SiteClientAccountID IS NOT NULL OR tblClientAccounts2.SiteClientAccountID IS NOT NULL
	)
	SELECT	
		BatchPaymentTypeName,
		Resolved,
		COUNT(*) AS ExceptionCount,
		SUM(CheckAmount) TotalAmount
		FROM 
			ExceptionRecords
		GROUP BY	
			BatchPaymentTypeName,
			Resolved
		ORDER BY	
			BatchPaymentTypeName ASC,
			Resolved DESC;

	DECLARE @auditMessage VARCHAR(1024) = 'Receivables Exceptions By Type executed for Parameters -'
											+ ' Date: ' + CAST(@filterDateKey AS VARCHAR(8))
											+ ' Entity ID: ' + ISNULL(CAST(@rootEntityID as VARCHAR(36)), 'NULL')
											+ ' Site Bank ID: ' + CASE WHEN @siteBankID IS NULL THEN 'All' ELSE CAST(@siteBankID as VARCHAR(10)) END
											+ ' Site Client Account ID: ' + CASE WHEN @siteClientAccountID IS NULL THEN 'All' ELSE CAST(@siteClientAccountID as VARCHAR(10)) END;

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubException.usp_Report_ReceivablesExceptionsByType', @parmEventName='Report Executed', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage;

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH