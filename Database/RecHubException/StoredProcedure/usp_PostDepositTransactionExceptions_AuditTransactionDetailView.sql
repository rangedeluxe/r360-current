--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_AuditTransactionDetailView
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView
( 
	@parmBatchID		BIGINT,
	@parmDepositDateKey INT,
	@parmTransactionID	INT,
	@parmSID			UNIQUEIDENTIFIER,
	@parmUserName		VARCHAR(128)
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 12/05/2017
*
* Purpose: Create audit record when user views page or PDE transaction detail page. 
*
* Modification History
* 2/05/2017 PT 147283509	JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
	@UserID INT,
	@AuditMessage VARCHAR(MAX),
	@BaseAuditMessage VARCHAR(MAX);

BEGIN TRY	

	/* Get the UserID from the RecHubUser.Users table via the SID */
	DECLARE @UserIDs TABLE (UserID INT);

	INSERT INTO @UserIDs
	EXEC RecHubUser.usp_Users_UserID_Get_BySID @parmSID = @parmSID;

	SELECT TOP(1) 
		@UserID = UserID
	FROM 
		@UserIDs;

	SET @AuditMessage = '';

	SELECT 
		@BaseAuditMessage = ' for Batch: ' + CAST(SourceBatchID AS VARCHAR(10)) 
		+ ', Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
		+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
		+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
		+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
		+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
		+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
		+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
	FROM
		RecHubData.factTransactionSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
		INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
	WHERE
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND TransactionID = @parmTransactionID
		AND IsDeleted = 0;

	SET	@AuditMessage = 'Post-Deposit Exceptions Transaction Details page was viewed by ' + @parmUserName + @BaseAuditMessage

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
		@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView',
		@parmEventName			= 'Viewed Page',
		@parmEventType			= 'Page View',
		@parmUserID				= @UserID,
		@parmAuditMessage		= @AuditMessage;

	SET	@AuditMessage = @parmUserName + ' viewed the Post-Deposit Exceptions Transaction Details' + @BaseAuditMessage

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
		@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_AuditTransactionDetailView',
		@parmEventName			= 'Post-Deposit Exceptions',
		@parmEventType			= 'Post-Deposit Exceptions',
		@parmUserID				= @UserID,
		@parmAuditMessage		= @AuditMessage;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

