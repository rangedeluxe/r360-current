--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Ins') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Ins
(
	@parmBatchID			BIGINT,
	@parmDepositDateKey		INT,
	@parmTransactionID		INT,
	@parmSID				UNIQUEIDENTIFIER,
	@parmUserName			VARCHAR(128),
	@parmErrorCode			INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 07/12/2017
*
* Purpose: Inserts a new PostDepositTransactionExceptions record when a user locks it
*
* Modification History
* 07/12/2017 PT 144805301 MGE	Created
* 07/17/2017 PT 144805301 MGE	Changed the parameters to use natural key
* 08/04/2017 PT 149496367 JPB	Insert record into batch exceptions table
* 12/11/2017 PT 147284587 JPB	User Activity Audit lock
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't change without being audited...
DECLARE @LocalTransaction BIT = 0,
		@LockedByUserID INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
		SET @parmErrorCode = 0;
	END

	DECLARE @errorDescription VARCHAR(1024),
			@AuditMessage VARCHAR(MAX),
			@TransactionKey	BIGINT;
	DECLARE @curTime DATETIME = GETDATE();

	DECLARE @UserIDs TABLE (UserID INT);

	INSERT INTO @UserIDs
	EXEC RecHubUser.usp_Users_UserID_Get_BySID @parmSID = @parmSID;

	SELECT TOP(1) 
		@LockedByUserID = UserID
	FROM 
		@UserIDs;

	-- Verify User ID
	IF NOT EXISTS(SELECT 1 FROM RecHubUser.Users WHERE UserID = @LockedByUserID)
	BEGIN
		SET @errorDescription = 'Unable to insert PostDepositTransactionExceptions record, user (' + ISNULL(CAST(@LockedByUserID AS VARCHAR(10)), 'NULL') + ') does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END

	-- Verify the Transaction isn't already locked
	IF EXISTS(SELECT 1 FROM RecHubException.PostDepositTransactionExceptions
		WHERE BatchID = @parmBatchID AND DepositDateKey = @parmDepositDateKey AND TransactionID = @parmTransactionID)
	BEGIN
		SET @parmErrorCode = 1; -- 1 = Duplicate
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
	ELSE
		BEGIN
		
		-- Insert the PostDepositTransactionExceptions record
		INSERT INTO RecHubException.PostDepositTransactionExceptions(BatchID, DepositDateKey, TransactionID, LockedDate, LockedByUserID)
		VALUES (@parmBatchID, @parmDepositDateKey, @parmTransactionID, GETDATE(), @LockedByUserID);

		SET @TransactionKey = SCOPE_IDENTITY();

		-- Audit the RecHubException.PostDepositTransactionExceptions insert
		SET @auditMessage = 'Added PostDepositTransactionExceptions Lock for Batch: ' + CAST(@parmBatchID AS VARCHAR(10))
			+ ': DepositDateKey = ' + CAST(@parmDepositDateKey AS VARCHAR(8)) 
			+ ': TransactionID = ' + CAST(@parmTransactionID AS VARCHAR(10))
			+ ': UserID = ' + CAST(@LockedByUserID AS VARCHAR(10))
			+ '.';

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @LockedByUserID,
				@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Ins',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'PostDepositTransactionExceptions',
				@parmColumnName			= 'TransactionKey',
				@parmAuditValue			= @TransactionKey,
				@parmAuditType			= 'INS',
				@parmAuditMessage		= @auditMessage;

		SET @AuditMessage =  @parmUserName + ' locked Post-Deposit Exceptions ';

		SELECT 
			@AuditMessage += 'Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
			+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
			+ ' in Batch: ' + CAST(SourceBatchID AS VARCHAR(10)) 
			+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
			+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
			+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
			+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
			+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
		FROM
			RecHubData.factTransactionSummary
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
			INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND TransactionID = @parmTransactionID
			AND IsDeleted = 0;

		EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Ins',
			@parmEventName			= 'Post-Deposit Exceptions',
			@parmEventType			= 'Post-Deposit Exceptions',
			@parmUserID				= @LockedByUserID,
			@parmAuditMessage		= @AuditMessage;

		BEGIN TRY
			--Multiple users could be inserting records at the same time. Rather than doing a check and possibly missing
			--the fact that the record already exists, just insert it and throw away any duplicate errors.
			INSERT INTO RecHubException.PostDepositBatchExceptions(DepositDateKey,BatchID,CreationDate)
			VALUES(@parmDepositDateKey,@parmBatchID,GETDATE());
		END TRY
		BEGIN CATCH
			--If the error is not a duplicate record, throw an error, i.e. do not throw an error if it is a duplicate record
			IF( @@ERROR <> 2601)
			BEGIN
				DECLARE @ErrorMessage    NVARCHAR(4000),
						@ErrorSeverity   INT,
						@ErrorState      INT;
				SELECT	@ErrorMessage = ERROR_MESSAGE(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE();
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			END
		END CATCH
		-- All updates are complete
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH