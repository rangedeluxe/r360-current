--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_del_RelatedItem
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_del_RelatedItem') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_del_RelatedItem
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_del_RelatedItem
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmBatchSequence INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: BLR
* Date: 09/20/2017
*
* Purpose: To Undo everything usp_UpSert_RelatedItems does. Business
*          case is to delete related items from PDE.
*
* Modification History
* 09/20/2017 PT 147282137 BLR Created
******************************************************************************/
DEClARE @LocalTran BIT,
		@ModificationDate DATETIME = GETDATE();
BEGIN TRY

	BEGIN TRAN
	SET @LocalTran = 1;

	/* Delete from RecHubData.factDataEntryDetails */
	EXEC RecHubException.usp_factDataEntryDetails_del
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmBatchSequence = @parmBatchSequence;

	/* Delete from RecHubData.factStubs */
	EXEC RecHubException.usp_factStubs_del
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmBatchSequence = @parmBatchSequence;

	/* Delete the PostDepositDataEntryDetails fields.*/
	EXEC RecHubException.usp_PostDepositDataEntryDetails_del
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmBatchSequence = @parmBatchSequence;

	COMMIT;

END TRY
BEGIN CATCH
	IF( @LocalTran = 1 )
		ROLLBACK TRAN;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH