--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_IntegraPAYExceptions_Upd_BatchComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_IntegraPAYExceptions_Upd_BatchComplete') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_IntegraPAYExceptions_Upd_BatchComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_IntegraPAYExceptions_Upd_BatchComplete 
(
	@parmGlobalBatchID						INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: Updates the DecisioningStatus column of all transactions in a batch to the next state (i.e. "Resolved")
*			Called by the application when checking in a transaction reports that
*			there are no more unresolved transactions in the batch.
*
* Modification History
* 07/09/2013 WI 108950 RDS	Created
* 07/18/2013 WI 109488 RDS	Added Audit Record (commented out)
* 04/25/2014 WI 139285 RDS	Updates for IntegraPAYExceptions table name change
*		Note: Stored procedure has been renamed from usp_CommonExceptions_Upd_BatchComplete to usp_IntegraPAYExceptions_Upd_BatchComplete
* 10/10/2014 WI 109488 KLC	Updated auditing text (still commented out)
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Record Audit Trail
	/* TODO: Include this audit event if we can apply an appropriate "service" user ID...
	DECLARE @auditMessage VARCHAR(1024) = 'Exception GlobalBatchID: ' + CAST(@parmGlobalBatchID as VARCHAR) + ' - Complete'
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins 
			@parmApplicationName='RecHubException.usp_IntegraPAYExceptions_Upd_BatchComplete', 
			@parmEventName='E Batch Complete', 
			@parmUserID = 0, -- This action is performed by the service automatically; we are just recording the time of the occurrence, not who did it
			@parmAuditMessage = @auditMessage
	*/

	-- Update all transactions in the given batch to a status of "Complete" / "Resolved"
	UPDATE RecHubException.IntegraPAYExceptions
		SET DecisioningStatus = 3
	WHERE GlobalBatchID = @parmGlobalBatchID;
	
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

