--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Payments_Get_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Payments_Get_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_Payments_Get_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Payments_Get_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/29/2014
*
* Purpose: Return Payment Records for Post-Process Exceptions
*
* Modification History
* 04/29/2014 WI 139288 RDS	Created
* 05/14/2014 WI 141676 RDS	Add auditing
* 05/21/2014 WI 139288 KLC	Updated to sort fields by screen order
* 09/10/2014 WI	139288 KLC	Added serial, tran code, and remitter name fields
* 12/12/2014 WI 139288 KLC	Split off a seperate procedure to read data entry fields
*							 and update this one to simply return the standard
*							 payment fields.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Note: This "view" was audited already by usp_Transactions_Get_ByCommonExceptionID

	SELECT
		RecHubException.Payments.BatchSequence,
		RecHubException.Payments.Amount,
		RecHubException.Payments.RT,
		RecHubException.Payments.Account,
		RecHubException.Payments.TransactionCode,
		RecHubException.Payments.Serial,
		RecHubException.Payments.RemitterName
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.Payments ON
		RecHubException.Payments.TransactionKey = RecHubException.Transactions.TransactionKey
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID
	ORDER BY RecHubException.Payments.BatchSequence;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH