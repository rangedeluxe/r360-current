--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PaymentDataEntry_Get_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PaymentDataEntry_Get_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_PaymentDataEntry_Get_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PaymentDataEntry_Get_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 12/12/2014
*
* Purpose: Return PaymentDataEntry Records for Post-Process Exceptions
*
* Modification History
* 12/12/2014 WI 181772 KLC	Created (split off from usp_Payments_Get_ByCommonExceptionID)
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Note: This "view" was audited already by usp_Transactions_Get_ByCommonExceptionID

	DECLARE @siteBankID INT,
			@siteWorkgroupID INT,
			@transactionKey BIGINT;
	SELECT	@siteBankID = RecHubException.ExceptionBatches.SiteBankID,
			@siteWorkgroupID = RecHubException.ExceptionBatches.SiteWorkgroupID,
			@transactionKey = RecHubException.DecisioningTransactions.TransactionKey
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

	WITH CTE_AllPaymentFields AS
	(
		SELECT	ROW_NUMBER() OVER ( PARTITION BY RecHubData.dimDataEntryColumns.FldName, RecHubData.dimDataEntryColumns.TableType
									ORDER BY RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey DESC ) AS RowNumber,
				RecHubData.dimDataEntryColumns.DataEntryColumnKey,
				RecHubData.dimDataEntryColumns.FldName,
				RecHubData.dimDataEntryColumns.TableType,
				RecHubData.dimDataEntryColumns.ScreenOrder
		FROM RecHubData.dimClientAccounts
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
			INNER JOIN RecHubData.dimDataEntryColumns
				ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
		WHERE RecHubData.dimClientAccounts.SiteBankID = @siteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @siteWorkgroupID
			AND RecHubData.dimDataEntryColumns.TableType = 1 -- Payments Data Entry
	),
	CTE_CurrentPaymentFields AS
	(
		SELECT DataEntryColumnKey, FldName, TableType, ScreenOrder
		FROM CTE_AllPaymentFields
		WHERE RowNumber = 1
	)
	SELECT RecHubException.Payments.BatchSequence,
			CTE_CurrentPaymentFields.DataEntryColumnKey,
			CTE_CurrentPaymentFields.ScreenOrder,
			RecHubException.PaymentDataEntry.Value AS FieldValue
	FROM RecHubException.Payments
		INNER JOIN RecHubException.PaymentDataEntry ON
			RecHubException.PaymentDataEntry.PaymentKey = RecHubException.Payments.PaymentKey
		INNER JOIN CTE_CurrentPaymentFields ON
				CTE_CurrentPaymentFields.FldName = RecHubException.PaymentDataEntry.FldName
			AND CTE_CurrentPaymentFields.TableType = 1 -- Payment Data Entry
	WHERE RecHubException.Payments.TransactionKey = @transactionKey		
	ORDER BY BatchSequence, ScreenOrder;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH