--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Payments_InsUpd_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Payments_InsUpd_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_Payments_InsUpd_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Payments_InsUpd_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT,
	@parmXmlUpdates			XML		-- <Updates><I Seq="1"><F ID="1234" Val="Something" /><F ... /></I><I ... /></Updates>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/29/2014
*
* Purpose: Update Payment Records with Post-Process Exception keyed values
*
* Modification History
* 04/29/2014 WI 139344 RDS	Created
* 05/14/2014 WI 141677 RDS	Add auditing
* 07/30/2014 WI 156358 RDS	Add auditing
* 09/11/2014 WI 139344 KLC	Updated to support serial, tran code and remitter name
*							 updates
* 10/10/2014 WI 141677 KLC	Updated auditing text
* 10/22/2014 WI 139344 KLC	Fixed bug with standard fields getting inserted
*							 as DE records
* 11/11/2014 WI 141677 KLC	Updated to handle specific date formats
* 12/12/2014 WI 139344 KLC	Changed to never update standard fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	
	-- Store updates to a temporary table
	DECLARE @tblUpdates TABLE (BatchSequence INT, DataEntryColumnKey BIGINT, FldName VARCHAR(128), FieldVal SQL_VARIANT, IsItemAmount BIT, IsItemRT BIT, IsItemAccount BIT, IsItemTranCode BIT, IsItemSerial BIT, IsItemRemitter BIT);
	INSERT INTO @tblUpdates (BatchSequence, DataEntryColumnKey, FieldVal)
		SELECT 
			X.c.value('../@Seq', 'int') PaymentSeq, 
			X.c.value('@ID', 'bigint') DataEntryColumnKey,
			COALESCE(X.c.value('@DateVal', 'varchar(255)'), X.c.value('@Val', 'varchar(255)')) FieldVal
		FROM @parmXmlUpdates.nodes('/Updates/I/F') AS X(c);

	-- Apply the correct data types and FldNames
	UPDATE tblUpdates
		SET FldName = RecHubData.dimDataEntryColumns.FldName,
			FieldVal = CASE WHEN FieldVal = '' THEN FieldVal
						ELSE
						CASE RecHubData.dimDataEntryColumns.DataType
							WHEN 11 THEN CAST(CONVERT(DATE, FieldVal, 112) AS SQL_VARIANT)
							WHEN 7 THEN CAST(FieldVal AS MONEY)
							WHEN 6 THEN CAST(FieldVAL AS FLOAT)
							ELSE SUBSTRING(CAST(FieldVal AS VARCHAR(255)), 1, RecHubData.dimDataEntryColumns.FldLength) END /*Using FldLength for the substring may cause problems here as ImageRPS always sends 0 for the FldLength*/
						END
	FROM @tblUpdates tblUpdates
	INNER JOIN RecHubData.dimDataEntryColumns ON
		tblUpdates.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
	WHERE RecHubData.dimDataEntryColumns.TableType <> 0;

	-- Construct audit message about data changes
	DECLARE @auditMessage2 VARCHAR(max) = '';
	SELECT @auditMessage2 = @auditMessage2 + ';  [Seq=' + cast(BatchSequence as varchar(20)) + '] ' + FldName + ': ' + cast(FieldVal as varchar(1000))
	FROM @tblUpdates;

	-- Update existing dynamic fields
	UPDATE RecHubException.PaymentDataEntry
		SET Value = tblUpdates.FieldVal
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.Payments ON
		RecHubException.Payments.TransactionKey = RecHubException.Transactions.TransactionKey
	INNER JOIN RecHubException.PaymentDataEntry ON
		RecHubException.PaymentDataEntry.PaymentKey = RecHubException.Payments.PaymentKey
	INNER JOIN @tblUpdates tblUpdates ON
		tblUpdates.FldName = RecHubException.PaymentDataEntry.FldName
		AND tblUpdates.BatchSequence = RecHubException.Payments.BatchSequence 
		AND tblUpdates.IsItemAmount = 0 AND tblUpdates.IsItemRT = 0 AND tblUpdates.IsItemAccount = 0 AND tblUpdates.IsItemTranCode = 0 AND tblUpdates.IsItemSerial = 0 AND tblUpdates.IsItemRemitter = 0
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

	-- Insert any new fields
	INSERT INTO RecHubException.PaymentDataEntry(PaymentKey, FldName, Value)
	SELECT RecHubException.Payments.PaymentKey, tblUpdates.FldName, tblUpdates.FieldVal
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.Payments ON
		RecHubException.Payments.TransactionKey = RecHubException.Transactions.TransactionKey
	INNER JOIN @tblUpdates tblUpdates ON
		tblUpdates.BatchSequence = RecHubException.Payments.BatchSequence 
		AND tblUpdates.IsItemAmount = 0 AND tblUpdates.IsItemRT = 0 AND tblUpdates.IsItemAccount = 0 AND tblUpdates.IsItemTranCode = 0 AND tblUpdates.IsItemSerial = 0 AND tblUpdates.IsItemRemitter = 0
	LEFT JOIN RecHubException.PaymentDataEntry ON
		RecHubException.PaymentDataEntry.PaymentKey = RecHubException.Payments.PaymentKey
		AND tblUpdates.FldName = RecHubException.PaymentDataEntry.FldName
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID
		AND RecHubException.PaymentDataEntry.PaymentDataEntryKey IS NULL;

	-- Construct the audit message
	DECLARE @auditMessage VARCHAR(MAX);
	SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
			+ ISNULL(', DepositDate: ' + CAST(RecHubException.ExceptionBatches.DepositDateKey as VARCHAR), '')
			+ ISNULL(', BatchID: ' + CAST(RecHubException.ExceptionBatches.BatchID as VARCHAR), '')
			+ ISNULL(', TransactionID: ' + CAST(RecHubException.Transactions.TransactionID as VARCHAR), '')
			+ ' - ' + CAST((SELECT COUNT(DISTINCT BatchSequence) FROM @tblUpdates) as VARCHAR) + ' payments(s) updated'
			+ @auditMessage2
	FROM RecHubException.DecisioningTransactions
	LEFT JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	LEFT JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;
				
	-- Audit this event
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName = 'RecHubException.usp_Payments_InsUpd_ByCommonExceptionID',
			@parmEventName = 'E Update Item',
			@parmEventType = 'Exceptions',
			@parmUserID = @parmUserID,
			@parmAuditMessage = @auditMessage

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH