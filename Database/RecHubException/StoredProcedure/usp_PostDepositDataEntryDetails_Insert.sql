--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositDataEntryDetails_Insert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_PostDepositDataEntryDetails_Insert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_PostDepositDataEntryDetails_Insert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositDataEntryDetails_Insert
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmSequenceWithinTransaction INT,
	@parmBatchSequence INT,
	@parmStubSequence INT,
	@parmModificationDate DATETIME,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 10/03/2017
*
* Purpose: Update an existing factDataEntryDatail records from 
*		PostDepositExceptions
*
* Modification History
* 10/09/2017 PT 147282969	MGE	Created
********************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	;WITH NewDataEntryField_CTE AS
	(	
		SELECT 
			FieldName,
			ABS(BatchSequence)+@parmBatchSequence AS BatchSequence,
			ABS(BatchSequence)+@parmStubSequence AS StubSequence,
			ABS(BatchSequence)+@parmBatchSequence AS InputBatchSequence,
			IsCheck
		FROM 
			@parmDataEntryValues 
	)
	
	INSERT INTO RecHubException.PostDepositDataEntryDetails
	(	
		DepositDateKey,
		BatchID,
		TransactionID,
		BatchSequence,
		InputBatchSequence,
		StubSequence,
		IsCheck,
		IsUIAddedField,
		FieldName
	)
	SELECT
		@parmDepositDateKey,
		@parmBatchID,
		@parmTransactionID,
		BatchSequence,
		InputBatchSequence,
		StubSequence,
		IsCheck,
		1 as ISUIAddedField,
		FieldName
	FROM 
		NewDataEntryField_CTE
			
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
