--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ReceivablesExceptions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Report_ReceivablesExceptions') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_Report_ReceivablesExceptions;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Report_ReceivablesExceptions
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     05/06/2013
*
* Purpose:  Gets the data needed for the Receivables Exceptions Report
*
* Modification History
* Created
* 05/24/2013 WI 107243 KLC	Created.
* 07/10/2013 WI	107243 KLC	Updated auditing to write individual parameters instead of the xml.
* 07/11/2013 WI 108622 KLC	Updated to make filter date optional and default to current date.
* 07/26/2013 WI 107243 JBS  Changing called proc from usp_OLClientAccounts_Get_ByUserID
*							to usp_OLClientAccounts_Get_ByUserID_Reporting
* 07/26/2013 WI 107243 KLC	Updated to return payment source's long name
* 09/09/2013 WI 107243 KLC	Updated to only return items with a status of pending or unresolved
* 09/11/2013 WI 107243 KLC	Updated to handle status as Open or Pending, not individual ones
* 09/13/2013 WI 107243 KLC	Updated to remove join to RecHubData.dimClientAccounts as unneeded and it was creating duplicate records
* 10/31/2013 WI 119758 CMC	Updated to use new Org Hierarchy Tree
* 11/13/2013 WI 122363 CMC  Select the distinct client account exceptions
* 11/25/2013 WI 123255 CMC  Restrict by deadline
* 07/11/2014 WI 151284 KLC	Updated to take session id and validate entitlements
*							 now that we have RAAM integration
* 07/30/2014 WI 151284 KLC	Updated to only return data for active workgroups
* 01/15/2014 WI 172328 LA   Added Entity BreadCrumb for Entity Name
* 11/03/2014 WI	151284 KLC	Updated to match Exceptions Summary Screen
* 11/21/2014 WI 151284 KLC	Updated obey display batch id and fix some sorting issues
* 01/05/2014 WI 183023 LA   Modified to show only Active Workgroups 
*******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @filterDateKey	INT,
			@groupBy		VARCHAR(20),
			@sortBy			VARCHAR(20),
			@sortDir		VARCHAR(10),
			@deadlineBuffer INT,
			@currentTime	VARCHAR(8) = CONVERT(VARCHAR(8), GETDATE(), 108),
			@currentDate DATETIME = CONVERT(DATE, GETDATE());
			
	SELECT	@filterDateKey = CASE WHEN Parms.val.value('(FD)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(FD)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@groupBy = COALESCE(LOWER(Parms.val.value('(GB)[1]', 'VARCHAR(20)')), ''),
			@sortBy = COALESCE(LOWER(Parms.val.value('(SB)[1]', 'VARCHAR(20)')), ''),
			@sortDir = COALESCE(LOWER(Parms.val.value('(SO)[1]', 'VARCHAR(10)')), '')
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);
	
	-- Add the EntityBreadcrumb data to a temp table
	-- So we can Join to it for RAAM data
	IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
        DROP TABLE #tmpEntities;

     CREATE TABLE #tmpEntities
     (
           EntityID              INT,
           BreadCrumb	         VARCHAR(100)
     );
     
     INSERT INTO #tmpEntities
     (
           EntityID,
           BreadCrumb       
     )

     SELECT     
           entityRequest.att.value('@EntityID', 'int') AS EntityID,
           entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
     FROM 
          @parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

	/* retrieve the deadline buffer minutes to apply */
	SELECT
		@deadlineBuffer = CASE WHEN ISNUMERIC(DefaultSetting) > 0 THEN DefaultSetting ELSE 0 END
	FROM 
		RecHubUser.OLPreferences
	WHERE
		PreferenceName = 'exceptiondeadlinebufferminutes'
	
	IF @deadlineBuffer IS NULL
		SET @deadlineBuffer = 0
	
	/*If no filter date was provided, then we need to default to the current date for filtering.  That is typically what the report will be run for
		except for development testing where providing a date may be useful*/
	IF @filterDateKey IS NULL
	BEGIN
		SET @filterDateKey = CONVERT(VARCHAR(8), GETDATE(), 112);
	END
	
	;WITH DistinctClientAccounts AS
	(
		SELECT DISTINCT
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			RecHubUser.OLWorkgroups.EntityID, 
			RecHubUser.SessionClientAccountEntitlements.EntityName,
			#tmpEntities.BreadCrumb AS EntityBreadCrumb,
			CAST(COALESCE(RecHubUser.OLWorkgroups.DisplayBatchID, RecHubUser.OLEntities.DisplayBatchID, 0) AS BIT) AS DisplayBatchID
		FROM
			RecHubUser.SessionClientAccountEntitlements
		INNER JOIN RecHubUser.OLWorkgroups ON 
			RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
			AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
		INNER JOIN RecHubUser.OLEntities ON
			RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		INNER JOIN #tmpEntities ON
			#tmpEntities.EntityID = RecHubUser.OLWorkgroups.EntityID 
		WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	),
	CTE_IntegraPAYExceptions AS
	(
		SELECT
			RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
			RecHubException.IntegraPAYExceptions.SiteBankID,
			RecHubException.IntegraPAYExceptions.SiteClientAccountID,
			RecHubException.IntegraPAYExceptions.DepositDateKey,
			'' AS BatchID,		-- NO R360 BatchID exists for Integrapay Exceptions
			RecHubException.IntegraPAYExceptions.BatchID AS SourceBatchID,
			RecHubException.IntegraPAYExceptions.ImmutableDateKey,
			RecHubException.IntegraPAYExceptions.CheckAmount,
			DATEADD(minute, - @deadlineBuffer, RecHubException.IntegraPAYExceptions.Deadline) AS Deadline,
			RecHubException.IntegraPAYExceptions.InProcessException,
			CASE WHEN TransactionStatusKey = 1 THEN '0' ELSE '1' END AS DecisionPending,
			RecHubException.IntegraPAYExceptions.DecisioningStatus,
			RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
			RecHubData.dimBatchSources.LongName AS BatchSourceName,
			COALESCE(RecHubException.DecisioningTransactions.UserID,0) AS Locked,
			CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
			COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
			RecHubData.dimClientAccountsView.DisplayLabel,
			RecHubData.dimClientAccountsView.LongName,
			RecHubException.IntegraPAYExceptions.TransactionID,
			tblClientAccounts.EntityID,
			tblClientAccounts.EntityBreadCrumb AS EntityName,
			RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
			tblClientAccounts.DisplayBatchID
		FROM
			RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.IntegraPAYExceptions ON
			RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
		INNER JOIN RecHubData.dimBatchSources ON
			RecHubException.IntegraPAYExceptions.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubException.IntegraPAYExceptions.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN DistinctClientAccounts AS tblClientAccounts  ON
			tblClientAccounts.SiteBankID = RecHubException.IntegraPAYExceptions.SiteBankID
			AND tblClientAccounts.SiteClientAccountID = RecHubException.IntegraPAYExceptions.SiteClientAccountID
		INNER JOIN RecHubData.dimClientAccountsView	ON
			RecHubData.dimClientAccountsView.SiteBankID = RecHubException.IntegraPAYExceptions.SiteBankID
			AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubException.IntegraPAYExceptions.SiteClientAccountID
			AND RecHubData.dimClientAccountsView.IsActive = 1
		INNER JOIN RecHubData.dimSiteCodes
			ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		LEFT OUTER JOIN RecHubUser.Users ON
			RecHubException.DecisioningTransactions.UserID = RecHubUser.Users.UserID
		WHERE 
			RecHubException.IntegraPAYExceptions.DecisioningStatus < 3
		AND 
			RecHubException.IntegraPAYExceptions.ImmutableDateKey = @filterDateKey
		AND
			CONVERT(VARCHAR(8), Deadline, 108) >= @currentTime
		AND
			CAST(CONVERT(VARCHAR(8), RecHubData.dimSiteCodes.CurrentProcessingDate, 112) AS INT) >= RecHubException.IntegraPAYExceptions.DepositDateKey
	),
	CTE_PostProcessExceptions AS
	(
		SELECT
			RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
			RecHubException.ExceptionBatches.SiteBankID,
			RecHubException.ExceptionBatches.SiteWorkGroupID AS SiteClientAccountID,
			RecHubException.ExceptionBatches.DepositDateKey,
			RecHubException.ExceptionBatches.BatchID,
			RecHubException.ExceptionBatches.SourceBatchID,
			RecHubException.ExceptionBatches.ImmutableDateKey,
			(SELECT SUM(Amount) FROM RecHubException.Payments WHERE TransactionKey = T.TransactionKey)  AS CheckAmount,
			CASE WHEN RecHubException.DataEntrySetups.DeadLineDay IS NULL THEN NULL
				ELSE DATEADD(mi, RecHubException.DataEntrySetups.DeadLineDay%100, DATEADD(hh, RecHubException.DataEntrySetups.DeadLineDay/100, @currentDate)) END AS Deadline,
			CAST(0 AS BIT) AS InProcessException,	-- These tables by definition contain Post-Process exceptions
			CASE WHEN TransactionStatusKey = 1 THEN '0' ELSE '1' END AS DecisionPending,
			(RecHubException.DecisioningTransactions.TransactionStatusKey - 1) AS DecisioningStatus,	-- Note: TransactionStatus values are one greater than CEDecisioning status values...  So we subtract one...
			RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
			RecHubData.dimBatchSources.LongName AS BatchSourceName,
			COALESCE(RecHubException.DecisioningTransactions.UserID,0) AS Locked,
			CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
			COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
			RecHubData.dimClientAccountsView.DisplayLabel,
			RecHubData.dimClientAccountsView.LongName,
			T.TransactionID,
			tblClientAccounts.EntityID,
			tblClientAccounts.EntityBreadCrumb AS EntityName,
			RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
			tblClientAccounts.DisplayBatchID
		FROM
			RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.Transactions T ON
			RecHubException.DecisioningTransactions.TransactionKey = T.TransactionKey
		INNER JOIN RecHubException.ExceptionBatches ON
			T.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
		INNER JOIN RecHubData.dimBatchSources ON
			RecHubException.ExceptionBatches.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubException.ExceptionBatches.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN DistinctClientAccounts AS tblClientAccounts  ON
			tblClientAccounts.SiteBankID = RecHubException.ExceptionBatches.SiteBankID
			AND tblClientAccounts.SiteClientAccountID = RecHubException.ExceptionBatches.SiteWorkGroupID
		INNER JOIN RecHubData.dimClientAccountsView	ON
			RecHubData.dimClientAccountsView.SiteBankID = RecHubException.ExceptionBatches.SiteBankID
			AND RecHubData.dimClientAccountsView.SiteClientAccountID = 	RecHubException.ExceptionBatches.SiteWorkGroupID
			AND RecHubData.dimClientAccountsView.IsActive = 1
		INNER JOIN RecHubData.dimSiteCodes
			ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		INNER JOIN RecHubException.DataEntrySetups ON 
			RecHubException.ExceptionBatches.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
			AND RecHubException.ExceptionBatches.SiteWorkgroupID = RecHubException.DataEntrySetups.SiteWorkgroupID
		LEFT OUTER JOIN RecHubUser.Users ON
			RecHubException.DecisioningTransactions.UserID = RecHubUser.Users.UserID
		WHERE 
			RecHubException.ExceptionBatches.BatchStatusKey < 3 -- Resolved, sent to warehouse
			-- TODO: Verify there is no condition on date.  I believe that there is no true deadline on when posting exceptions can be processed, so we should return them until the underlying warehouse algorithms purge the records... 
		AND
			CAST(CONVERT(VARCHAR(8), RecHubData.dimSiteCodes.CurrentProcessingDate, 112) AS INT) >= RecHubException.ExceptionBatches.DepositDateKey
	),
	CTE_AllExceptions AS
	(
		SELECT * from CTE_IntegraPAYExceptions
		union all
		SELECT * from CTE_PostProcessExceptions
	)
	SELECT 	CommonExceptionID,
			InProcessException,
			RecHubData.dimDates.CalendarDate AS DepositDate,
			DecisionPending,
			SourceBatchID,
			CheckAmount,
			Deadline,
			BatchPaymentTypeName,
			BatchSourceName,
			DisplayLabel AS LongName,
			TransactionID,
			EntityName,
			DisplayBatchID
	FROM 
		CTE_AllExceptions
		JOIN RecHubData.dimDates
			on CTE_AllExceptions.DepositDateKey = RecHubData.dimDates.DateKey

	ORDER BY	InProcessException DESC,
				CASE WHEN @groupBy <> @sortBy --only perform sorting on the group if the group is not already our sorting criteria also
					THEN CASE @groupBy	WHEN 'status'			THEN DecisionPending
										WHEN 'batchsource'		THEN BatchSourceName
										WHEN 'paymenttype'		THEN BatchPaymentTypeName
										END
					END ASC,
				CASE WHEN @sortDir = 'ascending'
					THEN CASE @sortBy	WHEN 'status'				THEN DecisionPending
										WHEN 'batchsource'			THEN BatchSourceName
										WHEN 'paymenttype'			THEN BatchPaymentTypeName
										WHEN 'checkamount'			THEN REPLICATE('0', 18 - LEN(CheckAmount)) + CAST(CheckAmount AS VARCHAR(20))
										WHEN 'commonexceptionid'	THEN REPLICATE('0', 19 - LEN(CommonExceptionID)) + CAST(CommonExceptionID AS VARCHAR(20))
										WHEN 'depositdate'			THEN CAST(DepositDateKey AS VARCHAR(8))
										WHEN 'deadline'				THEN CONVERT(VARCHAR, Deadline, 120)
										WHEN 'entity'				THEN EntityName
										WHEN 'tranid'				THEN REPLICATE('0', 19 - LEN(TransactionID)) + CAST(TransactionID AS VARCHAR(20))
										WHEN 'batchid'				THEN REPLICATE('0', 19 - LEN(SourceBatchID)) + CAST(SourceBatchID AS VARCHAR(20))
										WHEN 'workgroup'			THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + LongName
										END
					END ASC,
				CASE WHEN lower(@sortDir) = 'descending'
					THEN CASE @sortBy	WHEN 'status'				THEN DecisionPending
										WHEN 'batchsource'			THEN BatchSourceName
										WHEN 'paymenttype'			THEN BatchPaymentTypeName
										WHEN 'checkamount'			THEN REPLICATE('0', 18 - LEN(CheckAmount)) + CAST(CheckAmount AS VARCHAR(20))
										WHEN 'commonexceptionid'	THEN REPLICATE('0', 19 - LEN(CommonExceptionID)) + CAST(CommonExceptionID AS VARCHAR(20))
										WHEN 'depositdate'			THEN CAST(DepositDateKey AS VARCHAR(8))
										WHEN 'deadline'				THEN CONVERT(VARCHAR, Deadline, 120)
										WHEN 'entity'				THEN EntityName
										WHEN 'tranid'				THEN REPLICATE('0', 19 - LEN(TransactionID)) + CAST(TransactionID AS VARCHAR(20))
										WHEN 'batchid'				THEN REPLICATE('0', 19 - LEN(SourceBatchID)) + CAST(SourceBatchID AS VARCHAR(20))
										WHEN 'workgroup'			THEN REPLICATE('0', 19 - LEN(SiteClientAccountID)) + CAST(SiteClientAccountID AS VARCHAR(20)) + LongName
										END
					END DESC;

	DECLARE @auditMessage VARCHAR(1024) = 'Receivables Exceptions executed for Parameters -'
											+ ' Date: ' + CAST(@filterDateKey AS VARCHAR(8))
											+ ' Grouped By: ' + CASE WHEN @groupBy = '' THEN 'No grouping' ELSE @groupBy END
											+ ' Sorted By: ' + CASE WHEN @sortBy = '' THEN 'No sorting' ELSE @sortBy + ' ' + @sortDir END

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubException.usp_Report_ReceivablesExceptions', @parmEventName='Report Executed', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
