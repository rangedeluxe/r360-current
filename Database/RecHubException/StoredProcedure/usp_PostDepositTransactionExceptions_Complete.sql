--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_Complete
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Complete') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Complete
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Complete
( 
   @parmBatchID BIGINT,
   @parmDepositDateKey INT,
   @parmTransactionID INT,
   @parmUserName VARCHAR(128)
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 10/02/2017
*
* Purpose: Completes a transaction with exceptions in it 
*
* Modification History
* 10/02/2017 PT 151106278	MAA Created
* 10/19/2017 PT 147283509	JPB	Added auditing, renamed for consistency
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
	@localTran AS BIT,
	@UserID INT,
	@TransactionSequence INT,
	@TransactionAccepted TINYINT,
	@AuditMessage VARCHAR(MAX);

BEGIN TRY	
	BEGIN TRAN 
	SELECT @localTran = 1;

	SELECT
		@UserID = LockedByUserID
	FROM
		RecHubException.PostDepositTransactionExceptions
	WHERE 
		BatchID = @parmBatchID  
		AND DepositDateKey = @parmDepositDateKey  
		AND TransactionID = @parmTransactionID;

		
	SELECT
		@TransactionAccepted = TransactionExceptionStatusKey
	FROM
		RecHubData.dimTransactionExceptionStatuses
	WHERE
		ExceptionStatusName = 'Accepted';
		
	/* Delete transaction data form temp tables */
	DELETE 
		RecHubException.PostDepositDataEntryDetails 
	WHERE 
		BatchID = @parmBatchID  
		AND DepositDateKey = @parmDepositDateKey  
		AND TransactionID = @parmTransactionID;

	DELETE 
		RecHubException.PostDepositTransactionExceptions
	WHERE 
		BatchID = @parmBatchID  
		AND DepositDateKey = @parmDepositDateKey  
		AND TransactionID = @parmTransactionID;
		
	/* Update transaction summary status to mark the transaction as done*/
	UPDATE 
		RecHubData.factTransactionSummary 
	SET 
		TransactionExceptionStatusKey = @TransactionAccepted
	WHERE 
		BatchID = @parmBatchID  
		AND DepositDateKey = @parmDepositDateKey  
		AND TransactionID = @parmTransactionID;
	
	SET @AuditMessage =  @parmUserName + ' accepted the Post Deposit Exception(s) for Batch: ';

	SELECT 
		@AuditMessage += CAST(SourceBatchID AS VARCHAR(10)) 
		+ ', Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
		+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
		+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
		+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
		+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
		+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
		+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
	FROM
		RecHubData.factTransactionSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
		INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
		INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
	WHERE
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND TransactionID = @parmTransactionID
		AND IsDeleted = 0;

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
		@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Complete',
		@parmEventName			= 'Post-Deposit Exceptions',
		@parmEventType			= 'Post-Deposit Exceptions',
		@parmUserID				= @UserID,
		@parmAuditMessage		= @AuditMessage;

	COMMIT TRAN 
		
END TRY
BEGIN CATCH
	IF( @LocalTran = 1 )
		ROLLBACK TRAN;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


