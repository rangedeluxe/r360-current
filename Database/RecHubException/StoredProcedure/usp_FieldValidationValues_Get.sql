--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_FieldValidationValues_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_FieldValidationValues_Get') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_FieldValidationValues_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_FieldValidationValues_Get
(
	@parmFieldValidationKey	BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 05/11/2014
*
* Purpose: Reads the Field Validation Values for a specific field validation key
*
* Modification History
* 05/11/2014 WI 141054 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	
	SELECT	RecHubException.FieldValidationValues.Value
	FROM	RecHubException.FieldValidationValues
	WHERE	RecHubException.FieldValidationValues.FieldValidationKey = @parmFieldValidationKey;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

