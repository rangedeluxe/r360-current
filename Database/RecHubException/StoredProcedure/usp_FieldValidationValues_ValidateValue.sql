--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_FieldValidationValues_ValidateValue
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_FieldValidationValues_ValidateValue') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_FieldValidationValues_ValidateValue
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_FieldValidationValues_ValidateValue
(
	@parmFieldValidationKey	BIGINT,
	@parmFieldValue			VARCHAR(255),
	@parmValueExists		BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 05/11/2014
*
* Purpose: Validates the value against the Field Validation Values for a specific field validation key
*
* Modification History
* 06/03/2014 WI 145489 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	--This method may be called many times by the SSIS packages and as such must perform efficiently
	-- so all it should do is check if the value exists, nothing more.
	IF EXISTS(SELECT 1
				FROM RecHubException.FieldValidationValues
				WHERE RecHubException.FieldValidationValues.FieldValidationKey = @parmFieldValidationKey
					AND RecHubException.FieldValidationValues.Value = @parmFieldValue)
		SET @parmValueExists = 1;
	ELSE
		SET @parmValueExists = 0;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

