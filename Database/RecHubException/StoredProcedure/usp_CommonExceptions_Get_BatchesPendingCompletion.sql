--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_Get_BatchesPendingCompletion
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_Get_BatchesPendingCompletion') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_CommonExceptions_Get_BatchesPendingCompletion
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_Get_BatchesPendingCompletion 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: If there are errors updating the batch status, recovery is necessary.
*			This procedure checks the status of all transactions to find out if
*			there are any batches that should be updated (out of sync due to previous issue)
*
* Modification History
* 07/09/2013 WI 108949 RDS	Created
* 04/24/2014 WI 139283 RDS	Updated for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	-- Return IntegraPAY batch IDs where all transactions have been promoted to a status beyond "Unresolved" (0)
	--	but at least one transaction is still marked pending (1 or 2)
	SELECT 2 AS ExceptionType, GlobalBatchID, CAST(NULL AS bigint) AS ExceptionBatchKey
	FROM RecHubException.IntegraPAYExceptions
	GROUP BY GlobalBatchID
	HAVING MIN(DecisioningStatus) IN (1,2)
	
	UNION ALL

	-- Return Post Process batch IDs where all transactions have been promoted to a status beyond "Unresolved" (1)
	--	and the batch is still marked unresolved (1)
	SELECT 1 AS ExceptionType, NULL, RecHubException.ExceptionBatches.ExceptionBatchKey
	FROM RecHubException.ExceptionBatches 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
	INNER JOIN RecHubException.DecisioningTransactions ON
		RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
	WHERE RecHubException.ExceptionBatches.BatchStatusKey = 1 -- Unresolved
	GROUP BY RecHubException.ExceptionBatches.ExceptionBatchKey
	HAVING MIN(RecHubException.DecisioningTransactions.TransactionStatusKey) > 1;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

