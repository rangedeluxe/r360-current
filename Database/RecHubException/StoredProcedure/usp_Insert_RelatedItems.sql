--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Insert_RelatedItems
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_Insert_RelatedItems') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_Insert_RelatedItems
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Insert_RelatedItems
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 10/02/2017
*
* Purpose: Insert new factStubs/factDataEntryDetail records from 
*		PostDepositExceptions
*
* Modification History
* 10/02/2017 PT 147282969	MGE Created
******************************************************************************/
DECLARE @LocalTran BIT,
		@ModificationDate DATETIME = GETDATE();
DECLARE @MaxBatchSequence INT,
		@MaxStubSequence INT,
		@MaxSequenceWithinTransaction INT;

BEGIN TRY

	BEGIN TRAN
	SET @LocalTran = 1;

	/* Lock semaphor to prevent concurrent derivation of keys */
	EXEC sp_getapplock @Resource='PostDepositDEDetails', @LockMode='Exclusive', @LockTimeout=-1;
	/* Derive the keys */
	SELECT @MaxBatchSequence = COALESCE(MAX(BatchSequence),0), 
			@MaxStubSequence = COALESCE(MAX(StubSequence),0), 
			@MaxSequenceWithinTransaction = COALESCE(MAX(SequenceWithinTransaction),0)
	FROM RecHubData.factStubs
	WHERE 
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND TransactionID = @parmTransactionID
		AND IsDeleted = 0;
	SELECT @MaxBatchSequence = 
		CASE WHEN BatchSequence > @MaxBatchSequence THEN BatchSequence
		ELSE @MaxBatchSequence
		END,
		@MaxSequenceWithinTransaction = 
		CASE WHEN BatchSequence > @MaxSequenceWithinTransaction THEN BatchSequence
		ELSE @MaxSequenceWithinTransaction
		END
	FROM RecHubData.factChecks
	WHERE 
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND TransactionID = @parmTransactionID
		AND IsDeleted = 0;
	SELECT @MaxBatchSequence = 
		CASE WHEN BatchSequence > @MaxBatchSequence THEN BatchSequence
		ELSE @MaxBatchSequence
		END,
		@MaxSequenceWithinTransaction = 
		CASE WHEN BatchSequence > @MaxSequenceWithinTransaction THEN BatchSequence
		ELSE @MaxSequenceWithinTransaction
		END
	FROM RecHubData.factDocuments
	WHERE 
		DepositDateKey = @parmDepositDateKey
		AND BatchID = @parmBatchID
		AND TransactionID = @parmTransactionID
		AND IsDeleted = 0;

	--SELECT @MaxBatchSequence , @MaxStubSequence, @MaxBatchSequence ;		--DEBUG ONLY

	/* Insert RecHubExceptions.PostDepositDataEntryDetails   */

	EXEC RecHubException.usp_PostDepositDataEntryDetails_Insert
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmSequenceWithinTransaction = @MaxSequenceWithinTransaction,
		@parmBatchSequence = @MaxBatchSequence,
		@parmStubSequence = @MaxStubSequence,
		@parmDataEntryValues = @parmDataEntryValues;

	/* Insert RecHubData.factStubs */
	
	EXEC RecHubException.usp_factStubs_Insert
		@parmDepositDateKey = @parmDepositDateKey,
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmModificationDate = @ModificationDate,
		@parmSequenceWithinTransaction = @MaxSequenceWithinTransaction,
		@parmBatchSequence = @MaxBatchSequence,
		@parmStubSequence = @MaxStubSequence,
		@parmDataEntryValues = @parmDataEntryValues;
	
	/* Insert RecHubData.factDataEntryDetails */

	EXEC RecHubException.usp_factDataEntryDetails_Insert
		@parmSessionID = @parmSessionID,
		@parmDepositDateKey = @parmDepositDateKey,	
		@parmBatchID = @parmBatchID,
		@parmTransactionID = @parmTransactionID,
		@parmSequenceWithinTransaction=@MaxSequenceWithinTransaction,
		@parmBatchSequence = @MaxBatchSequence,
		@parmStubSequence = @MaxStubSequence,
		@parmModificationDate = @ModificationDate,
		@parmDataEntryValues = @parmDataEntryValues;

	/* Unlock the semaphore */
	EXEC sp_releaseapplock @Resource='PostDepositDEDetails'
	COMMIT;

END TRY
BEGIN CATCH
	IF( @LocalTran = 1 )
		ROLLBACK TRAN;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH