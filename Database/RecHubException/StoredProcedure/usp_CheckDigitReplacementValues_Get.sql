--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CheckDigitReplacementValues_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CheckDigitReplacementValues_Get') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_CheckDigitReplacementValues_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CheckDigitReplacementValues_Get
(
	@parmCheckDigitRoutineKey	BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/29/2014
*
* Purpose: Reads the Check Digit Replacement Values for a given Routine.
*
* Modification History
* 04/30/2014 WI 139462 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	
	SELECT	RecHubException.CheckDigitReplacementValues.OriginalValue,
			RecHubException.CheckDigitReplacementValues.ReplacementValue
	FROM	RecHubException.CheckDigitReplacementValues
	WHERE	RecHubException.CheckDigitReplacementValues.CheckDigitRoutineKey = @parmCheckDigitRoutineKey;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

