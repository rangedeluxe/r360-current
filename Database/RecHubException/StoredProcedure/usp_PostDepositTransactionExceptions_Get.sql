--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Get') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Get
(
	@parmBatchID			BIGINT = NULL,
	@parmDepositDateKey		INT = NULL,
	@parmTransactionID		INT = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 07/12/2017
*
* Purpose: Get current Payment Type definitions
*
* Modification History
* 07/12/2017 PT 144805301	MGE	Created
* 07/17/2017 PT 144805301	MGE	Changed the parameters to use natural key
* 07/31/2017 PT 144852639	MGE Return LockedDate AS UTC DateTime
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		RecHubException.PostDepositTransactionExceptions.TransactionKey,
		RecHubException.PostDepositTransactionExceptions.BatchID,
		RecHubException.PostDepositTransactionExceptions.DepositDateKey,
		RecHubException.PostDepositTransactionExceptions.TransactionID,
		RecHubException.PostDepositTransactionExceptions.LockedByUserID,
		DATEADD(minute, DATEDIFF(minute, GetDate(), GETUTCDATE()),RecHubException.PostDepositTransactionExceptions.LockedDate) AS LockedDate,
		RecHubUser.Users.RA3MSID
	FROM   
		RecHubException.PostDepositTransactionExceptions
		INNER JOIN RecHubUser.Users ON RecHubUser.Users.UserID = RecHubException.PostDepositTransactionExceptions.LockedByUserID
	WHERE 
		RecHubException.PostDepositTransactionExceptions.BatchID =
		CASE 
		WHEN @parmBatchID IS NULL THEN 
			RecHubException.PostDepositTransactionExceptions.BatchID
		ELSE @parmBatchID
		END
		AND
		RecHubException.PostDepositTransactionExceptions.DepositDateKey =
		CASE 
		WHEN @parmDepositDateKey IS NULL THEN 
			RecHubException.PostDepositTransactionExceptions.DepositDateKey
		ELSE @parmDepositDateKey
		END
		AND
		RecHubException.PostDepositTransactionExceptions.TransactionID =
		CASE 
		WHEN @parmTransactionID IS NULL THEN 
			RecHubException.PostDepositTransactionExceptions.TransactionID
		ELSE @parmTransactionID
		END
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
