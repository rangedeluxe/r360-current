--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Stubs_InsUpd_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Stubs_InsUpd_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_Stubs_InsUpd_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Stubs_InsUpd_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT,
	@parmXmlUpdates			XML		-- <Updates><I Seq="1"><F ID="1234" Val="Something" /><F ... /></I><I ... /></Updates>
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/29/2014
*
* Purpose: Update Stub Records with Post-Process Exception keyed values
*
* Modification History
* 04/29/2014 WI 139342 RDS	Created
* 05/14/2014 WI 141679 RDS	Add auditing
* 07/30/2014 WI 156359 RDS	Add auditing
* 09/10/2014 WI 139342 KLC	Add account number updates
* 10/10/2014 WI 141679 KLC	Updated auditing text
* 10/22/2014 WI 173969 KLC	Updated to support inserts and deletes
* 11/11/2014 WI 173969 KLC	Updated to handle specific date formats
******************************************************************************/
SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY

	DECLARE @deleteCount INT = 0,
			@itemCount INT = 0,
			@insertCount INT = 0;

	DECLARE @exceptionBatchKey BIGINT, @batchID BIGINT, @transactionKey BIGINT, @txnSequence INT, @transactionID INT, @depositDateKey INT;
	SELECT	@exceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey,
			@batchID = RecHubException.ExceptionBatches.BatchID,
			@transactionKey = RecHubException.Transactions.TransactionKey,
			@txnSequence = RecHubException.Transactions.TxnSequence,
			@transactionID = RecHubException.Transactions.TransactionID,
			@depositDateKey = RecHubException.ExceptionBatches.DepositDateKey
	FROM RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.Transactions
			ON RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
		INNER JOIN RecHubException.ExceptionBatches
			ON RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

	DECLARE @tblItemUpdates TABLE(TempItemId INT, StubKey BIGINT, BatchSequence INT, SequenceWithinTransaction INT, StubSequence INT, ActionType TINYINT);
	DECLARE @tblInsertedItems TABLE(BatchSequence INT, StubKey BIGINT);

	INSERT INTO @tblItemUpdates(TempItemId, StubKey, BatchSequence, ActionType)
	SELECT	X.c.value('@TmpId', 'INT'),
			RecHubException.Stubs.StubKey,
			X.c.value('@Seq', 'INT'),
			X.c.value('@Act', 'TINYINT')
	FROM @parmXmlUpdates.nodes('/Updates/I') AS X(c)
		LEFT OUTER JOIN RecHubException.Stubs
			ON RecHubException.Stubs.TransactionKey = @transactionKey
				AND RecHubException.Stubs.BatchSequence = X.c.value('@Seq', 'INT')
				AND X.c.value('@Act', 'TINYINT') <> 3/*Insert*/;

	SET @itemCount = @@ROWCOUNT;

	-- Store updates to a temporary table
	DECLARE @tblFieldUpdates TABLE (TempItemId INT, DataEntryColumnKey BIGINT, FldName VARCHAR(128), FieldVal SQL_VARIANT, IsItemAmount BIT, IsItemAccount BIT);
	INSERT INTO @tblFieldUpdates (TempItemId, DataEntryColumnKey, FieldVal)
		SELECT 
			X.c.value('../@TmpId', 'int'),
			X.c.value('@ID', 'bigint'),
			COALESCE(X.c.value('@DateVal', 'varchar(255)'), X.c.value('@Val', 'varchar(255)')) FieldVal
		FROM @parmXmlUpdates.nodes('/Updates/I/F') AS X(c);

	-- Apply the correct data types and FldNames
	UPDATE tblFieldUpdates
		SET FldName = RecHubData.dimDataEntryColumns.FldName,
			FieldVal = CASE WHEN FieldVal = '' THEN FieldVal
						ELSE
						CASE RecHubData.dimDataEntryColumns.DataType
							WHEN 11 THEN CAST(CONVERT(DATE, FieldVal, 112) AS SQL_VARIANT)
							WHEN 7 THEN CAST(FieldVal AS MONEY)
							WHEN 6 THEN CAST(FieldVAL AS FLOAT)
							ELSE SUBSTRING(CAST(FieldVal AS VARCHAR(255)), 1, RecHubData.dimDataEntryColumns.FldLength) END /*Using FldLength for the substring may cause problems here as ImageRPS always sends 0 for the FldLength*/
						END,
			IsItemAmount = CASE WHEN RecHubData.dimDataEntryColumns.TableType = 2 and RecHubData.dimDataEntryColumns.FldName = 'Amount' THEN 1 ElSE 0 END,
			IsItemAccount = CASE WHEN RecHubData.dimDataEntryColumns.TableType = 2 and RecHubData.dimDataEntryColumns.FldName = 'AccountNumber' THEN 1 ElSE 0 END
	FROM @tblFieldUpdates tblFieldUpdates
	INNER JOIN RecHubData.dimDataEntryColumns ON
		tblFieldUpdates.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey;

	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END
	--if we have items to delete flag them deleted now
	IF EXISTS(SELECT 1 FROM @tblItemUpdates WHERE ActionType = 2/*Delete*/)
	BEGIN
		UPDATE RecHubException.Stubs
		SET IsDeleted = 1
		FROM RecHubException.Stubs
			INNER JOIN @tblItemUpdates tblItemUpdates
				ON RecHubException.Stubs.StubKey = tblItemUpdates.StubKey
					and tblItemUpdates.ActionType = 2;/*Delete*/

		SET @deleteCount = @@ROWCOUNT;
	END

	--if we have items to insert we must go through the logic to find the various sequences and insert them
	IF EXISTS(SELECT 1 FROM @tblItemUpdates WHERE ActionType = 3/*Insert*/)
	BEGIN
		DECLARE @maxBatchSeq INT = 0,		@compareMaxBatchSeq INT,
				@maxSeqWithinTran INT = 0,	@compareMaxSeqWithinTran INT,
				@maxStubSequence INT = 0,	@compareMaxStubSeq INT;

		SELECT	@compareMaxBatchSeq = MAX(BatchSequence),
				@compareMaxSeqWithinTran = MAX(CASE WHEN TransactionID = @transactionID THEN SequenceWithinTransaction ELSE 0 END),
				@compareMaxStubSeq = MAX(StubSequence)
		FROM RecHubException.Transactions
			INNER JOIN RecHubException.Stubs WITH(TABLOCKX) --Locking the table so nothing else can insert a batch sequence we will be using
				ON RecHubException.Transactions.TransactionKey = RecHubException.Stubs.TransactionKey
		WHERE RecHubException.Transactions.ExceptionBatchKey = @exceptionBatchKey;
		IF @maxBatchSeq < @compareMaxBatchSeq SET @maxBatchSeq = @compareMaxBatchSeq;
		IF @maxSeqWithinTran < @compareMaxSeqWithinTran SET @maxSeqWithinTran = @compareMaxSeqWithinTran;
		IF @maxStubSequence < @compareMaxStubSeq SET @maxStubSequence = @compareMaxStubSeq;

		SELECT	@compareMaxBatchSeq = MAX(BatchSequence),
				@compareMaxSeqWithinTran = MAX(CASE WHEN TransactionID = @transactionID THEN SequenceWithinTransaction ELSE 0 END)
		FROM RecHubData.factChecks WHERE BatchID = @batchID;
		IF @maxBatchSeq < @compareMaxBatchSeq SET @maxBatchSeq = @compareMaxBatchSeq;
		IF @maxSeqWithinTran < @compareMaxSeqWithinTran SET @maxSeqWithinTran = @compareMaxSeqWithinTran;

		SELECT	@compareMaxBatchSeq = MAX(BatchSequence),
				@compareMaxSeqWithinTran = MAX(CASE WHEN TransactionID = @transactionID THEN SequenceWithinTransaction ELSE 0 END)
		FROM RecHubData.factDocuments WHERE BatchID = @batchID;
		IF @maxBatchSeq < @compareMaxBatchSeq SET @maxBatchSeq = @compareMaxBatchSeq;
		IF @maxSeqWithinTran < @compareMaxSeqWithinTran SET @maxSeqWithinTran = @compareMaxSeqWithinTran;

		SELECT	@compareMaxBatchSeq = MAX(BatchSequence),
				@compareMaxSeqWithinTran = MAX(CASE WHEN TransactionID = @transactionID THEN SequenceWithinTransaction ELSE 0 END),
				@compareMaxStubSeq = MAX(StubSequence)
		FROM RecHubData.factStubs WHERE BatchID = @batchID;
		IF @maxBatchSeq < @compareMaxBatchSeq SET @maxBatchSeq = @compareMaxBatchSeq;
		IF @maxSeqWithinTran < @compareMaxSeqWithinTran SET @maxSeqWithinTran = @compareMaxSeqWithinTran;
		IF @maxStubSequence < @compareMaxStubSeq SET @maxStubSequence = @compareMaxStubSeq;

		SELECT	@compareMaxBatchSeq = MAX(BatchSequence),
				@compareMaxSeqWithinTran = MAX(CASE WHEN TransactionID = @transactionID THEN SequenceWithinTransaction ELSE 0 END)
		FROM RecHubException.Transactions
			INNER JOIN RecHubException.Payments
				ON RecHubException.Transactions.TransactionKey = RecHubException.Payments.TransactionKey
		WHERE RecHubException.Transactions.ExceptionBatchKey = @exceptionBatchKey;
		IF @maxBatchSeq < @compareMaxBatchSeq SET @maxBatchSeq = @compareMaxBatchSeq;
		IF @maxSeqWithinTran < @compareMaxSeqWithinTran SET @maxSeqWithinTran = @compareMaxSeqWithinTran;

		--increment our sequences to be the next available
		SELECT @maxBatchSeq = @maxBatchSeq + 1, @maxSeqWithinTran = @maxSeqWithinTran + 1, @maxStubSequence = @maxStubSequence + 1;

		--Update the sequences, ensuring the order of inserted items remains as it came in
		UPDATE tblItemUpdates
		SET BatchSequence = @maxBatchSeq + OrderedInserts.Row,
			SequenceWithinTransaction = @maxSeqWithinTran + OrderedInserts.Row,
			StubSequence = @maxStubSequence + OrderedInserts.Row
		FROM @tblItemUpdates tblItemUpdates
			INNER JOIN (
				SELECT ROW_NUMBER() OVER(ORDER BY TempItemID ASC) AS Row, TempItemId
				FROM @tblItemUpdates
				WHERE ActionType = 3
				) OrderedInserts
				ON tblItemUpdates.TempItemId = OrderedInserts.TempItemId;

		INSERT INTO RecHubException.Stubs(TransactionKey, TxnSequence, SequenceWithinTransaction, BatchSequence, StubSequence)
		OUTPUT Inserted.BatchSequence, Inserted.StubKey INTO @tblInsertedItems
		SELECT @transactionKey, @txnSequence, SequenceWithinTransaction, BatchSequence, StubSequence
		FROM @tblItemUpdates
		WHERE ActionType = 3;

		SET @insertCount = @@ROWCOUNT;

		UPDATE tblItemUpdates
		SET StubKey = tblInsertedItems.StubKey
		FROM @tblItemUpdates tblItemUpdates
			INNER JOIN @tblInsertedItems tblInsertedItems
				ON tblItemUpdates.BatchSequence = tblInsertedItems.BatchSequence;
	END

	-- Update/Add existing dynamic fields
	MERGE RecHubException.StubDataEntry
	USING(
		SELECT tblItemUpdates.StubKey, tblFieldUpdates.FldName, tblFieldUpdates.FieldVal
		FROM @tblItemUpdates tblItemUpdates
			INNER JOIN @tblFieldUpdates tblFieldUpdates
				ON tblItemUpdates.TempItemId = tblFieldUpdates.TempItemId
					AND tblFieldUpdates.IsItemAmount = 0
					AND tblFieldUpdates.IsItemAccount = 0
		WHERE tblItemUpdates.ActionType <> 2/*Not being deleted*/
		) S
	ON RecHubException.StubDataEntry.StubKey = S.StubKey AND RecHubException.StubDataEntry.FldName = S.FldName
	WHEN MATCHED AND RecHubException.StubDataEntry.Value <> S.FieldVal THEN
		UPDATE SET Value = S.FieldVal
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (StubKey, FldName, Value)
		VALUES (S.StubKey, S.FldName, S.FieldVal);

	-- Update "standard" fields...
	UPDATE RecHubException.Stubs
	SET	Amount = CAST(AmountUpdate.FieldVal AS MONEY),
		AccountNumber = CAST(AccountUpdate.FieldVal AS VARCHAR(80))
	FROM RecHubException.Stubs
		INNER JOIN @tblItemUpdates tblItemUpdates
			ON RecHubException.Stubs.StubKey = tblItemUpdates.StubKey
		LEFT JOIN @tblFieldUpdates AmountUpdate
			ON AmountUpdate.IsItemAmount = 1
			AND AmountUpdate.TempItemId = tblItemUpdates.TempItemId 
		LEFT JOIN @tblFieldUpdates AccountUpdate
			ON AccountUpdate.IsItemAccount = 1
			AND AccountUpdate.TempItemId = tblItemUpdates.TempItemId 
	WHERE RecHubException.Stubs.TransactionKey = @transactionKey

	DECLARE @auditMessage2 VARCHAR(max) = '';
	SELECT @auditMessage2 = @auditMessage2 + ';  [Seq=' + cast(BatchSequence as varchar(20)) + '] ' + FldName + ': ' + cast(FieldVal as varchar(1000))
	FROM @tblItemUpdates tblItemUpdates
		INNER JOIN @tblFieldUpdates tblFieldUpdates
			ON tblItemUpdates.TempItemId = tblFieldUpdates.TempItemId
	WHERE tblItemUpdates.ActionType <> 2/*Delete*/;

	-- Construct the audit message
	DECLARE @auditMessage VARCHAR(MAX);
	SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
			+ ISNULL(', DepositDate: ' + CAST(@depositDateKey as VARCHAR), '')
			+ ISNULL(', BatchID: ' + CAST(@batchID as VARCHAR), '')
			+ ISNULL(', TransactionID: ' + CAST(@transactionID as VARCHAR), '')
			+ ' - ' + CAST(@itemCount - @insertCount - @deleteCount AS VARCHAR) + ' stub(s) updated'
			+ ', ' + CAST(@insertCount AS VARCHAR) + ' stub(s) inserted'
			+ ', ' + CAST(@deleteCount AS VARCHAR) + ' stub(s) deleted'
			+ @auditMessage2;

	-- Audit this event
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName = 'RecHubException.usp_Stubs_InsUpd_ByCommonExceptionID',
			@parmEventName = 'E Update Item',
			@parmEventType = 'Exceptions',
			@parmUserID = @parmUserID,
			@parmAuditMessage = @auditMessage

	IF @LocalTransaction = 1 COMMIT TRANSACTION;
END TRY

BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH