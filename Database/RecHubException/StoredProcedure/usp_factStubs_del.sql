--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factStubs_del
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factStubs_del') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factStubs_del
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factStubs_del
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmModificationDate DATETIME,
	@parmBatchSequence INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: BLR
* Date: 09/20/2017
*
* Purpose: Delete an existing factStubs record.
*
* Modification History
* 09/20/2017 PT 147282137 BLR Created
******************************************************************************/
SET NOCOUNT ON; 
BEGIN TRY
	UPDATE
		RecHubData.factStubs
	SET
		RecHubData.factStubs.IsDeleted = 1,
		RecHubData.factStubs.ModificationDate = @parmModificationDate
	WHERE RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
		AND RecHubData.factStubs.BatchID = @parmBatchID
		AND RecHubData.factStubs.TransactionID = @parmTransactionID
		AND RecHubData.factStubs.BatchSequence = @parmBatchSequence
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH