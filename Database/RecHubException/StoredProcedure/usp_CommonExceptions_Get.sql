--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_Get') IS NOT NULL
    DROP PROCEDURE	RecHubException.usp_CommonExceptions_Get 
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_Get 
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: Returns Common Exceptions visible to the provided user
*
* Modification History
* 05/23/2013 WI 103183 WJS	Created
* 07/15/2013 WI 108940 RDS	Updates to finish
* 07/26/2013 WI 103183 JBS  Changing called proc from usp_OLClientAccounts_Get_ByUserID
*							to usp_OLClientAccounts_Get_ByUserID_Reporting
* 10/18/2013 WI 103183 JMC  Changed logic to return Batches WHERE ProcessingDate=Today.
* 10/30/2013 WI 119489 CMC  Updated to use new procedures that select the pruned
*								org - client/account tree
* 11/06/2013 WI 121739 CMC  Select the distinct batches
* 11/26/2013 WI 123518 CMC  Restrict by deadline
* 04/25/2014 WI 139277 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
* 05/14/2014 WI 141668 RDS	Add auditing
* 06/03/2014 WI 143501 JBS	Adding SourceBatchID and Workgroug LongName to ResultSet
* 07/11/2014 WI 153428 KLC	Updated to take session id and validate entitlements
*							 now that we have RAAM integration
* 08/20/2014 WI 153428 KLC	Added Transaction ID to returned data
* 10/09/2014 WI 153428 KLC	Adde return of ImportType Short Name
* 10/10/2014 WI 141668 KLC	Updated auditing text
* 10/29/2014 WI 174866 KLC	Changed return of ImportType Short Name to Batch Source Short Name
* 10/30/2014 WI 174866 KLC	Updated to obey current processing date
* 11/10/2014 WI 174866 KLC	Updated to return both Batch Source and Import Type Short Names
* 11/11/2014 WI 174866 KLC	Updated to not return transactions with a status of 99
* 12/17/2014 WI 182277 LA   Modified to show only Active Workgroups 
******************************************************************************/
SET NOCOUNT ON;

DECLARE @Today INT = CONVERT(VARCHAR(30),  GETDATE(), 112);
DECLARE @deadlineBuffer INT;
DECLARE @currentTime VARCHAR(8) = CONVERT(VARCHAR(8), GETDATE(), 108);
DECLARE @currentDate DATETIME = CONVERT(DATE, GETDATE());

BEGIN TRY
	-- Audit this event
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName = 'RecHubException.usp_CommonExceptions_Get',
			@parmEventName = 'E View Summary List',
			@parmEventType = 'Exceptions',
			@parmUserID = @parmUserID,
			@parmAuditMessage = ''

	/* retrieve the deadline buffer minutes to apply */
	SELECT
		@deadlineBuffer = CASE WHEN ISNUMERIC(DefaultSetting) > 0 THEN DefaultSetting ELSE 0 END
	FROM 
		RecHubUser.OLPreferences
	WHERE
		PreferenceName = 'exceptiondeadlinebufferminutes'
	
	IF @deadlineBuffer IS NULL
		SET @deadlineBuffer = 0

	;WITH DistinctClientAccounts AS
	(
		SELECT DISTINCT
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			RecHubUser.OLWorkgroups.EntityID,
			CAST(COALESCE(RecHubUser.OLWorkgroups.DisplayBatchID, RecHubUser.OLEntities.DisplayBatchID, 0) AS BIT) AS DisplayBatchID
		FROM
			RecHubUser.SessionClientAccountEntitlements
		INNER JOIN RecHubUser.OLWorkgroups ON 
			RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
			AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
		INNER JOIN RecHubUser.OLEntities ON
			RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	),
	CTE_IntegraPAYExceptions AS
	(
		SELECT
			RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
			RecHubException.IntegraPAYExceptions.SiteBankID,
			RecHubException.IntegraPAYExceptions.SiteClientAccountID,
			RecHubException.IntegraPAYExceptions.DepositDateKey,
			'' AS BatchID,		-- NO R360 BatchID exists for Integrapay Exceptions
			RecHubException.IntegraPAYExceptions.BatchID AS SourceBatchID,
			RecHubException.IntegraPAYExceptions.ImmutableDateKey,
			RecHubException.IntegraPAYExceptions.CheckAmount,
			DATEADD(minute, - @deadlineBuffer, RecHubException.IntegraPAYExceptions.Deadline) AS Deadline,
			RecHubException.IntegraPAYExceptions.InProcessException,
			RecHubException.IntegraPAYExceptions.DecisioningStatus,
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey as BatchPaymentTypeKey,
			RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
			RecHubData.dimBatchSources.LongName AS BatchSourceName,
			COALESCE(RecHubException.DecisioningTransactions.UserID,0) AS Locked,
			CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
			COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
			RecHubData.dimClientAccountsView.DisplayLabel,
			RecHubException.IntegraPAYExceptions.TransactionID,
			tblClientAccounts.EntityID,
			RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
			RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
			tblClientAccounts.DisplayBatchID
		FROM
			RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.IntegraPAYExceptions ON
			RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
		INNER JOIN RecHubData.dimBatchSources ON
			RecHubException.IntegraPAYExceptions.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimImportTypes ON
			RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubException.IntegraPAYExceptions.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN DistinctClientAccounts AS tblClientAccounts  ON
			tblClientAccounts.SiteBankID = RecHubException.IntegraPAYExceptions.SiteBankID
			AND tblClientAccounts.SiteClientAccountID = RecHubException.IntegraPAYExceptions.SiteClientAccountID
		INNER JOIN RecHubData.dimClientAccountsView	ON
			RecHubData.dimClientAccountsView.SiteBankID = RecHubException.IntegraPAYExceptions.SiteBankID
			AND RecHubData.dimClientAccountsView.SiteClientAccountID = RecHubException.IntegraPAYExceptions.SiteClientAccountID
			AND RecHubData.dimClientAccountsView.IsActive = 1
		INNER JOIN RecHubData.dimSiteCodes
			ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		LEFT OUTER JOIN RecHubUser.Users ON
			RecHubException.DecisioningTransactions.UserID = RecHubUser.Users.UserID
		WHERE 
			RecHubException.IntegraPAYExceptions.DecisioningStatus < 3
		AND 
			RecHubException.IntegraPAYExceptions.ImmutableDateKey = @Today	-- TODO: Why is this restriction here?  Performance?  Couldn't this possibly filter out transactions before their deadline?
		AND
			CONVERT(VARCHAR(8), Deadline, 108) >= @currentTime
		AND
			CAST(CONVERT(VARCHAR(8), RecHubData.dimSiteCodes.CurrentProcessingDate, 112) AS INT) >= RecHubException.IntegraPAYExceptions.DepositDateKey
	),
	CTE_PostProcessExceptions AS
	(
		SELECT
			RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
			RecHubException.ExceptionBatches.SiteBankID,
			RecHubException.ExceptionBatches.SiteWorkGroupID AS SiteClientAccountID,
			RecHubException.ExceptionBatches.DepositDateKey,
			RecHubException.ExceptionBatches.BatchID,
			RecHubException.ExceptionBatches.SourceBatchID,
			RecHubException.ExceptionBatches.ImmutableDateKey,
			(SELECT SUM(Amount) FROM RecHubException.Payments WHERE TransactionKey = T.TransactionKey)  AS CheckAmount,
			CASE WHEN RecHubException.DataEntrySetups.DeadLineDay IS NULL THEN NULL
				ELSE DATEADD(mi, RecHubException.DataEntrySetups.DeadLineDay%100, DATEADD(hh, RecHubException.DataEntrySetups.DeadLineDay/100, @currentDate)) END AS Deadline,
			CAST(0 AS BIT) AS InProcessException,	-- These tables by definition contain Post-Process exceptions
			(RecHubException.DecisioningTransactions.TransactionStatusKey - 1) AS DecisioningStatus,	-- Note: TransactionStatus values are one greater than CEDecisioning status values...  So we subtract one...
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey as BatchPaymentTypeKey,
			RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
			RecHubData.dimBatchSources.LongName AS BatchSourceName,
			COALESCE(RecHubException.DecisioningTransactions.UserID,0) AS Locked,
			CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
			COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
			RecHubData.dimClientAccountsView.DisplayLabel,
			T.TransactionID,
			tblClientAccounts.EntityID,
			RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
			RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
			RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
			tblClientAccounts.DisplayBatchID
		FROM
			RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.Transactions T ON
			RecHubException.DecisioningTransactions.TransactionKey = T.TransactionKey
		INNER JOIN RecHubException.ExceptionBatches ON
			T.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
		INNER JOIN RecHubData.dimBatchSources ON
			RecHubException.ExceptionBatches.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
		INNER JOIN RecHubData.dimImportTypes ON
			RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
		INNER JOIN RecHubData.dimBatchPaymentTypes ON
			RecHubException.ExceptionBatches.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
		INNER JOIN DistinctClientAccounts AS tblClientAccounts  ON
			tblClientAccounts.SiteBankID = RecHubException.ExceptionBatches.SiteBankID
			AND tblClientAccounts.SiteClientAccountID = RecHubException.ExceptionBatches.SiteWorkGroupID
		INNER JOIN RecHubData.dimClientAccountsView	ON
			RecHubData.dimClientAccountsView.SiteBankID = RecHubException.ExceptionBatches.SiteBankID
			AND RecHubData.dimClientAccountsView.SiteClientAccountID = 	RecHubException.ExceptionBatches.SiteWorkGroupID
			AND RecHubData.dimClientAccountsView.IsActive = 1
		INNER JOIN RecHubData.dimSiteCodes
			ON RecHubData.dimClientAccountsView.SiteCodeID = RecHubData.dimSiteCodes.SiteCodeID
		INNER JOIN RecHubException.DataEntrySetups ON 
			RecHubException.ExceptionBatches.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
			AND RecHubException.ExceptionBatches.SiteWorkgroupID = RecHubException.DataEntrySetups.SiteWorkgroupID
		LEFT OUTER JOIN RecHubUser.Users ON
			RecHubException.DecisioningTransactions.UserID = RecHubUser.Users.UserID
		WHERE 
			RecHubException.ExceptionBatches.BatchStatusKey < 3 -- Resolved, sent to warehouse
		AND
			RecHubException.DecisioningTransactions.TransactionStatusKey <> 99 -- Transaction Completed, sent to warehouse
			-- TODO: Verify there is no condition on date.  I believe that there is no true deadline on when posting exceptions can be processed, so we should return them until the underlying warehouse algorithms purge the records... 
		AND
			CAST(CONVERT(VARCHAR(8), RecHubData.dimSiteCodes.CurrentProcessingDate, 112) AS INT) >= RecHubException.ExceptionBatches.DepositDateKey
	),
	CTE_AllExceptions AS
	(
		SELECT * from CTE_IntegraPAYExceptions
		union all
		SELECT * from CTE_PostProcessExceptions
	)
	SELECT
			CommonExceptionID,
			SiteBankID,
			SiteClientAccountID,
			DepositDateKey,
			BatchID,
			SourceBatchID,
			ImmutableDateKey,
			CheckAmount,
			Deadline,
			InProcessException,
			DecisioningStatus,
			BatchPaymentTypeKey,
			BatchPaymentTypeName,
			BatchSourceName,
			Locked,
			LockTime,
			LockOwnerName,
			DisplayLabel AS LongName,
			TransactionID,
			EntityID,
			LockOwnerEntityID,
			BatchSourceShortName,
			ImportTypeShortName,
			DisplayBatchID
		FROM
			CTE_AllExceptions;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH