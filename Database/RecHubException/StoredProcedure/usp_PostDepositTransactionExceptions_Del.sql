--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Del') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Del
(
	@parmBatchID			BIGINT,
	@parmDepositDateKey		INT,
	@parmTransactionID		INT,
	@parmSID				UNIQUEIDENTIFIER,
	@parmUserName			VARCHAR(128),
	@parmErrorCode			INT	OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 07/12/2017
*
* Purpose: Remove rows from RecHubException.PostDepositTransactionExceptions whenever 
*			the user completes a locked PostDepositTransactionExceptions record
*
* Modification History
* 07/12/2017 PT 144805301	MGE	Created
* 07/17/2017 PT 144805301	MGE	Changed the parameters to use natural key
* 12/12/2017 PT 147284619	JPB User Activity Audit unlock
******************************************************************************/
SET NOCOUNT ON;

-- Ensure there is a transaction so data can't be deleted without being audited...
DECLARE @LocalTransaction bit = 0,
		@UserID INT;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
		SET @parmErrorCode = 0;
	END

	DECLARE @auditMessage			VARCHAR(1024),
			@errorDescription		VARCHAR(1024),
			@TransactionKey			BIGINT;

	DECLARE @curTime DATETIME = GETDATE();
	
	/* Get user ID from SID */
	DECLARE @UserIDs TABLE (UserID INT);

	INSERT INTO @UserIDs
	EXEC RecHubUser.usp_Users_UserID_Get_BySID @parmSID = @parmSID;

	SELECT TOP(1) 
		@UserID = UserID
	FROM 
		@UserIDs;
	

	-- Verify the Transaction Exists
	SELECT 
		@TransactionKey = TransactionKey 
	FROM 
		RecHubException.PostDepositTransactionExceptions
	WHERE 
		BatchID = @parmBatchID 
		AND DepositDateKey = @parmDepositDateKey 
		AND TransactionID = @parmTransactionID;
	
	IF @TransactionKey IS NULL	
	BEGIN
		SET @parmErrorCode = 2; -- 2 = Does Not Exist
		IF @LocalTransaction = 1 COMMIT TRANSACTION;
	END
	ELSE
	BEGIN
		-- Delete the PostDepositTransactionExceptions record
		DELETE FROM RecHubException.PostDepositTransactionExceptions 
			WHERE BatchID = @parmBatchID AND DepositDateKey = @parmDepositDateKey AND TransactionID = @parmTransactionID

		-- Audit the RecHubException.PostDepositTransactionExceptions delete
		SET @auditMessage = 'Removed PostDepositTransactionExceptions Lock for Batch: ' + CAST(@parmBatchID AS VARCHAR(10))
			+ ': DepositDateKey = ' + CAST(@parmDepositDateKey AS VARCHAR(8)) 
			+ ': TransactionID = ' + CAST(@parmTransactionID AS VARCHAR(10))
			+ ': UserID = ' + CAST(@UserID AS VARCHAR(10))
			+ '.';

		EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
				@parmUserID				= @UserID,
				@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Del',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'PostDepositTransactionExceptions',
				@parmColumnName			= 'TransactionKey',
				@parmAuditValue			= @TransactionKey,
				@parmAuditType			= 'DEL',
				@parmAuditMessage		= @auditMessage;

		SET @AuditMessage =  @parmUserName + ' unlocked Post-Deposit Exceptions ';

		SELECT 
			@AuditMessage += 'Transaction: ' + CAST(@parmTransactionID AS VARCHAR(10)) 
			+ ', Transaction Sequence: ' + CAST(TxnSequence AS VARCHAR(10))
			+ ' in Batch: ' + CAST(SourceBatchID AS VARCHAR(10)) 
			+ ' for Deposit Date: ' + CONVERT(VARCHAR(20), CONVERT(DATE, CONVERT(VARCHAR(8), @parmDepositDateKey), 112),110) 
			+ ', Bank: ' + CAST(RecHubData.dimClientAccounts.SiteBankID AS VARCHAR(10))	+ ' - ' + RecHubData.dimBanks.BankName
			+ ', Workgroup: ' + CAST(RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + COALESCE(RecHubData.dimClientAccounts.LongName,RecHubData.dimClientAccounts.ShortName)
			+ ', Payment Source: ' + COALESCE(RecHubData.dimBatchSources.LongName,RecHubData.dimBatchSources.ShortName)
			+ ', Payment Type: ' + COALESCE(RecHubData.dimBatchPaymentTypes.LongName,RecHubData.dimBatchPaymentTypes.ShortName)
		FROM
			RecHubData.factTransactionSummary
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
			INNER JOIN RecHubData.dimBanks ON RecHubData.dimBanks.BankKey = RecHubData.factTransactionSummary.BankKey
			INNER JOIN RecHubData.dimBatchSources ON RecHubData.dimBatchSources.BatchSourceKey = RecHubData.factTransactionSummary.BatchSourceKey
			INNER JOIN RecHubData.dimBatchPaymentTypes ON RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey = RecHubData.factTransactionSummary.BatchPaymentTypeKey
		WHERE
			DepositDateKey = @parmDepositDateKey
			AND BatchID = @parmBatchID
			AND TransactionID = @parmTransactionID
			AND IsDeleted = 0;

		EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName	= 'RecHubException.usp_PostDepositTransactionExceptions_Del',
			@parmEventName			= 'Post-Deposit Exceptions',
			@parmEventType			= 'Post-Deposit Exceptions',
			@parmUserID				= @UserID,
			@parmAuditMessage		= @AuditMessage;
	END

	-- All updates are complete
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- Cleanup local transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;

    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH