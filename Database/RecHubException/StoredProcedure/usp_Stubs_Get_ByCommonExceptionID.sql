--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Stubs_Get_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Stubs_Get_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_Stubs_Get_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Stubs_Get_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/29/2014
*
* Purpose: Return Stub Records for Post-Process Exceptions
*
* Modification History
* 04/29/2014 WI 139289 RDS	Created
* 05/14/2014 WI 141678 RDS	Add auditing
* 05/21/2014 WI 139289 KLC	Updated to sort fields by screen order
* 09/10/2014 WI	139289 KLC	Added AccountNumber column
* 10/23/2014 WI	173968 KLC	Updated to filter out deleted stubs
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Note: This "view" was audited already by usp_Transactions_Get_ByCommonExceptionID

	DECLARE @siteBankID INT,
			@siteWorkgroupID INT;
	SELECT	@siteBankID = RecHubException.ExceptionBatches.SiteBankID,
			@siteWorkgroupID = RecHubException.ExceptionBatches.SiteWorkgroupID 
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

	WITH CTE_StubItems AS
	(
		SELECT
			RecHubException.Stubs.BatchSequence,
			RecHubException.Stubs.StubKey,
			RecHubException.Stubs.Amount,
			RecHubException.Stubs.AccountNumber
		FROM
			RecHubException.DecisioningTransactions 
		INNER JOIN RecHubException.Transactions ON
			RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
		INNER JOIN RecHubException.Stubs ON
			RecHubException.Stubs.TransactionKey = RecHubException.Transactions.TransactionKey
			AND RecHubException.Stubs.IsDeleted = 0
		WHERE 
			RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID
	),
	CTE_AllStubFields AS
	(
		SELECT	ROW_NUMBER() OVER ( PARTITION BY RecHubData.dimDataEntryColumns.FldName, RecHubData.dimDataEntryColumns.TableType
									ORDER BY RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey DESC ) AS RowNumber,
				RecHubData.dimDataEntryColumns.DataEntryColumnKey,
				RecHubData.dimDataEntryColumns.FldName,
				RecHubData.dimDataEntryColumns.TableType,
				RecHubData.dimDataEntryColumns.ScreenOrder
		FROM RecHubData.dimClientAccounts
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
			INNER JOIN RecHubData.dimDataEntryColumns
				ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
		WHERE RecHubData.dimClientAccounts.SiteBankID = @siteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @siteWorkgroupID
			AND RecHubData.dimDataEntryColumns.TableType IN (2, 3) -- Stubs, Stubs Data Entry
	),
	CTE_CurrentStubFields AS
	(
		SELECT DataEntryColumnKey, FldName, TableType, ScreenOrder
		FROM CTE_AllStubFields
		WHERE RowNumber = 1
	)
	SELECT CTE_StubItems.BatchSequence,
			CTE_CurrentStubFields.DataEntryColumnKey,
			CTE_CurrentStubFields.ScreenOrder,
			RecHubException.StubDataEntry.Value AS FieldValue
	FROM CTE_StubItems
		LEFT JOIN RecHubException.StubDataEntry ON
			RecHubException.StubDataEntry.StubKey = CTE_StubItems.StubKey
		LEFT JOIN CTE_CurrentStubFields ON
				CTE_CurrentStubFields.FldName = RecHubException.StubDataEntry.FldName
			AND CTE_CurrentStubFields.TableType = 3 -- Stubs Data Entry

	UNION ALL

	SELECT	CTE_StubItems.BatchSequence,
			CTE_CurrentStubFields.DataEntryColumnKey,
			CTE_CurrentStubFields.ScreenOrder,
			CTE_StubItems.Amount
	FROM CTE_StubItems
	INNER JOIN CTE_CurrentStubFields ON
			CTE_CurrentStubFields.FldName = 'Amount' 
		AND CTE_CurrentStubFields.TableType = 2 -- Stubs

	UNION ALL

	SELECT	CTE_StubItems.BatchSequence,
			CTE_CurrentStubFields.DataEntryColumnKey,
			CTE_CurrentStubFields.ScreenOrder,
			CTE_StubItems.AccountNumber
	FROM CTE_StubItems
	INNER JOIN CTE_CurrentStubFields ON
			CTE_CurrentStubFields.FldName = 'AccountNumber' 
		AND CTE_CurrentStubFields.TableType = 2 -- Stubs

	ORDER BY BatchSequence, ScreenOrder;

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH