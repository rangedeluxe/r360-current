--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_BusinessRules_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_BusinessRules_Merge') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_BusinessRules_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_BusinessRules_Merge
(
	@parmEntityID				INT,
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmDataEntrySetupKey		INT,
	@parmBusinessRules			XML,
	@parmUserID					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 05/12/2014
*
* Purpose: Updates the BusinessRules and then it's corresponding support
*			tables
*
* Modification History
* 05/12/2014 WI 141069 KLC	Created
* 10/01/2014 WI 168952 KLC	Added Exception Display Order column to DataEntrySetupFields
******************************************************************************/
DECLARE @prevDeadLineDay				SMALLINT,
		@AuditColumns					RecHubCommon.AuditValueChangeTable,
		@errorDescription				VARCHAR(1024),
		@auditMessage					VARCHAR(1024),
		@auditKey						BIGINT,
		@auditKey2						BIGINT,
		@auditType						VARCHAR(3);


--Define the tables for pulling shredding the xml and tracking changes made to the data
DECLARE @BusinessRules TABLE(TempID INT IDENTITY(1, 1) PRIMARY KEY, DataEntrySetupFieldKey BIGINT, BusinessRuleKey BIGINT, TableType TINYINT, FldName VARCHAR(128),
							BatchSourceKey TINYINT, BatchPaymentTypeKey TINYINT, BatchPaymentSubTypeKey TINYINT, ExceptionDisplayOrder TINYINT,
							MinLength SMALLINT, [MaxLength] SMALLINT, [Required] BIT, UserCanEdit BIT, FormatType TINYINT, Mask VARCHAR(40), RequiredMessage NVARCHAR(120),
							CheckDigitRoutineKey BIGINT, CheckDigitSetup XML, FieldValidationKey BIGINT, FieldValidationSetup XML);
DECLARE @DeleteBusinessRules TABLE(BusinessRuleKey BIGINT);
DECLARE @BusinessRuleChanges TABLE(TempID INT, BusinessRuleKey BIGINT, AuditType VARCHAR(3),
									NewMinLength SMALLINT, NewMaxLength SMALLINT, NewRequired BIT, NewUserCanEdit BIT, NewFormatType TINYINT, NewMask VARCHAR(40), NewRequiredMessage NVARCHAR(120),
									NewCheckDigitRoutineKey BIGINT, NewFieldValidationKey BIGINT,
									OldMinLength SMALLINT, OldMaxLength SMALLINT, OldRequired BIT, OldUserCanEdit BIT, OldFormatType TINYINT, OldMask VARCHAR(40), OldRequiredMessage NVARCHAR(120),
									OldCheckDigitRoutineKey BIGINT, OldFieldValidationKey BIGINT);

DECLARE @DataEntrySetupFieldChanges TABLE(DataEntrySetupFieldKey BIGINT, AuditType VARCHAR(3), BusinessRuleKey BIGINT, TableType TINYINT, FldName VARCHAR(128),
								BatchSourceKey TINYINT, BatchPaymentTypeKey TINYINT, BatchPaymentSubTypeKey TINYINT,
								NewExceptionDisplayOrder TINYINT, OldExceptionDisplayOrder TINYINT);

DECLARE @CheckDigitRoutines TABLE(TempID INT IDENTITY(1, 1) PRIMARY KEY, BusinessRuleTempID INT, CheckDigitRoutineKey INT, Method INT, Offset SMALLINT, Modulus INT, Compliment INT, WeightPatternDirection INT, WeightPatternValue NVARCHAR(40),
									IgnoreSpaces BIT, Remainder10Replacement INT, Remainder11Replacement INT, FailureMessage NVARCHAR(120), ReplacementValues XML);
DECLARE @DeleteCheckDigitRoutines TABLE(CheckDigitRoutineKey BIGINT);
DECLARE @CheckDigitRoutineChanges TABLE(TempID INT, BusinessRuleTempID INT, CheckDigitRoutineKey INT, AuditType VARCHAR(3),
										NewMethod INT, NewOffset SMALLINT, NewModulus INT, NewCompliment INT, NewWeightPatternDirection INT, NewWeightPatternValue NVARCHAR(40),
										NewIgnoreSpaces BIT, NewRemainder10Replacement INT, NewRemainder11Replacement INT, NewFailureMessage NVARCHAR(120),
										OldMethod INT, OldOffset SMALLINT, OldModulus INT, OldCompliment INT, OldWeightPatternDirection INT, OldWeightPatternValue NVARCHAR(40),
										OldIgnoreSpaces BIT, OldRemainder10Replacement INT, OldRemainder11Replacement INT, OldFailureMessage NVARCHAR(120));

DECLARE @CheckDigitReplacementValues TABLE(CheckDigitRoutineTempID BIGINT, OriginalValue NCHAR(1), ReplacementValue TINYINT);
DECLARE @CheckDigitReplacementValueChanges TABLE(CheckDigitReplacementValueKey BIGINT, AuditType VARCHAR(3), CheckDigitRoutineKey BIGINT,
												OriginalValue NCHAR(1), NewReplacementValue TINYINT, OldReplacementValue TINYINT);

DECLARE @FieldValidations TABLE(TempID INT IDENTITY(1, 1) PRIMARY KEY, BusinessRuleTempID INT, FieldValidationKey BIGINT, FieldValidationTypeKey TINYINT, FailureMessage NVARCHAR(120), ValidationValues XML);
DECLARE @DeleteFieldValidations TABLE(FieldValidationKey BIGINT);
DECLARE @FieldValidationChanges TABLE(TempID INT, BusinessRuleTempID INT, FieldValidationKey BIGINT, AuditType VARCHAR(3),
										NewValidationTypeKey TINYINT, NewFailureMessage NVARCHAR(120),
										OldValidationTypeKey TINYINT, OldFailureMessage NVARCHAR(120));

DECLARE @FieldValidationValues TABLE(FieldValidationTempID INT, Value SQL_VARIANT);
DECLARE @FieldValidationValueChanges TABLE(FieldValidationValueKey BIGINT, AuditType VARCHAR(3), FieldValidationKey BIGINT, Value SQL_VARIANT);


SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to update business rules in RecHubException.BusinessRules, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END
	
	IF NOT EXISTS(SELECT * FROM RecHubException.DataEntrySetups WHERE DataEntrySetupKey = @parmDataEntrySetupKey)
	BEGIN
		SET @errorDescription = 'Unable to update business rules in RecHubException.BusinessRules, the RecHubException.DataEntrySetups record does not exist for key ' + CAST(@parmDataEntrySetupKey AS VARCHAR(20));
		RAISERROR(@errorDescription, 16, 2);
	END

	DECLARE @curTime datetime = GETDATE();

	--Fill our temp business rules table with the rules we are adding/updating
	INSERT INTO @BusinessRules(DataEntrySetupFieldKey, BusinessRuleKey, TableType, FldName, BatchSourceKey, BatchPaymentTypeKey, BatchPaymentSubTypeKey, ExceptionDisplayOrder,
								MinLength, [MaxLength], [Required], UserCanEdit, FormatType, Mask, RequiredMessage,
								CheckDigitRoutineKey, CheckDigitSetup, FieldValidationKey, FieldValidationSetup)
	SELECT	RecHubException.DataEntrySetupFields.DataEntrySetupFieldKey,
			RecHubException.BusinessRules.BusinessRuleKey,
			BRs.BR.value('@tabletype', 'TINYINT'),
			BRs.BR.value('@fldname', 'VARCHAR(128)'),
			BRs.BR.value('@batchsourcekey', 'TINYINT'),
			BRs.BR.value('@batchpaymenttypekey', 'TINYINT'),
			BRs.BR.value('@batchpaymentsubtypekey', 'TINYINT'),
			BRs.BR.value('@exceptiondisplayorder', 'TINYINT'),
			BRs.BR.value('@minlength', 'SMALLINT'),
			BRs.BR.value('@maxlength', 'SMALLINT'), 
			BRs.BR.value('@required', 'BIT'),
			BRs.BR.value('@usercanedit', 'BIT'),
			BRs.BR.value('@formattype', 'TINYINT'),
			BRs.BR.value('@mask', 'VARCHAR(40)'),
			BRs.BR.value('@requiredmessage', 'NVARCHAR(120)'),
			CASE WHEN BRs.BR.exist('cd') = 1 THEN RecHubException.BusinessRules.CheckDigitRoutineKey ELSE NULL END,
			CASE WHEN BRs.BR.exist('cd') = 1 THEN BRs.BR.query('cd') ELSE NULL END,
			CASE WHEN BRs.BR.exist('fv') = 1 THEN RecHubException.BusinessRules.FieldValidationKey ELSE NULL END,
			CASE WHEN BRs.BR.exist('fv') = 1 THEN BRs.BR.query('fv') ELSE NULL END
	FROM @parmBusinessRules.nodes('/brs/br') BRs(BR)
		LEFT JOIN RecHubException.DataEntrySetupFields
			ON RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
				AND BRs.BR.value('@tabletype', 'TINYINT') = RecHubException.DataEntrySetupFields.TableType
				AND BRs.BR.value('@fldname', 'VARCHAR(128)') = RecHubException.DataEntrySetupFields.FldName
				AND COALESCE(BRs.BR.value('@batchsourcekey', 'TINYINT'), -1) = COALESCE(RecHubException.DataEntrySetupFields.BatchSourceKey, -1)
				AND COALESCE(BRs.BR.value('@batchpaymenttypekey', 'TINYINT'), -1) = COALESCE(RecHubException.DataEntrySetupFields.BatchPaymentTypeKey, -1)
				AND COALESCE(BRs.BR.value('@batchpaymentsubtypekey', 'TINYINT'), -1) = COALESCE(RecHubException.DataEntrySetupFields.BatchPaymentSubTypeKey, -1)
		LEFT JOIN RecHubException.BusinessRules
			ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey;

	--Track any business rules that exist and need to be removed as we'll need to remove their check digit routines and field validations
	INSERT INTO @DeleteBusinessRules(BusinessRuleKey)
	SELECT RecHubException.DataEntrySetupFields.BusinessRuleKey
	FROM RecHubException.DataEntrySetupFields
		JOIN RecHubException.BusinessRules
			ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey
		LEFT JOIN @BusinessRules BR
			ON RecHubException.DataEntrySetupFields.DataEntrySetupFieldKey = BR.DataEntrySetupFieldKey
	WHERE RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
		AND BR.DataEntrySetupFieldKey IS NULL;

	--Now parse the xml for check digit routines
	INSERT INTO @CheckDigitRoutines(BusinessRuleTempID, CheckDigitRoutineKey, Method, Offset, Modulus, Compliment, WeightPatternDirection, WeightPatternValue,
									IgnoreSpaces, Remainder10Replacement, Remainder11Replacement, FailureMessage, ReplacementValues)
	SELECT	TempID,
			CheckDigitRoutineKey,
			CheckDigitSetup.value('(/cd/@method)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@offset)[1]', 'SMALLINT'),
			CheckDigitSetup.value('(/cd/@modulus)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@compliment)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@weightsdirection)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@weights)[1]', 'NVARCHAR(40)'),
			CheckDigitSetup.value('(/cd/@ignorespaces)[1]', 'BIT'),
			CheckDigitSetup.value('(/cd/@rem10replacement)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@rem11replacement)[1]', 'INT'),
			CheckDigitSetup.value('(/cd/@failuremessage)[1]', 'NVARCHAR(120)'),
			CASE WHEN CheckDigitSetup.exist('(/cd/rvs)') = 1 THEN CheckDigitSetup.query('(/cd/rvs)[1]') ELSE NULL END
	FROM @BusinessRules
	WHERE CheckDigitSetup IS NOT NULL;

	--Track check digit routines that should be deleted due to business rules going away
	INSERT INTO @DeleteCheckDigitRoutines(CheckDigitRoutineKey)
	SELECT RecHubException.BusinessRules.CheckDigitRoutineKey
	FROM RecHubException.DataEntrySetupFields
		JOIN RecHubException.BusinessRules
			ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey
		LEFT JOIN @BusinessRules BR
			ON RecHubException.DataEntrySetupFields.DataEntrySetupFieldKey = BR.DataEntrySetupFieldKey
	WHERE RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
		AND BR.CheckDigitRoutineKey IS NULL;
		

	--Parse the check digit replacement values
	INSERT INTO @CheckDigitReplacementValues(CheckDigitRoutineTempID, OriginalValue, ReplacementValue)
	SELECT	TempID,
			RVS.RV.value('@orig', 'NCHAR(1)'),
			RVS.RV.value('@repl', 'INT')
	FROM @CheckDigitRoutines
	CROSS APPLY ReplacementValues.nodes('/rvs/rv') RVS(RV)
	WHERE ReplacementValues IS NOT NULL;


	--Now parse the xml for field validations
	INSERT INTO @FieldValidations(BusinessRuleTempID, FieldValidationKey, FieldValidationTypeKey, FailureMessage, ValidationValues)
	SELECT	TempID,
			FieldValidationKey,
			FieldValidationSetup.value('(/fv/@validationtypekey)[1]', 'TINYINT'),
			FieldValidationSetup.value('(/fv/@failuremessage)[1]', 'NVARCHAR(120)'),
			CASE WHEN FieldValidationSetup.exist('(/fv/vv)') = 1 THEN FieldValidationSetup.query('(/fv/vv)[1]') ELSE NULL END
	FROM @BusinessRules
	WHERE FieldValidationSetup IS NOT NULL;

	--Track field validations that should be deleted due to business rules going away
	INSERT INTO @DeleteFieldValidations(FieldValidationKey)
	SELECT RecHubException.BusinessRules.FieldValidationKey
	FROM RecHubException.DataEntrySetupFields
		JOIN RecHubException.BusinessRules
			ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey
		LEFT JOIN @BusinessRules BR
			ON RecHubException.DataEntrySetupFields.DataEntrySetupFieldKey = BR.DataEntrySetupFieldKey
	WHERE RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
		AND BR.FieldValidationKey IS NULL;

	--Parse the field validation values
	INSERT INTO @FieldValidationValues(FieldValidationTempID, Value)
	SELECT	TempID,
			RVS.RV.value('@val', 'VARCHAR(255)')--max field length based on tinyint FldLength
	FROM @FieldValidations
	CROSS APPLY ValidationValues.nodes('/vv/v') RVS(RV)
	WHERE ValidationValues IS NOT NULL;


	--Merge in the insert/updates for the check digit routines
	MERGE RecHubException.CheckDigitRoutines
	USING
	(
		SELECT	TempID, BusinessRuleTempID, CheckDigitRoutineKey, Method, Offset, Modulus, Compliment, WeightPatternDirection, WeightPatternValue, IgnoreSpaces, Remainder10Replacement, Remainder11Replacement, FailureMessage
		FROM @CheckDigitRoutines
	) S
	ON RecHubException.CheckDigitRoutines.CheckDigitRoutineKey = S.CheckDigitRoutineKey
	WHEN MATCHED AND (RecHubException.CheckDigitRoutines.Method <> S.Method OR RecHubException.CheckDigitRoutines.Offset <> S.Offset OR RecHubException.CheckDigitRoutines.Modulus <> S.Modulus
					OR RecHubException.CheckDigitRoutines.Compliment <> S.Compliment
					OR RecHubException.CheckDigitRoutines.WeightPatternDirection <> S.WeightPatternDirection OR RecHubException.CheckDigitRoutines.WeightPatternValue <> s.WeightPatternValue
					OR ISNULL(RecHubException.CheckDigitRoutines.IgnoreSpaces, 0) <> ISNULL(S.IgnoreSpaces, 0) OR RecHubException.CheckDigitRoutines.Remainder10Replacement <> S.Remainder10Replacement
					OR RecHubException.CheckDigitRoutines.Remainder11Replacement <> S.Remainder11Replacement OR CAST(RecHubException.CheckDigitRoutines.FailureMessage AS VARBINARY(120)) <> CAST(S.FailureMessage AS VARBINARY(120))) THEN
		UPDATE SET Method = S.Method, Offset = S.Offset, Modulus = S.Modulus, Compliment = S.Compliment, WeightPatternDirection = S.WeightPatternDirection, WeightPatternValue = s.WeightPatternValue,
				IgnoreSpaces = S.IgnoreSpaces, Remainder10Replacement = S.Remainder10Replacement, Remainder11Replacement = S.Remainder11Replacement, FailureMessage = S.FailureMessage,
				ModificationDate = @curTime, ModifiedBy = @parmUserID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (Method, Offset, Modulus, Compliment, WeightPatternDirection, WeightPatternValue, IgnoreSpaces, Remainder10Replacement, Remainder11Replacement, FailureMessage, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES (S.Method, S.Offset, S.Modulus, S.Compliment, S.WeightPatternDirection, S.WeightPatternValue, S.IgnoreSpaces, S.Remainder10Replacement, S.Remainder11Replacement, S.FailureMessage, @curTime, @curTime, @parmUserID, @parmUserID)
	OUTPUT	S.TempID, S.BusinessRuleTempID, INSERTED.CheckDigitRoutineKey, CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END,
		INSERTED.Method, INSERTED.Offset, INSERTED.Modulus, INSERTED.Compliment, INSERTED.WeightPatternDirection, INSERTED.WeightPatternValue,
		INSERTED.IgnoreSpaces BIT, INSERTED.Remainder10Replacement, INSERTED.Remainder11Replacement, INSERTED.FailureMessage,
		DELETED.Method, DELETED.Offset, DELETED.Modulus, DELETED.Compliment, DELETED.WeightPatternDirection, DELETED.WeightPatternValue,
		DELETED.IgnoreSpaces BIT, DELETED.Remainder10Replacement, DELETED.Remainder11Replacement, DELETED.FailureMessage
	INTO @CheckDigitRoutineChanges;

	--Merge in the check digit replacement values
	MERGE RecHubException.CheckDigitReplacementValues
	USING
	(
		SELECT	COALESCE(CDR.CheckDigitRoutineKey, CDRChanges.CheckDigitRoutineKey) AS CheckDigitRoutineKey, OriginalValue, ReplacementValue
		FROM @CheckDigitRoutines CDR
			JOIN @CheckDigitReplacementValues CDReplacements
				ON CDR.TempID = CDReplacements.CheckDigitRoutineTempID
			LEFT JOIN @CheckDigitRoutineChanges CDRChanges
				ON CDR.TempID = CDRChanges.TempID
	) S
	ON RecHubException.CheckDigitReplacementValues.CheckDigitRoutineKey = S.CheckDigitRoutineKey
		AND CAST(RecHubException.CheckDigitReplacementValues.OriginalValue AS BINARY(1)) = CAST(S.OriginalValue AS BINARY(1))
	WHEN MATCHED AND coalesce(RecHubException.CheckDigitReplacementValues.ReplacementValue, -1) <> coalesce(S.ReplacementValue, -1) THEN
		UPDATE SET ReplacementValue = S.ReplacementValue, ModificationDate = @curTime, ModifiedBy = @parmUserID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (CheckDigitRoutineKey, OriginalValue, ReplacementValue, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES (S.CheckDigitRoutineKey, S.OriginalValue, S.ReplacementValue, @curTime, @curTime, @parmUserID, @parmUserID)
	WHEN NOT MATCHED BY SOURCE AND RecHubException.CheckDigitReplacementValues.CheckDigitRoutineKey IN (SELECT CheckDigitRoutineKey FROM @CheckDigitRoutines)
		THEN DELETE
	OUTPUT	COALESCE(DELETED.CheckDigitReplacementValueKey, INSERTED.CheckDigitReplacementValueKey),
			CASE WHEN $action = 'DELETE' THEN 'DEL' ELSE CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END END,
			COALESCE(DELETED.CheckDigitRoutineKey, INSERTED.CheckDigitRoutineKey),
			COALESCE(DELETED.OriginalValue, INSERTED.OriginalValue),
			INSERTED.ReplacementValue,
			DELETED.ReplacementValue
	INTO @CheckDigitReplacementValueChanges;


	--Merge in the insert/updates for the field validations
	MERGE RecHubException.FieldValidations
	USING
	(
		SELECT	TempID, BusinessRuleTempID, FieldValidationKey, FieldValidationTypeKey, FailureMessage
		FROM @FieldValidations
	) S
	ON RecHubException.FieldValidations.FieldValidationKey = S.FieldValidationKey
	WHEN MATCHED AND (RecHubException.FieldValidations.FieldValidationTypeKey <> S.FieldValidationTypeKey
					OR CAST(RecHubException.FieldValidations.FailureMessage AS VARBINARY(120)) <> CAST(S.FailureMessage AS VARBINARY(120))) THEN
		UPDATE SET FieldValidationTypeKey = S.FieldValidationTypeKey, FailureMessage = S.FailureMessage,
				ModificationDate = @curTime, ModifiedBy = @parmUserID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (FieldValidationTypeKey, FailureMessage, FieldValidationName, LookupRequired, FieldValidationSortMethodKey, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES (S.FieldValidationTypeKey, S.FailureMessage, '',
				0, 5,/*Default to lookup not required and no sort until lookup tables supported*/
				@curTime, @curTime, @parmUserID, @parmUserID)
	OUTPUT S.TempID, S.BusinessRuleTempID, INSERTED.FieldValidationKey, CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END,
		INSERTED.FieldValidationTypeKey, INSERTED.FailureMessage,
		DELETED.FieldValidationTypeKey, DELETED.FailureMessage
	INTO @FieldValidationChanges;

	--We'll use two steps for the FieldValidationValues, insert new then delete removed values
	--Insert new FieldValidationValues
	;WITH CTE_FieldValidationValues AS
	(
		SELECT COALESCE(FV.FieldValidationKey, FVChanges.FieldValidationKey) AS FieldValidationKey, Value
		FROM @FieldValidations FV
			JOIN @FieldValidationValues FVV
				ON FV.TempID = FVV.FieldValidationTempID
			LEFT JOIN @FieldValidationChanges FVChanges
				ON FV.TempID = FVChanges.TempID
	)
	INSERT INTO RecHubException.FieldValidationValues(FieldValidationKey, Value)
	OUTPUT INSERTED.FieldValidationValueKey, 'INS', INSERTED.FieldValidationKey, INSERTED.Value
	INTO @FieldValidationValueChanges
	SELECT FVV.FieldValidationKey, FVV.Value
	FROM CTE_FieldValidationValues FVV
		LEFT JOIN RecHubException.FieldValidationValues
			ON RecHubException.FieldValidationValues.FieldValidationKey = FVV.FieldValidationKey
				AND CAST(RecHubException.FieldValidationValues.Value AS VARBINARY(255)) = CAST(FVV.Value AS VARBINARY(255))
	WHERE RecHubException.FieldValidationValues.FieldValidationKey IS NULL;

	--Delete removed values
	;WITH CTE_FieldValidations AS
	(
		SELECT COALESCE(FV.FieldValidationKey, FVChanges.FieldValidationKey) AS FieldValidationKey, FV.TempID
		FROM @FieldValidations FV
			LEFT JOIN @FieldValidationChanges FVChanges
				ON FV.TempID = FVChanges.TempID
	)
	DELETE RecHubException.FieldValidationValues
	OUTPUT DELETED.FieldValidationValueKey, 'DEL', DELETED.FieldValidationKey, DELETED.Value
	INTO @FieldValidationValueChanges
	FROM RecHubException.FieldValidationValues
		JOIN CTE_FieldValidations FV
			ON RecHubException.FieldValidationValues.FieldValidationKey = FV.FieldValidationKey
		LEFT JOIN @FieldValidationValues FVV
				ON FV.TempID = FVV.FieldValidationTempID
					AND CAST(RecHubException.FieldValidationValues.Value AS VARBINARY(255)) = CAST(FVV.Value AS VARBINARY(255))
	WHERE FVV.FieldValidationTempID IS NULL;


	--Insert/Update the business rules that have been changed or added
	MERGE RecHubException.BusinessRules
	USING
	(
		SELECT	BR.TempID, DataEntrySetupFieldKey, BusinessRuleKey, TableType, FldName, BatchSourceKey, BatchPaymentTypeKey, BatchPaymentSubTypeKey,
				MinLength, [MaxLength], [Required], UserCanEdit, FormatType, Mask, RequiredMessage,
				CASE WHEN CDRChanges.AuditType = 'INS' THEN CDRChanges.CheckDigitRoutineKey
					 WHEN CDRChanges.AuditType = 'DEL' THEN NULL
					 ELSE BR.CheckDigitRoutineKey END AS CheckDigitRoutineKey,
				CASE WHEN FVChanges.AuditType = 'INS' THEN FVChanges.FieldValidationKey
					 WHEN FVChanges.AuditType = 'DEL' THEN NULL
					 ELSE BR.FieldValidationKey END AS FieldValidationKey
		FROM @BusinessRules BR
			LEFT JOIN @CheckDigitRoutineChanges CDRChanges
				ON BR.TempID = CDRChanges.BusinessRuleTempID
			LEFT JOIN @FieldValidationChanges FVChanges
				ON BR.TempID = FVChanges.BusinessRuleTempID
	) S
	ON RecHubException.BusinessRules.BusinessRuleKey = S.BusinessRuleKey
	WHEN MATCHED AND (RecHubException.BusinessRules.MinLength <> S.MinLength
					OR RecHubException.BusinessRules.[MaxLength] <> S.[MaxLength]
					OR RecHubException.BusinessRules.[Required] <> S.[Required]
					OR RecHubException.BusinessRules.UserCanEdit <> S.UserCanEdit
					OR RecHubException.BusinessRules.FormatType <> S.FormatType
					OR CAST(COALESCE(RecHubException.BusinessRules.Mask, '') AS VARBINARY(80)) <> CAST(COALESCE(S.Mask, '') AS VARBINARY(80))
					OR CAST(COALESCE(RecHubException.BusinessRules.RequiredMessage, '') AS VARBINARY(120)) <> CAST(COALESCE(S.RequiredMessage, '') AS VARBINARY(120))
					OR COALESCE(RecHubException.BusinessRules.CheckDigitRoutineKey, -1) <> COALESCE(S.CheckDigitRoutineKey, -1)
					OR COALESCE(RecHubException.BusinessRules.FieldValidationKey, -1) <> COALESCE(S.FieldValidationKey, -1)
					) THEN
		UPDATE SET MinLength = S.MinLength, [MaxLength] = S.[MaxLength], [Required] = S.[Required], UserCanEdit = S.UserCanEdit,
				FormatType = S.FormatType, Mask = S.Mask, RequiredMessage = S.RequiredMessage,
				CheckDigitRoutineKey = S.CheckDigitRoutineKey, FieldValidationKey = S.FieldValidationKey,
				ModificationDate = @curTime, ModifiedBy = @parmUserID
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (MinLength, [MaxLength], [Required], UserCanEdit, FormatType, Mask, RequiredMessage, CheckDigitRoutineKey, FieldValidationKey, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES (S.MinLength, S.[MaxLength], S.[Required], S.UserCanEdit, S.FormatType, S.Mask, S.RequiredMessage, S.CheckDigitRoutineKey, S.FieldValidationKey,
				@curTime, @curTime, @parmUserID, @parmUserID)
	OUTPUT S.TempID, COALESCE(INSERTED.BusinessRuleKey, DELETED.BusinessRuleKey), CASE WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END,
		INSERTED.MinLength, INSERTED.[MaxLength], INSERTED.[Required], INSERTED.UserCanEdit, INSERTED.FormatType, INSERTED.Mask, INSERTED.RequiredMessage,
		INSERTED.CheckDigitRoutineKey, INSERTED.FieldValidationKey,
		DELETED.MinLength, DELETED.[MaxLength], DELETED.[Required], DELETED.UserCanEdit, DELETED.FormatType, DELETED.Mask, DELETED.RequiredMessage,
		DELETED.CheckDigitRoutineKey, DELETED.FieldValidationKey
	INTO @BusinessRuleChanges;


	--Merge the DataEntrySetupFields
	MERGE RecHubException.DataEntrySetupFields
	USING
	(
		SELECT	DataEntrySetupFieldKey, COALESCE(BR.BusinessRuleKey, BRChanges.BusinessRuleKey) BusinessRuleKey,
				TableType, FldName, BatchSourceKey, BatchPaymentTypeKey, BatchPaymentSubTypeKey, ExceptionDisplayOrder
		FROM @BusinessRules BR
			LEFT JOIN @BusinessRuleChanges BRChanges
				ON BR.TempID = BRChanges.TempID
	) S
	ON RecHubException.DataEntrySetupFields.DataEntrySetupFieldKey = S.DataEntrySetupFieldKey
	WHEN MATCHED AND ISNULL(CAST(RecHubException.DataEntrySetupFields.ExceptionDisplayOrder AS SMALLINT), -1) <> ISNULL(CAST(S.ExceptionDisplayOrder AS SMALLINT), -1) THEN
		UPDATE SET ExceptionDisplayOrder = S.ExceptionDisplayOrder
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (DataEntrySetupKey, BatchSourceKey, BatchPaymentTypeKey, BatchPaymentSubTypeKey, BusinessRuleKey, TableType, FldName, ExceptionDisplayOrder, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES(@parmDataEntrySetupKey, S.BatchSourceKey, S.BatchPaymentTypeKey, S.BatchPaymentSubTypeKey, S.BusinessRuleKey, S.TableType, S.FldName, S.ExceptionDisplayOrder,
				@curTime, @curTime, @parmUserID, @parmUserID)
	WHEN NOT MATCHED BY SOURCE AND RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
		THEN DELETE
	OUTPUT 
		CASE WHEN $action = 'DELETE' THEN DELETED.DataEntrySetupFieldKey ELSE INSERTED.DataEntrySetupFieldKey END,
		CASE WHEN $action = 'DELETE' THEN 'DEL' WHEN $action = 'UPDATE' THEN 'UPD' ELSE 'INS' END,
		CASE WHEN $action = 'DELETE' THEN DELETED.BusinessRuleKey ELSE INSERTED.BusinessRuleKey END,
		CASE WHEN $action = 'DELETE' THEN DELETED.TableType ELSE INSERTED.TableType END,
		CASE WHEN $action = 'DELETE' THEN DELETED.FldName ELSE INSERTED.FldName END,
		CASE WHEN $action = 'DELETE' THEN DELETED.BatchSourceKey ELSE INSERTED.BatchSourceKey END,
		CASE WHEN $action = 'DELETE' THEN DELETED.BatchPaymentTypeKey ELSE INSERTED.BatchPaymentTypeKey END,
		CASE WHEN $action = 'DELETE' THEN DELETED.BatchPaymentSubTypeKey ELSE INSERTED.BatchPaymentSubTypeKey END,
		DELETED.ExceptionDisplayOrder,
		INSERTED.ExceptionDisplayOrder
	INTO @DataEntrySetupFieldChanges;

	--Delete the BusinessRules that were removed
	DELETE RecHubException.BusinessRules
	OUTPUT NULL, DELETED.BusinessRuleKey, 'DEL',
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		DELETED.MinLength, DELETED.[MaxLength], DELETED.[Required], DELETED.UserCanEdit, DELETED.FormatType, DELETED.Mask, DELETED.RequiredMessage,
		DELETED.CheckDigitRoutineKey, DELETED.FieldValidationKey
	INTO @BusinessRuleChanges
	FROM @DeleteBusinessRules DBR
		JOIN RecHubException.BusinessRules
			ON DBR.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey;

	--Now that the BusinessRule is deleted, we can delete the check digit routines and field validations
	
	--Delete any check digit replacement values for routines that are going away
	DELETE RecHubException.CheckDigitReplacementValues
	OUTPUT DELETED.CheckDigitReplacementValueKey, 'DEL',
		DELETED.CheckDigitRoutineKey, DELETED.OriginalValue,
		NULL, DELETED.ReplacementValue
	INTO @CheckDigitReplacementValueChanges
	FROM @DeleteCheckDigitRoutines DCDR
		JOIN RecHubException.CheckDigitReplacementValues
			ON DCDR.CheckDigitRoutineKey = RecHubException.CheckDigitReplacementValues.CheckDigitRoutineKey;

	--Delete any check digit routines that are no longer assigned
	DELETE RecHubException.CheckDigitRoutines
	OUTPUT NULL, NULL, DELETED.CheckDigitRoutineKey, 'DEL',
		NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,
		DELETED.Method, DELETED.Offset, DELETED.Modulus, DELETED.Compliment, DELETED.WeightPatternDirection, DELETED.WeightPatternValue,
		DELETED.IgnoreSpaces BIT, DELETED.Remainder10Replacement, DELETED.Remainder11Replacement, DELETED.FailureMessage
	INTO @CheckDigitRoutineChanges
	FROM @DeleteCheckDigitRoutines DCDR
		JOIN RecHubException.CheckDigitRoutines
			ON DCDR.CheckDigitRoutineKey = RecHubException.CheckDigitRoutines.CheckDigitRoutineKey;

	--Delete any field validation values for field validations that are going away
	DELETE RecHubException.FieldValidationValues
	OUTPUT DELETED.FieldValidationValueKey, 'DEL', DELETED.FieldValidationKey, DELETED.Value
	INTO @FieldValidationValueChanges
	FROM @DeleteFieldValidations DFV
		JOIN RecHubException.FieldValidationValues
			ON DFV.FieldValidationKey = RecHubException.FieldValidationValues.FieldValidationKey;

	--Delete any field validations that are no longer assigned
	DELETE RecHubException.FieldValidations
	OUTPUT NULL, NULL, DELETED.FieldValidationKey, 'DEL',
		NULL, NULL, DELETED.FieldValidationTypeKey, DELETED.FailureMessage
	INTO @FieldValidationChanges
	FROM @DeleteFieldValidations DFV
		JOIN RecHubException.FieldValidations
			ON DFV.FieldValidationKey = RecHubException.FieldValidations.FieldValidationKey;


	--Perform Auditing
	DECLARE @auditValidationValue			VARCHAR(255),--TODO: (For FieldValidationValues) Update to SQL_VARIANT at some point?
			@auditOldValidationTypeKey		TINYINT,		@auditNewValidationTypeKey		TINYINT,
			@auditOldFailureMessage			NVARCHAR(120),	@auditNewFailureMessage			NVARCHAR(120),
			@auditOriginalValue				NCHAR(1),
			@auditOldReplacementValue		TINYINT,		@auditNewReplacementValue		TINYINT,
			@auditOldMethod					INT,			@auditNewMethod					INT,
			@auditOldOffset					SMALLINT,		@auditNewOffset					SMALLINT,
			@auditOldModulus				INT,			@auditNewModulus				INT,
			@auditOldCompliment				INT,			@auditNewCompliment				INT,
			@auditOldWeightPatternDirection	INT,			@auditNewWeightPatternDirection	INT,
			@auditOldWeightPatternValue		NVARCHAR(40),	@auditNewWeightPatternValue		NVARCHAR(40),
			@auditOldIgnoreSpaces			BIT,			@auditNewIgnoreSpaces			BIT,
			@auditOldRemainder10Replacement	INT,			@auditNewRemainder10Replacement	INT,
			@auditOldRemainder11Replacement	INT,			@auditNewRemainder11Replacement	INT,
			@auditOldMinLength				SMALLINT,		@auditNewMinLength				SMALLINT,
			@auditOldMaxLength				SMALLINT,		@auditNewMaxLength				SMALLINT,
			@auditOldRequired				BIT,			@auditNewRequired				BIT,
			@auditOldUserCanEdit			BIT,			@auditNewUserCanEdit			BIT,
			@auditOldCheckDigitRoutineKey	BIGINT,			@auditNewCheckDigitRoutineKey	BIGINT,
			@auditOldFieldValidationKey		BIGINT,			@auditNewFieldValidationKey		BIGINT,
			@auditOldFormatType				TINYINT,		@auditNewFormatType				TINYINT,
			@auditOldMask					VARCHAR(80),	@auditNewMask					VARCHAR(80),
			@auditOldRequiredMessage		NVARCHAR(120),	@auditNewRequiredMessage		NVARCHAR(120),
			@auditTableType					TINYINT,
			@auditFldName					VARCHAR(128),
			@auditBatchSourceKey			TINYINT,
			@auditBatchPaymentTypeKey		TINYINT,
			@auditBatchPaymentSubTypeKey	TINYINT,
			@auditOldExceptionDisplayOrder	TINYINT,		@auditNewExceptionDisplayOrder	TINYINT;



	--Audit insert of FieldValidationValues (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey					= FieldValidationValueKey,
					@auditType					= AuditType,
					@auditKey2					= FieldValidationKey,
					@auditValidationValue		= CAST(Value AS VARCHAR(255))
	FROM @FieldValidationValueChanges;

	WHILE @auditKey IS NOT NULL
	BEGIN
		SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' FieldValidationValues for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
			+ ' FieldValidationKey: ' + CAST(@auditKey2 AS VARCHAR(20))
			+ ' FieldValidationValueKey: ' + CAST(@auditKey AS VARCHAR(20))
			+ ': Value = ' + @auditValidationValue
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_INS
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
			@parmSchemaName			= 'RecHubException',
			@parmTableName			= 'FieldValidationValues',
			@parmColumnName			= 'FieldValidationValueKey',
			@parmAuditValue			= @auditKey,
			@parmAuditType			= @auditType,
			@parmAuditMessage		= @auditMessage;

		DELETE FROM @FieldValidationValueChanges WHERE FieldValidationValueKey = @auditKey;

		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey					= FieldValidationValueKey,
						@auditType						= AuditType,
						@auditKey2					= FieldValidationKey,
						@auditValidationValue		= CAST(Value AS VARCHAR(255))
		FROM @FieldValidationValueChanges;
	END



	--Audit changes of FieldValidations (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey					= FieldValidationKey,
					@auditType					= AuditType,
					@auditOldValidationTypeKey	= OldValidationTypeKey,
					@auditNewValidationTypeKey	= NewValidationTypeKey,
					@auditOldFailureMessage		= OldFailureMessage,
					@auditNewFailureMessage		= NewFailureMessage
	FROM @FieldValidationChanges;
	
	WHILE @auditKey IS NOT NULL
	BEGIN
		IF @auditType = 'UPD'
		BEGIN
			DELETE FROM @AuditColumns;

			IF @auditOldValidationTypeKey <> @auditNewValidationTypeKey
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ValidationTypeKey', CAST(@auditOldValidationTypeKey AS VARCHAR(3)), CAST(@auditNewValidationTypeKey AS VARCHAR(3)));
			IF CAST(COALESCE(@auditOldFailureMessage, '') AS VARBINARY(120)) <> CAST(COALESCE(@auditNewFailureMessage, '') AS VARBINARY(120))
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FailureMessage', COALESCE(@auditOldFailureMessage, ''), COALESCE(@auditNewFailureMessage, ''));

			-- Audit the RecHubException.FieldValidations update
			SET @auditMessage = 'Modified FieldValidations for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' FieldValidationKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'FieldValidations',
				@parmColumnName			= 'FieldValidationKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
		ELSE
		BEGIN
			SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' FieldValidations for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' FieldValidationKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': ValidationTypeKey = ' + CAST(COALESCE(@auditNewValidationTypeKey, @auditOldValidationTypeKey) AS VARCHAR(3))
				+ ' FailureMessage = ' + COALESCE(@auditNewFailureMessage, @auditOldFailureMessage, '')
				+ '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_INS
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'FieldValidations',
				@parmColumnName			= 'FieldValidationKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditMessage		= @auditMessage;
		END

		DELETE FROM @FieldValidationChanges WHERE FieldValidationKey = @auditKey;

		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey					= FieldValidationKey,
						@auditType					= AuditType,
						@auditOldValidationTypeKey	= OldValidationTypeKey,
						@auditNewValidationTypeKey	= NewValidationTypeKey,
						@auditOldFailureMessage		= OldFailureMessage,
						@auditNewFailureMessage		= NewFailureMessage
		FROM @FieldValidationChanges;
	END



	--Audit changes of CheckDigitReplacementValues (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey					= CheckDigitReplacementValueKey,
					@auditType					= AuditType,
					@auditKey2					= CheckDigitRoutineKey,
					@auditOriginalValue			= OriginalValue,
					@auditOldReplacementValue	= OldReplacementValue,
					@auditNewReplacementValue	= NewReplacementValue
	FROM @CheckDigitReplacementValueChanges;
	
	WHILE @auditKey IS NOT NULL
	BEGIN
		IF @auditType = 'UPD'
		BEGIN
			DELETE FROM @AuditColumns;

			IF COALESCE(@auditOldReplacementValue, -1) <> COALESCE(@auditNewReplacementValue, -1)
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('ReplacementValue', COALESCE(CAST(@auditOldReplacementValue AS VARCHAR(4)), 'NULL'), COALESCE(CAST(@auditNewReplacementValue AS VARCHAR(4)), 'NULL'));

			-- Audit the RecHubException.CheckDigitReplacementValues update
			SET @auditMessage = 'Modified CheckDigitReplacementValues for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' CheckDigitRoutineKey: ' + CAST(@auditKey2 AS VARCHAR(20))
				+ ' CheckDigitReplacementValueKey: ' + CAST(@auditKey AS VARCHAR(20)) + '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'CheckDigitReplacementValues',
				@parmColumnName			= 'CheckDigitReplacementValueKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
		ELSE
		BEGIN
			SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' CheckDigitReplacementValues for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' CheckDigitRoutineKey: ' + CAST(@auditKey2 AS VARCHAR(20))
				+ ' CheckDigitReplacementValueKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': OriginalValue = ' + @auditOriginalValue
				+ ' ReplacementValue = ' + CASE WHEN @auditType = 'DEL' THEN COALESCE(CAST(@auditOldReplacementValue AS VARCHAR(4)), 'NULL') ELSE COALESCE(CAST(@auditNewReplacementValue AS VARCHAR(4)), 'NULL') END
				+ '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_INS
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'CheckDigitReplacementValues',
				@parmColumnName			= 'CheckDigitReplacementValueKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditMessage		= @auditMessage;
		END

		DELETE FROM @CheckDigitReplacementValueChanges WHERE CheckDigitReplacementValueKey = @auditKey;

		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey					= CheckDigitReplacementValueKey,
						@auditType					= AuditType,
						@auditKey2					= CheckDigitRoutineKey,
						@auditOriginalValue			= OriginalValue,
						@auditOldReplacementValue	= OldReplacementValue,
						@auditNewReplacementValue	= NewReplacementValue
		FROM @CheckDigitReplacementValueChanges;
	END



	--Audit changes of CheckDigitRoutines (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey						= CheckDigitRoutineKey,
					@auditType						= AuditType,
					@auditOldMethod					= OldMethod,					@auditNewMethod					= NewMethod,
					@auditOldOffset					= OldOffset,					@auditNewOffset					= NewOffset,
					@auditOldModulus				= OldModulus,					@auditNewModulus				= NewModulus,
					@auditOldCompliment				= OldCompliment,				@auditNewCompliment				= NewCompliment,
					@auditOldWeightPatternDirection	= OldWeightPatternDirection,	@auditNewWeightPatternDirection	= NewWeightPatternDirection,
					@auditOldWeightPatternValue		= OldWeightPatternValue,		@auditNewWeightPatternValue		= NewWeightPatternValue,
					@auditOldIgnoreSpaces			= OldIgnoreSpaces,				@auditNewIgnoreSpaces			= NewIgnoreSpaces,
					@auditOldRemainder10Replacement	= OldRemainder10Replacement,	@auditNewRemainder10Replacement	= NewRemainder10Replacement,
					@auditOldRemainder11Replacement	= OldRemainder11Replacement,	@auditNewRemainder11Replacement	= NewRemainder11Replacement,
					@auditOldFailureMessage			= OldFailureMessage,			@auditNewFailureMessage			= NewFailureMessage
	FROM @CheckDigitRoutineChanges;
	
	WHILE @auditKey IS NOT NULL
	BEGIN
		IF @auditType = 'UPD'
		BEGIN
			DELETE FROM @AuditColumns;

			IF @auditOldMethod <> @auditNewMethod
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Method', CAST(@auditOldMethod AS VARCHAR(10)), CAST(@auditNewMethod AS VARCHAR(10)));
			IF @auditOldOffset <> @auditNewOffset
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Offset', CAST(@auditOldOffset AS VARCHAR(10)), CAST(@auditNewOffset AS VARCHAR(10)));
			IF @auditOldModulus <> @auditNewModulus
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Modulus', CAST(@auditOldModulus AS VARCHAR(10)), CAST(@auditNewModulus AS VARCHAR(10)));
			IF @auditOldCompliment <> @auditNewCompliment
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Compliment', CAST(@auditOldCompliment AS VARCHAR(10)), CAST(@auditNewCompliment AS VARCHAR(10)));
			IF @auditOldWeightPatternDirection <> @auditNewWeightPatternDirection
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('WeightPatternDirection', CAST(@auditOldWeightPatternDirection AS VARCHAR(10)), CAST(@auditNewWeightPatternDirection AS VARCHAR(10)));
			IF @auditOldWeightPatternValue <> @auditNewWeightPatternValue
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('WeightPatternValue', @auditOldWeightPatternValue, @auditNewWeightPatternValue);
			IF @auditOldIgnoreSpaces <> @auditNewIgnoreSpaces
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('IgnoreSpaces', CAST(@auditOldIgnoreSpaces AS VARCHAR(1)), CAST(@auditNewIgnoreSpaces AS VARCHAR(1)));
			IF @auditOldRemainder10Replacement <> @auditNewRemainder10Replacement
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Remainder10Replacement', CAST(@auditOldRemainder10Replacement AS VARCHAR(10)), CAST(@auditNewRemainder10Replacement AS VARCHAR(10)));
			IF @auditOldRemainder11Replacement <> @auditNewRemainder11Replacement
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Remainder11Replacement', CAST(@auditOldRemainder11Replacement AS VARCHAR(10)), CAST(@auditNewRemainder11Replacement AS VARCHAR(10)));
			IF CAST(COALESCE(@auditOldFailureMessage, '') AS VARBINARY(120)) <> CAST(COALESCE(@auditNewFailureMessage, '') AS VARBINARY(120))
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FailureMessage', COALESCE(@auditOldFailureMessage, ''), COALESCE(@auditNewFailureMessage, ''));

			-- Audit the RecHubException.CheckDigitRoutines update
			SET @auditMessage = 'Modified CheckDigitRoutines for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' CheckDigitRoutineKey: ' + CAST(@auditKey AS VARCHAR(20)) + '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'CheckDigitRoutines',
				@parmColumnName			= 'CheckDigitRoutineKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
		ELSE
		BEGIN
			SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' CheckDigitRoutines for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' CheckDigitRoutineKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': Method = ' + CAST(COALESCE(@auditOldMethod, @auditNewMethod) AS VARCHAR(10))
				+ ', Offset = ' + CAST(COALESCE(@auditOldOffset, @auditNewOffset) AS VARCHAR(5))
				+ ', Modulus = ' + CAST(COALESCE(@auditOldModulus, @auditNewModulus) AS VARCHAR(10))
				+ ', Compliment = ' + CAST(COALESCE(@auditOldCompliment, @auditNewCompliment) AS VARCHAR(10))
				+ ', WeightPatternDirection = ' + CAST(COALESCE(@auditOldWeightPatternDirection, @auditNewMethod) AS VARCHAR(10))
				+ ', WeightPatternValue = ' + COALESCE(@auditOldWeightPatternValue, @auditNewWeightPatternValue)
				+ ', IgnoreSpaces = ' + CAST(COALESCE(@auditOldIgnoreSpaces, @auditNewIgnoreSpaces) AS VARCHAR(1))
				+ ', Remainder10Replacement = ' + CAST(COALESCE(@auditOldRemainder10Replacement, @auditNewRemainder10Replacement) AS VARCHAR(10))
				+ ', Remainder11Replacement = ' + CAST(COALESCE(@auditOldRemainder11Replacement, @auditNewRemainder11Replacement) AS VARCHAR(10))
				+ ', FailureMessage = ' + COALESCE(@auditOldFailureMessage, @auditNewFailureMessage, '')
				+ '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_INS
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'CheckDigitRoutines',
				@parmColumnName			= 'CheckDigitRoutineKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditMessage		= @auditMessage;
		END

		DELETE FROM @CheckDigitRoutineChanges WHERE CheckDigitRoutineKey = @auditKey;

		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey						= CheckDigitRoutineKey,
						@auditType						= AuditType,
						@auditOldMethod					= OldMethod,					@auditNewMethod					= NewMethod,
						@auditOldOffset					= OldOffset,					@auditNewOffset					= NewOffset,
						@auditOldModulus				= OldModulus,					@auditNewModulus				= NewModulus,
						@auditOldCompliment				= OldCompliment,				@auditNewCompliment				= NewCompliment,
						@auditOldWeightPatternDirection	= OldWeightPatternDirection,	@auditNewWeightPatternDirection	= NewWeightPatternDirection,
						@auditOldWeightPatternValue		= OldWeightPatternValue,		@auditNewWeightPatternValue		= NewWeightPatternValue,
						@auditOldIgnoreSpaces			= OldIgnoreSpaces,				@auditNewIgnoreSpaces			= NewIgnoreSpaces,
						@auditOldRemainder10Replacement	= OldRemainder10Replacement,	@auditNewRemainder10Replacement	= NewRemainder10Replacement,
						@auditOldRemainder11Replacement	= OldRemainder11Replacement,	@auditNewRemainder11Replacement	= NewRemainder11Replacement,
						@auditOldFailureMessage			= OldFailureMessage,			@auditNewFailureMessage			= NewFailureMessage
		FROM @CheckDigitRoutineChanges;
	END



	--Audit changes of BusinessRules (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey						= BusinessRuleKey,
					@auditType						= AuditType,
					@auditOldMinLength				= OldMinLength,					@auditNewMinLength				= NewMinLength,
					@auditOldMaxLength				= OldMaxLength,					@auditNewMaxLength				= NewMaxLength,
					@auditOldRequired				= OldRequired,					@auditNewRequired				= NewRequired,
					@auditOldUserCanEdit			= OldUserCanEdit,				@auditNewUserCanEdit			= NewUserCanEdit,
					@auditOldCheckDigitRoutineKey	= OldCheckDigitRoutineKey,		@auditNewCheckDigitRoutineKey	= NewCheckDigitRoutineKey,
					@auditOldFieldValidationKey		= OldFieldValidationKey,		@auditNewFieldValidationKey		= NewFieldValidationKey,
					@auditOldFormatType				= OldFormatType,				@auditNewFormatType				= NewFormatType,
					@auditOldMask					= OldMask,						@auditNewMask					= NewMask,
					@auditOldRequiredMessage		= OldRequiredMessage,			@auditNewRequiredMessage		= NewRequiredMessage
	FROM @BusinessRuleChanges;
	
	WHILE @auditKey IS NOT NULL
	BEGIN
		IF @auditType = 'UPD'
		BEGIN
			DELETE FROM @AuditColumns;

			IF @auditOldMinLength <> @auditNewMinLength
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('MinLength', CAST(@auditOldMinLength AS VARCHAR(5)), CAST(@auditNewMinLength AS VARCHAR(5)));
			IF @auditOldMaxLength <> @auditNewMaxLength
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('MaxLength', CAST(@auditOldMaxLength AS VARCHAR(5)), CAST(@auditNewMaxLength AS VARCHAR(5)));
			IF @auditOldRequired <> @auditNewRequired
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Required', CAST(@auditOldRequired AS VARCHAR(1)), CAST(@auditNewRequired AS VARCHAR(1)));
			IF @auditOldUserCanEdit <> @auditNewUserCanEdit
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('UserCanEdit', CAST(@auditOldUserCanEdit AS VARCHAR(1)), CAST(@auditNewUserCanEdit AS VARCHAR(1)));
			IF COALESCE(@auditOldCheckDigitRoutineKey, -1) <> COALESCE(@auditNewCheckDigitRoutineKey, -1)
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('CheckDigitRoutineKey', COALESCE(CAST(@auditOldCheckDigitRoutineKey AS VARCHAR(20)), 'NULL'), COALESCE(CAST(@auditNewCheckDigitRoutineKey AS VARCHAR(20)), 'NULL'));
			IF COALESCE(@auditOldFieldValidationKey, -1) <> COALESCE(@auditNewFieldValidationKey, -1)
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FieldValidationKey', COALESCE(CAST(@auditOldFieldValidationKey AS VARCHAR(20)), 'NULL'), COALESCE(CAST(@auditNewFieldValidationKey AS VARCHAR(20)), 'NULL'));
			IF @auditOldFormatType <> @auditNewFormatType
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('FormatType', CAST(@auditOldFormatType AS VARCHAR(3)), CAST(@auditNewFormatType AS VARCHAR(3)));
			IF COALESCE(@auditOldMask, '') <> COALESCE(@auditNewMask, '')
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('Mask', COALESCE(@auditOldMask, ''), COALESCE(@auditNewMask, ''));
			IF CAST(COALESCE(@auditOldRequiredMessage, '') AS VARBINARY(120)) <> CAST(COALESCE(@auditNewRequiredMessage, '') AS VARBINARY(120))
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('RequiredMessage', COALESCE(@auditOldRequiredMessage, ''), COALESCE(@auditNewRequiredMessage, ''));

			-- Audit the RecHubException.BusinessRules update
			SET @auditMessage = 'Modified BusinessRules for  for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' BusinessRuleKey: ' + CAST(@auditKey AS VARCHAR(20)) + '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'BusinessRules',
				@parmColumnName			= 'BusinessRuleKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
		ELSE
		BEGIN
			SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' BusinessRules for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' BusinessRuleKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': MinLength = ' + CAST(COALESCE(@auditOldMinLength, @auditNewMinLength) AS VARCHAR(5))
				+ ', MaxLength = ' + CAST(COALESCE(@auditOldMaxLength, @auditNewmaxLength) AS VARCHAR(5))
				+ ', Required = ' + CAST(COALESCE(@auditOldRequired, @auditNewRequired) AS VARCHAR(1))
				+ ', UserCanEdit = ' + CAST(COALESCE(@auditOldUserCanEdit, @auditNewUserCanEdit) AS VARCHAR(1))
				+ ', CheckDigitRoutineKey = ' + COALESCE(CAST(COALESCE(@auditOldCheckDigitRoutineKey, @auditNewCheckDigitRoutineKey) AS VARCHAR(20)), 'NULL')
				+ ', FieldValidationKey = ' + COALESCE(CAST(COALESCE(@auditOldFieldValidationKey, @auditNewFieldValidationKey) AS VARCHAR(20)), 'NULL')
				+ ', FormatType = ' + CAST(COALESCE(@auditOldFormatType, @auditNewFormatType) AS VARCHAR(3))
				+ ', Mask = ' + COALESCE(@auditOldMask, @auditNewMask, '')
				+ ', RequiredMessage = ' + COALESCE(@auditOldRequiredMessage, @auditNewRequiredMessage, '')
				+ '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_INS
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'BusinessRules',
				@parmColumnName			= 'BusinessRuleKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditMessage		= @auditMessage;
		END

		DELETE FROM @BusinessRuleChanges WHERE BusinessRuleKey = @auditKey;

		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey						= BusinessRuleKey,
						@auditType						= AuditType,
						@auditOldMinLength				= OldMinLength,					@auditNewMinLength				= NewMinLength,
						@auditOldMaxLength				= OldMaxLength,					@auditNewMaxLength				= NewMaxLength,
						@auditOldRequired				= OldRequired,					@auditNewRequired				= NewRequired,
						@auditOldUserCanEdit			= OldUserCanEdit,				@auditNewUserCanEdit			= NewUserCanEdit,
						@auditOldCheckDigitRoutineKey	= OldCheckDigitRoutineKey,		@auditNewCheckDigitRoutineKey	= NewCheckDigitRoutineKey,
						@auditOldFieldValidationKey		= OldFieldValidationKey,		@auditNewFieldValidationKey		= NewFieldValidationKey,
						@auditOldFormatType				= OldFormatType,				@auditNewFormatType				= NewFormatType,
						@auditOldMask					= OldMask,						@auditNewMask					= NewMask,
						@auditOldRequiredMessage		= OldRequiredMessage,			@auditNewRequiredMessage		= NewRequiredMessage
		FROM @BusinessRuleChanges;
	END

	

	--Audit changes of DataEntrySetupFields (TODO: Update this to user audit proc that takes a table once that is created)
	SET @auditKey = NULL;
	SELECT TOP 1	@auditKey						= DataEntrySetupFieldKey,
					@auditType						= AuditType,
					@auditKey2						= BusinessRuleKey,
					@auditTableType					= TableType,
					@auditFldName					= FldName,
					@auditBatchSourceKey			= BatchSourceKey,
					@auditBatchPaymentTypeKey		= BatchPaymentTypeKey,
					@auditBatchPaymentSubTypeKey	= BatchPaymentSubTypeKey,
					@auditOldExceptionDisplayOrder	= OldExceptionDisplayOrder,			@auditNewExceptionDisplayOrder	= NewExceptionDisplayOrder
	FROM @DataEntrySetupFieldChanges;
	
	WHILE @auditKey IS NOT NULL
	BEGIN
		IF @auditType = 'UPD'
		BEGIN
			DELETE FROM @AuditColumns;

			IF ISNULL(CAST(@auditOldExceptionDisplayOrder AS SMALLINT), -1) <> ISNULL(CAST(@auditNewExceptionDisplayOrder AS SMALLINT), -1)
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue)
				VALUES('ExceptionDisplayOrder', ISNULL(CAST(@auditOldExceptionDisplayOrder AS VARCHAR(4)), 'NULL'), ISNULL(CAST(@auditNewExceptionDisplayOrder AS VARCHAR(4)), 'NULL'));

			-- Audit the RecHubException.DataEntrySetupFields update
			SET @auditMessage = 'Modified DataEntrySetupFields for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' DataEntrySetupKey: ' + CAST(@parmDataEntrySetupKey AS VARCHAR(20))
				+ ' DataEntrySetupFieldKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': BatchSourceKey = ' + COALESCE(CAST(@auditBatchSourceKey AS VARCHAR(4)), 'NULL')
				+ ', BatchPaymentTypeKey = ' + COALESCE(CAST(@auditBatchPaymentTypeKey AS VARCHAR(4)), 'NULL')
				+ ', BatchPaymentSubTypeKey = ' + COALESCE(CAST(@auditBatchPaymentSubTypeKey AS VARCHAR(4)), 'NULL')
				+ ', TableType = ' + CAST(@auditTableType AS VARCHAR(3))
				+ ', FldName = ' + @auditFldName;

			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'DataEntrySetupFields',
				@parmColumnName			= 'DataEntrySetupFieldKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
		ELSE
		BEGIN
			SET @auditMessage = CASE WHEN @auditType = 'DEL' THEN 'Deleted' ELSE 'Added' END + ' DataEntrySetupFields for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmSiteClientAccountID AS VARCHAR(10))
				+ ' DataEntrySetupKey: ' + CAST(@parmDataEntrySetupKey AS VARCHAR(20))
				+ ' DataEntrySetupFieldKey: ' + CAST(@auditKey AS VARCHAR(20))
				+ ': BatchSourceKey = ' + COALESCE(CAST(@auditBatchSourceKey AS VARCHAR(4)), 'NULL')
				+ ', BatchPaymentTypeKey = ' + COALESCE(CAST(@auditBatchPaymentTypeKey AS VARCHAR(4)), 'NULL')
				+ ', BatchPaymentSubTypeKey = ' + COALESCE(CAST(@auditBatchPaymentSubTypeKey AS VARCHAR(4)), 'NULL')
				+ ', BusinessRuleKey = ' + CAST(@auditKey2 AS VARCHAR(20))
				+ ', TableType = ' + CAST(@auditTableType AS VARCHAR(3))
				+ ', FldName = ' + @auditFldName
				+ ', ExceptionDisplayOrder = ' + COALESCE(CAST(@auditOldExceptionDisplayOrder AS VARCHAR(4)), CAST(@auditNewExceptionDisplayOrder AS VARCHAR(4)), '')
				+ '.';

			EXEC RecHubCommon.usp_WFS_DataAudit_INS
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_BusinessRules_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'DataEntrySetupFields',
				@parmColumnName			= 'DataEntrySetupFieldKey',
				@parmAuditValue			= @auditKey,
				@parmAuditType			= @auditType,
				@parmAuditMessage		= @auditMessage;
		END
		
		DELETE FROM @DataEntrySetupFieldChanges WHERE DataEntrySetupFieldKey = @auditKey;
		
		SET @auditKey = NULL;
		SELECT TOP 1	@auditKey						= DataEntrySetupFieldKey,
						@auditType						= AuditType,
						@auditKey2						= BusinessRuleKey,
						@auditTableType					= TableType,
						@auditFldName					= FldName,
						@auditBatchSourceKey			= BatchSourceKey,
						@auditBatchPaymentTypeKey		= BatchPaymentTypeKey,
						@auditBatchPaymentSubTypeKey	= BatchPaymentSubTypeKey,
						@auditOldExceptionDisplayOrder	= OldExceptionDisplayOrder,			@auditNewExceptionDisplayOrder	= NewExceptionDisplayOrder
		FROM @DataEntrySetupFieldChanges;
	END

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

