--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_DataEntryDetails_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_DataEntryDetails_Get') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_DataEntryDetails_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_DataEntryDetails_Get
(
	@parmSessionID				UNIQUEIDENTIFIER,	
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				INT,
	@parmTransactionID			INT,
	@parmBatchSequence			INT,
	@parmConvertDataType		BIT = 0
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 08/16/2017
*
* Purpose: Query Data Entry Detail fact table and PostDepositDataEntryDetails for batch, transaction 
*			data entry information.  Results reflect the latest state, whether unstarted, completed, or 
*			partially completed.
*			(Used RecHubData.usp_factDataEntryDetails_Get_StubsDE as template for this stored procedure)
*
*
* Parameter Information
*	@parmConvertDataType - 0 means to not convert, 1 means to convert.
*
*
* Modification History
* 08/16/2017 PT 144805801	MGE	Created.
* 08/30/2017 PT 149441599	JPB	Updated to latest tables.
* 09/05/2017 PT	149441599	JPB	Added IsEditable and minor fixes.
* 10/10/2017 PT	147282969	MGE	Added IsUIAddedField
******************************************************************************/
SET NOCOUNT ON;

DECLARE														
		@StartDateKey	INT,								
		@EndDateKey		INT,
		@BatchSourceKey	INT;
		
BEGIN TRY

	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,			
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 
		@BatchSourceKey = BatchSourceKey 
	FROM 
		RecHubData.factBatchSummary
	WHERE 
		BatchID = @parmBatchID
	OPTION( RECOMPILE );

	;WITH DataEntryValues_CTE AS
	(
		SELECT DISTINCT
			'Stubs' AS 'TableName',
			RecHubData.dimWorkgroupDataEntryColumns.FieldName as FldName,
			CASE @parmConvertDataType
				WHEN 0 
					THEN RecHubData.dimWorkgroupDataEntryColumns.DataType
				WHEN 1 
					THEN 
						CASE
							WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount')
								THEN 7
							ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
						END
			END AS FldDataTypeEnum,
			RecHubData.factDataEntryDetails.DataEntryValue
		FROM
			RecHubData.dimWorkgroupDataEntryColumns
			INNER JOIN RecHubData.factDataEntryDetails ON
				RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON			--WI 142841
				RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
		WHERE	
				RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID						--WI 142841
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID					--WI 142841
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID  --WI 142841
			AND RecHubData.factDataEntryDetails.DepositDateKey = @EndDateKey								--WI 142841
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
			AND RecHubData.factDataEntryDetails.BatchSequence = @parmBatchSequence
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
	)
	,RequiredDataEntryFields_CTE AS
	(
		SELECT
			'Stubs' AS TableName,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName AS FldName,
			CASE @parmConvertDataType
				WHEN 0 
					THEN RecHubData.dimWorkgroupDataEntryColumns.DataType
				WHEN 1 
					THEN 
						CASE
							WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount')
								THEN 7
							ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
						END
			END AS FldDataTypeEnum,
			ScreenOrder,
			'' AS DataEntryValue
		FROM
			RecHubData.dimWorkgroupDataEntryColumns
		WHERE 
			RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = @BatchSourceKey
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = @parmSiteBankID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
			AND RecHubData.dimWorkgroupDataEntryColumns.IsRequired = 1
	), EditedFields_CTE AS
	(
		SELECT
			FieldName,
			IsUIAddedField
		FROM
			RecHubException.PostDepositDataEntryDetails
		WHERE
			RecHubException.PostDepositDataEntryDetails.DepositDateKey = @EndDateKey
			AND RecHubException.PostDepositDataEntryDetails.BatchID = @parmBatchID
			AND RecHubException.PostDepositDataEntryDetails.TransactionID = @parmTransactionID
			AND RecHubException.PostDepositDataEntryDetails.BatchSequence = @parmBatchSequence	
	)
	SELECT 
		COALESCE(RequiredDataEntryFields_CTE.TableName,DataEntryValues_CTE.TableName) AS TableName,
		COALESCE(RequiredDataEntryFields_CTE.FldName,DataEntryValues_CTE.FldName) AS FldName,
		COALESCE(RequiredDataEntryFields_CTE.FldDataTypeEnum,DataEntryValues_CTE.FldDataTypeEnum) AS FldDataTypeEnum,
		COALESCE(DataEntryValues_CTE.DataEntryValue,RequiredDataEntryFields_CTE.DataEntryValue) AS DataEntryValue,
		CASE
			WHEN EditedFields_CTE.FieldName IS NOT NULL THEN 1 
			ELSE 0
		END	AS IsEditable,
		IsUIAddedField
	FROM
		RequiredDataEntryFields_CTE
		FULL OUTER JOIN DataEntryValues_CTE ON DataEntryValues_CTE.TableName = RequiredDataEntryFields_CTE.TableName
			AND DataEntryValues_CTE.FldName = RequiredDataEntryFields_CTE.FldName
		LEFT JOIN EditedFields_CTE ON EditedFields_CTE.FieldName = DataEntryValues_CTE.FldName
			OR EditedFields_CTE.FieldName = RequiredDataEntryFields_CTE.FldName
	ORDER BY
		ScreenOrder ASC
	OPTION( RECOMPILE );
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH