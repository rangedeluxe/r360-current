--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_VerifyTransactionCheckout
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_VerifyTransactionCheckout') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_CommonExceptions_VerifyTransactionCheckout
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_VerifyTransactionCheckout 
(
	@parmCommonExceptionID					INT,
	@parmUserID								INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: The procedure is used to verify the checkout status of a transaction
*			prior to updating the transaction.  It also returns any external
*			identifiers that the application would need to update the transaction.
*
* Modification History
* 07/09/2013 WI 108948 RDS	Created
* 04/25/2014 WI 139282 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	-- Return the ID parameters and lock state for the common exception
	SELECT CAST(case when RecHubException.DecisioningTransactions.UserID = @parmUserID then 1 else 0 end as BIT) AS Locked
		,	CASE 
				WHEN RecHubException.DecisioningTransactions.TransactionKey IS NOT NULL THEN 1 
				WHEN RecHubException.DecisioningTransactions.IntegraPAYExceptionKey IS NOT NULL THEN 2
				ELSE 0 
			END AS ExceptionType
		--Fields only used for In Process
		, RecHubException.IntegraPAYExceptions.GlobalBatchID
		, RecHubException.IntegraPAYExceptions.TransactionID
		--Fields only used for Post Process
		, RecHubException.DecisioningTransactions.TransactionStatusKey as TransactionStatus
		, RecHubException.ExceptionBatches.DataEntrySetupKey
		, RecHubException.ExceptionBatches.BatchSourceKey
		, RecHubException.ExceptionBatches.BatchPaymentTypeKey
	FROM RecHubException.DecisioningTransactions
	LEFT JOIN RecHubException.IntegraPAYExceptions
		ON RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = 
			RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
	LEFT JOIN RecHubException.Transactions
		ON RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
	LEFT JOIN RecHubException.ExceptionBatches
		ON RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;
	
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH