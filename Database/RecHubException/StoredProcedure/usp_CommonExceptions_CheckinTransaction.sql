--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_CheckinTransaction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_CheckinTransaction') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_CommonExceptions_CheckinTransaction
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_CheckinTransaction 
(
	@parmCommonExceptionID					INT,
	@parmUserID								INT,
	@parmStatus								TINYINT,
	@parmOverrideAllLocks					BIT,
	@parmOverrideRules						BIT,
	@parmForceBalance						BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: This procedure updates the lock status (unlocks the transaction),
*			updates related status fields for special in-process exceptions,
*			audits the checkin, and finally verifies the status of all other
*			transactions in the same batch to see if the batch is now complete.
*
* Modification History
* 07/09/2013 WI 108947 RDS	Created
* 07/18/2013 WI 109480 RDS	Added Audit records
* 07/25/2013 WI	109843 RDS	Return lock update result
* 11/15/2013 WI 122651 EAS  Update Modification Date on Transaction Update
* 04/25/2014 WI 139280 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
* 05/14/2014 WI 141669 RDS	Add auditing
* 09/10/2014 WI 141669 KLC	Add auditing of rule override and forcing balancing
* 10/10/2014 WI 141669 KLC	Updated auditing text
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @auditMessage VARCHAR(1024);
	DECLARE @auditEvent VARCHAR(64);
	DECLARE @integraPAYExceptionKey BIGINT;

	-- Checkin the transaction for this user (or override all user's locks, if appropriate)
	UPDATE RecHubException.DecisioningTransactions
		SET UserID = NULL,
			TransactionStatusKey = CASE @parmStatus 
				WHEN 1 THEN 2 /* Accepted */ 
				WHEN 2 THEN 3 /* Rejected */
				ELSE TransactionStatusKey /* No Update */
			END,
			TransactionEndWork = GETDATE(),
			ModificationDate = GETDATE(),
			@integraPAYExceptionKey = IntegraPAYExceptionKey
	WHERE DecisioningTransactionKey = @parmCommonExceptionID
		AND (UserID = @parmUserID OR @parmOverrideAllLocks = 1);

	-- Update the status of the common exception, if complete (and unlock succeeded)
	IF @@ROWCOUNT > 0
	BEGIN

		-- Construct the audit message
		SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
				+ ISNULL(', GlobalBatchID: ' + CAST(RecHubException.IntegraPAYExceptions.GlobalBatchID as VARCHAR), '')
				+ ISNULL(', TransactionID: ' + CAST(RecHubException.IntegraPAYExceptions.TransactionID as VARCHAR), '')
				+ ISNULL(', DepositDate: ' + CAST(RecHubException.ExceptionBatches.DepositDateKey as VARCHAR), '')
				+ ISNULL(', BatchID: ' + CAST(RecHubException.ExceptionBatches.BatchID as VARCHAR), '')
				+ ISNULL(', TransactionID: ' + CAST(RecHubException.Transactions.TransactionID as VARCHAR), '')
		FROM RecHubException.DecisioningTransactions
		LEFT JOIN RecHubException.IntegraPAYExceptions ON
			RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
		LEFT JOIN RecHubException.Transactions ON
			RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
		LEFT JOIN RecHubException.ExceptionBatches ON
			RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

		IF @parmStatus = 2
		BEGIN
			SELECT @auditMessage = @auditMessage
				+ ', Rules Overridden: ' + CASE WHEN @parmOverrideRules = 1 THEN 'Yes' ELSE 'No' END
				+ ', Forced Balance: ' + CASE WHEN @parmForceBalance = 1 THEN 'Yes' ELSE 'No' END;
		END

		IF @parmStatus > 0
		BEGIN

			IF (@integraPAYExceptionKey IS NOT NULL)
			BEGIN
				-- Update the IntegraPAY Status
				UPDATE RecHubException.IntegraPAYExceptions
				SET 
					DecisioningStatus = @parmStatus,
					ModificationDate = GETDATE()
				WHERE IntegraPAYExceptionKey = @integraPAYExceptionKey;
			END
			
			-- Complete the audit message
			SELECT @auditEvent = (case @parmStatus when 1 then 'E Accepted' when 2 then 'E Rejected' else 'E Status Unknown (' + cast(@parmStatus as varchar) + ')' end);

		END
		ELSE
		BEGIN
		
			-- Complete the audit message
			IF @parmOverrideAllLocks = 1
			BEGIN
			
				SELECT @auditEvent = 'E Lock Broken';
			
			END
			ELSE
			BEGIN
			
				SET @auditEvent = 'E Checked In'; -- Don't know if the transaction was updated or not...
			
			END
		END

		-- Write Audit Record				
		EXEC RecHubCommon.usp_WFS_EventAudit_Ins 
				@parmApplicationName = 'RecHubException.usp_CommonExceptions_CheckinTransaction', 
				@parmEventName = @auditEvent, 
				@parmEventType = 'Exceptions',
				@parmUserID = @parmUserID, 
				@parmAuditMessage = @auditMessage
	END
	
	
	-- Look for all common exceptions in this batch, and return the minimum status remaining for the batch
	IF (@integraPAYExceptionKey IS NOT NULL)
	BEGIN
		SELECT 2 AS ExceptionType 
			, MIN(DecisioningStatus) AS MinDecisioningStatus
			, GlobalBatchID AS GlobalBatchID
			, CASE WHEN ((SELECT UserID FROM RecHubException.DecisioningTransactions WHERE DecisioningTransactionKey = @parmCommonExceptionID) IS NULL) THEN 0 ELSE 1 END AS StillLocked
		FROM RecHubException.IntegraPAYExceptions
		WHERE GlobalBatchID = (SELECT GlobalBatchID 
								FROM RecHubException.IntegraPAYExceptions 
								WHERE IntegraPAYExceptionKey = @integraPAYExceptionKey)
		GROUP BY GlobalBatchID;
	END
	ELSE
	BEGIN
		WITH CTE_TransactionProperties AS
		(
			SELECT RecHubException.DecisioningTransactions.UserID, RecHubException.ExceptionBatches.ExceptionBatchKey
			FROM RecHubException.DecisioningTransactions
			INNER JOIN RecHubException.Transactions ON
				RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
			INNER JOIN RecHubException.ExceptionBatches ON
				RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
			WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID
		)
		SELECT 1 AS ExceptionType 
			, MIN(RecHubException.DecisioningTransactions.TransactionStatusKey) AS MinTransactionStatus
			, CTE_TransactionProperties.ExceptionBatchKey
			, CASE WHEN CTE_TransactionProperties.UserID IS NULL THEN 0 ELSE 1 END AS StillLocked
		FROM CTE_TransactionProperties
		INNER JOIN RecHubException.ExceptionBatches ON
			CTE_TransactionProperties.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
		INNER JOIN RecHubException.Transactions ON
			RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
		INNER JOIN RecHubException.DecisioningTransactions ON
			RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
		GROUP BY CTE_TransactionProperties.ExceptionBatchKey, CTE_TransactionProperties.UserID;
	END			
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

