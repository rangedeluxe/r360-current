--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_UpSert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factDataEntryDetails_UpSert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factDataEntryDetails_UpSert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factDataEntryDetails_UpSert
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmModificationDate DATETIME,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Update an existing factDataEntryDatail records from 
*		PostDepositExceptions
*
* Modification History
* 08/28/2017 PT 149441599	JPB Created
* 09/21/2017 PT 151074090   TWE updated to handle multiple updates
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @factDataEntryDetails TABLE
	(
	    factDataEntryDetailKey BIGINT NULL,
		BankKey INT NOT NULL,
		OrganizationKey INT NOT NULL,
		ClientAccountKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ImmutableDateKey INT NOT NULL,
		SourceProcessingDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		SourceBatchID BIGINT NOT NULL,
		BatchNumber INT NOT NULL,
		BatchSourceKey SMALLINT NOT NULL,
		BatchPaymentTypeKey TINYINT NOT NULL,
		DepositStatus INT NOT NULL,
		WorkgroupDataEntryColumnKey BIGINT NOT NULL,
		TransactionID INT NOT NULL,
		BatchSequence INT NOT NULL,
		DataEntryValueDateTime DATETIME NULL,
		DataEntryValueFloat FLOAT NULL,
		DataEntryValueMoney MONEY NULL,
		DataEntryValue VARCHAR(256) NOT NULL
	);

	DECLARE @NewFields TABLE
	(
		IsCheck BIT NOT NULL,
		BatchSequence INT NOT NULL,
		FieldName VARCHAR(256) NOT NULL
	);

	;WITH DataEntryFields_CTE AS
	(
		SELECT
			RecHubData.factDataEntryDetails.DepositDateKey,
			RecHubData.factDataEntryDetails.BatchID,
			RecHubData.factDataEntryDetails.TransactionID,
			RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName
		FROM
			RecHubData.factDataEntryDetails
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factDataEntryDetails.ClientAccountKey
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
				AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
				AND RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = RecHubData.factDataEntryDetails.BatchSourceKey
			INNER JOIN @parmDataEntryValues AS DataEntryValues ON DataEntryValues.IsCheck = RecHubData.dimWorkgroupDataEntryColumns.IsCheck
				AND DataEntryValues.FieldName = RecHubData.dimWorkgroupDataEntryColumns.FieldName
		WHERE
			RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
		GROUP BY
			RecHubData.factDataEntryDetails.DepositDateKey,
			RecHubData.factDataEntryDetails.BatchID,
			RecHubData.factDataEntryDetails.TransactionID,
			RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName
	),
	DataEntryValues_CTE AS
	(
		SELECT
			DataEntryFields_CTE.DepositDateKey,
			DataEntryFields_CTE.BatchID,
			DataEntryFields_CTE.TransactionID,
			DataEntryFields_CTE.WorkgroupDataEntryColumnKey,
			DataEntryValues.BatchSequence,
			DataEntryValues.DataEntryValueDateTime,
			DataEntryValues.DataEntryValueFloat,
			DataEntryValues.DataEntryValueMoney,
			DataEntryValues.DataEntryValue
		FROM
			DataEntryFields_CTE
			INNER JOIN @parmDataEntryValues AS DataEntryValues ON DataEntryValues.IsCheck = DataEntryFields_CTE.IsCheck
				AND DataEntryValues.FieldName = DataEntryFields_CTE.FieldName
			INNER JOIN RecHubException.PostDepositDataEntryDetails ON RecHubException.PostDepositDataEntryDetails.DepositDateKey = DataEntryFields_CTE.DepositDateKey
				AND RecHubException.PostDepositDataEntryDetails.BatchID = DataEntryFields_CTE.BatchID
				AND RecHubException.PostDepositDataEntryDetails.TransactionID = DataEntryFields_CTE.TransactionID
				AND RecHubException.PostDepositDataEntryDetails.BatchSequence = DataEntryValues.BatchSequence
				AND RecHubException.PostDepositDataEntryDetails.IsCheck = DataEntryValues.IsCheck
				AND RecHubException.PostDepositDataEntryDetails.FieldName = DataEntryValues.FieldName
	)
	INSERT INTO
		@factDataEntryDetails
	SELECT
	    RecHubData.factDataEntryDetails.factDataEntryDetailKey,
		RecHubData.factDataEntryDetails.BankKey,
		RecHubData.factDataEntryDetails.OrganizationKey,
		RecHubData.factDataEntryDetails.ClientAccountKey,
		RecHubData.factDataEntryDetails.DepositDateKey,
		RecHubData.factDataEntryDetails.ImmutableDateKey,
		RecHubData.factDataEntryDetails.SourceProcessingDateKey,
		RecHubData.factDataEntryDetails.BatchID,
		RecHubData.factDataEntryDetails.SourceBatchID,
		RecHubData.factDataEntryDetails.BatchNumber,
		RecHubData.factDataEntryDetails.BatchSourceKey,
		RecHubData.factDataEntryDetails.BatchPaymentTypeKey,
		RecHubData.factDataEntryDetails.DepositStatus,
		RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey,
		RecHubData.factDataEntryDetails.TransactionID,
		RecHubData.factDataEntryDetails.BatchSequence,
		DataEntryValues_CTE.DataEntryValueDateTime,
		DataEntryValues_CTE.DataEntryValueFloat,
		DataEntryValues_CTE.DataEntryValueMoney,
		DataEntryValues_CTE.DataEntryValue
	FROM
		DataEntryValues_CTE
		INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.DepositDateKey = DataEntryValues_CTE.DepositDateKey
			AND RecHubData.factDataEntryDetails.BatchID = DataEntryValues_CTE.BatchID
			AND RecHubData.factDataEntryDetails.TransactionID = DataEntryValues_CTE.TransactionID
			AND RecHubData.factDataEntryDetails.BatchSequence = DataEntryValues_CTE.BatchSequence
			AND RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = DataEntryValues_CTE.WorkgroupDataEntryColumnKey
	WHERE
		RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RecHubData.factDataEntryDetails.DataEntryValue <> DataEntryValues_CTE.DataEntryValue;

	;WITH NewDataEntryField_CTE AS
	(	
		SELECT 
			FieldName,
			BatchSequence,
			IsCheck
		FROM 
			@parmDataEntryValues 
		EXCEPT
		SELECT 
			RecHubData.dimWorkgroupDataEntryColumns.FieldName,
			RecHubData.factDataEntryDetails.BatchSequence,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck
		FROM
			@parmDataEntryValues AS DataEntryValues
			INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.BatchSequence = DataEntryValues.BatchSequence
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
				AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = DataEntryValues.FieldName
		WHERE
			RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
			AND RecHubData.factDataEntryDetails.BatchSequence = DataEntryValues.BatchSequence
		GROUP BY
			RecHubData.factDataEntryDetails.DepositDateKey,
			RecHubData.factDataEntryDetails.BatchID,
			RecHubData.factDataEntryDetails.TransactionID,
			RecHubData.factDataEntryDetails.BatchSequence,
			RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName	
	)
	INSERT INTO @NewFields(FieldName,BatchSequence,IsCheck)
	SELECT FieldName,BatchSequence,IsCheck FROM NewDataEntryField_CTE

	IF EXISTS(SELECT 1 FROM @NewFields)
	BEGIN
		;WITH factKeys_CTE AS
		(
			SELECT 
				RecHubData.factDataEntryDetails.BankKey,
				RecHubData.factDataEntryDetails.OrganizationKey,
				RecHubData.factDataEntryDetails.ClientAccountKey,
				RecHubData.factDataEntryDetails.DepositDateKey,
				RecHubData.factDataEntryDetails.ImmutableDateKey,
				RecHubData.factDataEntryDetails.SourceProcessingDateKey,
				RecHubData.factDataEntryDetails.BatchID,
				RecHubData.factDataEntryDetails.SourceBatchID,
				RecHubData.factDataEntryDetails.BatchNumber,
				RecHubData.factDataEntryDetails.BatchSourceKey,
				RecHubData.factDataEntryDetails.BatchPaymentTypeKey,
				RecHubData.factDataEntryDetails.DepositStatus,
				RecHubData.factDataEntryDetails.TransactionID,
				RecHubData.factDataEntryDetails.BatchSequence
			FROM
				@NewFields AS NewFields
				INNER JOIN @parmDataEntryValues AS DataEntryValues ON DataEntryValues.IsCheck = NewFields.IsCheck
					AND DataEntryValues.BatchSequence = NewFields.BatchSequence
					AND DataEntryValues.FieldName = NewFields.FieldName
				INNER JOIN RecHubData.factDataEntryDetails ON RecHubData.factDataEntryDetails.BatchSequence = NewFields.BatchSequence
			WHERE
				RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey
				AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
				AND RecHubData.factDataEntryDetails.TransactionID = @parmTransactionID
				AND RecHubData.factDataEntryDetails.IsDeleted = 0
			GROUP BY
				RecHubData.factDataEntryDetails.BankKey,
				RecHubData.factDataEntryDetails.OrganizationKey,
				RecHubData.factDataEntryDetails.ClientAccountKey,
				RecHubData.factDataEntryDetails.DepositDateKey,
				RecHubData.factDataEntryDetails.ImmutableDateKey,
				RecHubData.factDataEntryDetails.SourceProcessingDateKey,
				RecHubData.factDataEntryDetails.BatchID,
				RecHubData.factDataEntryDetails.SourceBatchID,
				RecHubData.factDataEntryDetails.BatchNumber,
				RecHubData.factDataEntryDetails.BatchSourceKey,
				RecHubData.factDataEntryDetails.BatchPaymentTypeKey,
				RecHubData.factDataEntryDetails.DepositStatus,
				RecHubData.factDataEntryDetails.TransactionID,
				RecHubData.factDataEntryDetails.BatchSequence			
		)
		INSERT INTO @factDataEntryDetails
		(
		    factDataEntryDetailKey,
			BankKey,
			OrganizationKey,
			ClientAccountKey,
			DepositDateKey,
			ImmutableDateKey,
			SourceProcessingDateKey,
			BatchID,
			SourceBatchID,
			BatchNumber,
			BatchSourceKey,
			BatchPaymentTypeKey,
			DepositStatus,
			WorkgroupDataEntryColumnKey,
			TransactionID,
			BatchSequence,
			DataEntryValueDateTime,
			DataEntryValueFloat,
			DataEntryValueMoney,
			DataEntryValue
		)
		SELECT
		    NULL,
			factKeys_CTE.BankKey,
			factKeys_CTE.OrganizationKey,
			factKeys_CTE.ClientAccountKey,
			factKeys_CTE.DepositDateKey,
			factKeys_CTE.ImmutableDateKey,
			factKeys_CTE.SourceProcessingDateKey,
			factKeys_CTE.BatchID,
			factKeys_CTE.SourceBatchID,
			factKeys_CTE.BatchNumber,
			factKeys_CTE.BatchSourceKey,
			factKeys_CTE.BatchPaymentTypeKey,
			factKeys_CTE.DepositStatus,
			RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
			factKeys_CTE.TransactionID,
			factKeys_CTE.BatchSequence,
			DataEntryValues.DataEntryValueDateTime,
			DataEntryValues.DataEntryValueFloat,
			DataEntryValues.DataEntryValueMoney,
			DataEntryValues.DataEntryValue
		FROM
			@NewFields AS NewFields
			INNER JOIN @parmDataEntryValues AS DataEntryValues ON DataEntryValues.IsCheck = NewFields.IsCheck
				AND DataEntryValues.BatchSequence = NewFields.BatchSequence
				AND DataEntryValues.FieldName = NewFields.FieldName
			INNER JOIN factKeys_CTE ON factKeys_CTE.BatchSequence = NewFields.BatchSequence
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = factKeys_CTE.ClientAccountKey
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = RecHubData.dimClientAccounts.SiteBankID
				AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = RecHubData.dimClientAccounts.SiteClientAccountID
				AND RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = factKeys_CTE.BatchSourceKey
				AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = NewFields.FieldName
				AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = NewFields.IsCheck
		WHERE
			DataEntryValues.DataEntryValue <> '';
	END

	INSERT INTO RecHubData.factDataEntryDetails
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		WorkgroupDataEntryColumnKey,
		TransactionID,
		BatchSequence,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue,
		CreationDate,
		ModificationDate
	)
	SELECT
		0 AS IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		WorkgroupDataEntryColumnKey,
		TransactionID,
		BatchSequence,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue,
		@parmModificationDate AS CreationDate,
		@parmModificationDate AS ModificationDate
	FROM 
		@factDataEntryDetails
	WHERE 
		DataEntryValue <> '';

	/*  Now update to flag the records as deleted  */
	UPDATE 
		RecHubData.factDataEntryDetails
	SET
		IsDeleted = 1,
		ModificationDate = @parmModificationDate
	FROM
	    RecHubData.factDataEntryDetails
		INNER JOIN @factDataEntryDetails AS TempDetail
		    ON RecHubData.factDataEntryDetails.factDataEntryDetailKey = TempDetail.factDataEntryDetailKey;

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
