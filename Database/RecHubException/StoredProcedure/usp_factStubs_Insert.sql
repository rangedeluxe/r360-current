--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factStubs_Insert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factStubs_Insert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factStubs_Insert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factStubs_Insert
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmModificationDate DATETIME,
	@parmSequenceWithinTransaction INT,
	@parmBatchSequence INT,
	@parmStubSequence INT,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications	
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 10/03/2017
*
* Purpose: Update an existing factStubs record or add it if not exists
*
* Modification History
* 10/03/2017 PT 147282969	MGE Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	/* Create a table to hold the pivoted data */
	DECLARE @StubInfo TABLE
	(
		DepositDateKey INT,
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT,
		Amount MONEY,
		AccountNumber VARCHAR(80)
	);


	DECLARE @factStub TABLE
	(
		BankKey INT NOT NULL,
		OrganizationKey INT NOT NULL,
		ClientAccountKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ImmutableDateKey INT NOT NULL,
		SourceProcessingDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		SourceBatchID BIGINT NOT NULL,
		BatchNumber INT NOT NULL, 
		BatchSourceKey SMALLINT NOT NULL,
		BatchPaymentTypeKey TINYINT NOT NULL,
		BatchCueID INT NOT NULL,
		DepositStatus INT NOT NULL,
		SystemType TINYINT NOT NULL,
		TransactionID INT NOT NULL,
		TxnSequence INT NOT NULL,
		SequenceWithinTransaction INT NULL,
		BatchSequence INT NULL,
		StubSequence INT NULL,
		IsCorrespondence BIT NOT NULL,
		DocumentBatchSequence INT NULL,
		BatchSiteCode INT NULL,
		IsOMRDetected BIT NULL,
		Amount MONEY NULL,
		AccountNumber VARCHAR(80) NULL
	);


	/* Pivot and insert data into working table */
	INSERT INTO @StubInfo
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		BatchSequence,
		Amount,
		AccountNumber
	)
	SELECT 
		@parmDepositDateKey,
		@parmBatchID,
		@parmTransactionID,
		*
	FROM
	(
		SELECT
			BatchSequence,
			FieldName,
			DataEntryValue
		FROM
			@parmDataEntryValues
		WHERE
			FieldName = 'Amount' OR FieldName = 'AccountNumber'
			And IsCheck = 0
	) AS DataEntryData
	PIVOT
	(
		MAX(DataEntryValue) FOR FieldName IN (Amount,AccountNumber)
	)  AS PivotDataEntryData

	/* Add any stubs that do not have an amount or account number defined */
	;WITH MissingBatchSequence_CTE AS
	(
		SELECT BatchSequence FROM @parmDataEntryValues
		EXCEPT
		SELECT BatchSequence FROM @StubInfo
	)
	INSERT INTO @StubInfo
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		BatchSequence
	)
	SELECT 
		@parmDepositDateKey,
		@parmBatchID,
		@parmTransactionID,
		BatchSequence
	FROM 
		MissingBatchSequence_CTE;

	--SELECT * from @StubInfo order by BatchSequence ASC  --DEBUG ONLY
	;WITH factTransactionSummary_CTE AS
	(
		SELECT TOP 1
			RecHubData.factTransactionSummary.BankKey,
			RecHubData.factTransactionSummary.OrganizationKey,
			RecHubData.factTransactionSummary.ClientAccountKey,
			RecHubData.factTransactionSummary.DepositDateKey,
			RecHubData.factTransactionSummary.ImmutableDateKey,
			RecHubData.factTransactionSummary.SourceProcessingDateKey,
			RecHubData.factTransactionSummary.BatchID,
			RecHubData.factTransactionSummary.SourceBatchID,
			RecHubData.factTransactionSummary.BatchNumber, 
			RecHubData.factTransactionSummary.BatchSourceKey,
			RecHubData.factTransactionSummary.BatchPaymentTypeKey,
			RecHubData.factTransactionSummary.BatchCueID,
			RecHubData.factTransactionSummary.DepositStatus,
			RecHubData.factTransactionSummary.SystemType,
			RecHubData.factTransactionSummary.TransactionID,
			RecHubData.factTransactionSummary.TxnSequence,
			RecHubData.factTransactionSummary.BatchSiteCode
		FROM
			RecHubData.factTransactionSummary
		WHERE
			RecHubData.factTransactionSummary.BatchID = @parmBatchID
			AND RecHubData.factTransactionSummary.TransactionID = @parmTransactionID
			AND RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factTransactionSummary.IsDeleted = 0
	),
	factStubs_CTE AS 
	(
		SELECT TOP 1
			RecHubData.factStubs.BankKey,
			RecHubData.factStubs.OrganizationKey,
			RecHubData.factStubs.ClientAccountKey,
			RecHubData.factStubs.DepositDateKey,
			RecHubData.factStubs.ImmutableDateKey,
			RecHubData.factStubs.SourceProcessingDateKey,
			RecHubData.factStubs.BatchID,
			RecHubData.factStubs.SourceBatchID,
			RecHubData.factStubs.BatchNumber, 
			RecHubData.factStubs.BatchSourceKey,
			RecHubData.factStubs.BatchPaymentTypeKey,
			RecHubData.factStubs.BatchCueID,
			RecHubData.factStubs.DepositStatus,
			RecHubData.factStubs.SystemType,
			RecHubData.factStubs.TransactionID,
			RecHubData.factStubs.TxnSequence,
			RecHubData.factStubs.IsCorrespondence,
			RecHubData.factStubs.DocumentBatchSequence,
			RecHubData.factStubs.BatchSiteCode,
			RecHubData.factStubs.IsOMRDetected
		FROM
			RecHubData.factStubs
		WHERE
			RecHubData.factStubs.BatchID = @parmBatchID
			AND RecHubData.factStubs.TransactionID = @parmTransactionID
			AND RecHubData.factStubs.DepositDateKey = @parmDepositDateKey
			AND RecHubData.factStubs.IsDeleted = 0
	)
	INSERT INTO @factStub
	(
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber, 
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		IsCorrespondence,
		DocumentBatchSequence,
		BatchSiteCode,
		IsOMRDetected
	)
	SELECT
		COALESCE(factStubs_CTE.BankKey,factTransactionSummary_CTE.BankKey) AS BankKey,
		COALESCE(factStubs_CTE.OrganizationKey,factTransactionSummary_CTE.OrganizationKey) AS OrganizationKey,
		COALESCE(factStubs_CTE.ClientAccountKey,factTransactionSummary_CTE.ClientAccountKey) AS ClientAccountKey,
		COALESCE(factStubs_CTE.DepositDateKey,factTransactionSummary_CTE.DepositDateKey) AS DepositDateKey,
		COALESCE(factStubs_CTE.ImmutableDateKey,factTransactionSummary_CTE.ImmutableDateKey) AS ImmutableDateKey,
		COALESCE(factStubs_CTE.SourceProcessingDateKey,factTransactionSummary_CTE.SourceProcessingDateKey) AS SourceProcessingDateKey,
		COALESCE(factStubs_CTE.BatchID,factTransactionSummary_CTE.BatchID) AS BatchID,
		COALESCE(factStubs_CTE.SourceBatchID,factTransactionSummary_CTE.SourceBatchID) AS SourceBatchID,
		COALESCE(factStubs_CTE.BatchNumber,factTransactionSummary_CTE.BatchNumber) AS BatchNumber, 
		COALESCE(factStubs_CTE.BatchSourceKey,factTransactionSummary_CTE.BatchSourceKey) AS BatchSourceKey,
		COALESCE(factStubs_CTE.BatchPaymentTypeKey,factTransactionSummary_CTE.BatchPaymentTypeKey) AS BatchPaymentTypeKey,
		COALESCE(factStubs_CTE.BatchCueID,factTransactionSummary_CTE.BatchCueID) AS BatchCueID,
		COALESCE(factStubs_CTE.DepositStatus,factTransactionSummary_CTE.DepositStatus) AS DepositStatus,
		COALESCE(factStubs_CTE.SystemType,factTransactionSummary_CTE.SystemType) AS SystemType,
		COALESCE(factStubs_CTE.TransactionID,factTransactionSummary_CTE.TransactionID) AS DepositDateKey,
		COALESCE(factStubs_CTE.TxnSequence,factTransactionSummary_CTE.TxnSequence) AS TxnSequence,
		COALESCE(factStubs_CTE.IsCorrespondence,0) AS IsCorrespondence,
		factStubs_CTE.DocumentBatchSequence AS DocumentBatchSequence,
		COALESCE(factStubs_CTE.BatchSiteCode,factTransactionSummary_CTE.BatchSiteCode) AS BatchSiteCode,
		COALESCE(factStubs_CTE.IsOMRDetected,0) AS IsOMRDetected
	FROM
		factTransactionSummary_CTE
		FULL OUTER JOIN factStubs_CTE ON factStubs_CTE.BatchID = factTransactionSummary_CTE.BatchID
			AND factStubs_CTE.TransactionID = factTransactionSummary_CTE.TransactionID
			AND factStubs_CTE.TxnSequence = factTransactionSummary_CTE.TxnSequence;	
	
	--select * from @factStub		--DeBUG ONLY

	INSERT INTO RecHubData.factStubs
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber, 
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		CreationDate,
		ModificationDate,
		DocumentBatchSequence,
		BatchSiteCode,
		IsOMRDetected,
		Amount,
		AccountNumber
	)
	SELECT
		0 AS IsDeleted,
		factStub.BankKey,
		factStub.OrganizationKey,
		factStub.ClientAccountKey,
		factStub.DepositDateKey,
		factStub.ImmutableDateKey,
		factStub.SourceProcessingDateKey,
		factStub.BatchID,
		factStub.SourceBatchID,
		factStub.BatchNumber, 
		factStub.BatchSourceKey,
		factStub.BatchPaymentTypeKey,
		factStub.BatchCueID,
		factStub.DepositStatus,
		factStub.SystemType,
		factStub.TransactionID,
		factStub.TxnSequence,
		ABS(StubInfo.BatchSequence) + @parmSequenceWithinTransaction AS SequenceWithinTransaction,
		ABS(StubInfo.BatchSequence) + @parmBatchSequence  AS BatchSequence,
		ABS(StubInfo.BatchSequence) + @parmStubSequence AS StubSequence,
		factStub.IsCorrespondence,
		@parmModificationDate AS CreationDate,
		@parmModificationDate AS ModificationDate,
		factStub.DocumentBatchSequence,
		factStub.BatchSiteCode,
		factStub.IsOMRDetected,
		StubInfo.Amount,
		StubInfo.AccountNumber
	FROM 
		@factStub AS factStub
		INNER JOIN @StubInfo AS StubInfo ON StubInfo.TransactionID = factStub.TransactionID
	ORDER BY 
		StubInfo.BatchSequence DESC;

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH