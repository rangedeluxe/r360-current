--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_ExceptionBatches_Upd_BatchStatus
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_ExceptionBatches_Upd_BatchStatus') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_ExceptionBatches_Upd_BatchStatus
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_ExceptionBatches_Upd_BatchStatus 
(
	@parmExceptionBatchKey						BIGINT,
	@parmBatchStatusKey							SMALLINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/30/2014
*
* Purpose: Updates the BatchStatus column of the batch to the next state (e.g. "Pending")
*			Called by the application when checking in a transaction reports that
*			there are no more unresolved transactions in the batch.
*
* Modification History
* 04/30/2014 WI 139286 RDS	Created
* 10/10/2014 WI 139286 KLC	Updated auditing text (still commented out)
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Record Audit Trail
	/* TODO: Include this audit event if we can apply an appropriate "service" user ID...
	DECLARE @auditMessage VARCHAR(1024) = 'Exception DepositDate: ' + CAST(TODO: DepositDateKey as VARCHAR) + ', BatchID: ' + CAST(@parmExceptionBatchKey as VARCHAR) + ' - Complete'
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins 
			@parmApplicationName='RecHubException.usp_ExceptionBatches_Upd_BatchStatus', 
			@parmEventName='E Batch Complete', 
			@parmUserID = 0, -- This action is performed by the service automatically; we are just recording the time of the occurrence, not who did it
			@parmAuditMessage = @auditMessage
	*/

	-- Update all transactions in the given batch to a status of "Complete" / "Resolved"
	UPDATE RecHubException.ExceptionBatches
		SET BatchStatusKey = @parmBatchStatusKey
	WHERE ExceptionBatchKey = @parmExceptionBatchKey
		AND BatchStatusKey < @parmBatchStatusKey	-- This procedure should only be used to "advance" the status... silently fail if that condition isn't true...
	
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

