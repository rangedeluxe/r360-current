--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_Transactions_Get_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_Transactions_Get_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_Transactions_Get_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_Transactions_Get_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmCommonExceptionID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: RDS
* Date: 04/29/2014
*
* Purpose: Return Transaction Data for Post-Process Exceptions
*
* Modification History
* 04/29/2014 WI 139287 RDS	Created
* 05/14/2014 WI 141680 RDS	Add auditing
* 10/10/2014 WI 141680 KLC	Updated auditing text
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	-- Construct the audit message
	DECLARE @auditMessage VARCHAR(1024);
	SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
			+ ISNULL(', DepositDate: ' + CAST(RecHubException.ExceptionBatches.DepositDateKey as VARCHAR), '')
			+ ISNULL(', BatchID: ' + CAST(RecHubException.ExceptionBatches.BatchID as VARCHAR), '')
			+ ISNULL(', TransactionID: ' + CAST(RecHubException.Transactions.TransactionID as VARCHAR), '')
	FROM RecHubException.DecisioningTransactions
	LEFT JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	LEFT JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;
				
	-- Audit this event
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName = 'RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID',
			@parmEventName = 'E View Details',
			@parmEventType = 'Exceptions',
			@parmUserID = @parmUserID,
			@parmAuditMessage = @auditMessage

	-- Return Transaction Information
	SELECT
		RecHubException.DecisioningTransactions.TransactionStatusKey AS TransactionStatus,
		RecHubException.ExceptionBatches.DataEntrySetupKey,
		RecHubException.ExceptionBatches.BatchSourceKey,
		RecHubException.ExceptionBatches.BatchPaymentTypeKey
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	INNER JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH