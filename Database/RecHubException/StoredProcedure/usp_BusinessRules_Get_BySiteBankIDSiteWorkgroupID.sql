--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_BusinessRules_Get_BySiteBankIDSiteWorkgroupID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_BusinessRules_Get_BySiteBankIDSiteWorkgroupID') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_BusinessRules_Get_BySiteBankIDSiteWorkgroupID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_BusinessRules_Get_BySiteBankIDSiteWorkgroupID
(
	@parmSiteBankID			INT,
	@parmSiteWorkgroupID	INT,
	@parmDeadLineDay		SMALLINT OUTPUT,
	@parmRequiresInvoice	BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/29/2014
*
* Purpose: Reads the BusinessRules, Check Digits and Validation Tables for
*			the given workgroup.
*
* Modification History
* 04/29/2014 WI 138970 KLC	Created
* 10/01/2014 WI 168986 KLC	Add Exception Display Order
* 10/23/2014 WI 173973 KLC	Update to return RequiresInvoice
* 03/19/2015 WI 196676 BLR  No longer filtering on Session Entitlements, we'll
*                           doing this after the call.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @DataEntrySetupKey BIGINT; 

	SELECT  @DataEntrySetupKey = RecHubException.DataEntrySetups.DataEntrySetupKey,
			@parmDeadLineDay = RecHubException.DataEntrySetups.DeadLineDay,
			@parmRequiresInvoice = RecHubException.DataEntrySetups.RequiresInvoice
	FROM	RecHubException.DataEntrySetups
	WHERE	RecHubException.DataEntrySetups.SiteBankID = @parmSiteBankID
		AND	RecHubException.DataEntrySetups.SiteWorkGroupID = @parmSiteWorkgroupID;

	IF @DataEntrySetupKey IS NOT NULL
	BEGIN
		SELECT	RecHubException.DataEntrySetupFields.TableType,
				RecHubException.DataEntrySetupFields.FldName,
				RecHubException.DataEntrySetupFields.BatchSourceKey,
				RecHubException.DataEntrySetupFields.BatchPaymentTypeKey,
				RecHubException.DataEntrySetupFields.BatchPaymentSubTypeKey,
				RecHubException.DataEntrySetupFields.ExceptionDisplayOrder,
				RecHubException.BusinessRules.MinLength,
				RecHubException.BusinessRules.[MaxLength],
				RecHubException.BusinessRules.[Required],
				RecHubException.BusinessRules.UserCanEdit,
				RecHubException.BusinessRules.FormatType,
				RecHubException.BusinessRules.Mask,
				RecHubException.BusinessRules.RequiredMessage,
				RecHubException.CheckDigitRoutines.CheckDigitRoutineKey,
				RecHubException.CheckDigitRoutines.Offset,
				RecHubException.CheckDigitRoutines.Method,
				RecHubException.CheckDigitRoutines.Modulus,
				RecHubException.CheckDigitRoutines.Compliment,
				RecHubException.CheckDigitRoutines.WeightPatternDirection,
				RecHubException.CheckDigitRoutines.WeightPatternValue,
				RecHubException.CheckDigitRoutines.IgnoreSpaces,
				RecHubException.CheckDigitRoutines.Remainder10Replacement,
				RecHubException.CheckDigitRoutines.Remainder11Replacement,
				RecHubException.CheckDigitRoutines.FailureMessage AS CheckDigitFailureMessage,
				RecHubException.FieldValidations.FieldValidationKey,	
				RecHubException.FieldValidations.LookupRequired,
				RecHubException.FieldValidations.FailureMessage AS FieldValidationFailureMessage,
				RecHubException.FieldValidations.FieldValidationTypeKey,
				RecHubException.FieldValidations.FieldValidationSortMethodKey
		FROM RecHubException.DataEntrySetupFields
			JOIN RecHubException.BusinessRules
				ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey
			LEFT JOIN RecHubException.CheckDigitRoutines
				ON RecHubException.BusinessRules.CheckDigitRoutineKey = RecHubException.CheckDigitRoutines.CheckDigitRoutineKey
			LEFT JOIN RecHubException.FieldValidations
				ON RecHubException.BusinessRules.FieldValidationKey = RecHubException.FieldValidations.FieldValidationKey
		WHERE RecHubException.DataEntrySetupFields.DataEntrySetupKey = @DataEntrySetupKey;
	END

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

