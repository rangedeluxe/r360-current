--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_Wt
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_Wt') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_CommonExceptions_Wt;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_Wt
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Pierre Sula
* Date: 07/22/2013
*
* Purpose: adding  checksum value for GlobalBatchID and TransactionID 
*	
*
* Modification History
* 07/22/2013 WI 113087	PS	Created.
* 12/13/2013 WI 125100 JBS	Added a TRuncate statement if table exists.
* 05/08/2014 WI 138848 JBS  Change table name to IntegraPAYExceptions
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	IF OBJECT_ID('dbo.WT_CommonExceptBatchTransac', 'U') IS NOT NULL  
	--  If this exists the package failed and the table did not get cleaned up. This way we will start fresh.
	BEGIN
		TRUNCATE TABLE dbo.WT_CommonExceptBatchTransac
	END
	IF OBJECT_ID('dbo.WT_CommonExceptBatchTransac', 'U') IS  NULL
	BEGIN
		CREATE TABLE dbo.WT_CommonExceptBatchTransac
		(
			WT_CommonExceptBatchTransacID INT IDENTITY(1,1) NOT NULL,
			GlobalBatchID INT NULL,
			TransactionID INT NULL,
			CheckSumGlobBatchIDTransacID INT NULL,
			CreationDate DATETIME NULL
		) ;

		ALTER TABLE dbo.WT_CommonExceptBatchTransac 
		ADD  CONSTRAINT DF_WT_CommonExceptBatchTransac_CreationDate DEFAULT (GETDATE()) FOR CreationDate;
	END

	INSERT INTO WT_CommonExceptBatchTransac WITH(TABLOCK)
	 (  
		GlobalBatchID,
		TransactionID,
		CheckSumGlobBatchIDTransacID
	)
	SELECT DISTINCT  
		GlobalBatchID, 
		TransactionID,
		CheckSum(GlobalBatchID,TransactionID) AS  CheckSumGlobBatchIDTransacID
	FROM 
		RecHubException.IntegraPAYExceptions;
  
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
