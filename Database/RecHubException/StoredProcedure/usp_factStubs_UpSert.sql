--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factStubs_UpSert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factStubs_UpSert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factStubs_UpSert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factStubs_UpSert
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmModificationDate DATETIME,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Update an existing factStubs record or add it if not exists
*
* Modification History
* 08/24/2017 PT 149441599	JPB Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	/* Create a table to hold the pivoted data */
	DECLARE @StubInfo TABLE
	(
		DepositDateKey INT,
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT,
		Amount MONEY,
		AccountNumber VARCHAR(80)
	);


	DECLARE @factStub TABLE
	(
		BankKey INT NOT NULL,
		OrganizationKey INT NOT NULL,
		ClientAccountKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ImmutableDateKey INT NOT NULL,
		SourceProcessingDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		SourceBatchID BIGINT NOT NULL,
		BatchNumber INT NOT NULL, 
		BatchSourceKey SMALLINT NOT NULL,
		BatchPaymentTypeKey TINYINT NOT NULL,
		BatchCueID INT NOT NULL,
		DepositStatus INT NOT NULL,
		SystemType TINYINT NOT NULL,
		TransactionID INT NOT NULL,
		TxnSequence INT NOT NULL,
		SequenceWithinTransaction INT NOT NULL,
		BatchSequence INT NOT NULL,
		StubSequence INT NOT NULL,
		IsCorrespondence BIT NOT NULL,
		DocumentBatchSequence INT NULL,
		BatchSiteCode INT NULL,
		IsOMRDetected BIT NULL,
		Amount MONEY NULL,
		AccountNumber VARCHAR(80) NULL
	);

	/* Pivot and insert data into working table */
	INSERT INTO @StubInfo
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		BatchSequence,
		Amount,
		AccountNumber
	)
	SELECT 
		@parmDepositDateKey,
		@parmBatchID,
		@parmTransactionID,
		*
	FROM
	(
		SELECT
			BatchSequence,
			FieldName,
			DataEntryValue
		FROM
			@parmDataEntryValues
		WHERE
			FieldName = 'Amount' OR FieldName = 'AccountNumber'
	) AS DataEntryData
	PIVOT
	(
		MAX(DataEntryValue) FOR FieldName IN (Amount,AccountNumber)
	)  AS PivotDataEntryData;

	UPDATE
		RecHubData.factStubs
	SET
		RecHubData.factStubs.IsDeleted = 1,
		RecHubData.factStubs.ModificationDate = @parmModificationDate
	OUTPUT
		DELETED.BankKey,
		DELETED.OrganizationKey,
		DELETED.ClientAccountKey,
		DELETED.DepositDateKey,
		DELETED.ImmutableDateKey,
		DELETED.SourceProcessingDateKey,
		DELETED.BatchID,
		DELETED.SourceBatchID,
		DELETED.BatchNumber, 
		DELETED.BatchSourceKey,
		DELETED.BatchPaymentTypeKey,
		DELETED.BatchCueID,
		DELETED.DepositStatus,
		DELETED.SystemType,
		DELETED.TransactionID,
		DELETED.TxnSequence,
		DELETED.SequenceWithinTransaction,
		DELETED.BatchSequence,
		DELETED.StubSequence,
		DELETED.IsCorrespondence,
		DELETED.DocumentBatchSequence,
		DELETED.BatchSiteCode,
		DELETED.IsOMRDetected,
		StubInfo.Amount,
		StubInfo.AccountNumber
	INTO
		@factStub
	FROM
		RecHubData.factStubs
		INNER JOIN @StubInfo AS StubInfo ON StubInfo.DepositDateKey = RecHubData.factStubs.DepositDateKey
			AND StubInfo.BatchID = RecHubData.factStubs.BatchID
			AND StubInfo.TransactionID = RecHubData.factStubs.TransactionID
			AND StubInfo.BatchSequence = RecHubData.factStubs.BatchSequence
	WHERE
		RecHubData.factStubs.IsDeleted = 0
		AND (
				RecHubData.factStubs.Amount <> StubInfo.Amount
				OR RecHubData.factStubs.AccountNumber <> StubInfo.AccountNumber
			);

	INSERT INTO RecHubData.factStubs
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber, 
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		CreationDate,
		ModificationDate,
		DocumentBatchSequence,
		BatchSiteCode,
		IsOMRDetected,
		Amount,
		AccountNumber
	)
	SELECT
		0 AS IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber, 
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		@parmModificationDate AS CreationDate,
		@parmModificationDate AS ModificationDate,
		DocumentBatchSequence,
		BatchSiteCode,
		IsOMRDetected,
		Amount,
		AccountNumber
	FROM 
		@factStub

END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH