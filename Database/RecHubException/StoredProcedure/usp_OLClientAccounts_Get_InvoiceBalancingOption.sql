--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_OLClientAccounts_Get_InvoiceBalancingOption
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_OLClientAccounts_Get_InvoiceBalancingOption') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_OLClientAccounts_Get_InvoiceBalancingOption
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_OLClientAccounts_Get_InvoiceBalancingOption
(
    @parmCommonExceptionID		INT,
    @parmUserID					INT,
    @parmInvoiceBalancingOption	INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 09/25/2013
*
* Purpose: Locate the Invoice Balancing Option set for this OLClientAccount
*
*
* Modification History
* 09/25/2013 WI 114954 TWE Created
* 04/25/2014 WI 139284 RDS	Updates for IntegraPAYExceptions table name change
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

    SELECT 
        @parmInvoiceBalancingOption = RecHubUser.OLClientAccounts.InvoiceBalancingOption
    FROM
		RecHubException.DecisioningTransactions
		INNER JOIN RecHubException.IntegraPAYExceptions ON
			RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
		INNER Join RecHubUser.OLClientAccounts ON	
			RecHubException.IntegraPAYExceptions.SiteBankID = RecHubUser.OLClientAccounts.SiteBankID     
            AND RecHubException.IntegraPAYExceptions.SiteClientAccountID = RecHubUser.OLClientAccounts.SiteClientAccountID
		INNER JOIN RecHubUser.Users ON 
            RecHubUser.Users.OLOrganizationID = RecHubUser.OLClientAccounts.OLOrganizationID
    WHERE
        RecHubUser.Users.UserID = @parmUserID
		and RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
