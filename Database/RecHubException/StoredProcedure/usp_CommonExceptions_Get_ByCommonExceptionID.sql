--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_Get_ByCommonExceptionID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID 
(
	@parmUserID				INT,
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmCommonExceptionID	INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: Returns a Common Exception visible to the provided user, by ID
*
* Modification History
* 07/29/2013 WI 110164 RDS	Created
* 04/25/2014 WI 139279 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
* 05/14/2014 WI 141672 RDS	Add auditing
* 07/29/2014 WI 156206 RDS	Update for new SessionEntitlements
* 08/20/2014 WI 156206 KLC	Added Transaction ID to returned data
* 09/08/2014 WI 156206 KLC	Updated to return workgroup display name
* 10/09/2014 WI 156206 KLC	Adde return of ImportType Short Name
* 10/10/2014 WI 141672 KLC	Updated auditing text
* 10/29/2014 WI 174865 KLC	Changed return of ImportType Short Name to Batch Source Short Name
* 11/10/2014 WI 174865 KLC	Updated to return both Batch Source and Import Type Short Names
* 12/17/2014 WI 182286 LA	Added BatchPaymentTypeKey to Output
******************************************************************************/
SET NOCOUNT ON;

DECLARE @deadlineBuffer INT;
DECLARE @currentDate DATETIME = CONVERT(DATE, GETDATE());

BEGIN TRY

	-- Construct the audit message
	DECLARE @auditMessage VARCHAR(1024);
	SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
			+ ISNULL(', GlobalBatchID: ' + CAST(RecHubException.IntegraPAYExceptions.GlobalBatchID as VARCHAR), '')
			+ ISNULL(', TransactionID: ' + CAST(RecHubException.IntegraPAYExceptions.TransactionID as VARCHAR), '')
			+ ISNULL(', DepositDate: ' + CAST(RecHubException.ExceptionBatches.DepositDateKey as VARCHAR), '')
			+ ISNULL(', BatchID: ' + CAST(RecHubException.ExceptionBatches.BatchID as VARCHAR), '')
			+ ISNULL(', TransactionID: ' + CAST(RecHubException.Transactions.TransactionID as VARCHAR), '')
	FROM RecHubException.DecisioningTransactions
	LEFT JOIN RecHubException.IntegraPAYExceptions ON
		RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
	LEFT JOIN RecHubException.Transactions ON
		RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
	LEFT JOIN RecHubException.ExceptionBatches ON
		RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
	WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;
				
	-- Audit this event
	EXEC RecHubCommon.usp_WFS_EventAudit_Ins
			@parmApplicationName = 'RecHubException.usp_CommonExceptions_Get_ByCommonExceptionID',
			@parmEventName = 'E View Summary',
			@parmEventType = 'Exceptions',
			@parmUserID = @parmUserID,
			@parmAuditMessage = @auditMessage

	/* retrieve the deadline buffer minutes to apply */
	SELECT
		@deadlineBuffer = CASE WHEN ISNUMERIC(DefaultSetting) > 0 THEN DefaultSetting ELSE 0 END
	FROM 
		RecHubUser.OLPreferences
	WHERE
		PreferenceName = 'exceptiondeadlinebufferminutes'
	
	IF @deadlineBuffer IS NULL
		SET @deadlineBuffer = 0

	-- Return IntegraPAY fields, if applicable
	;WITH DistinctClientAccounts AS
	(
		SELECT DISTINCT
			RecHubUser.SessionClientAccountEntitlements.SiteBankID,
			RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID,
			RecHubData.dimClientAccountsView.DisplayLabel,
			RecHubUser.OLWorkgroups.EntityID,
			CAST(COALESCE(RecHubUser.OLWorkgroups.DisplayBatchID, RecHubUser.OLEntities.DisplayBatchID, 0) AS BIT) AS DisplayBatchID
		FROM
			RecHubUser.SessionClientAccountEntitlements
		INNER JOIN RecHubData.dimClientAccountsView
			ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubData.dimClientAccountsView.SiteBankID
				AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubData.dimClientAccountsView.SiteClientAccountID
		INNER JOIN RecHubUser.OLWorkgroups
			ON RecHubUser.SessionClientAccountEntitlements.SiteBankID = RecHubUser.OLWorkgroups.SiteBankID
				AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = RecHubUser.OLWorkgroups.SiteClientAccountID
				AND RecHubUser.SessionClientAccountEntitlements.EntityID = RecHubUser.OLWorkgroups.EntityID
		INNER JOIN RecHubUser.OLEntities ON
			RecHubUser.OLWorkgroups.EntityID = RecHubUser.OLEntities.EntityID
		WHERE RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
	)
	SELECT
		RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
		RecHubException.IntegraPAYExceptions.SiteBankID,
		RecHubException.IntegraPAYExceptions.SiteClientAccountID,
		DistinctClientAccounts.DisplayLabel AS LongName,
		RecHubException.IntegraPAYExceptions.DepositDateKey,
		RecHubException.IntegraPAYExceptions.BatchID,
		RecHubException.IntegraPAYExceptions.BatchID AS SourceBatchID,
		RecHubException.IntegraPAYExceptions.ImmutableDateKey,
		RecHubException.IntegraPAYExceptions.CheckAmount,
		DATEADD(minute, - @deadlineBuffer, RecHubException.IntegraPAYExceptions.Deadline) AS Deadline,
		RecHubException.IntegraPAYExceptions.InProcessException,
		RecHubException.IntegraPAYExceptions.DecisioningStatus,
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey as BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
		RecHubData.dimBatchSources.LongName AS BatchSourceName,
		COALESCE(RecHubException.DecisioningTransactions.UserID, 0) AS Locked,
		CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
		COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
		RecHubException.IntegraPAYExceptions.TransactionID,
		DistinctClientAccounts.EntityID,
		RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
		RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
		DistinctClientAccounts.DisplayBatchID
	FROM
		RecHubException.DecisioningTransactions 
	INNER JOIN RecHubException.IntegraPAYExceptions ON
		RecHubException.DecisioningTransactions.IntegraPAYExceptionKey =
			RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
	INNER JOIN RecHubData.dimBatchSources ON
		RecHubException.IntegraPAYExceptions.BatchSourceKey = 
			RecHubData.dimBatchSources.BatchSourceKey
	INNER JOIN RecHubData.dimImportTypes ON
		RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	INNER JOIN RecHubData.dimBatchPaymentTypes ON
		RecHubException.IntegraPAYExceptions.BatchPaymentTypeKey =
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
	INNER JOIN DistinctClientAccounts ON
		DistinctClientAccounts.SiteBankID = 
			RecHubException.IntegraPAYExceptions.SiteBankID
		AND DistinctClientAccounts.SiteClientAccountID = 
			RecHubException.IntegraPAYExceptions.SiteClientAccountID
	LEFT OUTER JOIN RecHubUser.Users ON
		RecHubException.DecisioningTransactions.UserID =
			RecHubUser.Users.UserID
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID

	UNION ALL

	-- Return Post-Process fields, if applicable
	SELECT
		RecHubException.DecisioningTransactions.DecisioningTransactionKey AS CommonExceptionID,
		RecHubException.ExceptionBatches.SiteBankID,
		RecHubException.ExceptionBatches.SiteWorkGroupID AS SiteClientAccountID,
		DistinctClientAccounts.DisplayLabel AS LongName,
		RecHubException.ExceptionBatches.DepositDateKey,
		RecHubException.ExceptionBatches.BatchID,
		RecHubException.ExceptionBatches.SourceBatchID,
		RecHubException.ExceptionBatches.ImmutableDateKey,
		(SELECT SUM(Amount) FROM RecHubException.Payments WHERE TransactionKey = T.TransactionKey)  AS CheckAmount,
		CASE WHEN RecHubException.DataEntrySetups.DeadLineDay IS NULL THEN NULL
			ELSE DATEADD(mi, RecHubException.DataEntrySetups.DeadLineDay%100, DATEADD(hh, RecHubException.DataEntrySetups.DeadLineDay/100, @currentDate)) END AS Deadline,
		CAST(0 AS BIT) AS InProcessException,	-- These tables by definition contain Post-Process exceptions
			(RecHubException.DecisioningTransactions.TransactionStatusKey - 1) AS DecisioningStatus,	-- Note: TransactionStatus values are one greater than CEDecisioning status values...  So we subtract one...
		RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey as BatchPaymentTypeKey,
		RecHubData.dimBatchPaymentTypes.LongName as BatchPaymentTypeName,
		RecHubData.dimBatchSources.LongName AS BatchSourceName,
		COALESCE(RecHubException.DecisioningTransactions.UserID,0) AS Locked,
		CASE WHEN RecHubException.DecisioningTransactions.UserID IS NULL THEN 0 ELSE COALESCE(RecHubException.DecisioningTransactions.ModificationDate,0) END AS LockTime,
		COALESCE(RecHubUser.Users.LogonName, '') AS LockOwnerName,
		T.TransactionID,
		DistinctClientAccounts.EntityID,
		RecHubUser.Users.LogonEntityID as LockOwnerEntityID,
		RecHubData.dimBatchSources.ShortName as BatchSourceShortName,
		RecHubData.dimImportTypes.ShortName as ImportTypeShortName,
		DistinctClientAccounts.DisplayBatchID
	FROM
		RecHubException.DecisioningTransactions
	INNER JOIN RecHubException.Transactions T ON
		RecHubException.DecisioningTransactions.TransactionKey = T.TransactionKey
	INNER JOIN RecHubException.ExceptionBatches ON
		T.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
	INNER JOIN RecHubData.dimBatchSources ON
		RecHubException.ExceptionBatches.BatchSourceKey = 
			RecHubData.dimBatchSources.BatchSourceKey
	INNER JOIN RecHubData.dimImportTypes ON
		RecHubData.dimBatchSources.ImportTypeKey = RecHubData.dimImportTypes.ImportTypeKey
	INNER JOIN RecHubData.dimBatchPaymentTypes ON
		RecHubException.ExceptionBatches.BatchPaymentTypeKey =
			RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey
	INNER JOIN DistinctClientAccounts ON
		DistinctClientAccounts.SiteBankID = 
			RecHubException.ExceptionBatches.SiteBankID
		AND DistinctClientAccounts.SiteClientAccountID = 
			RecHubException.ExceptionBatches.SiteWorkGroupID
	INNER JOIN RecHubException.DataEntrySetups ON 
		RecHubException.ExceptionBatches.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
		AND RecHubException.ExceptionBatches.SiteWorkgroupID = RecHubException.DataEntrySetups.SiteWorkgroupID
	LEFT OUTER JOIN RecHubUser.Users ON
		RecHubException.DecisioningTransactions.UserID =
			RecHubUser.Users.UserID
	WHERE 
		RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH