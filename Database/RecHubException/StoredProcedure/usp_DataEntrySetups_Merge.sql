--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_DataEntrySetups_Merge
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_DataEntrySetups_Merge') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_DataEntrySetups_Merge
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_DataEntrySetups_Merge
(
	@parmEntityID				INT,
	@parmSiteBankID				INT,
	@parmWorkgroupID			INT,
	@parmDeadLineDay			SMALLINT,
	@parmRequiresInvoice		BIT,
	@parmBusinessRules			XML,
	@parmUserID					INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 05/12/2014
*
* Purpose: Updates the DataEntrySetup and then it's corresponding business
*			rules
*
* Modification History
* 05/12/2014 WI 141063 KLC	Created
* 10/23/2014 WI	173975 KLC	Updated to handle RequiresInvoice column
******************************************************************************/
DECLARE @dataEntrySetupKey				BIGINT,
		@prevDeadLineDay				SMALLINT,
		@prevRequiresInvoice			BIT,
		@AuditColumns					RecHubCommon.AuditValueChangeTable,
		@errorDescription				VARCHAR(1024),
		@auditMessage					VARCHAR(1024);

SET NOCOUNT ON;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	IF NOT EXISTS(SELECT * FROM RecHubUser.Users WHERE UserID = @parmUserID)
	BEGIN
		SET @errorDescription = 'Unable to update data entry setup in RecHubException.DataEntrySetups, user (' + ISNULL(CAST(@parmUserID AS VARCHAR(10)), 'NULL') + ' does not exist';
		RAISERROR(@errorDescription, 16, 1);
	END
	
	IF NOT EXISTS(SELECT * FROM RecHubData.dimClientAccounts WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmWorkgroupID)
	BEGIN
		SET @errorDescription = 'Unable to update data entry setups in RecHubException.ClientAccountsDataEntryColumns, the RecHubException.dimClientAccounts record does not exist for SiteBankID ' + CAST(@parmSiteBankID AS VARCHAR(10)) + ', SiteWorkgroupID' + CAST(@parmWorkgroupID AS VARCHAR(10));
		RAISERROR(@errorDescription, 16, 2);
	END

	DECLARE @curTime datetime = GETDATE();

	UPDATE	RecHubException.DataEntrySetups
	SET		RecHubException.DataEntrySetups.DeadLineDay = @parmDeadLineDay,
			RecHubException.DataEntrySetups.RequiresInvoice = @parmRequiresInvoice,
			@prevDeadLineDay = RecHubException.DataEntrySetups.DeadLineDay,
			@prevRequiresInvoice = RecHubException.DataEntrySetups.RequiresInvoice,
			@dataEntrySetupKey = RecHubException.DataEntrySetups.DataEntrySetupKey
	WHERE	RecHubException.DataEntrySetups.SiteBankID = @parmSiteBankID
		AND	RecHubException.DataEntrySetups.SiteWorkGroupID = @parmWorkgroupID;

	IF @dataEntrySetupKey IS NOT NULL
	BEGIN
		IF (COALESCE(@parmDeadLineDay, -1) <> COALESCE(@prevDeadLineDay, -1) OR @parmRequiresInvoice <> @prevRequiresInvoice)
		BEGIN
			IF COALESCE(@parmDeadLineDay, -1) <> COALESCE(@prevDeadLineDay, -1)
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('DeadLineDay', COALESCE(CAST(@prevDeadLineDay AS VARCHAR(5)), 'NULL'), COALESCE(CAST(@parmDeadLineDay AS VARCHAR(5)), 'NULL'));
			IF @parmRequiresInvoice <> @prevRequiresInvoice
				INSERT INTO @AuditColumns(ColumnName, OldValue, NewValue) VALUES('RequiresInvoice', CAST(@prevRequiresInvoice AS VARCHAR(1)), CAST(@parmRequiresInvoice AS VARCHAR(1)));

			-- Audit the RecHubException.DataEntrySetups update
			SET @auditMessage = 'Modified DataEntrySetups for EntityID: ' + CAST(@parmEntityID AS VARCHAR(10))
				+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
				+ ' WorkgroupID: ' + CAST(@parmWorkgroupID AS VARCHAR(10))
				+ ' DataEntrySetupKey: ' + CAST(@dataEntrySetupKey AS VARCHAR(10)) + '.';
			EXEC RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
				@parmUserID				= @parmUserID,
				@parmApplicationName	= 'RecHubException.usp_DataEntrySetups_Merge',
				@parmSchemaName			= 'RecHubException',
				@parmTableName			= 'DataEntrySetups',
				@parmColumnName			= 'DataEntrySetupKey',
				@parmAuditValue			= @dataEntrySetupKey,
				@parmAuditType			= 'UPD',
				@parmAuditColumns		= @AuditColumns,
				@parmAuditMessage		= @auditMessage;
		END
	END
	ELSE
	BEGIN
		INSERT INTO RecHubException.DataEntrySetups(SiteBankID, SiteWorkgroupID, DeadLineDay, RequiresInvoice, CreationDate, ModificationDate, CreatedBy, ModifiedBy)
		VALUES (@parmSiteBankID, @parmWorkgroupID, @parmDeadLineDay, @parmRequiresInvoice, @curTime, @curTime, @parmUserID, @parmUserID);

		SET @dataEntrySetupKey = SCOPE_IDENTITY();

		SET @auditMessage = 'Added DataEntrySetups for EntityID:' + CAST(@parmEntityID AS VARCHAR(10))
			+ ' SiteBankID: ' + CAST(@parmSiteBankID AS VARCHAR(10))
			+ ' WorkgroupID: ' + CAST(@parmWorkgroupID AS VARCHAR(10))
			+ ' DataEntrySetupKey: ' + CAST(@dataEntrySetupKey AS VARCHAR(10))
			+ ': DeadLineDay = ' + COALESCE(CAST(@parmDeadLineDay AS VARCHAR(5)), 'NULL')
			+ ', RequiresInvoice = ' + CAST(@parmRequiresInvoice AS VARCHAR(1))
			+ '.';
		EXEC RecHubCommon.usp_WFS_DataAudit_Ins
			@parmUserID				= @parmUserID,
			@parmApplicationName	= 'RecHubException.usp_DataEntrySetups_Merge',
			@parmSchemaName			= 'RecHubException',
			@parmTableName			= 'DataEntrySetups',
			@parmColumnName			= 'SiteBankID',
			@parmAuditValue			= @dataEntrySetupKey,
			@parmAuditType			= 'INS',
			@parmAuditMessage		= @auditMessage;
	END
	
	EXEC RecHubException.usp_BusinessRules_Merge
		@parmEntityID = @parmEntityID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmWorkgroupID,
		@parmDataEntrySetupKey = @dataEntrySetupKey,
		@parmBusinessRules = @parmBusinessRules,
		@parmUserID = @parmUserID;

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

