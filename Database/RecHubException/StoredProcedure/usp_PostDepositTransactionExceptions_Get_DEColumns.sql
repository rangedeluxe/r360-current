--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_PostDepositTransactionExceptions_Get_DEColumns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_PostDepositTransactionExceptions_Get_DEColumns') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Get_DEColumns
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositTransactionExceptions_Get_DEColumns 
(
	@parmSessionID				UNIQUEIDENTIFIER,
	@parmSiteBankID				INT, 
	@parmSiteClientAccountID	INT,
	@parmDepositDate			DATETIME,
	@parmBatchID				BIGINT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 09/11/2017
*
* Purpose: Query Data Entry Summary fact table for batch DE Setup information.
*
* Modification History
* 09/11/2017 PT	149441599	JPB Created (Adapted from RecHubData.usp_factDataEntryDetails_Get_DEColumns)
* 10/06/2017 PT 151106278   MAA Added is required flag
* 10/10/2017 PT	147282969	MGE Added WorkgroupDataEntryColumn to column list returned
******************************************************************************/
SET NOCOUNT ON; 

DECLARE 
	@StartDateKey INT,
	@EndDateKey INT,
	@BatchSourceKey	INT;
	
BEGIN TRY
	
	/* Ensure the start date is not beyond the max viewing days */
	EXEC RecHubUser.usp_AdjustStartDateForViewingDays
		@parmSessionID = @parmSessionID,
		@parmSiteBankID = @parmSiteBankID,
		@parmSiteClientAccountID = @parmSiteClientAccountID,
		@parmDepositDateStart = @parmDepositDate,
		@parmDepositDateEnd = @parmDepositDate,
		@parmStartDateKey = @StartDateKey OUT,
		@parmEndDateKey = @EndDateKey OUT;

	SELECT 
		@BatchSourceKey = BatchSourceKey 
	FROM 
		RecHubData.factBatchSummary
	WHERE 
		BatchID = @parmBatchID
	OPTION( RECOMPILE );

	;WITH factDataEntryDetails_CTE AS
	(
		SELECT	DISTINCT
			RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey,
			COALESCE(RecHubData.dimWorkgroupDataEntryColumns.UILabel, RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldTitle,
			RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldName,
			CASE
			   WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 THEN 'Checks'
			   ELSE 'Stubs'
			END AS 'TableName',
			CASE
				WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount') 
				THEN 7
				ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
			END AS FldDataTypeEnum,
			RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder,
			dimWorkgroupDataEntryColumns.IsRequired,
			RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey	
		FROM	
			RecHubData.factDataEntryDetails
			INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON 
				RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey			
			INNER JOIN RecHubUser.SessionClientAccountEntitlements ON										--WI 142843
				RecHubData.factDataEntryDetails.ClientAccountKey = RecHubUser.SessionClientAccountEntitlements.ClientAccountKey
	
		WHERE  	
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID							--WI 142843		
			AND RecHubUser.SessionClientAccountEntitlements.SiteBankID = @parmSiteBankID
			AND RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID = @parmSiteClientAccountID	--WI 142843
			AND RecHubData.factDataEntryDetails.DepositDateKey = @EndDateKey								--WI 142843	 WI 197805					
			AND RecHubData.factDataEntryDetails.BatchID = @parmBatchID
			AND RecHubData.factDataEntryDetails.IsDeleted = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
	),
	RequiredDataEntryFields_CTE AS
	(
		SELECT
			COALESCE(RecHubData.dimWorkgroupDataEntryColumns.UILabel, RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FldTitle,
			RecHubData.dimWorkgroupDataEntryColumns.FieldName AS FldName,
			CASE
			   WHEN RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 1 THEN 'Checks'
			   ELSE 'Stubs'
			END AS 'TableName',
			CASE
				WHEN (RTRIM(RecHubData.dimWorkgroupDataEntryColumns.FieldName) = 'Amount') 
				THEN 7
				ELSE RecHubData.dimWorkgroupDataEntryColumns.DataType
			END AS FldDataTypeEnum,
			ScreenOrder,
			RecHubData.dimWorkgroupDataEntryColumns.IsRequired,
			WorkgroupDataEntryColumnKey
		FROM
			RecHubData.dimWorkgroupDataEntryColumns
		WHERE 
			RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey = @BatchSourceKey
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = @parmSiteBankID
			AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = @parmSiteClientAccountID
			AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
			AND RecHubData.dimWorkgroupDataEntryColumns.IsActive = 1
			AND RecHubData.dimWorkgroupDataEntryColumns.IsRequired = 1
	)
	SELECT 
		COALESCE(RequiredDataEntryFields_CTE.ScreenOrder,factDataEntryDetails_CTE.ScreenOrder) AS ScreenOrder,
		COALESCE(RequiredDataEntryFields_CTE.FldTitle,factDataEntryDetails_CTE.FldTitle) AS FldTitle,
		COALESCE(RequiredDataEntryFields_CTE.FldName,factDataEntryDetails_CTE.FldName) AS FldName,
		COALESCE(RequiredDataEntryFields_CTE.TableName,factDataEntryDetails_CTE.TableName) AS TableName,
		COALESCE(RequiredDataEntryFields_CTE.FldDataTypeEnum,factDataEntryDetails_CTE.FldDataTypeEnum) AS FldDataTypeEnum,
		COALESCE(RequiredDataEntryFields_CTE.IsRequired,factDataEntryDetails_CTE.IsRequired) AS IsRequired,
		COALESCE(RequiredDataEntryFields_CTE.WorkgroupDataEntryColumnKey,factDataEntryDetails_CTE.WorkgroupDataEntryColumnKey) AS WorkgroupDataEntryColumnKey
	FROM
		RequiredDataEntryFields_CTE
		FULL OUTER JOIN factDataEntryDetails_CTE ON factDataEntryDetails_CTE.TableName = RequiredDataEntryFields_CTE.TableName
			AND factDataEntryDetails_CTE.FldName = RequiredDataEntryFields_CTE.FldName
	ORDER BY
		ScreenOrder ASC
	OPTION( RECOMPILE );

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH