--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_DataEntrySetups_Get_Key_BySiteBankIDWorkgroupID
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_DataEntrySetups_Get_Key_BySiteBankIDWorkgroupID') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_DataEntrySetups_Get_Key_BySiteBankIDWorkgroupID
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_DataEntrySetups_Get_Key_BySiteBankIDWorkgroupID
(
	@parmSiteBankID			INT,
	@parmWorkgroupID		INT,
	@parmDataEntrySetupKey	BIGINT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/04/2014
*
* Purpose: Retreived the data entry setup key for a given workgroup
*
* Modification History
* 06/04/2014 WI 145692 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	@parmDataEntrySetupKey = RecHubException.DataEntrySetups.DataEntrySetupKey
	FROM	RecHubException.DataEntrySetups
	WHERE	RecHubException.DataEntrySetups.SiteBankID = @parmSiteBankID
		AND RecHubException.DataEntrySetups.SiteWorkgroupID = @parmWorkgroupID;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

