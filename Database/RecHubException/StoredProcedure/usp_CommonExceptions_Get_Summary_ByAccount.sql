--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_Get_Summary_ByAccount 
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_Get_Summary_ByAccount') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_CommonExceptions_Get_Summary_ByAccount 
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_Get_Summary_ByAccount
(
	@parmSessionID			UNIQUEIDENTIFIER,
	@parmEntities			XML,
	@parmSiteBankID			INT,
	@parmWorkgroupID		INT,
	@parmDateTo				INT,
	@parmDateFrom			INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: DRP
* Date: 06/20/2013
*
* Purpose: Summarize exception item by count and amount, grouped by exception type and disposition.
*
* Modification History
* 06/20/2013 WI 101787 DRP	Created
* 07/26/2013 WI 101787 JBS  Changing called proc from usp_OLClientAccounts_Get_ByUserID
* 10/30/2012 WI 119497 CMC  Updated to use new procedures that select the pruned
*								org - client/account tree
* 11/07/2013 WI 121748 CMC  Select the distinct client accounts
* 11/26/2013 WI 123623 CMC	Apply Deadline Restrictions
* 12/04/2013 WI 124292 EAS  Order Results by LongName needed for Charts
* 05/23/2014 WI 143868 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process Exceptions
* 07/09/2014 WI 151287 KLC	Updated for RAAM integration
* 09/22/2014 WI 151287 JBS	Change Evaluations on TransactionStatusKey. 
* 01/13/2015 WI 183844 LA	Updated to filer out inactive workgroups
* 01/15/2015 WI 184100 BLR  Updated to include 2 date params, grouping by 
*                               post-process and in-process now.
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

BEGIN TRY

        DECLARE @Today INT = CONVERT(VARCHAR(30),  GETDATE(), 112),
				@CurrentTime VARCHAR(8) = CONVERT(VARCHAR(8), GETDATE(), 108);

        --Get the workgroup or list of all workgroups for the entities requested
        DECLARE @tblWorkgroups TABLE (SiteBankID INT, SiteClientAccountID INT, EntityID INT, PRIMARY KEY(SiteBankID, SiteClientAccountID));
        IF @parmWorkgroupID IS NOT NULL
        BEGIN
               INSERT INTO @tblWorkgroups(SiteBankID, SiteClientAccountID, EntityID)
               SELECT DISTINCT
                    RecHubUser.OLWorkgroups.SiteBankID,
                    RecHubUser.OLWorkgroups.SiteClientAccountID,
                    RecHubUser.OLWorkgroups.EntityID
               FROM RecHubUser.OLWorkgroups
                    JOIN RecHubUser.SessionClientAccountEntitlements
						ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
						AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
						AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
						AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
					INNER JOIN RecHubData.dimClientAccountsView 
						ON RecHubData.dimClientAccountsView.SiteCLientAccountId = RecHubUser.OLWorkgroups.SiteClientAccountID 
                        AND RecHubData.dimClientAccountsView.IsActive = 1
               WHERE RecHubUser.OLWorkgroups.SiteBankID = @parmSiteBankID
					AND RecHubUser.OLWorkgroups.SiteClientAccountID = @parmWorkgroupID;

        END
        ELSE
        BEGIN
               INSERT INTO @tblWorkgroups(SiteBankID, SiteClientAccountID, EntityID)
               SELECT DISTINCT
					RecHubUser.OLWorkgroups.SiteBankID,
					RecHubUser.OLWorkgroups.SiteClientAccountID,
					RecHubUser.OLWorkgroups.EntityID
               FROM @parmEntities.nodes('/ENTS/ENT') AS Entities(Ent)
					JOIN RecHubUser.OLWorkgroups
						ON Entities.Ent.value('.', 'INT') = RecHubUser.OLWorkgroups.EntityID
					JOIN RecHubUser.SessionClientAccountEntitlements
						ON RecHubUser.OLWorkgroups.SiteBankID = RecHubUser.SessionClientAccountEntitlements.SiteBankID
							AND RecHubUser.OLWorkgroups.SiteClientAccountID = RecHubUser.SessionClientAccountEntitlements.SiteClientAccountID
							AND RecHubUser.OLWorkgroups.EntityID = RecHubUser.SessionClientAccountEntitlements.EntityID --if these don't match, workgroup was moved and should not be displayed
							AND RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID
					INNER JOIN RecHubData.dimClientAccountsView 
						ON RecHubData.dimClientAccountsView.SiteCLientAccountId = RecHubUser.OLWorkgroups.SiteClientAccountID 
							AND RecHubData.dimClientAccountsView.IsActive = 1
        END

        CREATE TABLE #TodaysExceptions 
        (
               CheckAmount            MONEY,  
               DecisioningStatus      TINYINT,
               BatchPaymentTypeKey    TINYINT,
			   ProcessType		      VarChar(20)
        );

        INSERT INTO #TodaysExceptions 
        (
               CheckAmount,       
               DecisioningStatus,
               BatchPaymentTypeKey,
			   ProcessType
        )
               SELECT 
					ISNULL(RecHubException.IntegraPAYExceptions.CheckAmount, (SELECT SUM(Amount) FROM RecHubException.Payments WHERE TransactionKey = RecHubException.Transactions.TransactionKey)) AS CheckAmount,
					CASE WHEN RecHubException.DecisioningTransactions.TransactionStatusKey in (2, 3)
						AND RecHubException.IntegraPAYExceptions.Deadline IS NOT NULL
						AND CONVERT(VARCHAR(8), RecHubException.IntegraPAYExceptions.Deadline, 108) < @CurrentTime
						THEN 1 -- deadline has passed, so it should go back to unresolved
						ELSE
								RecHubException.DecisioningTransactions.TransactionStatusKey
						END AS DecisioningStatus,
					ISNULL(RecHubException.IntegraPAYExceptions.BatchPaymentTypeKey, RecHubException.ExceptionBatches.BatchPaymentTypeKey) AS BatchPaymentTypeKey,
					CASE WHEN RecHubException.DecisioningTransactions.IntegraPAYExceptionKey IS NULL
						THEN 'Post-Process'
					ELSE
						'In-Process'
					END AS ProcessType
               FROM RecHubException.DecisioningTransactions
                    LEFT JOIN RecHubException.IntegraPAYExceptions
						ON RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
						AND RecHubException.IntegraPAYExceptions.ImmutableDateKey = @Today
                    LEFT JOIN @tblWorkgroups AS tblClientAccounts
                        ON RecHubException.IntegraPAYExceptions.SiteClientAccountID = tblClientAccounts.SiteClientAccountID 
                        AND RecHubException.IntegraPAYExceptions.SiteBankID = tblClientAccounts.SiteBankID
                    LEFT JOIN RecHubException.Transactions
                        ON RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
                    LEFT JOIN RecHubException.ExceptionBatches
                        ON RecHubException.Transactions.ExceptionBatchKey = RecHubException.ExceptionBatches.ExceptionBatchKey
                        AND RecHubException.ExceptionBatches.DepositDateKey BETWEEN @parmDateFrom and @parmDateTo
                    LEFT JOIN @tblWorkgroups AS tblClientAccounts2
                        ON RecHubException.ExceptionBatches.SiteWorkgroupID = tblClientAccounts2.SiteClientAccountID
                        AND RecHubException.ExceptionBatches.SiteBankID = tblClientAccounts2.SiteBankID
               WHERE
                    RecHubException.DecisioningTransactions.TransactionStatusKey <> 0
                    AND (tblClientAccounts.SiteClientAccountID IS NOT NULL OR tblClientAccounts2.SiteClientAccountID IS NOT NULL)

        INSERT INTO #TodaysExceptions 
        (
            CheckAmount,       
            DecisioningStatus,
            BatchPaymentTypeKey,
			ProcessType    
        )
               SELECT 
                    RecHubData.factBatchSummary.CheckTotal AS CheckAmount,
                    RecHubData.factBatchSummary.BatchExceptionStatusKey AS DecisioningStatus,
                    RecHubData.factBatchSummary.BatchPaymentTypeKey AS BatchPaymentTypeKey,
					CASE WHEN RecHubData.dimBatchSources.ShortName LIKE '%IntegraPAY%'
						THEN 'In-Process'
					ELSE
						'Post-Process'
					END AS ProcessType

               FROM RecHubData.factBatchSummary
                    LEFT JOIN RecHubException.IntegraPAYExceptions
                            ON RecHubData.factBatchSummary.BatchID = RecHubException.IntegraPAYExceptions.BatchID
                    LEFT JOIN RecHubException.ExceptionBatches
                            ON RecHubData.factBatchSummary.BatchID = RecHubException.ExceptionBatches.BatchID
					INNER JOIN RecHubData.dimBatchSources
							ON RecHubData.factBatchSummary.BatchSourceKey = RecHubData.dimBatchSources.BatchSourceKey
               WHERE
                    RecHubData.factBatchSummary.DepositDateKey BETWEEN @parmDateFrom and @parmDateTo
					AND RecHubData.factBatchSummary.BatchExceptionStatusKey <> 0
                    AND RecHubException.IntegraPAYExceptions.BatchID IS NULL
                    AND RecHubException.ExceptionBatches.BatchID IS NULL

        SELECT 
			COUNT(*) AS ItemCount,
			SUM(CheckAmount) AS Amount,
			DecisioningStatus,
			ProcessType

        FROM #TodaysExceptions 
            INNER JOIN RecHubData.dimBatchPaymentTypes 
			ON #TodaysExceptions.BatchPaymentTypeKey = RecHubData.dimBatchPaymentTypes.BatchPaymentTypeKey  
        GROUP BY ProcessType, DecisioningStatus
        ORDER BY ProcessType;
  
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
