--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_CommonExceptions_CheckoutTransaction
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_CommonExceptions_CheckoutTransaction') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_CommonExceptions_CheckoutTransaction
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_CommonExceptions_CheckoutTransaction 
(
	@parmCommonExceptionID					INT,
	@parmUserID								INT,
	@parmSessionID							UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 05/23/2013
*
* Purpose: This procedure sets the user ID associated with editing this transaction
*			(after first verifying that user is allowed to right now) and returns
*			external IDs that will be used to load the transaction details, if applicable
*
* Modification History
* 06/04/2013 WI 104360 WJS	Created
* 07/09/2013 WI 108946 RDS	Updates to finish it
* 07/18/2013 WI 109476 RDS	Added Audit records
* 07/19/2003 WI 109476 RDS	Verify user permission to modify transactions
* 04/25/2014 WI 139281 RDS	Updates for IntegraPAYExceptions table name change;
*								Include Post-Process exceptions
* 05/14/2014 WI 141670 RDS	Add auditing
* 07/29/2014 WI 156205 RDS	Update for new SessionEntitlements
* 10/10/2014 WI 141670 KLC	Updated auditing text
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @ExceptionType TINYINT;
	DECLARE @iTransactionID INT;
	DECLARE @iGlobalBatchID INT;
	DECLARE @bLockedCreated BIT;
	DECLARE @auditMessage VARCHAR(1024);

	SELECT
			@ExceptionType = CASE 
				WHEN RecHubException.DecisioningTransactions.TransactionKey IS NOT NULL THEN 1 
				WHEN RecHubException.DecisioningTransactions.IntegraPAYExceptionKey IS NOT NULL THEN 2
				ELSE 0 END,
			@iGlobalBatchID = RecHubException.IntegraPAYExceptions.GlobalBatchID,
			@iTransactionID = RecHubException.IntegraPAYExceptions.TransactionID
	FROM
			RecHubException.DecisioningTransactions
	LEFT JOIN 
			RecHubException.IntegraPAYExceptions ON
				RecHubException.DecisioningTransactions.IntegraPAYExceptionKey = RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey
	LEFT JOIN
			RecHubException.Transactions ON
				RecHubException.DecisioningTransactions.TransactionKey = RecHubException.Transactions.TransactionKey
	
	-- TODO: Join to Session Entitlements to ensure appropriate access to this transaction
	
	WHERE 
			RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;

	-- Check for an existing checkout by another user
	IF EXISTS
		(SELECT 1 FROM RecHubException.DecisioningTransactions 
					WHERE DecisioningTransactionKey = @parmCommonExceptionID
					AND UserID IS NOT NULL AND UserID != @parmUserID)
	BEGIN
		
		SELECT @bLockedCreated = 0;
			
	END
	ELSE
	BEGIN
			
		-- Checkout - or update existing checkout by this user, if applicable
		SELECT @bLockedCreated = 1
		UPDATE [RecHubException].[DecisioningTransactions]
			SET  UserID = @parmUserID
				,TransactionBeginWork = GETDATE()
				,TransactionReset = 0
				,ModificationDate = GETDATE()
			WHERE 
				DecisioningTransactionKey = @parmCommonExceptionID
			AND TransactionStatusKey = 1 -- Must be Unresolved in order to checkout
			AND (UserID IS NULL OR UserID = @parmUserID);
			
		-- Check if lock update was made
		IF @@ROWCOUNT = 0
		BEGIN
			-- Failed to update record for lock?  Probably not in the correct status...
			SELECT @bLockedCreated = 0;
		END
		ELSE
		BEGIN
			-- Construct the audit message
			SELECT @auditMessage = 'Exception ID: ' + CAST(@parmCommonExceptionID as VARCHAR)
					+ ISNULL(', GlobalBatchID: ' + CAST(RecHubException.IntegraPAYExceptions.GlobalBatchID as VARCHAR), '')
					+ ISNULL(', TransactionID: ' + CAST(RecHubException.IntegraPAYExceptions.TransactionID as VARCHAR), '')
					+ ISNULL(', DepositDate: ' + CAST(RecHubException.ExceptionBatches.DepositDateKey as VARCHAR), '')
					+ ISNULL(', BatchID: ' + CAST(RecHubException.ExceptionBatches.BatchID as VARCHAR), '')
					+ ISNULL(', TransactionID: ' + CAST(RecHubException.Transactions.TransactionID as VARCHAR), '')
			FROM RecHubException.DecisioningTransactions
			LEFT JOIN RecHubException.IntegraPAYExceptions ON
				RecHubException.IntegraPAYExceptions.IntegraPAYExceptionKey = RecHubException.DecisioningTransactions.IntegraPAYExceptionKey
			LEFT JOIN RecHubException.Transactions ON
				RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey
			LEFT JOIN RecHubException.ExceptionBatches ON
				RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
			WHERE RecHubException.DecisioningTransactions.DecisioningTransactionKey = @parmCommonExceptionID;
				
			-- Audit this event
			EXEC RecHubCommon.usp_WFS_EventAudit_Ins
					@parmApplicationName = 'RecHubException.usp_CommonExceptions_CheckoutTransaction',
					@parmEventName = 'E Checked Out',
					@parmEventType = 'Exceptions',
					@parmUserID = @parmUserID,
					@parmAuditMessage = @auditMessage
		END
	END
	
	-- Return the identifiers for this common exception and status of the checkout
	SELECT
				@ExceptionType AS ExceptionType,
				@iGlobalBatchID As GlobalBatchID,
				@iTransactionID AS TransactionID,
				@bLockedCreated AS LockCreated;
			
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

