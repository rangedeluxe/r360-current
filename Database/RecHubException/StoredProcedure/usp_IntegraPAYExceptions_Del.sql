--WFSScripSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_IntegraPAYExceptions_Del
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_IntegraPAYExceptions_Del') IS NOT NULL
    DROP PROCEDURE RecHubException.usp_IntegraPAYExceptions_Del
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_IntegraPAYExceptions_Del 
AS
/* *****************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 07/08/2013
*
* Purpose: Delete rows from IntegraPAYExceptions
*
* Modification History
* 07/08/2013 WI 108266 JBS	Create procedure.
* 05/07/2014 WI 138847 JBS	Change Table reference 
******************************************************************************/
SET NOCOUNT ON;

DECLARE @DeletionDate DATETIME;

BEGIN TRY
	
	SELECT 
		@DeletionDate = GETDATE() - CAST(RecHubConfig.SystemSetup.Value AS INT) 
	FROM
		RecHubConfig.SystemSetup
	WHERE 
		RecHubConfig.SystemSetup.Section = 'Rec Hub Table Maintenance' AND
		RecHubConfig.SystemSetup.SetupKey = 'CommonExceptionRetentionDays';

	IF @DeletionDate IS NOT NULL	-- In case no data returned from SystemSetup table
	BEGIN
		DELETE 
		FROM	
			RecHubException.IntegraPAYExceptions 
		WHERE	
			RecHubException.IntegraPAYExceptions.ModificationDate < @DeletionDate;
	END

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH