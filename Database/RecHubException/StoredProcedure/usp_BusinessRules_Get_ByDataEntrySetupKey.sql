--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_BusinessRules_Get_ByDataEntrySetupKey
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubException.usp_BusinessRules_Get_ByDataEntrySetupKey') IS NOT NULL
       DROP PROCEDURE RecHubException.usp_BusinessRules_Get_ByDataEntrySetupKey
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_BusinessRules_Get_ByDataEntrySetupKey
(
	@parmDataEntrySetupKey	BIGINT,
	@parmRequiresInvoice	BIT OUTPUT,
	@parmBalancingOption	TINYINT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 04/29/2014
*
* Purpose: Reads the Currently Assigned Data Entry Columns and their BusinessRules
*			and Check Digits for the given DataEntrySetupKey.
*
* Modification History
* 05/16/2014 WI 142642 KLC	Created
* 10/02/2014 WI 169171 KLC	Updated to order by decisioning fields first
* 10/24/2014 WI 174200 KLC	Updated to return RequiresInvoice and InvoiceBalancingOption
* 11/12/2014 WI 174200 KLC	Updated to return FieldValidations Info
* 12/12/2014 WI 174200 KLC	Changed to not return the standard Payment fields
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY
	DECLARE @siteBankID INT;
	DECLARE @siteWorkgroupID INT;

	SELECT	@siteBankID = RecHubException.DataEntrySetups.SiteBankID,
			@siteWorkgroupID = RecHubException.DataEntrySetups.SiteWorkgroupID,
			@parmRequiresInvoice = RecHubException.DataEntrySetups.RequiresInvoice
	FROM RecHubException.DataEntrySetups
	WHERE RecHubException.DataEntrySetups.DataEntrySetupKey = @parmDataEntrySetupKey;

	SELECT @parmBalancingOption = InvoiceBalancingOption
	FROM RecHubUser.OLWorkgroups
	WHERE RecHubUser.OLWorkgroups.SiteBankID = @siteBankID
		AND RecHubUser.OLWorkgroups.SiteClientAccountID = @siteWorkgroupID;

	WITH CTE_AssignedDataEntryColumns AS
	(
		SELECT	ROW_NUMBER() OVER ( PARTITION BY RecHubData.dimDataEntryColumns.FldName, RecHubData.dimDataEntryColumns.TableType
									ORDER BY RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey DESC ) AS RowNumber,
				RecHubData.dimDataEntryColumns.DataEntryColumnKey,
				RecHubData.dimDataEntryColumns.FldName,
				RecHubData.dimDataEntryColumns.TableType,
				RecHubData.dimDataEntryColumns.DataType,
				RecHubData.dimDataEntryColumns.FldLength,
				RecHubData.dimDataEntryColumns.ScreenOrder,
				RecHubData.dimDataEntryColumns.DisplayName
		FROM RecHubData.dimClientAccounts
			INNER JOIN RecHubData.ClientAccountsDataEntryColumns
				ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey
			INNER JOIN RecHubData.dimDataEntryColumns
				ON RecHubData.ClientAccountsDataEntryColumns.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
		WHERE RecHubData.dimClientAccounts.SiteBankID = @siteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = @siteWorkgroupID
			AND RecHubData.dimDataEntryColumns.TableType <> 0/*standard check payment are automatically added by the service*/
	)
	SELECT CTE_AssignedDataEntryColumns.DataEntryColumnKey,
			CTE_AssignedDataEntryColumns.TableType,
			CTE_AssignedDataEntryColumns.DataType,
			CTE_AssignedDataEntryColumns.FldLength,
			CTE_AssignedDataEntryColumns.ScreenOrder,
			CTE_AssignedDataEntryColumns.FldName,
			CTE_AssignedDataEntryColumns.DisplayName,
			RecHubException.DataEntrySetupFields.BatchSourceKey,
			RecHubException.DataEntrySetupFields.BatchPaymentTypeKey,
			RecHubException.DataEntrySetupFields.BatchPaymentSubTypeKey,
			RecHubException.BusinessRules.BusinessRuleKey,
			RecHubException.BusinessRules.MinLength,
			RecHubException.BusinessRules.[MaxLength],
			RecHubException.BusinessRules.[Required],
			RecHubException.BusinessRules.UserCanEdit,
			RecHubException.BusinessRules.FormatType,
			RecHubException.BusinessRules.Mask,
			RecHubException.BusinessRules.RequiredMessage,
			RecHubException.BusinessRules.FieldValidationKey,
			RecHubException.CheckDigitRoutines.CheckDigitRoutineKey,
			RecHubException.CheckDigitRoutines.Offset,
			RecHubException.CheckDigitRoutines.Method,
			RecHubException.CheckDigitRoutines.Modulus,
			RecHubException.CheckDigitRoutines.Compliment,
			RecHubException.CheckDigitRoutines.WeightPatternDirection,
			RecHubException.CheckDigitRoutines.WeightPatternValue,
			RecHubException.CheckDigitRoutines.IgnoreSpaces,
			RecHubException.CheckDigitRoutines.Remainder10Replacement,
			RecHubException.CheckDigitRoutines.Remainder11Replacement,
			RecHubException.CheckDigitRoutines.FailureMessage AS CheckDigitFailureMessage,
			RecHubException.FieldValidations.FieldValidationTypeKey AS FieldValidationTypeKey,
			RecHubException.FieldValidations.FailureMessage AS FieldValidationFailureMessage
	FROM CTE_AssignedDataEntryColumns
		LEFT OUTER JOIN RecHubException.DataEntrySetupFields
			ON CTE_AssignedDataEntryColumns.FldName = RecHubException.DataEntrySetupFields.FldName
				AND CTE_AssignedDataEntryColumns.TableType = RecHubException.DataEntrySetupFields.TableType
				AND RecHubException.DataEntrySetupFields.DataEntrySetupKey = @parmDataEntrySetupKey
		LEFT OUTER JOIN RecHubException.BusinessRules
			ON RecHubException.DataEntrySetupFields.BusinessRuleKey = RecHubException.BusinessRules.BusinessRuleKey
		LEFT OUTER JOIN RecHubException.CheckDigitRoutines
			ON RecHubException.BusinessRules.CheckDigitRoutineKey = RecHubException.CheckDigitRoutines.CheckDigitRoutineKey
		LEFT OUTER JOIN RecHubException.FieldValidations
			ON RecHubException.BusinessRules.FieldValidationKey = RecHubException.FieldValidations.FieldValidationKey
	WHERE CTE_AssignedDataEntryColumns.RowNumber = 1
	ORDER BY ISNULL(CAST(RecHubException.DataEntrySetupFields.ExceptionDisplayOrder AS SMALLINT), 256/*force nulls to the end*/) ASC,	--Order by ExceptionDisplayOrder
			CASE WHEN RecHubException.BusinessRules.BusinessRuleKey IS NOT NULL THEN 0 ELSE 1 END ASC,									--Then field's with business rules and no Exception Display Order
			CTE_AssignedDataEntryColumns.ScreenOrder ASC,																				--Then by Screen Order
			CTE_AssignedDataEntryColumns.DataEntryColumnKey ASC;																		--Lastly by when they were created

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

