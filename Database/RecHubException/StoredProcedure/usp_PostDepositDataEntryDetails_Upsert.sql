--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_PostDepositDataEntryDetails_UpSert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_PostDepositDataEntryDetails_UpSert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_PostDepositDataEntryDetails_UpSert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_PostDepositDataEntryDetails_UpSert
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Update an existing PostDepositDataEntryDetails record or add it if not exists
*
* Modification History
* 08/24/2017 PT XXXXXX	JPB Created
******************************************************************************/
BEGIN TRY
	/* declare storage from the data entry value that are new */
	DECLARE @DataEntryFields TABLE
	(
		DepositDateKey INT,
		BatchID BIGINT,
		TransactionID INT,
		InputBatchSequence INT,
		IsCheck BIT,
		FieldName VARCHAR(80)
	);

	/* declare storage for the batch sequences */
	DECLARE @Sequences TABLE
	(
		DepositDateKey INT,
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT,
		InputBatchSequence INT,
		StubSequence INT
	);

	/* find records that do not exist in the RecHubException.PostDepositDataEntryDetails */
	;WITH DataEntryValues_CTE AS
	(
		SELECT
			@parmDepositDateKey AS DepositDateKey,
			@parmBatchID AS BatchID,
			@parmTransactionID AS TransactionID,
			DataEntryValues.BatchSequence AS InputBatchSequence,
			IsCheck,
			FieldName
		FROM 
			@parmDataEntryValues AS DataEntryValues
	)
	INSERT INTO @DataEntryFields
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		InputBatchSequence,
		IsCheck,
		FieldName
	)
	SELECT
		DataEntryValues_CTE.DepositDateKey,
		DataEntryValues_CTE.BatchID,
		DataEntryValues_CTE.TransactionID,
		DataEntryValues_CTE.InputBatchSequence,
		DataEntryValues_CTE.IsCheck,
		DataEntryValues_CTE.FieldName
	FROM 
		DataEntryValues_CTE
		LEFT JOIN RecHubException.PostDepositDataEntryDetails ON PostDepositDataEntryDetails.DepositDateKey = DataEntryValues_CTE.DepositDateKey 
				AND PostDepositDataEntryDetails.BatchID = DataEntryValues_CTE.BatchID
				AND PostDepositDataEntryDetails.TransactionID = DataEntryValues_CTE.TransactionID
				AND PostDepositDataEntryDetails.InputBatchSequence = DataEntryValues_CTE.InputBatchSequence
				AND PostDepositDataEntryDetails.IsCheck = DataEntryValues_CTE.IsCheck
				AND PostDepositDataEntryDetails.FieldName = DataEntryValues_CTE.FieldName
	WHERE
		RecHubException.PostDepositDataEntryDetails.PostDepositDataEntryDetailsKey IS NULL;


	/* Done as two inserts because of limitations of NEXT VALUE FOR */
	INSERT INTO @Sequences
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		InputBatchSequence,
		BatchSequence,
		StubSequence
	)
	SELECT DISTINCT
		DepositDateKey,
		BatchID,
		TransactionID,
		InputBatchSequence,
		InputBatchSequence AS BatchSequence,
		-1
	FROM
		@DataEntryFields
	WHERE 
		InputBatchSequence > 0;

	/* Use a CTE, NEXT VALUE FOR cannot be used in a CTE */
	;WITH NewBatchSequences AS
	(
		SELECT DISTINCT
			DepositDateKey,
			BatchID,
			TransactionID,
			InputBatchSequence
		FROM
			@DataEntryFields
		WHERE 
			InputBatchSequence <= 0
	)
	INSERT INTO @Sequences
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		InputBatchSequence,
		BatchSequence,
		StubSequence
	)
	SELECT DISTINCT
		DepositDateKey,
		BatchID,
		TransactionID,
		InputBatchSequence,
		/* 
			User ROW_NUMBER() to get a value from 1 to n, then add the next batch sequence number, but subtract 1 since fn_GetNextBatchSequence
			returns the actual next batch sequence number and we are starting at 1. 
			ORDER BY the InputBatchSequence DESC to get the correct sequences, InputBatchSequence are -1, -2, etc.
		*/
		ROW_NUMBER() OVER (ORDER BY InputBatchSequence DESC) + RecHubData.fn_GetNextBatchSequence(DepositDateKey,BatchID) - 1,
		ROW_NUMBER() OVER (ORDER BY InputBatchSequence DESC) + RecHubData.fn_GetNextStubSequence(DepositDateKey,BatchID) - 1
	FROM
		NewBatchSequences;

	/* Get the correct both the passed in and assigned batch sequences */
	;WITH DataEntryValues_CTE AS
	(
		SELECT
			DataEntryFields.DepositDateKey,
			DataEntryFields.BatchID,
			DataEntryFields.TransactionID,
			Sequences.BatchSequence,
			Sequences.InputBatchSequence,
			Sequences.StubSequence,
			DataEntryFields.IsCheck,
			DataEntryFields.FieldName
		FROM 
			@DataEntryFields AS DataEntryFields
			INNER JOIN @Sequences AS Sequences ON Sequences.InputBatchSequence = DataEntryFields.InputBatchSequence
	)
	INSERT INTO RecHubException.PostDepositDataEntryDetails
	(
		DepositDateKey,
		BatchID,
		TransactionID,
		BatchSequence,
		InputBatchSequence,
		StubSequence,
		IsCheck,
		FieldName
	)
	SELECT
			DataEntryValues_CTE.DepositDateKey,
			DataEntryValues_CTE.BatchID,
			DataEntryValues_CTE.TransactionID,
			DataEntryValues_CTE.BatchSequence,
			DataEntryValues_CTE.InputBatchSequence,
			DataEntryValues_CTE.StubSequence,
			DataEntryValues_CTE.IsCheck,
			DataEntryValues_CTE.FieldName
	FROM 
		DataEntryValues_CTE
		LEFT JOIN RecHubException.PostDepositDataEntryDetails ON PostDepositDataEntryDetails.DepositDateKey = DataEntryValues_CTE.DepositDateKey 
				AND PostDepositDataEntryDetails.BatchID = DataEntryValues_CTE.BatchID
				AND PostDepositDataEntryDetails.TransactionID = DataEntryValues_CTE.TransactionID
				AND PostDepositDataEntryDetails.BatchSequence = DataEntryValues_CTE.BatchSequence
				AND PostDepositDataEntryDetails.InputBatchSequence = DataEntryValues_CTE.InputBatchSequence
				AND PostDepositDataEntryDetails.IsCheck = DataEntryValues_CTE.IsCheck
				AND PostDepositDataEntryDetails.FieldName = DataEntryValues_CTE.FieldName
	WHERE
		RecHubException.PostDepositDataEntryDetails.PostDepositDataEntryDetailsKey IS NULL;
END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH