--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factChecksRemitter_Upd
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factChecksRemitter_Upd') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factChecksRemitter_Upd
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factChecksRemitter_Upd
(
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmRemitterNameValues RecHubException.ChecksRemitterValueChangeTable READONLY
)
AS
/******************************************************************************
**  Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 02/14/2019
*
* Purpose: Update an existing remitter in a check
*
* Modification History
* 02/14/2019 R360-1925	MGE Created
******************************************************************************/
SET NOCOUNT ON; 



BEGIN TRY
	UPDATE RecHubData.factChecks 
    SET RemitterName = RemitterNameTable.RemitterName, ModificationDate = GETDATE()
	FROM 
		RecHubData.factChecks
		INNER JOIN @parmRemitterNameValues RemitterNameTable ON RecHubData.factChecks.CheckSequence = RemitterNameTable.CheckSequence

    WHERE DepositDateKey = @parmDepositDateKey 
    AND BatchID = @parmBatchID 
    AND TransactionID = @parmTransactionID
	AND IsDeleted = 0;
    
END TRY
BEGIN CATCH
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


