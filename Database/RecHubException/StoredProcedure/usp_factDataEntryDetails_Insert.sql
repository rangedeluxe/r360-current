--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Insert
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubException.usp_factDataEntryDetails_Insert') IS NOT NULL
	DROP PROCEDURE RecHubException.usp_factDataEntryDetails_Insert
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubException.usp_factDataEntryDetails_Insert
(
	@parmSessionID		UNIQUEIDENTIFIER,
	@parmDepositDateKey INT,
	@parmBatchID BIGINT,
	@parmTransactionID INT,
	@parmSequenceWithinTransaction INT,
	@parmBatchSequence INT,
	@parmStubSequence INT,
	@parmModificationDate DATETIME,
	@parmDataEntryValues RecHubException.DataEntryValueChangeTable READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 10/03/2017
*
* Purpose: Update an existing factDataEntryDatail records from 
*		PostDepositExceptions
*
* Modification History
* 10/03/2017 PT 147282969	MGE	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	DECLARE @SiteBankID INT,
			@SiteClientAccountID INT,
			@DepositDate DATETIME;

	DECLARE @factDataEntryDetails TABLE
	(
		BankKey INT NOT NULL,
		OrganizationKey INT NOT NULL,
		ClientAccountKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ImmutableDateKey INT NOT NULL,
		SourceProcessingDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		SourceBatchID BIGINT NOT NULL,
		BatchNumber INT NOT NULL,
		BatchSourceKey SMALLINT NOT NULL,
		BatchPaymentTypeKey TINYINT NOT NULL,
		DepositStatus INT NOT NULL,
		TransactionID INT NOT NULL
	);

	DECLARE @NewFields TABLE
	(
		IsCheck BIT NOT NULL,
		BatchSequence INT NOT NULL,
		FieldName VARCHAR(256) NOT NULL,
		DataEntryValueDateTime DateTime NULL,
		DataEntryValueFloat Float NULL,
		DataEntryValueMoney Money NULL,
		DataEntryValue VARCHAR(256) NOT NULL,
		DepositDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		TransactionID INT NOT NULL
	);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEColumnList')) 
		DROP TABLE #DEColumnList;
	CREATE TABLE #DEColumnList 
	(
		ScreenOrder INT NOT NULL,
		FldTitle VARCHAR(256) NOT NULL,
		FldName VARCHAR(256) NOT NULL,
		TableName VARCHAR(64) NOT NULL,
		FldDataTypeEnum INT NOT NULL,
		IsRequired BIT NOT NULL,
		WorkgroupDataEntryColumnKey INT NOT NULL
	);


	/* Save the common factDataEntryDetail fields for the transaction in @factDataEntryDetails */
	INSERT INTO
		@factDataEntryDetails
	SELECT TOP 1
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.OrganizationKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID
	FROM
		RecHubData.factTransactionSummary 
	WHERE
		RecHubData.factTransactionSummary.IsDeleted = 0
		AND RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND RecHubData.factTransactionSummary.TransactionID = @parmTransactionID
		AND RecHubData.factTransactionSummary.DepositDateKey = @parmDepositDateKey
					
--	SELECT * from @factDataEntryDetails;			--DEBUG ONLY

	SELECT @SiteBankID = SiteBankID, @SiteClientAccountID = SiteClientAccountID
	FROM 
		RecHubData.dimClientAccounts
	WHERE 
		ClientAccountKey = (SELECT ClientAccountKey FROM @factDataEntryDetails);
	SELECT @DepositDate = CONVERT(CHAR(10),@parmDepositDateKey,101);

	INSERT INTO
		#DEColumnList
	(ScreenOrder, FldTitle, FldName,TableName,FldDataTypeEnum,IsRequired,WorkgroupDataEntryColumnKey)
	EXEC RecHubException.usp_PostDepositTransactionExceptions_Get_DEColumns
		@parmSessionID	= @parmSessionID,
		@parmSiteBankID = @SiteBankID,
		@parmSiteClientAccountID = @SiteClientAccountID,
		@parmDepositDate = @DepositDate,
		@parmBatchID = @parmBatchID


	;WITH NewDataEntryField_CTE AS
	(	
		SELECT 
			FieldName,
			ABS(BatchSequence)+@parmBatchSequence AS BatchSequence,
			IsCheck,
			DataEntryValueDateTime,
			DataEntryValueFloat,
			DataEntryValueMoney,
			DataEntryValue
		FROM 
			@parmDataEntryValues 
	)
	
	INSERT INTO @NewFields(FieldName,BatchSequence,IsCheck,DataEntryValueDateTime,DataEntryValueFloat,DataEntryValueMoney,DataEntryValue,
						DepositDateKey,BatchID,TransactionID)
	SELECT FieldName,BatchSequence,IsCheck,DataEntryValueDateTime,DataEntryValueFloat,DataEntryValueMoney,DataEntryValue,
						@parmDepositDateKey,@parmBatchID,@parmTransactionID FROM NewDataEntryField_CTE

--	SELECT * FROM @NewFields		--DEBUG ONLY


	/* >>>>>>>> Now Just Do an insert, joining the common fields with the @parmDataEntryValues table and dimWorkGroupDataEntryColumns and dimClientAccounts  <<<*/

	INSERT INTO RecHubData.factDataEntryDetails
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		WorkgroupDataEntryColumnKey,
		TransactionID,
		BatchSequence,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue,
		CreationDate,
		ModificationDate
	)
	SELECT
		0 AS IsDeleted,
		BankKey,
		OrganizationKey,
		factDataEntryDetails.ClientAccountKey,
		factDataEntryDetails.DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		factDataEntryDetails.BatchID,
		SourceBatchID,
		BatchNumber,
		factDataEntryDetails.BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		#DEColumnList.WorkgroupDataEntryColumnKey,
		factDataEntryDetails.TransactionID,
		NewFields.BatchSequence,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue,
		@parmModificationDate AS CreationDate,
		@parmModificationDate AS ModificationDate
	FROM 
		@factDataEntryDetails factDataEntryDetails
			INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = factDataEntryDetails.ClientAccountKey
			INNER JOIN @NewFields NewFields ON factDataEntryDetails.DepositDateKey = NewFields.DepositDateKey
			INNER JOIN #DEColumnList ON #DEColumnList.FldName = NewFields.FieldName
	WHERE #DEColumnList.TableName =	CASE 
		WHEN NewFields.IsCheck = 1 THEN 'Checks'
		WHEN NewFields.IsCheck = 0 THEN 'Stubs'
		END
	ORDER BY NewFields.BatchSequence;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEColumnList')) 
		DROP TABLE #DEColumnList;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DEColumnList')) 
	DROP TABLE #DEColumnList;
	IF @@NESTLEVEL > 1
	BEGIN
		DECLARE @ErrorMessage    NVARCHAR(4000),
				@ErrorProcedure	 NVARCHAR(200),
				@ErrorSeverity   INT,
				@ErrorState      INT,
				@ErrorLine		 INT;
		
		SELECT	@ErrorMessage = ERROR_MESSAGE(),
				@ErrorSeverity = ERROR_SEVERITY(),
				@ErrorState = ERROR_STATE(),
				@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
				@ErrorLine = ERROR_LINE();

		SET @ErrorMessage = @ErrorProcedure + ' (Line: %d)-> ' + @ErrorMessage;

		RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorLine);
	END
	ELSE
       EXEC RecHubCommon.usp_WfsRethrowException;


END CATCH
