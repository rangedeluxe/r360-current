--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorDataTypeName DataEntryValueChangeTable
--WFSScriptProcessorDataTypeCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2017
*
* Purpose: Table value used to pass PDE data into stored procedures
*
* Modification History
* 08/24/2017 PT 149441599	JPB Created
******************************************************************************/
CREATE TYPE RecHubException.DataEntryValueChangeTable AS TABLE
(
    BatchSequence INT NOT NULL,
    IsCheck BIT NOT NULL,
    DataEntryValueDateTime DATETIME NULL,
    DataEntryValueFloat FLOAT NULL,
    DataEntryValueMoney MONEY NULL,
    DataEntryValue VARCHAR(256) NOT NULL,
    FieldName VARCHAR(256) NOT NULL
);
