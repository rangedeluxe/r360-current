--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubException
--WFSScriptProcessorDataTypeName ChecksRemitterValueChangeTable
--WFSScriptProcessorDataTypeCreate
/******************************************************************************
**  Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 02/14/2019
*
* Purpose: Defines the table to pass RemitterName changes to usp_factChecksRemitter_upd
*
* Modification History
* 02/14/2019 CR R360-1925	MGE Created
******************************************************************************/



CREATE TYPE RecHubException.ChecksRemitterValueChangeTable AS TABLE(
	CheckSequence INT NOT NULL,
	RemitterName VARCHAR(60) NOT NULL
)
