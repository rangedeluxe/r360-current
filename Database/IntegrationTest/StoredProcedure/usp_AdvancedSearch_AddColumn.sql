IF OBJECT_ID('IntegrationTest.usp_AdvancedSearch_AddColumn') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_AdvancedSearch_AddColumn
GO

CREATE PROCEDURE IntegrationTest.usp_AdvancedSearch_AddColumn
(
	@parmIsCheck				BIT,
	@parmDataType				TINYINT,
	@parmFieldName				NVARCHAR(256)
)
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/17/2019
*
* Purpose: Add data entry columns to RecHubData.factAdvancedSearch
*
* Modification History
* 08/29/2019 R360-30221 JPB	Created
******************************************************************************/

SET NOCOUNT ON;

DECLARE @columnName VARCHAR(1024),
		@SQLCommand VARCHAR(MAX);

BEGIN TRY

	SELECT
		@columnName = @parmFieldName + '_' + RTRIM(CAST(@parmIsCheck AS CHAR)) + '_' + CAST(@parmDataType AS VARCHAR(2));

	IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = 'factAdvancedSearch' AND COLUMN_NAME = @columnName)
	BEGIN

		SELECT @SQLCommand = 'ALTER TABLE RecHubData.factAdvancedSearch ADD ' + @columnName + 
			CASE @parmDataType
				WHEN 6 THEN ' FLOAT'
				WHEN 7 THEN ' MONEY'
				WHEN 11 THEN ' DATETIME'
				ELSE ' VARCHAR(256)'
			END + ' SPARSE NULL;'
		EXEC(@SQLCommand);
	END

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH