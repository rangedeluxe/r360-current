IF OBJECT_ID('IntegrationTest.usp_dimBatchSources_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimBatchSources_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimBatchSources_Ins
(
	@parmShortName		VARCHAR(30),
	@parmLongName		VARCHAR(128),
	@parmImportType		VARCHAR(30),
	@parmIsActive		BIT,
	@parmBatchSourceKey SMALLINT = NULL,
	@parmEntityID		INT = NULL
)
AS

SET NOCOUNT ON;
DECLARE @ImportTypeKey INT;

BEGIN TRY
	SELECT @ImportTypeKey = ImportTypeKey
	FROM RecHubData.dimImportTypes
	WHERE ShortName = @parmImportType;

	if( @ImportTypeKey IS NULL )
		RAISERROR('Could not find Import Type %s.',16,1,@parmImportType);
		
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimBatchSources WHERE ShortName = @parmShortName)
	BEGIN
		IF @parmBatchSourceKey IS NOT NULL
		BEGIN
			SET IDENTITY_INSERT RecHubData.dimBatchSources ON;
			INSERT INTO RecHubData.dimBatchSources(BatchSourceKey,ShortName,LongName,IsActive,ImportTypeKey,CreationDate,ModificationDate,EntityID)
			SELECT @parmBatchSourceKey,@parmShortName,@parmLongName,1,@ImportTypeKey,GETDATE(),GETDATE(),@parmEntityID;
			SET IDENTITY_INSERT RecHubData.dimBatchSources OFF;
		END
		ELSE
		BEGIN
			INSERT INTO RecHubData.dimBatchSources(ShortName,LongName,IsActive,ImportTypeKey,CreationDate,ModificationDate,EntityID)
			SELECT @parmShortName,@parmLongName,1,@ImportTypeKey,GETDATE(),GETDATE(),@parmEntityID;
		END
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


