IF OBJECT_ID('IntegrationTest.usp_dimDates_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimDates_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimDates_Ins
(
	@parmDate	DATETIME
)
AS

BEGIN TRY
	INSERT INTO RecHubData.dimDates
	SELECT	CAST(LEFT(REPLACE(CONVERT(varchar,@parmDate,120),'-',''),8) as int),
			@parmDate CalendarDate,
			DATEPART(dd, @parmDate) CalendarDayMonth,
			DATEPART(dy, @parmDate) CalendarDayYear,
			DATEPART(dw, @parmDate) CalendarDayWeek,
			DATENAME(dw, @parmDate) CalendarDayName,
			DATEPART(ww, @parmDate) CalendarWeek,
			DATEPART(mm, @parmDate) CalendarMonth,
			DATENAME(mm, @parmDate) CalendarMonthName,
			DATEPART(qq, @parmDate) CalendarQuarter,
			'Q' + DATENAME(qq, @parmDate) + ' ' + DATENAME(yy, @parmDate) CalendarQuarterName,
			DATEPART(yy, @parmDate) CalendarYear;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
