IF OBJECT_ID('IntegrationTest.usp_dimImportTypes_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimImportTypes_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimImportTypes_Ins
(
	@parmShortName		VARCHAR(30),
	@parmIsActive		BIT,
	@parmImportTypeKey 	SMALLINT = NULL
)
AS

SET NOCOUNT ON;
DECLARE @ImportTypeKey TINYINT = 0;

BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimImportTypes WHERE ShortName = @parmShortName)
	BEGIN
		IF @parmImportTypeKey IS NULL
			SELECT @ImportTypeKey = (COALESCE(MAX(ImportTypeKey),@ImportTypeKey)+1) FROM RecHubData.dimImportTypes;
		ELSE SET @ImportTypeKey = @parmImportTypeKey;
		
		INSERT INTO RecHubData.dimImportTypes(ImportTypeKey,ShortName,IsActive,CreationDate,ModificationDate)
		SELECT @ImportTypeKey,@parmShortName,@parmIsActive,GETDATE(),GETDATE();
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


