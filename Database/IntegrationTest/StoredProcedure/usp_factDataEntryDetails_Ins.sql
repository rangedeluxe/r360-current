IF OBJECT_ID('IntegrationTest.usp_factDataEntryDetails_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factDataEntryDetails_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factDataEntryDetails_Ins
(
	@parmBankKey					INT,
	@parmClientAccountKey			INT,
	@parmDepositDateKey				INT,
	@parmImmutableDateKey			INT,
	@parmSourceProcessingDateKey	INT,
	@parmBatchID					BIGINT,
	@parmSourceBatchID				BIGINT,
	@parmBatchNumber				BIGINT,
	@parmBatchSourceKey				SMALLINT,
	@parmPaymentTypeKey				TINYINT,
	@parmDepositStatus				INT,
	@parmTransactionID				INT,
	@parmBatchSequence				INT,
	@parmIsCheck					BIT,
	@parmFieldName					VARCHAR(256),
	@parmValue						VARCHAR(256)
)
AS

SET NOCOUNT ON;

DECLARE @WorkgroupDataEntryColumnKey BIGINT = 0;

BEGIN TRY

	--SELECT 
	--	@WorkgroupDataEntryColumnKey = WorkgroupDataEntryColumnKey
	--FROM 
	--	RecHubData.dimWorkgroupDataEntryColumns
	--	INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimWorkgroupDataEntryColumns.SiteBankID
	--		AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID
	--WHERE
	--	dimClientAccounts.ClientAccountKey = @parmClientAccountKey
	--	AND dimWorkgroupDataEntryColumns.FieldName = @parmFieldName
	--	AND dimWorkgroupDataEntryColumns.IsCheck = @parmIsCheck;

	INSERT INTO RecHubData.factDataEntryDetails
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		TransactionID,
		BatchSequence,
		WorkgroupDataEntryColumnKey,
		DataEntryValueDateTime,
		DataEntryValueFloat,
		DataEntryValueMoney,
		DataEntryValue,
		CreationDate,
		ModificationDate
	)
	SELECT
		0,
		@parmBankKey,
		-1,
		@parmClientAccountKey,
		@parmDepositDateKey,
		@parmImmutableDateKey,
		@parmSourceProcessingDateKey,
		@parmBatchID,
		@parmSourceBatchID,
		@parmBatchNumber,
		@parmBatchSourceKey,
		@parmPaymentTypeKey,
		@parmDepositStatus,
		@parmTransactionID,
		@parmBatchSequence,
		WorkgroupDataEntryColumnKey,
		CASE RecHubData.dimWorkgroupDataEntryColumns.DataType
			WHEN 11 THEN CAST(@parmValue AS DATETIME)
			ELSE NULL
		END AS DataEntryValueDateTime,
		CASE RecHubData.dimWorkgroupDataEntryColumns.DataType
			WHEN 6 THEN CAST(@parmValue AS FLOAT)
			ELSE NULL
		END AS DataEntryValueFloat,
		CASE RecHubData.dimWorkgroupDataEntryColumns.DataType
			WHEN 7 THEN CAST(@parmValue AS MONEY)
			ELSE NULL
		END AS DataEntryValueMoney,
		@parmValue,
		GETDATE(),
		GETDATE()
	FROM
		RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = RecHubData.dimWorkgroupDataEntryColumns.SiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID
	WHERE
		RecHubData.dimWorkgroupDataEntryColumns.FieldName = @parmFieldName
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = @parmIsCheck
		AND RecHubData.dimClientAccounts.ClientAccountKey = @parmClientAccountKey;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
