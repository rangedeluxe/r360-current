IF OBJECT_ID('IntegrationTest.usp_factAdvancedSearch_Ins_WithDataEntry') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins_WithDataEntry
GO

CREATE PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins_WithDataEntry
(
	@parmBatchID BIGINT
)
AS

SET NOCOUNT ON;

DECLARE @SQLStatement			NVARCHAR(MAX) = N''		--Variable to hold t-sql query
DECLARE @UniqueDEColumns		NVARCHAR(MAX) = N''		--Variable to hold unique DE Columns to be used in PIVOT clause
DECLARE @PivotDEColumnsToSelect	NVARCHAR(MAX) = N''		--Variable to hold Pivot column name with alias to be used in select clause
DECLARE @PivotedColumns			NVARCHAR(MAX) = N'';	--Variable to hold unique DE Columns pivoted

BEGIN TRY

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;

	CREATE TABLE #tmpCombinedBaseRecords
	(
		IsDeleted BIT NOT NULL,
		BankKey INT NOT NULL,
		ClientAccountKey INT NOT NULL,
		DepositDateKey INT NOT NULL,
		ImmutableDateKey INT NOT NULL,
		SourceProcessingDateKey INT NOT NULL,
		BatchID BIGINT NOT NULL,
		SourceBatchID BIGINT NOT NULL,
		BatchNumber INT NOT NULL,
		BatchSourceKey SMALLINT NOT NULL,
		BatchPaymentTypeKey TINYINT NOT NULL,
		DepositStatus INT NOT NULL,
		TransactionID INT NOT NULL,
		TxnSequence INT NOT NULL,
		CheckCount INT NOT NULL,
		ScannedCheckCount INT NOT NULL,
		StubCount INT NOT NULL,
		DocumentCount INT NOT NULL,
		OMRCount INT NOT NULL,
		BatchSequence INT NULL,
		CheckBatchSequence INT,
		CheckSequence INT,
		CheckAmount MONEY,
		RoutingNumber VARCHAR(30),
		CheckAccountNumber VARCHAR(30),
		TransactionCode VARCHAR(30),
		Serial VARCHAR(30),
		RemitterName VARCHAR(60),
		DDAKey INT,
		NumericRoutingNumber BIGINT,
		NumericSerial BIGINT,
		StubBatchSequence INT,
		StubSequence INT,
		StubAmount MONEY,
		StubAccountNumber VARCHAR(80)
	);

	INSERT INTO #tmpCombinedBaseRecords
	(
		IsDeleted,
		BankKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		TransactionID,
		TxnSequence,
		CheckCount,
		ScannedCheckCount,
		StubCount,
		DocumentCount,
		OMRCount,
		BatchSequence,
		CheckBatchSequence,
		CheckSequence,
		CheckAmount,
		RoutingNumber,
		CheckAccountNumber,
		TransactionCode,
		Serial,
		RemitterName,
		DDAKey,
		NumericRoutingNumber,
		NumericSerial,
		StubBatchSequence,
		StubSequence,
		StubAmount,
		StubAccountNumber
	)
	SELECT
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount,
		CASE 
			WHEN RecHubData.factChecks.BatchSequence IS NOT NULL THEN factChecks.BatchSequence
			WHEN RecHubData.factStubs.BatchSequence IS NOT NULL THEN factStubs.BatchSequence
		END AS BatchSequence,
		RecHubData.factChecks.BatchSequence AS CheckBatchSequence,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.Amount AS CheckAmount,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account AS CheckAccountNumber,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.DDAKey,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factStubs.BatchSequence AS StubBatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.Amount AS StubAmount,
		RecHubData.factStubs.AccountNumber AS StubAccountNumber
	FROM
		RecHubData.factTransactionSummary
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factChecks.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factChecks.IsDeleted = 0
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factStubs.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factStubs.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factStubs.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factStubs.IsDeleted = 0
	WHERE 
		RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND (COALESCE(factTransactionSummary.CheckCount,0) <> 0 OR COALESCE(factTransactionSummary.StubCount,0) <> 0 OR COALESCE(factTransactionSummary.DocumentCount,0) <> 0)
	GROUP BY
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount,
		RecHubData.factChecks.BatchSequence,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.DDAKey,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factStubs.BatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.Amount,
		RecHubData.factStubs.AccountNumber;

	SELECT @PivotedColumns = @PivotedColumns + ',' +
		CASE 
			WHEN IsCheck = 0 THEN 'PivotedStubs.'
			ELSE 'PivotedChecks.'
		END  + FieldName   + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2))
	FROM 
		RecHubData.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE
		RecHubData.factDataEntryDetails.BatchID = @parmBatchID
	GROUP BY 
		FieldName, 
		IsCheck, 
		DataType;

	SELECT 
		@UniqueDEColumns = @UniqueDEColumns + ', ' + FieldName   + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2))
	FROM 
		RecHubData.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE
		RecHubData.factDataEntryDetails.BatchID = @parmBatchID
	GROUP BY 
		FieldName, 
		IsCheck, 
		DataType;

	SELECT @UniqueDEColumns = LTRIM(STUFF(@UniqueDEColumns, 1 ,1, ''));

	SELECT 
		@PivotDEColumnsToSelect = @PivotDEColumnsToSelect + ', ' +  FieldName  + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2))
	FROM 
		RecHubData.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE 
		RecHubData.factDataEntryDetails.BatchID = @parmBatchID
	GROUP BY 
		FieldName, 
		IsCheck, 
		DataType;

	SET @SQLStatement = N'
		;WITH DetailRows AS 
		(
			SELECT 
				RecHubData.factDataEntryDetails.BankKey, 
				RecHubData.factDataEntryDetails.ClientAccountKey, 
				RecHubData.factDataEntryDetails.DepositDateKey, 
				RecHubData.factDataEntryDetails.ImmutableDateKey, 
				RecHubData.factDataEntryDetails.SourceProcessingDateKey, 
				RecHubData.factDataEntryDetails.BatchID, 
				RecHubData.factDataEntryDetails.TransactionID, 
				RecHubData.factDataEntryDetails.BatchSequence, 
				CASE IsCheck
					WHEN 1 THEN BatchSequence
					ELSE NULL
				END AS CheckBatchSequence,
				CASE IsCheck
					WHEN 0 THEN BatchSequence
					ELSE NULL
				END AS StubBatchSequence,
				RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey, 
				RecHubData.dimWorkgroupDataEntryColumns.IsCheck, 
				RecHubData.dimWorkgroupDataEntryColumns.FieldName  + ''_'' + CAST(RecHubData.dimWorkgroupDataEntryColumns.IsCheck AS NVARCHAR(1)) + ''_'' + CAST(RecHubData.dimWorkgroupDataEntryColumns.DataType AS NVARCHAR(2)) AS FieldName, 
				RecHubData.factDataEntryDetails.DataEntryValue 
			FROM 
				RecHubData.factDataEntryDetails
				INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey  
			WHERE
				BatchID = ' + CAST(@parmBatchID AS VARCHAR(30)) + N'
		), 
		PivotedRows AS
		(
			SELECT 
				BankKey, 
				ClientAccountKey, 
				DepositDateKey, 
				ImmutableDateKey, 
				SourceProcessingDateKey, 
				BatchID, 
				TransactionID, 
				BatchSequence, 
				CheckBatchSequence, 
				StubBatchSequence' + @PivotDEColumnsToSelect +	N'
			FROM 
			(
				SELECT  
					BankKey, 
					ClientAccountKey, 
					DepositDateKey, 
					ImmutableDateKey, 
					SourceProcessingDateKey, 
					BatchID, 
					TransactionID, 
					BatchSequence, 
					CheckBatchSequence, 
					StubBatchSequence, 
					FieldName, 
					DataEntryValue 
				FROM 
					DetailRows
			) p
			PIVOT
			(
				MIN(DataEntryValue) FOR FieldName IN (' + @UniqueDEColumns + ')
			) 
		AS PVT), 
		PivotedChecks AS
		(
			SELECT 
				BankKey, 
				ClientAccountKey, 
				DepositDateKey, 
				ImmutableDateKey, 
				SourceProcessingDateKey, 
				BatchID, 
				TransactionID, 
				CheckBatchSequence,
				StubBatchSequence,' + @UniqueDEColumns + N' 
			FROM 
				PivotedRows
			WHERE 
				CheckBatchSequence IS NOT NULL
		),
		PivotedStubs AS 
		(
			SELECT 
				BankKey, 
				ClientAccountKey, 
				DepositDateKey, 
				ImmutableDateKey, 
				SourceProcessingDateKey, 
				BatchID, 
				TransactionID, 
				StubBatchSequence,'+ @UniqueDEColumns + N' 
			FROM 
				PivotedRows
			WHERE 
				StubBatchSequence IS NOT NULL
		),
		CombinedRows AS
		(
			SELECT 
				COALESCE(PivotedChecks.BankKey,PivotedStubs.BankKey) AS BankKey,    
				COALESCE(PivotedChecks.ClientAccountKey,PivotedStubs.ClientAccountKey) AS ClientAccountKey,    
				COALESCE(PivotedChecks.DepositDateKey,PivotedStubs.DepositDateKey) AS DepositDateKey,    
				COALESCE(PivotedChecks.ImmutableDateKey,PivotedStubs.ImmutableDateKey) AS ImmutableDateKey,    
				COALESCE(PivotedChecks.SourceProcessingDateKey, PivotedStubs.SourceProcessingDateKey) AS SourceProcessingDateKey,    
				COALESCE(PivotedChecks.BatchID,PivotedStubs.BatchID) AS BatchID,    
				COALESCE(PivotedChecks.TransactionID,PivotedStubs.TransactionID) AS TransactionID,  
				PivotedChecks.CheckBatchSequence,
				PivotedStubs.StubBatchSequence ' + @PivotedColumns + N'
			FROM 
				PivotedChecks
				FULL OUTER JOIN PivotedStubs ON PivotedChecks.DepositDateKey = PivotedStubs.DepositDateKey
				AND PivotedChecks.BatchID = PivotedStubs.BatchID
				AND PivotedChecks.TransactionID = PivotedStubs.TransactionID
		),
		FinalRowsToInsert AS
		(
			SELECT
				#tmpCombinedBaseRecords.IsDeleted,
				#tmpCombinedBaseRecords.BankKey,
				#tmpCombinedBaseRecords.ClientAccountKey,
				#tmpCombinedBaseRecords.DepositDateKey,
				#tmpCombinedBaseRecords.ImmutableDateKey,
				#tmpCombinedBaseRecords.SourceProcessingDateKey,
				#tmpCombinedBaseRecords.BatchID,
				#tmpCombinedBaseRecords.SourceBatchID,
				#tmpCombinedBaseRecords.BatchNumber,
				#tmpCombinedBaseRecords.BatchSourceKey,
				#tmpCombinedBaseRecords.BatchPaymentTypeKey,
				#tmpCombinedBaseRecords.DepositStatus,
				#tmpCombinedBaseRecords.TransactionID,
				#tmpCombinedBaseRecords.TxnSequence,
				#tmpCombinedBaseRecords.CheckCount,
				#tmpCombinedBaseRecords.ScannedCheckCount,
				#tmpCombinedBaseRecords.StubCount,
				#tmpCombinedBaseRecords.DocumentCount,
				#tmpCombinedBaseRecords.OMRCount,
				COALESCE(CombinedRows.CheckBatchSequence, #tmpCombinedBaseRecords.CheckBatchSequence) AS CheckBatchSequence, 
				#tmpCombinedBaseRecords.CheckSequence,
				#tmpCombinedBaseRecords.CheckAmount,
				#tmpCombinedBaseRecords.RoutingNumber,
				#tmpCombinedBaseRecords.CheckAccountNumber,
				#tmpCombinedBaseRecords.TransactionCode,
				#tmpCombinedBaseRecords.Serial,
				#tmpCombinedBaseRecords.RemitterName,
				#tmpCombinedBaseRecords.DDAKey,
				#tmpCombinedBaseRecords.NumericRoutingNumber,
				#tmpCombinedBaseRecords.NumericSerial,
				#tmpCombinedBaseRecords.StubBatchSequence,
				#tmpCombinedBaseRecords.StubSequence, 
				#tmpCombinedBaseRecords.StubAmount, 
				#tmpCombinedBaseRecords.StubAccountNumber,
				GETDATE() AS CreationDate,
				GETDATE() AS ModificationDate' + @PivotDEColumnsToSelect + N'
			FROM
				CombinedRows
				FULL OUTER JOIN #tmpCombinedBaseRecords ON #tmpCombinedBaseRecords.DepositDateKey = CombinedRows.DepositDateKey 
					AND #tmpCombinedBaseRecords.BatchID = CombinedRows.BatchID 
					AND #tmpCombinedBaseRecords.TransactionID = CombinedRows.TransactionID
					--AND (
					--		(CombinedRows.CheckBatchSequence IS NULL AND #tmpCombinedBaseRecords.CheckBatchSequence IS NULL)
					--		OR 
					--		(COALESCE(CombinedRows.CheckBatchSequence, #tmpCombinedBaseRecords.CheckBatchSequence)  = COALESCE(#tmpCombinedBaseRecords.CheckBatchSequence, CombinedRows.CheckBatchSequence))
					--	)
					AND (
							(CombinedRows.StubBatchSequence IS NULL AND #tmpCombinedBaseRecords.StubBatchSequence IS NULL)
							OR 
							(COALESCE(CombinedRows.StubBatchSequence, #tmpCombinedBaseRecords.StubBatchSequence)  = COALESCE(#tmpCombinedBaseRecords.StubBatchSequence, CombinedRows.StubBatchSequence))
						)
		)
		INSERT INTO RecHubData.factAdvancedSearch
		(
			IsDeleted,
			BankKey,
			ClientAccountKey,
			DepositDateKey,
			ImmutableDateKey,
			SourceProcessingDateKey,
			BatchID,
			SourceBatchID,
			BatchNumber,
			BatchSourceKey,
			BatchPaymentTypeKey,
			DepositStatus,
			TransactionID,
			TxnSequence,
			CheckCount,
			ScannedCheckCount,
			StubCount,
			DocumentCount,
			OMRCount,
			CheckBatchSequence, 
			CheckSequence,
			CheckAmount,
			RoutingNumber,
			CheckAccountNumber,
			TransactionCode,
			Serial,
			RemitterName,
			DDAKey,
			NumericRoutingNumber,
			NumericSerial,
			StubBatchSequence,
			StubSequence, 
			StubAmount, 
			StubAccountNumber,
			CreationDate,
			ModificationDate' + @PivotDEColumnsToSelect + N'
		)
		SELECT 
			IsDeleted,
			BankKey,
			ClientAccountKey,
			DepositDateKey,
			ImmutableDateKey,
			SourceProcessingDateKey,
			BatchID,
			SourceBatchID,
			BatchNumber,
			BatchSourceKey,
			BatchPaymentTypeKey,
			DepositStatus,
			TransactionID,
			TxnSequence,
			CheckCount,
			ScannedCheckCount,
			StubCount,
			DocumentCount,
			OMRCount,
			CheckBatchSequence, 
			CheckSequence,
			CheckAmount,
			RoutingNumber,
			CheckAccountNumber,
			TransactionCode,
			Serial,
			RemitterName,
			DDAKey,
			NumericRoutingNumber,
			NumericSerial,
			StubBatchSequence,
			StubSequence, 
			StubAmount, 
			StubAccountNumber,
			CreationDate,
			ModificationDate' + @PivotDEColumnsToSelect + N'
		FROM 
			FinalRowsToInsert';

	EXEC (@SQLStatement);

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH