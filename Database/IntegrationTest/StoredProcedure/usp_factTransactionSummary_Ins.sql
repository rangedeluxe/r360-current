IF OBJECT_ID('IntegrationTest.usp_factTransactionSummary_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factTransactionSummary_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factTransactionSummary_Ins
(
	@parmBankKey					INT,
	@parmClientAccountKey			INT,
	@parmDepositDateKey				INT,
	@parmImmutableDateKey			INT,
	@parmSourceProcessingDateKey	INT,
	@parmBatchID					BIGINT,
	@parmSourceBatchID				BIGINT,
	@parmBatchNumber				BIGINT,
	@parmBatchSourceKey				SMALLINT,
	@parmPaymentTypeKey				TINYINT,
	@parmTransactionExceptionStatus	VARCHAR(36),
	@parmBatchCueID					INT,
	@parmDepositStatus				INT,
	@parmSystemType					SMALLINT,
	@parmTransactionID				INT,
	@parmTxnSequence				INT,
	@parmCheckCount					INT,
	@parmScannedCheckCount			INT,
	@parmStubCount					INT,
	@parmDocumentCount				INT,
	@parmOMRCount					INT,
	@parmCheckTotal					MONEY,
	@parmStubTotal					MONEY,
	@parmBatchSiteCode				INT = NULL
)
AS

SET NOCOUNT ON;

DECLARE @TransactionExceptionStatusKey TINYINT,
		@DepositStatusKey INT;

BEGIN TRY
	SELECT @TransactionExceptionStatusKey = TransactionExceptionStatusKey
	FROM RecHubData.dimTransactionExceptionStatuses
	WHERE ExceptionStatusName = @parmTransactionExceptionStatus

	INSERT INTO RecHubData.factTransactionSummary
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		TransactionExceptionStatusKey,
		BatchCueID,
		SystemType,
		DepositStatus,
		TransactionID,
		TxnSequence,
		CheckCount,
		ScannedCheckCount,
		StubCount,
		DocumentCount,
		OMRCount,
		CheckTotal,
		StubTotal,
		BatchSiteCode,
		CreationDate,
		ModificationDate
	)
	SELECT
		0,
		@parmBankKey,
		-1,
		@parmClientAccountKey,
		@parmDepositDateKey,
		@parmImmutableDateKey,
		@parmSourceProcessingDateKey,
		@parmBatchID,
		@parmSourceBatchID,
		@parmBatchNumber,
		@parmBatchSourceKey,
		@parmPaymentTypeKey,
		@TransactionExceptionStatusKey,
		@parmBatchCueID,
		@parmSystemType,
		@parmDepositStatus,
		@parmTransactionID,
		@parmTxnSequence,
		@parmCheckCount,
		@parmScannedCheckCount,
		@parmStubCount,
		@parmDocumentCount,
		@parmOMRCount,
		@parmCheckTotal,
		@parmStubTotal,
		@parmBatchSiteCode,
		GETDATE(),
		GETDATE()

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
