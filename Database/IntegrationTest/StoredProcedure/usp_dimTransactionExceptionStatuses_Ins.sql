IF OBJECT_ID('IntegrationTest.usp_dimTransactionExceptionStatuses_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimTransactionExceptionStatuses_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimTransactionExceptionStatuses_Ins
(
	@parmExceptionStatusName			VARCHAR(36),
	@parmExceptionStatusDescription		VARCHAR(256),
	@parmTransactionExceptionStatusKey 	TINYINT = NULL
)
AS

SET NOCOUNT ON;
DECLARE @TransactionExceptionStatusKey TINYINT = 0;

BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimTransactionExceptionStatuses WHERE ExceptionStatusName = @parmExceptionStatusName)
	BEGIN
		IF @parmTransactionExceptionStatusKey IS NULL 
			SELECT @TransactionExceptionStatusKey = (COALESCE(MAX(TransactionExceptionStatusKey),@TransactionExceptionStatusKey)+1) FROM RecHubData.dimTransactionExceptionStatuses
		ELSE  SET @TransactionExceptionStatusKey = @parmTransactionExceptionStatusKey;

		INSERT INTO RecHubData.dimTransactionExceptionStatuses(TransactionExceptionStatusKey,ExceptionStatusName,ExceptionStatusDescripion,CreationDate,ModificationDate)
		SELECT @TransactionExceptionStatusKey,@parmExceptionStatusName,@parmExceptionStatusDescription,GETDATE(),GETDATE();
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
