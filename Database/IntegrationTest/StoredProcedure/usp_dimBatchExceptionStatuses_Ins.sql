IF OBJECT_ID('IntegrationTest.usp_dimBatchExceptionStatuses_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimBatchExceptionStatuses_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimBatchExceptionStatuses_Ins
(
	@parmExceptionStatusName		VARCHAR(36),
	@parmExceptionStatusDescription	VARCHAR(256),
	@parmBatchExceptionStatusKey 	TINYINT = NULL
)
AS

SET NOCOUNT ON;
DECLARE @BatchExceptionStatusKey TINYINT = 0;

BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimBatchExceptionStatuses WHERE ExceptionStatusName = @parmExceptionStatusName)
	BEGIN
		IF @parmBatchExceptionStatusKey IS NULL 
			SELECT @BatchExceptionStatusKey = (COALESCE(MAX(BatchExceptionStatusKey),@BatchExceptionStatusKey)+1) FROM RecHubData.dimBatchExceptionStatuses
		ELSE  SET @BatchExceptionStatusKey = @parmBatchExceptionStatusKey;

		INSERT INTO RecHubData.dimBatchExceptionStatuses(BatchExceptionStatusKey,ExceptionStatusName,ExceptionStatusDescripion,CreationDate,ModificationDate)
		SELECT @BatchExceptionStatusKey,@parmExceptionStatusName,@parmExceptionStatusDescription,GETDATE(),GETDATE();
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
