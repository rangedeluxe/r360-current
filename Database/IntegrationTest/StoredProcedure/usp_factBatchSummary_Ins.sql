IF OBJECT_ID('IntegrationTest.usp_factBatchSummary_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factBatchSummary_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factBatchSummary_Ins
(
	@parmSiteBankID					INT,
	@parmSiteClientAccountID		INT,
	@parmDepositDateKey				INT,
	@parmImmutableDateKey			INT,
	@parmSourceProcessingDateKey	INT,
	@parmSourceBatchID				BIGINT,
	@parmBatchNumber				BIGINT,
	@parmBatchSource				VARCHAR(30),
	@parmPaymentType				VARCHAR(30),
	@parmBatchExceptionStatus		VARCHAR(36),
	@parmBatchCueID					INT,
	@parmDepositStatusName			VARCHAR(80),
	@parmSystemType					SMALLINT,
	@parmTransactionCount			INT,
	@parmCheckCount					INT,
	@parmScannedCheckCount			INT,
	@parmStubCount					INT,
	@parmDocumentCount				INT,
	@parmCheckTotal					MONEY,
	@parmStubTotal					MONEY,
	@parmBatchSiteCode				INT = NULL,
	@parmDepositDDA					VARCHAR(40) = NULL
)
AS

SET NOCOUNT ON;

DECLARE @BankKey					INT,
		@BatchID					BIGINT,
		@BatchExceptionStatusKey	TINYINT,
		@BatchSourceKey				SMALLINT,
		@ClientAccountKey			INT,
		@DepositStatus				INT,
		@DepositStatusKey			INT,
		@PaymentTypeKey				TINYINT,
		@NewDate					DATETIME;

BEGIN TRY
	SELECT @DepositStatusKey = DepositStatusKey, @DepositStatus = DepositStatus
	FROM RecHubData.dimDepositStatuses
	WHERE DespositDisplayName = @parmDepositStatusName;

	SELECT @BatchSourceKey = BatchSourceKey
	FROM RecHubData.dimBatchSources
	WHERE ShortName = @parmBatchSource;

	SELECT @PaymentTypeKey = BatchPaymentTypeKey
	FROM RecHubData.dimBatchPaymentTypes
	WHERE ShortName = @parmPaymentType;

	SELECT @BatchExceptionStatusKey = BatchExceptionStatusKey
	FROM RecHubData.dimBatchExceptionStatuses
	WHERE ExceptionStatusName = @parmBatchExceptionStatus;

	SELECT @BatchID = NEXT VALUE FOR RecHubSystem.ReceivablesBatchID;

	SELECT @BankKey = BankKey
	FROM RecHubData.dimBanks 
	WHERE SiteBankID = @parmSiteBankID;

	SELECT @ClientAccountKey = ClientAccountKey
	FROM RecHubData.dimClientAccounts
	WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID;

	IF NOT EXISTS(SELECT 1 FROM RecHubData.dimDates WHERE DateKey = @parmDepositDateKey)
	BEGIN
		SET @NewDate = CONVERT(DATE,CONVERT(VARCHAR(10),@parmDepositDateKey,101));
		EXEC IntegrationTest.usp_dimDates_Ins @NewDate;
	END
		
	IF NOT EXISTS(SELECT 1 FROM RecHubData.dimDates WHERE DateKey = @parmImmutableDateKey)
	BEGIN
		SET @NewDate = CONVERT(DATE,CONVERT(VARCHAR(10),@parmImmutableDateKey,101));
		EXEC IntegrationTest.usp_dimDates_Ins @NewDate;
	END
		
	IF NOT EXISTS(SELECT 1 FROM RecHubData.dimDates WHERE DateKey = @parmSourceProcessingDateKey)
	BEGIN
		SET @NewDate = CONVERT(DATE,CONVERT(VARCHAR(10),@parmSourceProcessingDateKey,101));
		EXEC IntegrationTest.usp_dimDates_Ins @NewDate;
	END
		
	INSERT INTO RecHubData.factBatchSummary
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchExceptionStatusKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		DepositStatusKey,
		TransactionCount,
		CheckCount,
		ScannedCheckCount,
		StubCount,
		DocumentCount,
		CheckTotal,
		StubTotal,
		BatchSiteCode,
		DepositDDA,
		CreationDate,
		ModificationDate
	)
	SELECT
		0,
		@BankKey,
		-1,
		@ClientAccountKey,
		@parmDepositDateKey,
		@parmImmutableDateKey,
		@parmSourceProcessingDateKey,
		@BatchID,
		@parmSourceBatchID,
		@parmBatchNumber,
		@BatchSourceKey,
		@PaymentTypeKey,
		@BatchExceptionStatusKey,
		@parmBatchCueID,
		@DepositStatus,
		@parmSystemType,
		@DepositStatusKey,
		@parmTransactionCount,
		@parmCheckCount,
		@parmScannedCheckCount,
		@parmStubCount,
		@parmDocumentCount,
		@parmCheckTotal,
		@parmStubTotal,
		@parmBatchSiteCode,
		@parmDepositDDA,
		GETDATE(),
		GETDATE();

	SELECT 
		@BatchID AS BatchID,
		@BankKey AS BankKey,
		@ClientAccountKey AS ClientAccountKey,
		@BatchSourceKey AS BatchSourceKey,
		@PaymentTypeKey AS PaymentTypeKey,
		@BatchExceptionStatusKey AS BatchExceptionStatusKey,
		@DepositStatus AS DepositStatus,
		@DepositStatusKey AS DepositStatusKey;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
