IF OBJECT_ID('IntegrationTest.usp_fact_Upd_DataEntry') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_fact_Upd_DataEntry
GO

CREATE PROCEDURE IntegrationTest.usp_fact_Upd_DataEntry
(
	@parmBatchID BIGINT,
	@parmIsCheck BIT
)
AS


SET NOCOUNT ON;

DECLARE @SQLStatement			NVARCHAR(MAX) = N''		/* Variable to hold t-sql query */
DECLARE @UniqueDEColumns		NVARCHAR(MAX) = N''		/* Variable to hold unique DE Columns to be used in PIVOT clause */
DECLARE @PivotDEColumnsToSelect	NVARCHAR(MAX) = N''		/* Variable to hold Pivot column name with alias to be used in select clause */
DECLARE @PivotedColumns			NVARCHAR(MAX) = N'';	/* Variable to hold unique DE Columns pivoted */
DECLARE @MergeColumns			NVARCHAR(MAX) = N'';    /* Variable to hold merge column updates */

DECLARE	
	@DataType	TINYINT,
	@FieldName	VARCHAR(64),
	@IsCheck	BIT,
	@Loop		INT;

BEGIN TRY
	DECLARE @DEColumns TABLE
	(
		RowID INT IDENTITY(1,1),
		IsCheck BIT,
		DataType TINYINT,
		FieldName VARCHAR(64),
		CompositeFieldName VARCHAR(64)
	);

	INSERT INTO @DEColumns
	(
		IsCheck,
		DataType,
		FieldName,
		CompositeFieldName	
	)
	SELECT
		IsCheck,
		DataType,
		FieldName,
		FieldName   + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2)) AS CompositeFieldName
	FROM
		RecHubData.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE
		RecHubData.factDataEntryDetails.BatchID = @parmBatchID
	GROUP BY 
		FieldName, 
		IsCheck, 
		DataType;		

	SET @Loop = 1;
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @DEColumns) )
	BEGIN
		SELECT 
			@IsCheck = IsCheck,
			@DataType = DataType,
			@FieldName = FieldName
		FROM
			@DEColumns
		WHERE
			RowID = @Loop;

		EXEC IntegrationTest.usp_AddDataEntryColumn
			@parmIsCheck = @IsCheck,
			@parmDataType = @DataType,
			@parmFieldName = @FieldName;

		SET @Loop += 1;
	END

	SELECT @PivotedColumns = @PivotedColumns + ',' +
		CASE 
			WHEN IsCheck = 0 THEN 'PivotedStubs.'
			ELSE 'PivotedChecks.'
		END  + CompositeFieldName
	FROM 
		@DEColumns
	WHERE
		IsCheck = @parmIsCheck;

	SELECT 
		@UniqueDEColumns = @UniqueDEColumns + ', ' + CompositeFieldName
	FROM 
		@DEColumns
	WHERE
		IsCheck = @parmIsCheck;

	SELECT @UniqueDEColumns = LTRIM(STUFF(@UniqueDEColumns, 1 , 1, ''));

	SELECT 
		@PivotDEColumnsToSelect = @PivotDEColumnsToSelect + ', ' +  CompositeFieldName
	FROM 
		@DEColumns
	WHERE
		IsCheck = @parmIsCheck;

	SELECT 
		@MergeColumns = @MergeColumns + 'TARGET.' + CompositeFieldName + ' = SOURCE.' + CompositeFieldName + ','
	FROM 
		@DEColumns
	WHERE
		IsCheck = @parmIsCheck;

	SELECT @MergeColumns = LTRIM(STUFF(@MergeColumns, LEN(@MergeColumns) , 1, ''));

	SET @SQLStatement = N'
		;WITH DetailRows AS 
		(
			SELECT 
				RecHubData.factDataEntryDetails.DepositDateKey, 
				RecHubData.factDataEntryDetails.BatchID, 
				RecHubData.factDataEntryDetails.TransactionID, 
				RecHubData.factDataEntryDetails.BatchSequence, 
				RecHubData.dimWorkgroupDataEntryColumns.FieldName  + ''_'' + CAST(RecHubData.dimWorkgroupDataEntryColumns.IsCheck AS NVARCHAR(1)) + ''_'' + CAST(RecHubData.dimWorkgroupDataEntryColumns.DataType AS NVARCHAR(2)) AS FieldName, 
				RecHubData.factDataEntryDetails.DataEntryValue 
			FROM 
				RecHubData.factDataEntryDetails
				INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey  
			WHERE
				BatchID = ' + CAST(@parmBatchID AS VARCHAR(30)) + N'
				AND IsCheck = ' + CAST(@parmIsCheck AS VARCHAR(1)) + N'
		),
		PivotedRows AS
		(
			SELECT 
				DepositDateKey, 
				BatchID, 
				TransactionID, 
				BatchSequence' + @PivotDEColumnsToSelect +	N'
			FROM 
			(
				SELECT  
					DepositDateKey, 
					BatchID, 
					TransactionID, 
					BatchSequence, 
					FieldName, 
					DataEntryValue 
				FROM 
					DetailRows
			) p
			PIVOT
			(
				MIN(DataEntryValue) FOR FieldName IN (' + @UniqueDEColumns + ')
			) 
		AS PVT)
		MERGE RecHubData.' + CASE @parmIsCheck WHEN 1 THEN 'factChecks' ELSE 'factStubs' END + ' AS TARGET
		USING PivotedRows AS SOURCE
		ON(TARGET.DepositDateKey = SOURCE.DepositDateKey AND TARGET.BatchID = SOURCE.BatchID AND TARGET.TransactionID = SOURCE.TransactionID AND TARGET.BatchSequence = SOURCE.BatchSequence)
		WHEN MATCHED 
		THEN UPDATE SET ' + @MergeColumns + ';'

	EXEC (@SQLStatement);
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH