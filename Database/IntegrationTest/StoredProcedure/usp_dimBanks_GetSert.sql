IF OBJECT_ID('IntegrationTests.usp_dimBanks_GetSert') IS NOT NULL
       DROP PROCEDURE IntegrationTests.usp_dimBanks_GetSert
GO

CREATE PROCEDURE IntegrationTests.usp_dimBanks_GetSert
(
	@parmSiteBankID		INT,
	@parmBankName		VARCHAR(128),
	@parmFIEntityID		INT = NULL,
	@parmABA			VARCHAR(10) = NULL,
	@parmBankKey		INT	OUTPUT
)
AS

SET NOCOUNT ON;
BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimBanks WHERE SiteBankID = @parmSiteBankID)
	BEGIN
		INSERT INTO RecHubData.dimBanks(SiteBankID,BankName,EntityID,ABA,MostRecent,CreationDate,ModificationDate)
		SELECT @parmSiteBankID,@parmBankName,@parmFIEntityID,@parmABA,1,GETDATE(),GETDATE();

		SET @parmBankKey = SCOPE_IDENTITY();
	END
	ELSE
		SELECT @parmBankKey = BankKey FROM RecHubData.dimBanks WHERE SiteBankID = @parmSiteBankID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


