IF OBJECT_ID('IntegrationTest.usp_dimWorkgroupDataEntryColumns_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimWorkgroupDataEntryColumns_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimWorkgroupDataEntryColumns_Ins
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmBatchSource 			VARCHAR(30),
	@parmIsCheck				BIT,
	@parmIsActive				BIT,
	@parmIsRequired				BIT,
	@parmMarkSense				BIT,
	@parmDataType				TINYINT,
	@parmScreenOrder			TINYINT,
	@parmUILabel				NVARCHAR(64),
	@parmFieldName				NVARCHAR(256),
	@parmSourceDisplayName		NVARCHAR(256)
)
AS

SET NOCOUNT ON;
DECLARE @BatchSourceKey SMALLINT;

BEGIN TRY
	SELECT 
		@BatchSourceKey = BatchSourceKey
	FROM 
		RecHubData.dimBatchSources 
	WHERE 
		ShortName = @parmBatchSource;

	INSERT INTO RecHubData.dimWorkgroupDataEntryColumns
	(
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey,
		IsCheck,
		IsActive,
		IsRequired,
		MarkSense,
		DataType,
		ScreenOrder,
		CreationDate,
		UILabel,
		FieldName,
		SourceDisplayName
	)
	SELECT
		@parmSiteBankID,
		@parmSiteClientAccountID,
		@BatchSourceKey,
		@parmIsCheck,
		@parmIsActive,
		@parmIsRequired,
		@parmMarkSense,
		@parmDataType,
		@parmScreenOrder,
		GETDATE(),
		@parmUILabel,
		@parmFieldName,
		COALESCE(@parmSourceDisplayName,'');

	EXEC IntegrationTest.usp_AdvancedSearch_AddColumn
		@parmIsCheck	= @parmIsCheck,
		@parmDataType	= @parmDataType,
		@parmFieldName	= @parmFieldName;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
