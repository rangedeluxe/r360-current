IF OBJECT_ID('IntegrationTest.usp_dimDDAs_GetSert') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimDDAs_GetSert
GO

CREATE PROCEDURE IntegrationTest.usp_dimDDAs_GetSert
(
	@parmABA VARCHAR(10),
	@parmDDA VARCHAR(25),
	@parmDDAKey INT OUTPUT
)
AS

SET NOCOUNT ON;
BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimDDAs WHERE ABA = @parmABA AND DDA = @parmDDA)
	BEGIN
		INSERT INTO RecHubData.dimDDAs(ABA,DDA,CreationDate)
		SELECT @parmABA,@parmDDA,GETDATE();

		SET @parmDDAKey = SCOPE_IDENTITY();
	END
	ELSE
		SELECT @parmDDAKey = DDAKey FROM RecHubData.dimDDAs WHERE ABA = @parmABA AND DDA = @parmDDA;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH