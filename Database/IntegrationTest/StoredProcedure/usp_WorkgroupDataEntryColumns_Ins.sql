IF OBJECT_ID('IntegrationTest.usp_WorkgroupDataEntryColumns_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_WorkgroupDataEntryColumns_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_WorkgroupDataEntryColumns_Ins
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmBatchSource			VARCHAR(30),
	@parmIsCheck				BIT,
	@parmIsActive				BIT,
	@parmDataType				TINYINT,
	@parmIsRequired				BIT,
	@parmMarkSense				BIT,
	@parmScreenOrder			INT,
	@parmUILabel				VARCHAR(64),
	@parmFieldName				VARCHAR(256),
	@parmSourceDisplayName		VARCHAR(256)
)
AS

SET NOCOUNT ON;

DECLARE @BatchSourceKey SMALLINT;

BEGIN TRY

	SELECT @BatchSourceKey = BatchSourceKey
	FROM RecHubData.dimBatchSources
	WHERE ShortName = @parmBatchSource;

	if( @BatchSourceKey IS NULL )
		RAISERROR('Could not find batch source %s.',16,1,@parmBatchSource);


	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimWorkgroupDataEntryColumns WHERE 
						SiteBankID = @parmSiteBankID AND
						SiteClientAccountID = @parmSiteClientAccountID AND
						BatchSourceKey = @BatchSourceKey AND
						DataType = @parmDataType AND
						IsCheck = @parmIsCheck AND
						FieldName = @parmFieldName AND
						SourceDisplayName = @parmSourceDisplayName)
	BEGIN
		INSERT INTO RecHubData.dimWorkgroupDataEntryColumns
		(
			SiteBankID,
			SiteClientAccountID,
			BatchSourceKey,
			IsCheck,
			IsActive,
			IsRequired,
			MarkSense,
			DataType,
			ScreenOrder,
			UILabel,
			FieldName,
			SourceDisplayName,
			CreationDate,
			ModificationDate
		)
		SELECT
			@parmSiteBankID,
			@parmSiteClientAccountID,
			@BatchSourceKey,
			@parmIsCheck,
			@parmIsActive,
			@parmIsRequired,
			@parmMarkSense,
			@parmDataType,
			@parmScreenOrder,
			@parmUILabel,
			@parmFieldName,
			@parmSourceDisplayName,
			GETDATE(),
			GETDATE();

	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


