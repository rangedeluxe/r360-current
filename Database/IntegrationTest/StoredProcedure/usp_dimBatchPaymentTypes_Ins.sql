IF OBJECT_ID('IntegrationTest.usp_dimBatchPaymentTypes_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimBatchPaymentTypes_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimBatchPaymentTypes_Ins
(
	@parmShortName				VARCHAR(30),
	@parmLongName				VARCHAR(128),
	@parmBatchPaymentTypeKey 	TINYINT = NULL
)
AS

SET NOCOUNT ON;
DECLARE @BatchPaymentTypeKey TINYINT = 0;

BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimBatchPaymentTypes WHERE ShortName = @parmShortName)
	BEGIN
		IF @parmBatchPaymentTypeKey IS NULL 
			SELECT @BatchPaymentTypeKey = (COALESCE(MAX(BatchPaymentTypeKey),@BatchPaymentTypeKey)+1) FROM RecHubData.dimBatchPaymentTypes
		ELSE  SET @BatchPaymentTypeKey = @parmBatchPaymentTypeKey;

		INSERT INTO RecHubData.dimBatchPaymentTypes(BatchPaymentTypeKey,ShortName,LongName,CreationDate,ModificationDate)
		SELECT @BatchPaymentTypeKey,@parmShortName,@parmLongName,GETDATE(),GETDATE();
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


