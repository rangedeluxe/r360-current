IF OBJECT_ID('IntegrationTest.usp_factAdvancedSearch_InsCots') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factAdvancedSearch_InsCots
GO

CREATE PROCEDURE IntegrationTest.usp_factAdvancedSearch_InsCots
(
	@parmBatchID BIGINT
)
AS

SET NOCOUNT ON
BEGIN TRY

	INSERT INTO RecHubData.factAdvancedSearch
	(
		IsDeleted,
		BankKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		TransactionID,
		TxnSequence,
		CheckCount,
		ScannedCheckCount,
		StubCount,
		DocumentCount,
		OMRCount,
		CreationDate,
		ModificationDate
	)
	SELECT
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount,
		GETDATE(),
		GETDATE()
	FROM
		RecHubData.factTransactionSummary
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factChecks.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factChecks.IsDeleted = 0
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factStubs.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factStubs.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factStubs.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factStubs.IsDeleted = 0
	WHERE 
		RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND COALESCE(factTransactionSummary.CheckCount,0) = 0
		AND COALESCE(factTransactionSummary.StubCount,0) = 0
	GROUP BY
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH