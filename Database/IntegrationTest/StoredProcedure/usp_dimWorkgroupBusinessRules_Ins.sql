IF OBJECT_ID('IntegrationTest.usp_dimWorkgroupBusinessRules_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimWorkgroupBusinessRules_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimWorkgroupBusinessRules_Ins
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmInvoiceRequired		BIT,
	@parmBalancingRequired		BIT,
	@parmPDBalancingRequired    BIT,
	@parmPDPayerRequired		BIT
)
AS

SET NOCOUNT ON;

BEGIN TRY
	/* only insert records */
	IF NOT EXISTS(SELECT 1 FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID)
	BEGIN
		INSERT INTO RecHubData.dimWorkgroupBusinessRules 
		(
			SiteBankID,
			SiteClientAccountID,
			InvoiceRequired,
			BalancingRequired,
			PostDepositBalancingRequired,
			PostDepositPayerRequired,
			CreationDate
		)
		SELECT 
			@parmSiteBankID,
			@parmSiteClientAccountID,
			@parmInvoiceRequired,
			@parmBalancingRequired,
			@parmPDBalancingRequired,
			@parmPDPayerRequired,
			GETDATE();
	END
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


