IF OBJECT_ID('IntegrationTest.usp_factAdvancedSearch_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins
(
	@parmBatchID BIGINT
)
AS

SET NOCOUNT ON;

DECLARE @DataEntryCount INT;

BEGIN TRY

	SELECT @DataEntryCount = COUNT(*) 
	FROM 
		RecHubData.factDataEntryDetails 
	WHERE
		BatchID = @parmBatchID;


	IF( @DataEntryCount > 0 )
		EXEC IntegrationTest.usp_factAdvancedSearch_Ins_WithDataEntry @parmBatchID;
	ELSE
		EXEC IntegrationTest.usp_factAdvancedSearch_Ins_NoDataEntry @parmBatchID;

	EXEC IntegrationTest.usp_factAdvancedSearch_InsCots @parmBatchID;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH