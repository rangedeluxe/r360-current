IF OBJECT_ID('IntegrationTest.usp_factChecks_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factChecks_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factChecks_Ins
(
	@parmBankKey					INT,
	@parmClientAccountKey			INT,
	@parmDepositDateKey				INT,
	@parmImmutableDateKey			INT,
	@parmSourceProcessingDateKey	INT,
	@parmBatchID					BIGINT,
	@parmSourceBatchID				BIGINT,
	@parmBatchNumber				BIGINT,
	@parmBatchSourceKey				SMALLINT,
	@parmPaymentTypeKey				TINYINT,
	@parmBatchCueID					INT,
	@parmDepositStatus				INT,
	@parmTransactionID				INT,
	@parmTxnSequence				INT,
	@parmSequenceWithinTransaction	INT,
	@parmBatchSequence				INT,
	@parmCheckSequence				INT,
	@parmAmount						MONEY,
	@parmNumericRoutingNumber		BIGINT,
	@parmNumericSerial				BIGINT,
	@parmRoutingNumber				VARCHAR(30),
	@parmAccount					VARCHAR(30),
	@parmSerial						VARCHAR(30) = NULL,
	@parmTransactionCode			VARCHAR(30) = NULL,
	@parmRemitterName				VARCHAR(60) = NULL,
	@parmBatchSiteCode				INT = NULL,
	@parmABA						VARCHAR(30) = NULL,
	@parmDDA 						VARCHAR(30) = NULL

)
AS

SET NOCOUNT ON;

DECLARE @DDAKey INT = 0;

BEGIN TRY

	EXEC IntegrationTest.usp_dimDDAs_GetSert @parmABA, @parmDDA,  @DDAKey OUT;

	INSERT INTO RecHubData.factChecks
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DDAKey,
		BatchCueID,
		DepositStatus,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		CheckSequence,
		NumericRoutingNumber,
		NumericSerial,
		Amount,
		RoutingNumber,
		Account,
		Serial,
		TransactionCode,
		RemitterName,
		BatchSiteCode,
		CreationDate,
		ModificationDate
	)
	SELECT
		0,
		@parmBankKey,
		-1,
		@parmClientAccountKey,
		@parmDepositDateKey,
		@parmImmutableDateKey,
		@parmSourceProcessingDateKey,
		@parmBatchID,
		@parmSourceBatchID,
		@parmBatchNumber,
		@parmBatchSourceKey,
		@parmPaymentTypeKey,
		@DDAKey,
		@parmBatchCueID,
		@parmDepositStatus,
		@parmTransactionID,
		@parmTxnSequence,
		@parmSequenceWithinTransaction,
		@parmBatchSequence,
		@parmCheckSequence,
		@parmNumericRoutingNumber,
		@parmNumericSerial,
		@parmAmount,
		@parmRoutingNumber,
		@parmAccount,
		@parmSerial,
		@parmTransactionCode,
		@parmRemitterName,
		@parmBatchSiteCode,
		GETDATE(),
		GETDATE();

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
