IF OBJECT_ID('IntegrationTest.usp_dimDepositStatus_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_dimDepositStatus_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_dimDepositStatus_Ins
(
	@parmDepositStatusKey		INT,
	@parmDepositStatus			INT,
	@parmDespositDisplayName	VARCHAR(80)
)
AS

SET NOCOUNT ON;

BEGIN TRY
	INSERT INTO RecHubData.dimDepositStatuses(DepositStatusKey,DepositStatus,DespositDisplayName)
	SELECT @parmDepositStatusKey,@parmDepositStatus,@parmDespositDisplayName;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


