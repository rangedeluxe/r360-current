IF OBJECT_ID('IntegrationTests.usp_dimClientAccounts_GetSert') IS NOT NULL
       DROP PROCEDURE IntegrationTests.usp_dimClientAccounts_GetSert
GO

CREATE PROCEDURE IntegrationTests.usp_dimClientAccounts_GetSert
(
	@parmSiteBankID				INT,
	@parmSiteClientAccountID	INT,
	@parmDataRetentionDays		INT,
	@parmImageRetentionDays		INT,
	@parmShortName				VARCHAR(128),
	@parmLongName				VARCHAR(128),
	@parmClientAccountKey		INT	OUTPUT
)
AS

SET NOCOUNT ON;
BEGIN TRY
	IF NOT EXISTS( SELECT 1 FROM RecHubData.dimClientAccounts WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID)
	BEGIN
		INSERT INTO RecHubData.dimClientAccounts
		(
			SiteBankID,
			SiteClientAccountID,
			SiteOrganizationID,
			ShortName,
			LongName,
			IsActive,
			Cutoff,
			OnlineColorMode,
			IsCommingled,
			DataRetentionDays,
			ImageRetentionDays,
			SiteCodeID,
			MostRecent,
			CreationDate,
			ModificationDate
		)
		SELECT 
			@parmSiteBankID,
			@parmSiteClientAccountID,
			-1,
			@parmShortName,
			@parmLongName,
			1,
			0,
			0,
			0,
			@parmDataRetentionDays,
			@parmImageRetentionDays,
			-1,
			1,
			GETDATE(),
			GETDATE();

		SET @parmClientAccountKey = SCOPE_IDENTITY();
	END
	ELSE
		SELECT @parmClientAccountKey = ClientAccountKey FROM RecHubData.dimClientAccounts WHERE SiteBankID = @parmSiteBankID AND SiteClientAccountID = @parmSiteClientAccountID;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH


