IF OBJECT_ID('IntegrationTest.usp_factStubs_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factStubs_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_factStubs_Ins
(
	@parmBankKey					INT,
	@parmClientAccountKey			INT,
	@parmDepositDateKey				INT,
	@parmImmutableDateKey			INT,
	@parmSourceProcessingDateKey	INT,
	@parmBatchID					BIGINT,
	@parmSourceBatchID				BIGINT,
	@parmBatchNumber				BIGINT,
	@parmBatchSourceKey				SMALLINT,
	@parmPaymentTypeKey				TINYINT,
	@parmBatchCueID					INT,
	@parmDepositStatus				INT,
	@parmSystemType					TINYINT,
	@parmTransactionID				INT,
	@parmTxnSequence				INT,
	@parmSequenceWithinTransaction	INT,
	@parmBatchSequence				INT,
	@parmStubSequence				INT,
	@parmIsCorrespondence			BIT,
	@parmDocumentBatchSequence		INT = NULL,
	@parmIsOMRDetected				BIT = NULL,
	@parmAmount						MONEY = NULL,
	@parmAccountNumber				VARCHAR(80) = NULL,
	@parmBatchSiteCode				INT = NULL
)
AS

SET NOCOUNT ON;

DECLARE @DDAKey INT = 0;

BEGIN TRY

	INSERT INTO RecHubData.factStubs
	(
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		SystemType,
		DepositStatus,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		DocumentBatchSequence,
		IsOMRDetected,
		Amount,
		AccountNumber,
		BatchSiteCode,
		CreationDate,
		ModificationDate
	)
	SELECT
		0,
		@parmBankKey,
		-1,
		@parmClientAccountKey,
		@parmDepositDateKey,
		@parmImmutableDateKey,
		@parmSourceProcessingDateKey,
		@parmBatchID,
		@parmSourceBatchID,
		@parmBatchNumber,
		@parmBatchSourceKey,
		@parmPaymentTypeKey,
		@parmBatchCueID,
		@parmSystemType,
		@parmDepositStatus,
		@parmTransactionID,
		@parmTxnSequence,
		@parmSequenceWithinTransaction,
		@parmBatchSequence,
		@parmStubSequence,
		@parmIsCorrespondence,
		@parmDocumentBatchSequence,
		@parmIsOMRDetected,
		@parmAmount,
		@parmAccountNumber,
		@parmBatchSiteCode,
		GETDATE(),
		GETDATE();

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
