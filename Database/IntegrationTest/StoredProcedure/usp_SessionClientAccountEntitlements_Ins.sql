IF OBJECT_ID('IntegrationTest.usp_SessionClientAccountEntitlements_Ins') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_SessionClientAccountEntitlements_Ins
GO

CREATE PROCEDURE IntegrationTest.usp_SessionClientAccountEntitlements_Ins
(
	@parmSessionID 				UNIQUEIDENTIFIER,
	@parmStartDateKey			INT,
	@parmEndDateKey				INT,
	@parmSiteBankID 			INT,
	@parmSiteClientAccountID 	INT,
	@parmViewAhead				BIT,
	@parmViewingDays			INT,
	@parmMaxSearchDays			INT,
	@parmEntityID				INT,
	@parmEntityName				VARCHAR(128)

)
AS

BEGIN TRY
	INSERT INTO RecHubUser.SessionClientAccountEntitlements
	(
		SessionID,
		EntityID,
		SiteBankID,
		SiteClientAccountID,
		ClientAccountKey,
		ViewingDays,
		MaximumSearchDays,
		StartDateKey,
		EndDateKey,
		ViewAhead,
		EntityName,
		CreationDate
	)
	SELECT
		@parmSessionID,
		@parmEntityID,
		@parmSiteBankID,
		@parmSiteClientAccountID,
		ClientAccountKey,
		@parmViewingDays,
		@parmMaxSearchDays,
		@parmStartDateKey,
		@parmEndDateKey,
		@parmViewAhead,
		@parmEntityName,
		GETDATE()
	FROM
		RecHubData.dimClientAccounts
	WHERE
		SiteBankID = @parmSiteBankID
		AND SiteClientAccountID = @parmSiteClientAccountID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
