IF OBJECT_ID('IntegrationTest.usp_factAdvancedSearch_Ins_NoDataEntry') IS NOT NULL
       DROP PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins_NoDataEntry
GO

CREATE PROCEDURE IntegrationTest.usp_factAdvancedSearch_Ins_NoDataEntry
(
	@parmBatchID BIGINT
)
AS

SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO RecHubData.factAdvancedSearch
	(
		IsDeleted,
		BankKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		TransactionID,
		TxnSequence,
		CheckCount,
		ScannedCheckCount,
		StubCount,
		DocumentCount,
		OMRCount,
		CheckBatchSequence,
		CheckSequence,
		CheckAmount,
		RoutingNumber,
		CheckAccountNumber,
		TransactionCode,
		Serial,
		RemitterName,
		DDAKey,
		NumericRoutingNumber,
		NumericSerial,
		StubBatchSequence,
		StubSequence,
		StubAmount,
		StubAccountNumber,
		CreationDate,
		ModificationDate
	)
	SELECT
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount,
		CASE 
			WHEN RecHubData.factChecks.BatchSequence IS NOT NULL THEN factChecks.BatchSequence
			WHEN RecHubData.factStubs.BatchSequence IS NOT NULL THEN factStubs.BatchSequence
		END AS BatchSequence,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.Amount AS CheckAmount,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.DDAKey,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factStubs.BatchSequence AS StubBatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.Amount AS StubAmount,
		RecHubData.factStubs.AccountNumber,
		GETDATE(),
		GETDATE()
	FROM
		RecHubData.factTransactionSummary
		LEFT JOIN RecHubData.factChecks ON RecHubData.factChecks.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factChecks.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factChecks.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factChecks.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factChecks.IsDeleted = 0
		LEFT JOIN RecHubData.factStubs ON RecHubData.factStubs.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
			AND RecHubData.factStubs.BatchID = RecHubData.factTransactionSummary.BatchID
			AND RecHubData.factStubs.TransactionID = RecHubData.factTransactionSummary.TransactionID
			AND RecHubData.factStubs.TxnSequence = RecHubData.factTransactionSummary.TxnSequence
			AND RecHubData.factStubs.IsDeleted = 0
	WHERE 
		RecHubData.factTransactionSummary.BatchID = @parmBatchID
		AND (COALESCE(factTransactionSummary.CheckCount,0) <> 0 OR COALESCE(factTransactionSummary.StubCount,0) <> 0 OR COALESCE(factTransactionSummary.DocumentCount,0) <> 0)
	GROUP BY
		RecHubData.factTransactionSummary.IsDeleted,
		RecHubData.factTransactionSummary.BankKey,
		RecHubData.factTransactionSummary.ClientAccountKey,
		RecHubData.factTransactionSummary.DepositDateKey,
		RecHubData.factTransactionSummary.ImmutableDateKey,
		RecHubData.factTransactionSummary.SourceProcessingDateKey,
		RecHubData.factTransactionSummary.BatchID,
		RecHubData.factTransactionSummary.SourceBatchID,
		RecHubData.factTransactionSummary.BatchNumber,
		RecHubData.factTransactionSummary.BatchSourceKey,
		RecHubData.factTransactionSummary.BatchPaymentTypeKey,
		RecHubData.factTransactionSummary.DepositStatus,
		RecHubData.factTransactionSummary.TransactionID,
		RecHubData.factTransactionSummary.TxnSequence,
		RecHubData.factTransactionSummary.CheckCount,
		RecHubData.factTransactionSummary.ScannedCheckCount,
		RecHubData.factTransactionSummary.StubCount,
		RecHubData.factTransactionSummary.DocumentCount,
		RecHubData.factTransactionSummary.OMRCount,
		RecHubData.factChecks.BatchSequence,
		RecHubData.factChecks.CheckSequence,
		RecHubData.factChecks.Amount,
		RecHubData.factChecks.RoutingNumber,
		RecHubData.factChecks.Account,
		RecHubData.factChecks.TransactionCode,
		RecHubData.factChecks.Serial,
		RecHubData.factChecks.RemitterName,
		RecHubData.factChecks.DDAKey,
		RecHubData.factChecks.NumericRoutingNumber,
		RecHubData.factChecks.NumericSerial,
		RecHubData.factStubs.BatchSequence,
		RecHubData.factStubs.StubSequence,
		RecHubData.factStubs.Amount,
		RecHubData.factStubs.AccountNumber;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpCombinedBaseRecords')) 
		DROP TABLE #tmpCombinedBaseRecords;
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH