--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorFunctionName udf_EventAuditMessageBuilder
--WFSScriptProcessorFunctionDrop
IF OBJECT_ID('RecHubAudit.udf_EventAuditMessageBuilder') IS NOT NULL
	DROP Function RecHubAudit.udf_EventAuditMessageBuilder
GO
--WFSScriptProcessorFunctionCreate
CREATE FUNCTION RecHubAudit.udf_EventAuditMessageBuilder
(
	@parmAuditDateKey	INT,
	@parmAuditKey		UNIQUEIDENTIFIER
)
RETURNS VARCHAR(MAX)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: John P. Boehm
* Date: 04/30/2015
*
* Purpose: Return the concatenated factEventAuditMessages based on the AuditDate
*		and AuditKey.
*
*
* Modification History
* 05/15/2015 WI 213460 MGE	Created
******************************************************************************/
BEGIN
DECLARE @AuditMessage VARCHAR(MAX) = '';

	SELECT 
		@AuditMessage = @AuditMessage + RecHubAudit.factEventAuditMessages.AuditMessage
	FROM 
		RecHubAudit.factEventAuditMessages
	WHERE 
		RecHubAudit.factEventAuditMessages.AuditDateKey = @parmAuditDateKey
		AND RecHubAudit.factEventAuditMessages.AuditKey = @parmAuditKey
	ORDER BY 
		RecHubAudit.factEventAuditMessages.AuditMessagePart;

	RETURN @AuditMessage;
END