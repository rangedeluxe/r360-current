--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ExtractAudit
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_Report_ExtractAudit') IS NOT NULL
	DROP PROCEDURE RecHubAudit.usp_Report_ExtractAudit;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_Report_ExtractAudit
(
	@parmUserID				INT,
	@parmReportParameters	XML,
	@parmSessionID			UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     07/17/2013
*
* Purpose:  Gets the data needed for the Extract Audit Report
*
* Modification History
* Created
* 07/17/2013 WI 108651 KLC	Created.
* 09/03/2013 WI 108651 KLC	Fixed the date range to include the end date
* 06/01/2016 WI 283084 JAW	Only include workgroups the user has permission for.
*							Change WHERE to use AuditDateKey instead of CreationDate.
* 06/13/2016 WI 285768 MGE	Add Workgroup and EntityID columns to the result set
* 06/21/2016 WI 285768 MGE	Use LongName instead of ShortName for Workgroup
* 06/22/2016 WI 285768 MGE	Use ShortName if LongName is Null for Workgroup
**************************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @startDate		DATETIME,
			@endDate		DATETIME,
			@startDateKey	INT,
			@endDateKey		INT,
			@filterUserID	INT;

	SELECT	@startDate = CASE WHEN Parms.val.value('(SD)[1]', 'VARCHAR(10)') <> '' THEN Parms.val.value('(SD)[1]', 'DATETIME') ELSE NULL END,
			@endDate = CASE WHEN Parms.val.value('(ED)[1]', 'VARCHAR(10)') <> '' THEN Parms.val.value('(ED)[1]', 'DATETIME') ELSE NULL END
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);

	SET @startDateKey = CAST(CONVERT(VARCHAR, @startDate, 112) AS INT);
	SET @endDateKey = CAST(CONVERT(VARCHAR, @endDate, 112) AS INT);

	SELECT	CONVERT(VARCHAR(20), RecHubAudit.factExtractAudits.CreationDate, 22) AS CreationDate,
			CONVERT(VARCHAR(20), RecHubAudit.factExtractAudits.ExtractCreated, 22) AS ExtractCreated,
			RecHubUser.SessionClientAccountEntitlements.EntityID,
			CAST (RecHubData.dimClientAccounts.SiteClientAccountID AS VARCHAR(10)) + ' - ' + Coalesce(LongName,Shortname) AS Workgroup,
			RecHubAudit.factExtractAudits.IsComplete,
			RecHubAudit.factExtractAudits.ExtractCount1,
			RecHubAudit.factExtractAudits.ExtractCount2,
			RecHubAudit.factExtractAudits.ExtractAmount1,
			RecHubAudit.factExtractAudits.ExtractAmount2,
			RecHubAudit.factExtractAudits.ExtractRecordCount,
			RecHubAudit.factExtractAudits.ExtractFileName
	FROM
		RecHubAudit.factExtractAudits
		INNER JOIN RecHubUser.SessionClientAccountEntitlements ON
			RecHubUser.SessionClientAccountEntitlements.SessionID = @parmSessionID AND
			RecHubUser.SessionClientAccountEntitlements.ClientAccountKey = RecHubAudit.factExtractAudits.ClientAccountKey
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubAudit.factExtractAudits.ClientAccountKey
	WHERE RecHubAudit.factExtractAudits.AuditDateKey >= @startDateKey
		AND RecHubAudit.factExtractAudits.AuditDateKey <= @endDateKey
		AND IsDeleted = 0
	ORDER BY CONVERT(VARCHAR(20), RecHubAudit.factExtractAudits.CreationDate, 22) ASC,
					   RecHubUser.SessionClientAccountEntitlements.EntityName ASC,
					   RecHubData.dimClientAccounts.SiteClientAccountID;


	DECLARE @auditMessage VARCHAR(1024) = 'Extract Audit executed for parameters -'
											+ ' Date Range: ' + CONVERT(VARCHAR(8), @startDate, 112) + ' - ' + CONVERT(VARCHAR(8), @endDate, 112)

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubAudit.usp_Report_ExtractAudit', @parmEventName='Report Executed', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH