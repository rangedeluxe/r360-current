--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_factDITClientAudit_Upd_ProcessingEnd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_factDITClientAudit_Upd_ProcessingEnd') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_factDITClientAudit_Upd_ProcessingEnd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factDITClientAudit_Upd_ProcessingEnd
(
	@parmAuditDateKey				INT,
	@parmSourceTrackingID			UNIQUEIDENTIFIER,
	@parmProcessingEnd				DATETIME2,
	@parmIsDataSuccessful			BIT,
	@parmSiteBankID					INT = -1,
	@parmSiteClientAccountID		INT = -1,
	@parmImmutableDateKey			INT = -1,
	@parmSourceBatchID				INT = -1,
	@parmPaymentCount				INT = -1,
	@parmStubCount					INT = -1,
	@parmDocumentCount				INT = -1
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/22/2016
*
* Purpose: Update a DIT Client audit record with end time.
*
*
* NOTES: 
*	AuditDateKey is the DIT file creation date. 
*	Application code must use System.Data.SqlDbType.DateTime2 for @parmProcessingEnd
*		to ensure correct percision.
*
*
* Modification History
* 01/22/2016 WI 259281 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	UPDATE 
		RecHubAudit.factDITClientAudit
	SET
		ProcessingEnd = @parmProcessingEnd,
		IsDataSuccessful = @parmIsDataSuccessful,
		SiteBankID = 
			CASE @parmSiteBankID
				WHEN -1 THEN SiteBankID
				ELSE @parmSiteBankID
			END,
		SiteClientAccountID = 
			CASE @parmSiteClientAccountID
				WHEN -1 THEN SiteClientAccountID
				ELSE @parmSiteClientAccountID
			END,
		ImmutableDateKey = 
			CASE @parmImmutableDateKey
				WHEN -1 THEN ImmutableDateKey
				ELSE @parmImmutableDateKey
			END,
		SourceBatchID = 
			CASE @parmSourceBatchID
				WHEN -1 THEN SourceBatchID
				ELSE @parmSourceBatchID
			END,
		PaymentCount = 
			CASE @parmPaymentCount
				WHEN -1 THEN PaymentCount
				ELSE @parmPaymentCount
			END,
		StubCount = 
			CASE @parmStubCount
				WHEN -1 THEN StubCount
				ELSE @parmStubCount
			END,
		DocumentCount = 
			CASE @parmDocumentCount
				WHEN -1 THEN DocumentCount
				ELSE @parmDocumentCount
			END,
		ModificationDate = SYSDATETIME()
	WHERE
		AuditDateKey = @parmAuditDateKey
		AND SourceTrackingID = @parmSourceTrackingID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH