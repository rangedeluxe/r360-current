--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimAuditEvent_Get_EventType
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_dimAuditEvent_Get_EventType') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_dimAuditEvent_Get_EventType
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_dimAuditEvent_Get_EventType 

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: LA
* Date: 09/15/2014
*
* Purpose: Get the EventType for the event beging audited. 
*
*
* Modification History
* 09/15/2014 WI 165729 LA	Created
* 06/24/2015 WI 219771 JPB	Filter out event type 'File Submission'
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	/* get the event audit type */
	SELECT DISTINCT
		EventType
	FROM 
		RecHubAudit.dimAuditEvents
	WHERE
		EventType <> 'File Submission';

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
