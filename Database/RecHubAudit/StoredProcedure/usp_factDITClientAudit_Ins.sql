--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubData">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_factDITClientAudit_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_factDITClientAudit_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_factDITClientAudit_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factDITClientAudit_Ins
(
	@parmAuditDateKey				INT,
	@parmSourceTrackingID			UNIQUEIDENTIFIER,
	@parmProcessingStart			DATETIME2
)
AS 
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/08/2016
*
* Purpose: Insert a new DIT Client audit record.
*
*
* NOTES: 
*	AuditDateKey is the DIT file creation date. 
*	Application code must use System.Data.SqlDbType.DateTime2 for @parmProcessingStart
*		to ensure correct percision.
*
*
* Modification History
* 01/08/2016 WI 255646  JPB	Created.
* 01/22/2016 WI 259474 JPB	Renamed DITTrackingID to SourceTrackingID.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	INSERT INTO RecHubAudit.factDITClientAudit
	(
		IsDeleted,
		AuditDateKey,
		SourceTrackingID,
		ProcessingStart
	)
	VALUES
	(
		0,
		@parmAuditDateKey,
		@parmSourceTrackingID,
		@parmProcessingStart
	);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH