--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_dimAuditEvents_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_dimAuditEvents_Get') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_dimAuditEvents_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_dimAuditEvents_Get 
(
	@parmEventName		VARCHAR(64),
	@parmEventType		VARCHAR(64),
	@parmAuditEventKey	INT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Get the key for the event beging audited. If the event 
*	name is not in the table, add it.
*
*
* Modification History
* 05/30/2013 WI 103627 JPB	Created
* 08/27/2014 WI 158041 LA 	Modified to Add EventType Logic 
******************************************************************************/
SET NOCOUNT ON; 
SET XACT_ABORT ON;
DECLARE @LockResult INT,
		@RetryCount SMALLINT;

BEGIN TRY
	SELECT	@parmAuditEventKey = -1,
			@RetryCount = 4;


	BEGIN TRANSACTION;
	/* To keep from duplicate records from being created, lock the process */
	WHILE( @RetryCount >= 0 )
	BEGIN
		EXEC @LockResult = sp_getapplock 
			@Resource='dimAuditEvents_Get',
			@LockOwner='Transaction',
			@LockMode='Exclusive';

		IF( @LockResult < 0 )
		BEGIN 
			IF( @RetryCount > 0 )
			BEGIN
				SET @RetryCount = @RetryCount - 1;
				WAITFOR DELAY '00:00:05'; /* wait 5 seconds and try again */		
			END
			ELSE
			BEGIN
				SET @RetryCount = -1;
				RAISERROR('Could not obtain lock on dimAuditEvents table.',16,1) WITH NOWAIT;	
			END
		END
		ELSE
			SET @RetryCount = -1;
	END

	/* get the event audit key */
	SELECT 
		@parmAuditEventKey = AuditEventKey
	FROM 
		RecHubAudit.dimAuditEvents
	WHERE 
		UPPER(EventName) = UPPER(@parmEventName);

	IF( @parmAuditEventKey = -1 )
	BEGIN /* application not found */
		DECLARE @InsertRecord TABLE
		(
			AuditEventKey INT
		);

		BEGIN TRY
			INSERT INTO RecHubAudit.dimAuditEvents(EventName, EventType)
			OUTPUT inserted.AuditEventKey INTO @InsertRecord
			VALUES (@parmEventName, @parmEventType);
			
			SELECT 
				@parmAuditEventKey = AuditEventKey
			FROM 
				@InsertRecord;
		END TRY
		BEGIN CATCH
			IF( ERROR_NUMBER() = 2601 ) /* Duplicate record, should NEVER get here */
				SELECT 
					@parmAuditEventKey = AuditEventKey
				FROM 
					RecHubAudit.dimAuditEvents
				WHERE UPPER(EventName) = UPPER(@parmAuditEventKey);
		END CATCH
	END

	EXEC @LockResult = sp_releaseapplock
		@Resource='dimAuditEvents_Get';

	COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF( APPLOCK_MODE('public','dimAuditEvents_Get','Transaction') <> 'NoLock' )
		EXEC @LockResult = sp_releaseapplock
			@Resource='dimAuditEvents_Get';

	IF XACT_STATE() != 0 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
