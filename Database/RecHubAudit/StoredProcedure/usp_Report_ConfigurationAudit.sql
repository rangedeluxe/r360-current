--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_ConfigurationAudit
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_Report_ConfigurationAudit') IS NOT NULL
	DROP PROCEDURE RecHubAudit.usp_Report_ConfigurationAudit;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_Report_ConfigurationAudit
(
	@parmUserID				INT,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     07/08/2013
*
* Purpose:  Gets the data needed for the Configuration Audit Report
*
* Modification History
* Created
* 07/08/2013 WI 108649 KLC	Created.
* 10/01/2014 WI 167231 LA	Added RAAM Entity Data temp table.
* 11/06/2014 WI 176375 PKW  Added custom message for ExtractDefinition entries table reference.
* 11/11/2014 WI 177178 LA   Added Distinct Clause to get rid of duplicate records 
* 04/20/2015 WI 202940 JPB	Added DB Connection Permissions tags
* 04/30/2015 WI 204349 JPB	Performance Tuning and code cleanup
* 05/08/2015 WI 212222 MA	Updated EntityID with EntityName for event audit
*******************************************************************************/
SET NOCOUNT ON;

DECLARE @startDateKey	INT,
		@endDateKey		INT,
		@filterUserID	INT,
		@filterEntityID	INT,
		@userSelected	VARCHAR(100);

BEGIN TRY


	SELECT	
		@startDateKey = CASE WHEN Parms.val.value('(SD)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(SD)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
		@endDateKey = CASE WHEN Parms.val.value('(ED)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(ED)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
		@filterUserID = Parms.val.value('(USER)[1]', 'INT'),
		@filterEntityID = Parms.val.value('(ENT)[1]', 'INT')
	FROM 
		@parmReportParameters.nodes('/Parameters') AS Parms(val);

	/* Create temp tables to reduce execution times */
	IF OBJECT_ID('tempdb..#Report_Entities') IS NOT NULL
        DROP TABLE #Report_Entities;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummary') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummary;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryColumns') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryColumns;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryUsers') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryUsers;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails_Combined') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails_Combined;
	IF OBJECT_ID('tempdb..#Report_OldfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_OldfactDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_NewfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_NewfactDataAuditDetails;

	/* Entities to be used for RAAM info */
	CREATE TABLE #Report_Entities
	(
		RowID		INT NOT NULL IDENTITY(1,1),
		EntityID	INT,
		BreadCrumb	VARCHAR(100),
		PRIMARY KEY (EntityID,RowID)
	);
 
	/* factDataAuditSummary records that match request */
	 CREATE TABLE #Report_factDataAuditSummary
	(
		AuditDateKey INT NOT NULL,
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		ModificationDate DATETIME NOT NULL,
		AuditType TINYINT NOT NULL,
		UserID INT NOT NULL,
		AuditColumnKey INT NOT NULL,
		ApplicationName VARCHAR(256) NOT NULL,
		AuditValue VARCHAR(256) NULL,
		LogonName VARCHAR(24) NOT NULL,
		BreadCrumb	VARCHAR(100) NOT NULL,
		PRIMARY KEY (AuditKey)
	);

	/* factDataAuditSummary columns */
	CREATE TABLE #Report_factDataAuditSummaryColumns
	(
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		AuditColumnKey INT NOT NULL,
		TableName VARCHAR(128) NOT NULL,
		ColumnName VARCHAR(128) NOT NULL,
		PRIMARY KEY (AuditKey,AuditColumnKey)
	);

	/* factDataAuditSummary user info */
	CREATE TABLE #Report_factDataAuditSummaryUsers
	(
		UserID INT NOT NULL,
		LogonName VARCHAR(24) NOT NULL,
		LogonEntityID INT NULL,
		BreadCrumb	VARCHAR(100) NOT NULL,
		PRIMARY KEY (UserID)
	);

	/* factDataAuditDetails records that match request */
	CREATE TABLE #Report_factDataAuditDetails
	(
		RowID INT NOT NULL IDENTITY(1,1),
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		AuditType TINYINT NOT NULL,
		AuditApplicationKey INT NOT NULL,
		AuditColumnKey INT NOT NULL,
		AuditValue VARCHAR(256) NULL,
		PRIMARY KEY (RowID,AuditKey,AuditType)
	);

	/* actDataAuditDetails that hold the new and old values */
	CREATE TABLE #Report_factDataAuditDetails_Combined
	(
		RowID INT NOT NULL IDENTITY(1,1),
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		ColumnName VARCHAR(128) NOT NULL,
		NewValue VARCHAR(256) NULL,
		OldValue VARCHAR(256) NULL,
		PRIMARY KEY (RowID,AuditKey)
	);

	/* factDataAuditDetails that hold the previous value */
	CREATE TABLE #Report_OldfactDataAuditDetails
	(
		RowID INT NOT NULL IDENTITY(1,1),
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		AuditApplicationKey INT NOT NULL,
		AuditColumnKey INT NOT NULL,
		AuditValue VARCHAR(256) NULL,
		PRIMARY KEY (RowID,AuditKey,AuditApplicationKey,AuditColumnKey)
	);
    

	/* factDataAuditDetails that hold the new value */
	CREATE TABLE #Report_NewfactDataAuditDetails
	(
		RowID INT NOT NULL IDENTITY(1,1),
		AuditKey UNIQUEIDENTIFIER NOT NULL,
		AuditApplicationKey INT NOT NULL,
		AuditColumnKey INT NOT NULL,
		AuditValue VARCHAR(256) NULL,
		ColumnName VARCHAR(128) NOT NULL,
		PRIMARY KEY (RowID,AuditKey,AuditApplicationKey,AuditColumnKey)
	);

	/* Get the entities passed in via the XML RAAM/Entity node */
	INSERT INTO #Report_Entities
	(
		EntityID,
		BreadCrumb       
	)
	SELECT     
		entityRequest.att.value('@EntityID', 'int') AS EntityID,
		entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
	FROM 
		@parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);


	INSERT INTO #Report_factDataAuditSummaryUsers(UserID,LogonName,LogonEntityID,BreadCrumb)
	SELECT
		RecHubUser.Users.UserID,
		RecHubUser.Users.LogonName,
		RecHubUser.Users.LogonEntityID,
		#Report_Entities.BreadCrumb
	FROM
		RecHubUser.Users
		INNER JOIN #Report_Entities ON #Report_Entities.EntityID = RecHubUser.Users.LogonEntityID

	/* Get the factDataAuditSummary reords that match request, fact table so the query is recompiled */
	INSERT INTO #Report_factDataAuditSummary
	(
		AuditDateKey,
		AuditKey,
		ModificationDate,
		AuditType,
		UserID,
		AuditColumnKey,
		ApplicationName,
		AuditValue,
		LogonName,
		BreadCrumb
	)
	SELECT	DISTINCT 
		RecHubAudit.factDataAuditSummary.AuditDateKey,
		RecHubAudit.factDataAuditSummary.AuditKey,
		RecHubAudit.factDataAuditSummary.ModificationDate,
		RecHubAudit.factDataAuditSummary.AuditType,
		RecHubAudit.factDataAuditSummary.UserID,
		RecHubAudit.factDataAuditSummary.AuditColumnKey,
		RecHubAudit.dimAuditApplications.ApplicationName,
		RecHubAudit.factDataAuditSummary.AuditValue,
		#Report_factDataAuditSummaryUsers.LogonName,
		#Report_factDataAuditSummaryUsers.BreadCrumb
	FROM 
		RecHubAudit.factDataAuditSummary
		INNER JOIN RecHubAudit.dimAuditApplications	ON RecHubAudit.dimAuditApplications.AuditApplicationKey = RecHubAudit.factDataAuditSummary.AuditApplicationKey
		INNER JOIN #Report_factDataAuditSummaryUsers ON #Report_factDataAuditSummaryUsers.UserID = RecHubAudit.factDataAuditSummary.UserID
	WHERE 
		RecHubAudit.factDataAuditSummary.AuditDateKey BETWEEN @startDateKey and @endDateKey
		AND RecHubAudit.factDataAuditSummary.IsDeleted = 0
		AND RecHubAudit.factDataAuditSummary.UserID = CASE WHEN @filterUserID  IS NULL THEN RecHubAudit.factDataAuditSummary.UserID ELSE @filterUserID END
	OPTION (RECOMPILE);

	IF( @@ROWCOUNT > 0 )
	BEGIN /* factDataAuditSummary records found */
		/* Get the factDataAuditSummary column info */
		INSERT INTO #Report_factDataAuditSummaryColumns
		(
			AuditKey,
			AuditColumnKey,
			TableName,
			ColumnName
		)
		SELECT	
			#Report_factDataAuditSummary.AuditKey,
			#Report_factDataAuditSummary.AuditColumnKey,
			RecHubAudit.dimAuditColumns.TableName,
			RecHubAudit.dimAuditColumns.ColumnName
		FROM 
			#Report_factDataAuditSummary
			INNER JOIN RecHubAudit.dimAuditColumns ON  RecHubAudit.dimAuditColumns.AuditColumnKey =  #Report_factDataAuditSummary.AuditColumnKey;

		/* Get the factDataAuditDetail records that match request, fact table so the query is recompiled */
		INSERT INTO #Report_factDataAuditDetails
		(
			AuditKey,
			AuditType,
			AuditApplicationKey,
			AuditColumnKey,
			AuditValue
		)
		SELECT DISTINCT
			RecHubAudit.factDataAuditDetails.AuditKey,
			RecHubAudit.factDataAuditDetails.AuditType,
			RecHubAudit.factDataAuditDetails.AuditApplicationKey,
			RecHubAudit.factDataAuditDetails.AuditColumnKey,
			RecHubAudit.factDataAuditDetails.AuditValue
		FROM 
			RecHubAudit.factDataAuditDetails
		WHERE
			AuditDateKey BETWEEN @startDateKey and @endDateKey
			AND IsDeleted = 0
		OPTION (RECOMPILE);
		
		IF( @@ROWCOUNT > 0 )
		BEGIN /* factDataAuditDetails records found */
			/* Get factDataAuditDetails new values */ 
			INSERT INTO #Report_NewfactDataAuditDetails
			(
				AuditKey,
				AuditApplicationKey,
				AuditColumnKey,
				AuditValue,
				ColumnName
			)
			SELECT DISTINCT
				#Report_factDataAuditDetails.AuditKey,
				#Report_factDataAuditDetails.AuditApplicationKey,
				#Report_factDataAuditDetails.AuditColumnKey,
				#Report_factDataAuditDetails.AuditValue,
				RecHubAudit.dimAuditColumns.ColumnName
			FROM 
				#Report_factDataAuditDetails
				INNER JOIN RecHubAudit.dimAuditColumns ON RecHubAudit.dimAuditColumns.AuditColumnKey = #Report_factDataAuditDetails.AuditColumnKey
			WHERE 
				AuditType <> 3;

			/* Get factDataAuditDetails old values */ 
			INSERT INTO #Report_OldfactDataAuditDetails
			(
				AuditKey,
				AuditApplicationKey,
				AuditColumnKey,
				AuditValue
			)
			SELECT DISTINCT 
				#Report_factDataAuditDetails.AuditKey,
				#Report_factDataAuditDetails.AuditApplicationKey,
				#Report_factDataAuditDetails.AuditColumnKey,
				#Report_factDataAuditDetails.AuditValue
			FROM 
				#Report_factDataAuditDetails
			WHERE 
				AuditType = 3;
		END /* factDataAuditDetails records found */

		/* Combine the new and old values together to be used by the final select */
		INSERT INTO #Report_factDataAuditDetails_Combined
		(
			AuditKey,
			ColumnName,
			NewValue,
			OldValue
		)
		SELECT
			#Report_NewfactDataAuditDetails.AuditKey,
			#Report_NewfactDataAuditDetails.ColumnName,
			#Report_NewfactDataAuditDetails.AuditValue AS NewValue,
			#Report_OldfactDataAuditDetails.AuditValue AS OldValue
		FROM 
			#Report_NewfactDataAuditDetails
			LEFT JOIN #Report_OldfactDataAuditDetails ON #Report_OldfactDataAuditDetails.AuditKey = #Report_NewfactDataAuditDetails.AuditKey
				AND #Report_OldfactDataAuditDetails.AuditApplicationKey = #Report_NewfactDataAuditDetails.AuditApplicationKey
				AND #Report_OldfactDataAuditDetails.AuditColumnKey = #Report_NewfactDataAuditDetails.AuditColumnKey
	END /* factDataAuditSummary records found */

	/* Create the result set for the report */
	SELECT 
		#Report_factDataAuditSummary.AuditKey,
		CONVERT(VARCHAR(20), #Report_factDataAuditSummary.ModificationDate, 22) AS ModificationDate,
		#Report_factDataAuditSummary.BreadCrumb,
		#Report_factDataAuditSummary.LogonName,
		#Report_factDataAuditSummary.ApplicationName,
		#Report_factDataAuditSummary.AuditType,
		#Report_factDataAuditSummaryColumns.TableName,
		#Report_factDataAuditSummaryColumns.ColumnName AS SummaryColumnName,
		#Report_factDataAuditSummary.AuditValue,
		CASE 
			WHEN #Report_factDataAuditSummaryColumns.TableName = 'ExtractDefinitions' THEN CAST(RecHubAudit.udf_AuditMessageBuilder(#Report_factDataAuditSummary.AuditDateKey,#Report_factDataAuditSummary.AuditKey) AS VARCHAR(200)) + ' .... Full Definition file stored in RecHubAudit.factDataAuditMessages table.'
			ELSE RecHubAudit.udf_AuditMessageBuilder(#Report_factDataAuditSummary.AuditDateKey,#Report_factDataAuditSummary.AuditKey)
		END AS AuditMessage,			 /*--WI 176375*/
		#Report_factDataAuditDetails_Combined.ColumnName AS DetailColumnName,
		#Report_factDataAuditDetails_Combined.NewValue,
		#Report_factDataAuditDetails_Combined.OldValue
	FROM
		#Report_factDataAuditSummary
		INNER JOIN #Report_factDataAuditSummaryColumns ON #Report_factDataAuditSummaryColumns.AuditKey = #Report_factDataAuditSummary.AuditKey
			AND #Report_factDataAuditSummaryColumns.AuditColumnKey = #Report_factDataAuditSummary.AuditColumnKey
		LEFT JOIN #Report_factDataAuditDetails_Combined ON #Report_factDataAuditDetails_Combined.AuditKey = #Report_factDataAuditSummary.AuditKey
	ORDER BY
		#Report_factDataAuditSummary.ModificationDate,
		#Report_factDataAuditSummary.BreadCrumb,
		#Report_factDataAuditSummary.LogonName,
		#Report_factDataAuditSummary.ApplicationName,
		#Report_factDataAuditSummaryColumns.TableName,
		#Report_factDataAuditSummaryColumns.ColumnName,
		#Report_factDataAuditDetails_Combined.ColumnName,
		#Report_factDataAuditSummary.AuditKey;

	/* Audit the report event */
	IF(@filterUserID IS NOT NULL)
		SELECT @userSelected = ' Login ID: ' + LogonName 
		FROM 
			RecHubUser.Users 
		WHERE 
			UserID = @filterUserID
	ELSE IF(@filterEntityID IS NOT NULL)
	BEGIN
		SELECT 
			@userSelected = ' Entity: ' + #Report_Entities.BreadCrumb 
		FROM 
			#Report_Entities 
		WHERE 
			#Report_Entities.EntityID = @filterEntityID
	END
	ELSE
		SELECT 
			@userSelected = '';

	DECLARE @auditMessage VARCHAR(1024) = 'Configuration Audit executed for parameters - Date Range: '
		+ CONVERT(VARCHAR(10),CAST(CAST(@startDateKey AS VARCHAR(10)) AS DATETIME), 101) + ' - ' + CONVERT(VARCHAR(10),CAST(CAST(@endDateKey AS VARCHAR(10)) AS DATETIME), 101)
		+ @userSelected;

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubAudit.usp_Report_ConfigurationAudit', @parmEventName='Report Executed', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

	IF OBJECT_ID('tempdb..#Report_Entities') IS NOT NULL
        DROP TABLE #Report_Entities;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummary') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummary;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryColumns') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryColumns;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryUsers') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryUsers;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails_Combined') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails_Combined;
	IF OBJECT_ID('tempdb..#Report_OldfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_OldfactDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_NewfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_NewfactDataAuditDetails;

END TRY
BEGIN CATCH
	IF OBJECT_ID('tempdb..#Report_Entities') IS NOT NULL
        DROP TABLE #Report_Entities;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummary') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummary;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryColumns') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryColumns;
	IF OBJECT_ID('tempdb..#Report_factDataAuditSummaryUsers') IS NOT NULL
		DROP TABLE #Report_factDataAuditSummaryUsers;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_factDataAuditDetails_Combined') IS NOT NULL
		DROP TABLE #Report_factDataAuditDetails_Combined;
	IF OBJECT_ID('tempdb..#Report_OldfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_OldfactDataAuditDetails;
	IF OBJECT_ID('tempdb..#Report_NewfactDataAuditDetails') IS NOT NULL
		DROP TABLE #Report_NewfactDataAuditDetails;

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH