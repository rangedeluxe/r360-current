--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_factDataAuditSummary_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_factDataAuditSummary_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_factDataAuditSummary_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factDataAuditSummary_Ins 
(
	@parmAuditDateKey			INT,
	@parmUserID					INT,
	@parmAuditApplicationKey	INT,
	@parmAuditColumnKey			INT,
	@parmAuditType				VARCHAR(10),
	@parmAuditValue				VARCHAR(256) = NULL,
	@parmAuditKey				UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/18/2013
*
* Purpose: Insert an audit event into the factDataAuditSummary.
*
*
* @parmAuditType values:
*	'DEL' = 1 (Data was deleted)
*	'INS' = 2 (Data was inserted)
*	'UPD' = 3 - Old value, 4 - New value (Data was updated)
*	'SYSINFO' = 253 (System Information)
*	'SYSWARN' = 254 (System Warning)
*	'SYSERROR' = 255 (System Error)
*
* Modification History
* 05/18/2013 WI 102669 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
DECLARE @AuditType TINYINT;
BEGIN TRY

	SELECT 
		@AuditType = CASE @parmAuditType
			WHEN 'DEL' THEN 1
			WHEN 'INS' THEN 2
			WHEN 'UPD' THEN 4
			WHEN 'SYSINFO' THEN 253
			WHEN 'SYSWARN' THEN 254
			WHEN 'SYSERROR' THEN 255
		END

	INSERT INTO RecHubAudit.factDataAuditSummary
	(
		IsDeleted,
		AuditDateKey,
		AuditKey,
		UserID,
		AuditApplicationKey,
		AuditColumnKey,
		AuditType,
		AuditValue,
		CreationDate,
		ModificationDate
	)
	VALUES
	(
		0, /* IsDelete set to 0 on insert */
		@parmAuditDateKey,
		@parmAuditKey,
		@parmUserID,
		@parmAuditApplicationKey,
		@parmAuditColumnKey,
		@AuditType,
		@parmAuditValue,
		GETDATE(),
		GETDATE()
	);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
