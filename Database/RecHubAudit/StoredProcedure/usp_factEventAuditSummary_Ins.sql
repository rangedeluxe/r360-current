--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_factEventAuditSummary_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_factEventAuditSummary_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_factEventAuditSummary_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factEventAuditSummary_Ins 
(
	@parmAuditDateKey			INT,
	@parmUserID					INT,
	@parmAuditApplicationKey	INT,
	@parmAuditEventKey			INT,
	@parmWorkstation			INT,
	@parmAuditKey				UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Insert an audit event into the factEventAuditSummary.
*
*
* Modification History
* 05/30/2013 WI 103628 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	INSERT INTO RecHubAudit.factEventAuditSummary
	(
		IsDeleted,
		AuditDateKey,
		AuditKey,
		UserID,
		WorkStationID,
		AuditApplicationKey,
		AuditEventKey,
		CreationDate,
		ModificationDate
	)
	VALUES
	(
		0, /* IsDelete set to 0 on insert */
		@parmAuditDateKey,
		@parmAuditKey,
		@parmUserID,
		@parmWorkstation,
		@parmAuditApplicationKey,
		@parmAuditEventKey,
		GETDATE(),
		GETDATE()
	);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
