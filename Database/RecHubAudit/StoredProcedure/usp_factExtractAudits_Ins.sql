--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_factExtractAudits_Ins
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubAudit.usp_factExtractAudits_Ins') IS NOT NULL
	DROP PROCEDURE RecHubAudit.usp_factExtractAudits_Ins
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factExtractAudits_Ins
(
	@parmExtractAuditID UNIQUEIDENTIFIER,
	@parmIsComplete BIT,
	@parmExtractCount1 INT,
	@parmExtractCount2 INT,
	@parmExtractAmount1 MONEY,
	@parmExtractAmount2 MONEY,
	@parmExtractRecordCount INT,
	@parmExtractFileName VARCHAR(255),
	@parmSiteBankID INT = -1,
	@parmSiteClientAccountID INT = -1
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103842 JMC	Initial Version
* 09/09/2013 WI 113595 MLH	Added ModificationDate to insert statement (getdate() default 
* 03/09/2016 WI 269429 JPB	Added @parmSiteBankID and @parmSiteClientAccountID,
*							renamed to usp_factExtractAudits_Ins
* 06/10/2016 WI 285792 CEJ  Extract Audit - Payment\Stub Amounts Are Rounded To Full 
*							Dollar
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @AuditKey INT,
		@BankKey INT = -1,
		@ClientAccountKey INT = -1;

BEGIN TRY

	SELECT @AuditKey = CAST(CONVERT(VARCHAR, GETDATE(), 112) AS INT);

	SELECT 
		@BankKey = BankKey
	FROM
		RecHubData.dimBanks
	WHERE
		MostRecent = 1
		AND SiteBankID = CASE WHEN @parmSiteBankID = -1 THEN -1 ELSE @parmSiteBankID END;

	SELECT 
		@ClientAccountKey = ClientAccountKey
	FROM
		RecHubData.dimClientAccounts
	WHERE 
		MostRecent = 1
		AND SiteBankID = CASE WHEN @parmSiteBankID = -1 THEN -1 ELSE @parmSiteBankID END
		AND SiteClientAccountID = CASE WHEN @parmSiteBankID = -1 THEN -1 ELSE @parmSiteClientAccountID END;

	INSERT INTO RecHubAudit.factExtractAudits
		(
			IsDeleted,
			AuditDateKey,
			AuditKey,
			BankKey,
			ClientAccountKey,			
			ExtractCreated,
			IsComplete,
			ExtractCount1,
			ExtractCount2,
			ExtractAmount1,
			ExtractAmount2,
			ExtractRecordCount,
			ExtractFileName,
			CreationDate,
			ModificationDate
		)
	SELECT
		0,
		CAST(CONVERT(VARCHAR,GETDATE(),112) AS INT),
		@parmExtractAuditID,
		@BankKey,
		@ClientAccountKey,
		GETDATE(),
		@parmIsComplete,
		@parmExtractCount1,
		@parmExtractCount2,
		@parmExtractAmount1,
		@parmExtractAmount2,
		@parmExtractRecordCount,
		@parmExtractFileName,
		GETDATE(),
		GETDATE();

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
