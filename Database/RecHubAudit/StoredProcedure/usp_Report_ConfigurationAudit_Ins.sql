--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_Report_ConfigurationAudit_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_Report_ConfigurationAudit_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_Report_ConfigurationAudit_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_Report_ConfigurationAudit_Ins
(
	@parmApplicationName		VARCHAR(256),
	@parmAuditType				VARCHAR(10),
	@parmUserID					INT = 0,
	@parmAuditValue				VARCHAR(256) = NULL,
	@parmAuditMessage			VARCHAR(MAX) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MAA
* Date: 04/05/2017
*
* Purpose: Simpler wrapper for usp_WFS_DataAudit_Ins to insert Audit Config report
*			
*
* Modification History
* 04/05/2017 WI 136954911  MAA	Created
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY
	
	DECLARE	@schemaName		VARCHAR(128) = NULL;
	DECLARE	@tableName		VARCHAR(128) = NULL;
	DECLARE	@columnName		VARCHAR(128) = NULL;

	--Add more applications to table/schema/column here as needed
	IF(@parmApplicationName = 'WorkgroupMaintenance')
	BEGIN
		SET @schemaName		= 'RecHubData';
		SET @tableName		= 'dimWorkgroupBusinessRules';
		SET @columnName		= 'InvoiceRequired';
	END


	EXEC RecHubCommon.usp_WFS_DataAudit_Ins 
			@parmUserID				= @parmUserID,
			@parmApplicationName	= @parmApplicationName,
			@parmSchemaName			= @schemaName,
			@parmTableName			= @tableName,
			@parmColumnName			= @columnName,
			@parmAuditValue			= @parmAuditValue,
			@parmAuditType			= @parmAuditType,
			@parmAuditMessage		= @parmAuditMessage;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH