--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Report_UserActivityAudit
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_Report_UserActivityAudit') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_Report_UserActivityAudit
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_Report_UserActivityAudit
(
	@parmUserID		INT,
	@parmReportParameters	XML
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Kyle Colden
* Date:     07/11/2013
*
* Purpose:  Gets the data needed for the Audit Details Report
*
* Modification History
* Created
* 07/08/2013 WI 108650 KLC	Created.
* 09/06/2013 WI 108650 KLC	Updated to sort correctly based on datetime value not the formatted varchar
* 08/27/2014 WI 161790 LA	Modified to Filter on EventType Parameter
* 10/01/2014 WI 167230 LA	Added RAAM Entity Data temp table 
* 02/05/2015 WI 188470 MA   Added FullName field to return query
* 02/18/2015 WI 190927 MA   Added query for current user info to display on the report
* 04/21/2015 WI 202952 JPB	Removed FirstName, LastName. No longer in Users table.
* 04/27/2015 WI 203866 JBS	Remove SessionLog table data from this report.
* 04/29/2015 WI 204288 JBS	Add AuditDateKey in where clause to utilize Clustered index better
* 05/15/2015 WI 213461 MGE  Removed Cross apply and EventAuditMessages and used the new EventAuditMessageBuilder function
*****************************************************************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @startDateKey	INT,
			@endDateKey		INT,
			@filterUserID	INT,
			@eventType		VARCHAR(64),
			@filterEntityID	INT,
			@userLogin      varchar(24);
			
	SELECT	@startDateKey = CASE WHEN Parms.val.value('(SD)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(SD)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@endDateKey = CASE WHEN Parms.val.value('(ED)[1]', 'VARCHAR(10)') <> '' THEN CAST(CONVERT(VARCHAR, Parms.val.value('(ED)[1]', 'DATETIME'), 112) AS INT) ELSE NULL END,
			@eventType = Parms.val.value('(EVT)[1]', 'VARCHAR(64)'),
			@filterUserID = Parms.val.value('(USER)[1]', 'INT'),
			@filterEntityID = Parms.val.value('(ENT)[1]', 'INT')
	FROM @parmReportParameters.nodes('/Parameters') AS Parms(val);

	-- Add the EntityBreadcrumb data to a temp table
	-- So we can Join to it for RAAM data
	IF OBJECT_ID('tempdb..#tmpEntities') IS NOT NULL
        DROP TABLE #tmpEntities;

     CREATE TABLE #tmpEntities
     (
           EntityID              INT,
           BreadCrumb	         VARCHAR(100)
     );
     
     INSERT INTO #tmpEntities
     (
           EntityID,
           BreadCrumb       
     )

     SELECT     
           entityRequest.att.value('@EntityID', 'int') AS EntityID,
           entityRequest.att.value('@BreadCrumb', 'varchar(100)') AS BreadCrumb 
     FROM 
          @parmReportParameters.nodes('/Parameters/RAAM/Entity') entityRequest(att);

	SELECT	CONVERT(VARCHAR(20), RecHubAudit.factEventAuditSummary.ModificationDate, 22) AS ModificationDate,
			#tmpEntities.BreadCrumb,
			RecHubUser.Users.LogonName,
			RecHubAudit.dimAuditEvents.EventName,
			RecHubAudit.dimAuditApplications.ApplicationName,
			RecHubAudit.udf_EventAuditMessageBuilder(RecHubAudit.factEventAuditSummary.AuditDateKey, RecHubAudit.factEventAuditSummary.AuditKey) AS AuditMessage, 
			RecHubAudit.factEventAuditSummary.ModificationDate AS SortDate
	FROM RecHubAudit.factEventAuditSummary
	INNER JOIN RecHubAudit.dimAuditApplications
		ON RecHubAudit.factEventAuditSummary.AuditApplicationKey = RecHubAudit.dimAuditApplications.AuditApplicationKey
	INNER JOIN RecHubAudit.dimAuditEvents
		ON RecHubAudit.factEventAuditSummary.AuditEventKey = RecHubAudit.dimAuditEvents.AuditEventKey
	INNER JOIN RecHubUser.Users
		ON RecHubAudit.factEventAuditSummary.UserID = RecHubUser.Users.UserID
	INNER JOIN #tmpEntities
		ON #tmpEntities.EntityID = RecHubUser.Users.LogonEntityID
	WHERE RecHubAudit.factEventAuditSummary.AuditDateKey BETWEEN @startDateKey and @endDateKey
		AND RecHubAudit.factEventAuditSummary.IsDeleted = 0
		AND RecHubAudit.factEventAuditSummary.UserID = CASE WHEN @filterUserID  IS NULL THEN RecHubAudit.factEventAuditSummary.UserID ELSE @filterUserID END
		AND RecHubAudit.dimAuditEvents.EventType = CASE WHEN @eventType <> '' AND @eventType <> '-- ALL --' THEN @eventType else EventType end
	ORDER BY SortDate ASC;

	IF @filterUserID IS NOT NULL 
	BEGIN
		SELECT @userLogin = [LogonName] FROM RecHubUser.Users WHERE UserID = @filterUserID
	END

	DECLARE @auditMessage VARCHAR(1024) = 'User Activity Report executed for parameters -'
											+ ' Date Range: ' + CAST(@startDateKey AS VARCHAR(8)) + ' - ' + CAST(@endDateKey AS VARCHAR(8))
											+ ' User Login: ' + CASE WHEN @userLogin IS NULL THEN 'All Users' ELSE @userLogin END
											+ ' Event Type: ' + @eventType

	EXEC RecHubCommon.usp_WFS_EventAudit_Ins @parmApplicationName='RecHubAudit.usp_Report_UserActivityAudit', @parmEventName='Report Executed', @parmEventType='Reports', @parmUserID = @parmUserID, @parmAuditMessage = @auditMessage

END TRY
BEGIN CATCH
      EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
