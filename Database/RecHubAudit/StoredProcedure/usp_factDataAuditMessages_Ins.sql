--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_factDataAuditMessages_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_factDataAuditMessages_Ins') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_factDataAuditMessages_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_factDataAuditMessages_Ins 
(
	@parmAuditDateKey			INT,
	@parmUserID					INT,
	@parmAuditApplicationKey	INT,
	@parmAuditColumnKey			INT,
	@parmAuditKey				UNIQUEIDENTIFIER,
	@parmAuditMessage			VARCHAR(MAX)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/19/2013
*
* Purpose: Insert an audit event into RecHubAudit.factDataAuditMessages.
*
*
* Modification History
* 05/19/2013 WI 103618 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	/* Parse @parmAuditMessage into 128 byte lengths */
	;WITH AuditMessage_CTE(StartingPosition, EndingPosition, occurence)
	AS
	(		
		SELECT		
			StartingPosition = 1,
			EndingPosition = 128,
			1 AS occurence
		UNION ALL
		SELECT 		
			StartingPosition = EndingPosition + 1,
			EndingPosition = 
				CASE 
					WHEN EndingPosition <= LEN(@parmAuditMessage) + EndingPosition + 128 
						THEN EndingPosition + 128 
						ELSE EndingPosition + CAST(LEN(@parmAuditMessage) AS INT) 
					END,
			occurence + 1
		FROM 
			AuditMessage_CTE
		WHERE 
			EndingPosition + 1 <= LEN(@parmAuditMessage)
	)
	INSERT INTO RecHubAudit.factDataAuditMessages
	(
		IsDeleted,
		AuditDateKey,
		AuditKey,
		UserID,
		AuditApplicationKey,
		AuditMessagePart,
		AuditColumnKey,
		CreationDate,
		ModificationDate,
		AuditMessage
	)
	SELECT	
		0, /* IsDelete set to 0 on insert */
		@parmAuditDateKey,
		@parmAuditKey,
		@parmUserID,
		@parmAuditApplicationKey,
		occurence, 
		@parmAuditColumnKey,
		GETDATE(),
		GETDATE(),
		SUBSTRING(@parmAuditMessage, StartingPosition, EndingPosition-StartingPosition+1) AS AuditDetail
	FROM	
		AuditMessage_CTE
	OPTION (MaxRecursion 0);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
