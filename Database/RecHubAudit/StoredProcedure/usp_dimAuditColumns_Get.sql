--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorStoredProcedureName usp_dimAuditColumns_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubAudit.usp_dimAuditColumns_Get') IS NOT NULL
       DROP PROCEDURE RecHubAudit.usp_dimAuditColumns_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubAudit.usp_dimAuditColumns_Get 
(
	@parmSchemaName				VARCHAR(128),
	@parmTableName				VARCHAR(128),
	@parmColumnName				VARCHAR(128),
	@parmAuditColumnKey			INT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/18/2013
*
* Purpose: Get the key for the column beging audited. If the column is not 
*	in the table, add it.
*
*
* Modification History
* 05/18/2013 WI 102670 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON
DECLARE @LockResult INT,
		@RetryCount SMALLINT;
 
BEGIN TRY
	SELECT	@parmAuditColumnKey = -1,
			@RetryCount = 4;;

	BEGIN TRANSACTION;
	/* To keep from duplicate records from being created, lock the process */
	WHILE( @RetryCount >= 0 )
	BEGIN
		EXEC @LockResult = sp_getapplock 
			@Resource='dimAuditColumns_Get',
			@LockOwner='Transaction',
			@LockMode='Exclusive';


		IF( @LockResult < 0 )
		BEGIN 
			IF( @RetryCount > 0 )
			BEGIN
				SET @RetryCount = @RetryCount - 1;
				WAITFOR DELAY '00:00:05'; /* wait 5 seconds and try again */		
			END
			ELSE
			BEGIN
				SET @RetryCount = -1;
				RAISERROR('Could not obtain lock on dimAuditColumns table.',16,1) WITH NOWAIT;	
			END
		END
		ELSE
			SET @RetryCount = -1;

	END

	SELECT 
		@parmAuditColumnKey = AuditColumnKey
	FROM 
		RecHubAudit.dimAuditColumns
	WHERE 
		UPPER(SchemaName) = UPPER(@parmSchemaName)
		AND UPPER(TableName) = UPPER(@parmTableName)
		AND UPPER(ColumnName) = UPPER(@parmColumnName);

	IF( @parmAuditColumnKey = -1 )
	BEGIN /* application not found */
		BEGIN TRY
			DECLARE @InsertRecord TABLE
			(
				AuditColumnKey INT
			);

			INSERT INTO RecHubAudit.dimAuditColumns(SchemaName,TableName,ColumnName)
			OUTPUT inserted.AuditColumnKey INTO @InsertRecord
			VALUES (@parmSchemaName,@parmTableName,@parmColumnName);
			
			SELECT 
				@parmAuditColumnKey = AuditColumnKey
			FROM 
				@InsertRecord;

		END TRY
		BEGIN CATCH
			IF( ERROR_NUMBER() = 2601 ) /* Duplicate record, should NEVER get here */
				SELECT 
					@parmAuditColumnKey = AuditColumnKey
				FROM 
					RecHubAudit.dimAuditColumns
				WHERE 
					UPPER(SchemaName) = UPPER(@parmSchemaName)
					AND UPPER(TableName) = UPPER(@parmTableName)
					AND UPPER(ColumnName) = UPPER(@parmColumnName);
		END CATCH
	END

	EXEC @LockResult = sp_releaseapplock
		@Resource='dimAuditColumns_Get';

	COMMIT TRANSACTION;
END TRY
BEGIN CATCH
	IF( APPLOCK_MODE('public','dimAuditColumns_Get','Transaction') <> 'NoLock' )
		EXEC @LockResult = sp_releaseapplock
			@Resource='dimAuditColumns_Get';

	IF XACT_STATE() != 0 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
