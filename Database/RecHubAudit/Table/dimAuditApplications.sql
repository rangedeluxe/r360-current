--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName dimAuditApplications
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2013
*
* Purpose: Hold applications that are being audited. 
*
* Modification History
* 04/18/2013 WI 102663 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.dimAuditApplications
(
	AuditApplicationKey INT IDENTITY NOT NULL 
		CONSTRAINT PK_dimAuditApplications PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditApplications_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditApplications_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditApplications_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditApplications_ModifiedBy DEFAULT(SUSER_SNAME()),
	ApplicationName VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubAudit.dimAuditApplications.IDX_dimAuditApplications_ApplicationName
CREATE UNIQUE INDEX IDX_dimAuditApplications_ApplicationName ON RecHubAudit.dimAuditApplications (ApplicationName);
