--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factEventAuditSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Grain: one row for every audit event.
*		   
*
* Modification History
* 05/30/2013 WI 103660 JPB	Created
* 05/15/2015 WI 214615 MGE  Added index IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey
*****************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factEventAuditSummary
(
	factEventAuditSummaryKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	UserID INT NOT NULL,
	WorkStationID INT NOT NULL,
	AuditApplicationKey INT NOT NULL,
	AuditEventKey INT NOT NULL,
	AuditKey UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factEventAuditSummary ADD 
	CONSTRAINT PK_factEventAuditSummaryKey PRIMARY KEY NONCLUSTERED (factEventAuditSummaryKey,AuditDateKey),
	CONSTRAINT FK_factEventAuditSummary_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factEventAuditSummary_dimAuditApplications FOREIGN KEY(AuditApplicationKey) REFERENCES RecHubAudit.dimAuditApplications(AuditApplicationKey),
	CONSTRAINT FK_factEventAuditSummary_dimAuditEvents FOREIGN KEY(AuditEventKey) REFERENCES RecHubAudit.dimAuditEvents(AuditEventKey),
	CONSTRAINT FK_factEventAuditSummary_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);

--WFSScriptProcessorIndex RecHubAudit.factEventAuditSummary.IDX_factDataAuditSummary_AuditDatefactDataAuditSummaryKey
CREATE CLUSTERED INDEX IDX_factEventAuditSummary_AuditDatefactEventAuditSummaryKey ON RecHubAudit.factEventAuditSummary 
(
	AuditDateKey,
	factEventAuditSummaryKey
) $(OnAuditPartition);

CREATE NONCLUSTERED INDEX IDX_factEventAuditSummary_IsDeletedAuditEventKeyAuditDateKey ON RecHubAudit.factEventAuditSummary
(
	IsDeleted,
	AuditEventKey,
	AuditDateKey
)
INCLUDE
(
	UserID,
	AuditApplicationKey,
	AuditKey,
	ModificationDate
)