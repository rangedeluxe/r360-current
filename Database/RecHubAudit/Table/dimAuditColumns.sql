--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName dimAuditColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2013
*
* Purpose: Store information about columns that are being audited. 
*
* Modification History
* 04/18/2013 WI 102664 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.dimAuditColumns
(
	AuditColumnKey INT IDENTITY(0,1) NOT NULL 
		CONSTRAINT PK_dimAuditColumns PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditColumns_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditColumns_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditColumns_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditColumns_ModifiedBy DEFAULT(SUSER_SNAME()),
	SchemaName VARCHAR(128) NOT NULL,
	TableName VARCHAR(128) NOT NULL,
	ColumnName VARCHAR(128) NOT NULL,
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubAudit.dimAuditColumns.IDX_dimAuditColumns_SchemaTableColumnName
CREATE UNIQUE INDEX IDX_dimAuditColumns_SchemaTableColumnName ON RecHubAudit.dimAuditColumns 
(
	SchemaName,
	TableName,
	ColumnName
);
