--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factExtractAudits
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103839 JMC	Initial Version
* 03/09/2016 WI 269428 JPB	Added BankKey and ClientAccountKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factExtractAudits
(
	factExtractAuditKey BIGINT IDENTITY(1, 1),
	IsDeleted			BIT NOT NULL,
	AuditDateKey		INT NOT NULL,
	AuditKey 			UNIQUEIDENTIFIER NOT NULL,
	BankKey				INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	ExtractCreated 		DATETIME NOT NULL,
	IsComplete 			BIT NOT NULL,
	ExtractCount1 		INT NOT NULL,
	ExtractCount2 		INT NOT NULL,
	ExtractAmount1 		MONEY NOT NULL,
	ExtractAmount2 		MONEY NOT NULL,
	ExtractRecordCount 	INT NOT NULL,
	ExtractFileName 	VARCHAR(256) NOT NULL,
	CreationDate 		DATETIME NOT NULL,
	ModificationDate	DATETIME NOT NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factExtractAudits ADD
	CONSTRAINT PK_factExtractAudits PRIMARY KEY NONCLUSTERED (factExtractAuditKey,AuditDateKey),
	CONSTRAINT FK_factExtractAudits_dimBanks FOREIGN KEY(BankKey) REFERENCES RecHubData.dimBanks(BankKey),
	CONSTRAINT FK_factExtractAudits_dimClientAccounts FOREIGN KEY(ClientAccountKey) REFERENCES RecHubData.dimClientAccounts(ClientAccountKey);

--WFSScriptProcessorIndex RecHubAudit.factExtractAudits.IDX_factExtractAuditsAuditDatefactExtractAuditKey
CREATE CLUSTERED INDEX IDX_factExtractAuditsAuditDatefactExtractAuditKey ON RecHubAudit.factExtractAudits 
(	
	AuditDateKey,
	factExtractAuditKey
) $(OnAuditPartition);