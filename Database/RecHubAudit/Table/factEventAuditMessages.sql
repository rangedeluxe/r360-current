--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factEventAuditMessages
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Grain: One or more rows for each audit message.
*		   
*
* Modification History
* 05/30/2013 WI 103661 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factEventAuditMessages
(
	factEventAuditMessageKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	UserID INT NOT NULL,
	AuditApplicationKey INT NOT NULL,
	AuditEventKey INT NOT NULL,
	AuditMessagePart INT NOT NULL,
	AuditKey UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	AuditMessage VARCHAR(128) NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factEventAuditMessages ADD 
	CONSTRAINT PK_factEventAuditMessages PRIMARY KEY NONCLUSTERED (factEventAuditMessageKey,AuditDateKey),
	CONSTRAINT FK_factEventAuditMessages_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factEventAuditMessages_dimAuditApplications FOREIGN KEY(AuditApplicationKey) REFERENCES RecHubAudit.dimAuditApplications(AuditApplicationKey),
	CONSTRAINT FK_factEventAuditMessages_dimAuditEvents FOREIGN KEY(AuditEventKey) REFERENCES RecHubAudit.dimAuditEvents(AuditEventKey),
	CONSTRAINT FK_factEventAuditMessages_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);

--WFSScriptProcessorIndex RecHubAudit.factEventAuditMessages.IDX_factDataAuditMessages_AuditDatefactEventAuditMessageKey
CREATE CLUSTERED INDEX IDX_factDataAuditMessages_AuditDatefactEventAuditMessageKey ON RecHubAudit.factEventAuditMessages
(
	AuditDateKey,
	factEventAuditMessageKey
) $(OnAuditPartition);
