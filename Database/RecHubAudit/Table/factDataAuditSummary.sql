--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factDataAuditSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2013
*
* Purpose: Grain: one row for every audit event.
*		   
*
* Modification History
* 04/18/2013 WI 102666 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factDataAuditSummary
(
	factDataAuditSummaryKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	UserID INT NOT NULL,
	AuditApplicationKey INT NOT NULL,
	AuditColumnKey INT NOT NULL,
	AuditType TINYINT NOT NULL,
	AuditKey UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	AuditValue VARCHAR(256) NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factDataAuditSummary ADD 
	CONSTRAINT PK_factDataAuditSummary PRIMARY KEY NONCLUSTERED (factDataAuditSummaryKey,AuditDateKey),
	CONSTRAINT FK_factDataAuditSummary_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataAuditSummary_dimAuditApplications FOREIGN KEY(AuditApplicationKey) REFERENCES RecHubAudit.dimAuditApplications(AuditApplicationKey),
	CONSTRAINT FK_factDataAuditSummary_dimAuditColumns FOREIGN KEY(AuditColumnKey) REFERENCES RecHubAudit.dimAuditColumns(AuditColumnKey),
	CONSTRAINT FK_factDataAuditSummary_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);

--WFSScriptProcessorIndex RecHubAudit.factAuditSummary.IDX_factDataAuditSummary_AuditDatefactDataAuditSummaryKey
CREATE CLUSTERED INDEX IDX_factDataAuditSummary_AuditDatefactDataAuditSummaryKey ON RecHubAudit.factDataAuditSummary 
(
	AuditDateKey,
	factDataAuditSummaryKey
) $(OnAuditPartition);
