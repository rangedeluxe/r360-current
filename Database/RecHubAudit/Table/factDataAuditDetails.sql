--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factDataAuditDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2013
*
* Purpose: Grain: one row for every audit event.
*		   
*
* Modification History
* 04/18/2013 WI 102665 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factDataAuditDetails
(
	factDataAuditDetailKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	UserID INT NOT NULL,
	AuditApplicationKey INT NOT NULL,
	AuditColumnKey INT NOT NULL,
	AuditType TINYINT NOT NULL,
	AuditKey UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	AuditValue VARCHAR(256) NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factDataAuditDetails ADD 
	CONSTRAINT PK_factDataAuditDetails PRIMARY KEY NONCLUSTERED (factDataAuditDetailKey,AuditDateKey),
	CONSTRAINT FK_factDataAuditDetails_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataAuditDetails_dimAuditApplications FOREIGN KEY(AuditApplicationKey) REFERENCES RecHubAudit.dimAuditApplications(AuditApplicationKey),
	CONSTRAINT FK_factDataAuditDetails_dimAuditColumns FOREIGN KEY(AuditColumnKey) REFERENCES RecHubAudit.dimAuditColumns(AuditColumnKey),
	CONSTRAINT FK_factDataAuditDetails_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);

--WFSScriptProcessorIndex RecHubAudit.factDataAuditDetails.IDX_factDataAuditSummary_AuditDatefactDataAuditDetailKey
CREATE CLUSTERED INDEX IDX_factDataAuditDetails_AuditDatefactDataAuditDetailKey ON RecHubAudit.factDataAuditDetails 
(
	AuditDateKey,
	factDataAuditDetailKey
) $(OnAuditPartition);
