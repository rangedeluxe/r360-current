--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName dimAuditEvents
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/30/2013
*
* Purpose: Events that are being audited. 
*
* Modification History
* 05/30/2013 WI 103696 JPB	Created
* 08/25/2014 WI 161314 JBS	Add Column EventType
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.dimAuditEvents
(
	AuditEventKey INT IDENTITY NOT NULL 
		CONSTRAINT PK_dimAuditEvents PRIMARY KEY CLUSTERED,
	CreationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditEvents_CreationDate DEFAULT(GETDATE()),
	CreatedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditEvents_CreatedBy DEFAULT(SUSER_SNAME()),
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimAuditEvents_ModificationDate DEFAULT(GETDATE()),
	ModifiedBy VARCHAR(128) NOT NULL 
		CONSTRAINT DF_dimAuditEvents_ModifiedBy DEFAULT(SUSER_SNAME()),
	EventName VARCHAR(64) NOT NULL,
	EventType VARCHAR(64) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubAudit.dimAuditEvents.IDX_dimAuditEvents_EventName
CREATE UNIQUE INDEX IDX_dimAuditEvents_EventName ON RecHubAudit.dimAuditEvents (EventName);
