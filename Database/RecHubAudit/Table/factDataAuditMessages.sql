--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factDataAuditMessages
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/18/2013
*
* Purpose: Grain: One or more rows for each AuditDetail message.
*		   
*
* Modification History
* 04/18/2013 WI 103647 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factDataAuditMessages
(
	factDataAuditMessageKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	UserID INT NOT NULL,
	AuditApplicationKey INT NOT NULL,
	AuditColumnKey INT NOT NULL,
	AuditMessagePart INT NOT NULL,
	AuditKey UNIQUEIDENTIFIER NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	AuditMessage VARCHAR(128) NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factDataAuditMessages ADD 
	CONSTRAINT PK_factDataAuditMessages PRIMARY KEY NONCLUSTERED (factDataAuditMessageKey,AuditDateKey),
	CONSTRAINT FK_factDataAuditMessages_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey),
	CONSTRAINT FK_factDataAuditMessages_dimAuditApplications FOREIGN KEY(AuditApplicationKey) REFERENCES RecHubAudit.dimAuditApplications(AuditApplicationKey),
	CONSTRAINT FK_factDataAuditMessages_dimAuditColumns FOREIGN KEY(AuditColumnKey) REFERENCES RecHubAudit.dimAuditColumns(AuditColumnKey),
	CONSTRAINT FK_factDataAuditMessages_Users FOREIGN KEY(UserID) REFERENCES RecHubUser.Users(UserID);

--WFSScriptProcessorIndex RecHubAudit.factDataAuditMessages.IDX_factDataAuditMessages_AuditDatefactDataAuditMessageKey
CREATE CLUSTERED INDEX IDX_factDataAuditMessages_AuditDatefactDataAuditMessageKey ON RecHubAudit.factDataAuditMessages
(
	AuditDateKey,
	factDataAuditMessageKey
) $(OnAuditPartition);
