--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubAudit
--WFSScriptProcessorTableName factDITClientAudit
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/08/2016
*
* Purpose: Store DIT audit records. 
*
* Modification History
* 01/08/2016 WI 255644 JPB	Created
* 01/22/2016 WI 259472 JPB	Renamed DITTrackingID to SourceTrackingID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHubAudit.factDITClientAudit
(
	factDITClientAuditKey BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDataSuccessful BIT NULL,
	IsImageSuccessful BIT NULL,
	SiteBankID INT NULL,
	SiteClientAccountID INT NULL,
	ImmutableDateKey INT NULL,
	SourceBatchID BIGINT NULL,
	PaymentCount INT NULL,
	PaymentImageCount INT NULL,
	StubCount INT NULL,
	DocumentCount INT NULL,
	DocumentImageCount INT NULL,
	CreationDate DATETIME2(7) NOT NULL
		CONSTRAINT DF_factDITClientAudit_CreationDate DEFAULT SYSDATETIME(),
	ModificationDate DATETIME2(7) NOT NULL
		CONSTRAINT DF_factDITClientAudit_ModificationDate DEFAULT SYSDATETIME(),
	ProcessingStart DATETIME2(7) NOT NULL,
	ProcessingEnd DATETIME2(7) NULL,
	ValidationStart DATETIME2(7) NULL,
	ValidationEnd DATETIME2(7) NULL,
	QueueSubmission DATETIME2(7) NULL,
	ResponseStart DATETIME2(7) NULL,
	ResponseEnd DATETIME2(7) NULL,
	ImageStart DATETIME2(7) NULL,
	ImageEnd DATETIME2(7) NULL
) $(OnAuditPartition);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubAudit.factDITClientAudit ADD 
	CONSTRAINT PK_factDITClientAudit PRIMARY KEY NONCLUSTERED (factDITClientAuditKey,AuditDateKey),
	CONSTRAINT FK_factDITClientAudit_dimDates FOREIGN KEY(AuditDateKey) REFERENCES RecHubData.dimDates(DateKey);

--WFSScriptProcessorIndex RecHubAudit.factDITClientAudit.IDX_factDITClientAudit_factDITClientAuditKey
CREATE CLUSTERED INDEX IDX_factDITClientAudit_factDITClientAuditKey ON RecHubAudit.factDITClientAudit
(
	AuditDateKey,
	factDITClientAuditKey
) $(OnAuditPartition);

--WFSScriptProcessorIndex RecHubAudit.factDITClientAudit.IDX_factDITClientAudit_AuditDateKeySourceTrackingID
CREATE INDEX IDX_factDITClientAudit_AuditDateKeySourceTrackingID ON RecHubAudit.factDITClientAudit
(
	AuditDateKey,
	SourceTrackingID
) $(OnAuditPartition);
