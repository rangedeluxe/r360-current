
IF OBJECT_ID('CommonStaging.usp_factStubsLoad') IS NOT NULL
       DROP PROCEDURE CommonStaging.usp_factStubsLoad
GO

CREATE PROCEDURE CommonStaging.usp_factStubsLoad
	(
	@parmSchemaName NVARCHAR(128)
	)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/05/2019
*
* Purpose: Merge load RecHubData.factStubs from [schemaName].Stubs
*
* Modification History
* 11/05/2019 R360-30933	MGE	Created
*******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @SQLStatement			NVARCHAR(MAX) = N'';	--Variable to hold t-sql query
	DECLARE @InsertColumns			NVARChar(MAX) = N'';
	DECLARE @SelectColumns			NVARChar(MAX) = N'';

	SELECT  @InsertColumns = @InsertColumns + @parmSchemaName + '.factStubs.' + COLUMN_NAME + ', '
	FROM INFORMATION_SCHEMA.COLUMNS 
	WHERE TABLE_NAME = 'factStubs' AND TABLE_SCHEMA = @parmSchemaName
	AND COLUMN_NAME <> 'RecordID'
	AND COLUMN_NAME <> 'BatchTrackingID'

	IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'factStubs' AND TABLE_SCHEMA = @parmSchemaName AND COLUMN_NAME = 'OrganizationKey')
		BEGIN
			SET @SelectColumns = @InsertColumns + '-1 AS OrganizationKey'
			SET @InsertColumns = @InsertColumns + 'OrganizationKey'
		END
	ELSE
		BEGIN
		IF RIGHT(RTRIM(@InsertColumns),1) = ','
			SET @InsertColumns = LEFT(@InsertColumns,(LEN(@InsertColumns) - 1))
			SET @SelectColumns = @InsertColumns
		END

	SELECT @SQLStatement = N'
	INSERT INTO RecHubData.factStubs (' + @InsertColumns + ')' 
	+ ' SELECT ' + @SelectColumns +
	' FROM ' + @parmSchemaName + '.factStubs WHERE IsDeleted = 0' 

	EXEC(@SQLStatement)


END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH