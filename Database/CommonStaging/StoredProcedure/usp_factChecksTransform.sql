IF OBJECT_ID('CommonStaging.usp_factChecksTransform') IS NOT NULL
       DROP PROCEDURE CommonStaging.usp_factChecksTransform
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE CommonStaging.usp_factChecksTransform
	(
	@parmSchemaName NVARCHAR(128)
	)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/05/2019
*
* Purpose: Merge ChecksDE with Checks
*
* Modification History
* 11/05/2019 R360-30933	MGE	Created
*******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @SQLStatement			NVARCHAR(MAX) = N'';	--Variable to hold t-sql query
	DECLARE @UpdateColumns			NVARChar(MAX) = N'SET ';

	SELECT  @UpdateColumns = @UpdateColumns +  COLUMN_NAME + '=' +@parmSchemaName + '.WorkDataEntryWideChecks.' + COLUMN_NAME + ', '
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkDataEntryWideChecks' AND TABLE_SCHEMA = @parmSchemaName
	AND COLUMN_NAME LIKE'%_1_%'
	SELECT @UpdateColumns = LEFT(@UpdateColumns, LEN(@UpdateColumns)-1)
	
	IF LEN(@UpdateColumns) > 2
	BEGIN
		SELECT @SQLStatement = N'
		UPDATE ' + @parmSchemaName + '.factChecks ' + @UpdateColumns + 
		' FROM ' + @parmSchemaName + '.WorkDataEntryWideChecks ' +
		'WHERE factChecks.BankKey = WorkDataEntryWideChecks.BankKey
		AND factChecks.ClientAccountKey = WorkDataEntryWideChecks.ClientAccountKey
		AND factChecks.DepositDateKey = WorkDataEntryWideChecks.DepositDateKey
		AND factChecks.ImmutableDateKey = WorkDataEntryWideChecks.ImmutableDateKey
		AND factChecks.BatchID = WorkDataEntryWideChecks.BatchID
		AND factChecks.TransactionID = WorkDataEntryWideChecks.TransactionID
		AND factChecks.BatchSequence = WorkDataEntryWideChecks.BatchSequence'

		EXEC(@SQLStatement)

	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
