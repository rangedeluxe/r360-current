
IF OBJECT_ID('CommonStaging.usp_factStubsTransform') IS NOT NULL
       DROP PROCEDURE CommonStaging.usp_factStubsTransform
GO

CREATE PROCEDURE CommonStaging.usp_factStubsTransform
	(
	@parmSchemaName NVARCHAR(128)
	)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/05/2019
*
* Purpose: Merge StubsDE with Stubs
*
* Modification History
* 11/05/2019 R360-30933	MGE	Created
*******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @SQLStatement			NVARCHAR(MAX) = N'';	--Variable to hold t-sql query
	DECLARE @UpdateColumns			NVARChar(MAX) = N'SET ';

	SELECT  @UpdateColumns = @UpdateColumns + COLUMN_NAME + '=' +@parmSchemaName + '.WorkDataEntryWideStubs.' + COLUMN_NAME + ', '
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkDataEntryWideStubs' AND TABLE_SCHEMA = @parmSchemaName
	AND COLUMN_NAME LIKE'%_0_%'
	SELECT @UpdateColumns = LEFT(@UpdateColumns, LEN(@UpdateColumns)-1)
	
	IF LEN(@UpdateColumns) > 2
	BEGIN
		SELECT @SQLStatement = N'
		UPDATE ' + @parmSchemaName + '.factStubs ' + @UpdateColumns + 
		' FROM ' + @parmSchemaName + '.WorkDataEntryWideStubs ' +
		'WHERE factStubs.BankKey = WorkDataEntryWideStubs.BankKey
		AND factStubs.ClientAccountKey = WorkDataEntryWideStubs.ClientAccountKey
		AND factStubs.DepositDateKey = WorkDataEntryWideStubs.DepositDateKey
		AND factStubs.ImmutableDateKey = WorkDataEntryWideStubs.ImmutableDateKey
		AND factStubs.BatchID = WorkDataEntryWideStubs.BatchID
		AND factStubs.TransactionID = WorkDataEntryWideStubs.TransactionID
		AND factStubs.BatchSequence = WorkDataEntryWideStubs.BatchSequence'

		EXEC(@SQLStatement)

	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH