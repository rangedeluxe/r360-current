﻿<#
	.SYNOPSIS
	PowerShell script used to exec the Extract Images 1.05.x to 2.01 converion package.
	
	.DESCRIPTION
	The script will create a XML document based on the input and call the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER SourceServer
	The name of the server hosting the OLTA 1.05.x database to be upgraded.

	.PARAMETER SourceDB
	The name of the OLTA 1.05.x database to be upgraded.

	.PARAMETER SSISDirectory
	Folder where the PowerShell scripts are located.

	.PARAMETER DepositDateKeyStart
	The start of the range to convert. NOTE: This is a DepositDateKey and must be in the format of YYYYMMDD.

	.PARAMETER DepositDateKeyEnd
	The end of the range to convert. NOTE: This is a DepositDateKey and must be in the format of YYYYMMDD.

	.PARAMETER UnloadFolderName
	Folder where data files will be written too. NOTE: This folder must have enough free space to write all data
	to be unloaded. Depending on the date range, this could be in the gigabytes of space needed.
#>

param
(
	[parameter(Mandatory = $true)][string] $SourceServer,
	[parameter(Mandatory = $true)][string] $SourceDB,
	[parameter(Mandatory = $true)][string] $SSISDirectory,
	[parameter(Mandatory = $true)][string] $DepositDateKeyStart,
	[parameter(Mandatory = $true)][string] $DepositDateKeyEnd,
	[parameter(Mandatory = $true)][string] $UnloadFolderName
)

$ScriptVerison = "2.01";

Write-Host "Execute OLTA 1.05.x to 2.01 Extract Images PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 03/23/15 JPB	2.01	WI 197251	Created.
#
################################################################################

#BuildXML document to be sent to execute PowerShellScript
$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISDirectory</PackageFolder>
	<PackageName>105x_201_ExtractImages</PackageName>
	<Parameters>
		<Parameter Name=""aSourceServerName"" Value=""$SourceServer""/>
		<Parameter Name=""aSourceInitialCatalog"" Value=""$SourceDB""/>
		<Parameter Name=""aDepositBeginDate"" Value=""$DepositDateKeyStart""/>
		<Parameter Name=""aDepositEndDate"" Value=""$DepositDateKeyEnd""/>
		<Parameter Name=""aUnloadFolderName"" Value=""$UnloadFolderName""/>
	</Parameters>
</SSISPackage>"
);

$SSISFolder = $xml.SSISPackage.PackageFolder;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

$CurrentFolder = Get-Location;
Set-Location $SSISFolder;
.\ExecuteSSISPackage $xml;
Set-Location $CurrentFolder;