﻿Param (
	#Version of SQL the install is going against
	#Valid values: 2012, 2016
	$SQLVersion="2012",
	#Name of the SQL Database server
	$DBSERVER = "SERVERNAME",
	#Name of the R360 Database-Default is WFSDB_R360
	$R360DB = "WFSDB_R360",
	#Name of the RAAM Database-Default is RAAM
	$RAAMDB = "RAAM",
	#Name of the Framework DB- default is Framework
	$FRAMEWORKDB = "Framework",
	#Name of the SSIS Configuration Database-Default is WFS_SSIS_Configuration
	$SSISConfigDB = "WFS_SSIS_Configuration",
	#Name of the SSIS Logging Database-Default is WFS_SSIS_Logging
	$SSISLoggingDB = "WFS_SSIS_Logging",
	#Installation Folder
	$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03.02",
	#Installation Folder
	$FRAMEWORKINSTALLFOLDER = "D:\WFSStaging\DB\Framework2.04",
	#Log Folder
	$LOGFOLDER = "D:\WFSStaging\DB\Installlogs",
	#Drive we're installing XML DLL to
	$INSTALLDRIVE = "D:",
	$Verbose = $true
)

IF( $SQLVersion -eq "2016" )
{
	$Installer = "$INSTALLFOLDER\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "$INSTALLFOLDER\InstallApp\WFSDatabaseInstallConsole"
}

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.02.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
if ($Verbose -eq $true) {
	Read-Host 'R360 Patch 2.03.02.00 has completed-Press any key to continue' | Out-Null
}

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.02.00SSISPatch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
if ($Verbose -eq $true) {
	Read-Host 'SSIS Patch 2.03.02.00 has completed-Press any key to continue' | Out-Null
}

&"$Installer"  -DBServer $DBSERVER -DBName $RAAMDB -CommandFile RAAMR360Online2.03.02.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER	
if ($Verbose -eq $true) {
Read-Host 'R360 RAAM Patch 2.03.02.00 has completed-Press any key to Exit' | Out-Null
}

&"$Installer"  -DBServer $DBSERVER -DBName $FRAMEWORKDB -CommandFile Framework2.04.00.00.xml -InstallationFolder $FRAMEWORKINSTALLFOLDER -LogFolder $LOGFOLDER	
if ($Verbose -eq $true) {
Read-Host 'Framework Patch 2.04.00.00 has completed-Press any key to Exit' | Out-Null
}

#Copy SSIS helper DLLs
$BinSourcePath = Join-Path $INSTALLFOLDER "bin"
$BinTargetPath = Join-Path $INSTALLDRIVE "WFSApps\RecHub\bin"
if (!(Test-Path $BinTargetPath)) {
    New-Item $BinTargetPath -ItemType Directory | Out-Null
}
Copy-Item (Join-Path $BinSourcePath "*") $BinTargetPath -Force
if ($Verbose -eq $true) {
	Read-Host 'SSIS Patch 2.03.02.00 has completed-Press any key to continue' | Out-Null
}
