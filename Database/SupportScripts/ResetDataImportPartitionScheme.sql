--<<< WARNING 1: This is only used for repairing the DataImport Partition Scheme >>>
--<<< WARNING 2: THERE IS AN ALTER DATABASE ADD FILE COMMAND AT ABOUT LINE 110 THAT NEEDS TO BE MODIFIED BEFORE RUNNING >>>
--<<< WARNING 2a: Change both the Database Name and the path >>>

DECLARE	@Indexes	TABLE	(RowID INT IDENTITY(1,1), IndexName VARCHAR(128), PartitionScheme VARCHAR(128), TableName VARCHAR(128));
DECLARE	@IndexCount INT,
		@Loop INT, 
		@IndexName VARCHAR(128),
		@DatabaseName VARCHAR(128),
		@Message VARCHAR(max),
		@SQLcmd VARCHAR(max)

IF EXISTS (Select 1 FROM sys.tables
		INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
		WHERE sys.schemas.name = 'RecHubSystem'
		AND sys.tables.name = 'SwitchableDataImportQueue')
		BEGIN
			RAISERROR('Dropping SwitchableDataImportQueue table', 10,1) WITH NOWAIT;
			DROP TABLE RecHubSystem.SwitchableDataImportQueue;
		END


IF EXISTS (Select 1 FROM sys.tables
		INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
		WHERE sys.schemas.name = 'RecHubSystem'
		AND sys.tables.name = 'TEMP_DataImportQueue')
		BEGIN
			RAISERROR('Dropping TEMP_DataImportQueue table', 10,1) WITH NOWAIT;
			DROP TABLE RecHubSystem.TEMP_DataImportQueue;
		END
CREATE TABLE [RecHubSystem].[TEMP_DataImportQueue](
	[DataImportQueueID] [bigint] IDENTITY(1,1) NOT NULL,
	[QueueType] [tinyint] NOT NULL,
	[QueueStatus] [tinyint] ,
	[ResponseStatus] [tinyint] ,
	[RetryCount] [tinyint] ,
	[AuditDateKey] [int] NOT NULL,
	[XSDVersion] [varchar](12) NOT NULL,
	[ClientProcessCode] [varchar](40) NOT NULL,
	[SourceTrackingID] [uniqueidentifier] NOT NULL,
	[EntityTrackingID] [uniqueidentifier] NOT NULL,
	[ResponseTrackingID] [uniqueidentifier] NULL,
	[XMLDataDocument] [xml] NOT NULL,
	[XMLResponseDocument] [xml] NULL,
	[CreationDate] [datetime] ,
	[CreatedBy] [varchar](128) ,
	[ModificationDate] [datetime] ,
	[ModifiedBy] [varchar](128) 
)

INSERT INTO [RecHubSystem].[TEMP_DataImportQueue]
           ([QueueType]
           ,[QueueStatus]
           ,[ResponseStatus]
           ,[RetryCount]
           ,[AuditDateKey]
           ,[XSDVersion]
           ,[ClientProcessCode]
           ,[SourceTrackingID]
           ,[EntityTrackingID]
           ,[ResponseTrackingID]
           ,[XMLDataDocument]
           ,[XMLResponseDocument]
           ,[CreationDate]
           ,[CreatedBy]
           ,[ModificationDate]
           ,[ModifiedBy])

	SELECT [QueueType]
      ,[QueueStatus]
      ,[ResponseStatus]
      ,[RetryCount]
      ,[AuditDateKey]
      ,[XSDVersion]
      ,[ClientProcessCode]
      ,[SourceTrackingID]
      ,[EntityTrackingID]
      ,[ResponseTrackingID]
      ,[XMLDataDocument]
      ,[XMLResponseDocument]
      ,[CreationDate]
      ,[CreatedBy]
      ,[ModificationDate]
      ,[ModifiedBy]
  FROM [RecHubSystem].[DataImportQueue];


IF EXISTS (Select 1 FROM sys.tables
		INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
		WHERE sys.schemas.name = 'RecHubSystem'
		AND sys.tables.name = 'DataImportQueue')
		BEGIN
			RAISERROR('Dropping DataImportQueue table', 10,1) WITH NOWAIT;
			DROP TABLE RecHubSystem.DataImportQueue;
		END

IF EXISTS(SELECT 1 from sys.filegroups where name = 'DataImportCatchAll')
	RAISERROR('DataImportCatchAll filegroup already exists',10,1) WITH NOWAIT
ELSE
	BEGIN
	RAISERROR('Creating Filegroup DataImportCatchAll',10,1) WITH NOWAIT
	SELECT @DatabaseName = DB_NAME()
	SELECT @SQLcmd = 'ALTER DATABASE ' +@DatabaseName + ' ADD FILEGROUP DataImportCatchAll'
	EXEC (@SQLcmd)
	END;
IF EXISTS(SELECT 1 from sys.database_files where name = 'DataImportCatchAll')
	RAISERROR('DataImportCatchAll database file already exists',10,1) WITH NOWAIT
ELSE
	BEGIN
	RAISERROR('Creating database File DataImportCatchAll',10,1) WITH NOWAIT
	--remember to change the database name and the path before running
	ALTER DATABASE WFSDB_R360 ADD FILE  (NAME = DataImportCatchAll, FILENAME = 'D:\SQLData\Dataimport\PartitionTestingDataImportCatchAll.mdf', SIZE = 10, MAXSIZE = UNLIMITED, FILEGROWTH = 10%) TO FILEGROUP DataImportCatchAll 
	RAISERROR(@SQLcmd,10,1) WITH NOWAIT
	EXEC (@SQLcmd)
	END;

EXEC RecHubSystem.usp_ResetPartitionScheme 'DataImport', 'DataImport_PartitionFunction'

--SELECT  *
--FROM    ph.FileGroupDetail
--WHERE pf_name = 'DataImport_PartitionFunction';
--GO

CREATE TABLE RecHubSystem.DataImportQueue
(
	DataImportQueueID BIGINT NOT NULL IDENTITY(1,1),
	QueueType TINYINT NOT NULL
		CONSTRAINT CK_DataImportQueue_QueueType CHECK(QueueType IN (0,1,2)),
	QueueStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_QueueStatus DEFAULT 10
		CONSTRAINT CK_DataImportQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99,120,145,150)),
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_ResponseStatus DEFAULT 255
		CONSTRAINT CK_DataImportQueue_ResponseStatus CHECK(ResponseStatus IN (0,1,2,30,255)),
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_DataImportQueue_RetryCount DEFAULT 0,
	AuditDateKey INT NOT NULL,
	XSDVersion VARCHAR(12) NOT NULL,
	ClientProcessCode VARCHAR(40) NOT NULL, 
	SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
	EntityTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseTrackingID UNIQUEIDENTIFIER NULL,
	XMLDataDocument XML NOT NULL,
	XMLResponseDocument XML NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_DataImportQueue_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_DataImportQueue_ModifiedBy DEFAULT SUSER_NAME()
) ON DataImport (AuditDateKey);

--WFSScriptProcessorForeignKey
ALTER TABLE RecHubSystem.DataImportQueue ADD 
	CONSTRAINT PK_DataImportQueue PRIMARY KEY NONCLUSTERED (DataImportQueueID, AuditDateKey)

--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_EntityTrackingID
CREATE CLUSTERED INDEX IDX_DataImportQueue_DataImportQueueID_AuditDateKey ON RecHubSystem.DataImportQueue (DataImportQueueID, AuditDateKey) ON DataImport (AuditDateKey);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_EntityTrackingID
CREATE INDEX IDX_DataImportQueue_EntityTrackingID ON RecHubSystem.DataImportQueue (EntityTrackingID) ON DataImport (AuditDateKey);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_SourceTrackingID
CREATE INDEX IDX_DataImportQueue_SourceTrackingID ON RecHubSystem.DataImportQueue (SourceTrackingID) ON DataImport (AuditDateKey);
--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_ClientProcessCode
CREATE INDEX IDX_DataImportQueue_ClientProcessCode ON RecHubSystem.DataImportQueue (ClientProcessCode) ON DataImport (AuditDateKey);

--WFSScriptProcessorIndex RecHubSystem.DataImportQueue.IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus
CREATE INDEX IDX_DataImportQueue_QueueTypeClientProcessCodeQueueStatus ON RecHubSystem.DataImportQueue 
	(
	QueueType, 
	ClientProcessCode,
	QueueStatus
	) 
INCLUDE 
	(SourceTrackingID) ON DataImport (AuditDateKey);

--WFSScriptProcessorIndex  RecHubSystem.DataImportQueue.IDX_DataImportQueue_QueueStatusResponseTrackingID 
CREATE INDEX IDX_DataImportQueue_QueueStatusResponseTrackingID ON RecHubSystem.DataImportQueue 
	(
	QueueStatus, 
	ResponseTrackingID
	) ON DataImport (AuditDateKey);

INSERT INTO [RecHubSystem].[DataImportQueue]
           ([QueueType]
           ,[QueueStatus]
           ,[ResponseStatus]
           ,[RetryCount]
           ,[AuditDateKey]
           ,[XSDVersion]
           ,[ClientProcessCode]
           ,[SourceTrackingID]
           ,[EntityTrackingID]
           ,[ResponseTrackingID]
           ,[XMLDataDocument]
           ,[XMLResponseDocument]
           ,[CreationDate]
           ,[CreatedBy]
           ,[ModificationDate]
           ,[ModifiedBy])

	SELECT [QueueType]
      ,[QueueStatus]
      ,[ResponseStatus]
      ,[RetryCount]
      ,[AuditDateKey]
      ,[XSDVersion]
      ,[ClientProcessCode]
      ,[SourceTrackingID]
      ,[EntityTrackingID]
      ,[ResponseTrackingID]
      ,[XMLDataDocument]
      ,[XMLResponseDocument]
      ,[CreationDate]
      ,[CreatedBy]
      ,[ModificationDate]
      ,[ModifiedBy]
  FROM [RecHubSystem].[TEMP_DataImportQueue];  

--SELECT  *
--FROM    ph.FileGroupDetail
--WHERE pf_name = 'DataImport_PartitionFunction';
--GO