--<< Before Running this command press CTRL T to turn on Results To Text				>>--
--<< Set Defaults to desired values (these are used when the table values are NULL		>>--

DECLARE @defaultViewingDays		INT = 365,
		@defaultMaxSearchDays	INT = 365;

;WITH OLInfo AS
(
	SELECT
		ROW_NUMBER() OVER (PARTITION BY OLTA.OLLockboxes.SiteBankID,OLTA.OLLockboxes.SiteLockboxID ORDER BY OLTA.OLLockboxes.SiteBankID,OLTA.OLLockboxes.SiteLockboxID,OLTA.OLCustomers.CustomerCode) AS RecID,
		OLTA.OLCustomers.CustomerCode,
		OLTA.OLLockboxes.SiteBankID,
		OLTA.OLLockboxes.SiteLockboxID,
		COALESCE(OLTA.OLLockboxes.ViewingDays, OLCustomers.ViewingDays) AS ViewingDays,
		COALESCE(OLTA.OLLockboxes.MaximumSearchDays, OLCustomers.MaximumSearchDays) AS MaxSearchDays
	FROM 
		OLTA.OLLockboxes
		INNER JOIN OLTA.OLCustomers ON OLTA.OLLockboxes.OLCustomerID = OLCustomers.OLCustomerID
	WHERE OLTA.OLLockboxes.IsActive = 1
)
SELECT 
	CASE RecID
		WHEN 1 THEN
			COALESCE(CustomerCode,'EntityName')
		WHEN COALESCE(RecID,99) THEN 'DUPLICATE: ' + COALESCE(CustomerCode, 'EntityName')
		ELSE
			'UNKNOWN'
	END  AS EntityName, ', ',
	COALESCE(LongName,ShortName) AS WorkgroupLongName, ',', 
	OLTA.dimLockboxes.SiteBankID AS BankID, ',',
	OLTA.dimLockboxes.SiteLockboxID AS WorkgroupID, ',',
	COALESCE(OLInfo.ViewingDays, @defaultViewingDays) AS ViewingDays, ',', 
	COALESCE(OLInfo.MaxSearchDays, @defaultMaxSearchDays) AS MaxSearchDays
FROM
	OLTA.dimLockboxes
	LEFT JOIN OLInfo ON OLInfo.SiteBankID = OLTA.dimLockboxes.SiteBankID 
		AND OLInfo.SiteLockboxID = OLTA.dimLockboxes.SiteLockboxID 
WHERE OLTA.dimLockboxes.MostRecent = 1
ORDER BY
	OLTA.dimLockboxes.SiteBankID,
	OLTA.dimLockboxes.SiteLockboxID,
	RecID,
	CustomerCode;

--<< After Running this command save the Results pane to a file with a suffix of .csv	>>--
--<< The file can then be opened in Excel												>>--

