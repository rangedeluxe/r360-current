﻿Param (
	#Version of SQL the install is going against
	#Valid values: 2012, 2016
	$SQLVersion="2012",
	#Name of the SQL Database server
	$DBSERVER = "SERVERNAME",
	#Name of the R360 Database-Default is WFSDB_R360
	$R360DB = "WFSDB_R360",
	#Installation Folder
	$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03.03",
	#Database Install Application Folder
	$INSTALLERAPP = "D:\WFSApps\Utilities\WFSDatabaseInstall",
	#Log Folder
	$LOGFOLDER = "D:\WFSStaging\DB\Installlogs",
	#Drive we're installing XML DLL to
	$INSTALLDRIVE = "D:",
	$Verbose = $true
)

# Copy out the utilties folder first.
$utildest = "$INSTALLDRIVE\WFSApps\Utilities"
$utilfrom = "$INSTALLDRIVE\WFSStaging\Utilities"
if (test-path $utilfrom) {
	write-host "Copying utilities folder to $utildest..."
	copy-item $utilfrom $utildest -force -recurse -container
	write-host "done."
}
else {
	write-host "No utilities found in folder $utilfrom."
}

# Find our installer location
if ( $SQLVersion -eq "2016" )
{
	$Installer = "$INSTALLERAPP\WFSDatabaseInstallConsole2016"
}
else {
	$Installer = "$INSTALLERAPP\WFSDatabaseInstallConsole"
}

# Install our DB
&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.03.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
if ($Verbose -eq $true) {
	Read-Host 'R360 Patch 2.03.03.00 has completed-Press any key to continue' | Out-Null 
}

&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.03.00SSISPatch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
if ($Verbose -eq $true) {
	Read-Host 'SSIS Patch 2.03.03.00 has completed-Press any key to continue' | Out-Null 
}

