param
(
	[parameter(Mandatory = $true)][string] $DBServer = "",
	[parameter(Mandatory = $true)][string] $DBName = "",
	[parameter(Mandatory = $true)][string] $CommandFile = "",
	[parameter(Mandatory = $true)][string] $URL = "",
	[string] $InstallationFolder = "",
	[string] $LogFile = "",
	[string] $LogFolder = "",
	[string] $UserName = "",
	[string] $LoginID = "",
	[string] $Password = "",
	[int] $SQLServerVersion =$null,
	[string] $SSRSFolder = ""
) 

$ScriptVerison = "1.4";

Write-Host "Install SSRS PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2013-2018 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2013-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 07/02/13 JPB	1.0		WI 108247	Created.
# 01/19/14 JPB	1.1		WI XXXXXX	Added support when RDS is not part of the install.
# 02/01/14 JPB	1.2		WI XXXXXX	Added support for LoginID/password
# 09/28/17 MGE	1.3		PT 150458530	Adapted to SQL 2016
# 01/30/18 MGE  1.4     PT 154337090    Fixed bug discovered during 2.01 rollup testing
# 02/11/19 MGE	1.5		CR R360-15101	Added tls1.2 to protocols
################################################################################

################################################################################
# CreateSSRSFolder
# Adapted from http://randypaulo.wordpress.com/2012/02/21/how-to-install-deploy-ssrs-rdl-using-powershell/
################################################################################
function CreateSSRSFolder
{
	param
	(
		[string] $local:ReportFolder
	)

	try
	{
		$SSRSProxy.CreateFolder($local:ReportFolder,"/",$null) | Out-Null;
	}
	catch [System.Web.Services.Protocols.SoapException]
	{
		if( $_.Exception.Detail.InnerText -notmatch "[^rsItemAlreadyExists400]" )
		{
			$local:ErrorMsg = "Error creating folder $local:ReportFolder. Error Message: {0}" -f $_.Exception.Detail.InnerText
			Write-Host $local:ErrorMsg;
			Write-Log $LogFile $local:ErrorMsg;
		}
	}
}

################################################################################
# InstallRDL
# Adapted from http://randypaulo.wordpress.com/2012/02/21/how-to-install-deploy-ssrs-rdl-using-powershell/
# and http://blogs.devhorizon.com/reza/2008/10/22/powershelldeploying-ssrs-reports-in-integrated-mode/
################################################################################
function InstallRDL
{
	param
	(
		[string] $local:Report,
		[string] $local:ReportFolder,
		[string] $local:ReportName,
		[string] $local:LogFileName,
		[string] $local:RDSName,
		[ref] $local:DataSourceDefinition
	)
	
	try
    {
		$Ret = 0;
	
        #Get Report content in bytes
        #Write-Host "Source=""$local:Report" -NoNewline
        $ByteArray = Get-Content $local:Report -encoding byte
        $local:ReportFolder = "/"+$local:ReportFolder
        $local:ReportFolder
        #Call Proxy to upload report
		$Warnings = New-Object SSRSProxy.Warning;

        #Write-Host "Adding/Updating ""$local:ReportName"" ""$local:ReportFolder""..."
		#Add the report to SSRS
        $serverRpt = $SSRSProxy.CreateCatalogItem("Report",$local:ReportName,$local:ReportFolder,$true,$ByteArray,$null,[ref]$Warnings);

		#Setup the connection to the shared data source
        
        $referencedDataSourceName = (@($SSRSProxy.GetItemReferences($serverRpt.Path, "DataSource")))[0].Name
        #Write-Host "RDS=""$referencedDataSourceName"
        $local:RDSName = $referencedDataSourceName
        
		#[SSRSProxy.DataSource[]] $ItemDataSources = $SSRSProxy.GetItemDataSources($serverRpt.path) #($local:ReportFolder+"/"+[System.IO.Path]::GetFileNameWithoutExtension($local:ReportName))
		[SSRSProxy.DataSource] $DataSource = New-Object SSRSProxy.DataSource;

		$DataSource.Name = $referencedDataSourceName;
        #$DataSource.Item = New-Object SSRSProxy.DataSourceReference;
        $DataSource.Item = New-Object SSRSProxy.DataSourceReference;
        #Write-Host "RDS=""$SSRSProxy.DataSource[0].Name";
		$DataSource.Item.Reference =  $local:ReportFolder + "/" + $local:RDSName + ".rds";
        #Write-Host "DataSource.Item =" "$DataSource.Item"
		#$SSRSProxy.SetItemDataSources($local:ReportFolder+"/"+$local:ReportName,$ItemDataSources);
        $SSRSProxy.SetItemDataSources($serverRpt.Path, [SSRSProxy.DataSource[]]$DataSource)

		if( $Warnings.Count -gt 0 )
		{
			foreach( $Warning in $Warnings )
			{
				if( $Warning.Code -ne "rsDataSourceReferenceNotPublished" ) #Ignore the warning that there is no data source, it was done above.
				{
					Write-Log $local:LogFileName $Warning.Message;
					$Ret = 1;
				}
			}
		}
	}
    catch [System.IO.IOException]
    {
        $msg = "Error while reading rdl file : '{0}', Message: '{1}'" -f $local:Report, $_.Exception.Message;
		Write-Log $local:LogFileName $msg;
		$Ret = 2;
   }
    catch [System.Web.Services.Protocols.SoapException]
    {
        $msg = "Error while uploading rdl file : '{0}', Message: '{1}'" -f $local:Report, $_.Exception.Detail.InnerText;
		Write-Log $local:LogFileName $msg;
		$Ret = 2;
    }
	catch
	{
        $msg = "Error proceessing rdl file : '{0}', Message: '{1}'" -f $local:Report, $_.Exception.Message;
		Write-Log $local:LogFileName $msg;
		$Ret = 2;
	}
	if( !$bRet )
	{
		Write-Log $local:LogFileName ("Successfully installed " + [System.IO.Path]::GetFileName($local:Report));
	}
	return $Ret;
}

################################################################################
# InstallRDS
################################################################################
function InstallRDS
{
	param
	(
		[string] $local:RDS,
		[string] $local:ReportFolder,
		[string] $local:DBServer,
		[string] $local:DBName,
		[string] $local:ResultPath,
		[ref] $local:DataSourceDefinition
	)

	$bRet = $true;
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:RDS) + "_" + "Results.txt";

	try
	{
		$rdsXML = New-Object "System.Xml.XmlDocument";
		$rdsXML.load($local:RDS);
		$RptDataSource = $rdsXML.RptDataSource;
		$ConnectionString = $RptDataSource.ConnectionProperties.ConnectString.Replace("`$(DBServer)",$local:DBServer);
		$ConnectionString = $ConnectionString.Replace("`$(DBName)",$local:DBName);
		[SSRSProxy.DataSource] $DataSource = New-Object SSRSProxy.DataSource;
		$DataSource.Name = $RptDataSource.Name;
		$DataSource.Item = $local:DataSourceDefinition.Value;
		$local:DataSourceDefinition.Value.Extension = $RptDataSource.ConnectionProperties.Extension;
		$local:DataSourceDefinition.Value.ConnectString  = $ConnectionString;
		$local:DataSourceDefinition.Value.Enabled = $true;
		$local:DataSourceDefinition.Value.WindowsCredentials = $true;
		$local:DataSourceDefinition.Value.CredentialRetrieval = [SSRSProxy.CredentialRetrievalEnum]::Integrated;
		$SSRSProxy.CreateDataSource([System.IO.Path]::GetFileName($local:RDS),"/"+$local:ReportFolder,$true,$local:DataSourceDefinition.Value,$null) | Out-Null;
	}
    catch [System.IO.IOException]
    {
        $msg = "Error while reading rds file : '{0}', Message: '{1}'" -f $local:RDS, $_.Exception.Message;
		Write-Log $local:LogFileName $msg;
		$bRet = $false;
    }
    catch [System.Web.Services.Protocols.SoapException]
    {
        $msg = "Error while uploading rds file : '{0}', Message: '{1}'" -f $local:RDS, $_.Exception.Detail.InnerText;
		Write-Log $local:LogFileName $msg;
		$bRet = $false;
    }	
	if( $bRet )
	{
		Write-Log $local:LogFileName ("Successfully installed " + [System.IO.Path]::GetFileName($local:RDS));
	}
	return $bRet;
}

################################################################################
# Write-Log
################################################################################
function Write-Log 
{
	param
	(
		[String] $LogFileName,
		[String] $LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFileName) -eq $null )
		{
			$LogFileName = Get-Location + "\" + $LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date -f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date -f "yyyy-MM-dd HH:mm:ss") + " " + $LogInformation | Out-File -FilePath $LogFileName -Encoding unicode -Append;
	}
}

################################################################################
# Main.
################################################################################

if( $LogFile.Length -eq 0 )
{
	$LogFile += [system.io.path]::GetFilenameWithoutExtension($CommandFile) + "_Results.txt";
}

if( $UserName.Length -eq 0 )
{
	$UserName = (gwmi -class win32_computerSystem).UserName;
}

if( $SSRSFolder.Length -eq 0 )
{
	$SSRSFolder = "R360HubReports";
}

if( !$URL.ToUpper().Contains("WSDL") )
{
	$WSDL = $URL + "/ReportService2010.asmx?wsdl";
}

if( [System.IO.Path]::GetExtension($CommandFile).Length -eq 0 )
{
	$CommandFile += ".XML";
}

if( $LoginID.Length -ne 0 )
{
	if( $Password.Length -eq 0 )
	{
		Write-Host "Password must be included when LoginID is used. Aborting installation.";
		break;
	}
	$WindowsAuth = $false;
}
else
{
	$WindowsAuth = $true;
}

[string] $ScriptPath | Out-Null;

if( $InstallationFolder.Length -gt 0 )
{
	if((Test-Path $InstallationFolder) -ne $true )
	{
		Write-Host "Installation Folder "$InstallationFolder" not found. Aborting Installation."
		break;
	}
	$ScriptPath = $InstallationFolder;
}
else
{
	$ScriptPath = [System.IO.Path]::GetDirectoryName($CommandFile);
	if( $ScriptPath.Length -eq 0 -or $ScriptPath -eq "\" -or $ResultPath -eq "" -or $ResultPath -eq $null )
	{
		$ScriptPath = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
	}
}

if( !$ScriptPath.EndsWith("\") )
{
	$ScriptPath += "\";
}

#Setup folders
if( $LogFolder.Length -eq 0 )
{
	$ResultPath = $ScriptPath + "Results" + "_" + (Get-Date -f yyyyMMddHHmmss)+ "\";
}
else
{
	if( !$LogFolder.EndsWith("\") )
	{
		$LogFolder += "\";
	}
	
	$ResultPath = $LogFolder + "Results" + "_" + (Get-Date -f yyyyMMddHHmmss)+ "\";
	if( !$ResultPath.EndsWith("\") )
	{
		$ResultPath += "\";
	}
}
$LogPath = $ResultPath;

#Check for the results folder
if ((Test-Path -path $ResultPath) -ne $true)
{
	Write-Host "Creating Results Folder";
	New-Item $ResultPath -type directory | Out-Null;
}

$LogFile = $ResultPath + $LogFile;
$FQP = $ScriptPath+[system.io.path]::GetFileName($CommandFile);

#cleanup path names
IF( $LogFile.Contains(" ") )
{
	$LogFile = """" + $LogFile + """";
}

IF( $FQP.Contains(" ") )
{
	$FQP = """" + $FQP + """";
}

Write-Host "Processing command file "$FQP;
Write-Host "Running as user         "$UserName;
Write-Host "Database Server         "$DBServer;
Write-Host "Database Name           "$DBName;
Write-Host "SSRS Server             "$URL;
Write-Host "WSDL                    "$WSDL;
Write-Host "SSRS Folder             "$SSRSFolder;
Write-Host "Command File            "$CommandFile;
Write-Host "Log File                "$LogFile;
Write-Host "Script Path             "$ScriptPath;
Write-Host "Results Path            "$ResultPath;
Write-Host;

Write-Log $LogFile ("Install SSRS PowerShell Script Version " + $ScriptVerison);
Write-Log $LogFile "-------------------------------------------------------------------------------";
Write-Log $LogFile ("Running as user       " + $UserName);
Write-Log $LogFile ("Database Server       " + $DBServer);
Write-Log $LogFile ("Database Name         " + $DBName);
Write-Log $LogFile ("SSRS Server           " + $URL);
Write-Log $LogFile ("WSDL                  " + $WSDL);
Write-Log $LogFile ("SSRS Folder           " + $SSRSFolder);
Write-Log $LogFile ("Command File          " + $CommandFile);
Write-Log $LogFile ("Windows Authenticaion " + $WindowsAuth);
Write-Log $LogFile ("Script Path           " + $ScriptPath);
Write-Log $LogFile ("Results Path          " + $ResultPath);
Write-Log $LogFile "-------------------------------------------------------------------------------";

#Make sure we can access the XML file
if ((Test-Path $FQP) -eq $false )
{
	Write-Host "Could not access command file "$CommandFile". Aborting installation.....";
	break;
}

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

if( $WindowsAuth )
{
	try
	{
		$SSRSProxy = New-WebServiceProxy -Uri $WSDL -UseDefaultCredential -NameSpace SSRSProxy -Class SSRSProxy
	}
	catch
	{
		Write-Error $_ -ErrorAction:Stop;
	}
}
else
{
	try
	{
		$SSRSProxy = New-WebServiceProxy -Uri $WSDL -Credential $LoginID -NameSpace SSRSProxy -Class SSRSProxy
	}
	catch
	{
		Write-Error $_ -ErrorAction:Stop;
	}
}
$DataSourceDefinition = New-Object SSRSProxy.DataSourceDefinition;

$UpdateDataSource = $true;

CreateSSRSFolder $SSRSFolder;

$xml = New-Object "System.Xml.XmlDocument";
$xml.load($FQP);

foreach( $ScriptFile in $xml.SSRSInstall.ProcessFile )
{
	Write-Host "Processing WI(s): "$ScriptFile.WI"..." -NoNewline;
	if( $ScriptFile.GetAttribute("ReportName") )
	{
		$ReportName = $ScriptFile.ReportName;
	}
	else
	{
		
		$ReportName = [System.IO.Path]::GetFileNameWithoutExtension($ScriptFile.get_InnerText());
        Write-Host ""$ReportName"" -NoNewline
	}
	$ScriptFQP = $ScriptPath+$ScriptFile.LastChild.InnerText.Trim();
	$FileType = [System.IO.Path]::GetExtension($ScriptFQP);
	if( $FileType.ToUpper().Contains("RDL") )
	{
		$LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($ScriptFQP) + "_" + "Results.txt";
		switch( (InstallRDL $ScriptFQP $SSRSFolder $ReportName $LogFileName $RDSName ([ref]$DataSourceDefinition)) )
		{
			0 
				{
		 			Write-Log $LogFile ("Successfully processed WI " + $ScriptFile.WI + ".");
					Write-Host " ... Successful";
				}
			1
				{
		 			Write-Log $LogFile ("Warning processing WI " + $ScriptFile.WI + ". Review individual log file for additional details.");
					Write-Host "Warning" -ForegroundColor Yellow;
				}
			2
				{
		 			Write-Log $LogFile ("Error processing WI " + $ScriptFile.WI + ". Review individual log file for additional details.");
					Write-Host "Error" -ForegroundColor Red;
				}
		}
	}
	else
	{
		$RDSName = $ScriptFile.LastChild.InnerText.Trim();
		if( $RDSName.Contains("\") )
		{
			$RDSName = $RDSName.Substring($RDSName.IndexOf('\')+1,$RDSName.Length-$RDSName.IndexOf('\')-1);
		}
		if( !(InstallRDS $ScriptFQP $SSRSFolder $DBServer $DBName $ResultPath ([ref]$DataSourceDefinition)) )
		{
 			Write-Log $LogFile ("Error processing WI " + $ScriptFile.WI + ". Review individual log file for additional details.");
			Write-Host "Error" -ForegroundColor Red;
		}
		else
		{
 			Write-Log $LogFile ("Successfully processed WI " + $ScriptFile.WI + ".");
			Write-Host " ... Successful";
		}
		#Reset the $RDSConnectionInfo to false if we had a RDS file to process
		$UpdateDataSource = $false;
	}
}

