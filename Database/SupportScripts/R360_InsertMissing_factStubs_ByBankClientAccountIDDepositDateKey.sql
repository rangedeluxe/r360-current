/* Script Name = R360_InsertMissing_factStubs_ByBankClientAccountIDDepositDateKey.sql			*/
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 12/20/2016
*
* Purpose: The script Adds factStubs by BankID, ClientAccountID and DepositDateKey.
*				This will identify if FactStubs rows are missing related to
*				FactDataEntryDetails rows loaded and Construct and ADD these FactStub rows.
*				FactBatchSummary and FactTransactionSummary rows related to these new stub rows will be updated.
*				We will use the date supplied as the Max date, So we will add any rows
*				that are equal to or less than the date supplied.
*
*			BELOW in the SET STatements you will set the values for 
*			@parmBankID				Equal to the BankID identified as having rows missing
*			@parmClientAccountID	Equal to the ClientAccountID identified as having rows missing
*			@parmDepositDateKey		Equal to the Maximum (most recent) DepositdateKey identified as having rows missing
*
*			
*
*
* Modification History
* 12/20/2016 PT	#136048737	JBS	Created
* 02/09/2017 PT #138865311	MGE Updated to use dim tables vs. views for Banks and ClientAccounts
* 02/15/2017 PT #138865311  MGE Updating to handle addition items found by Herb and Client
******************************************************************************/
SET NOCOUNT ON; 
SET ARITHABORT ON;

DECLARE @parmBankID				INT,
		@parmClientAccountID	INT,
		@parmDepositDateKey		INT


----  SET your parameters here then execute 
----  SET your parameters here then execute 
----
SET @parmBankID = 999					-- BankID 
SET @parmClientAccountID = 7703			-- LockBox,ClientAccount,Workgroup ID
SET @parmDepositDateKey = 20141231      -- Stubs will be added equal to or older than this date  FORMAT  CCYYMMDD
----
----  SET your parameters here then execute 
----  SET your parameters here then execute 


BEGIN TRY

	SELECT ' Searching for Stubs to add '

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubstoInsert')) 
		DROP TABLE #StubstoInsert;

	CREATE TABLE #StubstoInsert
	(Bankkey  INT, OrganizationKey  INT, ClientAccountKey   INT ,DepositDateKey  INT, ImmutableDateKey  INT, SourceProcessingDateKey INT, BatchID  BIGINT,  SourceBatchID BIGINT, BatchNumber INT, 	BatchSourceKey SMALLINT,
	 BatchPaymentTypeKey  TINYINT,		BatchCueID INT, DepositStatus INT,	SystemType TINYINT,	 TransactionID INT, TxnSequence INT, SequenceWithinTransaction INT, BatchSequence  INT, StubSequence INT,
	DocumentBatchSequence INT, 	IsOMRDetected BIT,	CreationDate	DATETIME, 	ModificationDate DATETIME, 	BatchSiteCode  INT,  Amount MONEY, AccountNumber VARCHAR(80) 	
	)

	INSERT INTO #StubstoInsert
	(Bankkey, OrganizationKey, ClientAccountKey,DepositDateKey, ImmutableDateKey, BatchID,TransactionID,BatchSequence)
		SELECT
	Distinct
		RechubData.factDataEntryDetails.Bankkey,
		RechubData.factDataEntryDetails.OrganizationKey,
		RechubData.factDataEntryDetails.ClientAccountKey,
		RechubData.factDataEntryDetails.DepositDateKey,
		RechubData.factDataEntryDetails.ImmutableDateKey,
		RechubData.factDataEntryDetails.BatchID,
		RechubData.factDataEntryDetails.TransactionID,
		RechubData.factDataEntryDetails.BatchSequence
	FROM RechubData.factDataEntryDetails JOIN RecHubData.dimBanks ON RechubData.factDataEntryDetails.BankKey = RechubData.dimBanks.BankKey
										JOIN RecHubData.dimClientAccounts ON RechubData.factDataEntryDetails.ClientAccountKey = RechubData.dimClientAccounts.ClientAccountKey
										JOIN RecHubData.dimWorkgroupDataEntryColumns ON RechubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
										LEFT OUTER JOIN RecHubData.factstubs ON 
													RecHubData.factstubs.BankKey = RechubData.factDataEntryDetails.BankKey
													AND RecHubData.factstubs.OrganizationKey = RechubData.factDataEntryDetails.OrganizationKey
													AND RecHubData.factstubs.ClientAccountKey = RechubData.factDataEntryDetails.ClientAccountKey
													AND RecHubData.factstubs.DepositDateKey = RechubData.factDataEntryDetails.DepositDateKey
													AND RecHubData.factstubs.ImmutableDateKey = RechubData.factDataEntryDetails.ImmutableDateKey
													AND RecHubData.factstubs.BatchID = RechubData.factDataEntryDetails.BatchID
													AND RecHubData.factstubs.TransactionID = RecHubData.factDataEntryDetails.TransactionID
													AND RecHubData.factstubs.BatchSequence = RechubData.factDataEntryDetails.BatchSequence
													AND RecHubData.factstubs.IsDeleted = 0  
	WHERE	
		(RecHubData.factDataEntryDetails.DepositDateKey = @parmDepositDateKey OR RecHubData.factDataEntryDetails.DepositDateKey < @parmDepositDateKey)
		AND RecHubData.dimBanks.SiteBankID = @parmBankID
		AND RecHubData.dimClientAccounts.SiteClientAccountID =  @parmClientAccountID 
		AND RecHubData.dimClientAccounts.SiteBankID = @parmBankID
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
		AND RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RechubData.factStubs.IsDeleted IS NULL
	OPTION (RECOMPILE);

--- above needs to be IS NULL  for the real deal and it will only try to build when business key is not present
---     if it is made IS NOT NULL it will show ALL that SHOULD be present.  This should not change for Production

	SELECT ' Adding in Ammount and AccountNumbers '

	Update #StubstoInsert 
		SET #StubstoInsert.Amount = Coalesce(RechubData.factDataEntryDetails.DataEntryValueMoney, 0)
	FROM RechubData.factDataEntryDetails  
		INNER JOIN #StubstoInsert ON RecHubData.factDataEntryDetails.BankKey = #StubstoInsert.BankKey
							AND RecHubData.factDataEntryDetails.OrganizationKey = #StubstoInsert.OrganizationKey
							AND RecHubData.factDataEntryDetails.ClientAccountKey = #StubstoInsert.ClientAccountKey
							AND RecHubData.factDataEntryDetails.DepositDateKey = #StubstoInsert.DepositDateKey
							AND RecHubData.factDataEntryDetails.ImmutableDateKey = #StubstoInsert.ImmutableDateKey
							AND RecHubData.factDataEntryDetails.BatchID = #StubstoInsert.BatchID
							AND RecHubData.factDataEntryDetails.TransactionID = #StubstoInsert.TransactionID
							AND RecHubData.factDataEntryDetails.BatchSequence = #StubstoInsert.BatchSequence
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RechubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
	WHERE	
  		RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = 'Amount'

	Update #StubstoInsert 
		SET #StubstoInsert.AccountNumber = RechubData.factDataEntryDetails.DataEntryValue
	FROM RechubData.factDataEntryDetails  
		INNER JOIN #StubstoInsert ON RecHubData.factDataEntryDetails.BankKey = #StubstoInsert.BankKey
							AND RecHubData.factDataEntryDetails.OrganizationKey = #StubstoInsert.OrganizationKey
							AND RecHubData.factDataEntryDetails.ClientAccountKey = #StubstoInsert.ClientAccountKey
							AND RecHubData.factDataEntryDetails.DepositDateKey = #StubstoInsert.DepositDateKey
							AND RecHubData.factDataEntryDetails.ImmutableDateKey = #StubstoInsert.ImmutableDateKey
							AND RecHubData.factDataEntryDetails.BatchID = #StubstoInsert.BatchID
							AND RecHubData.factDataEntryDetails.TransactionID = #StubstoInsert.TransactionID
							AND RecHubData.factDataEntryDetails.BatchSequence = #StubstoInsert.BatchSequence
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RechubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey
	WHERE	
  		RecHubData.factDataEntryDetails.IsDeleted = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.IsCheck = 0
		AND RecHubData.dimWorkgroupDataEntryColumns.FieldName = 'AccountNumber'


--  Fill in all but Sequences
	Update #StubstoInsert 
		SET #StubstoInsert.SourceProcessingDateKey = RechubData.factDataEntryDetails.SourceProcessingDateKey,
			#StubstoInsert.SourceBatchID = RechubData.factDataEntryDetails.SourceBatchID,
			#StubstoInsert.BatchNumber = RechubData.factDataEntryDetails.BatchNumber,
			#StubstoInsert.BatchSourceKey = RechubData.factDataEntryDetails.BatchSourceKey,
			#StubstoInsert.BatchPaymentTypeKey = RechubData.factDataEntryDetails.BatchPaymentTypeKey,
			#StubstoInsert.DepositStatus = RechubData.factDataEntryDetails.DepositStatus, 
			#StubstoInsert.CreationDate = RechubData.factDataEntryDetails.CreationDate,
			#StubstoInsert.ModificationDate = RechubData.factDataEntryDetails.ModificationDate
	FROM RechubData.factDataEntryDetails  
		INNER JOIN #StubstoInsert ON RecHubData.factDataEntryDetails.BankKey = #StubstoInsert.BankKey
							AND RecHubData.factDataEntryDetails.OrganizationKey = #StubstoInsert.OrganizationKey
							AND RecHubData.factDataEntryDetails.ClientAccountKey = #StubstoInsert.ClientAccountKey
							AND RecHubData.factDataEntryDetails.DepositDateKey = #StubstoInsert.DepositDateKey
							AND RecHubData.factDataEntryDetails.ImmutableDateKey = #StubstoInsert.ImmutableDateKey
							AND RecHubData.factDataEntryDetails.BatchID = #StubstoInsert.BatchID
							AND RecHubData.factDataEntryDetails.TransactionID = #StubstoInsert.TransactionID
							AND RecHubData.factDataEntryDetails.BatchSequence = #StubstoInsert.BatchSequence 
	WHERE	
  		RecHubData.factDataEntryDetails.IsDeleted = 0

	Update #StubstoInsert 
		SET 
	 		#StubstoInsert.BatchCueID = RechubData.factTransactionSummary.BatchCueID,    
	 		#StubstoInsert.SystemType = RechubData.factTransactionSummary.SystemType,
			#StubstoInsert.BatchSiteCode = RechubData.factTransactionSummary.BatchSiteCode,
			#StubstoInsert.TxnSequence = RechubData.factTransactionSummary.TxnSequence,
			#StubstoInsert.IsOMRDetected = CASE 
												WHEN RechubData.factTransactionSummary.OMRCount > 0 THEN 1  ELSE 0
											END  
	FROM RechubData.factTransactionSummary  
		INNER JOIN #StubstoInsert ON RecHubData.factTransactionSummary.BankKey = #StubstoInsert.BankKey
							AND RecHubData.factTransactionSummary.OrganizationKey = #StubstoInsert.OrganizationKey
							AND RecHubData.factTransactionSummary.ClientAccountKey = #StubstoInsert.ClientAccountKey
							AND RecHubData.factTransactionSummary.DepositDateKey = #StubstoInsert.DepositDateKey
							AND RecHubData.factTransactionSummary.ImmutableDateKey = #StubstoInsert.ImmutableDateKey
							AND RecHubData.factTransactionSummary.BatchID = #StubstoInsert.BatchID
							AND RecHubData.factTransactionSummary.TransactionID = #StubstoInsert.TransactionID 
	WHERE	
  		RecHubData.factTransactionSummary.IsDeleted = 0

-- UPDate the DocumentBatchSequence  from the Factdocuments
/*
    REMOVING the setting of DocumentBatchSequence because we do not have enough information to relate data

UPDATE #StubstoInsert
	SET DocumentBatchSequence = RecHubData.factDocuments.BatchSequence
FROM 
	RecHubData.factDocuments
WHERE
	RecHubData.factDocuments.DepositDateKey = #StubstoInsert.DepositDateKey AND 
	RecHubData.factDocuments.ImmutableDateKey = #StubstoInsert.ImmutableDateKey AND
	RecHubData.factDocuments.Bankkey = #StubstoInsert.Bankkey AND 
	RecHubData.factDocuments.OrganizationKey = #StubstoInsert.OrganizationKey AND 
	RecHubData.factDocuments.ClientAccountKey = #StubstoInsert.ClientAccountKey AND 
	RecHubData.factDocuments.BatchID = #StubstoInsert.BatchID AND 
	RecHubData.factDocuments.TransactionID = #StubstoInsert.TransactionID 
*/ 


 	SELECT ' Inserting Stubs  '

	BEGIN TRAN

-- NOW Insert the Stub records
INSERT INTO RecHubData.factStubs
           (IsDeleted
           ,BankKey
           ,OrganizationKey
           ,ClientAccountKey
           ,DepositDateKey
           ,ImmutableDateKey
           ,SourceProcessingDateKey
           ,BatchID
           ,SourceBatchID
           ,BatchNumber
           ,BatchSourceKey
           ,BatchPaymentTypeKey
           ,BatchCueID
           ,DepositStatus
           ,SystemType
           ,TransactionID
           ,TxnSequence
           ,SequenceWithinTransaction
           ,BatchSequence
           ,StubSequence
		   ,IsCorrespondence
           ,DocumentBatchSequence
           ,IsOMRDetected
           ,CreationDate
           ,ModificationDate
           ,BatchSiteCode
           ,Amount
           ,AccountNumber)
SELECT 
            0
           ,Bankkey
           ,OrganizationKey
           ,ClientAccountKey
           ,DepositDateKey
           ,ImmutableDateKey 
           ,SourceProcessingDateKey 
           ,BatchID
           ,SourceBatchID 
           ,BatchNumber 
           ,BatchSourceKey 
           ,BatchPaymentTypeKey 
           ,BatchCueID 
           ,DepositStatus 
           ,SystemType 
           ,TransactionID
           ,TxnSequence 
           ,0 --ROW_NUMBER() OVER (Partition BY Bankkey,OrganizationKey,ClientAccountKey,DepositDateKey,BatchID,TransactionID
			--				ORDER BY Bankkey,OrganizationKey,ClientAccountKey,DepositDateKey,BatchID,TransactionID,BatchSequence) AS  SequenceWithintransaction 
           ,BatchSequence
           ,0 --ROW_NUMBER() OVER (Partition BY Bankkey,OrganizationKey,ClientAccountKey,DepositDateKey,BatchID
				--			ORDER BY Bankkey,OrganizationKey,ClientAccountKey,DepositDateKey,BatchID,TransactionID,BatchSequence) AS  StubSequence
		   ,0    --  Default	
           ,DocumentBatchSequence
           ,IsOMRDetected
           ,CreationDate
           ,ModificationDate 
           ,BatchSiteCode 
           ,Amount
           ,AccountNumber
  FROM #StubstoInsert

   	SELECT ' Updating BatchSummary and TransactionSummary Rows  '

-- NOW TO UPDATE the Summary records
UPDATE RecHubData.factBatchSummary
SET RecHubData.factBatchSummary.StubCount = Coalesce(SCount,0),
	RecHubData.factBatchSummary.StubTotal = Coalesce(STotal,0)
FROM RecHubData.factBatchSummary
INNER JOIN (SELECT DepositDateKey, Bankkey, OrganizationKey, ClientAccountKey,BatchID ,SUM(Amount) AS STotal , SUM (1) AS SCount
				FROM RecHubData.FactStubs
				WHERE RecHubData.factStubs.factStubKey IN 
						(SELECT Distinct factStubKey  
						FROM RecHubData.FactStubs
							INNER JOIN #StubstoInsert ON 
								RecHubData.FactStubs.DepositDateKey = #StubstoInsert.DepositDateKey AND 
								RecHubData.FactStubs.Bankkey = #StubstoInsert.Bankkey AND 
								RecHubData.FactStubs.OrganizationKey = #StubstoInsert.OrganizationKey AND 
								RecHubData.FactStubs.ClientAccountKey = #StubstoInsert.ClientAccountKey AND 
								RecHubData.FactStubs.BatchID = #StubstoInsert.BatchID   
						WHERE IsDeleted = 0)
				GROUP BY 
				RecHubData.FactStubs.DepositDateKey, RecHubData.FactStubs.Bankkey,  RecHubData.FactStubs.OrganizationKey, RecHubData.FactStubs.ClientAccountKey, RecHubData.FactStubs.BatchID  
						) AS Stubs
ON 
	RecHubData.factBatchSummary.DepositDateKey = Stubs.DepositDateKey AND
	RecHubData.factBatchSummary.Bankkey = Stubs.Bankkey AND
	RecHubData.factBatchSummary.OrganizationKey = Stubs.OrganizationKey AND
	RecHubData.factBatchSummary.ClientAccountKey = Stubs.ClientAccountKey AND
	RecHubData.factBatchSummary.BatchID = Stubs.BatchID 


UPDATE RecHubData.factTransactionSummary
SET RecHubData.factTransactionSummary.StubCount = Coalesce(SCount,0),
	RecHubData.factTransactionSummary.StubTotal = Coalesce(STotal,0)
FROM RecHubData.factTransactionSummary
INNER JOIN (SELECT DepositDateKey, Bankkey, OrganizationKey, ClientAccountKey,BatchID, TransactionID,SUM(Amount) AS STotal , SUM (1) AS SCount
				FROM RecHubData.FactStubs
				WHERE RecHubData.factStubs.factStubKey IN 
						(SELECT Distinct factStubKey  
						FROM RecHubData.FactStubs
							INNER JOIN #StubstoInsert ON 
								RecHubData.FactStubs.DepositDateKey = #StubstoInsert.DepositDateKey AND 
								RecHubData.FactStubs.Bankkey = #StubstoInsert.Bankkey AND 
								RecHubData.FactStubs.OrganizationKey = #StubstoInsert.OrganizationKey AND 
								RecHubData.FactStubs.ClientAccountKey = #StubstoInsert.ClientAccountKey AND 
								RecHubData.FactStubs.BatchID = #StubstoInsert.BatchID   AND
								RecHubData.FactStubs.TransactionID = #StubstoInsert.TransactionID 
						WHERE IsDeleted = 0)
				GROUP BY 
				RecHubData.FactStubs.DepositDateKey, RecHubData.FactStubs.Bankkey,  RecHubData.FactStubs.OrganizationKey, RecHubData.FactStubs.ClientAccountKey, RecHubData.FactStubs.BatchID , RecHubData.FactStubs.TransactionID
						) AS Stubs
ON 
	RecHubData.factTransactionSummary.DepositDateKey = Stubs.DepositDateKey AND
	RecHubData.factTransactionSummary.Bankkey = Stubs.Bankkey AND
	RecHubData.factTransactionSummary.OrganizationKey = Stubs.OrganizationKey AND
	RecHubData.factTransactionSummary.ClientAccountKey = Stubs.ClientAccountKey AND
	RecHubData.factTransactionSummary.BatchID = Stubs.BatchID AND
	RecHubData.factTransactionSummary.TransactionID = Stubs.TransactionID 

	SELECT ' Setting Stub Sequences  '

	;with sequenced_factStubs AS
	(SELECT 
		factStubKey,
		ROW_NUMBER() OVER (Partition BY factStubs.Bankkey,factStubs.OrganizationKey,factStubs.ClientAccountKey,factStubs.DepositDateKey,factStubs.BatchID,factStubs.TransactionID
      ORDER BY factStubs.Bankkey,factStubs.OrganizationKey,factStubs.ClientAccountKey,factStubs.DepositDateKey,factStubs.BatchID,factStubs.TransactionID,factStubs.BatchSequence) AS SequenceWithinTransaction,   
    ROW_NUMBER() OVER (Partition BY factStubs.Bankkey,factStubs.OrganizationKey,factStubs.ClientAccountKey,factStubs.DepositDateKey,factStubs.BatchID
      ORDER BY factStubs.Bankkey,factStubs.OrganizationKey,factStubs.ClientAccountKey,factStubs.DepositDateKey,factStubs.BatchID,factStubs.TransactionID,factStubs.BatchSequence) AS StubSequence
	FROM RecHubData.factStubs 
	INNER JOIN RecHubData.dimBanks ON RechubData.factStubs.BankKey = RechubData.dimBanks.BankKey
	INNER JOIN RecHubData.dimClientAccounts ON RechubData.factStubs.ClientAccountKey = RechubData.dimClientAccounts.ClientAccountKey
	WHERE 
	(RecHubData.factStubs.DepositDateKey = @parmDepositDateKey OR RecHubData.factStubs.DepositDateKey < @parmDepositDateKey)
			AND RecHubData.dimBanks.SiteBankID = @parmBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID =  @parmClientAccountID 
			AND RecHubData.dimClientAccounts.SiteBankID = @parmBankID
			AND RecHubData.factStubs.IsDeleted = 0
	)
	UPDATE RecHubData.factStubs
	SET RecHubData.factStubs.SequenceWithinTransaction = sequenced_factStubs.SequenceWithinTransaction,
		RecHubData.factStubs.StubSequence = sequenced_factStubs.StubSequence
	FROM RecHubData.factStubs INNER JOIN sequenced_factStubs ON sequenced_factStubs.factStubKey = RecHubData.factStubs.factStubKey


	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubstoInsert')) 
		DROP TABLE #StubstoInsert;

	Commit TRAN

END TRY
BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubstoInsert')) 
		DROP TABLE #StubstoInsert;

	ROLLBACK TRAN
	DECLARE @ErrorMessage    NVARCHAR(4000),
			@ErrorSeverity   INT,
			@ErrorState      INT;
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH
