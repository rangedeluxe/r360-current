param 
(
	[string] $SQLCommand = "",
	[int] $IDType = 1
)

$ScriptVerison = "1.00";
$ScriptName = "SQLHashID";
################################################################################
# NOTE: This is an internal development tool only. It cannot be released or shared
# outside of deelopment.
################################################################################
# 08/29/2015 WI 232855 JPB	1.00	Created.
################################################################################


################################################################################
# Create a SH1Hash.
################################################################################
function SHA1Hash([string]$local:SQLCommand)
{
	$local:SH1 = [System.Security.Cryptography.SHA1]::Create();

	$local:SQLBuff = [System.Text.Encoding]::Unicode.GetBytes($SQLCommand);
	$local:SH1Hash = $local:SH1.ComputeHash($local:SQLBuff,0,$local:SQLBuff.Length);
	return [System.BitConverter]::ToString($local:SH1Hash).Replace("-", "");
}

################################################################################
# Create a MD5Hash.
################################################################################
function MD5Hash([string]$local:SQLCommand)
{
	$local:MD5 = [System.Security.Cryptography.MD5]::Create();

	$local:SQLBuff = [System.Text.Encoding]::Unicode.GetBytes($SQLCommand);
	$local:MD5 = $local:MD5.ComputeHash($local:SQLBuff,0,$local:SQLBuff.Length);
	return [System.BitConverter]::ToString($local:MD5).Replace("-", "");
}

################################################################################
# Main.
################################################################################
Write-Host $ScriptName $ScriptVerison;

if( $SQLCommand.Length -lt 1 )
{
	Write-Host "SQLCommand must be defined";
	break;
}


################################################################################
# Determine what hash to use
# 1 - SHA1
# 2 - MD5
################################################################################
switch( $IDType )
{
	1 
	{
		Write-Host (SHA1Hash $SQLCommand);
	}
	2
	{
		Write-Host (MD5Hash $SQLCommand);
	}
}