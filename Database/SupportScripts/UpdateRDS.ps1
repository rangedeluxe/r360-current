param
(
	[parameter(Mandatory = $true)][string] $DBServer = "",
	[parameter(Mandatory = $true)][string] $DBName = "",
	[parameter(Mandatory = $true)][string] $URL = "",
	[parameter(Mandatory = $true)][string] $SSRSFolder = "",
	[parameter(Mandatory = $true)][string] $RDSName = "",
	[string] $LogFolder = "",
	[string] $UserName = "",
	[string] $LoginID = "",
	[string] $Password = "",
	[int] $SQLServerVersion =$null,
	[string] $ConnectionUserName = "",
	[string] $ConnectionPassword = ""
) 

$ScriptVerison = "1.2";

Write-Host "Update SSRS RDS PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 01/05/2016 JPB	1.0		WI 252113	Created.
#
################################################################################

################################################################################
# UpdateRDS
# WI 252113
################################################################################
function UpdateRDS
{
	param
	(
		[string] $local:DataSourceName,
		[string] $local:ReportFolder,
		[string] $local:DBServer,
		[string] $local:DBName,
		[string] $local:ResultPath,
		[string] $local:ConnectionUserName,
		[string] $local:ConnectionPassword,
		[ref] $local:DataSourceDefinition
	)

	$local:Ret = $true;
	try
	{
		$local:LogFileName = $local:ResultPath + $local:DataSourceName + "_" + "Results.txt";
		$local:DataSourceName+=".rds";
		$local:DataSourceDefinition.Value = $SSRSProxy.GetDataSourceContents("/"+$SSRSFolder+"/"+$local:DataSourceName);
		$local:DataSourceDefinition.value.ConnectString = [string]::Format("Data Source={0};Initial Catalog={1}",$local:DBServer,$local:DBName);
		if( $local:ConnectionUserName.Length -ne 0 )
		{
			$local:DataSourceDefinition.Value.WindowsCredentials = $false;
			$local:DataSourceDefinition.Value.CredentialRetrieval = ([string]::Format("{0}",[SSRSProxy.CredentialRetrievalEnum]::Store));
			$local:DataSourceDefinition.Value.UserName = $local:ConnectionUserName;
			$local:DataSourceDefinition.Value.Password = $local:ConnectionPassword;
			Write-Log $local:LogFileName "Updating RDS to Credentials stored securely in the report server";
		}
		else
		{
			$local:DataSourceDefinition.Value.UserName = [System.Management.Automation.Language.NullString]::Value;
			$local:DataSourceDefinition.Value.WindowsCredentials = $true;
			$local:DataSourceDefinition.Value.CredentialRetrieval = ([string]::Format("{0}",[SSRSProxy.CredentialRetrievalEnum]::Integrated));
			Write-Log $local:LogFileName ("Updating RDS to Windows integrated security");
		}
		$SSRSProxy.CreateDataSource($local:DataSourceName,"/"+$SSRSFolder,$true,$local:DataSourceDefinition.Value,$null) | Out-Null;
	}
    catch [System.IO.IOException]
    {
        $msg = "Error while reading rds : '{0}', Message: '{1}'" -f $local:DataSourceName, $_.Exception.Message;
		Write-Log $local:LogFileName $msg;
		$local:Ret = $false;
    }
    catch [System.Web.Services.Protocols.SoapException]
    {
        $msg = "Error accessing rds : '{0}', Message: '{1}'" -f $local:DataSourceName, $_.Exception.Detail.InnerText;
		Write-Log $local:LogFileName $msg;
		$local:Ret = $false;
    }	
	return $local:Ret;

}
################################################################################
# Write-Log
################################################################################
function Write-Log 
{
	param
	(
		[String] $LogFileName,
		[String] $LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFileName) -eq $null )
		{
			$LogFileName = Get-Location + "\" + $LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $LogInformation | Out-File -FilePath $LogFileName -Encoding unicode -Append;
	}
}

################################################################################
# Main.
################################################################################

if( $UserName.Length -eq 0 )
{
	$UserName = (gwmi -class win32_computerSystem).UserName;
}

if( !$URL.ToUpper().Contains("WSDL") )
{
	$WSDL = $URL + "/ReportService2010.asmx?wsdl";
}

if( $LoginID.Length -ne 0 )
{
	if( $Password.Length -eq 0 )
	{
		Write-Host "Password must be included when LoginID is used. Aborting installation.";
		break;
	}
	$WindowsAuth = $false;
}
else
{
	$WindowsAuth = $true;
}

#Check if Connection information is being passed in for the RDS file WI 252113
if( $ConnectionUserName.Length -ne 0 )
{
	if( $ConnectionPassword.Length -eq 0 )
	{
		Write-Host "ConnectionPassword required when submitting a ConnectionUserName";
		break;
	}
}

if( $LogFolder.Length -eq 0 )
{
	$LogFolder = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
}

if( !$LogFolder.EndsWith("\") )
{
	$LogFolder += "\";
}

$LogFolder+= "Results_UpdateRDS_" + (Get-Date –f yyyyMMddHHmmss)+ "\";
if( !$LogFolder.EndsWith("\") )
{
	$LogFolder += "\";
}

#Check for the results folder
if ((Test-Path -path $LogFolder) -ne $true)
{
	Write-Host "Creating Log Folder";
	New-Item $LogFolder -type directory | Out-Null;
}

#strip .rds from name if it was included, it will be added back later
$RDSName = [System.IO.Path]::GetFileNameWithoutExtension($RDSName);

$LogFile = $LogFolder + $RDSName + "_Results.txt";

Write-Host "Running as user         "$UserName;
Write-Host "Windows Authenticaion   "$WindowsAuth;
Write-Host "Database Server         "$DBServer;
Write-Host "Database Name           "$DBName;
Write-Host "SSRS Server             "$URL;
Write-Host "WSDL                    "$WSDL;
Write-Host "SSRS Folder             "$SSRSFolder;
Write-Host "RDS                     "$RDSName;
Write-Host "Log File                "$LogFile;
Write-Host "Results Path            "$LogFolder;
Write-Host;

Write-Log $LogFile ("Install SSRS PowerShell Script Version " + $ScriptVerison);
Write-Log $LogFile "-------------------------------------------------------------------------------";
Write-Log $LogFile ("Running as user       " + $UserName);
Write-Log $LogFile ("Windows Authenticaion " + $WindowsAuth);
Write-Log $LogFile ("Database Server       " + $DBServer);
Write-Log $LogFile ("Database Name         " + $DBName);
Write-Log $LogFile ("SSRS Server           " + $URL);
Write-Log $LogFile ("WSDL                  " + $WSDL);
Write-Log $LogFile ("SSRS Folder           " + $SSRSFolder);
Write-Log $LogFile ("RDS                   " + $RDSName);
Write-Log $LogFile ("Log File              " + $LogFile);
Write-Log $LogFile ("Results Path          " + $LogFolder);
Write-Log $LogFile "-------------------------------------------------------------------------------";

if( $WindowsAuth )
{
	try
	{
		$SSRSProxy = New-WebServiceProxy -Uri $WSDL -UseDefaultCredential -NameSpace SSRSProxy -Class SSRSProxy
	}
	catch
	{
		Write-Error $_ -ErrorAction:Stop;
	}
}
else
{
	try
	{
		$SSRSProxy = New-WebServiceProxy -Uri $WSDL -Credential $LoginID -NameSpace SSRSProxy -Class SSRSProxy
	}
	catch
	{
		Write-Error $_ -ErrorAction:Stop;
	}
}

$DataSourceDefinition = New-Object SSRSProxy.DataSourceDefinition;
		
Write-Host "Updating RDS (Data Source)..." -NoNewline;

if( !(UpdateRDS $RDSName $SSRSFolder $DBServer $DBName $LogFolder $ConnectionUserName $ConnectionPassword ([ref]$DataSourceDefinition)) )
{
	Write-Log $LogFile ("Error updating RDS (Data Source). Review individual log file for additional details.");
	Write-Host "Error" -ForegroundColor Red;
}
else
{
	Write-Log $LogFile ("Successfully updated RDS (Data Source)");
	Write-Host "Successful";
}

