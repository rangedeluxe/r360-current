param
(
	[parameter(Mandatory = $true)][int] $BankID,
	[parameter(Mandatory = $true)][string] $WorkgroupID
)

[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null;


$ScriptVerison = "2.02";

Write-Host "R360 Delete Bank Workgroup All Relate PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
#	Purpose:	Delete ALL related setup data for given BankID and ClientAccountID
#				This script is the 3rd step in the process.  
#				Step 1: R360DeleteBankWorkgroupAllRelated.ps1
#				Step 2: R360 Data and Image Purge processes 
#				Step 3: R360RemoveBankWorkgroup.ps1
#
# 09/16/2016 PT #129555329 JBS	Created
#								
#
################################################################################

################################################################################
# Write message to log file.
################################################################################
function Write-Log 
{
	param
	(
		[String] $local:LogFileName,
		[String] $local:LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($local:LogFileName) -eq $null )
		{
			$local:LogFileName = Get-Location + "\" + $local:LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $local:LogInformation | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	}
}

################################################################################
# Open the database connection
################################################################################
function Create-DatabaseConnection
{
	param
	(
		[ref] $local:dbConnection,
		[string] $local:DBServer,
		[string] $local:DBName,
		[string] $local:LogFile
	)
	$local:dbConnectionOptions = ("Data Source=$local:DBServer; Initial Catalog=$local:DBName;Integrated Security=SSPI")
	$local:dbConnection.value = New-Object System.Data.SqlClient.SqlConnection($local:dbConnectionOptions);

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		Write-Log $local:LogFile $event.Message 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$Results = $false;
		}
	};
	$local:dbConnection.value.add_InfoMessage($handler);
	$local:dbConnection.value.FireInfoMessageEventOnUserErrors = $true;
}

################################################################################
# Open the database connection
################################################################################
function Open-DatabaseConnection
{
	param
	(
		[ref] $local:dbConnection,
		[String] $LogFile
	)
	try
	{
		$local:dbConnection.value.Open();
		$Results = $true;
	}
	catch
	{
		Write-Log $LogFile "Error Opening Database Connection";
		Write-Log $LogFile $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# Query the database for bank name
################################################################################
function Get-BankName
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[ref] $local:BankName
	)
	
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_dimBanks_Get_BySiteBankID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;

		$reader = $dbCommand.ExecuteReader();
		if( $reader.HasRows )
		{
			while($reader.Read()) 
			{
				$local:BankName.value = $reader['BankName']; 
			}
			$Results = $true;
		}
		else
		{
			Write-Host ("Could not locate BankID {0}. Aborting....." -f $local:SiteBankID);
			Write-Log $LogFile ("Could not locate BankID {0}." -f $local:SiteBankID);
			$Results = $false;
		}
		
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Query the database for workgroup name
################################################################################
function Get-WorkgroupName
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[int] $local:SiteClientAccountID,
		[ref] $local:ClientAccountName
	)
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_dimClientAccounts_Get_NameBySiteBankIDSiteClientAccountID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;
		$dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", $local:SiteClientAccountID) | Out-Null;

		$reader = $dbCommand.ExecuteReader();
		if( $reader.HasRows )
		{
			while($reader.Read()) 
			{
				$local:ClientAccountName.value = $reader['LongName']; 
			}
			$Results = $true;
		}
		else
		{
			Write-Host ("Could not locate WorkgroupID {0}. Aborting....." -f $local:SiteClientAccountID);
			Write-Log $LogFile ("Could not locate WorkgroupID {0}." -f $local:SiteClientAccountID);
			$Results = $false;
		}
		
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}
################################################################################
# Query the database for workgroup Counts for Deleting ALL
################################################################################
function Get-WorkgroupCount
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[ref] $local:ClientAccountCount
	)
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_dimClientAccounts_Get_CountBySiteBankID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;

		$reader = $dbCommand.ExecuteReader();
		if( $reader.HasRows )
		{
			while($reader.Read()) 
			{
				$local:ClientAccountCount.value = $reader['Count']; 
			}
			$Results = $true;
		}
		else
		{
			$local:ClientAccountCount.value = 0;
		}
		
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Query the database for Batch data associated to Bank and ClientAccountID
################################################################################
function Get-CheckForBatchData
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[string] $local:SiteClientAccountID
	)
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_factBatch_Exists_ByBankIDClientAccountID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;
		$dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", $local:SiteClientAccountID) | Out-Null;
		$local:dbCommand.Parameters.Add("@parmBatchExists",[System.Data.SqlDbType]::Bit) | Out-Null;
		$local:dbCommand.Parameters["@parmBatchExists"].Direction = [System.Data.ParameterDirection]::Output
	
		if( $local:dbCommand.ExecuteNonQuery() -eq -1 )
		{
			if ($local:dbCommand.Parameters["@parmBatchExists"].Value -eq 1)
			{
				$Results = $true;
			}
			else
			{
				$Results = $false;
			}			
			
		}
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Delete Bank-workgroup - This procedure will delete the ClientAccount rows  
#						And the Bank ID if WorkGroupID = ALL 
############################################################################
function Delete-BankWorkgroup
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[String] $LogFile,
		[int] $local:BankID,
		[string] $local:WorkgroupID
	)
	$Results = $false;
	
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
	$local:dbCommand.CommandText = "RecHubData.usp_dimClientAccounts_Del_BankClientAccountDelete";
	$local:dbCommand.Parameters.AddWithValue("@parmSiteBankID", "$local:BankID") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", "$local:WorkgroupID") | Out-Null
	$local:dbCommand.Parameters.Add("@parmUpdateSuccess",[System.Data.SqlDbType]::Bit) | Out-Null;
	$local:dbCommand.Parameters["@parmUpdateSuccess"].Direction = [System.Data.ParameterDirection]::Output
	try
	{
		if( $local:dbCommand.ExecuteNonQuery() -eq -1 )
		{
			if ($local:dbCommand.Parameters["@parmUpdateSuccess"].Value -eq 1)
			{
				$Results = $true;
			}
			else
			{
				$Results = $false;
			}			
		}
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Update System Setup table so Purge process happens sooner than default
################################################################################
function Update-SystemSetup
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[string] $local:Section,
		[string] $local:SetupKey,
		[string] $local:KeyValue,
		[String] $LogFile
	)
	$Results = $false;
	
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
	$local:dbCommand.CommandText = "RecHubConfig.usp_SystemSetup_Update";
	$local:dbCommand.Parameters.AddWithValue("@parmSection", $local:Section) | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmSetupKey", $local:SetupKey) | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmValue", $local:KeyValue) | Out-Null;
	try
	{
		if( $local:dbCommand.ExecuteNonQuery() -eq -1 )
		{
			$Results = $true;
		}
	}
	catch
	{
		Write-Log $LogFile "Error executing SQL Command - RecHubConfig.usp_SystemSetup_Update";
		Write-Log $LogFile $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}


################################################################################
# Main
################################################################################
[String] $BankName = "";
[String] $WorkgroupName = "";
[String] $ClientAccountCount = "";
[String] $Section = "";
[String] $SetupKey = "";
[String] $KeyValue = "";
[Boolean] $BatchFindSuccess = $false;
[System.Data.SqlClient.SqlConnection] $dbConnection = $null;

$ConfigFile = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Definition) + "\R360DeleteBankWorkgroupAllRelated.Config"

if( (Test-Path $ConfigFile) -ne $true )
{
	Write-Host $ConfigFile "not found, aborting.....";
	return;
}

$UserName = $env:username;

$xml = New-Object "System.Xml.XmlDocument";
$xml.load($ConfigFile);

$LogFolder = $xml.Config.LogFolder;
$DBServer = $xml.Config.DBServer;
$DBName = $xml.Config.DBName;

if( !$LogFolder.EndsWith("\") )
{
	$LogFolder += "\";
}

$LogFile = $LogFolder + [system.io.path]::GetFilenameWithoutExtension($MyInvocation.MyCommand.Definition) + "_" + (Get-Date –f yyyyMMdd) + ".txt";

Create-DatabaseConnection ([ref]$dbConnection) $DBServer $DBName $LogFile;

Write-Log $LogFile "-------------------------------------------------------------------------------";
Write-Log $LogFile ("R360 Remove Bank-Workgroup data PowerShell Script Version {0}" -f $ScriptVerison);
Write-Log $LogFile ("Database Server {0}" -f $DBServer);
Write-Log $LogFile ("Database Name   {0}" -f $DBName);
Write-Log $LogFile ("User            {0}" -f $UserName);


if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
{
	Write-Host "Getting workgoup information ..... " -NoNewline;
	$Results = Get-BankName $dbConnection $BankID ([ref]$BankName);
	if( $Results )
	{
		$dbConnection.Close();
		
		if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
		{
			if ($WorkgroupID -eq "ALL")
			{
				$Results = Get-WorkgroupCount $dbConnection $BankID ([ref]$ClientAccountCount);
			}
			else
			{
				$Results = Get-WorkgroupName $dbConnection $BankID $WorkgroupID ([ref]$WorkgroupName);
			}
		}
	}
	$dbConnection.Close();
	if( $Results )
	{
		Write-Host "Complete";
	}
}

#  check for remaining batches associated with Bank and Workgroup IDs
if( $Results )
{
	if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
	{
		Write-Host "Checking For Existing Batch Data ..... " -NoNewline;
		$Results = Get-CheckForBatchData $dbConnection $BankID $WorkgroupID ;
		$dbConnection.Close();
		Write-Host "Checking For Existing Batch Data Complete ";
	} 
}

if( $Results )
{
#  There  are still batches we can not delete setup data
	if ($WorkgroupID -eq "ALL")
	{
		[string]$Message = [string]::Format("There is still Batch data associated with the following setup data. `r`n We can not delete setup data for: `r`n`r`nBank: {0} - {1}`r`nNumber of Workgroups: {2} ",$BankID,$BankName,$ClientAccountCount);
		$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::OK,[System.Windows.Forms.MessageBoxIcon]::Information,[System.Windows.Forms.MessageBoxDefaultButton]::Button1)
		Write-Log $LogFile ("There is still Batch data loaded for this setup data.  Delete cancelled." );
		Write-Log $LogFile ("Bank            {0} ({1})" -f $BankID,$BankName.TrimEnd());
		Write-Log $LogFile ("ALL Workgroups  " );
	}
	else
	{
		[string]$Message = [string]::Format("There is still Batch data associated with the following setup data. `r`n We can not delete setup data for: `r`n`r`nWorkgroup: {0} - {1} ",$WorkgroupID,$WorkgroupName);
		$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::OK,[System.Windows.Forms.MessageBoxIcon]::Information,[System.Windows.Forms.MessageBoxDefaultButton]::Button1)
		Write-Log $LogFile ("There is still Batch data loaded for this setup data.  Delete cancelled." );
		Write-Log $LogFile ("Bank            {0} ({1})" -f $BankID,$BankName.TrimEnd());
		Write-Log $LogFile ("Workgroup       {0} ({1})" -f $WorkgroupID,$WorkgroupName.TrimEnd());
	}
	
		Write-Log $LogFile "Cancelled delete.";
		$Results = $false;
}
ELSE		#  if no batches   delete setup data
{
	if ($WorkgroupID -eq "ALL")
	{
		[string]$Message = [string]::Format("NOTE: Please verify you have successfully completed a backup of the database before continuing. `r`nThis is a permanent delete process!`r`n`r`nYou are about to delete ALL Setup DATA for:`r`n`r`nBank: {0} - {1}`r`nNumber of Workgroups: {2} `r`n`r`nWould you like to continue? ",$BankID,$BankName,$ClientAccountCount);
		$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::YesNo,[System.Windows.Forms.MessageBoxIcon]::Warning,[System.Windows.Forms.MessageBoxDefaultButton]::Button2)
		Write-Log $LogFile ("Bank      {0} ({1})" -f $BankID,$BankName.TrimEnd());
		Write-Log $LogFile ("ALL Workgroups  " );
		
		#  They want a confirmation response
		if( $Response -eq "YES" )
		{
			[string]$Message = [string]::Format("NOTE: This is a permanent delete process and there is no undo button! `r`n`r`n Are you sure you have completed a backup of the database and you want to continue?");
			$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::YesNo,[System.Windows.Forms.MessageBoxIcon]::Warning,[System.Windows.Forms.MessageBoxDefaultButton]::Button2)
		}
		if( $Response -eq "YES" )
		{
			Write-Log $LogFile ("Response Confirmed  " );
		}
	}
	else
	{
		[string]$Message = [string]::Format("NOTE: Please verify you have successfully completed a backup of the database before continuing. `r`nThis is a permanent delete process!`r`n`r`nYou are about to delete ALL Setup DATA for: `r`n`r`nBank: {0} - {1}`r`nWorkgroup: {2} - {3} ",$BankID,$BankName,$WorkgroupID,$WorkgroupName);
		$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::YesNo,[System.Windows.Forms.MessageBoxIcon]::Warning,[System.Windows.Forms.MessageBoxDefaultButton]::Button2)
		Write-Log $LogFile ("Bank            {0} ({1})" -f $BankID,$BankName.TrimEnd());
		Write-Log $LogFile ("Workgroup       {0} ({1})" -f $WorkgroupID,$WorkgroupName.TrimEnd());
		
		#  They want a confirmation response
		if( $Response -eq "YES" )
		{
			[string]$Message = [string]::Format("NOTE: This is a permanent delete process and there is no undo button! `r`n`r`nAre you sure you have completed a backup of the database and you want to continue?");
			$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Deletion of Bank and Workgroup Setup Data" , [System.Windows.Forms.MessageBoxButtons]::YesNo,[System.Windows.Forms.MessageBoxIcon]::Warning,[System.Windows.Forms.MessageBoxDefaultButton]::Button2)
		}
		if( $Response -eq "YES" )
		{
			Write-Log $LogFile ("Response Confirmed  " );
		}
	}
	
	if( $Response -eq "NO" )
	{
		Write-Log $LogFile "Cancelled delete.";
		$Results = $false;
	}
	else
	{
		Write-Host ("Bank                 {0}" -f $BankID);
		Write-Host ("Workgroup            {0}" -f $WorkGroupID); 
		Write-Host ("Log File            {0}" -f $LogFile);
		$dbConnection.Close();
		
		if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
		{
			Write-Host "Deleting Bank-WorkGroup setup data ..... " -NoNewline;
			$Results = Delete-BankWorkgroup $dbConnection $LogFile $BankID $WorkgroupID; 
			$dbConnection.Close();
			Write-Host "Deleting Bank-WorkGroup Complete";
		}
	}
	$dbConnection.Close();
}

if( $Results )
{
	Write-Host "Successfully Updated Workgroup."
	Write-Log $LogFile "Successfully Updated Workgroup."
}
else
{
	if( $Response -eq "YES" )  #  Here we tried to update but failed
	{
		Write-Host "Failed Updated Workgroup. See log file for details." -ForegroundColor Red;
		Write-Log $LogFile "Failed Updated Workgroup. See error message above.";
	}
	else 
	{
		Write-Host "Cancelled Update";  #  Either we chose NO to cancel or OK because there are still batches
	}
}

if ( $Results )
{
	Write-Host "Update SystemSetup Table Delete Percentage.";
	Write-Log $LogFile "Update SystemSetup Table Delete Percentage.";
	$dbConnection.Close();
	
		if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
		{
			Write-Host "Executing Update ..... " -NoNewline;
			#  Going to change the value in the SystemSetup table to .005 percent so purges happen faster
			#  The followup script after batch data is purged will set it back to .05 percent (Default)
			$Section = "Rec Hub Table Maintenance";
			$SetupKey = "PurgeDeletePercentage";
			$KeyValue = ".05";    			# Load Default value back into System Table
			$Results = Update-SystemSetup $dbConnection $Section $SetupKey $KeyValue $LogFile; 
			$dbConnection.Close();
			Write-Host "Executing Update Complete";
		}	
}

Write-Log $LogFile "-------------------------------------------------------------------------------";
if( $dbConnection.State -eq [System.Data.ConnectionState]::Open )
{
	$dbConnection.Close();
}