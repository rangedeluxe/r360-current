﻿<#
	.SYNOPSIS
	PowerShell script used to exec the Dimensions 1.05.x to 2.01 converion package.
	
	.DESCRIPTION
	The script will create a XML document based on the input and call the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER SourceServer
	The name of the server hosting the OLTA 1.05.x database to be upgraded.

	.PARAMETER SourceDB
	The name of the OLTA 1.05.x database to be upgraded.

	.PARAMETER TargetServer
	The name of the server hosting the R360 2.01 database to receive the OLTA 1.05.x data.

	.PARAMETER TargetDB
	The name of the R360 2.01 database to receive the OLTA 1.05.x data.

	.PARAMETER SSISDirectory
	Folder where the PowerShell scripts are located.

#>
param
(
	[parameter(Mandatory = $true)][string] $SourceServer,
	[parameter(Mandatory = $true)][string] $SourceDB,
	[parameter(Mandatory = $true)][string] $TargetServer,
	[parameter(Mandatory = $true)][string] $TargetDB,
	[parameter(Mandatory = $true)][string] $SSISDirectory
)

$ScriptVerison = "2.01";

Write-Host "Execute OLTA 1.05.x to 2.01 Dimensions PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 03/23/15 JPB	2.01	WI 197248	Created.
#
################################################################################

#BuildXML document to be sent to execute PowerShellScript
$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISDirectory</PackageFolder>
	<PackageName>105x_201_ETLDimensions</PackageName>
	<Parameters>
		<Parameter Name=""SourceServerName"" Value=""$SourceServer""/>
		<Parameter Name=""SourceInitialCatalog"" Value=""$SourceDB""/>
		<Parameter Name=""TargetServerName"" Value=""$TargetServer""/>
		<Parameter Name=""TargetInitialCatalog"" Value=""$TargetDB""/>
	</Parameters>
</SSISPackage>"
);

$SSISFolder = $xml.SSISPackage.PackageFolder;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

$CurrentFolder = Get-Location;
Set-Location $SSISFolder;
.\ExecuteSSISPackage $xml;
Set-Location $CurrentFolder;