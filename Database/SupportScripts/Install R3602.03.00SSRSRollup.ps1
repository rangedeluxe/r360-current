
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"

#Installation Folder
$INSTALLROOT = "D:\WFSStaging\"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"

$SSRSFolder = "R360Reports"
$URL = "https://**YOUR URL**/ReportServer"


$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\InstallSSRS.ps1"



function RunInstaller([string] $DBServer, [string] $DBName, [string] $Release, [string] $InstallFolderPath, [string] $CommandFile)
{
	$InstallPath = [System.IO.Path]::Combine($INSTALLROOT,$Release,$InstallFolderPath);
	&"$Installer" -DBServer $DBServer -DBName $DBName -CommandFile $CommandFile -InstallationFolder $InstallPath -LogFolder $LOGFOLDER -SSRSFolder $SSRSFolder -URL $URL

}

Write-Host "**** Starting CDS RecHub Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;

RunInstaller $DBSERVER $R360DB "2.00" "R360H_2.00.00.00\Database" "RecHub2.00.00.00SSRS.xml";
Write-Host "**** R360 SSRS rollup complete for 2.00.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.00" "R360H_2.00.01.00\Database" "RecHub2.00.01.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.00.01.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.01" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "RecHub2.01.00.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.01.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.01" "R360_2.01.02.00_alpha2\DB\RecHub2.1\2.01.02.00" "RecHub2.01.02.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.01.02.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.02" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "R360Online2.02.00.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.02.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.02" "R360_2.02.03.00\DB\RecHub2.1\2.02.03.00" "R360Online2.02.03.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.02.03.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $R360DB "2.02" "R360_2.02.10.00\DB\RecHub2.1\2.02.10.00" "R360Online2.02.10.00SSRSPatch.xml";
Write-Host "**** R360 SSRS rollup complete for 2.02.10.00" -ForegroundColor GREEN;


Write-Host "**** R360 SSRS rollup to Version 2.03.00.00 has completed." -ForegroundColor GREEN;


