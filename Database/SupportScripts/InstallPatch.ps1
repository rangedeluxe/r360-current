﻿<#
	.SYNOPSIS
	PowerShell script used to install WFS database release scripts.
	
	.DESCRIPTION
	The script will process the contents of the supplied XML file, executing each script
	against the database server and database name supplied.
	
	.PARAMETER DBServer
	Name of the SQL Server where the patch will be installed.
	
	.PARAMETER DBName
	Name of the database where the patch will be installed.
	
	.PARAMETER CommandFile
	Name of the XML file containing the commands and file list used by WFSSI. (Will usually be something
	like integraPAY4.0.03.180.xml.)

	.PARAMETER DisplayDetails
	Displays the full SQLCMD command executed. 0 - False, 1 - True
	Default: False (0)
	
	.PARAMETER InstallationFolder
	Folder where the DB Scripts are located.
	Default: Folder where the script is executed from.

	.PARAMETER LogFolder
	Folder where the log folder will be created.
	Default: Folder where the script is executed from.
	
	.PARAMETER LoginID
	Login ID used to log into the SQL Server.
	
	.PARAMETER Partitioned
	Used to determine if the database is partitioned. (Used only for OLTA installations.)
	0 - False, 1 - True
	Default: True (1)
	
	.PARAMETER PartitionYears
	When Partitioned is true (1), this value determines the number of years partitions will be created for.
	Default: 0
	
	.PARAMETER PartitionType
	Determines the partitioning type.
	0 - No partititioning
	1 - Weekly partitioning
	2 - Monthly partitioning
	Defaults: 
		0 - When Partitioned is fales (0)
		2 - When Partitioned is true (1)
	
	.PARAMETER Password
	Password for the LoginID.

	.PARAMETER SQLServerVersion
	Used when the SQL Server version cannot be determined.
	Valid Values: 2000, 2005, 2008, 2012
	
	.PARAMETER UserName
	Name of user running scripts, enclosed in quotes.
	(Exampe: -UserName "JPB WFS")
	Default: Network ID of logged on user.

#>
param
(
	[parameter(Mandatory = $true)][string] $DBServer = "",
	[parameter(Mandatory = $true)][string] $DBName = "",
	[parameter(Mandatory = $true)][string] $CommandFile = "",
	[bool] $DisplayDetails = $false,
	[string] $InstallationFolder = "",
	[string] $LoginID = "",
	[string] $LogFile = "",
	[string] $LogFolder = "",
	[bool] $Partitioned = $true,
	[int] $PartitionYears = 0,
	[System.Nullable[datetime]] $PartitionStartDate,
	[int] $SQLServerVersion =$null,
	[string] $Password = "",
	[string] $UserName = "",
	[bool] $KeepSSISVersions = $false,
	[int] $PartitionType = -1
) 

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ManagedDTS") | Out-Null;

$ScriptVerison = "1.28";

Write-Host "Install Patch PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 11/22/09 JPB	1.0					Created.
# 12/02/09 JPB	1.1					Added support for script to get the version 
#										of the SQL Server.
# 12/03/09 JPB	1.2 				Fixed script file name to support spaces in 
#										folder name.
# 12/04/09 JPB  1.3 				Added command line options, etc to log file.
# 12/05/09 JPB 	1.4 				Added -TargetSQLServerVersion to be used 
#										when the SQL Server	version cannot be 
#										determined.
# 12/31/09 JPB	1.5					Corrected error handling from CMD call.
#									Set SQLCMD severity level to 10.
# 03/10/10 JPB	1.6					Display the log file name.
# 03/16/10 JPB 	1.7 				Displays results for each script on screen.
# 04/15/10 JPB	1.8 				Added more user feed back, get user confirmation, etc.
# 05/19/10 JPB	1.9 				Enhanced SQLServerVersion param to allow 
#										it to override actual version of database.
# 06/17/10 JPB 1.10 				Added Version number to SQLInstall Node so 
#										Systems table can be updated.
# 07/12/10 JPB 1.11					Fixed Log Path handling and fixed up paths.
# 07/13/10 JPB 1.12					Added partition support and PoSH style help.
# 07/16/10 JPB 1.13					Added DB Server and DB Name as standard vars 
#										to SQLCMD.
# 07/19/10 JPB 1.14					Script path is taken from CommandFile name by 
#										default.
#										Removed help info. Use PoSH style help now.
# 01/12/11 JPB 1.15 				Output partition state to console.
# 01/21/11 JPB 1.16					Create InstallScript Function and added 
#										PostInstall	processing functionality.
# 02/15/11 JPB 1.17					Add option to stop processing an XML document 
#										if there if an error.
# 02/21/11 JPB 1.18					Fixed path name when passing in the LogFolder.
# 09/30/11 JPB 1.19		CR 47244 	Reads static data from XML file.
# 02/17/12 JPB 1.20		CR 50389 	Rename static data results file to 
#										_StaticData_Results.
# 03/11/12 JPB 1.21		CR 51144 	Added ability to create databas, dynamic vars
#										from XML, other minor fixes.
# 03/20/12 JPB 1.22		CR 50398 	Added SSIS package install via DTUtil.
# 06/28/12 JPB 1.23		CR 53867	Add Partitioned to dynamic vars.
#									Fixed dynamic vars.
# 07/08/12 JPB 1.24		CR 54806	Added support to execute SSIS Packages.
# 08/06/12 JPB 1.25 	CR 53413 	Change how SSIS Folders checked/created.
#									Allows multiple versions of a package to be stored.
#									Allow dynamic vars per script.
#									Call SSIS for schema changes (fact tables)
# 11/29/12 JPB 1.26		WI 71212	Added WI support.
# 01/03/13 JPB 1.27		WI 70330	Added support for SSIS packages installed on instance.
# 03/01/13 JPB 1.28		WI 109352	Added PartitionType param.
#									Added PartitionYears to determine number of partition years to create.
#									Added PartitionStartDate to determine the first partition date.
#									Added schema name for static data SP call.
#									Result file written to same subfolder as script file.
#									Added support to determine if SQL is enterprise edition.
################################################################################


function GetCurrentDBVersion
{
	param
	(
		[String] $local:DBServer,
		[String] $local:DBName,
		[String] $local:ScriptName,
		[String] $local:ResultPath,
		[String] $local:SystemName,
		[Bool] $local:WindowsAuth,
		[Bool] $local:DisplayDetails,
		[REF] $local:DatabaseVersion
	)
	
	[String] $local:LogFileName;
	[bool] $local:SQLError | Out-Null;
	
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_StaticData_Results.txt";
	$local:SQLError = $false;
	
	#For now, only support Windows Auth
	$dbConnectionOptions = ("Data Source=$local:DBServer; Initial Catalog=$local:DBName;Integrated Security=SSPI")
	$dbConnection = New-Object System.Data.SqlClient.SqlConnection($dbConnectionOptions);
	$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$dbCommand.Connection = $dbConnection;
	$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$dbCommand.CommandType = [System.Data.CommandType]'Text';
	$dbCommand.CommandText = "SELECT RTRIM(LTRIM(SystemVersion)) FROM dbo.Systems WHERE SystemName = '$local:SystemName'";
	
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		$event.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$local:SQLError = $true;
		}
	};
	$dbConnection.add_InfoMessage($handler);
	$dbConnection.FireInfoMessageEventOnUserErrors = $true;

	try
	{
		$Results = $dbConnection.Open();
	}
	catch
	{
			"Error Opening Database Connection" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	}

	try
	{
		$DBReader = $dbCommand.ExecuteReader();
		
		while( $DBReader.Read() )
		{
			$local:DatabaseVersion.value = $DBReader[0];
		}
		
		$Results = $true;
		
#		$Results = $dbCommand.ExecuteQuery();
#
#		if( $Results -eq -1 -and $local:SQLError -eq $false )
#		{
#			$Results = $true;
#		}
#		else
#		{
#			$Results = $false;
#		}
	}
	catch
	{
		if( $dbConnection.State -eq "Open" )
		{
			$dbConnection.Close();
		}
		$Results = $false;
		"Error executing SQL Command" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;

  	}
	if( $dbConnection.State -eq "Open" )
	{
		$dbConnection.Close();
	}
	return $Results;
}

################################################################################
# This function installs the script on the DBServer/DBName and logs the results.
################################################################################
function InstallScript
{
	param
	(
		[String] $local:DBServer,
		[String] $local:DBName,
		[String] $local:ScriptName,
		[String] $local:ResultPath,
		[Bool] $local:WindowsAuth,
		[Bool] $local:DisplayDetails
	)
	
	[String] $local:LogFileName;
	
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $local:DBServer ");
	[Void]$CommandLine.Append("-d $local:DBName ");
	
	if( $local:WindowsAuth )
	{
		$LoginInfo = "-E";
	}
	else 
	{
		$LoginInfo = "-U $LoginID -P $Password";
	}

	[Void]$CommandLine.Append("$LoginInfo ");
	
	#we are going to set these as a standard.
	[Void]$CommandLine.Append("-v DBServer=""$local:DBServer"" ");
	[Void]$CommandLine.Append("-v DBName=""$local:DBName"" ");

	[Void]$CommandLine.Append("-i ");
	[Void]$CommandLine.Append("""$local:ScriptName""");
	[Void]$CommandLine.Append(" ");	
	[Void]$CommandLine.Append("-r 1 -V 10 ");
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_" + "Results.txt";
	
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$local:LogFileName""");
	
	if( $local:DisplayDetails -eq $true )
	{
		Write-Host $CommandLine;
		Write-Host "";
	}

	cmd /c $CommandLine;
	return $?;
}

################################################################################
# This function processes a static data XML on the DBServer/DBName and logs the results.
################################################################################
function InstallStaticData
{
	param
	(
		[String] $local:DBServer,
		[String] $local:DBName,
		[String] $local:ScriptName,
		[String] $local:ResultPath,
		[Bool] $local:WindowsAuth,
		[Bool] $local:DisplayDetails,
		[String] $local:InstallVersionNumber
	)
	
	[String] $local:LogFileName;
	[bool] $local:SQLError;

	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_StaticData_Results.txt";
	$local:SQLError = $false;
	
	$xmlDoc = Get-Content $ScriptName;
	
	if( $xmlDoc.Lenght -eq 0 )
	{
		"Could not load XML Static Data File" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		return;
	}
	$local:xml = New-Object "System.Xml.XmlDocument";
	$local:xml.load($ScriptName);

	$local:SchemaName = $local:xml.StaticData.LoaderSchema;
	
	#For now, only support Windows Auth
	$dbConnectionOptions = ("Data Source=$local:DBServer; Initial Catalog=$local:DBName;Integrated Security=SSPI")
	$dbConnection = New-Object System.Data.SqlClient.SqlConnection($dbConnectionOptions);
	$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$dbCommand.Connection = $dbConnection;
	$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$dbCommand.CommandType = [System.Data.CommandType]'StoredProcedure';
	$dbCommand.CommandText = $local:SchemaName + ".usp_WFS_Process_StaticData";
#	$dbCommand.CommandText = "dbo.usp_WFS_Process_StaticData";
	$dbCommand.Parameters.AddWithValue("@parmStaticData", "$xmlDoc") | Out-Null;
	$dbCommand.Parameters.AddWithValue("@parmSystemVersion", "$local:InstallVersionNumber") | Out-Null;

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		$event.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$local:SQLError = $true;
		}
	};
	$dbConnection.add_InfoMessage($handler);
	$dbConnection.FireInfoMessageEventOnUserErrors = $true;

	try
	{
		$Results = $dbConnection.Open();
	}
	catch
	{
			"Error Opening Database Connection" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	}
	
	try
	{
		$Results = $dbCommand.ExecuteNonQuery();

		if( $Results -eq -1 -and $local:SQLError -eq $false )
		{
			$Results = $true;
		}
		else
		{
			$Results = $false;
		}
	}
	catch
	{
			$Results = $false;
			"Error executing SQL Command" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;

  	}
	$dbConnection.Close();
	return $Results;
}

################################################################################
# This function creats a new database on DBServer and logs the results.
################################################################################
function CreateDatabase
{
	param
	(
		[String] $local:DBServer,
		[String] $local:ScriptName,
		[String] $local:ResultPath,
		[Bool] $local:WindowsAuth,
		[Bool] $local:DisplayDetails
	)
	[String] $local:LogFileName;
	
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $local:DBServer ");
	[Void]$CommandLine.Append("-d MASTER ");
	
	if( $local:WindowsAuth )
	{
		$LoginInfo = "-E";
	}
	else 
	{
		$LoginInfo = "-U $LoginID -P $Password";
	}

	[Void]$CommandLine.Append("$LoginInfo ");
	
	#we are going to set these as a standard.
	[Void]$CommandLine.Append("-v DBServer=""$local:DBServer"" ");
	[Void]$CommandLine.Append("-v DBName=""MASTER"" ");

	[Void]$CommandLine.Append("-i ");
	[Void]$CommandLine.Append("""$local:ScriptName""");
	[Void]$CommandLine.Append(" ");	
	[Void]$CommandLine.Append("-r 1 -V 10 ");
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_" + "Results.txt";
	
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$local:LogFileName""");
	
	if( $local:DisplayDetails -eq $true )
	{
		Write-Host $CommandLine;
		Write-Host "";
	}

	cmd /c $CommandLine;
	return $?;
}

################################################################################
# Create WFSPackages Package Path.
# #adapted from http://billfellows.blogspot.com/2011_08_01_archive.html
################################################################################
function CreateWFSFolderOnSqlServer
{
	param
	(
		[String] $local:DBServer,
		[String] $local:FolderName,
		[String] $local:LogFileName
	)
	$FolderDelimter = "\";
	try
	{
		# if they supply multiple-level of folders, might need to create whole structure
		if( $Folder -ne $folderDelimter )
		{
		    $folderList = $local:FolderName.Split($folderDelimter);
		}
		else
		{
		    $folderList = [array]$local:FolderName;
		}
		$app = New-Object Microsoft.SqlServer.Dts.Runtime.Application;
		$parentFolder = "\";
		foreach( $subFolder in $folderList )
		{
		    # test for existing folders
		    $testFolder = $parentFolder + $folderDelimter + $subFolder;
		    if( !$app.FolderExistsOnSqlServer($testFolder,$local:DBServer,$null,$null) )
		    {
				[string]::Format("Creating folder {0} on server {1}", $testFolder, $local:DBServer) | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
				if( $subFolder.IndexOf(".") -gt 0 )
				{
					$app.CreateFolderOnSqlServer($parentFolder,$subFolder.Replace(".",""),$local:DBServer,$null,$null);
					$app.RenameFolderonSqlServer($parentFolder,$subFolder.Replace(".",""),$subFolder,$local:DBServer,$null,$null);
				}
				else
				{
					$app.CreateFolderOnSqlServer($parentFolder,$subFolder,$local:DBServer,$null,$null);
				}
		    }
		    $parentFolder += $folderDelimter + $subFolder;
		}
		$Results = $true;
	}
	catch 
	{
		[string]::Format("Failed to create folder {0} on server {1}", $testFolder, $local:DBServer) | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}

	return $Results;
}

################################################################################
# Exec a DTUtil command.
################################################################################
function ExecDTUtilCommand([System.Text.StringBuilder]$local:DTUtilCommand,[String] $local:CommandType, [String] $local:LogFileName)
{
	Invoke-Expression -Command $local:DTUtilCommand;
	if ($LASTEXITCODE -eq 0) 
	{
		"Successfully executed "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $true;
	}
	elseif( $LASTEXITCODE -eq 1 )
	{
		"Fail executing "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 4 )
	{
		$local:CommandType+" could not locate packagae" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 5 )
	{
		$local:CommandType+" cannot load package" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	elseif( $LASTEXITCODE -eq 6 )
	{
		"Syntax error executing "+$local:CommandType | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# Install SSIS Package.
################################################################################
function InstallSSISPackage
{
    param
    (
		[String]$local:DBServer,
		[String]$local:PathToDtsx,
		[String]$local:SSISPackageFQFP,
		[String]$local:FolderName = "\\",
		[String]$local:ResultPath
    )
	
	$Results = $true;
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:SSISPackageFQFP) + "_" + "Results.txt";

	$local:PackageName = [System.IO.Path]::GetFileNameWithoutExtension($local:SSISPackageFQFP);

	#Write-Host "...Installing SSIS Package" $local:PackageName -NoNewline;
	"Installing SSIS Package "+$local:FolderName+"\"+$local:PackageName | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	$Events = $null;

	#WI 70330
	if( $local:DBServer.Contains("\") )
	{
		$local:DBServer = $local:DBServer.Substring(0,$local:DBServer.IndexOf("\"));
	}
	#WI 70330
	
	if( CreateWFSFolderOnSqlServer $local:DBServer $local:FolderName $local:LogFileName )
	{
		$Results = $true;
		$app = New-Object Microsoft.SqlServer.Dts.Runtime.Application;
		$Package = $app.LoadPackage($local:PathToDtsx, $null);
		$PackageName = $local:FolderName+"\"+$local:PackageName;
		
		$app.SaveToSQLServerAs($Package,$Events,$PackageName,$local:DBServer,$null,$null);
	}
	else
	{
		$Results = $false;
	}
	return $Results;
}	

################################################################################
# Run SSIS Package.
################################################################################
function RunSSISPackage
{
	param
	(
		[String] $local:DBServer,
		[String] $local:DBName,
		[String] $local:SSISPackageFQFP,
		[String] $local:ResultPath,
		[Bool] $local:DisplayDetails		
	)
	
	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:SSISPackageFQFP) + "_" + "Results.txt";
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("dtexec /FILE ");
	[Void]$CommandLine.Append("'"+$local:SSISPackageFQFP+"'");
	[Void]$CommandLine.Append(" ""/SET ""\Package.Variables[User::TargetServerName].Properties[Value]"";");
	[Void]$CommandLine.Append("$local:DBServer""");
	[Void]$CommandLine.Append(" ""/SET ""\Package.Variables[User::TargetDatabaseName].Properties[Value]"";");
	[Void]$CommandLine.Append("$local:DBName""");
	[Void]$CommandLine.Append(" ""/SET ""\Package.Variables[User::LogPath].Properties[Value]"";");
	[Void]$CommandLine.Append("$local:ResultPath""");
	$Results = ExecDTUtilCommand $CommandLine "DTExec" $local:LogFileName;
	if( $Results )
	{
		$CommandLine.ToString() | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		if((Test-Path ($local:ResultPath+"*.err")) -eq $true )  
		{
			$Results = $false;
		}
	}
	return $Results;
}


################################################################################
# This function processes a static data XML on the DBServer/DBName and logs the results.
################################################################################
function RunCDCScript
{
	param
	(
		[String] $local:DBServer,
		[String] $local:DBName,
		[String] $local:ScriptName,
		[String] $local:ResultPath,
		[Bool] $local:WindowsAuth,
		[Bool] $local:DisplayDetails,
		[String] $local:InstallVersionNumber
	)
	
	[String] $local:LogFileName;
	[bool] $local:SQLError;

	$local:LogFileName = $local:ResultPath + [System.IO.Path]::GetFileNameWithoutExtension($local:ScriptName) + "_CDC_Results.txt";
	$local:SQLError = $false;
	
	$xmlDoc = Get-Content $ScriptName;
	
	if( $xmlDoc.Lenght -eq 0 )
	{
		"Could not load CDC XML Data File" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		return;
	}
	$local:xml = New-Object "System.Xml.XmlDocument";
	$local:xml.load($ScriptName);

	$local:SchemaName = $local:xml.CDCDefinition.LoaderSchema;
	
	#For now, only support Windows Auth
	$dbConnectionOptions = ("Data Source=$local:DBServer; Initial Catalog=$local:DBName;Integrated Security=SSPI")
	$dbConnection = New-Object System.Data.SqlClient.SqlConnection($dbConnectionOptions);
	$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$dbCommand.Connection = $dbConnection;
	$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$dbCommand.CommandType = [System.Data.CommandType]'StoredProcedure';
	$dbCommand.CommandText = $local:SchemaName + ".usp_WFS_Process_CDCSetup";
	$dbCommand.Parameters.AddWithValue("@parmCDCDefinition", "$xmlDoc") | Out-Null;
	$dbCommand.Parameters.AddWithValue("@parmSystemVersion", "$local:InstallVersionNumber") | Out-Null;

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		$event.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$local:SQLError = $true;
		}
	};
	$dbConnection.add_InfoMessage($handler);
	$dbConnection.FireInfoMessageEventOnUserErrors = $true;

	try
	{
		$Results = $dbConnection.Open();
	}
	catch
	{
			"Error Opening Database Connection" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	}
	
	try
	{
		$Results = $dbCommand.ExecuteNonQuery();

		if( $Results -eq -1 -and $local:SQLError -eq $false )
		{
			$Results = $true;
		}
		else
		{
			$Results = $false;
		}
	}
	catch
	{
			$Results = $false;
			"Error executing SQL Command" | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
			$_.Exception.Message | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;

  	}
	$dbConnection.Close();
	return $Results;
}

################################################################################
# Main.
################################################################################

$UpdateVersionNumber = $false;
$WindowsAuth = $true;
[int]$DBSQLVersion | Out-Null;
[String] $DatabaseVersion="" | Out-Null;
[bool] $EnterpriseEdition=$false; # | Out-Null;


if( $LoginID.Length -gt 0 -or $Password.Length -gt 0 )
{
	if( $LoginID.Length -eq 0 )
	{
		Write-Host "Password defined, must include Login ID";
		HelpInfo;
		break;
	}
	if( $Password.Length -eq 0 )
	{
		Write-Host "Login ID defined, must include Password";
		HelpInfo;
		break;
	}
	$WindowsAuth = $false;
}

if( $LogFile.Length -eq 0 )
{
	$LogFile += [system.io.path]::GetFilenameWithoutExtension($CommandFile) + "_Results.txt";
}

if( $UserName.Length -eq 0 )
{
	$UserName = (gwmi -class win32_computerSystem).UserName;
}

$Server = new-object ('Microsoft.SqlServer.Management.Smo.Server') "$DBServer";
if( $SQLServerVersion -eq $null -or $SQLServerVersion -eq 0 )
{
	$DBSQLVersion = $Server.Version.Major;
}
else
{
	if( $SQLServerVersion -eq 2000 )
	{
		$DBSQLVersion = 8;
	}
	elseif($SQLServerVersion -eq 2005 )
	{
		$DBSQLVersion = 9;
	}
	elseif( $SQLServerVersion -eq 2008 )
	{
		$DBSQLVersion = 10;
	}
}

if( $Server.EngineEdition.ToString().Contains("Enterprise") )
{
	
	$EnterpriseEdition = $true;
}

if( $DBSQLVersion -eq $null -and ($SQLServerVersion -ne 2000 -and $SQLServerVersion -ne 2005 -and $SQLServerVersion -ne 2008) )
{
	Write-Host "Cound not determine SQL Server Version for "$DBServer;
	Write-Host "Restart and include -SQLServerVersion [2000,2005,2008]";
	break;
}

if( $DBSQLVersion -eq $null -and $SQLServerVersion -eq 2000 )
{
	$DBSQLVersion = 8;
}
elseif( $DBSQLVersion -eq $null -and $SQLServerVersion -eq 2005 )
{
	$DBSQLVersion = 9;
}
elseif( $DBSQLVersion -eq $null -and $SQLServerVersion -eq 2008 )
{
	$DBSQLVersion = 10;
}

#define paritition env vars to we can process OTLA scripts as well.
if( !$Partitioned )
{
	$env:PartitionType="0";
	$env:OnPartition=" ";
}
else
{
	if( $PartitionType -eq -1 )
	{
		$env:PartitionType="2";
	}
	else
	{
		$env:PartitionType="$PartitionType";
	}
	$env:OnPartition="ON OLTAFacts_Partition_Scheme(DepositDateKey)";
}
$env:PartitionYears="$PartitionYears";
if( !$PartitionStartDate )
{
	$PartitionStartDate = Get-Date;
}
$env:PartitionStartDate=$PartitionStartDate.ToShortDateString();

Write-Host "Running as user        "$UserName;
Write-Host "Database Server        "$DBServer;
Write-Host "Database Name          "$DBName;

if( $DBSQLVersion -eq 8 )
{
	Write-Host "DB Server SQL Version   2000";
}
elseif( $DBSQLVersion -eq 9 )
{
	Write-Host "DB Server SQL Version   2005";
}
elseif( $DBSQLVersion -eq 10 )
{
	Write-Host "DB Server SQL Version   2008";
}

if( [System.IO.Path]::GetExtension($CommandFile).Length -eq 0 )
{
	$CommandFile += ".XML";
}

Write-Host "Command File           "$CommandFile;
Write-Host "Log File               "$LogFile;
Write-Host "Windows Authentication "$WindowsAuth;
Write-Host "Partitioned            "$Partitioned;
if( $Partitioned )
{
	Write-Host "Partitioned Start Date "$PartitionStartDate.ToShortDateString();
}

[string] $ScriptPath | Out-Null;

if( $InstallationFolder.Length -gt 0 )
{
	if((Test-Path $InstallationFolder) -ne $true )
	{
		Write-Host "Installation Folder "$InstallationFolder" not found. Aborting Installation."
		break;
	}
	$ScriptPath = $InstallationFolder;
}
else
{
	$ScriptPath = [System.IO.Path]::GetDirectoryName($CommandFile);
	if( $ScriptPath.Length -eq 0 -or $ScriptPath -eq "\" -or $ResultPath -eq "" -or $ResultPath -eq $null )
	{
		$ScriptPath = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
	}
}

if( !$ScriptPath.EndsWith("\") )
{
	$ScriptPath += "\";
}

#Setup folders
if( $LogFolder.Length -eq 0 )
{
	$ResultPath = $ScriptPath + "Results" + "_" + (Get-Date –f yyyyMMddHHmmss)+ "\";
}
else
{
	if( !$LogFolder.EndsWith("\") )
	{
		$LogFolder += "\";
	}
	
	$ResultPath = $LogFolder + "Results" + "_" + (Get-Date –f yyyyMMddHHmmss)+ "\";
	if( !$ResultPath.EndsWith("\") )
	{
		$ResultPath += "\";
	}
}
$LogPath = $ResultPath;

#Check for the results folder
if ((Test-Path -path $ResultPath) -ne $true)
{
	#Write-Host "Creating Results Folder";
	New-Item $ResultPath -type directory | Out-Null;
}

Write-Host "Script Path           "$ScriptPath;
Write-Host "Results Path          "$ResultPath;
Write-Host;

$LogFile = $LogPath + $LogFile;
$FQP = $ScriptPath+[system.io.path]::GetFileName($CommandFile);

#cleanup path names
IF( $LogFile.Contains(" ") )
{
	$LogFile = """" + $LogFile + """";
}

IF( $FQP.Contains(" ") )
{
	$FQP = """" + $FQP + """";
}


Write-Host "Processing command file "$FQP;

"Install Patch PowerShell Script Version " + $ScriptVerison | Out-File -FilePath $LogFile -Encoding unicode -Append;
"-------------------------------------------------------------------------------" | Out-File -FilePath $LogFile -Encoding unicode -Append;
if( $DBSQLVersion -eq 8 )
{
	"DB Server SQL Version 2000" | Out-File -FilePath $LogFile -Encoding unicode -Append;
}
elseif( $DBSQLVersion -eq 9 )
{
	"DB Server SQL Version 2005"  | Out-File -FilePath $LogFile -Encoding unicode -Append;
}
elseif( $DBSQLVersion -eq 10 )
{
	"DB Server SQL Version 2008"  | Out-File -FilePath $LogFile -Encoding unicode -Append;
}

"Running as user       " + $UserName | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Database Server       " + $DBServer | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Database Name         " + $DBName | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Command File          " + $CommandFile | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Windows Authenticaion " + $WindowsAuth | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Partitioned           " + $Partitioned | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Script Path           " + $ScriptPath | Out-File -FilePath $LogFile -Encoding unicode -Append;
"Results Path          " + $ResultPath | Out-File -FilePath $LogFile -Encoding unicode -Append;
"-------------------------------------------------------------------------------" | Out-File -FilePath $LogFile -Encoding unicode -Append;

#Make sure we can access the XML file
if ((Test-Path $FQP) -eq $false )
{
	Write-Host "Could not access command file "$CommandFile". Aborting installation.....";
	break;
}


$bEnviromentVariables = $false;

$xml = New-Object "System.Xml.XmlDocument";
$xml.load($FQP);
for( $i=0;$i -lt $xml.SQLInstall.ChildNodes.Count;$i++ )
{
	if( $xml.SQLInstall.ChildNodes.Item($i).Name.Contains("EnviromentVariables") )
	{
		foreach( $EnviromentVariable in $xml.SQLInstall.EnviromentVariables.EnviromentVariable )
		{
			if( $EnviromentVariable.Value -eq "DBNAME" )
			{
				$EnvVar = "env:"+$EnviromentVariable.InnerText;
				if( (Test-Path -Path $EnvVar) -eq $true )
				{
					Remove-Item $EnvVar;
				}
				New-Item -Path env: -Name $EnviromentVariable.InnerText -Value $DBName | Out-Null;
			}
			else
			{
				$EnvVar = "env:"+$EnviromentVariable.Value;
				if( $EnviromentVariable.GetAttribute("Partitioned") )
				{
					if( $EnviromentVariable.GetAttribute("Partitioned") -eq 0 -and $Partitioned -eq $false )
					{
						if( (Test-Path -Path $EnvVar) -eq $true )
						{
							Remove-Item $EnvVar;
						}
						if( $EnviromentVariable.InnerText.Length -eq 0 -or $EnviromentVariable.InnerText.Length -eq $null )
						{
							New-Item -Path env: -Name $EnviromentVariable.Value -Value " " | Out-Null;
						}
						else
						{
							New-Item -Path env: -Name $EnviromentVariable.Value -Value $EnviromentVariable.InnerText | Out-Null;
						}
					}
					elseif ( $EnviromentVariable.GetAttribute("Partitioned") -eq 1 -and $Partitioned -eq $true )
					{
						if( (Test-Path -Path $EnvVar) -eq $true )
						{
							Remove-Item $EnvVar;
						}
						New-Item -Path env: -Name $EnviromentVariable.Value -Value $EnviromentVariable.InnerText | Out-Null;
					}
				}
				else
				{
					if( (Test-Path -Path $EnvVar) -eq $true )
					{
						Remove-Item $EnvVar;
					}
					New-Item -Path env: -Name $EnviromentVariable.Value -Value $EnviromentVariable.InnerText | Out-Null;
				}
			}
			$bEnviromentVariables = $true;
		}
	}
}
##process the file line by line
foreach( $ScriptFile in $xml.SQLInstall.ProcessFile )
{
	if( $ScriptFile.GetAttribute("CR") )
	{
		$IssueID = $ScriptFile.CR;
		$IssueType = "CR";
	}
	if( $ScriptFile.GetAttribute("WI") )
	{
		$IssueID = $ScriptFile.WI;
		$IssueType = "WI";
	}
	#Process SQL Server version specific scripts
	if( $ScriptFile.GetAttribute("SQLVersion") )
	{
		if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2000") -and ($DBSQLVersion -ne 8))
		{
			##skip this script because it is not meant for SQL Server 2000
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
			continue;
		}
		if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2005") -and ($DBSQLVersion -ne 9))
		{
			##skip this script because it is not meant for SQL Server 2005
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
			continue;
		}
		if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2008") -and ($DBSQLVersion -ne 10))
		{
			##skip this script because it is not meant for SQL Server 2008
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
			continue;
		}
	}
	if( $ScriptFile.GetAttribute("SQLEdition") )
	{
		if( ($ScriptFile.GetAttribute("SQLEdition") -eq "Enterprise") -and (!$EnterpriseEdition) )
		{ 
			##skip this script because it requires enterprise edition
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it enterprise edition of SQL Server." | Out-File -FilePath $LogFile -Encoding unicode -Append;
			continue;
		}
	}
	$ContinueOnError = $true;
	if( $ScriptFile.GetAttribute("ContinueOnError") )
	{
		if( $ScriptFile.GetAttribute("ContinueOnError") -eq "FALSE" )
		{
			$ContinueOnError = $false;
		}
	}
	
	Write-Host "Processing "$IssueType"(s): "$IssueID -NoNewline;

	if( $ScriptFile.ChildNodes.Count -gt 1 )
	{
		for( $i=0;$i -lt $ScriptFile.ChildNodes.Count;$i++ )
		{
			if( $ScriptFile.ChildNodes.Item($i).Name.Contains("EnviromentVariables") )
			{ #Enviroment Variables for the script.
				foreach( $EnviromentVariable in $ScriptFile.EnviromentVariables.EnviromentVariable )
				{
					$ScriptEnviromentVariableValue = $EnviromentVariable.InnerText;
					if( $EnviromentVariable.VariableName -eq "SSISJobName" -and $KeepSSISVersions )
					{
						$ScriptEnviromentVariableValue = $ScriptEnviromentVariableValue+"_"+$ScriptFile.Version;
					}
					if( $EnviromentVariable.VariableName -eq "SSISPackagePath" -and $KeepSSISVersions )
					{
						$ScriptEnviromentVariableValue = $ScriptEnviromentVariableValue+"\"+$ScriptFile.Version;
					}
					New-Item -Path env: -Name $EnviromentVariable.VariableName -Value $ScriptEnviromentVariableValue | Out-Null;
				}
			}
			if( $ScriptFile.ChildNodes.Item($i).Name.Contains("SchemaModification") )
			{ #Schema Modification SSIS package
				if( $xml.SQLInstall.GetAttribute("SystemName") -ne $null )
				{
					$SystemName = $xml.SQLInstall.GetAttribute("SystemName");
				}
				else
				{
					$SystemName = "CheckBOX";
				}			
				GetCurrentDBVersion $DBServer $DBName $ScriptFile.LastChild.InnerText.Trim() $ResultPath $SystemName $WindowsAuth $DisplayDetails ([REF]$DatabaseVersion);
				foreach( $Version in $ScriptFile.SchemaModification.Version )
				{
					if( !$Version.SourceVersion.CompareTo($DatabaseVersion) )
					{
						$ScriptFile.LastChild.InnerText = $Version.SchemaPackage;
					}
				}
			}
		}
		$ScriptFQP = $ScriptPath+$ScriptFile.LastChild.InnerText.Trim();
	}
	else
	{
		$ScriptFileFolderName = split-path $ScriptFile.get_InnerText() -Parent
		if( $ScriptFileFolderName -ne $null )
		{
			if( !$ScriptFileFolderName.EndsWith("\") )
			{
				$ScriptFileFolderName += "\";
			}
			if ((Test-Path -path ($ResultPath+$ScriptFileFolderName)) -ne $true)
			{
				New-Item ($ResultPath+$ScriptFileFolderName) -type directory | Out-Null;
			}
		}
		$ScriptFQP = $ScriptPath+$ScriptFile.get_InnerText();
	}

	if( $ScriptFile.GetAttribute("StaticData") -eq "1" )
	{
		$CommandResult = InstallStaticData $DBServer $DBName $ScriptFQP ($ResultPath+$ScriptFileFolderName) $WindowsAuth $DisplayDetails $xml.SQLInstall.GetAttribute("Version");
	}
	elseif( $ScriptFile.GetAttribute("CreateDatabase") -eq "1" )
	{
		$CommandResult = CreateDatabase $DBServer $ScriptFQP $ResultPath $WindowsAuth $DisplayDetails;
	}
	elseif( $ScriptFile.GetAttribute("SSISPackage") -eq "1" )
	{
		if( $ScriptFile.GetAttribute("FolderName") )
		{
		    $FolderName = $ScriptFile.GetAttribute("FolderName");
		}
		if( $ScriptFile.GetAttribute("Version") )
		{
		    $SSISVersion = $ScriptFile.GetAttribute("Version");
		}
		else
		{
		    $SSISVersion = $null;
		}

		if( $KeepSSISVersions -and $SSISVersion -ne $null)
		{
			$SSISFolderName = "WFSPackages\" + $FolderName + "\" + $SSISVersion;
		}
		else
		{
			$SSISFolderName = "WFSPackages\" + $FolderName;
		}
		$CommandResult = InstallSSISPackage $DBServer $ScriptFQP $ScriptFile.get_InnerText() $SSISFolderName $ResultPath;
	}
	elseif( $ScriptFile.GetAttribute("CDC") -eq "1" )
	{
		$CommandResult = RunCDCScript $DBServer $DBName $ScriptFQP ($ResultPath+$ScriptFileFolderName) $WindowsAuth $DisplayDetails $xml.SQLInstall.GetAttribute("Version");		
	}
	elseif( [System.IO.Path]::GetExtension($ScriptFQP).ToUpper() -eq '.DTSX' )
	{
		$CommandResult = RunSSISPackage $DBServer $DBName $ScriptFQP $ResultPath $DisplayDetails;
	}
	else
	{
		$CommandResult = InstallScript $DBServer $DBName $ScriptFQP ($ResultPath+$ScriptFileFolderName) $WindowsAuth $DisplayDetails;
	}

	if( $ScriptFile.HasChildNodes )
	{
		for( $i=0;$i -lt $ScriptFile.ChildNodes.Count;$i++ )
		{
			if( $ScriptFile.ChildNodes.Item($i).Name.Contains("EnviromentVariables") )
			{
				foreach( $ScriptEnviromentVariable in $ScriptFile.EnviromentVariables.EnviromentVariable )
				{
					$EnvVar = "env:"+$ScriptEnviromentVariable.VariableName;
					Remove-Item $EnvVar;
				}
			}
		}
	}

	if( $CommandResult -eq $true )
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Successfully processed " + $IssueType + "(s) " + $IssueID + ", Script File " + [System.IO.Path]::GetFileName($ScriptFQP) | Out-File -FilePath $LogFile -Encoding unicode -Append;
		Write-Host "...Successful";
		$UpdateVersionNumber = $true;
	}
	else
	{
#		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Error processing " + $IssueType + "(s) " + $IssueID + ", Script File " + [System.IO.Path]::GetFileName($ScriptFQP) | Out-File -FilePath $LogFile -Encoding unicode -Append;
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Error processing " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() | Out-File -FilePath $LogFile -Encoding unicode -Append;
		Write-Host "...Error processing file"$ScriptFile.get_InnerText() -ForegroundColor Red;
		if( $ContinueOnError -eq $false )
		{
			Write-Host "ContinueOnError set to false ..... aborting install." -ForegroundColor Red;
			break;
		}
	}
}

if( $UpdateVersionNumber -eq $true -and $xml.SQLInstall.GetAttribute("Version").Length -ne 0 )
{
	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $DBServer ");
	[Void]$CommandLine.Append("-d $DBName ");
	
	if( $WindowsAuth )
	{
		$LoginInfo = "-E";
	}
	else 
	{
		$LoginInfo = "-U $LoginID -P $Password";
	}

	$Version = $xml.SQLInstall.GetAttribute("Version");
	if( $xml.SQLInstall.GetAttribute("SystemName") -ne $null )
	{
		$SystemName = $xml.SQLInstall.GetAttribute("SystemName");
	}
	else
	{
		$SystemName = "CheckBOX";
	}
	$LogFileName = $ResultPath + "VersionUpdate_Results.txt";
##Fix update statement
	[Void]$CommandLine.Append("$LoginInfo ");
	[Void]$CommandLine.Append("-Q ");
	[Void]$CommandLine.Append("""IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'RecHubConfig' AND TABLE_NAME = 'Systems') BEGIN IF(SELECT 1 FROM RecHubConfig.Systems WHERE SystemName = '$SystemName') = 1 UPDATE RecHubConfig.Systems SET SystemVersion = '$Version',DateInstalled = GETDATE() WHERE SystemName = '$SystemName' ELSE INSERT INTO RecHubConfig.Systems (SystemName,DateInstalled,SystemVersion) VALUES ('$SystemName',GETDATE(),'$Version') END ELSE IF(SELECT 1 FROM dbo.Systems WHERE SystemName = '$SystemName') = 1 UPDATE dbo.Systems SET SystemVersion = '$Version',DateInstalled = GETDATE() WHERE SystemName = '$SystemName' ELSE INSERT INTO dbo.Systems (SystemName,DateInstalled,SystemVersion) VALUES ('$SystemName',GETDATE(),'$Version')""");
	[Void]$CommandLine.Append(" ");	
	[Void]$CommandLine.Append("-r 1 -V 10 ");
	
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$LogFileName""");
	Write-Host "Updating Database Version information" -NoNewline;
	
#	Invoke-Expression $CommandLine;
	cmd /c $CommandLine;
	$CommandResult = $?
	if( $CommandResult -eq $true )
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Successfully processed Version Information" | Out-File -FilePath $LogFile -Encoding unicode -Append;
		Write-Host "...Successful";
	}
	else
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Error processingVersion Information" | Out-File -FilePath $LogFile -Encoding unicode -Append;
		Write-Host "...Error";
	}
}

#Run any post install scripts
if( $xml.SQLInstall.PostInstall -ne $null )
{
	foreach( $ScriptFile in $xml.SQLInstall.PostInstall.ProcessFile )
	{
		#Process SQL Server version specific scripts
		if( $ScriptFile.GetAttribute("SQLVersion") )
		{
			if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2000") -and ($DBSQLVersion -ne 8))
			{
				##skip this script because it is not meant for SQL Server 2000
				(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
				continue;
			}
			if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2005") -and ($DBSQLVersion -ne 9))
			{
				##skip this script because it is not meant for SQL Server 2005
				(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
				continue;
			}
			if( ($ScriptFile.GetAttribute("SQLVersion") -eq "2008") -and ($DBSQLVersion -ne 10))
			{
				##skip this script because it is not meant for SQL Server 2008
				(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Skipped " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() + " because it is for SQL Server " + $ScriptFile.GetAttribute("SQLVersion") | Out-File -FilePath $LogFile -Encoding unicode -Append;
				continue;
			}
		}
		Write-Host "Processing " + $IssueType + "(s): "$IssueID -NoNewline;

		$ScriptFQP = $ScriptPath+$ScriptFile.get_InnerText();
		
		$CommandResult = InstallScript $DBServer $DBName $ScriptFQP $ResultPath $WindowsAuth $DisplayDetails
		
		if( $CommandResult -eq $true )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Successfully processed " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() | Out-File -FilePath $LogFile -Encoding unicode -Append;
			Write-Host "...Successful";
			$UpdateVersionNumber = $true;
		}
		else
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " Error processing " + $IssueType + "(s) " + $IssueID + ", Script File " + $ScriptFile.get_InnerText() | Out-File -FilePath $LogFile -Encoding unicode -Append;
			Write-Host "...Error" -ForegroundColor Red;
		}
	}
}
#remove the environment vars, not needed any more
if( $bEnviromentVariables )
{
	foreach( $EnviromentVariable in $xml.SQLInstall.EnviromentVariables.EnviromentVariable )
	{
		if( $EnviromentVariable.Value -eq "DBNAME" )
		{
			$EnvVar = "env:"+$EnviromentVariable.InnerText;
		}
		else
		{
			$EnvVar = "env:"+$EnviromentVariable.Value;
		}
		if( $EnviromentVariable.GetAttribute("Partitioned") )
		{
			if( $EnviromentVariable.GetAttribute("Partitioned") -eq 0 -and $Partitioned -eq $false )
			{
				Remove-Item -LiteralPath $EnvVar;
			}
			elseif ( $EnviromentVariable.GetAttribute("Partitioned") -eq 1 -and $Partitioned -eq $true )
			{
				Remove-Item -LiteralPath $EnvVar;
			}
		}
		else
		{
			Remove-Item -LiteralPath $EnvVar;
		}
	}
}
Remove-Item Env:PartitionYears;
Remove-Item Env:PartitionStartDate;
Remove-Item Env:\OnPartition;
Remove-Item Env:\PartitionType;

