﻿Param (
	#Version of SQL the install is going against
	#Valid values: 2012, 2016
	$SQLVersion="2016",
	#Name of the SQL Database server
	$DBSERVER = "SERVERNAME",
	#Name of the R360 Database-Default is WFSDB_R360
	$R360DB = "WFSDB_R360",
	#Name of the RAAM Database-Default is WFSDB_R360
	$RAAMDB = "RAAM",
	#Name of the SSIS Configuration Database-Default is WFS_SSIS_Configuration
	$SSISConfigDB = "WFS_SSIS_Configuration",
	#Installation Folder
	$INSTALLFOLDER = "D:\WFSStaging\DB\RecHub2.03.09.02",
	#Database Install Application Folder
	$INSTALLERAPP = "D:\WFSApps\Utilities\WFSDatabaseInstall",
	#Log Folder
	$LOGFOLDER = "D:\WFSStaging\DB\Installlogs",
	#Drive we're installing XML DLL to
	$INSTALLDRIVE = "D:",
	$Verbose = $true
)


function Copy-Utilities([string] $local:utilfrom, [string] $local:utildest)
{
	$local:Utilities = Get-ChildItem $local:utilfrom -Directory
	foreach( $local:Utility in $local:Utilities )
	{
		$local:SourceFolder = Join-Path -Path $local:utilfrom -ChildPath $local:Utility;
		$local:TargetFolder = Join-Path -Path $local:utildest -ChildPath $local:Utility;
		Write-Host "Checking folder" $local:SourceFolder;
		if( !(Test-Path $local:TargetFolder) )
		{
			Write-Host "Copying $local:SourceFolder to $local:TargetFolder..." -NoNewline;
			Copy-Item $local:SourceFolder $local:TargetFolder -Force -Recurse -Container;
			Write-Host "done.";
		}
	}
}

# Copy bin directory
if (Test-Path $INSTALLFOLDER\bin)
{
	Copy-Item $INSTALLFOLDER\bin -Destination $INSTALLDRIVE\WFSApps\RecHub -Recurse -Force -Verbose

	# Copy out the utilties folder first.
	if (Test-Path $INSTALLDRIVE\WFSStaging\Utilities)
	{
		$utildest = "$INSTALLDRIVE\WFSApps\Utilities"
		$utilfrom = "$INSTALLDRIVE\WFSStaging\Utilities"
		Copy-Utilities $utilfrom $utildest;
	}
}
# Find our installer location
if ( $SQLVersion -eq "2016" )
{
	$Installer = "$INSTALLERAPP\WFSDatabaseInstallConsole2016"
}
else {
	$Installer = "$INSTALLERAPP\WFSDatabaseInstallConsole"
}

#Install our DB
&"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.09.02Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
if ($Verbose -eq $true) {
	Read-Host 'R360 Patch 2.03.09.02 has completed-Press any key to continue' | Out-Null 
}

 &"$Installer" -DBServer $DBSERVER -DBName $R360DB -CommandFile R360Online2.03.09.02SSISPatch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
 if ($Verbose -eq $true) {
	 Read-Host 'SSIS Patch 2.03.09.02 has completed-Press any key to continue' | Out-Null 
 }
 
 # &"$Installer" -DBServer $DBSERVER -DBName $RAAMDB -CommandFile RAAMRecHub2.04.04.01Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
 # if ($Verbose -eq $true) {
	 # Read-Host 'RAAM Patch 2.03.09.00 has completed-Press any key to continue' | Out-Null 
 # }


# &"$Installer" -DBServer $DBSERVER -DBName $SSISConfigDB -CommandFile SSISConfigurationDatabase2.03.09.00Patch.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
# if ($Verbose -eq $true) {
	# Read-Host 'SSIS Configuration Patch 2.03.09.00 has completed-Press any key to continue' | Out-Null 
# }
