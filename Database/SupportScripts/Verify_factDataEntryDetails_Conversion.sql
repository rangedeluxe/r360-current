

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: TWE
* Date: 07/17/2015
*
* Purpose: 
*     factDataEntryDetails was run through a conversion process to add the
*      new WorkgroupDataEntryKey to the factDataEntryDetails table.
*
*     dimDataEntryColumns was run through a conversion process to create
*       the new table dimWorkgroupDataEntryColumns
*
*     This will run various checks against the tables looking for problems
*      
*
* Modification History
* 07/17/2015 WI 221839  TWE	Created.
******************************************************************************/


select count(*) as [Total Record Count of factDataEntryDetails table] from RecHubData.factDataEntryDetails;
--select count(*) from RecHubData.dimWorkgroupDataEntryColumns;

-------------------------------------------------------------------------------------------------------
-- Validate the max WorkgroupDataEntryColumnKey is not greater (out of bounds) than the actual key on file.
-------------------------------------------------------------------------------------------------------
;WITH ValidateMaxKey AS
(
    SELECT
	    (SELECT max(WorkgroupDataEntryColumnKey) FROM RecHubData.dimWorkgroupDataEntryColumns) AS MaxAvailableKey,
		(SELECT max(WorkgroupDataEntryColumnKey) FROM RecHubData.factDataEntryDetails )        AS MaxUsedKey
)
SELECT
    'Check for Key out of bounds'    AS Validation,
    MaxAvailableKey,
	MaxUsedKey,
	CASE
	  WHEN MaxUsedKey > MaxAvailableKey Then '***ERROR***   Invalid key found:  '
	  ELSE 'No Problem ;WITH Max Key found'
	END                              AS Message

FROM
    ValidateMaxKey;


-------------------------------------------------------------------------------------------------------
-- Validate the max WorkgroupDataEntryColumnKey is not greater (out of bounds) than the actual key on file.
-------------------------------------------------------------------------------------------------------

;WITH KeyUsage AS
(
    SELECT DISTINCT
	    WorkgroupDataEntryColumnKey  AS KeyUsed
	FROM
	    RecHubData.dimWorkgroupDataEntryColumns
),
KeyUsageRecords AS
(
SELECT
    KeyUsed,
    (SELECT Count(*) 
	 FROM RecHubData.factDataEntryDetails 
	 WHERE RecHubData.factDataEntryDetails.WorkgroupDataEntryColumnKey = KeyUsed) AS RecordCount
FROM KeyUsage
)
select 
    KeyUsed,
	--RecordCount,
	CASE
	  WHEN RecordCount = 0 THEN '**Warning** No records assigned to this key'
	  ELSE CONVERT(VARCHAR(5),RecordCount) + '  --  records assigned to this key'
	END                  AS Message
FROM 
    KeyUsageRecords
ORDER BY
    KeyUsed;

-------------------------------------------------------------------------------------------------------
-- Make sure WorkgroupDataEntryColumnKey assigned to the record exists as a valid key
-------------------------------------------------------------------------------------------------------
;WITH KeyUsage AS
(
    SELECT DISTINCT
	    WorkgroupDataEntryColumnKey       AS KeyUsed
	FROM
	    RecHubData.factDataEntryDetails
),
KeyUsageRecords AS
(
SELECT
    KeyUsed,
    (SELECT Count(*) 
	 FROM RecHubData.dimWorkgroupDataEntryColumns 
	 WHERE RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = KeyUsed) AS RecordCount
FROM KeyUsage
)
select 
    KeyUsed AS WorkgroupDataEntryColumnKey,
	--RecordCount,
	CASE
	  WHEN RecordCount = 0 THEN '**Error** Invalid WorkgroupDataEntryColumnKey assigned to this record'
	  ELSE 'Valid Key'
	END                  AS Message
FROM 
    KeyUsageRecords
ORDER BY
    KeyUsed;


-------------------------------------------------------------------------------------------------------
-- Locate dimDataEntryColumns that were not converted
--     because they are not currently used by RecHubData.factDataEntryDetails
-------------------------------------------------------------------------------------------------------
;WITH KeyUsage AS
(
    SELECT DISTINCT
	    WorkgroupDataEntryColumnKey       AS NewKeyUsed,
		DataEntryColumnKey                AS OldKeyUsed
	FROM
	    RecHubData.factDataEntryDetails
),
ValidateKeyUsage  AS
(
SELECT 
    DataEntryColumnKey,
	NewKeyUsed,
	ISNULL(NewKeyUsed, -1) AS FldSwitch
FROM RecHubData.dimDataEntryColumns
LEFT OUTER JOIN KeyUsage
    ON DataEntryColumnKey = OldKeyUsed
)
SELECT 
    'RecHubData.dimDataEntryColumns DataEntryColumnKey=' + CONVERT(VARCHAR(10),DataEntryColumnKey) + ' was not converted' AS Message
   
FROM ValidateKeyUsage
WHERE FldSwitch < 0
;

-------------------------------------------------------------------------------------------------------
-- Compare the fields created in dimWorkgroupDataEntryColumns
-------------------------------------------------------------------------------------------------------

--Create temp file 
IF OBJECT_ID('tempdb..#FinalResults') IS NOT NULL
    DROP TABLE #FinalResults;
--create a table to store the final results
CREATE TABLE #FinalResults
(
	ErrorFlag             INT          NOT NULL,
	ErrorMessage          VARCHAR(250) NOT NULL,
	WorkgroupDataEntryKey BIGINT       NOT NULL,
	DataEntryKey          INT          NOT NULL
);

;WITH KeyUsage AS
(
    SELECT DISTINCT
	    WorkgroupDataEntryColumnKey       AS NewKeyUsed,
		DataEntryColumnKey                AS OldKeyUsed
	FROM
	    RecHubData.factDataEntryDetails
),
CompareData AS
(
    SELECT 
	   NewKeyUsed,    --key from dimWorkgroupDataEntryColumns
	   OldKeyUsed,    --key from dimDataEntryColumns
	   NewDE.IsActive					 AS NewDE_IsActive                 ----defaulted to IsActive

      ,NewDE.SiteBankID                  AS NewDE_SiteBankID
      ,NewDE.SiteClientAccountID		 AS NewDE_SiteClientAccountID
      ,NewDE.BatchSourceKey			     AS NewDE_BatchSourceKey
      ,NewDE.IsCheck					 AS NewDE_IsCheck
	  ,NewDE.UILabel					 AS NewDE_UILabel

	  ----the following dimDataEntryColumns were converted
      ,NewDE.MarkSense				     AS NewDE_MarkSense
	  ,OldDE.MarkSense					 AS OldDE_MarkSense
      ,NewDE.DataType					 AS NewDE_DataType
	  ,OldDE.DataType					 AS OldDE_DataType
      ,NewDE.ScreenOrder				 AS NewDE_ScreenOrder
	  ,OldDE.ScreenOrder				 AS OldDE_ScreenOrder
      ,NewDE.FieldName				     AS NewDE_FieldName
	  ,OldDE.FldName					 AS OldDE_FieldName
      ,NewDE.SourceDisplayName		     AS NewDE_SourceDisplayName
	  ,OldDE.DisplayName				 AS OldDE_SourceDisplayName

	  ----the following dimDataEntryColumns were not converted
	  ,OldDE.TableType                   AS OldDE_TableType	  
	  ,OldDE.FldLength					 AS OldDE_FldLength  
	  ,OldDE.TableName					 AS OldDE_TableName
	  
   FROM KeyUsage
   INNER JOIN RecHubData.dimWorkgroupDataEntryColumns AS NewDE
       ON WorkgroupDataEntryColumnKey = NewKeyUsed
   INNER JOIN RecHubData.dimDataEntryColumns   AS OldDE
       ON DataEntryColumnKey = OldKeyUsed
)
INSERT INTO #FinalResults 
(
    ErrorFlag,
	ErrorMessage,
	WorkgroupDataEntryKey,
	DataEntryKey
)
SELECT
   CASE 
     WHEN NewDE_SourceDisplayName = OldDE_SourceDisplayName then 0
     ELSE 1
   END                        AS ErrorFlag,
   CASE 
     WHEN NewDE_SourceDisplayName = OldDE_SourceDisplayName then 'Data column matches' 
     ELSE 'Fail:  SourceDisplayName column does not match Newvalue=' + NewDE_SourceDisplayName + '  OldValue= ' + OldDE_SourceDisplayName
	 --ELSE 'Fail:  SourceDisplayName column does not match Newvalue=' + CONVERT(VARCHAR(10),NewDE_SourceDisplayName) + '  OldValue= ' + CONVERT(VARCHAR(10),OldDE_SourceDisplayName)
   END                        AS ErrorMessage
   ,NewKeyUsed                 AS WorkgroupDataEntryKey
   ,OldKeyUsed                 AS DataEntryKey
FROM CompareData

UNION

SELECT
   CASE 
     WHEN NewDE_FieldName = OldDE_FieldName then 0
     ELSE 1
   END                        AS ErrorFlag,
   CASE 
     WHEN NewDE_FieldName = OldDE_FieldName then 'Data column matches' 
     ELSE 'Fail:  FieldName column does not match Newvalue=' + CONVERT(VARCHAR(10),NewDE_FieldName) + '  OldValue= ' + CONVERT(VARCHAR(10),OldDE_FieldName)
   END                        AS ErrorMessage
   ,NewKeyUsed                 AS WorkgroupDataEntryKey
   ,OldKeyUsed                 AS DataEntryKey
FROM CompareData

UNION

SELECT
   CASE 
     WHEN NewDE_ScreenOrder = OldDE_ScreenOrder then 0
     ELSE 1
   END                        AS ErrorFlag,
   CASE 
     WHEN NewDE_ScreenOrder = OldDE_ScreenOrder then 'Data column matches' 
     ELSE 'Fail:  ScreenOrder column does not match Newvalue=' + CONVERT(VARCHAR(10),NewDE_ScreenOrder) + '  OldValue= ' + CONVERT(VARCHAR(10),OldDE_ScreenOrder)
   END                        AS ErrorMessage
   ,NewKeyUsed                 AS WorkgroupDataEntryKey
   ,OldKeyUsed                 AS DataEntryKey
FROM CompareData

UNION

SELECT
   CASE 
     WHEN NewDE_DataType = OldDE_DataType then 0
     ELSE 1
   END                        AS ErrorFlag,
   CASE 
     WHEN NewDE_DataType = OldDE_DataType then 'Data column matches' 
     ELSE 'Fail:  DataType column does not match Newvalue=' + CONVERT(VARCHAR(10),NewDE_DataType) + '  OldValue= ' + CONVERT(VARCHAR(10),OldDE_DataType)
   END                        AS ErrorMessage
   ,NewKeyUsed                 AS WorkgroupDataEntryKey
   ,OldKeyUsed                 AS DataEntryKey
FROM CompareData

UNION

SELECT
   CASE 
     WHEN NewDE_MarkSense = OldDE_MarkSense then 0
     ELSE 1
   END                        AS ErrorFlag,
   CASE 
     WHEN NewDE_MarkSense = OldDE_MarkSense then 'Data column matches' 
     ELSE 'Fail:  MarkSense column does not match Newvalue=' + CONVERT(VARCHAR(10),NewDE_MarkSense) + '  OldValue= ' + CONVERT(VARCHAR(10),OldDE_MarkSense)
   END                        AS ErrorMessage
   ,NewKeyUsed                 AS WorkgroupDataEntryKey
   ,OldKeyUsed                 AS DataEntryKey
FROM CompareData;

---------------------------------------------------------------------------------------------------------
--all error messages should have been created
--    now go check for any errors that may have been generated and
---------------------------------------------------------------------------------------------------------
if ((SELECT count(*) FROM #FinalResults Where ErrorFlag = 1) < 1)
	BEGIN;
		select 'Comparing files no errors were found'  AS ErrorMessage;
	END;
ELSE
	BEGIN;
		SELECT *
		FROM #FinalResults
		Where ErrorFlag = 1;
	END;

----- for testing Dump the complete table
--Select * from #FinalResults;
-------------------------------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------------------------------

