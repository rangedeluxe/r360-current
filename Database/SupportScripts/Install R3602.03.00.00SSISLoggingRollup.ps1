######### Update the following variables to match the installation #########
#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is SSISConfiguration
$SSISConfig = "WFS_SSIS_Logging"
#Installation Folder
$INSTALLROOT = "D:\WFSStaging\DB\RecHub2.03"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"


function RunInstaller([string] $local:DBServer, [string] $local:DBName, [string] $local:Release, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:InstallPath = [System.IO.Path]::Combine($INSTALLROOT,$local:Release,$local:InstallFolderPath);
	&"$Installer" -DBServer $local:DBServer -DBName $local:DBName -CommandFile $local:CommandFile -InstallationFolder $local:InstallPath -LogFolder $LOGFOLDER
}

function RunInstallXML([string] $local:Release, [string] $local:Version, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:Message = [string]::Format("**** SSIS Logging rollup starting {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor YELLOW;
	RunInstaller $DBSERVER $SSISConfig $local:Release $local:InstallFolderPath $local:CommandFile;
	$local:Message = [string]::Format("**** SSIS Logging rollup complete for {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor GREEN;
}


################################################################################
# Main
################################################################################

Write-Host "**** Starting SSIS Logging Database Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;


if( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
else
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

# 2.0
RunInstallXML "2.00" "2.00.00.00" "R360H_2.00.00.00\Database" "SSISLoggingDatabase2.00.00.00.xml";
RunInstallXML "2.00" "2.00.01.00" "R360H_2.00.01.00\Database" "SSISLoggingDatabase2.01.00.00Patch.xml";

# 2.01
RunInstallXML "2.01" "2.01.00.00" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "SSISLoggingDatabase2.01.00.00Patch.xml";

# 2.02
RunInstallXML "2.02" "2.02.00.00" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "SSISLoggingDatabase2.02.00.00Patch.xml";
RunInstallXML "2.02" "2.02.14.00" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00" "SSISLoggingDatabase2.02.14.00Patch.xml";

