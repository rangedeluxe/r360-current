<#
	.SYNOPSIS
	PowerShell script used to exec the Billing SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for Billing that will create an extract file.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER BeginDate
	The Begin date of the Billing Extract.

	.PARAMETER EndDate
	The End date of the Billing Extract.
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER SourceDB
	The name of the OLTA 1.05.x database to be upgraded.



	.PARAMETER SSISDirectory
	Folder where the PowerShell scripts are located.

#>
param
(
	[parameter(Mandatory = $true)][datetime] $BeginDate,
	[parameter(Mandatory = $true)][datetime] $EndDate,
	[parameter(Mandatory = $true)][string] $ConfigFile
)

$ScriptVerison = "2.02";

Write-Host "Execute Billing SSIS package PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 06/01/16 JBS	2.02	WI 280158	Created.
# 06/15/16 JBS	2.02	WI 285693	Add in Configuration variable collection.
# 08/25/2016 JBS 		PT #127603945 Add variable for Rerun option
#
################################################################################

#BuildXML document to be sent to execute PowerShellScript
$Configxml = New-Object "System.Xml.XmlDocument";
$Configxml.Load("$ConfigFile");

$OutputPath = $Configxml.BillingConfig.OutputPath;
$FileName = $Configxml.BillingConfig.FileName;
$Rerun = $Configxml.BillingConfig.Rerun;
$WFSConfigurationServerName = $Configxml.BillingConfig.WFSConfigurationServerName;
$WFSConfigurationInitialCatalog = $Configxml.BillingConfig.WFSConfigurationInitialCatalog;

$SSISFolder = $Configxml.BillingConfig.SSISPath;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

if( !$OutputPath.EndsWith("\") )
{
	$OutputPath += "\";
}


$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISFolder</PackageFolder>
	<PackageName>Billing</PackageName>
	<Parameters>
		<Parameter Name=""BeginDate"" Value=""$BeginDate""/>
		<Parameter Name=""EndDate"" Value=""$EndDate""/>
		<Parameter Name=""OutputPath"" Value=""$OutputPath""/>
		<Parameter Name=""FileName"" Value=""$FileName""/>
		<Parameter Name=""Rerun"" Value=""$Rerun""/>
		<Parameter Name=""WFSConfigurationServerName"" Value=""$WFSConfigurationServerName""/>
		<Parameter Name=""WFSConfigurationInitialCatalog"" Value=""$WFSConfigurationInitialCatalog""/>
	</Parameters>
</SSISPackage>"
);


$CurrentFolder = Get-Location;
Set-Location "$SSISFolder";
.\ExecuteSSISPackage $xml;
Set-Location "$CurrentFolder";