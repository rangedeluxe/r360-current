param
(
	[parameter(Mandatory = $true)][string] $DBServer = "",
	[parameter(Mandatory = $true)][string] $DBName = "",
	[parameter(Mandatory = $true)][string] $BackupPath = "",
    [string] $LogFile = "",
	[string] $LogFolder = "",
	[string] $BackupFile = "",
	[string] $LoginID = "",
	[string] $Password = "",
	[bool] $VerifyBackup = $true,
    [bool] $CopyOnly = $true,
    [bool] $TruncateLog = $true
) 

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.Common") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null;

[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("System.Data.DataRow") | Out-Null;

$ScriptVersion = "1.02";
################################################################################
#
# 07/21/10 JPB	            1.0	    Created.
# 04/20/15 JPB	            1.01	Added verify option (true by default)
# 11/30/15 JPB 	            1.02	Added CopyOnly (true by default)
# 02/20/18 #152316370  MGE  1.03    Added truncation of the transaction logs and added logging
################################################################################

################################################################################
#
# Error trapping function. Just abort and close any connections that are open.
#
################################################################################
Trap 
{
	Write-Host
	Write-Host "*********************************************************************" -ForegroundColor Red;
	$err = $_.Exception
	while ( $err.InnerException )
	{
		$err = $err.InnerException
		Write-Output $err.Message
	};
	if( $err.Errors.Count -gt 0 )
	{
		foreach( $errorMessage in $err.Errors )
		{
			Write-Output $errorMessage.Message;
		}
	}
	Write-Host "*********************************************************************"  -ForegroundColor Red;
	Write-Host "Aborting restore....."  -ForegroundColor Red;
	if( $SQLServerConnection.State -eq [System.Data.ConnectionState]::Open )
	{
		Write-Host "Disconnecting from "$DBServer"."$DBName"";
		$SQLServerConnection.Close();
		$SQLServerConnection.Dispose();
        $ProgressForm.Close();
        $ProgressForm.Dispose();
	}
	break;
 }


################################################################################
# Write-Log
################################################################################
function Write-Log 
{
	param
	(
		[String] $LogFileName,
		[String] $LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($LogFileName) -eq $null )
		{
			$LogFileName = Get-Location + "\" + $LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date -f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date -f "yyyy-MM-dd HH:mm:ss") + " " + $LogInformation | Out-File -FilePath $LogFileName -Encoding unicode -Append;
	}
}
 
################################################################################
#
# Main
#
################################################################################

Write-Host "BackupDB.PS1" $ScriptVersion "beginning backup for" $DBServer $DBName;

$ExecutePath = (Get-Location).path;

if( $LogFile.Length -eq 0 )
{
	$LogFile += (Get-Location).path + "\Backup_" + $ServerName + (Get-Date -format yyyyMMdd_hhmmss) + "_Results.txt"
}

$dbConnection = New-Object Microsoft.SqlServer.Management.Common.ServerConnection;
$dbConnection.ServerInstance=$DBServer;
$dbConnection.DatabaseName="master";
$dbConnection.ConnectTimeout = 21600;
$dbConnection.StatementTimeout = 0;

if( $LoginID.Length -eq 0 )
{
	$dbConnection.LoginSecure=$true;
}
else
{
	$dbConnection.LoginSecure=$false;
	$dbConnection.Login=$LoginID;
	$dbConnection.Password=$Password;
}
$SQLServer = new-object Microsoft.SqlServer.Management.Smo.Server($dbConnection);


$SQLDatabase = $SQLServer.Databases[$DBName];
$Backup = new-object ("Microsoft.SqlServer.Management.Smo.Backup");
$Backup.Action = [Microsoft.SqlServer.Management.Smo.BackupActionType]::Database;
$Backup.BackupSetDescription = "Full backup of " + $DBName;
$Backup.BackupSetName = $DBName + " Backup"
$Backup.Database = $DBName
$Backup.MediaDescription = "Disk"
$Backup.LogTruncation = [Microsoft.SqlServer.Management.Smo.BackupTruncateLogType]::Truncate;
$Backup.CopyOnly = $CopyOnly;

if( !$BackupPath.EndsWith("\") )
{
	$BackupPath += "\";
}

if( $BackupFile.Length -gt 0 )
{
	$BackupFileName = $BackupPath+$BackupFile;
}
else
{
	$BackupFileName = $BackupPath + $DBName + "_db_" + (get-date -format yyyyMMddHHmmss) + ".bak";
}

$Backup.Devices.AddDevice($BackupFileName, [Microsoft.SqlServer.Management.Smo.DeviceType]::File);
$Backup.Initialize = $TRUE

Write-Log $LogFile ("Database Server       " + $DBServer);
Write-Log $LogFile ("Database Name         " + $DBName);
Write-Log $LogFile ("LogFile               " + $LogFile);
Write-Log $LogFile ("Backup File Name      " + $BackupFileName);
Write-Log $LogFIle ("Execute Path          " + $ExecutePath);

######################### Create Progress Bar Window #########################

$FormWidth = 400;
$ProgressForm = New-Object System.Windows.Forms.Form;
$ProgressForm.Text = "Database Backup $ScriptVersion";
$ProgressForm.Height = 150;
$ProgressForm.Width = 400;
$ProgressForm.BackColor = "White";
$ProgressForm.FormBorderStyle = [System.Windows.Forms.FormBorderStyle]::FixedSingle;
$ProgressForm.StartPosition = [System.Windows.Forms.FormStartPosition]::CenterScreen;
$DatabaseServerLabel= New-Object System.Windows.Forms.Label;
$DatabaseServerLabel.Text = "Server $DBServer";
$DatabaseServerLabel.Left = 5;
$DatabaseServerLabel.Top = 10;
$DatabaseServerLabel.Width = $FormWidth - 20;
$DatabaseServerLabel.Height = 15;
$DatabaseServerLabel.Font = "Verdana";
$ProgressForm.Controls.Add($DatabaseServerLabel);

$DatabaseNameLabel= New-Object System.Windows.Forms.Label;
$DatabaseNameLabel.Text = "Database $DBName";
$DatabaseNameLabel.Left = 5;
$DatabaseNameLabel.Top = 25;
$DatabaseNameLabel.Width = $FormWidth - 20;
$DatabaseNameLabel.Height = 15;
$DatabaseNameLabel.Font = "Verdana";
$ProgressForm.Controls.Add($DatabaseNameLabel);

$ProgressBar = New-Object System.Windows.Forms.ProgressBar;
$ProgressBar.Name = "progressBar1";
$ProgressBar.Value = 0;
$ProgressBar.Style = [System.Windows.Forms.ProgressBarStyle]::Continuous;
$System_Drawing_Size = New-Object System.Drawing.Size;
$System_Drawing_Size.Width = $FormWidth - 40;
$System_Drawing_Size.Height = 20;
$ProgressBar.Size = $System_Drawing_Size;
$ProgressBar.Left = 5;
$ProgressBar.Top = 50;
#$ProgressBar.Step = 5;
$ProgressForm.Controls.Add($ProgressBar);

$PercentCompleteLabel= New-Object System.Windows.Forms.Label;
$PercentCompleteLabel.Text = "Backup";
$PercentCompleteLabel.Left = 5;
$PercentCompleteLabel.Top = 70;
$PercentCompleteLabel.Width = $FormWidth - 20;
$PercentCompleteLabel.Height = 15;
$PercentCompleteLabel.Font = "Verdana";
$PercentCompleteLabel.TextAlign = [System.Drawing.ContentAlignment]::MiddleCenter;
$ProgressForm.Controls.Add($PercentCompleteLabel);


$ProgressForm.Show()| out-null;
$ProgressForm.Focus() | out-null;

######################### Create Progress Bar Window #########################

#Set event handlers
$Backup.PercentCompleteNotification = 5;
$BackupPercentEventHandler = [Microsoft.SqlServer.Management.Smo.PercentCompleteEventHandler] {
	$ProgressBar.Value = $_.Percent;
	$PercentCompleteLabel.Text= "Backup {0}% complete" -f $_.Percent;
	$ProgressForm.Refresh();
}
$BackupCompletedEventHandler = [Microsoft.SqlServer.Management.Common.ServerMessageEventHandler] {Write-Host "Database $DBName backup created successfuly!";}
$Backup.add_PercentComplete($BackupPercentEventHandler);
$Backup.add_Complete($BackupCompletedEventHandler);

Write-Host "Beginning Backup Database $DBServer.$DBName";
Write-Log $LogFile "Beginning Backup Database $DBServer.$DBName";

$Backup.SqlBackup($SQLServer);

Write-Log $LogFile "Back up file written";

if( $VerifyBackup )
{
#$ProgressForm.Close();
	$PercentCompleteLabel.Text = "Verify";
	$ProgressBar.Value = 0;
	$ProgressForm.Refresh();
	Write-Host "Verifying Backup";
	Write-Log "Beginning Verification... "
	#create the restore SMO object and add the backup file to the devices
	$Restore = New-Object "Microsoft.SqlServer.Management.Smo.Restore";
	$Restore.Devices.AddDevice($BackupFileName,[Microsoft.SqlServer.Management.Smo.DeviceType]::File);
	$Restore.Action = [Microsoft.SqlServer.Management.Smo.RestoreActionType]::Database;
	
	#Set event handlers
	$Restore.PercentCompleteNotification = 5;
	$VerifyPercentEventHandler = [Microsoft.SqlServer.Management.Smo.PercentCompleteEventHandler] {
		$ProgressBar.Value = $_.Percent;
		$PercentCompleteLabel.Text= "Verify {0}% complete" -f $_.Percent;
		$ProgressForm.Refresh();
	}
	$VerifyCompletedEventHandler = [Microsoft.SqlServer.Management.Common.ServerMessageEventHandler] {Write-Host "Database " $DBName " verification complete.";}
	$Restore.add_PercentComplete($VerifyPercentEventHandler);
	$Restore.add_Complete($VerifyCompletedEventHandler);

	$VerifyResults = $Restore.SqlVerify($SQLServer);
	if( $VerifyResults )
	{
		Write-Host "Database Backup verified";
        Write-Log $LogFile "Database Backup verified";
	}
	else
	{
		Write-Host "Database Backup did not verify" -ForegroundColor Red;
        Write-Log $LogFile "Database Backup did not verify"
	}

	$ProgressForm.Close();
    $CompletionDate = Get-Date -format g;
    Write-Host "$DBserver.$DBname backup complete and Verified:  $CompletionDate" -ForegroundColor Green;
    Write-Log $LogFile "$DBserver.$DBname backup complete and Verified:  $CompletionDate"

    if( $TruncateLog )
    {
    Write-Log $LogFile "Beginning Truncation..."
	$Result = Invoke-Sqlcmd -Query ("SELECT CAST(database_id AS VARCHAR(3)) FROM sys.databases WHERE name= '$DBName';");
    $DatabaseID = $Result[0];
    $Result = Invoke-Sqlcmd -Query ("SELECT CAST(name AS VARCHAR(100)) FROM sys.master_files WHERE type = 1 AND database_id = $DatabaseID;");
    $LogFileName = $Result[0];
    Write-Host "$DBserver.$DBName truncating log $LogFileName" -ForegroundColor White;
    Write-Log $LogFile "$DBserver.$DBName truncating log $LogFileName"
    Invoke-Sqlcmd -Query ("USE $DBName; ALTER DATABASE $DBNAME SET RECOVERY SIMPLE WITH NO_WAIT;");
    Invoke-Sqlcmd -Query ("USE $DBName; DBCC SHRINKFILE($LogFileName,100); ALTER DATABASE $DBName SET RECOVERY FULL WITH NO_WAIT;");
    Write-Host "$LogFileName Truncated" -ForegroundColor Green;
    Write-Log $LogFile "$LogFileName Truncated"
    }
}
else
{
$ProgressForm.Close();
}

# Alias for 7-zip
if (-not (test-path "$env:ProgramFiles\7-Zip\7z.exe")) {Write-Host "$env:ProgramFiles\7-Zip\7z.exe needed, but is not installed"}
set-alias sz "$env:ProgramFiles\7-Zip\7z.exe"

$ZipPath = $BackupFileName -replace '.bak', '.7z'

If (Test-Path $BackupFileName)
{
    Write-Host "Zipping $BackupFileName ..."
	Write-Log $LogFile "Beginning Compression..."
    sz a -t7z $ZipPath $BackupFileName
    Write-Host "$BackupFileName Compressed" -ForegroundColor White;
    Write-Log $LogFile "$BackupFileName Compressed"
}

If (Test-Path $BackupFileName)
{
    If (Test-Path $ZipPath) 
    {
    Remove-Item -Path $BackupFileName;
    Write-Host "$BackupFileName Deleted" -ForegroundColor White;
    Write-Log $LogFile "$BackupFileName Deleted";
    }
}

if ((Get-Location).path -ne $ExecutePath) 
{
    Set-Location $ExecutePath
}