--WFSScriptProcessorDoNotFormat
/*********************************************************************************************************
*																										**		
*				Fill in the value for the AdminLoginID in the first DECLARE statement before running!!  **
*				Run this script for 2.03.04.00 AFTER the RAAM MSI install								**
*********************************************************************************************************/
DECLARE @AdminLoginID VARCHAR(256) = 'WFSAdmin';
DECLARE @UserID INT,
		@EncryptedPassword NVARCHAR(128);

SELECT @UserID = UserID FROM dbo.Users WHERE LoginID = @AdminLoginID;
SET @EncryptedPassword = (SELECT Top 1 EncryptedPassword from dbo.PasswordHistory WHERE UserID = @UserID ORDER BY CreationDate DESC);
UPDATE dbo.Users SET EncryptedPassword = @EncryptedPassword WHERE UserID = @UserID