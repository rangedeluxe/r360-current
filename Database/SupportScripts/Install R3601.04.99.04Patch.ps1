#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$VER104DB = "104DB"
$INSTALLDRIVE = "D:"

$INSTALLFOLDER = "$INSTALLDRIVE\WFSStaging\DB\RecHubMigration\"
#Log Folder
$LOGFOLDER = "$INSTALLDRIVE\WFSStaging\DB\InstallLogs"
#Drive we're installing XML DLL to


IF( $SQLVersion -eq "2016" )
{
	$Installer = "$INSTALLDRIVE\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "$INSTALLDRIVE\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

&"$Installer" -DBServer $DBSERVER -DBName $VER104DB -CommandFile OLTA1.04.01.99.04Migration.xml -InstallationFolder $INSTALLFOLDER -LogFolder $LOGFOLDER
Read-Host 'Version 104 Patch has completed-Press any key to continue' | Out-Null