######### Update the following variables to match the installation #########
#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"
#Installation Folder
$INSTALLROOT = "D:\WFSStaging\"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"
#Drive we're installing XML DLL to
$INSTALLDRIVE = "D:"
######### End user defined variables #########

function RunInstaller([string] $local:DBServer, [string] $local:DBName, [string] $local:Release, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:InstallPath = [System.IO.Path]::Combine($INSTALLROOT,$local:Release,$local:InstallFolderPath);
	&"$Installer" -DBServer $local:DBServer -DBName $local:DBName -CommandFile $local:CommandFile -InstallationFolder $local:InstallPath -LogFolder $LOGFOLDER
}

function RunR360InstallXML([string] $local:Release, [string] $local:Version, [string] $local:InstallFolderPath, [string] $local:CommandFile)
{
	$local:Message = [string]::Format("**** R360 Online SSIS rollup starting {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor YELLOW;
	RunInstaller $DBSERVER $R360DB $local:Release $local:InstallFolderPath $local:CommandFile;
	$local:Message = [string]::Format("**** R360 Online SSIS rollup complete for {0} ****",$local:Version);
	Write-Host $local:Message -ForegroundColor GREEN;
}

function CopySSISBin([string] $local:Release, [string] $local:Version, [string] $local:InstallFolderPath)
{
	Write-Host
	Write-Host "Copying SSIS support DLLs from $local:Version..." -NoNewline -ForegroundColor Yellow;
	$local:BinSourcePath = [System.IO.Path]::Combine($INSTALLROOT,$local:Release,$local:InstallFolderPath,"bin");
	$local:BinTargetPath = Join-Path $INSTALLDRIVE "WFSApps\RecHub\bin"
	if (!(Test-Path $local:BinTargetPath)) 
	{
	    New-Item $local:BinTargetPath -ItemType Directory | Out-Null
	}
	Copy-Item (Join-Path $local:BinSourcePath "*") $local:BinTargetPath -Force
	Write-Host "Complete" -ForegroundColor Green;
	Write-Host
}

Write-Host "**** Starting R360 Database SSIS Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;

if( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016";
}
else
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole";
}

# 2.01
RunR360InstallXML "2.01" "2.01.00.00" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "RecHub2.01.00.00SSISPatch.xml";
RunR360InstallXML "2.01" "2.01.01.00" "R360_2.01.01.00\DB\RecHub2.1\2.01.01.00" "R360Online2.01.01.00SSISPatch.xml";

# 2.02
RunR360InstallXML "2.02" "2.02.00.00" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "R360Online2.02.00.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.02.00" "R360_2.02.02.00\DB\RecHub2.1\2.02.02.00" "R360Online2.02.02.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.03.00" "R360_2.02.03.00\DB\RecHub2.1\2.02.03.00" "R360Online2.02.03.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.04.00" "R360_2.02.04.00\DB\RecHub2.1\2.02.04.00" "R360Online2.02.04.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.06.00" "R360_2.02.06.00\DB\RecHub2.1\2.02.06.00" "R360Online2.02.06.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.07.00" "R360_2.02.07.00\DB\RecHub2.1\2.02.07.00" "R360Online2.02.07.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.08.00" "R360_2.02.08.00\DB\RecHub2.1\2.02.08.00" "R360Online2.02.08.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.09.00" "R360_2.02.09.00\DB\RecHub2.1\2.02.09.00" "R360Online2.02.09.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.10.00" "R360_2.02.10.00\DB\RecHub2.1\2.02.10.00" "R360Online2.02.10.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.11.00" "R360_2.02.11.00\DB\RecHub2.1\2.02.11.00" "R360Online2.02.11.00SSISPatch.xml";
RunR360InstallXML "2.02" "2.02.12.00" "R360_2.02.12.00\DB\RecHub2.1\2.02.12.00" "R360Online2.02.12.00SSISPatch.xml";
CopySSISBin "2.02" "2.02.12.00" "R360_2.02.12.00\DB\RecHub2.1\2.02.12.00"
RunR360InstallXML "2.02" "2.02.13.00" "R360_2.02.13.00\DB\RecHub2.1\2.02.13.00" "R360Online2.02.13.00SSISPatch.xml";
CopySSISBin "2.02" "2.02.13.00" "R360_2.02.13.00\DB\RecHub2.1\2.02.13.00"
RunR360InstallXML "2.02" "2.02.14.00" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00" "R360Online2.02.14.00SSISPatch.xml";
CopySSISBin "2.02" "2.02.14.00" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00"
#No SSIS packages from 2.02.15.00, just new DLLs
CopySSISBin "2.02" "2.02.15.00" "R360_2.02.15.00\DB\RecHub2.1\2.02.15.00"
RunR360InstallXML "2.02" "2.02.16.00" "R360_2.02.16.00\DB\RecHub2.1\2.02.16.00" "R360Online2.02.16.00SSISPatch.xml";

# 2.03
RunR360InstallXML "2.03" "2.03.00.00" "R360_2.03.00.00\DB\RecHub2.03\2.03.00.00" "R360Online2.03.00.00SSISPatch.xml";
CopySSISBin "2.03" "2.03.00.00" "R360_2.03.00.00\DB\RecHub2.03\2.03.00.00"


Write-Host '**** R360 Database SSIS Rollup to Patch 2.03.00.00 has completed' -ForegroundColor GREEN | Out-Null