/**************************************************************************************************************
* This SQL will discover Lockboes in 1.04 that have had at least one use of the LockboxRemitter functionality *
*   in the past 60 days. It must have been updated by an online user.                                         *
* This will produce a set of SQL statements to update/insert to the dimWorkgroup Business Rules table.        *
* When the query had finished, copy the SQL statements produced into a SSMS window attached to the WFSDB_R360 *
*   database and execute it.																				  *
* You can verify results by using this SQL statement on R360 after executing the SQL:						  *
*	SELECT * FROM RecHubData.dimWorkgroupBusinessRules ORDER BY ModificationDate DESC						  *
* All of the update/inserted rows will appear at the top of the output and the column named					  *
*	PostDepositPayerRequired should be set to 1.															  *
**************************************************************************************************************/
SET NOCOUNT ON;
DECLARE @SQL VARCHAR(2056);
DECLARE @Iteration INT = 0,
		@RowCount INT = 0;
DECLARE @LockboxList Table (RowID INT IDENTITY(1,1),BankID INT, LockboxID INT);

INSERT INTO @LockboxList
select Distinct BankID, LockboxID from OLTA.dimRemitters
INNER JOIN  
(select RoutingNumber, Account, LockboxRemitter.RemitterName AS LockboxRemitterName, BankID, LockboxID, LockboxRemitter.ModificationDate from dbo.LockboxRemitter
INNER JOIN dbo.globalRemitter ON globalRemitter.GlobalRemitterID = lockboxremitter.globalremitterid
INNER JOIN OLTA.Users ON (LockboxRemitter.CreatedBy = Users.LogonName OR LockboxRemitter.ModifiedBy = Users.LogonName)) AS A
ON A.RoutingNumber = OLTA.dimRemitters.RoutingNumber
AND A.Account = dimRemitters.Account
AND LockboxRemitterName IS NOT NULL
AND A.ModificationDate >= GETDATE() - 60

SET @Iteration = 1;
SELECT @RowCount = Count(*) FROM @LockboxList;
WHILE (@Iteration <= @RowCount)
	BEGIN
	SELECT @SQL = 'IF EXISTS(Select 1 FROM RecHubData.dimWorkgroupBusinessRules WHERE SiteBankID = ' + CAST(BankID AS VARCHAR(10)) + ' AND SiteClientAccountID = ' + CAST(LockboxID AS VARCHAR(10)) +') 
	UPDATE RecHubData.dimWorkgroupBusinessRules SET PostDepositPayerRequired = 1, ModificationDate = GETDATE() WHERE SiteBankID = ' + CAST(BankID AS VARCHAR(10)) + ' AND SiteClientAccountID = ' + CAST(LockboxID AS VARCHAR(10)) + '
		ELSE
	INSERT INTO RecHubData.dimWorkgroupBusinessRules (SiteBankID,SiteClientAccountID,InvoiceRequired,BalancingRequired,PostDepositBalancingRequired,PostDepositPayerRequired,CreationDate,ModificationDate)
	VALUES (' + CAST(BankID AS VARCHAR(10)) + ', ' + CAST(LockboxID AS VARCHAR(10)) + ', 0, 0, 0, 1, GETDATE(), GETDATE());
	'
	FROM @LockboxList
	WHERE RowID = @Iteration
	PRINT @SQL
	SET @Iteration = @Iteration + 1
	END
