﻿<#
	.SYNOPSIS
	PowerShell script used to exec the Workgroup Assignment SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for importing Users from a CSV file.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER FileName
	The path to the file (including filename).
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER SSISDirectory
	Folder where the PowerShell scripts are located.

#>
param
(
#	[parameter(Mandatory = $true)][string] $FileName,
#	[parameter(Mandatory = $true)][string] $DLLPath,
	[parameter(Mandatory = $true)][string] $ConfigFile
)

$ScriptVerison = "2.02";

Write-Host "Execute AdvSearchMigration SSIS package PowerShell Script Version " $ScriptVerison

################################################################################
## Deluxe Corporation (DLX)
## Copyright © 2018 Deluxe Corporation, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2018 Deluxe Corporation All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of DLX.
################################################################################
#
# 02/13/2018 MGE	2.02.11	PT 154804395	Created.
#
################################################################################

#BuildXML document to be sent to execute PowerShellScript
$Configxml = New-Object "System.Xml.XmlDocument";
$Configxml.Load("$ConfigFile");
Write-Host "Config File = " $ConfigFile
Write-Host "Configxml = " $Configxml

#$DLLPath = 'C:\WFSApps\RecHub\Bin';
$TargetServerName = $Configxml.AdvSearchMigrationConfig.AdvSearchMigrationServerName;
$TargetInitialCatalog = $Configxml.AdvSearchMigrationConfig.AdvSearchMigrationInitialCatalog;
$StartDate = $Configxml.AdvSearchMigrationConfig.AdvSearchMigrationStartDate;
$EndDate = $Configxml.AdvSearchMigrationConfig.AdvSearchMigrationEndDate;
$LoadSize = $Configxml.AdvSearchMigrationConfig.AdvSearchMigrationChunkSize;

[string]$LogFolder = Get-Location

if( !$LogFolder.EndsWith("\") )
{
	$LogFolder += "\";
}

# if( !$DLLPath.EndsWith("\") )
# {
	# $DLLPath += "\";
# }

$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$LogFolder</PackageFolder>
	<PackageName>AdvancedSearchWideTableMigration</PackageName>
	<Parameters>
		<Parameter Name=""TargetServerName"" Value=""$TargetServerName""/>
		<Parameter Name=""TargetInitialCatalog"" Value=""$TargetInitialCatalog""/>
		<Parameter Name=""StartDate"" Value=""$StartDate""/>
		<Parameter Name=""EndDate"" Value=""$EndDate""/>
		<Parameter Name=""LoadSize"" Value=""$LoadSize""/>
		<Parameter Name=""LogFolder"" Value=""$LogFolder""/>
	</Parameters>
</SSISPackage>"
);

Write-Host "Execute AdvSearchMigration SSIS package PowerShell Script Folder = " $LogFolder

$CurrentFolder = Get-Location;
try {
    Set-Location "$LogFolder";
    & (Join-Path $CurrentFolder "\ExecuteSSISPackage.ps1") $xml;
} finally {
    Set-Location "$CurrentFolder";
}
