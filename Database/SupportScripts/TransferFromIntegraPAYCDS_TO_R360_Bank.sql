/* Script Name = TransferFromIntegraPAYCDS_TO_R360_Bank.sql			*/
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/18/2016
*
* Purpose: Copies Bank (QueueType 20) record from IntegraPAY CDS database to R360 Database by loading the data into the CDSQueue.
*		This is to be run in a Query window connected to the IntegraPAY CDS database
*
* Replace variable values:
*   <bankid>  
*	Example SET @BankID = 1; 
*
* Modification History
* 05/18/2016 WI 280147	Created
******************************************************************************/

DECLARE @BankID INT;

SET NOCOUNT ON

/*
replace <VariableName(s)> with the Value that will be copied to R360 from integraPAY
for example, to copy bank 1 the SET will be:
SET @BankID = 1;
*/

SET @BankID = <bankid>; 

EXEC RecHub.usp_CDSQueue_Ins_Bank @parmBankID = @BankID;
