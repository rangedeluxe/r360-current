SET NOCOUNT ON;

DECLARE @LogonName NVARCHAR(256),
		@LongName NVARCHAR(256),
		@Loop INT = 1,
		@RowCount INT,
		@SiteBankID INT,
		@SiteLockboxID INT,
		@SQLCommand NVARCHAR(MAX);

DECLARE @UserList TABLE
(
	RowID INT IDENTITY(1,1),
	SiteBankID INT,
	SiteLockboxID INT,
	LongName NVARCHAR(256),
	LogonName NVARCHAR(256)
);

INSERT INTO @UserList(SiteBankID, SiteLockboxID,LongName, LogonName)
SELECT 
	OLTA.OLLockboxes.SiteBankID,
	OLTA.OLLockboxes.SiteLockboxID, 
	RTRIM(COALESCE(LongName, ShortName)) AS LongName,
	LogonName
FROM 
	OLTA.OLLockboxUsers
	INNER JOIN OLTA.Users ON OLTA.Users.UserID = OLTA.OLLockboxUsers.UserID
	INNER JOIN OLTA.OLLockboxes ON OLTA.OLLockboxes.OLLockboxID = OLTA.OLLockboxUsers.OLLockboxID
	INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.SiteBankID = OLTA.OLLockboxes.SiteBankID
		AND OLTA.dimLockboxes.SiteLockboxID = OLTA.OLLockboxes.SiteLockboxID

SET @RowCount = @@ROWCOUNT;

SET @SQLCommand = N'DECLARE 
	@UserID INT,
	@GroupID INT;';

WHILE( @Loop <= @RowCount)
BEGIN

	SELECT 
		@LogonName = LogonName,
		@SiteBankID = SiteBankID,
		@SiteLockboxID = SiteLockboxID,
		@LongName = LongName
	FROM
		@UserList
	WHERE
		RowID = @Loop;

	SET @SQLCommand = @SQLCommand + N'
IF EXISTS( SELECT 1 FROM dbo.Users WHERE LoginID = ' + QUOTENAME(@LogonName,CHAR(39)) + N')
BEGIN
	IF EXISTS( SELECT 1 FROM dbo.Roles WHERE RoleName = ' + QUOTENAME(LEFT(N'WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N' User Role',50), CHAR(39)) + N')
	BEGIN
	IF EXISTS( SELECT 1 FROM dbo.Groups WHERE GroupName = ' + QUOTENAME(LEFT(N'WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N' User Grp',50), CHAR(39)) + N') 
		BEGIN
			IF EXISTS( SELECT 1 FROM dbo.Users INNER JOIN dbo.Groups ON dbo.Groups.EntityID = dbo.Users.EntityID WHERE dbo.Users.LoginID = ' + QUOTENAME(@LogonName,CHAR(39)) + N' AND dbo.Groups.GroupName = ' + QUOTENAME(LEFT(N'WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N' User Grp',50), CHAR(39)) + N' )
			BEGIN			
				SELECT @UserID = UserID FROM dbo.Users WHERE LoginID = ' + QUOTENAME(@LogonName,CHAR(39)) + N';
				SELECT @GroupID = GroupID FROM dbo.Groups WHERE GroupName = ' + QUOTENAME(LEFT(N'WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N' User Grp',50), CHAR(39)) + N';
				IF NOT EXISTS( SELECT 1 FROM dbo.UsersGroups WHERE UserID = @UserID AND GroupID = @GroupID )
				BEGIN
					EXEC dbo.usp_UsersGroups_Insert @UserID, @GroupID;
					PRINT '+ QUOTENAME(N'Added "' + @LogonName + N'" to group "WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N'" User Grp.', CHAR(39)) + N';
				END
				ELSE
					PRINT '+ QUOTENAME(N'"' + @LogonName + N'" is already a member of group "WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N'" User Grp, skipped.', CHAR(39)) + N';
			END
				ELSE PRINT '+ QUOTENAME(N'"' + @LogonName + N'" and group "WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N'" have different Entity IDs, skipped.', CHAR(39)) + N';
		END
		ELSE PRINT ' + QUOTENAME(N'"WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N'" User Grp NOT FOUND, skipped.', CHAR(39)) + N';
	END
	ELSE PRINT ' + QUOTENAME(N'"WG ' + CAST(@SiteLockboxID AS NVARCHAR(10)) + N' - ' + @LongName + N'" User Role NOT FOUND, skipped.', CHAR(39)) + N';
END
ELSE PRINT ' + QUOTENAME(N'"' + @LogonName + N'" NOT FOUND, Skipped.', CHAR(39)) + N';
';
	PRINT @SQLCommand;

	SET @SQLCommand = N'';
	SET @Loop = @Loop + 1;
END

