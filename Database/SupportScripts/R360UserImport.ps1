﻿<#
	.SYNOPSIS
	PowerShell script used to exec the UserImport SSIS package.
	
	.DESCRIPTION
	The script will Exec an SSIS package for importing Users from a CSV file.  This script uses the ExecuteSSISPackage PowerShell script.
	
	.PARAMETER FileName
	The path to the file (including filename).

	.PARAMETER DLLPath
	The folder where the CryptSharp DLL exists (used to encrypt the password).
	
	.PARAMETER ConfigFile 
	The name of the Configuration file used for all other variables.

	.PARAMETER SSISDirectory
	Folder where the PowerShell scripts are located.

#>
param
(
	[parameter(Mandatory = $true)][string] $FileName,
#	[parameter(Mandatory = $true)][string] $DLLPath,
	[parameter(Mandatory = $true)][string] $ConfigFile
)

$ScriptVerison = "2.02";

Write-Host "Execute UserImport SSIS package PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 01/17/17 MGE	2.02.11	PT 136193255	Created.
#
################################################################################

#BuildXML document to be sent to execute PowerShellScript
$Configxml = New-Object "System.Xml.XmlDocument";
$Configxml.Load("$ConfigFile");

$DLLPath = $Configxml.UserImportConfig.DLLPath;
$WFSConfigurationServerName = $Configxml.UserImportConfig.WFSConfigurationServerName;
$WFSConfigurationInitialCatalog = $Configxml.UserImportConfig.WFSConfigurationInitialCatalog;

$SSISFolder = $Configxml.UserImportConfig.SSISPath;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

if( !$DLLPath.EndsWith("\") )
{
	$DLLPath += "\";
}

$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISFolder</PackageFolder>
	<PackageName>UserImportCSV</PackageName>
	<Parameters>
		<Parameter Name=""DLLPath"" Value=""$DLLPath""/>
		<Parameter Name=""FileName"" Value=""$((Resolve-Path $FileName).Path)""/>
		<Parameter Name=""SSISPath"" Value=""$SSISFolder""/>
		<Parameter Name=""WFSConfigurationServerName"" Value=""$WFSConfigurationServerName""/>
		<Parameter Name=""WFSConfigurationInitialCatalog"" Value=""$WFSConfigurationInitialCatalog""/>
	</Parameters>
</SSISPackage>"
);

Write-Host "Execute UserImport SSIS package PowerShell Script Folder = " $SSISFolder

$CurrentFolder = Get-Location;
try {
    Set-Location "$SSISFolder";
    & (Join-Path $CurrentFolder "\ExecuteSSISPackage.ps1") $xml;
} finally {
    Set-Location "$CurrentFolder";
}
