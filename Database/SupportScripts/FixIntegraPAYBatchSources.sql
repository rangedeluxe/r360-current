SET NOCOUNT ON;
DECLARE @BatchSources TABLE
(
	RowID		INT IDENTITY(1,1),
	ShortName	VARCHAR(30)
);

/* Update this list to match the source Consolidated database */
INSERT INTO @BatchSources(ShortName)
VALUES ('integraPAY'),('WebDDL'),('ImageRPS'),('Transactis'),('ACH');


/* Do not edit below this line */
DECLARE 
	@BankLoop INT = 1,
	@BatchSourceKey SMALLINT,
	@EntityID INT,
	@FactTableLoop INT = 1,
	@ImportTypeKey TINYINT,
	@BatchSourceLoop INT = 1,
	@OldBatchSourceKey SMALLINT,
	@OldShortName VARCHAR(30),
	@ShortName VARCHAR(30),
	@SiteBankID INT,
	@SiteClientAccountID INT,
	@SQLCommand NVARCHAR(MAX),
	@SQLParams NVARCHAR(MAX),
	@TableName NVARCHAR(128);

DECLARE @BankList TABLE
(
	RowID INT IDENTITY(1,1),
	SiteBankID INT,
	SiteClientAccountID INT
);

DECLARE @BatchSourceKeys TABLE
(
	RowID INT IDENTITY(1,1),
	BatchSourceKey SMALLINT
);

DECLARE @FactTables TABLE
(
	RowID		INT IDENTITY(1,1),
	TableName	NVARCHAR(128)
);

/* List of fact tables to update */
INSERT INTO @FactTables(TableName)
VALUES (N'factBatchData'),(N'factBatchSummary'),(N'factCheckImages'),(N'factChecks'),(N'factDataEntryDetails'),(N'factDocumentImages'),(N'factDocuments'),(N'factItemData'),(N'factRawPaymentData'),(N'factStubs'),(N'factTransactionSummary');

/* Get the import type key for integraPAY, only update integraPAY batches in this script */
SELECT @ImportTypeKey = ImportTypeKey FROM RecHubData.dimImportTypes WHERE ShortName = 'integraPAY';

/* Get a list of the integraPAY batch sources combining the batch source list and the integraPAY import key */
INSERT INTO @BatchSourceKeys(BatchSourceKey)
SELECT 
	RecHubData.dimBatchSources.BatchSourceKey
FROM 
	RecHubData.dimBatchSources
	INNER JOIN @BatchSources AS BatchSources ON UPPER(BatchSources.ShortName) =UPPER(RecHubData.dimBatchSources.ShortName)
WHERE
	RecHubData.dimBatchSources.ImportTypeKey = @ImportTypeKey;

/* Get a list of the bank/workgroups that have batches with the old batch sources */
;WITH BankList AS
(
	SELECT DISTINCT 
		SiteBankID,
		SiteClientAccountID
	FROM 
		RecHubData.factBatchSummary
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
		INNER JOIN @BatchSourceKeys AS BatchSourceKeys ON BatchSourceKeys.BatchSourceKey = RecHubData.factBatchSummary.BatchSourceKey
	UNION ALL
	SELECT DISTINCT 
		SiteBankID,
		SiteClientAccountID
	
	FROM 
		RecHubData.dimWorkgroupDataEntryColumns
		INNER JOIN @BatchSourceKeys AS BatchSourceKeys ON BatchSourceKeys.BatchSourceKey = RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey
)
INSERT INTO @BankList(SiteBankID,SiteClientAccountID)
SELECT DISTINCT 
	SiteBankID,
	SiteClientAccountID
FROM 
	BankList;

WHILE( @BankLoop <= (SELECT MAX(RowID) FROM @BankList) )
BEGIN
	SELECT 
		@SiteBankID = SiteBankID,
		@SiteClientAccountID = SiteClientAccountID
	FROM
		@BankList
	WHERE
		RowID = @BankLoop;

	SELECT @EntityID = EntityID FROM RecHubData.dimBanks WHERE SiteBankID = @SiteBankID AND MostRecent = 1;

	RAISERROR(N'Updating Payment Sources for BankID: %d, ClientAccountID: %d, EntityID: %d.',10,1,@SiteBankID,@SiteClientAccountID,@EntityID) WITH NOWAIT;

	/* Create the missing batch sources */
	SET @BatchSourceLoop = 1;
	WHILE( @BatchSourceLoop <= (SELECT MAX(RowID) FROM @BatchSources) )
	BEGIN
		SELECT
			@ShortName = ShortName,
			@OldShortName = ShortName
		FROM
			@BatchSources
		WHERE
			RowID = @BatchSourceLoop;

		RAISERROR(N'Processing Payment Source "%s".',10,1,@ShortName) WITH NOWAIT;

		SET @ShortName = @ShortName + '-I' + CAST(@SiteBankID AS VARCHAR(10));
		IF NOT EXISTS( SELECT 1 FROM RecHubData.dimBatchSources WHERE UPPER(ShortName) = UPPER(@ShortName) )
		BEGIN
			INSERT INTO RecHubData.dimBatchSources(IsActive,ImportTypeKey,CreationDate,CreatedBy,ModificationDate,ModifiedBy,EntityID,ShortName,LongName)
			SELECT 1,@ImportTypeKey,GETDATE(),'ConversionScript',GETDATE(),'ConversionScript',@EntityID,@ShortName,@ShortName;
		END
		ELSE
		BEGIN
			RAISERROR(N'%s already exists, skipping.',10,1,@ShortName) WITH NOWAIT;
		END

		/* Get old BatchSourceKey just to be sure */
		SELECT @OldBatchSourceKey = BatchSourceKey FROM RecHubData.dimBatchSources WHERE ShortName = @OldShortName;

		/* Get the new BatchSourceKey */
		SELECT @BatchSourceKey = BatchSourceKey FROM RecHubData.dimBatchSources WHERE ShortName = @ShortName;

		RAISERROR(N'Changing "%s" (Key: %d) to "%s" (Key: %d).',10,1,@OldShortName,@OldBatchSourceKey,@ShortName,@BatchSourceKey) WITH NOWAIT;
		/* Update the data entry column keys */
		UPDATE 
			RecHubData.dimWorkgroupDataEntryColumns
		SET 
			BatchSourceKey = @BatchSourceKey
		FROM 
			RecHubData.dimWorkgroupDataEntryColumns
		WHERE 
			SiteBankID = @SiteBankID 
			AND SiteClientAccountID = @SiteClientAccountID
			AND BatchSourceKey = @OldBatchSourceKey;

		RAISERROR(N'Updated %5d RecHubData.dimWorkgroupDataEntryColumns rows.',10,1, @@ROWCOUNT) WITH NOWAIT;

		SET @FactTableLoop = 1;
		/* Now update all the fact tables with the new integraPAY-Ixxx BatchSourceKey */
		WHILE( @FactTableLoop <= (SELECT MAX(RowID) FROM @FactTables) )
		BEGIN
			SELECT
				@TableName = TableName
			FROM
				@FactTables
			WHERE 
				RowID = @FactTableLoop;

			SET @SQLCommand = N'
				UPDATE 
					RecHubData.' + @TableName + N'
				SET 
					BatchSourceKey = @BatchSourceKey
				FROM 
					RecHubData.' + @TableName + N'
					INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.ClientAccountKey = RecHubData.' + @TableName + N'.ClientAccountKey
				WHERE 
					SiteBankID = @SiteBankID 
					AND SiteClientAccountID = @SiteClientAccountID
					AND BatchSourceKey = @OldBatchSourceKey;'

			SET @SQLParams = N'@BatchSourceKey SMALLINT,
				@SiteBankID INT,
				@SiteClientAccountID INT,
				@OldBatchSourceKey SMALLINT';

			EXECUTE sp_executesql 
				@SQLCommand, 
				@SQLParams,
				@BatchSourceKey = @BatchSourceKey,
				@SiteBankID = @SiteBankID,
				@SiteClientAccountID = @SiteClientAccountID,
				@OldBatchSourceKey = @OldBatchSourceKey;

			RAISERROR(N'Updated %5d RecHubData.%s rows.',10,1,@@ROWCOUNT,@TableName) WITH NOWAIT;

			SET @FactTableLoop += 1;
		END
		SET @BatchSourceLoop += 1;
	END
	SET @BankLoop += 1;
END