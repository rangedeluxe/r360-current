--WFSScriptProcessorDoNotFormat
/*********************************************************************************************
* Use this script to create a new workgroup mapping to an ACH Addenda template		         *
*		Set @BankID and @ WorkgroupID (in the declare statement) equal to workgroup to add   *
*		Set @ACHAddendaName equal to Addenda template to map the Workgroup to.               *
*********************************************************************************************/
DECLARE @BankID			INT = 999,								--Enter numeric value
		@WorkgroupID	INT = 9999,								--Enter numeric value
		@ACHAddendaName VARCHAR(64) = 'Standard ACH Template';	--Enter name of Addenda template

IF NOT EXISTS(SELECT AddendaName FROM RecHubConfig.ACHAddenda WHERE AddendaName = @ACHAddendaName)
	RAISERROR('ACH Addenda Name does not exist',10,1);


IF EXISTS (SELECT 1 FROM RecHubConfig.ACHAddendaWorkgroups
					INNER JOIN RecHubConfig.ACHAddenda ON RecHubConfig.ACHAddenda.ACHAddendaKey = RecHubConfig.ACHAddendaWorkgroups.ACHAddendaKey
					 WHERE RecHubConfig.ACHAddenda.AddendaName = @ACHAddendaName
					 AND RecHubConfig.ACHAddendaWorkgroups.SiteBankID = @BankID
					 AND RecHubConfig.ACHAddendaWorkgroups.SiteClientAccountID = @WorkgroupID)
	RAISERROR('The Workgroup is already mapped to the ACH Addenda template.',10,1);

INSERT INTO RecHubConfig.ACHAddendaWorkgroups 
(ACHAddendaKey,SiteBankID,SiteClientAccountID)
VALUES 
	(
	(SELECT ACHAddendaKey FROM RecHubConfig.ACHAddenda WHERE AddendaName = @ACHAddendaName)
	,@BankID
	,@WorkgroupID
	);




