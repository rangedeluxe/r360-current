﻿param
(
	[parameter(Mandatory = $true)][int] $BankID,
	[parameter(Mandatory = $true)][int] $WorkgroupID,
	[parameter(Mandatory = $true)][datetime] $DepositDate,
	[parameter(Mandatory = $true)][datetime] $ImmutableDate,
	[parameter(Mandatory = $true)][int] $BatchID,
	[parameter(Mandatory = $true)][string] $PaymentSource
)

[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms") | Out-Null;


$ScriptVerison = "2.01";

Write-Host "R360 Delete Batch PowerShell Script Version " $ScriptVerison

################################################################################
## WAUSAU Financial Systems (WFS)
## Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
################################################################################
################################################################################
## DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
################################################################################
# Copyright © 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
# other trademarks cited herein are property of their respective owners.
# These materials are unpublished confidential and proprietary information
# of WFS and contain WFS trade secrets.  These materials may not be used, 
# copied, modified or disclosed except as expressly permitted in writing by 
# WFS (see the WFS license agreement for details).  All copies, modifications 
# and derivative works of these materials are property of WFS.
################################################################################
#
# 08/28/2013 WI 112912 	JPB	Created
# 02/23/2015 WI 165894	JPB	FP to R360
#
################################################################################

################################################################################
# Write message to log file.
################################################################################
function Write-Log 
{
	param
	(
		[String] $local:LogFileName,
		[String] $local:LogInformation
	)
	begin
	{
		if( [System.IO.Path]::GetDirectoryName($local:LogFileName) -eq $null )
		{
			$local:LogFileName = Get-Location + "\" + $local:LogFileName;
		}
	}
	process
	{
		if( $_ -ne $null )
		{
			(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $_ | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
		}
	}
	end
	{
		(Get-Date –f "yyyy-MM-dd HH:mm:ss") + " " + $local:LogInformation | Out-File -FilePath $local:LogFileName -Encoding unicode -Append;
	}
}

################################################################################
# Open the database connection
################################################################################
function Create-DatabaseConnection
{
	param
	(
		[ref] $local:dbConnection,
		[string] $local:DBServer,
		[string] $local:DBName,
		[string] $local:LogFile
	)
	$local:dbConnectionOptions = ("Data Source=$local:DBServer; Initial Catalog=$local:DBName;Integrated Security=SSPI")
	$local:dbConnection.value = New-Object System.Data.SqlClient.SqlConnection($local:dbConnectionOptions);

	#Setup a handler so the print/raiseerror information from the SP is written to the results file.
	#Adapted from http://sqlskills.com/blogs/jonathan/post/Capturing-InfoMessage-Output-%28PRINT-RAISERROR%29-from-SQL-Server-using-PowerShell.aspx
	$handler = [System.Data.SqlClient.SqlInfoMessageEventHandler]{
		param($sender, $event) 
		Write-Log $local:LogFile $event.Message 
		if( $event.Message.Contains("Error") -or $event.Message.Contains("failed" ) )
		{
			$Results = $false;
		}
	};
	$local:dbConnection.value.add_InfoMessage($handler);
	$local:dbConnection.value.FireInfoMessageEventOnUserErrors = $true;
}

################################################################################
# Open the database connection
################################################################################
function Open-DatabaseConnection
{
	param
	(
		[ref] $local:dbConnection,
		[String] $LogFile
	)
	try
	{
		$local:dbConnection.value.Open();
		$Results = $true;
	}
	catch
	{
		Write-Log $LogFile "Error Opening Database Connection";
		Write-Log $LogFile $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# Query the database for batch
################################################################################
function Check-BatchExists
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[int] $local:SiteClientAccountID,
		[int] $local:DepositDateKey,
		[int] $local:ImmutableDateKey,
		[int] $local:SourceBatchID,
		[string] $local:BatchSource
	)
	$Results = $false;
	try
	{
	
		$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$local:dbCommand.Connection = $local:dbConnection;
		$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$local:dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$local:dbCommand.CommandText = "RecHubData.usp_factBatch_Exists";
		$local:dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;
		$local:dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", $local:SiteClientAccountID) | Out-Null;
		$local:dbCommand.Parameters.AddWithValue("@parmSourceBatchID", $local:SourceBatchID) | Out-Null;
		$local:dbCommand.Parameters.AddWithValue("@parmDepositDateKey", $local:DepositDateKey) | Out-Null;
		$local:dbCommand.Parameters.AddWithValue("@parmImmutableDateKey", $local:ImmutableDateKey) | Out-Null;
		$local:dbCommand.Parameters.AddWithValue("@parmBatchSource", "$local:BatchSource") | Out-Null;
		$local:dbCommand.Parameters.Add("@parmBatchExists",[System.Data.SqlDbType]::Bit) | Out-Null;
		$local:dbCommand.Parameters["@parmBatchExists"].Direction = [System.Data.ParameterDirection]::Output

		if( $local:dbCommand.ExecuteNonQuery() -eq -1 )
		{
			$Results = $local:dbCommand.Parameters["@parmBatchExists"].Value;
		}
	}
	catch
	{
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
		$Results = $false;
	}
	return $Results;
}

################################################################################
# Query the database for bank name
################################################################################
function Get-BankName
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[ref] $local:BankName
	)
	
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_dimBanks_Get_BySiteBankID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;

		$reader = $dbCommand.ExecuteReader();
		if( $reader.HasRows )
		{
			while($reader.Read()) 
			{
				$local:BankName.value = $reader['BankName']; 
			}
			$Results = $true;
		}
		else
		{
			Write-Host ("Could not locate BankID {0}. Aborting....." -f $local:SiteBankID);
			Write-Log $LogFile ("Could not locate BankID {0}." -f $local:SiteBankID);
			$Results = $false;
		}
		
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Query the database for workgroup name
################################################################################
function Get-WorkgroupName
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[int] $local:SiteBankID,
		[int] $local:SiteClientAccountID,
		[ref] $local:ClientAccountName
	)
	try
	{
		$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
		$dbCommand.Connection = $local:dbConnection;
		$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
		$dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
		$dbCommand.CommandText = "RecHubData.usp_dimClientAccounts_Get_BySiteBankIDSiteClientAccountID";
		$dbCommand.Parameters.AddWithValue("@parmSiteBankID", $local:SiteBankID) | Out-Null;
		$dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", $local:SiteClientAccountID) | Out-Null;

		$reader = $dbCommand.ExecuteReader();
		if( $reader.HasRows )
		{
			while($reader.Read()) 
			{
				$local:ClientAccountName.value = $reader['LongName']; 
			}
			$Results = $true;
		}
		else
		{
			Write-Host ("Could not locate WorkgroupID {0}. Aborting....." -f $local:SiteClientAccountID);
			Write-Log $LogFile ("Could not locateWorkgroupID {0}." -f $local:SiteClientAccountID);
			$Results = $false;
		}
		
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}

################################################################################
# Delete batch
############################################################################
function Delete-Batch
{
	param
	(
		[System.Data.SqlClient.SqlConnection] $local:dbConnection,
		[String] $LogFile
	)
	$Results = $false;
	$local:dbCommand = New-Object System.Data.SqlClient.SqlCommand;
	$local:dbCommand.Connection = $local:dbConnection;
	$local:dbCommand.CommandTimeout = 21600; #give it 6 hours to run
	$local:dbCommand.CommandType = [System.Data.CommandType]::StoredProcedure;
	$local:dbCommand.CommandText = "RecHubData.usp_factBatch_Delete";
	$local:dbCommand.Parameters.AddWithValue("@parmSiteBankID", "$BankID") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmSiteClientAccountID", "$WorkgroupID") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmSourceBatchID", "$BatchID") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmDepositDateKey", "$DepositDateKey") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmImmutableDateKey", "$ImmutableDateKey") | Out-Null;
	$local:dbCommand.Parameters.AddWithValue("@parmBatchSource", "$PaymentSource") | Out-Null;
	$local:dbCommand.Parameters.Add("@parmRowDeleted",[System.Data.SqlDbType]::Bit) | Out-Null;
	$local:dbCommand.Parameters["@parmRowDeleted"].Direction = [System.Data.ParameterDirection]::Output
	try
	{
		if( $local:dbCommand.ExecuteNonQuery() -eq -1 )
		{
			$Results = $local:dbCommand.Parameters["@parmRowDeleted"].Value;
		}
	}
	catch
	{
		$Results = $false;
		Write-Log $LogFile "Error executing SQL Command";
		Write-Log $LogFile $_.Exception.Message;
	}
	return $Results;
}


################################################################################
# Main
################################################################################
[String] $BankName = "";
[String] $WorkgroupName = "";
[System.Data.SqlClient.SqlConnection] $dbConnection = $null;

$ConfigFile = [System.IO.Path]::GetDirectoryName($MyInvocation.MyCommand.Definition) + "\R360Delete.Config"

if( (Test-Path $ConfigFile) -ne $true )
{
	Write-Host $ConfigFile "not found, aborting.....";
	return;
}

$UserName = $env:username;

$xml = New-Object "System.Xml.XmlDocument";
$xml.load($ConfigFile);

$LogFolder = $xml.Config.LogFolder;
$DBServer = $xml.Config.DBServer;
$DBName = $xml.Config.DBName;

if( !$LogFolder.EndsWith("\") )
{
	$LogFolder += "\";
}

$LogFile = $LogFolder + [system.io.path]::GetFilenameWithoutExtension($MyInvocation.MyCommand.Definition) + "_" + (Get-Date –f yyyyMMdd) + ".txt";

#Convert datetime to keys
$DepositDateKey = ($DepositDate.Year*10000)+($DepositDate.Month*100)+($DepositDate.Day);
$ImmutableDateKey = ($ImmutableDate.Year*10000)+($ImmutableDate.Month*100)+($ImmutableDate.Day);

Create-DatabaseConnection ([ref]$dbConnection) $DBServer $DBName $LogFile;

if( (Open-DatabaseConnection ([ref]$dbConnection) $LogFile) )
{
	Write-Host "Finding batch ..... " -NoNewline;
	if( !(Check-BatchExists $dbConnection $BankID $WorkgroupID $DepositDateKey $ImmutableDateKey $BatchID $PaymentSource) )
	{
		Write-Host "Batch not found";
		$dbConnection.Close();

		return;
	}
	Write-Host "Complete";
	$dbConnection.Close();
}

Write-Log $LogFile "-------------------------------------------------------------------------------";
Write-Log $LogFile ("R360 Delete Batch PowerShell Script Version {0}" -f $ScriptVerison);
Write-Log $LogFile ("Database Server {0}" -f $DBServer);
Write-Log $LogFile ("Database Name   {0}" -f $DBName);
Write-Log $LogFile ("User            {0}" -f $UserName);


if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
{
	Write-Host "Getting workgoup information ..... " -NoNewline;
	$Results = Get-BankName $dbConnection $BankID ([ref]$BankName);
	if( $Results )
	{
		$dbConnection.Close();
		if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
		{
			$Results = Get-WorkgroupName $dbConnection $BankID $WorkgroupID ([ref]$WorkgroupName);
		}
	}
	$dbConnection.Close();
	if( $Results )
	{
		Write-Host "Complete";
	}

}

if( $Results )
{
	[string]$Message = [string]::Format("Are you sure you want to delete: `r`n`r`nBank: {0} - {1}`r`nWorkgroup: {2} - {3}`r`nBatch: {4}`r`nDeposit Date: {5,2:00}/{6,2:00}/{7}`r`nImmutable Date: {8,2:00}/{9,2:00}/{10}",$BankID,$BankName,$WorkgroupID,$WorkgroupName,$BatchID,$DepositDate.Month,$DepositDate.Day,$DepositDate.Year,$ImmutableDate.Month,$ImmutableDate.Day,$ImmutableDate.Year);
	$Response = [System.Windows.Forms.MessageBox]::Show($Message , "Delete Batch" , [System.Windows.Forms.MessageBoxButtons]::YesNo,[System.Windows.Forms.MessageBoxIcon]::Warning,[System.Windows.Forms.MessageBoxDefaultButton]::Button2)

	Write-Log $LogFile ("Bank            {0} ({1})" -f $BankID,$BankName.TrimEnd());
	Write-Log $LogFile ("Workgroup       {0} ({1})" -f $WorkgroupID,$WorkgroupName.TrimEnd());
	Write-Log $LogFile ("Batch           {0}" -f $BatchID);
	Write-Log $LogFile ("Deposit Date    {0,2:00}/{1,2:00}/{2}" -f $DepositDate.Month,$DepositDate.Day,$DepositDate.Year);
	Write-Log $LogFile ("Immutable Date  {0,2:00}/{1,2:00}/{2}" -f $ImmutableDate.Month,$ImmutableDate.Day,$ImmutableDate.Year);
	Write-Log $LogFile ("Payment Source  {0}" -f $PaymentSource);

	if( $Response -eq "NO" )
	{
		Write-Log $LogFile "Cancelled delete.";
		$Results = $false;
	}
	else
	{
		Write-Host ("Bank                 {0}" -f $BankID);
		Write-Host ("Workgrup             {0}" -f $WorkGroupID);
		Write-Host ("Batch                {0}" -f $BatchID);
		Write-Host ("Deposit Date         {0,2:00}/{1,2:00}/{2}" -f $DepositDate.Month,$DepositDate.Day,$DepositDate.Year);
		Write-Host ("Immutable Date       {0,2:00}/{1,2:00}/{2}" -f $ImmutableDate.Month,$ImmutableDate.Day,$ImmutableDate.Year);
		Write-Host ("Payment Source       {0}" -f $PaymentSource);
		Write-Host ("Log File            {0}" -f $LogFile);

		if( Open-DatabaseConnection ([ref]$dbConnection) $LogFile )
		{
			Write-Host "Deleting batch ..... " -NoNewline;
			$Results = Delete-Batch $dbConnection $LogFile
			$dbConnection.Close();
			Write-Host "Complete";
		}
	}
}

if( $Results )
{
	Write-Host "Successfully deleted batch."
	Write-Log $LogFile "Successfully deleted batch."
}
else
{
	if( $Response -ne "NO" )
	{
		Write-Host "Failed deleting batch. See log file for details." -ForegroundColor Red;
		Write-Log $LogFile "Failed deleting batch. See error message above.";
	}
	else 
	{
		Write-Host "Cancelled delete";
	}
}
Write-Log $LogFile "-------------------------------------------------------------------------------";
if( $dbConnection.State -eq [System.Data.ConnectionState]::Open )
{
	$dbConnection.Close();
}

