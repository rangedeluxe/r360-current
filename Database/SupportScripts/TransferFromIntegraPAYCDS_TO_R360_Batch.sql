/* Script Name = TransferFromIntegraPAYCDS_TO_R360_Batch.sql			*/
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/18/2016
*
* Purpose: Copies Batch (QueueType 10) record from IntegraPAY CDS database to R360 Database by loading the data into the CDSQueue.
*		This is to be run in a Query window connected to the IntegraPAY CDS database
*
* Replace variable values:
*   <bankid> <LockboxID> <BatchID> <ProcessingDate> <DepositDate> <ActionCode> <BatchSourceShortName>  
*	Example SET @BankID = 1; 
*
*	Action code Choices:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* Modification History
* 05/18/2016 WI 280153	Created
******************************************************************************/

DECLARE @BankID		INT,
		@LockboxID	INT,
		@BatchID	INT,
		@ProcessingDate			DATETIME,
		@DepositDate			DATETIME,
		@ActionCode				INT,
		@BatchSourceShortName	VARCHAR(10);

SET NOCOUNT ON

/*
replace <VariableName(s)> with the Value that will be copied to R360 from integraPAY
for example, to copy bank 1 the SET will be:
SET @BankID = 1;

Format for Date fields to be input as  'CCYY-MM-DD'
BatchSourceShortName corresponds to dbo.BatchSource.SourceShortName  Value
*/

SET @BankID = <bankid>; 
SET @LockboxID = <LockboxID>; 
SET @BatchID = <BatchID>; 
SET @ProcessingDate = '<ProcessingDate>'; 
SET @DepositDate = '<DepositDate>'; 
SET @ActionCode = <ActionCode>; 
SET @BatchSourceShortName = '<BatchSourceShortName>'; 

EXEC RecHub.usp_CDSQueue_Ins_Batch	@parmBankID = @BankID, 
									@parmLockboxID = @LockboxID, 
									@parmBatchID = @BatchID,
									@parmProcessingDate = @ProcessingDate, 
									@parmDepositDate = @DepositDate, 
									@parmActionCode = @ActionCode,
									@parmBatchSourceShortName = @BatchSourceShortName;