/*

Database: R360

Purpose: Display the statics of R360 fact tables

Usage:
1)	Run the script to view the current statics of the fact tables. The last column (% Rows Changed) shows the percentage of rows changed since the last time the table stats were updated.
2)	Run the RecHubFactTableStats SQL Agent job.
3)	Run the script to view the stats again. There may or may not be an improvement in the last column based on the last time the job was run.

*/

SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED
SELECT
	sys.schemas.name AS SchemaName,
	sys.tables.name AS TableName,
	sys.sysindexes.name AS IndexName,
	STATS_DATE(sys.sysindexes.id,sys.sysindexes.indid) AS 'Statistics Last Updated',
	sys.sysindexes.rowcnt AS 'Row Count',
	sys.sysindexes.rowmodctr AS 'Number Of Changes',
	CAST((CAST(sys.sysindexes.rowmodctr AS DECIMAL(28,8))/CAST(sys.sysindexes.rowcnt AS	DECIMAL(28,2)) * 100.0)	AS DECIMAL(28,2)) AS '% Rows Changed'
FROM 
	sys.sysindexes
	INNER JOIN sys.tables ON sys.tables.[object_id] = sys.sysindexes.[id]
	INNER JOIN sys.schemas ON sys.schemas.[schema_id] = sys.tables.[schema_id]
WHERE 
	sys.sysindexes.id > 100
    AND sys.sysindexes.indid > 0
    AND sys.sysindexes.rowcnt >= 500
    AND sys.tables.name IN 
	(
		'factChecks', 
		'factTransactionSummary', 
		'dimDataEntryColumns', 
		'factBatchSummary', 
		'factDocuments', 
		'factDataEntryDetails'
	)
ORDER BY 
	SchemaName, 
	TableName, 
	IndexName;
