/*

Database: Consolidated

Purpose: Emulate how Consolidator inserts records into the Consolidated database RecHub.CDSQueue table. This table 
is used by the OTIS SSIS job to copy records from the Consolidated database to the R360 database. 

Usage: Each stored procedure should be called individually. Replace the parm examples with the data that should be 
inserted into the RecHub.CDSQueue table. There are SELECT statement examples above each SP to show how to retrieve 
the data necessary to fill in the parm values.

*/

/* Process site code */
--SELECT * FROM dbo.SiteCodes
EXEC RecHub.usp_CDSQueue_Ins_SiteCode
       @parmSiteCodeID=;

/* Process a bank record */
--SELECT * FROM dbo.Bank
EXEC RecHub.usp_CDSQueue_Ins_Bank
       @parmBankID=;

/* Process a lockbox record */
--SELECT * FROM dbo.Lockbox
EXEC RecHub.usp_CDSQueue_Ins_Lockbox
       @parmBankID=,
       @parmCustomerID=,
       @parmLockboxID=;

/* Process a data entry */
--SELECT * FROM dbo.DESetup
EXEC RecHub.usp_CDSQueue_Ins_DataEntrySetup
       @parmDESetupID=;

/* Process document record */
--SELECT * FROM dbo.DocumentTypes
EXEC RecHub.usp_CDSQueue_Ins_DocumentType
       @parmFileDescriptor='CO';

/* Process batch record */
/*
This select will return all records in the batch table. The columns are needed for the stored procedure call, use the DESetupID for the RecHub.usp_CDSQueue_Ins_DataEntrySetup SP

SELECT BankID,LockboxID,BatchID,ProcessingDate,DepositDate,SourceShortName,DESetupID FROM dbo.Batch INNER JOIN dbo.BatchSource ON dbo.BatchSource.[BatchSourceKey ] = dbo.Batch.BatchSourceKey

Example data filled in. Do no change the ActionCode value.
*/
EXEC RecHub.usp_CDSQueue_Ins_Batch
       @parmBankID=5002,
       @parmLockboxID=9210,
       @parmBatchID=1687,
       @parmProcessingDate='2016-09-14',
       @parmDepositDate='2016-09-14',
       @parmActionCode=1, --do not change
       @parmBatchSourceShortName='integraPAY';
