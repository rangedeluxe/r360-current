/*

Database: R360

Purpose: Check what partitions will be deleted and and verify results of the DataImportQueuePartitionMaintenance 
SQL Agent Job

Usage: Run the "Pre Execute" SQL statements to view what partitions will be removed by the SQL Agent job. The
script will return a list of the partitions that will be removed by running the SQL Agent job.

Run the "Post Execute" SQL statements to verify the results. This will display all the partitions in date order. The 
first row is the catch all partition and will always be there. The next row will be the oldest partition and the row will 
be the newest partition. As part of the stored procedure that is execute by the SQL Agent job, new partitions will be 
added to ensure there is always one years� worth of partitions on the system. Keep this in mind when reviewing the results.

For best results, run the "Pre Execute" SQL statements 
in one window and the "Post Execute" SQL statements in another window.

*/


/* Pre Execute check */
DECLARE @DITRetentionDays INT,
		@OldestCalendarDateToKeep DateTime,
		@RangeDateKey INT;

SELECT 
	@DITRetentionDays = [Value] 
FROM 
	RecHubConfig.SystemSetup 
WHERE 
	Section = 'Rec Hub Table Maintenance' 
	AND SetupKey = 'DataImportQueueRetentionDays';

SELECT @OldestCalendarDateToKeep = GETDATE() - @DITRetentionDays;

SELECT @RangeDateKey = (Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @OldestCalendarDateToKeep AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC);

SELECT 'Partitions to be removed from RecHubSystem.DataImportQueue (Retention Days ' + CAST(@DITRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition  FROM RecHubSystem.FileGroupDetailView WHERE partition_scheme_name = 'DataImport' and range_value <= @RangeDateKey ORDER BY partition_number;;
SELECT 'Partitions to remain in RecHubSystem.DataImportQueue';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM RecHubSystem.FileGroupDetailView WHERE partition_scheme_name = 'DataImport' and range_value > @RangeDateKey ORDER BY partition_number;;

/* Post Execute check */
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM RecHubSystem.FileGroupDetailView WHERE partition_scheme_name = 'DataImport' ORDER BY partition_number;
