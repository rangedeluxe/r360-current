/*

Database: SSIS Logging

Purpose: Check what partitions will be deleted and and verify results of the SISLoggingDatabaseMaintenance 
SQL Agent Job

Usage: Run the "Pre Execute" SQL statements to view what partitions will be removed by the SQL Agent job. The
script will return a list of the partitions that will be removed by running the SQL Agent job.

Run the "Post Execute" SQL statements to verify the results. This will display all the partitions in date order. The 
first row is the catch all partition and will always be there. The next row will be the oldest partition and the row will 
be the newest partition. As part of the stored procedure that is execute by the SQL Agent job, new partitions will be 
added to ensure there is always one years� worth of partitions on the system. Keep this in mind when reviewing the results.

The following tables are handled by this process:
BatchLog
PackageLog
PackageErrorLog
PackageTaskLog
PackageVariableLog

For best results, run the "Pre Execute" SQL statements 
in one window and the "Post Execute" SQL statements in another window.

*/


/* Pre Execute check */
DECLARE @DITRetentionDays INT,
		@OldestCalendarDateToKeep DateTime,
		@RangeDateKey INT,
		@JobID UNIQUEIDENTIFIER,
		@LogRetentionDays INT;

SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '(WFS_SSIS_Logging) SSISLoggingDatabaseMaintenance';

SELECT @LogRetentionDays=CAST(SUBSTRING(command,CHARINDEX('@parmLogRetentionDays=',command)+22,LEN(command)-1) AS INT) FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID and step_id = 1;
SELECT @OldestCalendarDateToKeep = GETDATE() - @LogRetentionDays;


IF( DATEPART(WEEKDAY, @OldestCalendarDateToKeep) <> 2 )
BEGIN
	SELECT 
		@OldestCalendarDateToKeep = 
			CASE DATEPART(WEEKDAY, @OldestCalendarDateToKeep)
				WHEN 1 THEN DATEADD(dd,-6,@OldestCalendarDateToKeep)					
				WHEN 3 THEN DATEADD(dd,-1,@OldestCalendarDateToKeep)
				WHEN 4 THEN DATEADD(dd,-2,@OldestCalendarDateToKeep)
				WHEN 5 THEN DATEADD(dd,-3,@OldestCalendarDateToKeep)
				WHEN 6 THEN DATEADD(dd,-4,@OldestCalendarDateToKeep)
				WHEN 7 THEN DATEADD(dd,-5,@OldestCalendarDateToKeep)
			END;
END

SELECT @RangeDateKey = CAST(CONVERT(CHAR(8),@OldestCalendarDateToKeep,112) AS INT);

SELECT 'Partitions to be removed from BatchLog (Retention Days ' + CAST(@LogRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'BatchLog' AND range_value <= @RangeDateKey ORDER BY partition_number;
SELECT 'Partitions to remain in BatchLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'BatchLog' AND range_value > @RangeDateKey ORDER BY partition_number;;

SELECT 'Partitions to be removed from PackageLog (Retention Days ' + CAST(@LogRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageLog' AND range_value <= @RangeDateKey ORDER BY partition_number;
SELECT 'Partitions to remain in PackageLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageLog' AND range_value > @RangeDateKey ORDER BY partition_number;;

SELECT 'Partitions to be removed from PackageErrorLog (Retention Days ' + CAST(@LogRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageErrorLog' AND range_value <= @RangeDateKey ORDER BY partition_number;
SELECT 'Partitions to remain in PackageErrorLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageErrorLog' AND range_value > @RangeDateKey ORDER BY partition_number;;

SELECT 'Partitions to be removed from PackageTaskLog (Retention Days ' + CAST(@LogRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageTaskLog' AND range_value <= @RangeDateKey ORDER BY partition_number;
SELECT 'Partitions to remain in PackageTaskLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageTaskLog' AND range_value > @RangeDateKey ORDER BY partition_number;;

SELECT 'Partitions to be removed from PackageVariableLog (Retention Days ' + CAST(@LogRetentionDays AS VARCHAR(10)) + ')';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageVariableLog' AND range_value <= @RangeDateKey ORDER BY partition_number;
SELECT 'Partitions to remain in PackageVariableLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageVariableLog' AND range_value > @RangeDateKey ORDER BY partition_number;;

/* Post Execute check */
SELECT 'Partitions for BatchLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'BatchLog' ORDER BY partition_number;
SELECT 'Partitions for PackageLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageLog' ORDER BY partition_number;
SELECT 'Partitions for PackageErrorLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageErrorLog' ORDER BY partition_number;
SELECT 'Partitions for PackageTaskLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageTaskLog' ORDER BY partition_number;
SELECT 'Partitions for PackageVariableLog';
SELECT partition_number AS PartitionNumber, partition_filegroup AS 'FileGroup', range_value AS PartitionKey, num_rows AS RowsInPartition FROM LoggingSystem.FileGroupDetailView WHERE partition_scheme_name = 'WFSSSISLogging' AND object_name = 'PackageVariableLog' ORDER BY partition_number;

