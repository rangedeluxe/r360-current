/*

Database: master

Purpose: Used to display the indexes that will be updated by the (WFS) IndexMaintenance SQL Agent Job.

Usage:
1)	Run the script before running the (WFS) IndexMaintenance job.
2)	Execute the (WFS) IndexMaintenance job.
3)	Run the script. The results should be 0 rows.
	a) Note that there may be rows returned in some cases. This may happen when a table does not have many rows.

Dev Notes: The index check is done with dynamic SQL because sys.dm_db_index_physical_stats uses the current 
database. The MS docs say you can pass a DB_ID into the system stored procedure, but it does not seem to work.

*/

DECLARE @DBName NVARCHAR(256),
		@Loop SMALLINT = 1,
		@JobID UNIQUEIDENTIFIER,
		@SQLCommand NVARCHAR(MAX);

DECLARE @DBList TABLE
(
	RowID SMALLINT IDENTITY,
	DBName SYSNAME
);

SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '(WFS) IndexMaintenance';

INSERT INTO @DBList(DBName)
SELECT SUBSTRING(step_name,CHARINDEX('(',step_name)+1,CHARINDEX(')',step_name)-2) FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @DBList) )
BEGIN

	SELECT 
		@DBName = DBName
	FROM 
		@DBList DBList
	WHERE 
		RowID = @Loop;

	SET @SQLCommand = 'USE ' + @DBName;

	SET @SQLCommand+= ';SELECT  ''['' + DB_NAME() + ''].['' + OBJECT_SCHEMA_NAME(ddips.[object_id],DB_ID()) + ''].[''+ OBJECT_NAME(ddips.[object_id], DB_ID()) + '']'' AS DatabaseName,
        i.[name] AS IndexName,
        ddips.[index_type_desc] AS IndexType,
        ddips.[partition_number] AS PartitionNumber,
        ddips.[alloc_unit_type_desc] AS AllocationType,
        ddips.[index_depth] AS IndexLevel,
        ddips.[index_level] AS IndexLevel,
        CAST(ddips.[avg_fragmentation_in_percent] AS SMALLINT) AS [AvgFrag%] ,
        CAST(ddips.[avg_fragment_size_in_pages] AS SMALLINT) AS AvgFragSizeInPages,
        ddips.[fragment_count] AS FragmentCount,
        ddips.[page_count] AS [PageCount]
FROM    sys.dm_db_index_physical_stats(DB_ID(), NULL,
                                         NULL, NULL, ''limited'') ddips
        INNER JOIN sys.[indexes] i ON ddips.[object_id] = i.[object_id]
                                       AND ddips.[index_id] = i.[index_id]
WHERE   ddips.[avg_fragmentation_in_percent] > 15
        AND ddips.[page_count] > 500
ORDER BY ddips.[avg_fragmentation_in_percent] desc,
        OBJECT_NAME(ddips.[object_id], DB_ID()) ,
        i.[name];';

	EXEC(@SQLCommand);
	SET @Loop+=1;
END