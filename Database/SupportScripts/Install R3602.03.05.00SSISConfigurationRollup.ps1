﻿#Version of SQL the install is going against
#Valid values: 2012, 2016
$SQLVersion="2012"
#Name of the SQL Database server
$DBSERVER = "SERVERNAME"
#Name of the R360 Database-Default is WFSDB_R360
$R360DB = "WFSDB_R360"
#Name of the SSIS Configuration Database-Default is WFS_SSIS_Configuration
$SSISConfigDB = "WFS_SSIS_Configuration"
#Name of the SSIS Logging Database-Default is WFS_SSIS_Logging
$SSISLoggingDB = "WFS_SSIS_Logging"
#Installation Folder
$INSTALLROOT = "D:\WFSStaging\"
#Log Folder
$LOGFOLDER = "D:\WFSStaging\DB\Installlogs"


IF( $SQLVersion -eq "2016" )
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole2016"
}
ELSE
{
	$Installer = "D:\WFSStaging\DB\RecHub2.03\InstallApp\WFSDatabaseInstallConsole"
}

function RunInstaller([string] $DBServer, [string] $DBName, [string] $Release, [string] $InstallFolderPath, [string] $CommandFile)
{
	$InstallPath = [System.IO.Path]::Combine($INSTALLROOT,$Release,$InstallFolderPath);
	&"$Installer" -DBServer $DBServer -DBName $DBName -CommandFile $CommandFile -InstallationFolder $InstallPath -LogFolder $LOGFOLDER

}

Write-Host "**** Starting SSIS Configuration Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;


RunInstaller $DBSERVER $SSISConfigDB "2.00" "R360H_2.00.00.00\Database\SSIS_Configuration" "SSISConfigurationDatabase2.00.00.00.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.00.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.00" "R360H_2.00.01.00\Database\" "SSISConfigurationDatabase2.00.01.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.00.01.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.01" "R360_2.01.00.00\DB\RecHub2.1\2.00.01.00" "SSISConfigurationDatabase2.00.01.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.00.01.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.01" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00" "SSISConfigurationDatabase2.01.00.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.01.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00" "SSISConfigurationDatabase2.02.00.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.03.00\DB\RecHub2.1\2.02.03.00" "SSISConfigurationDatabase2.02.03.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.03.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.04.00\DB\RecHub2.1\2.02.04.00" "SSISConfigurationDatabase2.02.04.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.04.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.09.00\DB\RecHub2.1\2.02.09.00" "SSISConfigurationDatabase2.02.09.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.09.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.12.00\DB\RecHub2.1\2.02.12.00" "SSISConfigurationDatabase2.02.12.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.12.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.02" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00" "SSISConfigurationDatabase2.02.14.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.02.14.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.03" "R360_2.03.00.00\DB\RecHub2.03\2.03.00.00" "SSISConfigurationDatabase2.03.00.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.03.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISConfigDB "2.03" "R360_2.03.04.00\DB\RecHub2.03.04\2.03.04.00" "SSISConfigurationDatabase2.03.04.00Patch.xml";
Write-Host "**** SSIS Configuration rollup complete for 2.03.04.00" -ForegroundColor GREEN;

Write-Host '**** SSIS Configuration Rollup to Patch 2.03.05.00 has completed' -ForegroundColor GREEN | Out-Null


Write-Host "**** Starting SSIS Logging Database Installer - Roll-up to 2.03.00.00" -ForegroundColor GREEN;

RunInstaller $DBSERVER $SSISLoggingDB "2.00" "R360H_2.00.00.00\Database" "SSISLoggingDatabase2.00.00.00.xml";
Write-Host "**** SSIS Logging Database rollup complete for 2.00.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISLoggingDB "2.01" "R360_2.01.00.00\DB\RecHub2.1\2.01.00.00\" "SSISLoggingDatabase2.01.00.00Patch.xml";
Write-Host "**** SSIS Logging Database rollup complete for 2.01.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISLoggingDB "2.02" "R360_2.02.00.00\DB\RecHub2.1\2.02.00.00\" "SSISLoggingDatabase2.02.00.00Patch.xml";
Write-Host "**** SSIS Logging Database rollup complete for 2.02.00.00" -ForegroundColor GREEN;
RunInstaller $DBSERVER $SSISLoggingDB "2.02" "R360_2.02.14.00\DB\RecHub2.1\2.02.14.00\" "SSISLoggingDatabase2.02.14.00Patch.xml";
Write-Host "**** SSIS Logging Database rollup complete for 2.02.14.00" -ForegroundColor GREEN;

Write-Host '**** SSIS Logging Database Rollup to Patch 2.03.00.00 has completed' -ForegroundColor GREEN | Out-Null
