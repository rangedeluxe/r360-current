--WFSScriptProcessorDoNotFormat
/*********************************************************************************************************
*																										**		
*				Fill in the value for the AdminLoginID in the first DECLARE statement before running!!  **
*				Run this script for 2.03.04.00 BEFORE the RAAM MSI install								**
*********************************************************************************************************/
DECLARE @AdminLoginID VARCHAR(256) = 'WFSAdmin';
DECLARE @UserID INT,
		@EncryptedPassword NVARCHAR(128),
		@CreationDate DATETIME;
SELECT @CreationDate = GETDATE();
SELECT @UserID = UserID FROM dbo.Users WHERE LoginID = @AdminLoginID;
SELECT @EncryptedPassword = EncryptedPassword FROM dbo.Users WHERE LoginID = @AdminLoginID;
INSERT INTO dbo.PasswordHistory
	(UserID, EncryptedPassword)
	VALUES (@UserID, @EncryptedPassword);

