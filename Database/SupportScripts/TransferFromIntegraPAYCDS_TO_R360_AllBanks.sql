--WFSScriptProcessorDoNotFormat
/* Script Name = TransferFromIntegraPAYCDS_TO_R360_AllBanks.sql			*/
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016-2018 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016-2018 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/12/2018
*
* Purpose: Copies Bank (QueueType 20) record from IntegraPAY CDS database to R360 Database by loading the data into the CDSQueue.
*		This is to be run in a Query window connected to the IntegraPAY CDS database
*
* Modification History
* 01/12/2018 PT 154292721	Created (Based on TransferFromIntegraPAYCDS_TO_R360_Bank)
******************************************************************************/

DECLARE @BankID INT,
		@Loop INT = 1;

SET NOCOUNT ON

DECLARE @Banks TABLE
(
	RowID INT IDENTITY(1,1),
	BankID INT
);

INSERT INTO @Banks(BankID)
SELECT BankID FROM dbo.Bank ORDER BY BankID;

WHILE( @Loop <= (SELECT MAX(RowID) FROM @Banks) )
BEGIN
	SELECT @BankID = BankID FROM @Banks WHERE RowID = @Loop;

	EXEC RecHub.usp_CDSQueue_Ins_Bank @parmBankID = @BankID;

	SET @Loop+=1;
END
