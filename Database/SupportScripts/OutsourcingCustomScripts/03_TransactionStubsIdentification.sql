--WFSScriptProcessorDoNotFormat
/*********************************************************************************************************
*		This script is the first in a series to fix stub counts on the factTransaction Summary table.	**
*		This will list all transactions that have invalid StubCount values.								**
*		The transactions to be updated will be save in dbo.tempTransactionCounts						**
*********************************************************************************************************/
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tempTransactionStubCounts' 
	AND TABLE_TYPE = 'BASE TABLE' 
	AND TABLE_SCHEMA = 'dbo')
	DROP TABLE dbo.tempTransactionStubCounts;

CREATE TABLE dbo.tempTransactionStubCounts
(DepositDateKey INT,
 BankKey INT, 
 ClientAccountKey INT, 
 BatchID BIGINT, 
 TransactionID INT,
 StubCount INT,
 DetailTransactionStubsCount INT
);

;WITH DetailTransactionStubCount AS 
(
select DepositDateKey, BankKey, ClientAccountKey, BatchID, TransactionID, Count(*) AS DetailTransactionStubsCount 
from RecHubData.factStubs
WHERE IsDeleted = 0
GROUP BY DepositDateKey, BankKey, ClientAccountKey, BatchID, TransactionID)
INSERT INTO dbo.tempTransactionStubCounts
SELECT 
	DetailTransactionStubCount.DepositDateKey, 
	DetailTransactionStubCount.BankKey, 
	DetailTransactionStubCount.ClientAccountKey, 
	DetailTransactionStubCount.BatchID, 
	DetailTransactionStubCount.TransactionID,
	StubCount, 
	DetailTransactionStubsCount
FROM RecHubData.factTransactionSummary
INNER JOIN DetailTransactionStubCount 
	ON DetailTransactionStubCount.DepositDateKey = RecHubData.factTransactionSummary.DepositDateKey
	AND DetailTransactionStubCount.BankKey = RecHubData.factTransactionSummary.BankKey
	AND DetailTransactionStubCount.ClientAccountKey = RecHubData.factTransactionSummary.ClientAccountKey
	AND DetailTransactionStubCount.BatchID = RecHubData.factTransactionSummary.BatchID
	AND DetailTransactionStubCount.TransactionID = RecHubData.factTransactionSummary.TransactionID
WHERE 
	IsDeleted = 0
	AND StubCount <> DetailTransactionStubsCount
ORDER BY DetailTransactionStubCount.DepositDateKey, 
	DetailTransactionStubCount.BankKey, 
	DetailTransactionStubCount.ClientAccountKey, 
	DetailTransactionStubCount.BatchID, 
	DetailTransactionStubCount.TransactionID,
	RecHubData.factTransactionSummary.StubCount

Select * from dbo.tempTransactionStubCounts;