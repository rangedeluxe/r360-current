--WFSScriptProcessorDoNotFormat
/*****************************************************************************************************************
*			This script is the second in a series to fix stub counts on the factTransactionSummary table.		**
*			This will use the rows in dbo.tempTransactionStubCounts to update the factTransactionSummary table. **
*			The batches that were updated will remain in dbo.tempTransactionStubCounts							**
*****************************************************************************************************************/

Update RecHubData.factTransactionSummary SET RecHubData.factTransactionSummary.StubCount = DetailTransactionStubsCount
FROM RecHubData.factTransactionSummary
INNER JOIN dbo.tempTransactionStubCounts ON
factTransactionSummary.DepositDateKey = dbo.tempTransactionStubCounts.DepositDateKey
AND factTransactionSummary.BankKey = dbo.tempTransactionStubCounts.BankKey
AND factTransactionSummary.ClientAccountKey = dbo.tempTransactionStubCounts.ClientAccountKey
AND factTransactionSummary.BatchID = dbo.tempTransactionStubCounts.BatchID
AND factTransactionSummary.TransactionID = dbo.tempTransactionStubCounts.TransactionID
AND IsDeleted = 0