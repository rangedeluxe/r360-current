--WFSScriptProcessorDoNotFormat
/*********************************************************************************************************
*				This script is the first in a series to fix stub counts on the factBatchSummary table.  **
*				This will list all batches that have invalid StubCount values.							**
*				The batches to be updated will be save in dbo.tempStubCounts							**
*********************************************************************************************************/
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = 'tempStubCounts' 
	AND TABLE_TYPE = 'BASE TABLE' 
	AND TABLE_SCHEMA = 'dbo')
	DROP TABLE dbo.tempStubCounts;

CREATE TABLE dbo.tempStubCounts
(DepositDateKey INT,
 BankKey INT, 
 ClientAccountKey INT, 
 BatchID BIGINT, 
 StubCount INT,
 DetailStubsCount INT
);

;WITH DetailStubCount AS 
(
select DepositDateKey, BankKey, ClientAccountKey, BatchID, Count(*) AS DetailStubsCount 
from RecHubData.factStubs
WHERE IsDeleted = 0
GROUP BY DepositDateKey, BankKey, ClientAccountKey, BatchID)
INSERT INTO dbo.tempStubCounts
SELECT 
	DetailStubCount.DepositDateKey, 
	DetailStubCount.BankKey, 
	DetailStubCount.ClientAccountKey, 
	DetailStubCount.BatchID, 
	StubCount, 
	DetailStubsCount
FROM RecHubData.factBatchSummary
INNER JOIN DetailStubCount 
	ON DetailStubCount.DepositDateKey = RecHubData.factBatchSummary.DepositDateKey
	AND DetailStubCount.BankKey = RecHubData.factBatchSummary.BankKey
	AND DetailStubCount.ClientAccountKey = RecHubData.factBatchSummary.ClientAccountKey
	AND DetailStubCount.BatchID = RecHubData.factBatchSummary.BatchID
WHERE 
	IsDeleted = 0
	AND StubCount <> DetailStubsCount
ORDER BY DetailStubCount.DepositDateKey, 
	DetailStubCount.BankKey, 
	DetailStubCount.ClientAccountKey, 
	DetailStubCount.BatchID, 
	StubCount

Select * from dbo.tempStubCounts;


