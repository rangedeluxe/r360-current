--WFSScriptProcessorDoNotFormat
/*********************************************************************************************************
*				This script is the second in a series to fix stub counts on the factBatchSummary table. **
*				This will use the rows in dbo.tempStubCounts to update the factBatchSummary table.		**
*				The batches that were updated will remain in dbo.tempStubCounts							**
*********************************************************************************************************/

Update RecHubData.factBatchSummary SET RecHubData.factBatchSummary.StubCount = DetailStubsCount
FROM RecHubData.factBatchSummary
INNER JOIN dbo.tempStubCounts ON
factBatchSummary.DepositDateKey = dbo.tempStubCounts.DepositDateKey
AND factBatchSummary.BankKey = dbo.tempStubCounts.BankKey
AND factBatchSummary.ClientAccountKey = dbo.tempStubCounts.ClientAccountKey
AND factBatchSummary.BatchID = dbo.tempStubCounts.BatchID
AND IsDeleted = 0



