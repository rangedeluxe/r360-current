--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorViewName BatchKeysViewInsertWithoutBatchID
--WFSScriptProcessorViewDrop
IF OBJECT_ID('integraPAYStaging.BatchKeysViewInsertWithoutBatchID') IS NOT NULL
	DROP VIEW integraPAYStaging.BatchKeysViewInsertWithoutBatchID
GO

--WFSScriptProcessorViewCreate
CREATE VIEW integraPAYStaging.BatchKeysViewInsertWithoutBatchID
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/15/2014
*
* Purpose: View used to insert records into BatchKeys that do not have a
*		BatchID defined. (I.E. new batch.)
*
*
* Modification History
* 09/18/2015 WI 236576 JPB	Created
* 12/12/2017 PT 152545607 MGE	Removed OrganizationKey
******************************************************************************/
SELECT
	Batch_Id,
	ActionCode,
	SiteBankID,
	SiteLockboxID,
	BankKey,
	ClientAccountKey,
	DepositDateKey,
	ImmutableDateKey,
	SourceProcessingDateKey,
	SourceBatchID,
	BatchNumber,
	BatchSourceKey,
	BatchPaymentTypeKey,
	BatchCueID,
	DepositStatus,
	SystemType,
	DepositStatusKey,
	BatchSiteCode,
	DDAKey,
	ABA,
	DDA
FROM
	integraPAYStaging.BatchKeys;