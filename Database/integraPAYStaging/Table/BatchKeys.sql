--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable BatchKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.BatchKeys') IS NOT NULL
       DROP TABLE integraPAYStaging.BatchKeys
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold batch keys information for processing by the SSIS package. 
*
I
* Modification History
* 03/07/2013 WI  92275 JPB	Created
* 02/28/2014 WI 131197 JBS	Added DDAKey and DDA and ABA
* 04/24/2014 WI 138213 DLD  Updated DDA column size from 30 to 35.
* 06/09/2014 WI 146345 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
* 09/18/2015 WI 236575 JPB	Patched for post 2.01 release.
* 01/06/2017 PT#136908915 JPB	Chagned BatchCueID to NOT NULL.
* 12/12/2017 PT 152545607 MGE	Removed OrganizationKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.BatchKeys
(
	Batch_Id BIGINT NOT NULL,
	ActionCode TINYINT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteLockboxID INT NOT NULL,
	BankKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL
		CONSTRAINT DF_BatchKeys_BatchID DEFAULT (NEXT VALUE FOR RecHubSystem.ReceivablesBatchID),
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NULL,
	SystemType TINYINT NULL,
	DepositStatusKey INT NULL,
	BatchSiteCode INT NULL,
	DDAKey	INT	NULL,
	ABA		VARCHAR(10) NULL,
	DDA		VARCHAR(35)	NULL
);
--WFSScriptProcessorTableProperties
