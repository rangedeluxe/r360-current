--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName DataEntryColumnKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.DataEntryColumnKeys') IS NOT NULL
       DROP TABLE integraPAYStaging.DataEntryColumnKeys
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold data entry column keys information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92279 JPB	Created
* 11/04/2015 WI 244867 JPB	Updates for WorkgroupDataEntryColumnKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.DataEntryColumnKeys
(
	WorkgroupDataEntryColumnKey BIGINT,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	IsCheck BIT NOT NULL,
	DataType TINYINT NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex integraPAYStaging.DataEntryColumnKeys.IDX_DataEntryColumnKeys_SiteBankSiteClientAccountIDBatchSourceKey
CREATE CLUSTERED INDEX IDX_DataEntryColumnKeys_SiteBankSiteClientAccountIDBatchSourceKey ON integraPAYStaging.DataEntryColumnKeys
(
	SiteBankID,
	SiteClientAccountID,
	BatchSourceKey
);
