--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable SiteCodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.SiteCodes') IS NOT NULL
       DROP TABLE integraPAYStaging.SiteCodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold site code information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92291 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.SiteCodes
(
	SiteCode_Id BIGINT NOT NULL,
	SiteCodeID INT NOT NULL,
	CWDBActiveSite BIT NOT NULL,
	LocalTimeZoneBias INT NOT NULL,
	AutoUpdateCurrentProcessingDate BIT NOT NULL,
	CurrentProcessingDate DATETIME,
	ShortName VARCHAR(30) NOT NULL,
	LongName VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
