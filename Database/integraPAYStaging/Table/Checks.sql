--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable Checks
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.Checks') IS NOT NULL
       DROP TABLE integraPAYStaging.Checks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold check information for processing by the SSIS package.
*
I
* Modification History
* 03/07/2013 WI 92276 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.Checks
(
	Batch_Id BIGINT NOT NULL,
	Check_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	SequenceWithInTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	CheckSequence INT NOT NULL,
	Amount MONEY NOT NULL,
	RoutingNumber VARCHAR(30) NOT NULL,
	Account VARCHAR(30) NOT NULL,
	Serial VARCHAR(30) NULL,
	TransactionCode VARCHAR(30) NULL,
	RemitterName VARCHAR(60) NULL
);
--WFSScriptProcessorTableProperties
