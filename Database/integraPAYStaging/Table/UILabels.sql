--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName UILabels
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.UILabels') IS NOT NULL
       DROP TABLE integraPAYStaging.UILabels
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/13/2016
*
* Purpose: Working space to find duplicate data entry columns. 
*		   
*
* Modification History
* 05/13/2016 WI 280785 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.UILabels
(
	LabelSource BIT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	IsCheck BIT NOT NULL,
	DataType TINYINT NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
