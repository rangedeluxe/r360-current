--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName DataEntryColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.DataEntryColumns') IS NOT NULL
       DROP TABLE integraPAYStaging.DataEntryColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold data entry column information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92280 JPB	Created
* 09/27/2015 WI 237953 JPB	Updates for WorkgroupDataEntryColumns
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.DataEntryColumns
(
	DataEntrySetup_Id BIGINT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	IsCheck BIT NOT NULL,
	IsActive BIT NOT NULL,
	MarkSense BIT NOT NULL,
	DataType TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	UILabel NVARCHAR(64) NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
