--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable DataEntry
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.DataEntry') IS NOT NULL
       DROP TABLE integraPAYStaging.DataEntry
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold data entry information for processing by the SSIS package.
*
I
* Modification History
* 03/07/2013 WI 92278 JPB	Created
* 11/03/2015 WI 244767 JPB	Updates for WorkgroupDataEntryColumnKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.DataEntry
(
	Batch_Id BIGINT NOT NULL,
	DataEntry_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TransactionSequence INT NOT NULL,
	BatchSequence INT NOT NULL,
	IsCheck BIT NOT NULL,
	DataType TINYINT NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
