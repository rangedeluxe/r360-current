--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable DocumentTypes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.DocumentTypes') IS NOT NULL
       DROP TABLE integraPAYStaging.DocumentTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/11/2013
*
* Purpose:Work table to hold document type information for processing by the SSIS package. 
*
*
* Modification History
* 03/11/2013 WI 92282 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.DocumentTypes
(
	DocumentType_Id BIGINT NOT NULL,
	FileDescriptor VARCHAR(30) NOT NULL,
	DocumentTypeDescription VARCHAR(64) NOT NULL,
	IMSDocumentType VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
