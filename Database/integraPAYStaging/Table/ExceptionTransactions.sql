
--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName ExceptionTransactions

--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.ExceptionTransactions') IS NOT NULL
	DROP TABLE integraPAYStaging.ExceptionTransactions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2019 Deluxe Corporation. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets. These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details). All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 02/11/2019
*
* Purpose: Workspace for the integraPAY OTIS SSIS package.
*
*
* Modification History
* 02/12/2019 R360-1924	MGE Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.ExceptionTransactions
(
	BatchID BIGINT NOT NULL,
	TransactionID INT NOT NULL
);