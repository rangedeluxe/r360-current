IF OBJECT_ID('integraPAYStaging.WorkDataEntryWideChecks') IS NOT NULL
	DROP TABLE integraPAYStaging.WorkDataEntryWideChecks

/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets. These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details). All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 05/22/2019
*
* Purpose: Workspace for the integraPAY OTIS SSIS package - to populate 
*	factAdvancedSearch (a.k.a. the advanced search wide table)
*
*
* Modification History
* 10/30/2019 R360-30933	MGE Created
******************************************************************************/

CREATE TABLE integraPAYStaging.WorkDataEntryWideChecks
(
	BankKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL
);