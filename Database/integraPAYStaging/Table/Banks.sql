--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName Banks
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.Banks') IS NOT NULL
       DROP TABLE integraPAYStaging.Banks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold bank information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92273 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.Banks
(
	Bank_Id BIGINT NOT NULL,
	SiteBankID INT NOT NULL,
	BankName VARCHAR(128) NOT NULL,
	ABA VARCHAR(10) NULL
);
--WFSScriptProcessorTableProperties
