--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName dimWorkgroupDataEntryColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.dimWorkgroupDataEntryColumns') IS NOT NULL
       DROP TABLE integraPAYStaging.dimWorkgroupDataEntryColumns
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/13/2016
*
* Purpose: Working table for Workgoup Data Entry Columns dimension. 
*		   
*
* Modification History
* 05/13/2016 WI 281431 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.dimWorkgroupDataEntryColumns
(
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	IsCheck BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsCheck DEFAULT 0,
	IsActive BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_IsActive DEFAULT 1,
	MarkSense BIT NOT NULL
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_MarkSense DEFAULT 0,
	DataType TINYINT NOT NULL,
	ScreenOrder TINYINT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL 
		CONSTRAINT DF_dimWorkgroupDataEntryColumns_ModificationDate DEFAULT GETDATE(),
	UILabel NVARCHAR(64) NOT NULL,
	FieldName NVARCHAR(256) NOT NULL,
	SourceDisplayName NVARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
