--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable Batch
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.Batch') IS NOT NULL
       DROP TABLE integraPAYStaging.Batch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold batch information for processing by the SSIS package.
*
I
* Modification History
* 03/07/2013 WI 92274 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.Batch
(
	Batch_Id BIGINT NOT NULL,
	DepositDDA VARCHAR(40) NULL
);
--WFSScriptProcessorTableProperties
