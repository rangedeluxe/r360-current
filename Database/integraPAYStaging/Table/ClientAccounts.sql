--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName ClientAccounts
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.ClientAccounts') IS NOT NULL
       DROP TABLE integraPAYStaging.ClientAccounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold client account information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92277 JPB	Created
* 12/13/2017 PT 152545607 JPB Removed SiteOrganizationID
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimLockboxes
CREATE TABLE integraPAYStaging.ClientAccounts
(
	Lockbox_Id BIGINT NOT NULL,
	SiteCodeID INT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	IsActive TINYINT NOT NULL,
	CutOff TINYINT NOT NULL,
	OnlineColorMode TINYINT NOT NULL,
	IsCommingled BIT NOT NULL,
	DataRetentionDays SMALLINT NOT NULL,
	ImageRetentionDays SMALLINT NOT NULL,
	SiteClientAccountKey UNIQUEIDENTIFIER,
	ShortName VARCHAR(20) NOT NULL,
	LongName VARCHAR(40) NULL,
	POBox VARCHAR(32) NULL,
	DDA VARCHAR(40) NULL
);
--WFSScriptProcessorTableProperties
