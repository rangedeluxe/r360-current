--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable Documents
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.Documents') IS NOT NULL
       DROP TABLE integraPAYStaging.Documents
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold document information for processing by the SSIS package.
*
I
* Modification History
* 03/07/2013 WI 92281 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.Documents
(
	Batch_Id BIGINT NOT NULL,
	Document_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	SequenceWithInTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	DocumentSequence INT NOT NULL,
	FileDescriptor VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
