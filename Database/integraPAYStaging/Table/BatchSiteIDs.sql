--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable BatchSiteIDs
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.BatchSiteIDs') IS NOT NULL
       DROP TABLE integraPAYStaging.BatchSiteIDs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/16/2013
*
* Purpose: Work table to hold batch site IDs information for processing by the SSIS package. 
*
I
* Modification History
* 03/16/2013 WI 92558 JPB	Created
* 12/12/2017 PT 152545607 MGE	Removed OrganizationID
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.BatchSiteIDs
(
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL
);
--WFSScriptProcessorTableProperties
