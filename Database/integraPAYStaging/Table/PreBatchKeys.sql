--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTable PreBatchKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.PreBatchKeys') IS NOT NULL
       DROP TABLE integraPAYStaging.PreBatchKeys
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/20/2014
*
* Purpose: Work table to hold batch keys information for processing by the SSIS package. 
*
I
* Modification History
* 09/18/2015 WI 236571 JPB	Created
* 01/06/2017 PT#136908915 JPB	Changed BatchCueID to NOT NULL.
* 12/12/2017 PT 152545607 MGE	Removed OrganizationID
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.PreBatchKeys
(
	Batch_Id BIGINT NOT NULL,
	ActionCode TINYINT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BankKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NULL,
	SystemType TINYINT NULL,
	DepositStatusKey INT NULL,
	BatchSiteCode INT NULL,
	DDAKey	INT	NULL,
	BankABA	VARCHAR(10) NULL,
	DDA		VARCHAR(35)	NULL
);
--WFSScriptProcessorTableProperties
