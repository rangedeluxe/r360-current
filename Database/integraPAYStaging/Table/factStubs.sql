--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorTableName factStubs
--WFSScriptProcessorTableDrop
IF OBJECT_ID('integraPAYStaging.factStubs') IS NOT NULL
       DROP TABLE integraPAYStaging.factStubs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/07/2013
*
* Purpose: Work table to hold fact stub information for processing by the SSIS package.
*		   
*
* Modification History
* 03/07/2013 WI 92288 JPB	Created
* 06/09/2014 WI 146332 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
* 09/20/2015 WI 236733 JPB	Patched for post 2.01 release.
* 10/27/2016 PT 133221777 JPB	Added IsCorrespondence.
* 12/12/2017 PT 152545607 MGE	Removed OrganizationKey
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE integraPAYStaging.factStubs
(
	RecordID BIGINT IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	StubSequence INT NOT NULL,
	IsCorrespondence BIT NOT NULL,
	DocumentBatchSequence INT NOT NULL,
	IsOMRDetected BIT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	Amount MONEY NULL,
	AccountNumber VARCHAR(80) NULL
);
--WFSScriptProcessorTableProperties