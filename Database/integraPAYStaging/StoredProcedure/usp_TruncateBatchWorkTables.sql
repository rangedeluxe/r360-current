--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateBatchWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_TruncateBatchWorkTables') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_TruncateBatchWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_TruncateBatchWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/01/2013
*
* Purpose: Truncate tables used by SSIS for import Batch Data.
*
* Modification History
* 03/01/2013 WI 92305 JPB	Created
* 09/20/2015 WI 236735 JPB	Added PreBatchKeys
* 02/06/2019 R360-1924 MGE  Added ExceptionTransactions
* 05/21/2019 R360-16299 MGE Added WorkDataEntryWideTable
* 06/04/2019 R360-16299 MGE Added WorkGroupedTransactionDetails
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	TRUNCATE TABLE integraPAYStaging.PreBatchKeys;
	TRUNCATE TABLE integraPAYStaging.BatchKeys;
	TRUNCATE TABLE integraPAYStaging.DataEntryColumnKeys;
	TRUNCATE TABLE integraPAYStaging.Batch;
	TRUNCATE TABLE integraPAYStaging.Checks;
	TRUNCATE TABLE integraPAYStaging.DataEntry;
	TRUNCATE TABLE integraPAYStaging.Documents;
	TRUNCATE TABLE integraPAYStaging.Stubs;
	TRUNCATE TABLE integraPAYStaging.Transactions;
	TRUNCATE TABLE integraPAYStaging.factBatchSummary;
	TRUNCATE TABLE integraPAYStaging.factTransactionSummary;
	TRUNCATE TABLE integraPAYStaging.factChecks;
	TRUNCATE TABLE integraPAYStaging.factStubs;
	TRUNCATE TABLE integraPAYStaging.factDocuments;
	TRUNCATE TABLE integraPAYStaging.factDataEntrySummary;
	TRUNCATE TABLE integraPAYStaging.factDataEntryDetails;
	TRUNCATE TABLE integraPAYStaging.ExceptionTransactions;
	TRUNCATE TABLE integraPAYStaging.WorkDataEntryWideChecks;
	TRUNCATE TABLE integraPAYStaging.WorkDataEntryWideStubs;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
