--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_factDataEntrySummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_factDataEntrySummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_factDataEntrySummary_Upd_IsDeleted;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_factDataEntrySummary_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/05/2013
*
* Purpose: Mark rows as deleted in the RecHubData.factDataEntrySummary table based on
*			integraPAY SSIS import.
*
* Modification History
* 03/05/2013 WI 92297 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factDataEntrySummary
	SET		
		RecHubData.factDataEntrySummary.IsDeleted = 1,
		RecHubData.factDataEntrySummary.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factDataEntrySummary RFD
		INNER JOIN integraPAYStaging.factDataEntrySummary IFD ON RFD.DepositDateKey = IFD.DepositDateKey
			AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
			AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
			AND RFD.ClientAccountKey = IFD.ClientAccountKey
			AND RFD.BatchID = IFD.BatchID
	WHERE	
		RFD.IsDeleted = 0
		AND IFD.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
