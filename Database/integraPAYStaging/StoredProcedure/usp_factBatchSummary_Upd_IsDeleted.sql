--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_factBatchSummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_factBatchSummary_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_factBatchSummary_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/25/2013
*
* Purpose: Mark rows as deleted in the RecHubData.factBatchSummary table based on
*			integraPAY SSIS import.
*
* Modification History
* 03/05/2013 WI 92294  JPB	Created
* 01/13/2014 WI 126667 JPB	Account for action code 4 and records do not exist.
* 01/28/2016 WI 260900 JPB	Remove deprecated table factDataEntrySummary.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factBatchSummary
	SET		
		RecHubData.factBatchSummary.IsDeleted = 1,
		RecHubData.factBatchSummary.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factBatchSummary RFD
		INNER JOIN integraPAYStaging.factBatchSummary IFD ON RFD.DepositDateKey = IFD.DepositDateKey
			AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
			AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
			AND RFD.ClientAccountKey = IFD.ClientAccountKey
			AND RFD.BatchID = IFD.BatchID
	WHERE	
		RFD.IsDeleted = 0
		AND IFD.IsDeleted = 0;

	IF EXISTS( SELECT 1 FROM integraPAYStaging.BatchKeys WHERE ActionCode = 4 )
	BEGIN
		UPDATE	
			RecHubData.factBatchSummary
		SET		
			RecHubData.factBatchSummary.IsDeleted = 1,
			RecHubData.factBatchSummary.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factBatchSummary RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

		UPDATE	
			RecHubData.factTransactionSummary
		SET		
			RecHubData.factTransactionSummary.IsDeleted = 1,
			RecHubData.factTransactionSummary.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factTransactionSummary RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

		UPDATE	
			RecHubData.factChecks
		SET		
			RecHubData.factChecks.IsDeleted = 1,
			RecHubData.factChecks.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factChecks RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

		UPDATE	
			RecHubData.factDataEntryDetails
		SET		
			RecHubData.factDataEntryDetails.IsDeleted = 1,
			RecHubData.factDataEntryDetails.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factDataEntryDetails RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

		UPDATE	
			RecHubData.factDocuments
		SET		
			RecHubData.factDocuments.IsDeleted = 1,
			RecHubData.factDocuments.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factDocuments RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

		UPDATE	
			RecHubData.factStubs
		SET		
			RecHubData.factStubs.IsDeleted = 1,
			RecHubData.factStubs.ModificationDate = @parmModificationDate
		FROM	
			RecHubData.factStubs RFD
			INNER JOIN integraPAYStaging.BatchKeys IFD ON RFD.DepositDateKey = IFD.DepositDateKey
				AND RFD.ImmutableDateKey = IFD.ImmutableDateKey
				AND RFD.SourceProcessingDateKey= IFD.SourceProcessingDateKey
				AND RFD.ClientAccountKey = IFD.ClientAccountKey
				AND RFD.BatchID = IFD.BatchID
		WHERE	
			RFD.IsDeleted = 0
			AND IFD.ActionCode = 4;

	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
