--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_DataEntryColumns_Upd_DuplicateUILabels
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_DataEntryColumns_Upd_DuplicateUILabels') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_DataEntryColumns_Upd_DuplicateUILabels;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_DataEntryColumns_Upd_DuplicateUILabels
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/17/16
*
* Purpose: Update the UILabel to include the data type for duplicate source
*		display names.
*
* Modification History
* 05/17/16 WI 280786 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY

	;WITH FindDuplicates AS 
	(
		SELECT 
			ROW_NUMBER() OVER 
				(
					PARTITION BY integraPAYStaging.UILabels.SiteBankID,integraPAYStaging.UILabels.SiteClientAccountID,integraPAYStaging.UILabels.IsCheck,integraPAYStaging.UILabels.SourceDisplayName
					ORDER BY integraPAYStaging.UILabels.IsCheck,integraPAYStaging.UILabels.SourceDisplayName,integraPAYStaging.UILabels.LabelSource,integraPAYStaging.UILabels.DataType
				) AS RecNum,
			SiteBankID,
			SiteClientAccountID,
			IsCheck,
			DataType,
			SourceDisplayName
		FROM
			integraPAYStaging.UILabels
	)
	UPDATE 
		integraPAYStaging.DataEntryColumns
	SET 
		UILabel = 
			CAST(integraPAYStaging.DataEntryColumns.SourceDisplayName AS NVARCHAR(51))
				+ N'_' 
				+ CASE integraPAYStaging.DataEntryColumns.DataType
					WHEN 6 THEN N'Float'
					WHEN 7 THEN N'Money'
					WHEN 11 THEN N'Date'
					ELSE N'Alphanumeric'
				END
	FROM
		integraPAYStaging.DataEntryColumns
		INNER JOIN FindDuplicates ON FindDuplicates.SiteBankID = integraPAYStaging.DataEntryColumns.SiteBankID
			AND FindDuplicates.SiteClientAccountID = integraPAYStaging.DataEntryColumns.SiteClientAccountID
			AND FindDuplicates.IsCheck = integraPAYStaging.DataEntryColumns.IsCheck
			AND FindDuplicates.DataType = integraPAYStaging.DataEntryColumns.DataType
			AND FindDuplicates.SourceDisplayName = integraPAYStaging.DataEntryColumns.SourceDisplayName
	WHERE RecNum > 1;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH	
