--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_TruncateSetupWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_TruncateSetupWorkTables') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_TruncateSetupWorkTables;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_TruncateSetupWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2013
*
* Purpose: Truncate tables used by SSIS for import Setup.
*
* Modification History
* 02/26/2013 WI 92306 JPB	Created
* 05/17/2016 WI 281417 JPB	Added UILabels
* 12/13/2017 PT 152545607 JPB Removed integraPAYStaging.Organizations
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;
BEGIN TRY
	TRUNCATE TABLE integraPAYStaging.Banks;
	TRUNCATE TABLE integraPAYStaging.ClientAccounts;
	TRUNCATE TABLE integraPAYStaging.dimWorkgroupDataEntryColumns;
	TRUNCATE TABLE integraPAYStaging.DataEntryColumns;
	TRUNCATE TABLE integraPAYStaging.DocumentTypes;
	TRUNCATE TABLE integraPAYStaging.SiteCodes;
	TRUNCATE TABLE integraPAYStaging.UILabels;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
