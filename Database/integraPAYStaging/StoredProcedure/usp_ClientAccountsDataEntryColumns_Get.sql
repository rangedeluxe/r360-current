--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_ClientAccountsDataEntryColumns_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_ClientAccountsDataEntryColumns_Get') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_ClientAccountsDataEntryColumns_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_ClientAccountsDataEntryColumns_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/18/2013
*
* Purpose: Get list of DE columns.
*
* Modification History
* 05/18/2013 WI 92302 JPB	Created
* 09/20/2015 WI 236870 JPB	Updated for 2.x.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	;WITH ClientAccountKeyList AS
	(
		SELECT DISTINCT SiteBankID,SiteLockboxID
		FROM integraPAYStaging.BatchKeys	
	),
	DataEntryColumnList AS
	(
		SELECT	ROW_NUMBER() OVER (ORDER BY RecHubData.dimClientAccounts.ClientAccountKey) As RecID,
				ClientAccountKeyList.SiteBankID,
				ClientAccountKeyList.SiteLockboxID,
				RecHubData.dimClientAccounts.ClientAccountKey,
				RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey AS DataEntryColumnKey,
				--RTRIM(UPPER(RecHubData.dimWorkgroupDataEntryColumns.TableName)) AS TableName,
				RTRIM(UPPER(RecHubData.dimWorkgroupDataEntryColumns.FieldName)) AS FldName,
				RTRIM(UPPER(RecHubData.dimWorkgroupDataEntryColumns.SourceDisplayName)) AS DisplayName,
				RecHubData.dimWorkgroupDataEntryColumns.DataType,
				0 AS FldLength,
				RecHubData.dimWorkgroupDataEntryColumns.ScreenOrder,
				RecHubData.dimWorkgroupDataEntryColumns.IsActive AS TableType,
				RecHubData.dimWorkgroupDataEntryColumns.CreationDate
		FROM	ClientAccountKeyList
				INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = ClientAccountKeyList.SiteBankID 
					AND RecHubData.dimClientAccounts.SiteClientAccountID = ClientAccountKeyList.SiteLockboxID
				--INNER JOIN RecHubData.ClientAccountsDataEntryColumns ON RecHubData.ClientAccountsDataEntryColumns.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
				INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = ClientAccountKeyList.SiteBankID
					AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = ClientAccountKeyList.SiteLockboxID
	)
	SELECT	SiteBankID,
			SiteLockboxID,
			ClientAccountKey,
			DataEntryColumnKey,
			'' AS TableName,
			CAST(FldName AS VARCHAR) AS FldName,
			CAST(DisplayName AS VARCHAR) AS DisplayName,
			FldLength,
			ScreenOrder,
			TableType,
			DataType,
			CreationDate
	FROM	DataEntryColumnList
	WHERE	RecID IN
	(
			SELECT MAX(RecID)
			FROM DataEntryColumnList
			GROUP BY DataEntryColumnKey
	);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
