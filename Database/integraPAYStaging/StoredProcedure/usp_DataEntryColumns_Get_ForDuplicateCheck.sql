--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_DataEntryColumns_Get_ForDuplicateCheck
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_DataEntryColumns_Get_ForDuplicateCheck') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_DataEntryColumns_Get_ForDuplicateCheck;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_DataEntryColumns_Get_ForDuplicateCheck
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/17/16
*
* Purpose: Get existing data entry column info for duplicate checking.
*
* Modification History
* 05/17/16 WI 281416 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY

	SELECT
		CAST(1 AS BIT) AS LabelSource,
		SiteBankID,
		SiteClientAccountID,
		IsCheck,
		DataType,
		SourceDisplayName
	FROM
		integraPAYStaging.DataEntryColumns
	GROUP BY
		SiteBankID,
		SiteClientAccountID,
		IsCheck,
		DataType,
		SourceDisplayName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH	