--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ForValidation
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_factChecks_Get_ForValidation') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_factChecks_Get_ForValidation
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_factChecks_Get_ForValidation

AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 02/05/2019
*
* Purpose: Reads the check details currently in the integraPAYStaging tables
*			returning them so that business rules can be applied before moving
*			to RecHubData
*
* Modification History
* 06/06/2014 R360-1924 MGE	Created
* 09/06/2019 R360-30474 CMC	Need to include batch sequence or BRE won't 
*                           recognize as separate payments
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY


	SELECT	
		RecHubData.dimClientAccountsView.SiteBankID,
		RecHubData.dimClientAccountsView.SiteClientAccountID,
		integraPAYStaging.factChecks.BatchID,
		integraPAYStaging.factChecks.TransactionID,
		integraPAYStaging.factChecks.RemitterName,
		integraPAYStaging.factChecks.BatchSequence
	FROM	
		integraPAYStaging.factChecks
		INNER JOIN RecHubData.dimClientAccountsView ON RecHubData.dimClientAccountsView.ClientAccountKey = integraPAYStaging.factChecks.ClientAccountKey
	ORDER BY
		RecHubData.dimClientAccountsView.SiteBankID,
		RecHubData.dimClientAccountsView.SiteClientAccountID,
		integraPAYStaging.factChecks.BatchSourceKey,
		integraPAYStaging.factChecks.BatchID,
		integraPAYStaging.factChecks.TransactionID,
		integraPAYStaging.factChecks.BatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH