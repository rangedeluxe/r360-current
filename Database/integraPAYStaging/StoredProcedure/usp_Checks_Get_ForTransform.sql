--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_Checks_Get_ForTransform
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_Checks_Get_ForTransform') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_Checks_Get_ForTransform
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_Checks_Get_ForTransform

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/29/2017
*
* Purpose: Get checks for transformation step. Uses TRY_PARSE to ensure the value
*			can be converted to BIGINT.
*
* Modification History
* 04/29/2017 #143614215  JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY

	SELECT
		integraPAYStaging.Checks.Batch_Id,
		integraPAYStaging.Checks.TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		CheckSequence,
		Amount,
		RoutingNumber,
		Account,
		Serial,
		TransactionCode,
		RemitterName,
		COALESCE(TRY_PARSE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(RoutingNumber,'\',''),'-',''),'+',''),'$',''),'.',''),',',''),' ','') AS BIGINT),0) AS NumericRoutingNumber,
		COALESCE(TRY_PARSE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(Serial,'\',''),'-',''),'+',''),'$',''),'.',''),',',''),' ','') AS BIGINT),0) AS NumericSerial
	FROM 
		integraPAYStaging.Checks
		INNER JOIN integraPayStaging.Transactions ON integraPAYStaging.Checks.Batch_Id = integraPayStaging.Transactions.Batch_Id 
			AND integraPAYStaging.Checks.TransactionID = integraPayStaging.Transactions.TransactionID 
	ORDER BY 
		integraPAYStaging.Checks.Batch_Id,
		integraPayStaging.Transactions.TransactionID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		

