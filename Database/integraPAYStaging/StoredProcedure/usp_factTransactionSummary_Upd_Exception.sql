--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Upd_Exception
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_factTransactionSummary_Upd_Exception') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_factTransactionSummary_Upd_Exception
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_factTransactionSummary_Upd_Exception
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 04/07/2017
*
* Purpose: Update integraPAYStaging.factTransactionSummary.Exception
*
* Modification History
* 04/07/2017 R360-1924 MGE	Created
******************************************************************************/
SET NOCOUNT ON;
 
BEGIN TRY
	UPDATE
		integraPAYStaging.factTransactionSummary
	SET
		Exception = 1
	FROM
		integraPAYStaging.factTransactionSummary
		INNER JOIN integraPAYStaging.ExceptionTransactions ON integraPAYStaging.ExceptionTransactions.BatchID = integraPAYStaging.factTransactionSummary.BatchID
			AND integraPAYStaging.ExceptionTransactions.TransactionID = integraPAYStaging.factTransactionSummary.TransactionID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH