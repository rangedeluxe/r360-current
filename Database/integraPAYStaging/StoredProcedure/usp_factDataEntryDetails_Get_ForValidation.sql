--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_ForValidation
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_factDataEntryDetails_Get_ForValidation') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_factDataEntryDetails_Get_ForValidation
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_factDataEntryDetails_Get_ForValidation
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 02/05/2019
*
* Purpose: Get integraPAYStaging.factDataEntryDetails for Post Deposit BRE validation.
*
* Modification History
* 04/07/2017 PT 141788325 MGE	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		integraPAYStaging.factDataEntryDetails.BatchID,
		integraPAYStaging.factDataEntryDetails.TransactionID,
		integraPAYStaging.factDataEntryDetails.BatchSequence,
		RecHubData.dimWorkgroupDataEntryColumns.SiteBankID,
		RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID,
		integraPAYStaging.factDataEntryDetails.BatchSourceKey,
		RecHubData.dimWorkgroupDataEntryColumns.IsCheck,
		integraPAYStaging.factDataEntryDetails.DataEntryValue,
		RecHubData.dimWorkgroupDataEntryColumns.FieldName
	FROM
		integraPAYStaging.factDataEntryDetails
		INNER JOIN RecHubData.dimWorkgroupDataEntryColumns ON 
			RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey = integraPAYStaging.factDataEntryDetails.WorkGroupDataEntryColumnKey
	ORDER BY RecHubData.dimWorkgroupDataEntryColumns.SiteBankID,
		RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID,
		integraPAYStaging.factDataEntryDetails.BatchSourceKey,
        integraPAYStaging.factDataEntryDetails.BatchID,
        integraPAYStaging.factDataEntryDetails.TransactionID,
        integraPAYStaging.factDataEntryDetails.BatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
