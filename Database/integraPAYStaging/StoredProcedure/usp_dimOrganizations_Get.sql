--WFSScriptProcessorSchema integraPAYStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_dimOrganizations_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('integraPAYStaging.usp_dimOrganizations_Get') IS NOT NULL
       DROP PROCEDURE integraPAYStaging.usp_dimOrganizations_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE integraPAYStaging.usp_dimOrganizations_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/25/2013
*
* Purpose: Retrieve organization keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 02/25/2013 WI 92304 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	;WITH BankList AS
	(
		SELECT DISTINCT SiteBankID
		FROM	integraPAYStaging.BatchSiteIDs
	)
	SELECT	RecHubData.dimOrganizations.OrganizationKey,
			RecHubData.dimOrganizations.SiteBankID AS SiteBankID,
			RecHubData.dimOrganizations.SiteOrganizationID AS SiteOrganizationID,
			RecHubData.dimOrganizations.MostRecent AS MostRecentOrganizationKey
	FROM	RecHubData.dimOrganizations
			INNER JOIN BankList ON RecHubData.dimOrganizations.SiteBankID = BankList.SiteBankID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
