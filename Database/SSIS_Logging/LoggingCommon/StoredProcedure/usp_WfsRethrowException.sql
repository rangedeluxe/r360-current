--WFSScriptProcessorSchema LoggingCommon
--WFSScriptProcessorSystemName SSISLogging
--WFSScriptProcessorStoredProcedureName usp_WfsRethrowException
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('LoggingCommon.usp_WfsRethrowException') IS NOT NULL
       DROP PROCEDURE LoggingCommon.usp_WfsRethrowException
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE LoggingCommon.usp_WfsRethrowException
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 3/23/2008
*
* Purpose: This procedure is designed to be called from the CATCH block of all
*      WFS stored procedures for SQL Server 2005+.  This will return error information 
*      to the WFS data provider in a defined/parseable format via RAISERROR.
*
* Modification History
* 03/23/2008 CR 25817		JJR	Created
* 03/20/2013 WI 90814		JPB	2.0. Moved to RecHubCommon schema.
* 05/22/2017 PT 145857133	JPB	Adapted from R360
******************************************************************************/

SET NOCOUNT ON 

DECLARE @ErrorMessage    NVARCHAR(4000),
		@ErrorNumber     INT,
		@ErrorSeverity   INT,
		@ErrorState      INT,
		@ErrorLine       INT,
		@ErrorProcedure  NVARCHAR(200);

SELECT 
	@ErrorNumber = ERROR_NUMBER(),
	@ErrorSeverity = ERROR_SEVERITY(),
	@ErrorState = ERROR_STATE(),
	@ErrorLine = ERROR_LINE(),
	@ErrorProcedure = ISNULL(ERROR_PROCEDURE(), '-'),
	@ErrorMessage = ERROR_MESSAGE();

IF (LEFT(@ErrorMessage, 15) = N'( Error Number=')
BEGIN
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
END
ELSE
BEGIN
	SELECT @ErrorMessage = N'( Error Number=%d, Severity=%d, State=%d, Procedure=%s, Line=%d )' + CHAR(13) + @ErrorMessage;
	RAISERROR (@ErrorMessage,@ErrorSeverity,@ErrorState,@ErrorNumber,@ErrorSeverity,@ErrorState,@ErrorProcedure,@ErrorLine);
END
