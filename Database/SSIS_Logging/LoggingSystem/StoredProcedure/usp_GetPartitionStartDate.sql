--WFSScriptProcessorSchema LoggingSystem
--WFSScriptProcessorSystemName SSISLogging
--WFSScriptProcessorStoredProcedureName usp_GetPartitionStartDate
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('LoggingSystem.usp_GetPartitionStartDate') IS NOT NULL
       DROP PROCEDURE LoggingSystem.usp_GetPartitionStartDate
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE LoggingSystem.usp_GetPartitionStartDate
       @parmBeginDate DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/22/2017
*
* Purpose: Calculate a partition start date based on the input date.
*
* Modification History
* 05/22/2017 PT 145857133	JPB	Created
***************************************************************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	DECLARE @CreateDate DATETIME = DATEADD(dd,-7,@parmBeginDate);
	;WITH date_CTE(CalendarDate,LoggingDateKey,CalendarDayName,Occurence) AS
	(
		SELECT	
			@CreateDate AS CalendarDate,
			CAST(LEFT(REPLACE(CONVERT(VARCHAR,@CreateDate,120),'-',''),8) AS INT) AS LoggingDateKey,
			DATENAME(dw, @CreateDate) CalendarDayName,
			1 AS Occurence
		UNION ALL
		SELECT
			DATEADD(dd,Occurence,@CreateDate),
			CAST(LEFT(REPLACE(CONVERT(VARCHAR,DATEADD(dd,Occurence,@CreateDate),120),'-',''),8) AS INT),
			DATENAME(dw, DATEADD(dd,Occurence,@CreateDate)),
			Occurence + 1
		FROM
			date_CTE
		WHERE
			Occurence <= 14
		
	)
	SELECT TOP 1 
		CalendarDate 
	FROM 
		date_CTE 
	WHERE 
		CalendarDate <= @parmBeginDate 
		AND CalendarDayName = 'Monday' 
	ORDER BY CalendarDate DESC;

END TRY 
BEGIN CATCH
	EXEC LoggingCommon.usp_WfsRethrowException;
END CATCH