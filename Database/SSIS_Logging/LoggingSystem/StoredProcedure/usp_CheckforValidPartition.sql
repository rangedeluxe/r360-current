--WFSScriptProcessorSchema LoggingSystem
--WFSScriptProcessorStoredProcedureName usp_CheckforValidPartition
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('LoggingSystem.usp_CheckforValidPartition') IS NOT NULL
       DROP PROCEDURE LoggingSystem.usp_CheckforValidPartition
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE LoggingSystem.usp_CheckforValidPartition
       @parmPartitionDate SMALLDATETIME
AS
/******************************************************************************
** Wausau Financial Systems
** Copyright 2008-2017 Wausau Financial Systems All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau Financial Systems 2008-2017.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau Financial Systems and contain Wausau Financial Systems 
* trade secrets.  These materials may not be used, copied, modified or disclosed 
* except as expressly permitted in writing by Wausau Financial Systems (see 
* the Wausau Financial Systems license agreement for details).  All copies, 
* modifications and derivative works of these materials are property of Wausau 
* Financial Systems.
*
* Author: JJR
* Date: 4/20/2008
*
* Purpose:    Checks for the existance of a valid partition for the partition identifier
*             based on the partition date.  If no valid parition exists, one will be 
*             created.
*
* Modification History
* 04/20/2008 CR 25817		JJR	Created
* 03/13/2013 WI 90593		JBS	Update to 2.0 release.  Change Schema Name.
* 05/22/2017 PT 145857133	JPB	Adapted from R360
* 06/15/2017 PT 143256699	MGE Fix the Partition Function Name
******************************************************************************/
SET NOCOUNT ON        

DECLARE @PartitionFunction SYSNAME = 'WFSSSISLogging_Partition_Range',
		@PartitionScheme SYSNAME,
		@PartitionFromDate SMALLDATETIME,
		@PartitionToDate SMALLDATETIME,
		@PartitionMethod SMALLINT,
		@PreviousValidDate SMALLDATETIME, 
		@NextValidDate SMALLDATETIME, 
		@PartitionManagerID INT, 
		@FileSize INT, 
		@FirstDiskID INT, 
		@NextDiskID INT,
		@LastDiskID INT, 
		@DiskPath VARCHAR(1000),
		@NewFGFileName VARCHAR(1000);

/* Ensure that dates are always midnight */
SET @parmPartitionDate = CAST(CONVERT(VARCHAR,@parmPartitionDate,112) AS SMALLDATETIME)

BEGIN TRY        
	/* find the closest partition range value less than PartitionDate */
	SELECT 
		@PartitionFromDate = CAST(COALESCE(CAST(MAX(sys.partition_range_values.Value) AS VARCHAR),@parmPartitionDate) AS SMALLDATETIME)
	FROM 
		sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_range_values.function_id = sys.partition_functions.function_id
	WHERE 
		sys.partition_range_values.Value <= CAST(CONVERT(VARCHAR, @parmPartitionDate, 112) AS INT)
		AND sys.partition_functions.name = @PartitionFunction;

	/* find the closest partition range value greater than PartitionDate */
	SELECT 
		@PartitionToDate = CAST(CAST(MIN(sys.partition_range_values.Value) AS VARCHAR) AS SMALLDATETIME)
	FROM 
		sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_range_values.function_id = sys.partition_functions.function_id
	WHERE 
		sys.partition_range_values.Value > CAST(CONVERT(VARCHAR, @parmPartitionDate, 112) as INT)
		AND sys.partition_functions.name = @PartitionFunction;

    /* Find what the previous partition date should be based on setting weekly/monthly */
    SET DATEFIRST 1
    SELECT 
		@PreviousValidDate = DATEADD(DD, 1 - DATEPART(DW, @parmPartitionDate),@parmPartitionDate);

    /* Find what the next partition date should be based on setting weekly/monthly */
    SELECT 
		@NextValidDate = DATEADD(DD, 7,@PreviousValidDate);

	IF @PartitionFromDate <= @PreviousValidDate OR @PartitionFromDate IS NULL
	BEGIN
		SET @NewFGFileName = 'WFSSSISLogging' + CONVERT(VARCHAR,@PreviousValidDate,112)
		EXEC LoggingSystem.usp_CreatePartitions 
			@parmRangeDate = @PreviousValidDate, 
			   @parmFileGroupName = @NewFGFileName, 
			   @parmFileSize = 1;
	END
	ELSE IF @PartitionToDate > @NextValidDate OR @PartitionToDate IS NULL /* If partition to date is not the next valid date, create that partition */
	BEGIN
		SET @NewFGFileName = 'WFSSSISLogging' + CONVERT(VARCHAR,@NextValidDate,112)
		EXEC LoggingSystem.usp_CreatePartitions 
			@parmRangeDate = @PreviousValidDate, 
			   @parmFileGroupName = @NewFGFileName, 
			   @parmFileSize = 1;
	END
END TRY 
BEGIN CATCH
	EXEC LoggingCommon.usp_WfsRethrowException;
END CATCH
