--WFSScriptProcessorSchema LoggingSystem
--WFSScriptProcessorStoredProcedureName usp_CreatePartitions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('LoggingSystem.usp_CreatePartitions') IS NOT NULL
       DROP PROCEDURE LoggingSystem.usp_CreatePartitions
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE LoggingSystem.usp_CreatePartitions
       @parmRangeDate SMALLDATETIME,
       @parmFileGroupName VARCHAR(1000),
       @parmFileSize INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 3/09/2009
*
* Purpose: Creates a new filegroup and file and creates/alters the partition 
*      scheme and function to use the new file/filegroup.
*
* Modification History
* 03/09/2008 CR 25817		JJR	Created
* 07/09/2012 CR 53205		JPB	Removed call to create check constraints.
* 03/13/2013 WI 90599		JBS	Update to 2.0 release. Change Schema Name.
* 03/22/2013 PT 138837605	MGE	Update Create Partition Function to use Right Boundary and Create Partition Scheme to work properly with Right Boundary
* 03/28/2013 PT 138837605	MGE	Initalize CatchAll filegroup and file
* 05/22/2017 PT 145857133	JPB	Adapted from R360
******************************************************************************/
DECLARE 
	@RangeDate INT,
	@DiskPath VARCHAR(1000),
	@parmPartitionScheme VARCHAR(255) = 'WFSSSISLogging',
	@parmPartitionFunction VARCHAR(255) = 'WFSSSISLogging_Partition_Range',
	@SQLcmd VARCHAR(max),
	@msg VARCHAR(max),
	@CatchAllFGName VARCHAR(128);


BEGIN TRY
	SET @RangeDate=CAST(CONVERT(VARCHAR,@parmRangeDate,112) AS INT)

	SET @SQLcmd = 'ALTER DATABASE '+ DB_NAME() +' ADD FILEGROUP '+ @parmFileGroupName
	RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
	EXEC (@SQLcmd)

	--Use the same disk the logging MDF file is on
	SELECT 
		@DiskPath = REVERSE(RIGHT(REVERSE(physical_name),(LEN(physical_name)-CHARINDEX('\', REVERSE(physical_name),1))+1))
	FROM 
		sys.master_files 
		INNER JOIN sys.databases ON sys.databases.database_id = sys.master_files.database_id	
	WHERE
		sys.databases.name = DB_NAME()
		AND type = 0;

	SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' ADD FILE  (NAME = '+ @parmFileGroupName + ','
	SET @SQLcmd = @SQLcmd + ' FILENAME = ' + ''''+ @DiskPath + @parmFileGroupName + '.mdf'+'''' + ','
	SET @SQLcmd = @SQLcmd + ' SIZE = ' + CAST(@parmFileSize as VARCHAR) + ','
	SET @SQLcmd = @SQLcmd + ' MAXSIZE = UNLIMITED, FILEGROWTH = 10MB)'
	SET @SQLcmd = @SQLcmd + ' TO FILEGROUP '+ @parmFileGroupName
	RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
	EXEC (@SQLcmd)      

	/*
		CREATE Partition Scheme/Function if it does not already exist; otherwise, ALTER
		ALSO - Check to see if CatchAll File and FileGroup exist and create if necessary
	*/
	IF NOT EXISTS (SELECT * FROM sys.partition_functions WHERE name = @parmPartitionFunction)
	BEGIN
		RAISERROR ('INITIALIZING CATCHALL AND SCHEME AND FUNCTION',10,1) WITH NOWAIT;
		SET @CatchAllFGName = @parmPartitionScheme + 'CatchAll';
		IF EXISTS(SELECT 1 FROM sys.filegroups WHERE name = @CatchAllFGName)
			RAISERROR('DataImportCatchAll filegroup already exists',10,1) WITH NOWAIT;
		ELSE
		BEGIN
			RAISERROR('Creating Filegroup CatchAll',10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER DATABASE ' + DB_Name() + ' ADD FILEGROUP ' +@parmPartitionScheme + 'CatchAll';
			RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
		END;
												
		RAISERROR('Creating database File CatchAll',10,1) WITH NOWAIT;
				
		IF EXISTS(SELECT 1 FROM sys.database_files WHERE name = @CatchAllFGName)
			RAISERROR('%s database file already exists',@CatchAllFGName,10,1) WITH NOWAIT;
		ELSE
		BEGIN
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' ADD FILE  (NAME = '+ @parmPartitionScheme + 'CatchAll,';
			SET @SQLcmd = @SQLcmd + ' FILENAME = ' + ''''+ @DiskPath + @parmPartitionScheme + 'CatchAll.mdf' + '''' + ',';
			SET @SQLcmd = @SQLcmd + ' SIZE = ' + CAST(@parmFileSize as VARCHAR) + ',';
			SET @SQLcmd = @SQLcmd + ' MAXSIZE = UNLIMITED, FILEGROWTH=10MB)';
			SET @SQLcmd = @SQLcmd + ' TO FILEGROUP '+ @parmPartitionScheme  + 'CatchAll';
			RAISERROR(@SQLcmd,10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
		END;
		SET @SQLcmd='CREATE PARTITION FUNCTION [WFSSSISLogging_Partition_Range](int) AS RANGE RIGHT FOR VALUES ('+CAST(@RangeDate as VARCHAR)+')';
		RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
		EXEC (@SQLcmd);

		SET @SQLcmd='CREATE PARTITION SCHEME ['+@parmPartitionScheme+'] AS PARTITION ['+@parmPartitionFunction+'] TO (' + @parmPartitionScheme + 'CatchAll,' +@parmFileGroupName + ')'
		RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
		EXEC (@SQLcmd);
	END
	ELSE
	BEGIN
		SET @SQLcmd = 'ALTER PARTITION SCHEME '+@parmPartitionScheme+' NEXT USED '+@parmFileGroupName+'';
		EXEC (@SQLcmd);      

		SET @SQLcmd = 'ALTER PARTITION FUNCTION WFSSSISLogging_Partition_Range() SPLIT RANGE('+CAST(@RangeDate as VARCHAR)+')';
		EXEC (@SQLcmd);
		/* Set CHECK constraint on fact tables */
		EXEC LoggingSystem.usp_CreateCheckConstraintsForPartition @parmPartitionFunction
	END
END TRY 
BEGIN CATCH
	EXEC LoggingCommon.usp_WfsRethrowException;
END CATCH
