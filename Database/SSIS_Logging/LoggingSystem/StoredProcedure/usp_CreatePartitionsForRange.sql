--WFSScriptProcessorSchema LoggingSystem
--WFSScriptProcessorStoredProcedureName usp_CreatePartitionsForRange
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('LoggingSystem.usp_CreatePartitionsForRange') IS NOT NULL
       DROP PROCEDURE LoggingSystem.usp_CreatePartitionsForRange
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE LoggingSystem.usp_CreatePartitionsForRange
       @parmBeginDate DATETIME, 
       @parmEndDate DATETIME
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright 2008-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright 2008-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JJR
* Date: 03/24/2009
*
* Purpose: This SP will call the partition check/create SPs for a range of dates.
*      It accepts a date range and Partition Identifier and will call the checker
*      once for each week in the range.
*
* Modification History
* 03/24/2009 CR 25817		JJR	Created
* 03/13/2013 WI 90600		JBS	Update to 2.0 release. Change Schema Name.
* 05/22/2017 PT 145857133	JPB	Adapted from R360
******************************************************************************/
SET NOCOUNT ON 
DECLARE @PartitionMethod TINYINT = 1, --0:none/1:weekly/2:monthly
		@Loop INT,
		@DateValue DATETIME

BEGIN TRY
	/* Get 1 day per week to pass into partition checker */
	DECLARE @Dates TABLE (RowID INT IDENTITY(1,1), DateValue DATETIME)

	/* Make sure the start date is a Monday */
	IF( DATEPART(WEEKDAY, @parmBeginDate) <> 2 )
	BEGIN
		SELECT 
			@parmBeginDate = 
				CASE DATEPART(WEEKDAY, @parmBeginDate)
					WHEN 1 THEN DATEADD(dd,1,@parmBeginDate)					
					WHEN 3 THEN DATEADD(dd,6,@parmBeginDate)
					WHEN 4 THEN DATEADD(dd,5,@parmBeginDate)
					WHEN 5 THEN DATEADD(dd,4,@parmBeginDate)
					WHEN 6 THEN DATEADD(dd,3,@parmBeginDate)
					WHEN 7 THEN DATEADD(dd,2,@parmBeginDate)
				END
	END

	/* Use a CTE to build the dates to check */
	;WITH date_CTE(CalendarDate,CalendarDayName,Occurence) AS
	(
		SELECT	
			@parmBeginDate AS CalendarDate,
			DATENAME(dw, @parmBeginDate) CalendarDayName,
			1 AS Occurence
		UNION ALL
		SELECT
			DATEADD(WEEK,Occurence,@parmBeginDate),
			DATENAME(dw, DATEADD(WEEK,Occurence,@parmBeginDate)),
			Occurence + 1
		FROM
			date_CTE
		WHERE
			CalendarDate <= @parmEndDate		
	)
	INSERT INTO @Dates
	SELECT 
		CalendarDate
	FROM 
		date_CTE
	WHERE
		CalendarDate <= @parmEndDate;

	/* Pass each day into partition checker to create partitions */
	SET @Loop=1;
	WHILE @Loop<=(SELECT MAX(RowID) FROM @Dates)
	BEGIN
		SELECT 
			@DateValue=DateValue
		FROM 
			@Dates
		WHERE 
			RowID=@Loop;

		EXEC LoggingSystem.usp_CheckforValidPartition @DateValue;
          
		SET @Loop+=1;
	END 
END TRY
BEGIN CATCH
	EXEC LoggingCommon.usp_WfsRethrowException;
END CATCH
