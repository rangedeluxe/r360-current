--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPrint WI 259597
--WFSScriptProcessorPrint Adding index IDX_PackageTaskLog_PackageLogSourceEndDateTime
--WFSScriptProcessorCRBegin
IF EXISTS (SELECT Name FROM sysindexes WHERE Name = 'IDX_PackageTaskLog_PackageLogSourceEndDateTime') 
	BEGIN
		RAISERROR('WI 259597 Already applied',10,1) WITH NOWAIT;
	END
ELSE	
	BEGIN
		RAISERROR('Creating index IDX_PackageTaskLog_PackageLogSourceEndDateTime',10,1) WITH NOWAIT;
		CREATE NONCLUSTERED INDEX IDX_PackageTaskLog_PackageLogSourceEndDateTime 
			ON dbo.PackageTaskLog 
			(PackageLogID ASC, SourceID ASC, EndDateTime ASC);
		
		IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'dbo', 'TABLE', 'PackageTaskLog', default, default) )
				EXEC sys.sp_dropextendedproperty 
					@name = N'Table_Description',
					@level0type = N'SCHEMA',
					@level0name = N'dbo',
					@level1type = N'TABLE',
					@level1name = N'PackageTaskLog';	

		EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = dbo,
		@level1type = N'TABLE',@level1name = PackageTaskLog,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50316 JPB	Created.
* 06/20/2013 WI 105903 JPB	Updated for 2.0.
* 02/08/2016 WI 259597 MGE	Added nonclustered index
******************************************************************************/';
	END
--WFSScriptProcessorCREnd