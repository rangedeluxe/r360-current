--WFSScriptProcessorDoNotFormat
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 06/12/2017
*
* Purpose: Build the partitions for the SSIS Logging tables.
* 
* Prerequisites:	None
*
* Modification History
* 06/12/2017 PT 146868813	MGE	Created - use this for partitioning the SSIS Logging tables
***********************************************************************************/
DECLARE @LoggingRetentionDays INT = 8,		--Since we are dropping the tables there is no history at this step
		@BeginDate	DATETIME,
		@EndDate	DATETIME,
		@Message	VARCHAR(90),
		@TotalNewCount	INT,
		@TotalOldCount	INT;

IF NOT EXISTS 
	(
	SELECT 1 
	FROM sys.tables
	JOIN sys.indexes 
	ON sys.tables.object_id = sys.indexes.object_id
	JOIN sys.partition_schemes 
	ON sys.partition_schemes.data_space_id = sys.indexes.data_space_id
	WHERE sys.tables.name = 'PackageLog'  
	AND sys.indexes.type = 1 
	AND SCHEMA_ID('dbo') = sys.tables.schema_id
	AND sys.partition_schemes.name = 'WFSSSISLogging'
	)
BEGIN
	RAISERROR('Beginning process to create partitions for WFSSSISLogging',10,1) WITH NOWAIT;
	RAISERROR('Building Partitions for WFSSSISLogging',10,1) WITH NOWAIT;

	--<<< CALCULATE First partition using RetentionDays >>>
	
	SELECT @BeginDate = GETDATE() - @LoggingRetentionDays		-- n days prior to today
	IF( DATEPART(WEEKDAY, @BeginDate) <> 2 )
	BEGIN
		SELECT 
			@BeginDate = 
				CASE DATEPART(WEEKDAY, @BeginDate)
					WHEN 1 THEN DATEADD(dd,1,@BeginDate)					
					WHEN 3 THEN DATEADD(dd,6,@BeginDate)
					WHEN 4 THEN DATEADD(dd,5,@BeginDate)
					WHEN 5 THEN DATEADD(dd,4,@BeginDate)
					WHEN 6 THEN DATEADD(dd,3,@BeginDate)
					WHEN 7 THEN DATEADD(dd,2,@BeginDate)
				END
	END
	SELECT @Message = 'Oldest WFSSSISLogging Partition to build is: ' + CONVERT(CHAR(10),@BeginDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364						-- 52 weeks into the future
	IF( DATEPART(WEEKDAY, @EndDate) <> 2 )
	BEGIN
		SELECT 
			@EndDate = 
				CASE DATEPART(WEEKDAY, @EndDate)
					WHEN 1 THEN DATEADD(dd,1,@EndDate)					
					WHEN 3 THEN DATEADD(dd,6,@EndDate)
					WHEN 4 THEN DATEADD(dd,5,@EndDate)
					WHEN 5 THEN DATEADD(dd,4,@EndDate)
					WHEN 6 THEN DATEADD(dd,3,@EndDate)
					WHEN 7 THEN DATEADD(dd,2,@EndDate)
				END
	END
	SELECT @Message = 'Last Future Partition to build for WFSSSISLogging is: ' + CONVERT(CHAR(10),@EndDate,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Build partitions by calling usp_CreatePartitionsForRange >>>
	RAISERROR('Creating Partitions for WFSSSISLogging',10,1) WITH NOWAIT;
	EXEC LoggingSystem.usp_CreatePartitionsForRange @BeginDate, @EndDate

	RAISERROR('WFSSSISLogging partitions have been successfully created.',10,1) WITH NOWAIT;
END
ELSE
	RAISERROR('PT 146868813_WFSSSISLogging has already been applied to the database.',10,1) WITH NOWAIT;