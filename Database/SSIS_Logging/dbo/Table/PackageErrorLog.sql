--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName PackageErrorLog
--WFSScriptProcessorTableDrop
IF OBJECT_ID('dbo.PackageErrorLog') IS NOT NULL
       DROP TABLE dbo.PackageErrorLog;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50315 JPB	Created.
* 06/20/2013 WI 105901 JPB	Updated for 2.0.
* 06/12/2017 PT 146868813	MGE Added Partitioning
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.PackageErrorLog
(
	PackageErrorLogID INT IDENTITY(1,1) NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_PackageErrorLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	PackageLogID INT NOT NULL,
	SourceName VARCHAR(64) NOT NULL,
	SourceID UNIQUEIDENTIFIER NOT NULL,
	[ErrorCode] INT NULL,
	ErrorDescription VARCHAR(2000) NULL,
	LogDateTime DATETIME NOT NULL
)  ON WFSSSISLogging(CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE dbo.PackageErrorLog ADD
	CONSTRAINT PK_PackageErrorLog PRIMARY KEY CLUSTERED (PackageErrorLogID, CreationDateKey);
--ALTER TABLE dbo.PackageErrorLog  WITH CHECK ADD  
--	CONSTRAINT FK_PackageErrorLog_PackageLog FOREIGN KEY(PackageLogID) REFERENCES dbo.PackageLog (PackageLogID)
