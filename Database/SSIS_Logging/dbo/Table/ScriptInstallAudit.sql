--WFSScriptProcessorSystemName WFS_SSIS
--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName ScriptInstallAudit
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 04/01/2014
*
* Purpose: Store the results of scripts run against the database.
*
*
* Modification History
* 04/01/2014 WI 133056 JBS	Created for 2.1 release
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.ScriptInstallAudit
(
	ScriptInstallAuditID BIGINT IDENTITY (1,1) NOT NULL
		CONSTRAINT PK_ScriptInstallAudit PRIMARY KEY NONCLUSTERED,
	SystemName	VARCHAR(128) NOT NULL,
	IssueID		INT NOT NULL,
	BuildNumber VARCHAR(20) NOT NULL,
	Results		VARCHAR(256) NOT NULL,
	ScriptName	VARCHAR(256) NOT NULL,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_ScriptInstallAudit_CreationDate DEFAULT GETDATE(),
	CreatedBy	VARCHAR(128) NOT NULL
		CONSTRAINT DF_ScriptInstallAudit_CreatedBy DEFAULT SUSER_SNAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_ScriptInstallAudit_ModificationDate DEFAULT GETDATE(),
	ModifiedBy	VARCHAR(128) NOT NULL
		CONSTRAINT DF_ScriptInstallAudit_ModifiedBy DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex dbo.ScriptInstallAudit.IDX_ScriptInstallAudit_CreationDate
CREATE CLUSTERED INDEX [IDX_ScriptInstallAudit_CreationDate] ON dbo.ScriptInstallAudit
(
	CreationDate ASC
);
