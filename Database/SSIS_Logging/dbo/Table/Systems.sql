--WFSScriptProcessorSystemName WFS_SSIS
--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorTableName Systems
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 04/01/2014
*
* Purpose:	Contains Database version installed.
*
*
* Modification History
* 04/01/2014 WI 133054 JBS	Created for 2.01 release.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.Systems
(
	SystemName		VARCHAR(128) NOT NULL
		CONSTRAINT PK_Systems PRIMARY KEY NONCLUSTERED,
	DateInstalled	DATETIME NULL,
	SystemVersion	VARCHAR(20) NULL
)
--WFSScriptProcessorTableProperties