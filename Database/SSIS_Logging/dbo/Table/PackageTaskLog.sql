--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName PackageTaskLog
--WFSScriptProcessorTableDrop
IF OBJECT_ID('dbo.PackageTaskLog') IS NOT NULL
       DROP TABLE dbo.PackageTaskLog;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50316 JPB	Created.
* 06/20/2013 WI 105903 JPB	Updated for 2.0.
* 02/08/2016 WI 259597 MGE	Added nonclustered index
* 06/12/2017 PT 146868813	MGE Added Partitioning
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.PackageTaskLog
(
	PackageTaskLogID INT IDENTITY(1,1) NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_PackageTaskLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	PackageLogID INT NOT NULL,
	SourceName VARCHAR(255) NOT NULL,
	SourceID UNIQUEIDENTIFIER NOT NULL,
	StartDateTime DATETIME NOT NULL,
	EndDateTime DATETIME NULL
) ON WFSSSISLogging(CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE dbo.PackageTaskLog ADD
	CONSTRAINT PK_PackageTaskLog PRIMARY KEY CLUSTERED (PackageTaskLogID, CreationDateKey);
--ALTER TABLE [dbo].[PackageTaskLog]  WITH CHECK ADD  
--	CONSTRAINT [FK_PackageTaskLog_PackageLog] FOREIGN KEY([PackageLogID]) REFERENCES [dbo].[PackageLog] ([PackageLogID]);

CREATE NONCLUSTERED INDEX IDX_PackageTaskLog_PackageLogSourceEndDateTime 
	ON dbo.PackageTaskLog 
	(PackageLogID ASC, SourceID ASC, EndDateTime ASC, CreationDateKey ASC) ON WFSSSISLogging(CreationDateKey);