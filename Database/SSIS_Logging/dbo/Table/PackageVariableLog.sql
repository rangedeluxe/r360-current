--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName PackageVariableLog
--WFSScriptProcessorTableDrop
IF OBJECT_ID('dbo.PackageVariableLog') IS NOT NULL
       DROP TABLE dbo.PackageVariableLog;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50317 JPB	Created.
* 06/20/2013 WI 105904 JPB	Updated for 2.0.
* 06/12/2017 PT 146868813	MGE Added Partitioning
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.PackageVariableLog
(
	PackageVariableLogID INT IDENTITY(1,1) NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_PackageVariableLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	PackageLogID INT NOT NULL,
	VariableName VARCHAR(255) NOT NULL,
	VariableValue VARCHAR(MAX) NOT NULL,
	LogDateTime DATETIME NOT NULL
) ON WFSSSISLogging(CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE dbo.PackageVariableLog ADD
	CONSTRAINT PK_PackageVariableLog PRIMARY KEY CLUSTERED (PackageVariableLogID, CreationDateKey);
--ALTER TABLE [dbo].[PackageVariableLog]  WITH CHECK ADD  
--	CONSTRAINT [FK_PackageVariableLog_PackageLog] FOREIGN KEY([PackageLogID]) REFERENCES [dbo].[PackageLog] ([PackageLogID])

