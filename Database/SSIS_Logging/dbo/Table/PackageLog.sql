--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName PackageLog
--WFSScriptProcessorTableDrop
IF OBJECT_ID('dbo.PackageLog') IS NOT NULL
       DROP TABLE dbo.PackageLog;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50314 JPB	Created.
* 06/20/2013 WI 105902 JPB	Updated for 2.0.
* 06/12/2017 PT 146868813	MGE Added Partitioning
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.PackageLog
(
	PackageLogID int IDENTITY(1,1) NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_PackageLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	BatchLogID INT NOT NULL,
	PackageVersionID INT NOT NULL,
	ExecutionInstanceID UNIQUEIDENTIFIER NOT NULL,
	MachineName VARCHAR(64) NOT NULL,
	UserName VARCHAR(64) NOT NULL,
	StartDateTime DATETIME NOT NULL 
		CONSTRAINT DF_PackageLog_StartDateTime DEFAULT GETDATE(),
	EndDateTime DATETIME NULL,
	[Status] CHAR(1) NOT NULL
) ON WFSSSISLogging(CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE [dbo].[PackageLog]  ADD
	CONSTRAINT PK_PackageLog PRIMARY KEY CLUSTERED (PackageLogID, CreationDateKey);  
--ALTER TABLE dbo.PackageLog  WITH CHECK ADD  
--	CONSTRAINT FK_PackageLog_BatchLog FOREIGN KEY(BatchLogID) REFERENCES dbo.BatchLog (BatchLogID),
--	CONSTRAINT FK_PackageLog_PackageVersion FOREIGN KEY(PackageVersionID) REFERENCES dbo.PackageVersion (PackageVersionID)
