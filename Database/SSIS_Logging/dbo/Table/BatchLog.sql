--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName BatchLog
--WFSScriptProcessorTableDrop
IF OBJECT_ID('dbo.BatchLog') IS NOT NULL
       DROP TABLE dbo.BatchLog;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50320		JPB	Created.
* 06/20/2013 WI 105899		JPB	Updated to 2.0.
* 06/12/2017 PT 146868813	MGE Added Partitioning
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE dbo.BatchLog
(
	BatchLogID INT IDENTITY(1,1) NOT NULL,
	CreationDateKey INT NOT NULL
		CONSTRAINT DF_BatchLog_CreationDateKey DEFAULT CAST(CONVERT(VARCHAR(10), GetDate(), 112) AS INT),
	StartDateTime DATETIME NOT NULL 
		CONSTRAINT DF_BatchLog_StartDateTime DEFAULT GETDATE(),
	EndDateTime DATETIME NULL,
	[Status] CHAR(1) NOT NULL
) ON WFSSSISLogging(CreationDateKey);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE dbo.BatchLog ADD
	CONSTRAINT PK_BatchLog PRIMARY KEY CLUSTERED (BatchLogID, CreationDateKey);
