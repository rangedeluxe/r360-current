--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTableName Package
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: Stored information about SSIS Packages. Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50312 JPB	Created.
* 06/20/2013 WI 105900 JPB	Updated for 2.0.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE [dbo].[Package]
(
	[PackageID] [int] IDENTITY(1,1) NOT NULL
		CONSTRAINT [PK_Package] PRIMARY KEY CLUSTERED,
	[PackageGUID] [uniqueidentifier] NOT NULL,
	[PackageName] [varchar](255) NOT NULL,
	[CreationDate] [datetime] NOT NULL,
	[CreatedBy] [varchar](255) NOT NULL,
	[EnteredDateTime] [datetime] NOT NULL 
		CONSTRAINT DF_Package_EnteredDateTime DEFAULT GETDATE()
)
--WFSScriptProcessorTableProperties
