--  06/20/2013	WI 106540 JPB Created for 2.0 release.
--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStaticDataName Create User Accounts
--WFSScriptPRocessorStaticDataCreateBegin
USE [master]
GO
IF NOT EXISTS (SELECT * FROM sys.syslogins WHERE [name] = 'WFSLogging_User')
	CREATE LOGIN [WFSLogging_User] WITH PASSWORD=N'user', DEFAULT_DATABASE=[$(DBName)], CHECK_EXPIRATION=OFF, CHECK_POLICY=OFF;
GO

USE [$(DBName)]
GO
IF NOT EXISTS (SELECT * FROM sys.sysusers WHERE [name] = 'WFSLogging_User')
	BEGIN
		CREATE ROLE dbRole_WFSLogging;
		CREATE USER [WFSLogging_User] FOR LOGIN [WFSLogging_User];
		ALTER USER [WFSLogging_User] WITH DEFAULT_SCHEMA=[dbo];
		EXEC sp_addrolemember N'dbRole_WFSLogging', N'WFSLogging_User';
		GRANT EXECUTE ON dbo.usp_PackageLastRunReport TO dbRole_WFSLogging;
		GRANT EXECUTE ON dbo.usp_PackageListRuns TO dbRole_WFSLogging;
		GRANT EXECUTE ON dbo.usp_PackageRunReport TO dbRole_WFSLogging;
	END

--WFSScriptPRocessorStaticDataCreateEnd