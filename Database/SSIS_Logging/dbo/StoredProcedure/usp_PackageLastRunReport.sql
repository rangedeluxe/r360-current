--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_PackageLastRunReport
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_PackageLastRunReport') IS NOT NULL
       DROP PROCEDURE dbo.usp_PackageLastRunReport
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[usp_PackageLastRunReport]
(
	@parmPackageName VARCHAR(128)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/02/2012
*
* Purpose: 
*		   
*
* Modification History
* 02/08/2012 CR 52037 JPB	Created
* 06/20/2013 WI 105912 JPB	Updated for 2.0. Add change log info.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @PackageID INT,
		@PackageVersionID INT,
		@PackageLogID INT;

BEGIN TRY
			
	/* Get the package ID */		
	SELECT @PackageID = PackageID FROM dbo.Package WHERE PackageName = @parmPackageName;

	/* Get the latest version ID */
	SELECT @PackageVersionID = MAX(PackageVersionID) FROM dbo.PackageVersion WHERE PackageID = @PackageID;

	/* Get the latest package log ID */
	SELECT @PackageLogID = MAX(PackageLogID) FROM dbo.PackageLog WHERE PackageVersionID = @PackageVersionID;

	/* Summary of the execution */
	SELECT 	
		PackageName,
		VersionMajor,
		VersionMinor,
		VersionBuild,
		CASE [Status]
			WHEN 'S' THEN 'Success'
			WHEN 'F' THEN 'Fail'
			WHEN 'R' THEN 'Running'
			ELSE 'Unknown'
		END
		AS Results,
		StartDateTime,
		EndDateTime,
		CASE
			WHEN EndDateTime IS NOT NULL THEN 
				CAST(((DATEDIFF(s,StartDateTime,EndDateTime))/3600) AS VARCHAR) + ' hour(s), '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%3600)/60 AS VARCHAR) + ' min, '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%60) AS VARCHAR) + ' sec'
			ELSE NULL
			END AS ExecutionTime
	FROM	
		dbo.PackageLog 
		INNER JOIN dbo.PackageVersion ON dbo.PackageVersion.PackageVersionID = dbo.PackageLog.PackageVersionID
		INNER JOIN dbo.Package ON dbo.Package.PackageID = dbo.PackageVersion.PackageID
	WHERE	
		PackageLogID = @PackageLogID

	/* Get the high level tasks start/end/run time */
	SELECT	
		SourceName,
		StartDateTime,
		EndDateTime,
		CASE
			WHEN EndDateTime IS NOT NULL THEN 
				CAST(((DATEDIFF(s,StartDateTime,EndDateTime))/3600) AS VARCHAR) + ' hour(s), '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%3600)/60 AS VARCHAR) + ' min, '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%60) AS VARCHAR) + ' sec'
			ELSE NULL
			END AS ExecutionTime
	FROM	
		dbo.PackageTaskLog 
	WHERE	
		PackageLogID = @PackageLogID
	
	/* Get any var change values */
	IF EXISTS( SELECT 1 FROM dbo.PackageVariableLog WHERE PackageVariableLog.PackageLogID = @PackageLogID )
		SELECT * FROM dbo.PackageVariableLog WHERE PackageVariableLog.PackageLogID = @PackageLogID AND PackageVariableLog.VariableName NOT LIKE '{%';

	/* Get any errors */
	IF EXISTS( SELECT 1 FROM dbo.PackageErrorLog WHERE PackageErrorLog.PackageLogID = @PackageLogID )
		SELECT * FROM dbo.PackageErrorLog WHERE PackageErrorLog.PackageLogID = @PackageLogID
END TRY
BEGIN CATCH
END CATCH
