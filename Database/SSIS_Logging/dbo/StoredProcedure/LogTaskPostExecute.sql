--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName LogTaskPostExecute
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.LogTaskPostExecute') IS NOT NULL
       DROP PROCEDURE dbo.LogTaskPostExecute;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.LogTaskPostExecute
(    
	@PackageLogID	INT,
	@SourceID		UNIQUEIDENTIFIER,
	@PackageID		UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: 
*	Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50322 JPB	Created.
* 09/09/2012 CR	55537 JPB	Added deadlock handling.
* 06/20/2013 WI 105909 JPB	Updated to 2.0.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @RetryCount		INT,
		@ErrorMessage   NVARCHAR(4000),
		@ErrorSeverity  INT,
		@ErrorState     INT;

BEGIN TRY
	SET @RetryCount = 4;

	WHILE( @RetryCount >= 0 )
	BEGIN
		BEGIN TRY
			IF @PackageID <> @SourceID
			BEGIN
				UPDATE	
					dbo.PackageTaskLog 
				SET		
					EndDateTime = GETDATE()
				WHERE	
					PackageLogID = @PackageLogID 
					AND SourceID = @SourceID
					AND EndDateTime IS NULL;
				SET @RetryCount = -1;
			END
			ELSE SET @RetryCount = -1;
		END TRY
		BEGIN CATCH
			/* 
				Catch deadlock condition 
				If a deadlock is found, wait a few seconds and try again
				If it is not a deadlock, pass control to the main catch block and 
					return that information to the caller.
			*/
			
			IF (ERROR_NUMBER() = 1205) AND (@RetryCount > 0)
			BEGIN
				SET @RetryCount = @RetryCount - 1;
				WAITFOR DELAY '00:00:05'; /* wait 5 seconds and try again */
			END
			ELSE
			BEGIN
				SET @RetryCount = -1;		
				SELECT	@ErrorMessage = ERROR_MESSAGE(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE();
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			END
		END CATCH
	END
END TRY
BEGIN CATCH
	SELECT	@ErrorMessage = ERROR_MESSAGE(),
			@ErrorSeverity = ERROR_SEVERITY(),
			@ErrorState = ERROR_STATE();
	RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
END CATCH