--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName LogPackageEnd
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.LogPackageEnd') IS NOT NULL
       DROP PROCEDURE dbo.LogPackageEnd
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[LogPackageEnd]
(    
	@PackageLogID	INT,
	@BatchLogID		INT,
	@EndBatchAudit	BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: 
*	Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50318 JPB	Created.
* 06/20/2013 WI 105906 JPB	Updated for 2.0
******************************************************************************/
SET NOCOUNT ON;
BEGIN
	UPDATE	
		dbo.PackageLog
	SET 
		[Status] = 'S',
		EndDatetime = GETDATE()
	WHERE 
		PackageLogID = @PackageLogID;

	IF @EndBatchAudit = 1
	BEGIN
		UPDATE	
			dbo.BatchLog
		SET		
			[Status] = 'S',
			EndDatetime = GETDATE()
		WHERE	
			BatchLogID = @BatchLogID;
	END
END