--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName LogPackageStart
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.LogPackageStart') IS NOT NULL
       DROP PROCEDURE dbo.LogPackageStart;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[LogPackageStart]
(    
	@BatchLogID				INT,
	@PackageName			VARCHAR(255),
	@ExecutionInstanceID	UNIQUEIDENTIFIER,
	@MachineName			VARCHAR(64),
	@UserName				VARCHAR(64),
	@StartDatetime			DATETIME,
	@PackageVersionGUID		UNIQUEIDENTIFIER,
	@VersionMajor			INT,
	@VersionMinor			INT,
	@VersionBuild			INT,
	@VersionComment			VARCHAR(1000),
	@PackageGUID			UNIQUEIDENTIFIER,
	@CreationDate			DATETIME,
	@CreatedBy				VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: 
*	Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50321 JPB	Created.
* 06/20/2013 WI 105908 JPB	Updated for 2.0.
******************************************************************************/
SET NOCOUNT ON;

DECLARE	@PackageID INT,
		@PackageVersionID INT,
		@PackageLogID INT,
		@EndBatchAudit BIT;

BEGIN

	/* Initialize Variables */
	SELECT @EndBatchAudit = 0;

	/* Get Package Metadata ID */
	IF NOT EXISTS (SELECT 1 FROM dbo.Package WHERE PackageGUID = @PackageGUID AND PackageName = @PackageName)
	BEGIN
		INSERT INTO dbo.Package (PackageGUID, PackageName, CreationDate, CreatedBy)
		VALUES (@PackageGUID, @PackageName, @CreationDate, @CreatedBy);
	END

	SELECT	
		@PackageID = PackageID
	FROM	
		dbo.Package 
	WHERE	
		PackageGUID = @PackageGUID
		AND PackageName = @PackageName;

	/* Get Package Version MetaData ID */
	IF NOT EXISTS (SELECT 1 FROM dbo.PackageVersion WHERE PackageVersionGUID = @PackageVersionGUID)
	BEGIN
		INSERT INTO dbo.PackageVersion (PackageID, PackageVersionGUID, VersionMajor, VersionMinor, VersionBuild, VersionComment)
		VALUES (@PackageID, @PackageVersionGUID, @VersionMajor, @VersionMinor, @VersionBuild, @VersionComment);
	END
	
	SELECT	
		@PackageVersionID = PackageVersionID
	FROM	
		dbo.PackageVersion 
	WHERE	
		PackageVersionGUID = @PackageVersionGUID;

	/* Get BatchLogID */
	IF ISNULL(@BatchLogID,0) = 0
	BEGIN
		INSERT INTO dbo.BatchLog (StartDatetime, [Status]) VALUES (@StartDatetime, 'R');
		SELECT @BatchLogID = SCOPE_IDENTITY();
		SELECT @EndBatchAudit = 1;
	END

	/* Create PackageLog Record */
	INSERT INTO dbo.PackageLog (BatchLogID, PackageVersionID, ExecutionInstanceID, MachineName, UserName, StartDatetime, [Status])
	VALUES(@BatchLogID, @PackageVersionID, @ExecutionInstanceID, @MachineName, @UserName, @StartDatetime, 'R');

	SELECT @PackageLogID = SCOPE_IDENTITY();

	SELECT	
		@BatchLogID AS BatchLogID, 
		@PackageLogID AS PackageLogID, 
		@EndBatchAudit AS EndBatchAudit;
END