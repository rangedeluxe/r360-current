--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_PackageListRuns
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_PackageListRuns') IS NOT NULL
       DROP PROCEDURE dbo.usp_PackageListRuns;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_PackageListRuns
(
	@parmPackageName	VARCHAR(128),
	@parmStartTime		DATETIME = NULL,
	@parmEndTime		DATETIME = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/02/2012
*
* Purpose: 
*		   
*
* Modification History
* 02/08/2012 CR 52039 JPB	Created
* 06/20/2013 WI 105913 JPB	Updated for 2.0.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
BEGIN TRY
				
	/* List the runs order by StartDateTime */
	SELECT	
		PackageLogID,
		VersionMajor,
		VersionMinor,
		VersionBuild,
		CASE [Status]
			WHEN 'S' THEN 'Success'
			WHEN 'F' THEN 'Fail'
			WHEN 'R' THEN 'Running'
			ELSE 'Unknown'
		END
		AS Results,		
		StartDateTime,
		EndDateTime,
		CASE
			WHEN EndDateTime IS NOT NULL THEN 
				CAST(((DATEDIFF(s,StartDateTime,EndDateTime))/3600) AS VARCHAR) + ' hour(s), '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%3600)/60 AS VARCHAR) + ' min, '
					+ CAST((DATEDIFF(s,StartDateTime,EndDateTime)%60) AS VARCHAR) + ' sec'
			ELSE NULL
			END AS ExecutionTime
	FROM	
		dbo.PackageLog
		INNER JOIN dbo.PackageVersion ON dbo.PackageVersion.PackageVersionID = dbo.PackageLog.PackageVersionID
		INNER JOIN dbo.Package ON dbo.Package.PackageID = dbo.PackageVersion.PackageID
	WHERE	
		PackageName = @parmPackageName
		AND (StartDateTime >= @parmStartTime OR @parmStartTime IS NULL)
		AND (EndDateTime <= @parmEndTime OR @parmEndTime IS NULL)
	ORDER BY 
		dbo.PackageLog.StartDateTime;
END TRY
BEGIN CATCH
END CATCH
