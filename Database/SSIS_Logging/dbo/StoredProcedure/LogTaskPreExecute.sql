--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName LogTaskPreExecute
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.LogTaskPreExecute') IS NOT NULL
       DROP PROCEDURE dbo.LogTaskPreExecute;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.LogTaskPreExecute
(    
	@PackageLogID	INT,
	@SourceName		VARCHAR(64),
	@SourceID		UNIQUEIDENTIFIER,
	@PackageID		UNIQUEIDENTIFIER
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2012
*
* Purpose: 
*	Adopted from 
*	"Microsoft SQL Server 2008 Integration Services Problem - Design - Solution"
*	Copyright � 2010 Wiley Publishing, Inc.
*		   
*
* Modification History
* 02/16/2012 CR 50323 JPB	Created.
* 06/20/2013 WI 105910 JPB	Updated for 2.0.
******************************************************************************/
SET NOCOUNT ON;
BEGIN
	IF( @PackageID <> @SourceID
		AND @SourceName <> 'SQL LogPackageStart'
		AND @SourceName <> 'SQL LogPackageEnd'
		AND @SourceName <> 'SQL LogTaskPreExecute' 
		AND @SourceName <> 'SQL LogPostTaskExecute'  )
		INSERT INTO dbo.PackageTaskLog (PackageLogID, SourceName, SourceID, StartDateTime)
		VALUES (@PackageLogID, @SourceName, @SourceID, GETDATE());
END