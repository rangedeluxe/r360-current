--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_WFSSSISLogging_PartitionMaintenance
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_WFSSSISLogging_PartitionMaintenance') IS NOT NULL
       DROP PROCEDURE dbo.usp_WFSSSISLogging_PartitionMaintenance
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_WFSSSISLogging_PartitionMaintenance
	@parmLogRetentionDays INT
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 6/14/2017
*
* Purpose: Switches out oldest partitions (based on RetentionDays in SystemSetup) and adds partitions necessary
*      to remain at 52 partitions into the future.
*
* Modification History
* 06/14/2017 PT	143256699	MGE	Created
************************************************************************************************************************************/
DECLARE @ReturnValue INT,
       @RangeDateKey INT,
	   @HighestRangeDateKey INT,
	   @SwitchPartitionNumber INT,
	   @HighestRangeDateTime DATETIME,
	   @OldestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonKey sql_variant,
	   @HighestExistingPartitonDateTime DATETIME,
	   @TempDateTime DATETIME,
       @DiskPath VARCHAR(1000),
       @LogRetentionDays INT,
       @OldestCalendarDateToKeep DateTime,
	   @EndDate	DATETIME,
       @LastDiskID INT, 
	   @Message VARCHAR(max),
       @SQLcmd VARCHAR(max)

SET @ReturnValue = 0
RAISERROR('Beginning process to maintain WFSSSISLogging partitions.',10,1) WITH NOWAIT;
BEGIN TRY
       --<<< CALCULATE First partition using RetentionDays >>>
	SELECT @LogRetentionDays = @parmLogRetentionDays;
	
	PRINT 'LogRetentionDays = ' + Convert(VARCHAR(10), @LogRetentionDays, 101)
	SELECT @OldestCalendarDateToKeep = GETDATE() - @LogRetentionDays		-- n days prior to today
	PRINT 'Calculated Oldest Date to keep = ' + Convert(VARCHAR(10), @OldestCalendarDateToKeep, 101)
	IF( DATEPART(WEEKDAY, @OldestCalendarDateToKeep) <> 2 )
	BEGIN
		SELECT 
			@OldestCalendarDateToKeep = 
				CASE DATEPART(WEEKDAY, @OldestCalendarDateToKeep)
					WHEN 1 THEN DATEADD(dd,-6,@OldestCalendarDateToKeep)					
					WHEN 3 THEN DATEADD(dd,-1,@OldestCalendarDateToKeep)
					WHEN 4 THEN DATEADD(dd,-2,@OldestCalendarDateToKeep)
					WHEN 5 THEN DATEADD(dd,-3,@OldestCalendarDateToKeep)
					WHEN 6 THEN DATEADD(dd,-4,@OldestCalendarDateToKeep)
					WHEN 7 THEN DATEADD(dd,-5,@OldestCalendarDateToKeep)
				END
	END
	SELECT @RangeDateKey = CAST(CONVERT(CHAR(8),@OldestCalendarDateToKeep,112) AS INT)  --(Select Top 1 DateKey FROM RecHubData.dimDates WHERE CalendarDate < @OldestCalendarDateToKeep AND CalendarDayName = 'Monday' ORDER BY CalendarDate DESC)
	SELECT @Message = 'Oldest Partition to keep is: ' + CONVERT(CHAR(10),@RangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
    --<<< Calculate last partition by adding 52 weeks to today >>>
	SELECT @EndDate = GETDATE() +	364	
	IF( DATEPART(WEEKDAY, @EndDate) <> 2 )
	BEGIN
		SELECT 
			@EndDate = 
				CASE DATEPART(WEEKDAY, @EndDate)
					WHEN 1 THEN DATEADD(dd,1,@EndDate)					
					WHEN 3 THEN DATEADD(dd,6,@EndDate)
					WHEN 4 THEN DATEADD(dd,5,@EndDate)
					WHEN 5 THEN DATEADD(dd,4,@EndDate)
					WHEN 6 THEN DATEADD(dd,3,@EndDate)
					WHEN 7 THEN DATEADD(dd,2,@EndDate)
				END
	END					-- 52 weeks into the future
	SELECT @HighestRangeDateKey = CAST(CONVERT(CHAR(8),@EndDate,112) AS INT)
	SELECT @HighestRangeDateTime = @EndDate
	SELECT @Message = 'Last Future Partition to add is: ' + CONVERT(CHAR(10),@HighestRangeDateKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	
	--<<< Find oldest existing Partition >>>

	SELECT @OldestExistingPartitonKey = Value FROM sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'WFSSSISLogging_Partition_Range' AND boundary_id = 1;
	SELECT @Message = 'Oldest Existing Partition is: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;

	--<<< Find highest existing Partition >>>

	SELECT @HighestExistingPartitonKey = MAX(Value) FROM sys.partition_range_values
		INNER JOIN sys.partition_functions ON sys.partition_functions.function_id = sys.partition_range_values.function_id
	WHERE sys.partition_functions.name = 'WFSSSISLogging_Partition_Range'
	SELECT @Message = 'Highest Existing Partition Key is: ' + CONVERT(CHAR(10),@HighestExistingPartitonKey,101)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	SELECT @HighestExistingPartitonDateTime = CAST(@HighestExistingPartitonKey AS VARCHAR(10))
	SELECT @Message = 'Highest Existing Partition DateTime is: ' + CONVERT(CHAR(10),@HighestExistingPartitonDateTime,111)
	RAISERROR(@Message, 10,1) WITH NOWAIT;
	--<<< Determine if there is work to do >>>
	
	IF	(@OldestExistingPartitonKey >= @RangeDateKey)
		BEGIN
			SELECT @Message = 'No partitions to Switch out '
			RAISERROR(@Message, 10,1) WITH NOWAIT;
		END
	ELSE
	  BEGIN
	  	  
	  WHILE (@OldestExistingPartitonKey < @RangeDateKey)
		BEGIN
			
			IF EXISTS (SELECT 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'dbo'
				AND sys.tables.name = 'SwitchablePackageVariableLog')
				BEGIN
					RAISERROR('Dropping SwitchablePackageVariableLog table', 10,1) WITH NOWAIT;
					DROP TABLE dbo.SwitchablePackageVariableLog;
				END

			IF EXISTS (SELECT 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'dbo'
				AND sys.tables.name = 'SwitchablePackageTaskLog')
				BEGIN
					RAISERROR('Dropping SwitchablePackageTaskLog table', 10,1) WITH NOWAIT;
					DROP TABLE dbo.SwitchablePackageTaskLog;
				END

			IF EXISTS (SELECT 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'dbo'
				AND sys.tables.name = 'SwitchablePackageErrorLog')
				BEGIN
					RAISERROR('Dropping SwitchablePackageErrorLog table', 10,1) WITH NOWAIT;
					DROP TABLE dbo.SwitchablePackageErrorLog;
				END

			IF EXISTS (SELECT 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'dbo'
				AND sys.tables.name = 'SwitchablePackageLog')
				BEGIN
					RAISERROR('Dropping SwitchablePackageLog table', 10,1) WITH NOWAIT;
					DROP TABLE dbo.SwitchablePackageLog;
				END

			IF EXISTS (SELECT 1 FROM sys.tables
				INNER JOIN sys.schemas ON sys.tables.schema_id = sys.schemas.schema_id
				WHERE sys.schemas.name = 'dbo'
				AND sys.tables.name = 'SwitchableBatchLog')
				BEGIN
					RAISERROR('Dropping SwitchableBatchLog table', 10,1) WITH NOWAIT;
					DROP TABLE dbo.SwitchableBatchLog;
				END

			SELECT @Message = 'Creating SwitchablePackageVariableLog table on WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE dbo.SwitchablePackageVariableLog
			(
				PackageVariableLogID INT IDENTITY(1,1) NOT NULL,
				CreationDateKey INT NOT NULL,
				PackageLogID INT NOT NULL,
				VariableName VARCHAR(255) NOT NULL,
				VariableValue VARCHAR(MAX) NOT NULL,
				LogDateTime DATETIME NOT NULL
			) ON WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			
			EXEC (@SQLcmd);
			ALTER TABLE dbo.SwitchablePackageVariableLog ADD
				CONSTRAINT IDX_PackageVariableLog PRIMARY KEY CLUSTERED (PackageVariableLogID, CreationDateKey);

			SELECT @Message = 'Creating SwitchablePackageTaskLog table on WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE dbo.SwitchablePackageTaskLog
			(
				PackageTaskLogID INT IDENTITY(1,1) NOT NULL,
				CreationDateKey INT NOT NULL,
				PackageLogID INT NOT NULL,
				SourceName VARCHAR(255) NOT NULL,
				SourceID UNIQUEIDENTIFIER NOT NULL,
				StartDateTime DATETIME NOT NULL,
				EndDateTime DATETIME NULL
			) ON WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			
			EXEC (@SQLcmd);
			ALTER TABLE dbo.SwitchablePackageTaskLog ADD
				CONSTRAINT IDX_PackageTaskLog PRIMARY KEY CLUSTERED (PackageTaskLogID, CreationDateKey);

			SELECT @Message = 'Creating SwitchablePackageErrorLog table on WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE dbo.SwitchablePackageErrorLog
			(
				PackageErrorLogID INT IDENTITY(1,1) NOT NULL,
				CreationDateKey INT NOT NULL,
				PackageLogID INT NOT NULL,
				SourceName VARCHAR(64) NOT NULL,
				SourceID UNIQUEIDENTIFIER NOT NULL,
				[ErrorCode] INT NULL,
				ErrorDescription VARCHAR(2000) NULL,
				LogDateTime DATETIME NOT NULL
			) ON WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));

			EXEC (@SQLcmd);
			ALTER TABLE dbo.SwitchablePackageErrorLog ADD
				CONSTRAINT IDX_PackageErrorLog PRIMARY KEY CLUSTERED (PackageErrorLogID, CreationDateKey);
			
			SELECT @Message = 'Creating SwitchablePackageLog table on WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE dbo.SwitchablePackageLog
			(
				PackageLogID int IDENTITY(1,1) NOT NULL,
				CreationDateKey INT NOT NULL,
				BatchLogID INT NOT NULL,
				PackageVersionID INT NOT NULL,
				ExecutionInstanceID UNIQUEIDENTIFIER NOT NULL,
				MachineName VARCHAR(64) NOT NULL,
				UserName VARCHAR(64) NOT NULL,
				StartDateTime DATETIME NOT NULL, 
				EndDateTime DATETIME,
				[Status] CHAR(1) NOT NULL
			) ON WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));

			EXEC (@SQLcmd);
			ALTER TABLE dbo.SwitchablePackageLog ADD
				CONSTRAINT IDX_PackageLog PRIMARY KEY CLUSTERED (PackageLogID, CreationDateKey);

			SELECT @Message = 'Creating SwitchableBatchLog table on WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 
			'CREATE TABLE dbo.SwitchableBatchLog
			(
				BatchLogID INT IDENTITY(1,1) NOT NULL,
				CreationDateKey INT NOT NULL,
				StartDateTime DATETIME NOT NULL,
				EndDateTime DATETIME,
				[Status] CHAR(1) NOT NULL
			) ON WFSSSISLogging' + CAST(@OldestExistingPartitonKey AS VARCHAR(10));

			EXEC (@SQLcmd);
			ALTER TABLE dbo.SwitchableBatchLog ADD
				CONSTRAINT IDX_BatchLog PRIMARY KEY CLUSTERED (BatchLogID, CreationDateKey);
						
			/* <<<< Now do the SWITCHES  >>>>>>>>>>>> */	
			SELECT @SwitchPartitionNumber = partition_number FROM LoggingSystem.FileGroupDetailView 
				WHERE range_value = @OldestExistingPartitonKey AND partition_scheme_name = 'WFSSSISLogging'
			
			SELECT @Message = 'Switching out the oldest partition: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER TABLE dbo.PackageVariableLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO dbo.SwitchablePackageVariableLog'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			SELECT @SQLcmd = 'ALTER TABLE dbo.PackageTaskLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO dbo.SwitchablePackageTaskLog'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			SELECT @SQLcmd = 'ALTER TABLE dbo.PackageLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO dbo.SwitchablePackageLog'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);

			SELECT @SQLcmd = 'ALTER TABLE dbo.BatchLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO dbo.SwitchableBatchLog'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			SELECT @SQLcmd = 'ALTER TABLE dbo.PackageErrorLog SWITCH PARTITION ' + CAST(@SwitchPartitionNumber AS VARCHAR(8)) + ' TO dbo.SwitchablePackageErrorLog'
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd); 

			SELECT @Message = 'Merging the oldest range: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SELECT @SQLcmd = 'ALTER PARTITION FUNCTION WFSSSISLogging_Partition_Range() MERGE RANGE (' + (CONVERT(CHAR(10),@OldestExistingPartitonKey,101)) + ')';
			RAISERROR(@SQLcmd, 10,1) WITH NOWAIT;
			EXEC (@SQLcmd);
			
			DROP TABLE dbo.SwitchablePackageVariableLog
			DROP TABLE dbo.SwitchablePackageTaskLog
			DROP TABLE dbo.SwitchablePackageErrorLog
			DROP TABLE dbo.SwitchablePackageLog
			DROP TABLE dbo.SwitchableBatchLog

			SELECT @Message = 'Removing the oldest partiton file ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILE WFSSSISLogging'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SELECT @Message = 'Removing the oldest partition filegroup: ' + CONVERT(CHAR(10),@OldestExistingPartitonKey,101);
			RAISERROR(@Message, 10,1) WITH NOWAIT;
			SET @SQLcmd = 'ALTER DATABASE ' + DB_NAME() +' REMOVE FILEGROUP WFSSSISLogging'+CAST(@OldestExistingPartitonKey AS VARCHAR(8));
			EXEC (@SQLcmd);
			
			SET @TempDateTime = Convert(VARCHAR(10),(CONVERT(datetime, convert(varchar(10), @OldestExistingPartitonKey)) + 7),101);
			select @OldestExistingPartitonKey = CAST(CONVERT(CHAR(8), @TempDateTime, 112) AS INT)
			
		END 
	  END
	  
	--<<< Add Partitions up through @HighestRangeKey >>>
    
	IF (@HighestExistingPartitonKey < @HighestRangeDateKey)
		BEGIN
		RAISERROR('Creating new WFSSSISLogging partitions',10,1) WITH NOWAIT;
		SET @HighestExistingPartitonDateTime = DATEADD(dd,7,@HighestExistingPartitonDateTime)
		SELECT @Message = 'Calling usp_CreatePartitionsForRange WFSSSISLogging ' + CONVERT(VARCHAR(11),@HighestExistingPartitonDateTime)  + ' ' + Convert(Varchar(11),@HighestRangeDateTime);
		RAISERROR(@Message,10,1) WITH NOWAIT;
		EXEC LoggingSystem.usp_CreatePartitionsForRange @HighestExistingPartitonDateTime, @HighestRangeDateTime;
		END
	   
END TRY 
BEGIN CATCH
	EXEC LoggingCommon.usp_WfsRethrowException
END CATCH
