--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_OrganizationKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Get_OrganizationKeys') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Get_OrganizationKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Get_OrganizationKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Retrieve customer keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 02/26/2012 CR 50814 JPB	Created
* 04/09/2013 WI 92082 JBS	Update to 2.0 release. Change schema 
*							Change proc name FROM usp_DataImportQueue_GetCustomerKeys
*							Change references of Customer to Organization
* 01/13/2014 WI 125914 JBS	Change Output name for Organization from SiteCustomerID
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	;WITH BankList AS
	(
		SELECT DISTINCT SiteBankID
		FROM	DITStaging.DataImportWorkBatchStartup
	)
	SELECT	RecHubData.dimOrganizations.OrganizationKey,
			RecHubData.dimOrganizations.SiteBankID AS SiteBankID,
			RecHubData.dimOrganizations.SiteOrganizationID AS SiteOrganizationID,
			RecHubData.dimOrganizations.MostRecent AS MostRecentCustomerKey
	FROM	RecHubData.dimOrganizations
			INNER JOIN BankList ON RecHubData.dimOrganizations.SiteBankID = BankList.SiteBankID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
