--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factBatchSummary_Get_ExistingKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factBatchSummary_Get_ExistingKeys') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factBatchSummary_Get_ExistingKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factBatchSummary_Get_ExistingKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/09/2017
*
* Purpose: Get existing batch keys from RecHubData.factBatchSummary.
*
* Modification History
* 04/07/2017 PT 134074013 JPB	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT
		RecHubData.factBatchSummary.BankKey AS Existing_BankKey,
		RecHubData.factBatchSummary.OrganizationKey AS Existing_OrganizationKey,
		RecHubData.factBatchSummary.ClientAccountKey AS Existing_ClientAccountKey,
		RecHubData.factBatchSummary.DepositDateKey,
		RecHubData.factBatchSummary.ImmutableDateKey,
		RecHubData.factBatchSummary.BatchID AS Existing_BatchID,
		RecHubData.factBatchSummary.SourceBatchID,
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.factBatchSummary.BatchSourceKey
	FROM 
		DITStaging.DataImportWorkBatchKeys
		INNER JOIN RecHubData.dimClientAccounts ON RecHubData.dimClientAccounts.SiteBankID = DITStaging.DataImportWorkBatchKeys.SiteBankID
			AND RecHubData.dimClientAccounts.SiteClientAccountID = DITStaging.DataImportWorkBatchKeys.SiteClientAccountID
		INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
			AND RecHubData.factBatchSummary.ImmutableDateKey = DITStaging.DataImportWorkBatchKeys.ImmutableDateKey
			AND RecHubData.factBatchSummary.BatchSourceKey = DITStaging.DataImportWorkBatchKeys.BatchSourceKey
			AND RecHubData.factBatchSummary.SourceBatchID = DITStaging.DataImportWorkBatchKeys.SourceBatchID
	WHERE 
		RecHubData.factBatchSummary.IsDeleted = 0
	ORDER BY
		RecHubData.factBatchSummary.ImmutableDateKey,
		RecHubData.factBatchSummary.SourceBatchID,
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		RecHubData.factBatchSummary.BatchSourceKey;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH