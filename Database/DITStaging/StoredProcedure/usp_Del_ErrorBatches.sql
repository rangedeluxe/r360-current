--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Del_ErrorBatches
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_Del_ErrorBatches') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_Del_ErrorBatches
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_Del_ErrorBatches
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/06/2017
*
* Purpose: Delete duplicate batch details.
*
* Modification History
* 02/06/2017 PT	#137726225 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @BatchCount BIGINT,
		@ElectronicBatchCount BIGINT;

BEGIN TRY

	DELETE 
		DITStaging.XMLBatch
	FROM 
		DITStaging.XMLBatch
		INNER JOIN DITStaging.DataImportWorkBatchResponses ON DITStaging.DataImportWorkBatchResponses.Batch_Id = DITStaging.XMLBatch.Batch_Id;

	DELETE 
		DITStaging.XMLElectronicBatch
	FROM 
		DITStaging.XMLElectronicBatch
		INNER JOIN DITStaging.DataImportWorkBatchResponses ON DITStaging.DataImportWorkBatchResponses.Batch_Id = DITStaging.XMLElectronicBatch.Batch_Id;

	SELECT 
		@BatchCount = COUNT(*) 
	FROM 
		DITStaging.XMLBatch;
	
	SELECT 
		@ElectronicBatchCount = COUNT(*) 

	FROM 
		DITStaging.XMLElectronicBatch;

	SELECT 
		@BatchCount AS BatchCount, 
		@ElectronicBatchCount AS ElectronicBatchCount;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
