--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Del_ErrorBatchDetails
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_Del_ErrorBatchDetails') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_Del_ErrorBatchDetails
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_Del_ErrorBatchDetails
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/06/2017
*
* Purpose: Delete error batch details.
*
* Modification History
* 02/06/2017 PT	#137726225 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	DECLARE @BatchIDList TABLE
	(
		Batch_Id BIGINT
	);

	INSERT INTO @BatchIDList(Batch_Id)
	SELECT 
		Batch_Id
	FROM 
		DITStaging.DataImportWorkBatchResponses
	WHERE 
		ResponseStatus = 1;

	INSERT INTO @BatchIDList(Batch_Id)
	SELECT DISTINCT
		DITStaging.XMLTransaction.Batch_Id
	FROM
		DITStaging.XMLTransaction
		LEFT JOIN DITStaging.XMLBatch ON DITStaging.XMLBatch.Batch_Id = DITStaging.XMLTransaction.Batch_Id
	WHERE
		DITStaging.XMLBatch.Batch_Id IS NULL;

	DELETE
		DITStaging.XMLBatchData
	FROM
		DITStaging.XMLBatchData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLBatchData.Batch_Id;

	DELETE
		DITStaging.XMLTransaction
	FROM 
		DITStaging.XMLTransaction
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLTransaction.Batch_Id;
	
	DELETE
		DITStaging.XMLDocument
	FROM 
		DITStaging.XMLDocument
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLDocument.Batch_Id;
	
	DELETE
		DITStaging.XMLDocumentData
	FROM 
		DITStaging.XMLDocumentData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLDocumentData.Batch_Id;
	
	DELETE
		DITStaging.XMLDocumentItemData
	FROM 
		DITStaging.XMLDocumentItemData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLDocumentItemData.Batch_Id;
	
	DELETE
		DITStaging.XMLGhostDocument
	FROM 
		DITStaging.XMLGhostDocument
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLGhostDocument.Batch_Id;
	
	DELETE
		DITStaging.XMLGhostDocumentData
	FROM 
		DITStaging.XMLGhostDocumentData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLGhostDocumentData.Batch_Id;

	DELETE
		DITStaging.XMLGhostDocumentItemData
	FROM 
		DITStaging.XMLGhostDocumentItemData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLGhostDocumentItemData.Batch_Id;

	DELETE
		DITStaging.XMLPayment
	FROM 
		DITStaging.XMLPayment
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLPayment.Batch_Id;
	
	DELETE
		DITStaging.XMLPaymentData
	FROM 
		DITStaging.XMLPaymentData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLPaymentData.Batch_Id;
	
	DELETE
		DITStaging.XMLPaymentItemData
	FROM 
		DITStaging.XMLPaymentItemData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLPaymentItemData.Batch_Id;
	
	DELETE
		DITStaging.XMLRawPaymentData
	FROM 
		DITStaging.XMLRawPaymentData
		INNER JOIN @BatchIDList BatchIDList ON BatchIDList.Batch_Id = DITStaging.XMLRawPaymentData.Batch_Id;
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
