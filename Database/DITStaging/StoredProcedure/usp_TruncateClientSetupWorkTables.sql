--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateClientSetupWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_TruncateClientSetupWorkTables') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_TruncateClientSetupWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_TruncateClientSetupWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Truncate tables used by SSIS for importing Client Setup.
*
* Modification History
* 02/26/2012 CR 50834 JPB	Created
* 05/11/2012 CR 52710 JPB	Performance improvements.
* 04/04/2013 WI 92114 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	TRUNCATE TABLE DITStaging.DataImportWorkClientSetup;
	TRUNCATE TABLE DITStaging.DataImportWorkClientSetupResponses;
	TRUNCATE TABLE DITStaging.XMLClient;
	TRUNCATE TABLE DITStaging.XMLClientGroup;
	TRUNCATE TABLE DITStaging.XMLClientGroups;
	TRUNCATE TABLE DITStaging.XMLClientGroupNodes;
	TRUNCATE TABLE DITStaging.XMLClientNodes;
	TRUNCATE TABLE DITStaging.XMLBank;
	TRUNCATE TABLE DITStaging.XMLBankNodes;
	TRUNCATE TABLE DITStaging.XMLImageRPSAliasMappings;
	TRUNCATE TABLE DITStaging.XMLImageRPSAliasMappingsNodes;
	TRUNCATE TABLE DITStaging.XMLDataEntryColumnNodes;
	TRUNCATE TABLE DITStaging.XMLDataEntryColumnsNodes;
	TRUNCATE TABLE DITStaging.XMLDataEntryColumns;
	TRUNCATE TABLE DITStaging.XMLDocumentTypes;
	TRUNCATE TABLE DITStaging.XMLDocumentTypesNodes;
	TRUNCATE TABLE DITStaging.XMLSite;
	TRUNCATE TABLE DITStaging.XMLSiteNodes;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
