--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLElectronicBatch_Del_InProcessDuplicates
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLElectronicBatch_Del_InProcessDuplicates') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLElectronicBatch_Del_InProcessDuplicates
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLElectronicBatch_Del_InProcessDuplicates
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 12/03/2015
*
* Purpose: Find any ACH files that are the same in the given run and remove
*			them from data flow.
*
* Modification History
* 12/03/2015 WI 250268 JPB	Created
* 06/01/2016 WI 283109 JPB	Remove the BatchTrackingID value from the string 
*						before calculating the check sum.
* 02/06/2017 PT #137726225	JPB Return count.
* 09/21/2017 PT #150564042	JPB	Use XML column for duplicate check.
* 07/31/2018 PT #156815221	JPB Pull BatchTrackingID from XMLElectronicBatch
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @DuplicateACHList DITStaging.DuplicateBatchList;

BEGIN TRY

	;WITH BatchTracking_CTE AS
	(
		SELECT	
			DITStaging.XMLElectronicBatch.Batch_Id,
			CHECKSUM(BatchTrackingID) AS BatchTrackingIDCheckSum,
			DITStaging.XMLElectronicBatch.FileHash, 
			DITStaging.XMLElectronicBatch.FileSignature
		FROM
			DITStaging.XMLElectronicBatch
	),
	BatchList_CTE AS 
	(
		SELECT	
			MAX(Batch_Id) AS Batch_Id
		FROM
			BatchTracking_CTE
		GROUP BY 
			FileHash, 
			FileSignature,
			BatchTrackingIDCheckSum
	)	
	INSERT INTO @DuplicateACHList
	(
		Batch_Id,
		BatchTrackingID,
		DepositDate,
		BatchDate,
		ImmutableDate,
		SourceBatchID,
		SiteBankID,
		SiteClientAccountID,
		BatchSource,
		BatchPaymentType,
		BatchSiteCode
	)
	SELECT	
		DITStaging.XMLElectronicBatch.Batch_Id,
		DITStaging.XMLElectronicBatch.BatchTrackingID,
		CAST(DITStaging.XMLElectronicBatch.DepositDate AS DATE) AS DepositDate,
		CAST(DITStaging.XMLElectronicBatch.BatchDate AS DATE) AS BatchDate,
		CAST(DITStaging.XMLElectronicBatch.ImmutableDate AS DATE) AS ImmutableDate,
		0 AS SourceBatchID,
		0 AS SiteBankID,
		0 AS SiteClientAccountID,
		DITStaging.XMLElectronicBatch.BatchSource,
		DITStaging.XMLElectronicBatch.BatchPaymentType,
		0 AS BatchSiteCode
	FROM	
		DITStaging.XMLElectronicBatch
	WHERE	
		DITStaging.XMLElectronicBatch.Batch_Id NOT IN (SELECT Batch_Id FROM BatchList_CTE);

	EXEC DITStaging.usp_DataImportQueue_Upd_SetDuplicateBatchesComplete @parmDuplicateBatchList=@DuplicateACHList;

	DELETE	DITStaging.XMLElectronicBatch
	FROM	DITStaging.XMLElectronicBatch
			INNER JOIN @DuplicateACHList DL ON DITStaging.XMLElectronicBatch.Batch_Id = DL.Batch_Id;
	
	SELECT 
		COUNT(*) AS ElectronicBatchCount
	FROM
		 DITStaging.XMLElectronicBatch;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
