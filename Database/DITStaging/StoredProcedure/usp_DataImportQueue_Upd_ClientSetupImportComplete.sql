--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Upd_ClientSetupImportComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Upd_ClientSetupImportComplete') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Upd_ClientSetupImportComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Upd_ClientSetupImportComplete
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Updating client setups in process to completed.
*
* Modification History
* 02/26/2012 CR 50821 JPB	Created
* 04/09/2013 WI 92088 JBS	Update to 2.0 release. Change schema name
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 
BEGIN TRY
	UPDATE 	RecHubSystem.DataImportQueue 
	SET 	RecHubSystem.DataImportQueue.QueueStatus = 99,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBY = SUSER_SNAME() 
	FROM 	RecHubSystem.DataImportQueue 
			INNER JOIN DITStaging.DataImportWorkClientSetupResponses ON RecHubSystem.DataImportQueue.EntityTrackingID = DITStaging.DataImportWorkClientSetupResponses.ClientTrackingID
	WHERE	QueueStatus = 20;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
