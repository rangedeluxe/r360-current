--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkClientSetup_Upd_Response
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkClientSetup_Upd_Response') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportWorkClientSetup_Upd_Response
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkClientSetup_Upd_Response
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2012
*
* Purpose: 
*
* Modification History
* 08/24/2012 CR 55262 JPB	Created
* 04/10/2013 WI 92095 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_DataImportWorkClientSetup_SetXMLResponseDocument.
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

DECLARE @ClientTrackingID UNIQUEIDENTIFIER,
		@Loop INT,
		@ResponseType TINYINT,
		@XMLResponse XML;

BEGIN TRY

	DECLARE @ResponseList TABLE 
	(
		RowID INT IDENTITY(1,1), 
		ClientTrackingID UNIQUEIDENTIFIER,
		ResponseType TINYINT
	);

	/* Get a distinct list of client tracking IDs from the response table */
	INSERT INTO @ResponseList(ClientTrackingID,ResponseType)
	SELECT	
		DISTINCT ClientTrackingID,
		MIN(ResponseStatus)
	FROM	
		DITStaging.DataImportWorkClientSetupResponses
	GROUP BY 
		ClientTrackingID;
		
	/* Update Work table with error if error row exists */
	UPDATE	
		DITStaging.DataImportWorkClientSetup
	SET		
		ResponseStatus = ResponseType
	FROM	
		DITStaging.DataImportWorkClientSetup
		INNER JOIN @ResponseList RL ON RL.ClientTrackingID = DITStaging.DataImportWorkClientSetup.ClientTrackingID;
	
	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get recods that exist in the response table but not in the work table */
	INSERT INTO @ResponseList(ClientTrackingID,ResponseType)
	SELECT	ClientTrackingID,
			0
	FROM	DITStaging.DataImportWorkClientSetupResponses
	EXCEPT 
	SELECT	ClientTrackingID,
			0
	FROM	DITStaging.DataImportWorkClientSetup;

	UPDATE	
		@ResponseList
	SET		
		ResponseType = ResponseStatus
	FROM	
		@ResponseList RL
		INNER JOIN DITStaging.DataImportWorkClientSetupResponses ON DITStaging.DataImportWorkClientSetupResponses.ClientTrackingID = RL.ClientTrackingID;

	INSERT INTO DITStaging.DataImportWorkClientSetup
	(
		ClientTrackingID,
		ClientGroup_Id,
		ResponseStatus,
		ClientGroupID,
		ClientGroupName
	)
	SELECT	
		DITStaging.XMLClientGroup.ClientTrackingID,
		ClientGroup_Id,
		RL.ResponseType, /*ResponseStatus*/
		ClientGroupID,
		ClientGroupName
	FROM	
		DITStaging.XMLClientGroup
		INNER JOIN @ResponseList RL ON RL.ClientTrackingID = DITStaging.XMLClientGroup.ClientTrackingID;

	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get all records that need to be processed */
	INSERT INTO @ResponseList(ClientTrackingID,ResponseType)
	SELECT	
		ClientTrackingID,
		ResponseStatus
	FROM	
		DITStaging.DataImportWorkClientSetup;
	
	SET @Loop = 1;

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ResponseList) )
	BEGIN

		SELECT	
			@ClientTrackingID = ClientTrackingID,
			@ResponseType = ResponseType
		FROM	
			@ResponseList 
		WHERE 
			RowID = @Loop;


		SELECT @XMLResponse = 
		(
			SELECT	TOP (1) ClientTrackingID AS '@ClientTrackingID',
					ClientGroupID AS '@ClientGroupID',
					ClientGroupName AS '@ClientGroupName',
					(
						SELECT CASE	ResponseStatus
							WHEN 0 THEN 'Success'
							WHEN 1 THEN 'Fail' 
							WHEN 2 THEN 'Warning'
						END AS 'Results/*'
						FROM DITStaging.DataImportWorkClientSetup
						WHERE ClientTrackingID = @ClientTrackingID
						FOR XML PATH(''), TYPE
					),
					(
						SELECT	ResultsMessage AS 'ErrorMessage/*',NULL
						FROM	DITStaging.DataImportWorkClientSetupResponses
						WHERE	C1.ClientTrackingID = DITStaging.DataImportWorkClientSetupResponses.ClientTrackingID
						FOR XML PATH(''), TYPE
					)
			FROM DITStaging.DataImportWorkClientSetup C1
			WHERE ClientTrackingID = @ClientTrackingID
			FOR XML PATH('ClientGroup'), TYPE
		);
				
		UPDATE 
			RecHubSystem.DataImportQueue
		SET	
			XMLResponseDocument = @XMLResponse,
			QueueStatus = 99,
			ResponseStatus = @ResponseType,
			ModificationDate = GETDATE(),
			ModifiedBy = SUSER_NAME()
		WHERE 
			EntityTrackingID = @ClientTrackingID;

		SET @Loop = @Loop + 1;	
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH