IF OBJECT_ID('DITStaging.usp_factStubs_AddMissingStub') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factStubs_AddMissingStub
GO

CREATE PROCEDURE DITStaging.usp_factStubs_AddMissingStub
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2017-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 10/17/2015
*
* Purpose: For every data entry "row" (TranID/BatchSeq) there needs to be a stub row.
*	Add any stubs that are missing based on data entry detail information
*
* Modification History
* 10/17/2015 PT 151414157 JPB	Created
* 01/10/2018 PT 153683396 MGE	Corrected logic used to update factBatchSummary StubCount
* 07/31/2018 PT 156815221 JPB	Correct JOIN when calculating new stub sequence, upate 
*									factTransactionSummary StubCount
* 05/17/2019 R360-16205	  JPB	Added links to documents for missing stubs
*****************************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	DECLARE @MissingStubs TABLE
	(
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT
	);

	DECLARE @MissingStubDocuments TABLE
	(
		BatchID BIGINT,
		TransactionID INT,
		BatchSequence INT,
		DocumentBatchSequence INT
	);


	/* find all related documents */
	;WITH Transactions AS
	(
		SELECT
			DITStaging.XMLTransaction.Batch_Id,
			DITStaging.XMLTransaction.Transaction_Id,
			DITStaging.XMLTransaction.TransactionID,
			DITStaging.XMLTransaction.TransactionSequence
		FROM
			DITStaging.DataImportWorkBatch
			INNER JOIN DITStaging.XMLTransaction ON DITStaging.DataImportWorkBatch.Batch_ID = DITStaging.XMLTransaction.Batch_Id 
	),
	Documents AS
	(
		SELECT
			Transactions.Batch_Id,
			Transactions.Transaction_Id,
			DITStaging.XMLDocument.Document_Id,
			Transactions.TransactionID,
			Transactions.TransactionSequence,
			DITStaging.XMLDocument.BatchSequence AS DocumentBatchSequence,
			DITStaging.XMLDocument.SequenceWithinTransaction,
			DITStaging.XMLDocument.IsCorrespondence
		FROM
			DITStaging.XMLDocument
			INNER JOIN Transactions ON DITStaging.XMLDocument.Batch_Id = Transactions.Batch_Id 
				AND DITStaging.XMLDocument.Transaction_Id = Transactions.Transaction_Id
	),
	DocumentData AS
	(
		SELECT DISTINCT
			DITStaging.XMLDocumentData.Batch_Id,
			Documents.TransactionID,
			Documents.DocumentBatchSequence,
			DITStaging.XMLDocumentData.BatchSequence
		FROM
			DITStaging.XMLDocumentData
			INNER JOIN Documents ON DITStaging.XMLDocumentData.Batch_Id = Documents.Batch_Id 
				AND DITStaging.XMLDocumentData.Transaction_Id = Documents.Transaction_Id 
				AND DITStaging.XMLDocumentData.Document_Id = Documents.Document_Id
	)
	INSERT INTO  @MissingStubDocuments(BatchID,TransactionID,BatchSequence,DocumentBatchSequence)
	SELECT 
		BatchID, 
		TransactionID,
		BatchSequence,
		DocumentBatchSequence 
	FROM DocumentData
		INNER JOIN DITStaging.DataImportWorkBatchKeys ON DITStaging.DataImportWorkBatchKeys.Batch_Id = DocumentData.Batch_Id;

	;WITH DataEntryRows AS
	(
		SELECT DISTINCT 
			BatchID,
			TransactionID,
			BatchSequence 
		FROM 
			DITStaging.factDataEntryDetails 
			INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkGroupDataEntryColumnKey 
		WHERE 
			DITStaging.DataImportWorkgroupDataEntryColumnKeys.IsCheck = 0
		EXCEPT
		SELECT 
			BatchID,
			TransactionID,
			BatchSequence 
		FROM 
			DITStaging.factStubs
	)
	INSERT INTO @MissingStubs
	(
		BatchID,
		TransactionID,
		BatchSequence
	)
	SELECT
		BatchID,
		TransactionID,
		BatchSequence
	FROM 
		DataEntryRows;

	INSERT INTO DITStaging.factStubs
	(
		BatchTrackingID,
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchCueID,
		DepositStatus,
		SystemType,
		TransactionID,
		TxnSequence,
		SequenceWithinTransaction,
		BatchSequence,
		StubSequence,
		IsCorrespondence,
		DocumentBatchSequence,
		IsOMRDetected,
		CreationDate,
		ModificationDate,
		BatchSiteCode
	)
	SELECT
		DITStaging.factTransactionSummary.BatchTrackingID,
		DITStaging.factTransactionSummary.IsDeleted,
		DITStaging.factTransactionSummary.BankKey,
		DITStaging.factTransactionSummary.OrganizationKey,
		DITStaging.factTransactionSummary.ClientAccountKey,
		DITStaging.factTransactionSummary.DepositDateKey,
		DITStaging.factTransactionSummary.ImmutableDateKey,
		DITStaging.factTransactionSummary.SourceProcessingDateKey,
		DITStaging.factTransactionSummary.BatchID,
		DITStaging.factTransactionSummary.SourceBatchID,
		DITStaging.factTransactionSummary.BatchNumber,
		DITStaging.factTransactionSummary.BatchSourceKey,
		DITStaging.factTransactionSummary.BatchPaymentTypeKey,
		DITStaging.factTransactionSummary.BatchCueID,
		DITStaging.factTransactionSummary.DepositStatus,
		DITStaging.factTransactionSummary.SystemType,
		DITStaging.factTransactionSummary.TransactionID,
		DITStaging.factTransactionSummary.TxnSequence,
		DITStaging.factTransactionSummary.TxnSequence AS SequenceWithinTransaction,
		MissingStubs.BatchSequence,
		0 AS StubSequence,
		0 AS IsCorrespondence,
		DocumentBatchSequence,
		0 AS IsOMRDetected,
		DITStaging.factTransactionSummary.CreationDate,
		DITStaging.factTransactionSummary.ModificationDate,
		DITStaging.factTransactionSummary.BatchSiteCode
	FROM
		@MissingStubs AS MissingStubs
		INNER JOIN DITStaging.factTransactionSummary ON DITStaging.factTransactionSummary.BatchID = MissingStubs.BatchID
			AND DITStaging.factTransactionSummary.TransactionID = MissingStubs.TransactionID
		LEFT JOIN @MissingStubDocuments MissingStubDocuments ON MissingStubDocuments.BatchID = MissingStubs.BatchID
			AND MissingStubDocuments.BatchSequence = MissingStubs.BatchSequence
			AND MissingStubDocuments.TransactionID = MissingStubs.TransactionID;

	/* Update the stub sequence so they are in the correct order according to the batch sequence number */
	;WITH UpdateStubSequence AS
	(
		SELECT
			DITStaging.factStubs.BatchID,
			DITStaging.factStubs.BatchSequence,
			ROW_NUMBER() OVER(PARTITION BY DITStaging.factStubs.BatchID ORDER BY DITStaging.factStubs.BatchID,DITStaging.factStubs.BatchSequence) AS StubSequence
		FROM 
			@MissingStubs MissingStubs
			INNER JOIN DITStaging.factStubs ON DITStaging.factStubs.BatchID = MissingStubs.BatchID
				AND DITStaging.factStubs.BatchSequence = MissingStubs.BatchSequence
	)
	UPDATE
		DITStaging.factStubs
	SET
		DITStaging.factStubs.StubSequence = UpdateStubSequence.StubSequence
	FROM
		DITStaging.factStubs
		INNER JOIN UpdateStubSequence ON UpdateStubSequence.BatchID = DITStaging.factStubs.BatchID
			AND UpdateStubSequence.BatchSequence = DITStaging.factStubs.BatchSequence;

	/* Update the stub count on the factBatchSummary record */
	;WITH StubInfo AS
	(
		SELECT
			DITStaging.factStubs.BatchID,
			COUNT(DITStaging.factStubs.BatchID) AS StubCount
		FROM
			DITStaging.factStubs
		GROUP BY
			DITStaging.factStubs.BatchID
	)
	UPDATE
		DITStaging.factBatchSummary
	SET
		DITStaging.factBatchSummary.StubCount = StubInfo.StubCount
	FROM
		DITStaging.factBatchSummary
		INNER JOIN StubInfo ON DITStaging.factBatchSummary.BatchID = StubInfo.BatchID;

	/* Update the stub count on the factTransactionSummary record */
	;WITH StubInfo AS
	(
		SELECT
			DITStaging.factStubs.BatchID,
			COUNT(DITStaging.factStubs.BatchID) AS StubCount,
			DITStaging.factStubs.TransactionID
		FROM
			DITStaging.factStubs
		GROUP BY
			DITStaging.factStubs.BatchID,
			DITStaging.factStubs.TransactionID
	)
	UPDATE
		DITStaging.factTransactionSummary
	SET
		DITStaging.factTransactionSummary.StubCount = StubInfo.StubCount
	FROM
		DITStaging.factTransactionSummary
		INNER JOIN StubInfo ON DITStaging.factTransactionSummary.BatchID = StubInfo.BatchID
			AND DITStaging.factTransactionSummary.TransactionID = StubInfo.TransactionID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH