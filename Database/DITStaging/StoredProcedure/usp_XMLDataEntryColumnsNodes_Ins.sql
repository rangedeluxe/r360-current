--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLDataEntryColumnsNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLDataEntryColumnsNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLDataEntryColumnsNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLDataEntryColumnsNodes_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Extract the DataEntryColumns Node from the XML data and insert into the 
*			DITStaging.XMLDataEntryColumnsNodes table.
*
* Modification History
* 05/01/2012 CR 52750 JPB	Created
* 04/19/2013 WI 92126 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_XMLDataEntryColumnsNodes_Insert
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

DECLARE @xmlString VARCHAR(MAX),
		@StartTag VARCHAR(30),
		@EndTag CHAR(1),
		@ClientGroup_Id INT,
		@Loop INT,
		@RecordCount INT;

BEGIN TRY
	SELECT @RecordCount = COUNT(*) FROM DITStaging.XMLClientGroups;
	SELECT @StartTag = '<DataEntryColumns ',@EndTag = '>';
	SET @Loop = 1;
	
	WHILE( @Loop <= @RecordCount )
	BEGIN
		SELECT	@ClientGroup_Id = ClientGroup_Id,
				@xmlString = xmlString
		FROM	DITStaging.XMLClientGroups
		WHERE	RowID = @Loop;

		IF( CHARINDEX(@StartTag,@xmlString,0) > 0 )
		BEGIN
			;WITH DataEntryColumns_CTE(ClientGroup_Id,StartingPosition, EndingPosition, occurence)
			AS
			(		
				SELECT		@ClientGroup_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,0) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag,@xmlString,CHARINDEX(@StartTag,@xmlString,0)+1) AS INT),
							1 AS occurence
				UNION ALL
				SELECT 		@ClientGroup_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,EndingPosition + 1) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag, @xmlString, CHARINDEX(@StartTag,@xmlString,EndingPosition + 1))AS INT),
							occurence + 1
				FROM DataEntryColumns_CTE
				WHERE CHARINDEX(@StartTag, @xmlString, EndingPosition + 1) <> 0
			)
			INSERT DITStaging.XMLDataEntryColumnsNodes(ClientGroup_Id,xmlString)
			SELECT	@ClientGroup_Id,SUBSTRING(@xmlString, StartingPosition, EndingPosition-StartingPosition+1) AS xmlString 
			FROM	DataEntryColumns_CTE
			OPTION (MaxRecursion 0);
		END

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
