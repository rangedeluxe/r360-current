IF OBJECT_ID('DITStaging.usp_WorkDataEntryWideTable_AddColumns') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_WorkDataEntryWideTable_AddColumns
GO

CREATE PROCEDURE DITStaging.usp_WorkDataEntryWideTable_AddColumns
(
	@parmColumnName				NVARCHAR(128)
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 08/20/2019
*
* Purpose: ALTER WorkDataEntryWideTable ADD column_name
*
* Modification History
* 08/07/2019 R360-16297 MGE	Created
*
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	DECLARE @SQLCommand NVARCHAR(MAX);
	DECLARE @ReturnCode INT;

	SELECT @SQLCommand = 'ALTER TABLE DITStaging.WorkDataEntryWideTable ADD ';

	SELECT @SQLCommand = @SQLCommand + 
		@parmColumnName +
		CASE 
			WHEN RIGHT(@parmColumnName, 2) = '_1' THEN ' VARCHAR(256) SPARSE NULL; '
			WHEN RIGHT(@parmColumnName, 2) = '_7' THEN ' MONEY SPARSE NULL; '
			WHEN RIGHT(@parmColumnName, 2) = '11' THEN ' DATETIME SPARSE NULL; '
			WHEN RIGHT(@parmColumnName, 2) = '_6' THEN ' FLOAT SPARSE NULL; '
			ELSE ' VARCHAR(256) SPARSE NULL; '
		END 
	
	EXEC sp_executesql @SQLCommand;
	
END TRY	
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH