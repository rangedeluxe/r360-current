IF OBJECT_ID('DITStaging.usp_factAdvancedSearch_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factAdvancedSearch_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factAdvancedSearch_Ins
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 03/01/2013
*
* Purpose: Insert rows into factAdvancedSearch by DIT SSIS pkg for import Batch Data.
*
*
* Modification History
* 08/07/2019 R360-16297	MGE	Created
* 09/09/2019 R360-30221 MGE	Change the way the checks and stubs were combined to account for checks only and stubs only batches
* 09/10/2019 R360-30221 MGE Correct the last outer join by adding two more criteria
* 09/16/2019 R360-30221 MGE Handle Rename to CheckAccountNumber and StubAccountNumber
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @SQLStatement			NVARCHAR(MAX) = N''		--Variable to hold t-sql query
	DECLARE @PivotedColumns			NVARCHAR(MAX) = N'';	--Variable to hold unique DE Columns pivoted
	DECLARE @GroupingColumns		NVARCHAR(MAX) = N'';	--Variable to hold unique DE Columns used for grouping
	SELECT @PivotedColumns = @PivotedColumns + ',' +
	CASE 
		WHEN RIGHT(COLUMN_NAME,4) ='_1_1' THEN 'PivotedChecks.' +  COLUMN_NAME
		WHEN RIGHT(COLUMN_NAME,4) ='_1_6' THEN 'PivotedChecks.' +  COLUMN_NAME
		WHEN RIGHT(COLUMN_NAME,4) ='_1_7' THEN 'PivotedChecks.' +  COLUMN_NAME
		WHEN RIGHT(COLUMN_NAME,4) ='1_11' THEN 'PivotedChecks.' +  COLUMN_NAME
		ELSE 'PivotedStubs.' + COLUMN_NAME
	END  
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkDataEntryWideTable' AND TABLE_SCHEMA = 'DITStaging'
	AND COLUMN_NAME NOT IN 
	('BankKey','ClientAccountKey','DepositDateKey','ImmutableDateKey','SourceProcessingDateKey','BatchID','TransactionID','BatchSequence','CheckBatchSequence','StubBatchSequence')
	AND COLUMN_NAME IN 
		(SELECT FieldName   + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2))
		from DITStaging.factDataEntryDetails
		INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys
		ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkgroupDataEntryColumnKey
		GROUP BY FieldName, IsCheck, DataType);

	SELECT @GroupingColumns = @GroupingColumns + ', ' +  COLUMN_NAME 
	FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'WorkDataEntryWideTable'  AND TABLE_SCHEMA = 'DITStaging'
	AND COLUMN_NAME NOT IN 
	('BankKey','ClientAccountKey','DepositDateKey','ImmutableDateKey','SourceProcessingDateKey','BatchID','TransactionID','BatchSequence','CheckBatchSequence','StubBatchSequence')
	AND COLUMN_NAME IN 
		(SELECT FieldName   + '_' + CAST(IsCheck AS NVARCHAR(1)) + '_' +  CAST(DataType AS NVARCHAR(2))
		from DITStaging.factDataEntryDetails
		INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys
		ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkgroupDataEntryColumnKey
		GROUP BY FieldName, IsCheck, DataType);

	SELECT @SQLStatement = N'
	;WITH PivotedChecks AS
	(SELECT BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, CheckBatchSequence,
	StubBatchSequence AS StubBatchSequence' + @GroupingColumns +
	N' FROM DITStaging.WorkDataEntryWideTable
	WHERE CheckBatchSequence IS NOT NULL
	), ' +
	N'
	PivotedStubs AS 
	(SELECT BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, StubBatchSequence'
	+ @GroupingColumns +
	N' FROM DITStaging.WorkDataEntryWideTable
	WHERE StubBatchSequence IS NOT NULL
	), CombinedRows AS
		(SELECT COALESCE(PivotedChecks.BankKey,PivotedStubs.BankKey) AS BankKey,    
		 COALESCE(PivotedChecks.ClientAccountKey,PivotedStubs.ClientAccountKey) AS ClientAccountKey,    
		 COALESCE(PivotedChecks.DepositDateKey,PivotedStubs.DepositDateKey) AS DepositDateKey,    
		 COALESCE(PivotedChecks.ImmutableDateKey,PivotedStubs.ImmutableDateKey) AS ImmutableDateKey,    
		 COALESCE(PivotedChecks.SourceProcessingDateKey, PivotedStubs.SourceProcessingDateKey) AS SourceProcessingDateKey,    
		 COALESCE(PivotedChecks.BatchID,PivotedStubs.BatchID) AS BatchID,    
		 COALESCE(PivotedChecks.TransactionID,PivotedStubs.TransactionID) AS TransactionID,  
		PivotedChecks.CheckBatchSequence,
		PivotedStubs.StubBatchSequence ' + @PivotedColumns +
		N' FROM PivotedChecks
		FULL OUTER JOIN PivotedStubs 
		ON PivotedChecks.BankKey = PivotedStubs.BankKey
		AND  PivotedChecks.ClientAccountKey = PivotedStubs.ClientAccountKey
		AND PivotedChecks.DepositDateKey = PivotedStubs.DepositDateKey
		AND PivotedChecks.ImmutableDateKey = PivotedStubs.ImmutableDateKey
		AND PivotedChecks.SourceProcessingDateKey = PivotedStubs.SourceProcessingDateKey
		AND PivotedChecks.BatchID = PivotedStubs.BatchID
		AND PivotedChecks.TransactionID = PivotedStubs.TransactionID
	), FinalRowsToInsert AS 
		(
		SELECT IsDeleted, WorkGroupedTransactionDetails.BankKey, WorkGroupedTransactionDetails.ClientAccountKey, WorkGroupedTransactionDetails.DepositDateKey, WorkGroupedTransactionDetails.ImmutableDateKey, 
			WorkGroupedTransactionDetails.SourceProcessingDateKey, WorkGroupedTransactionDetails.BatchID, SourceBatchID, 
			BatchNumber, BatchSourceKey, BatchPaymentTypeKey, DepositStatus, CheckCount, StubCount, DocumentCount, OMRCount, ScannedCheckCount,
			WorkGroupedTransactionDetails.TransactionID, TxnSequence,
			COALESCE(CombinedRows.CheckBatchSequence, WorkGroupedTransactionDetails.CheckBatchSequence) AS CheckBatchSequence, CheckSequence, CheckAmount, RoutingNumber, Account, TransactionCode, Serial, RemitterName, DDAKey, NumericRoutingNumber, NumericSerial,
			CombinedRows.StubBatchSequence, StubSequence, StubAmount, AccountNumber, GETDATE() AS CreationDate, GETDATE() AS ModificationDate ' + @GroupingColumns + 
		N' FROM CombinedRows
		FULL OUTER JOIN DITStaging.WorkGroupedTransactionDetails 
		ON CombinedRows.BankKey = WorkGroupedTransactionDetails.BankKey   
		AND  CombinedRows.ClientAccountKey = WorkGroupedTransactionDetails.ClientAccountKey   
		AND CombinedRows.DepositDateKey = WorkGroupedTransactionDetails.DepositDateKey   
		AND CombinedRows.ImmutableDateKey = WorkGroupedTransactionDetails.ImmutableDateKey   
		AND CombinedRows.SourceProcessingDateKey = WorkGroupedTransactionDetails.SourceProcessingDateKey   
		AND CombinedRows.BatchID = WorkGroupedTransactionDetails.BatchID   
		AND CombinedRows.TransactionID = WorkGroupedTransactionDetails.TransactionID
		AND 
		((CombinedRows.CheckBatchSequence IS NULL AND WorkGroupedTransactionDetails.CheckBatchSequence IS NULL)
		OR
		(COALESCE(CombinedRows.CheckBatchSequence, WorkGroupedTransactionDetails.CheckBatchSequence)  = COALESCE(WorkGroupedTransactionDetails.CheckBatchSequence, CombinedRows.CheckBatchSequence)))
		AND 
		((CombinedRows.StubBatchSequence IS NULL AND WorkGroupedTransactionDetails.StubBatchSequence IS NULL)
		OR
		(COALESCE(CombinedRows.StubBatchSequence, WorkGroupedTransactionDetails.StubBatchSequence)  = COALESCE(WorkGroupedTransactionDetails.StubBatchSequence, CombinedRows.StubBatchSequence)))
		)
	INSERT INTO RecHubData.factAdvancedSearch
		(IsDeleted, BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, 
				SourceProcessingDateKey, BatchID, SourceBatchID, 
				BatchNumber, BatchSourceKey, BatchPaymentTypeKey, DepositStatus, CheckCount, StubCount, DocumentCount, OMRCount, ScannedCheckCount,
				TransactionID, TxnSequence, 
				CheckBatchSequence, CheckSequence, CheckAmount, RoutingNumber, CheckAccountNumber, TransactionCode, Serial, RemitterName, DDAKey, NumericRoutingNumber, NumericSerial,
				StubBatchSequence, StubSequence, StubAmount, StubAccountNumber, CreationDate, ModificationDate' + @GroupingColumns + ')' + 
		'SELECT IsDeleted, BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, 
			SourceProcessingDateKey, BatchID, SourceBatchID, 
			BatchNumber, BatchSourceKey, BatchPaymentTypeKey, DepositStatus, CheckCount, StubCount, DocumentCount, OMRCount, ScannedCheckCount,
			TransactionID, TxnSequence,
			CheckBatchSequence, CheckSequence, CheckAmount, RoutingNumber, Account AS CheckAccountNumber, TransactionCode, Serial, RemitterName, DDAKey, NumericRoutingNumber, NumericSerial,
			StubBatchSequence, StubSequence, StubAmount, AccountNumber AS StubAccountNumber, GETDATE() AS CreationDate, GETDATE() AS ModificationDate' + @GroupingColumns + 
		' FROM FinalRowsToInsert'

	EXEC(@SQLStatement)
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH