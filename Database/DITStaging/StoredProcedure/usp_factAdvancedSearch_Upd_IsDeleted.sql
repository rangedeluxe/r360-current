IF OBJECT_ID('DITStaging.usp_factAdvancedSearch_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factAdvancedSearch_Upd_IsDeleted
GO

CREATE PROCEDURE DITStaging.usp_factAdvancedSearch_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 08/07/2019
*
* Purpose: Mark rows as deleted in the RecHubData.factAdvancedSearch table based on
*			DIT SSIS import.
*
* Modification History
* 08/07/2019 R360-16297 MGE	Created
* 09/10/2019 R360-30221 MGE Fix the where clause to examine the correct table.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factAdvancedSearch
	SET		
		RecHubData.factAdvancedSearch.IsDeleted = 1,
		RecHubData.factAdvancedSearch.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factAdvancedSearch 
		INNER JOIN DITStaging.factBatchSummary ON factAdvancedSearch.DepositDateKey = factBatchSummary.DepositDateKey
			AND factAdvancedSearch.ImmutableDateKey = factBatchSummary.ImmutableDateKey
			AND factAdvancedSearch.SourceProcessingDateKey= factBatchSummary.SourceProcessingDateKey
			AND factAdvancedSearch.ClientAccountKey = factBatchSummary.ClientAccountKey
			AND factAdvancedSearch.BatchID = factBatchSummary.BatchID
	WHERE	
		RecHubData.factAdvancedSearch.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH