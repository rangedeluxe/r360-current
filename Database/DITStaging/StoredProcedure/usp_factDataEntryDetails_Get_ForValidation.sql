--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factDataEntryDetails_Get_ForValidation
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factDataEntryDetails_Get_ForValidation') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factDataEntryDetails_Get_ForValidation
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factDataEntryDetails_Get_ForValidation
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2017
*
* Purpose: Get DITStaging.factDataEntryDetails for Post Deposit BRE validation.
*
* Modification History
* 04/07/2017 PT 141788325 JPB	Created
* 05/18/2017 PT 143447629 JPB	Sort by SiteBankID, SiteClientAccountID,BatchSourceKey
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT 
		DITStaging.factDataEntryDetails.BatchID,
		DITStaging.factDataEntryDetails.TransactionID,
		DITStaging.factDataEntryDetails.BatchSequence,
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.SiteBankID,
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.SiteClientAccountID,
		DITStaging.factDataEntryDetails.BatchSourceKey,
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.IsCheck,
		DITStaging.factDataEntryDetails.DataEntryValue,
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.FieldName
	FROM
		DITStaging.factDataEntryDetails
		INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys ON 
			DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkGroupDataEntryColumnKey
	ORDER BY
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.SiteBankID,
		DITStaging.DataImportWorkgroupDataEntryColumnKeys.SiteClientAccountID,
		DITStaging.factDataEntryDetails.BatchSourceKey,
        DITStaging.factDataEntryDetails.BatchID,
        DITStaging.factDataEntryDetails.TransactionID,
        DITStaging.factDataEntryDetails.BatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

