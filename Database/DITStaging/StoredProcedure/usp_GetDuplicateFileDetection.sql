--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_GetDuplicateFileDetection
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_GetDuplicateFileDetection') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_GetDuplicateFileDetection
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_GetDuplicateFileDetection
(
	@parmDuplicatedDetectDays INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/03/2014
*
* Purpose: Populate working tables with records that match date range to check.
*
* Modification History
* 11/03/2014 WI 175920 JPB	Created
* 10/17/2016 PT 127604083 JPB	Added GROUP BY to ensure one row per file sig.
******************************************************************************/
SET NOCOUNT ON;
BEGIN TRY

	INSERT INTO DITStaging.DuplicateFileDetection
	(
		FileHashChecker,		
		FileHash,
		FileSignature
	)
	SELECT
		FileHashChecker,
		FileHash,
		FileSignature
	FROM 
		RecHubSystem.DuplicateFileDetection
	WHERE
		CAST(CONVERT(VARCHAR,ImportDate,112) AS INT) >= CAST(CONVERT(VARCHAR,DATEADD(DAY,-@parmDuplicatedDetectDays,GETDATE()),112) AS INT)
	GROUP BY
		FileHashChecker,
		FileHash,
		FileSignature;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
