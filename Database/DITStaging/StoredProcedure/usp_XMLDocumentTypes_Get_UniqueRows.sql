--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLDocumentTypes_Get_UniqueRows
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLDocumentTypes_Get_UniqueRows') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLDocumentTypes_Get_UniqueRows
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLDocumentTypes_Get_UniqueRows
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/20/2012
*
* Purpose: Retrieve only the most current records from the XMLDocumentTypes table.
*			I.E. remove duplicates from the record set.
*
* Modification History
* 05/20/2012 CR 52907 JPB	Created
* 04/10/2013 WI 92131 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_XMLDocumentTypes_GetNonDuplicateRecords.
* 04/14/2017 PT 142782913 JPB	The actual number of rows returned was a cartesian 
*					product. Partition the records and return just one row for each
*					FileDescriptor. There is not a good order column, so take the
*					longest DocumentTypeDescription if there are duplicates.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH DocumentTypesList AS
	(
		SELECT
			ROW_NUMBER() OVER (PARTITION BY FileDescriptor ORDER BY LEN(DocumentTypeDescription) DESC) AS RowNumber,
			ClientGroup_Id,
			ClientTrackingID,
			FileDescriptor,
			DocumentTypeDescription
		FROM
			DITStaging.XMLDocumentTypes
	)
	SELECT 
		* 
	FROM 
		DocumentTypesList 
	WHERE 
		RowNumber = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
