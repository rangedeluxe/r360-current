--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_PendingClientSetups
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Get_PendingClientSetups') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Get_PendingClientSetups
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Get_PendingClientSetups
(
	@parmPendingClientSetups BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Retrieve client setups from OLTA.DataImportQueue that are ready for processing.
*
* Modification History
* 02/26/2012 CR 50817 JPB	Created
* 05/11/2012 CR 52708 JPB	Performance improvements.
* 04/09/2013 WI 92085 JBS	Update to 2.0 release. Change schema name
*							Rename proc from usp_DataImportQueue_GetPendingClientSetups
* 
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	INSERT INTO DITStaging.XMLClientGroups(ClientGroup_Id,xmlClientGroup,xmlString)
	SELECT	RecHubSystem.DataImportQueue.DataImportQueueID AS ClientGroup_Id,
			C.Client.query('.') AS xmlBatch,
			CAST(C.Client.query('.') AS VARCHAR(MAX)) AS xmlString
	FROM	RecHubSystem.DataImportQueue
			CROSS APPLY XMLDataDocument.nodes('/ClientGroup') AS C(Client)
	WHERE	QueueType = 0
			AND QueueStatus IN (10,20);

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingClientSetups = 1;
		UPDATE	RecHubSystem.DataImportQueue
		SET		QueueStatus = 20,
				ResponseStatus = 1,
				ModificationDate = GETDATE(),
				ModifiedBy = SUSER_SNAME()
		FROM	RecHubSystem.DataImportQueue
				INNER JOIN DITStaging.XMLClientGroups ON DITStaging.XMLClientGroups.ClientGroup_Id = RecHubSystem.DataImportQueue.DataImportQueueID;
	END
	ELSE SET @parmPendingClientSetups = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
