--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factRawPaymentData_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factRawPaymentData_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factRawPaymentData_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factRawPaymentData_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/07/2014
*
* Purpose: Delete RecHubData fact rows based from DIT Staging fact rows.
*
* Modification History
* 05/07/2014 WI 140365 JPB	Created
* 08/20/2014 WI 160046 JBS	Changed to use SourceBatchID instead of BatchID 
*							to match up rows to mark deleted.  BatchID always a newly generated number
* 12/02/2015 WI 249669 MGE	Changed to not use DepositDateKey and SourceProcessingDateKey in join (for case where DepositDate changes)
* 02/23/2016 WI 265491 JPB	Removed OrganizationKey from INNER JOIN.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factRawPaymentData
	SET		
		RecHubData.factRawPaymentData.IsDeleted = 1,
		RecHubData.factRawPaymentData.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factRawPaymentData
		INNER JOIN DITStaging.DataImportWorkBatch ON 
				RecHubData.factRawPaymentData.ImmutableDateKey = DITStaging.DataImportWorkBatch.ImmutableDateKey
			AND RecHubData.factRawPaymentData.BankKey = DITStaging.DataImportWorkBatch.BankKey
			AND RecHubData.factRawPaymentData.ClientAccountKey = DITStaging.DataImportWorkBatch.ClientAccountKey
			AND RecHubData.factRawPaymentData.SourceBatchID = DITStaging.DataImportWorkBatch.SourceBatchID
			AND RecHubData.factRawPaymentData.BatchSourceKey = DITStaging.DataImportWorkBatch.BatchSourceKey
	WHERE 
		DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
		AND RecHubData.factRawPaymentData.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
