--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factBatchData_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factBatchData_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factBatchData_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factBatchData_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete RecHubData fact rows based from DIT Staging fact rows.
*
* Modification History
* 02/28/2012 CR 50824 JPB	Created
* 04/09/2013 WI 92102 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_OLTAfactBatchData_DeleteByWorkTableJoin.
*							Added @parmModificationDate.
* 08/20/2014 WI 160031 JBS	Changed to use SourceBatchID instead of BatchID 
*							to match up rows to mark deleted.  BatchID always a newly generated number
* 12/02/2015 WI 249663 MGE	Changed to not inlcude depositdatekey in the join and remove SourceProcessingDateKey from join
* 02/23/2016 WI 265491 JPB	Removed OrganizationKey from INNER JOINs.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factBatchData
	SET		
		RecHubData.factBatchData.IsDeleted = 1,
		RecHubData.factBatchData.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factBatchData
		INNER JOIN DITStaging.DataImportWorkBatch ON 
				RecHubData.factBatchData.ImmutableDateKey = DITStaging.DataImportWorkBatch.ImmutableDateKey
			AND RecHubData.factBatchData.BankKey = DITStaging.DataImportWorkBatch.BankKey
			AND RecHubData.factBatchData.ClientAccountKey = DITStaging.DataImportWorkBatch.ClientAccountKey
			AND RecHubData.factBatchData.SourceBatchID = DITStaging.DataImportWorkBatch.SourceBatchID
        INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.DepositDateKey = RecHubData.factBatchData.DepositDateKey
			AND RecHubData.factBatchSummary.ImmutableDateKey = RecHubData.factBatchData.ImmutableDateKey
			AND RecHubData.factBatchSummary.SourceProcessingDateKey= RecHubData.factBatchData.SourceProcessingDateKey
			AND RecHubData.factBatchSummary.BankKey = RecHubData.factBatchData.BankKey
			AND RecHubData.factBatchSummary.ClientAccountKey = RecHubData.factBatchData.ClientAccountKey
			AND RecHubData.factBatchSummary.BatchID = RecHubData.factBatchData.BatchID
			AND RecHubData.factBatchSummary.SourceBatchID = RecHubData.factBatchData.SourceBatchID
	WHERE 
		RecHubData.factBatchSummary.BatchSourceKey = DITStaging.DataImportWorkBatch.BatchSourceKey
		AND DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
		AND RecHubData.factBatchData.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH