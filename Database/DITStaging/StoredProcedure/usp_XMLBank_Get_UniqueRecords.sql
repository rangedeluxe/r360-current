--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBank_Get_UniqueRecords
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBank_Get_UniqueRecords') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBank_Get_UniqueRecords 
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBank_Get_UniqueRecords
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/20/2012
*
* Purpose: Retrieve only the most current records from the XMLBank table.
*			I.E. remove duplicates from the record set.
*
* Modification History
* 05/20/2012 CR 52905 JPB	Created
* 04/19/2013 WI 92116 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc FROM usp_XMLBank_GetNonDuplicateRecords
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH BankList AS
	(
		SELECT	MAX(DITStaging.XMLBank.ClientGroup_Id) AS ClientGroup_Id
		FROM	DITStaging.XMLBank
		GROUP BY DITStaging.XMLBank.BankID
	)
	SELECT DITStaging.XMLBank.* FROM DITStaging.XMLBank
		INNER JOIN BankList ON BankList.ClientGroup_Id = DITStaging.XMLBank.ClientGroup_Id;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
