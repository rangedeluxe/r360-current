--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_DataEntryDetails_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataEntryDetails_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataEntryDetails_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataEntryDetails_Get
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/01/2018
*
* Purpose: Get the data entry rows needed for processing
*
* Modification History
* 08/01/2018 156815868 JPB	Created
* 08/09/2018 156815868 MGE  Increased length of varchar in convert statement to allow for time
* 08/14/2018 156815868 MGE	Increased length of varchar in convert statement to allow for AM/PM
*****************************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	SELECT
		Batch_Id,
		Transaction_Id,
		BatchTrackingID,
		IsDeleted,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		BatchID,
		SourceBatchID,
		BatchNumber,
		BatchSourceKey,
		BatchPaymentTypeKey,
		DepositStatus,
		WorkGroupDataEntryColumnKey,
		TransactionID,
		BatchSequence,
		CreationDate,
		ModificationDate,
		FieldValue,
		MarkSense,
		FieldName,
		CASE
			WHEN DataType = 1 THEN CAST(1 AS BIT)
			WHEN DataType = 6 AND ISNUMERIC(FieldValue) = 1 THEN CAST(1 AS BIT)
			WHEN DataType = 7 THEN CASE WHEN ISNUMERIC(REPLACE(FieldValue,'$','')) = 1 AND IsCorrespondence = 0 THEN CAST(1 AS BIT) 
										WHEN IsCorrespondence = 1 THEN CAST(1 AS BIT)
										ELSE CAST(0 AS BIT)
									END
			WHEN DataType = 11 AND ISDATE(FieldValue) = 1 THEN CAST(1 AS BIT)
			ELSE CAST(0 AS BIT)
		END AS IsValidDataType,
		IsCheck,
		IsCorrespondence,
		DataType,
		CASE WHEN DataType = 11 AND ISDATE(FieldValue) = 1 THEN CONVERT(DATE,CONVERT(VARCHAR(25),FieldValue,101)) END AS ConvertedDate
	FROM 
		DITStaging.DataEntryDetails;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH