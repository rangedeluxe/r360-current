--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLClientGroup_Get_UniqueRows
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLClientGroup_Get_UniqueRows') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLClientGroup_Get_UniqueRows
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLClientGroup_Get_UniqueRows
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/20/2012
*
* Purpose: Retrieve only the most current records from the XMLClientGroup table.
*			I.E. remove duplicates from the record set.
*
* Modification History
* 05/20/2012 CR 52908 JPB	Created
* 04/10/2013 WI 92122 JPB	Updated to 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_XMLClientGroup_GetNonDuplicateRecords.
* 02/12/2016 WI 263684 JPB	Add BankID to Group BY
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH ClientGroupList AS
	(
		SELECT	
			MAX(ClientGroup_Id) AS ClientGroup_Id
		FROM	
			DITStaging.XMLClientGroup
		GROUP BY 
			ClientGroupID,
			BankID
	)
	SELECT 
		DITStaging.XMLClientGroup.* 
	FROM 
		DITStaging.XMLClientGroup
		INNER JOIN ClientGroupList ON ClientGroupList.ClientGroup_Id = DITStaging.XMLClientGroup.ClientGroup_Id;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
