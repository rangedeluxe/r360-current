IF OBJECT_ID('DITStaging.usp_factStubsPivot') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factStubsPivot
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factStubsPivot
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 DELUXE Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 DELUXE Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/12/2019
*
* Purpose: Pivot StubsDE 
*
* Modification History
* 11/12/2019 R360-31442	MGE	Created
*******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	DECLARE @SQLStatement			NVARCHAR(MAX) = N''		--Variable to hold t-sql query
	DECLARE @UniqueDEColumns		NVARCHAR(MAX) = N''		--Variable to hold unique DE Columns to be used in PIVOT clause
	DECLARE @PivotDEColumnsToSelect	NVARCHAR(MAX) = N''		--Variable to hold Pivot column name with alias to be used in select clause

	SELECT @UniqueDEColumns = @UniqueDEColumns + ', ' + FieldName   + '_0_' +  CAST(DataType AS NVARCHAR(2))
	from DITStaging.factDataEntryDetails
	INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys
		ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE IsCheck = 0
	AND FieldName NOT IN ('AccountNumber', 'Amount')
	GROUP BY FieldName, IsCheck, DataType;
	SELECT @UniqueDEColumns = LTRIM(STUFF(@UniqueDEColumns, 1 ,1, ''));

	SELECT @PivotDEColumnsToSelect = @PivotDEColumnsToSelect + ', ' +  FieldName  + '_0_' +  CAST(DataType AS NVARCHAR(2))
	from DITStaging.factDataEntryDetails
	INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys
		ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkgroupDataEntryColumnKey
	WHERE IsCheck = 0
	AND FieldName NOT IN ('AccountNumber', 'Amount')
	GROUP BY FieldName, IsCheck, DataType;

	SET @SQLStatement = 
	N'
	;WITH DetailRows AS (
	select BankKey, factDataEntryDetails.ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, BatchSequence, 
	factDataEntryDetails.WorkgroupDataEntryColumnKey, IsCheck, 
	FieldName  + ''_0_''  + CAST(DataType AS NVARCHAR(2)) AS FieldName, DataEntryValue 
	FROM DITStaging.factDataEntryDetails
	INNER JOIN DITStaging.DataImportWorkgroupDataEntryColumnKeys
		ON DITStaging.DataImportWorkgroupDataEntryColumnKeys.WorkgroupDataEntryColumnKey = DITStaging.factDataEntryDetails.WorkgroupDataEntryColumnKey  
	WHERE IsCheck = 0
	), PivotedRows AS
	(SELECT BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, BatchSequence' + @PivotDEColumnsToSelect +
	'
	FROM 
	(SELECT  BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, BatchSequence, FieldName, DataEntryValue FROM DetailRows) p
	PIVOT
	(
	Min(DataEntryValue) FOR FieldName IN (' + @UniqueDEColumns + ')
	) AS PVT)
	INSERT INTO DITStaging.WorkDataEntryWideStubs 
	(BankKey, ClientAccountKey, DepositDateKey, ImmutableDateKey, SourceProcessingDateKey, BatchID, TransactionID, BatchSequence' + @PivotDEColumnsToSelect + ')
	SELECT * FROM PivotedRows'

	EXEC (@SQLStatement);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH