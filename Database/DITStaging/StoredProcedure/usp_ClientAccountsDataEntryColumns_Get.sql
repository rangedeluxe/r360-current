IF OBJECT_ID('DITStaging.usp_ClientAccountsDataEntryColumns_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_ClientAccountsDataEntryColumns_Get
GO

CREATE PROCEDURE DITStaging.usp_ClientAccountsDataEntryColumns_Get
(
	@parmIsCheck BIT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/28/2015
*
* Purpose: Get list of DE columns.
*
* Modification History
* 07/28/2015 WI 224472 JPB	Created
*							Merged usp_ClientAccountsDataEntryColumns_Get_DocumentDEColumns
*							and usp_ClientAccountsDataEntryColumns_Get_PaymentDEColumns
* 05/01/2019 R360-16298 MGE	Added MixedCaseFieldName to support adding columns to factTransactionDetails table.
****************************************************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	;WITH ClientAccountKeyList AS
	(
		SELECT DISTINCT SiteBankID,SiteClientAccountID 
		FROM DITStaging.DataImportWorkBatch	
	),
	DataEntryColumnList AS
	(
		SELECT	ROW_NUMBER() OVER (ORDER BY ClientAccountKeyList.SiteBankID,ClientAccountKeyList.SiteClientAccountID,RecHubData.dimWorkgroupDataEntryColumns.CreationDate) As RecID,
				ClientAccountKeyList.SiteBankID,
				ClientAccountKeyList.SiteClientAccountID,
				RecHubData.dimWorkgroupDataEntryColumns.BatchSourceKey,
				RecHubData.dimWorkgroupDataEntryColumns.WorkgroupDataEntryColumnKey,
				UPPER(RecHubData.dimWorkgroupDataEntryColumns.FieldName) AS FieldName,
				RecHubData.dimWorkgroupDataEntryColumns.FieldName AS MixedCaseFieldName,
				@parmIsCheck AS IsCheck,
				RecHubData.dimWorkgroupDataEntryColumns.DataType,
				RecHubData.dimWorkgroupDataEntryColumns.CreationDate,
				RecHubData.dimWorkgroupDataEntryColumns.MarkSense
		FROM	ClientAccountKeyList
				INNER JOIN RecHubData.dimWorkgroupDataEntryColumns 
					ON RecHubData.dimWorkgroupDataEntryColumns.SiteBankID = ClientAccountKeyList.SiteBankID
					AND RecHubData.dimWorkgroupDataEntryColumns.SiteClientAccountID = ClientAccountKeyList.SiteClientAccountID
		WHERE	IsCheck = @parmIsCheck
	)
	SELECT	
		WorkgroupDataEntryColumnKey,
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey,
		IsCheck,
		MarkSense,
		DataType,
		FieldName,
		MixedCaseFieldName
	FROM	
		DataEntryColumnList
	WHERE	
		RecID IN
		(
				SELECT 
					MAX(RecID)
				FROM 
					DataEntryColumnList
				GROUP BY 
					SiteBankID,
					SiteClientAccountID,
					BatchSourceKey,
					IsCheck,
					FieldName
		)
	ORDER BY
		SiteBankID,
		SiteClientAccountID,
		BatchSourceKey,
		FieldName;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
