--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_LoadFactBatchSummary_Get_TransactionCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_LoadFactBatchSummary_Get_TransactionCounts') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_LoadFactBatchSummary_Get_TransactionCounts
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_LoadFactBatchSummary_Get_TransactionCounts
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: CMC
* Date: 02/06/2019
*
* Purpose: Reads the factTransactionSummary staging table 
*          to get the transaction counts ordered by BatchID
*
* Modification History
* 02/06/2019 R360-15357 CMC	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
        BatchID,
        COUNT(*) AS TransactionCount
    FROM
	    DITStaging.factTransactionSummary
    GROUP BY
        BatchID
    ORDER BY 
        BatchID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH