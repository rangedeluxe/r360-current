IF OBJECT_ID('DITStaging.usp_TruncateBatchDataWorkTables') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_TruncateBatchDataWorkTables
GO

CREATE PROCEDURE DITStaging.usp_TruncateBatchDataWorkTables
AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2019 Deluxe Corp. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Truncate tables used by SSIS for import Batch Data.
*
* Modification History
* 02/26/2012 CR 50833 JPB	Created
* 04/20/2012 CR 53027 JPB	Added additional work tables.
* 04/08/2013 WI 92113 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
* 05/06/2014 WI 140301 JPB	Added new raw payment data working tables.
* 10/04/2014 WI 175922 JPB	Added duplicate detect working tables.
* 02/02/2015 WI 187610 JPB	Added DataEntryDetails staging table.
* 02/11/2015 WI 190143 JPB	Added DataImportWorkBatchKeys and XMLElectronicBatch.
* 08/19/2015 WI	231112 JPB	Changed DataEntryColumns to WorkgroupDataEntryColumns.
* 07/08/2016 WI 291257 JPB  Added XMLGhostDocument table.
* 02/06/2017 PT 137726225	JPB Removed deprecated objects.
* 04/07/2017 PT 141788325	JPB	Added DITStaging.ExceptionTransactions
* 08/07/2019 R360-16297 MGE Added work tables for factAdvancedSearch functionality
* 11/11/2019 R360-31442 MGE Chnaged work tables for Advanced Search functionality
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	TRUNCATE TABLE DITStaging.ExceptionTransactions;
	TRUNCATE TABLE DITStaging.DataEntryDetails;
	TRUNCATE TABLE DITStaging.DataImportWorkBatch;
	TRUNCATE TABLE DITStaging.DataImportWorkBatchKeys;
	TRUNCATE TABLE DITStaging.DataImportWorkBatchStartup;
	TRUNCATE TABLE DITStaging.DataImportWorkBatchResponses;
	TRUNCATE TABLE DITStaging.DataImportWorkgroupDataEntryColumnKeys;
	TRUNCATE TABLE DITStaging.DataImportWorkDuplicateTransactions;
	TRUNCATE TABLE DITStaging.DataImportWorkMarkSense;
	TRUNCATE TABLE DITStaging.DuplicateFileDetection;
	TRUNCATE TABLE DITStaging.DuplicateTransactionDetection;
	TRUNCATE TABLE DITStaging.factBatchSummary;
	TRUNCATE TABLE DITStaging.factTransactionSummary;
	TRUNCATE TABLE DITStaging.factChecks;
	TRUNCATE TABLE DITStaging.factStubs;
	TRUNCATE TABLE DITStaging.factDocuments;
	TRUNCATE TABLE DITStaging.factDataEntrySummary;
	TRUNCATE TABLE DITStaging.factDataEntryDetails;
	TRUNCATE TABLE DITStaging.factBatchData;
	TRUNCATE TABLE DITStaging.factItemData;
	TRUNCATE TABLE DITStaging.factRawPaymentData;
	TRUNCATE TABLE DITStaging.WorkDataEntryWideChecks;
	TRUNCATE TABLE DITStaging.WorkDataEntryWideStubs;
	TRUNCATE TABLE DITStaging.XMLBatches;
	TRUNCATE TABLE DITStaging.XMLBatch;
	TRUNCATE TABLE DITStaging.XMLBatchData;
	TRUNCATE TABLE DITStaging.XMLElectronicBatch;
	TRUNCATE TABLE DITStaging.XMLTransaction;
	TRUNCATE TABLE DITStaging.XMLPayment;
	TRUNCATE TABLE DITStaging.XMLPaymentData;
	TRUNCATE TABLE DITStaging.XMLPaymentItemData;
	TRUNCATE TABLE DITStaging.XMLDocument;
	TRUNCATE TABLE DITStaging.XMLDocumentData;
	TRUNCATE TABLE DITStaging.XMLDocumentItemData;
	TRUNCATE TABLE DITStaging.XMLGhostDocument;
	TRUNCATE TABLE DITStaging.XMLGhostDocumentData;
	TRUNCATE TABLE DITStaging.XMLGhostDocumentItemData;
	TRUNCATE TABLE DITStaging.XMLRawPaymentData;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
