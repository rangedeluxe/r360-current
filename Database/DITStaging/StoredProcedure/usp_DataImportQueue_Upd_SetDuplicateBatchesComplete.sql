--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Upd_SetDuplicateBatchesComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Upd_SetDuplicateBatchesComplete') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Upd_SetDuplicateBatchesComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Upd_SetDuplicateBatchesComplete
(
	@parmDuplicateBatchList	DITStaging.DuplicateBatchList READONLY
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 12/03/2015
*
* Purpose: Create a duplicate batch error and update DataImportQueue table.
*
* Modification History
* 12/03/2015 WI 250257 JPB	Created.
* 06/23/2016 JPB 288584 Changed duplicate response to warning from error
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @Batch_Id		BIGINT,
		@Loop			INT,
		@XMLResponse	XML;

BEGIN TRY
	SET @Loop = 1;
	
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @parmDuplicateBatchList) )
	BEGIN

		SELECT	
			@Batch_Id = Batch_ID
		FROM	
			@parmDuplicateBatchList 
		WHERE	
			RowID = @Loop;

		SELECT @XMLResponse = 
		(
			SELECT	
				BatchTrackingID AS '@BatchTrackingID',
				SiteBankID AS '@SiteBankID',
				SiteClientAccountID AS '@SiteClientAccountID',
				SourceBatchID AS '@BatchID',
				BatchSiteCode AS '@BatchSiteCode',
				CONVERT(CHAR(10),BatchDate,126) AS '@BatchDate',
				CONVERT(CHAR(10),DepositDate,126) AS '@DepositDate',
				CONVERT(CHAR(10),ImmutableDate,126) AS '@SourceProcessingDate',
				BatchPaymentType AS '@BatchPaymentType',
				ImmutableDate AS '@ImmutableDate',
				BatchSource AS '@BatchSource',
				(SELECT 'Warning' AS 'Results' FOR XML PATH(''), TYPE),
				(SELECT 'Duplicate Record In Process' AS 'ErrorMessage' FOR XML PATH(''), TYPE)
			FROM	
				@parmDuplicateBatchList
			WHERE	
				Batch_Id = @Batch_Id
			FOR XML PATH('Batch'), TYPE
		);

		UPDATE	
			RecHubSystem.DataImportQueue
		SET		
			RecHubSystem.DataImportQueue.XMLResponseDocument = @XMLResponse,
			RecHubSystem.DataImportQueue.QueueStatus = 99,
			RecHubSystem.DataImportQueue.ResponseStatus = 2,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBy = SUSER_NAME()
		FROM 
			RecHubSystem.DataImportQueue
		WHERE	
			RecHubSystem.DataImportQueue.DataImportQueueID = @Batch_Id;

		SET @Loop = @Loop + 1;	

	END
	
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH