--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factStubs_Get_ForValidation
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factStubs_Get_ForValidation') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factStubs_Get_ForValidation
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factStubs_Get_ForValidation

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/06/2014
*
* Purpose: Reads the stub details currently in the DITStagging tables
*			returning them so that business rules can be applied before moving
*			to RecHubData
*
* Modification History
* 06/06/2014 WI 146278 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	DITStaging.factStubs.BatchID,
			DITStaging.factStubs.TransactionID,
			DITStaging.factStubs.BatchSequence,
			DITStaging.factStubs.Amount,
			DITStaging.factStubs.AccountNumber
	FROM	DITStaging.factTransactionSummary
		INNER JOIN DITStaging.factStubs
			ON DITStaging.factTransactionSummary.BatchID = DITStaging.factStubs.BatchID
				AND DITStaging.factTransactionSummary.TransactionID = DITStaging.factStubs.TransactionID
	WHERE	DITStaging.factTransactionSummary.Exception IS NULL
	--Order matters as a merge is performed with the transactions, these are split into items
	-- and another merge is done with data entry columns in the business rules engine
	ORDER BY	DITStaging.factTransactionSummary.BatchID,
				DITStaging.factTransactionSummary.TransactionID,
				DITStaging.factStubs.BatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

