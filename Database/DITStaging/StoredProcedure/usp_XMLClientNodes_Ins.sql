--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLClientNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLClientNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLClientNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLClientNodes_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Extract the Client Node from the XML data and insert into the 
*			SSISStaging.XMLClientNodes table.
*
* Modification History
* 05/01/2012 CR 52748 JPB	Created
* 04/05/2013 WI 92124 JBS	Update to 2.0 release. Change schema name to DITStaging
*							Rename proc from usp_XMLClientNodes_Insert
* 03/31/2017 PT #141520883 JPB	REPLACE &amp; with &
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

DECLARE @xmlString VARCHAR(MAX),
		@StartTag VARCHAR(30),
		@EndTag CHAR(1),
		@ClientGroup_Id INT,
		@Loop INT,
		@RecordCount INT;

BEGIN TRY
	SELECT @RecordCount = COUNT(*) FROM DITStaging.XMLClientGroups
	SELECT @StartTag = '<Client ',@EndTag = '>';
	SET @Loop = 1;
	
	WHILE( @Loop <= @RecordCount )
	BEGIN
		SELECT	@ClientGroup_Id = ClientGroup_Id,
				@xmlString = xmlString
		FROM	DITStaging.XMLClientGroups
		WHERE	RowID = @Loop;

		IF( CHARINDEX(@StartTag,@xmlString,0) > 0 )
		BEGIN
			;WITH Client_CTE(ClientGroup_Id,StartingPosition, EndingPosition, occurence)
			AS
			(		
				SELECT		@ClientGroup_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,0) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag,@xmlString,CHARINDEX(@StartTag,@xmlString,0)+1) AS INT),
							1 AS occurence
				UNION ALL
				SELECT 		@ClientGroup_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,EndingPosition + 1) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag, @xmlString, CHARINDEX(@StartTag,@xmlString,EndingPosition + 1))AS INT),
							occurence + 1
				FROM Client_CTE
				WHERE CHARINDEX(@StartTag, @xmlString, EndingPosition + 1) <> 0
			)
			INSERT DITStaging.XMLClientNodes(ClientGroup_Id,xmlString)
			SELECT	@ClientGroup_Id,REPLACE(SUBSTRING(@xmlString, StartingPosition, EndingPosition-StartingPosition+1),'&amp;','&') AS xmlString 
			FROM	Client_CTE
			OPTION (MaxRecursion 0)
		END

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
