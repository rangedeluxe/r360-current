--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_PopulateExceptions
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_PopulateExceptions') IS NOT NULL
    DROP PROCEDURE DITStaging.usp_PopulateExceptions
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_PopulateExceptions 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: CMC
* Date: 08/28/2014
*
* Purpose: This procedure loads the RecHubException schema from DITStaging.
*
* Modification History
* 08/28/2014 WI 162184 CMC	Created
* 12/19/2014 WI 182565 CMC	Exclude 'AccountNumber' and 'Amount' 
*							special fields from StubsDataEntry.
******************************************************************************/
SET NOCOUNT ON;

DECLARE @RunDate DATETIME;

BEGIN TRY

	SELECT @RunDate = GETDATE();

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatches')) 
       DROP TABLE #tmpBatches

	CREATE TABLE #tmpBatches
	(
		   BatchTrackingID UNIQUEIDENTIFIER, 
		   BatchID BIGINT
	);

	CREATE INDEX IDX_tmpBatches ON #tmpBatches(BatchID);

	INSERT INTO #tmpBatches SELECT DISTINCT BatchTrackingID, BatchID FROM DITStaging.factTransactionSummary WHERE Exception = 1;

	INSERT INTO 
		RecHubException.ExceptionBatches
		(
			DataEntrySetupKey,
			SiteBankID,
			SiteWorkgroupID,
			BatchID,
			SourceBatchID,
			DepositDateKey,
			ImmutableDateKey,
			SourceProcessingDateKey,
			BatchSourceKey,
			BatchPaymentTypeKey,
			BatchStatusKey,
			CreationDate,             
			ModificationDate  
			--,ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.DataEntrySetups.DataEntrySetupKey,
		RecHubData.dimClientAccounts.SiteBankID,
		RecHubData.dimClientAccounts.SiteClientAccountID,
		DITStaging.factBatchSummary.BatchID,
		DITStaging.factBatchSummary.SourceBatchID,
		DITStaging.factBatchSummary.DepositDateKey,
		DITStaging.factBatchSummary.ImmutableDateKey,
		DITStaging.factBatchSummary.SourceProcessingDateKey,
		DITStaging.factBatchSummary.BatchSourceKey,
		DITStaging.factBatchSummary.BatchPaymentTypeKey,
		1,/*Batch Status of Pending*/
		@RunDate,
		@RunDate
	FROM DITStaging.factBatchSummary
	INNER JOIN RecHubData.dimClientAccounts
		ON DITStaging.factBatchSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
	INNER JOIN RecHubException.DataEntrySetups
		ON RecHubData.dimClientAccounts.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
		AND	RecHubData.dimClientAccounts.SiteClientAccountID = RecHubException.DataEntrySetups.SiteWorkgroupID
	INNER JOIN #tmpBatches IMPORT
		ON DITStaging.factBatchSummary.BatchTrackingID = IMPORT.BatchTrackingID;


	INSERT INTO 
		RecHubException.Transactions
		(
			ExceptionBatchKey,
			TransactionID,
			TxnSequence,
			CreationDate,            
			ModificationDate  
			--,ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.ExceptionBatches.ExceptionBatchKey,
		DITStaging.factTransactionSummary.TransactionID,
		DITStaging.factTransactionSummary.TxnSequence,
		@RunDate,
		@RunDate
	FROM DITStaging.factTransactionSummary
	INNER JOIN #tmpBatches IMPORT 
		ON DITStaging.factTransactionSummary.BatchTrackingID = IMPORT.BatchTrackingID
	INNER JOIN RecHubException.ExceptionBatches 
		ON IMPORT.BatchID = RecHubException.ExceptionBatches.BatchID
	WHERE 
		DITStaging.factTransactionSummary.Exception = 1;

	
	INSERT INTO 
		RecHubException.DecisioningTransactions
		(
			TransactionKey, 
			IntegraPayExceptionKey, 
			UserID, 
			TransactionStatusKey, 
			TransactionBeginWork, 
			TransactionReset, 
			TransactionEndWork, 
			CreationDate, 
			ModificationDate, 
			ModifiedBy
		)
	SELECT
		RecHubException.Transactions.TransactionKey, 
		NULL, 
		NULL, 
		1,	
		@RunDate,
		0, 
		NULL,	
		@RunDate,
		@RunDate,
		'Admin' 
	FROM RecHubException.Transactions 
    LEFT JOIN RecHubException.DecisioningTransactions 
		ON RecHubException.Transactions.TransactionKey = RecHubException.DecisioningTransactions.TransactionKey 
	WHERE
		RecHubException.DecisioningTransactions.TransactionKey IS NULL;


	INSERT INTO 
		RecHubException.Payments
		(
			TransactionKey,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			PaymentSequence,
			Amount,
			RT,
			Account,
			TransactionCode,
			Serial,
			RemitterName,
			CreationDate,           
			ModificationDate  
			--ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.Transactions.TransactionKey,
		DITStaging.factChecks.TxnSequence,
		DITStaging.factChecks.SequenceWithinTransaction,
		DITStaging.factChecks.BatchSequence,
		DITStaging.factChecks.CheckSequence,
		DITStaging.factChecks.Amount,
		DITStaging.factChecks.RoutingNumber,
		DITStaging.factChecks.Account,
		DITStaging.factChecks.TransactionCode,
		DITStaging.factChecks.Serial,
		DITStaging.factChecks.RemitterName,
		@RunDate,
		@RunDate
	FROM #tmpBatches IMPORT
	INNER JOIN DITStaging.factChecks
		ON IMPORT.BatchID = DITStaging.factChecks.BatchID
	INNER JOIN RecHubException.ExceptionBatches
		ON IMPORT.BatchID = RecHubException.ExceptionBatches.BatchID
	INNER JOIN RecHubException.Transactions
		ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		AND	DITStaging.factChecks.TransactionID = RecHubException.Transactions.TransactionID;


	INSERT INTO 
		RecHubException.Stubs
		(
			TransactionKey,
			TxnSequence,
			SequenceWithinTransaction,
			BatchSequence,
			StubSequence,
			Amount,
			AccountNumber,
			CreationDate,             
			ModificationDate  
			--ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.Transactions.TransactionKey,
		DITStaging.factStubs.TxnSequence,
		DITStaging.factStubs.SequenceWithinTransaction,
		DITStaging.factStubs.BatchSequence,
		DITStaging.factStubs.StubSequence,
		DITStaging.factStubs.Amount,
		DITStaging.factStubs.AccountNumber,
		@RunDate,
		@RunDate
	FROM #tmpBatches IMPORT
	INNER JOIN DITStaging.factStubs
		ON IMPORT.BatchID = DITStaging.factStubs.BatchID
	INNER JOIN RecHubException.ExceptionBatches
		ON IMPORT.BatchID = RecHubException.ExceptionBatches.BatchID
	INNER JOIN RecHubException.Transactions
		ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		AND DITStaging.factStubs.TransactionID = RecHubException.Transactions.TransactionID;


	INSERT INTO 
		RecHubException.PaymentDataEntry
		(
			PaymentKey,
			FldName,
			Value,
			CreationDate,             
			ModificationDate  
			--ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.Payments.PaymentKey,
		RecHubData.dimDataEntryColumns.FldName,
		COALESCE
		(
			CAST(DITStaging.factDataEntryDetails.DataEntryValueDateTime AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValueFloat AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValueMoney AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValue AS SQL_VARIANT)
		),
		@RunDate,
		@RunDate
	FROM #tmpBatches IMPORT
	INNER JOIN DITStaging.factChecks
		ON IMPORT.BatchID = DITStaging.factChecks.BatchID
	INNER JOIN DITStaging.factDataEntryDetails
		ON DITStaging.factChecks.BatchID = DITStaging.factDataEntryDetails.BatchID
		AND DITStaging.factChecks.TransactionID = DITStaging.factDataEntryDetails.TransactionID
		AND DITStaging.factChecks.BatchSequence = DITStaging.factDataEntryDetails.BatchSequence
	INNER JOIN RecHubException.ExceptionBatches
		ON IMPORT.BatchID = RecHubException.ExceptionBatches.BatchID
	INNER JOIN RecHubException.Transactions
		ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		AND DITStaging.factChecks.TransactionID = RecHubException.Transactions.TransactionID
	INNER JOIN RecHubException.Payments
		ON RecHubException.Transactions.TransactionKey = RecHubException.Payments.TransactionKey
		AND DITStaging.factChecks.BatchSequence = RecHubException.Payments.BatchSequence
	INNER JOIN RecHubData.dimDataEntryColumns
		ON DITStaging.factDataEntryDetails.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey;


	INSERT INTO 
		RecHubException.StubDataEntry
		(
			StubKey,
			FldName,
			Value,
			CreationDate,             
			ModificationDate  
			--ModifiedBy               --TODO
		)
	SELECT 
		RecHubException.Stubs.StubKey,
		RecHubData.dimDataEntryColumns.FldName,
		COALESCE
		(
			CAST(DITStaging.factDataEntryDetails.DataEntryValueDateTime AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValueFloat AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValueMoney AS SQL_VARIANT),
			CAST(DITStaging.factDataEntryDetails.DataEntryValue AS SQL_VARIANT)
		),
		@RunDate,
		@RunDate
	FROM #tmpBatches IMPORT
	INNER JOIN DITStaging.factStubs
		ON IMPORT.BatchID = DITStaging.factStubs.BatchID
	INNER JOIN DITStaging.factDataEntryDetails
		ON DITStaging.factStubs.BatchID = DITStaging.factDataEntryDetails.BatchID
		AND DITStaging.factStubs.TransactionID = DITStaging.factDataEntryDetails.TransactionID
		AND DITStaging.factStubs.BatchSequence = DITStaging.factDataEntryDetails.BatchSequence
	INNER JOIN RecHubException.ExceptionBatches
		ON IMPORT.BatchID = RecHubException.ExceptionBatches.BatchID
	INNER JOIN RecHubException.Transactions
		ON RecHubException.ExceptionBatches.ExceptionBatchKey = RecHubException.Transactions.ExceptionBatchKey
		AND DITStaging.factStubs.TransactionID = RecHubException.Transactions.TransactionID
	INNER JOIN RecHubException.Stubs
		ON RecHubException.Transactions.TransactionKey = RecHubException.Stubs.TransactionKey
		AND DITStaging.factStubs.BatchSequence = RecHubException.Stubs.BatchSequence
	INNER JOIN RecHubData.dimDataEntryColumns
		ON DITStaging.factDataEntryDetails.DataEntryColumnKey = RecHubData.dimDataEntryColumns.DataEntryColumnKey
	WHERE RecHubData.dimDataEntryColumns.FldName <> 'amount'
	  AND RecHubData.dimDataEntryColumns.FldName <> 'accountnumber';

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatches')) 
		   DROP TABLE #tmpBatches

END TRY

BEGIN CATCH

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatches')) 
		   DROP TABLE #tmpBatches

	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

