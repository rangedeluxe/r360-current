--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkClientSetup_Del_ByResponseTableJoin
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkClientSetup_Del_ByResponseTableJoin') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportWorkClientSetup_Del_ByResponseTableJoin
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkClientSetup_Del_ByResponseTableJoin
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete Work Client Setup rows from SSIS Staging where error has occurred.
*
* Modification History
* 03/26/2012 CR 51754 JPB	Created
* 04/10/2013 WI 92094 JPB	Updated to 2.0.
*							Moved to DITStaging schema.
*							Renamed from Renamed from usp_DataImportWorkClientSetup_DeleteByResponseTableJoin.
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	DELETE	
		DITStaging.DataImportWorkClientSetup
	FROM	
		DITStaging.DataImportWorkClientSetup
		INNER JOIN DITStaging.DataImportWorkClientSetupResponses ON DITStaging.DataImportWorkClientSetup.ClientTrackingID = DITStaging.DataImportWorkClientSetupResponses.ClientTrackingID
	WHERE	
		DITStaging.DataImportWorkClientSetupResponses.ResponseStatus = 1;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
