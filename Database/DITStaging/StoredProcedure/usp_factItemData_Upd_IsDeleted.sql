--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factItemData_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factItemData_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factItemData_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factItemData_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete RecHubData fact rows based from DIT Staging fact rows.
*
* Modification History
* 02/28/2012 CR 50830 JPB	Created
* 04/04/2013 WI 92108 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_OLTAfactItemData_DeleteByWorkTableJoin.
*							Added @parmModificationDate.
* 08/20/2014 WI 160045 JBS	Changed to use SourceBatchID instead of BatchID 
*							to match up rows to mark deleted.  BatchID always a newly generated number
* 12/02/2015 WI 249667 MGE	Changed to not use DepositDateKey and SourceProcessingDateKey in join (for case where deposit date changes)
* 02/23/2016 WI 265496 JPB	Removed OrganizationKey from INNER JOINs.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factItemData
	SET		
		RecHubData.factItemData.IsDeleted = 1,
		RecHubData.factItemData.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factItemData
		INNER JOIN DITStaging.DataImportWorkBatch ON 
				RecHubData.factItemData.ImmutableDateKey = DITStaging.DataImportWorkBatch.ImmutableDateKey
			AND RecHubData.factItemData.BankKey = DITStaging.DataImportWorkBatch.BankKey
			AND RecHubData.factItemData.ClientAccountKey = DITStaging.DataImportWorkBatch.ClientAccountKey
			AND RecHubData.factItemData.SourceBatchID = DITStaging.DataImportWorkBatch.SourceBatchID
        INNER JOIN RecHubData.factBatchSummary ON RecHubData.factBatchSummary.DepositDateKey = RecHubData.factItemData.DepositDateKey
			AND RecHubData.factBatchSummary.ImmutableDateKey = RecHubData.factItemData.ImmutableDateKey
			AND RecHubData.factBatchSummary.SourceProcessingDateKey= RecHubData.factItemData.SourceProcessingDateKey
			AND RecHubData.factBatchSummary.BankKey = RecHubData.factItemData.BankKey
			AND RecHubData.factBatchSummary.ClientAccountKey = RecHubData.factItemData.ClientAccountKey
			AND RecHubData.factBatchSummary.BatchID = RecHubData.factItemData.BatchID
			AND RecHubData.factBatchSummary.SourceBatchID = RecHubData.factItemData.SourceBatchID
	WHERE 
		RecHubData.factBatchSummary.BatchSourceKey = DITStaging.DataImportWorkBatch.BatchSourceKey
		AND DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
		AND RecHubData.factItemData.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
