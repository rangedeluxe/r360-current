--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubException">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_UpGet_ForBusinessRules
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factTransactionSummary_UpGet_ForBusinessRules') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factTransactionSummary_UpGet_ForBusinessRules
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factTransactionSummary_UpGet_ForBusinessRules

AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: KLC
* Date: 06/05/2014
*
* Purpose: Reads the transactions currently in the DITStagging tables, updating
*			those with no business rules to not be Exceptions and returning
*			all others so that business rules can be applied before moving to
*			RecHubData
*
* Modification History
* 06/05/2014 WI 146065 KLC	Created
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	--If the client account record does not exist yet or there is not data entry setup, then it's not possible
	-- to have rules defined for the transaction and we should flag those as good right away
	UPDATE	DITStaging.factTransactionSummary
	SET		DITStaging.factTransactionSummary.Exception = 0
	FROM	DITStaging.factTransactionSummary
		LEFT JOIN RecHubData.dimClientAccounts
			ON DITStaging.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		LEFT JOIN RecHubException.DataEntrySetups
			ON RecHubData.dimClientAccounts.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubException.DataEntrySetups.SiteWorkgroupID
	WHERE RecHubException.DataEntrySetups.DataEntrySetupKey IS NULL;

	--Now return any other transactions that have rules to apply
	SELECT	DITStaging.factTransactionSummary.BatchID,
			DITStaging.factTransactionSummary.TransactionID,
			DITStaging.factTransactionSummary.BatchSourceKey,
			DITStaging.factTransactionSummary.BatchPaymentTypeKey,
			RecHubException.DataEntrySetups.DataEntrySetupKey
	FROM	DITStaging.factTransactionSummary
		INNER JOIN RecHubData.dimClientAccounts
			ON DITStaging.factTransactionSummary.ClientAccountKey = RecHubData.dimClientAccounts.ClientAccountKey
		INNER JOIN RecHubException.DataEntrySetups
			ON RecHubData.dimClientAccounts.SiteBankID = RecHubException.DataEntrySetups.SiteBankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = RecHubException.DataEntrySetups.SiteWorkgroupID
	WHERE	DITStaging.factTransactionSummary.Exception IS NULL
	ORDER BY	DITStaging.factTransactionSummary.BatchID ASC,
				DITStaging.factTransactionSummary.TransactionID ASC;--Order matters as proc getting items/fields relies on it for merging in code

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

