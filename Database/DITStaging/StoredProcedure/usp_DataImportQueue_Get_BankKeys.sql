--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_BankKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Get_BankKeys') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Get_BankKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Get_BankKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Retrieve bank keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 02/26/2012 CR 50813 JPB	Created
* 04/08/2013 WI 92081 JBS	Update to 2.0 release.  Change schema name.
*							Rename proc from usp_DataImportQueue_GetBankKeys
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	;WITH BankList AS
	(
		SELECT DISTINCT SiteBankID
		FROM	DITStaging.DataImportWorkBatchStartup
	)
	SELECT	BankKey,
			RecHubData.dimBanks.SiteBankID,
			MostRecent AS MostRecentBankKey
	FROM	RecHubData.dimBanks
			INNER JOIN BankList ON RecHubData.dimBanks.SiteBankID = BankList.SiteBankID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		