--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factItemData_Get_ForCopy
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factItemData_Get_ForCopy') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factItemData_Get_ForCopy
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factItemData_Get_ForCopy
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/25/2017
*
* Purpose: retrieve the factItemData records from the staging table.
*
* Modification History
* 08/25/2017 PT 149779911	JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT 
		DITStaging.factItemData.IsDeleted,
		DITStaging.factItemData.BankKey,
		DITStaging.factItemData.OrganizationKey,
		DITStaging.factItemData.ClientAccountKey,
		DITStaging.factItemData.DepositDateKey,
		DITStaging.factItemData.ImmutableDateKey,
		DITStaging.factItemData.SourceProcessingDateKey,
		DITStaging.factItemData.BatchID,
		DITStaging.factItemData.SourceBatchID,
		DITStaging.factItemData.BatchNumber,
		DITStaging.factItemData.BatchSourceKey,
		DITStaging.factItemData.TransactionID,
		DITStaging.factItemData.BatchSequence,
		DITStaging.factItemData.ItemDataSetupFieldKey,
		DITStaging.factItemData.CreationDate,
		DITStaging.factItemData.ModificationDate,
		DITStaging.factItemData.DataValueDateTime,
		DITStaging.factItemData.DataValueMoney,
		DITStaging.factItemData.DataValueInteger,
		DITStaging.factItemData.DataValueBit,
		DITStaging.factItemData.ModifiedBy,
		DITStaging.factItemData.DataValue
	FROM 
		DITStaging.factItemData
		INNER JOIN DITStaging.DataImportWorkBatch ON DITStaging.DataImportWorkBatch.BatchTrackingID = DITStaging.factItemData.BatchTrackingID
	WHERE 
		DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH