--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLClient_Get_UniqueRows
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLClient_Get_UniqueRows') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLClient_Get_UniqueRows
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLClient_Get_UniqueRows
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/20/2012
*
* Purpose: Retrieve only the most current records from the XMLClient table.
*			I.E. remove duplicates from the record set.
*
* Modification History
* 05/20/2012 CR 52909 JPB	Created
* 04/10/2013 WI 92121 JPB	Updated to 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_XMLClient_GetNonDuplicateRecords.
* 11/19/2014 WI 178621 JBS	Add NewSumValue column.  This will be used to determine when
*							to add historical dimension record change.
* 11/09/2016 PT 133205983 JPB	Added Client_Id to the query to return the correct data set.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH ClientList AS
	(
		SELECT	
			MAX(ClientGroup_Id) AS ClientGroup_Id,MAX(Client_Id) AS Client_Id
		FROM	
			DITStaging.XMLClient
		GROUP BY 
			BankID,
			ClientID
	)
	SELECT 
		DITStaging.XMLClient.*, CHECKSUM (OnlineColorMode, POBox) AS NewSumValue
	FROM 
		DITStaging.XMLClient
		INNER JOIN ClientList ON ClientList.ClientGroup_Id = DITStaging.XMLClient.ClientGroup_Id
			AND ClientList.Client_Id = DITStaging.XMLClient.Client_Id
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
