--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkBatch_Upd_Response
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkBatch_Upd_Response') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportWorkBatch_Upd_Response
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkBatch_Upd_Response
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/24/2012
*
* Purpose: 
*
* Modification History
* 08/24/2012 CR 55261 JPB	Created
* 04/09/2013 WI 92093 JBS	Update to 2.0 release.  Change schema name
*							Rename proc FROM usp_DataImportWorkBatch_SetXMLResponseDocument
*							Change references From Customer to Organization,
*							Lockbox to ClientAccount, Processing to Immutable.
* 06/16/2014 WI 148061 CMC	Handle missing XMLBatch records.
* 07/07/2014 WI 152237 CMC	Added SourceBatchID.
* 02/05/2015 WI 188118 JPB	Added retry support.
* 02/11/2015 WI 190142 JPB	Added new working table to get the correct values for the repsonse.
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

DECLARE @BatchTrackingID	UNIQUEIDENTIFIER,
		@Loop				INT,
		@ResponseType		TINYINT,
		@XMLResponse		XML;

BEGIN TRY

	DECLARE @ResponseList TABLE 
	(
		RowID			INT IDENTITY(1,1), 
		BatchTrackingID UNIQUEIDENTIFIER,
		ResponseType	TINYINT
	);

	/* Get a distinct list of batch tracking IDs from the response table */
	INSERT INTO @ResponseList(BatchTrackingID,ResponseType)
	SELECT	DISTINCT DITStaging.DataImportWorkBatchResponses.BatchTrackingID,
			MIN(DITStaging.DataImportWorkBatchResponses.ResponseStatus)
	FROM	DITStaging.DataImportWorkBatchResponses
	GROUP BY DITStaging.DataImportWorkBatchResponses.BatchTrackingID;
		
	/* Update Work table with error if error row exists */
	UPDATE DITStaging.DataImportWorkBatch
	SET DITStaging.DataImportWorkBatch.ResponseStatus = ResponseType
	FROM	DITStaging.DataImportWorkBatch
			INNER JOIN @ResponseList RL ON RL.BatchTrackingID = DITStaging.DataImportWorkBatch.BatchTrackingID
	
	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get recods that exist in the response table but not in the work table */
	INSERT INTO @ResponseList(BatchTrackingID,ResponseType)
	SELECT	BatchTrackingID,
			0
	FROM	DITStaging.DataImportWorkBatchResponses
	EXCEPT 
	SELECT	BatchTrackingID,
			0
	FROM	DITStaging.DataImportWorkBatch;

	UPDATE @ResponseList
	SET	ResponseType = ResponseStatus
	FROM	@ResponseList RL
			INNER JOIN DITStaging.DataImportWorkBatchResponses ON DITStaging.DataImportWorkBatchResponses.BatchTrackingID = RL.BatchTrackingID;

	INSERT INTO DITStaging.DataImportWorkBatch
	(
		BatchTrackingID,
		Batch_Id,
		ResponseStatus,
		BankKey,
		OrganizationKey,
		ClientAccountKey,
		DepositDateKey,
		ImmutableDateKey,
		SourceProcessingDateKey,
		SourceBatchID,
		BatchID,
		BatchNumber,
		SiteBankID,
		SiteClientAccountID,
		DepositDate,
		ImmutableDate,
		SourceProcessingDate,
		BatchCueID,
		BatchSourceKey,
		BatchPaymentTypeKey,
		BatchSiteCode
	)
	SELECT	COALESCE(DITStaging.XMLBatch.BatchTrackingID, RL.BatchTrackingID),
			COALESCE(DITStaging.XMLBatch.Batch_Id, 0),
			RL.ResponseType, /*ResponseStatus*/
			0, /*BankKey*/
			0, /*OrganizationKey*/
			0, /*ClientAccountKey*/
			0, /*DepositDateKey*/
			0, /*ImmutableDateKey*/
			0, /*SourceProcessingDateKey*/
			COALESCE(DITStaging.XMLBatch.SourceBatchID, 0),
			COALESCE(DITStaging.XMLBatch.BatchID, 0),
			COALESCE(DITStaging.XMLBatch.BatchNumber, 0),
			COALESCE(DITStaging.XMLBatch.SiteBankID, 0),
			COALESCE(DITStaging.XMLBatch.SiteClientAccountID, 0),
			COALESCE(DITStaging.XMLBatch.DepositDate, GETDATE()),
			COALESCE(DITStaging.XMLBatch.BatchDate, GETDATE()),
			COALESCE(DITStaging.XMLBatch.ImmutableDate, GETDATE()),
			COALESCE(DITStaging.XMLBatch.BatchCueID, 0),
			COALESCE(DITStaging.DataImportWorkBatchKeys.BatchSourceKey, 0),
			COALESCE(DITStaging.DataImportWorkBatchKeys.BatchPaymentTypeKey, 0),
			COALESCE(DITStaging.XMLBatch.BatchSiteCode, 0)
	FROM DITStaging.XMLBatch
			LEFT JOIN DITStaging.DataImportWorkBatchKeys ON DITStaging.DataImportWorkBatchKeys.Batch_Id = DITStaging.XMLBatch.Batch_Id
			RIGHT JOIN @ResponseList RL ON RL.BatchTrackingID = DITStaging.XMLBatch.BatchTrackingID;

	/* Clear the response list */
	DELETE FROM @ResponseList;
	
	/* Get all records that need to be processed */
	INSERT INTO @ResponseList(BatchTrackingID,ResponseType)
	SELECT	BatchTrackingID,
			ResponseStatus
	FROM	DITStaging.DataImportWorkBatch;
	
	SET @Loop = 1;

	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ResponseList) )
	BEGIN

		SELECT	@BatchTrackingID = BatchTrackingID,
				@ResponseType = ResponseType
		FROM	@ResponseList 
		WHERE RowID = @Loop;


		SELECT @XMLResponse = 
		(
			SELECT	TOP (1) BatchTrackingID AS '@BatchTrackingID',
					SiteBankID AS '@BankID',
					SiteClientAccountID AS '@ClientID',
					SourceBatchID AS '@BatchID',
					BatchSiteCode AS '@BatchSiteCode',
					CONVERT(CHAR(10),ImmutableDate,126) AS '@BatchDate',
					CONVERT(CHAR(10),DepositDate,126) AS '@DepositDate',
					CONVERT(CHAR(10),SourceProcessingDate,126) AS '@SourceProcessingDate',
					BatchPaymentTypeKey AS '@BatchPaymentTypeKey',
					ImmutableDateKey AS '@ImmutableDateKey',
					BatchSourceKey AS '@BatchSourceKey',
					(
						SELECT CASE	ResponseStatus
							WHEN 0 THEN 'Success'
							WHEN 1 THEN 'Fail' 
							WHEN 2 THEN 'Warning'
						END AS 'Results/*'
						FROM DITStaging.DataImportWorkBatch
						WHERE DITStaging.DataImportWorkBatch.BatchTrackingID = @BatchTrackingID
						FOR XML PATH(''), TYPE
					),
					(
						SELECT	ResultsMessage AS 'ErrorMessage/*',NULL
						FROM	DITStaging.DataImportWorkBatchResponses
						WHERE	B1.BatchTrackingID = DITStaging.DataImportWorkBatchResponses.BatchTrackingID
						FOR XML PATH(''), TYPE
					)
			FROM DITStaging.DataImportWorkBatch B1
			WHERE B1.BatchTrackingID = @BatchTrackingID
			FOR XML PATH('Batch'), TYPE
		);
				
		UPDATE RecHubSystem.DataImportQueue
		SET	RecHubSystem.DataImportQueue.XMLResponseDocument = @XMLResponse,
			RecHubSystem.DataImportQueue.ResponseStatus = @ResponseType
		WHERE RecHubSystem.DataImportQueue.EntityTrackingID = @BatchTrackingID;

		SET @Loop = @Loop + 1;	
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH