--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Upd_BatchDataImportComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Upd_BatchDataImportComplete') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Upd_BatchDataImportComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Upd_BatchDataImportComplete
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Updating batches in process to completed.
*
* Modification History
* 02/26/2012 CR 50820 JPB	Created
* 04/19/2013 WI 92087 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc from usp_DataImportQueue_UpdateBatchDataImportComplete
* 03/05/2015 WI 188154 JPB	Added retry logic.
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;
 
BEGIN TRY
	UPDATE 	RecHubSystem.DataImportQueue 
	SET 	RecHubSystem.DataImportQueue.QueueStatus = 
				CASE DITStaging.DataImportWorkBatchResponses.Retry
					WHEN 0 THEN 99
					WHEN 1 THEN CASE WHEN RecHubSystem.DataImportQueue.RetryCount < 9 THEN 15 ELSE 99 END
					ELSE 99
				END,
			RecHubSystem.DataImportQueue.RetryCount = 
				CASE DITStaging.DataImportWorkBatchResponses.Retry
					WHEN 1 THEN RecHubSystem.DataImportQueue.RetryCount + 1
					ELSE RecHubSystem.DataImportQueue.RetryCount
				END,
			RecHubSystem.DataImportQueue.ModificationDate = GETDATE(),
			RecHubSystem.DataImportQueue.ModifiedBY = SUSER_SNAME() 
	FROM 	RecHubSystem.DataImportQueue
			INNER JOIN DITStaging.DataImportWorkBatchResponses 
				ON RecHubSystem.DataImportQueue.EntityTrackingID = DITStaging.DataImportWorkBatchResponses.BatchTrackingID
	WHERE	QueueStatus = 20;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
