--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkBatch_Upd_ResponseStatusByResponseJoin
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkBatch_Upd_ResponseStatusByResponseJoin') IS NOT NULL
       DROP PROCEDURE SSISStaging.usp_DataImportWorkBatch_Upd_ResponseStatusByResponseJoin
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkBatch_Upd_ResponseStatusByResponseJoin
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Update Response on DITStaging.DataImportWorkBatch
*
* Modification History
* 02/28/2012 CR 50823 JPB	Created
* 04/22/2013 WI 92092 JBS	Update table to 2.0 release. Change schema to DITStaging
*							Rename proc from usp_DataImportWorkBatch_SetResponseStatusByResponseJoin
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	UPDATE DITStaging.DataImportWorkBatch
		SET	DITStaging.DataImportWorkBatch.ResponseStatus = COALESCE(DITStaging.DataImportWorkBatchResponses.ResponseStatus,0)
	FROM DITStaging.DataImportWorkBatch
		LEFT JOIN DITStaging.DataImportWorkBatchResponses ON DITStaging.DataImportWorkBatch.BatchTrackingID = DITStaging.DataImportWorkBatchResponses.BatchTrackingID;
END TRY

BEGIN CATCH
	EXEC RecHUbCommon.usp_WfsRethrowException;
END CATCH