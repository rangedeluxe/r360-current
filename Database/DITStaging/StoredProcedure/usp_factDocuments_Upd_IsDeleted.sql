--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDocuments_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factDocuments_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factDocuments_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factDocuments_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete RecHubData fact rows based from DIT Staging fact rows.
*
* Modification History
* 02/28/2012 CR 50829 JPB	Created
* 04/04/2013 WI 92107 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_OLTAfactDocuments_DeleteByWorkTableJoin.
*							Added @parmModificationDate.
* 08/20/2014 WI 160043 JBS	Changed to use SourceBatchID instead of BatchID 
*							to match up rows to mark deleted.  BatchID always a newly generated number
* 12/02/2015 WI 249666 MGE	Changed to not use DepositDateKey and SourceProcessingDateKey in join
* 02/23/2016 WI 265495 JPB	Removed OrganizationKey from INNER JOIN.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	UPDATE	
		RecHubData.factDocuments
	SET		
		RecHubData.factDocuments.IsDeleted = 1,
		RecHubData.factDocuments.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factDocuments
		INNER JOIN DITStaging.DataImportWorkBatch ON 
				RecHubData.factDocuments.ImmutableDateKey = DITStaging.DataImportWorkBatch.ImmutableDateKey
			AND RecHubData.factDocuments.BankKey = DITStaging.DataImportWorkBatch.BankKey
			AND RecHubData.factDocuments.ClientAccountKey = DITStaging.DataImportWorkBatch.ClientAccountKey
			AND RecHubData.factDocuments.SourceBatchID = DITStaging.DataImportWorkBatch.SourceBatchID
			AND RecHubData.factDocuments.BatchSourceKey = DITStaging.DataImportWorkBatch.BatchSourceKey
	WHERE 
		DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
		AND RecHubData.factDocuments.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
