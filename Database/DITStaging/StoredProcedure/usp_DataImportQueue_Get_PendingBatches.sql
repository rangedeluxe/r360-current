--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_PendingBatches
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Get_PendingBatches') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Get_PendingBatches
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Get_PendingBatches
(
	@parmPendingBatches BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Retrieve batch data from OLTA.DataImportQueue that are ready for processing.
*
* Modification History
* 02/26/2012 CR 50816 JPB	Created
* 05/15/2012 CR 53023 JPB	Performance improvements.
* 04/08/2013 WI 92084 JPB	Updated to 2.0. Moved to DITStaging schema.
*							Renamed from usp_DataImportQueue_GetPendingBatches.
* 02/05/2015 WI 188199 JPB	Added retry support.
* 05/31/2016 WI 283606 JPB	Corrected retry QueueType to 1
* 08/08/2017 PT 149403903 JPB/MGE Removed CROSS APPLY and CAST of XMLDataDocument
* 07/02/2018 PT 15779079  CMC Change for Json
**********************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;
BEGIN TRY
	INSERT INTO DITStaging.XMLBatches(Batch_Id,xmlBatch,jsonBatch)
	SELECT	DataImportQueueID AS Batch_Id,
			XMLDataDocument AS xmlBatch, 
			JSONDataDocument as jsonBatch
	FROM	RecHubSystem.DataImportQueue
	WHERE	QueueType = 1
			AND QueueStatus IN (10,15,20)
			AND CreationDate <= (SELECT COALESCE(MAX(ModificationDate),GETDATE()) FROM RecHubSystem.DataImportQueue WHERE QueueType = 1 AND QueueStatus IN (10,20));

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingBatches = 1;
		UPDATE	RecHubSystem.DataImportQueue
		SET		QueueStatus = 20,
				ResponseStatus = 1,
				ModificationDate = GETDATE(),
				ModifiedBy = SUSER_SNAME()
		FROM	RecHubSystem.DataImportQueue
				INNER JOIN DITStaging.XMLBatches ON DITStaging.XMLBatches.Batch_Id = RecHubSystem.DataImportQueue.DataImportQueueID;
	END
	ELSE SET @parmPendingBatches = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH