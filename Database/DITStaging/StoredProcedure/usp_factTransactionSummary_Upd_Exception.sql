--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factTransactionSummary_Upd_Exception
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factTransactionSummary_Upd_Exception') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factTransactionSummary_Upd_Exception
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factTransactionSummary_Upd_Exception
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/07/2017
*
* Purpose: Update DITStaging.factTransactionSummary.Exception
*
* Modification History
* 04/07/2017 PT 141788325 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
 
BEGIN TRY
	UPDATE
		DITStaging.factTransactionSummary
	SET
		Exception = 1
	FROM
		DITStaging.factTransactionSummary
		INNER JOIN DITStaging.ExceptionTransactions ON DITStaging.ExceptionTransactions.BatchID = DITStaging.factTransactionSummary.BatchID
			AND DITStaging.ExceptionTransactions.TransactionID = DITStaging.factTransactionSummary.TransactionID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH