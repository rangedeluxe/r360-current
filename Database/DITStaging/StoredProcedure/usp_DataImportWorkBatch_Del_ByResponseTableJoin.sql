--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkBatch_Del_ByResponseTableJoin
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkBatch_Del_ByResponseTableJoin') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportWorkBatch_Del_ByResponseTableJoin
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkBatch_Del_ByResponseTableJoin
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete Work Client Setup rows from SSIS Staging where error has occurred.
*
* Modification History
* 03/26/2012 CR 51514 JPB	Created
* 04/22/2013 WI 92090 JBS	Update to 2.0 Release. Change schema to DITStaging
*							Rename proc from usp_DataImportWorkBatch_DeleteByResponseTableJoin
* 12/30/2014 WI 182848 CMC	Filter out warnings too
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON; 

BEGIN TRY

	DELETE	DITStaging.DataImportWorkBatch
	FROM	DITStaging.DataImportWorkBatch
			INNER JOIN DITStaging.DataImportWorkBatchResponses 
				ON DITStaging.DataImportWorkBatch.BatchTrackingID = DITStaging.DataImportWorkBatchResponses.BatchTrackingID
	WHERE	DITStaging.DataImportWorkBatchResponses.ResponseStatus IN (1, 2);

END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
