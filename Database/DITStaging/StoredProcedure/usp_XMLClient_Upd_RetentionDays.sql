--WFSScriptProcessorSystemName Rechub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_XMLClient_Upd_RetentionDays
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLClient_Upd_RetentionDays') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLClient_Upd_RetentionDays
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLClient_Upd_RetentionDays
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 05/14/2014
*
* Purpose: Used in SSIS Package DataImportIntegrationServices_ClientSetup_LoadData.dtsx
*			to Update RetentionDays columns in XMLClient table if they are NULL.
*
* Modification History
* 05/14/2014 WI142188 JBS Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY

	IF EXISTS( SELECT 1 FROM DITStaging.XMLClient
	WHERE DITStaging.XMLClient.DataRetentionDays IS NULL OR DITStaging.XMLClient.ImageRetentionDays IS NULL)
	BEGIN
		DECLARE @DataRetentionDays INT = 0,
				@ImageRetentionDays INT = 0;

		SELECT @DataRetentionDays = Value FROM RechubConfig.SystemSetup WHERE Section = 'Rec Hub Table Maintenance' AND SetupKey = 'DefaultDataRetentionDays'
		SELECT @ImageRetentionDays = Value FROM RechubConfig.SystemSetup WHERE Section = 'Rec Hub Table Maintenance' AND SetupKey = 'DefaultImageRetentionDays'

		UPDATE 
			DITStaging.XMLClient
		SET 
			DITStaging.XMLClient.DataRetentionDays = COALESCE(DITStaging.XMLClient.DataRetentionDays, RecHubData.dimClientAccounts.DataRetentionDays, @DataRetentionDays),
			DITStaging.XMLClient.ImageRetentionDays = COALESCE(DITStaging.XMLClient.ImageRetentionDays, RecHubData.dimClientAccounts.ImageRetentionDays, @ImageRetentionDays)
		FROM 
			DITStaging.XMLClient 
			LEFT OUTER JOIN RecHubData.dimClientAccounts ON 
				DITStaging.XMLClient.BankID = RecHubData.dimClientAccounts.SiteBankID
				AND DITStaging.XMLClient.ClientID = RecHubData.dimClientAccounts.SiteClientAccountID
				AND RecHubData.dimClientAccounts.MostRecent = 1	
		WHERE
			DITStaging.XMLClient.DataRetentionDays IS NULL OR
			DITStaging.XMLClient.ImageRetentionDays IS NULL		
	END

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
