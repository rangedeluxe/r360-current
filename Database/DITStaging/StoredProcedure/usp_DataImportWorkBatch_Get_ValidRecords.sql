--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportWorkBatch_Get_ValidRecords
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportWorkBatch_Get_ValidRecords') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportWorkBatch_Get_ValidRecords
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportWorkBatch_Get_ValidRecords
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: 
*
* Modification History
* 02/28/2012 CR 50822 JPB	Created
* 04/19/2013 WI 92091 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc from usp_DataImportWorkBatch_GetValidRecords
* 07/03/2014 WI 151964 CMC	Include SourceBatchID in selection
* 07/14/2015 WI 224015 JPB	Added order by so SSIS package does not have to sort
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON; 

BEGIN TRY

	SELECT 
		DITStaging.DataImportWorkBatch.BatchTrackingID,
		DITStaging.DataImportWorkBatch.Batch_Id,
		DITStaging.DataImportWorkBatch.ResponseStatus,
        DITStaging.DataImportWorkBatch.BankKey,
        DITStaging.DataImportWorkBatch.OrganizationKey,
        DITStaging.DataImportWorkBatch.ClientAccountKey,
        DITStaging.DataImportWorkBatch.DepositDateKey,
        DITStaging.DataImportWorkBatch.ImmutableDateKey,
        DITStaging.DataImportWorkBatch.SourceProcessingDateKey,
		DITStaging.DataImportWorkBatch.SourceBatchID,
        DITStaging.DataImportWorkBatch.BatchID,
        DITStaging.DataImportWorkBatch.BatchNumber,
        DITStaging.DataImportWorkBatch.SiteBankID,
        DITStaging.DataImportWorkBatch.SiteClientAccountID,
        DITStaging.DataImportWorkBatch.DepositDate,
        DITStaging.DataImportWorkBatch.ImmutableDate,
        DITStaging.DataImportWorkBatch.SourceProcessingDate,
        DITStaging.DataImportWorkBatch.BatchCueID,
        DITStaging.DataImportWorkBatch.BatchSourceKey,
        DITStaging.DataImportWorkBatch.BatchPaymentTypeKey,
        DITStaging.DataImportWorkBatch.BatchSiteCode
	FROM 
		DITStaging.DataImportWorkBatch 
	WHERE 
		DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
	ORDER BY
		DITStaging.DataImportWorkBatch.BatchTrackingID;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH