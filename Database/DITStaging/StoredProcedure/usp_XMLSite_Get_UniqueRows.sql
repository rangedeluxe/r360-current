--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLSite_Get_UniqueRows
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLSite_Get_UniqueRows') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLSite_Get_UniqueRows
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLSite_Get_UniqueRows
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/20/2012
*
* Purpose: Retrieve only the most current records from the XMLSite table.
*			I.E. remove duplicates from the record set.
*
* Modification History
* 05/20/2012 CR 52906 JPB	Created
* 04/05/2013 WI 92142 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc from usp_XMLSite_GetNonDuplicateRecords
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	;WITH SiteList AS 
	(
		SELECT	MAX(ClientGroup_Id) AS ClientGroup_Id
			FROM	DITStaging.XMLSite
			GROUP BY SiteCode
	)
	SELECT DITStaging.XMLSite.* FROM DITStaging.XMLSite
		INNER JOIN SiteList ON SiteList.ClientGroup_Id = DITStaging.XMLSite.ClientGroup_Id
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH