--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatch_Del_Duplicates
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatch_Del_Duplicates') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBatch_Del_Duplicates
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatch_Del_Duplicates
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/21/2012
*
* Purpose: Extract the Batch Node from the XML data and insert into the 
*			DITStaging.XMLBankNodes table.
*
* Modification History
* 05/21/2012 CR 52968 JPB	Created
* 04/19/2013 WI 92118 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc from usp_XMLBatch_DeleteDuplicates
* 02/11/2015 WI 190141 JPB	Replaced keys with string version
* 05/18/2015 WI 213669 JPB	Replace BatchID with SourceBatchID in dup detect.
* 12/03/2015 WI 250258 JPB	Call new SP to process duplicate errors.
* 05/31/2016 WI 283107 JPB	Check for in process dup the same as existing batch dup check.
* 08/29/2016 PT	#127604069	JPB	Changed ImmutableDate to BatchDate.
* 02/06/2017 PT #137726225	JPB	Return count.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @DuplicateBatchList DITStaging.DuplicateBatchList;

BEGIN TRY

	;WITH BatchList AS
	(
		SELECT	
			MAX(DITStaging.XMLBatch.Batch_Id) AS Batch_Id
		FROM
			DITStaging.XMLBatch
		GROUP BY 
			DITStaging.XMLBatch.BatchDate, 
			DITStaging.XMLBatch.SiteBankID, 
			DITStaging.XMLBatch.SiteClientAccountID, 
			DITStaging.XMLBatch.SourceBatchID,
			UPPER(DITStaging.XMLBatch.BatchSource)
	)
	INSERT INTO @DuplicateBatchList
	(
		Batch_Id,
		BatchTrackingID,
		DepositDate,
		BatchDate,
		ImmutableDate,
		SourceBatchID,
		SiteBankID,
		SiteClientAccountID,
		BatchSource,
		BatchPaymentType,
		BatchSiteCode
	)
	SELECT	
		DITStaging.XMLBatch.Batch_Id,
		DITStaging.XMLBatch.BatchTrackingID,
		CAST(DITStaging.XMLBatch.DepositDate AS DATE) AS DepositDate,
		CAST(DITStaging.XMLBatch.BatchDate AS DATE) AS BatchDate,
		CAST(DITStaging.XMLBatch.ImmutableDate AS DATE) AS ImmutableDate,
		DITStaging.XMLBatch.SourceBatchID,
		DITStaging.XMLBatch.SiteBankID,
		DITStaging.XMLBatch.SiteClientAccountID,
		DITStaging.XMLBatch.BatchSource,
		DITStaging.XMLBatch.BatchPaymentType,
		DITStaging.XMLBatch.BatchSiteCode
	FROM	
		DITStaging.XMLBatch
	WHERE	
		DITStaging.XMLBatch.Batch_Id NOT IN (SELECT Batch_Id FROM BatchList);

	EXEC DITStaging.usp_DataImportQueue_Upd_SetDuplicateBatchesComplete @parmDuplicateBatchList=@DuplicateBatchList;

	DELETE	DITStaging.XMLBatch
	FROM	DITStaging.XMLBatch
			INNER JOIN @DuplicateBatchList DL ON DITStaging.XMLBatch.Batch_Id = DL.Batch_Id;
	
	SELECT 
		COUNT(*) AS BatchCount
	FROM 
		DITStaging.XMLBatch;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
