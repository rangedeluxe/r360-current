--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorStoredProcedureName usp_factChecks_Get_ForValidation
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factChecks_Get_ForValidation') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factChecks_Get_ForValidation
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factChecks_Get_ForValidation

AS
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: KLC
* Date: 06/06/2014
*
* Purpose: Reads the check details currently in the DITStagging tables
*			returning them so that business rules can be applied before moving
*			to RecHubData
*
* Modification History
* 06/06/2014 WI 146277 KLC	Created
* 02/04/2016 R360-15457 JPB	Updated to support latest BRE structure
* 09/04/2019 R360-30474 CMC	Need to include batch sequence or BRE won't 
*                           recognize as separate payments
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	SELECT	
		DITStaging.DataImportWorkBatchKeys.SiteBankID,
		DITStaging.DataImportWorkBatchKeys.SiteClientAccountID,
		DITStaging.factChecks.BatchID,
		DITStaging.factChecks.TransactionID,
		DITStaging.factChecks.RemitterName,
		DITStaging.factChecks.BatchSequence
	FROM	
		DITStaging.factChecks
		INNER JOIN DITStaging.DataImportWorkBatchKeys ON DITStaging.DataImportWorkBatchKeys.BankKey = DITStaging.factChecks.BankKey
			AND DITStaging.DataImportWorkBatchKeys.ClientAccountKey = DITStaging.factChecks.ClientAccountKey
	ORDER BY
		DITStaging.DataImportWorkBatchKeys.SiteBankID,
		DITStaging.DataImportWorkBatchKeys.SiteClientAccountID,
		DITStaging.factChecks.BatchSourceKey,
		DITStaging.factChecks.BatchID,
		DITStaging.factChecks.TransactionID,
		DITStaging.factChecks.BatchSequence;

END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

