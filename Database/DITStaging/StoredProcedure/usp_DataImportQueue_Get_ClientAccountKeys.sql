--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DataImportQueue_Get_ClientAccountKeys
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_DataImportQueue_Get_ClientAccountKeys') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_DataImportQueue_Get_ClientAccountKeys
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_DataImportQueue_Get_ClientAccountKeys
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Retrieve ClientAccount keys for batches to be processed by the SSIS toolkit.
*
* Modification History
* 02/26/2012 CR 50815 JPB	Created
* 04/09/2013 WI 92083 JBS	Update to 2.0 release. Change schema to DITStaging
*							Rename proc FROM usp_DataImportQueue_GetLockboxKeys
*							Change references from Lockbox to ClientAccount,
*							Customer to Organization.
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON; 

BEGIN TRY
	;WITH LockboxList AS
	(
		SELECT DISTINCT SiteBankID,
						SiteClientAccountID
		FROM DITStaging.DataImportWorkBatchStartup
	)
	SELECT	RecHubData.dimClientAccounts.ClientAccountKey,
			RecHubData.dimClientAccounts.SiteClientAccountID AS SiteClientAccountID,
			RecHubData.dimClientAccounts.SiteBankID AS SiteBankID,
			RecHubData.dimClientAccounts.SiteOrganizationID AS SiteOrganizationID,
			RecHubData.dimClientAccounts.MostRecent AS MostRecentClientAccountKey
	FROM	RecHubData.dimClientAccounts
			INNER JOIN LockboxList ON RecHubData.dimClientAccounts.SiteBankID = LockboxList.SiteBankID
				AND RecHubData.dimClientAccounts.SiteClientAccountID = LockboxList.SiteClientAccountID;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH		
