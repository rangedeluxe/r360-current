--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_factDataEntrySummary_Upd_IsDeleted
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_factDataEntrySummary_Upd_IsDeleted') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_factDataEntrySummary_Upd_IsDeleted
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_factDataEntrySummary_Upd_IsDeleted
(
	@parmModificationDate DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/26/2012
*
* Purpose: Delete RecHubData fact rows from DIT Staging fact rows.
*
* Modification History
* 02/28/2012 CR 50828 JPB	Created
* 04/04/2013 WI 92106 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
*							Renamed from usp_OLTAfactBatchSummary_DeleteByWorkTableJoin.
*							Added @parmModificationDate.
* 08/20/2014 WI 160036 JBS	Changed to use SourceBatchID instead of BatchID 
*							to match up rows to mark deleted.  BatchID always a newly generated number
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	UPDATE	
		RecHubData.factDataEntrySummary
	SET		
		RecHubData.factDataEntrySummary.IsDeleted = 1,
		RecHubData.factDataEntrySummary.ModificationDate = @parmModificationDate
	FROM	
		RecHubData.factDataEntrySummary
		INNER JOIN DITStaging.DataImportWorkBatch ON RecHubData.factDataEntrySummary.DepositDateKey = DITStaging.DataImportWorkBatch.DepositDateKey
			AND RecHubData.factDataEntrySummary.ImmutableDateKey = DITStaging.DataImportWorkBatch.ImmutableDateKey
			AND RecHubData.factDataEntrySummary.SourceProcessingDateKey= DITStaging.DataImportWorkBatch.SourceProcessingDateKey
			AND RecHubData.factDataEntrySummary.BankKey = DITStaging.DataImportWorkBatch.BankKey
			AND RecHubData.factDataEntrySummary.OrganizationKey = DITStaging.DataImportWorkBatch.OrganizationKey
			AND RecHubData.factDataEntrySummary.ClientAccountKey = DITStaging.DataImportWorkBatch.ClientAccountKey
			AND RecHubData.factDataEntrySummary.SourceBatchID = DITStaging.DataImportWorkBatch.SourceBatchID
			AND RecHubData.factDataEntrySummary.BatchSourceKey = DITStaging.DataImportWorkBatch.BatchSourceKey
	WHERE 
		DITStaging.DataImportWorkBatch.ResponseStatus IN (0,2)
		AND RecHubData.factDataEntrySummary.IsDeleted = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH