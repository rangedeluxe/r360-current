--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorViewName XMLBatchViewInsertWithoutBatchIDNumber
--WFSScriptProcessorViewDrop
IF OBJECT_ID('DITStaging.XMLBatchViewInsertWithoutBatchIDNumber') IS NOT NULL
	DROP VIEW DITStaging.XMLBatchViewInsertWithoutBatchIDNumber
GO

--WFSScriptProcessorViewCreate
CREATE VIEW DITStaging.XMLBatchViewInsertWithoutBatchIDNumber
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2014
*
* Purpose: View used to insert records into XMLBatch that do not have a
*		BatchID and BatchNumber defined.
*
*
* Modification History
* 04/04/2014 WI 135414 JPB	Created
* 10/22/2014 WI 174915 JPB	Added duplicate detect columns
* 02/11/2015 WI 190139 JPB	Replaced key columns with string version
******************************************************************************/
SELECT
	Batch_Id,
	BatchTrackingID,
	DepositDate,
	BatchDate,
	ImmutableDate,
	SiteBankID,
	SiteClientAccountID,
	BatchCueID,
	BatchSource,
	BatchPaymentType,
	BatchSiteCode,
	FileHashChecker,
	ABA,
	DDA,
	FileHash,
	FileSignature
FROM
	DITStaging.XMLBatch;
