--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorViewName XMLBatchViewInsertWithoutBatchNumber
--WFSScriptProcessorViewDrop
IF OBJECT_ID('DITStaging.XMLBatchViewInsertWithoutBatchNumber') IS NOT NULL
	DROP VIEW DITStaging.XMLBatchViewInsertWithoutBatchNumber
GO

--WFSScriptProcessorViewCreate
CREATE VIEW DITStaging.XMLBatchViewInsertWithoutBatchNumber
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/04/2014
*
* Purpose: View used to insert records into XMLBatch that do not have a
*		BatchID and BatchNumber defined.
*
*
* Modification History
* 04/04/2014 WI 135416 JPB	Created
* 07/09/2014 WI 152816 CMC  Select SourceBatchID
* 10/22/2014 WI 174914 JPB	Added duplicate detect columns
* 02/11/2015 WI 190140 JPB	Replaced key columns with string version
******************************************************************************/
SELECT
	Batch_Id,
	BatchTrackingID,
	DepositDate,
	BatchDate,
	ImmutableDate,
	SiteBankID,
	SiteClientAccountID,
	BatchCueID,
	BatchSource,
	BatchPaymentType,
	BatchSiteCode,
	BatchID,
	SourceBatchID,
	FileHashChecker,
	ABA,
	DDA,
	FileHash,
	FileSignature
FROM
	DITStaging.XMLBatch;

