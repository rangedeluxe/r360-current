--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorDataTypeName DuplicateBatchList
--WFSScriptProcessorDataTypeCreate
CREATE TYPE DITStaging.DuplicateBatchList AS TABLE
(
	RowID				INT IDENTITY(1,1),
	Batch_Id			BIGINT,
	BatchTrackingID		UNIQUEIDENTIFIER,
	DepositDate			DATE,
	BatchDate			DATE,
	ImmutableDate		DATE,
	SourceBatchID		BIGINT,
	SiteBankID			INT,
	SiteClientAccountID INT,
	BatchSource			VARCHAR(30),
	BatchPaymentType	VARCHAR(30),
	BatchSiteCode		INT
);
