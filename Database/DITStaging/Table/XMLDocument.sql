--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLDocument
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLDocument') IS NOT NULL
       DROP TABLE DITStaging.XMLDocument;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52867 JPB	Created
* 04/05/2013 WI 92160 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
* 10/05/2016 PT 127613917 JPB	Added IsCorrespondence.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLDocument
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Document_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	BatchSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	DocumentSequence INT NOT NULL,
	IsCorrespondence BIT NOT NULL,
	DocumentDescriptor VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLDocument.IDX_XMLDocument_Batch_Transaction_Document_Id
CREATE CLUSTERED INDEX IDX_XMLDocument_Batch_Transaction_Document_Id ON DITStaging.XMLDocument
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[Document_Id] ASC
);