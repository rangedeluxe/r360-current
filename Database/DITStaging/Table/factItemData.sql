--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTable factItemData
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factItemData') IS NOT NULL
       DROP TABLE DITStaging.factItemData
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2011
*
* Purpose: Copy of RecHubData.factItemData minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50804 JPB	Created
* 07/23/2012 CR 54482 JPB	Added BatchNumber.
* 04/08/2013 WI 92074 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
*							Changed ModifiedBy from 32 to 128.
* 06/24/2014 WI 135336 JPB	Changed BatchID, Added SourceBatchID.
* 03/20/2015 WI 196955 JPB	Added BatchSourceKey.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factItemData
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	ItemDataSetupFieldKey INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataValueDateTime DATETIME NULL,
	DataValueMoney MONEY NULL,
	DataValueInteger INT NULL,
	DataValueBit BIT NULL,
	ModifiedBy VARCHAR(128) NOT NULL,
	DataValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
