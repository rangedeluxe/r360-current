--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName factBatchSummary
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factBatchSummary') IS NOT NULL
       DROP TABLE DITStaging.factBatchSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 02/24/2012
*
* Purpose: Copy of RecHubData.factBatchSummary minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50799 JPB	Created
* 07/23/2012 CR 54475 JPB	Added BatchNumber and BatchCueID.
* 04/08/2013 WI 92069 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
* 06/24/2014 WI 135331 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID
* 10/08/2014 WI 170253 JPB	Added BatchExceptionStatusKey
* 06/22/2018 WI 158492981 JPB	Default OrganizationKey to -1.
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factBatchSummary
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL
		CONSTRAINT DF_factBatchSummary_OrganizationKey DEFAULT(-1),
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchExceptionStatusKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	DepositStatusKey INT NOT NULL,
	TransactionCount INT NOT NULL,
	CheckCount INT NOT NULL,
	ScannedCheckCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	CheckTotal MONEY NOT NULL,
	StubTotal MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	DepositDDA VARCHAR(40) NULL
);
--WFSScriptProcessorTableProperties
