--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName factChecks
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factChecks') IS NOT NULL
       DROP TABLE DITStaging.factChecks
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2012
*
* Purpose: Copy of RecHubData.factChecks minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50800 JPB	Created
* 07/23/2012 CR 54476 JPB	Added BatchNumber and BatchCueID.
* 04/08/2013 WI 92070 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
*							Added NumericRoutingNumber.
*							Added RoutingNumber.
*							Added Account.
*							Added RemitterName.
*							Dropped RemitterKey.
* 02/28/2014 WI 131191 JBS	Added DDAKey column. 
* 06/24/2014 WI 135332 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factChecks
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	CheckSequence INT NOT NULL,
	NumericRoutingNumber BIGINT NOT NULL,
	NumericSerial BIGINT NOT NULL,
	Amount MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	RoutingNumber VARCHAR(30) NOT NULL,
	Account VARCHAR(30) NOT NULL,
	TransactionCode VARCHAR(30) NULL,
	Serial VARCHAR(30) NULL,
	RemitterName VARCHAR(60) NULL,
	DDAKey	INT NULL
);
--WFSScriptProcessorTableProperties
