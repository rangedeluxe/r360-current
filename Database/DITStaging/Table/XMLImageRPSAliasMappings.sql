--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLImageRPSAliasMappings
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLImageRPSAliasMappings') IS NOT NULL
       DROP TABLE DITStaging.XMLImageRPSAliasMappings;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/24/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/24/2012 CR 53037 JPB	Created
* 04/05/2013 WI 92175 JBS	Update to 2.0 release. Change schema name to DITStaging
* 10/24/2014 WI 171460 CMC	Adding FieldLength
* 07/20/2015 WI 224997 JPB	Added BatchSource,defaulted FieldLength.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLImageRPSAliasMappings
(
	ClientGroup_Id BIGINT NOT NULL,
	Client_Id BIGINT NOT NULL,
	ExtractType TINYINT NOT NULL,
	DocType TINYINT NOT NULL,
	DataType SMALLINT NOT NULL,
	FieldType CHAR(1) NOT NULL,
	AliasName VARCHAR(256) NOT NULL,
	BatchSource	VARCHAR(30) NOT NULL,
	FieldLength	TINYINT NULL
		CONSTRAINT DF_XMLImageRPSAliasMappings_FieldLength DEFAULT 0
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLImageRPSAliasMappings.IDX_XMLImageRPSAliasMappings_ClientGroup_Id
CREATE CLUSTERED INDEX IDX_XMLImageRPSAliasMappings_ClientGroup_Id ON DITStaging.XMLImageRPSAliasMappings
(
	[ClientGroup_Id] ASC
);