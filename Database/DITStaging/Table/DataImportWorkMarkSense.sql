--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkMarkSense
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkMarkSense') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkMarkSense
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2014-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 01/30/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.  This is a helper table 
*			to map what DE columns have the MarkSense value turned on and sets the value
*			in the factStubs table
*		   
*
* Modification History
* 07/15/2014 WI 153770 JBS	Created
* 02/07/2015 WI 188795 JPB	Added Transaction_Id
* 06/22/2018 WI 158492981 JPB	Replaced Batch_Id with BatchID, removed Transaction_Id
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkMarkSense
(
	BatchID			BIGINT NOT NULL,
	TransactionID	INT NOT NULL,
	MarkSense		BIT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkMarkSense.IDX_DataImportWorkMarkSense_BatchTransactionID
CREATE CLUSTERED INDEX IDX_DataImportWorkMarkSense_BatchTransactionID ON DITStaging.DataImportWorkMarkSense
(
	BatchID ASC,
	TransactionID ASC
);