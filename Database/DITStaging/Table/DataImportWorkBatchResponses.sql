--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkBatchResponses
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkBatchResponses') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkBatchResponses
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/27/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* ResponseStatus:
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*	
*
* Modification History
* 01/27/2012 CR 50808 JPB	Created
* 04/08/2013 WI 92063 JPB	Update to 2.0 release. Move to DITStaging Schema
* 02/05/2015 WI 188119 JPB	Added Retry.
* 02/09/2015 WI	262702 JPB	Added Batch_Id as nullable.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkBatchResponses
(
	Batch_Id BIGINT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseStatus TINYINT NOT NULL, 
	Retry BIT NOT NULL
		CONSTRAINT DF_DataImportWorkBatchResponses_Retry DEFAULT 0,
	ResultsMessage VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkResponses.IDX_DataImportWorkResponses_DataImportQueueIDResponseStatus
CREATE INDEX IDX_DataImportWorkBatchResponses_DataImportQueueIDResponseType ON DITStaging.DataImportWorkBatchResponses 
(
	BatchTrackingID,
	ResponseStatus
);
