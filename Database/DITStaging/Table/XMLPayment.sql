--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLPayment
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLPayment') IS NOT NULL
       DROP TABLE DITStaging.XMLPayment;
GO--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52876 JPB	Created
* 04/09/2013 WI 92184 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
* 03/07/2014 WI 132137 JBS	Added ABA and DDA to Payment level.  
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLPayment
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Payment_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	BatchSequence INT NOT NULL,
	CheckSequence INT NULL,
	Amount MONEY NOT NULL,
	RT VARCHAR(30) NOT NULL,
	Account VARCHAR(30) NOT NULL,
	Serial VARCHAR(30) NOT NULL,
	TransactionCode VARCHAR(30) NOT NULL,
	RemitterName VARCHAR(60) NOT NULL,
	ABA		VARCHAR(10) NOT NULL,
	DDA		VARCHAR(35)	NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLPayment.IDX_XMLPayment_Batch_Transaction_Payment_Id
CREATE CLUSTERED INDEX IDX_XMLPayment_Batch_Transaction_Payment_Id ON DITStaging.XMLPayment
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[Payment_Id] ASC
);