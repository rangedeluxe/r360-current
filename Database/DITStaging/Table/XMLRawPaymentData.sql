--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLRawPaymentData
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLRawPaymentData') IS NOT NULL
       DROP TABLE DITStaging.XMLRawPaymentData;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/06/2014
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/06/2014 WI 140154 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLRawPaymentData
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Payment_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	RawDataSequence INT NOT NULL,
	RawDataPart SMALLINT NOT NULL,
	RawData VARCHAR(128)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLRawPaymentData.IDX_XMLRawPaymentData_Batch_Transaction_Payment_Id
CREATE CLUSTERED INDEX IDX_XMLRawPaymentData_Batch_Transaction_Payment_Id ON DITStaging.XMLRawPaymentData
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[Payment_Id] ASC
);