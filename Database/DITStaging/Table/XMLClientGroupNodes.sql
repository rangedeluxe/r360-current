--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLClientGroupNodes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/11/2012 CR 52736 JPB	Created
* 04/04/2013 WI 92154 JBS	Update to 2.0 release. change schema to DITStaging
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLClientGroupNodes
(
	ClientGroup_Id BIGINT NOT NULL,
	xmlString VARCHAR(MAX)
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLClientGroupNodes.IDX_XMLClientGroupNodes_ClientGroup_Id
CREATE CLUSTERED INDEX IDX_XMLClientGroupNodes_ClientGroup_Id ON DITStaging.XMLClientGroupNodes
(
	[ClientGroup_Id] ASC
)