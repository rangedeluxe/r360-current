--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataEntryDetails
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataEntryDetails') IS NOT NULL
       DROP TABLE DITStaging.DataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/01/2015
*
* Purpose: Staging table for data entry details work.
*		   
*
* Modification History
* 02/01/2015 WI 187609 JPB	Created
* 07/14/2015 WI 223946 JPB	Added WorkgroupDataEntryColumnKey
* 07/28/2015 WI	227003 JPB	Updates for WorkgroupDataEntryColumn
* 10/06/2016 PT 127613917 JPB	Added IsCorrespondence.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataEntryDetails
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	DepositStatus INT NOT NULL,
	WorkgroupDataEntryColumnKey BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	FieldName VARCHAR(256) NOT NULL,
	FieldValue VARCHAR(256) NOT NULL,
	IsCheck BIT NOT NULL,
	MarkSense BIT NOT NULL,
	IsCorrespondence BIT NOT NULL,
	DataType TINYINT NOT NULL
);
--WFSScriptProcessorTableProperties
