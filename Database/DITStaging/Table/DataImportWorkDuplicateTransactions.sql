--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkDuplicateTransactions
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkDuplicateTransactions') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkDuplicateTransactions
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/30/14
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.  This is a helper table 
*			to identify duplicate transactions so they can be removed from the 
*			work flow.
*		   
*
* Modification History
* 10/30/14 WI 176024 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkDuplicateTransactions
(
	Batch_Id		BIGINT NOT NULL,
	Transaction_Id	BIGINT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkDuplicateTransactions.IDX_DataImportWorkDuplicateTransactions_BatchTransaction_Id
CREATE CLUSTERED INDEX IDX_DataImportWorkDuplicateTransactions_BatchTransaction_Id ON DITStaging.DataImportWorkDuplicateTransactions
(
	Batch_id ASC,
	Transaction_Id ASC
);