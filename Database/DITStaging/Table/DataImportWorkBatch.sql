--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkBatch
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkBatch') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkBatch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/25/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*
* Modification History
* 01/25/2012 CR 50807 JPB	Created
* 07/23/2012 CR 54473 JPB	Added BatchNumber and BatchCueID.
* 04/08/2013 WI 92062 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed BankID to SiteBankID.
*							Renamed ClientID to SiteClientAccountID.
*							Renamed ProcessingDate to ImmutableDate.
* 02/28/2014 WI 131194 JBS	Added DDAKey column.  Removing column
* 07/03/2014 WI 151871 CMC  Changed BatchID, SourceBatchKey, added SourceBatchID
* 07/03/2014 WI 175793 JPB	Added index for eletronic batch processing.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkBatch
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	Batch_Id BIGINT NOT NULL,
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_DataImportWorkBatch_ResponseStatus DEFAULT 255,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	DepositDate DATETIME NOT NULL,
	ImmutableDate DATETIME NOT NULL,
	SourceProcessingDate DATETIME NOT NULL,
	BatchCueID int NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey INT NOT NULL,
	BatchSiteCode INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkBatch.IDX_DataImportWorkBatch_DepositDateImmutableDateBankCustomeOrganizationAccountKeyBatchID
CREATE CLUSTERED INDEX IDX_DataImportWorkBatch_DepositDateImmutableDateBankCustomeOrganizationAccountKeyBatchID ON DITStaging.DataImportWorkBatch
(
	[DepositDateKey] ASC,
	[ImmutableDateKey] ASC,
	[BankKey] ASC,
	[OrganizationKey] ASC,
	[ClientAccountKey] ASC,
	[BatchID] ASC
);
--WI 175793
--WFSScriptProcessorIndex DITStaging.DataImportWorkBatch.IDX_DataImportWorkBatch_Batch_IdBatchTrackingID
CREATE INDEX IDX_DataImportWorkBatch_Batch_IdBatchTrackingID ON DITStaging.DataImportWorkBatch
(
	Batch_Id,
	BatchTrackingID
);
