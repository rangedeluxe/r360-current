--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkBatchStartup
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkBatchStartup') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkBatchStartup
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/25/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 01/25/2012 CR 50809 JPB	Created
* 04/08/2013 WI 92064 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed BankID to SiteBankID.
*							Renamed ClientID to SiteClientAccountID.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkBatchStartup
(
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL	
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkBatchStartup.IDX_DataImportWorkBatchStartup_SiteBankSiteClientAccountID
CREATE CLUSTERED INDEX IDX_DataImportWorkBatchStartup_SiteBankSiteClientAccountID ON DITStaging.DataImportWorkBatchStartup
(
	[SiteBankID] ASC,
	[SiteClientAccountID] ASC
);
