--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkDataEntryColumnKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkDataEntryColumnKeys') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkDataEntryColumnKeys
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/30/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 01/30/2012 CR 50812 JPB	Created
* 05/18/2012 CR 53018 JPB	Replaced BatchTrackingID with LockboxKey.
*							Added SiteBankID/SiteLockboxID.
* 04/08/2013 WI 92067 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed BankID to SiteBankID.
*							Renamed ClientID to SiteClientAccountID.
*							Renamed LockboxKey to ClientAccountKey.
* 01/13/2014 WI 125915 JBS	Update column FieldName from 32 to 128
* 07/10/2014 WI 153058 JBS	Add MarkSense Column
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkDataEntryColumnKeys
(
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	ClientAccountKey	INT NOT NULL,
	DataEntryColumnKey	INT NOT NULL,
	DocumentsSource		TINYINT NOT NULL,
	MarkSense		TINYINT NOT NULL,
	TableType		TINYINT NOT NULL,
	DataType		SMALLINT NOT NULL,
	FieldName		VARCHAR(128) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkDataEntryColumnKeys.IDX_DataImportWorkDataEntryColumnKeys_ClientAccountIDDataEntryColumnKeyFieldName
CREATE CLUSTERED INDEX IDX_DataImportWorkDataEntryColumnKeys_ClientAccountIDDataEntryColumnKeyFieldName ON DITStaging.DataImportWorkDataEntryColumnKeys
(
	ClientAccountKey ASC,
	DataEntryColumnKey ASC,
	FieldName ASC
);