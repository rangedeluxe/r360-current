IF OBJECT_ID('DITStaging.WorkDataEntryWideChecks') IS NOT NULL
	DROP TABLE DITStaging.WorkDataEntryWideChecks
GO

/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets. These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details). All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/11/2019
*
* Purpose: Workspace for the DIT SSIS package - to populate dynamic columns
*	for AdvancedSearch (a.k.a. the advanced search wide table)
*
*
* Modification History
* 11/11/2019 R360-31442	MGE Created
******************************************************************************/

CREATE TABLE DITStaging.WorkDataEntryWideChecks(
	[BankKey] [int] NOT NULL,
	[ClientAccountKey] [int] NOT NULL,
	[DepositDateKey] [int] NOT NULL,
	[ImmutableDateKey] [int] NOT NULL,
	[SourceProcessingDateKey] [int] NOT NULL,
	[BatchID] [bigint] NOT NULL,
	[TransactionID] [int] NOT NULL,
	[BatchSequence] [int] NOT NULL
) ON [PRIMARY]
GO

RAISERROR('Updating DITStaging.WorkDataEntryWideChecks table properties.',10,1) WITH NOWAIT
	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'DITStaging', 'TABLE', 'WorkDataEntryWideChecks', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'DITStaging',
			@level1type = N'TABLE',
			@level1name = N'WorkDataEntryWideChecks';		

	EXEC sys.sp_addextendedproperty 
	@name = N'Table_Description',
	@level0type = N'SCHEMA',@level0name = DITStaging,
	@level1type = N'TABLE',@level1name = WorkDataEntryWideChecks,
	@value = N'/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2019 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corporation. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets. These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details). All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/11/2019
*
* Purpose: Workspace for the DIT SSIS package - to populate dynamic columns
*	for AdvancedSearch (a.k.a. the advanced search wide table)
*
*
* Modification History
* 11/11/2019 R360-31442	MGE Created
******************************************************************************/';
