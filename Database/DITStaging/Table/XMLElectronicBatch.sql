--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLElectronicBatch
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLElectronicBatch') IS NOT NULL
       DROP TABLE DITStaging.XMLElectronicBatch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/10/20152
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 02/10/2015 WI 190136 JPB	Created
* 03/04/2016 WI 267949 JPB	Added FileHashCheck
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLElectronicBatch
(
	Batch_Id BIGINT NOT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	DepositDate DATETIME NOT NULL,
	BatchDate DATETIME NOT NULL,
	ImmutableDate DATETIME NOT NULL,
	BatchID BIGINT NOT NULL
		CONSTRAINT DF_XMLElectronicBatch_BatchID DEFAULT (NEXT VALUE FOR RecHubSystem.ReceivablesBatchID),
	SourceBatchID BIGINT NOT NULL
		CONSTRAINT DF_XMLElectronicBatch_SourceBatchID DEFAULT (NEXT VALUE FOR RecHubSystem.SourceBatchID),
	BatchNumber INT NOT NULL
		CONSTRAINT DF_XMLElectronicBatch_BatchNumber DEFAULT (NEXT VALUE FOR RecHubSystem.BatchNumber),
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchCueID INT NOT NULL,
	BatchSource VARCHAR(30) NOT NULL,
	BatchPaymentType VARCHAR(30) NOT NULL,
	BatchSiteCode INT NOT NULL,
	FileHashChecker BIGINT NULL,
	ABA VARCHAR(10) NULL,
	DDA	VARCHAR(35)	NULL,
	FileHash VARCHAR(40) NULL,
	FileSignature VARCHAR(55) NULL
);
--WFSScriptProcessorTableProperties