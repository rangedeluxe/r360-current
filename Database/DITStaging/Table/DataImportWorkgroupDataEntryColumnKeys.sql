--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkgroupDataEntryColumnKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkgroupDataEntryColumnKeys') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkgroupDataEntryColumnKeys
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/28/2015
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 07/28/2015 WI 227000 JPB	Created
* 05/01/2019 R360-16298 MGE Added MixedCaseFieldName to support adding rows to factTransactionDetails
********************************************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkgroupDataEntryColumnKeys
(
	WorkgroupDataEntryColumnKey BIGINT NOT NULL,
	SiteBankID					INT NOT NULL,
	SiteClientAccountID			INT NOT NULL,
	ClientAccountKey			INT NOT NULL,
	BatchSourceKey				SMALLINT NOT NULL,
	IsCheck						BIT NOT NULL,
	MarkSense					BIT NOT NULL,
	DataType					TINYINT NOT NULL,
	FieldName					NVARCHAR(256) NOT NULL,
	MixedCaseFieldName			NVARCHAR(256) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkgroupDataEntryColumnKeys.IDX_DataImportWorkgroupDataEntryColumnKeys_ClientAccountWorkgroupDataEntryColumnKeyFieldName
CREATE CLUSTERED INDEX IDX_DataImportWorkgroupDataEntryColumnKeys_ClientAccountWorkgroupDataEntryColumnKeyFieldName ON DITStaging.DataImportWorkgroupDataEntryColumnKeys
(
	ClientAccountKey ASC,
	WorkgroupDataEntryColumnKey ASC,
	FieldName ASC
);