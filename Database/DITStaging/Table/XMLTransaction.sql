--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLTransaction
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLTransaction') IS NOT NULL
       DROP TABLE DITStaging.XMLTransaction;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52880 JPB	Created
* 04/09/2013 WI 92192 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
* 10/22/2014 WI 174901 JPB Added duplicate detect columns
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLTransaction
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TransactionSequence INT NOT NULL,
	TransactionHashChecker BIGINT NULL,
	TransactionHash VARCHAR(40) NULL,
	TransactionSignature VARCHAR(65) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLTransaction.IDX_XMLTransaction_Batch_Transaction_Id
CREATE CLUSTERED INDEX IDX_XMLTransaction_Batch_Transaction_Id ON DITStaging.XMLTransaction
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC
);
