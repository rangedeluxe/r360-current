--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLDataEntryColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLDataEntryColumns') IS NOT NULL
       DROP TABLE DITStaging.XMLDataEntryColumns
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/11/2012 CR 52740 JPB	Created
* 04/04/2013 WI 92158 JBS	Update to 2.0 release. Change schema to DITStaging
* 01/13/2014 WI 119081 JBS	Expand columns for 2.1 DIT changes. All were previously 32
* 07/20/2015 WI 224325 JPB	Added BatchSource, defaulted FieldLength
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLDataEntryColumns
(
	ClientGroup_Id	BIGINT NOT NULL,
	Client_Id		BIGINT NOT NULL,
	FieldLength		TINYINT NOT NULL
		CONSTRAINT DF_XMLDataEntryColumns_FieldLength DEFAULT 0,
	DataType		SMALLINT NOT NULL,
	ScreenOrder		TINYINT NOT NULL,
	MarkSense		TINYINT NOT NULL,
	DisplayGroup	VARCHAR(64) NOT NULL,  
	FieldName		VARCHAR(256) NOT NULL,		
	DisplayName		VARCHAR(256) NOT NULL,
	BatchSource		VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLDataEntryColumns.IDX_XMLDataEntryColumns_ClientGroup_Id
CREATE CLUSTERED INDEX IDX_XMLDataEntryColumns_ClientGroup_Id ON DITStaging.XMLDataEntryColumns
(
	[ClientGroup_Id] ASC
);