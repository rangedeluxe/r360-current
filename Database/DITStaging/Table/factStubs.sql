--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName factStubs
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factStubs') IS NOT NULL
       DROP TABLE DITStaging.factStubs
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2012
*
* Purpose: Copy of RecHubData.factBatchData minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50805 JPB	Created
* 07/23/2012 CR 54478 JPB	Added BatchNumber and BatchCueID.
* 04/08/2013 WI 92077 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
* 04/09/2014 WI 135337 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
* 08/07/2014 WI 157624 JPB	Changed DocumentBatchSequence to NULLABLE.
* 10/04/2016 PT 127613917 JPB	Added IsCorrespondence.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factStubs
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType TINYINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	SequenceWithinTransaction INT NOT NULL,
	BatchSequence INT NOT NULL,
	StubSequence INT NOT NULL,
	IsCorrespondence BIT NOT NULL,
	DocumentBatchSequence INT NULL,
	IsOMRDetected BIT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	Amount MONEY NULL,
	AccountNumber VARCHAR(80) NULL
);
--WFSScriptProcessorTableProperties
