--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLGhostDocument
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLGhostDocument') IS NOT NULL
       DROP TABLE DITStaging.XMLGhostDocument;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2016
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 07/08/2016 WI 291223 JPB	Created
* 10/05/2016 PT 127613917 JPB	Added BatchSequence, IsCorrespondence.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLGhostDocument
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	GhostDocument_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	BatchSequence INT NOT NULL,
	IsCorrespondence BIT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLGhostDocument.IDX_XMLGhostDocument_BatchTransactionDocument_Id
CREATE CLUSTERED INDEX IDX_XMLGhostDocument_BatchTransactionDocument_Id ON DITStaging.XMLGhostDocument
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[GhostDocument_Id] ASC
);