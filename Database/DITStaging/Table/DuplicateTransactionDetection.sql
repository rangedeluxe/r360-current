--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DuplicateTransactionDetection
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DuplicateTransactionDetection') IS NOT NULL
       DROP TABLE DITStaging.DuplicateTransactionDetection
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 11/03/2014
*
* Purpose: Working space of files that have been imported.
*
* Modification History
* 11/03/2014 WI 175919 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DuplicateTransactionDetection
(
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	TransactionHashChecker BIGINT NOT NULL,
	TransactionHash VARCHAR(40) NOT NULL,
	TransactionSignature VARCHAR(65) NOT NULL
);
--WFSScriptProcessorTableProperties
