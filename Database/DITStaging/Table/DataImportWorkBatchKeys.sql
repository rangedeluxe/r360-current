--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkBatchKeys
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkBatchKeys') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkBatchKeys
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/11/2015
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 02/11/2015 WI 190135 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkBatchKeys
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	Batch_Id BIGINT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	DepositDate DATETIME NOT NULL,
	ImmutableDate DATETIME NOT NULL,
	SourceProcessingDate DATETIME NOT NULL,
	BatchCueID INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey INT NOT NULL,
	BatchSiteCode INT NOT NULL
);
--WFSScriptProcessorTableProperties
