--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLGhostDocumentData
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLGhostDocumentData') IS NOT NULL
       DROP TABLE DITStaging.XMLGhostDocumentData
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/30/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/30/2012 CR 52872 JPB	Created
* 04/09/2013 WI 92169 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLGhostDocumentData
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	GhostDocument_Id BIGINT NOT NULL,
	RemittanceDataRecord_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	FieldName VARCHAR(32) NOT NULL,
	FieldValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLGhostDocumentData.IDX_XMLGhostDocumentData_Batch_Transaction_GhostDocument_Id
CREATE CLUSTERED INDEX IDX_XMLGhostDocumentData_Batch_Transaction_GhostDocument_Id ON DITStaging.XMLGhostDocumentData
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[GhostDocument_Id] ASC
);