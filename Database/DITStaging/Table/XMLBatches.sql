--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLBatches
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLBatches') IS NOT NULL
       DROP TABLE DITStaging.XMLBatches; 

--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52865 JPB	Created
* 04/05/2013 WI 92150 JBS	Update to 2.0 release.  Change schema name to DITStaging
* 07/02/2018 PT 15779079 CMC Change for Json
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLBatches
(
	RowID INT NOT NULL IDENTITY(1,1),
	Batch_Id BIGINT NOT NULL,
	xmlBatch XML,
	xmlString VARCHAR(MAX),
	jsonBatch VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLBatches.IDX_XMLBatches_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLBatches_Batch_Id ON DITStaging.XMLBatches
(
	[Batch_Id] ASC
);