--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLBank
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLBank') IS NOT NULL
       DROP TABLE DITStaging.XMLBank
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/11/2012 CR 52732 JPB	Created
* 04/04/2013 WI 92145 JBS	Update to 2.0 release. Move to DITStaging Schema
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLBank
(
	ClientGroup_Id BIGINT NOT NULL,
	BankID INT NOT NULL,
	BankName VARCHAR(25) NULL,
	ABA VARCHAR(10) NULL
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLBank.IDX_XMLBank_ClientGroup_Id
CREATE CLUSTERED INDEX IDX_XMLBank_ClientGroup_Id ON DITStaging.XMLBank
(
	[ClientGroup_Id] ASC
)