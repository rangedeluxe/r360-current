--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName DataImportWorkClientSetupResponses
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.DataImportWorkClientSetupResponses') IS NOT NULL
       DROP TABLE DITStaging.DataImportWorkClientSetupResponses;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/27/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* ResponseStatus
*	0 = Success, batch imported
*	1 = Rejected, batch was not imported
*	2 = Warning, batch imported with warnings
*	255 = Status not set
*
* Modification History
* 01/27/2012 CR 50811 JPB	Created
* 04/04/2013 WI 92066 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.DataImportWorkClientSetupResponses
(
	ClientTrackingID UNIQUEIDENTIFIER NOT NULL,
	ResponseStatus TINYINT NOT NULL, 
	ResultsMessage VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.DataImportWorkClientSetupResponses.IDX_DataImportWorkClientSetupResponses_ClientTrackingIDResponseStatus
CREATE INDEX IDX_DataImportWorkClientSetupResponses_ClientTrackingIDResponseType ON DITStaging.DataImportWorkClientSetupResponses 
(
	ClientTrackingID,
	ResponseStatus
);
