--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName factDataEntryDetails
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factDataEntryDetails') IS NOT NULL
       DROP TABLE DITStaging.factDataEntryDetails
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/24/2012
*
* Purpose: Copy of RecHubData.factDataEntryDetails minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50801 JPB	Created
* 07/23/2012 CR 54480 JPB	Added BatchNumber.
* 04/08/2013 WI 92071 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
* 06/24/2014 WI 135333 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
* 07/06/2015 WI 221800 JBS	Add WorkGroupDataEntryColumnKey 
* 08/18/2015 WI 227004 JPB	Removed DataEntryColumnKey
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factDataEntryDetails
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL,
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	DepositStatus INT NOT NULL,
	WorkGroupDataEntryColumnKey BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
