--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLBatch
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLBatch') IS NOT NULL
       DROP TABLE DITStaging.XMLBatch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52969 JPB	Created
* 07/23/2012 CR 55617 JPB	Added BatchNumber and BatchCueID.
* 04/05/2013 WI 92147 JPB	Updated to 2.0.
*						Moved to DITStaging schema.
*						Renamed ProcessingDate to ImmutableDate.
*						Renamed BankID to SiteBankID.
*						Renamed ClientID to SiteClientAccountID.
* 03/30/2014 WI 134602 JPB	Added ABA/DDA columns.
*						Default sequence number for BachID/BatchNumber.
* 06/25/2014 WI 150348 CMC  
*                      Changed BatchID, SourceBatchKey, added SourceBatchID
* 10/22/2014 WI 174900 JPB	Added duplicate detect columns
* 12/16/2014 WI 182051 JBS	Change SourceBatchID to use Sequence SourceBatchID
*							Change BatchNumber to use Sequence Batchnumber
* 02/10/2015 WI 190137 JPB	Changed BatchSourceKey to BatchSource,
						BatchPaymentTypeKey to BatchPaymentTYpe
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLBatch
(
	Batch_Id BIGINT NOT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	DepositDate DATETIME NOT NULL,
	BatchDate DATETIME NOT NULL,
	ImmutableDate DATETIME NOT NULL,
	BatchID BIGINT NOT NULL
		CONSTRAINT DF_XMLBatch_BatchID DEFAULT (NEXT VALUE FOR RecHubSystem.ReceivablesBatchID),
	SourceBatchID BIGINT NOT NULL
		CONSTRAINT DF_XMLBatch_SourceBatchID DEFAULT (NEXT VALUE FOR RecHubSystem.SourceBatchID),
	BatchNumber INT NOT NULL
		CONSTRAINT DF_XMLBatch_BatchNumber DEFAULT (NEXT VALUE FOR RecHubSystem.BatchNumber),
	SiteBankID INT NOT NULL,
	SiteClientAccountID INT NOT NULL,
	BatchCueID INT NOT NULL,
	BatchSource VARCHAR(30) NOT NULL,
	BatchPaymentType VARCHAR(30) NOT NULL,
	BatchSiteCode INT NOT NULL,
	FileHashChecker BIGINT NULL,
	ABA VARCHAR(10) NULL,
	DDA	VARCHAR(35)	NULL,
	FileHash VARCHAR(40) NULL,
	FileSignature VARCHAR(55) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLBatch.IDX_XMLBatch_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLBatch_Batch_Id ON DITStaging.XMLBatch
(
	[Batch_Id] ASC
);
