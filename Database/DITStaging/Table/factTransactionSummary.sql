--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTable factTransactionSummary
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.factTransactionSummary') IS NOT NULL
       DROP TABLE DITStaging.factTransactionSummary
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright � 2012-2018 Deluxe Corporation. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2018 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 02/24/2012
*
* Purpose: Copy of RecHubData.factBatchData minus FKs and indexes.
*		   
*
* Modification History
* 02/24/2012 CR 50806 JPB	Created
* 07/23/2012 CR 54479 JPB	Added BatchNumber and BatchCueID.
* 04/08/2013 WI 92078 JPB	Update to 2.0 release. Move to DITStaging Schema.
*							Renamed CustomerKey to OrganizationKey.
*							Renamed LockboxKey to ClientAccountKey.
*							Renamed ProcessingDateKey to ImmutableDateKey.
*							Renamed LoadDate to CreationDate.
*							Added StubTotal.
*							Dropped DESetupID.
* 06/05/2014 WI 146077 KLC	Added Exception column
* 06/24/2014 WI 135338 JPB	Changed BatchID,SourceBatchKey, Added SourceBatchID.
* 06/22/2018 WI 158492981 JPB	Default OrganizationKey to -1.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.factTransactionSummary
(
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL,
	IsDeleted BIT NOT NULL,
	BankKey INT NOT NULL,
	OrganizationKey INT NOT NULL
		CONSTRAINT DF_factTransactionSummary_OrganizationKey DEFAULT(-1),
	ClientAccountKey INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceProcessingDateKey INT NOT NULL,
	BatchID BIGINT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	BatchSourceKey SMALLINT NOT NULL,
	BatchPaymentTypeKey TINYINT NOT NULL,
	BatchCueID int NOT NULL,
	DepositStatus INT NOT NULL,
	SystemType tinyint NOT NULL,
	TransactionID INT NOT NULL,
	TxnSequence INT NOT NULL,
	CheckCount INT NOT NULL,
	ScannedCheckCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	OMRCount INT NOT NULL,
	CheckTotal MONEY NOT NULL,
	StubTotal MONEY NOT NULL,
	CreationDate DATETIME NOT NULL,
	ModificationDate DATETIME NOT NULL,
	BatchSiteCode INT NULL,
	Exception BIT NULL
);
--WFSScriptProcessorTableProperties
