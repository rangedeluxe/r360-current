--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLPaymentData
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLPaymentData') IS NOT NULL
       DROP TABLE DITStaging.XMLPaymentData;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/20/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/20/2012 CR 52877 JPB	Created
* 04/09/2013 WI 92185 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLPaymentData
(
	Batch_Id BIGINT NOT NULL,
	Transaction_Id BIGINT NOT NULL,
	Payment_Id BIGINT NOT NULL,
	TransactionID INT NOT NULL,
	BatchSequence INT NOT NULL,
	FieldName VARCHAR(32) NOT NULL,
	FieldValue VARCHAR(256) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLPaymentData.IDX_XMLPaymentData_Batch_Transaction_Payment_Id
CREATE CLUSTERED INDEX IDX_XMLPaymentData_Batch_Transaction_Payment_Id ON DITStaging.XMLPaymentData
(
	[Batch_Id] ASC,
	[Transaction_Id] ASC,
	[Payment_Id] ASC
);