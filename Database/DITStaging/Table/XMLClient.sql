--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLClient
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLClient') IS NOT NULL
       DROP TABLE DITStaging.XMLClient
GO
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/11/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/11/2012 CR 52734 JPB	Created
* 04/04/2013 WI 92152 JBS	Update to 2.0 release. Change schema to DITStaging
* 01/13/2014 WI 119083 JBS	Increase ShortName from 11 to VC(20).
* 02/11/2014 WI 129236 JBS	Add DataRetentionDays and ImageRetentionDays columns
* 05/12/2014 WI 140804 JBS  Make DataRetentionDays and ImageRetentionDays columns nullable
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLClient
(
	ClientGroup_Id	BIGINT NOT NULL,
	Client_Id		BIGINT NOT NULL,
	BankID			INT NOT NULL,
	ClientGroupID	INT NOT NULL,
	SiteCode		INT NOT NULL,
	ClientID		INT NOT NULL,
	DataRetentionDays SMALLINT NULL,
	ImageRetentionDays SMALLINT NULL,
	ShortName		VARCHAR(20) NOT NULL,
	LongName		VARCHAR(40) NULL,
	DDA				VARCHAR(40) NULL,
	POBox			VARCHAR(32) NULL,
	OnlineColorMode TINYINT NOT NULL
)
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLClient.IDX_XMLClient_ClientGroup_Id
CREATE CLUSTERED INDEX IDX_XMLClient_ClientGroup_Id ON DITStaging.XMLClient
(
	[ClientGroup_Id] ASC
)