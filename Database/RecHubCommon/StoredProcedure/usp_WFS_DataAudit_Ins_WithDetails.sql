--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorStoredProcedureName usp_WFS_DataAudit_Ins_WithDetails
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails') IS NOT NULL
       DROP PROCEDURE RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubCommon.usp_WFS_DataAudit_Ins_WithDetails 
(
	@parmApplicationName		VARCHAR(256),
	@parmAuditType				VARCHAR(10),
	@parmUserID					INT = 0,
	@parmSchemaName				VARCHAR(128),
	@parmTableName				VARCHAR(128),
	@parmColumnName				VARCHAR(128),
	@parmAuditValue				VARCHAR(256),
	@parmAuditColumns			RecHubCommon.AuditValueChangeTable READONLY,
	@parmAuditMessage			VARCHAR(MAX) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/19/2013
*
* Purpose: Insert an audit event into the factDataAuditSummary, 
*	factDataAuditDetails and factDataAuditMessages.
*
* NOTES: This is a multi step process, calling several stored procedures and
*	inserting into multiple tables. The stored procedure processes a 'list' of
*	columns that were changed via the RecHubCommon.AuditValueChangeTable table
*	that was passed in. 
*
* Modification History
* 05/19/2013 WI 102830 JPB	Created
* 10/01/2014 WI 169037 KLC	Updated to handle transactions properly when caller
*							 already has transaction open and dynamically
*							 determine @parmAuditColumns row ids for when
*							 table variable is reused
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @AuditApplicationKey INT,	/* lookup from dimAuditApplications */
		@AuditColumnKey INT,		/* lookup from dimAuditColumns */
		@AuditDateKey INT,
		@AuditKey UNIQUEIDENTIFIER,
		@ColumnName VARCHAR(128),
		@NewValue VARCHAR(256),
		@OldValue VARCHAR(256),
		@CurRowID INT,
		@EndRowID INT;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	/* Set the audit date key and audit key */
	SELECT
		@AuditDateKey = CAST(CONVERT(VARCHAR,GETDATE(),112) AS INT),
		@AuditKey = NEWID();

	/* First step is to get the AuditApplicationKey */
	EXEC RecHubAudit.usp_dimAuditApplications_Get @parmApplicationName, @AuditApplicationKey OUT;
	/* Second step is to get the AuditColumnKey */
	EXEC RecHubAudit.usp_dimAuditColumns_Get @parmSchemaName, @parmTableName, @parmColumnName, @AuditColumnKey OUT;

	SELECT	@CurRowID = MIN(RowID),
			@EndRowID = MIN(RowID) + Count(*) FROM @parmAuditColumns;

	/* Open a transaction if necessary and loop through the table that was passed in. */
	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	/* Insert a summary record. The column is considered the primary key */
	EXEC RecHubAudit.usp_factDataAuditSummary_Ins 	
		@parmAuditDateKey			= @AuditDateKey,
		@parmUserID					= @parmUserID,
		@parmAuditApplicationKey	= @AuditApplicationKey,
		@parmAuditColumnKey			= @AuditColumnKey,
		@parmAuditType				= @parmAuditType,
		@parmAuditValue				= @parmAuditValue,
		@parmAuditKey				= @AuditKey;

	/* Loop through each column record */
	WHILE( @CurRowID <= @EndRowID )
	BEGIN
		SELECT 
			@ColumnName = ColumnName,
			@OldValue = OldValue,
			@NewValue = NewValue
		FROM 
			@parmAuditColumns
		WHERE
			RowID = @CurRowID;

		EXEC RecHubAudit.usp_dimAuditColumns_Get @parmSchemaName, @parmTableName, @ColumnName, @AuditColumnKey OUT;

		EXEC RecHubAudit.usp_factDataAuditDetails_Ins 	
			@parmAuditDateKey			= @AuditDateKey,
			@parmUserID					= @parmUserID,
			@parmAuditApplicationKey	= @AuditApplicationKey,
			@parmAuditColumnKey			= @AuditColumnKey,
			@parmAuditType				= 'UPD',
			@parmNewValue				= @NewValue,
			@parmOldValue				= @OldValue,
			@parmAuditKey				= @AuditKey;

		SET @CurRowID = @CurRowID + 1;
	END

	/* If an audit message was provided insert it */
	IF( @parmAuditMessage IS NOT NULL )
		EXEC RecHubAudit.usp_factDataAuditMessages_Ins 
			@parmAuditDateKey			= @AuditDateKey,
			@parmUserID					= @parmUserID,
			@parmAuditApplicationKey	= @AuditApplicationKey,
			@parmAuditColumnKey			= 0, /* pass in 0 since there were mulitple columns */
			@parmAuditKey				= @AuditKey,
			@parmAuditMessage			= @parmAuditMessage;

	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
