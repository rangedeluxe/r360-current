--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="RecHubUser_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorStoredProcedureName usp_WFS_DataAudit_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubCommon.usp_WFS_DataAudit_Ins') IS NOT NULL
       DROP PROCEDURE RecHubCommon.usp_WFS_DataAudit_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubCommon.usp_WFS_DataAudit_Ins 
(
	@parmApplicationName		VARCHAR(256),
	@parmAuditType				VARCHAR(10),
	@parmUserID					INT = 0,
	@parmSchemaName				VARCHAR(128) = NULL,
	@parmTableName				VARCHAR(128) = NULL,
	@parmColumnName				VARCHAR(128) = NULL,
	@parmAuditValue				VARCHAR(256) = NULL,
	@parmAuditMessage			VARCHAR(MAX) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/18/2013
*
* Purpose: Insert an audit event into the factAuditSummary and factAuditDetails.
*
* NOTES: This is a multi step process, calling several stored procedures and
*	inserting into multiple tables.
*
* Modification History
* 05/18/2013 WI 102829 JPB	Created
* 10/01/2014 WI 169052 KLC	Updated to handle transactions properly when caller
*							 already has transaction open
******************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

DECLARE @AuditApplicationKey INT,	/* lookup from dimAuditApplications */
		@AuditColumnKey INT,		/* lookup from dimAuditColumns */
		@AuditDateKey INT,
		@AuditKey UNIQUEIDENTIFIER;

DECLARE @LocalTransaction BIT = 0;

BEGIN TRY
	/* Set the audit date key and audit key */
	SELECT
		@AuditDateKey = CAST(CONVERT(VARCHAR,GETDATE(),112) AS INT),
		@AuditKey = NEWID();

	/* First step is to get the AuditApplicationKey */
	EXEC RecHubAudit.usp_dimAuditApplications_Get @parmApplicationName, @AuditApplicationKey OUT;

	/* Second step is to get the AuditColumnKey if column provided */
	IF( @parmSchemaName IS NOT NULL OR @parmTableName IS NOT NULL OR @parmColumnName IS NOT NULL )
	BEGIN
		/* If any of the column name parts are included, they must all be included */
		IF( @parmSchemaName IS NULL )
			RAISERROR('Schema name must be provided when including column information',16,1) WITH NOWAIT;
		IF( @parmTableName IS NULL )
			RAISERROR('Table name must be provided when including column information',16,1) WITH NOWAIT;
		IF( @parmColumnName IS NULL )
			RAISERROR('Column name must be provided when including column information',16,1) WITH NOWAIT;

		/* Look up the audit column key */
		EXEC RecHubAudit.usp_dimAuditColumns_Get @parmSchemaName, @parmTableName, @parmColumnName, @AuditColumnKey OUT;
	END
	ELSE
		SET @AuditColumnKey = 0;

	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	EXEC RecHubAudit.usp_factDataAuditSummary_Ins 	
		@parmAuditDateKey			= @AuditDateKey,
		@parmUserID					= @parmUserID,
		@parmAuditApplicationKey	= @AuditApplicationKey,
		@parmAuditColumnKey			= @AuditColumnKey,
		@parmAuditType				= @parmAuditType,
		@parmAuditValue				= @parmAuditValue,
		@parmAuditKey				= @AuditKey;

	IF( @parmAuditMessage IS NOT NULL )
		EXEC RecHubAudit.usp_factDataAuditMessages_Ins 
			@parmAuditDateKey			= @AuditDateKey,
			@parmUserID					= @parmUserID,
			@parmAuditApplicationKey	= @AuditApplicationKey,
			@parmAuditColumnKey			= @AuditColumnKey,
			@parmAuditKey				= @AuditKey,
			@parmAuditMessage			= @parmAuditMessage;
	
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
