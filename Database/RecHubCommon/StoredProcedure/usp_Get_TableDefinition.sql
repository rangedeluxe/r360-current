--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorStoredProcedureName usp_Get_TableDefinition
--WFSScriptProcessorStoredProcedureDrop 
IF OBJECT_ID('RecHubCommon.usp_Get_TableDefinition') IS NOT NULL
	DROP PROCEDURE RecHubCommon.usp_Get_TableDefinition
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubCommon.usp_Get_TableDefinition
(
	@parmSchemaName VARCHAR(255),
	@parmTableName VARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013  WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 06/04/2013
*
* Purpose: Request Batch Setup Fields  
*
* Modification History
* 06/04/2013 WI 103840 JMC   Initial Version
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT
		information_schema.columns.TABLE_SCHEMA				AS 'Schema',
		information_schema.columns.TABLE_NAME				AS 'TableName',
		information_schema.columns.COLUMN_NAME				AS 'ColumnName',
		information_schema.columns.DATA_TYPE				AS 'Type',
		information_schema.columns.CHARACTER_MAXIMUM_LENGTH	AS 'Length'
	FROM information_schema.columns
	WHERE information_schema.columns.TABLE_SCHEMA = @parmSchemaName
		AND information_schema.columns.TABLE_NAME = @parmTableName
	ORDER BY ordinal_position;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
