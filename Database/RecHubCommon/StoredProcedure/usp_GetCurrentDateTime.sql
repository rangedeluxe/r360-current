--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorStoredProcedureName usp_GetCurrentDateTime
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubCommon.usp_GetCurrentDateTime') IS NOT NULL
       DROP PROCEDURE RecHubCommon.usp_GetCurrentDateTime
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubCommon.usp_GetCurrentDateTime 
    
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: Tom Emery
* Date: 12/26/2012
*
* Purpose: Retrieve the Current Date Time of the database server
*
* Modification History
* 12/26/2012 WI 72160 TWE   Created by converting embedded SQL
* 03/21/2013 WI 86245 JBS	Update to 2.0 release. Change Schema Name
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
    SELECT 
        Current_TimeStamp as DBCurrentDateTime;
END TRY
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH

