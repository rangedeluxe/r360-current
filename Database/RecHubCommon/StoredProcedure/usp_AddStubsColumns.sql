IF OBJECT_ID('RecHubCommon.usp_AddStubsColumns') IS NOT NULL
       DROP PROCEDURE RecHubCommon.usp_AddStubsColumns
GO

CREATE PROCEDURE RecHubCommon.usp_AddStubsColumns
(
	@parmColumnName				NVARCHAR(128),
	@parmSchemaName				NVARCHAR(128)
)
AS
/******************************************************************************
** DELUXE Corporation (DLX)
** Copyright � 2019 Deluxe Corp. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2019 Deluxe Corp. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: MGE
* Date: 11/04/2019
*
* Purpose: ALTER factStubs and WorkDataEntryWideStubs ADD column_name
*
* Modification History
* 11/04/2019 R360-30933 MGE	Created
* 11/11/2019 R360-31442 Added SchemaName parameter
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY
	DECLARE @SQLCommand NVARCHAR(MAX);
	DECLARE @ReturnCode INT;

	IF NOT EXISTS (Select 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHubData' AND TABLE_NAME = N'factStubs'
				AND COLUMN_NAME = @parmColumnName) 

		BEGIN
			SELECT @SQLCommand = 'ALTER TABLE RecHubData.factStubs ADD ';

			SELECT @SQLCommand = @SQLCommand + 
			@parmColumnName +
			CASE 
				WHEN RIGHT(@parmColumnName, 2) = '_1' THEN ' VARCHAR(256) SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '_7' THEN ' MONEY SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '11' THEN ' DATETIME SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '_6' THEN ' FLOAT SPARSE NULL; '
				ELSE ' VARCHAR(256) SPARSE NULL; '
			END

			EXEC sp_executesql @SQLCommand;
		END
		
	IF NOT EXISTS (Select 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @parmSchemaName AND TABLE_NAME = N'WorkDataEntryWideStubs'
				AND COLUMN_NAME = @parmColumnName) 
	
		BEGIN
			SELECT @SQLCommand = 'ALTER TABLE ' + @parmSchemaName +'.WorkDataEntryWideStubs ADD ';

			SELECT @SQLCommand = @SQLCommand + 
			@parmColumnName +
			CASE 
				WHEN RIGHT(@parmColumnName, 2) = '_1' THEN ' VARCHAR(256) SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '_7' THEN ' MONEY SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '11' THEN ' DATETIME SPARSE NULL; '
				WHEN RIGHT(@parmColumnName, 2) = '_6' THEN ' FLOAT SPARSE NULL; '
				ELSE ' VARCHAR(256) SPARSE NULL; '
			END

			EXEC sp_executesql @SQLCommand;
		END
	
	IF EXISTS (Select 1 FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = @parmSchemaName AND TABLE_NAME = N'factStubs')
		BEGIN
		IF NOT EXISTS (Select 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = @parmSchemaName AND TABLE_NAME = N'factStubs'
					AND COLUMN_NAME = @parmColumnName) 
	
			BEGIN
				SELECT @SQLCommand = 'ALTER TABLE ' + @parmSchemaName + '.factStubs ADD ';

				SELECT @SQLCommand = @SQLCommand + 
				@parmColumnName +
				CASE 
					WHEN RIGHT(@parmColumnName, 2) = '_1' THEN ' VARCHAR(256) SPARSE NULL; '
					WHEN RIGHT(@parmColumnName, 2) = '_7' THEN ' MONEY SPARSE NULL; '
					WHEN RIGHT(@parmColumnName, 2) = '11' THEN ' DATETIME SPARSE NULL; '
					WHEN RIGHT(@parmColumnName, 2) = '_6' THEN ' FLOAT SPARSE NULL; '
					ELSE ' VARCHAR(256) SPARSE NULL; '
				END

				EXEC sp_executesql @SQLCommand;
			END
		END
	
END TRY	
BEGIN CATCH
    EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH