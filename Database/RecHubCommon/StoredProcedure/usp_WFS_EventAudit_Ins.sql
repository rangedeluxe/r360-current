--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubAudit">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubSystem">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubConfig_Admin">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubExtractWizard_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions <Permission ID="RecHubException_User">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_WFS_EventAudit_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubCommon.usp_WFS_EventAudit_Ins') IS NOT NULL
       DROP PROCEDURE RecHubCommon.usp_WFS_EventAudit_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubCommon.usp_WFS_EventAudit_Ins
(
	@parmApplicationName		VARCHAR(256),
	@parmEventName			VARCHAR(64),
	@parmEventType			VARCHAR(64) = NULL,
	@parmUserID			INT = 0,
	@parmWorkstation		INT = -1,
	@parmAuditMessage		VARCHAR(MAX) = NULL
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright ? 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright ? 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/29/2013
*
* Purpose: Insert an Event audit into the factEventAuditSummary and factEventAuditMessages.
*
* NOTES: This is a multi step process, calling several stored procedures and
*	inserting into multiple tables.
*
* Modification History
* 05/29/2013 WI 103630 JPB	Created
* 08/28/2014 WI 162164 LA	Update to Account for missing EventType
* 01/23/2015 WI 185886 RDS	Add permissions for use by File Purge Service
******************************************************************************/
SET NOCOUNT ON;
SET XACT_ABORT ON;
SET ARITHABORT ON;

DECLARE @AuditApplicationKey INT,	/* lookup from dimAuditApplications */
		@AuditEventKey INT,			/* lookup from dimAuditEvents */
		@AuditDateKey INT,
		@AuditKey UNIQUEIDENTIFIER;

BEGIN TRY

	/* If the EventType parameter is NULL, set it to the default value */
	IF (@parmEventType IS NULL) 
		SET @parmEventType = 'Uncategorized'
		
	/* Set the audit date key and audit key */
	SELECT
		@AuditDateKey = CAST(CONVERT(VARCHAR,GETDATE(),112) AS INT),
		@AuditKey = NEWID();

	/* First step is to get the AuditApplicationKey */
	EXEC RecHubAudit.usp_dimAuditApplications_Get @parmApplicationName, @AuditApplicationKey OUT;

	/* Second step is to get the AuditEventKey */
	EXEC RecHubAudit.usp_dimAuditEvents_Get @parmEventName, @parmEventType, @AuditEventKey OUT;

	BEGIN TRAN;

		EXEC RecHubAudit.usp_factEventAuditSummary_Ins
			@parmAuditDateKey		= @AuditDateKey,
			@parmUserID			= @parmUserID,
			@parmAuditApplicationKey	= @AuditApplicationKey,
			@parmAuditEventKey		= @AuditEventKey,
			@parmWorkstation		= @parmWorkstation,
			@parmAuditKey			= @AuditKey;

		IF( @parmAuditMessage IS NOT NULL )
			EXEC RecHubAudit.usp_factEventAuditMessages_Ins 
				@parmAuditDateKey		= @AuditDateKey,
				@parmUserID			= @parmUserID,
				@parmAuditApplicationKey	= @AuditApplicationKey,
				@parmAuditEventKey		= @AuditEventKey,
				@parmAuditKey			= @AuditKey,
				@parmAuditMessage		= @parmAuditMessage;
	COMMIT TRAN;

END TRY
BEGIN CATCH
	IF XACT_STATE() != 0 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
