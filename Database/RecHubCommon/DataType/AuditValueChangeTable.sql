--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubCommon">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubCommon
--WFSScriptProcessorDataTypeName AuditValueChangeTable
--WFSScriptProcessorDataTypeCreate
CREATE TYPE RecHubCommon.AuditValueChangeTable AS TABLE
(
	RowID INT IDENTITY(1,1) NOT NULL,
	ColumnName VARCHAR(128) NOT NULL,
	OldValue VARCHAR(256) NULL,
	NewValue VARCHAR(256) NULL
);
