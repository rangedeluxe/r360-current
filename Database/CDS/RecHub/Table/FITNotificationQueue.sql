--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable FITNotificationQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets.  These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details).  All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: XXX
* Date: XX/XX/XXXX
*
* Purpose:
*
*
* Modification History
* 09/30/2016 PT #127604053 JBS	Created (FP CR 55447)
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.FITNotificationQueue
(
	FITNotificationQueueID INT IDENTITY(1,1) NOT NULL
		CONSTRAINT PK_FITNotificationQueue PRIMARY KEY CLUSTERED,
	NotificationID UNIQUEIDENTIFIER NOT NULL,
	SourceTrackingID UNIQUEIDENTIFIER NOT NULL,
	QueueStatus INT NOT NULL
		CONSTRAINT DF_FITNotificationQueue_QueueStatus  DEFAULT 10,
	RetryCount INT NOT NULL
		CONSTRAINT DF_FITNotificationQueue_RetryCount  DEFAULT 0,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_FITNotificationQueue_CreationDate  DEFAULT GETDATE(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_FITNotificationQueue_ModificationDate  DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_FITNotificationQueue_CreatedBy  DEFAULT SUSER_SNAME(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_FITNotificationQueue_ModifiedBy  DEFAULT SUSER_SNAME()
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHub.FITNotificationQueue  WITH CHECK ADD  
	CONSTRAINT FK_FITNotificationQueue_Notifications FOREIGN KEY(NotificationID) REFERENCES dbo.Notifications (NotificationID);

