--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable Batch
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.Batch') IS NOT NULL
       DROP TABLE RecHub.Batch
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/22/2013
*
* Purpose: Stores batch data used by the SSIS import process. 
*
*
* Modification History
* 02/22/2013 WI 100312 JPB	Created
* 11/14/2013 WI 122435 JPB	Added BatchPaymentShortName
* 04/08/2014 WI 133346 DLD  Added DepositDDA and BankABA columns.
* 06/15/2014 WI 146628 JPB	Renamed BatchID to SourceBatchID, added BatchNumber.
* 01/06/2017 PT#136908915 JPB	Chagned BatchCueID to NOT NULL.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.Batch
(
	Batch_Id BIGINT NOT NULL,
	BatchStatus TINYINT NOT NULL,
	ActionCode TINYINT NOT NULL,
	BankID INT NOT NULL,
	CustomerID INT NOT NULL,
	LockboxID INT NOT NULL,
	DepositDate DATETIME NOT NULL,
	ProcessingDate DATETIME NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	BatchNumber INT NOT NULL,
	GlobalBatchID INT NULL,
	BatchCueID INT NOT NULL,
	DepositStatus INT NULL,
	BatchSourceShortName VARCHAR(30) NULL,
	BatchPaymentShortName VARCHAR(30) NULL,
	SystemType TINYINT NULL,
	BatchSiteCode INT NULL,
	DepositDDA varchar(40) NULL,
	BankABA varchar(10) NULL
);
--WFSScriptProcessorTableProperties
