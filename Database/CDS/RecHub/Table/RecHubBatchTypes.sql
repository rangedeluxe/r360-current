--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTrigger RecHubBatchTypes
CREATE TRIGGER dbo.RecHubBatchTypes ON dbo.BatchTypes
AFTER INSERT, UPDATE 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/06/2016
*
* Purpose: Insert row into RecHub.CDSQueue for Inserted/Updated BatchType.
*
*
* Modification History
* 04/06/2016 WI 275055 JPB	Created
******************************************************************************/
BEGIN TRY
	IF EXISTS ( SELECT 1 FROM inserted ) /* only continue if there is something to process */
	BEGIN
		IF OBJECT_ID('RecHub.usp_CDSQueue_Ins_DataEntrySetup') IS NOT NULL /* only continue if the SP is there */
		BEGIN
			IF( UPDATE(DESetupID) ) /* only continue if a DESetupID has changed */
			BEGIN
				DECLARE @OldDESetupID INT,
						@NewDESetupID INT,
						@RecCount INT,
						@Loop INT;
;

				DECLARE @DESetupIDs TABLE
				(
					RowID INT IDENTITY(1,1),
					OldDESetupID INT,
					NewDESetupID INT
				);

				/* Only process the records where the DESetupID has changed */
				INSERT INTO @DESetupIDs(OldDESetupID, NewDESetupID)
				SELECT 
					deleted.DESetupID,
					inserted.DESetupID
				FROM 
					inserted
					INNER JOIN deleted on deleted.BankID = inserted.BankID
						AND deleted.CustomerID = inserted.CustomerID
						AND deleted.LockboxID = inserted.LockboxID
						AND deleted.Code = inserted.Code
						AND deleted.SubCode = inserted.SubCode;

				SET @RecCount = @@ROWCOUNT;

				SET @Loop = 1;

				WHILE( @Loop <= @RecCount )
				BEGIN
					SELECT 
						@OldDESetupID = COALESCE(OldDESetupID,-1),
						@NewDESetupID = COALESCE(NewDESetupID,-1)
					FROM 
						@DESetupIDs
					WHERE
						RowID = @Loop;

					/* Insert the changed DESetupID */	
					IF( @OldDESetupID <> @NewDESetupID AND @NewDESetupID <> -1 )
						EXEC RecHub.usp_CDSQueue_Ins_DataEntrySetup @parmDESetupID = @NewDESetupID;

					SET @Loop = @Loop + 1;
				END
			END
		END
	END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
