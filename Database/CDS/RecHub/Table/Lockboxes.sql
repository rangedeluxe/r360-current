--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable Lockboxes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.Lockboxes') IS NOT NULL
       DROP TABLE RecHub.Lockboxes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2013
*
* Purpose: Work table for integraPAY Import SSIS Package. 
*
* Modification History
* 03/06/2013 WI 99979 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.Lockboxes
(
	Lockbox_Id BIGINT NOT NULL,
	BankID INT NOT NULL,
	CustomerID INT NOT NULL,
	LockboxID INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.Lockboxes.IDX_Lockboxes_Lockbox_Id
CREATE CLUSTERED INDEX IDX_Lockboxes_Lockbox_Id ON RecHub.Lockboxes
(
	Lockbox_Id ASC
);