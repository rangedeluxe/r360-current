--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable SetupResponses
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.SetupResponses') IS NOT NULL
       DROP TABLE RecHub.SetupResponses;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/03/2013
*
* Purpose: Work table for integraPAY Import SSIS Package. 
*
I
* Modification History
* 05/03/2013 WI 101702 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.SetupResponses
(
	CDSQueueID BIGINT NOT NULL,
	ResponseStatus TINYINT NOT NULL,
	ResponseMessage VARCHAR(MAX) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.SetupResponses.IDX_SetupResponses_CDSQueueIDResponseStatus
CREATE CLUSTERED INDEX IDX_SetupResponses_CDSQueueIDResponseStatus ON RecHub.SetupResponses
(
	CDSQueueID ASC,
	ResponseStatus ASC
);
