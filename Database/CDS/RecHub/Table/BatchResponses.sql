--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable BatchResponses
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.BatchResponses') IS NOT NULL
       DROP TABLE RecHub.BatchResponses;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/08/2013
*
* Purpose: Work table for integraPAY Import SSIS Package. 
*
I
* Modification History
* 06/08/2013 WI 102083 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.BatchResponses
(
	CDSQueueID BIGINT NOT NULL,
	ResponseStatus TINYINT NOT NULL,
	ResponseMessage VARCHAR(MAX) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.BatchResponses.IDX_SetupResponses_CDSQueueIDResponseStatus
CREATE CLUSTERED INDEX IDX_BatchResponses_CDSQueueIDResponseStatus ON RecHub.BatchResponses
(
	CDSQueueID ASC,
	ResponseStatus ASC
);
