--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTableName CDSQueue
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/19/2013
*
* Purpose: Queue items to be processed by the intergraPAY import SSIS packages.
*		   
*
* ActionCode:
*	1 - Batch Data Complete/Setup Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated

* QueueStatus:
*	10 - Ready to sent to RH360
*	15 - Failed processing on RH360 - but can resend
*	20 - Sent to RH360, waiting for a response
*	30 - Failed processing on RH360 - but cannot resend
*	99 - Successfully processed on RH360
*
*
* QueueType:
*	10 - Batch
*	20 - Bank
*	30 - Customer
*	40 - Lockbox
*	50 - SiteCode
*	60 - DESetup
*	70 - Document Type
*
* 
* Modification History
* 02/19/2013 WI 99974 JPB	Created
* 01/08/2016 WI 253714 JPB	Added BatchSourceShortName
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.CDSQueue
(
	CDSQueueID BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_CDSQueue PRIMARY KEY CLUSTERED,
	QueueType TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_QueueType CHECK(QueueType IN (10,20,30,40,50,60,70)), 
	QueueStatus TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99)), 
	ResponseStatus TINYINT NOT NULL
		CONSTRAINT DF_CDSQueue_ResponseStatus DEFAULT 255,
	ActionCode TINYINT NOT NULL
		CONSTRAINT CK_CDSQueue_ActionCode CHECK(ActionCode IN (1,2,3,4,5,6)),
	RetryCount TINYINT NOT NULL
		CONSTRAINT DF_CDSQueue_RetryCount DEFAULT 0,
	NotifyIMS BIT NOT NULL
		CONSTRAINT DF_CDSQueue_NotifyIMS DEFAULT 0,
	CreationDate DATETIME NOT NULL
		CONSTRAINT DF_CDSQueue_CreationDate DEFAULT GETDATE(),
	CreatedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_CDSQueue_CreatedBy DEFAULT SUSER_NAME(),
	ModificationDate DATETIME NOT NULL
		CONSTRAINT DF_CDSQueue_ModificationDate DEFAULT GETDATE(),
	ModifiedBy VARCHAR(128) NOT NULL
		CONSTRAINT DF_CDSQueue_ModifiedBy DEFAULT SUSER_NAME(),
	BankID INT NULL,
	CustomerID INT NULL,
	LockboxID INT NULL,
	BatchID INT NULL,
	SiteCode INT NULL,
	DESetupID INT NULL,
	ProcessingDate DATETIME NULL,
	DepositDate DATETIME NULL,
	BatchSourceShortName VARCHAR(10) NULL,
	FileDescriptor VARCHAR(30) NULL,
	XMLResponseDocument XML NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.CDSQueue.IDX_OTISQueue_BankIDLockboxIDProcessingDateBatchIDQueueStatus
CREATE INDEX IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID ON RecHub.CDSQueue 
(
	QueueType,
	QueueStatus,
	CDSQueueID
)
INCLUDE
(
	BankID,
	CustomerID,
	LockboxID,
	BatchID,
	ProcessingDate,
	DepositDate,
	SiteCode,
	DESetupID,
	FileDescriptor
);
--WFSScriptProcessorIndex RecHub.CDSQueue.IDX_OTISQueue_QueueStatusModificationDateRetryCount
CREATE NONCLUSTERED INDEX IDX_CDSQueue_QueueStatusModificationDateRetryCount ON RecHub.CDSQueue
(
	QueueStatus,
	ModificationDate,
	RetryCount
)
INCLUDE
(
	BankID,
	CustomerID,
	LockboxID,
	BatchID,
	ProcessingDate,
	DepositDate,
	SiteCode,
	DESetupID,
	FileDescriptor,
	ActionCode
);
