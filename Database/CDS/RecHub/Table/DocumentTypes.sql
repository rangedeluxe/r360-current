--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable DocumentTypes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.DocumentTypes') IS NOT NULL
       DROP TABLE RecHub.DocumentTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2013
*
* Purpose: Work table for integraPAY Import SSIS Package. 
*
I
* Modification History
* 03/06/2013 WI 99978 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.DocumentTypes
(
	DocumentType_Id BIGINT NOT NULL,
	FileDescriptor VARCHAR(30) NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.DocumentTypes.IDX_DocumentTypes_DocumentType_Id
CREATE CLUSTERED INDEX IDX_DocumentTypes_DocumentType_Id ON RecHub.DocumentTypes
(
	DocumentType_Id ASC
);
