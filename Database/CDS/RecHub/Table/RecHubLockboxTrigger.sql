--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTrigger RecHubLockbox
CREATE TRIGGER dbo.RecHubLockbox ON dbo.Lockbox
AFTER INSERT, UPDATE 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/08/2013
*
* Purpose: Insert row into RecHub.CDSQueue for Inserted/Updated locboxes.
*
*
* Modification History
* 06/08/2013 WI 104864 JPB	Created
* 10/29/2013 WI 119224 JPB	Account for an update not actually doing an update.
* 10/30/2013 WI 119388 JPB	Support multiple rows insert or updated.
* 04/06/2016 WI 272990 JPB	Insert a DESetupID if it changed.
******************************************************************************/
BEGIN TRY
	IF EXISTS ( SELECT 1 FROM inserted )
	BEGIN
		IF OBJECT_ID('RecHub.usp_CDSQueue_Ins_Lockbox') IS NOT NULL
		BEGIN
			DECLARE @BankID INT,
					@CustomerID INT,
					@LockboxID INT,
					@OldDESetupID INT,
					@NewDESetupID INT,
					@RecCount INT,
					@Loop INT;

			DECLARE @Lockboxes TABLE
			(
				RowID INT IDENTITY(1,1),
				BankID INT,
				CustomerID INT,
				LockboxID INT,
				OldDESetupID INT,
				NewDESetupID INT
			);

			INSERT INTO @Lockboxes(BankID, CustomerID, LockboxID, OldDESetupID, NewDESetupID)
			SELECT 
				inserted.BankID,
				inserted.CustomerID,
				inserted.LockboxID,
				deleted.DESetupID,
				inserted.DESetupID
			FROM 
				inserted
				LEFT JOIN deleted ON deleted.BankID = inserted.BankID
					AND deleted.CustomerID = inserted.CustomerID
					AND deleted.LockboxID = inserted.LockboxID;

			SET @RecCount = @@ROWCOUNT;

			SET @Loop = 1;

			WHILE( @Loop <= @RecCount )
			BEGIN
				SELECT 
					@BankID = BankID,
					@CustomerID = CustomerID,
					@LockboxID = LockboxID,
					@OldDESetupID = COALESCE(OldDESetupID,-1),
					@NewDESetupID = COALESCE(NewDESetupID,-1)
				FROM 
					@Lockboxes
				WHERE
					RowID = @Loop;
				
				EXEC RecHub.usp_CDSQueue_Ins_Lockbox @parmBankID = @BankID, @parmCustomerID = @CustomerID, @parmLockboxID = @LockboxID;

				/* WI 272990 - if the DESetupID has changed, insert it into the queue table for processing */
				IF( @OldDESetupID <> @NewDESetupID AND @NewDESetupID <> -1 )
					EXEC RecHub.usp_CDSQueue_Ins_DataEntrySetup @parmDESetupID = @NewDESetupID;

				SET @Loop = @Loop + 1;
			END
		END
	END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
