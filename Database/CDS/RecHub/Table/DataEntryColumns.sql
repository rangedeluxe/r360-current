--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable DataEntryColumns
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.DataEntryColumns') IS NOT NULL
       DROP TABLE RecHub.DataEntryColumns;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/22/2013
*
* Purpose:  Work table for integraPAY Import SSIS Package. 
*
* Modification History
* 02/22/2013 WI 100314 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.DataEntryColumns
(
	TableName VARCHAR(36),
	ColumnName VARCHAR(64),
	FieldType SMALLINT
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBatchDataSetupFields.IDX_dimBatchDataSetupFields_Keyword_BatchSourceKey
--CREATE UNIQUE INDEX IDX_dimBatchDataSetupFields_Keyword_BatchSourceKey ON RecHubData.dimBatchDataSetupFields (Keyword,BatchSourceKey);
