--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable DataEntryPivot
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.DataEntryPivot') IS NOT NULL
       DROP TABLE RecHub.DataEntryPivot;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/22/2013
*
* Purpose: Work table for integraPAY Import SSIS Package.
*
* Modification History
* 02/22/2013 WI 100315 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.DataEntryPivot
(
	GlobalBatchID INT NOT NULL,
	TransactionID INT NOT NULL,
	TransactionSequence INT NOT NULL,
	BatchSequence INT NOT NULL,
	GlobalID INT NOT NULL,
	TableType TINYINT NULL,
	ColumnName VARCHAR(32) NULL,
	FieldLength TINYINT NULL,
	DataType SMALLINT NULL,
	ScreenOrder TINYINT NULL,
	DisplayName VARCHAR(32) NULL,
	DataEntryValueDateTime DATETIME NULL,
	DataEntryValueFloat FLOAT NULL,
	DataEntryValueMoney MONEY NULL,
	DataEntryValue varchar(256) NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubData.dimBatchDataSetupFields.IDX_dimBatchDataSetupFields_Keyword_BatchSourceKey
--CREATE UNIQUE INDEX IDX_dimBatchDataSetupFields_Keyword_BatchSourceKey ON RecHubData.dimBatchDataSetupFields (Keyword,BatchSourceKey);
