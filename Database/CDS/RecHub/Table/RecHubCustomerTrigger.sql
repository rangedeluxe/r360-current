--WFSScriptProcessorSchema dbo
--WFSScriptProcessorTrigger RecHubCustomer
CREATE TRIGGER dbo.RecHubCustomer ON dbo.Customer
AFTER INSERT, UPDATE 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/08/2013
*
* Purpose: Insert row into RecHub.CDSQueue for Inserted/Updated Banks.
*
*
* Modification History
* 06/08/2013 WI 104861 JPB	Created
* 10/29/2013 WI 119221 JPB	Account for an update not actually doing an update.
* 10/30/2013 WI 119385 JPB	Support multiple rows insert or updated.
******************************************************************************/
BEGIN TRY
	IF EXISTS ( SELECT 1 FROM inserted )
	BEGIN
		IF OBJECT_ID('RecHub.usp_CDSQueue_Ins_Customer') IS NOT NULL
		BEGIN
			DECLARE @BankID INT,
					@CustomerID INT,
					@RecCount INT,
					@Loop INT;

			DECLARE @Customers TABLE
			(
				RowID INT IDENTITY(1,1),
				BankID INT,
				CustomerID INT
			);

			INSERT INTO @Customers(BankID, CustomerID)
			SELECT BankID,CustomerID
			FROM inserted;

			SET @RecCount = @@ROWCOUNT;

			SET @Loop = 1;

			WHILE( @Loop <= @RecCount )
			BEGIN
				SELECT	
					@BankID = BankID,
					@CustomerID = CustomerID
				FROM 
					@Customers
				WHERE
					RowID = @Loop;

				EXEC RecHub.usp_CDSQueue_Ins_Customer @parmBankID = @BankID, @parmCustomerID = @CustomerID;

				SET @Loop = @Loop + 1;
			END
		END
	END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
