--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorTable Banks
--WFSScriptProcessorTableDrop
IF OBJECT_ID('RecHub.Banks') IS NOT NULL
       DROP TABLE RecHub.Banks;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2013
*
* Purpose: Work table for integraPAY Import SSIS Package. 
*
I
* Modification History
* 03/06/2013 WI 99975 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE RecHub.Banks
(
	Bank_Id BIGINT NOT NULL,
	BankID INT NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHub.Banks.IDX_Banks_Bank_Id
CREATE CLUSTERED INDEX IDX_Banks_Bank_Id ON RecHub.Banks
(
	Bank_Id ASC
);
