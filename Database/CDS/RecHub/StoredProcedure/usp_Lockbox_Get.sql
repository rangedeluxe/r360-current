--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Lockbox_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Lockbox_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Lockbox_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Lockbox_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/08/2013
*
* Purpose: Retrieve lockbox information to be processed by the integraPAY Import 
*		SSIS Package.
*
* Modification History
* 03/08/2013 WI 99972 JPB	Created
* 02/11/2016 WI 263456 JPB	Hard coded CustomerID to -1
* 12/13/2017 PT 152545607 JPB	Removed SiteOrganizationID, updated SiteCode 
*								to return -1 if NULL
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	SELECT	
		RecHub.Lockboxes.Lockbox_Id,
		COALESCE(dbo.Lockbox.SiteCode,-1) AS SiteCode,
		dbo.Lockbox.BankID AS SiteBankID,
		dbo.Lockbox.LockboxID AS SiteClientAccountID,
		dbo.Lockbox.IsActive,
		dbo.Lockbox.Cutoff,
		dbo.Lockbox.OnlineColorMode,
		dbo.Lockbox.CWDBBatchRetention AS DataRetentionDays, 
		(SELECT MAX(ImageRetention) FROM (VALUES (dbo.Lockbox.CWDBBitonalRetention), (dbo.Lockbox.CWDBColorRetention), (dbo.Lockbox.CWDBGrayScaleRetention)) AS ImageRetentions(ImageRetention)) AS ImageRetentionDays,
		dbo.Lockbox.IsCommingled,
		dbo.Lockbox.LockboxKey AS SiteClientAccountKey,
		RTRIM(dbo.Lockbox.ShortName) AS ShortName,
		RTRIM(dbo.Lockbox.LongName) AS LongName,
		RTRIM(dbo.Lockbox.POBox) AS POBox,
		RTRIM(dbo.Lockbox.DDA) AS DDA
	FROM 
		dbo.Lockbox
		INNER JOIN RecHub.Lockboxes ON dbo.Lockbox.BankID = RecHub.Lockboxes.BankID
			AND dbo.Lockbox.CustomerID = RecHub.Lockboxes.CustomerID
			AND dbo.Lockbox.LockboxID =  RecHub.Lockboxes.LockboxID;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
