--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_DocumentTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_DocumentTypes_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_DocumentTypes_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_DocumentTypes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/11/2013
*
* Purpose: Retrieve document type information to be processed by the integraPAY Import 
*		SSIS Package.
*
* NOTE: Alias are used since the table names are the same in both schemas.
*
 Modification History
* 03/11/2013 WI 99971 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT	
		RDocumentTypes.DocumentType_Id,
		IDocumentTypes.FileDescriptor,
		IDocumentTypes.[Description] AS DocumentTypeDescription,
		IDocumentTypes.IMSDocumentType
	FROM 
		dbo.DocumentTypes AS IDocumentTypes
		INNER JOIN RecHub.DocumentTypes AS RDocumentTypes ON IDocumentTypes.FileDescriptor = RDocumentTypes.FileDescriptor;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
