--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_Stubs_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Stubs_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Stubs_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Stubs_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/25/2013
*
* Purpose: Retreive stubs that are ready for processing.
*
* Modification History
* 02/25/2013 WI 100323 JPB	Created
* 02/01/2016 WI 260905 JPB	Do not return ActionCode 4 items
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT	
		RecHub.Batch.Batch_Id,
		ROW_NUMBER() OVER (ORDER BY dbo.Stubs.TransactionID, dbo.Stubs.TransactionSequence) AS Stub_Id,
		dbo.Stubs.TransactionID,
		dbo.Stubs.TransactionSequence AS SequenceWithInTransaction,
		dbo.Stubs.BatchSequence,
		dbo.Stubs.StubSequence,
		ISNULL(dbo.Documents.BatchSequence,-1) AS DocumentBatchSequence,
		dbo.Stubs.IsOMRDetected,
		dbo.Stubs.Amount,
		dbo.Stubs.AccountNumber
	FROM 
		dbo.Stubs
		INNER JOIN RecHub.Batch ON dbo.Stubs.GlobalBatchID = RecHub.Batch.GlobalBatchID
		LEFT JOIN dbo.StubSource ON dbo.StubSource.GlobalStubID = dbo.Stubs.GlobalStubID
		LEFT JOIN dbo.Documents ON dbo.Documents.GlobalDocumentID = dbo.StubSource.GlobalDocumentID
	WHERE
		ActionCode <> 4;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
