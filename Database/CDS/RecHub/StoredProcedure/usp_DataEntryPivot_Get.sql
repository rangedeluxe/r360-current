--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_DataEntryPivot_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_DataEntryPivot_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_DataEntryPivot_Get
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_DataEntryPivot_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/14/2013
*
* Purpose: Retreive data entry that are ready for processing.
*
* Modification History
* 02/27/2013 WI 100321 JPB	Created
* 10/03/2015 WI 244769 JPB	Updates for WorkgroupDataEntryColumnKey.
* 02/01/2016 WI 260903 JPB	Do not return ActionCode 4 items
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

BEGIN TRY
	SELECT
		RecHub.Batch.Batch_Id,
		ROW_NUMBER() OVER (ORDER BY RecHub.DataEntryPivot.TransactionID, RecHub.DataEntryPivot.TransactionSequence) AS DataEntry_Id,
		RecHub.DataEntryPivot.TransactionID,
		RecHub.DataEntryPivot.TransactionSequence,
		RecHub.DataEntryPivot.BatchSequence,
		CASE RecHub.DataEntryPivot.TableType
			WHEN 0 THEN 1
			WHEN 1 THEN 1
			ELSE 0
		END AS IsCheck,
		RecHub.DataEntryPivot.DataType,
		CAST(UPPER(RecHub.DataEntryPivot.ColumnName) AS NVARCHAR(256)) AS FieldName,
		CAST(UPPER(RecHub.DataEntryPivot.DisplayName) AS NVARCHAR(256)) AS SourceDisplayName,
		RecHub.DataEntryPivot.DataEntryValueDateTime,
		RecHub.DataEntryPivot.DataEntryValueFloat,
		RecHub.DataEntryPivot.DataEntryValueMoney,
		RecHub.DataEntryPivot.DataEntryValue
	FROM	
		RecHub.Batch
		INNER JOIN RecHub.DataEntryPivot ON RecHub.DataEntryPivot.GlobalBatchID = RecHub.Batch.GlobalBatchID
	WHERE
		ActionCode <> 4;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
