--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Bank_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Bank_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Bank_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Bank_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/0/2013
*
* Purpose: Retrieve bank information to be processed by the integraPAY Import 
*		SSIS Package.
*
* Modification History
* 02/25/2013 WI 99968 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	SELECT	
		RecHub.Banks.Bank_Id,
		dbo.Bank.BankID AS SiteBankID,
		RTRIM(dbo.Bank.BankName) AS BankName,
		RTRIM(dbo.Bank.BankABA) AS ABA
	FROM 
		dbo.Bank
		INNER JOIN RecHub.Banks ON dbo.Bank.BankID = RecHub.Banks.BankID;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
