--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_Documents_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Documents_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Documents_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Documents_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/25/2013
*
* Purpose: Retrieve document information to be processed by the integraPAY Import 
*		SSIS Package.
*
* Modification History
* 02/25/2013 WI 100322 JPB	Created
* 02/01/2016 WI 260904 JPB	Do not return ActionCode 4 items
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT	
		RecHub.Batch.Batch_Id,
		ROW_NUMBER() OVER (ORDER BY dbo.Documents.TransactionID, dbo.Documents.TransactionSequence) AS Document_Id,
		dbo.Documents.TransactionID,
		dbo.Documents.TransactionSequence AS SequenceWithInTransaction,
		dbo.Documents.BatchSequence,
		dbo.Documents.DocumentSequence,
		dbo.Documents.FileDescriptor
	FROM 
		dbo.Documents
		INNER JOIN RecHub.Batch ON dbo.Documents.GlobalBatchID = RecHub.Batch.GlobalBatchID
	WHERE
		ActionCode <> 4;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
