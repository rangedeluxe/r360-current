--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_Customer_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Customer_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Customer_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Customer_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/08/2013
*
* Purpose: Retrieve customer information to be processed by the integraPAY Import 
*		SSIS Package.
*
* Modification History
* 03/08/2013 WI 99969 JPB	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT	
		RecHub.Customers.Customer_Id,
		dbo.Customer.CustomerID AS SiteOrganizationID,
		dbo.Customer.BankID AS SiteBankID,
		RTRIM(dbo.Customer.Name) AS Name,
		RTRIM(dbo.Customer.[Description]) AS [Description]
	FROM 
		dbo.Customer
		INNER JOIN RecHub.Customers ON dbo.Customer.BankID = RecHub.Customers.BankID
			AND dbo.Customer.CustomerID = RecHub.Customers.CustomerID;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
