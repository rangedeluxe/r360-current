--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingDataEntrySetups
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_CDSQueue_Get_PendingDataEntrySetups') IS NOT NULL
       DROP PROCEDURE RecHub.usp_CDSQueue_Get_PendingDataEntrySetups;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_CDSQueue_Get_PendingDataEntrySetups
(
	@parmPendingDataEntrySetups BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2013
*
* Purpose: Retreive list of data entry setups that are ready for processing.
*
* Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* Modification History
* 03/06/2013 WI 99964 JPB	Created
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

BEGIN TRY
	;WITH PendingList AS
	(
		SELECT 	TOP(150000) MIN(CDSQueueID) AS RecordID,DESetupID
		FROM 	RecHub.CDSQueue
		WHERE 	QueueType = 60
				AND QueueStatus <= 20
		GROUP BY DESetupID
		ORDER BY RecordID
	)
	INSERT INTO RecHub.DataEntrySetups
	(
		DataEntrySetup_Id,
		DESetupID
	)
	SELECT	RecordID,
			RecHub.CDSQueue.DESetupID
	FROM	RecHub.CDSQueue 
			INNER JOIN PendingList ON RecHub.CDSQueue.CDSQueueID = PendingList.RecordID
	WHERE 	(QueueStatus = 10) OR (QueueStatus = 20) OR (QueueStatus = 15 AND RecHub.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RetryCount),GETDATE()));

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingDataEntrySetups = 1;
		UPDATE	RecHub.CDSQueue
		SET		QueueStatus = 20,
				RecHub.CDSQueue.ModificationDate = GETDATE(),
				RecHub.CDSQueue.ModifiedBy = SUSER_SNAME()
		FROM	RecHub.CDSQueue
				INNER JOIN RecHub.DataEntrySetups ON RecHub.DataEntrySetups.DataEntrySetup_Id = RecHub.CDSQueue.CDSQueueID;
	END
	ELSE SET @parmPendingDataEntrySetups = 0;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
