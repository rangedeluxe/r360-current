--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_PivotDataEntry
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_PivotDataEntry') IS NOT NULL
       DROP PROCEDURE RecHub.usp_PivotDataEntry;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_PivotDataEntry
(
	@parmPendingDataEntry BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2010-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/14/2013
*
* Purpose: Pivot check/stub data entry data
*
* NOTE: Alias are used since the table names are the same in both schemas.
*
* Modification History
* 02/27/2013 WI 100325 JPB	Created
* 12/16/2013 WI 125441 JPB	Corrected float value in (string) value column.
* 01/25/2014 WI 128412 JBS	(FP 127911)Added StubsDataentry 'Amount' column to accumulate as Money not Float
* 09/26/2014 WI 168161 JBS	Changing max length for data value. In the SSIS process we will evaluate if the 
*							Lengths match and determine action there.
* 10/30/2015 WI 244770 JPB	Added return BIT so SSIS knows if there are records to process.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @SQLCommand VARCHAR(MAX),
		@ColumnList VARCHAR(MAX),
		@CaseStatement VARCHAR(MAX);

BEGIN TRY


	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CheckDataEntry')) 
		DROP TABLE #CheckDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubDataEntry')) 
		DROP TABLE #StubDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DataEntryColumns')) 
		DROP TABLE #DataEntryColumns;


	CREATE TABLE #CheckDataEntry
	(
		GlobalBatchID INT NOT NULL, 
		TransactionID INT NOT NULL, 
		TransactionSequence INT NOT NULL, 
		BatchSequence INT NOT NULL, 
		GlobalCheckID INT NOT NULL
	);

	CREATE TABLE #StubDataEntry
	(
		GlobalBatchID INT NOT NULL, 
		TransactionID INT NOT NULL, 
		TransactionSequence INT NOT NULL, 
		BatchSequence INT NOT NULL, 
		GlobalStubID INT NOT NULL
	);

	CREATE TABLE #DataEntryColumns
	(
		TableName VARCHAR(36),
		ColumnName VARCHAR(64),
		FieldType SMALLINT
	);

	INSERT INTO #DataEntryColumns(TableName,ColumnName,FieldType)
	SELECT DISTINCT 
		RTRIM(TableName) AS TableName,
		RTRIM(FldName) AS FldName,
		FldDataTypeEnum
	FROM	
		dbo.DESetupFields
		INNER JOIN dbo.Batch AS IBatch ON IBatch.DESetupID = dbo.DESetupFields.DESetupID
		INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
		INNER JOIN INFORMATION_SCHEMA.COLUMNS ON INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME = dbo.DESetupFields.FldName
	WHERE	
		DocumentType = 0
		AND (UPPER(INFORMATION_SCHEMA.COLUMNS.TABLE_NAME) = 'CHECKS' OR UPPER(INFORMATION_SCHEMA.COLUMNS.TABLE_NAME) = 'CHECKSDATAENTRY')
	ORDER BY 
		FldDataTypeEnum,
		TableName;

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @SQLCommand = 'ALTER TABLE #CheckDataEntry ADD ';
		SET @ColumnList = '';

		;WITH DEColumns AS
		(
			SELECT 
				RTRIM(FldName) AS FldName, 
				FldDataTypeEnum, 
				FldLength, 
				#DataEntryColumns.TableName AS TableName
			FROM	
				dbo.Batch AS IBatch
				INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
				LEFT JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = IBatch.DESetupID AND dbo.DESetupFields.DocumentType = 0
				LEFT JOIN #DataEntryColumns ON #DataEntryColumns.ColumnName = dbo.DESetupFields.FldName
		)
		SELECT @SQLCommand = @SQLCommand + FldName + 
			CASE MAX(FldDataTypeEnum)
				WHEN 1 THEN ' VARCHAR(256)'   -- WI168161
				WHEN 6 THEN ' FLOAT'
				WHEN 7 THEN ' MONEY'
				WHEN 11 THEN ' DATETIME '
			END + ', ',
				@ColumnList = @ColumnList + TableName + '.' + FldName + ','
		FROM 
			DEColumns
		WHERE 
			FldName IS NOT NULL
		GROUP BY 
			TableName,
			FldName;

		IF LEN(@SQLCommand) > 0	
			SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
		IF LEN(@ColumnList) > 0	
			SET @ColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);

		EXEC(@SQLCommand);

		SET @SQLCommand = 'INSERT INTO #CheckDataEntry(GlobalBatchID,TransactionID,TransactionSequence,BatchSequence,GlobalCheckID,'+@ColumnList+')
			SELECT	Checks.GlobalBatchID, 
					Checks.TransactionID,
					Checks.TransactionSequence,
					Checks.BatchSequence,
					Checks.GlobalCheckID,
			' + @ColumnList + '
			FROM	dbo.Batch AS IBatch
					INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
					INNER JOIN dbo.Checks ON dbo.Checks.GlobalBatchID = RBatch.GlobalBatchID
					INNER JOIN dbo.ChecksDataEntry ON dbo.ChecksDataEntry.GlobalCheckID = dbo.Checks.GlobalCheckID;';
	
		EXEC(@SQLCommand);

		SET @SQLCommand = 'SELECT	#CheckDataEntry.GlobalBatchID, 
				#CheckDataEntry.TransactionID,
				#CheckDataEntry.TransactionSequence,
				#CheckDataEntry.BatchSequence,
				#CheckDataEntry.GlobalCheckID,';


		SET @CaseStatement = '';
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN ' +
			CASE 
				WHEN FieldType = 6 THEN 'CASE WHEN PATINDEX(''%[1-9]%'', REVERSE(STR(#CheckDataEntry.' + ColumnName + ',60,15))) < PATINDEX(''%.%'', REVERSE(STR(#CheckDataEntry.' + ColumnName + ',60,15))) THEN RTRIM(LTRIM(LEFT(STR(#CheckDataEntry.' + ColumnName + ',60,15), LEN(STR(#CheckDataEntry.' + ColumnName + ',60,15)) - PATINDEX(''%[1-9]%'', REVERSE(STR(#CheckDataEntry.' + ColumnName + ',60,15))) + 1))) ELSE RTRIM(LTRIM(LEFT(STR(#CheckDataEntry.' + ColumnName + ',60,15), LEN(STR(#CheckDataEntry.' + ColumnName + ',60,15)) - PATINDEX(''%.%'', REVERSE(STR(#CheckDataEntry.' + ColumnName + ',60,15)))))) END'
				WHEN FieldType = 7 THEN 'CAST(#CheckDataEntry.' + ColumnName + ' AS VARCHAR(128))'
				WHEN FieldType = 11 THEN 'CONVERT(VARCHAR,#CheckDataEntry.' + ColumnName + ',120)'
				ELSE 'RTRIM(#CheckDataEntry.' + ColumnName + ')'
			END
		FROM #DataEntryColumns;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValue,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValue, ';

		SET @CaseStatement = '';
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #CheckDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 6;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueFloat,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueFloat, ';

		SET @CaseStatement = ''
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #CheckDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 7;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueMoney,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueMoney, ';

		SET @CaseStatement = ''
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #CheckDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 11;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueDateTime,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueDateTime, ';

		SET @SQLCommand = @SQLCommand + '
				CASE RTRIM(dbo.DESetupFields.TableName)
					WHEN ''Checks'' THEN 0
					WHEN ''ChecksDataEntry'' THEN 1
					WHEN ''Stubs'' THEN 2
					WHEN ''StubsDataEntry'' THEN 3
				END AS TableType,
				RTRIM(FldName) AS ColumnName,
				FldLength AS FieldLength,
				FldDataTypeEnum AS DataType,
				ScreenOrder,
				COALESCE(RTRIM(ReportTitle),RTRIM(FldPrompt),RTRIM(FldName)) AS DisplayName
		FROM	RecHub.Batch AS RBatch
				INNER JOIN #CheckDataEntry ON #CheckDataEntry.GlobalBatchID = RBatch.GlobalBatchID
				INNER JOIN dbo.Batch AS IBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
				LEFT JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = IBatch.DESetupID AND dbo.DESetupFields.DocumentType = 0
				LEFT JOIN #DataEntryColumns ON #DataEntryColumns.ColumnName = dbo.DESetupFields.FldName;';

		INSERT INTO RecHub.DataEntryPivot
		(
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			GlobalID,
			DataEntryValue,
			DataEntryValueFloat,
			DataEntryValueMoney,
			DataEntryValueDateTime,
			TableType,
			ColumnName,
			FieldLength,
			DataType,
			ScreenOrder,
			DisplayName
		)
		EXEC (@SQLCommand);
	END

	TRUNCATE TABLE #DataEntryColumns;

-- End of Checks Data Entry and start of Stubs Data Entry

	INSERT INTO #DataEntryColumns(TableName,ColumnName,FieldType)
	SELECT DISTINCT 
		RTRIM(TableName) AS TableName,
		RTRIM(FldName) AS FldName,
		FldDataTypeEnum
	FROM	
		dbo.DESetupFields
		INNER JOIN dbo.Batch AS IBatch ON IBatch.DESetupID = dbo.DESetupFields.DESetupID
		INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
		INNER JOIN INFORMATION_SCHEMA.COLUMNS ON INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME = dbo.DESetupFields.FldName
	WHERE	
		DocumentType = 1
		AND (UPPER(INFORMATION_SCHEMA.COLUMNS.TABLE_NAME) = 'STUBS' OR UPPER(INFORMATION_SCHEMA.COLUMNS.TABLE_NAME) = 'STUBSDATAENTRY')
	ORDER BY 
		FldDataTypeEnum,
		TableName;

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @SQLCommand = 'ALTER TABLE #StubDataEntry ADD ';
		SET @ColumnList = '';

		;WITH DEColumns AS
		(
			SELECT 
				RTRIM(FldName) AS FldName, 
				FldDataTypeEnum, 
				FldLength, 
				#DataEntryColumns.TableName AS TableName
			FROM	
				dbo.Batch AS IBatch
				INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
				LEFT JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = IBatch.DESetupID AND dbo.DESetupFields.DocumentType = 1
				LEFT JOIN #DataEntryColumns ON #DataEntryColumns.ColumnName = dbo.DESetupFields.FldName
		)
		SELECT @SQLCommand = @SQLCommand + FldName + 
			CASE MAX(FldDataTypeEnum)
				--WHEN 1 THEN ' VARCHAR(' + CAST(MAX(FldLength) AS VARCHAR) + ')'
				WHEN 1 THEN ' VARCHAR(256)'		-- WI168161
				WHEN 6 THEN 
						CASE RTRIM(FldName)
						WHEN 'Amount' THEN ' MONEY'
						ELSE ' FLOAT'
						END
				WHEN 7 THEN ' MONEY'
				WHEN 11 THEN ' DATETIME '
			END + ', ',
				@ColumnList = @ColumnList + TableName + '.' + FldName + ','
		FROM 
			DEColumns
		WHERE 
			FldName IS NOT NULL
		GROUP BY 
			TableName,
			FldName;

		IF LEN(@SQLCommand) > 0	
			SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1);
		IF LEN(@ColumnList) > 0	
			SET @ColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1);
		EXEC(@SQLCommand);

		SET @SQLCommand = 'INSERT INTO #StubDataEntry(GlobalBatchID,TransactionID,TransactionSequence,BatchSequence,GlobalStubID,'+@ColumnList+')
			SELECT	Stubs.GlobalBatchID, 
					Stubs.TransactionID,
					Stubs.TransactionSequence,
					Stubs.BatchSequence,
					Stubs.GlobalStubID,
			' + @ColumnList + '
			FROM	dbo.Batch AS IBatch
					INNER JOIN RecHub.Batch AS RBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
					INNER JOIN dbo.Stubs ON dbo.Stubs.GlobalBatchID = RBatch.GlobalBatchID
					INNER JOIN dbo.StubsDataEntry ON dbo.StubsDataEntry.GlobalStubID = dbo.Stubs.GlobalStubID;';

		EXEC(@SQLCommand);

		SET @SQLCommand = 'SELECT	#StubDataEntry.GlobalBatchID, 
				#StubDataEntry.TransactionID,
				#StubDataEntry.TransactionSequence,
				#StubDataEntry.BatchSequence,
				#StubDataEntry.GlobalStubID,';

		SET @CaseStatement = '';
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN ' +
			CASE 
				WHEN FieldType = 6 THEN
						CASE ColumnName
						WHEN 'Amount' THEN 'CAST(#StubDataEntry.' + ColumnName + ' AS VARCHAR(128))'
						ELSE 'CASE WHEN PATINDEX(''%[1-9]%'', REVERSE(STR(#StubDataEntry.' + ColumnName + ',60,15))) < PATINDEX(''%.%'', REVERSE(STR(#StubDataEntry.' + ColumnName + ',60,15))) THEN RTRIM(LTRIM(LEFT(STR(#StubDataEntry.' + ColumnName + ',60,15), LEN(STR(#StubDataEntry.' + ColumnName + ',60,15)) - PATINDEX(''%[1-9]%'', REVERSE(STR(#StubDataEntry.' + ColumnName + ',60,15))) + 1))) ELSE RTRIM(LTRIM(LEFT(STR(#StubDataEntry.' + ColumnName + ',60,15), LEN(STR(#StubDataEntry.' + ColumnName + ',60,15)) - PATINDEX(''%.%'', REVERSE(STR(#StubDataEntry.' + ColumnName + ',60,15)))))) END'
						END  
				WHEN FieldType = 7 THEN 'CAST(#StubDataEntry.' + ColumnName + ' AS VARCHAR(128))'
				WHEN FieldType = 11 THEN 'CONVERT(VARCHAR,#StubDataEntry.' + ColumnName + ',120)'
				ELSE 'RTRIM(#StubDataEntry.' + ColumnName + ')'
			END
		FROM #DataEntryColumns;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValue,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValue, ' ;

		SET @CaseStatement = '';
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #StubDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 6 AND (FieldType = 6 and ColumnName <> 'Amount');

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueFloat,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueFloat, '; 

		SET @CaseStatement = ''
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #StubDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 7 or (FieldType = 6 and ColumnName = 'Amount');

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueMoney,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueMoney, '; 

		SET @CaseStatement = ''
		SELECT @CaseStatement = @CaseStatement + ' WHEN ' + CHAR(39) + ColumnName + CHAR(39) + ' THEN #StubDataEntry.' + ColumnName 
		FROM #DataEntryColumns
		WHERE FieldType = 11;

		IF @@ROWCOUNT >= 1
			SET @SQLCommand =  @SQLCommand + '
			CASE ColumnName ' + @CaseStatement + ' END AS DataEntryValueDateTime,'
		ELSE SET @SQLCommand =  @SQLCommand + ' 
			NULL AS DataEntryValueDateTime, ';

		SET @SQLCommand = @SQLCommand + '
				CASE RTRIM(dbo.DESetupFields.TableName)
					WHEN ''Checks'' THEN 0
					WHEN ''ChecksDataEntry'' THEN 1
					WHEN ''Stubs'' THEN 2
					WHEN ''StubsDataEntry'' THEN 3
				END AS TableType,
				RTRIM(FldName) AS ColumnName,
				FldLength AS FieldLength,
				FldDataTypeEnum AS DataType,
				ScreenOrder,
				COALESCE(RTRIM(ReportTitle),RTRIM(FldPrompt),RTRIM(FldName)) AS DisplayName
		FROM	RecHub.Batch AS RBatch
				INNER JOIN #StubDataEntry ON #StubDataEntry.GlobalBatchID = RBatch.GlobalBatchID
				INNER JOIN dbo.Batch AS IBatch ON IBatch.GlobalBatchID = RBatch.GlobalBatchID
				LEFT JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = IBatch.DESetupID AND dbo.DESetupFields.DocumentType = 1
				LEFT JOIN #DataEntryColumns ON #DataEntryColumns.ColumnName = dbo.DESetupFields.FldName;';

		INSERT INTO RecHub.DataEntryPivot
		(
			GlobalBatchID,
			TransactionID,
			TransactionSequence,
			BatchSequence,
			GlobalID,
			DataEntryValue,
			DataEntryValueFloat,
			DataEntryValueMoney,
			DataEntryValueDateTime,
			TableType,
			ColumnName,
			FieldLength,
			DataType,
			ScreenOrder,
			DisplayName
		)
		EXEC (@SQLCommand);
	END

	DELETE
	FROM 
		RecHub.DataEntryPivot
	WHERE 
		DataEntryValue IS NULL;

	IF( SELECT COUNT(GlobalBatchID) FROM RecHub.DataEntryPivot) > 0
		SET @parmPendingDataEntry = 1;
	ELSE SET @parmPendingDataEntry = 0;

	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CheckDataEntry')) 
		DROP TABLE #CheckDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubDataEntry')) 
		DROP TABLE #StubDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DataEntryColumns')) 
		DROP TABLE #DataEntryColumns;

END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CheckDataEntry')) 
		DROP TABLE #CheckDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StubDataEntry')) 
		DROP TABLE #StubDataEntry;
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#DataEntryColumns')) 
		DROP TABLE #DataEntryColumns;

	EXEC dbo.usp_WfsRethrowException;
END CATCH

