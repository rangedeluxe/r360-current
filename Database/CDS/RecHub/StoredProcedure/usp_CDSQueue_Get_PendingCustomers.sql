--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingCustomers
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_CDSQueue_Get_PendingCustomers') IS NOT NULL
       DROP PROCEDURE RecHub.usp_CDSQueue_Get_PendingCustomers
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_CDSQueue_Get_PendingCustomers
(
	@parmPendingCustomers BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/06/2013
*
* Purpose: Retreive list of Customers that are ready for processing.
*
*	Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* Modification History
* 03/06/2013 WI 99963 JPB	Created
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON

BEGIN TRY
	;WITH PendingList AS
	(
		SELECT 	TOP(150000) MIN(CDSQueueID) AS RecordID, BankID, CustomerID
		FROM 	RecHub.CDSQueue
		WHERE 	QueueType = 30
				AND QueueStatus <= 20
		GROUP BY BankID, CustomerID
		ORDER BY RecordID
	)
	INSERT INTO RecHub.Customers
	(
		Customer_Id,
		BankID,
		CustomerID
	)
	SELECT	RecordID,
			RecHub.CDSQueue.BankID,
			RecHub.CDSQueue.CustomerID
	FROM	RecHub.CDSQueue 
			INNER JOIN PendingList ON RecHub.CDSQueue.CDSQueueID = PendingList.RecordID
	WHERE 	(QueueStatus = 10) OR (QueueStatus = 20) OR (QueueStatus = 15 AND RecHub.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RetryCount),GETDATE()));

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingCustomers = 1;
		UPDATE	RecHub.CDSQueue
		SET		QueueStatus = 20,
				RecHub.CDSQueue.ModificationDate = GETDATE(),
				RecHub.CDSQueue.ModifiedBy = SUSER_SNAME()
		FROM	RecHub.CDSQueue
				INNER JOIN RecHub.Customers ON RecHub.Customers.Customer_Id = RecHub.CDSQueue.CDSQueueID;
	END
	ELSE SET @parmPendingCustomers = 0;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
