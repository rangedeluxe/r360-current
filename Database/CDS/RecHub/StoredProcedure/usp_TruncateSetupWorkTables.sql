--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_TruncateSetupWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_TruncateSetupWorkTables') IS NOT NULL
       DROP PROCEDURE RecHub.usp_TruncateSetupWorkTables;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_TruncateSetupWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/33/2013
*
* Purpose: Truncate tables used by SSIS for import Setup Data.
*
* Modification History
* 02/22/2013 WI 99981 JPB	Created
******************************************************************************/
SET ARITHABORT ON; 
SET NOCOUNT ON;
BEGIN TRY
	TRUNCATE TABLE RecHub.SetupResponses;
	TRUNCATE TABLE RecHub.Banks;
	TRUNCATE TABLE RecHub.Customers;
	TRUNCATE TABLE RecHub.Lockboxes;
	TRUNCATE TABLE RecHub.DataEntrySetups;
	TRUNCATE TABLE RecHub.DocumentTypes;
	TRUNCATE TABLE RecHub.SiteCodes;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
