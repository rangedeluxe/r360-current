--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_FITGetPendingNotifications
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_FITGetPendingNotifications') IS NOT NULL
       DROP PROCEDURE RecHub.usp_FITGetPendingNotifications
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_FITGetPendingNotifications 
(
	@parmRetentionDays	INT,
	@parmMaxRows		INT = 5
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/06/2012
*
* Purpose: Allows ICON to submit a client setup file directly.
*
* Modification History
* 09/30/2016 PT #127604053 JBS Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

BEGIN TRY

	SELECT TOP (CASE WHEN @parmMaxRows IS NOT NULL AND @parmMaxRows > 0 THEN @parmMaxRows ELSE 5 END)
		   Notifications.NotificationID,
		   Notifications.NotificationDate,
		   Notifications.NotificationDeliveryDate,
		   Notifications.MessageText,
		   Contacts.BankID,
		   -1 As CustomerID,
		   Contacts.LockboxID,
		   RenderedFiles.FileID,
		   RenderedFiles.FileDescription,
		   RenderedFiles.FileType,
		   RenderedFiles.FilePathName
	FROM
		dbo.Contacts 
			INNER JOIN 
			dbo.Alerts
				INNER JOIN dbo.EventRules ON 
					dbo.Alerts.EventRuleID = dbo.EventRules.EventRuleID
				INNER JOIN dbo.DeliveryMethods ON
					dbo.Alerts.DeliveryMethodID = dbo.DeliveryMethods.DeliveryMethodID 
				INNER JOIN dbo.Notifications 
					LEFT OUTER JOIN dbo.NotificationFiles 
						INNER JOIN dbo.RenderedFiles ON
							dbo.NotificationFiles.FileID = dbo.RenderedFiles.FileID ON
						dbo.Notifications.NotificationID = dbo.NotificationFiles.NotificationID ON
					dbo.Alerts.AlertID = dbo.Notifications.AlertID 
					ON
				dbo.Contacts.ContactID = dbo.Alerts.ContactID
	WHERE
		dbo.EventRules.RuleIsActive = 1
		AND dbo.Notifications.NotificationDeliveryStatus = 4
		AND dbo.Notifications.RedundantNotification = 0
		AND ((dbo.EventRules.OnlineViewable = 1 OR dbo.EventRules.OnlineModifiable = 1) OR
		dbo.DeliveryMethods.DeliveryMethodName = 'Internet')
		AND dbo.Notifications.NotificationDeliveryDate >= DATEADD(dd, -@parmRetentionDays, GetDate())
		AND dbo.Contacts.BankID > -1
		AND dbo.Notifications.NotificationID NOT IN( 
			SELECT RecHub.FITNotificationQueue.NotificationID 
			FROM RecHub.FITNotificationQueue 
			WHERE RecHub.FITNotificationQueue.QueueStatus > 10 
		)
	ORDER BY 
		  dbo.Notifications.NotificationDate, 
		  dbo.Notifications.NotificationID

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException
END CATCH