--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_FITUpdateNotificationQueueStatusComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_FITUpdateNotificationQueueStatusComplete') IS NOT NULL
       DROP PROCEDURE RecHub.usp_FITUpdateNotificationQueueStatusComplete
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_FITUpdateNotificationQueueStatusComplete 
(
	@parmSourceTrackingID	UNIQUEIDENTIFIER,
	@parmCurrentQueueStatus INT,
	@parmNewQueueStatus		INT,
	@parmIncrementRetryCount BIT,
	@parmRowsAffected		INT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JMC
* Date: 09/06/2012
*
* Purpose: Allows ICON to submit a client setup file directly to OLTA.
*
* Modification History
* 09/30/2016 PT #127604053 JBS Created
******************************************************************************/
SET ARITHABORT ON

BEGIN TRY

	UPDATE RecHub.FITNotificationQueue
	SET
		QueueStatus = @parmNewQueueStatus,
		RetryCount = 
			CASE 
				WHEN @parmIncrementRetryCount > 0 THEN RetryCount+1 
				ELSE RetryCount 
			END,
		ModificationDate = GETDATE(),
		ModifiedBy = SUSER_SNAME()
	WHERE SourceTrackingID = @parmSourceTrackingID
		AND QueueStatus = @parmCurrentQueueStatus
	
	SET @parmRowsAffected = @@ROWCOUNT

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException
END CATCH