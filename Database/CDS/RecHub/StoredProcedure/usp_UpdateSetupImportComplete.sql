--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_UpdateSetupImportComplete
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_UpdateSetupImportComplete') IS NOT NULL
       DROP PROCEDURE RecHub.usp_UpdateSetupImportComplete;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_UpdateSetupImportComplete
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/11/2013
*
* Purpose: Updating setup in process to completed.
*
* Modification History
* 02/11/2013 CR 99982 JPB	Created
* 11/12/2014 WI 177517 JPB	Added retry logic.
* 08/12/2016 PT 127604123	JPB	Consolidate Results node, removed duplicate delete 
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

DECLARE @XMLResponse XML,
		@Loop INT,
		@RecordCount INT,
		@RetryCount TINYINT,
		@ResponseStatus TINYINT,
		@CDSQueueID BIGINT;

BEGIN TRY

	DECLARE @ReponseList TABLE 
	(
		RowID INT IDENTITY(1,1),
		CDSQueueID BIGINT,
		ResponseStatus TINYINT
	);

	INSERT INTO @ReponseList(CDSQueueID,ResponseStatus)
	SELECT 
		CDSQueueID,
		MIN(ResponseStatus) AS ResponseStatus
	FROM
		RecHub.SetupResponses
	GROUP BY CDSQueueID;

	SELECT @RecordCount = @@ROWCOUNT, @Loop = 1;

	WHILE( @Loop <= @RecordCount )
	BEGIN

		SELECT 
			@CDSQueueID = RecHub.SetupResponses.CDSQueueID,
			@ResponseStatus = RL.ResponseStatus,
			@RetryCount = RecHub.CDSQueue.RetryCount
		FROM 
			@ReponseList RL
			INNER JOIN RecHub.SetupResponses ON RecHub.SetupResponses.CDSQueueID = RL.CDSQueueID
			INNER JOIN RecHub.CDSQueue ON RecHub.CDSQueue.CDSQueueID = RL.CDSQueueID
		WHERE 
			RowID = @Loop;

		SELECT @XMLResponse = 
		(
			SELECT	(
						SELECT CASE	@ResponseStatus
							WHEN 0 THEN 'Success'
							WHEN 1 THEN 'Failure' 
							WHEN 2 THEN 'Warning'
						END AS 'Results/*'
						FROM 
							@ReponseList
						WHERE  
							CDSQueueID = T.CDSQueueID
						FOR XML PATH(''), TYPE
					),
					(
						SELECT	
							CASE 
								WHEN @RetryCount < 9 THEN ResponseMessage + ' Re-queued.'
								ELSE ResponseMessage
								END AS 'ErrorMessage/*',
							NULL
						FROM	
							RecHub.SetupResponses
						WHERE  
							RecHub.SetupResponses.CDSQueueID = T.CDSQueueID
						FOR XML PATH(''), TYPE
					)
			FROM 
				@ReponseList T
			WHERE 
				T.CDSQueueID = @CDSQueueID
			FOR XML PATH('ImportResults'), TYPE
		);


		UPDATE
			RecHub.CDSQueue
		SET 
			XMLResponseDocument = @XMLResponse,
			QueueStatus = CASE @ResponseStatus
				WHEN 1 THEN CASE WHEN @RetryCount < 9 THEN 15 ELSE 30 END
				WHEN 2 THEN 99
				ELSE 99
			END,
			ResponseStatus = 
				CASE 
					WHEN RetryCount < 9 THEN 2
					ELSE @ResponseStatus
				END,
			RetryCount = CASE @ResponseStatus 
				WHEN 1 THEN @RetryCount + 1 
				ELSE @RetryCount 
			END,
			ModificationDate = GETDATE(),
			ModifiedBy = SUSER_NAME()
		WHERE
			RecHub.CDSQueue.CDSQueueID = @CDSQueueID;

		UPDATE 
			RecHub.SetupResponses
		SET
			ResponseStatus = 
				CASE 
					WHEN @RetryCount < 9 THEN 2
					ELSE @ResponseStatus
				END
		WHERE
			CDSQueueID = @CDSQueueID;
			
		SET @Loop = @Loop + 1;
	END	

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
