--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Get_PendingBatches
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_CDSQueue_Get_PendingBatches') IS NOT NULL
       DROP PROCEDURE RecHub.usp_CDSQueue_Get_PendingBatches
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_CDSQueue_Get_PendingBatches
(
	@parmPendingBatches BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/19/2013
*
* Purpose: Notify SSB that some action is required on a dbo.batch record.
*	Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* Modification History
* 02/19/2013 WI 100317 JPB	Created
* 11/12/2013 WI 122242 JPB	Update to compare date part only of Processing/Deposit Date
* 11/14/2013 WI 122437 JPB	Determine payment type
* 01/02/2014 WI 125979 JBS	Changing the Default date value if NULL is passed in for Deposit Date for ActionCode = 4
*							Removed the DepositDate from the Join( not needed), also created some default values if Batch not present
* 03/26/2014 WI 133347 DLD  Added DDA information; DepositDDA and BankABA.
* 06/15/2014 WI 148167 JPB	Replaced BatchID with SourceBatchID, added BatchNumber.
* 01/08/2016 WI 253716 JPB	Get BatchSourceName from CDS queue instead of batch records.
* 06/09/2016 WI 285726 JPB	Only select Batch Data Complete, Batch Complete With Images and Batch Delete.
* 01/06/2017 PT#136908915 JPB	Set BatchCueID to 0 if NULL.
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;
DECLARE @DepositDateDefault DateTime;
SET @DepositDateDefault = '01/01/1899';  --  WI 125979 - default if NULL is passed in for deposit date 

BEGIN TRY
	;WITH BatchList AS
	(
		SELECT 	TOP(150000) 
			MIN(CDSQueueID) AS RecordID,
			BankID,
			LockboxID,
			BatchID,
			ProcessingDate,
			DepositDate,
			CAST(CONVERT(VARCHAR,ProcessingDate,112) AS INT) ProcessingDateKey,
			CAST(CONVERT(VARCHAR,DepositDate,112) AS INT) DepositDateKey
		FROM 	
			RecHub.CDSQueue
		WHERE 	
			QueueType = 10
			AND QueueStatus <= 20
			AND ActionCode IN (1,3,4)
		GROUP BY 
			BankID,
			LockboxID,
			BatchID,
			ProcessingDate,
			DepositDate
		ORDER BY 
			RecordID
	)
	INSERT INTO RecHub.Batch
	(
		Batch_Id,
		BatchStatus,
		ActionCode,
		GlobalBatchID,
		BankID,
		CustomerID,
		LockboxID,
		DepositDate,
		ProcessingDate,
		SourceBatchID,
		BatchNumber,
		BatchCueID,
		DepositStatus,
		BatchSourceShortName,
		BatchPaymentShortName,
		SystemType,
		BatchSiteCode,
		DepositDDA,    -- WI 133347 Added DDA information 
		BankABA        -- WI 133347 Added DDA information 
	)
	SELECT	
		RecordID,
		20,
		RecHub.CDSQueue.ActionCode,
		COALESCE(dbo.Batch.GlobalBatchID, -1),  -- WI 125979 This is for situation if no batch is present we will default to -1
		BatchList.BankID,
		COALESCE(dbo.Lockbox.CustomerID,-1) AS CustomerID,
		BatchList.LockboxID,
		CASE 
		WHEN BatchList.DepositDate IS NOT NULL THEN BatchList.DepositDate
		WHEN BatchList.DepositDate IS NULL AND RecHub.CDSQueue.ActionCode = '4' THEN @DepositDateDefault
		ELSE BatchList.ProcessingDate
		END,
		BatchList.ProcessingDate,
		BatchList.BatchID AS SourceBatchID,
		BatchList.BatchID AS BatchNumber,
		COALESCE(dbo.Batch.BatchCueID,0),
		COALESCE(dbo.Batch.DepositStatus, -1),  -- WI 125979 This is for situation if no batch is present we will default to -1  
		UPPER(ISNULL(RecHub.CDSQueue.BatchSourceShortName,'Unknown')),
		CASE 
			WHEN UPPER(RecHub.CDSQueue.BatchSourceShortName) = 'ACH' THEN CAST('ACH' AS VARCHAR(30)) 
			ELSE CAST('LOCKBOX' AS VARCHAR(30)) 
		END	AS BatchPaymentType,
		ISNULL(dbo.Batch.SystemType,0),
		dbo.Batch.SiteCode,
		LTRIM(RTRIM(COALESCE(dbo.Batch.DepositDDA,dbo.Lockbox.DDA,dbo.BatchTypes.DDA))), -- WI 133347 Added DDA information
		COALESCE(dbo.Bank.BankABA,'000000000') -- WI 133347 Default BankABA to nine zeros when the value is null.
	FROM	
		RecHub.CDSQueue 
		INNER JOIN BatchList ON RecHub.CDSQueue.CDSQueueID = BatchList.RecordID
		LEFT JOIN dbo.Batch ON dbo.Batch.BankID = BatchList.BankID
			AND dbo.Batch.LockboxID = BatchList.LockboxID
			AND dbo.Batch.BatchID = BatchList.BatchID
			AND CAST(CONVERT(VARCHAR,dbo.Batch.ProcessingDate,112) AS INT) = BatchList.ProcessingDateKey
		LEFT JOIN dbo.Lockbox ON dbo.Lockbox.BankID = RecHub.CDSQueue.BankID AND dbo.Lockbox.LockboxID = RecHub.CDSQueue.LockboxID
		LEFT JOIN dbo.Bank ON dbo.Bank.BankID = RecHub.CDSQueue.BankID -- WI 133347 Retrieve BankABA number for DDA information.
		LEFT JOIN dbo.BatchTypes ON dbo.BatchTypes.BankID = dbo.Batch.BankID -- WI 133347 get DDA info
			AND dbo.BatchTypes.LockboxID = dbo.Batch.LockboxID
			AND dbo.BatchTypes.BatchTypeID = dbo.Batch.BatchTypeID
	WHERE 	
		(QueueStatus = 10) OR (QueueStatus = 20) 
		OR (QueueStatus = 15 AND RecHub.CDSQueue.ModificationDate <= DATEADD(SECOND,(120*RetryCount),GETDATE()));

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @parmPendingBatches = 1;
		UPDATE	RecHub.CDSQueue
		SET		QueueStatus = 20,
				RecHub.CDSQueue.ModificationDate = GETDATE(),
				RecHub.CDSQueue.ModifiedBy = SUSER_SNAME()
		FROM	RecHub.CDSQueue
				INNER JOIN RecHub.Batch ON RecHub.Batch.Batch_Id = RecHub.CDSQueue.CDSQueueID;
	END
	ELSE SET @parmPendingBatches = 0;

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
