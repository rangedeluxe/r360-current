--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_DESetupFields_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_DESetupFields_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_DESetupFields_Get;
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_DESetupFields_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/08/2013
*
* Purpose: Retrieve data entry setup information to be processed by the 
*		integraPAY Import SSIS Package.
*
* Modification History
* 03/08/2013 WI 99970 JPB	Created
* 09/27/2015 WI 237952 JPB	Updates to support WorkGroupDataEntryColumns
* 10/08/2015 WI 238992 JBS	Change IsActive = 1, added Distinct
* 05/17/2016 WI 281419 JPB	Replace calculated UILabel with SourceDisplayName
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
	;WITH MatchingRecords AS
	(
		SELECT
			DataEntrySetup_Id,
			RecHub.DataEntrySetups.DESetupID,
			dbo.Lockbox.BankID AS SiteBankID,
			dbo.Lockbox.CustomerID AS SiteOrganizationID,
			dbo.Lockbox.LockboxID AS SiteClientAccountID
		FROM 
			RecHub.DataEntrySetups
			LEFT JOIN dbo.Lockbox ON dbo.Lockbox.DESetupID = RecHub.DataEntrySetups.DESetupID
		WHERE dbo.Lockbox.BankID IS NOT NULL
		UNION ALL
		SELECT
			DataEntrySetup_Id,
			RecHub.DataEntrySetups.DESetupID,
			dbo.BatchTypes.BankID AS SiteBankID,
			dbo.BatchTypes.CustomerID AS SiteOrganizationID,
			dbo.BatchTypes.LockboxID AS SiteClientAccountID
		FROM 
			RecHub.DataEntrySetups
			LEFT JOIN dbo.BatchTypes ON dbo.BatchTypes.DESetupID = RecHub.DataEntrySetups.DESetupID
		WHERE dbo.BatchTypes.BankID IS NOT NULL
	),
	NonMatchingRecords AS
	(
		SELECT DataEntrySetup_Id
		FROM RecHub.DataEntrySetups
		EXCEPT 
		SELECT DataEntrySetup_Id
		FROM MatchingRecords
	),
	DESetupIDs AS
	(
		SELECT
			RecHub.DataEntrySetups.DataEntrySetup_Id,
			RecHub.DataEntrySetups.DESetupID,
			SiteBankID,
			SiteOrganizationID,
			SiteClientAccountID
		FROM MatchingRecords
			INNER JOIN RecHub.DataEntrySetups ON RecHub.DataEntrySetups.DataEntrySetup_Id = MatchingRecords.DataEntrySetup_Id
		UNION ALL
		SELECT
			RecHub.DataEntrySetups.DataEntrySetup_Id,
			RecHub.DataEntrySetups.DESetupID,
			NULL,
			NULL,
			NULL
		FROM NonMatchingRecords
			INNER JOIN RecHub.DataEntrySetups ON RecHub.DataEntrySetups.DataEntrySetup_Id = NonMatchingRecords.DataEntrySetup_Id
	)
	SELECT	DISTINCT
		DataEntrySetup_Id,
		SiteBankID,
		SiteClientAccountID,
		CASE UPPER(dbo.DESetupFields.TableName)
				WHEN 'CHECKS' THEN 1
				WHEN 'CHECKSDATAENTRY' THEN 1
				WHEN 'STUBS' THEN 0
				WHEN 'STUBSDATAENTRY' THEN 0
		END AS IsCheck,
		1 AS IsActive,		
		0 AS MarkSense,
		dbo.DESetupFields.FldDataTypeEnum AS DataType,
		dbo.DESetupFields.ScreenOrder,
		CAST(COALESCE(RTRIM(dbo.DESetupFields.ReportTitle),RTRIM(dbo.DESetupFields.FldPrompt),RTRIM(dbo.DESetupFields.FldName)) AS NVARCHAR(64)) AS UILabel,
		CAST(RTRIM(dbo.DESetupFields.FldName) AS NVARCHAR(256)) AS FieldName,
		CAST(COALESCE(RTRIM(dbo.DESetupFields.ReportTitle),RTRIM(dbo.DESetupFields.FldPrompt),RTRIM(dbo.DESetupFields.FldName)) AS NVARCHAR(256)) AS SourceDisplayName,
		CAST(SourceShortName+'-I'+CAST(SiteBankID AS VARCHAR(10)) AS VARCHAR(64)) AS BatchSourceShortName
	FROM DESetupIDs
		INNER JOIN dbo.DESetupFields ON dbo.DESetupFields.DESetupID = DESetupIDs.DESetupID
		CROSS APPLY (SELECT SourceShortName FROM dbo.BatchSource) AS SourceShortName;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
