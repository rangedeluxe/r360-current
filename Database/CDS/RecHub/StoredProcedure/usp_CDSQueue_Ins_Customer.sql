--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_CDSQueue_Ins_Customer
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_CDSQueue_Ins_Customer') IS NOT NULL
       DROP PROCEDURE RecHub.usp_CDSQueue_Ins_Customer
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_CDSQueue_Ins_Customer
(
	@parmBankID	INT,
	@parmCustomerID INT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 03/08/2013
*
* Purpose: Insert customer record into CDSQueue.
*
* Action codes could be:
*	1 - Batch Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated
*
* Modification History
* 03/08/2013 WI 100283 JPB	Created
******************************************************************************/
SET NOCOUNT ON;
SET ARITHABORT ON;

DECLARE @RetryCount		INT,
		@ErrorMessage	NVARCHAR(4000),
		@ErrorSeverity	INT,
		@ErrorState		INT;

BEGIN TRY

	SET @RetryCount = 4;
	
	WHILE( @RetryCount >= 0 )
	BEGIN
		BEGIN TRY
			
			INSERT INTO RecHub.CDSQueue
			(
				BankID,
				CustomerID,
				ActionCode,
				QueueStatus,
				QueueType
			)
			VALUES
			(
				@parmBankID,
				@parmCustomerID,
				1,
				10,
				30

			);
			
			SET @RetryCount = -1;
		END TRY
		BEGIN CATCH
			/* 
				Catch deadlock condition 
				If a deadlock is found, wait a few seconds and try again
				If the retry count is exceeded, pass the deadlock error to the main catch block
				If it is not a deadlock, pass control to the main catch block and 
					return that information to the application.
			*/
			
			IF (ERROR_NUMBER() = 1205) AND (@RetryCount > 0)
			BEGIN
				SET @RetryCount = @RetryCount - 1;
				WAITFOR DELAY '00:00:05'; /* wait 5 seconds and try again */
			END
			ELSE
			BEGIN
				SET @RetryCount = -1;		
				SELECT	@ErrorMessage = ERROR_MESSAGE(),
						@ErrorSeverity = ERROR_SEVERITY(),
						@ErrorState = ERROR_STATE();
				RAISERROR(@ErrorMessage,@ErrorSeverity,@ErrorState);
			END
		END CATCH
	END
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
