--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorStoredProcedureName usp_Transactions_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_Transactions_Get') IS NOT NULL
       DROP PROCEDURE RecHub.usp_Transactions_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_Transactions_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/25/2013
*
* Purpose: Retreive transactions that are ready for processing.
*
* Modification History
* 02/25/2013 WI 100324 JPB	Created
* 02/01/2016 WI 261215 JPB	Do not return ActionCode 4 items
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT	
		RecHub.Batch.Batch_Id,
		dbo.Transactions.TransactionID,
		dbo.Transactions.Sequence AS TxnSequence
	FROM	
		dbo.Transactions
		INNER JOIN RecHub.Batch ON dbo.Transactions.GlobalBatchID = RecHub.Batch.GlobalBatchID
	WHERE
		ActionCode <> 4;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH		
