--WFSScriptProcessorSchema RecHub
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_TruncateBatchWorkTables
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHub.usp_TruncateBatchWorkTables') IS NOT NULL
       DROP PROCEDURE RecHub.usp_TruncateBatchWorkTables
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHub.usp_TruncateBatchWorkTables
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/33/2013
*
* Purpose: Truncate tables used by SSIS for import Batch Data.
*
* Modification History
* 02/22/2013 WI 102082 JPB	Created
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 
BEGIN TRY
	TRUNCATE TABLE RecHub.Batch;
	TRUNCATE TABLE RecHub.BatchResponses;
	TRUNCATE TABLE RecHub.DataEntryPivot;
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
