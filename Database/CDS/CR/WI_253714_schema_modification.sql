--WFSScriptProcessorPrint WI 253714
--WFSScriptProcessorPrint Add BatchSourceShortName to CDSQueue if necessary
--WFSScriptProcessorCRBegin
IF NOT EXISTS(	SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'RecHub' AND TABLE_NAME = 'CDSQueue' AND COLUMN_NAME = 'BatchSourceShortName' )
BEGIN
	RAISERROR('Dropping IMSInterfaceQueue contraints',10,1) WITH NOWAIT;
	IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_SCHEMA='RecHub' AND TABLE_NAME='CDSQueue' AND CONSTRAINT_NAME='PK_CDSQueue' )
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT PK_CDSQueue;

	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_CDSQueue_QueueType')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT CK_CDSQueue_QueueType;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_CDSQueue_QueueStatus')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT CK_CDSQueue_QueueStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ResponseStatus')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_ResponseStatus;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'CK_CDSQueue_ActionCode')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT CK_CDSQueue_ActionCode;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_RetryCount')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_RetryCount;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_NotifyIMS')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_NotifyIMS;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_CreationDate')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_CreationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_CreatedBy')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_CreatedBy;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ModificationDate')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_ModificationDate;
	IF EXISTS (SELECT 1 FROM sys.objects WHERE name = 'DF_CDSQueue_ModifiedBy')
		ALTER TABLE RecHub.CDSQueue DROP CONSTRAINT DF_CDSQueue_ModifiedBy;

	RAISERROR('Rebuilding RecHub.CDSQueue',10,1) WITH NOWAIT;
	EXEC sp_rename 'RecHub.CDSQueue', 'OLDCDSQueue';


	CREATE TABLE RecHub.CDSQueue
	(
		CDSQueueID BIGINT NOT NULL IDENTITY(1,1)
			CONSTRAINT PK_CDSQueue PRIMARY KEY CLUSTERED,
		QueueType TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_QueueType CHECK(QueueType IN (10,20,30,40,50,60,70)), 
		QueueStatus TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_QueueStatus CHECK(QueueStatus IN (10,15,20,30,99)), 
		ResponseStatus TINYINT NOT NULL
			CONSTRAINT DF_CDSQueue_ResponseStatus DEFAULT 255,
		ActionCode TINYINT NOT NULL
			CONSTRAINT CK_CDSQueue_ActionCode CHECK(ActionCode IN (1,2,3,4,5,6)),
		RetryCount TINYINT NOT NULL
			CONSTRAINT DF_CDSQueue_RetryCount DEFAULT 0,
		NotifyIMS BIT NOT NULL
			CONSTRAINT DF_CDSQueue_NotifyIMS DEFAULT 0,
		CreationDate DATETIME NOT NULL
			CONSTRAINT DF_CDSQueue_CreationDate DEFAULT GETDATE(),
		CreatedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_CDSQueue_CreatedBy DEFAULT SUSER_NAME(),
		ModificationDate DATETIME NOT NULL
			CONSTRAINT DF_CDSQueue_ModificationDate DEFAULT GETDATE(),
		ModifiedBy VARCHAR(128) NOT NULL
			CONSTRAINT DF_CDSQueue_ModifiedBy DEFAULT SUSER_NAME(),
		BankID INT NULL,
		CustomerID INT NULL,
		LockboxID INT NULL,
		BatchID INT NULL,
		SiteCode INT NULL,
		DESetupID INT NULL,
		ProcessingDate DATETIME NULL,
		DepositDate DATETIME NULL,
		BatchSourceShortName VARCHAR(10) NULL,
		FileDescriptor VARCHAR(30) NULL,
		XMLResponseDocument XML NULL
	);

	IF EXISTS(SELECT 1 FROM fn_listextendedproperty('Table_Description','SCHEMA', 'RecHub', 'TABLE', 'CDSQueue', default, default) )
		EXEC sys.sp_dropextendedproperty 
			@name = N'Table_Description',
			@level0type = N'SCHEMA',
			@level0name = N'RecHub',
			@level1type = N'TABLE',
			@level1name = N'CDSQueue';		

	EXEC sys.sp_addextendedproperty 
		@name = N'Table_Description',
		@level0type = N'SCHEMA',@level0name = RecHub,
		@level1type = N'TABLE',@level1name = CDSQueue,
		@value = N'/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/19/2013
*
* Purpose: Queue items to be processed by the intergraPAY import SSIS packages.
*		   
*
* ActionCode:
*	1 - Batch Data Complete/Setup Data Complete
*	2 - Images Complete
*	3 - Batch Data Complete No Images coming
*	4 - Batch Deleted
*	5 - Batch Data Updated
*	6 - Batch Images Updated

* QueueStatus:
*	10 - Ready to sent to RH360
*	15 - Failed processing on RH360 - but can resend
*	20 - Sent to RH360, waiting for a response
*	30 - Failed processing on RH360 - but cannot resend
*	99 - Successfully processed on RH360
*
*
* QueueType:
*	10 - Batch
*	20 - Bank
*	30 - Customer
*	40 - Lockbox
*	50 - SiteCode
*	60 - DESetup
*	70 - Document Type
*
* 
* Modification History
* 02/19/2013 WI 99974 JPB	Created
* 01/08/2016 WI XXXXX JPB	Added BatchSourceShortName
******************************************************************************/';

	RAISERROR('Copying data from old table to rebuilt table.',10,1) WITH NOWAIT 

	SET IDENTITY_INSERT RecHub.CDSQueue ON;

	/* NOTE: Only the last 7 days of records and all error records are being saved */
	INSERT INTO RecHub.CDSQueue
	(
		CDSQueueID,
		QueueType, 
		QueueStatus, 
		ResponseStatus,
		ActionCode,
		RetryCount,
		NotifyIMS,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		BankID,
		CustomerID,
		LockboxID,
		BatchID,
		SiteCode,
		DESetupID,
		ProcessingDate,
		DepositDate,
		FileDescriptor,
		XMLResponseDocument
	)
	SELECT
		CDSQueueID,
		QueueType, 
		QueueStatus, 
		ResponseStatus,
		ActionCode,
		RetryCount,
		NotifyIMS,
		CreationDate,
		CreatedBy,
		ModificationDate,
		ModifiedBy,
		BankID,
		CustomerID,
		LockboxID,
		BatchID,
		SiteCode,
		DESetupID,
		ProcessingDate,
		DepositDate,
		FileDescriptor,
		XMLResponseDocument
	FROM
		RecHub.OLDCDSQueue
	WHERE 
		DATEDIFF(DAY,CreationDate,GETDATE()) <= 7 
		OR QueueStatus = 30;

	SET IDENTITY_INSERT RecHub.CDSQueue OFF;

	RAISERROR('Creating index IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID',10,1) WITH NOWAIT;
	CREATE INDEX IDX_CDSQueue_QueueTypeQueueStatusCDSQueueID ON RecHub.CDSQueue 
	(
		QueueType,
		QueueStatus,
		CDSQueueID
	)
	INCLUDE
	(
		BankID,
		CustomerID,
		LockboxID,
		BatchID,
		ProcessingDate,
		DepositDate,
		SiteCode,
		DESetupID,
		FileDescriptor
	);

	RAISERROR('Creating index IDX_CDSQueue_QueueStatusModificationDateRetryCount',10,1) WITH NOWAIT;
	CREATE NONCLUSTERED INDEX IDX_CDSQueue_QueueStatusModificationDateRetryCount ON RecHub.CDSQueue
	(
		QueueStatus,
		ModificationDate,
		RetryCount
	)
	INCLUDE
	(
		BankID,
		CustomerID,
		LockboxID,
		BatchID,
		ProcessingDate,
		DepositDate,
		SiteCode,
		DESetupID,
		FileDescriptor,
		ActionCode
	);

	IF OBJECT_ID('RecHub.OLDCDSQueue') IS NOT NULL
		DROP TABLE RecHub.OLDCDSQueue;

END
ELSE
	RAISERROR('WI has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd
