--WFSScriptProcessorPrint WI 113664
--WFSScriptProcessorPrint Adding R360Exception to dbo.BatchTrace if necessary.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'BatchTrace' AND COLUMN_NAME = 'R360Exception' )
BEGIN
	ALTER TABLE dbo.BatchTrace ADD R360Exception DATETIME NULL
END
ELSE
	RAISERROR('WI 113664 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd