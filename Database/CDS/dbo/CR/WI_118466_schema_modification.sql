--WFSScriptProcessorPrint WI 118466
--WFSScriptProcessorPrint Adding RecHub to dbo.Systems if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM dbo.Systems WHERE SystemName='RecHub' )
	INSERT INTO dbo.Systems(SystemName,SystemVersion) VALUES ('RecHub','2.00.00.00');
ELSE
	RAISERROR('WI 118466 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd