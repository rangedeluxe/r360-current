--WFSScriptProcessorPrint WI 246472
--WFSScriptProcessorPrint Removing trigger dbo.RecHubCustomer.
--WFSScriptProcessorCRBegin
--WFSScriptProcessorCR
IF OBJECT_ID('dbo.RecHubCustomer', 'TR') IS NOT NULL
BEGIN
	DROP TRIGGER dbo.RecHubCustomer;
END
ELSE
	RAISERROR('WI 246472 has already been applied to the database.',10,1) WITH NOWAIT;
--WFSScriptProcessorCREnd

