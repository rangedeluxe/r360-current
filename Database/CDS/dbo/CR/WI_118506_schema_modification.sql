--WFSScriptProcessorPrint WI 118506
--WFSScriptProcessorPrint Adding IMSDocumentType to dbo.DocumentTypes if necessary.
--WFSScriptProcessorCRBegin
IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'DocumentTypes' and column_name = 'IMSDocumentType' )
	ALTER TABLE dbo.DocumentTypes ADD IMSDocumentType [varchar] (30) NOT NULL CONSTRAINT [DF__DocumentTypes__IMSDocumentType]  DEFAULT ('Invoice') WITH VALUES
--WFSScriptProcessorCREnd
--WFSScriptProcessorPrint Converting/Updating data for dbo.DocumentTypes.
--WFSScriptProcessorCRBegin
IF EXISTS ( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'DocumentTypes' and column_name = 'IMSDocumentType' )
	UPDATE dbo.DocumentTypes SET IMSDocumentType = 'Per/Bus Check' WHERE [Description] = 'Captured Check'
--WFSScriptProcessorCREnd
