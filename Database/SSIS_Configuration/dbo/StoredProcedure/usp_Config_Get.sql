--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_Config_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_Config_Get') IS NOT NULL
       DROP PROCEDURE dbo.usp_Config_Get
GO

--WFSScriptProcessorStoredProcedureCreated
CREATE PROCEDURE dbo.usp_Config_Get 
(
	@parmFilter NVARCHAR(255)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/13/2016
*
* Purpose: Retrieve config values from the SSIS config table based on filter
*
* Modification History
* 01/13/2016 WI 257881  JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

BEGIN TRY

	SELECT
		SUBSTRING(PackagePath,CHARINDEX(':',PackagePath)+2,CHARINDEX(']',PackagePath)-CHARINDEX(':',PackagePath)-2) AS PackagePath,
		ConfiguredValue,
		ConfiguredValueType
	FROM 
		dbo.SSISConfigurations
	WHERE
		ConfigurationFilter = @parmFilter

END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH