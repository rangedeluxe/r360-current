--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_SSISConfigurations_Select
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_SSISConfigurations_Select') IS NOT NULL
       DROP PROCEDURE dbo.usp_SSISConfigurations_Select
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_SSISConfigurations_Select
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/24/2012
*
* Purpose: Select SSIS Configuration Table
*		   
*
* Modification History
* 04/24/2012 CR  52277  WJS	Created
* 11/27/2013 WI 123767	JBS Update to 2.0 database schema
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY

	SELECT * 
		FROM  dbo.SSISConfigurations;

	RETURN 0
END TRY
BEGIN CATCH
    RETURN 1
END CATCH
