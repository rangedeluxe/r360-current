--WFSScriptProcessorSystemName WFS_SSIS
--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_SSISConfigurations_Update
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_SSISConfigurations_Update') IS NOT NULL
	DROP PROCEDURE dbo.usp_SSISConfigurations_Update
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_SSISConfigurations_Update
(
	@parmSection	NVARCHAR(255),
	@parmSetupKey	NVARCHAR(255),
	@parmValue		NVARCHAR(2048)
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: WJS
* Date: 04/23/2012
*
* Purpose: Update SSIS Configuration Table
*		   
*
* Modification History
* 04/23/2012 CR  52268  WJS	Created
* 11/27/2013 WI 123775	JBS	Update to 2.0 schema
* 04/16/2015 WI	202344	JPB	Updated for 2.01, made more user friendly.
******************************************************************************/
SET NOCOUNT ON

BEGIN TRY

	IF( @parmSection IS NULL )
		RAISERROR('A value for @parmSection must be included',16,1);

	IF( @parmSetupKey IS NULL )
		RAISERROR('A value for @parmSetupKey must be included',16,1);
	
	IF( EXISTS (SELECT 1 FROM dbo.SSISConfigurations WHERE ConfigurationFilter = @parmSection AND PackagePath = '\Package.Variables[User::'+@parmSetupKey +'].Properties[Value]') )
	BEGIN
		UPDATE 
			dbo.SSISConfigurations
		SET
			ConfiguredValue = @parmValue
		WHERE 
			ConfigurationFilter = @parmSection
			AND PackagePath = '\Package.Variables[User::'+@parmSetupKey +'].Properties[Value]';
	END
	ELSE
		RAISERROR('Section %s, SetupKey %s could not be found.',16,1,@parmSection,@parmSetupKey);
			
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException;
END CATCH
