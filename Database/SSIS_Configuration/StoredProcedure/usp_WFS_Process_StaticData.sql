--WFSScriptProcessorSchema dbo
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_WFS_Process_StaticData
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_WFS_Process_StaticData') IS NOT NULL
       DROP PROCEDURE dbo.usp_WFS_Process_StaticData
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE [dbo].[usp_WFS_Process_StaticData]
	@parmStaticData xml,
	@parmSystemVersion VARCHAR(32) = NULL
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 09/29/2011
*
* Purpose: Insert, update or delete static data records passed in as XML. This is not
*	the most efficent SP every written. :( But speed is not important, readablility
*	and maintenance are. This SP is only executed at install/upgrade time.
*
* Modification History
* 09/29/2011 CR 46052 JPB	Created
* 01/24/2012 CR 49627 JPB	Added LoadDate with current date/time if exists.
* 02/17/2012 CR 50309 JPB	FP to SSISConfiguration Database.
* 06/19/2013 WI 105896 JPB	Updated for 2.0.
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON
DECLARE	@SystemVersion 		VARCHAR(32),
		@StaticDataVersion	VARCHAR(32),
		@Loop				INT,
		@SystemVer			BIGINT,
		@DataVer			BIGINT,
		@ActionCode			CHAR(1),
		@ColumnName			VARCHAR(256),
		@ColumnList			VARCHAR(MAX),
		@SQLCommand			VARCHAR(MAX),
		@FullTableName		VARCHAR(512),
		@SchemaName			VARCHAR(256),
		@TableName			VARCHAR(256),
		@ProcessRow			BIT,
		@ModificationDate	BIT,
		@ModifiedBy			BIT,
		@LoadDate			BIT

BEGIN TRY
	/* Remove the temp tables if they exist */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues

	/* Table is altered to look like the static data that is being handled. */
	CREATE TABLE #StaticDataTable 
	(
		RowID INT IDENTITY(1,1),
		[Version] VARCHAR(64),
		ActionCode CHAR(1),
	)

	/* Stored the individual XML nodes */
	CREATE TABLE #StaticDataValues
	(
		RowID INT IDENTITY(1,1),
		StaticData XML
	)

	/* Store the column definations */
	DECLARE @ColumnMapping TABLE
	(
		RowID INT IDENTITY(1,1),
		[Version] VARCHAR(64),
		ColumnName VARCHAR(256),
		KeyValue BIT,
		Updateable BIT,
		DataType VARCHAR(10),
		Size INT
	)

	/* Get the schema/table name from the XML document. */
	SELECT 	@SchemaName = TableInfo.att.value('@Schema','VARCHAR(256)'),
			@TableName = TableInfo.att.value('@Name','VARCHAR(256)')
	FROM	@parmStaticData.nodes('/StaticData/Table') TableInfo(att)
	
	/* See if Modification Date exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'ModificationDate' )
		SET @ModificationDate = 1
	ELSE SET @ModificationDate = 0

	/* See if ModifiedBy Date exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'ModifiedBy' )
		SET @ModifiedBy = 1
	ELSE SET @ModifiedBy = 0

	/* See if Load Date exists in the table */
	IF EXISTS( 	SELECT 1 
				FROM INFORMATION_SCHEMA.COLUMNS 
				WHERE TABLE_SCHEMA = @SchemaName AND TABLE_NAME = @TableName AND COLUMN_NAME = 'LoadDate' )
		SET @LoadDate = 1
	ELSE SET @LoadDate = 0

	/* Create a var with the fully qualified schema and table name as [scheme].[table] */
	SELECT @FullTableName = '[' + @SchemaName + '].[' + @TableName + ']'

	PRINT 'Processing static data for ' + @SchemaName + '.' + @TableName

	/* Get the column definitions from the XML document and store them for use. */
	INSERT INTO @ColumnMapping([Version],ColumnName,KeyValue,Updateable,DataType,Size)
	SELECT	COALESCE(ColumnMap.att.value('@Version', 'VARCHAR(64)'),'1.00.00.00'),
			ColumnMap.att.query('.').value('.', 'VARCHAR(256)'),
			CASE ColumnMap.att.value('@Key', 'VARCHAR(5)')
				WHEN 'TRUE' THEN 1
				ELSE 0
			END AS KeyValue,
			CASE ColumnMap.att.value('@Update', 'VARCHAR(5)')
				WHEN 'TRUE' THEN 1
				ELSE 0
			END AS Updateable,
			ColumnMap.att.value('@DataType','VARCHAR(10)'),
			CASE UPPER(ColumnMap.att.value('@DataType','VARCHAR(10)'))
				WHEN 'VARCHAR' THEN COALESCE(ColumnMap.att.value('@Size', 'VARCHAR(32)'),64)
				WHEN 'CHAR' THEN COALESCE(ColumnMap.att.value('@Size', 'VARCHAR(32)'),64)
				ELSE 0
			END
	FROM @parmStaticData.nodes('/StaticData/Table/ColumnMap/Column') ColumnMap(att)

	/* Get the current database level from the systems table based on the system defined in the XML doc. */
	IF( @parmSystemVersion IS NULL )
	BEGIN /* only go to the Systems table if the system version was not passed in */
		SELECT 	@SystemVersion = SystemVersion
		FROM	dbo.Systems
				INNER JOIN 
				(
					SELECT	StaticData.att.value('@SystemName','VARCHAR(64)') AS SystemName
					FROM	@parmStaticData.nodes('/StaticData') StaticData(att)
				) SD ON dbo.Systems.SystemName = SD.SystemName
	END
	ELSE SET @SystemVersion = @parmSystemVersion
	
	/* Break the system version down into it's parts using a udf */
	SELECT	@SystemVer = FullVersion
	FROM	dbo.udf_ParseSystemVersion(@SystemVersion)

	/* Remove columns that are greater then the current system verison */
	SET @Loop = 1
	WHILE( @Loop <= (SELECT MAX(RowID) FROM @ColumnMapping) )
	BEGIN
		SELECT 	@StaticDataVersion = [Version]
		FROM 	@ColumnMapping
		WHERE	RowID = @Loop
		/* Break the static data version down into it's parts */
		SELECT 	@DataVer = FullVersion
		FROM	dbo.udf_ParseSystemVersion(@StaticDataVersion)

		IF 	@DataVer > @SystemVer
			DELETE FROM @ColumnMapping WHERE RowID = @Loop
		SET @Loop = @Loop + 1;
	END

	/* Get the individual xml nodes (/StaticData/Data) from document passed in. */
	INSERT INTO #StaticDataValues(StaticData)
	SELECT 	StaticData.att.query('.') AS StaticData
	FROM   	@parmStaticData.nodes('/StaticData/Data') StaticData(att);

	/* Build out the StaticDataTable by adding the columns from the column map. */ 
	SET @SQLCommand = 'ALTER TABLE #StaticDataTable ADD '
	SELECT 	@SQLCommand = @SQLCommand + ColumnName
			+CASE DataType
				WHEN 'VARCHAR' THEN ' VARCHAR(' + CAST(Size AS VARCHAR) + ') NULL'
				WHEN 'INTEGER' THEN ' INT NULL'
				WHEN 'INT' THEN ' INT NULL'
				WHEN 'BIT' THEN ' BIT NULL'
				WHEN 'TINYINT' THEN ' TINYINT NULL'
				ELSE ' VARCHAR(64) NULL'
			END
			+ ','
	FROM 	@ColumnMapping

	IF( @@ROWCOUNT > 0 )
	BEGIN /* IF @@ROWCOUNT */
		/* Clean up the SQL command I.E. remove the last , in the string. */
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)
		/* Execute the Alter that was build */
		EXEC (@SQLCommand)

		/* Insert the parsed XML data into the temp table, each column/data type exists so it is 'real' at this point. */
		SET @SQLCommand = 'INSERT INTO #StaticDataTable([Version],ActionCode,'
		SELECT 	@SQLCommand = @SQLCommand + ColumnName + ','
		FROM	@ColumnMapping
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)
		SET @SQLCommand = @SQLCommand + ') '
		SET @SQLCommand = @SQLCommand + 'SELECT COALESCE(StaticData.att.value(' + QUOTENAME('@Version',CHAR(39)) + ',' + QUOTENAME('VARCHAR(64)',CHAR(39)) + '),'+QUOTENAME('1.00.00.00',CHAR(39)) + '),COALESCE(StaticData.att.value('+QUOTENAME('@ActionCode',CHAR(39))+ ',' + QUOTENAME('CHAR(1)',CHAR(39)) + '),' + QUOTENAME('I',CHAR(39))+'),'
		SELECT 	@SQLCommand = @SQLCommand + 'StaticData.att.value(' + CHAR(39) + '@' + ColumnName +  CHAR(39) + ','
				+CASE DataType
					WHEN 'VARCHAR' THEN CHAR(39) + 'VARCHAR(' + CAST(Size AS VARCHAR) + ')' + CHAR(39)
					WHEN 'INTEGER' THEN QUOTENAME('INT',CHAR(39))
					WHEN 'INT' THEN QUOTENAME('INT',CHAR(39))
					WHEN 'BIT' THEN QUOTENAME('BIT',CHAR(39))
					WHEN 'TINYINT' THEN QUOTENAME('TINYINT',CHAR(39))
					ELSE QUOTENAME('VARCHAR(512) NULL',CHAR(39))
				END
				+ '),'
		FROM	@ColumnMapping
		/* Clean up the SQL command I.E. remove the last , in the string. */
		SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)
		/* Add in the name of the table and cross apply so we get the individual parts. */
		SET @SQLCommand = @SQLCommand + ' FROM #StaticDataValues CROSS APPLY StaticData.nodes('+QUOTENAME('/Data',CHAR(39))+') StaticData(att)'
		/* Execute the insert command */
		EXEC (@SQLCommand)

		/* Build out the column list for later use. */
		SET @ColumnList = ''
		SELECT 	@ColumnList = @ColumnList + ColumnName + ', '
		FROM 	@ColumnMapping
		SET @ColumnList = SUBSTRING(@ColumnList,1,LEN(@ColumnList)-1)
		
		/* If LoadDate is on the table, add it to the column list */
		IF( @LoadDate = 1 )
			SET @ColumnList = @ColumnList + ',LoadDate'

		/* Wish there was a better way, but each row has to be checked against the current DB version 
			and updates/deletes have to be account for as well */
		SET @Loop = 1
		WHILE( @Loop <= (SELECT MAX(RowID) FROM #StaticDataTable) )
		BEGIN /* WHILE @Loop */
			SELECT 	@ActionCode = ActionCode,
					@StaticDataVersion = [Version]
			FROM 	#StaticDataTable
			WHERE	RowID = @Loop
			/* Break the static data version down into it's parts */
			SELECT 	@DataVer = FullVersion
			FROM	dbo.udf_ParseSystemVersion(@StaticDataVersion)

			/* If the static data version is the same or lower in value, then work the row, else skip it. */
			IF( @DataVer <= @SystemVer )
			BEGIN /* IF @DataVer <= @SystemVer */
				IF( @ActionCode = 'I' )
				BEGIN /* Insert or update the record. */
					/* First build an insert command that will only be execute if the row does not exist in the static data table. */
					SET @SQLCommand = 'INSERT INTO ' + @FullTableName + ' (' + @ColumnList + ') SELECT '
					SELECT 	@SQLCommand = @SQLCommand + '#StaticDataTable.' + ColumnName + ','
					FROM 	@ColumnMapping
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1)
					IF @LoadDate = 1
						SET @SQLCommand = @SQLCommand + ',LoadDate = GETDATE()'
					SET @SQLCommand = @SQLCommand + ' FROM #StaticDataTable  LEFT JOIN ' + @FullTableName +' ON';
					SELECT 	@SQLCommand = @SQLCommand + ' #StaticDataTable.' + ColumnName + ' = ' + @FullTableName + '.' + ColumnName + ' AND'
					FROM 	@ColumnMapping
					WHERE	KeyValue = 1
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
					SET @SQLCommand = @SQLCommand + ' WHERE '
					SELECT 	@SQLCommand = @SQLCommand + @FullTableName + '.' + ColumnName + ' IS NULL AND '
					FROM	@ColumnMapping
					WHERE 	KeyValue = 1
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
					SET @SQLCommand = @SQLCommand + ' AND RowID = ' + CAST(@Loop AS VARCHAR)

					EXEC(@SQLCommand)
					IF( @@ROWCOUNT = 0 )
					BEGIN /* 0 rows where returned, so the record existed. Do an update if allowed. */
						/* Check to see if any fields are different before doing the update. */
						SELECT @SQLCommand = 'IF NOT EXISTS(SELECT 1 FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON'
						SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
						FROM 	@ColumnMapping
						/* remove the extra space + AND */
						SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
						SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR)
						
						/* Add the update to the IF NOT EXISTS statement so the update will only be done if something is different. */
						SELECT @SQLCommand = @SQLCommand + ') UPDATE SD SET '
						SELECT 	@SQLCommand = @SQLCommand + 'SD.' + ColumnName + ' = TSD.' + ColumnName + ','
						FROM	@ColumnMapping
						WHERE	Updateable = 1
						IF( @@ROWCOUNT > 0 )
						BEGIN /* Updateable columns exist, if 0 was returned no updateable columns so nothing happens. */
							SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-1) /* remove the trailing , */
							IF @ModificationDate = 1
								SET @SQLCommand = @SQLCommand + ',ModificationDate = GETDATE()'
							IF @ModifiedBy = 1
								SET @SQLCommand = @SQLCommand + ',ModifiedBy = SUSER_SNAME()'
							IF @LoadDate = 1
								SET @SQLCommand = @SQLCommand + ',LoadDate = GETDATE()'
							SET @SQLCommand = @SQLCommand + ' FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON'
							SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
							FROM 	@ColumnMapping
							WHERE	KeyValue = 1
							/* remove the extra space + AND */
							SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
							SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR)

							EXEC(@SQLCommand)
						END
					END
				END /* Insert or update the record. */
				ELSE IF( @ActionCode = 'D' )
				BEGIN /* Delete the record */
					SET @SQLCommand = 'DELETE '+ @FullTableName + ' FROM ' + @FullTableName + ' SD INNER JOIN #StaticDataTable TSD ON'
					SELECT 	@SQLCommand = @SQLCommand + ' SD.' + ColumnName + ' = TSD.' + ColumnName + ' AND'
					FROM 	@ColumnMapping
					WHERE	KeyValue = 1
					/* remove the extra space + AND */
					SET @SQLCommand = SUBSTRING(@SQLCommand,1,LEN(@SQLCommand)-4)
					SET @SQLCommand = @SQLCommand + ' WHERE RowID = ' + CAST(@Loop AS VARCHAR)
					EXEC(@SQLCommand)
				END
			END /* IF @DataVer <= @SystemVer */
			SET @Loop = @Loop + 1
		END	/* WHILE @Loop */
	END /* IF @@ROWCOUNT */

	/* Remove the temp tables that were used. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues
	PRINT 'The database update succeeded'
END TRY
BEGIN CATCH
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataTable')) 
		DROP TABLE #StaticDataTable
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#StaticDataValues')) 
		DROP TABLE #StaticDataValues
	EXEC dbo.usp_WfsRethrowException
END CATCH
