--WFSScriptProcessorSchema dbo
--WFSScriptProcessorAlterTable DocumentTypes
IF NOT EXISTS( SELECT 1 FROM [information_schema].[columns] WHERE table_name = 'DocumentTypes' and column_name = 'IMSDocumentType' )
	ALTER TABLE dbo.DocumentTypes ADD IMSDocumentType [varchar] (30) NOT NULL CONSTRAINT [DF__DocumentTypes__IMSDocumentType]  DEFAULT ('Invoice') WITH VALUES
--WFSScriptProcessorDataConversion DocumentTypes
SET NOCOUNT ON
UPDATE dbo.DocumentTypes SET IMSDocumentType = 'Per/Bus Check' WHERE [Description] = 'Captured Check'
SET NOCOUNT OFF