--WFSScriptProcessorSchema dbo
--WFSScriptProcessorAlterTable BatchTrace
IF NOT EXISTS( SELECT 1 FROM [information_schema].[columns] WHERE table_name = 'BatchTrace' and column_name = 'OLTAQueueInsert' )
	ALTER TABLE dbo.BatchTrace ADD OLTAQueueInsert [datetime] NULL
