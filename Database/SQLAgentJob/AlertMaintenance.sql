--WFSScriptProcessorSQLAgentJobName AlertMaintenance  
--WFSScriptProcessorSQLAgentJobCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 03/12/2016
*
* Purpose: SQL Agent Job to control Alert Maintenance SSIS packages.
*
* Modification History
* 03/12/2016 WI 267879 JPS	Created.
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT

SELECT @ReturnCode = 0, @JobID = NULL;

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_job 
			@job_name=N'$(SSISJobName)', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'Performs maintenance on the Alert related database objects.', 
			@category_name=N'Database Maintenance', 
			@owner_login_name=N'sa', 
			@job_id = @JobID OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)'

/* Create step 1 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'EventLog Maintenance'))
BEGIN	
	RAISERROR('Creating Step 1 - EventLog Maintenance.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'EventLog Maintenance', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'SSIS', 
			@command=N'/DTS "$(SSISPackagePath)\AlertMaintenance" /SERVER $(DBServer) /SET "\Package.Variables[User::WFSConfigurationServerName].Properties[Value]";$(DBServer) /SET "\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]";"WFS_SSIS_Configuration" /REPORTING E', 
			@database_name=N'master', 
			@flags=0
END

/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1


/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
	EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
