--WFSScriptProcessorSQLAgentJobName R360Alerts
--WFSScriptProcessorSQLAgentJobCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2015 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 06/01/2013
*
* Purpose: Execute R360Alert SSIS Packages
*		   
*
* Modification History
* 06/01/2013 WI 99887 JPB	Created
* 07/18/2013 WI 99897 JPB	Added event log task
* 11/17/2014 WI 178258 JBS	Change step one on Failure to go to next step
* 02/04/2014 WI 178258 JBS	Remove Step 1 for 2.01 (No CDC)
* 12/18/2015 WI 253996 TWE  Add Step 1 back in for 2.02 (Now we have CDC again)
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT,
		@StartDate INT;

SELECT @ReturnCode = 0, @JobID = NULL;


SELECT @StartDate = CAST(CONVERT(varchar, GetDate(), 112) AS INT);

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_job 
			@job_name=N'$(SSISJobName)', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'R360 Alerts', 
			@category_name=N'', 
			@owner_login_name=N'sa', 
			@job_id = @JobID OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)'

/* Create step 1 Process CDC Tables if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Process CDC Tables' AND step_id = 1))	
BEGIN
	RAISERROR('Creating Step 1 - Process CDC Tables.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Process CDC Tables', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'SSIS', 
			@command=N'/DTS "$(SSISPackagePath)\RecHubAlertsCDC" /SERVER $(DBServer) /CHECKPOINTING OFF /SET "\Package.Variables[User::WFSConfigurationServerName].Properties[Value]";$(DBServer) /SET "\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]";"WFS_SSIS_Configuration" /REPORTING E', 
			@database_name=N'master', 
			@flags=0
END

/* Create step 2 Process Event Log if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Process Event Log'))	
BEGIN
	RAISERROR('Creating Step 2 - Process Event Log.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Process Event Log', 
			@step_id=2, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'SSIS', 
			@command=N'/DTS "$(SSISPackagePath)\RecHubAlertsEventLog" /SERVER $(DBServer) /CHECKPOINTING OFF /SET "\Package.Variables[User::WFSConfigurationServerName].Properties[Value]";$(DBServer) /SET "\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]";"WFS_SSIS_Configuration" /REPORTING E', 
			@database_name=N'master', 
			@flags=0;
END

/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1;


/* All steps created as for initial install - now we will update steps to create final order*/
/* UPDATE step 1  WI 178258 */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Process CDC Tables'))	
BEGIN
	RAISERROR('Update Step 1 - Process CDC Tables.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_update_jobstep 
			@job_id=@JobID, 
			@step_id=1,
			@on_fail_action=3;			-- Goes to Next Step in Job and does not fail entire Job
END
					
/* Add schedule */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysschedules WHERE name = '$(SSISJobName)'))	
BEGIN
	RAISERROR('Adding Job Schedule.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule 
			@job_id=@JobID, 
			@name=N'$(SSISJobName)', 
			@enabled = 1,
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=4, 
			@freq_subday_interval=1, 
			@freq_relative_interval=1, 
			@freq_recurrence_factor=0, 
			@active_start_date=@StartDate, 
			@active_end_date = 99991231, 
			@active_start_time=0, 
			@active_end_time=235959 
END

/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
	EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
