--WFSScriptProcessorPrint WI 70646
--WFSScriptProcessorSQLAgentJobName RecHub Table Maintenance
--WFSScriptProcessorSQLAgentJobCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011-2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 10/31/2011
*
* Purpose: Remove old records from RecHub schema.
*
* Modification History
* 10/31/2011 CR 45556 JPB	Created.
* 03/31/2012 CR 49193 JPB	Added OTISQueue step.
* 12/05/2012 WI 70646 JPB	Added Session Tables step.
* 07/19/2013 WI 109373 JPB	Updated to 2.0. 
*							Removed all steps except CDS Queue table.
* 01/28/2014 WI 128269 JPB	Updated SET value information to WFS SSIS Config 
*							database. (FP:128109)
* 01/29/2014 WI 128354 JPB	Removed hard coded job name.
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT

SELECT @ReturnCode = 0, @JobID = NULL;

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_job 
			@job_name=N'$(SSISJobName)', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'Deletes old records from RecHub working tables.', 
			@category_name=N'Database Maintenance', 
			@owner_login_name=N'sa', 
			@job_id = @JobID OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)'

/* Create step 1 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'CDS Queue'))
BEGIN	
	RAISERROR('Creating Step 1 - CDS Queue.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'CDS Queue', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=1, 
			@on_success_step_id=0, 
			@on_fail_action=2, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'SSIS', 
			@command=N'/DTS "$(SSISPackagePath)\CDSQueueMaintenance" /SERVER $(DBServer) /SET "\Package.Variables[User::WFSConfigurationServerName].Properties[Value]";$(DBServer) /SET "\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]";"WFS_SSIS_Configuration" /REPORTING E', 
			@database_name=N'master', 
			@flags=0
END
ELSE BEGIN
	RAISERROR('Updating Step 1 - CDS Queue.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_update_jobstep 
			@job_id=@JobID, 
			@step_name=N'CDS Queue', 
			@step_id=1,
			@on_success_action=3, 
			@on_fail_action=3 
END

/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
					
/* Job updated to start at step 1 */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysschedules WHERE name = 'RecHub Table Maintenance'))	
BEGIN
	RAISERROR('Adding Job Schedule.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule 
			@job_id=@JobID, 
			@name=N'RecHub Table Maintenance', 
			@enabled=1, 
			@freq_type=8, 
			@freq_interval=64, 
			@freq_subday_type=1, 
			@freq_subday_interval=0, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=1, 
			@active_start_date=20111019, 
			@active_end_date=99991231, 
			@active_start_time=210000, 
			@active_end_time=235959
END

/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
	EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
