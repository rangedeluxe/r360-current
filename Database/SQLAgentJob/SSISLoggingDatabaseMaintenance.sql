--WFSScriptProcessorPrint WI 167111 , PT146868813 
--WFSScriptProcessorSQLAgentJobName SSISLoggingDatabaseMaintenance
--WFSScriptProcessorSQLAgentJobCreate
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT,
		@StartDate INT;

SELECT @ReturnCode = 0, @JobID = NULL;

SELECT @StartDate = CAST(CONVERT(varchar, GetDate(), 112) AS INT);


IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode =  msdb.dbo.sp_add_job @job_name=N'$(SSISJobName)', 
		@enabled=1, 
		@notify_level_eventlog=0, 
		@notify_level_email=0, 
		@notify_level_netsend=0, 
		@notify_level_page=0, 
		@delete_level=0, 
		@description=N'SSIS Logging Database Maintenance', 
		@category_name=N'', 
		@owner_login_name=N'sa', 
		@job_id = @jobId OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)';

/* Create step 1 if it does not exist */
/* @flags=4 will Write Transact-SQL Job step output to Step History.  */
/* Remove Step 1 if it exists and then replace with partition maintenance stored procedure */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_id = 1))
	EXEC msdb.dbo.sp_delete_jobstep @job_name = N'$(SSISJobName)', @step_id = 1 ;
/* Add step 1 with new name and call to stored procedure */
RAISERROR('Creating Step 1 - Maintain Partitions.',10,1) WITH NOWAIT;
EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id=@jobId, @step_name=N'Maintain SSIS Logging partitions', 
	@step_id=1, 
	@cmdexec_success_code=0, 
	@on_success_action=1, 
	@on_success_step_id=0, 
	@on_fail_action=2, 
	@on_fail_step_id=0, 
	@retry_attempts=0, 
	@retry_interval=0, 
	@os_run_priority=0, 
	@subsystem=N'TSQL', 
	@command=N'EXEC dbo.usp_WFSSSISLogging_PartitionMaintenance @parmLogRetentionDays=8', 
	@database_name=N'$(DBName)', 
	@flags=4;
	
/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1;


/* Add schedule */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysschedules WHERE name = '$(SSISJobName)'))	
BEGIN
	RAISERROR('Adding Job Schedule.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule @job_id=@jobId, @name='$(SSISJobName)', 
		@enabled=1, 
		@freq_type=8, 
		@freq_interval=65, 
		@freq_subday_type=1, 
		@freq_subday_interval=0, 
		@freq_relative_interval=0, 
		@freq_recurrence_factor=1, 
		@active_start_date=20140814, 
		@active_end_date=99991231, 
		@active_start_time=500, 
		@active_end_time=235959;
END

/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
	EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
