/******************************************************************************
** Deluxe Corporation (DLX) 
** Copyright � 2013-2019 Deluxe Corporation All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013-2019 Deluxe Corporation All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: Pierre Sula
* Date: 10/09/2013
*
* Purpose: Execute RecHubPurge SSIS Package
*		   
*
* Modification History
* 10/09/2013 WI 96892 PS	Created.
* 01/27/2015 WI 186646 JBS	Add to 2.01 to incorporate $DBName
* 11/11/2015 WI 243968 BLR  Added data partitions as step 2.
* 08/19/2019 R360-29886 MGE Added steps for each partition scheme
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
        @ReturnCode INT,
        @StartDate INT;

SELECT @ReturnCode = 0, @JobID = NULL;


SELECT @StartDate = CAST(CONVERT(varchar, GetDate(), 112) AS INT);

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
    RAISERROR('Creating Job.',10,1) WITH NOWAIT;
    EXEC @ReturnCode = msdb.dbo.sp_add_job 
            @job_name=N'$(SSISJobName)', 
            @enabled=1, 
            @notify_level_eventlog=0, 
            @notify_level_email=0, 
            @notify_level_netsend=0, 
            @notify_level_page=0, 
            @delete_level=0, 
            @description=N'Purging Data', 
            @category_name=N'', 
            @owner_login_name=N'sa', 
            @job_id = @JobID OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)'

/* Create step 1 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Purging Data'))	
BEGIN
    RAISERROR('Creating Step 1 - Purging Data.',10,1) WITH NOWAIT;
    EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
            @job_id=@JobID, 
            @step_name=N'Purging Data', 
            @step_id=1, 
            @cmdexec_success_code=0, 
            @on_success_action=1, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=0, 
            @retry_interval=0, 
            @os_run_priority=0, 
            @subsystem=N'SSIS', 
            @command=N'/DTS "$(SSISPackagePath)\RecHubPurge" /SERVER $(DBServer) /CHECKPOINTING OFF /SET "\Package.Variables[User::WFSConfigurationServerName].Properties[Value]";$(DBServer) /SET "\Package.Variables[User::WFSConfigurationInitialCatalog].Properties[Value]";"WFS_SSIS_Configuration" /REPORTING E', 
            @database_name=N'master', 
            @flags=0
END

/* Create step 2 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Data Partitions'))	
BEGIN
	RAISERROR('Removing Step 2 - Data Partitions.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_delete_jobstep
			@job_id=@JobID,
			@step_id=2
END 

IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Data Partitions RecHubAudits'))	
BEGIN
	RAISERROR('Creating Step 2 - Data Partitions - RecHubAudits.',10,1) WITH NOWAIT;
    EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
            @job_id=@JobID, 
            @step_name=N'Data Partitions RecHubAudits', 
            @step_id=2, 
            @cmdexec_success_code=0, 
            @on_success_action=3, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=0, 
            @retry_interval=0, 
            @os_run_priority=0, 
            @subsystem = N'TSQL',
            @command=N'EXEC RecHubSystem.usp_AddPartitionsToDatabase ''$(DBName)'', ''RecHubAudits'', 12', 
            @database_name = N'$(DBName)',
            @flags=0
END
   
IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Data Partitions RecHubFacts'))	
BEGIN
	RAISERROR('Creating Step 3 - Data Partitions - RecHubFacts.',10,1) WITH NOWAIT;
    EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
            @job_id=@JobID, 
            @step_name=N'Data Partitions RecHubFacts', 
            @step_id=3, 
            @cmdexec_success_code=0, 
            @on_success_action=3, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=0, 
            @retry_interval=0, 
            @os_run_priority=0, 
            @subsystem = N'TSQL',
            @command=N'EXEC RecHubSystem.usp_AddPartitionsToDatabase ''$(DBName)'', ''RecHubFacts'', 12', 
            @database_name = N'$(DBName)',
            @flags=0
END

IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Data Partitions RecHubNotifications'))	
BEGIN
	RAISERROR('Creating Step 4 - Data Partitions - RecHubNotifications.',10,1) WITH NOWAIT;
    EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
            @job_id=@JobID, 
            @step_name=N'Data Partitions RecHubNotifications', 
            @step_id=4, 
            @cmdexec_success_code=0, 
            @on_success_action=1, 
            @on_success_step_id=0, 
            @on_fail_action=2, 
            @on_fail_step_id=0, 
            @retry_attempts=0, 
            @retry_interval=0, 
            @os_run_priority=0, 
            @subsystem = N'TSQL',
            @command=N'EXEC RecHubSystem.usp_AddPartitionsToDatabase ''$(DBName)'', ''RecHubNotifications'', 12', 
            @database_name = N'$(DBName)',
            @flags=0
END


RAISERROR('Updating Step 1 with a continue action on success.',10,1) WITH NOWAIT;
EXEC msdb.dbo.sp_update_jobstep
    @job_name = N'$(SSISJobName)',
    @step_id = 1,
    @on_success_action=3;

/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
    EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
                    
/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
    AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
    RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
    EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
