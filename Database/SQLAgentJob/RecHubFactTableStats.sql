--WFSScriptProcessorSQLAgentJobName RecHub Table Maintenance
--WFSScriptProcessorSQLAgentJobCreate
/******************************************************************************
** Deluxe Corporation (DLX)
** Copyright © 2013-2019 Deluxe Corporation (DLX) All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013-2019 Deluxe Corporation. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of DLX and contain DLX trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* DLX (see the DLX license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of DLX.
*
* Author: JPB
* Date: 08/06/2013
*
* Purpose: Update statistics on fact and dim tables.
*
* Modification History
* 08/06/2013 WI 111169 		JPB	Created. (FP WIs 77756, 102057)
* 09/09/2013 WI 101869 		JBS	Add Step to Clean up old/completed CommonExceptions
* 01/23/2014 WI 128280 		JBS	FP 127858 Add schema RecHubException to stored procedure in step 7
*							Had to add an Update option to Step 7
* 01/29/2014 WI 128353 		JPB	Removed hard coded job name.
* 07/28/2015 WI 225195 		JBS	Remove Step 7(step_name = 'Remove Old Rows from CommonExceptions')
* 11/02/2016 PT #127613949  JBS	Change Step 3 command and Step Name
* 03/21/2019 R360-15940 	MGE Changes for Dashboard performance
* 04/02/2019 R360-15940		MGE Getting heavy handed - drop and replace all steps.
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
		@JobStep INT,
		@ReturnCode INT,
		@ScheduleID INT;

SELECT @ReturnCode = 0, @JobID = NULL;

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_job 
			@job_name=N'$(SSISJobName)', 
			@enabled=1, 
			@notify_level_eventlog=0, 
			@notify_level_email=0, 
			@notify_level_netsend=0, 
			@notify_level_page=0, 
			@delete_level=0, 
			@description=N'Update R360 fact table statistics.', 
			@category_name=N'Database Maintenance', 
			@owner_login_name=N'sa', 
			@job_id = @JobID OUTPUT;
END	
ELSE SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = N'$(SSISJobName)';
/* Blow away any steps that exist for this job (this is a Deluxe job - should not be modified by customers */

DELETE FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID 

/* Now add them back the way we want them */

/* Create step 1 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on factBatchSummary'))	
BEGIN
	RAISERROR('Creating Step 1 - Update Stats on factBatchSummary.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on factBatchSummary', 
			@step_id=1, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.factBatchSummary', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Create step 2 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on factTransactionSummary'))	
BEGIN
	RAISERROR('Creating Step 2 - Update Stats on factTransactionSummary.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on factTransactionSummary', 
			@step_id=2, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.factTransactionSummary', 
			@database_name=N'$(DBName)', 
			@flags=0
END


/* Updating the Step Name and Command Line switching to dimWorkgroupDataEntryColumns table. */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on dimWorkgroupDataEntryColumns'))

BEGIN
	SELECT @JobStep = step_id from msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on dimWorkgroupDataEntryColumns'
	RAISERROR('Creating Step 3 for RecHubData.dimWorkgroupDataEntryColumns.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep
			@job_id=@JobID, 
			@step_name=N'Update Stats on dimWorkgroupDataEntryColumns',
			@step_id=3, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.dimWorkgroupDataEntryColumns', 
			@database_name=N'$(DBName)',
			@flags=0
END

/* Create step 4 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on SessionClientAccountEntitlements'))	
BEGIN
	RAISERROR('Creating Step 4 - Update Stats on SessionClientAccountEntitlements.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on SessionClientAccountEntitlements', 
			@step_id=4, 
			@cmdexec_success_code=0, 
			@on_success_action=3,  
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubUser.SessionClientAccountEntitlements', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Create step 5 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on factChecks'))	
BEGIN
	RAISERROR('Creating Step 5 - Update Stats on factChecks.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on factChecks', 
			@step_id=5, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.factChecks', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Create step 6 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on factDocuments'))	
BEGIN
	RAISERROR('Creating Step 6 - Update Stats on factDocuments.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on factDocuments', 
			@step_id=6, 
			@cmdexec_success_code=0, 
			@on_success_action=3, 
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.factDocuments', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Create step factDataEntryDetails job as job setp 3 if it does not exist as job step 3 */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Update Stats on factDataEntryDetails' ))	
BEGIN
	RAISERROR('Creating Step 7 - Update Stats on factDataEntryDetails.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Update Stats on factDataEntryDetails', 
			@step_id=7, 
			@cmdexec_success_code=0, 
			@on_success_action=3,  
			@on_success_step_id=0, 
			@on_fail_action=3, 
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'UPDATE STATISTICS RecHubData.factDataEntryDetails', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Create step 8 if it does not exist */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID AND step_name = 'Mark dashboard stored procedure for recompile.'))	
BEGIN
	RAISERROR('Creating Step 8 - Mark dashboard stored procedure for recompile.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobstep 
			@job_id=@JobID, 
			@step_name=N'Mark dashboard stored procedure for recompile.', 
			@step_id=8, 
			@cmdexec_success_code=0, 
			@on_success_action=1,		-- Quit reporting Success since this is the last step 
			@on_success_step_id=0, 
			@on_fail_action=2,			-- Quit reporting failure since this is the last step
			@on_fail_step_id=0, 
			@retry_attempts=0, 
			@retry_interval=0, 
			@os_run_priority=0, 
			@subsystem=N'TSQL', 
			@command=N'EXEC sp_recompile ''RecHubData.usp_factBatchSummary_Get_ClientAccountSummary_by_SP''', 
			@database_name=N'$(DBName)', 
			@flags=0
END

/* Steps created, set the job to start at step 1 */
IF (@@ERROR = 0 OR @ReturnCode = 0)	
	EXEC @ReturnCode = msdb.dbo.sp_update_job @job_id = @JobID, @start_step_id = 1
					
/* Remove the old schedules for twice daily if they exist  */
IF EXISTS (SELECT 1 FROM msdb.dbo.sysschedules WHERE (name = N'Receivables Hub Update Fact Table Stats (Morning)'))
BEGIN 
	SELECT @ScheduleID = schedule_id FROM msdb.dbo.sysschedules WHERE name = N'Receivables Hub Update Fact Table Stats (Morning)'
	RAISERROR('Removing 6:30 AM schedule.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_delete_schedule  @schedule_id = @ScheduleID, @force_delete = 1
END

IF EXISTS (SELECT 1 FROM msdb.dbo.sysschedules WHERE (name = N'Receivables Hub Update Fact Table Stats (Afternoon)'))
BEGIN 
	SELECT @ScheduleID = schedule_id FROM msdb.dbo.sysschedules WHERE name = N'Receivables Hub Update Fact Table Stats (Afternoon)'
	RAISERROR('Removing 12:30 PM schedule.',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_delete_schedule  @schedule_id = @ScheduleID, @force_delete = 1
END

/* Add schedule for every other hour */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysschedules WHERE name = N'factStats Bi-Hourly'))	
BEGIN
	RAISERROR('Adding Job Schedule (Bi-Hourly).',10,1) WITH NOWAIT;
	EXEC @ReturnCode = msdb.dbo.sp_add_jobschedule 
			@job_id=@JobID, 
			@name=N'factStats Bi-Hourly', 
			@enabled=1, 
			@freq_type=4, 
			@freq_interval=1, 
			@freq_subday_type=8, 
			@freq_subday_interval=2, 
			@freq_relative_interval=0, 
			@freq_recurrence_factor=0, 
			@active_start_date=20130102,
			@active_end_date=99991231, 
			@active_start_time=60000, 
			@active_end_time=220101
END

/* everything is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job Server.',10,1) WITH NOWAIT;
	EXEC msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)'
END
