--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSQLAgentJobName UpdateCurrentProcessingDate
--WFSScriptProcessorSQLAgentJobCreate
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 06/18/2014
*
* Purpose: A SQL Agent Job created to execute RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate
*		   As described in the stored procedure this should execute at midnight to advance the 
*		   CurrentProcessingDate on dimSiteCodes table
*		   
* Modification History
* 06/18/2014 WI 71783 JBS	Created
******************************************************************************/
DECLARE @JobID UNIQUEIDENTIFIER,
		@ReturnCode INT,
		@StartDate INT,
		@ScheduleID INT;

SELECT @ReturnCode = 0, @JobID = NULL;

-- Sets StartDate to next day since this job will run on the Midnight schedule
SELECT @StartDate = CAST(CONVERT(VARCHAR, GETDATE() + 1, 112) AS INT)

IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)')
BEGIN
	RAISERROR('Creating Job.',10,1) WITH NOWAIT;	
	
	EXEC @ReturnCode = msdb.dbo.sp_add_job 
		@job_name = N'$(SSISJobName)',
		@enabled = 1,
		@owner_login_name = N'sa',
		@description = N'This job will change the current processing date to the database servers current date. As a general rule, this job will be scheduled to run at midnight every night to advance the processing date to the current day.',
		@category_name = N'Database Maintenance',
		@notify_level_eventlog = 2,
		@notify_level_email = 0,
		@notify_level_netsend = 0,
		@notify_level_page = 0,
		@delete_level = 0,
		@job_id = @JobID OUTPUT;
END
ELSE 
BEGIN
	RAISERROR('Update Job.',10,1) WITH NOWAIT;	
	
	EXEC @ReturnCode = msdb.dbo.sp_update_job 
		@job_name = N'$(SSISJobName)',
		@enabled = 1,
		@owner_login_name = N'sa',
		@description = N'This job will change the current processing date to the database servers current date. As a general rule, this job will be scheduled to run at midnight every night to advance the processing date to the current day.',
		@category_name = N'Database Maintenance',
		@notify_level_eventlog = 2,
		@notify_level_email = 0,
		@notify_level_netsend = 0,
		@notify_level_page = 0,
		@delete_level = 0;

	SELECT @JobID = job_id FROM msdb.dbo.sysjobs WHERE name = '$(SSISJobName)'
END

IF (@@ERROR = 0 OR @ReturnCode = 0)
BEGIN
IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobsteps WHERE job_id = @JobID)
/* Create step 1 */
	BEGIN
		RAISERROR('Creating Job Step 1.',10,1) WITH NOWAIT;	
	
		EXEC @ReturnCode = msdb.dbo.sp_add_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'AdvanceProcessingDate',
			@command = N'EXEC RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate',
			@database_name = N'$(DBName)',
			@subsystem = N'TSQL',
			@flags = 2,
			@retry_attempts = 1,
			@retry_interval = 0,
			@on_success_step_id = 0,
			@on_success_action = 1,
			@on_fail_step_id = 0,
			@on_fail_action = 2;
	END
ELSE
/* Update Job step 1 */
	BEGIN
		RAISERROR('Update Job Step 1.',10,1) WITH NOWAIT;	
	
		EXEC @ReturnCode = msdb.dbo.sp_update_jobstep @job_id = @JobID, @step_id = 1, @step_name = N'AdvanceProcessingDate',
			@command = N'EXEC RecHubData.usp_dimSiteCodes_Upd_CurrentProcessingDate',
			@database_name = N'$(DBName)',
			@subsystem = N'TSQL',
			@flags = 2,
			@retry_attempts = 1,
			@retry_interval = 0,
			@on_success_step_id = 0,
			@on_success_action = 1,
			@on_fail_step_id = 0,
			@on_fail_action = 2;
	END
END

/* If ALL is good, add the job to database */
IF ((@@ERROR = 0 OR @ReturnCode = 0)
	AND NOT EXISTS(SELECT 1 FROM msdb.dbo.sysjobservers WHERE job_id = @JobID))	
BEGIN
	RAISERROR('Adding Job To Server.',10,1) WITH NOWAIT;
	EXEC @ReturnCode =  msdb.dbo.sp_add_jobserver @job_id = @JobID, @server_name = N'$(DBServer)';
END

IF (@@ERROR = 0 OR @ReturnCode = 0)
BEGIN
IF NOT EXISTS(SELECT 1 FROM msdb.dbo.sysschedules WHERE name = 'Every MidNight')
/* Add schedule */
	BEGIN
		RAISERROR('Adding Schedule for Job.',10,1) WITH NOWAIT;

		EXEC @ReturnCode = msdb.dbo.sp_add_schedule 
				@schedule_name = N'Every MidNight',
				@enabled = 1,
				@freq_type = 4,
				@active_start_date = @StartDate,
				@active_end_date = 99991231,
				@freq_interval = 1,
				@freq_subday_type = 1,
				@freq_subday_interval = 1,
				@freq_relative_interval = 1,
				@freq_recurrence_factor = 1,
				@active_start_time = 0,
				@active_end_time = 235959,
				@schedule_id = @ScheduleID OUTPUT;
	END
ELSE
/* Update schedule */
	BEGIN
		RAISERROR('Updating Schedule for Job.',10,1) WITH NOWAIT;

		EXEC @ReturnCode = msdb.dbo.sp_update_schedule 
				@name = N'Every MidNight',
				@enabled = 1,
				@freq_type = 4,
				--@active_start_date = @StartDate,  -- this will leave the original start date on schedule
				@active_end_date = 99991231,
				@freq_interval = 1,
				@freq_subday_type = 1,
				@freq_subday_interval = 1,
				@freq_relative_interval = 1,
				@freq_recurrence_factor = 1,
				@active_start_time = 0,
				@active_end_time = 235959;
		
		SELECT @ScheduleID = schedule_id FROM msdb.dbo.sysschedules WHERE name = N'Every MidNight'
	END
END

IF (@@ERROR = 0 OR @ReturnCode = 0)
BEGIN
	-- Attaching Schedule to Job...  If the Attachment already exists nothing will change
	EXEC msdb.dbo.sp_attach_schedule @job_id = @jobID, @schedule_id = @ScheduleID
END
