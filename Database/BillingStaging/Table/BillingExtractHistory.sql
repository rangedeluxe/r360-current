--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema BillingStaging
--WFSScriptProcessorTableName BillingExtractHistory
--WFSScriptProcessorTableDrop
IF OBJECT_ID('BillingStaging.BillingExtractHistory') IS NOT NULL
       DROP TABLE BillingStaging.BillingExtractHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 06/14/2016
*
* Purpose: Billing Staging table for BillingExtractHistory
*		   
*
* Modification History
* 06/14/2016 WI 286595 JBS	Created
* 06/27/2016 WI 289016 JBS	Add Bank and Workgroup Name
* 07/08/2016 WI 288621 JBS	Add EntityID for lookup in SSIS package
* 08/23/2016 PT #127613859 JBS	Add ViewingDays and RetentionDays
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE BillingStaging.BillingExtractHistory
(
	BankID			INT NOT NULL,
	ClientAccountID INT NOT NULL,
	DepositDateKey	INT NOT NULL, 
	ImmutableDateKey INT NOT NULL, 
	SourceBatchID	BIGINT NOT NULL,
	BatchSourceKey	SMALLINT NOT NULL,
	CheckCount		INT NOT NULL,
	StubCount		INT NOT NULL,
	DocumentCount	INT NOT NULL,
	BankName		VARCHAR(128) NOT NULL,
	WorkgroupName	VARCHAR(40) NOT NULL,
	EntityID		INT	NULL,
	ViewingDays		INT NULL,
	RetentionDays	INT NULL
) ;
--WFSScriptProcessorTableProperties
