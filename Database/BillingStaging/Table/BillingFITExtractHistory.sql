--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema BillingStaging
--WFSScriptProcessorTableName BillingFITExtractHistory
--WFSScriptProcessorTableDrop
IF OBJECT_ID('BillingStaging.BillingFITExtractHistory') IS NOT NULL
       DROP TABLE BillingStaging.BillingFITExtractHistory
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: MGE
* Date: 05/16/2017
*
* Purpose: Billing Staging table for BillingFITExtractHistory
*		   
*
* Modification History
* 05/16/2016 PT #139494807 MGE	Created 
*****************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE BillingStaging.BillingFITExtractHistory
(
	BankID			INT NOT NULL,
	ClientAccountID INT NOT NULL,
	TableUsed       VARCHAR(256) NOT NULL,
	TableKey        BIGINT NOT NULL,
	DateKey         INT NOT NULL,
	NotificationsWithAttachmentsCount		INT NOT NULL,
	NotificationsWithoutAttachmentsCount		INT NOT NULL,
	AttachmentCount	INT NOT NULL,
	AttachmentSize  INT NOT NULL,
	CSVDownloadCount INT NOT NULL,
	CSVRowCount     INT NOT NULL,
	BankName		VARCHAR(128) NOT NULL,
	EntityID		INT	NULL,
	ViewingDays		INT NULL,
	RetentionDays	INT NULL,
	FileList		VARCHAR(2048) NULL
) ;
--WFSScriptProcessorTableProperties
