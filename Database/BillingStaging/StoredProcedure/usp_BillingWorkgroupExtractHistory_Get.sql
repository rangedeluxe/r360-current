--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema BillingStaging
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_BillingWorkgroupExtractHistory_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('BillingStaging.usp_BillingWorkgroupExtractHistory_Get') IS NOT NULL
	   DROP PROCEDURE BillingStaging.usp_BillingWorkgroupExtractHistory_Get;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE BillingStaging.usp_BillingWorkgroupExtractHistory_Get 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	TWE
* Date:		05/04/2017
*
* Purpose: Query BillingWorkgroupExtractHistory table for Billing Extract process for SSIS data flow 
*			that populates historical table of data reported.
*
* Modification History
* 05/04/2017 PT #142166535 TWE	Create
* 05/08/2017 PT #139494807 MGE	Prevent duplicate rows from going into history table
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT 
	    BillingStaging.BillingWorkgroupExtractHistory.DateKey,
		BillingStaging.BillingWorkgroupExtractHistory.BankID,
		BillingStaging.BillingWorkgroupExtractHistory.ClientAccountID,
		BillingStaging.BillingWorkgroupExtractHistory.TableUsed,
		BillingStaging.BillingWorkgroupExtractHistory.TableKey
	FROM 
		BillingStaging.BillingWorkgroupExtractHistory
		LEFT OUTER JOIN RecHubBilling.BillingWorkgroupExtractHistory History
			ON History.TableUsed = BillingStaging.BillingWorkgroupExtractHistory.TableUsed
				AND History.TableKey = BillingStaging.BillingWorkgroupExtractHistory.TableKey
	WHERE History.BillingWorkgroupExtractHistoryKey IS NULL	
	ORDER BY 
		BillingStaging.BillingWorkgroupExtractHistory.DateKey;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH