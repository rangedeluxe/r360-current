--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema BillingStaging
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorStoredProcedureName usp_BillingExtractHistory_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('BillingStaging.usp_BillingExtractHistory_Get') IS NOT NULL
	   DROP PROCEDURE BillingStaging.usp_BillingExtractHistory_Get;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE BillingStaging.usp_BillingExtractHistory_Get 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	JBS
* Date:		06/27/2016
*
* Purpose: Query BillingExtractHistory table for Billing Extract process for SSIS data flow 
*			that populates historical table of data reported.
*
* Modification History
* 06/27/2016 WI 289115 JBS	Create
* 06/05/2017 PT 145038913 MGE Prevent Cartesian product on Rebill.
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT 
		BillingStaging.BillingExtractHistory.BankID,
		BillingStaging.BillingExtractHistory.ClientAccountID,
		BillingStaging.BillingExtractHistory.DepositDateKey,
		BillingStaging.BillingExtractHistory.ImmutableDateKey,
		BillingStaging.BillingExtractHistory.SourceBatchID,
		BillingStaging.BillingExtractHistory.BatchSourceKey
	FROM 
		BillingStaging.BillingExtractHistory 
		LEFT JOIN RecHubBilling.BillingExtractHistory BEH
			ON BillingStaging.BillingExtractHistory.BankID = BEH.BankID
			AND BillingStaging.BillingExtractHistory.ClientAccountID = BEH.ClientAccountID	
			AND BillingStaging.BillingExtractHistory.DepositDateKey = BEH.DepositDateKey
			AND BillingStaging.BillingExtractHistory.ImmutableDateKey = BEH.ImmutableDateKey
			AND BillingStaging.BillingExtractHistory.SourceBatchID = BEH.SourceBatchID
			AND BillingStaging.BillingExtractHistory.BatchSourceKey = BEH.BatchSourceKey
	WHERE BEH.ClientAccountID IS NULL
	ORDER BY 
		BillingStaging.BillingExtractHistory.DepositDateKey;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH