--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema BillingStaging
--WFSScriptProcessorStoredProcedureName usp_BillingFITExtractHistory_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('BillingStaging.usp_BillingFITExtractHistory_Get') IS NOT NULL
	   DROP PROCEDURE BillingStaging.usp_BillingFITExtractHistory_Get;
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE BillingStaging.usp_BillingFITExtractHistory_Get 
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved. 
* All other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:	MGE
* Date:		05/17/2017
*
* Purpose: Query BillingFITExtractHistory table for Billing Extract process for SSIS data flow 
*			that populates historical table of data reported.
*
* Modification History
* 05/17/2017 PT #139494807 MGE	Create
* 06/06/2017 PT #145038913 MGE	Don't add duplicate files to history
******************************************************************************/
SET NOCOUNT ON 

BEGIN TRY

	SELECT 
	    BillingStaging.BillingFITExtractHistory.DateKey,
		BillingStaging.BillingFITExtractHistory.BankID,
		BillingStaging.BillingFITExtractHistory.ClientAccountID,
		BillingStaging.BillingFITExtractHistory.TableUsed,
		BillingStaging.BillingFITExtractHistory.TableKey,
		BillingStaging.BillingFITExtractHistory.FileList	
	FROM 
		BillingStaging.BillingFITExtractHistory
		LEFT OUTER JOIN RecHubBilling.BillingFITExtractHistory History
			ON (History.TableUsed = BillingStaging.BillingFITExtractHistory.TableUsed
				AND History.TableKey = BillingStaging.BillingFITExtractHistory.TableKey)
			OR (History.TableUsed = BillingStaging.BillingFITExtractHistory.TableUsed
				AND History.TableKey <> BillingStaging.BillingFITExtractHistory.TableKey
				AND History.DateKey = BillingStaging.BillingFITExtractHistory.DateKey
				AND History.FileList = BillingStaging.BillingFITExtractHistory.FileList
				AND History.BankID = BillingStaging.BillingFITExtractHistory.BankID
				AND History.ClientAccountID = BillingStaging.BillingFITExtractHistory.ClientAccountID
				)
	WHERE History.BillingFITExtractHistoryKey IS NULL	
	ORDER BY 
		BillingStaging.BillingFITExtractHistory.DateKey;

END TRY
BEGIN CATCH
       EXEC RecHubCommon.usp_WfsRethrowException
END CATCH