--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorTableName dimMeasureTypes
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/07/2017
*
* Purpose: A type 0 SCD holding information about measurements
*		   
*
* Modification History
* 03/09/2009 PT 149850327 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimMeasureTypes
CREATE TABLE RecHubMeasure.dimMeasureTypes
(
	MeasureTypeKey BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_dimMeasureTypes PRIMARY KEY NONCLUSTERED,
	MeasureTypeName NVARCHAR(256) NOT NULL,
	CreationDate DATETIME2
		CONSTRAINT DF_dimMeasureTypes_CreationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubMeasure.dimMeasureTypes.IDX_dimMeasureTypes_MeasureTypeName
CREATE UNIQUE CLUSTERED INDEX IDX_dimMeasureTypes_MeasureTypeName ON RecHubMeasure.dimMeasureTypes
(
	MeasureTypeName
);