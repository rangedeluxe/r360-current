--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorTableName factBatchImport
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/07/2017
*
* Purpose: Measurements about importing batch records. Grain 1 measurement per row.
*		   
*
* Modification History
* 03/09/2009 PT 149850327 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate factBatchImport
CREATE TABLE RecHubMeasure.factBatchImport
(
	factBatchImportKey BIGINT NOT NULL IDENTITY(1,1),
	IsDeleted BIT NOT NULL,
	IsSuccessful BIT NOT NULL,
	AuditDateKey INT NOT NULL,
	BatchKey BIGINT NOT NULL,
	MeasureTypeKey BIGINT NOT NULL,
	SourceTrackingKey BIGINT NOT NULL,
	BatchCountKey BIGINT NOT NULL,
	DurationMilliseconds BIGINT NOT NULL,
	CreationDate DATETIME2 NOT NULL,
	BatchTrackingID UNIQUEIDENTIFIER NOT NULL
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorForeignKey
ALTER TABLE RecHubMeasure.factBatchImport ADD 
	CONSTRAINT PK_factBatchImport PRIMARY KEY CLUSTERED (factBatchImportKey,AuditDateKey),
	CONSTRAINT FK_factBatchImport_dimBatches FOREIGN KEY(BatchKey) REFERENCES RecHubMeasure.dimBatches(BatchKey),
	CONSTRAINT FK_factBatchImport_dimBatchCounts FOREIGN KEY(BatchCountKey) REFERENCES RecHubMeasure.dimBatchCounts(BatchCountKey),
	CONSTRAINT FK_factBatchImport_dimMeasureTypes FOREIGN KEY(MeasureTypeKey) REFERENCES RecHubMeasure.dimMeasureTypes(MeasureTypeKey),
	CONSTRAINT FK_factBatchImport_dimSourceTracking FOREIGN KEY(SourceTrackingKey) REFERENCES RecHubMeasure.dimSourceTracking(SourceTrackingKey);
