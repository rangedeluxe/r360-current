--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorTableName dimBatches
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/07/2017
*
* Purpose: A type 0 SCD holding information about batches
*		   
*
* Modification History
* 03/09/2009 PT 149850327 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimBatches
CREATE TABLE RecHubMeasure.dimBatches
(
	BatchKey BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_dimBatch PRIMARY KEY NONCLUSTERED,
	SiteBankID INT NOT NULL,
	SiteWorkgroupID INT NOT NULL,
	DepositDateKey INT NOT NULL,
	ImmutableDateKey INT NOT NULL,
	SourceBatchID BIGINT NOT NULL,
	CreationDate DATETIME2 NOT NULL
		CONSTRAINT DF_dimBatch_CreationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubMeasure.dimBatches.IDX_dimBatches_SourceBatchID_SiteBankWorkgroupID_DepositImmutableDateKey
CREATE UNIQUE CLUSTERED INDEX IDX_dimBatches_SourceBatchID_SiteBankWorkgroupID_DepositImmutableDateKey ON RecHubMeasure.dimBatches
(
	SourceBatchID,
	ImmutableDateKey,
	DepositDateKey,
	SiteBankID,
	SiteWorkgroupID
);

