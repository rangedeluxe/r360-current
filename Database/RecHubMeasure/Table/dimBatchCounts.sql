--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorTableName dimBatchCounts
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/07/2017
*
* Purpose: A type 0 SCD holding information about batch counts
*		   
*
* Modification History
* 03/09/2009 PT 149850327 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate dimBatchCounts
CREATE TABLE RecHubMeasure.dimBatchCounts
(
	BatchCountKey BIGINT NOT NULL IDENTITY(1,1)
		CONSTRAINT PK_dimBatchCounts PRIMARY KEY NONCLUSTERED,
	TransactionCount INT NOT NULL,
	PaymentCount INT NOT NULL,
	StubCount INT NOT NULL,
	DocumentCount INT NOT NULL,
	CreationDate DATETIME2 NOT NULL
		CONSTRAINT DF_dimBatchCounts_CreationDate DEFAULT(GETDATE())
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex RecHubMeasure.dimBatchCounts.IDX_dimBatchCounts_BatchCountKey_TransactionPaymentStubDocumentCount
CREATE UNIQUE CLUSTERED INDEX IDX_dimBatchCounts_TransactionPaymentStubDocumentCount ON RecHubMeasure.dimBatchCounts
(
	TransactionCount,
	PaymentCount,
	StubCount,
	DocumentCount
);