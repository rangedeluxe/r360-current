--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorPermissions <Permissions>
--WFSScriptProcessorPermissions <Permission ID="dbRole_RecHubUser">EXECUTE</Permission>
--WFSScriptProcessorPermissions </Permissions>
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorStoredProcedureName usp_factBatchImport_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMeasure.usp_factBatchImport_Ins') IS NOT NULL
       DROP PROCEDURE RecHubMeasure.usp_factBatchImport_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMeasure.usp_factBatchImport_Ins 
(
	@parmSourceBatchID			BIGINT,
	@parmDurationMilliseconds	BIGINT,
	@parmBatchCount				INT,
	@parmSiteBankID				INT,
	@parmSiteWorkgroupID		INT,
	@parmDepositDateKey			INT,
	@parmImmutableDateKey		INT,
	@parmSourceTrackingID		UNIQUEIDENTIFIER,
	@parmBatchTrackingID		UNIQUEIDENTIFIER,
	@parmMeasureTypeName		NVARCHAR(256),
	@parmSourceFileName			NVARCHAR(260),
	@parmClientProcessCode		NVARCHAR(40),
	@parmIsSuccessful			BIT = 1,
	@parmTransactionCount		INT = -1,
	@parmPaymentCount			INT = -1,
	@parmStubCount				INT = -1,
	@parmDocumentCount			INT = -1
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/08/2017
*
* Purpose: Insert a new fact import measurement record.
*
*
* Modification History
* 08/08/2017 PT 149850327 JPB	Created
* 11/09/2017 PT 152447647 JPB	Added permissions
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @BatchKey BIGINT,
		@BatchCountKey BIGINT,
		@MeasureTypeKey BIGINT,
		@SourceTrackingKey BIGINT,
		@AuditDateKey INT,
		@CreationDate DATETIME2,
		@LocalTransaction BIT = 0;

BEGIN TRY

	SELECT
		@CreationDate = GETDATE();
	SELECT
		@AuditDateKey = CAST(CONVERT(VARCHAR,@CreationDate,112) AS INT);

	IF @@TRANCOUNT = 0 
	BEGIN 
		BEGIN TRANSACTION;
		SET @LocalTransaction = 1;
	END

	EXEC RecHubMeasure.usp_dimMeasureTypes_Get 
		@parmMeasureTypeName	= @parmMeasureTypeName,
		@parmMeasureTypeKey		= @MeasureTypeKey OUTPUT;

	EXEC RecHubMeasure.usp_dimSourceTracking_Get 
		@parmBatchCount			= @parmBatchCount,
		@parmSourceTrackingID	= @parmSourceTrackingID,
		@parmSourceFileName		= @parmSourceFileName,
		@parmClientProcessCode	= @parmClientProcessCode,
		@parmSourceTrackingKey  = @SourceTrackingKey OUTPUT;

	EXEC RecHubMeasure.usp_dimBatches_Get 
		@parmSiteBankID			= @parmSiteBankID,
		@parmSiteWorkgroupID	= @parmSiteWorkgroupID,
		@parmDepositDateKey		= @parmDepositDateKey,
		@parmImmutableDateKey	= @parmImmutableDateKey,
		@parmSourceBatchID		= @parmSourceBatchID,
		@parmBatchKey			= @BatchKey OUTPUT;

	IF( @parmTransactionCount = -1
		AND @parmPaymentCount = -1
		AND @parmStubCount = -1
		AND @parmDocumentCount = -1 )
		SET @BatchCountKey = -1;
	ELSE 
		EXEC RecHubMeasure.usp_dimBatchCounts_Get 
			@parmTransactionCount	= @parmTransactionCount,
			@parmPaymentCount		= @parmPaymentCount,
			@parmStubCount			= @parmStubCount,
			@parmDocumentCount		= @parmDocumentCount,
			@parmBatchCountKey		= @BatchCountKey OUTPUT;

	INSERT INTO RecHubMeasure.factBatchImport 
	(
		IsDeleted,
		IsSuccessful,
		AuditDateKey,
		BatchKey,
		MeasureTypeKey,
		SourceTrackingKey,
		BatchCountKey,
		DurationMilliseconds,
		CreationDate,
		BatchTrackingID
	)
	VALUES
	(
		0, /* IsDelete set to 0 on insert */
		@parmIsSuccessful,
		@AuditDateKey,
		@BatchKey,
		@MeasureTypeKey,
		@SourceTrackingKey,
		@BatchCountKey,
		@parmDurationMilliseconds,
		@CreationDate,
		@parmBatchTrackingID
	);

	--All good, commit the transaction
	IF @LocalTransaction = 1 COMMIT TRANSACTION;

END TRY
BEGIN CATCH
	-- something bad happened, rollback the transaction
	IF @LocalTransaction = 1 ROLLBACK TRANSACTION;
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
