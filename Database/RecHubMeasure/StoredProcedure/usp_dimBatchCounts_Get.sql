--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorStoredProcedureName usp_dimBatchCounts_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMeasure.usp_dimBatchCounts_Get') IS NOT NULL
       DROP PROCEDURE RecHubMeasure.usp_dimBatchCounts_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMeasure.usp_dimBatchCounts_Get 
(
	@parmTransactionCount INT,
	@parmPaymentCount INT,
	@parmStubCount INT,
	@parmDocumentCount INT,
	@parmBatchCountKey BIGINT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/08/2017
*
* Purpose: Retrieve BatchCountKey, insert if it is not there.
*
*
* Modification History
* 08/08/2017 PT 149850327 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Key BIGINT;
DECLARE @dimBatcheCountChanges TABLE (BatchCountKey BIGINT);

BEGIN TRY

	MERGE 
		INTO RecHubMeasure.dimBatchCounts AS dimBatchCountsTarget
	USING 
		(
			SELECT 
				@parmTransactionCount AS TransactionCount,
				@parmPaymentCount AS PaymentCount,
				@parmStubCount AS StubCount,
				@parmDocumentCount AS DocumentCount
		) AS dimBatchCountsSource
	ON 
		dimBatchCountsTarget.TransactionCount = dimBatchCountsSource.TransactionCount
		AND dimBatchCountsTarget.PaymentCount = dimBatchCountsSource.PaymentCount
		AND dimBatchCountsTarget.StubCount = dimBatchCountsSource.StubCount
		AND dimBatchCountsTarget.DocumentCount = dimBatchCountsSource.DocumentCount
	WHEN MATCHED THEN 
		UPDATE SET @parmBatchCountKey = @Key
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (TransactionCount,PaymentCount,StubCount,DocumentCount,CreationDate) VALUES (@parmTransactionCount,@parmPaymentCount,@parmStubCount,@parmDocumentCount,GETDATE())
	OUTPUT inserted.BatchCountKey INTO @dimBatcheCountChanges;

	SELECT @parmBatchCountKey = COALESCE(@Key,BatchCountKey) FROM @dimBatcheCountChanges;

	SELECT @parmBatchCountKey;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
