--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorStoredProcedureName usp_dimSourceTracking_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMeasure.usp_dimSourceTracking_Get') IS NOT NULL
       DROP PROCEDURE RecHubMeasure.usp_dimSourceTracking_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMeasure.usp_dimSourceTracking_Get 
(
	@parmBatchCount			INT,
	@parmSourceTrackingID	UNIQUEIDENTIFIER,
	@parmSourceFileName		NVARCHAR(260),
	@parmClientProcessCode  NVARCHAR(40),
	@parmSourceTrackingKey	BIGINT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/08/2017
*
* Purpose: Retrieve SourceTrackingID, insert if it is not there.
*
*
* Modification History
* 08/08/2017 PT 149850327 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Key BIGINT;
DECLARE @dimSourceChanges TABLE (SourceTrackingKey BIGINT);

BEGIN TRY

	MERGE 
		INTO RecHubMeasure.dimSourceTracking AS dimSourceTrackingTarget
	USING 
		(SELECT @parmSourceTrackingID AS SourceTrackingID) AS dimSourceTrackingSource
	ON 
		dimSourceTrackingTarget.SourceTrackingID = dimSourceTrackingSource.SourceTrackingID
	WHEN MATCHED THEN 
		UPDATE SET @parmSourceTrackingKey = @Key
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (BatchCount,SourceTrackingID,SourceFileName,ClientProcessCode,CreationDate) VALUES (@parmBatchCount,@parmSourceTrackingID,@parmSourceFileName,@parmClientProcessCode,GETDATE())
	OUTPUT inserted.SourceTrackingKey INTO @dimSourceChanges;

	SELECT @parmSourceTrackingKey = COALESCE(@Key,SourceTrackingKey) FROM @dimSourceChanges;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
