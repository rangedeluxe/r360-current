--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorStoredProcedureName usp_dimMeasureTypes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMeasure.usp_dimMeasureTypes_Get') IS NOT NULL
       DROP PROCEDURE RecHubMeasure.usp_dimMeasureTypes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMeasure.usp_dimMeasureTypes_Get 
(
	@parmMeasureTypeName	NVARCHAR(256),
	@parmMeasureTypeKey		BIGINT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/08/2017
*
* Purpose: Retrieve MeasureTypeKey, insert if it is not there.
*
*
* Modification History
* 08/08/2017 PT 149850327 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Key BIGINT;
DECLARE @dimMeasureTypeChanges TABLE (MeasureTypeKey BIGINT);

BEGIN TRY

	MERGE 
		INTO RecHubMeasure.dimMeasureTypes AS dimMeasureTypesTarget
	USING 
		(SELECT @parmMeasureTypeName AS MeasureTypeName) AS dimMeasureTypesSource
	ON 
		dimMeasureTypesTarget.MeasureTypeName = dimMeasureTypesSource.MeasureTypeName
	WHEN MATCHED THEN 
		UPDATE SET @parmMeasureTypeKey = @Key
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (MeasureTypeName,CreationDate) VALUES (@parmMeasureTypeName,GETDATE())
	OUTPUT inserted.MeasureTypeKey INTO @dimMeasureTypeChanges;

	SELECT @parmMeasureTypeKey = COALESCE(@Key,MeasureTypeKey) FROM @dimMeasureTypeChanges;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
