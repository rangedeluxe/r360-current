--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema RecHubMeasure
--WFSScriptProcessorStoredProcedureName usp_dimBatches_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('RecHubMeasure.usp_dimBatches_Get') IS NOT NULL
       DROP PROCEDURE RecHubMeasure.usp_dimBatches_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE RecHubMeasure.usp_dimBatches_Get 
(
	@parmSiteBankID INT,
	@parmSiteWorkgroupID INT,
	@parmDepositDateKey INT,
	@parmImmutableDateKey INT,
	@parmSourceBatchID BIGINT,
	@parmBatchKey BIGINT OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2017 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 08/08/2017
*
* Purpose: Retrieve BatchKey, insert if it is not there.
*
*
* Modification History
* 08/08/2017 PT 149850327 JPB	Created
******************************************************************************/
SET NOCOUNT ON; 

DECLARE @Key BIGINT;
DECLARE @dimBatchesChanges TABLE (BatchKey BIGINT);

BEGIN TRY

	MERGE 
		INTO RecHubMeasure.dimBatches AS dimBatchesTarget
	USING 
		(
			SELECT 
				@parmSiteBankID AS SiteBankID,
				@parmSiteWorkgroupID AS SiteWorkgroupID,
				@parmDepositDateKey AS DepositDateKey,
				@parmImmutableDateKey AS ImmutableDateKey,
				@parmSourceBatchID AS SourceBatchID
		) AS dimBatchesSource
	ON 
		dimBatchesTarget.SourceBatchID = dimBatchesSource.SourceBatchID
		AND dimBatchesTarget.SiteBankID = dimBatchesSource.SiteBankID
		AND dimBatchesTarget.SiteWorkgroupID = dimBatchesSource.SiteWorkgroupID
		AND dimBatchesTarget.DepositDateKey = dimBatchesSource.DepositDateKey
		AND dimBatchesTarget.ImmutableDateKey = dimBatchesSource.ImmutableDateKey
	WHEN MATCHED THEN 
		UPDATE SET @parmBatchKey = @Key
	WHEN NOT MATCHED BY TARGET THEN
		INSERT (SiteBankID,SiteWorkgroupID,DepositDateKey,ImmutableDateKey,SourceBatchID,CreationDate) 
		VALUES (@parmSiteBankID,@parmSiteWorkgroupID,@parmDepositDateKey,@parmImmutableDateKey,@parmSourceBatchID,GETDATE())
	OUTPUT inserted.BatchKey INTO @dimBatchesChanges;

	SELECT @parmBatchKey = COALESCE(@Key,BatchKey) FROM @dimBatchesChanges;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
