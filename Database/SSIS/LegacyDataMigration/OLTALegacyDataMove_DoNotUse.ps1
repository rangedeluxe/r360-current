﻿<#
	.SYNOPSIS
	PowerShell script used to execute the Leagcy Data Move SSIS Package.
	
	.DESCRIPTION
	The script will prepare the SSIS XML file and exexute the SSIS package to move a partition
	from the offline server to the production server.
	
	.PARAMETER StartDate
	The start date of the partition to be moved. The start date must be a Tuesday.
	
#>
param
(
	[parameter(Mandatory = $true)][datetime] $StartDate = "",
	[string] $ConfigFile = "LegacyDataMigration.dtsConfig"
) 

$ScriptVerison = "1.0";

Write-Host "OLTA Legacy Data Move PowerShell Script Version " $ScriptVerison

################################################################################
#
# 05/12/2011 JPB	CR 34316 1.0	Created.
#
################################################################################

cls

###############################################################################
#
# The first thing that has to be done is validate the input date
#
###############################################################################
if( $StartDate.DayOfWeek -ne 'Tuesday' )
{
	Write-Host "Start date must be a Tuesday.";
	break;
}

#Setup path and package information to be used
$PackageFolder = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
[System.IO.Directory]::SetCurrentDirectory((Get-Location -PSProvider FileSystem).ProviderPath);

$SSISPackage = "LegacyDataMove.dtsx";
$SSISConfiguration = $ConfigFile;

#Read the SSIS package config file for the source server/db name
$xml = New-Object "System.Xml.XmlDocument";
$xml.load($SSISConfiguration);
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[ServerName]") };
$DBServer = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[InitialCatalog]") };
$DBName = $ConfigurationNode.ConfiguredValue;

$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::PartitionName].Properties[Value]") };


#For now, only support Windows Auth
$dbConnectionOptions = ("Data Source=$DBServer; Initial Catalog=$DBName;Integrated Security=SSPI")
$dbConnection = New-Object System.Data.SqlClient.SqlConnection($dbConnectionOptions);
$dbConnection.Open();
$dbCommand = $dbConnection.CreateCommand();

#Get the partition name so the SSIS package XML can be updated with the correct information.
$dbCommand.CommandText = "
	SELECT	PartitionName
	FROM	dbo.LegacyDataMove
	WHERE	StartDate = {0}{1:00}{2:00}" -f $StartDate.Year,$StartDate.Month,$StartDate.Day;

$dbReader = $dbCommand.ExecuteReader();

if( !$dbReader.HasRows )
{
	Write-Host "Could not located PartitionName $PartitionName in the source database. Aborting....";
	break;
}
else
{
	$dbReader.Read() | Out-Null;
	$PartitionName = $dbReader['PartitionName'];
}

Write-Host "Source Server" $DBServer;
Write-Host "Source DBName" $DBName;
Write-Host "Partition Name" $PartitionName;


#Update the SSIS XML file with the new partition name.
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::PartitionName].Properties[Value]") };
$ConfigurationNode.ConfiguredValue = $PartitionName;
$xml.Save($SSISConfiguration);

#Create the command to execute the SSIS package
$CommandLine = New-Object System.Text.StringBuilder; 
[Void]$CommandLine.Append("dtexec ");
###SSIS Package name
[Void]$CommandLine.Append("/File ""$SSISPackage"" ");
###Add the configuration file 
[Void]$CommandLine.Append("/ConfigFile ""$SSISConfiguration"" ");

Write-Host $CommandLine;
cmd /c $CommandLine;
$CommandResult = $?
Write-Host $CommandResult;
#