--WFSScriptProcessorStoredProcedureName usp_LegacyDataMove_CalcPartitionCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_LegacyDataMove_VerifyPartitionCounts') IS NOT NULL
       DROP PROCEDURE dbo.usp_LegacyDataMove_VerifyPartitionCounts
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_LegacyDataMove_VerifyPartitionCounts
(
	@parmConversionStartDate DATETIME,
	@parmPartitionStartDate	DATETIME,
	@parmHTMLMessage NVARCHAR(MAX) OUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/20/2011
*
* Purpose: Populate the OTISQueue table via the LegacyDataMove table. To be used
*			only in the legacy data move process, install only on offline database.
*
* Modification History
* 01/20/2011 CR 34428 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @Msg VARCHAR(2047),
		@PartitionName VARCHAR(100),
		@StartDate DATETIME,
		@EndDate DATETIME,
		@StartDateKey INT,
		@EndDateKey INT,
		@DepositDateKey INT,
		@Loop INT,
		@htmlMsg NVARCHAR(MAX)

BEGIN TRY
	/* Figure out what is the start of the partition. */
	IF( DATEPART(dw,@parmPartitionStartDate) <> 3 )
	BEGIN 
		RAISERROR('Start Date must be a Tuesday',11,1)
	END

	SET @PartitionName = 'OLTAFacts' + CONVERT(VARCHAR,DATEADD(d,6,@parmPartitionStartDate),112)
	/* Verify that the partition exists. */
	IF NOT EXISTS(SELECT 1 FROM sys.database_files WHERE [Name] = @PartitionName)
	BEGIN
		RAISERROR('Partition %s could not be found.',11,1,@PartitionName)
	END
	
	/* Drop the temp tables if it is there. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
		DROP TABLE #tmpBatchList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OTISList')) 
		DROP TABLE #OTISList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CountsByDepositDateKey')) 
		DROP TABLE #CountsByDepositDateKey
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#MissingBatches')) 
		DROP TABLE #MissingBatches
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#MissingItems')) 
		DROP TABLE #MissingItems


	/* 	
		Create temp tables to hold list of batches to process. Using temp table instead of 
		table var because there could be 1,000s of records to process. 
	*/
	
	/*
		This table holds the list of batches that will actually be processed.
	*/
	CREATE TABLE #tmpBatchList
	(
		GlobalBatchID INT,
		BankID INT,
		LockboxID INT,
		BatchID INT,
		DepositDate DATETIME,
		ProcessingDate DATETIME
	)

	/*
		This table will hold the list of lockboxes that will actually be processed.
	*/
	CREATE TABLE #OTISList
	(
		BankID INT,
		LockboxID INT,
		DepositDate DATETIME
	)

	/*
		Store the counts from dbo/OLTA so they can be compared. 
	*/
	CREATE TABLE #CountsByDepositDateKey
	(
		DepositDateKey INT,
		BatchesInDBO INT,
		BatchesInOLTA INT,
		ChecksInDBO INT,
		ChecksInOLTA INT,
		DocumentsInDBO INT,
		DocumentsInOLTA INT,
		StubsInDBO INT,
		StubsInOLTA INT
	)
	
	/*
		Store the batches that are missing from OLTA.
	*/
	CREATE TABLE #MissingBatches
	(
		BankID INT,
		LockboxID INT,
		ProcessingDateKey INT,
		DepositDateKey INT,
		BatchID INT
	)

	/*
		Store information about individual items that are missing.
		ItemType:
			0 - Check
			1 - Document
			2 - Stub
	*/
	CREATE TABLE #MissingItems
	(
		ItemType TINYINT,
		BankID INT,
		LockboxID INT,
		ProcessingDate DATETIME,
		DepositDate DATETIME,
		BatchID INT,
		TransactionID INT,
		BatchSequence INT
	)
	
	/*
		Table var to store the deposit dates to check for missing batches.
	*/
	DECLARE @MissingBatchesDepositDates TABLE
	(
		RowID SMALLINT IDENTITY(1,1),
		DepositDateKey INT
	)

	/*
		Table var to store the deposit dates to check for missing checks.
	*/
	DECLARE @MissingChecksDepositDates TABLE
	(
		RowID SMALLINT IDENTITY(1,1),
		DepositDateKey INT
	)
	
	/*
		Table var to store the deposit dates to check for missing Documents.
	*/
	DECLARE @MissingDocumentsDepositDates TABLE
	(
		RowID SMALLINT IDENTITY(1,1),
		DepositDateKey INT
	)

	/*
		Table var to store the deposit dates to check for missing stubs.
	*/
	DECLARE @MissingStubsDepositDates TABLE
	(
		RowID SMALLINT IDENTITY(1,1),
		DepositDateKey INT
	)
	
	/* Get the deposit date range and the batch count for the partition being worked on. Use the partition name to figure this out. */
	SELECT 	@StartDateKey = CAST(CONVERT(varchar,DATEADD(DAY,-6,CONVERT(DATETIME,CONVERT(VARCHAR,SUBSTRING([name],LEN('OLTAFacts')+1,LEN([name])-LEN('OTLAFacts'))),112)),112) AS INT),
			@EndDateKey = CAST(SUBSTRING([name],LEN('OLTAFacts')+1,LEN([name])-LEN('OTLAFacts')) AS INT)
	FROM sys.database_files
	WHERE [Name] = @PartitionName


	SELECT 	@StartDate = CONVERT(DATETIME,CONVERT(VARCHAR,@StartDateKey),112),
			@EndDate = CONVERT(DATETIME,CONVERT(VARCHAR,@EndDateKey),112) + CONVERT(DATETIME,'23:59:59',114)

	/* begin a transaction so we can roll back if the record counts do not match */
	BEGIN TRANSACTION
	/*	
		This finds the items based on the CDSPurgeSetup table.
	*/
	INSERT INTO #OTISList(BankID,LockboxID,DepositDate)
	SELECT 	BankID,
			LockboxID,
			@parmConversionStartDate - RetentionDays AS DepositDate
	FROM	dbo.CDSPurgeSetup
	WHERE	(@parmConversionStartDate - RetentionDays) <= @EndDate
	ORDER BY RetentionDays

	/*
		This will add the Bank/Lockboxes that are not in the CDSPurgeSetup table. Any Bank/Lockbox that
		is not listed in the CDSPurgeSetup table has an infinite retention period.
	*/
	INSERT INTO #OTISList(BankID,LockboxID,DepositDate)
	SELECT	BankID,
			LockboxID,
			@parmConversionStartDate
	FROM	dbo.Lockbox
	EXCEPT
	SELECT 	BankID,
			LockboxID,
			@parmConversionStartDate
	FROM	dbo.CDSPurgeSetup
	
	/*
		Create a list of all the batches that match our retention from above.
	*/
	INSERT INTO #tmpBatchList(GlobalBatchID,BankID,LockboxID,BatchID,DepositDate,ProcessingDate)
	SELECT 	dbo.Batch.GlobalBatchID,dbo.Batch.BankID,dbo.Batch.LockboxID,dbo.Batch.BatchID,dbo.Batch.DepositDate,dbo.Batch.ProcessingDate
	FROM	dbo.Batch
			INNER JOIN #OTISList ON #OTISList.BankID = dbo.Batch.BankID AND #OTISList.LockboxID = dbo.Batch.LockboxID
	WHERE	dbo.Batch.DepositDate >= @StartDate
			AND dbo.Batch.DepositDate <= @EndDate


	/* 
		Insert into the deposit date working table the deposit dates for this partition.
	*/
	INSERT INTO #CountsByDepositDateKey(DepositDateKey)
	SELECT DISTINCT CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDate
	FROM	Batch
			INNER JOIN #tmpBatchList ON dbo.Batch.GlobalBatchID = #tmpBatchList.GlobalBatchID
 
 	/* 
		Calculate the totals for each type using a big CTE and update the deposit date working table with the results 
	*/
 	;WITH BatchCounts AS
	(
		SELECT	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDate, COUNT(*) AS BatchCount
		FROM	dbo.Batch
				INNER JOIN #tmpBatchList ON dbo.Batch.GlobalBatchID = #tmpBatchList.GlobalBatchID
		GROUP BY #tmpBatchList.DepositDate
	),
	BatchesByDepositDate AS
	(
			SELECT	DepositDate, SUM(BatchCount) AS BatchCount
			FROM	BatchCounts
			GROUP BY DepositDate
	),
	CheckCounts AS
	(
		SELECT	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDate, COUNT(*) AS CheckCount
		FROM	dbo.Checks
				INNER JOIN #tmpBatchList ON dbo.Checks.GlobalBatchID = #tmpBatchList.GlobalBatchID
		GROUP BY DepositDate
	),
	ChecksByDepositDate AS
	(
			SELECT	DepositDate, SUM(CheckCount) AS CheckCount
			FROM	CheckCounts
			GROUP BY DepositDate
	),
	DocumentCounts AS
	(
		SELECT	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDate, COUNT(*) AS DocumentCount
		FROM	dbo.Documents
				INNER JOIN #tmpBatchList ON dbo.Documents.GlobalBatchID = #tmpBatchList.GlobalBatchID
		GROUP BY DepositDate
	),
	DocumentsByDepositDate AS
	(
			SELECT	DepositDate, SUM(DocumentCount) AS DocumentCount
			FROM	DocumentCounts
			GROUP BY DepositDate
	),
	StubCounts AS
	(
		SELECT	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDate, COUNT(*) AS StubCount
		FROM	dbo.Stubs
				INNER JOIN #tmpBatchList ON dbo.Stubs.GlobalBatchID = #tmpBatchList.GlobalBatchID
		GROUP BY DepositDate
	),
	StubsByDepositDate AS
	(
			SELECT	DepositDate, SUM(StubCount) AS StubCount
			FROM	StubCounts
			GROUP BY DepositDate
	),
	factBatchSummary AS
	(
		SELECT 	DepositDateKey, COUNT(*) AS BatchCount
		FROM	OLTA.factBatchSummary
		WHERE	DepositDateKey >= @StartDateKey
				AND DepositDateKey <= @EndDateKey
		GROUP BY DepositDateKey
	),
	factChecks AS
	(
		SELECT 	DepositDateKey, COUNT(*) AS CheckCount
		FROM	OLTA.factChecks
		WHERE	DepositDateKey >= @StartDateKey
				AND DepositDateKey <= @EndDateKey
		GROUP BY DepositDateKey
	),
	factDocuments AS
	(
		SELECT 	DepositDateKey, COUNT(*) AS DocumentCount
		FROM	OLTA.factDocuments
		WHERE	DepositDateKey >= @StartDateKey
				AND DepositDateKey <= @EndDateKey
		GROUP BY DepositDateKey
	),
	factStubs AS
	(
		SELECT 	DepositDateKey, COUNT(*) AS StubCount
		FROM	OLTA.factStubs
		WHERE	DepositDateKey >= @StartDateKey
				AND DepositDateKey <= @EndDateKey
		GROUP BY DepositDateKey
	)
	UPDATE #CountsByDepositDateKey
	SET 	BatchesInDBO = BatchesByDepositDate.BatchCount,
			ChecksInDBO = COALESCE(ChecksByDepositDate.CheckCount,0),
			DocumentsInDBO = COALESCE(DocumentsByDepositDate.DocumentCount,0),
			StubsInDBO = COALESCE(StubsByDepositDate.StubCount,0),
			BatchesInOLTA = COALESCE(factBatchSummary.BatchCount,0),
			ChecksInOLTA = COALESCE(factChecks.CheckCount,0),
			DocumentsInOLTA = COALESCE(factDocuments.DocumentCount,0),
			StubsInOLTA = COALESCE(factStubs.StubCount,0)
	FROM	BatchesByDepositDate
			LEFT JOIN ChecksByDepositDate ON ChecksByDepositDate.DepositDate = BatchesByDepositDate.DepositDate
			LEFT JOIN DocumentsByDepositDate ON DocumentsByDepositDate.DepositDate = BatchesByDepositDate.DepositDate
			LEFT JOIN StubsByDepositDate ON StubsByDepositDate.DepositDate = BatchesByDepositDate.DepositDate
			LEFT JOIN factBatchSummary ON factBatchSummary.DepositDateKey = BatchesByDepositDate.DepositDate
			LEFT JOIN factChecks ON factChecks.DepositDateKey = BatchesByDepositDate.DepositDate
			LEFT JOIN factDocuments ON factDocuments.DepositDateKey = BatchesByDepositDate.DepositDate
			LEFT JOIN factStubs ON factStubs.DepositDateKey = BatchesByDepositDate.DepositDate
			INNER JOIN #CountsByDepositDateKey ON #CountsByDepositDateKey.DepositDateKey = BatchesByDepositDate.DepositDate

	COMMIT TRANSACTION

	SELECT @htmlMsg = CAST(
	(
		SELECT 	CAST(DepositDateKey  AS VARCHAR) AS 'td',
				'',
				CAST(BatchesInDBO AS VARCHAR) AS 'td',
				'',
				CAST(BatchesInOLTA AS VARCHAR) AS 'td',
				'',
				CAST(ChecksInDBO AS VARCHAR) AS 'td',
				'',
				CAST(ChecksInOLTA AS VARCHAR) AS 'td',
				'',
				CAST(DocumentsInDBO AS VARCHAR) AS 'td',
				'',
				CAST(DocumentsInOLTA AS VARCHAR) AS 'td',
				'',
				CAST(StubsInDBO AS VARCHAR) AS 'td',
				'',
				CAST(StubsInOLTA AS VARCHAR) AS 'td',
				''
		FROM #CountsByDepositDateKey
		FOR XML PATH('tr'),ELEMENTS 
	)AS NVARCHAR(MAX))
	SET @parmHTMLMessage = '<table><tr align=center><table border=10><tr><th colspan=9 align=center>Partition Summary</th></tr><tr><th>Deposit Date</th><th>Batches in dbo</th><th>Batches in OLTA</th><th>Checks in dbo</th><th>Checks in OLTA</th><th>Documents in dbo</th><th>Documents in OLTA</th><th>Stubs in dbo</th><th>Stubs in OLTA</th></tr>' + @htmlMsg + '</table></tr><tr></tr>'
	SET @parmHTMLMessage = @parmHTMLMessage + '<tr align=center><td colspan=9>The above table lists the deposit dates in the partition along with the counts for Batches, Checks, Documents and Stubs for each schema.</td></tr><tr></tr>'

	/* Find any missing batches */

	INSERT INTO @MissingBatchesDepositDates(DepositDateKey)
	SELECT 	DepositDateKey 
	FROM 	#CountsByDepositDateKey
	WHERE 	BatchesInDBO <> BatchesInOLTA 

	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @Loop = 1
		WHILE( @Loop <= (SELECT MAX(RowID) FROM @MissingBatchesDepositDates) )
		BEGIN
			SELECT 	@DepositDateKey = DepositDateKey
			FROM	@MissingBatchesDepositDates
			WHERE	RowID = @Loop

			;WITH BatchDetailList AS
			(
				SELECT	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) AS DepositDateKey,CAST(CONVERT(VARCHAR,#tmpBatchList.ProcessingDate,112) AS INT) AS ProcessingDateKey,Batch.BankID,Batch.LockboxID,Batch.BatchID
				FROM	#tmpBatchList 
						INNER JOIN dbo.Batch ON dbo.Batch.GlobalBatchID = #tmpBatchList.GlobalBatchID
				WHERE 	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) = @DepositDateKey
			)
			INSERT INTO #MissingBatches(DepositDateKey,BankID,LockboxID,BatchID,ProcessingDateKey)
			SELECT 	DepositDateKey,BankID,LockboxID,BatchID,ProcessingDateKey
			FROM	BatchDetailList
			EXCEPT 
			SELECT	OLTA.factBatchSummary.DepositDateKey,OLTA.dimLockboxes.SiteBankID,OLTA.dimLockboxes.SiteLockboxID,OLTA.factBatchSummary.BatchID,OLTA.factBatchSummary.ProcessingDateKey
			FROM	OLTA.factBatchSummary
					INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey
			--inc looop counter
			SET @Loop = @Loop + 1;
		END
		
	END

	/* delete the missing batches from the #tmpBatchList table as they are not needed any more. I.e. do not repeat a search for items when we know the batch is missing. */
	DELETE 	#tmpBatchList
	FROM 	#tmpBatchList
			RIGHT OUTER JOIN #MissingBatches ON #MissingBatches.BankID = #tmpBatchList.BankID
					AND #MissingBatches.LockboxID = #tmpBatchList.LockboxID
					AND #MissingBatches.BatchID = #tmpBatchList.BatchID
					AND #MissingBatches.ProcessingDateKey = CAST(CONVERT(VARCHAR,#tmpBatchList.ProcessingDate,112) AS INT)


	IF( SELECT COUNT(*) FROM #MissingBatches) > 0
	BEGIN
		SELECT @htmlMsg = CAST(
		(
			SELECT 	CAST(dbo.Batch.BankID AS VARCHAR) AS 'td',
					'',
					CAST(dbo.Batch.LockboxID AS VARCHAR) AS 'td',
					'',
					CONVERT(varchar,dbo.Batch.ProcessingDate,101) AS 'td',
					'',
					CAST(dbo.Batch.BatchID AS VARCHAR) AS 'td'
			FROM #MissingBatches
					INNER JOIN dbo.Batch ON dbo.Batch.BankID = #MissingBatches.BankID
						AND dbo.Batch.LockboxID = #MissingBatches.LockboxiD
						AND CAST(CONVERT(VARCHAR,dbo.Batch.ProcessingDate,112) AS INT) = #MissingBatches.ProcessingDateKey
						AND dbo.Batch.BatchID = #MissingBatches.BatchID
			ORDER BY dbo.Batch.ProcessingDate, dbo.Batch.BankID, dbo.Batch.LockboxID,  dbo.Batch.BatchID
			FOR XML PATH('tr'),ELEMENTS 
		) AS NVARCHAR(MAX))
	END
	ELSE
	BEGIN
		SELECT @htmlMsg = CAST(
		(
			SELECT 	'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					''
			FOR XML PATH('tr'),ELEMENTS 
		) AS NVARCHAR(MAX))
		SET @htmlMsg = REPLACE(@htmlMsg,'nbsp;','&nbsp;')
	END
	SET @parmHTMLMessage = @parmHTMLMessage + '<tr align=center><table border=10><tr><th colspan=4 align=center>Batch(es) Missing from OLTA</th></tr><tr><th>Bank ID</th><th>Lockbox ID</th><th>Procesing Date</th><th>Batch ID</th></tr>' + @htmlMsg + '</table></tr><tr></tr>'

	/* Find any missing checks */
	
	INSERT INTO @MissingChecksDepositDates(DepositDateKey)
	SELECT 	DepositDateKey 
	FROM 	#CountsByDepositDateKey
	WHERE 	ChecksInDBO <> ChecksInOLTA
	
	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @Loop = 1
		WHILE( @Loop <= (SELECT MAX(RowID) FROM @MissingChecksDepositDates) )
		BEGIN
			SELECT 	@DepositDateKey = DepositDateKey
			FROM	@MissingChecksDepositDates
			WHERE	RowID = @Loop
			
			;WITH dboCheckList AS
			(
				SELECT 	MAX(BankID) AS BankID,MAX(LockboxID) AS LockboxID,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)) AS DepositDateKey,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.ProcessingDate,112) AS INT)) AS ProcessingDateKey,MAX(BatchID) AS BatchID,COUNT(*) AS CheckCount
				FROM	dbo.Checks
						INNER JOIN #tmpBatchList ON dbo.Checks.GlobalBatchID = #tmpBatchList.GlobalBatchID
				WHERE	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) = @DepositDateKey
				GROUP BY #tmpBatchList.GlobalBatchID
			),
			OLTACheckList AS
			(
				SELECT 	OLTA.factChecks.DepositDateKey,OLTA.dimLockboxes.SiteBankID AS BankID,OLTA.dimLockboxes.SiteLockboxID AS LockboxID,OLTA.factChecks.BatchID,MAX(OLTA.factChecks.ProcessingDateKey) AS ProcessingDateKey,COUNT(OLTA.factChecks.DepositDateKey) AS CheckCount
				FROM	OLTA.factChecks
						INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.factChecks.LockboxKey
						INNER JOIN #tmpBatchList ON
							OLTA.factChecks.DepositDateKey = CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)
							AND OLTA.dimLockboxes.SiteBankID = #tmpBatchList.BankID
							AND OLTA.dimLockboxes.SiteLockboxID = #tmpBatchList.LockboxID
							AND OLTA.factChecks.BatchID = #tmpBatchList.BatchID
				GROUP BY OLTA.factChecks.DepositDateKey,OLTA.dimLockboxes.SiteBankID,OLTA.dimLockboxes.SiteLockboxID,OLTA.factChecks.BatchID
			),
			MissingChecks AS
			(
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, CheckCount
				FROM	dboCheckList
				EXCEPT 
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, CheckCount
				FROM	OLTACheckList
			)
			INSERT INTO #MissingItems(ItemType,BankID,LockboxID,BatchID,ProcessingDate,DepositDate,TransactionID,BatchSequence)
			SELECT	0 AS ItemType,dbo.Batch.BankID, dbo.Batch.LockboxID, dbo.Batch.BatchID, dbo.Batch.ProcessingDate, dbo.Batch.DepositDate, dbo.Checks.TransactionID, dbo.Checks.BatchSequence
			FROM	dbo.Batch			
					INNER JOIN MissingChecks ON dbo.Batch.BankID = MissingChecks.BankID
						AND dbo.Batch.LockboxID = MissingChecks.LockboxID
						AND dbo.Batch.BatchID = MissingChecks.BatchID
						AND CAST(CONVERT(VARCHAR,dbo.Batch.ProcessingDate,112) AS INT) = MissingChecks.ProcessingDateKey
					INNER JOIN dbo.Checks ON dbo.Checks.GlobalBatchID = dbo.Batch.GlobalBatchID
			SET @Loop = @Loop + 1;
		END
	END
	
	/* Find any missing documents */
	
	INSERT INTO @MissingDocumentsDepositDates(DepositDateKey)
	SELECT 	DepositDateKey 
	FROM 	#CountsByDepositDateKey
	WHERE 	DocumentsInDBO <> DocumentsInOLTA
	
	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @Loop = 1
		WHILE( @Loop <= (SELECT MAX(RowID) FROM @MissingDocumentsDepositDates) )
		BEGIN
			SELECT 	@DepositDateKey = DepositDateKey
			FROM	@MissingDocumentsDepositDates
			WHERE	RowID = @Loop
			
			;WITH dboDocumentList AS
			(
				SELECT 	MAX(BankID) AS BankID,MAX(LockboxID) AS LockboxID,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)) AS DepositDateKey,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.ProcessingDate,112) AS INT)) AS ProcessingDateKey,MAX(BatchID) AS BatchID,COUNT(*) AS DocumentCount
				FROM	dbo.Documents
						INNER JOIN #tmpBatchList ON dbo.Documents.GlobalBatchID = #tmpBatchList.GlobalBatchID
				WHERE	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) = @DepositDateKey
				GROUP BY #tmpBatchList.GlobalBatchID
			),
			OLTADocumentList AS
			(
				SELECT 	OLTA.factDocuments.DepositDateKey,OLTA.dimLockboxes.SiteBankID AS BankID,OLTA.dimLockboxes.SiteLockboxID AS LockboxID,OLTA.factDocuments.BatchID,MAX(OLTA.factDocuments.ProcessingDateKey) AS ProcessingDateKey,COUNT(OLTA.factDocuments.DepositDateKey) AS DocumentCount
				FROM	OLTA.factDocuments
						INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.factDocuments.LockboxKey
						INNER JOIN #tmpBatchList ON
							OLTA.factDocuments.DepositDateKey = CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)
							AND OLTA.dimLockboxes.SiteBankID = #tmpBatchList.BankID
							AND OLTA.dimLockboxes.SiteLockboxID = #tmpBatchList.LockboxID
							AND OLTA.factDocuments.BatchID = #tmpBatchList.BatchID
				GROUP BY OLTA.factDocuments.DepositDateKey,OLTA.dimLockboxes.SiteBankID,OLTA.dimLockboxes.SiteLockboxID,OLTA.factDocuments.BatchID
			),
			MissingDocuments AS
			(
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, DocumentCount
				FROM	dboDocumentList
				EXCEPT 
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, DocumentCount
				FROM	OLTADocumentList
			)
			INSERT INTO #MissingItems(ItemType,BankID,LockboxID,BatchID,ProcessingDate,DepositDate,TransactionID,BatchSequence)
			SELECT	1 AS ItemType,dbo.Batch.BankID, dbo.Batch.LockboxID, dbo.Batch.BatchID, dbo.Batch.ProcessingDate, dbo.Batch.DepositDate, dbo.Documents.TransactionID, dbo.Documents.BatchSequence
			FROM	dbo.Batch			
					INNER JOIN MissingDocuments ON dbo.Batch.BankID = MissingDocuments.BankID
						AND dbo.Batch.LockboxID = MissingDocuments.LockboxID
						AND dbo.Batch.BatchID = MissingDocuments.BatchID
						AND CAST(CONVERT(VARCHAR,dbo.Batch.ProcessingDate,112) AS INT) = MissingDocuments.ProcessingDateKey
					INNER JOIN dbo.Documents ON dbo.Documents.GlobalBatchID = dbo.Batch.GlobalBatchID
			SET @Loop = @Loop + 1;
		END
	END

	/* Find any missing stubs */
	
	INSERT INTO @MissingStubsDepositDates(DepositDateKey)
	SELECT 	DepositDateKey 
	FROM 	#CountsByDepositDateKey
	WHERE 	StubsInDBO <> StubsInOLTA
	
	IF( @@ROWCOUNT > 0 )
	BEGIN
		SET @Loop = 1
		WHILE( @Loop <= (SELECT MAX(RowID) FROM @MissingStubsDepositDates) )
		BEGIN
			SELECT 	@DepositDateKey = DepositDateKey
			FROM	@MissingStubsDepositDates
			WHERE	RowID = @Loop
			
			;WITH dboStubList AS
			(
				SELECT 	MAX(BankID) AS BankID,MAX(LockboxID) AS LockboxID,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)) AS DepositDateKey,MAX(CAST(CONVERT(VARCHAR,#tmpBatchList.ProcessingDate,112) AS INT)) AS ProcessingDateKey,MAX(BatchID) AS BatchID,COUNT(*) AS StubCount
				FROM	dbo.Stubs
						INNER JOIN #tmpBatchList ON dbo.Stubs.GlobalBatchID = #tmpBatchList.GlobalBatchID
				WHERE	CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT) = @DepositDateKey
				GROUP BY #tmpBatchList.GlobalBatchID
			),
			OLTAStubList AS
			(
				SELECT 	OLTA.factStubs.DepositDateKey,OLTA.dimLockboxes.SiteBankID AS BankID,OLTA.dimLockboxes.SiteLockboxID AS LockboxID,OLTA.factStubs.BatchID,MAX(OLTA.factStubs.ProcessingDateKey) AS ProcessingDateKey,COUNT(OLTA.factStubs.DepositDateKey) AS StubCount
				FROM	OLTA.factStubs
						INNER JOIN OLTA.dimLockboxes ON OLTA.dimLockboxes.LockboxKey = OLTA.factStubs.LockboxKey
						INNER JOIN #tmpBatchList ON
							OLTA.factStubs.DepositDateKey = CAST(CONVERT(VARCHAR,#tmpBatchList.DepositDate,112) AS INT)
							AND OLTA.dimLockboxes.SiteBankID = #tmpBatchList.BankID
							AND OLTA.dimLockboxes.SiteLockboxID = #tmpBatchList.LockboxID
							AND OLTA.factStubs.BatchID = #tmpBatchList.BatchID
				GROUP BY OLTA.factStubs.DepositDateKey,OLTA.dimLockboxes.SiteBankID,OLTA.dimLockboxes.SiteLockboxID,OLTA.factStubs.BatchID
			),
			MissingStubs AS
			(
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, StubCount
				FROM	dboStubList
				EXCEPT 
				SELECT	BankID, LockboxID, BatchID, DepositDateKey, ProcessingDateKey, StubCount
				FROM	OLTAStubList
			)
			INSERT INTO #MissingItems(ItemType,BankID,LockboxID,BatchID,ProcessingDate,DepositDate,TransactionID,BatchSequence)
			SELECT	2 AS ItemType,dbo.Batch.BankID, dbo.Batch.LockboxID, dbo.Batch.BatchID, dbo.Batch.ProcessingDate, dbo.Batch.DepositDate, dbo.Stubs.TransactionID, dbo.Stubs.BatchSequence
			FROM	dbo.Batch			
					INNER JOIN MissingStubs ON dbo.Batch.BankID = MissingStubs.BankID
						AND dbo.Batch.LockboxID = MissingStubs.LockboxID
						AND dbo.Batch.BatchID = MissingStubs.BatchID
						AND CAST(CONVERT(VARCHAR,dbo.Batch.ProcessingDate,112) AS INT) = MissingStubs.ProcessingDateKey
					INNER JOIN dbo.Stubs ON dbo.Stubs.GlobalBatchID = dbo.Batch.GlobalBatchID
			SET @Loop = @Loop + 1;
		END
	END

	IF( SELECT COUNT(*) FROM #MissingItems) > 0
	BEGIN
		SELECT @htmlMsg = CAST(
		(
			SELECT 	CAST(BankID AS VARCHAR) AS 'td',
					'',
					CAST(LockboxID AS VARCHAR) AS 'td',
					'',				
					CONVERT(varchar,ProcessingDate,101) AS 'td',
					'',
					CONVERT(varchar,DepositDate,101)+' '+ CONVERT(varchar,DepositDate,108) AS 'td',
					'',
					CAST(BatchID AS VARCHAR) AS 'td',
					'',
					CASE
						WHEN ItemType = 0 THEN 'Check'
						WHEN ItemType = 1 THEN 'Document'
						WHEN ItemType = 2 THEN 'Stub'
					END AS 'td',
					'',
					CAST(TransactionID AS VARCHAR) AS 'td',
					'',
					CAST(BatchSequence AS VARCHAR) AS 'td'
			FROM #MissingItems
			ORDER BY ProcessingDate,BankID,LockboxID,BatchID,TransactionID,BatchSequence
			FOR XML PATH('tr'),ELEMENTS 
		)AS NVARCHAR(MAX))
	END
	ELSE
	BEGIN
		SELECT @htmlMsg = CAST(
		(
			SELECT 	'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					'',
					'nbsp;' AS 'td',
					''
			FOR XML PATH('tr'),ELEMENTS 
		) AS NVARCHAR(MAX))
		SET @htmlMsg = REPLACE(@htmlMsg,'nbsp;','&nbsp;')
	END
	SET @parmHTMLMessage = @parmHTMLMessage + '<tr align=center><table border=10><tr><th colspan=8 align=center>Item(s) Missing from OLTA</th></tr><tr><th>Bank ID</th><th>Lockbox ID</th><th>Procesing Date</th><th>Deposit Date</th><th>Batch ID</th><th>Item</th><th>Transaction ID</th><th>Batch Sequence</th></tr>' + @htmlMsg + '</table><tr></table>'

	/* Drop the temp table if it is there. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
		DROP TABLE #tmpBatchList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OTISList')) 
		DROP TABLE #OTISList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CountsByDepositDateKey')) 
		DROP TABLE #CountsByDepositDateKey
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#MissingBatches')) 
		DROP TABLE #MissingBatches
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..##MissingChecks')) 
		DROP TABLE #MissingChecks
END TRY
BEGIN CATCH
	/* Drop the temp table if it is there. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
		DROP TABLE #tmpBatchList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OTISList')) 
		DROP TABLE #OTISList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#CountsByDepositDateKey')) 
		DROP TABLE #CountsByDepositDateKey
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#MissingBatches')) 
		DROP TABLE #MissingBatches
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#MissingItems')) 
		DROP TABLE #MissingItems
	/* roll back any open transactions */
	IF XACT_STATE() != 0 
		ROLLBACK TRANSACTION
	/* Call standard method of notifing caller of the error. */
	EXEC dbo.usp_WfsRethrowException 
END CATCH
