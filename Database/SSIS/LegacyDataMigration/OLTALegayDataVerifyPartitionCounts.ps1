﻿<#
	.SYNOPSIS
	PowerShell script used to call stored procedure that calculate items totals for LegacyDataMove table.
	
	.DESCRIPTION
	The script will call stored procedure to calculate items totals for the LegacyDataMove table.
	The script uses the SSIS configuration file to determine the database server/name and 
	the conversion started state.
	
	.PARAMETER StartDate
	The start date of the partition to be moved. The start date must be a Tuesday.

	.PARAMETER ConverisonStartDate
	The start date of the partition to be moved. The start date must be a Tuesday.
	
#>
param
(
	[parameter(Mandatory = $true)][datetime] $StartDate = "",
	[string] $ConfigFile = "LegacyDataMigration.dtsConfig"
) 

$ScriptVerison = "1.0";

Write-Host "OLTA Legacy Data Move Detailed Item Check PowerShell Script Version " $ScriptVerison

################################################################################
#
# 06/09/2011 JPB	CR 45178 1.0	Created.
#
################################################################################

cls

###############################################################################
#
# The first thing that has to be done is validate the input date
#
###############################################################################
if( $StartDate.DayOfWeek -ne 'Tuesday' )
{
	Write-Host "Start date must be a Tuesday.";
	break;
}

#Setup path and package information to be used
$PackageFolder = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
[System.IO.Directory]::SetCurrentDirectory((Get-Location -PSProvider FileSystem).ProviderPath);

$SSISPackage = "LegacyDataMove.dtsx";
$SSISConfiguration = $ConfigFile;

#Read the SSIS package config file for the source server/db name
$xml = New-Object "System.Xml.XmlDocument";
$xml.load($SSISConfiguration);
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[ServerName]") };
$DBServer = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[InitialCatalog]") };
$DBName = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::ConversionStartDate].Properties[Value]") };
$ConversionStartDate = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SMTPConnection].Properties[SmtpServer]") };
$smtpServer = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::MailFrom].Properties[Value]") };
$emailFrom = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::MailTo].Properties[Value]") };
$emailTo = $ConfigurationNode.ConfiguredValue;

Write-Host "Source Server" $DBServer;
Write-Host "Source DBName" $DBName;
Write-Host "Start Date" $StartDate;
Write-Host "Conversion Start Date" $ConversionStartDate;
Write-Host 
Write-Host "SMTP Server" $smtpServer;
Write-Host "email From"  $emailFrom;
Write-Host "email To"    $emailTo;

#For now, only support Windows Auth
$dbConnectionOptions = ("Data Source=$DBServer; Initial Catalog=$DBName;Integrated Security=SSPI")
$dbConnection = New-Object System.Data.SqlClient.SqlConnection;
$dbConnection.ConnectionString = $dbConnectionOptions;
$dbCommand = New-Object System.Data.SqlClient.SqlCommand;
$dbCommand.Connection = $dbConnection;
$dbCommand.CommandTimeout = 21600; #give it 6 hours to run
$dbCommand.CommandType = [System.Data.CommandType]'StoredProcedure';
$dbCommand.CommandText = "dbo.usp_LegacyDataMove_VerifyPartitionCounts";
#$dbCommand.CommandText = "dbo.usp_ver_test";
$SD = "{0}/{1}/{2}" -f $StartDate.Month,$StartDate.Day,$StartDate.Year;
$dbCommand.Parameters.AddWithValue("@parmPartitionStartDate", "$SD") | Out-Null;
$dbCommand.Parameters.AddWithValue("@parmConversionStartDate", "$ConversionStartDate") | Out-Null;
#setup an out param for the html data
$outParameter = new-object System.Data.SqlClient.SqlParameter;
$outParameter.ParameterName = "@parmHTMLMessage";
$outParameter.Direction = [System.Data.ParameterDirection]'Output';
$outParameter.DbType = [System.Data.DbType]'String';
$outParameter.Size = 64000;
$dbCommand.Parameters.Add($outParameter) | Out-Null;
$dbConnection.Open();
Write-Host "Executing Detailed Partition Verification";
$Results = $dbCommand.ExecuteNonQuery();
if( $Results -eq -1 )
{
	$body = $dbCommand.Parameters["@parmHTMLMessage"].Value
	$body = "<html><body><div align=center><H1>Detailed Partition Verification Report</H1></div>" + $body + "</body></html>";
	Send-MailMessage -SmtpServer $smtpServer -To $emailTo -From $emailFrom -BodyAsHtml $body -Subject "Detailed Partition Verification Report"
}
else
{
	$body = "<html><body><div align=center><H1>Detailed Partition Verification Report</H1></div><p>Database Error</p></body></html>"
	Send-MailMessage -SmtpServer $smtpServer -To $emailTo -From $emailFrom -BodyAsHtml $body -Subject "Detailed Partition Verification Report"
}
$dbConnection.Close();
