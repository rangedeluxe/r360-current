--WFSScriptProcessorStoredProcedureName usp_LegacyDataMove_CalcPartitionCounts
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_LegacyDataMove_CalcPartitionCounts') IS NOT NULL
       DROP PROCEDURE dbo.usp_LegacyDataMove_CalcPartitionCounts
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_LegacyDataMove_CalcPartitionCounts
(
	@parmConversionStartDate DATETIME,
	@parmPartitionStartDate	DATETIME
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 01/20/2011
*
* Purpose: Calculate the number of batches, checks, documents and stubs on both
*		the dbo and OLTA tables.
*
* Modification History
* 01/20/2011 CR 34317 JPB	Created
******************************************************************************/
SET NOCOUNT ON 
SET ARITHABORT ON

DECLARE @Msg VARCHAR(2047),
		@PartitionName VARCHAR(100),
		@StartDate DATETIME,
		@EndDate DATETIME,
		@StartDateKey INT,
		@EndDateKey INT,
		@BatchesInDBO INT,
		@BatchesInOLTA INT,
		@ChecksInDBO INT,
		@ChecksInOLTA INT,
		@DocumentsInDBO INT,
		@DocumentsInOLTA INT,
		@StubsInDBO INT,
		@StubsInOLTA INT,
		@BankID INT,
		@LockboxID INT,
		@ProcessingDate DATETIME,
		@BatchID INT,
		@Loop INT

BEGIN TRY
	/* Figure out what is the start of the partition. */
	IF( DATEPART(dw,@parmPartitionStartDate) <> 3 )
	BEGIN 
		RAISERROR('Start Date must be a Tuesday',11,1)
	END

	SET @PartitionName = 'OLTAFacts' + CONVERT(VARCHAR,DATEADD(d,6,@parmPartitionStartDate),112)
	/* Verify that the partition exists. */
	IF NOT EXISTS(SELECT 1 FROM LegacyDataMove WHERE PartitionName = @PartitionName)
	BEGIN
		RAISERROR('Partition %s could not be found.',11,1,@PartitionName)
	END
	
	/* Verify that the partition totals have not been processed yet. */
	IF NOT EXISTS(SELECT 1 FROM LegacyDataMove WHERE PartitionName = @PartitionName AND CountsValidated = 0)
	BEGIN
		RAISERROR('Partition %s has already been processed.',11,1,@PartitionName)
	END
	/* Drop the temp tables if it is there. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
		DROP TABLE #tmpBatchList
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#OTISList')) 
		DROP TABLE #OTISList

	/* 	
		Create temp tables to hold list of batches to process. Using temp table instead of 
		table var because there could be 1,000s of records to process. 
	*/
	
	/*
		This table holds the list of batches that will actually be processed.
	*/
	CREATE TABLE #tmpBatchList
	(
		GlobalBatchID INT,
		DepositDate DATETIME
	)

	/*
		This table will hold the list of lockboxes that will actually be processed.
	*/
	CREATE TABLE #OTISList
	(
		BankID INT,
		LockboxID INT,
		DepositDate DATETIME
	)

	
	/* Get the deposit date range and the batch count for the partition being worked on. */
	SELECT 	@StartDate = CONVERT(DATETIME,CONVERT(VARCHAR,StartDate),112),
			@EndDate = CONVERT(DATETIME,CONVERT(VARCHAR,EndDate),112) + CONVERT(DATETIME,'23:59:59',114),
			@StartDateKey = StartDate,
			@EndDateKey = EndDate
	FROM	dbo.LegacyDataMove
	WHERE	PartitionName = @PartitionName

	/* begin a transaction so we can roll back if the record counts do not match */
	BEGIN TRANSACTION
	/*	
	*/
	INSERT INTO #OTISList(BankID,LockboxID,DepositDate)
	SELECT 	BankID,
			LockboxID,
			@parmConversionStartDate - RetentionDays AS DepositDate
	FROM	dbo.CDSPurgeSetup
	WHERE	(@parmConversionStartDate - RetentionDays) <= @EndDate
	ORDER BY RetentionDays

	INSERT INTO #OTISList(BankID,LockboxID,DepositDate)
	SELECT	BankID,
			LockboxID,
			@parmConversionStartDate
	FROM	dbo.Lockbox
	EXCEPT
	SELECT 	BankID,
			LockboxID,
			@parmConversionStartDate
	FROM	dbo.CDSPurgeSetup
	
	INSERT INTO #tmpBatchList(GlobalBatchID)
	SELECT 	dbo.Batch.GlobalBatchID
	FROM	dbo.Batch
			INNER JOIN #OTISList ON #OTISList.BankID = dbo.Batch.BankID AND #OTISList.LockboxID = dbo.Batch.LockboxID
	WHERE	dbo.Batch.DepositDate >= @StartDate
			AND dbo.Batch.DepositDate <= @EndDate

	SET @BatchesInDBO = @@ROWCOUNT
	
	COMMIT TRANSACTION
	
	/* Get DBO Counts */
	IF( @BatchesInDBO > 0 )
	BEGIN
		SELECT 	@ChecksInDBO = COUNT(*) 
		FROM 	dbo.Checks
				INNER JOIN #tmpBatchList ON dbo.Checks.GlobalBatchID = #tmpBatchList.GlobalBatchID
		
		SELECT 	@DocumentsInDBO = COUNT(*) 
		FROM 	dbo.Documents
				INNER JOIN #tmpBatchList ON dbo.Documents.GlobalBatchID = #tmpBatchList.GlobalBatchID
		
		SELECT 	@StubsInDBO = COUNT(*) 
		FROM 	dbo.Stubs
				INNER JOIN #tmpBatchList ON dbo.Stubs.GlobalBatchID = #tmpBatchList.GlobalBatchID
	END

	/* Get OLTA Counts */
	SELECT 	@BatchesInOLTA = COUNT(*)
	FROM	OLTA.factBatchSummary
	WHERE	DepositDateKey >= @StartDateKey
			AND DepositDateKey <= @EndDateKey

	SELECT 	@ChecksInOLTA = COUNT(*)
	FROM	OLTA.factChecks
	WHERE	DepositDateKey >= @StartDateKey
			AND DepositDateKey <= @EndDateKey

	SELECT 	@DocumentsInOLTA = COUNT(*)
	FROM	OLTA.factDocuments
	WHERE	DepositDateKey >= @StartDateKey
			AND DepositDateKey <= @EndDateKey

	SELECT 	@StubsInOLTA = COUNT(*)
	FROM	OLTA.factStubs
	WHERE	DepositDateKey >= @StartDateKey
			AND DepositDateKey <= @EndDateKey

	BEGIN TRANSACTION
	UPDATE 	dbo.LegacyDataMove
	SET		BatchesInDBO = @BatchesInDBO,
			ChecksInDBO = @ChecksInDBO,
			DocumentsInDBO = @DocumentsInDBO,
			StubsInDBO = @StubsInDBO,
			BatchesInOLTA = @BatchesInOLTA,
			ChecksInOLTA = @ChecksInOLTA,
			DocumentsInOLTA = @DocumentsInOLTA,
			StubsInOLTA = @StubsInOLTA,
			CountsValidated = 1
	WHERE	PartitionName = @PartitionName
	COMMIT TRANSACTION

END TRY
BEGIN CATCH
	/* Drop the temp table if it is there. */
	IF EXISTS (SELECT * FROM tempdb..sysobjects WHERE id=OBJECT_ID('tempdb..#tmpBatchList')) 
		DROP TABLE #tmpBatchList
	/* roll back any open transactions */
	IF XACT_STATE() != 0 
		ROLLBACK TRANSACTION
	/* Call standard method of notifing caller of the error. */
	EXEC dbo.usp_WfsRethrowException 
END CATCH
