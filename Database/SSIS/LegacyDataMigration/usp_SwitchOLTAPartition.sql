--WFSScriptProcessorSchema dbo
--WFSScriptProcessorStoredProcedureName usp_SwitchOLTAPartition
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('dbo.usp_SwitchOLTAPartition') IS NOT NULL
       DROP PROCEDURE dbo.usp_SwitchOLTAPartition
GO
--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE dbo.usp_SwitchOLTAPartition
		@parmPartitionNumber	int
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/05/2011
*
* Purpose: Switch newly loaded partition in and old partition out
*
* Modification History
* 05/05/2011 CR 34314 JPB	Created
******************************************************************************/
SET NOCOUNT ON
SET ARITHABORT ON
BEGIN TRY
	ALTER TABLE olta.factBatchSummary SWITCH PARTITION @parmPartitionNumber TO olta.factBatchSummary_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factTransactionSummary SWITCH PARTITION @parmPartitionNumber TO olta.factTransactionSummary_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factChecks SWITCH PARTITION @parmPartitionNumber TO olta.factChecks_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDocuments SWITCH PARTITION @parmPartitionNumber TO olta.factDocuments_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factStubs SWITCH PARTITION @parmPartitionNumber TO olta.factStubs_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDataEntrySummary SWITCH PARTITION @parmPartitionNumber TO olta.factDataEntrySummary_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDataEntryDetails SWITCH PARTITION @parmPartitionNumber TO olta.factDataEntryDetails_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factBatchData SWITCH PARTITION @parmPartitionNumber TO olta.factBatchData_out PARTITION @parmPartitionNumber
	ALTER TABLE olta.factItemData SWITCH PARTITION @parmPartitionNumber TO olta.factItemData_out PARTITION @parmPartitionNumber


	--Switch in the data to be loaded
	ALTER TABLE olta.factBatchSummary_load SWITCH  PARTITION @parmPartitionNumber TO olta.factBatchSummary PARTITION @parmPartitionNumber
	ALTER TABLE olta.factTransactionSummary_load SWITCH PARTITION @parmPartitionNumber TO olta.factTransactionSummary PARTITION @parmPartitionNumber
	ALTER TABLE olta.factChecks_load SWITCH PARTITION @parmPartitionNumber TO olta.factChecks PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDocuments_load SWITCH PARTITION @parmPartitionNumber TO olta.factDocuments PARTITION @parmPartitionNumber
	ALTER TABLE olta.factStubs_load SWITCH PARTITION @parmPartitionNumber TO olta.factStubs PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDataEntrySummary_load SWITCH PARTITION @parmPartitionNumber TO olta.factDataEntrySummary PARTITION @parmPartitionNumber
	ALTER TABLE olta.factDataEntryDetails_load SWITCH PARTITION @parmPartitionNumber TO olta.factDataEntryDetails PARTITION @parmPartitionNumber
	ALTER TABLE olta.factBatchData_load SWITCH PARTITION @parmPartitionNumber TO olta.factBatchData PARTITION @parmPartitionNumber
	ALTER TABLE olta.factItemData_load SWITCH PARTITION @parmPartitionNumber TO olta.factItemData PARTITION @parmPartitionNumber
END TRY
BEGIN CATCH
	EXEC dbo.usp_WfsRethrowException
END CATCH

