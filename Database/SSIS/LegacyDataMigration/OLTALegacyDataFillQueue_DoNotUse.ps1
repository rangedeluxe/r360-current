﻿<#
	.SYNOPSIS
	PowerShell script used to fill the OTISQueue table for processing.
	
	.DESCRIPTION
	The script will call stored procedure to fill the OTISQueue table. The script
	uses the SSIS configuration file to determine the database server/name and 
	the conversion started state.
	
	.PARAMETER StartDate
	The start date of the partition to be moved. The start date must be a Tuesday.

	.PARAMETER ConverisonStartDate
	The start date of the partition to be moved. The start date must be a Tuesday.
	
#>
param
(
	[parameter(Mandatory = $true)][datetime] $StartDate = "",
	[string] $ConfigFile = "LegacyDataMigration.dtsConfig"
) 

$ScriptVerison = "1.0";

Write-Host "OLTA Legacy Data Move Fill Queue PowerShell Script Version " $ScriptVerison

################################################################################
#
# 05/23/2011 JPB	CR 34422 1.0	Created.
# 06/09/2011 JPB 	CR 45173 1.0	Added DocumentSequenceIMSMapping truncation
#									as first step in process. Also reseeds table
#									back to 1.
#
################################################################################

cls

###############################################################################
#
# The first thing that has to be done is validate the input date
#
###############################################################################
if( $StartDate.DayOfWeek -ne 'Tuesday' )
{
	Write-Host "Start date must be a Tuesday.";
	break;
}

#Setup path and package information to be used
$PackageFolder = (Get-Location -PSProvider FileSystem).ProviderPath + "\";
[System.IO.Directory]::SetCurrentDirectory((Get-Location -PSProvider FileSystem).ProviderPath);

$SSISPackage = "LegacyDataMove.dtsx";
$SSISConfiguration = $ConfigFile;

#Read the SSIS package config file for the source server/db name
$xml = New-Object "System.Xml.XmlDocument";
$xml.load($SSISConfiguration);
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[ServerName]") };
$DBServer = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Connections[SourceDataConnection].Properties[InitialCatalog]") };
$DBName = $ConfigurationNode.ConfiguredValue;
$ConfigurationNode = $xml.SelectNodes("/DTSConfiguration/Configuration") | where { $_.Path.startsWith("\Package.Variables[User::ConversionStartDate].Properties[Value]") };
$ConversionStartDate = $ConfigurationNode.ConfiguredValue;

Write-Host "Source Server" $DBServer;
Write-Host "Source DBName" $DBName;
Write-Host "Start Date" $StartDate;
Write-Host "Conversion Start Date" $ConversionStartDate;

#For now, only support Windows Auth
$dbConnectionOptions = ("Data Source=$DBServer; Initial Catalog=$DBName;Integrated Security=SSPI")
$dbConnection = New-Object System.Data.SqlClient.SqlConnection($dbConnectionOptions);
$dbConnection.Open();
$dbCommand = $dbConnection.CreateCommand();
$dbCommand.CommandTimeout = 3600;

#CR 45173 JPB 06/09/11 BEGIN
#Execute truncation
$dbCommand.CommandText = "TRUNCATE TABLE OLTA.DocumentSequenceIMSMapping";
Write-Host $dbCommand.CommandText;
$dbReader = $dbCommand.ExecuteNonQuery();

#Execute reseed table
$dbCommand.CommandText = "DBCC CHECKIDENT ('OLTA.DocumentSequenceIMSMapping',RESEED,1)";
Write-Host $dbCommand.CommandText;
$dbReader = $dbCommand.ExecuteNonQuery();
#CR 45173 JPB 06/09/11 END

#Execute Stored PRocedure.
$dbCommand.CommandText = "EXEC usp_OTISQueueInsertForLegacyDataMove @parmPartitionStartDate='{0}/{1}/{2}',@parmConversionStartDate='{3}' " -f $StartDate.Month,$StartDate.Day,$StartDate.Year,$ConversionStartDate;
Write-Host $dbCommand.CommandText;
$dbReader = $dbCommand.ExecuteNonQuery();

