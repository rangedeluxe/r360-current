# DataImportIntegrationServices SSIS Packages

## Setup
 - Be sure that the repo is cloned into C:\Dev\R360-Current so that the references in the packages that call DLLs are correct
 - Be sure to always build the entire solution before starting work so the DLLs are all up to date

## Any package that calls DLLs have a "sister" package int the DataImportIntegrationServices2016 folder. Be sure to update these packages if making changes to the 2012 packages.

 
