﻿using System;
using System.Collections.Generic;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace PostDepositBusinessRules.Ssis
{
    class StubBuilder
    {
        private int? _currentBatchSequence = null;
        private Stub _stub;
        private Action<Stub> AddStub { get; set; }

        public Stub Stub => _stub;

        public StubBuilder(Action<Stub> addStub)
        {
            AddStub = addStub;
        }

        public void ProcessDataEntry(int batchSequence, string fieldName, string dataEntryValue)
        {
            if (batchSequence != _currentBatchSequence)
            {
                if (_currentBatchSequence != null)
                {
                    AddStub(_stub);
                }
                _stub = new Stub();
                _currentBatchSequence = batchSequence;
            }
            _stub.DataEntryFields.Add(new DataEntryField { Name = fieldName, Value = dataEntryValue });
        }
    }
}
