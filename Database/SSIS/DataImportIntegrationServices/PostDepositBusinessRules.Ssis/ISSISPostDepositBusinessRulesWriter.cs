﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostDepositBusinessRules.Ssis
{
    public interface ISSISPostDepositBusinessRulesWriter
    {
        void WriteInvalidTransaction(InvalidTransaction invalidTransaction);
    }
}
