﻿using System;
using System.Collections.Generic;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace PostDepositBusinessRules.Ssis
{
    class TransactionBuilder
    {
        private long _currentBatchId;
        private int _currentTransactionId;
        private Transaction _transaction;
        private PaymentBuilder _paymentBuilder;
        private StubBuilder _stubBuilder;

        private Action ValidateTransaction { get; set; }

        public long CurrentBatchId => _currentBatchId;
        public int CurrentTransactionId => _currentTransactionId;
        public Transaction CurrentTransaction => _transaction;

        public TransactionBuilder(Action validateTransaction)
        {
            ValidateTransaction = validateTransaction;
        }

        private void AddPayment(Payment payment)
        {
            _transaction.Payments.Add(payment);
        }

        private void AddStub(Stub stub)
        {
            _transaction.Stubs.Add(stub);
        }

        private void ProcessDataEntryData(int batchSequence, bool isCheck, string fieldName, string dataEntryValue)
        {
            if (isCheck)
            {
                if (_paymentBuilder == null)
                {
                    _paymentBuilder = new PaymentBuilder(AddPayment);
                }
                _paymentBuilder.ProcessDataEntry(batchSequence, fieldName, dataEntryValue);
            }
            else
            {
                if (_stubBuilder == null)
                {
                    _stubBuilder = new StubBuilder(AddStub);
                }
                _stubBuilder.ProcessDataEntry(batchSequence, fieldName, dataEntryValue);
            }
        }

        public void ProcessRow(int siteBankId, int siteWorkgroupId, int paymentSourceKey, long batchId, int transactionId, int batchSequence, bool isCheck, string fieldName, string dataEntryValue)
        {
            if (batchId != _currentBatchId || transactionId != _currentTransactionId)
            {
                CompleteTransaction();
                //start a new transaction
                _transaction = new Transaction
                {
                    SiteBankId = siteBankId,
                    SiteWorkgroupId = siteWorkgroupId,
                    PaymentSourceKey = paymentSourceKey
                };
                _currentBatchId = batchId;
                _currentTransactionId = transactionId;
            }
            ProcessDataEntryData(batchSequence, isCheck, fieldName, dataEntryValue);
        }

        public void CompleteTransaction()
        {
            if (_transaction != null)
            {
                if (_paymentBuilder != null)
                {
                    _transaction.Payments.Add(_paymentBuilder.Payment);
                    _paymentBuilder = null;
                }
                if (_stubBuilder != null)
                {
                    _transaction.Stubs.Add(_stubBuilder.Stub);
                    _stubBuilder = null;
                }
                ValidateTransaction();
                _transaction = null;
            }
        }
    }
}
