﻿using System;
using System.Collections.Generic;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace PostDepositBusinessRules.Ssis
{
    class PaymentBuilder
    {
        private int? _currentBatchSequence = null;
        private Payment _payment;
        private Action<Payment> AddPayment { get; set; }

        public Payment Payment => _payment;

        public PaymentBuilder(Action<Payment> addPayment)
        {
            AddPayment = addPayment;
        }

        public void ProcessDataEntry(int batchSequence, string fieldName, string dataEntryValue)
        {
            if (batchSequence != _currentBatchSequence)
            {
                if (_currentBatchSequence != null)
                {
                    AddPayment(_payment);
                }
                _payment = new Payment();
                _currentBatchSequence = batchSequence;
            }

            if (string.Equals(fieldName, "remittername", StringComparison.InvariantCultureIgnoreCase))
                _payment.Payer = dataEntryValue;

            _payment.DataEntryFields.Add(new DataEntryField { Name = fieldName, Value = dataEntryValue });
        }
    }
}
