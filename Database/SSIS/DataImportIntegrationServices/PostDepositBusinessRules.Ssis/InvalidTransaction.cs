﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostDepositBusinessRules.Ssis
{
    public class InvalidTransaction
    {
        public InvalidTransaction(long batchId, int transactionId)
        {
            BatchId = batchId;
            TransactionId = transactionId;
        }

        public long BatchId { get; }
        public int TransactionId { get; }
    }
}
