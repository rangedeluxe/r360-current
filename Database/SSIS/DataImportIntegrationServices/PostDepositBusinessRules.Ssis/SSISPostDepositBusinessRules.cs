﻿using PostDepositBusinessRules.Ssis.Dtos;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;

namespace PostDepositBusinessRules.Ssis
{
    public class SSISPostDepositBusinessRules
    {
        private readonly BusinessRulesEngine _businessRulesEngine;
        private readonly ISSISPostDepositBusinessRulesWriter _writer;

        private readonly TransactionBuilder _transactionBuilder;

        public SSISPostDepositBusinessRules(ISSISPostDepositBusinessRulesWriter writer, BusinessRulesEngine businessRulesEngine)
        {
            //setup the invalid transaction writer
            _writer = writer;
            //Initialize the BRE
            _businessRulesEngine = businessRulesEngine;
            _transactionBuilder = new TransactionBuilder(ValidateTransaction);
        }

        private void AddInvalidTransaction(long currentBatchId, int currentTransactionId)
        {
            _writer.WriteInvalidTransaction(new InvalidTransaction(currentBatchId, currentTransactionId));
        }

        private void ValidateTransaction()
        {
            //call the BRE to validate the transaction
            var results = _businessRulesEngine.Validate(_transactionBuilder.CurrentTransaction);
            //add the results to the pipeline
            if (!results.Success)
            {
                AddInvalidTransaction(_transactionBuilder.CurrentBatchId, _transactionBuilder.CurrentTransactionId);
            }
        }

        public void ProcessRowData(RowData rowData)
        {
            
            _transactionBuilder.ProcessRow(rowData.SiteBankId, rowData.SiteWorkgroupId, rowData.PaymentSourceKey, rowData.BatchId, rowData.TransactionId, rowData.BatchSequence, rowData.IsCheck, rowData.FieldName, rowData.DataEntryValue);
        }

        public void CompleteTransaction()
        {
            _transactionBuilder.CompleteTransaction();
        }
    }
}
