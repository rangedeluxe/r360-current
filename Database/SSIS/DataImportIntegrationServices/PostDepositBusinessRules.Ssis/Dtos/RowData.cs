﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostDepositBusinessRules.Ssis.Dtos
{
    public class RowData
    {
        public int SiteBankId { get; set; }
        public int SiteWorkgroupId { get; set; }
        public int PaymentSourceKey { get; set; }
        public long BatchId { get; set; }
        public int TransactionId { get; set; }
        public int BatchSequence { get; set; }
        public bool IsCheck { get; set; }
        public string FieldName { get; set; }
        public string DataEntryValue { get; set; }
    }
}
