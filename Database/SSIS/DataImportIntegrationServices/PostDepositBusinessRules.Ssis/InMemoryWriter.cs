﻿using System.Collections.Generic;


namespace PostDepositBusinessRules.Ssis
{
    public class InMemoryWriter : ISSISPostDepositBusinessRulesWriter
    {
        public IList<InvalidTransaction> InvalidTransaction { get; } = new List<InvalidTransaction>();
        public void WriteInvalidTransaction(InvalidTransaction invalidTransaction)
        {
            InvalidTransaction.Add(invalidTransaction);
        }
    }
}
