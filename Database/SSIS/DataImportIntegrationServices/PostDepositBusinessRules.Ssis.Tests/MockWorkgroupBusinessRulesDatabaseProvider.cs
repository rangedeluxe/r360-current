﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace PostDepositBusinessRules.Ssis.Tests
{
    class MockWorkgroupBusinessRulesDatabaseProvider : IRuleProvider
    {
        private Rules _rules = new Rules();

        public MockWorkgroupBusinessRulesDatabaseProvider()
        {
            _rules.DataEntryFieldRules.Add(new DataEntryFieldRule { FieldName = "RemitterName", IsCheck = true, IsRequired = true });
        }

        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey = 0)
        {
            return _rules;
        }
    }
}
