﻿using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace PostDepositBusinessRules.Ssis.Tests
{
    class MockDatabaseRuleProvider : IRuleProvider
    {
        private Rules _rules;
        public MockDatabaseRuleProvider(Rules rules)
        {
            _rules = rules;
        }
        public Rules GetRules(int siteBankId, int siteWorkgroupId, int batchSourceKey)
        {
            return _rules;
        }
    }
}
