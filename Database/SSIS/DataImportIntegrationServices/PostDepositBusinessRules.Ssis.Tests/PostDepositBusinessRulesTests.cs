﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using PostDepositBusinessRules.Ssis;
using PostDepositBusinessRules.Ssis.Dtos;
using WFS.RecHub.PostDepositBusinessRules;
using WFS.RecHub.PostDepositBusinessRules.Dtos;
using WFS.RecHub.PostDepositBusinessRules.RuleProvider;

namespace PostDepositBusinessRules.Ssis.Tests
{
    [TestClass]
    public class PostDepositBusinessRulesTests
    {
        public class TransactionValidation
        {
            public long BatchId { get; set; }
            public int TransactionId { get; set; }
        }

        private bool ValidateTransaction(IList<InvalidTransaction> invalidTransactions,
            IList<TransactionValidation> expectedErrors, string testMethod)
        {
            //First assert we get the expected number of errors returned.
            Assert.AreEqual(expectedErrors.Count, invalidTransactions.Count);
            //Now walk the errors from the SSIS BRE and match them against the expected errors
            for (int i = 0; i < invalidTransactions.Count; i++)
            {
                Assert.AreEqual(expectedErrors[i].BatchId, invalidTransactions[i].BatchId, $"{testMethod} - BatchId");
                Assert.AreEqual(expectedErrors[i].TransactionId, invalidTransactions[i].TransactionId, $"{testMethod} - TransactionId");
            }
            return true;
        }

        private DataEntryFieldRule CreateDataEntryFieldRule(string fieldName, bool isCheck = true, bool isRequird = true)
        {
            return new DataEntryFieldRule
            {
                IsCheck = isCheck,
                FieldName = fieldName,
                IsRequired = isRequird
            };
        }

        private static BusinessRulesEngine CreateBusinessRulesEngine(Rules rules)
        {
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            return new BusinessRulesEngine(ruleProvider);
        }

        [TestMethod]
        public void ValidOnePaymentDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOnePaymentDataEntry");
        }

        [TestMethod]
        public void ValidOnePaymentMultipleDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField2"));
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOnePaymentMultipleDataEntry");
        }

        [TestMethod]
        public void InvalidOnePaymentMissingDataEntryField()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField2"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidOnePaymentMissingDataEntryField");
        }

        [TestMethod]
        public void InvalidOnePaymentDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidOnePaymentDataEntry");
        }

        [TestMethod]
        public void ValidTwoPaymentOneTransactionsDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data rows
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidTwoPaymentOneTransactionsDataEntry");
        }

        [TestMethod]
        public void InvalidTwoPaymentOneTransactionsDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data rows
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidTwoPaymentOneTransactionsDataEntry");
        }

        [TestMethod]
        public void ValidOnePaymentTwoTransactionsDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data rows
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOnePaymentTwoTransactionsDataEntry");
        }

        [TestMethod]
        public void InvalidOnePaymentTwoTransactionsDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data rows
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidOnePaymentTwoTransactionsDataEntry");
        }

        [TestMethod]
        public void ValidOneStubDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOneStubDataEntry");
        }

        [TestMethod]
        public void ValidOneStubMultipleDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField2", false));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOneStubMultipleDataEntry");
        }

        [TestMethod]
        public void InvalidOneStubtMissingDataEntryField()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField2", false));
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidOnePaymentTwoTransactionsDataEntry");
        }

        [TestMethod]
        public void ValidPaymentStubMultiTransactionMultipleDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField2"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField2", false));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 2, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 3, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 3, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 4, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 4, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 5, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 5, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 6, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 6, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 7, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 7, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 8, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 8, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 9, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 9, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidPaymentStubMultiTransactionMultipleDataEntry");
        }

        [TestMethod]
        public void InvalidPaymentStubMultiTransactionMultipleDataEntry()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField2"));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField2", false));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 2, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 3, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 3, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 4, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 4, IsCheck = false, FieldName = "StubField2", DataEntryValue = "V2" });

            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 5, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 5, IsCheck = true, FieldName = "PaymentField2", DataEntryValue = "V2" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 6, IsCheck = false, FieldName = "StubField1", DataEntryValue = "V1" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 6, IsCheck = false, FieldName = "StubField2", DataEntryValue = "" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 3 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "InvalidPaymentStubMultiTransactionMultipleDataEntry");
        }
        [TestMethod]
        public void ValidEmptyRowsetComplete()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("PaymentField1"));
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            //ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "PaymentField1", DataEntryValue = "V1" });

            //Complete the data row with nothing in it
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(0, invalidTransactionWriter.InvalidTransaction.Count, "ValidOnePaymentDataEntry");
        }
        [TestMethod]
        public void InvalidOneStubDataEntryWithExtraCompleteTransactionCall()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var rules = new Rules();
            rules.DataEntryFieldRules.Add(CreateDataEntryFieldRule("StubField1", false));
            var ruleProvider = new MockDatabaseRuleProvider(rules);
            var businessRulesEngine = CreateBusinessRulesEngine(rules);

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = false, FieldName = "StubField1", DataEntryValue = "" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();
            //Extra Call to CompleteTransaction
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Validate the results
            Assert.AreEqual(1, invalidTransactionWriter.InvalidTransaction.Count, "ValidOneStubDataEntry");
        }

        [TestMethod]
        public void CheckRemitterName()
        {
            //Setup invalid transaction space
            var invalidTransactionWriter = new InMemoryWriter();

            //Create a rule to test
            var ruleProvider = new MockWorkgroupBusinessRulesDatabaseProvider();
            var businessRulesEngine = CreateBusinessRulesEngine(ruleProvider.GetRules(1,123));

            //Initialize the SSIS BRE Wrapper
            SSISPostDepositBusinessRules ssisPostDepositBusinessRules = new SSISPostDepositBusinessRules(invalidTransactionWriter, businessRulesEngine);

            //Add a data row
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 1, BatchSequence = 1, IsCheck = true, FieldName = "RemitterName", DataEntryValue = null });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 2, BatchSequence = 2, IsCheck = true, FieldName = "RemitterName", DataEntryValue = "Fred" });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 3, BatchSequence = 3, IsCheck = true, FieldName = "RemitterName", DataEntryValue = string.Empty });
            ssisPostDepositBusinessRules.ProcessRowData(new RowData { SiteBankId = 1, SiteWorkgroupId = 123, BatchId = 1, PaymentSourceKey = 1, TransactionId = 4, IsCheck = true, FieldName = "RemitterName", DataEntryValue = "" });

            //Complete the data row
            ssisPostDepositBusinessRules.CompleteTransaction();

            //Setup the expected errors
            IList<TransactionValidation> expectedErrors = new List<TransactionValidation>();
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 1 });
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 3 });
            expectedErrors.Add(new TransactionValidation { BatchId = 1, TransactionId = 4 });

            //Validate the results
            ValidateTransaction(invalidTransactionWriter.InvalidTransaction, expectedErrors, "CheckRemitterName");
        }
    }
}
