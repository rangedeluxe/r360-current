DECLARE @xmlBatch XML,
		@batchCount int = 1,
		@batchStart int = 1,
		@batchEnd int = 10,
		@bankId varchar(10) = '999',
		@clientId varchar(10) = '8011',
		@paymentSource varchar(50) = 'devimagerps',
		@batchTrackingId varchar(50),
		@batchNumber varchar(20); 

while (@batchCount between @batchStart and @batchEnd)
begin
	SET @batchNumber = CAST(@batchCount AS varchar(20)); 
	SET @batchCount = @batchCount + 1;
	SET @batchTrackingId = NEWID();
	SET @xmlBatch = 
	'<Batches SourceTrackingID="16925a85-ddae-402c-9461-4d6adf39b2ca" ClientProcessCode="2.01BatchImageRPS" XSDVersion="2.01.02.00">
		<Batch DepositDate="2015-09-19" 
			BatchDate="2015-01-30" ProcessingDate="2015-01-30" 
			BankID="' + @bankId + '" ClientID="' + @clientId + '" BatchID="' + @batchNumber + '" BatchNumber="' + @batchNumber + '" BatchSiteCode="1" 
			BatchSource="' + @paymentSource + '" PaymentType="Lockbox" BatchCueID="0" 
			BatchTrackingID="' + @batchTrackingId + '" ABA="883888888" DDA="93939393">
		  <BatchDataRecord>
			<BatchData FieldName="Client Name" FieldValue="Bevent Insurance" />
			<BatchData FieldName="Doc Grp ID" FieldValue="1" />
			<BatchData FieldName="Batch Date" FieldValue="09/19/2015" />
			<BatchData FieldName="Receive Date" FieldValue="04/07/2014" />
			<BatchData FieldName="Deposit Number" FieldValue="0" />
			<BatchData FieldName="Deposit Time" FieldValue="0" />
			<BatchData FieldName="Consolidation Date" FieldValue="04/16/2014" />
			<BatchData FieldName="Consolidation Number" FieldValue="1" />
			<BatchData FieldName="P1 Station ID" FieldValue="5" />
			<BatchData FieldName="P1 Operator ID" FieldValue="*MARY" />
			<BatchData FieldName="P2 Station ID" FieldValue="5" />
			<BatchData FieldName="P2 Operator ID" FieldValue="*MARY" />
			<BatchData FieldName="Location ID" FieldValue="0" />
			<BatchData FieldName="Site ID" FieldValue="1" />
			<BatchData FieldName="Work Type" FieldValue="Singles" />
		  </BatchDataRecord>
		  <Transaction Transaction_Id="1" TransactionID="1" TransactionSequence="1">
			<Payment Transaction_Id="1" Payment_Id="1" BatchSequence="2" Amount="200.00" RT="080283672" Account="95707039" Serial="2404" TransactionCode="" RemitterName="" ABA="883888888" DDA="93939393">
			  <PaymentItemData Payment_Id="1" FieldName="Transaction Identifier" FieldValue="01241200010104072014" />
			  <PaymentItemData Payment_Id="1" FieldName="Item Identifier" FieldValue="0124120000020104072014" />
			  <PaymentItemData Payment_Id="1" FieldName="Document ID" FieldValue="1" />
			  <PaymentItemData Payment_Id="1" FieldName="Applied OpID" FieldValue="bryon,car" />
			  <PaymentItemData Payment_Id="1" FieldName="P2 Sequence" FieldValue="1" />
			  <PaymentItemData Payment_Id="1" FieldName="Pocket Cut ID" FieldValue="01241220101" />
			  <PaymentItemData Payment_Id="1" FieldName="P2 Pocket Seq Num" FieldValue="1" />
			  <PaymentItemData Payment_Id="1" FieldName="Reject Job" FieldValue="1" />
			  <PaymentItemData Payment_Id="1" FieldName="Audit Trail" FieldValue="$1,car,bryon,M1," />
			  <PaymentItemData Payment_Id="1" FieldName="Document Type" FieldValue="Check" />
			  <PaymentItemData Payment_Id="1" FieldName="Endpoint Name" FieldValue="Paper Endpoint" />
			</Payment>
			<Document Transaction_Id="1" Document_Id="1" BatchSequence="1" DocumentSequence="1" SequenceWithinTransaction="1" DocumentDescriptor="IN">
			  <DocumentRemittanceData Document_Id="1" RemittanceDataRecord_Id="1" BatchSequence="1" FieldName="Amount" FieldValue="200.00" />
			  <DocumentRemittanceData Document_Id="1" RemittanceDataRecord_Id="1" BatchSequence="1" FieldName="AccountNumber" FieldValue="7505698546567237" />
			  <DocumentRemittanceData Document_Id="1" RemittanceDataRecord_Id="1" BatchSequence="1" FieldName="DueDate" FieldValue="09272002" />
			  <DocumentRemittanceData Document_Id="1" RemittanceDataRecord_Id="1" BatchSequence="1" FieldName="AmountDue" FieldValue="154.18" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Transaction Identifier" FieldValue="01241200010104072014" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Item Identifier" FieldValue="0124120000010104072014" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Document ID" FieldValue="21" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Applied OpID" FieldValue="bryon,sca" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="P2 Sequence" FieldValue="0" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="P2 Pocket Seq Num" FieldValue="0" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Reject Job" FieldValue="1" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Audit Trail" FieldValue="$3,sca,bryon,,$2,p1,*MARY,,$1,p1,*MARY,," />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Document Type" FieldValue="Stub" />
			  <DocumentItemData Document_Id="1" BatchSequence="1" FieldName="Endpoint Name" FieldValue="Paper Endpoint" />
			</Document>
		  </Transaction>
		</Batch>
	</Batches>'

	EXEC [RecHubSystem].[usp_DataImportQueue_Ins_Batch] @xmlBatch;
end
