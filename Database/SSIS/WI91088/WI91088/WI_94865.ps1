﻿##############################################################
# 04/30/2013 WI 100344 JPB Created
##############################################################

param
(
	[parameter(Mandatory = $true)][string] $SourceServer,
	[parameter(Mandatory = $true)][string] $SourceDB,
	[parameter(Mandatory = $true)][string] $TargetServer,
	[parameter(Mandatory = $true)][string] $TargetDB,
	[parameter(Mandatory = $true)][string] $SSISDirectory
)

#BuildXML document to be sent to execute PowerShellScript
$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISDirectory</PackageFolder>
	<PackageName>WI_94865</PackageName>
	<Parameters>
		<Parameter Name=""SourceServerName"" Value=""$SourceServer""/>
		<Parameter Name=""SourceDatabaseName"" Value=""$SourceDB""/>
		<Parameter Name=""TargetServerName"" Value=""$TargetServer""/>
		<Parameter Name=""TargetDatabaseName"" Value=""$TargetDB""/>
	</Parameters>
</SSISPackage>"
);

$SSISFolder = $xml.SSISPackage.PackageFolder;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

$CurrentFolder = Get-Location;
Set-Location $SSISFolder;
.\ExecuteSSISPackage $xml;
Set-Location $CurrentFolder;