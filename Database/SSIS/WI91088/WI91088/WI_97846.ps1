﻿##############################################################
# 05/08/2013 WI 101443 JPB Created
##############################################################

param
(
	[parameter(Mandatory = $true)][string] $SourceServer,
	[parameter(Mandatory = $true)][string] $SourceDB,
	[parameter(Mandatory = $true)][string] $SSISDirectory,
	[parameter(Mandatory = $true)][string] $DepositDateKeyStart,
	[parameter(Mandatory = $true)][string] $DepositDateKeyEnd,
	[parameter(Mandatory = $true)][string] $UnloadFolderName
)

#BuildXML document to be sent to execute PowerShellScript
$xml = New-Object "System.Xml.XmlDocument";
$xml.LoadXml(
"<SSISPackage>
	<PackageFolder>$SSISDirectory</PackageFolder>
	<PackageName>WI_97846</PackageName>
	<Parameters>
		<Parameter Name=""SourceServerName"" Value=""$SourceServer""/>
		<Parameter Name=""SourceDatabaseName"" Value=""$SourceDB""/>
		<Parameter Name=""DepositDateKeyStart"" Value=""$DepositDateKeyStart""/>
		<Parameter Name=""DepositDateKeyEnd"" Value=""$DepositDateKeyEnd""/>
		<Parameter Name=""UnloadFolderName"" Value=""$UnloadFolderName""/>
	</Parameters>
</SSISPackage>"
);

$SSISFolder = $xml.SSISPackage.PackageFolder;

if( !$SSISFolder.EndsWith("\") )
{
	$SSISFolder += "\";
}

$CurrentFolder = Get-Location;
Set-Location $SSISFolder;
.\ExecuteSSISPackage $xml;
Set-Location $CurrentFolder;