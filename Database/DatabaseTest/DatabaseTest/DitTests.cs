﻿using System;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers;

namespace DatabaseTest
{
    [TestClass]
    public class DitTests
    {
        private static IntegrationDatabase R360Database { get; set; }
        private static SsisConfig SsisConfigDatabase { get; set; }
        private static SsisLogging SsisLoggingDatabase { get; set; }
        private static IntegrationDatabaseSsis IntegrationDatabaseSsis { get; set; }
        private static DateTime _runDate;

        private static DitStaging _ditStaging;
        private static RecHubData _recHubData;
        private static RecHubSystem _recHubSystem;

        private static void SetUpDitStaging()
        {
            _ditStaging = new DitStaging(R360Database, _runDate);
            _ditStaging.CreateBatchDataObjects();
        }

        private static void SetUpRecHubData()
        {
            _recHubData = new RecHubData(R360Database, _runDate);
            _recHubData.MakeTable("dimBatchDataSetupFields");
            _recHubData.MakeTable("dimBatchExceptionStatuses");
            _recHubData.MakeTable("dimDDAs");
            _recHubData.MakeTable("dimDocumentTypes");
            _recHubData.MakeTable("dimImportTypes");
            _recHubData.MakeTable("dimItemDataSetupFields");
            _recHubData.MakeTable("dimTransactionExceptionStatuses");
            _recHubData.MakeTable("dimWorkgroupDataEntryColumns");

            _recHubData.MakeTable("factBatchData");
            _recHubData.MakeTable("factBatchSummary");
            _recHubData.MakeTable("factChecks");
            _recHubData.MakeTable("factDataEntryDetails");
            _recHubData.MakeTable("factDocuments");
            _recHubData.MakeTable("factItemData");
            _recHubData.MakeTable("factRawPaymentData");
            _recHubData.MakeTable("factStubs");
            _recHubData.MakeTable("factTransactionSummary");
        }

        private static void PopulateRecHubData()
        {
            _recHubData.InsertDimBank(1, 1, bankName: "Test Bank");
            _recHubData.InsertDimClientAccount(1, siteBankId: 1, siteClientAccountId: 999,
                shortName: "Test Client", longName: "Test Client");
            _recHubData.InsertDimBatchPaymentTypes(0, "TestingType",
                "Test Payment Type");
            _recHubData.InsertDimBatchSources(1, shortName: "TestingSource",
                longName: "Test Batch Source");
            _recHubData.InsertDimDepositStatuses(2, 850,
                "Deposit Complete");
        }

        private static void CleanUpRecHubSystem()
        {
            _recHubSystem.DeleteDitAgentJob();
        }

        private static void SetUpRecHubSystem()
        {
            _recHubSystem = new RecHubSystem(R360Database, _runDate);
            _recHubSystem.MakeTable("DataImportQueue");
            _recHubSystem.MakeTable("DuplicateFileDetection");
            _recHubSystem.MakeTable("DuplicateTransactionDetection");
            _recHubSystem.MakeDataType("DataImportQueueInsertTable");
            _recHubSystem.MakeStoredProcedure("usp_DataImportQueue_Ins_Batch");
            _recHubSystem.MakeSequence("BatchNumber");
            _recHubSystem.MakeSequence("ElectronicBatchID");
            _recHubSystem.MakeSequence("ReceivablesBatchID");
            _recHubSystem.MakeSequence("SourceBatchID");
            _recHubSystem.CreateDitAgentJob(R360Database.Connection.DataSource, SsisConfigDatabase.SqlServer, SsisConfigDatabase.DatabaseName);
        }

        private static void SetupSsisConfig()
        {
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("WFSLoggingServerName", SsisLoggingDatabase.SqlServer);
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("WFSLoggingInitialCatalog", SsisLoggingDatabase.DatabaseName);
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("PackageStore", R360Database.Connection.DataSource);
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("RunFromPackageStore", "False", type: "Boolean");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("TargetServerName", R360Database.Connection.DataSource);
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("TargetInitialCatalog", R360Database.Connection.Database);

            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackagePath", Path.Combine(@"D:\", "SSISPackages", R360Database.Connection.Database), filter: "DataImportIntegrationServices");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("BusinessRulesEngineEnabled", "False", filter: "DataImportIntegrationServices_BatchData", type: "Boolean");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_DuplicateDetect", "DataImportIntegrationServices_BatchData_DuplicateDetect", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_ExtractXML", "DataImportIntegrationServices_BatchData_ExtractXML", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_LoadData", "DataImportIntegrationServices_BatchData_LoadStaging", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_LoadFacts", "DataImportIntegrationServices_BatchData_LoadFacts", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_PostDepositBRE", "DataImportIntegrationServices_BatchData_PostDepositBRE", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_Transform", "DataImportIntegrationServices_BatchData_Transform", filter: "DataImportIntegrationServices_BatchData");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("DuplicateFileDetectDays","0", filter: "DataImportIntegrationServices_BatchData", type: "Int32");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("DuplicateTransactionDetectDays", "0", filter: "DataImportIntegrationServices_BatchData", type: "Int32");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("BatchShredderPath", Path.Combine(@"D:\", "SSISPackages", R360Database.Connection.Database), filter: "DataImportIntegrationServices_BatchData_Extract");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_Batches", "DataImportIntegrationServices_BatchData_TransformBatches", filter: "DataImportIntegrationServices_BatchData_Transform");
            SsisConfigDatabase.SsisConfigDbo.InsertConfig("ChildPackage_Stubs", "DataImportIntegrationServices_BatchData_TransformStubs", filter: "DataImportIntegrationServices_BatchData_Transform");
        }

        private static void SetupSsis()
        {
            IntegrationDatabaseSsis.CreateDiskSsisPackageFolder("DIT");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData_ExtractXML");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData_LoadFacts");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData_Transform");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData_TransformBatches");
            IntegrationDatabaseSsis.CopyDiskSsisPackage("DIT", "DataImportIntegrationServices_BatchData_TransformStubs");
        }

        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            _runDate = DateTime.Now;
            R360Database = IntegrationDatabase.CreateNew();
            SsisLoggingDatabase = new SsisLogging(IntegrationDatabase.CreateNew("SSIS_Logging"), _runDate);
            SsisConfigDatabase = new SsisConfig(IntegrationDatabase.CreateNew("SSIS_Configuration"), _runDate);
            IntegrationDatabaseSsis = new IntegrationDatabaseSsis(R360Database.Connection.DataSource, R360Database.Connection.Database, R360Database.DatabaseScriptsFolder);

            SetUpRecHubData();
            SetUpRecHubSystem();
            SetUpDitStaging();
            PopulateRecHubData();
            SetupSsisConfig();
            SetupSsis();
        }

        [ClassCleanup]
        public static void ClassCleanUp()
        {
            IntegrationDatabaseSsis.RemoveDiskSsisPackageFolder("DIT");
            CleanUpRecHubSystem();
        }

        /*Ignore test. A new method is need to create user defined data types */
        [Ignore]
        [TestMethod]
        public void DIT_AddMissingStub()
        {
            var xDataDocument = new XElement(_recHubSystem.CreateBatchesElement());

            _recHubSystem.CreateBatchElement(xDataDocument.AncestorsAndSelf().First(), 100, "TestingSource", "TestingType", bankId: 1, clientId: 999);

            var xmlBatchElement = xDataDocument.Descendants("Batch").First(batch => (int)batch.Attribute("BatchID") == 100);

            _recHubSystem.CreateTransactionElement(xmlBatchElement, 1, 1);
            var xmlTransactionElement = xmlBatchElement.Descendants("Transaction")
                .First(transaction => (int)transaction.Attribute("TransactionID") == 1);

            _recHubSystem.CreateDocumentElement(xmlTransactionElement, 1, 1, 1, 1, "IN", true);
            var xDocumentElement = xmlTransactionElement.Descendants("Document")
                .First(document => (int)document.Attribute("Document_Id") == 1);

            _recHubSystem.CreateDocumentRemittanceDataElement(xDocumentElement, 1, "TestField", "TestValue");
            _recHubSystem.CreateDocumentRemittanceDataElement(xDocumentElement, 3, "TestField2", "TestValue2");

            _recHubSystem.InsertDataImportQueueRecord_Batch(xDataDocument);
            _recHubSystem.CreateDiskSsisPackageFolder("DIT");
        }
    }
}
