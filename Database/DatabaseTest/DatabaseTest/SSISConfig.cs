﻿using System;
using DatabaseIntegrationTestHelpers;
using SSISConfigDatabaseIntegrationTestHelpers;

namespace DatabaseTest
{
    public class SsisConfig
    {
        public Dbo SsisConfigDbo;
        public string SqlServer;
        public string DatabaseName;
        public SsisConfig(IntegrationDatabase database, DateTime runDate)
        {
            SsisConfigDbo = new Dbo(database, runDate);
            SqlServer = database.Connection.DataSource;
            DatabaseName = database.Connection.Database;
        }
    }
}
