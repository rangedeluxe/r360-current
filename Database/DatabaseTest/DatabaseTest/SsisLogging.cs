﻿using System;
using DatabaseIntegrationTestHelpers;
using SSISLoggingDatabaseIntegrationTestHelpers;

namespace DatabaseTest
{
    public class SsisLogging
    {
        public Dbo SsisLoggingDbo;
        public string SqlServer;
        public string DatabaseName;

        public SsisLogging(IntegrationDatabase database, DateTime runDate)
        {
            SsisLoggingDbo = new Dbo(database, runDate);
            SqlServer = database.Connection.DataSource;
            DatabaseName = database.Connection.Database;
        }
    }
}
