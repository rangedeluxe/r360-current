﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using DatabaseIntegrationTestHelpers;
using DatabaseTest.AdvancedSearchTests.DataObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using R360DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers.DataObjects;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.Common.DTO;

namespace DatabaseTest.AdvancedSearchTests
{
    [TestClass]
    public class AdvancedSearchTests
    {
        private static IntegrationDatabase R360Database { get; set; }
        private static AdvancedSearchHelpers _helper;
        private static RecHubCommon _recHubCommon;
        private static RecHubData _recHubData;
        private static RecHubUser _recHubUser;
        private static RecHubSystem _recHubSystem;
        private static IntegrationTests _integrationTests;
        private static DateTime _runDate;

        public TestContext TestContext { get; set; }

        private readonly AdvancedSearchValidator _validator = new AdvancedSearchValidator();

        [ClassInitialize]
        public static void ClassSetUp(TestContext context)
        {
            _runDate = DateTime.Now;
            R360Database = IntegrationDatabase.CreateNew();

            _helper = new AdvancedSearchHelpers(R360Database, _runDate, R360Database.DatabaseScriptsFolder);
            _recHubCommon = _helper.SetupRecHubCommon();
            _recHubData = _helper.SetUpRecHubData();
            _recHubUser = _helper.SetRecHubUser();
            _recHubSystem = _helper.SetRecHubSystem();
            _integrationTests = _helper.SetIntegrationTests();
        }

        [ClassCleanup]
        public static void ClassCleanUp()
        {
            //CleanUpRecHubSystem();
        }

        [TestMethod]
        [Ignore]
        public void NewTest()
        {

            //Load the setup, batch data and expected results from json file
            var test = _helper.LoadTestFromJsonFile1("Test");

            var bankKey = _integrationTests.GetBankKey(test.Setup.SiteBankId);
            var clientAccountKey = _integrationTests.GetClientAccountKey(test.Setup.SiteBankId, test.Setup.SiteClientAccountId);

            if (test.Setup.ImportTypes.Any())
            {
                foreach (var importType in test.Setup.ImportTypes)
                {
                    _integrationTests.InsertImportTypes(importType);
                }
            }

            if (test.Setup.BatchSources.Any())
            {
                foreach (var batchSource in test.Setup.BatchSources)
                {
                    _integrationTests.InsertBatchSources(batchSource.ShortName,batchSource.LongName,batchSource.ImportType);

                    if (batchSource.DataEntrySetupFields.Any())
                    {
                        foreach (var dataEntrySetupField in batchSource.DataEntrySetupFields)
                        {
                            _integrationTests.InsertWorkgroupDataEntryColumns(test.Setup.SiteBankId, test.Setup.SiteClientAccountId, batchSource.ShortName, dataEntrySetupField.FieldName, dataType: dataEntrySetupField.DataType, isCheck: dataEntrySetupField.IsCheck);
                        }
                    }
                }
            }

            if (test.Setup.PaymentTypes.Any())
            {
                foreach (var paymentType in test.Setup.PaymentTypes)
                {
                    _integrationTests.InsertBatchPaymentTypes(paymentType.ShortName, paymentType.LongName);
                }
            }

            //copy some of the data around
            test.Request.BankId = test.Setup.SiteBankId;
            test.Request.WorkgroupId = test.Setup.SiteClientAccountId;
            test.Request.DepositDateFrom = _runDate;
            test.Request.DepositDateTo = _runDate;

            if (test.Batches.Any())
            {
                foreach (var batch in test.Batches)
                {
                    batch.BankKey = bankKey;
                    batch.ClientAccountKey = clientAccountKey;
                    if (batch.DepositDate == default(DateTime))
                        batch.DepositDate = null;
                    if (batch.ImmutableDate == default(DateTime))
                        batch.ImmutableDate = null;
                    if (batch.SourceProcessingDate == default(DateTime))
                        batch.SourceProcessingDate = null;
                    if (batch.DepositStatus == 0)
                        batch.DepositStatus = 850;

                    _integrationTests.InsertFactBatchSummary(batch);

                    if (batch.Transactions.Any())
                    {
                        int batchSequence = 0;
                        int transactionId = 1;
                        int txnSequence = 1;

                        foreach (var transaction in batch.Transactions)
                        {
                            int checkSequence = 0;
                            int stubSequence = 0;

                            transaction.TransactionId = transactionId++;
                            transaction.TxnSequence = txnSequence++;
                            _integrationTests.InsertFactTransactionSummary(batch, transaction);

                            if (transaction.Checks != null && transaction.Checks.Any())
                            {
                                foreach (var check in transaction.Checks)
                                {
                                    check.BatchSequence = ++batchSequence;
                                    check.CheckSequence = checkSequence++;
                                    check.SequenceWithinTransaction = 1;
                                    _integrationTests.InsertFactChecks(batch, transaction, check);

                                    if (check.DataEntryRows != null && check.DataEntryRows.Any())
                                    {
                                        _helper.InsertDataEntry(batch, transaction, check.DataEntryRows,
                                            test.Setup.SiteBankId, test.Setup.SiteClientAccountId, check.BatchSequence ?? 1,
                                            true);
                                    }
                                }
                            }

                            if (transaction.Stubs != null && transaction.Stubs.Any())
                            {
                                foreach (var stub in transaction.Stubs)
                                {
                                    stub.BatchSequence = ++batchSequence;
                                    stub.StubSequence = stubSequence++;
                                    stub.SequenceWithinTransaction = 1;
                                    _integrationTests.InsertFacStubs(batch, transaction, stub);

                                    if (stub.DataEntryRows != null && stub.DataEntryRows.Any())
                                    {
                                        _helper.InsertDataEntry(batch, transaction, stub.DataEntryRows,
                                            test.Setup.SiteBankId, test.Setup.SiteClientAccountId, stub.BatchSequence ?? 1,
                                            false);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

        [TestMethod]
        public void AdvSearch_Basic_OneCheck()
        {
            var resultsXml = new XmlDocument();
            var resultsDataTable = new DataTable();
            var requesttransformer = new AdvancedSearchRequestDataTableTransformer();

            var sessionId = Guid.NewGuid();

            //test data setup: Config Setup
            const int bankKey = 1;
            const int clientAccountKey = 1;
            const int siteBankId = 1;
            const int siteClientAccountId = 1;

            _recHubData.InsertDimBank(bankKey,siteBankId);
            _recHubData.InsertDimClientAccount(clientAccountKey, siteBankId: siteBankId, siteClientAccountId: siteClientAccountId);
            _recHubUser.InsertSessionClientAccountEntitlements(sessionId, _runDate, _runDate,
                clientAccountKey: clientAccountKey, siteBankId: siteBankId, siteClientAccountId: siteClientAccountId,
                viewingDays: 100);

            //test data setup: Batch
            const long batchId = 100L;
            const long sourceBatchId = 1;
            const int batchPaymentTypeKey = 1;
            const int batchSourceKey = 1;
            const int transactionExceptionStatusKey = 1;

            //create batch
            var batch = new Batch(_runDate, batchId, sourceBatchId, batchPaymentTypeKey, batchSourceKey: batchSourceKey,
                bankKey: bankKey, clientAccountKey: clientAccountKey,
                transactions: new List<Transaction>
                {
                    new Transaction
                    {
                        TransactionExceptionStatusKey = transactionExceptionStatusKey,
                        TransactionId = 1,
                        TxnSequence = 1,
                        Checks = new List<Check>
                        {
                            new Check
                            {
                                DdaKey = 0,
                                Amount = 100.00M                                
                            }
                        }
                    }
                });

            _recHubData.InsertBatch(batch);

            //Create the request
            var request = new AdvancedSearchRequestDto()
            {
                BankId = siteBankId,
                WorkgroupId = siteClientAccountId,
                DepositDateFrom = _runDate,
                DepositDateTo = _runDate,
                RecordsPerPage = 10,
                StartRecord = 0,
                SessionId = sessionId,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = new List<AdvancedSearchWhereClauseDto>(),
                SortBy = "",
                SortByDir = "",
                SortByDisplayName = "",
                SelectFields = new List<AdvancedSearchWhereClauseDto>(),
                PaymentSourceKey = batchSourceKey,
                PaymentTypeKey = batchPaymentTypeKey,
                Paginate = true
            };

            var tables = requesttransformer.ToDataTable(request);

            if (!_recHubData.ExecuteAdvancedSearch(tables, out resultsXml, out resultsDataTable))
            {
                Assert.Fail("Call to advance search stored procedure failed.");
            }

            //Assert the results XML
            _validator.CompareResultsXml(TestContext.TestName,resultsXml,1,1,100.00M);

            //setup the expected results
            var expectedResults = new List<AdvSearchExpectedDataRows>
            {
                new AdvSearchExpectedDataRows
                {
                    RowNumber = 0,
                    ExpectedDataFields=new List <AdvSearchExpectedDataFieldResult>
                    {
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BankID",
                            Value = siteBankId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "ClientAccountID",
                            Value = siteClientAccountId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BatchID",
                            Value = batchId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "SourceBatchID",
                            Value = sourceBatchId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "CheckCount",
                            Value = "1"
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BatchSourceShortName",
                            Value = "AdvSrch1"
                        }
                    }
                }
            };

            //Assert the returned data
            Assert.AreEqual(1, resultsDataTable.Rows.Count, "Returned Row Count");
            _validator.CompareResultsData(TestContext.TestName, expectedResults,resultsDataTable);
        }

        [TestMethod]
        [Ignore]
        public void AdvSearch_Basic_SelectOneStub()
        {
            var resultsXml = new XmlDocument();
            var resultsDataTable = new DataTable();
            var requesttransformer = new AdvancedSearchRequestDataTableTransformer();

            var sessionId = Guid.NewGuid();

            //test data setup: Config Setup
            const int bankKey = 2;
            const int clientAccountKey = 2;
            const int siteBankId = 2;
            const int siteClientAccountId = 2;

            _recHubData.InsertDimBank(bankKey, siteBankId);
            _recHubData.InsertDimClientAccount(clientAccountKey, siteBankId: siteBankId, siteClientAccountId: siteClientAccountId);
            _recHubUser.InsertSessionClientAccountEntitlements(sessionId, _runDate, _runDate,
                clientAccountKey: clientAccountKey, siteBankId: siteBankId, siteClientAccountId: siteClientAccountId,
                viewingDays: 100);

            //test data setup: Batch
            const long batchId = 101L;
            const long sourceBatchId = 2;
            const int batchPaymentTypeKey = 1;
            const int batchSourceKey = 1;
            const int transactionExceptionStatusKey = 1;

            _recHubData.InsertDimWorkgroupDataEntryColumns(1, siteBankId, siteClientAccountId, batchSourceKey, "Amount", 7, false);

            //create batch
            var batch = new Batch(_runDate, batchId, sourceBatchId, batchPaymentTypeKey, batchSourceKey: batchSourceKey,
                bankKey: bankKey, clientAccountKey: clientAccountKey,
                transactions: new List<Transaction>
                {
                    new Transaction
                    {
                        TransactionExceptionStatusKey = transactionExceptionStatusKey,
                        Checks = new List<Check>
                        {
                            new Check
                            {
                                DdaKey = 0,
                                Amount = 100.00M
                            }
                        },
                        Stubs = new List<Stub>
                        {
                            new Stub
                            {
                                Amount = 100.00M,
                                DataEntryRows = new List<DataEntryRow>
                                {
                                    new DataEntryRow
                                    {
                                        DataEntryValues = new List<DataEntryValue>
                                        {
                                            new DataEntryValue
                                            {
                                                WorkgroupDataEntryColumnKey = 1,
                                                Value = "100.00",
                                                ValueMoney = 100.00M
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                });

            _recHubData.InsertBatch(batch);


            //Create the request
            var request = new AdvancedSearchRequestDto()
            {
                BankId = siteBankId,
                WorkgroupId = siteClientAccountId,
                DepositDateFrom = _runDate,
                DepositDateTo = _runDate,
                RecordsPerPage = 10,
                StartRecord = 0,
                SessionId = sessionId,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = new List<AdvancedSearchWhereClauseDto>(),
                SortBy = "",
                SortByDir = "",
                SortByDisplayName = "",
                SelectFields = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        FieldName = "Amount",
                        Table = AdvancedSearchWhereClauseTable.Stubs,
                        BatchSourceKey = batchSourceKey,
                        ReportTitle = "[INV].Amount",
                        IsDataEntry = false
                    }                     
                },
                PaymentSourceKey = batchSourceKey,
                PaymentTypeKey = batchPaymentTypeKey,
                Paginate = true
            };

            var tables = requesttransformer.ToDataTable(request);

            if (!_recHubData.ExecuteAdvancedSearch(tables, out resultsXml, out resultsDataTable))
            {
                Assert.Fail("Call to advance search stored procedure failed.");
            }

            var expectedResultsSelectFields = new List<AdvSearchResultsSelectedFields>
            {
                new AdvSearchResultsSelectedFields
                {
                    TableName = "Stubs",
                    FieldName = "Amount",
                    DataType = 7,
                    ReportTitle = "[INV].Amount",
                    Column = "01"
                }
            };

            //Assert the results XML
            _validator.CompareResultsXml(TestContext.TestName, resultsXml, expectedResultsSelectFields, 1, 1, 100.00M );

            //setup the expected results
            var expectedResults = new List<AdvSearchExpectedDataRows>
            {
                new AdvSearchExpectedDataRows
                {
                    RowNumber = 0,
                    ExpectedDataFields=new List <AdvSearchExpectedDataFieldResult>
                    {
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BankID",
                            Value = siteBankId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "ClientAccountID",
                            Value = siteClientAccountId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BatchID",
                            Value = batchId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "SourceBatchID",
                            Value = sourceBatchId.ToString()
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "CheckCount",
                            Value = "1"
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "BatchSourceShortName",
                            Value = "AdvSrch1"
                        },
                        new AdvSearchExpectedDataFieldResult
                        {
                            FieldName = "StubsAmount_01",
                            Value = "100.0000"
                        }
                    }
                }
            };

            //Assert the returned data
            Assert.AreEqual(1, resultsDataTable.Rows.Count, "Returned Row Count");
            _validator.CompareResultsData(TestContext.TestName, expectedResults, resultsDataTable);
        }

        [TestMethod]
        public void SearchInvoiceBatchSeqLessThan()
        {
            var resultsXml = new XmlDocument();
            var resultsDataTable = new DataTable();
            var sessionId = Guid.NewGuid();
            var requesttransformer = new AdvancedSearchRequestDataTableTransformer();

            //Load the setup, batch data and expected results from json file
            var test = _helper.LoadTestFromJsonFile(TestContext.TestName);

            //Insert the setup data
            _helper.InsertSetup(test.Setup, sessionId, test.Batch.BatchSourceKey);
            //Insert the batch data
            _recHubData.InsertBatch(test.Batch);

            //Create the request
            var selectFields = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = false,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Remitter",
                        BatchSourceKey =  test.Batch.BatchSourceKey,
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[PMT].Remitter"
                    }
                };

            var whereClause = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = true,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "BatchSequence",
                        Operator = AdvancedSearchWhereClauseOperator.IsLessThan,
                        Value = "3",
                        ReportTitle = "Payment Batch Sequence",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        BatchSourceKey = 255
                    }
                };

            var request = new AdvancedSearchRequestDto()
            {
                BankId = test.Setup.SiteBankId,
                WorkgroupId = test.Setup.SiteClientAccountId,
                DepositDateFrom = _runDate,
                DepositDateTo = _runDate,
                RecordsPerPage = 10,
                StartRecord = 0,
                SessionId = sessionId,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = whereClause,
                SortBy = "",
                SortByDir = "",
                SortByDisplayName = "",
                SelectFields = selectFields,
                PaymentSourceKey = test.Batch.BatchSourceKey,
                PaymentTypeKey = test.Batch.BatchPaymentTypeKey,
                Paginate = true
            };

            var tables = requesttransformer.ToDataTable(request);

            //Call Advanced Search SP
            if (!_recHubData.ExecuteAdvancedSearch(tables, out resultsXml, out resultsDataTable))
            {
                Assert.Fail("Call to advance search stored procedure failed.");
            }

            //Assert the results XML
            _validator.CompareResultsXml(TestContext.TestName, resultsXml, test.ExpectedResults.RecordCount, test.ExpectedResults.CheckCount, test.ExpectedResults.CheckAmount);

            //Assert the returned data
            Assert.AreEqual(test.ExpectedResults.RecordCount, resultsDataTable.Rows.Count, "Returned Row Count");
            _validator.CompareResultsData(TestContext.TestName, test.ExpectedResults.Rows, resultsDataTable);
        }
        [TestMethod]
        public void SearchInvoiceBatchSeqGreaterThan()
        {
            var resultsXml = new XmlDocument();
            var resultsDataTable = new DataTable();
            var sessionId = Guid.NewGuid();
            var requesttransformer = new AdvancedSearchRequestDataTableTransformer();

            //Load the setup, batch data and expected results from json file
            var test = _helper.LoadTestFromJsonFile(TestContext.TestName);

            //Insert the setup data
            _helper.InsertSetup(test.Setup, sessionId, test.Batch.BatchSourceKey);
            //Insert the batch data
            _recHubData.InsertBatch(test.Batch);

            //Create the request
            var selectFields = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = false,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Remitter",
                        BatchSourceKey =  test.Batch.BatchSourceKey,
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[PMT].Remitter"
                    }
                };

            var whereClause = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = true,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "BatchSequence",
                        Operator = AdvancedSearchWhereClauseOperator.IsGreaterThan,
                        Value = "1",
                        ReportTitle = "Payment Batch Sequence",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        BatchSourceKey = 255
                    }
                };

            var request = new AdvancedSearchRequestDto()
            {
                BankId = test.Setup.SiteBankId,
                WorkgroupId = test.Setup.SiteClientAccountId,
                DepositDateFrom = _runDate,
                DepositDateTo = _runDate,
                RecordsPerPage = 10,
                StartRecord = 0,
                SessionId = sessionId,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = whereClause,
                SortBy = "",
                SortByDir = "",
                SortByDisplayName = "",
                SelectFields = selectFields,
                PaymentSourceKey = test.Batch.BatchSourceKey,
                PaymentTypeKey = test.Batch.BatchPaymentTypeKey,
                Paginate = true
            };

            var tables = requesttransformer.ToDataTable(request);

            //Call Advanced Search SP
            if (!_recHubData.ExecuteAdvancedSearch(tables, out resultsXml, out resultsDataTable))
            {
                Assert.Fail("Call to advance search stored procedure failed.");
            }

            //Assert the results XML
            _validator.CompareResultsXml(TestContext.TestName, resultsXml, test.ExpectedResults.RecordCount, test.ExpectedResults.CheckCount, test.ExpectedResults.CheckAmount);

            //Assert the returned data
            Assert.AreEqual(test.ExpectedResults.RecordCount, resultsDataTable.Rows.Count, "Returned Row Count");
            _validator.CompareResultsData(TestContext.TestName, test.ExpectedResults.Rows, resultsDataTable);
        }
        [TestMethod]
        public void SearchInvoiceBatchSeqEqual()
        {
            var resultsXml = new XmlDocument();
            var resultsDataTable = new DataTable();
            var sessionId = Guid.NewGuid();
            var requesttransformer = new AdvancedSearchRequestDataTableTransformer();

            //Load the setup, batch data and expected results from json file
            var test = _helper.LoadTestFromJsonFile(TestContext.TestName);

            //Insert the setup data
            _helper.InsertSetup(test.Setup, sessionId, test.Batch.BatchSourceKey);
            //Insert the batch data
            _recHubData.InsertBatch(test.Batch);

            //Create the request
            var selectFields = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = false,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "Remitter",
                        BatchSourceKey =  test.Batch.BatchSourceKey,
                        DataType = AdvancedSearchWhereClauseDataType.String,
                        ReportTitle = "[PMT].Remitter"
                    }
                };

            var whereClause = new List<AdvancedSearchWhereClauseDto>
                {
                    new AdvancedSearchWhereClauseDto
                    {
                        IsDataEntry = true,
                        Table = AdvancedSearchWhereClauseTable.Checks,
                        FieldName = "BatchSequence",
                        Operator = AdvancedSearchWhereClauseOperator.Equals,
                        Value = "2",
                        ReportTitle = "Payment Batch Sequence",
                        DataType = AdvancedSearchWhereClauseDataType.Float,
                        BatchSourceKey = 255
                    }
                };

            var request = new AdvancedSearchRequestDto()
            {
                BankId = test.Setup.SiteBankId,
                WorkgroupId = test.Setup.SiteClientAccountId,
                DepositDateFrom = _runDate,
                DepositDateTo = _runDate,
                RecordsPerPage = 10,
                StartRecord = 0,
                SessionId = sessionId,
                MarkSenseOnly = false,
                COTSOnly = false,
                WhereClauses = whereClause,
                SortBy = "",
                SortByDir = "",
                SortByDisplayName = "",
                SelectFields = selectFields,
                PaymentSourceKey = test.Batch.BatchSourceKey,
                PaymentTypeKey = test.Batch.BatchPaymentTypeKey,
                Paginate = true
            };

            var tables = requesttransformer.ToDataTable(request);

            //Call Advanced Search SP
            if (!_recHubData.ExecuteAdvancedSearch(tables, out resultsXml, out resultsDataTable))
            {
                Assert.Fail("Call to advance search stored procedure failed.");
            }

            //Assert the results XML
            _validator.CompareResultsXml(TestContext.TestName, resultsXml, test.ExpectedResults.RecordCount, test.ExpectedResults.CheckCount, test.ExpectedResults.CheckAmount);

            //Assert the returned data
            Assert.AreEqual(test.ExpectedResults.RecordCount, resultsDataTable.Rows.Count, "Returned Row Count");
            _validator.CompareResultsData(TestContext.TestName, test.ExpectedResults.Rows, resultsDataTable);
        }
    }
}
