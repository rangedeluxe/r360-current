﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using DatabaseTest.AdvancedSearchTests.DataObjects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace DatabaseTest.AdvancedSearchTests
{
    public class AdvancedSearchValidator
    {
        private void ShredResultsXml(XmlDocument resultsXml, out int recordCount, out int checkCount, out decimal checkTotal, out int documentCount)
        {
            recordCount = -1;
            checkCount = -1;
            checkTotal = -1;
            documentCount = -1;

            int.TryParse(resultsXml.SelectSingleNode("/Page/RecordSet[@Name='Results']/@TotalRecords")?.InnerText, out recordCount);
            int.TryParse(resultsXml.SelectSingleNode("/Page/RecordSet[@Name='Results']/@CheckCount")?.InnerText, out checkCount);
            decimal.TryParse(resultsXml.SelectSingleNode("/Page/RecordSet[@Name='Results']/@CheckTotal")?.InnerText, out checkTotal);
            int.TryParse(resultsXml.SelectSingleNode("/Page/RecordSet[@Name='Results']/@DocumentCount")?.InnerText, out documentCount);
        }

        private void ShredResultsXml(XmlDocument resultsXml, out int recordCount, out int checkCount, out decimal checkTotal, out int documentCount, out List<AdvSearchResultsSelectedFields> selectedFields)
        {
            ShredResultsXml(resultsXml, out recordCount, out checkCount, out checkTotal, out documentCount);

            var selectedFieldsXml = resultsXml.SelectNodes("/Page/RecordSet/SelectFields/field");
            if (selectedFieldsXml != null)
            {
                var selectedFieldsTemp = new List<AdvSearchResultsSelectedFields>();

                foreach (XmlNode selectedField in selectedFieldsXml)
                {
                    int.TryParse(selectedField.Attributes["datatype"].Value, out var dataType);

                    selectedFieldsTemp.Add(new AdvSearchResultsSelectedFields
                    {
                        TableName = selectedField.Attributes["tablename"].Value,
                        FieldName = selectedField.Attributes["fieldname"].Value,
                        DataType = dataType,
                        ReportTitle = selectedField.Attributes["reporttitle"].Value,
                        Column = selectedField.Attributes["ColID"].Value
                    });
                }

                selectedFields = selectedFieldsTemp;
            }
            else selectedFields = null;
        }

        public void CompareResultsXml(string testName, XmlDocument resultsXml, int? expectedRecordCount = 0, int? expectedCheckCount = 0, decimal? expectedCheckTotal = 0, int? expectedDocumentCount = 0)
        {
            ShredResultsXml(resultsXml, out var acutalRecordCount, out var actualCheckCount, out var actualCheckTotal, out var actualDocumentCount);
            Assert.AreEqual(expectedRecordCount, acutalRecordCount, $"{testName} - XML Results Record Count");
            Assert.AreEqual(expectedCheckCount, actualCheckCount, $"{testName} - XML Results Check Count");
            Assert.AreEqual(expectedCheckTotal, actualCheckTotal, $"{testName} - XML Results Check Total");
            Assert.AreEqual(expectedDocumentCount, actualDocumentCount, $"{testName} - XML Results Document Count");
        }

        public void CompareResultsXml(string testName, XmlDocument resultsXml,
            List<AdvSearchResultsSelectedFields> expectedSelectedFields, int? expectedRecordCount = 0,
            int? expectedCheckCount = 0, decimal? expectedCheckTotal = 0, int? expectedDocumentCount = 0)
        {
            var actualSelectedFields = new List<AdvSearchResultsSelectedFields>();
            ShredResultsXml(resultsXml, out var acutalRecordCount, out var actualCheckCount, out var actualCheckTotal, out var actualDocumentCount, out actualSelectedFields);
            Assert.AreEqual(expectedRecordCount, acutalRecordCount, $"{testName} - XML Results Record Count");
            Assert.AreEqual(expectedCheckCount, actualCheckCount, $"{testName} - XML Results Check Count");
            Assert.AreEqual(expectedCheckTotal, actualCheckTotal, $"{testName} - XML Results Check Total");
            Assert.AreEqual(expectedDocumentCount, actualDocumentCount, $"{testName} - XML Results Document Count");

            if ((expectedSelectedFields != null && expectedSelectedFields.Any()) &&
                (actualSelectedFields != null && actualSelectedFields.Any()))
            {
                foreach (var expectedSelectedField in expectedSelectedFields)
                {
                    var actualSelectedField = actualSelectedFields.Find(f =>
                        f.TableName == expectedSelectedField.TableName &&
                        f.FieldName == expectedSelectedField.FieldName);

                    if (actualSelectedField != null)
                    {
                        Assert.AreEqual(expectedSelectedField.DataType, actualSelectedField.DataType,
                            $"{testName} - XML Results Selected Field {expectedSelectedField.TableName}.{expectedSelectedField.FieldName} Data Type");
                        Assert.AreEqual(expectedSelectedField.ReportTitle, actualSelectedField.ReportTitle,
                            $"{testName} - XML Results Selected Field {expectedSelectedField.TableName}.{expectedSelectedField.FieldName} Report Title");
                        Assert.AreEqual(expectedSelectedField.Column, actualSelectedField.Column,
                            $"{testName} - XML Results Selected Field {expectedSelectedField.TableName}.{expectedSelectedField.FieldName} Column ID");
                    }
                    else
                    {
                        Assert.Fail($"{testName} - XML Results Selected Field {expectedSelectedField.TableName}.{expectedSelectedField.FieldName} not returned");
                    }
                }
            }
        }

        public void CompareResultsData(string testName, IEnumerable<AdvSearchExpectedDataRows> expectedResults, DataTable actualResults)
        {
            foreach (var expectedResult in expectedResults)
            {
                if (expectedResult.RowNumber > actualResults.Rows.Count - 1)
                    Assert.Fail("CompareResultsSelectFields looking for a row that does exist");

                foreach (var expectedFieldResults in expectedResult.ExpectedDataFields)
                {
                    Assert.AreEqual(true, actualResults.Columns.Contains(expectedFieldResults.FieldName), $"{testName} - Result Column '{expectedFieldResults.FieldName}' does not exist");
                    Assert.AreEqual(expectedFieldResults.Value, actualResults.Rows[expectedResult.RowNumber][expectedFieldResults.FieldName].ToString(), $"{testName} - Result Column {expectedFieldResults.FieldName}");
                }
            }
        }
    }
}
