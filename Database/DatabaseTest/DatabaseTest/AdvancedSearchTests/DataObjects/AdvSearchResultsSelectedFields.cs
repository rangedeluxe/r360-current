﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchResultsSelectedFields : AdvSearchAddtionalData
    {
        public string Column { get; set; }
    }
}
