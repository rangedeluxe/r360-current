﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchSetup_New
    {
        public int SiteBankId { get; set; }
        public int SiteClientAccountId { get; set; }
        public int? ViewDays { get; set; }
        public List<string> ImportTypes { get; set; }
        public List<AdvSearchBatchSources> BatchSources { get; set; }
        public List<AdvSearchBatchPaymentTypes> PaymentTypes { get; set; }
    }
}
