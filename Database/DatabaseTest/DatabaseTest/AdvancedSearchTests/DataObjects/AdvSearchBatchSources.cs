﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchBatchSources
    {
        public string ImportType { get; set; }
        public string ShortName { get; set; }
        public string LongName { get; set; }
        public List<AdvSearchDataEntryFieldSetup_New> DataEntrySetupFields { get; set; }
    }
}
