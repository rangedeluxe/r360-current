﻿using R360DatabaseIntegrationTestHelpers.DataObjects;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchTest
    {
        public AdvSearchSetup Setup { get; set; }
        public Batch Batch { get; set; }
        public AdvSearchExpectedResults ExpectedResults { get; set; }
    }
}
