﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchWhereField : AdvSearchAddtionalData
    {
        public int BatchSourceKey { get; set; }
        public string Operator { get; set; }
        public string Value { get; set; }
    }
}
