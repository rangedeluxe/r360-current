﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchExpectedDataRows
    {
        public int RowNumber { get; set; }
        public List<AdvSearchExpectedDataFieldResult> ExpectedDataFields { get; set; }
    }
}
