﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchBatchPaymentTypes
    {
        public string ShortName { get; set; }
        public string LongName { get; set; }
    }
}
