﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchExpectedResults
    {
        public int RecordCount { get; set; }
        public int? CheckCount { get; set; }
        public decimal? CheckAmount { get; set; }
        public int? DocumentCount { get; set; }
        public List<AdvSearchExpectedDataRows> Rows { get; set; }
    }
}
