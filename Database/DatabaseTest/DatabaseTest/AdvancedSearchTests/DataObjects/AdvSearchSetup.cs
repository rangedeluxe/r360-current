﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchSetup
    {
        public int BankKey { get; set; }
        public int SiteBankId { get; set; }
        public int ClientAccountKey { get; set; }
        public int SiteClientAccountId { get; set; }
        public int? ViewDays { get; set; }
        public List<AdvSearchDataEntryFieldSetup> DataEntrySetupFields { get; set; }
    }
}
