﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using R360DatabaseIntegrationTestHelpers.DataObjects;
using WFS.RecHub.R360Services.Common.DTO;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchTest_New
    {
        public AdvSearchSetup_New Setup { get; set; }
        public List<Batch> Batches { get; set; }
        public AdvancedSearchRequestDto Request { get; set; }
    }
}
