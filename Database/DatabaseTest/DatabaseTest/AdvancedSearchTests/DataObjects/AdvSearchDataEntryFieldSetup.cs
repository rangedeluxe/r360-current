﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchDataEntryFieldSetup
    {
        public int WorkgroupDataEntryColumnKey { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public bool IsCheck { get; set; }
    }
}
