﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace R360DatabaseIntegrationTestHelpers.DataObjects
{
    public class AdvSearchExpectedDataResult
    {
        public int RowNumber { get; set; }
        public List<AdvSearchExpectedDataFieldResult> ExpectedDataFields { get; set; }
    }
}
