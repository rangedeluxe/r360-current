﻿using WFS.RecHub.R360Services.Common.DTO;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchWhereClause : AdvancedSearchWhereClauseDto
    {
        public string BatchSourceName { get; set; }
        public string DataTypeName { get; set; }
        public string OperatorName { get; set; }
    }
}
