﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DatabaseTest.AdvancedSearchTests.DataObjects
{
    public class AdvSearchAddtionalData
    {
        public string TableName { get; set; }
        public string FieldName { get; set; }
        public int DataType { get; set; }
        public string ReportTitle { get; set; }
    }
}
