﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using System.Xml.Linq;
using DatabaseIntegrationTestHelpers;
using DatabaseTest.AdvancedSearchTests.DataObjects;
using Newtonsoft.Json;
using R360DatabaseIntegrationTestHelpers;
using R360DatabaseIntegrationTestHelpers.DataObjects;
using WFS.RecHub.R360Services.Common.Services;
using WFS.RecHub.R360Services.Common.DTO;

namespace DatabaseTest.AdvancedSearchTests
{
    public class AdvancedSearchHelpers
    {
        private readonly IntegrationDatabase _r360Database;
        private readonly DateTime _runDate;
        private readonly string _testFilesFolder;

        private static RecHubCommon _recHubCommon;
        private static RecHubData _recHubData;
        private static RecHubUser _recHubUser;
        private static RecHubSystem _recHubSystem;
        private static IntegrationTests _integrationTests;

        private static void PopulateRecHubData()
        {
            //setup some basic data to be used by all the tests
            _recHubData.InsertDimTransactionExceptionStatuses(1, "AdvSrch1", "AdvSearch1");
            _recHubData.InsertDimTransactionExceptionStatuses(2, "AdvSrch2", "AdvSearch");
            _recHubData.InsertDimBatchSources(1, 1, 1, "AdvSrch1", "AdvSearch1");
            _recHubData.InsertDimBatchSources(2, 2, 1, "AdvSrch2", "AdvSearch2");
            _recHubData.InsertDimBatchPaymentTypes(1, "AdvSrch1");
            _recHubData.InsertDimBatchPaymentTypes(2, "AdvSrch2");
            _recHubData.InsertDimImportTypes(1, shortName: "AdvSrch1");
            _recHubData.InsertDimImportTypes(2, shortName: "AdvSrch2");
            _recHubData.InsertDimDda(0, "0", "0");
        }

        private Batch LoadBatchFromJsonFile(string testName)
        {
            string json = File.ReadAllText(Path.Combine(_testFilesFolder, "Data", testName + ".json"));
            Batch batch = JsonConvert.DeserializeObject<Batch>(json);
            if (batch.DepositDate == default(DateTime))
                batch.DepositDate = null;
            if (batch.ImmutableDate == default(DateTime))
                batch.ImmutableDate = null;
            if (batch.SourceProcessingDate == default(DateTime))
                batch.SourceProcessingDate = null;
            if (batch.DepositStatus == 0)
                batch.DepositStatus = 850;
            return batch;
        }

        private AdvSearchExpectedResults LoadExpectedDataResultsFromJsonFile(string testName)
        {
            string json = File.ReadAllText(Path.Combine(_testFilesFolder, "Results", testName + ".json"));
            AdvSearchExpectedResults results = JsonConvert.DeserializeObject<AdvSearchExpectedResults>(json);
            return results;
        }

        private AdvSearchSetup LoadSetupFromJsonFile(string testName)
        {
            string json = File.ReadAllText(Path.Combine(_testFilesFolder, "Setup", testName + ".json"));
            AdvSearchSetup advSearchSetup = JsonConvert.DeserializeObject<AdvSearchSetup>(json);
            return advSearchSetup;
        }

        public AdvancedSearchHelpers(IntegrationDatabase r360Database, DateTime runDate, string databaseScriptFolder)
        {
            _r360Database = r360Database;
            _runDate = runDate;
            _testFilesFolder = Path.Combine(databaseScriptFolder,"DatabaseTest","DatabaseTest", "AdvancedSearchTests", "TestFiles");

        }

        public AdvSearchTest LoadTestFromJsonFile(string testName)
        {
            var test = new AdvSearchTest
            {
                Batch = LoadBatchFromJsonFile(testName),
                ExpectedResults = LoadExpectedDataResultsFromJsonFile(testName),
                Setup = LoadSetupFromJsonFile(testName)
            };

            if (test.Batch.BankKey == 0)
                test.Batch.BankKey = test.Setup.BankKey;
            if (test.Batch.ClientAccountKey == 0)
                test.Batch.ClientAccountKey = test.Setup.ClientAccountKey;
            return test;
        }

        public AdvSearchTest_New LoadTestFromJsonFile1(string fileName)
        {
            string json = File.ReadAllText(Path.Combine(_testFilesFolder, fileName + ".json"));
            AdvSearchTest_New test = JsonConvert.DeserializeObject<AdvSearchTest_New>(json);
            return test;
        }

        public RecHubCommon SetupRecHubCommon()
        {
            _recHubCommon = new RecHubCommon(_r360Database, _runDate);
            _recHubCommon.MakeStoredProcedure("usp_WfsRethrowException");
            return _recHubCommon;

        }

        public RecHubData SetUpRecHubData()
        {
            _recHubData = new RecHubData(_r360Database, _runDate);
            _recHubData.MakeTable("dimDDAs");
            _recHubData.MakeTable("dimWorkgroupDataEntryColumns");
            _recHubData.MakeTable("dimImportTypes");
            _recHubData.MakeTable("dimDocumentTypes");
            _recHubData.MakeTable("factBatchSummary");
            _recHubData.MakeTable("factChecks");
            _recHubData.MakeTable("factDataEntryDetails");
            _recHubData.MakeTable("factDocuments");
            _recHubData.MakeTable("factStubs");
            _recHubData.MakeTable("factTransactionSummary");
            _recHubData.MakeDataType("AdvancedSearchBaseKeysTable");
            _recHubData.MakeDataType("AdvancedSearchSelectFieldsTable");
            _recHubData.MakeDataType("AdvancedSearchWhereClauseTable");
            _recHubData.MakeStoredProcedure("usp_ClientAccount_Search");
            PopulateRecHubData();
            return _recHubData;
        }

        public RecHubSystem SetRecHubSystem()
        {
            _recHubSystem = new RecHubSystem(_r360Database,_runDate);
            _recHubSystem.MakeSequence("ReceivablesBatchID");
            return _recHubSystem;
        }

        public RecHubUser SetRecHubUser()
        {
            _recHubUser = new RecHubUser(_r360Database, _runDate);
            _recHubUser.MakeTable("SessionClientAccountEntitlements");
            return _recHubUser;
        }

        public IntegrationTests SetIntegrationTests()
        {
            _integrationTests = new IntegrationTests(_r360Database, _runDate);
            return _integrationTests;
        }

        public void InsertDataEntry(Batch batch, Transaction transaction, List<DataEntryRow> dataEntryRows,
            int siteBankId, int siteClientAccountId, int batchSequence, bool isCheck)
        {
            if (dataEntryRows.Any())
            {
                foreach (var dataEntryRow in dataEntryRows)
                {
                    if (dataEntryRow.DataEntryValues != null && dataEntryRow.DataEntryValues.Any())
                    {
                        dataEntryRow.BatchSequence = batchSequence;
                        foreach (var dataEntryValue in dataEntryRow.DataEntryValues)
                        {
                            _integrationTests.InsertFactDataEntryDetails(batch, transaction, dataEntryValue, siteBankId,
                                siteClientAccountId, dataEntryRow.BatchSequence ?? 1, isCheck);
                        }
                    }
                }
            }

        }

        public void InsertSetup(AdvSearchSetup setup,Guid sessionId, int batchSourceKey)
        {
            _recHubData.InsertDimBank(setup.BankKey, setup.SiteBankId);
            _recHubData.InsertDimClientAccount(setup.ClientAccountKey, siteBankId: setup.SiteBankId, siteClientAccountId: setup.SiteClientAccountId);
            _recHubUser.InsertSessionClientAccountEntitlements(sessionId, _runDate, _runDate, clientAccountKey: setup.ClientAccountKey,
                siteBankId: setup.SiteBankId, siteClientAccountId: setup.SiteClientAccountId, viewingDays: setup.ViewDays ?? 100);

            //Setup any DEFields
            if (setup.DataEntrySetupFields.Any())
            {
                foreach (var dataEntrySetupField in setup.DataEntrySetupFields)
                {
                    _recHubData.InsertDimWorkgroupDataEntryColumns(dataEntrySetupField.WorkgroupDataEntryColumnKey, setup.SiteBankId, setup.SiteClientAccountId, batchSourceKey, dataEntrySetupField.FieldName, dataEntrySetupField.DataType, dataEntrySetupField.IsCheck);
                }
            }
        }

        public string CreateRequestXML(int siteBankId = 9999, int siteClientAccountId = 99,
            Guid sessionId = new Guid(), DateTime? dateFrom = null, DateTime? dateTo = null, int? batchIdFrom = null,
            int? batchIdTo = null, int? batchNumberFrom = null, int? batchNumberTo = null, double? amountFrom = null,
            double? amountTo = null, int? batchSourceKey = null, int? paymentTypeKey = null, bool cotsOnly = false,
            IReadOnlyCollection<AdvSearchSelectField> selectFields = null,
            IReadOnlyCollection<AdvSearchWhereField> whereFields = null)
        {
            var requestXml = new XElement("Root",
                    //new XAttribute("ReqID", Guid.NewGuid()), new XAttribute("ReqDateTime",DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss tt")),
                    //new XElement("Action", "Search"),
                    //new XElement("PageAction", "search"),
                    new XElement("BankID", siteBankId),
                    new XElement("ClientAccountID", siteClientAccountId),
                    new XElement("SessionID", sessionId == Guid.Empty ? Guid.NewGuid() : sessionId),
                    new XElement("DateFrom", (dateFrom ?? DateTime.Now).ToString("d")),
                    new XElement("DateTo", (dateTo ?? DateTime.Now).ToString("d")),
                    new XElement("BatchIDFrom", batchIdFrom),
                    new XElement("BatchIDTo", batchIdTo),
                    new XElement("BatchNumberFrom", batchNumberFrom),
                    new XElement("BatchNumberTo", batchNumberTo),
                    new XElement("AmountFrom", batchNumberFrom),
                    new XElement("AmountTo", batchNumberTo),
                    new XElement("BatchSourceKey", batchSourceKey),
                    new XElement("BatchPaymentTypeKey", paymentTypeKey),
                    new XElement("COTSOnly", cotsOnly ? "True" : "False")
                    );

            if (selectFields != null && selectFields.Any())
            {
                var selectXml = new XElement("SelectFields");
                foreach (var selectField in selectFields)
                {
                    var selectFieldXml = new XElement("field",
                        new XAttribute("tablename", selectField.TableName),
                        new XAttribute("fieldname", selectField.FieldName),
                        new XAttribute("datatype", selectField.DataType),
                        new XAttribute("reporttitle", selectField.ReportTitle),
                        new XAttribute("batchsourcekey", selectField.BatchSourceKey));
                    selectXml.Add(selectFieldXml);
                }
                requestXml.Add(selectXml);
            }

            if (whereFields != null && whereFields.Any())
            {
                var whereXml = new XElement("WhereClause");
                foreach (var whereField in whereFields)
                {
                    var whereFieldXml = new XElement("field",
                        new XAttribute("tablename", whereField.TableName),
                        new XAttribute("fieldname", whereField.FieldName),
                        new XAttribute("datatype", whereField.DataType),
                        new XAttribute("reporttitle", whereField.ReportTitle),
                        new XAttribute("batchsourcekey", whereField.BatchSourceKey),
                        new XAttribute("operator", whereField.Operator),
                        new XAttribute("value", whereField.Value));
                    whereXml.Add(whereFieldXml);
                }
                requestXml.Add(whereXml);
            }

            return requestXml.ToString();
        }
    }
}
