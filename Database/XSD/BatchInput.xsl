<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSourceKey">
				<xsl:value-of select="@BatchSourceKey"/>
			</xsl:attribute>
			<xsl:attribute name="BatchPaymentTypeKey">
				<xsl:value-of select="@BatchPaymentTypeKey"/>
			</xsl:attribute>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:attribute name="RT">
				<xsl:value-of select="@RT"/>
			</xsl:attribute>
			<xsl:attribute name="Account">
				<xsl:value-of select="@Account"/>
			</xsl:attribute>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:if test="CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord">
		<xsl:element name="DocumentRemittanceDataRecord">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord">
		<xsl:element name="GhostDocumentRemittanceDataRecord">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>