<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="ClientGroups">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
			<xsl:attribute name="XSDVersion">
				<xsl:value-of select="@XSDVersion"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup">
		<xsl:copy>
			<xsl:attribute name="ClientGroupID">
				<xsl:value-of select="@ClientGroupID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientGroupName">
				<xsl:value-of select="@ClientGroupName"/>
			</xsl:attribute>
			<xsl:attribute name="ClientTrackingID">
				<xsl:value-of select="@ClientTrackingID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Bank">
		<xsl:copy>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="BankName">
				<xsl:value-of select="@BankName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Site">
		<xsl:copy>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:attribute name="LongName">
				<xsl:value-of select="@LongName"/>
			</xsl:attribute>
			<xsl:attribute name="LocalTimeZoneBias">
				<xsl:value-of select="@LocalTimeZoneBias"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/DocumentTypes">
		<xsl:copy>
			<xsl:attribute name="FileDescriptor">
				<xsl:value-of select="@FileDescriptor"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentTypeDescription">
				<xsl:value-of select="@DocumentTypeDescription"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="SiteCode">
				<xsl:value-of select="@SiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="ShortName">
				<xsl:value-of select="@ShortName"/>
			</xsl:attribute>
			<xsl:attribute name="LongName">
				<xsl:value-of select="@LongName"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:attribute name="POBOX">
				<xsl:value-of select="@POBOX"/>
			</xsl:attribute>
			<xsl:attribute name="OnlineColorMode">
				<xsl:value-of select="@OnlineColorMode"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnsID">
				<xsl:value-of select="@DataEntryColumnsID"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/DataEntryColumns/DataEntryColumn">
		<xsl:copy>
			<xsl:attribute name="DataEntryColumns_Id">
				<xsl:value-of select="count(preceding::DataEntryColumns)+1"/>
			</xsl:attribute>
			<xsl:attribute name="DataEntryColumnID">
				<xsl:value-of select="@DataEntryColumnID"/>
			</xsl:attribute>
			<xsl:attribute name="FieldLength">
				<xsl:value-of select="@FieldLength"/>
			</xsl:attribute>
			<xsl:attribute name="DataType">
				<xsl:value-of select="@DataType"/>
			</xsl:attribute>
			<xsl:attribute name="ScreenOrder">
				<xsl:value-of select="@ScreenOrder"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayGroup">
				<xsl:value-of select="@DisplayGroup"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="DisplayName">
				<xsl:value-of select="@DisplayName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="ClientGroup/Client/ImageRPSAliasMappings">
		<xsl:copy>
			<xsl:attribute name="Client_Id">
				<xsl:value-of select="count(preceding::Client)+1"/>
			</xsl:attribute>
			<xsl:attribute name="ExtractType">
				<xsl:value-of select="@ExtractType"/>
			</xsl:attribute>
			<xsl:attribute name="DocType">
				<xsl:value-of select="@DocType"/>
			</xsl:attribute>
			<xsl:attribute name="FieldType">
				<xsl:value-of select="@FieldType"/>
			</xsl:attribute>
			<xsl:attribute name="AliasName">
				<xsl:value-of select="@AliasName"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>