<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:template match="Batches">
		<xsl:copy>
			<xsl:attribute name="SourceTrackingID">
				<xsl:value-of select="@SourceTrackingID"/>
			</xsl:attribute>
      <xsl:attribute name="XSDVersion">
        <xsl:value-of select="@XSDVersion"/>
      </xsl:attribute>
      <xsl:choose>
        <xsl:when test="@ClientProcessCode">
          <xsl:attribute name="ClientProcessCode">
            <xsl:value-of select="@ClientProcessCode"/>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ClientProcessCode">Unassigned</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <xsl:if test="../@FileHash">
        <xsl:attribute name="FileHash">
          <xsl:value-of select="../@FileHash"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="../@FileSignature">
        <xsl:attribute name="FileSignature">
          <xsl:value-of select="../@FileSignature"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord">
		<xsl:copy>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/BatchDataRecord/BatchData">
		<xsl:copy>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch">
		<xsl:copy>
			<xsl:attribute name="DepositDate">
				<xsl:value-of select="@DepositDate"/>
			</xsl:attribute>
			<xsl:attribute name="BatchDate">
				<xsl:value-of select="@BatchDate"/>
			</xsl:attribute>
			<xsl:attribute name="ProcessingDate">
				<xsl:value-of select="@ProcessingDate"/>
			</xsl:attribute>
			<xsl:attribute name="BankID">
				<xsl:value-of select="@BankID"/>
			</xsl:attribute>
			<xsl:attribute name="ClientID">
				<xsl:value-of select="@ClientID"/>
			</xsl:attribute>
			<xsl:attribute name="BatchID">
				<xsl:value-of select="@BatchID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchNumber">
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchNumber">
						<xsl:value-of select="@BatchID"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchSiteCode">
				<xsl:value-of select="@BatchSiteCode"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSource">
				<xsl:value-of select="@BatchSource"/>
			</xsl:attribute>
			<xsl:attribute name="PaymentType">
				<xsl:value-of select="@PaymentType"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@BatchCueID">
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="@BatchCueID"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="BatchCueID">
						<xsl:value-of select="-1"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="BatchTrackingID">
				<xsl:value-of select="@BatchTrackingID"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="@ABA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@ABA">
					<xsl:attribute name="ABA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@ABA"/>
					</xsl:attribute>
				</xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="ABA">
            <xsl:value-of select="''"/>
          </xsl:attribute>
        </xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="@DDA"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:when test="Transaction[1]/Payment[1]/@DDA">
					<xsl:attribute name="DDA">
						<xsl:value-of select="Transaction[1]/Payment[1]/@DDA"/>
					</xsl:attribute>
				</xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="DDA">
            <xsl:value-of select="''"/>
          </xsl:attribute>
        </xsl:otherwise>
			</xsl:choose>
			<xsl:if test="../@FileHash">
				<xsl:attribute name="FileHash">
					<xsl:value-of select="../@FileHash"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="../@FileSignature">
				<xsl:attribute name="FileSignature">
					<xsl:value-of select="../@FileSignature"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionID">
				<xsl:value-of select="@TransactionID"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionSequence">
				<xsl:value-of select="@TransactionSequence"/>
			</xsl:attribute>
      <xsl:if test="@TransactionHash">
        <xsl:attribute name="TransactionHash">
				  <xsl:value-of select="@TransactionHash"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:if test="@TransactionSignature">
        <xsl:attribute name="TransactionSignature">
				  <xsl:value-of select="@TransactionSignature"/>
			  </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="Amount">
				<xsl:value-of select="@Amount"/>
			</xsl:attribute>
			<xsl:choose>
				<xsl:when test="@RT">
					<xsl:attribute name="RT">
						<xsl:value-of select="@RT"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="RT">
						<xsl:value-of select="''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:choose>
				<xsl:when test="@Account">
					<xsl:attribute name="Account">
						<xsl:value-of select="@Account"/>
					</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="Account">
						<xsl:value-of select="''"/>
					</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>
			<xsl:attribute name="Serial">
				<xsl:value-of select="@Serial"/>
			</xsl:attribute>
			<xsl:attribute name="TransactionCode">
				<xsl:value-of select="@TransactionCode"/>
			</xsl:attribute>
			<xsl:attribute name="RemitterName">
				<xsl:value-of select="@RemitterName"/>
			</xsl:attribute>
			<xsl:attribute name="ABA">
				<xsl:value-of select="@ABA"/>
			</xsl:attribute>
			<xsl:attribute name="DDA">
				<xsl:value-of select="@DDA"/>
			</xsl:attribute>
			<xsl:if test="@CheckSequence">
				<xsl:attribute name="CheckSequence">
					<xsl:value-of select="@CheckSequence"/>
				</xsl:attribute>
			</xsl:if>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/ItemDataRecord/ItemData">
		<xsl:element name="PaymentItemData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RemittanceDataRecord/RemittanceData">
		<xsl:element name="PaymentRemittanceData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Payment/RawDataRecord/RawData">
		<xsl:element name="PaymentRawData">
			<xsl:attribute name="Payment_Id">
				<xsl:value-of select="count(preceding::Payment)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RawSequence">
				<xsl:value-of select="@RawSequence"/>
			</xsl:attribute>
			<xsl:attribute name="RawDataPart">
				<xsl:value-of select="@RawDataPart"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentSequence">
				<xsl:value-of select="@DocumentSequence"/>
			</xsl:attribute>
			<xsl:attribute name="SequenceWithinTransaction">
				<xsl:value-of select="@SequenceWithinTransaction"/>
			</xsl:attribute>
			<xsl:attribute name="DocumentDescriptor">
				<xsl:value-of select="@DocumentDescriptor"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/ItemDataRecord/ItemData">
		<xsl:element name="DocumentItemData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/Document/RemittanceDataRecord/RemittanceData">
		<xsl:element name="DocumentRemittanceData">
			<xsl:attribute name="Document_Id">
				<xsl:value-of select="count(preceding::Document)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument">
		<xsl:copy>
			<xsl:attribute name="Transaction_Id">
				<xsl:value-of select="count(preceding::Transaction)+1"/>
			</xsl:attribute>
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:apply-templates select="node()"/>
		</xsl:copy>
	</xsl:template>
	<xsl:template match="Batch/Transaction/GhostDocument/RemittanceDataRecord/RemittanceData">
		<xsl:element name="GhostDocumentRemittanceData">
			<xsl:attribute name="GhostDocument_Id">
				<xsl:value-of select="count(preceding::GhostDocument)+1"/>
			</xsl:attribute>
			<xsl:attribute name="RemittanceDataRecord_Id">
				<xsl:value-of select="count(preceding::RemittanceDataRecord)+1"/>
			</xsl:attribute>
			<xsl:attribute name="BatchSequence">
				<xsl:value-of select="../@BatchSequence"/>
			</xsl:attribute>
			<xsl:attribute name="FieldName">
				<xsl:value-of select="@FieldName"/>
			</xsl:attribute>
			<xsl:attribute name="FieldValue">
				<xsl:value-of select="@FieldValue"/>
			</xsl:attribute>
		</xsl:element>
	</xsl:template>
</xsl:stylesheet>