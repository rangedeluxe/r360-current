﻿<#
.SYNOPSIS
    Populates the local computer's R360 and Framework configuration files under C:\WFSApps,
    based on the template files from version control.

    This script currently depends on ipoCrypto.dll (which is why it's in the R360 repository).
    We may want to consider porting the ipoCrypto logic to PowerShell so we can put the
    Framework config-file processing into a script in the Framework repository.
#>

#Requires -Version 4    # Probably works in PowerShell 3, but not tested.

function Initialize() {
	$global:ErrorActionPreference = "Stop"
	
	$global:R360Current = $PSScriptRoot
	$global:WFSApps = "C:\WFSApps"
	
	$global:Framework = ResolvePathFromPossibilities @(
		Join-Path $R360Current "..\Shared-Framework"
		Join-Path $R360Current "..\..\Shared\Framework"
		Join-Path $R360Current "..\Framework"
	)

	# Load ipoCrypto.dll
	Add-Type -Path (Join-Path $R360Current "SharedDependencies\RecHub\Current\ipoCrypto.dll")

	# Set up replacements (some of which are encrypted, and therefore depend on ipoCrypto.dll being loaded)
	$global:Replacements = @{
		"@@APP_SERVER@@" = "localhost:44306"
		"@@FILE_SERVER@@" = "localhost:44306"
		"@@NOTIFICATION_SERVICES_SERVER@@" = "RecHubQaApp02.qalabs.nwk"
		"@@RAAM_APP_SERVER@@" = "localhost:44306"
		"@@IDENTITY_SERVER@@" = "localhost:44302"
		"@@RAAM_SYSTEM_PASSWORD@@" = Encrypt "Wausau#1"
		"@@RAAM_SYSTEM_USER@@" = Encrypt "middletier"
		"@@RAAM_THUMBPRINT@@" = "F500FE305F6037A593F88170D8687A66791FBBBB"
	}
}
function ResolvePathFromPossibilities($Possibilities) {
    # Returns the normalized version of the first existing path in the list,
    # or $Null if none of the paths exist.
    $Possibilities |
        Where-Object { Test-Path $_ } |
        ForEach-Object { (Resolve-Path $_).Path } |
        Select-Object -First 1
}

function Encrypt($Value) {
    $OutValue = ""
    $Crypto3Des = New-Object WFS.RecHub.Common.Crypto.cCrypto3DES
    $Crypto3Des.Encrypt($Value, [ref] $OutValue) | Out-Null
    $OutValue
}

function StripEncryptedStrings($Content) {
    $Content -replace '"AQAAA[^"]+"', '<encrypted>'
}

function CopyAndExpandPlaceholders($SourceDir, $TargetDir, $FileMask) {
    if (!(Test-Path $TargetDir -PathType Container)) {
        New-Item $TargetDir -ItemType Directory | Out-Null
    }

    Get-Item (Join-Path $SourceDir $FileMask) | ForEach-Object {
        $FileName = $_.Name
        $SourcePath = Join-Path $SourceDir $FileName
        $TargetPath = Join-Path $TargetDir $FileName
        $Content = Get-Content $SourcePath -Raw

        $Replacements.GetEnumerator() | ForEach-Object {
            $Content = $Content -replace $_.Key, $_.Value
        }

		# Now update the identity server to be 44302
		$IdentityReplacements = @{
		"https://localhost:44306/Wfs.Raam.IdentityServer/issue/wstrust/mixed/username" = "https://localhost:44302/Wfs.Raam.IdentityServer/issue/wstrust/mixed/username"
		"https://localhost:44306/Wfs.Raam.IdentityServer/FederationService.svc" = "https://localhost:44302/Wfs.Raam.IdentityServer/FederationService.svc"
		}
		
		$IdentityReplacements.GetEnumerator()| ForEach-Object {
            $Content = $Content -replace $_.Key, $_.Value
        }
		
        if ($Content -match "@@") {
            Write-Warning "Unresolved @@ placeholders found in $SourcePath"
            ([Regex] "@@\w+@@").Matches($Content) | Sort-Object -Unique | ForEach-Object {
                Write-Host "  $_" -ForegroundColor Yellow
            }
        }

        $OldTargetContent = Get-Content $TargetPath -Raw -ErrorAction SilentlyContinue
        if ((StripEncryptedStrings $Content) -ne (StripEncryptedStrings $OldTargetContent)) {

            $tempDir = "c:\temp"
            if(!(Test-Path -Path $tempDir )){
                New-Item -ItemType directory -Path $tempDir
                Write-Host "New folder created"
            }

            StripEncryptedStrings $Content | Set-Content $tempDir\1.txt -Encoding UTF8
            StripEncryptedStrings $OldTargetContent | Set-Content $tempDir\2.txt -Encoding UTF8

            # Use WriteAllText instead of Set-Content, because WriteAllText has the same
            # extra-newline-at-end-of-file behavior as Get-Content -Raw.
            [System.IO.File]::WriteAllText($TargetPath, $Content)
            if ($OldTargetContent) {
                Write-Host "Updated:" -NoNewline -ForegroundColor Green
            } else {
                Write-Host "Copied:" -NoNewline -ForegroundColor Green
            }
            Write-Host " $TargetPath"
        } else {
            Write-Host "Unchanged: $TargetPath" -ForegroundColor DarkGray
        }
    }
}

function CopyRechubIni()
{
	$EncryptReplace = @{"@@DBConn2User@@" = Encrypt("RecHubUser_User")
					"@@DBConn2Pwd@@" = Encrypt("user") 
					"@@DBConnComExUser@@" = Encrypt("IPUser")
					"@@DBConnComExPwd@@" = Encrypt("123456")} 
	get-childitem -path (Join-Path $R360Current "Ini") -Filter "rechub.ini.dev" | ForEach-Object {
		$FileName = $_.Name
		$SourcePath = Join-Path (Join-Path $R360Current "Ini") $FileName
		$Content = Get-Content $SourcePath -Raw
		$EncryptReplace.GetEnumerator() | ForEach-Object {
					$Content = $Content -replace $_.Key, $_.Value}
		[System.IO.File]::WriteAllText((Join-Path $WFSApps "RecHub\bin2\rechub.ini"), $Content)
		#Copy-Item -Destination {  (Join-Path $WFSApps "RecHub\bin2\rechub.ini")}
		Write-Host "Copied: " -NoNewline -ForegroundColor Green
		Write-Host (Join-Path $WFSApps "RecHub\bin2\rechub.ini") -ForegroundColor White
	}
}

Initialize
if ($Framework) {
    CopyAndExpandPlaceholders (Join-Path $Framework "Src\FrameworkSolution") (Join-Path $WFSApps "Framework") "WCFClients.*.config"
} else {
    Write-Warning "Could not discover location of Framework source code. Framework config files will not be updated."
}
CopyAndExpandPlaceholders (Join-Path $R360Current "Ini") (Join-Path $WFSApps "RecHub\bin2") "WCFClients.*.config"

# Copy the rechub.ini consolidated file
if (!(Test-Path -Path (Join-Path $WFSApps "RecHub\bin2\rechub.ini")))
{
	CopyRechubIni
}
else
{
	Write-Host "Unchanged:" (Join-Path $WFSApps "RecHub\bin2\rechub.ini") -ForegroundColor DarkGray
}