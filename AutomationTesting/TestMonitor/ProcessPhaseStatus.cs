﻿using AutomationTestInterfaces;

namespace TestMonitor
{
    public class ProcessPhaseStatus
    {
        public ProcessPhaseStatus(DitProcessingPhases phase, bool status)
        {
            Phase = phase;
            Status = status;
        }
        public bool Status { get; set; }
        public DitProcessingPhases Phase { get; }
    }
}
