﻿using System;
using System.Data;
using System.Data.SqlClient;
using TestMonitor.Interfaces;

namespace TestMonitor
{
    public class DataImportQueueDal:IDataImportQueueDal
    {
        public string ConnectionString { get; private set; }

        public DataImportQueueDal(string connectionString)
        {
            ConnectionString = connectionString;
        }
        public TriggerStatus CheckForNewQueueRow(string clientProcessCode, int lastDataImportQueueId)
        {
            var sql =
                "SELECT TOP(1) Result.Status " +
                "	FROM " +
                "	(		SELECT TOP (1) " +
                "				CASE ResponseStatus " +
                "					WHEN 0 THEN 'PASS' ELSE 'FAIL' END AS Status " +
                "				FROM [Current_R360_NP].[RecHubSystem].[DataImportQueue] " +
                "				WHERE ClientProcessCode = @ClientProcessCode " +
                "					   AND DataImportQueueID > @LastDataImportQueueId " +
                "					   AND QueueStatus > 98 " +
                "		Union " +
                "			SELECT Status = 'WAIT' " +
                "	) AS Result ";
            var result = TriggerStatus.Fail;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    try
                    {
                        command.Parameters.AddRange(new SqlParameter[]
                        {
                            new SqlParameter("@ClientProcessCode", SqlDbType.VarChar) {Value = clientProcessCode},
                            new SqlParameter("@LastDataImportQueueId", SqlDbType.Int) {Value = lastDataImportQueueId}
                        });
                        connection.Open();
                        result = ConvertStringToTriggerStatus(command.ExecuteScalar().ToString());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }

            return result;
        }
        public int GetLastDataImportQueueId(string clientProcessCode)
        {
            var sql =
                "SELECT MAX([DataImportQueueID])" +
                 "FROM [Current_R360_NP].[RecHubSystem].[DataImportQueue]" +
                 $"Where ClientProcessCode = '{clientProcessCode}'";
            var result = 0;

            using (SqlConnection connection = new SqlConnection(ConnectionString))
            {
                using (SqlCommand command = new SqlCommand(sql, connection))
                {
                    try
                    {
                        command.Parameters.Add(new SqlParameter("@ClientProcessCode", SqlDbType.VarChar)
                        {
                            Value = clientProcessCode
                        });
                        connection.Open();
                        result = Convert.ToInt32(command.ExecuteScalar());
                    }
                    finally
                    {
                        connection.Close();
                    }
                }
            }
            return result;
        }

        private static TriggerStatus ConvertStringToTriggerStatus(string value)
        {
            TriggerStatus result;
            switch (value)
            {
                case "PASS":
                    result = TriggerStatus.Pass;
                    break;
                case "WAIT":
                    result = TriggerStatus.Wait;
                    break;
                default:
                    result = TriggerStatus.Fail;
                    break;
            }
            return result;
        }
    }
}
