﻿using System;
using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using AutomationTestInterfaces;
using TestMonitor.Interfaces;

namespace TestMonitor
{
    public class ProcessMonitor : IMonitor
    {
        public ProcessMonitor(IMonitorConfig settings)
        {
            Settings = settings;
        }

        public Action<string, MessageType> LogMethod { get; }

        public IMonitorConfig Settings { get; }

        public static Task<bool> CreatePollingTask(Func<TriggerStatus> pollingFunction, TimeSpan pollingFreq, TimeSpan timeout)
        {
            var tcs = new TaskCompletionSource<bool>();
            var timer = new System.Timers.Timer()
            {
                AutoReset = false,
                Interval = timeout.TotalMilliseconds,
                Enabled = false
            };
            var pollingTimer = new System.Timers.Timer()
            {
                AutoReset = true,
                Interval = pollingFreq.TotalMilliseconds,
                Enabled = false
            };

            System.Timers.ElapsedEventHandler timeoutHandler = null;
            System.Timers.ElapsedEventHandler pollingTimerElapsed = null;
            
            Action removeHandlers = () =>
            {
                pollingTimer.Elapsed -= pollingTimerElapsed;
                timer.Elapsed -= timeoutHandler;
                pollingTimer.Dispose();
                timer.Dispose();
            };

            timeoutHandler = (sender, eventArgs) =>
            {
                tcs.TrySetCanceled(CancellationToken.None);
                removeHandlers();
            };

            pollingTimerElapsed = (sender, eventArgs) =>
            {
                pollingTimer.Enabled = false;
                switch (pollingFunction())
                {
                    case TriggerStatus.Wait:
                        pollingTimer.Enabled = true;
                        break;

                    case TriggerStatus.Pass:
                        tcs.TrySetResult(true);
                        removeHandlers();
                        break;

                    default:
                        tcs.TrySetResult(false);
                        removeHandlers();
                        break;
                }
            };
            
            timer.Elapsed += timeoutHandler;
            pollingTimer.Elapsed += pollingTimerElapsed;
            timer.Enabled = true;
            pollingTimer.Enabled = true;
            return tcs.Task;
        }
    }
}
