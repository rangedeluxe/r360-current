﻿using AutomationTestInterfaces;
using System;
using System.Threading.Tasks;

namespace TestMonitor.Interfaces
{
    public interface IMonitor : ILoggable
    {
        IMonitorConfig Settings { get; }
    }
}
