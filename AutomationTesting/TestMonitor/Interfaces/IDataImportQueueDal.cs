﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMonitor.Interfaces
{
    public interface IDataImportQueueDal {
        TriggerStatus CheckForNewQueueRow(string clientProcessCode, int lastDataImportQueueId);
        int GetLastDataImportQueueId(string clientProcessCode);
    }
}
