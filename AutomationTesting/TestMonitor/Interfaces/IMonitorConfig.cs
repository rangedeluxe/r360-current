﻿using System;

namespace TestMonitor.Interfaces
{
    public interface IMonitorConfig
    {
        string ConnectionString { get; }

        TimeSpan DatabasePollingFreq { get; }
    }
}
