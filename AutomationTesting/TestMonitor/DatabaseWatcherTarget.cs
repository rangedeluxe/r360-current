﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTestInterfaces;

namespace TestMonitor
{
    public class DatabaseWatcherTarget:IWatcherTarget
    {
        public DatabaseWatcherTarget(DitProcessingType processingType, DitProcessingPhases phase,
            string clientProcessingCode)
        {
            ProcessType = processingType;
            ProcessPhase = phase;
            ClientProcessingCode = clientProcessingCode;
        }
        public DitProcessingType ProcessType { get; }
        public DitProcessingPhases ProcessPhase { get; }
        public string ClientProcessingCode { get; }
    }
}
