﻿using System;
using AutomationTestInterfaces;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;
using System.Runtime.Remoting.Channels;
using System.Threading;
using System.Threading.Tasks;
using TestFileManager.Interfaces;

namespace TestFileManager
{
    public class FileManager : IFileManager, ILoggable
    {
        public Action<string, MessageType> LogMethod { get; }
        private string[] DriveLetters { get; }

        public FileManager(string[] driveLetters, Action<string, MessageType> logMethod)
        {
            LogMethod = logMethod;
            DriveLetters = SortAvaliableDrives(driveLetters);
        }

        private string[] SortAvaliableDrives(string[] drivePriorities)
        {
            var availableDrives = DriveInfo.GetDrives().Select(drive => drive.Name.Trim().Replace(":\\", ""));
            var result = drivePriorities.Where(driveLetter => availableDrives.Contains(driveLetter));

            result = result.Concat(availableDrives.Where(driveLetter => !drivePriorities.Contains(driveLetter)));
            return result.ToArray();
        }
        public string[] ListFiles(string directoryPath, string pattern, bool createDirectory = false)
        {
            if (CheckOrCreatePath(directoryPath, createDirectory))
            {
                return Directory.GetFiles(directoryPath);
            }
            else {
                return new string[] { };
            }
        }

        public string FindInputPaths(ImportType importType)
        {
            var inputDirectoryTable = new Dictionary<ImportType, string>() {
                {ImportType.ClientSetup,  @"ICONClientSetupImport_Auto\00_Input"},
                {ImportType.ImageRpsBatch, @"ICONBatchImport_Auto\00_Input" }
            };
            var paths = DriveLetters.Select(driveLetter =>
               Path.Combine($"{driveLetter}:\\", @"WFSApps\RECHUB\Data\DataImport", inputDirectoryTable[importType]));
            var result = paths.FirstOrDefault(Directory.Exists);
            if (result == null)
            {
                throw new Exception("Input Directory not found.");
            }
            return Path.GetFullPath(Path.Combine(result, @"..\..\"));
        }

        public bool ClearPath(string directoryPath, bool createDirectory = false)
        {
            try
            {
                if (CheckOrCreatePath(directoryPath, createDirectory))
                {
                    foreach (var filePath in Directory.GetFiles(directoryPath))
                    {
                        File.Delete(filePath);
                    }
                    foreach (var dirPath in Directory.GetDirectories(directoryPath))
                    {
                        Directory.Delete(dirPath);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                LogMethod(ex.Message, MessageType.Error);
                return false;
            }
        }

        public bool CheckOrCreatePath(string path, bool createIfNeeded)
        {
            if (!Directory.Exists(path) && createIfNeeded)
            {
                Directory.CreateDirectory(path);
            }
            return Directory.Exists(path);
        }

        public bool CopyFile(string sourceFile, string destinationDirectory, bool createDirectory = false)
        {
            try
            {
                if (CheckOrCreatePath(destinationDirectory, true))
                {
                    File.Copy(sourceFile, Path.Combine(destinationDirectory, Path.GetFileName(sourceFile)));
                }
                return true;
            }
            catch (Exception ex)
            {
                LogMethod(ex.Message, MessageType.Error);
                return false;
            }
        }

        public bool WriteFile(string fileName, string content, bool createDirectory = false)
        {
            try
            {
                if (CheckOrCreatePath(Path.GetDirectoryName(fileName), createIfNeeded: true))
                {
                    File.WriteAllText(fileName, content);
                }
                return true;
            }
            catch (Exception ex)
            {
                LogMethod(ex.Message, MessageType.Error);
                return false;
            }
        }

        public Task<bool> CreateFileChangeTask(string pathToWatch, Func<string, bool> isMatchingFile, TimeSpan timeout)
        {
            
            var tcs = new TaskCompletionSource<bool>();
            var timer = new System.Timers.Timer(timeout.TotalMilliseconds);
            var watcher = new FileSystemWatcher(pathToWatch);

            System.Timers.ElapsedEventHandler timeoutHandler = null;
            FileSystemEventHandler createdHandler = null;
            RenamedEventHandler renamedHandler = (s, e) => createdHandler(s, e);

            Action removeHandlers = () =>
            {
                watcher.Created -= createdHandler;
                watcher.Renamed -= renamedHandler;
                timer.Elapsed -= timeoutHandler;
                watcher.Dispose();
                timer.Dispose();
            };

            createdHandler = (s, e) =>
            {
                if (isMatchingFile(e.Name))
                {
                    tcs.TrySetResult(true);
                    removeHandlers();
                }
            };
            
            timeoutHandler = (sender, eventArgs) =>
            {
                tcs.TrySetCanceled(CancellationToken.None);
                removeHandlers();
            };

            
            watcher.Created += createdHandler;
            watcher.Renamed += renamedHandler;
            timer.Elapsed += timeoutHandler;
            watcher.EnableRaisingEvents = true;
            timer.Enabled = true;
            var foundFiles = Directory.GetFiles(pathToWatch).Where(isMatchingFile);
            if (foundFiles.Any())
            {
                removeHandlers();
                return Task.FromResult(true);
            }

            return tcs.Task;
        }

        public bool CheckIfFileExists(string filename)
        {
            return File.Exists(filename);
        }

        public bool WriteFile(string fileName, Bitmap content, bool createDirectory = false)
        {
            try
            {
                if (CheckOrCreatePath(Path.GetDirectoryName(fileName), true))
                {
                    content.Save(fileName);
                }
                return true;
            }
            catch (Exception ex)
            {
                LogMethod(ex.Message, MessageType.Error);
                return false;
            }
        }
    }
}
