﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTestInterfaces;

namespace TestFileManager
{
    public class FileWatcherTarget:IWatcherTarget
    {
        public FileWatcherTarget(DitProcessingType processType, DitProcessingPhases phase, string directory, Func<string, bool> isMatch)
        {
            ProcessType = processType;
            ProcessPhase = phase;
            DirectoryPath = directory;
            IsMatch = isMatch;
        }
        public Func<string, bool> IsMatch { get; }
        public string DirectoryPath { get; }
        public DitProcessingType ProcessType { get; }
        public DitProcessingPhases ProcessPhase { get; }
    }
}
