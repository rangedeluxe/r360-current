﻿using System;
using System.Drawing;
using System.Threading.Tasks;

namespace TestFileManager.Interfaces
{
    public interface IFileManager
    {
        bool CheckOrCreatePath(string path, bool createIfNeeded);
        bool ClearPath(string directoryPath, bool createDirectory = false);
        bool CopyFile(string sourceFile, string destinationDirectory, bool createDirectory = false);
        bool WriteFile(string fileName, string content, bool createDirectory = false);
        bool WriteFile(string fileName, Bitmap content, bool createDirectory = false);
        string[] ListFiles(string directoryPath, string pattern, bool createDirectory = false);
        string FindInputPaths(ImportType importType);
        Task<bool> CreateFileChangeTask(string pathToWatch, Func<string, bool> isMatchingFile, TimeSpan timeout);
        bool CheckIfFileExists(string filename);
    }

    public enum ImportType
    {
        ClientSetup,
        ImageRpsBatch
    }
}