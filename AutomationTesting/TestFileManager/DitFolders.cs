﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestFileManager
{
    public class DitFolders
    {
        public DitFolders(string basePath, string imageSubPath)
        {
            ClientSetupInputFolder = Path.Combine(basePath, @"ICONClientSetupImport_Auto\00_Input");
            ClientSetupInProcessFolder = Path.Combine(basePath, @"ICONClientSetupImport_Auto\01_InProcess");
            BatchInputFolder = Path.Combine(basePath, @"ICONBatchImport_Auto\00_Input");
            ImagePath = Path.Combine(basePath, imageSubPath);
            ClientSetupInputFile = Path.Combine(ClientSetupInputFolder,
                "Client987001_DataGenTest987001_1_20170825_104408.xml");
            BatchInputFile = Path.Combine(BatchInputFolder, "BatchFile_987001_21_21.dat");
        }
        public string BatchInputFile { get; }
        public string BatchInputFolder { get; }
        public string ClientSetupInProcessFolder { get; }
        public string ClientSetupInputFile { get; }
        public string ClientSetupInputFolder { get; }
        public string ImagePath { get; }
        
    }
}
