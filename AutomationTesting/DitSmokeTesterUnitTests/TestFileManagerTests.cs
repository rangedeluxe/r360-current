﻿using System;
using AutomationTestInterfaces;
using System.Collections.Generic;
using DitSmokeTester;
using DitSmokeTester.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestMonitor;

namespace DitSmokeTesterUnitTests
{
    [TestClass]
    public class TestRunnerTest
    {
        private IConfig GetConfig()
        {
                return new SettingsProviderStub();
        }

        [TestMethod]
        public void RunMethod_ClientSetupAndBatchScenario_ShouldPass() {
            var triggerStateQueue = new Queue<TriggerStatus>(new[] {
                TriggerStatus.Pass, 
                TriggerStatus.Pass
            });
            var errorList = new List<string>();
            var fileManager = new FileManagerStub();
            var systemMonitor = new ProcessMonitorStub(GetConfig());
            var ditDal = new DataImportQueueDalStub(triggerStateQueue);

            var runner = new TestRunner(GetConfig(), fileManager, systemMonitor, ditDal, ErrorLogToList(errorList));
            var testJob = runner.Run();
            fileManager.AddAFile("Client987001_DataGenTest987001_1_20170825_104408.xml", @"X:\WFSApps\RecHub\Data\DataImport\ICONClientSetupImport_Auto\01_InProcess");
            testJob.Wait();
            var results = testJob.Result;
            Assert.AreEqual(0, errorList.Count, "Errors occured");
            Assert.IsTrue(results, "File was not moved to InProcess");
        }
        [TestMethod]
        public void RunMethod_ClientSetupAndBatchScenarioWrongFileName_ShouldPass()
        {
            var errorList = new List<string>();
            var fileManager = new FileManagerStub();
            var systemMonitor = new ProcessMonitorStub(GetConfig());
            var triggerStateQueue = new Queue<TriggerStatus>(new[] {
                TriggerStatus.Pass,
                TriggerStatus.Pass
            });

            var runner = new TestRunner(
                GetConfig(), 
                fileManager, 
                systemMonitor, 
                new DataImportQueueDalStub(triggerStateQueue), 
                ErrorLogToList(errorList));
            var testJob = runner.Run();
            fileManager.AddAFile("TestDummy.txt", @"X:\WFSApps\RecHub\Data\DataImport\ICONClientSetupImport_Auto\01_InProcess");
            testJob.Wait();
            var results = testJob.Result;
            Assert.AreEqual(1, errorList.Count, "Errors occured");
            Assert.IsFalse(results, "File was not moved to InProcess");
        }
        [TestMethod]
        public void RunMethod_ClientSetupScenarioDBNotResponding_ShouldPass()
        {
            var errorList = new List<string>();
            var fileManager = new FileManagerStub();
            var systemMonitor = new ProcessMonitorStub(GetConfig());

            var runner = new TestRunner(
                GetConfig(), 
                fileManager, 
                systemMonitor, 
                new DataImportQueueDalStub(() => TriggerStatus.Wait), 
                ErrorLogToList(errorList));
            var testJob = runner.Run();
            fileManager.AddAFile("Client987001_DataGenTest987001_1_20170825_104408.xml", @"X:\WFSApps\RecHub\Data\DataImport\ICONClientSetupImport_Auto\01_InProcess");
            testJob.Wait();
            var results = testJob.Result;
            Assert.AreEqual(1, errorList.Count, "Errors occured");
            Assert.IsFalse(results, "File was not moved to InProcess");
        }

        private Action<string, MessageType> ErrorLogToList(List<string> logList)
        {
            return (message, type) =>
            {
                if (type == MessageType.Error)
                {
                    logList.Add(message);
                }
            };
        }
    }
}
