﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutomationTestInterfaces;
using TestMonitor;
using TestMonitor.Interfaces;

namespace DitSmokeTesterUnitTests
{
    internal class ProcessMonitorStub : IMonitor
    {
        public ProcessMonitorStub(IMonitorConfig settings)
        {
            Settings = settings;
            LogMethod = (message, messageType) => {};
        }

        public IMonitorConfig Settings { get; }
        public Action<string, MessageType> LogMethod { get; }
    }
}