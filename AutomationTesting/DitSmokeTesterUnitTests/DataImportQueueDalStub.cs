﻿using System;
using System.Collections.Generic;
using System.Linq;
using TestMonitor;
using TestMonitor.Interfaces;

namespace DitSmokeTesterUnitTests
{
    public class DataImportQueueDalStub : IDataImportQueueDal {
        private Func<TriggerStatus> _checkForNewQueueRow;
        public DataImportQueueDalStub(Func<TriggerStatus> checkForQueueRecord) {
            _checkForNewQueueRow = checkForQueueRecord;
        }

        public DataImportQueueDalStub(Queue<TriggerStatus> dataQueue) : this(
            () => {
                if (dataQueue.Count > 0) {
                    return dataQueue.Dequeue();
                }
                return TriggerStatus.Fail;
            }) {
        }

        public TriggerStatus CheckForNewQueueRow(string clientProcessCode, int lastDataImportQueueId) {
            return _checkForNewQueueRow();
        }
        public int GetLastDataImportQueueId(string clientProcessCode) {
            return 0;
        }
    }
}