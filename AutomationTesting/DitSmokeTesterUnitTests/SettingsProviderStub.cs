﻿using System;
using System.Collections.Generic;
using AutomationTestInterfaces;
using DitSmokeTester.Interfaces;
using TestMonitor.Interfaces;

namespace DitSmokeTesterUnitTests
{
    public class SettingsProviderStub : IConfig
    {
        string IMonitorConfig.ConnectionString => "";
        public TimeSpan DatabasePollingFreq => TimeSpan.FromMilliseconds(10);
        public TimeSpan GetTimeout(DitProcessingType processType, DitProcessingPhases processingPhase)
        {
            if (processType == DitProcessingType.ImageRpsClientSetup)
            {
                switch (processingPhase)
                {
                    case DitProcessingPhases.Input:
                        return TimeSpan.Zero;
                    case DitProcessingPhases.ClientProcessing:
                        return TimeSpan.FromMilliseconds(500);
                    case DitProcessingPhases.SsisProcessing:
                        return TimeSpan.FromMilliseconds(500);
                    default:
                        return TimeSpan.Zero;
                }
            }
            else {
                switch (processingPhase)
                {
                    case DitProcessingPhases.Input:
                        return TimeSpan.Zero;
                    case DitProcessingPhases.ClientProcessing:
                        return TimeSpan.FromMilliseconds(500);
                    case DitProcessingPhases.SsisProcessing:
                        return TimeSpan.FromMilliseconds(500);
                    default:
                        return TimeSpan.Zero;
                }
            }
        }

        public string ImagesFolderPath => "";
    }
}