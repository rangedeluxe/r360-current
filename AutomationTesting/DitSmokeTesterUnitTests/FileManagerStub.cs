﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using TestFileManager.Interfaces;

namespace DitSmokeTesterUnitTests
{
    internal class FileManagerStub : IFileManager
    {
        public delegate bool CopyFileHandlerDelegate(
            string sourceFile, string destinationDirectory, bool createDirectory = false);
        public delegate bool WriteFileHandlerDelegate(
            string fileName, string content, bool createDirectory = false);
        public FileManagerStub()
        {
            TriggerMethods = new List<Action<string, string>>();
        }

        private List<Action<string, string>> TriggerMethods { get; }

        public CopyFileHandlerDelegate CopyFileHandler { get; set; }
        public WriteFileHandlerDelegate WriteFileHandler { get; set; }

        public bool CheckOrCreatePath(string path, bool createIfNeeded)
        {
            throw new NotImplementedException();
        }

        public bool ClearPath(string directoryPath, bool createDirectory = false)
        {
            throw new NotImplementedException();
        }

        public void AddAFile(string filename, string destinationDirectory)
        {
            TriggerMethods.ToList().ForEach(
                method => method(destinationDirectory, Path.GetFileName(filename))
            );
        }

        public bool CopyFile(string sourceFile, string destinationDirectory, bool createDirectory = false)
        {
            return CopyFileHandler?.Invoke(sourceFile, destinationDirectory, createDirectory) ?? false;
        }

        public bool WriteFile(string fileName, string content, bool createDirectory = false)
        {
            AddAFile(Path.GetFileName(fileName), Path.GetDirectoryName(fileName));
            return WriteFileHandler?.Invoke(fileName, content, createDirectory) ?? true;
        }

        public string[] ListFiles(string directoryPath, string pattern, bool createDirectory = false)
        {
            throw new NotImplementedException();
        }

        public string FindInputPaths(ImportType importType)
        {
            return @"X:\WFSApps\RecHub\Data\DataImport";
        }
        public Task<bool> CreateFileChangeTask(string pathToWatch, Func<string, bool> isMatchingFile, TimeSpan timeout) {
            var tcs = new TaskCompletionSource<bool>();
            var timer = new System.Timers.Timer();

            System.Timers.ElapsedEventHandler timeoutHandler = null;
            Action<string, string> fileAddedHandler = null;
            
            Action removeHandlers = () =>
            {
                if (TriggerMethods.Contains(fileAddedHandler)) {
                    TriggerMethods.Remove(fileAddedHandler);
                }
                timer.Elapsed -= timeoutHandler;
                timer.Dispose();
            };

            fileAddedHandler = (directory, filename) => {
                if (pathToWatch.Equals(directory, StringComparison.InvariantCultureIgnoreCase)) {
                    if (isMatchingFile(filename)) {
                        tcs.TrySetResult(true);
                        removeHandlers();
                    }
                }
            };
            
            timeoutHandler = (sender, eventArgs) =>
            {
                tcs.TrySetCanceled(CancellationToken.None);
                removeHandlers();
            };

            TriggerMethods.Add(fileAddedHandler);
            timer.Elapsed += timeoutHandler;
            timer.Enabled = true;
            return tcs.Task;
        }

        public bool CheckIfFileExists(string filename)
        {
            return true;
        }

        public bool WriteFile(string fileName, Bitmap content, bool createDirectory = false)
        {
            AddAFile(Path.GetFileName(fileName), Path.GetDirectoryName(fileName));
            return WriteFileHandler?.Invoke(fileName, $"Bitmap {Path.GetFileName(fileName)}", createDirectory) ?? false;
        }
    }
}