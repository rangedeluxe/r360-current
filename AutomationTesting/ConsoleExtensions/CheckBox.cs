﻿using System;

namespace ConsoleExtensions
{
    public class CheckBox : ConsoleControl<bool>
    {
        public CheckBox(ConsoleColorSet highlightColors, ConsoleColorSet backColors) : base(highlightColors, backColors)
        {
        }
        public CheckBox(ConsoleColorSet highlightColors) : this(highlightColors, new ConsoleColorSet(Console.ForegroundColor, Console.BackgroundColor))
        {
        }

        protected override void DrawMe()
        {
            (ControlValue ? HighlightColors : BackgroundColors).WithColors(() =>
            {
                Console.Write(ControlValue ? "*" : " ");
            });
        }

        public static CheckBox CreateCheckbox(bool value)
        {
            return CreateCheckbox(new ConsoleColorSet(ConsoleColor.Black, ConsoleColor.Gray), value);
        }

        public static CheckBox CreateCheckbox(ConsoleColorSet highlightColors, bool value)
        {
            var result = new CheckBox(highlightColors) { ControlValue = value };
            result.DrawMe();
            return result;
        }
    }
}
