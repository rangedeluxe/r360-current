﻿namespace ConsoleExtensions
{
    public abstract class ConsoleControl<TControlValueType>
    {
        private TControlValueType _controlValue;
        protected ConsolePoint Location { get; set; }

        protected ConsoleControl(ConsoleColorSet highlightColors, ConsoleColorSet backColors)
        {
            Location = Extensions.GetLocation();
            HighlightColors = highlightColors;
            BackgroundColors = backColors;
        }

        protected ConsoleColorSet BackgroundColors { get; set; }

        protected ConsoleColorSet HighlightColors { get; set; }

        public TControlValueType ControlValue
        {
            get { return _controlValue; }
            set
            {
                _controlValue = value;
                UpdateMe();
            }
        }

        protected void UpdateMe()
        {
            Location.AtLocation(DrawMe);
        }

        protected abstract void DrawMe();
    }
}
