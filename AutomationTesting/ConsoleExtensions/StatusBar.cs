﻿using System;

namespace ConsoleExtensions
{
    public class StatusBar : ConsoleControl<int>
    {
        public StatusBar(ConsoleColorSet barColors, int width, Func<int, string> getContent) : this(barColors, new ConsoleColorSet(Console.ForegroundColor, Console.BackgroundColor), width, getContent) { }

        public StatusBar(ConsoleColorSet barColors, ConsoleColorSet backColors, int width, Func<int, string> getContent)
                : base(barColors, backColors)
        {
            Width = width;
            GetContent = getContent;
        }

        public int Width { get; set; }

        private Func<int, string> GetContent { get; }


        protected override void DrawMe()
        {
            var barLength = (int)((Width * ControlValue) / 100.0);
            var content = GetContent(ControlValue).PadRight(Width);
            if (barLength > 0)
            {
                HighlightColors.WithColors(() =>
                {
                    Console.Write(content.Substring(0, barLength));
                });
            }
            BackgroundColors.WithColors(() =>
            {
                Console.Write(content.Substring(barLength, Width - barLength));
            });
        }

        public static StatusBar CreateStatusBar(ConsoleColorSet barColors, Func<int, string> getContent)
        {
            return new StatusBar(barColors, getContent(100).Length, getContent);
        }

        public static StatusBar CreateStatusBar(ConsoleColorSet barColors, string content)
        {
            return CreateStatusBar(barColors, percent => content);
        }

        public static StatusBar CreateStatusBar(string content)
        {
            var result = CreateStatusBar(new ConsoleColorSet(ConsoleColor.Red, ConsoleColor.White), content);

            result.DrawMe();
            return result;
        }
    }
}
