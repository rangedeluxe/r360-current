﻿using System;

namespace ConsoleExtensions
{
    public static class Extensions
    {
        public static void WriteCenter(this string text)
        {
            int pad = (Console.BufferWidth - text.Length) / 2;

            Console.WriteLine(text.PadLeft(pad));
        }

        public static void AtLocation(this ConsolePoint point, Action writeAction)
        {
            var oldPoint = GetLocation();

            SetLocation(point);
            writeAction();
            oldPoint.SetLocation();
        }

        public static void WithColors(this ConsoleColorSet colors, Action writeAction)
        {
            var oldColors = GetColors();

            colors.SetColors();
            writeAction();
            oldColors.SetColors();
        }

        public static ConsoleColorSet GetColors()
        {
            return new ConsoleColorSet(Console.ForegroundColor, Console.BackgroundColor);
        }

        public static void SetColors(this ConsoleColorSet colors)
        {
            Console.ForegroundColor = colors.Foreground;
            Console.BackgroundColor = colors.Background;
        }

        public static void WriteAt(this string text, ConsolePoint point, ConsoleColorSet colors)
        {
            point.AtLocation(() => Console.Write(text));
        }

        public static void WriteAt(this string text, int left, int top)
        {
            text.WriteAt(new ConsolePoint(left, top));
        }

        public static void WriteAt(this string text, ConsolePoint point)
        {
            AtLocation(point, () => Console.Write(text));
        }

        public static ConsolePoint GetLocation()
        {
            return new ConsolePoint(Console.CursorLeft, Console.CursorTop);
        }

        public static void SetLocation(this ConsolePoint point)
        {
            Console.SetCursorPosition(point.X, point.Y);
        }
    }
}
