﻿using System;

namespace ConsoleExtensions
{
    public class ConsoleColorSet
    {
        public ConsoleColorSet(ConsoleColor foreground, ConsoleColor background)
        {
            Foreground = foreground;
            Background = background;
        }

        public ConsoleColor Background { get; }
        public ConsoleColor Foreground { get; }
    }
}
