﻿using System;
using System.Collections.Generic;
using System.Linq;
using static ConsoleExtensions.Extensions;

namespace ConsoleExtensions
{
    public class ScrollingDisplay : ConsoleControl<Queue<Action>>
    {
        public ScrollingDisplay(int height) : this(height, "") { }
        public ScrollingDisplay(int height, string prefix) : this(height, prefix, GetColors(), GetColors()) { }
        public ScrollingDisplay(int height, string prefix, ConsoleColorSet highlightColors) : this(height, prefix, highlightColors, GetColors()) { }

        public ScrollingDisplay(int height, ConsoleColorSet highlightColors) : this(height, highlightColors, GetColors()) { }

        public ScrollingDisplay(int height, ConsoleColorSet highlightColors, ConsoleColorSet backColors)
            : this(height, "", highlightColors, backColors)
        {
        }
        public ScrollingDisplay(int height, string prefix, ConsoleColorSet highlightColors, ConsoleColorSet backColors)
            : base(highlightColors, backColors)
        {
            ControlValue = new Queue<Action>();
            Height = height;
            Prefix = prefix;
        }

        public string Prefix { get; set; }

        public int Height { get; protected set; }

        public void QueueMessage(Action drawMessage)
        {
            ControlValue.Enqueue(drawMessage);
            UpdateMe();
        }

        protected override void DrawMe()
        {
            var messages = ControlValue.ToList();
            while (messages.Count > Height)
            {
                messages.RemoveAt(0);
            }
            if (messages.Count < Height)
            {
                messages = messages.Concat(Enumerable.Repeat<Action>(() => Console.Write(""), (Height - messages.Count))).ToList();
            }
            foreach (var drawLine in messages)
            {
                Console.Write(Prefix);
                drawLine();
                Console.WriteLine();
            }
        }

        public string StandardizeText(string value)
        {
            return value.PadRight(Console.WindowWidth - 1 - Prefix.Length);
        }

        public static ScrollingDisplay CreateDisplay(int height, params Action[] messages)
        {
            var result = new ScrollingDisplay(height);

            foreach (var message in messages)
            {
                result.ControlValue.Enqueue(message);
            }
            result.UpdateMe();
            return result;
        }
    }
}
