﻿namespace ConsoleExtensions
{
    public class ConsolePoint
    {
        public ConsolePoint(int x, int y)
        {
            X = x;
            Y = y;
        }
        public int X { get; }
        public int Y { get; }
    }
}
