﻿using System;
using System.Collections.Generic;
using AutomationTestInterfaces;
using DitSmokeTester.Interfaces;

namespace DitSmokeTester
{
    public class SettingDefaults : IConfig
    {
        public TimeSpan GetTimeout(DitProcessingType processType, DitProcessingPhases processingPhase) {
            if (processType == DitProcessingType.ImageRpsClientSetup) {
                switch (processingPhase) {
                    case DitProcessingPhases.Input:
                        return TimeSpan.Zero;
                    case DitProcessingPhases.ClientProcessing:
                        return TimeSpan.FromMilliseconds(20000);
                    case DitProcessingPhases.SsisProcessing:
                        return TimeSpan.FromMilliseconds(90000);
                    default:
                        return TimeSpan.Zero;
                }
            }
            switch (processingPhase) {
                case DitProcessingPhases.Input:
                    return TimeSpan.Zero;
                case DitProcessingPhases.ClientProcessing:
                    return TimeSpan.FromMilliseconds(20000);
                case DitProcessingPhases.SsisProcessing:
                    return TimeSpan.FromMilliseconds(90000);
                default:
                    return TimeSpan.Zero;
            }
        }
        public string ConnectionString =>
            "Password = Wausau#1;User ID=sa;Initial Catalog=Current_R360_NP;Data Source=RECHUBDEVDB02.qalabs.nwk";
        public TimeSpan DatabasePollingFreq => TimeSpan.FromSeconds(1.0);
        public string ImagesFolderPath => @"..\..\Images";
    }
}
