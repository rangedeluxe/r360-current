﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using AutomationTestInterfaces;
using DitSmokeTester.Interfaces;

namespace DitSmokeTester
{
    public class SettingsProvider : IConfig
    {
        public string ConnectionString { get; set; }
        public TimeSpan DatabasePollingFreq { get; set; }

        public string ImagesFolderPath { get; set; }

        public TimeSpan GetTimeout(DitProcessingType processType, DitProcessingPhases processingPhase) {
            var typeName = Enum.GetName(typeof (DitProcessingType), processType);
            var phaseName = Enum.GetName(typeof (DitProcessingPhases), processingPhase);
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            return new TimeSpan(0, 0, 0, 0, ReadValueOrDefault(configFile.AppSettings.Settings, $"{typeName}_{phaseName}_TimoutInMs", 1000)); 
        }
        public static SettingsProvider LoadConfiguration(IConfig defaultSettings)
		{
            var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            EnsureSetting(configFile, "ConnectionString", defaultSettings.ConnectionString);
            EnsureSetting(configFile, "ImageFolderPath", defaultSettings.ImagesFolderPath);
            EnsureSetting(configFile, "DatabasePollingFreq", defaultSettings.DatabasePollingFreq);
            return new SettingsProvider()
        	{
				ConnectionString = ReadValueOrDefault(configFile.AppSettings.Settings, "ConnectionString", defaultSettings.ConnectionString),
            	ImagesFolderPath = ReadValueOrDefault(configFile.AppSettings.Settings, "ImageFolderPath", defaultSettings.ImagesFolderPath),
                DatabasePollingFreq = ReadValueOrDefault(configFile.AppSettings.Settings, "DatabasePollingFraq", defaultSettings.DatabasePollingFreq)
            };    
        }

        private static void EnsureSetting<T>(Configuration configSettings, string key, T defaultValue)
        {
            if (!configSettings.AppSettings.Settings.AllKeys.Contains(key))
            {
                configSettings.AppSettings.Settings.Add(key, defaultValue.ToString());
            }
        }

        private static T ReadValueOrDefault<T>(KeyValueConfigurationCollection settings, string key, T defaultValue)
        {
            return settings.AllKeys.Contains(key) ? (T)Convert.ChangeType(settings[key].Value, typeof(T)) : defaultValue;
        }
    }
}
