﻿using System;
using System.Collections.Generic;
using AutomationTestInterfaces;
using TestMonitor.Interfaces;

namespace DitSmokeTester.Interfaces
{
    public interface IConfig : IMonitorConfig
    {
        TimeSpan GetTimeout(DitProcessingType processType, DitProcessingPhases processingPhase);
		string ImagesFolderPath { get; }
    }
}
