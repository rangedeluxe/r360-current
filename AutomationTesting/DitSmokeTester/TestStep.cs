﻿using System;
using AutomationTestInterfaces;

namespace DitSmokeTester
{
    public interface IStepEventArgs
    {
        bool Succeeded { get; }
        string StepName { get; }
        string ProcessPath { get; }
    }

    public class InputFileWrittenEventArgs : IStepEventArgs
    {
        public InputFileWrittenEventArgs(string processPath, bool succeeded)
        {
            Succeeded = succeeded;
            ProcessPath = processPath;
        }

        public string SourceFile { get; set; }
        public string DestinationPath { get; set; }
        public bool Succeeded { get; }

        public string StepName => "InputFileWritten";
        public string ProcessPath { get; }
    }

    public class MonitoredFileFound : IStepEventArgs
    {
        public MonitoredFileFound(string processPath, bool succeeded)
        {
            Succeeded = succeeded;
            ProcessPath = processPath;
        }
        public bool Succeeded { get; }
        public string StepName => "ClientProcessing";

        public string ProcessPath { get; }
    }

    public class MonitoredTableRecordFound : IStepEventArgs
    {
        public MonitoredTableRecordFound(string processPath, string filename, bool succeeded)
        {
            Succeeded = succeeded;
            Filename = filename;
            StepName = filename;
            ProcessPath = processPath;
        }
        public string Filename { get; set; }
        public bool Succeeded { get; }
        public string StepName { get; }

        public string ProcessPath { get; }
    }

    public class GeneralStepCompleted : IStepEventArgs
    {
        public GeneralStepCompleted(string processPath, DitProcessingPhases step, bool succeeded)
        {
            Succeeded = succeeded;
            StepName = Enum.GetName(typeof(DitProcessingPhases), step);
            ProcessPath = processPath;
        }
        public bool Succeeded { get; }
        public string StepName { get; }
        public string ProcessPath { get; }
    }

    public class ProcessPathCompleted : IStepEventArgs
    {
        public ProcessPathCompleted(string processPath, string stepName)
        {
            Succeeded = true;
            StepName = stepName;
            ProcessPath = processPath;
        }
        public bool Succeeded { get; }
        public string StepName { get; }
        public string ProcessPath { get; }
    }
}