﻿using System.Linq;
using System.Xml.Linq;

namespace DitSmokeTester
{
    public class FileGroup
    {
        private const string DESTATTRIBNAME = "Destination";
        private const string PATHATTRIBNAME = "Path";
        private const string FILENODENAME = "File";
        private const string FILEGROUPNODENAME = "FileGroup";
        //public string Destination { get; set; }
        public string[] SourceFiles { get; set; }

        public XElement ToXml()
        {
            return new XElement(FILEGROUPNODENAME,
                    //new XAttribute(DESTATTRIBNAME, Destination),
                    SourceFiles.Select(filePath => new XElement(FILENODENAME, new XAttribute(PATHATTRIBNAME, filePath))));
        }

        public static FileGroup LoadFileGroup(XElement fileGroupNode)
        {
            return new FileGroup()
            {
                //Destination = fileGroupNode.Attribute(DESTATTRIBNAME)?.Value,
                SourceFiles = fileGroupNode.Elements(FILENODENAME).Select(fileNode => fileNode.Attribute(PATHATTRIBNAME)?.Value).ToArray()
            };
        }
    }
}