﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DitSmokeTester
{
    public static class EnumerableExtensions
    {
        public static void ForEach<T>(this IEnumerable<T> items, Action<T, int> doAction)
        {
            foreach (var row in items.Select((item, index) => new { item, index }))
            {
                doAction(row.item, row.index);
            }
        }

        public static void ForEach<T>(this IEnumerable<T> items, Action<T> doAction)
        {
            foreach (var row in items.Select((item, index) => new { item, index }))
            {
                doAction(row.item);
            }
        }
    }
}
