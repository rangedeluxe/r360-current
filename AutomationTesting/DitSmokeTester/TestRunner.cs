﻿using AutomationTestInterfaces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Threading;
using DitSmokeTester.Interfaces;
using TestFileManager.Interfaces;
using TestMonitor;
using TestMonitor.Interfaces;
using System.Threading.Tasks;
using DitSmokeTester.Properties;
using TestFileManager;

namespace DitSmokeTester
{
    public class TestRunner : ILoggable
    {
        public delegate void TestStepCompletedDelegate(IStepEventArgs stepData);

        public TestRunner(
            IConfig settings, IFileManager fileManager,
            IMonitor systemMonitor,
            IDataImportQueueDal ditDal,
            Action<string, MessageType> logMethod)
        {
            Settings = settings;
            FileSystemManager = fileManager;
            SystemMonitor = systemMonitor;
            DitDal = ditDal;
            LogMethod = logMethod;
            PhaseStatuses = new List<ProcessPhaseStatus>();
            TestComplete = false;
        }

        public event TestStepCompletedDelegate StepCompleted;

        public async Task<bool> Run()
        {
            var chainIntact = true;
            var ditFolders = new DitFolders(FileSystemManager.FindInputPaths(ImportType.ClientSetup), Settings.ImagesFolderPath);

            try
            {
                var imagesFolder = ditFolders.ImagePath;
                var batchInputFolder = ditFolders.BatchInputFolder;
                BuildImageFolder(imagesFolder);
                var clientSetupTargets = new List<IWatcherTarget>() {
                    new FileWatcherTarget(
                        DitProcessingType.ImageRpsClientSetup, 
                        DitProcessingPhases.ClientProcessing,
                        ditFolders.ClientSetupInProcessFolder,
                        filename =>
                                Path.GetFileName(filename)
                                    ?.StartsWith(Path.GetFileName(ditFolders.ClientSetupInputFile), StringComparison.InvariantCultureIgnoreCase) 
                                    ?? false
                    ),
                    new DatabaseWatcherTarget(
                        DitProcessingType.ImageRpsClientSetup, 
                        DitProcessingPhases.SsisProcessing,
                        "AutoClientCon"
                    )
                };
                var batchTargets = new List<IWatcherTarget>() {
                    new DatabaseWatcherTarget(
                        DitProcessingType.ImageRpsBatch, 
                        DitProcessingPhases.SsisProcessing,
                        "AutoBatchCon"
                    )
                };

                if (FileSystemManager.WriteFile(ditFolders.ClientSetupInputFile,
                    Properties.Resources.Client987001_DataGenTest987001_1_20170825_104408)) {
                    foreach (var target in clientSetupTargets)
                    {
                        var currentTask = CreateWatcher(target);
                        if (chainIntact) {
                            chainIntact &= await currentTask;
                            OnStepComplete("Client Setup", target.ProcessPhase, chainIntact);
                        }
                    }

                    if (chainIntact) {
                        if (FileSystemManager.WriteFile(ditFolders.BatchInputFile,
                            EditBatchContent(Properties.Resources.BatchFile_987001_21_21, imagesFolder), 
                            createDirectory: true))
                        {
                            foreach (var target in batchTargets) {
                                var currentTask = CreateWatcher(target);
                                if (chainIntact) {
                                    chainIntact &= await currentTask;
                                    OnStepComplete("Batch", target.ProcessPhase, chainIntact);
                                }
                            }
                        }
                        else {
                            chainIntact = false;
                        }
                        FileTestResults(chainIntact);
                    }
                }
            }
            catch (Exception ex) {
                chainIntact = false;
                PhaseStatuses.Add(new ProcessPhaseStatus(DitProcessingPhases.Input, false));
                LogMethod(ex.Message, MessageType.Error);
            }
            return chainIntact;
        }

        private Task<bool> CreateWatcher(IWatcherTarget target)
        {
            if (target is FileWatcherTarget)
            {
                var fileTarget = (FileWatcherTarget)target;
                return FileSystemManager.CreateFileChangeTask(
                    fileTarget.DirectoryPath,
                    fileTarget.IsMatch,
                    Settings.GetTimeout(fileTarget.ProcessType, fileTarget.ProcessPhase)
                );
            }
            var dbTarget = (DatabaseWatcherTarget) target;
            var lastDataImportQueueId = DitDal.GetLastDataImportQueueId(dbTarget.ClientProcessingCode);
            return ProcessMonitor.CreatePollingTask(
                () => DitDal.CheckForNewQueueRow(dbTarget.ClientProcessingCode, lastDataImportQueueId),
                Settings.DatabasePollingFreq,
                Settings.GetTimeout(dbTarget.ProcessType, dbTarget.ProcessPhase)
             );
        }
        
        private void FileTestResults(bool result)
        {
            TestResult = result;
            TestComplete = true;
        }

        private string EditBatchContent(string content, string imagesFolder)
        {
            return content.Replace("<ImageFolderPath>", imagesFolder);
        }

        private void BuildImageFolder(string imageFolder)
        {
            var imageTable = new Dictionary<string, Bitmap>() {
                { "CheckFront.tif", Properties.Resources.CheckFront},
                { "CheckBack.tif", Properties.Resources.CheckBack},
                { "DocumentFront.tif", Properties.Resources.DocumentFront},
                { "DocumentBack.tif", Properties.Resources.DocumentBack}
            };
            imageTable.ForEach(pair =>
            {
                var fullName = Path.Combine(imageFolder, pair.Key);
                if (!FileSystemManager.CheckIfFileExists(fullName))
                {
                    FileSystemManager.WriteFile(fullName, pair.Value, true);
                }
            });
        }

        public bool WaitForResults()
        {
            while (!TestComplete)
            {
                Thread.Sleep(100);
            }
            return TestResult;
        }

        private void OnStepComplete(string processPath, DitProcessingPhases phase, bool succeeded)
        {
            StepCompleted?.Invoke(new GeneralStepCompleted(processPath, phase, succeeded));
        }

        private void OnInputFileWritten(string processPath, string sourceFile, string destPath, bool succeeded)
        {
            StepCompleted?.Invoke(new InputFileWrittenEventArgs(processPath, succeeded)
            {
                SourceFile = sourceFile,
                DestinationPath = destPath
            });
        }

        private IConfig Settings { get; }

        private IFileManager FileSystemManager { get; }

        private IMonitor SystemMonitor { get; }

        private List<ProcessPhaseStatus> PhaseStatuses { get; }
        private bool TestComplete { get; set; }
        private bool TestResult { get; set; }
        public Action<string, MessageType> LogMethod { get; }
        public IDataImportQueueDal DitDal { get; private set; }
    }
}
