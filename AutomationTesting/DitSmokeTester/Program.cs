﻿using AutomationTestInterfaces;
using System;
using System.Collections.Generic;
using ConsoleExtensions;
using DitSmokeTester.Interfaces;
using TestFileManager;
using TestFileManager.Interfaces;
using TestMonitor;
using TestMonitor.Interfaces;

namespace DitSmokeTester
{
    class Program
    {
        private static readonly IConfig _settings = SettingsProvider.LoadConfiguration(new SettingDefaults());
        private static readonly IFileManager _fileManager =
            new FileManager(Properties.Settings.Default.DriveSearchOrder.Split(','), LogMessage);
        private static readonly IMonitor _systemMonitor =
            new ProcessMonitor(_settings);

        public static void Main(string[] args)
        {
            var runner = new TestRunner(_settings, _fileManager, _systemMonitor, new DataImportQueueDal(_settings.ConnectionString), LogMessage);
            var colorsTable = new Dictionary<bool, ConsoleColorSet>() {
                {true, new ConsoleColorSet(ConsoleColor.Green, ConsoleColor.Black)},
                {false, new ConsoleColorSet(ConsoleColor.Red, ConsoleColor.Black)}
            };

            "DIT Smoke Tester".WriteCenter();

            runner.StepCompleted +=
                data =>
                {
                    colorsTable[data.Succeeded].WithColors(
                                () => Console.WriteLine($"{data.ProcessPath}: {(data.Succeeded ? $"Completed step {data.StepName}" : $"Failed in step {data.StepName}")}"));
                };
            var testJob = runner.Run();
            testJob.Wait();
            Console.WriteLine(testJob.Result ? "Complete" : "Failure");
            Console.WriteLine("Press Any Key to Exit...");
            Console.ReadKey();
        }

        private static void LogMessage(string message, MessageType messageType)
        {
            var colorsTable = new Dictionary<MessageType, ConsoleColorSet>() {
                {MessageType.Note, new ConsoleColorSet(ConsoleColor.Gray, ConsoleColor.Black )},
                {MessageType.Warning, new ConsoleColorSet(ConsoleColor.Yellow, ConsoleColor.Black )},
                {MessageType.Error, new ConsoleColorSet(ConsoleColor.Red, ConsoleColor.White )}
            };

            colorsTable[messageType].WithColors(
                () => Console.WriteLine(message)
            );
        }
    }
}