﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutomationTestInterfaces;

namespace AutomationTestInterfaces
{
    public interface IWatcherTarget
    {
        DitProcessingType ProcessType { get; }
        DitProcessingPhases ProcessPhase { get; }
    }
}
