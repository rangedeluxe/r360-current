﻿using System;

namespace AutomationTestInterfaces
{
    public interface ILoggable
    {
        Action<string, MessageType> LogMethod { get; }
    }

    public enum MessageType
    {
        Error,
        Warning,
        Note
    }
}
