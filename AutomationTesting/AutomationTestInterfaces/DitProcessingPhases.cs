﻿namespace AutomationTestInterfaces
{
    public enum DitProcessingPhases
    {
        Input,
        ClientProcessing,
        SsisProcessing
    }
}
