﻿using System;
using System.Web;
using System.Web.Mvc;


namespace Wfs.Raam.Core.Helper
{
    [AttributeUsage(AttributeTargets.Method | AttributeTargets.Class)]
    public sealed class NoCacheAttribute : FilterAttribute, IResultFilter
    {
        public void OnResultExecuting(ResultExecutingContext filterContext)
        {
        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var cache = filterContext.HttpContext.Response.Cache;
            cache.SetCacheability(HttpCacheability.NoCache);
            cache.SetExpires(DateTime.Now.AddYears(-5));
            cache.SetProxyMaxAge(TimeSpan.Zero);
            cache.SetNoStore();
        }
    }
}
