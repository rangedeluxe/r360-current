﻿
/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     05/21/2012
*
* Purpose:  This call provides tools common to pages implementing branding elements
*
* Modification History
*
*  CR 51575: CEJ 05/21/2012
*   - Should not create branding cookie when brand folder does not exist
* WI 90252 CRG 03/05/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoWebLib
*******************************************************************************/
namespace WFS.RecHub.Common
    {
    public class cBrandingMethods
        {
        public const string BRANDING_BASE_PATH = "../Styles/";

        public static string GetTemplatePath(System.Web.UI.Page pagCurPage, string sBrandingFolderName)
            {
            return GetTemplatePath(pagCurPage.Server.MapPath(BRANDING_BASE_PATH), sBrandingFolderName);
            }

        public static string GetTemplatePath(string sBrandingBasePath, string sBrandingFolderName)
            {
            return System.IO.Path.Combine(sBrandingBasePath, sBrandingFolderName, "templates");
            }

        public static string GetTemplateFileName(System.Web.UI.Page pagCurPage, string sBrandingFolderName, string sTemplateName)
            {
            return GetTemplateFileName(GetTemplatePath(pagCurPage, sBrandingFolderName),
                    sTemplateName);
            }

        public static string GetTemplateFileName(string sTemplatePath, string sTemplateName)
            {
            return System.IO.Path.Combine(sTemplatePath,
                    sTemplateName + (sTemplateName.EndsWith(".xsl") ? "" : ".xsl"));
            }

        public static bool IsBrandingValid(System.Web.UI.Page pagCurPage, string sBrandingFolderName)
            {
            return System.IO.Directory.Exists(GetTemplatePath(pagCurPage, sBrandingFolderName));
            }
        }
    }
