/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
*      Module: constants.asp
*    Filename: _Page.cs
*      Author: Joel Caples
* Description: Contains references for all publicly used constants.
*
* Revisions:
*
* ----------------------
* 05.13.2003    JMC
*    -Created module to contain definitions for publicly used constants.
* 12.19.2003    JMC
*    -Added constants for IPO Service Xml functions.
* CR 11337 JMC 01/18/2005
*    -Added cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED constant.
* CR 10537 JMC 03/03/2005
*    -Added activityType constants
* CR 22114 JMC 11/06/2007
*    -Ported application to .Net from asp.
* CR 28738 JMC 03/23/2010
*   -Added QS_PARM_GLOBALBATCHID constant.
* CR 45984 JCS 01/19/2012
*   -Added IPBrand cookie for dynamic branding.
* CR 53947 JNE 07/18/2012
*   -Added XML_ATTR_DOCUMENT_DISPLAY_MODE & XML_ATTR_CHECK_DISPLAY_MODE constants
*   -Added OLFImageDisplayMode enumeration
* CR 54169 JNE 08/08/2012
*   -Added XML_ELE_DISPLAYBATCHID & QS_PARM_BATCHNUMBER constants 
* WI 90252 CRG 03/05/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoWebLib
* WI 161977 SAS 08/28/2014
*	Changes done to add NoReport page constant for ImageRPSReport
**************************************************************************/
namespace WFS.RecHub.Common
{
    public static class Constants {

        public const string cCHECKED = "on";

        public const string cACTION_CLEAR = "clear";

        public const string cICON_CHECK = "../Styles/Online/images/cbo_icon_check.gif";
        public const string cICON_DOCUMENT = "../Styles/Online/images/cbo_icon_document.gif";
        public const string cICON_BATCH_DETAIL = "../Styles/Online/images/cbo_icon_batchdetail.gif";
        public const string cICON_COTS = "../Styles/Online/images/cbo_icon_cots.gif";
        public const string cICON_MARK_SENSE = "../Styles/Online/images/cbo_icon_mark_sense.gif";

        public const string cASCENDING = "ASCENDING";
        public const string cDESCENDING = "DESCENDING";


        //*************************************************************************
        //* IPO Image Service Constants
        //*************************************************************************
        public const string cIPO_IMAGE_JOB_TYPE_ZIP = "ZIP";
        public const string cIPO_IMAGE_JOB_TYPE_PRINT_PDF = "PDF";
        public const string cIPO_IMAGE_JOB_TYPE_PRINT_HTML = "IPO_IMAGE_JOB_TYPE_PRINT_HTML";
        public const string cIPO_IMAGE_JOB_TYPE_CSV = "IPO_IMAGE_JOB_TYPE_CSV";
        public const string cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED = "IPO_IMAGE_JOB_TYPE_VIEW_SELECTED";

        public const string XML_ELE_JOB_REQ_OPTIONS = "jobRequestOptions";
        public const string XML_ATTR_RETURN_TYPE = "returnType";
        public const string XML_ATTR_DOCUMENT_DISPLAY_MODE = "documentImageDisplayMode";
        public const string XML_ATTR_CHECK_DISPLAY_MODE = "checkImageDisplayMode";

        public const string XML_ELE_DISPLAYBATCHID = "displaybatchid";

        /// <summary>
        /// Enumeration of possible states for display sides of image
        /// </summary>
        public enum OLFImageDisplayMode
        {
            /// <summary></summary>
            Undefined = 0,
            /// <summary></summary>
            BothSides = 1,
            /// <summary></summary>
            FrontOnly = 2
        }

        //***** Outputs Node
        public const string XML_ELE_OUTPUTS = "outputs";

        //***** Output Options
        public const string XML_ELE_OUTPUT = "output";
        public const string XML_ATTR_OUTPUT_FORMAT = "outputFormat";

        //***** Text Options
        public const string XML_ELE_TEXT_OPTIONS = "textOptions";
        public const string XML_ATTR_TEXT_FILE_EXT = "fileExtension";

        public const string FILE_EXT_PDF = "pdf";
        public const string FILE_EXT_ZIP = "zip";
        public const string FILE_EXT_JPG = "jpg";
        public const string FILE_EXT_XML = "xml";
        public const string FILE_EXT_HTML = "html";
        public const string FILE_EXT_TIF = "tif";
        public const string FILE_EXT_CSV = "csv";

        public const string BRANDING_BASE_PATH = "../Styles/";
        public const string NOIMAGE = "/noimage.aspx";
        public const string NOREPORT = "/noreport.aspx";
        public const string NOIMAGE_RESEARCH = "../Styles/Research/noimage.aspx";
        public const string NOREPORT_RESEARCH = "../Styles/Research/noreport.aspx";

        public const string QS_PARM_OLLOCKBOXID = "lckbx";
        public const string QS_PARM_BANKID = "bank";
        public const string QS_PARM_CUSTOMERID = "customer";
        public const string QS_PARM_LOCKBOXID = "lbx";
        public const string QS_PARM_GLOBALBATCHID = "gbatch";
        public const string QS_PARM_BATCHID = "batch";
        public const string QS_PARM_DEPOSITDATE = "date";
        public const string QS_PARM_PICSDATE = "picsdate";
        public const string QS_PARM_TXNID = "txn";
        public const string QS_PARM_TXNSEQUENCE = "seq";
        public const string QS_PARM_BATCHSEQUENCE = "item";
        public const string QS_PARM_RT = "rt";
        public const string QS_PARM_ACCOUNT = "acct";
        public const string QS_PARM_PAGE = "page";
        public const string QS_PARM_IMAGEFILTEROPTION = "imagefilteroption";
        public const string QS_PARM_PROCESSINGDATE = "processingdate";
        public const string QS_PARM_BATCHNUMBER = "batchnumber";
    }
}
