
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     1/14/2009
*
* Purpose:  Message object.
*
* Modification History
*   11/07/2006 CR 18949 JMC
*       - Initial release version.
*   11/06/2007 CR 22114 JMC
*       - Ported application to .Net from asp.
* WI 90252 CRG 03/05/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoWebLib
******************************************************************************/
namespace WFS.RecHub.Common {

	/// <summary>
	/// Summary description for cError.
	/// </summary>
	public class cMsg
	{
        private EventType _Code;
        private string _Message;

		public cMsg() {
		}

        public EventType Code {
            get { return(_Code); }
            set { _Code = value; }
        }

        public string Message {
            get { return(_Message); }
            set { _Message = value; }
        }
	}
}
