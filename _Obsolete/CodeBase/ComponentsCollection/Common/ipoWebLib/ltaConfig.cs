﻿using System.Configuration;
using System.Web.Configuration;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Wayne Schwarz
* Date: 
*
* Purpose: 
*
* Modification History
* CR 46148 WJS 08/31/2011
*   - Initial Release
* WI 90252 CRG 03/05/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoWebLib
******************************************************************************/
namespace WFS.RecHub.Common 
{
    /// <summary>
    /// Read ltaConfig
    /// </summary>
    public class ltaConfig
    {
        /// <summary>
        /// Read app config
        /// </summary>
        /// <param name="path"></param>
        /// <param name="valueToRead"></param>
        /// <returns></returns>
        public static string ReadAppConfig(string path, string valueToRead)
        {
            string appConfigRtnVal = string.Empty;
            Configuration rootWebConfig1 = WebConfigurationManager.OpenWebConfiguration(path);
            if (rootWebConfig1.AppSettings.Settings.Count > 0)
            {
                KeyValueConfigurationElement customSetting =  rootWebConfig1.AppSettings.Settings[valueToRead];
                if (customSetting != null)
                    appConfigRtnVal = customSetting.Value;
            }
            return appConfigRtnVal;
        }
    }
}
