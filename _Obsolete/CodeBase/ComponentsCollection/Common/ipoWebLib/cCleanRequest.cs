﻿using System;
using System.Collections.Specialized;
using System.Web;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Joel Caples
* Date:     12/03/2008
*
* Purpose:  HTTP input parameter value cleaner.
*
* Modification History
*   12/03/2008 CR 24816 JMC
*       - Initial version.
* WI 90252 CRG 03/05/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoWebLib
******************************************************************************/
namespace WFS.RecHub.Common {

    public class cCleanRequest {

        private string _SiteKey;

        private NameValueCollection _Form;
        private NameValueCollection _QueryString;
        
        public cCleanRequest(string SiteKey, HttpRequest PageRequest) {

            _SiteKey = SiteKey;

            _Form = new NameValueCollection();
            foreach(string str in PageRequest.Form) {
                _Form.Add(str, CleanVariable(PageRequest.Form[str]));
            }

            _QueryString = new NameValueCollection();
            foreach(string str in PageRequest.QueryString) {
                _QueryString.Add(str, CleanVariable(PageRequest.QueryString[str]));
            }
        }

        public NameValueCollection Form {
            get {
                return(_Form);
            }
        }

        public NameValueCollection QueryString {
            get {
                return(_QueryString);
            }
        }

        public bool HasFormValue(string Key) {
            if(this.Form[Key] == null) {
                return(false);
            } else {
                return(true);
            }
        }

        public bool HasQueryStringValue(string Key) {
            if(this.QueryString[Key] == null) {
                return(false);
            } else {
                return(true);
            }
        }

        #region GetFormValue Functions

        public string GetFormValue(string Key, string DefaultValue) {

            if(this.Form[Key] == null) {
                return(DefaultValue);
            } else {
                return(this.Form[Key]);
            }
        }

        public int GetFormValue(string Key, int DefaultValue) {

            int intTemp;

            if(this.Form[Key] == null) {
                return(DefaultValue);
            } else {
                if(int.TryParse(this.Form[Key], out intTemp)) {
                    return(intTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public DateTime GetFormValue(string Key, DateTime DefaultValue) {

            DateTime dteTemp;

            if(this.Form[Key] == null) {
                return(DefaultValue);
            } else {
                if(DateTime.TryParse(this.Form[Key], out dteTemp)) {
                    return(dteTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public Decimal GetFormValue(string Key, Decimal DefaultValue) {

            Decimal decTemp;

            if(this.Form[Key] == null) {
                return(DefaultValue);
            } else {
                if(Decimal.TryParse(this.Form[Key], out decTemp)) {
                    return(decTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public Guid GetFormValue(string Key, Guid DefaultValue) {

            Guid gidTemp;

            if(this.Form[Key] == null) {
                return(DefaultValue);
            } else {
                if(ipoLib.TryParse(this.Form[Key], out gidTemp)) {
                    return(gidTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        #endregion GetFormValue Functions

        #region GetQueryStringValue Functions

        public string GetQueryStringValue(string Key, string DefaultValue) {

            if(this.QueryString[Key] == null) {
                return(DefaultValue);
            } else {
                return(this.QueryString[Key]);
            }
        }

        public int GetQueryStringValue(string Key, int DefaultValue) {

            int intTemp;

            if(this.QueryString[Key] == null) {
                return(DefaultValue);
            } else {
                if(int.TryParse(this.QueryString[Key], out intTemp)) {
                    return(intTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public DateTime GetQueryStringValue(string Key, DateTime DefaultValue) {

            DateTime dteTemp;

            if(this.QueryString[Key] == null) {
                return(DefaultValue);
            } else {
                if(DateTime.TryParse(this.QueryString[Key], out dteTemp)) {
                    return(dteTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public Decimal GetQueryStringValue(string Key, Decimal DefaultValue) {

            Decimal decTemp;

            if(this.QueryString[Key] == null) {
                return(DefaultValue);
            } else {
                if(Decimal.TryParse(this.QueryString[Key], out decTemp)) {
                    return(decTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        public Guid GetQueryStringValue(string Key, Guid DefaultValue) {

            Guid gidTemp;

            if(this.QueryString[Key] == null) {
                return(DefaultValue);
            } else {
                if(ipoLib.TryParse(this.QueryString[Key], out gidTemp)) {
                    return(gidTemp);
                } else {
                    return(DefaultValue);
                }
            }
        }

        #endregion GetQueryStringValue Functions

        #region TryGetFormValue Functions

        public bool TryGetFormValue(string Key, out string Value) {

            if(this.Form[Key] == null) {
                Value = string.Empty;
                return(false);
            } else {
                Value = this.Form[Key];
                return(true); // was set to false, but should be true.  02/17/09 JNE
            }
        }

        public bool TryGetFormValue(string Key, out int Value) {

            int intTemp;

            if(this.Form[Key] == null) {
                Value = -1;
                return(false);
            } else {
                if(int.TryParse(this.Form[Key], out intTemp)) {
                    Value = intTemp;
                    return(true);
                } else {
                    Value = -1;
                    return(false);
                }
            }
        }

        public bool TryGetFormValue(string Key, out DateTime Value) {

            DateTime dteTemp;

            if(this.Form[Key] == null) {
                Value = DateTime.MinValue;
                return(false);
            } else {
                if(DateTime.TryParse(this.Form[Key], out dteTemp)) {
                    Value = dteTemp;
                    return(true);
                } else {
                    Value = DateTime.MinValue;
                    return(false);
                }
            }
        }

        public bool TryGetFormValue(string Key, out Decimal Value) {

            Decimal decTemp;

            if(this.Form[Key] == null) {
                Value = Decimal.MinValue;
                return(false);
            } else {
                if(Decimal.TryParse(this.Form[Key], out decTemp)) {
                    Value = decTemp;
                    return(true);
                } else {
                    Value = Decimal.MinValue;
                    return(false);
                }
            }
        }

        public bool TryGetFormValue(string Key, out Guid Value) {

            Guid gidTemp;

            if(this.Form[Key] == null) {
                Value = Guid.Empty;
                return(false);
            } else {
                if(ipoLib.TryParse(this.Form[Key], out gidTemp)) {
                    Value = gidTemp;
                    return(true);
                } else {
                    Value = Guid.Empty;
                    return(false);
                }
            }
        }

        #endregion TryGetFormValue Functions

        #region TryGetQueryStringValue Functions

        public bool TryGetQueryStringValue(string Key, out string Value) {

            if(this.QueryString[Key] == null) {
                Value = string.Empty;
                return(false);
            } else {
                Value = this.QueryString[Key];
                return(true);
            }
        }

        public bool TryGetQueryStringValue(string Key, out int Value) {

            int intTemp;

            if(this.QueryString[Key] == null) {
                Value = -1;
                return(false);
            } else {
                if(int.TryParse(this.QueryString[Key], out intTemp)) {
                    Value = intTemp;
                    return(true);
                } else {
                    Value = -1;
                    return(false);
                }
            }
        }

        public bool TryGetQueryStringValue(string Key, out DateTime Value) {

            DateTime dteTemp;

            if(this.QueryString[Key] == null) {
                Value = DateTime.MinValue;
                return(false);
            } else {
                if(DateTime.TryParse(this.QueryString[Key], out dteTemp)) {
                    Value = dteTemp;
                    return(true);
                } else {
                    Value = DateTime.MinValue;
                    return(false);
                }
            }
        }

        public bool TryGetQueryStringValue(string Key, out Decimal Value) {

            Decimal decTemp;

            if(this.QueryString[Key] == null) {
                Value = Decimal.MinValue;
                return(false);
            } else {
                if(Decimal.TryParse(this.QueryString[Key], out decTemp)) {
                    Value = decTemp;
                    return(true);
                } else {
                    Value = Decimal.MinValue;
                    return(false);
                }
            }
        }

        public bool TryGetQueryStringValue(string Key, out Guid Value) {

            Guid gidTemp;

            if(this.QueryString[Key] == null) {
                Value = Guid.Empty;
                return(false);
            } else {
                if(ipoLib.TryParse(this.QueryString[Key], out gidTemp)) {
                    Value = gidTemp;
                    return(true);
                } else {
                    Value = Guid.Empty;
                    return(false);
                }
            }
        }

        #endregion TryGetQueryStringValue Functions

        private string CleanVariable(string Value) {

	        string strTmp;
	        string strCharacters;
	        string[] arrValues;
        	   
            if(Value == null) {
                strTmp = string.Empty;
            } else {
                strTmp = Value;
            	
                if(strTmp.Length > 0) {

	               strCharacters = ipoINILib.IniReadValue(_SiteKey, "ExclusionCharacters");
               	           	
	               if(strCharacters.Length > 0) {
		               arrValues = strCharacters.Split(',');
                       foreach(string strValue in arrValues) {
			               strTmp = strTmp.Replace(strValue, string.Empty);
		               }
	               }
                }
            }
           
            return(strTmp);
        }

    }
}
