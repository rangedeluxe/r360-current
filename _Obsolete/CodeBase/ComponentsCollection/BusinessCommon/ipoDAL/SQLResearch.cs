﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using WFS.integraPAY.Online.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2008 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/19/2010
*
* Purpose:  
*
* Modification History
* 04/19/2010 JMC CR 29392
*     - Corrected call to SQL_SetWebDeliverdFlag to enclose 
*       @parmOnlineImageQueueID in quotes.
* 04/19/2010 JMC CR 29391
*     - Correct call to usp_SessionActivityLog_UpdateWebDeliveryFlag to 
*       remove extraneous comma.
* CR 46418 JMC 09/08/2011
*   -Removed SQL_GetImageDisplayModeForBatch() function
* CR 49397 JNE 3/1/2012
*   -Added bScannedChecks to SQL_GetDocuments and usp_factDocuments_GetByTransaction call 
* WI 69376 CEJ 11/30/2012 
*  - Modify the CSR Research Batch Details page to expect pagination within the stored procedure
******************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLResearch {


        //********************************************************************
        //* Item Proc
        //********************************************************************
        public static string SQL_GetBankByID(int vBankID) {
            /*
            return(" SELECT " + 
                   "    SiteBankID     AS BankID," + 
                   "    RTrim(BankName) AS BankName," + 
                   "    Address," + 
                   "    City," + 
                   "    State," + 
                   "    Zip" + 
                   " FROM" + 
                   "    dimBanks" + 
                   " WHERE SiteBankID=" + vBankID.ToString() +
                   "    AND MostRecent = 1");
            */
            return("EXEC usp_dimBanks_GetByID @parmSiteBankID = " + vBankID.ToString());                               
        }

        public static string SQL_GetCustomerByID(int vBankID, int vCustomerID) {
            /*
            return(" SELECT SiteCustomerID AS CustomerID, SiteBankID AS BankID, RTrim(Name) AS Name, Description" + 
                   " FROM dimCustomers" + 
                   " WHERE SiteBankID=" + vBankID.ToString() + 
                   " AND SiteCustomerID=" + vCustomerID.ToString() +
                   " AND MostRecent = 1");
            */
            return("EXEC usp_dimCustomers_GetByID" + 
                   "    @parmSiteBankID = " + vBankID.ToString() + ", " +
                   "    @parmSiteCustomerID = " + vCustomerID.ToString());                               
        }

        public static string SQL_GetLockboxByID(int vBankID, int vLockboxID) {
            return("EXEC usp_dimLockboxes_GetByID" + 
                   "    @parmSiteBankID = " + vBankID.ToString() + ", " +
                   "    @parmSiteLockboxID = " + vLockboxID.ToString());
        }

        public static string SQL_GetLockboxByLockboxKey(int LockboxKey) {
            return("EXEC usp_GetLockboxByLockboxKey" + 
                   "    @parmLockboxKey = " + LockboxKey.ToString());
        }

        /*
        public static string SQL_GetLockboxLongName(int BankID, int CustomerID, int LockboxID) {
            return(" SELECT" + 
                   "    RTrim(LongName) AS LongName" + 
                   " FROM dimLockboxes" + 
                   " WHERE" + 
                   "    SiteBankID = " + BankID.ToString() + 
                   "    AND SiteCustomerID = " + CustomerID.ToString() + 
                   "    AND SiteLockboxID = " + LockboxID.ToString());
        }
        */



        //********************************************************************
        //* Permissions
        //********************************************************************
        public static string SQL_GetAllPermissions() {
            /*
            return(" SELECT" + 
                   "    Permissions.PermissionID," + 
                   "    PermissionCategory,PermissionName," + 
                   "    PermissionDefault,ScriptFile," + 
                   "    PermissionMode" + 
                   " FROM Permissions" + 
                   " ORDER BY" + 
                   "    PermissionCategory,PermissionName");
            */
            return("EXEC usp_Permissions_GetAll");
        }

        public static string SQL_GetUserPermissions (int vUserID) {
            // Return all permissions user has.
            /*
            return("SELECT" + 
                   "    Permissions.PermissionID," + 
                   "    PermissionCategory,PermissionName," + 
                   "    PermissionDefault,ScriptFile,PermissionMode" + 
                   " FROM UserPermissions" + 
                   "    INNER JOIN Permissions ON" + 
                   "        Permissions.PermissionID=UserPermissions.PermissionID" + 
                   "        AND UserPermissions.UserID=" + vUserID.ToString() +
                   " WHERE" + 
                   "    Permissions.PermissionCategory <> 'System'" + 
                   " ORDER BY" + 
                   "    PermissionCategory,PermissionName");
            */
            return("EXEC usp_Permissions_GetByUserID" + 
                   "    @parmUserID = " + vUserID.ToString());
        }

        public static string SQL_GetPermissionByID(int vPermissionID) {
            /*
            return(" SELECT *" + 
                   " FROM Permissions" + 
                   " WHERE" + 
                   "    Permissions.PermissionID=" + vPermissionID.ToString());
            */
            return("EXEC usp_Permissions_GetByID" + 
                   "    @parmPermissionID = " + vPermissionID.ToString());
        }

        public static string SQL_GetUserPermissionsByScript(int vUserID, string vScriptFile) {
            // Return all permissions user has.
            /*
            return("SELECT" + 
                   "    Permissions.PermissionID," + 
                   "    PermissionCategory," + 
                   "    PermissionName," + 
                   "    PermissionDefault," + 
                   "    ScriptFile,PermissionMode" + 
                   " FROM UserPermissions" + 
                   "    INNER JOIN Permissions ON" + 
                   "        Permissions.PermissionID=UserPermissions.PermissionID" + 
                   " WHERE UserPermissions.UserID=" + vUserID.ToString() +
                   "    AND lower(Permissions.ScriptFile)='" + vScriptFile.ToLower() + "'");
            */
            return("EXEC usp_UserPermissions_GetByScript" + 
                   "    @parmUserID = " + vUserID.ToString() + ", " +
                   "    @parmScriptFile = '" + vScriptFile + "'");
        }

        public static string SQL_GetPagePermissions(string vScriptFile) {

            // Check if((script requires permission
            /*
            return(" SELECT DISTINCT ScriptFile" + 
                   " FROM Permissions" + 
                   " WHERE lower(ScriptFile)='" + vScriptFile.ToLower() + "'");
            */
            return("EXEC usp_Permissions_GetByPage" + 
                   "    @parmScriptFile = '" + vScriptFile + "'");
        }
            
        /*
        public static string SQL_CheckUserPagePermissions(string vScriptFile, int UserID) {
            // Check if((user has required permission
            return(" SELECT PermissionName" + 
                   " FROM Permissions" + 
                   "    INNER JOIN UserPermissions ON" + 
                   "        Permissions.PermissionID=UserPermissions.PermissionID" + 
                   " WHERE" + 
                   "    lower(Permissions.ScriptFile)='" + vScriptFile.ToLower() + "'" + 
                   "    AND UserPermissions.UserID=" + UserID.ToString());
        }
        */

        public static string SQL_CheckPageModePermissions(string  vScriptFile, string vPermissionMode, int UserID) {
            /*
            return(" SELECT PermissionName" + 
                   " FROM Permissions" + 
                   "    INNER JOIN UserPermissions ON" + 
                   "        Permissions.PermissionID = UserPermissions.PermissionID" + 
                   " WHERE" + 
                   "    lower(Permissions.ScriptFile)='" + vScriptFile.ToLower() + "'" + 
                   "    AND lower(Permissions.PermissionMode)='" + vPermissionMode.ToLower() + "'" + 
                   "    AND UserPermissions.UserID=" + UserID.ToString());
             */

            return("EXEC usp_Permissions_CheckPageMode" + 
                   "    @parmScriptFile = '" + vScriptFile + "', " + 
                   "    @parmPermissionMode = '" + vPermissionMode + "', " + 
                   "    @parmUserID = " + UserID.ToString());
        }

        /*
        public static string SQL_PageModePermissionExists(string vScriptFile,string vPermissionMode, int UserID) {
            return("SELECT PermissionName" + 
                   " FROM Permissions" + 
                   " INNER JOIN UserPermissions ON" + 
                   "    Permissions.PermissionID=UserPermissions.PermissionID" + 
                   " WHERE" + 
                   "    lower(Permissions.ScriptFile)='" + vScriptFile.ToLower() + "'" + 
                   "    AND lower(Permissions.PermissionMode)='" + vPermissionMode.ToLower() + "'" + 
                   "    AND UserPermissions.UserID=" + UserID.ToString());
        }
        */



        //********************************************************************
        //* OLPreferences
        //********************************************************************
        public static string SQL_GetDefaultOLPreference(string PreferenceName) {
            /*
            return(" SELECT DefaultSetting" + 
                   " FROM OLPreferences" + 
                   " WHERE OLPName='" + OLPName + "'");
             */
            return("EXEC usp_Permissions_GetByPage" + 
                   "    @parmPreferenceName = '" + PreferenceName + "'");
        }



        //********************************************************************
        //* Users
        //********************************************************************
        public static string SQL_GetUserByID(int vUserID) {
            return("EXECUTE proc_Users_GetUserByUserID " +
                   "    @parmUserID=" + vUserID.ToString() + ", " +
                   "    @parmRowsReturned=0, " +
                   "    @parmErrorDescription='', " +
                   "    @parmSQLErrorNumber=0");
        }

        public static string SQL_GetUserByLogon(string vLogonName, UserTypes vUserType) {
        
            /*
            StringBuilder sbSQL = new StringBuilder();
        
            sbSQL.Append(" SELECT"); 
            sbSQL.Append("      Users.UserID,"); 
            sbSQL.Append("      Users.LogonName,"); 
            sbSQL.Append("      Users.FirstName,"); 
            sbSQL.Append("      Users.MiddleInitial,"); 
            sbSQL.Append("      Users.LastName,"); 
            sbSQL.Append("      Users.SuperUser,");
            sbSQL.Append("      Users.Password,"); 
            sbSQL.Append("      Users.PasswordSet, ");
            sbSQL.Append("      Users.IsFirstTime,"); 
            sbSQL.Append("      Users.CreationDate,");      //DateTimeCreated
            sbSQL.Append("      Users.CreatedBy,");
            sbSQL.Append("      Users.ModificationDate,");  //DateLastUpdated
            sbSQL.Append("      Users.ModifiedBy,");        //LastUpdatedBy
            sbSQL.Append("      Users.FailedLogonAttempts, ");
            sbSQL.Append("      Users.EmailAddress,"); 
            sbSQL.Append("      Users.IsActive,"); 
            sbSQL.Append("      Users.OLCustomerID,"); 
            sbSQL.Append("      Users.UserType,");
            sbSQL.Append("      Users.ExternalID1,"); 
            sbSQL.Append("      Users.ExternalID2,");
            sbSQL.Append("      OLCustomers.CustomerCode ");
            sbSQL.Append(" FROM Users LEFT JOIN");
            sbSQL.Append("      OLCustomers ON"); 
            sbSQL.Append("          Users.OLCustomerID = OLCustomers.OLCustomerID");
            sbSQL.Append(" WHERE (Users.LogonName = '" + vLogonName + "')");
            sbSQL.Append("    AND Users.UserType=" + ((int)vUserType).ToString());

            if(vUserType == UserTypes.Online) {
                sbSQL.Append("    AND Users.OLCustomerID IS NOT NULL");
            } else {
                sbSQL.Append("    AND Users.OLCustomerID IS NULL");
            }

            return(sbSQL.ToString());
             */
            return("EXEC usp_Users_GetByLogon" + 
                   "    @parmLogonName = '" + vLogonName + "', " + 
                   "    @parmUserType = " + ((int)vUserType).ToString());
        }

        public static string SQL_UserExists(string LogonName, UserTypes UserType, Guid OLCustomerID) {

            string strSQL;

            strSQL = " SELECT Count(*) AS TheCount"
                   + " FROM Users"
                   + " WHERE LogonName='" + LogonName + "'";

            switch(UserType) {
                case UserTypes.Research:
                    strSQL += " AND OLCustomerID IS NULL" + 
                              " AND UserType=" + ((int)UserType).ToString();
                    break;
                case UserTypes.Admin:
                    strSQL += " AND OLCustomerID IS NULL" + 
                              " AND UserType=" + ((int)UserType).ToString();
                    break;
                default:
                    strSQL += " AND OLCustomerID='" + OLCustomerID.ToString() + "'";
                    break;
            }

            return(strSQL);
        }

        /*
        public static string SQL_GetFailedLogonAttmpts(int UserID) {
            return(" SELECT FailedLogonAttempts" + 
                   " FROM Users" + 
                   " WHERE UserID=" + UserID.ToString());
        }
        */

        public static string SQL_UpdateFailedLogonAttempts(int UserID, int FailedLogonAttempts) {
            /*
            return("UPDATE Users SET FailedLogonAttempts=" + LogonAttempts.ToString() + " WHERE UserID=" + UserID.ToString());
            */
            return("EXEC usp_Users_UpdateFailedLogonAttempts" + 
                   "    @parmUserID = " + UserID.ToString() + ", " + 
                   "    @parmFailedLogonAttempts = " + FailedLogonAttempts.ToString());
        }



        //********************************************************************
        //* Session
        //********************************************************************
        public static string SQL_GetActiveSessionByID(Guid SessionID) {
            /*
            return("SELECT SessionID,UserID,LastPageServed FROM Session WHERE SessionID='" + SessionID.ToString() + "'");
            */
            return("EXEC usp_Session_GetByID" + 
                   "    @parmSessionID = '" + SessionID.ToString() + "'");
        }

        public static string SQL_CreateSession(Guid SessionID, int UserID, string RemoteAddr, string UserAgent, int PageCounter, bool IsSuccess) {
            /*
            return("INSERT INTO Session "
                + "(SessionID,UserID,IPAddress,BrowserType,LogonDateTime,LastPageServed,PageCounter,IsSuccess) "
                + " VALUES ('" + SessionID.ToString() + "','" + 
                                 UserID.ToString()  + "','" + 
                                 RemoteAddr + "','" + 
                                 UserAgent + "','" + 
                                 DateTime.Now.ToString() + "','" + 
                                 DateTime.Now.ToString() + "'," + 
                                 PageCounter.ToString() + "," + 
                                 (IsSuccess ? "1" : "0") + ")");
            */
            return("EXEC usp_Session_Ins" +
                   "    @parmSessionID = '" + SessionID.ToString() + "', " + 
                   "    @parmUserID = " + UserID.ToString() + ", " + 
                   "    @parmIPAddress = '" + RemoteAddr + "', " + 
                   "    @parmBrowserType = '" + UserAgent + "', " + 
                   "    @parmPageCounter = " + PageCounter.ToString() + ", " + 
                   "    @parmIsSuccess = " + (IsSuccess ? "1" : "0"));

        }

        public static string SQL_UpdateSession(int NextPage, Guid SessionID) {
            // Update Session record
            /*
            return("UPDATE Session "
                + "SET LastPageServed='" + DateTime.Now.ToString() + "', PageCounter=" + NextPage.ToString()
                + "WHERE SessionID='" + SessionID.ToString() + "'");
            */
            return("EXEC usp_Session_UpdPageCounter" + 
                   "    @parmSessionID = '" + SessionID + "', " + 
                   "    @parmNextPage = " + NextPage.ToString());
        }

        public static string SQL_EndSession(Guid SessionID) {
            /*
            return("UPDATE Session SET IsSessionEnded = 1 WHERE SessionID = '" + SessionID.ToString() + "'");
            */
            return("EXEC usp_Session_EndSession" + 
                   "    @parmSessionID = '" + SessionID.ToString() + "'");
        }

        //********************************************************************
        //** SQL_GetLockboxes
        //********************************************************************
        public static string SQL_GetLockboxes(int BankID) {

            /*
            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(SQL_GetLockboxesFields());
            sbSQL.Append(" FROM dimLockboxes");
            sbSQL.Append("      INNER JOIN SiteCodes ON");
            sbSQL.Append("          dimLockboxes.SiteCode = SiteCodes.SiteCodeID");
            sbSQL.Append(" WHERE dimLockboxes.MostRecent = 1");
            sbSQL.Append("      AND dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append(" ORDER BY");
            sbSQL.Append("      dimLockboxes.SiteBankID ASC,");
            sbSQL.Append("      dimLockboxes.SiteCustomerID ASC,");
            sbSQL.Append("      dimLockboxes.SiteLockboxID ASC");

            return(sbSQL.ToString());
            */
            
            return("EXEC usp_dimLockboxes_GetByBank" + 
                   "    @parmSiteBankID = " + BankID.ToString());
        }

        public static string SQL_GetLockboxes(int BankID, int CustomerID) {

            /*
            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(SQL_GetLockboxesFields());
            sbSQL.Append(" FROM dimLockboxes");
            sbSQL.Append("      INNER JOIN SiteCodes ON");
            sbSQL.Append("          dimLockboxes.SiteCode = SiteCodes.SiteCodeID");
            sbSQL.Append(" WHERE dimLockboxes.MostRecent = 1");
            sbSQL.Append("      AND dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteCustomerID = " + CustomerID.ToString());
            sbSQL.Append(" ORDER BY");
            sbSQL.Append("      dimLockboxes.SiteBankID ASC,");
            sbSQL.Append("      dimLockboxes.SiteCustomerID ASC,");
            sbSQL.Append("      dimLockboxes.SiteLockboxID ASC");

            return(sbSQL.ToString());
            */

            return("EXEC usp_dimLockboxes_GetByCustomer" + 
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +
                   "    @parmSiteCustomerID = " + CustomerID.ToString());
        }

        /*
        public static string SQL_GetLockboxes(int BankID, int CustomerID, int LockboxID) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(SQL_GetLockboxesFields());
            sbSQL.Append(" FROM dimLockboxes");
            sbSQL.Append("      INNER JOIN SiteCodes ON");
            sbSQL.Append("          dimLockboxes.SiteCode = SiteCodes.SiteCodeID");
            sbSQL.Append(" WHERE dimLockboxes.MostRecent = 1");
            sbSQL.Append("      AND dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteCustomerID = " + CustomerID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append(" ORDER BY");
            sbSQL.Append("      dimLockboxes.SiteBankID ASC,");
            sbSQL.Append("      dimLockboxes.SiteCustomerID ASC,");
            sbSQL.Append("      dimLockboxes.SiteLockboxID ASC");

            return(sbSQL.ToString());
        }
        */  

        public static string SQL_GetLockbox(int BankID, int LockboxID) {
            return("EXEC usp_dimLockboxes_GetByID" + 
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +
                   "    @parmSiteLockboxID = " + LockboxID.ToString());
        }

        /*
        private static StringBuilder SQL_GetLockboxesFields() {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT");
            sbSQL.Append("      dimLockboxes.SiteCode,");
            sbSQL.Append("      dimLockboxes.SiteBankID          AS BankID,");
            sbSQL.Append("      dimLockboxes.SiteCustomerID      AS CustomerID,");
            sbSQL.Append("      dimLockboxes.SiteLockboxID       AS LockboxID,");
            sbSQL.Append("      RTrim(dimLockboxes.ShortName) AS ShortName,");
            sbSQL.Append("      RTrim(dimLockboxes.LongName) AS LongName,");
            sbSQL.Append("      dimLockboxes.DDA,");
            sbSQL.Append("      dimLockboxes.OnlineColorMode,");
            sbSQL.Append("      dimLockboxes.CutOff,");
            sbSQL.Append("      dimLockboxes.IsActive,");
            sbSQL.Append("      dimSiteCodes.CurrentProcessingDate");

            return(sbSQL);
        }
        */
        //********************************************************************
        //** 
        //********************************************************************


        public static string SQL_GetLockboxTotal(int BankID, int LockboxID, DateTime DepositDate, int CSRDepositStatus) {

            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT    COALESCE(SUM(OLTA.factBatchSummary.CheckTotal), 0) AS DepositTotal");
            sbSQL.Append(" FROM    OLTA.factBatchSummary");
            sbSQL.Append("      INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE      OLTA.dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND OLTA.dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositDateKey >= " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositDateKey <= " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());

            return(sbSQL.ToString());
            */

            return("EXEC usp_GetLockboxTotal" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +  
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " + 
                   "    @parmDepositDateStart = '" + DepositDate.ToString("yyyyMMdd") + "', " + 
                   "    @parmDepositDateEnd = '" + DepositDate.ToString("yyyyMMdd") + "', " + 
                   "    @parmMinDepositStatus = " + CSRDepositStatus);
        }

        public static string SQL_GetBatchCount(int BankID, int LockboxID, DateTime DepositDate, int CSRDepositStatus) {

            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT    COUNT(OLTA.factBatchSummary.GlobalBatchID) AS BatchCount");
            sbSQL.Append(" FROM    OLTA.factBatchSummary");
            sbSQL.Append("      INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE    OLTA.dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND OLTA.dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositDateKey >= " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositDateKey <= " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());

            return(sbSQL.ToString());
            */

            return("EXEC usp_GetBatchCount" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +  
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " + 
                   "    @parmDepositDateStart = '" + DepositDate.ToString("yyyyMMdd") + "', " + 
                   "    @parmDepositDateEnd = '" + DepositDate.ToString("yyyyMMdd") + "', " + 
                   "    @parmMinDepositStatus = " + CSRDepositStatus);
        }

        public static string SQL_GetTransactionCount(int BankID, int LockboxID, DateTime DepositDate, int CSRDepositStatus) {
                
            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT    COALESCE(SUM(OLTA.factBatchSummary.TransactionCount), 0) AS TransactionCount");
            sbSQL.Append(" FROM    OLTA.factBatchSummary");
            sbSQL.Append("      INNER JOIN OLTA.dimLockboxes ON OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE    OLTA.dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND OLTA.dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND OLTA.factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());

            return(sbSQL.ToString());
            */

            return("EXEC usp_GetTransactionCount" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +  
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " + 
                   "    @parmDepositDateStart = '" + DepositDate.ToString("MM/dd/yyyy") + "', " + 
                   "    @parmDepositDateEnd = '" + DepositDate.ToString("MM/dd/yyyy") + "', " + 
                   "    @parmMinDepositStatus = " + CSRDepositStatus);
        }

        public static string SQL_ContainsBatchesNotReleased(int BankID, int LockboxID, DateTime DepositDate) {

            //string strSQL;

            ////strSQL = "SELECT COUNT(Batch.GlobalBatchID) AS BatchCount"
            ////       + " FROM Batch"
            ////       + " WHERE Batch.BankID = " + BankID
            ////       + " AND Batch.LockboxID = " + LockboxID
            ////       + " AND CONVERT(VARCHAR(10), Batch.DepositDate, 101) = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
            ////       + " AND Batch.DepositStatus = 825";

            //strSQL = string.Empty;

            //return(strSQL);
            return("EXEC usp_GetBatchCountOfNotReleased" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +  
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " + 
                   "    @parmDepositDateStart = '" + DepositDate.ToString("MM/dd/yyyy") + "', " + 
                   "    @parmDepositDateEnd = '" + DepositDate.ToString("MM/dd/yyyy") + "'");
        }

        /*---------------------------------------------------------------
        'PURPOSE:   Get an ADODB Recordset object of BatchSummary
        '           Information for a given lockbox and depositdate
        'AUTHOR:    Eric Goetzinger
        'REVISIONS:
        '---------------------------------------------------------------*/
        public static string SQL_GetBatchSummary(int BankID,
                                                 int LockboxID,
                                                 DateTime DepositDate,
                                                 bool DisplayScannedCheck,
                                                 int CSRDepositStatus) {
            
            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT");
            sbSQL.Append("     dimLockboxes.SiteBankID          AS BankID,");
            sbSQL.Append("     dimLockboxes.SiteCustomerID      AS CustomerID,");
            sbSQL.Append("     dimLockboxes.SiteLockboxID       AS LockboxID,");
            sbSQL.Append("     factBatchSummary.BatchID,");
            sbSQL.Append("     dimDepositDates.CalendarDate     AS DepositDate,");
            sbSQL.Append("     factBatchSummary.CheckTotal,");
            ////sbSQL.Append(" --    factBatchSummary.DepositDDA, ");
            sbSQL.Append("     factBatchSummary.TransactionCount,");
            sbSQL.Append("     factBatchSummary.CheckCount,"); 

            if(DisplayScannedCheck) {
                sbSQL.Append("     factBatchSummary.DocumentCount,");
            } else {
                sbSQL.Append("     factBatchSummary.DocumentCount - factBatchSummary.ScannedCheckCount        AS DocumentCount,");
            }

            sbSQL.Append(" factBatchSummary.DepositStatusDisplayName    AS DepositStatusDisplay");
            sbSQL.Append(" FROM factBatchSummary");
            sbSQL.Append("      INNER JOIN dimLockboxes ON");
            sbSQL.Append("          factBatchSummary.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append("      INNER JOIN dimDates AS dimDepositDates ON");
            sbSQL.Append("          factBatchSummary.DepositDateKey = dimDepositDates.DateKey");
            sbSQL.Append(" WHERE dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND factBatchSummary.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("     factBatchSummary.BatchID ASC");
                
            return(sbSQL.ToString());
            */
            
            return("EXEC usp_Research_GetBatchSummary" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "', " +
                   "    @parmMinDepositStatus = " + CSRDepositStatus.ToString() + ", " +
                   "    @parmDisplayScannedCheck = " + DisplayScannedCheck.ToString());
        }

        /*
        public static string SQL_GetCheckCount(int BankID, 
                                               int LockboxID, 
                                               DateTime DepositDate, 
                                               int CSRDepositStatus) {
            
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT SUM(COALESCE(factBatchSummary.CheckCount,0)) AS CheckCount");
            sbSQL.Append(" FROM factBatchSummary");
            sbSQL.Append("      INNER JOIN dimLockboxes ON");
            sbSQL.Append("          factBatchSummary.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND factBatchSummary.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());
                
            return(sbSQL.ToString());
        }

        public static string SQL_GetDocumentCount(int BankID, 
                                                  int LockboxID, 
                                                  DateTime DepositDate, 
                                                  int CSRDepositStatus, 
                                                  bool DisplayScannedCheck) {

            StringBuilder sbSQL = new StringBuilder();

            if(DisplayScannedCheck) {
                sbSQL.Append("  factBatchSummary.DocumentCount");
            } else {
                sbSQL.Append("     factBatchSummary.DocumentCount - factBatchSummary.ScannedCheckCount        AS DocumentCount");
            }

            sbSQL.Append(" FROM factBatchSummary");
            sbSQL.Append("      INNER JOIN dimLockboxes ON");
            sbSQL.Append("          factBatchSummary.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("      AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("         AND factBatchSummary.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("         AND factBatchSummary.DepositStatus >= " + CSRDepositStatus.ToString());
                
            return(sbSQL.ToString());
        }
        */

        //'---------------------------------------------------------------
        //'PURPOSE:   Get an adodb recordset of users and information for
        //'           a bank and customer
        //'AUTHOR:    Eric Goetzinger
        //'REVISIONS:
        //'---------------------------------------------------------------
        public static string SQL_GetOnlineUsers(Guid OLCustomerID) {

            /*
            string strSQL;
            
            strSQL = "";
            strSQL += " SELECT";
            strSQL += "     OLCustomers.CustomerCode,";
            strSQL += "     Users.UserType,";
            strSQL += "     Users.UserID,";
            strSQL += "     Users.LogonName,";
            strSQL += "     Users.IsActive,";
            strSQL += "     OLCustomers.CustomerCode,";
            strSQL += "     Users.UserType AS Expr2,";
            strSQL += "     Users.LastName,";
            strSQL += "     Users.FirstName,";
            strSQL += "     Users.EmailAddress,";
            strSQL += "     Users.ModificationDate  AS DateLastUpdated,";
            strSQL += "     MAX([Session].LastPageServed) AS LastPageServed";
            strSQL += " FROM Users";
            strSQL += "     LEFT OUTER JOIN [Session] ON";
            strSQL += "         Users.UserID = [Session].UserID";
            strSQL += "     LEFT OUTER JOIN OLCustomers ON";
            strSQL += "         Users.OLCustomerID = OLCustomers.OLCustomerID";
            strSQL += " WHERE";
            strSQL += "     Users.OLCustomerID='" + OLCustomerID.ToString() + "'";
            strSQL += "     AND Users.UserType=" + ((int)UserTypes.Online).ToString();
            strSQL += " GROUP BY";
            strSQL += "     Users.UserID,";
            strSQL += "     Users.LogonName,";
            strSQL += "     Users.FirstName,";
            strSQL += "     Users.LastName,";
            strSQL += "     Users.ModificationDate,";
            strSQL += "     Users.EmailAddress,";
            strSQL += "     OLCustomers.CustomerCode,";
            strSQL += "     Users.UserType,";
            strSQL += "     Users.IsActive,";
            strSQL += "     OLCustomers.CustomerCode";
            strSQL += " ORDER BY";
            strSQL += "     Users.LogonName";

            return(strSQL);
            */

            return("EXEC usp_Users_GetByOLCustomerID" + 
                   "    @parmOLCustomerID = '" + OLCustomerID + "'");
        }

        /*
        public static string SQL_GetUser(int UserID) {

            string strSQL;

            strSQL = " SELECT UserID," + 
                     "      LogonName," + 
                     "      FirstName," + 
                     "      MiddleInitial," + 
                     "      LastName," + 
                     "      SuperUser," + 
                     "      Password," + 
                     "      PasswordSet," + 
                     "      IsFirstTime," + 
                     "      Users.CreationDate," + 
                     "      Users.CreatedBy," +
                     "      Users.ModificationDate," +
                     "      Users.ModifiedBy," +
                     "      FailedLogonAttempts," + 
                     "      EmailAddress," + 
                     "      IsActive," + 
                     "      OLCustomerID," + 
                     "      UserType," + 
                     "      ExternalID1," + 
                     "      ExternalID2" +
                     " FROM Users" + 
                     " WHERE UserID=" + UserID.ToString();

            return(strSQL);
        }
        */

        public static string SQL_GetCustomerContacts(int BankID, int CustomerID) {

            string strSQL;

            strSQL = "SELECT RTrim(ContactID) AS ContactID,"
                    + " RTrim(ContactName) AS ContactName,"
                    + " RTrim(Address) AS Address,"
                    + " RTrim(City) AS City,"
                    + " RTrim(State) AS State,"
                    + " RTrim(Zip) AS Zip,"
                    + " RTrim(Voice) AS Voice,"
                    + " RTrim(Fax) AS Fax,"
                    + " RTrim(Email) AS Email,"
                    + " IsActive, IsPrimary"
                    + " FROM Contacts WHERE BankID = " + BankID.ToString()
                    + " AND CustomerID = " + CustomerID.ToString()
                    + " AND LockboxID = -1"
                    + " ORDER BY ContactName, ContactID ASC";
                    
            return(strSQL);
        }

        public static string SQL_GetLockboxContacts(int BankID, int CustomerID, int LockboxID) {
            
            string strSQL;
            
            strSQL = "SELECT RTrim(ContactID) AS ContactID,"
                    + " RTrim(ContactName) AS ContactName,"
                    + " RTrim(Address) AS Address,"
                    + " RTrim(City) AS City,"
                    + " RTrim(State) AS State,"
                    + " RTrim(Zip) AS Zip,"
                    + " RTrim(Voice) AS Voice,"
                    + " RTrim(Fax) AS Fax,"
                    + " RTrim(Email) AS Email,"
                    + " IsActive, IsPrimary"
                    + " FROM Contacts WHERE BankID = " + BankID.ToString()
                    + " AND CustomerID = " + CustomerID.ToString()
                    + " AND LockboxID = " + LockboxID.ToString()
                    + " ORDER BY ContactName, ContactID ASC";
                    
            return(strSQL);
        }

        public static string SQL_GetContactByID(Guid ContactID) {

            string strSQL;
            
            strSQL = "SELECT RTrim(ContactID) AS ContactID," +
                     " BankID," +
                     " CustomerID," +
                     " LockboxID," +
                     " RTrim(ContactName) AS ContactName," +
                     " RTrim(Address) AS Address," +
                     " RTrim(City) AS City," +
                     " RTrim(State) AS State," +
                     " RTrim(Zip) AS Zip," +
                     " RTrim(Voice) AS Voice," +
                     " RTrim(Fax) AS Fax," +
                     " RTrim(Email) AS Email," +
                     " ContactPIN, " +
                     " RTrim(ContactPassword) AS ContactPassword," +
                     " CONVERT(varchar, ModificationDate, 110) AS Modified," +
                     " IsActive, IsPrimary" +
                     " FROM Contacts WHERE ContactID='" + ContactID.ToString() + "'";

            return(strSQL);

        }

        /*
        public static string SQL_GetCustomerProfile(int BankID, int CustomerID) {

            string strSQL;

            strSQL = string.Empty;
            strSQL += "SELECT Bank.BankID, Bank.BankName, Customer.CustomerID, Customer.Name,";
            strSQL += " Customer.IsMagnet, ";
            strSQL += " Customer.Description";
            strSQL += " FROM Bank INNER JOIN Customer ON Customer.BankID = Bank.BankID";
            strSQL += " WHERE Bank.BankID = " + BankID.ToString();
            strSQL += " AND Customer.CustomerID = " + CustomerID.ToString();

            return(strSQL);
        }
        */

        public static string SQL_GetTransactionDetails(int BankID, 
                                                       int LockboxID, 
                                                       int BatchID, 
                                                       DateTime DepositDate, 
                                                       int TransactionID,
                                                       int CSRDepositStatus) {

            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     factTransactionSummary.BatchID, ");
            sbSQL.Append("     dimDepositDates.CalendarDate    AS DepositDate,");
            sbSQL.Append("     factTransactionSummary.DepositStatus, ");
            sbSQL.Append("     factTransactionSummary.TxnSequence,");
            sbSQL.Append("     (SELECT StatusDisplayName ");
            sbSQL.Append("      FROM StatusTypes ");
            sbSQL.Append("      WHERE ");
            sbSQL.Append("         StatusType = 'Deposit' ");
            sbSQL.Append("         AND StatusValue = factTransactionSummary.DepositStatus) AS DepositStatusDisplay ");
            sbSQL.Append(" FROM factTransactionSummary ");
            sbSQL.Append("     INNER JOIN dimDates AS dimDepositDates ON ");
            sbSQL.Append("         factTransactionSummary.DepositDateKey = dimDepositDates.DateKey");
            sbSQL.Append("     INNER JOIN dimLockboxes ON");
            sbSQL.Append("         factTransactionSummary.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE ");
            sbSQL.Append("     dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("     AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("     AND factTransactionSummary.BatchID = " + BatchID.ToString());
            sbSQL.Append("     AND factTransactionSummary.TransactionID = " + TransactionID.ToString());
            sbSQL.Append("     AND factTransactionSummary.DepositStatus >= " + CSRDepositStatus.ToString());

            return(sbSQL.ToString());
            */
            
            return("usp_Research_GetTransactionDetail" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + "," +
                   "    @parmBatchID = " + BatchID.ToString() + "," +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'," +
                   "    @parmTransactionID = " + TransactionID.ToString() + "," +
                   "    @parmMinDepositStatus = " + CSRDepositStatus.ToString());
        }

        public static string SQL_GetChecksData(int BankID, 
                                               int LockboxID, 
                                               int BatchID, 
                                               DateTime DepositDate, 
                                               int TransactionID) {

            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT factChecks.BatchID,");
            sbSQL.Append("     Convert(Char(8), factChecks.ProcessingDateKey)    AS PICSDate,");
            //sbSQL.Append("     --factChecks.DESetupID,");
            sbSQL.Append("     RTrim(dimRemitters.RoutingNumber)                AS RT,");
            sbSQL.Append("     factChecks.Amount,");
            sbSQL.Append("     RTrim(factChecks.Serial)                        AS Serial,");
            sbSQL.Append("     RTrim(dimRemitters.Account)                        AS Account,");
            sbSQL.Append("     RTrim(dimRemitters.RemitterName)                AS RemitterName,");
            sbSQL.Append("     factChecks.BatchSequence,");
            sbSQL.Append("     factChecks.SequenceWithinTransaction            AS TransactionSequence");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     factChecks ");
            sbSQL.Append("         INNER JOIN dimLockboxes ON ");
            sbSQL.Append("             factChecks.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append("         INNER JOIN dimRemitters ON ");
            sbSQL.Append("             factChecks.RemitterKey = dimRemitters.RemitterKey");
            sbSQL.Append(" WHERE dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("     AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("     AND factChecks.BatchID = " + BatchID.ToString());
            sbSQL.Append("     AND factChecks.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("     AND factChecks.TransactionID = " + TransactionID.ToString());
            sbSQL.Append(" ORDER BY factChecks.SequenceWithinTransaction ASC");

            return(sbSQL.ToString());
            */
            
            return("EXEC usp_GetTransactionChecks" +
                   "    @parmSiteBankID = " + BankID.ToString() + ", " +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + ", " +
                   "    @parmBatchID = " + BatchID.ToString() + ", " +
                   "     @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "', " +
                   "    @parmTransactionID = " + TransactionID.ToString());
        }


        public static string SQL_GetPayees(int BankID, int CustomerID, int LockboxID) {

            /*
            string SQL;

            SQL = ""
                + " SELECT "
                + "     p.PayeeID"
                + "   , p.IsValid"
                + "   , IsNull(p.PayeeName, '')                   AS PayeeName"
                + "   , p.BankID"
                + "   , p.CustomerID"
                + "   , p.LockboxID"
                + "   , CAST(p.LockboxID AS VARCHAR) + ' - ' + l.LongName         AS LockboxDescription"
                + "   , CASE WHEN p.IsValid=0 THEN 'Invalid' ELSE 'Valid' END     AS PayeeStatus"
                + " FROM Payee AS p INNER JOIN Lockbox AS l ON "
                + "       p.BankID=l.BankID "
                + "   AND p.CustomerID=l.CustomerID "
                + "   AND p.LockboxID=l.LockboxID"
                + " WHERE p.BankID=" + BankID.ToString()
                + "   AND p.CustomerID=" + CustomerID.ToString();
            
            if(LockboxID > 0) {
                SQL += "   AND p.LockboxID=" + LockboxID.ToString();
            } else {
                SQL += "   AND p.LockboxID > 0";
            }
            
            SQL += " ORDER BY p.LockboxID, p.IsValid, p.PayeeName";

            return(SQL);
            */

            if(LockboxID > 0) {
                return("EXEC usp_Payee_GetByCustomer" + 
                       "    @parmSiteBankID = " + BankID.ToString() + "," + 
                       "    @parmSiteCustomerID = " + CustomerID.ToString());
            } else {
                return("EXEC usp_Payee_GetByLockbox" + 
                       "    @parmSiteBankID = " + BankID.ToString() + "," + 
                       "    @parmSiteLockboxID = " + LockboxID.ToString());
            }
        }

        public static string SQL_GetBatchChecks(int BankID, 
                                                int LockboxID, 
                                                int BatchID, 
                                                DateTime DepositDate,
                                                int CSRDepositStatus) {

            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     Convert(Char(8), factChecks.ProcessingDateKey) AS PICSDate,");
            sbSQL.Append("     dimLockboxes.SiteBankID        AS BankID, ");
            sbSQL.Append("     dimLockboxes.SiteLockboxID     AS LockboxID, ");
            sbSQL.Append("     dimLockboxes.SiteCustomerID    AS CustomerID, ");
            sbSQL.Append("     factChecks.BatchID, ");
            sbSQL.Append("     dimDepositDates.CalendarDate     AS DepositDate, ");
            //sbSQL.Append("     factChecks.Amount AS BatchTotal,");
            //sbSQL.Append("     --factChecks.DESetupID, ");
            sbSQL.Append("     factChecks.TransactionID,");
            sbSQL.Append("     factChecks.TxnSequence, ");
            sbSQL.Append("     factChecks.BatchSequence,");
            sbSQL.Append("     RTRIM(factChecks.Serial) AS Serial, ");
            sbSQL.Append("     factChecks.Amount,");
            sbSQL.Append("     RTRIM(dimRemitters.RoutingNumber) AS RT, ");
            sbSQL.Append("     RTRIM(dimRemitters.Account) AS Account,");
            sbSQL.Append("     RTRIM(dimRemitters.RemitterName) AS RemitterName,");

            //sbSQL.Append("     --RTRIM(factChecks.GlobalCheckID) AS GlobalCheckID,");

            sbSQL.Append("     (SELECT StatusDisplayName FROM StatusTypes WHERE StatusType = 'Deposit' AND StatusValue = factChecks.DepositStatus) AS DepositStatusDisplay");
            sbSQL.Append(" FROM factChecks ");
            sbSQL.Append("     INNER JOIN dimLockboxes ON ");
            sbSQL.Append("         factChecks.LockboxKey = dimLockboxes.LockboxKey");
            sbSQL.Append("     INNER JOIN dimDates AS dimDepositDates ON ");
            sbSQL.Append("         factChecks.DepositDateKey = dimDepositDates.DateKey");
            sbSQL.Append("     INNER JOIN dimRemitters ON ");
            sbSQL.Append("         factChecks.RemitterKey = dimRemitters.RemitterKey");
            sbSQL.Append(" WHERE dimLockboxes.SiteBankID = " + BankID.ToString());
            ////sbSQL.Append("     AND dimLockboxes.SiteCustomerID = " + CustomerID.ToString());
            sbSQL.Append("     AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            if(BatchID>0){sbSQL.Append("     AND factChecks.BatchID = " + BatchID.ToString());}
            sbSQL.Append("     AND factChecks.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("     AND factChecks.DepositStatus >= " + CSRDepositStatus.ToString());
            sbSQL.Append(" ORDER BY ");
            sbSQL.Append("     factChecks.ProcessingDateKey ASC, ");
            sbSQL.Append("     factChecks.BatchID ASC, ");
            sbSQL.Append("     factChecks.TxnSequence ASC, ");
            sbSQL.Append("     factChecks.SequenceWithinTransaction ASC");

            return(sbSQL.ToString());
            */
            
            return("EXEC usp_Research_GetBatchChecks" +
                   "	@parmSiteBankID = " + BankID.ToString() + "," +
                   "	@parmSiteLockboxID = " + LockboxID.ToString() + "," +
                   "	@parmBatchID = " + BatchID.ToString() + "," +
                   "	@parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'," +
                   "	@parmMinDepositStatus = " + CSRDepositStatus.ToString());
        }

        public static string SQL_GetInstructionsList(string strLevel, int iBankID, int iCustomerID, int iLockboxID){
            /*
            string strSQL = "";
            string SQL_Where = "";
            if(strLevel.Length == 0){
                SQL_Where = "";   
            }else if(strLevel.ToLower() == "customer"){
               SQL_Where = "WHERE InstructionsText.BankID = " + iBankID.ToString() + 
                           " AND InstructionsText.CustomerID = " + iCustomerID.ToString();
            }else if(strLevel.ToLower() == "lockbox"){ 
                SQL_Where = "WHERE InstructionsText.BankID = " + iBankID.ToString() + 
                           " AND InstructionsText.CustomerID = " + iCustomerID.ToString() +
                           " AND InstructionsText.LockboxID = " + iLockboxID.ToString();
            }
            strSQL = "SELECT InstructionsText.BankID,"
            + " InstructionsText.CustomerID,"
            + " InstructionsText.LockboxID,"
            + " InstructionsText.Type,"
            + " ISNULL(RTRIM(InstructionTypes.Description), 'Not Defined') AS InstructionType,"
            + " RTRIM(InstructionsText.Description) AS InstructionsDescription,"
            + " RTRIM(InstructionsText.Tag) AS Tag,"
            + " MAX(InstructionsText.AsOf) AS AsOf, "
            + "'instructions_viewer.aspx?bank='+CAST(InstructionsText.BankID AS Varchar(20))+'&customer='+CAST(InstructionsText.CustomerID AS Varchar(20))+'&lockbox='+CAST(InstructionsText.LockboxID AS Varchar(20))+'&type='+CAST(InstructionsText.Type AS Varchar(20))+'&tag='+RTRIM(InstructionsText.Tag)+'&asof='+CONVERT(varchar,MAX(InstructionsText.AsOf),101) AS linkurl"
            + " FROM InstructionsText"
            + " LEFT OUTER JOIN InstructionTypes ON InstructionsText.Type = InstructionTypes.InstructionTypeID"
            + " " + SQL_Where
            + " GROUP BY BankID, CustomerID, LockboxID, Type, InstructionTypes.Description, InstructionsText.Description, Tag"
            + " ORDER BY BankID ASC, CustomerID ASC, LockboxID ASC, Type ASC, Tag ASC";

            return(strSQL);
            */

            if(strLevel.ToLower() == "customer") {
                return("EXEC usp_InstructionsList_GetByCustomer" + 
                       "    @parmSiteBankID = " + iBankID.ToString() + ", " +
                       "    @parmSiteCustomerID = " + iCustomerID.ToString());
            } else if(strLevel.ToLower() == "lockbox") { 
                return("EXEC usp_InstructionsList_GetByLockbox" + 
                       "    @parmSiteBankID = " + iBankID.ToString() + ", " + 
                       "    @parmSiteCustomerID = " + iCustomerID.ToString() + ", " + 
                       "    @parmSiteLockboxID = " + iLockboxID.ToString());
            } else {
                return("EXEC usp_InstructionsList_GetAll");
            }
        }

        public static string SQL_GetInstructions(int iBankID, int iCustomerID, int iLockboxID, int iType, string strTag, DateTime AsOf){
            /*
            string strSQL = "";
            strSQL = "SELECT InstructionsText.Instructions AS Instructions," +
             " ISNULL(RTRIM(InstructionsText.Description), 'Not Defined') AS Description," +
             " ISNULL(RTRIM(InstructionTypes.Description), 'Not Defined') AS InstructionType" +
             " FROM InstructionsText" +
             " LEFT OUTER JOIN InstructionTypes ON InstructionsText.Type = InstructionTypes.InstructionTypeID" +
             " WHERE BankID="+iBankID.ToString() +
             " AND CustomerID="+iCustomerID.ToString() +
             " AND LockboxID="+iLockboxID.ToString() +
             " AND Type="+iType.ToString() +
             " AND Tag='"+strTag+"'" +
             " AND AsOf='"+strAsOf+"'";

            return(strSQL);
            */

            return("EXEC usp_Instructions_GetByKey" + 
                   "    @parmSiteBankID = " + iBankID.ToString() + ", " +
                   "    @parmSiteCustomerID = " + iCustomerID.ToString() + ", " +
                   "    @parmSiteLockboxID = " + iLockboxID.ToString() + ", " +
                   "    @parmType = " + iType.ToString() + ", " +
                   "    @parmTag = '" + strTag + "', " +
                   "    @parmAsOf = '" + AsOf.ToString("MM/dd/yyyy") + "'");
            
        }
        public static string SQL_GetLastSystemAccess(int iBankID, int iCustomerID){
            /*
            string strSQL = "";
            strSQL = "SELECT TOP 1 dbo.Users.UserID, dbo.Users.Logonname,"+
                " MAX(Session.LastPageServed) AS LastPageServed"+
                " FROM dbo.Users"+
                " INNER JOIN Session ON dbo.Users.UserID = Session.UserID"+
                " WHERE dbo.Users.BankID="+iBankID.ToString() +
                " AND dbo.Users.CustomerID="+iCustomerID.ToString()+
                " AND Session.IsSuccess = 1"+
                " GROUP BY dbo.Users.UserID, dbo.Users.Logonname"+
                " ORDER BY LastPageServed DESC";
            return(strSQL);
            */
            
            return("EXEC usp_Users_GetLastSystemAccess" +
                   "    @parmSiteBankID = " + iBankID.ToString() + ", " +
                   "    @parmSiteCustomerID = " + iCustomerID.ToString());
        }

        public static string SQL_GetAllBatches(int iBankID, int iCustomerID, DateTime DepositDate){
            /*
            string strSQL = "";
            strSQL = "SELECT Batch.LockboxID,"+
                " Batch.GlobalBatchID,"+
                " Batch.BatchID,"+
                " Batch.ProcessingDate,"+
                " Batch.DepositStatus,"+
                " Batch.DEStatus,"+
                " Batch.ScanStatus,"+
                " Batch.ModificationDate,"+
                " StatA.StatusDisplayName AS DepositStatusDisplay,"+
                " COALESCE(StatB.StatusDisplayName,'Not Set') AS DataEntryStatusDisplay,"+
                " COALESCE(StatC.StatusDisplayName,'Not Set') AS ScanStatusDisplay"+
                " FROM Batch "+
                " INNER JOIN Lockbox ON Lockbox.BankID =Batch.BankID AND Lockbox.LockboxID = Batch.LockboxID"+
                " LEFT JOIN StatusTypes StatA ON StatA.StatusValue = Batch.DepositStatus"+
                " LEFT JOIN StatusTypes StatB ON StatB.StatusValue = Batch.DEStatus"+
                " LEFT JOIN StatusTypes StatC ON StatC.StatusValue = Batch.ScanStatus"+
                " WHERE Batch.BankID ="+iBankID.ToString()+
                " AND Lockbox.CustomerID ="+iCustomerID.ToString()+
                " AND Convert(varchar,Batch.DepositDate,101) ='"+strDepositDate+"'"+
                " AND StatA.StatusType = 'Deposit'"+
                " AND StatB.StatusType = 'DataEntry'"+
                " AND StatC.StatusType = 'Scan'"+
                " ORDER BY Batch.LockboxID, Batch.BatchID, Batch.ProcessingDate ASC";
            return(strSQL);
            */
            return("EXEC usp_Batch_GetByCustomer" +
                   "    @parmSiteBankID = " + iBankID.ToString() + ", " +
                   "    @parmSiteCustomerID = " + iCustomerID.ToString() + ", " +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'");

        }
        public static string SQL_GetEventRules(string strLevel, int iBankID, int iCustomerID, int iLockboxID){
            /*
            string strSQL = "";
            string SQL_Where = "";
            if(strLevel.Length == 0){
                SQL_Where = "";   
            }else if(strLevel.ToLower() == "customer"){
               SQL_Where = "WHERE EventRules.BankID = " + iBankID.ToString() + 
                           " AND EventRules.CustomerID = " + iCustomerID.ToString();
            }else if(strLevel.ToLower() == "lockbox"){ 
                SQL_Where = "WHERE EventRules.BankID = " + iBankID.ToString() + 
                           " AND EventRules.CustomerID = " + iCustomerID.ToString() +
                           " AND (EventRules.LockboxID = -1 OR EventRules.LockboxID = " + iLockboxID.ToString()+")";
            };
            strSQL = "SELECT EventRules.EventRuleID,"+
                " EventRules.EventRuleDescription,"+
                " EventRules.LockboxID,"+
                " CASE EventRules.EventOperator "+
                    " WHEN '=' THEN ' Equals '"+
                    " WHEN '!=' THEN ' Is Not Equal To '"+
                    " WHEN '<' THEN ' Is Less Than '"+
                    " WHEN '>' THEN ' Is Greater Than '"+
                    " ELSE ' '"+
                "  END AS EventOperator,"+
                " EventRules.EventRuleValue,"+
                " Replace(Replace(CAST(EventRules.EventAction AS nvarchar(4000)),'<',''),'>',' - ') as EventAction,"+
                " EventRules.OnlineViewable,"+
                " Events.EventName,"+
                " EventArguments.EventArgumentDescription"+
                " FROM EventRules "+
                " INNER JOIN Events ON EventRules.EventID = Events.EventID"+
                " INNER JOIN EventArguments ON EventArguments.EventArgumentCode = EventRules.EventArgumentCode AND EventArguments.EventID = EventRules.EventID"+
                " "+SQL_Where+
                " AND EventRules.RuleIsActive = 1"+
                " ORDER BY EventRules.EventRuleDescription, EventRules.LockboxID, EventRules.EventRuleID ASC";
            return(strSQL);
            */
            
            if(strLevel.ToLower() == "customer"){
                return("EXEC usp_EventRules_GetByCustomer" +
                       "    @parmSiteBankID = " + iBankID.ToString() + ", " + 
                       "    @parmSiteCustomerID = " + iCustomerID.ToString());
            }else if(strLevel.ToLower() == "lockbox") { 
                return("EXEC usp_EventRules_GetByLockbox" +
                       "    @parmSiteBankID = " + iBankID.ToString() + ", " + 
                       "    @parmSiteCustomerID = " + iCustomerID.ToString() + ", " + 
                       "    @parmSiteLockboxID = " + iLockboxID.ToString());
            } else {
                return(string.Empty);
            }
            
        }
        public static string SQL_GetEventRuleByID(Guid gidEventRuleID){
            /*
            string strSQL = "";
            strSQL = "SELECT EventRules.EventRuleID,"+
                " EventRules.EventRuleDescription,"+
                " EventRules.BankID,"+
                " EventRules.CustomerID,"+
                " EventRules.LockboxID,"+
                " CASE EventRules.EventOperator "+
                    " WHEN '=' THEN ' Equals '"+
                    " WHEN '!=' THEN ' Is Not Equal To '"+
                    " WHEN '<' THEN ' Is Less Than '"+
                    " WHEN '>' THEN ' Is Greater Than '"+
                    " ELSE ' '"+
                "  END AS EventOperator,"+
                " EventRules.EventRuleValue,"+
                " Replace(Replace(CAST(EventRules.EventAction AS nvarchar(4000)),'<',''),'>',' - ') as EventAction,"+
                " EventRules.OnlineViewable,"+
                " Events.EventName,"+
                " EventArguments.EventArgumentDescription"+
                " FROM EventRules "+
                " INNER JOIN Events ON EventRules.EventID = Events.EventID"+
                " INNER JOIN EventArguments ON EventArguments.EventArgumentCode = EventRules.EventArgumentCode AND EventArguments.EventID = EventRules.EventID"+
                " WHERE EventRules.EventRuleID = '" + gidEventRuleID.ToString() + "'";
            return(strSQL);
            */
            return("EXEC usp_EventRules_GetByID" + 
                   "    @parmEventRuleID = '" + gidEventRuleID.ToString() + "'");
        }
        public static string SQL_GetAlerts(Guid gidEventRuleID){
            /*
            string strSQL = "";
            strSQL = "SELECT RTRIM(Contacts.ContactName) AS ContactName,"+
                     " Contacts.BankID,"+
                     " Contacts.CustomerID,"+
                     " Contacts.LockboxID,"+
                     " RTRIM(Contacts.Fax) AS Fax,"+
                     " RTRIM(Contacts.Email) AS Email,"+
                     " DeliveryMethods.DeliveryMethodName"+
                     " FROM Alerts "+
                     " INNER JOIN Contacts ON Contacts.ContactID = Alerts.ContactID"+
                     " INNER JOIN DeliveryMethods ON DeliveryMethods.DeliveryMethodID = Alerts.DeliveryMethodID"+
                     " WHERE Alerts.EventRuleID = '" + gidEventRuleID.ToString() + "'" +
                     " ORDER BY Contacts.ContactName, DeliveryMethods.DeliveryMethodName ASC";
            return(strSQL);
            */
            
            return("EXEC usp_AlertsGetByID @parmEventRuleID = '" + gidEventRuleID.ToString() + "'");
        }

        public static string SQL_GetDocuments(int iBankID, 
                                                int iLockboxID, 
                                                int iBatchID, 
                                                DateTime DepositDate,
                                                int iTransactionID,
                                                bool bScannedChecks){
            /*
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append("SELECT ");
            sbSQL.Append("Convert(Char(8), factDocuments.ProcessingDateKey, 112) AS PICSDate,");
            sbSQL.Append("factBatchSummary.BatchID,");
            sbSQL.Append("factDocuments.DocumentSequence,");
            sbSQL.Append("RTrim(dimDocumentTypes.FileDescriptor) AS FileDescriptor,");
            sbSQL.Append("factDocuments.BatchSequence,");
            sbSQL.Append("factDocuments.GlobalDocumentID,");
            sbSQL.Append("ISNULL(RTrim(dimDocumentTypes.DocumentTypeDescription), 'Unknown') AS Description");
            sbSQL.Append("FROM factBatchSummary");
            sbSQL.Append("INNER JOIN dimLockboxes ON");
            sbSQL.Append("dimLockboxes.LockboxKey = factBatchSummary.LockboxKey");
            sbSQL.Append("INNER JOIN factDocuments ON");
            sbSQL.Append("factDocuments.BankKey = factBatchSummary.BankKey AND ");
            sbSQL.Append("factDocuments.CustomerKey = factBatchSummary.CustomerKey AND ");
            sbSQL.Append("factDocuments.LockboxKey = factBatchSummary.LockboxKey AND ");
            sbSQL.Append("factDocuments.ProcessingDateKey = factBatchSummary.ProcessingDateKey AND ");
            sbSQL.Append("factDocuments.DepositDateKey = factBatchSummary.DepositDateKey ");
            sbSQL.Append("INNER JOIN dimDocumentTypes ON ");
            sbSQL.Append("dimDocumentTypes.DocumentTypeKey = factDocuments.DocumentTypeKey ");
            sbSQL.Append("WHERE dimDocumentTypes.FileDescriptor <> 'C' ");
            sbSQL.Append(" AND dimLockboxes.SiteBankID = " + iBankID.ToString());
            sbSQL.Append(" AND dimLockboxes.SiteLockboxID = " + iLockboxID.ToString());
            sbSQL.Append(" AND factBatchSummary.DepositDateKey = " + dtDepositDate.ToString("yyyyMMdd"));
            //if !(DisplayScannedCheck){
            // sbSQL.Append(" AND dimDocumentTypes.FileDescriptor <> 'SC' ");
            //}
            sbSQL.Append("ORDER BY factDocuments.TxnSequence ASC ");
            
            return(sbSQL.ToString());
            */
            
            return("EXEC usp_factDocuments_GetByTransaction" +
                   "    @parmSiteBankID = " + iBankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + iLockboxID.ToString() + "," +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy")+"'," +
                   "    @parmBatchID = " + iBatchID.ToString() + ", "+
                   "    @parmTransactionID = " + iTransactionID.ToString() + ", "+
                   "    @parmScannedChecks = " + (bScannedChecks==true?1:0) );
        }

        public static string SQL_GetLockboxDataEntryFields(int BankID, int LockboxID) {
            return("EXEC usp_GetLockboxDataEntryFieldsByLockbox" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString());
        }

        public static string SQL_GetChecksDE(int BankID, 
                                               int LockboxID, 
                                               int BatchID, 
                                               DateTime DepositDate, 
                                               int TransactionID) {

            return("EXEC usp_Research_GetChecksDE" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + "," +
                   "    @parmBatchID = " + BatchID.ToString() + "," +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'," +
                   "    @parmTransactionID = " + TransactionID.ToString());
        }

        public static string SQL_GetStubsDE(int BankID,
                                            int LockboxID,
                                            int BatchID,
                                            DateTime DepositDate,
                                            int TransactionID) {
        return("EXEC usp_Research_GetStubsDE" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + "," +
                   "    @parmBatchID = " + BatchID.ToString() + "," +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'," +
                   "    @parmTransactionID = " + TransactionID.ToString());
        }
        public static string SQL_GetStubs(int BankID,
                                            int LockboxID,
                                            int BatchID,
                                            DateTime DepositDate,
                                            int TransactionID) {
        return("EXEC usp_Research_GetStubs" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString() + "," +
                   "    @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'," +
                   "    @parmBatchID = " + BatchID.ToString() + "," +
                   "    @parmTransactionID = " + TransactionID.ToString());
        }
        public static string SQL_SetWebDeliveryFlag(Guid parmOnlineImageQueueID, bool parmInternetDelivered){
            return("EXEC usp_SessionActivityLog_UpdateWebDeliveryFlag" +
                    "   @parmOnlineImageQueueID = '"+ parmOnlineImageQueueID.ToString() +"',"+
                    "   @parmInternetDelivered = "+ (parmInternetDelivered ? "1" : "0"));
        }
    }
}
