using System;
using System.Collections.Generic;
using System.Text;

using WFS.integraPAY.Online.Common;

/**********************************************************************************
*  Module:       Module for common sql statements
*  Filename:     SQLPublic.bas
*  Author:       Joel Caples
*  Description:
*    This bas module provides public functions to return sql statements.
*  Revisions:
* CR 5610 EJG 11/19/2003 - Modified functions to support CBXSrv.GetTransactionDetails()
*                        - Modified functions to also support lookup by TransactionID or
*                          Transactions.Sequence
* CR 7073 JMC 03/16/2004 - Modified SQL_GetTransactionStubs to include SystemType field.
* CR 4968 JMC 03/16/2004 - Added SQL_GetStubDocuments Function.
* CR 9470 JMC 09/28/2004
*     -Modified GetBatchSummary procedure to accept OLLockboxID instead of LockboxID
*     -Removed old commented procedures.
* CR 9555 JMC 09/28/2004
*     -Modified SQL_GetBatchDetail function to accept OLLockboxID instead of BankID,
*      CustomerId, and LockboxID.
* CR 7292 JMC 09/21/2004
*     -Added SQL_RemittanceSearch function as part of the Remittance Search Xslt
*      conversion.
* CR 11277 EJG 02/01/2005 - Modified SQL_GetBatchDetail, SQL_GetTransactionChecks, SQL_GetTransactionStubs,
*                           and SQL_GetTransactionDocuments to use the TableName.TransactionSequence.
* CR 10537 JMC 03/03/2005
*     -Added SQL_InsertSessionActivityLog method
*     -Added SQL_SetWebDeliveredFlag method
*     -Added SQL_UpdateActivityLockbox method
* CR 11383 EJG 03/09/2005 - Removed the NOT from an If/ElseIf statment in SQL_RemittanceSearch causing
*                           the statment to return all records regardless of date when no to date was specified
*                           in a date range search.
* CR 12650 JMC 10/12/2005
*     -Added the SQLEncodeString function which will SQL encode string values.
* CR 15169 EJG 03/08/2006 - Modified SQL_RemittanceSearch() to support column click sorting.
* CR 18947 JMC 11/06/2006
*     -Added SQL_DeleteOLUserMachines() method.
*     -Added SQL_DeleteOLUserQuestions() method.
*     -Added SQL_UpdateUserSetupToken() method.
**********************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLPublic {

        internal static string SQLEncodeString(string Value) {
	        return Value.Replace("'", "''");
        }

        internal static string SQL_GetDataValueVarchar(string DataValue) {

            string strRetVal;

            if(DataValue != null) {
                strRetVal = "'" + SQLEncodeString(DataValue) + "'";
            } else {
                strRetVal = "NULL";
            }

            return(strRetVal);
        }

        internal static string SQL_GetDataValueDateTime(string DataValue) {

            DateTime dteTemp;
            string strRetVal;

            if(DataValue != null && DateTime.TryParse(DataValue, out dteTemp)) {
                strRetVal = "'" + dteTemp.ToString() + "'";
            } else {
                //if not a dateTime put a NULL here
                strRetVal = "NULL";
            }

            return(strRetVal);
        }

        internal static string SQL_GetDataValueDouble(string DataValue) {

            double dblTemp;
            string strRetVal;

            if(DataValue != null && double.TryParse(DataValue, out dblTemp)) {
                strRetVal = dblTemp.ToString();
            } else {
                //if not a Double put a NULL here
                strRetVal = "NULL";
            }

            return(strRetVal);
        }

        internal static string SQL_GetDataValueInt(string DataValue) {

            int intTemp;
            string strRetVal;

            if(DataValue != null && int.TryParse(DataValue, out intTemp)) {
                strRetVal = intTemp.ToString();
            } else {
                //if not an int put a NULL here
                strRetVal = "NULL";
            }

            return(strRetVal);
        }

        internal static string SQL_GetDataValueBit(string DataValue) {

            string strRetVal;

            if(DataValue != null && DataValue.Trim().Length > 0) {
                if(DataValue.Trim() == "1" || DataValue.Trim().ToLower() == "true") {
                    strRetVal = "1";
                } else if(DataValue.Trim() == "0" || DataValue.Trim().ToLower() == "false") {
                    strRetVal = "0";
                } else {
                    //if not a boolean put a NULL here
                    strRetVal = "NULL";
                }
            } else {
                //if not a boolean put a NULL here
                strRetVal = "NULL";
            }

            return(strRetVal);
        }
    }
}
