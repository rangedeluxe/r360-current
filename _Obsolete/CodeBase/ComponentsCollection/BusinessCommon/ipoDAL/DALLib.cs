using System;
using System.Data;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2003.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
*  Module:       Module for common DAL statements
*  Filename:     DALLib.cs
*  Author:       Calvin Glomb
*  Description:
*    This module provides functions to process DAL statements.
*  Revisions:
* WI 90244 CRG 03/07/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
* WI 92344 CRG 03/14/2013
*	Create a function that converts the new column headings to the old headings
**********************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Data Access Layer Library for data conversion 
	/// </summary>
	internal static class DALLib {

		/// <summary>
		/// SQLs the encode string.
		/// </summary>
		/// <param name="Value">The value.</param>
		/// <returns></returns>
		internal static string SQLEncodeString(string Value) {
			return Value.Replace("'", "''");
		}

		/// <summary>
		/// Gets the data value varchar.
		/// </summary>
		/// <param name="DataValue">The data value.</param>
		/// <returns></returns>
		internal static string GetDataValueVarchar(string DataValue) {
			string strRetVal;
			if(DataValue != null) {
				strRetVal = "'" + SQLEncodeString(DataValue) + "'";
			} else {
				strRetVal = "NULL";
			}

			return(strRetVal);
		}

		/// <summary>
		/// Gets the data value date time.
		/// </summary>
		/// <param name="DataValue">The data value.</param>
		/// <returns></returns>
		internal static string GetDataValueDateTime(string DataValue) {
			DateTime dateTemp;
			string strRetVal;
			if(DataValue != null && DateTime.TryParse(DataValue, out dateTemp)) {
				strRetVal = "'" + dateTemp.ToString() + "'";
			} else {
				//if not a dateTime put a NULL here
				strRetVal = "NULL";
			}

			return(strRetVal);
		}

		/// <summary>
		/// Gets the data value double.
		/// </summary>
		/// <param name="DataValue">The data value.</param>
		/// <returns></returns>
		internal static string GetDataValueDouble(string DataValue) {
			double doubleTemp;
			string strRetVal;
			if(DataValue != null && double.TryParse(DataValue, out doubleTemp)) {
				strRetVal = doubleTemp.ToString();
			} else {
				//if not a Double put a NULL here
				strRetVal = "NULL";
			}

			return(strRetVal);
		}

		/// <summary>
		/// Gets the data value int.
		/// </summary>
		/// <param name="DataValue">The data value.</param>
		/// <returns></returns>
		internal static string GetDataValueInt(string DataValue) {
			int intTemp;
			string strRetVal;
			if(DataValue != null && int.TryParse(DataValue, out intTemp)) {
				strRetVal = intTemp.ToString();
			} else {

				//if not an int put a NULL here
				strRetVal = "NULL";
			}

			return(strRetVal);
		}

		/// <summary>
		/// Gets the data value bit.
		/// </summary>
		/// <param name="DataValue">The data value.</param>
		/// <returns></returns>
		internal static string GetDataValueBit(string DataValue) {
			string strRetVal;
			if(DataValue != null && DataValue.Trim().Length > 0) {
				if(DataValue.Trim() == "1" || DataValue.Trim().ToLower() == "true") {
					strRetVal = "1";
				} else if(DataValue.Trim() == "0" || DataValue.Trim().ToLower() == "false") {
					strRetVal = "0";
				} else {

					//if not a boolean put a NULL here
					strRetVal = "NULL";
				}
			} else {

				//if not a boolean put a NULL here
				strRetVal = "NULL";
			}

			return (strRetVal);
		}

		/// <summary>
		/// Converts the data table, by replacing the .
		/// </summary>
		/// <param name="dt">The data table.</param>
		/// <returns></returns>
		internal static DataTable ConvertDataTable(ref DataTable dt) {
			if(dt != null) {
				foreach(DataColumn column in dt.Columns) {
					switch(column.ColumnName.ToLower()) {
						case "olclientaccount":
							column.ColumnName = "OLLockbox";
							break;
						case "olclientaccountid":
							column.ColumnName = "OLLockboxID";
							break;
						case "siteclientaccountid":
							column.ColumnName = "SiteLockboxID";
							break;
						case "siteclientaccountkey":
							column.ColumnName = "SiteLockboxKey";
							break;
						case "olorganization":
							column.ColumnName = "OLCustomer";
							break;
						case "olorganizationid":
							column.ColumnName = "OLCustomerID";
							break;
						case "olorganizationcode":
							column.ColumnName = "OLCustomerCode";
							break;
						case "organizationid":
							column.ColumnName = "CustomerID";
							break;
						case "clientaccountid":
							column.ColumnName = "LockboxID";
							break;
						case "organizationcode":
							column.ColumnName = "CustomerCode";
							break;
						case "immutabledatekey":
							column.ColumnName = "ProcessingDateKey";
							break;
						case "immutabledate":
							column.ColumnName = "ProcessingDate";
							break;
					}
				}
			}

			return dt;
		}
	}
}
