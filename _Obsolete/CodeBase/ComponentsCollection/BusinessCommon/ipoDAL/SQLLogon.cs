using System;
using System.Text;
using WFS.integraPAY.Online.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose: 
*
* Modification History
* CR 18946 JMC 11/07/2006
*    -Initial release version.
* CR 19306 JMC 12/07/2006
*    -Changes to SQL based on DBA spec change.
* CR 20064 JMC 02/08/2007
*    -Changed Session update statement to use correct field name.
* CR 22721 GRG 10/18/2007
*    -Changed ValidateSecretAnswer select statement to validate on userid. 
* CR 25530 JMC 09/25/2008
*    -Added SQL_GetUserByExtID1 in in order to support the addition of ExtID1
*     logon method.
* CR 25946 JMC 10/29/2008
*    -Added IncludePassword parameter to SQL_GetUser function.
* CR 32263 WJS 08/12/2011
*    -Added support for get session last logon date
* CR 46314 WJS 09/26/2011
*    -Added support for get user password expired time. Move inlinesql for user 
* 	  questions to stored procedure
* CR 47037 WJS 10/7/2011 -
*	- Convert inline to sql to existing stored proc
* CR 49708 JCS 02/29/2012
*    -Added BrandingSchemeID field to SQL_GetOLCustomer.
* CR 52359 JNE 05/22/2012
*    -Removed SQL_DeleteSecretQuestions
******************************************************************************/
namespace WFS.integraPAY.Online.DAL {

	/// <summary>
	/// Summary description for ipoSQL.
	/// </summary>
	internal static class SQLLogon {


        //******************************
        // OLCustomers
        //******************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="olCustomerID"></param>
        /// <returns></returns>
        public static string SQL_GetOLCustomer(Guid olCustomerID) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT");
            sbSQL.Append("     OLCustomers.OLCustomerID,");
            sbSQL.Append("     OLCustomers.CustomerCode,");
            ////sbSQL.Append("     OLCustomers.OrigBankID,");
            ////sbSQL.Append("     OLCustomers.OrigCustomerID,");
            sbSQL.Append("     OLCustomers.Description,");
            sbSQL.Append("     OLCustomers.IsActive,");
            sbSQL.Append("     OLCustomers.ViewingDays,");
            sbSQL.Append("     OLCustomers.ExternalID1,");
            sbSQL.Append("     OLCustomers.ExternalID2,");
            sbSQL.Append("     OLCustomers.BrandingSchemeID,");
            sbSQL.Append("     OLCustomers.CreationDate,");
            sbSQL.Append("     OLCustomers.CreatedBy,");
            sbSQL.Append("     OLCustomers.ModificationDate,");
            sbSQL.Append("     OLCustomers.ModifiedBy");
            sbSQL.Append(" FROM OLCustomers");
            sbSQL.Append(" WHERE OLCustomers.OLCustomerID = '" + olCustomerID.ToString() + "'");
            
            return(sbSQL.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="failedLogonAttempts"></param>
        /// <returns></returns>
        public static string SQL_UpdateFailedLogonAttempts(int userID, int failedLogonAttempts) {

           /* StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" UPDATE Users");
            sbSQL.Append(" SET FailedLogonAttempts = " + failedLogonAttempts.ToString() + ",");
			
			#if USE_ITEMPROC
	            sbSQL.Append("     DateLastUpdated=GetDate(),");
	            sbSQL.Append("     LastUpdatedBy=suser_sname()");
			#else
	            sbSQL.Append("     ModificationDate=GetDate(),");
	            sbSQL.Append("     ModifiedBy=suser_sname()");
			#endif
            sbSQL.Append(" WHERE UserID = " + userID.ToString());
             return(sbSQL.ToString());
           */
            return ("EXEC usp_Users_UpdateFailedLogonAttempts" +
                  "    @parmUserID = " + userID.ToString() + ", " +
                  "    @parmFailedLogonAttempts = " + failedLogonAttempts.ToString());
           
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="setupToken"></param>
        /// <returns></returns>
        public static string SQL_ValidateSetupToken(int UserID, string SetupToken) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT Count(*) AS theCount");
            sbSQL.Append(" FROM Users");
            sbSQL.Append(" WHERE UserID = " + UserID.ToString());
            sbSQL.Append("     AND SetupToken = '" + SetupToken.Replace("'", "''") + "'");

            return(sbSQL.ToString());
        }

        //******************************
        // OLUserQuestions
        //******************************
        public static string SQL_GetOLUserQuestions(int userID) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT");
            sbSQL.Append("     OLUserQuestionID,");
            sbSQL.Append("     UserID,");
            sbSQL.Append("     QuestionText,");
            sbSQL.Append("     OLUserQuestionID,");
            sbSQL.Append("     CreationDate,");
            sbSQL.Append("     CreatedBy,");
            sbSQL.Append("     ModificationDate,");
            sbSQL.Append("     ModifiedBy");
            sbSQL.Append(" FROM OLUserQuestions");
            sbSQL.Append(" WHERE UserID = " + userID.ToString());

            return(sbSQL.ToString());
        }

        //******************************
        // OLUserMachines
        //******************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="machineToken"></param>
        /// <returns></returns>
        public static string SQL_CreateMachineRegistration(int userID, Guid machineToken) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" INSERT INTO OLUserMachines (");
            sbSQL.Append("     OLUserMachineID,");
            sbSQL.Append("     UserID,");
            sbSQL.Append("     MachineToken,");
            sbSQL.Append("     CreationDate,");
            sbSQL.Append("     CreatedBy,");
            sbSQL.Append("     ModificationDate,");
            sbSQL.Append("     ModifiedBy");
            sbSQL.Append(" ) VALUES (");
            sbSQL.Append("     NewID(),");
            sbSQL.Append("     " + userID.ToString() + ",");
            sbSQL.Append("     '" + machineToken.ToString() + "',");
            sbSQL.Append("     GetDate(),");
            sbSQL.Append("     suser_sname(),");
            sbSQL.Append("     GetDate(),");
            sbSQL.Append("     suser_sname())");

            return(sbSQL.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userID"></param>
        /// <param name="machineToken"></param>
        /// <returns></returns>
        public static string SQL_ValidateMachineToken(int userID, Guid machineToken) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT Count(*) AS theCount");
            sbSQL.Append(" FROM OLUserMachines");
            sbSQL.Append(" WHERE UserID = " + userID.ToString());
            sbSQL.Append("     AND MachineToken = '" + machineToken.ToString() + "'");

            return(sbSQL.ToString());
        }

        //******************************
        // Users.SetupToken
        //******************************
        public static string SQL_DeleteSetupToken(int UserID) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" UPDATE Users");
            sbSQL.Append(" SET SetupToken = NULL,");
			#if USE_ITEMPROC
	            sbSQL.Append("     DateLastUpdated=GetDate(),");
	            sbSQL.Append("     LastUpdatedBy=suser_sname()");
			#else
	            sbSQL.Append("     ModificationDate=GetDate(),");
	            sbSQL.Append("     ModifiedBy=suser_sname()");
			#endif
            sbSQL.Append(" WHERE UserID = " + UserID.ToString());

            return(sbSQL.ToString());
        }

        //******************************
        // SessionToken
        //******************************
        public static string SQL_GetSessionTokenBySessionID(Guid SessionID) {
            return("SELECT SessionTokenID FROM SessionToken WHERE SessionID = '" + SessionID.ToString() + "'");
        }

        public static string SQL_InsertSessionToken(Guid SessionTokenID, Guid SessionID, Guid EmulationID) {
            // emulation session so insert with EmulationID
            return("INSERT INTO SessionToken(SessionTokenID, SessionID, EmulationID)"
                    + " VALUES('" + SessionTokenID.ToString() + "'"
                    + ", '" + SessionID.ToString() + "'"
                    + ", '" + EmulationID.ToString() + "')");
        }
            
        public static string SQL_InsertSessionToken(Guid SessionTokenID, Guid SessionID) {
            return("INSERT INTO SessionToken(SessionTokenID, SessionID)"
                    + " VALUES('" + SessionTokenID.ToString() + "'"
                    + ", '" + SessionID.ToString() + "')");
        }

        public static string SQL_GetSessionToken(Guid SessionTokenID) {
            return("SELECT SessionTokenID, SessionID, EmulationID, CreationDate, DateUsed, GetDate() AS LogonDate"
                + " FROM SessionToken"
                + " WHERE SessionTokenID = '" + SessionTokenID.ToString() + "'");
        }

        public static string SQL_UpdateSessionToken(Guid SessionTokenID, DateTime LogonDate) {
            // update date used
            return("UPDATE SessionToken SET DateUsed = '" + LogonDate.ToString("MM/dd/yyyy HH:mm:ss") + "'"
                + " WHERE SessionTokenID = '" + SessionTokenID.ToString() + "'");
        }

         public static string SQL_GetSessionLastLogonDate(int userID) {
             return ("EXEC [OLTA].[usp_GetSessionLastLogonDate] @parmUserID = " + userID.ToString());
           
        }

        public static string SQL_GetUserPasswordExpiredTime(int userID) {
             return ("EXEC [OLTA].[usp_GetUserPasswordExpiredTime] @parmUserID = " + userID.ToString());
           
        }
        
	}
}
