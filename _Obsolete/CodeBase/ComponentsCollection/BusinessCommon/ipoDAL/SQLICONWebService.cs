using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using WFS.integraPAY.Online.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Mark Horwich
* Date:     MM/DD/YYYY
*
* Purpose:  
*
* Modification History
* CR 28461 MEH 01/22/2010
*   -Initial release version.
******************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    /// <summary>
    /// 
    /// </summary>
    internal static class SQLICONWebService
    {
        public static string SQL_GetHylandDocumentHandles(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID)
        {
            return ("SET ARITHABORT ON EXEC [OLTA].usp_ICONWebGetDocHandles @parmProcessingDate ='" + processingDate.ToString("MM/dd/yyyy") + "', @parmSiteBankID = " + bankID.ToString() + ", @parmSiteCustomerID = " + customerID.ToString() + ", @parmSiteLockboxID = " + lockboxID.ToString() + ", @parmSiteCodeID = " + siteCodeID.ToString() + ",  @parmBatchID = " + batchID.ToString() + " SET ARITHABORT OFF");
        }

        public static string SQL_GetHylandDocumentHandlesBySequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int batchSequence)
        {
            return ("SET ARITHABORT ON EXEC [OLTA].usp_ICONWebGetDocHandlesBySequence @parmProcessingDate ='" + processingDate.ToString("MM/dd/yyyy") + "', @parmSiteBankID = " + bankID.ToString() + ", @parmSiteCustomerID = " + customerID.ToString() + ", @parmSiteLockboxID = " + lockboxID.ToString() + ", @parmSiteCodeID = " + siteCodeID.ToString() + ",  @parmBatchID = " + batchID.ToString() + ", @parmBatchSequence = " + batchSequence.ToString() + " SET ARITHABORT OFF");
        }

        public static string SQL_GetBatchSequenceNumbers(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID)
        {
            return ("EXEC [OLTA].usp_ICONWebGetCheckDocumentBatchSequence @parmProcessingDate ='" + processingDate.ToString("MM/dd/yyyy") + "', @parmSiteBankID = " + bankID.ToString() + ", @parmSiteCustomerID = " + customerID.ToString() + ", @parmSiteLockboxID = " + lockboxID.ToString() + ", @parmSiteCodeID = " + siteCodeID.ToString() + ",  @parmBatchID = " + batchID.ToString());
        }

        public static string SQL_GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int iBatchSequence)
        {
            return ("EXEC [OLTA].usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence @parmProcessingDate ='" + processingDate.ToString("MM/dd/yyyy") + "', @parmSiteBankID = " + bankID.ToString() + ", @parmSiteCustomerID = " + customerID.ToString() + ", @parmSiteLockboxID = " + lockboxID.ToString() + ", @parmSiteCodeID = " + siteCodeID.ToString() + ",  @parmBatchID = " + batchID.ToString() + ", @parmBatchSequence = " + iBatchSequence.ToString());
        }       

        public static string SQL_NotifyServiceBroker(string sXML, int iActionCode, int iNotifyIMS)
        {
            return ("EXEC [dbo].[usp_ICONBatchNotifyOLTAServiceBroker] @parmXML = '" + sXML + "', @parmActionCode = " + iActionCode.ToString() + ", @parmNotifyIMS = " + iNotifyIMS.ToString());
        }
    }
}
