﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using WFS.integraPAY.Online.Common;

/******************************************************************************
** Wausau
** Copyright © 2001-2012 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2012.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Jason Efken
* Date:     1/16/2012
*
* Purpose:  
*
* Modification History
* CR 54054 JNE 07/09/2012
*   - Added @parmUserID to SQL_GetHOAPMASummaryData. 
*********************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLReport {

        public static string SQL_GetHOAPMASummaryData(int intCustomerID, int intLockboxID, int intDepositDate, int intUserID) {
            return("EXEC usp_GetHOAPMASummaryDetail" + 
                   "    @parmCustomerID = " + intCustomerID.ToString() + ", " +
                   "    @parmLockboxID = " + intLockboxID.ToString() + ", " +
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                   "    @parmDepositDateKey = '" + intDepositDate.ToString() + "'");
        }
        public static string SQL_GetHOAPMASummaryHeader(int intCustomerID, int intLockboxID, int intDepositDate, int intUserID){
            return("EXEC usp_GetHOAPMASummaryHeader" +
                    "   @parmSiteCustomerID = "+ intCustomerID.ToString() + ", " +
                    "   @parmSiteLockboxID = "+ intLockboxID.ToString() + ", "+
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                    "   @parmDepositDateKey = '" + intDepositDate.ToString() + "'");
        }
        public static string SQL_GetHOAPMADetailHeader( int intCustomerID, int intLockboxID, int intDepositDate, int intUserID, string strHOA_Number){
            return("EXEC usp_GetHOAPMADetailHeader" +
                    "   @parmCustomerID = "+ intCustomerID.ToString() + ", " +
                    "   @parmLockboxID = "+ intLockboxID.ToString() + ", "+
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                    "   @parmDepositDateKey = '" + intDepositDate.ToString() + "', "+
                    "   @parmHOA_Number = '" + strHOA_Number+ "'");
        }
        public static string SQL_GetHOAPMADetailData( int intCustomerID, int intLockboxID, int intDepositDate, int intUserID, string strHOA_Number){
            return("EXEC usp_GetHOAPMADetailData" +
                    "   @parmCustomerID = "+ intCustomerID.ToString() + ", " +
                    "   @parmLockboxID = "+ intLockboxID.ToString() + ", "+
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                    "   @parmDepositDateKey = '" + intDepositDate.ToString() + "', "+
                    "   @parmHOA_Number = '" + strHOA_Number+ "'");
        }
        public static string SQL_GetHOAPMADetailFooter( int intCustomerID, int intLockboxID, int intDepositDate, int intUserID, string strHOA_Number){
            return("EXEC usp_GetHOAPMADetailFooter" +
                    "   @parmCustomerID = "+ intCustomerID.ToString() + ", " +
                    "   @parmLockboxID = "+ intLockboxID.ToString() + ", "+
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                    "   @parmDepositDateKey = '" + intDepositDate.ToString() + "', "+
                    "   @parmHOA_Number = '" + strHOA_Number+ "'");
        }
        public static string SQL_GetHOAPMACustomers(int intDepositDate, int intUserID){
            return("EXEC usp_GetHOAPMACustomers" +
                "   @parmUserID = "+ intUserID.ToString() + ", "+
                "   @parmDepositDateKey = '" + intDepositDate.ToString() + "'");
        }
       public static string SQL_GetHOAPMAHOANames(int intDepositDate, int intUserID, int intCustomerID, int intLockboxID){
            return("EXEC usp_GetHOAPMAHOANames" +
                    "   @parmUserID = "+ intUserID.ToString() + ", "+
                    "   @parmDepositDateKey = '" + intDepositDate.ToString() +"', "+
                    "   @parmCustomerID = "+ intCustomerID.ToString() + ", " +
                    "   @parmLockboxID = "+ intLockboxID.ToString() ); 
        }
    }
}
