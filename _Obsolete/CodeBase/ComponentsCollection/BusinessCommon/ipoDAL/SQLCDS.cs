﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/22/2010
*
* Purpose:  
*
* Modification History
* 04/22/2010 JMC CR 29416
*     - Initial release.
******************************************************************************/
namespace WFS.integraPAY.Online.CDS.Console {

    internal static class SQLCDS {
        public static string SQL_ResetBatch(List<int> GlobalBatchIDList) {

            StringBuilder sbSQL = new StringBuilder();
            bool bolIsFirst = true;

            if(GlobalBatchIDList.Count > 0) {
                sbSQL.Append("UPDATE BatchTrace SET CWDBBatchStatus=NULL, CWDBImageStatus=NULL WHERE GlobalBatchID IN(");
                foreach(int globalbatchid in GlobalBatchIDList) {
                    if(bolIsFirst) {
                        bolIsFirst = false;
                    } else {
                        sbSQL.Append(", ");
                    }
                    sbSQL.Append(globalbatchid.ToString());
                }
                sbSQL.Append(")");
            }

            return(sbSQL.ToString());
        }

        public static string SQL_ResetBatchData(List<int> GlobalBatchIDList) {

            StringBuilder sbSQL = new StringBuilder();
            bool bolIsFirst = true;

            if(GlobalBatchIDList.Count > 0) {
                sbSQL.Append("UPDATE BatchTrace SET CWDBBatchStatus=NULL WHERE GlobalBatchID IN(");
                foreach(int globalbatchid in GlobalBatchIDList) {
                    if(bolIsFirst) {
                        bolIsFirst = false;
                    } else {
                        sbSQL.Append(", ");
                    }
                    sbSQL.Append(globalbatchid.ToString());
                }
                sbSQL.Append(")");
            }

            return(sbSQL.ToString());
        }

        public static string SQL_ResetBatchImages(List<int> GlobalBatchIDList) {

            StringBuilder sbSQL = new StringBuilder();
            bool bolIsFirst = true;

            if(GlobalBatchIDList.Count > 0) {
                sbSQL.Append("UPDATE BatchTrace SET CWDBImageStatus=NULL WHERE GlobalBatchID IN(");
                foreach(int globalbatchid in GlobalBatchIDList) {
                    if(bolIsFirst) {
                        bolIsFirst = false;
                    } else {
                        sbSQL.Append(", ");
                    }
                    sbSQL.Append(globalbatchid.ToString());
                }
                sbSQL.Append(")");
            }

            return(sbSQL.ToString());
        }
        
        public static string SQL_SetBatchConsolidationPriorities(List<int> GlobalBatchIDList, ConsolidationPriorities Priority) {

            StringBuilder sbSQL = new StringBuilder();
            bool bolIsFirst = true;

            if(GlobalBatchIDList.Count > 0) {
                sbSQL.Append("UPDATE BatchTrace SET CWDBConsolidationPriority=" + ((int)Priority).ToString() + " WHERE GlobalBatchID IN(");
                foreach(int globalbatchid in GlobalBatchIDList) {
                    if(bolIsFirst) {
                        bolIsFirst = false;
                    } else {
                        sbSQL.Append(", ");
                    }
                    sbSQL.Append(globalbatchid.ToString());
                }
                sbSQL.Append(")");
            }

            return(sbSQL.ToString());
        }
        
        public static string SQL_GetMaxBatchStatus() {
            return("SELECT MAX(CWDBBatchStatus) AS MaxCWDBBatchStatus FROM BatchTrace");
        }
        
        public static string SQL_GetBatchList(DateTime ProcessingDateFrom,
                                              DateTime ProcessingDateTo,
                                              int BankID,
                                              int LockboxID,
                                              bool OLCustomerSynch,
                                              bool ShowOnlyInternetCustomers,
                                              bool ViewShowAllBatchData,
                                              bool ShowOnlyTransferredBatches,
                                              bool ShowOnlyNewBatches,
                                              int SortOrder,
                                              int SortColumn) {
        
            StringBuilder sbSQL = new StringBuilder();
            StringBuilder sbCondition = new StringBuilder();
            StringBuilder sbSortOrder = new StringBuilder();

            const int COL_LOCKBOX_ID = 0;
            const int COL_LOCKBOX_NAME = 1;
            const int COL_BATCH_ID = 2;
            const int COL_BANK_NAME = 3;
            const int COL_CUSTOMER_NAME = 4;
            const int COL_PROCESSING_DATE = 5;
            const int COL_STATUS = 6;
        
            sbSQL.Append("SELECT DISTINCT");
            sbSQL.Append(" Batch.GlobalBatchID,");
            sbSQL.Append(" Batch.LockboxID          AS Lockbox,");
            sbSQL.Append(" Batch.LockboxID,");
            sbSQL.Append(" Customer.CustomerID,");
            sbSQL.Append(" Batch.BankID             AS Bank,");
            sbSQL.Append(" Batch.BankID,");
            sbSQL.Append(" Lockbox.LongName         AS Name,");
            sbSQL.Append(" Batch.BatchID            AS Batch,");
            sbSQL.Append(" Bank.BankName            AS Bank,");
            sbSQL.Append(" Customer.Name            AS Customer,");
            sbSQL.Append(" Batch.ProcessingDate,");
            sbSQL.Append(" Batch.DepositStatus,");
            sbSQL.Append(" Batch.ScanStatus,");
            sbSQL.Append(" Batch.DEStatus,");
            sbSQL.Append(" BatchTrace.CWDBBatchStatus,");
            sbSQL.Append(" BatchTrace.CWDBImageStatus,");
            sbSQL.Append(" ISNull(BatchTrace.CWDBConsolidationPriority, 5)     AS CWDBConsolidationPriority");
            sbSQL.Append(" FROM Batch");
            sbSQL.Append(" INNER JOIN BatchTrace ON Batch.GlobalBatchID=BatchTrace.GlobalBatchID");
            sbSQL.Append(" INNER JOIN Bank ON Batch.BankID=Bank.BankID");
            sbSQL.Append(" INNER JOIN Lockbox ON Batch.BankID=Lockbox.BankID AND Batch.LockboxID=Lockbox.LockboxID");
            sbSQL.Append(" INNER JOIN Customer ON Lockbox.BankID=Customer.BankID AND Lockbox.CustomerID=Customer.CustomerID");
                
            if(OLCustomerSynch) {
                sbSQL.Append(" LEFT OUTER JOIN OLLockboxes ON Batch.BankID=OLLockboxes.BankID AND Batch.LockboxID=OLLockboxes.LockboxID");
                sbSQL.Append(" LEFT OUTER JOIN OLCustomers ON OLLockboxes.OLCustomerID=OLCustomers.OLCustomerID");
                        
                if(ShowOnlyInternetCustomers) {
                    sbCondition.Append(" WHERE OLCustomers.OLCustomerID IS NOT NULL");
                }
            }

            if(ProcessingDateFrom > DateTime.MinValue) {
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" Batch.ProcessingDate >= '" + ProcessingDateFrom.ToString("MM/dd/yyyy") + "'");
            }

            if(ProcessingDateTo > DateTime.MinValue) {
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" Batch.ProcessingDate <= '" + ProcessingDateTo.ToString("MM/dd/yyyy") + "'");
            }

            if(BankID > -1) {
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" Batch.BankID = " + BankID.ToString());
            }

            if(LockboxID > -1) {
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" Batch.LockboxID = " + LockboxID.ToString());
            }

            if(ShowOnlyTransferredBatches) {
                // show only transferred batch data
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" NOT CWDBBatchStatus IS NULL and NOT CWDBImageStatus IS NULL");

            } else if(ShowOnlyNewBatches) {
                // restrict to untransferred batches
                sbCondition.Append((sbCondition.Length > 0 ? " AND " : " WHERE "));
                sbCondition.Append(" ((CWDBBatchStatus IS NULL or DATEDIFF(second,CWDBBatchStatus,Batch.ModificationDate) > 0)");
                sbCondition.Append(" OR (CWDBImageStatus IS NULL or DATEDIFF(second,CWDBImageStatus,Batch.ModificationDate) > 0))");
            }

            sbSQL.Append(sbCondition);
            
            if(SortOrder == 1) {
                sbSortOrder.Append("ASC");
            } else {
                sbSortOrder.Append("DESC");
            }
            
            switch(SortColumn) {
                case COL_LOCKBOX_ID:
                    sbSQL.Append(" ORDER BY Batch.LockboxID " + sbSortOrder.ToString() + ", Batch.ProcessingDate, Batch.BatchID");
                    break;
                case COL_LOCKBOX_NAME:
                    sbSQL.Append(" ORDER BY Lockbox.LongName " + sbSortOrder.ToString() + ", Batch.ProcessingDate, Batch.BatchID");
                    break;
                case COL_BATCH_ID:
                    sbSQL.Append(" ORDER BY Batch.BatchID " + sbSortOrder.ToString() + ", Batch.LockboxID, Batch.ProcessingDate");
                    break;
                case COL_BANK_NAME:
                    sbSQL.Append(" ORDER BY Bank.BankName " + sbSortOrder.ToString() + ", Batch.LockboxID, Batch.ProcessingDate");
                    break;
                case COL_CUSTOMER_NAME:
                    sbSQL.Append(" ORDER BY Customer.Name " + sbSortOrder.ToString() + ", Batch.LockboxID, Batch.ProcessingDate");
                    break;
                case COL_PROCESSING_DATE:
                    sbSQL.Append(" ORDER BY Batch.ProcessingDate " + sbSortOrder.ToString() + ", Batch.LockboxID, Batch.BatchID");
                    break;
                case COL_STATUS:
                    sbSQL.Append(" ORDER BY Batch.LockboxID " + sbSortOrder.ToString() + ", Batch.ProcessingDate, Batch.BatchID");
                    break;
            }

            return(sbSQL.ToString());
        }

        public static string SQL_CreateBatchTraceColumn(string NewColumnName, string DataType) {

            StringBuilder sb = new StringBuilder();

            sb.Append(" ALTER TABLE BatchTrace ADD " + NewColumnName + " " + DataType + " NULL");

            return(sb.ToString());
        }

    }
}
