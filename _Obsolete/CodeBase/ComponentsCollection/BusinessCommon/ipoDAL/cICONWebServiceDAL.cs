using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     MM/DD/YYYY
*
* Purpose:  Used to access database
*
* Modification History
* CR 33541 JMC 04/09/2011
*    -
* WI 87324 CRG 02/08/2013	
*    Remove SQLICONWebService.cs
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// USed to access database
	/// </summary>
    public class cICONWebServiceDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cICONWebServiceDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
        public cICONWebServiceDAL(string vSiteKey) : base(vSiteKey)
        {
        }

		/// <summary>
		/// Gets the hyland document handles by sequence.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="customerID">The customer ID.</param>
		/// <param name="lockboxID">The lockbox ID.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="siteCodeID">The site code ID.</param>
		/// <param name="batchSequence">The batch sequence.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool GetHylandDocumentHandlesBySequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int batchSequence, out DataTable dt)
        {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, customerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, batchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONWebGetDocHandlesBySequence", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHylandDocumentHandlesBySequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int batchSequence, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Gets the hyland document handles.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="customerID">The customer ID.</param>
		/// <param name="lockboxID">The lockbox ID.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="siteCodeID">The site code ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool GetHylandDocumentHandles(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, out DataTable dt)
        {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, customerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONWebGetDocHandles", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetHylandDocumentHandles(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Gets the batch sequence numbers.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="customerID">The customer ID.</param>
		/// <param name="lockboxID">The lockbox ID.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="siteCodeID">The site code ID.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool GetBatchSequenceNumbers(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, out DataTable dt)
        {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, customerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONWebGetCheckDocumentBatchSequence", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchSequenceNumbers(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Gets the batch sequence numbers by batch sequence.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="bankID">The bank ID.</param>
		/// <param name="customerID">The customer ID.</param>
		/// <param name="lockboxID">The lockbox ID.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="siteCodeID">The site code ID.</param>
		/// <param name="BatchSequence">The batch sequence.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int BatchSequence, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, bankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCustomerID", SqlDbType.Int, customerID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteLockboxID", SqlDbType.Int, lockboxID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteCodeID", SqlDbType.Int, siteCodeID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchSequence", SqlDbType.Int, BatchSequence, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONWebGetCheckDocumentBatchSequenceByBatchSequence", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetBatchSequenceNumbersByBatchSequence(DateTime processingDate, int bankID, int customerID, int lockboxID, int batchID, int siteCodeID, int iBatchSequence, out DataTable dt)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Updates the OLTA batch.
		/// </summary>
		/// <param name="XML">The XML.</param>
		/// <param name="ActionCode">The action code.</param>
		/// <param name="NotifyIMS">The notify IMS.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool UpdateOLTABatch(string XML, int ActionCode, int NotifyIMS)
        {
			bool bRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmXML", SqlDbType.Xml, XML, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmActionCode", SqlDbType.Int, ActionCode, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmNotifyIMS", SqlDbType.Int, NotifyIMS, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONBatchNotifyOLTAServiceBroker", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "UpdateOLTABatch(string XML, int ActionCode, int NotifyIMS)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Deletes the OLTA batch.
		/// </summary>
		/// <param name="XML">The XML.</param>
		/// <param name="ActionCode">The action code.</param>
		/// <param name="NotifyIMS">The notify IMS.</param>
		/// <returns>
		/// Returns the results of the store procedure call
		/// </returns>
		public bool DeleteOLTABatch(string XML, int ActionCode, int NotifyIMS)
        {
			bool bRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmXML", SqlDbType.Xml, XML, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmActionCode", SqlDbType.Int, ActionCode, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmNotifyIMS", SqlDbType.Int, NotifyIMS, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubUser.usp_ICONBatchNotifyOLTAServiceBroker", parms);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DeleteOLTABatch(string XML, int ActionCode, int NotifyIMS)");
			}

			return bRetVal;
		}
    }
}
