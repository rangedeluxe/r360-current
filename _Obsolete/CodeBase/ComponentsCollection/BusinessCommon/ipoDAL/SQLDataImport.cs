﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
* CR 49967 WJS 2/10/2012
*  - Change name of schema revision looking for
 * - Add xsdversion support
******************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLDataImport {

        public const string XSD_BATCH = "Data Import Integration Services for Batch Data";
        public const string XSD_CLIENT_SETUP = "Data Import Integration Services for Client Setup";
        public const string XSL_BATCH = "Data Import Integration Services for Batch Data XSL";
        public const string XSL_CLIENT_SETUP = "Data Import Integration Services for Client Setup XSL";


        public static string SQL_GetCurrentSchemaRevision(string XsdName, string XSDVersion) {
            return ("EXEC olta.usp_GetCurrentSchemaRevision @parmSchemaType = '" + XsdName + "' ,@parmXSDVersion ='" + XSDVersion + "'");
        }

     
    }
}
