﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

using WFS.integraPAY.Online.Common;

namespace WFS.integraPAY.Online.DAL {

    public class cImageSrvDAL : _DALBase {

        public cImageSrvDAL(string vSiteKey) : base(vSiteKey) {

        }

        public bool GetOnlineImageQueue(Guid OnlineImageQueueID, out DataTable dt) {
            return(ExecuteSQL(SQLImageSrv.SQL_GetOnlineImageQueue(OnlineImageQueueID), out dt));
        }

        public bool CreateOnlineImageQueue(Guid OnlineImageQueueID,
                                                        int SessionUserID,
                                                        bool DisplayScannedCheck, out DataTable dt) {

            return(ExecuteSQL(SQLImageSrv.SQL_CreateOnlineImageQueue(OnlineImageQueueID,
                                                        SessionUserID,
                                                        DisplayScannedCheck), out dt));
        }
    }
}
