﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     02/16/2011
*
* Purpose:  
*
* Modification History
* CR 32576 WJS 02/09/2011 
*     -Added IMS specific methods from cItemProcDAL
******************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLIMSIntegration {

        public static string SQL_GetIMSIntefaceQueueInsert(int numberOfItems)
        {
            return ("EXEC [OLTA].usp_GetIMSInterfaceQueue_GetInsert" + 
                    "    @paramNumItemsToRetrive = " + numberOfItems.ToString());
        }

        public static string SQL_GetIMSIntefaceQueueDelete(int numberOfItems)
        {
            return ("EXEC [OLTA].usp_GetIMSInterfaceQueue_GetDelete" + 
                    "    @paramNumItemsToRetrive = " + numberOfItems.ToString());
        }

        public static string SQL_UpdateIMSInterfaceQueueStatus(Guid queueId, int status, bool incrementRetryCount)
        {
            return ("EXEC [OLTA].usp_IMSInterfaceQueue_UpdateStatus" + 
                    "    @parmQueueID = '" + queueId.ToString() + "'," + 
                    "    @parmQueueStatus = " + status.ToString() + "," + 
                    "    @parmIncrementRetryCount = " + (incrementRetryCount ? "1" : "0"));
        }

        public static string SQL_UpdateIMSInterfaceQueueResponse(int BankID, int LockboxID, DateTime ProcessingDate, int BatchID, int NewQueueStatus)
        {
            return ("EXEC [OLTA].usp_IMSInterfaceQueue_UpdateResponse" + 
                    "    @SiteBankID = " + BankID.ToString() + ", " + 
                    "    @SiteLockboxID = " + LockboxID.ToString() + ", " + 
                    "    @ProcessingDateKey = " + ProcessingDate.ToString("yyyyMMdd") + ", " +
                    "    @BatchID = " + BatchID.ToString() + ", " +
                    "    @NewQueueStatus = " + NewQueueStatus.ToString());
        }

        public static string SQL_UpdateIMSInterfaceQueueStatusInUse(Guid queueId)
        {
            return ("EXEC [OLTA].usp_IMSInterfaceQueue_UpdateStatusInUse" + 
                    "    @parmQueueID = '" + queueId.ToString() + "'");
        }

        //CR 27910 WJS 10-14-2009 ICON Service
        public static string SQL_GetIMSInterfaceQueueIcon(int numberOfItems)
        {
            return ("EXEC [OLTA].usp_GetIMSInterfaceQueueIcon" + 
                    "    @paramNumItemsToRetrive = " + numberOfItems.ToString());
        }
    }
}
