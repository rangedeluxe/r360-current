﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Xml;

using WFS.integraPAY.Online.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/22/2010
*
* Purpose:  
*
* Modification History
* 04/22/2010 JMC CR 29416
*     - Initial release.
******************************************************************************/
namespace WFS.integraPAY.Online.CDS.Console {

    public class cCDSDAL : _DALBase {

        public cCDSDAL(string vSiteKey) : base(vSiteKey) {
            
        }

        public bool ResetBatches(List<int> GlobalBatchIDList,
                                 bool RetransmitBatchData,
                                 bool RetransmitBatchImages, 
                                 out Exception DbException) {

            SqlParameter[] parms;

            List<SqlParameter> arParms = new List<SqlParameter>();

            XmlDocument docBatches = new XmlDocument();
            XmlNode nodeRoot = docBatches.CreateNode(XmlNodeType.Element, "Root", docBatches.NamespaceURI);
            XmlNode nodeBatch;
            XmlAttribute attGlobalBatchID;
            foreach(int globalbatchid in GlobalBatchIDList) {
                nodeBatch = docBatches.CreateNode(XmlNodeType.Element, "Batch", docBatches.NamespaceURI);
                attGlobalBatchID = docBatches.CreateAttribute("GlobalBatchID");
                attGlobalBatchID.Value = globalbatchid.ToString();
                nodeBatch.Attributes.Append(attGlobalBatchID);
                nodeRoot.AppendChild(nodeBatch);
            }
            docBatches.AppendChild(nodeRoot);

            arParms.Add(BuildParameter("@parmBatchInfoXML", SqlDbType.Xml, docBatches.OuterXml, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmRetransmitBatchData", SqlDbType.Bit, RetransmitBatchData, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmRetransmitBatchImages", SqlDbType.Bit, RetransmitBatchImages, ParameterDirection.Input));

            parms = arParms.ToArray();

            return(ExecuteProcedure("proc_CWDBConsole_RetransmitBatch", parms, out DbException));
        }
        
        public bool SetBatchConsolidationPriorities(List<int> GlobalBatchIDList, 
                                                    ConsolidationPriorities CWDBConsolidationPriority, 
                                                    out Exception DbException) {

            SqlParameter[] parms;

            List<SqlParameter> arParms = new List<SqlParameter>();

            XmlDocument docBatches = new XmlDocument();
            XmlNode nodeRoot = docBatches.CreateNode(XmlNodeType.Element, "Root", docBatches.NamespaceURI);
            XmlNode nodeBatch;
            XmlAttribute attGlobalBatchID;
            foreach(int globalbatchid in GlobalBatchIDList) {
                nodeBatch = docBatches.CreateNode(XmlNodeType.Element, "Batch", docBatches.NamespaceURI);
                attGlobalBatchID = docBatches.CreateAttribute("GlobalBatchID");
                attGlobalBatchID.Value = globalbatchid.ToString();
                nodeBatch.Attributes.Append(attGlobalBatchID);
                nodeRoot.AppendChild(nodeBatch);
            }
            docBatches.AppendChild(nodeRoot);

            arParms.Add(BuildParameter("@parmBatchInfoXML", SqlDbType.Xml, docBatches.OuterXml, ParameterDirection.Input));
            arParms.Add(BuildParameter("@parmCWDBConsolidationPriority", SqlDbType.TinyInt, ((int)CWDBConsolidationPriority).ToString(), ParameterDirection.Input));

            parms = arParms.ToArray();

            return(ExecuteProcedure("proc_CWDBConsole_SetConsolidationPriority", parms, out DbException));
        }

        public bool GetMaxBatchStatus(out DataTable dt) {
            return(ExecuteSQL("proc_CWDBConsole_GetLastConsolidationInfo", out dt));
        }

        public bool GetBatchList(DateTime ProcessingDateFrom,
                                 DateTime ProcessingDateTo,
                                 int BankID,
                                 int LockboxID,
                                 bool ShowOnlyInternetCustomers,
                                 bool ShowAllBatchData,
                                 bool ShowOnlyTransferredBatches,
                                 bool ShowOnlyNewBatches,
                                 int SortOrder,
                                 int SortColumn, 
                                 out DataTable dt) {

            bool bolRetVal;
            Exception exDbException;

            bool bolTried1 = false;
            bool bolTried2 = false;
            bool bolTried3 = false;
            bool bolKeepTrying;


            SqlParameter[] parms;

            bolRetVal = false;
            dt = null;
            
            List<SqlParameter> arParms;
            
            bolKeepTrying = true;
            while(bolKeepTrying) {

                arParms = new List<SqlParameter>();

                arParms.Add(BuildParameter("@parmShowOnlyTransferredBatches", SqlDbType.Bit, ShowOnlyTransferredBatches, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmShowOnlyNewBatches", SqlDbType.Bit, ShowOnlyNewBatches, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmShowOnlyInternetCustomers", SqlDbType.Bit, ShowOnlyInternetCustomers, ParameterDirection.Input));

                if(ProcessingDateFrom > DateTime.MinValue) {
                    arParms.Add(BuildParameter("@parmProcessingDateFrom", SqlDbType.DateTime, ProcessingDateFrom, ParameterDirection.Input));
                } else {
                    arParms.Add(BuildParameter("@parmProcessingDateFrom", SqlDbType.DateTime, null, ParameterDirection.Input));
                }

                if(ProcessingDateTo > DateTime.MinValue) {
                    arParms.Add(BuildParameter("@parmProcessingDateTo", SqlDbType.DateTime, ProcessingDateTo, ParameterDirection.Input));
                } else {
                    arParms.Add(BuildParameter("@parmProcessingDateTo", SqlDbType.DateTime, null, ParameterDirection.Input));
                }

                if(BankID > -1) {
                    arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, BankID, ParameterDirection.Input));
                } else {
                    arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, null, ParameterDirection.Input));
                }

                if(LockboxID > -1) {
                    arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, LockboxID, ParameterDirection.Input));
                } else {
                    arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, null, ParameterDirection.Input));
                }

                parms = arParms.ToArray();

                bolRetVal = ExecuteProcedure("proc_CWDBConsole_GetBatchList", parms, out dt, out exDbException);

                bolKeepTrying = false;
                
                if(!bolRetVal) {           
                
                    if(exDbException != null && exDbException.Message.ToLower().IndexOf("invalid column name") > -1) {

                        if((!bolTried1) && exDbException.Message.ToLower().IndexOf("cwdbconsolidationpriority") > -1) {
                            if(CreateBatchTraceColumn("CWDBConsolidationPriority", "TINYINT", out exDbException)) {
                                bolKeepTrying = true;
                            } else {
                                //TODO: Report this back to the client to inform the user when if an error occurs.
                            }
                            bolTried1 = true;
                        } else if((!bolTried2) && exDbException.Message.ToLower().IndexOf("cwdbbatchstatus") > -1) {
                            if(CreateBatchTraceColumn("CWDBBatchStatus", "DATETIME", out exDbException)) {
                                bolKeepTrying = true;
                            } else {
                                //TODO: Report this back to the client to inform the user when if an error occurs.
                            }
                            bolTried2 = true;
                        } else if((!bolTried3) && exDbException.Message.ToLower().IndexOf("cwdbimagestatus") > -1) {
                            if(CreateBatchTraceColumn("CWDBImageStatus", "DATETIME", out exDbException)) {
                                bolKeepTrying = true;
                            } else {
                                //TODO: Report this back to the client to inform the user when if an error occurs.
                            }
                            bolTried3 = true;
                        }

                    }
                }
            }

            return(bolRetVal);
        }

        private bool CreateBatchTraceColumn(string NewColumnName, 
                                            string DataType, 
                                            out Exception exDbException) {

            string strSQL;
            bool bolRetVal;
            
            strSQL = "EXECUTE proc_BatchTrace_AddColumn @parmNewColumnName = '" + NewColumnName + "', @parmDataType = " + DataType;

            EventLog.logWarning("BatchTrace column: [" + NewColumnName + "] not found.  Adding column: " + strSQL, MessageImportance.Essential);

            bolRetVal = ExecuteNonScalar(strSQL);
            
            if(bolRetVal) {
                exDbException = null;
            } else {
                exDbException = this.LastDbException;
            }            

            return(bolRetVal);
        }
    }
}
