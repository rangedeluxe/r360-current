using System;
using System.Collections.Generic;
using System.Text;
using WFS.integraPAY.Online.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2010 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: Joel Caples
* Date: 
*
* Purpose:
*
* Modification History
* CR 28738 JMC 03/23/2010
*    -Added SQL_GetDestLockboxes function.
* CR 30053 JNE 07/01/2010
*    -SQL_GetTransactionChecksDE,SQL_GetTransactionStubsDE changed
*     dimLockboxesView to dimLockboxes
* CR 31140 JMC CR 9/28/2010
*    -Add status as a parameter to call to
*     usp_IMSInterfaceQueue_UpdateStatus
*    -Add method to execute usp_IMSInterfaceQueue_UpdateStatusInUse
*     stored procedure.
* CR 32576 JMC 02/01/2011
*    -Removed SQL_UpdateFactChecksXML() and SQL_UpdateFactDocumentsXML() 
*     methods.
* CR 32576 WJS 02/09/2011 
*    -Add method SQL_GetKeysForBatch()
* CR 31587 JMC 02/15/2011
*    -Added SQL_UpdateIMSInterfaceQueueResponse() method 
* CR 31587 JMC 02/16/2011 
*    -Moved IMS specific methods to SQLIMSIntegration
* CR 32562 JMC 03/02/2011 
*    -Removed old conditional compilation code.
*    -Added SQL_GetCheck() and SQL_GetDocument() methods.
* CR 32538 JMC 03/24/2011 
*    -Modified SQL_GetKeysForBatch() to include BatchSourceKey.
* CR 31536 WJS 03/09/2011
*    -Added support for InsertDataTrails method
*     inserting items in factBatchData and factItemData
* CR 34099 JMC 09/08/2011
*    -Added SQL_GetLockboxSummary() method.
* CR 30305 WJS 10/4/2011
*    -Removed SQL_GetDocumentTypes 
* CR 49316 WJS 2/8/2012
*    -Removed SQL_GatherDocumentData 
* CR 49790 JCS 01/24/2012
*    -Modified SQL_GetOLCustomer to include BrandingSchemeID.
* CR 52261 WJS 5/21/2012
*    -Remove GetImageRPSAlias Mapping call and SQL_UpsertImageRPSAliasKeyword
* CR 52276 JNE 05/15/2012
*    -Added SQL_GetLockbox() method.
* CR 52661 WJS 7/31/2012
 *    - Add back UpsertImageRPSAliasKeyword
******************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLItemProc {

        //static string to prvent build the starting string everytime
        private static string   _factBatchDataStartSQLQuery     = string.Empty;
        private static string   _factItemDataStartSQLQuery      = string.Empty;
        private const string    _sICONBatchSourceName           = "ICON";  //configure what source to use.
        private const string    _sintegraPAYBatchSourceName     = "integraPAY";

        //************************************************************************
        //* SQL_GetOLCustomer
        //************************************************************************
        public static string SQL_GetOLCustomer(Guid OLCustomerID) {

            string strSQL;

            strSQL = "SELECT" +
                     "  OLCustomerID," +
                     "  CustomerCode," +
                     "  Description," +
                     "  IsActive," +
                     "  ViewingDays," +
                     "  ExternalID1," +
                     "  ExternalID2," +
                     "  BrandingSchemeID," +
                     "  CreationDate," +
                     "  CreatedBy," +
                     "  ModificationDate," +
                     "  ModifiedBy" +
                     " FROM OLCustomers" +
                     " WHERE OLCustomerID='" + OLCustomerID + "'";

            return (strSQL);
        }

        //************************************************************************
        //* SQL_LookupCustomerName
        //************************************************************************
        public static string SQL_LookupCustomerName(int BankID, int CustomerID) {
            return("EXEC usp_LookupCustomerName @parmSiteBankID = " + BankID.ToString() +
                                             ", @parmSiteCustomerID = " + CustomerID.ToString());
        }

        //************************************************************************
        //* SQL_GetLockboxByID
        //************************************************************************
        public static string SQL_GetLockboxByID(cUser SessionUser, Guid OLLockboxID) {
            
            return("EXEC usp_GetLockboxByID @parmOLCustomerID='" + SessionUser.OLCustomerID.ToString() + "'" +
                                         ", @parmOLLockboxID='" + OLLockboxID.ToString() + "'" +
                                         ", @parmUserID=" + SessionUser.UserID.ToString());
        }

        public static string SQL_GetDestLockboxes(int BankID, int LockboxID) {
            return("EXEC usp_CommingledLockbox_GetDestLockboxes" +
                   "    @parmSiteBankID = " + BankID.ToString() + "," +
                   "    @parmSiteLockboxID = " + LockboxID.ToString());
        }

        //************************************************************************
        //* SQL_GetLockboxByLockboxID
        //************************************************************************
        public static string SQL_GetLockboxByLockboxID(cUser SessionUser, int BankID, int LockboxID) {
            
            return("EXEC usp_GetLockboxByLockboxID @parmSiteBankID=" + BankID.ToString() +
                                                ", @parmSiteLockboxID=" + LockboxID.ToString() +
                                                ", @parmOLCustomerID='" + SessionUser.OLCustomerID.ToString() + "'" +
                                                ", @parmUserID=" + SessionUser.UserID.ToString());

        }

        //************************************************************************
        //* SQL_GetLockboxTotal
        //************************************************************************
        public static string SQL_GetLockboxTotal(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate) {

            return("EXEC usp_GetLockboxTotal @parmSiteBankID = " + BankID.ToString() 
                                        + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                        + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                        + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'");
        }

        //************************************************************************
        //* SQL_GetBatchCount
        //************************************************************************
        public static string SQL_GetBatchCount(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate) {


            return("EXEC usp_GetBatchCount @parmSiteBankID = " + BankID.ToString() 
                                      + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                      + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                      + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'");
        }

        //************************************************************************
        //* SQL_GetBatch
        //************************************************************************
        public static string SQL_GetBatch(Guid OLCustomerID, 
                                          Guid OLLockboxID, 
                                          int BankID,
                                          int LockboxID, 
                                          int BatchID, 
                                          DateTime DepositDate) {

            if(OLLockboxID != Guid.Empty) {
                return("EXEC olta.usp_GetBatchBYGlobalBatchIDOLCustomerIDOLLockboxID"
                     + "    @parmOLCustomerID = '" + OLCustomerID.ToString() + "'"
                     + "  , @parmOLLockboxID = '" + OLLockboxID.ToString() + "'"
                     + "  , @parmSiteBankID = " + BankID.ToString() 
                     + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                     + "  , @parmBatchID = " + BatchID.ToString() 
                     + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'");
            } else {
                return("EXEC olta.usp_GetBatchByGlobalBatchIDOLCustomerID"
                     + "    @parmOLCustomerID = '" + OLCustomerID.ToString() + "'"
                     + "  , @parmSiteBankID = " + BankID.ToString() 
                     + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                     + "  , @parmBatchID = " + BatchID.ToString() 
                     + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'");
            }
        }

        public static string SQL_GetBatch(int BankID,
                                          int LockboxID,
                                          int ProcessingDateKey,
                                          int BatchID) {

            return("EXEC usp_GetBatch"
                 + "    @parmSiteBankID = " + BankID.ToString() 
                 + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                 + "  , @parmProcessingDateKey = " + ProcessingDateKey.ToString()
                 + "  , @parmBatchID = " + BatchID.ToString());
        }

        //************************************************************************
        //* SQL_GetBatchSummary
        //************************************************************************
        public static string SQL_GetBatchSummary(int BankID, 
                                                 int LockboxID, 
                                                 DateTime StartDate, 
                                                 DateTime EndDate, 
                                                 bool DisplayScannedCheckOnline) {

            return("EXEC usp_GetBatchSummary @parmSiteBankID = " + BankID.ToString() 
                                        + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                        + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                        + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'"
                                        + ", @parmDisplayScannedCheck = " + (DisplayScannedCheckOnline ? "1" : "0"));
        }

        //************************************************************************
        //* SQL_GetBatchDetail
        //************************************************************************
        public static string SQL_GetBatchDetail(Guid OLLockboxID, 
                                                int BatchID, 
                                                DateTime DepositDate, 
                                                bool DisplayScannedCheckOnline) {
            return("EXEC usp_GetBatchDetail"
                 + "    @parmOLLockboxID = '" + OLLockboxID.ToString() + "'"
                 + "  , @parmBatchID = " + BatchID.ToString() 
                 + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                 + "  , @parmDisplayScannedCheck = " + (DisplayScannedCheckOnline ? '1' : '0'));
        }

        //************************************************************************
        //* SQL_GetDataEntryFields
        //************************************************************************
        public static string SQL_GetDataEntryFields(int BankID, 
                                                    int LockboxID, 
                                                    int BatchID, 
                                                    DateTime DepositDate) {

            return(" EXEC usp_GetDataEntryFields @parmSiteBankID = " + BankID.ToString()
                                            + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                            + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                            + ", @parmBatchID = " + BatchID.ToString());
        }

        public static string SQL_GetDataEntryFields(int BankID, 
                                                    int LockboxID, 
                                                    int BatchID, 
                                                    DateTime DepositDate,
                                                    TableTypes TableType) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT	");
            sbSQL.Append(" 	    COALESCE(OLTA.dimDataEntryColumns.DisplayName, OLTA.dimDataEntryColumns.FldName) AS FldTitle,");
            sbSQL.Append(" 	    RTrim(OLTA.dimDataEntryColumns.FldName) AS FldName,");
            sbSQL.Append(" 	    OLTA.dimDataEntryColumns.TableName AS TableName,");
            sbSQL.Append(" 	    CASE");
            sbSQL.Append(" 		    WHEN ((OLTA.dimDataEntryColumns.TableType = 0 OR OLTA.dimDataEntryColumns.TableType = 2) ");
            sbSQL.Append(" 				    AND RTRIM(OLTA.dimDataEntryColumns.FldName) = 'Amount') ");
            sbSQL.Append(" 		    THEN 7");
            sbSQL.Append(" 		    ELSE OLTA.dimDataEntryColumns.DataType");
            sbSQL.Append(" 	    END AS FldDataTypeEnum,");
            sbSQL.Append(" 	    OLTA.dimDataEntryColumns.FldLength AS FldLength");
            sbSQL.Append(" FROM	OLTA.factDataEntrySummary");
            sbSQL.Append(" 	    INNER JOIN OLTA.dimDataEntryColumns ON OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntrySummary.DataEntryColumnKey");
            sbSQL.Append(" 	    INNER JOIN OLTA.dimLockboxes ON OLTA.factDataEntrySummary.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE  	OLTA.dimLockboxes.SiteBankID = @parmSiteBankID");
            sbSQL.Append(" 	    AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID");
            sbSQL.Append(" 	    AND OLTA.factDataEntrySummary.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)");
            sbSQL.Append(" 	    AND OLTA.factDataEntrySummary.BatchID = @parmBatchID");

            if(TableType == TableTypes.Checks) {
                sbSQL.Append(" 	    AND OLTA.dimDataEntryColumns.TableType IN(0, 1)");
            } else {
                sbSQL.Append(" 	    AND OLTA.dimDataEntryColumns.TableType IN(2, 3)");
            }

            sbSQL.Append(" ORDER BY OLTA.dimDataEntryColumns.ScreenOrder ASC");

            return(sbSQL.ToString());
        }

        //************************************************************************
        //* SQL_GetTransactionCount
        //************************************************************************
        public static string SQL_GetTransactionCount(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate) {


            if(EndDate > DateTime.MinValue) {
            return("EXEC usp_GetTransactionCount @parmSiteBankID = " + BankID.ToString()
                                            + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                            + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                            + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'");
            } else {
            return("EXEC usp_GetTransactionCount @parmSiteBankID = " + BankID.ToString()
                                            + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                            + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                            + ", @parmDepositDateEnd = '" + StartDate.ToString("MM/dd/yyyy") + "'");
            }
        }

        //************************************************************************
        //* SQL_GetTransactionDetails
        //************************************************************************
        public static string SQL_GetTransactionDetails(Guid OLCustomerID, 
                                                       int BankID, 
                                                       int LockboxID, 
                                                       int BatchID, 
                                                       DateTime DepositDate, 
                                                       int TransactionID,
                                                       bool DisplayScannedCheckOnline) {

            return("EXEC usp_GetTransactionDetail @parmOLCustomerID = '" + OLCustomerID.ToString() + "'" 
                 + "  , @parmSiteBankID = " + BankID.ToString() 
                 + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                 + "  , @parmBatchID = " + BatchID.ToString() 
                 + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                 + "  , @parmTransactionID = " + TransactionID.ToString()
                 + "  , @parmDisplayScannedCheck = " + (DisplayScannedCheckOnline ? '1' : '0'));
        }

        public static string SQL_GetTransactionDetailsBySequence(Guid OLCustomerID, 
                                                                 int BankID, 
                                                                 int LockboxID, 
                                                                 int BatchID, 
                                                                 DateTime DepositDate,
                                                                 int TxnSequence,
                                                                 bool DisplayScannedCheckOnline) {

            return("EXEC usp_GetTransactionDetailBySequence @parmOLCustomerID = '" + OLCustomerID.ToString() + "'" 
                 + "  , @parmSiteBankID = " + BankID.ToString() 
                 + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                 + "  , @parmBatchID = " + BatchID.ToString() 
                 + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                 + "  , @parmTxnSequence = " + TxnSequence.ToString()
                 + "  , @parmDisplayScannedCheck = " + (DisplayScannedCheckOnline ? '1' : '0'));
        }

        //************************************************************************
        //* SQL_GetCheckCount
        //************************************************************************
        public static string SQL_GetCheckCount(int BankID, int LockboxID, DateTime StartDate, DateTime EndDate) {
            return("EXEC usp_GetCheckCount @parmSiteBankID = " + BankID.ToString() 
                                      + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                      + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                      + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'");
        }

        //************************************************************************
        //* SQL_GetTransactionChecks
        //************************************************************************
        public static string SQL_GetTransactionChecks(int BankID,
                                                      int LockboxID,
                                                      DateTime DepositDate,
                                                      int BatchID,
                                                      int TransactionID) {

            return("EXEC usp_GetTransactionChecks @parmSiteBankID = " + BankID.ToString()
                                             + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                             + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                             + ", @parmBatchID = " + BatchID.ToString()
                                             + ", @parmTransactionID = " + TransactionID.ToString());
        }

        //************************************************************************
        //* SQL_GetTransactionChecksBySequence
        //************************************************************************
        public static string SQL_GetTransactionChecksBySequence(int BankID,
                                                                int LockboxID,
                                                                DateTime DepositDate,
                                                                int BatchID,
                                                                int TxnSequence) {

            return("EXEC usp_GetTransactionChecksBySequence @parmSiteBankID = " + BankID.ToString()
                                                       + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                                       + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                                       + ", @parmBatchID = " + BatchID.ToString()
                                                       + ", @parmTxnSequence = " + TxnSequence.ToString());
        }

        //************************************************************************
        //* SQL_GetCheck
        //************************************************************************
        public static string SQL_GetCheck(int BankID, 
                                          int LockboxID, 
                                          int BatchID, 
                                          DateTime DepositDate, 
                                          int TransactionID, 
                                          int BatchSequence) {
            //return("EXEC usp_GetCheck @parmGlobalCheckID = " + GlobalCheckID.ToString());

            return("EXEC usp_GetCheck @parmSiteBankID = " + BankID.ToString() 
                               + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                               + "  , @parmBatchID = " + BatchID.ToString() 
                               + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                               + "  , @parmTransactionID = " + TransactionID.ToString()
                               + "  , @parmBatchSequence = " + BatchSequence.ToString());
        }

        public static string SQL_GetCheck2(int BankID,
                                           int LockboxID,
                                           int ProcessingDateKey,
                                           int BatchID,
                                           int BatchSequence) {

            return ("EXEC [OLTA].[usp_GetCheck2]  @parmSiteBankID = " + BankID.ToString()
                                             + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                             + ", @parmProcessingDateKey = " + ProcessingDateKey.ToString()
                                             + ", @parmBatchID = " + BatchID.ToString()
                                             + ", @parmBatchSequence = " + BatchSequence.ToString());

        }

        //************************************************************************
        //* SQL_GetTransactionChecksDE
        //************************************************************************
        public static string SQL_GetTransactionChecksDE(int BankID, 
                                                        int LockboxID, 
                                                        int BatchID, 
                                                        DateTime DepositDate, 
                                                        int TransactionID, 
                                                        int BatchSequence, 
                                                        string CheckDEFieldList) {

            ////return("EXEC SQL_GetTransactionChecksDE @parmSiteBankID = " + BankID.ToString() 
            ////                                 + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
            ////                                 + "  , @parmBatchID = " + BatchID.ToString() 
            ////                                 + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
            ////                                 + "  , @parmTransactionID = " + TransactionID.ToString()
            ////                                 + "  , @parmBatchSequence = " + BatchSequence.ToString()
            ////                                 + "  , @parmCheckDEFieldList = '" + CheckDEFieldList + "'");

            StringBuilder sbSQL = new StringBuilder();
            ////string strFieldList;

            ////if(CheckDEFieldList.Length > 0) {
            ////    if(CheckDEFieldList.EndsWith(", ")) {
            ////        strFieldList = CheckDEFieldList.Substring(0, CheckDEFieldList.Length - 2);
            ////    }
            ////    strFieldList = CheckDEFieldList;
            ////} else {
            ////    strFieldList = " OLTA.factDataEntryDetails.TransactionID," +
            ////                   " OLTA.factDataEntryDetails.BatchSequence," +
            ////                   " OLTA.dimDataEntryColumns.ColumnName AS FldTitle," +
            ////                   " OLTA.factDataEntryDetails.DataEntryValue";
            ////}
            
            sbSQL.Append(" DECLARE @parmSiteBankID int");
            sbSQL.Append(" DECLARE @parmSiteLockboxID int");
            sbSQL.Append(" DECLARE @parmBatchID int");
            sbSQL.Append(" DECLARE @parmDepositDate datetime");
            sbSQL.Append(" DECLARE @parmTransactionID int");
            sbSQL.Append(" DECLARE @parmBatchSequence int");
            sbSQL.Append("");
            sbSQL.Append(" SET @parmSiteBankID = " + BankID.ToString());
            sbSQL.Append(" SET @parmSiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append(" SET @parmBatchID = " + BatchID.ToString());
            sbSQL.Append(" SET @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'");
            sbSQL.Append(" SET @parmTransactionID = " + TransactionID.ToString());
            sbSQL.Append(" SET @parmBatchSequence = " + BatchSequence.ToString());
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append("      OLTA.dimDataEntryColumns.TableName,");
            sbSQL.Append("      OLTA.dimDataEntryColumns.FldName,");
            sbSQL.Append("      OLTA.dimDataEntryColumns.DataType               AS FldDataTypeEnum,");
            sbSQL.Append("      OLTA.factDataEntryDetails.DataEntryValue,");
            sbSQL.Append("      COALESCE(OLTA.dimDataEntryColumns.DisplayName, OLTA.dimDataEntryColumns.FldName) AS FldTitle");
            //sbSQL.Append(" SELECT " + strFieldList);
            sbSQL.Append(" FROM");
            sbSQL.Append("   OLTA.dimDataEntryColumns");
            sbSQL.Append("      INNER JOIN OLTA.factDataEntryDetails ON");
            sbSQL.Append("          OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey");
            sbSQL.Append("      INNER JOIN OLTA.dimLockboxes ON");
            sbSQL.Append("          OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE");
            sbSQL.Append("   OLTA.dimLockboxes.SiteBankID = @parmSiteBankID");
            sbSQL.Append("   AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.BatchID = @parmBatchID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.BatchSequence = @parmBatchSequence");
            sbSQL.Append("   AND OLTA.dimDataEntryColumns.TableType IN (0, 1)");
            sbSQL.Append(" ORDER BY OLTA.factDataEntryDetails.TransactionID,");
            sbSQL.Append("   OLTA.factDataEntryDetails.BatchSequence,");
            sbSQL.Append("   OLTA.dimDataEntryColumns.ScreenOrder ASC");

            return(sbSQL.ToString());

        }

        //************************************************************************
        //* SQL_GetTransactionStubs
        //************************************************************************
        #if USE_ITEMPROC
            public static string SQL_GetTransactionStubs(long GlobalBatchID, int TransactionID, int TxnSequence) {

                StringBuilder sbSQL = new StringBuilder();

                sbSQL.Append(" SELECT");
                sbSQL.Append("      GlobalStubID,");
                sbSQL.Append("      BatchSequence,");
                sbSQL.Append("      Amount,");
                sbSQL.Append("      AccountNumber,");
                sbSQL.Append("      IsOMRDetected,");
                sbSQL.Append("      SystemType");
                sbSQL.Append(" FROM Batch");
                sbSQL.Append("      INNER JOIN Transactions ON");
                sbSQL.Append("          Batch.GlobalBatchID = Transactions.GlobalBatchID");
                sbSQL.Append("          INNER JOIN Stubs ON");
                sbSQL.Append("              Transactions.GlobalBatchID = Stubs.GlobalBatchID");
                sbSQL.Append("              AND Transactions.TransactionID = Stubs.TransactionID");
                sbSQL.Append(" WHERE");
                sbSQL.Append("      Batch.GlobalBatchID = " + GlobalBatchID);

                if(TransactionID >= 0) {
                    sbSQL.Append(" AND Transactions.TransactionID = " + TransactionID.ToString());
                } else {
                    sbSQL.Append(" AND Transactions.Sequence = " + TxnSequence.ToString());
                }

                sbSQL.Append(" ORDER BY Stubs.TransactionSequence ASC");

                return sbSQL.ToString();
            }
        #else
            public static string SQL_GetTransactionStubs(int BankID, 
                                                         int LockboxID, 
                                                         int BatchID, 
                                                         DateTime DepositDate, 
                                                         int TransactionID) {

                StringBuilder sbSQL = new StringBuilder();

                return("EXEC usp_GetTransactionStubs @parmSiteBankID = " + BankID.ToString() 
                                              + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                                              + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                              + "  , @parmBatchID = " + BatchID.ToString() 
                                              + "  , @parmTransactionID = " + TransactionID.ToString());
            }
        #endif

        //************************************************************************
        //* SQL_GetTransactionStubsDE
        //************************************************************************

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="TransactionID"></param>
        /// <param name="BatchSequence"></param>
        /// <param name="StubDEFieldList"></param>
        /// <returns></returns>
        public static string SQL_GetTransactionStubsDE(int BankID, 
                                                       int LockboxID, 
                                                       int BatchID, 
                                                       DateTime DepositDate, 
                                                       int TransactionID, 
                                                       int BatchSequence, 
                                                       string StubDEFieldList) {

            //return("EXEC usp_GetTransactionStubsDE @parmSiteBankID = " + BankID.ToString() 
            //                                + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
            //                                + "  , @parmBatchID = " + BatchID.ToString() 
            //                                + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
            //                                + "  , @parmTransactionID = " + TransactionID.ToString()
            //                                + "  , @parmBatchSequence = " + BatchSequence.ToString()
            //                                + "  , @parmStubDEFieldList = '" + StubDEFieldList + "'");


            StringBuilder sbSQL = new StringBuilder();
            ////string strFieldList;

            ////if(StubDEFieldList.Length > 0) {
            ////    if(StubDEFieldList.EndsWith(", ")) {
            ////        strFieldList = StubDEFieldList.Substring(0, StubDEFieldList.Length - 2);
            ////    }
            ////    strFieldList = StubDEFieldList;
            ////} else {
            ////    strFieldList = " OLTA.factDataEntryDetails.TransactionID," +
            ////                   " OLTA.factDataEntryDetails.BatchSequence," +
            ////                   " OLTA.dimDataEntryColumns.ColumnName AS FldTitle," +
            ////                   " OLTA.factDataEntryDetails.DataEntryValue";
            ////}
            
            sbSQL.Append(" DECLARE @parmSiteBankID int");
            sbSQL.Append(" DECLARE @parmSiteLockboxID int");
            sbSQL.Append(" DECLARE @parmBatchID int");
            sbSQL.Append(" DECLARE @parmDepositDate datetime");
            sbSQL.Append(" DECLARE @parmTransactionID int");
            sbSQL.Append(" DECLARE @parmBatchSequence int");
            sbSQL.Append("");
            sbSQL.Append(" SET @parmSiteBankID = " + BankID.ToString());
            sbSQL.Append(" SET @parmSiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append(" SET @parmBatchID = " + BatchID.ToString());
            sbSQL.Append(" SET @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'");
            sbSQL.Append(" SET @parmTransactionID = " + TransactionID.ToString());
            sbSQL.Append(" SET @parmBatchSequence = " + BatchSequence.ToString());
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append("      OLTA.dimDataEntryColumns.TableName,");
            sbSQL.Append("      OLTA.dimDataEntryColumns.FldName,");
            sbSQL.Append("      OLTA.dimDataEntryColumns.DataType               AS FldDataTypeEnum,");
            sbSQL.Append("      OLTA.factDataEntryDetails.DataEntryValue,");
            sbSQL.Append("      COALESCE(OLTA.dimDataEntryColumns.DisplayName, OLTA.dimDataEntryColumns.FldName) AS FldTitle");

            //sbSQL.Append(" SELECT " + strFieldList);
            sbSQL.Append(" FROM OLTA.dimDataEntryColumns");
            sbSQL.Append("      INNER JOIN OLTA.factDataEntryDetails ON");
            sbSQL.Append("          OLTA.dimDataEntryColumns.DataEntryColumnKey = OLTA.factDataEntryDetails.DataEntryColumnKey");
            sbSQL.Append("      INNER JOIN OLTA.dimLockboxes ON");
            sbSQL.Append("          OLTA.factDataEntryDetails.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append(" WHERE   OLTA.dimLockboxes.SiteBankID = @parmSiteBankID");
            sbSQL.Append("   AND OLTA.dimLockboxes.SiteLockboxID = @parmSiteLockboxID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.DepositDateKey = CAST(CONVERT(varchar,@parmDepositDate,112) as int)");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.BatchID = @parmBatchID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.TransactionID = @parmTransactionID");
            sbSQL.Append("   AND OLTA.factDataEntryDetails.BatchSequence = @parmBatchSequence");
            sbSQL.Append("   AND OLTA.dimDataEntryColumns.TableType IN (2, 3)");
            sbSQL.Append(" ORDER BY OLTA.factDataEntryDetails.TransactionID,");
            sbSQL.Append("   OLTA.factDataEntryDetails.BatchSequence,");
            sbSQL.Append("   OLTA.dimDataEntryColumns.ScreenOrder ASC");

            return(sbSQL.ToString());
        }

        //************************************************************************
        //* SQL_GetDocumentCount
        //************************************************************************
        public static string SQL_GetDocumentCount(int BankID, 
                                                  int LockboxID, 
                                                  DateTime StartDate, 
                                                  DateTime EndDate, 
                                                  bool DisplayScannedCheck) {

            return("EXEC usp_GetDocumentCount @parmSiteBankID = " + BankID.ToString()
                                         + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                         + ", @parmDepositDateStart = '" + StartDate.ToString("MM/dd/yyyy") + "'"
                                         + ", @parmDepositDateEnd = '" + EndDate.ToString("MM/dd/yyyy") + "'"
                                         + ", @parmDisplayScannedCheck = " + (DisplayScannedCheck ? "1" : "0"));
        }

        //************************************************************************
        //* SQL_GetTransactionDocuments
        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <param name="displayScannedCheck"></param>
        /// <returns></returns>
        public static string SQL_GetTransactionDocuments(int BankID, 
                                                         int LockboxID, 
                                                         int BatchID, 
                                                         DateTime DepositDate, 
                                                         int TransactionID, 
                                                         bool DisplayScannedCheck) {

            return("EXEC usp_GetTransactionDocuments @parmSiteBankID = " + BankID.ToString() 
                                              + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                                              + "  , @parmBatchID = " + BatchID.ToString() 
                                              + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                              + "  , @parmTransactionID = " + TransactionID.ToString()
                                              + "  , @parmDisplayScannedCheck = " + (DisplayScannedCheck ? "1" : "0"));

        }

        //************************************************************************
        //* SQL_GetTransactionDocumentsBySequence
        //************************************************************************

        /// <summary>
        /// 
        /// </summary>
        /// <param name="BankID"></param>
        /// <param name="LockboxID"></param>
        /// <param name="BatchID"></param>
        /// <param name="DepositDate"></param>
        /// <param name="TxnSequence"></param>
        /// <param name="DisplayScannedCheck"></param>
        /// <returns></returns>
        public static string SQL_GetTransactionDocumentsBySequence(int BankID, 
                                                                   int LockboxID, 
                                                                   int BatchID, 
                                                                   DateTime DepositDate, 
                                                                   int TxnSequence, 
                                                                   bool DisplayScannedCheck) {

            return("EXEC usp_GetTransactionDocumentsBySequence @parmSiteBankID = " + BankID.ToString() 
                                                        + "  , @parmSiteLockboxID = " + LockboxID.ToString() 
                                                        + "  , @parmBatchID = " + BatchID.ToString() 
                                                        + "  , @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                                        + "  , @parmTxnSequence = " + TxnSequence.ToString()
                                                        + "  , @parmDisplayScannedCheck = " + (DisplayScannedCheck ? "1" : "0"));
        }

        //************************************************************************
        //* SQL_GetDocument
        //************************************************************************

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalDocumentID"></param>
        /// <returns></returns>
        public static string SQL_GetDocument(int BankID,
                                             int LockboxID,
                                             DateTime DepositDate,
                                             int BatchID,
                                             int TransactionID,
                                             int BatchSequence,
                                             bool DisplayScannedCheck) {

            return("EXEC usp_GetDocument @parmSiteBankID = " + BankID.ToString()
                                    + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                    + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                    + ", @parmBatchID = " + BatchID.ToString()
                                    + ", @parmTransactionID = " + TransactionID.ToString()
                                    + ", @parmBatchSequence = " + BatchSequence.ToString()
                                    + ", @parmDisplayScannedCheck = " + (DisplayScannedCheck ? "1" : "0"));
        }

        //************************************************************************
        //* SQL_GetLockboxOnlineColorMode
        //************************************************************************
        public static string SQL_GetLockboxOnlineColorMode(int BankID,
                                                           int LockboxID) {
            return("EXEC usp_GetLockboxOnlineColorMode @parmSiteBankID = " + BankID.ToString()
                                                  + ", @parmSiteLockboxID = " + LockboxID.ToString());
        }

        //************************************************************************
        //* SQL_GetBatchLockbox
        //************************************************************************
        public static string SQL_GetBatchLockbox(int BankID, 
                                                 int LockboxID, 
                                                 int BatchID, 
                                                 DateTime DepositDate) {

            return("EXEC usp_GetBatchLockbox @parmSiteBankID = " + BankID.ToString()
                                        + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                        + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                        + ", @parmBatchID = " + BatchID.ToString());
        }

        //************************************************************************
        //* SQL_GetBatchTransactions
        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetBatchTransactions(int BankID, 
                                                      int LockboxID, 
                                                      DateTime DepositDate, 
                                                      int BatchID) {

            return("EXEC usp_GetBatchTransactions @parmSiteBankID = " + BankID.ToString()
                                             + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                             + ", @parmDepositDate = '" + DepositDate.ToString("MM/dd/yyyy") + "'"
                                             + ", @parmBatchID = " + BatchID.ToString());
        }

        //************************************************************************
        //* SQL_GetOLLockbox
        //************************************************************************
        /// <summary>
        /// 
        /// </summary>
        /// <param name="OLLockboxID"></param>
        /// <returns></returns>
        public static string SQL_GetOLLockbox(Guid OLLockboxID) {
            return("EXEC usp_GetOLLockbox @parmOLLockboxID = '" + OLLockboxID.ToString() + "'");
        }

        public static string SQL_GetSystemType(int BankID, int LockboxID, int BatchID, DateTime ProcessingDate)
        {
            StringBuilder sbSQL = new StringBuilder();
            sbSQL.AppendFormat("SELECT OLTA.factBatchSummary.SystemType " +
                    " FROM  OLTA.factBatchSummary" + 
                    "    INNER JOIN OLTA.dimLockboxes ON" + 
                    "       OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey " +
                    " WHERE (OLTA.dimLockboxes.SiteBankID = {0})" + 
                    "    AND (OLTA.dimLockboxes.SiteLockboxID = {1})" + 
                    "    AND (OLTA.factBatchSummary.BatchID = {2})" + 
                    "    AND (OLTA.factBatchSummary.ProcessingDateKey = {3})",
                BankID.ToString(), 
                LockboxID.ToString(), 
                BatchID.ToString(), 
                ProcessingDate.ToString("yyyyMMdd"));

            /*string strSQL;
             strSQL = "SELECT" +
                     "  SystemType" +
                     " FROM [OLTA].[factBatchSummary]" +
                     " WHERE BankKey=" + bankId        +
                     " AND  LockboxKey=" + lockboxId   +
                     " AND  BatchId=" + batchId        +
                     " AND  ProcessingDateKey='" + processingDate + "'";*/

            return (sbSQL.ToString());
        }






        // END CR 27092 MEH 09-30-2009 ICON service


        public static string SQL_GetKeysForBatch(int BankID, int LockboxID, DateTime ProcessingDate, int BatchID)
        {
            StringBuilder strSql = new StringBuilder();

            strSql.Append(" SELECT");
            strSql.Append(" 	OLTA.factBatchSummary.BankKey,");
            strSql.Append(" 	OLTA.factBatchSummary.LockboxKey,");
            strSql.Append(" 	OLTA.factBatchSummary.BatchSourceKey");
            strSql.Append(" FROM");
            strSql.Append(" 	OLTA.factBatchSummary");
            strSql.Append(" 		INNER JOIN OLTA.dimLockboxes ON");
            strSql.Append(" 			OLTA.factBatchSummary.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            strSql.Append(" WHERE");
            strSql.AppendFormat(" 	OLTA.factBatchSummary.ProcessingDateKey = {0}", ProcessingDate.ToString("yyyyMMdd"));
            strSql.AppendFormat(" 	AND OLTA.dimLockboxes.SiteBankID = {0}", BankID.ToString());
            strSql.AppendFormat(" 	AND OLTA.dimLockboxes.SiteLockboxID = {0}", LockboxID.ToString());
            strSql.AppendFormat(" 	AND OLTA.factBatchSummary.BatchID = {0}", BatchID.ToString());

            return strSql.ToString();
        }

        public static string SQL_GetCheck(int BankID, 
                                          int LockboxID, 
                                          int ProcessingDateKey, 
                                          int BatchID, 
                                          int BatchSequence) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @BankID int");
            sbSQL.Append(" DECLARE @LockboxID int");
            sbSQL.Append(" DECLARE @ProcessingDateKey int");
            sbSQL.Append(" DECLARE @BatchID int");
            sbSQL.Append(" DECLARE @BatchSequence int");
            sbSQL.Append("");
            sbSQL.Append(" SET @BankID = " + BankID.ToString());
            sbSQL.Append(" SET @LockboxID = " + LockboxID.ToString());
            sbSQL.Append(" SET @ProcessingDateKey = " + ProcessingDateKey.ToString());
            sbSQL.Append(" SET @BatchID = " + BatchID.ToString());
            sbSQL.Append(" SET @BatchSequence = " + BatchSequence.ToString());
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append("     OLTA.dimLockboxes.SiteBankID,");
            sbSQL.Append("     OLTA.dimLockboxes.SiteLockboxID,");
            sbSQL.Append("     OLTA.dimLockboxes.OnlineColorMode,");
            sbSQL.Append("     OLTA.factChecks.ProcessingDateKey,");
            sbSQL.Append("     OLTA.factChecks.DepositDateKey,");
            sbSQL.Append("     OLTA.factChecks.BatchID,");
            sbSQL.Append("     OLTA.factChecks.TransactionID,");
            sbSQL.Append("     OLTA.factChecks.TxnSequence,");
            sbSQL.Append("     OLTA.factChecks.BatchSequence,");
            sbSQL.Append("     'C' AS FileDescriptor,");
            sbSQL.Append("     OLTA.factChecks.CheckSequence,");
            sbSQL.Append("     OLTA.dimRemitters.RoutingNumber,");
            sbSQL.Append("     OLTA.dimRemitters.Account,");
            sbSQL.Append("     OLTA.factChecks.Serial,");
            sbSQL.Append("     OLTA.factChecks.Amount,");
            sbSQL.Append("     OLTA.dimRemitters.RemitterName");
            sbSQL.Append(" FROM OLTA.factChecks");
            sbSQL.Append("     INNER JOIN OLTA.dimLockboxes ON");
            sbSQL.Append("         OLTA.factChecks.lockboxKey = OLTA.dimLockboxes.lockboxKey");
            sbSQL.Append("     INNER JOIN OLTA.dimRemitters ON");
            sbSQL.Append("         OLTA.factChecks.RemitterKey = OLTA.dimRemitters.RemitterKey");
            sbSQL.Append(" WHERE");
            sbSQL.Append("     OLTA.dimLockboxes.SiteBankID = @BankID");
            sbSQL.Append("     AND OLTA.dimLockboxes.SiteLockboxID = @LockboxID");
            sbSQL.Append("     AND OLTA.factChecks.ProcessingDateKey = @ProcessingDateKey");
            sbSQL.Append("     AND OLTA.factChecks.BatchID = @BatchID");
            sbSQL.Append("     AND OLTA.factChecks.BatchSequence = @BatchSequence");

            return(sbSQL.ToString());
        }

        public static string SQL_GetDocument(int BankID, 
                                             int LockboxID, 
                                             int ProcessingDateKey, 
                                             int BatchID, 
                                             int BatchSequence) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @BankID int");
            sbSQL.Append(" DECLARE @LockboxID int");
            sbSQL.Append(" DECLARE @ProcessingDateKey int");
            sbSQL.Append(" DECLARE @BatchID int");
            sbSQL.Append(" DECLARE @BatchSequence int");
            sbSQL.Append("");
            sbSQL.Append(" SET @BankID = " + BankID.ToString());
            sbSQL.Append(" SET @LockboxID = " + LockboxID.ToString());
            sbSQL.Append(" SET @ProcessingDateKey = " + ProcessingDateKey.ToString());
            sbSQL.Append(" SET @BatchID = " + BatchID.ToString());
            sbSQL.Append(" SET @BatchSequence = " + BatchSequence.ToString());
            sbSQL.Append("");
            sbSQL.Append(" SELECT");
            sbSQL.Append("     OLTA.dimLockboxes.SiteBankID,");
            sbSQL.Append("     OLTA.dimLockboxes.SiteLockboxID,");
            sbSQL.Append("     OLTA.dimLockboxes.OnlineColorMode,");
            sbSQL.Append("     OLTA.factDocuments.ProcessingDateKey,");
            sbSQL.Append("     OLTA.factDocuments.DepositDateKey,");
            sbSQL.Append("     OLTA.factDocuments.BatchID,");
            sbSQL.Append("     OLTA.factDocuments.TransactionID,");
            sbSQL.Append("     OLTA.factDocuments.TxnSequence,");
            sbSQL.Append("     OLTA.factDocuments.BatchSequence,");
            sbSQL.Append("     OLTA.dimDocumentTypes.FileDescriptor");
            sbSQL.Append(" FROM OLTA.factDocuments");
            sbSQL.Append("     INNER JOIN OLTA.dimLockboxes ON");
            sbSQL.Append("         OLTA.factDocuments.LockboxKey = OLTA.dimLockboxes.LockboxKey");
            sbSQL.Append("     INNER JOIN olta.dimDocumentTypes ON");
            sbSQL.Append("         OLTA.factDocuments.DocumentTypeKey = OLTA.dimDocumentTypes.DocumentTypeKey");
            sbSQL.Append(" WHERE");
            sbSQL.Append("     OLTA.dimLockboxes.SiteBankID = @BankID");
            sbSQL.Append("     AND OLTA.dimLockboxes.SiteLockboxID = @LockboxID");
            sbSQL.Append("     AND OLTA.factDocuments.ProcessingDateKey = @ProcessingDateKey");
            sbSQL.Append("     AND OLTA.factDocuments.BatchID = @BatchID");
            sbSQL.Append("     AND OLTA.factDocuments.BatchSequence = @BatchSequence");

            return(sbSQL.ToString());
        }


        //WJS 3-10-2011 CR 31536 Start


        public static string SQL_GetStubData(int BankID,
                                           int LockboxID,
                                           int BatchID,
                                           DateTime DepositDate,
                                           DateTime ProcessingDate,
                                           int TransactionID,
                                           int BatchSourceKey) {

            return ("EXEC [OLTA].[usp_IMSTranItemReport_GetStubData]  @parmBankID = " + BankID.ToString()
                                           + ", @parmLockboxID = " + LockboxID.ToString()
                                           + ", @parmProcessingDateKey = " + ProcessingDate.ToString("yyyyMMdd")
                                           + ", @parmDepositDateKey = " + DepositDate.ToString("yyyyMMdd")
                                           + ", @parmBatchID = " + BatchID.ToString()
                                           + ", @parmTransactionID = " + TransactionID.ToString()
                                           + ", @parmBatchSourceKey = " + BatchSourceKey.ToString());

        }
           

        public static string SQL_GetCheckData(int BankID, 
                                          int LockboxID,
                                          int BatchID,  
                                          DateTime DepositDate,
                                          DateTime ProcessingDate, 
                                          int TransactionID,
                                          int BatchSourceKey) {

            return ("EXEC [OLTA].[usp_IMSTranItemReport_GetCheckData]  @parmBankID = " + BankID.ToString()
                                             + ", @parmLockboxID = " + LockboxID.ToString()
                                             + ", @parmProcessingDateKey = " + ProcessingDate.ToString("yyyyMMdd")
                                             + ", @parmDepositDateKey = " + DepositDate.ToString("yyyyMMdd")
                                             + ", @parmBatchID = " + BatchID.ToString()
                                             + ", @parmTransactionID = " + TransactionID.ToString()
                                             + ", @parmBatchSourceKey = " + BatchSourceKey.ToString());

        }
         
   
        public static string SQL_GetDataEntryData(int BankID, 
                                          int LockboxID,
                                          int BatchID,  
                                          DateTime DepositDate,
                                          DateTime ProcessingDate, 
                                          int TransactionID,
                                          int BatchSequence,
                                          int BatchSourceKey)  {
            return ("EXEC [OLTA].[usp_IMSTranItemReport_GetDataEntryData]  @parmBankID = " + BankID.ToString()
                                             + ", @parmLockboxID = " + LockboxID.ToString()
                                             + ", @parmProcessingDateKey = " + ProcessingDate.ToString("yyyyMMdd")
                                             + ", @parmDepositDateKey = " + DepositDate.ToString("yyyyMMdd")
                                             + ", @parmBatchID = " + BatchID.ToString()
                                             + ", @parmTransactionID = " + TransactionID.ToString()
                                             + ", @parmBatchSequence = " + BatchSequence.ToString()
                                             + ", @parmBatchSourceKey = " + BatchSourceKey.ToString());

        }

        public static string SQL_UpsertImageRPSAliasKeyword(int BankID, int LockboxID, int ExtractType,
                            string FieldType, int DocType, string AliasType)
        {
            return ("EXEC [OLTA].[usp_dimImageRPSAliasMappingsUpsert]  @parmSiteBankID = " + BankID.ToString()
                                             + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                             + ", @parmExtractType = " + ExtractType.ToString()
                                             + ", @parmDocType = " + DocType.ToString()
                                             + ", @parmFieldType = '" + FieldType.ToString() + "'"
                                             + ", @parmAliasName = '" + AliasType.ToString() + "'");
        }


        public static string SQL_GetStub(int BankID,
                                         int LockboxID,
                                         int ProcessingDateKey,
                                         int BatchID,
                                         int BatchSequence) {

            return ("EXEC [OLTA].[usp_GetStub]  @parmSiteBankID = " + BankID.ToString()
                                           + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                           + ", @parmProcessingDateKey = " + ProcessingDateKey.ToString()
                                           + ", @parmBatchID = " + BatchID.ToString()
                                           + ", @parmBatchSequence = " + BatchSequence.ToString());

        }

        public static string SQL_GetDocumentSequenceIMSMapping(int BankID,
                                                   int LockboxID,
                                                   int ProcessingDateKey,
                                                   int BatchID) {

            return ("EXEC [OLTA].[usp_GetDocumentSequenceIMSMapping]  @parmSiteBankID = " + BankID.ToString()
                                       + ", @parmSiteLockboxID = " + LockboxID.ToString()
                                       + ", @parmProcessingDateKey = " + ProcessingDateKey.ToString()
                                       + ", @parmBatchID = " + BatchID.ToString());
        }

        public static string SQL_GetLockboxSummary(int UserID, int DepositDateKey, bool UseCutoff) {

            return ("EXEC [OLTA].[usp_GetLockboxSummary]  @parmUserID = " + UserID.ToString()
                                       + ", @parmDepositDateKey = " + DepositDateKey.ToString()
                                       + ", @parmRespectCutoff = " + (UseCutoff ? "1" : "0"));
        }

        public static string SQL_GetLockbox(int BankID, int LockboxID) {
             return("EXEC usp_dimLockboxes_GetByID" + 
                    "    @parmSiteBankID = " + BankID.ToString() + ", " +
                    "    @parmSiteLockboxID = " + LockboxID.ToString());
        }
    }
}


