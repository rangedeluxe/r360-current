﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using WFS.RecHub.Common;
/*******************************************************************************
*
*    Module: DecisioningSQL
*  Filename: DecisioningSQL.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 12257 JMC 07/28/2005
*   -Initial release version.
* CR 12257 JMC 8/19/2005
*   -Modified queries updating decision status to not update if status is
*    unknown.
*   -Modified Auditfile insert SQL.
*   -Added Decisioning Item counts.
* CR 12257 JMC 8/22/2005
*   -Added SQL_GetRejectedDEItemCount method.
* CR 12257 JMC 8/25/2005
*   -Modified SQL_UpdateVertStubDE method to use the formatValue function.
* CR 14020 JMC 09/23/2005
*   -Corrected SQL_GetBatchDetails to select the DecisioningDeadline for the
*    proper date.
* CR 13911 JMC 09/23/2005
*   -Modified SQL_GetDecisioningBatch by adding BatchEndWork column to list of 
*    SELECT fields.
*   -Modified SQL_ResetDecisioningBatch by setting BatchEndWork when resetting
*    a Batch.
*   -Added SQL_CompleteDecisioningBatch function.
* CR 14213 JMC 10/11/2005
*   -Renamed "RejectBatchID" --> "RejectedBatchID"
*   -Renamed "GlobalBatchID" --> "OriginalGlobalBatchID"
* CR 14245 JMC 10/11/2005
*   -Added SQL_GetUnRejectedTxnCount function
* CR 14384 JMC 10/28/2005
*   -Modified SQL_GetStubs and SQL_GetDEItemFields functions to only return
*    Data Entry when DEStatus = Complete.
* CR 14377 JMC 10/28/2005
*   -Modified SQL_UpdateBatchDepositStatus to include a parameter that will
*    update MICRVerifyStatus = 1 when true.
* CR 14496 JMC 11/03/2005
*   -Modified SQL_GetUnKilledCheckCount function to only count Checks belonging
*    to Transactions that have not been rejected.
* CR 15184 JMC 01/16/2006
*   -Removed MICRVerifyIsComplete parameter from SQL_UpdateBatchDepositStatus 
*    function. 
* CR 15324 JMC 01/16/2006
*   -Modified queries that pull stubs to pull stubs that are either in Done
*    or Pending Verify.
* CR 15513 JMC 01/24/2006
*   -Replaced GlobalBatchID with BatchID when inserting into the AuditFile table.
* CR 15324 JMC 02/02/2006
*   -Added additional status when pulling stubs: IncompleteVerify.
* CR 12522 JMC 05/10/2006
*   -Modified SQL_GetDocuments to include PICSDate and Description
*   -Removed unused SQL_UpdateChecks and SQL_UpdateChecksDE functions.
* CR 15514 JMC 05/10/2006
*   -Modified SQL_GetDecisioningBatch function to include FName and LName.
*   -Modified SQL_InsertDecisioningBatches function to include FName and LName.
* CR 17324 JMC 08/18/2006
*   -Added Bank.IsImageExchange field to list of SELECT fields in GetBatch 
*    functions.
* CR 18607 JMC 11/27/2006
*   -Added SQL_GetRejectedTransactionCount() function.
*   -Removed SQL_GetUnRejectedTxnCount() function.
* CR 17138 JMC 02/07/2007
*   -Modified SQL_GetBatches function to return SiteCodes.LocalTimeZoneBias
*   -Modified SQL_GetBatchDetails function to return Lockbox.SiteCode and 
*    SitedCodes.LocalTimeZoneBias
* CR 28738 JMC 03/23/2010
*   -Added Lockbox.IsCommingled column to selected databaset for queries that
*    return Batch data.
*   -Added DestLockbox columns to selected dataset returned by 
*    SQL_GetTransactions
*   -Added SQL_UpdateTxnDecisionStatus function.
* CR 31649 JMC 10/26/2010
*   -Modify the Online Decisioning API to only return Batches WHERE 
*    Batch.ProcessingDate = SiteCodes.CurrentProcessingDate
* CR 45181 JCS 09/08/2011
*   -Added SQL_GetNewGlobalID to return new GlobalID for Online Balancing
*   -Added SQL_InsertStubsBalancingDE to create invoice balancing Stubs row
*   -Added SQL_InsertVertStubsBalancingRowDE to create invoice balancing
*    DEItemRowData row for invoice amount
*   -Added SQL_InsertVertStubsBalancingFieldDE to create invoice balancing
*    DEItemFieldData row for invoice amount
* CR 32602 JCS 10/26/2010
*   -Added SQL_GetStub to return specified vertical DEItemRowData item.
*   -Added BatchSequence column to SQL_GetStubs.
* CR 47719 JCS 11/07/2011
*   -Modified SQL_InsertStubsBalancingDE to use input parm for BatchSequence.
*   -Modified SQL_InsertVertStubsBalancingRowDE to use input parm BatchSequence.
*   -Added SQL_GetBatchMaxBatchSequence to return the maximum BatchSequence values.
* CR 32602 JCS 12/20/2011
*   -Modified SQL_GetStubs and SQL_GetStub to return BatchSequence from Stubs table.
*   -Added SQL_InsertAuditFile overload that includes item parameter.
* CR 52151 JMC 04/17/2012
*   -Modified SQL_GetUndefinedDestLbxCount() to de-reference dimLockboxesView.
* WI 90244 CRG 03/19/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
******************************************************************************/
namespace WFS.RecHub.DAL {

    /// <summary>
    /// 
    /// </summary>
    internal static class SQLDecisioning {

        private static string SQL_DefineSystemCurrentProcessingDate() {
            
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" DECLARE @SystemCurrentProcessingDate DateTime");
            sbSQL.Append(" SELECT @SystemCurrentProcessingDate = CurrentProcessingDate");
            sbSQL.Append(" FROM Systems");
            sbSQL.Append(" WHERE SystemName='CHECKBOX'");

            return(sbSQL.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankID"></param>
        /// <param name="customerID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="depositStatus"></param>
        /// <returns></returns>
        public static string SQL_GetBatches(long bankID, long customerID, long lockboxID, DepositStatusValues depositStatus) {

            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT"
                   + "    Lockbox.BankID"
                   + "  , Lockbox.CustomerID"
                   + "  , Lockbox.LockboxID"
                   + "  , Lockbox.SiteCode"
                   + "  , Lockbox.IsCommingled"
                   + "  , SiteCodes.LocalTimeZoneBias"
                   + "  , Batch.IsImageExchange"
                   + "  , Batch.GlobalBatchID"
                   + "  , Batch.Priority"
                   + "  , Batch.BatchID"
                   + "  , Batch.DepositDate"
                   + "  , Batch.ProcessingDate"
                   + "  , Batch.DepositStatus"
                   + "  , Batch.ScanStatus"
                   + "  , Batch.DecisionStatus"
                   + "  , Batch.UseCar"
                   + "  , (SELECT TOP 1"
                   + "         Schedule.ScheduleTime"
                   + "     FROM ScheduleActivities"
                   + "         INNER JOIN schedule ON"
                   + "             ScheduleActivities.ActivityID = Schedule.ActivityID"
                   + "     WHERE Schedule.BankID =  Lockbox.BankID"
                   + "         AND Schedule.CustomerID =  Lockbox.CustomerID"
                   + "         AND Schedule.LockboxID =  Lockbox.LockboxID"
                   + "         AND Schedule.DayOfWeek = (datepart(dw, getdate()) -1)"
                   + "         AND ScheduleActivities.Description = 'OLDDecisionDeadline' ) AS DecisioningDeadline"
                   + "  , Count(*)                          AS ItemsInBatch"
                   + " FROM Bank"
                   + "     INNER JOIN Lockbox"
                   + "         INNER JOIN Batch"
                   + "             LEFT OUTER JOIN Transactions ON"
                   + "                 Batch.GlobalBatchID = Transactions.GlobalBatchID ON"
                   + "             Lockbox.BankID = Batch.BankID"
                   + "             AND Lockbox.LockboxID = Batch.LockboxID ON"
                   + "         Bank.BankID = Lockbox.BankID"
                   + "     LEFT OUTER JOIN SiteCodes ON"
                   + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                   + " WHERE Batch.DepositStatus = " + ((int)depositStatus).ToString()
                   + "     AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)";

            if(bankID > -1) {
                strSQL += "    AND Lockbox.BankID = " + bankID.ToString();
            }

            if(customerID > -1) {
                strSQL += "    AND Lockbox.CustomerID = " + customerID.ToString();
            }

            if(lockboxID > -1) {
                strSQL += "    AND Lockbox.LockboxID = " + lockboxID.ToString();
            }

            strSQL += " GROUP BY"
                    + "    Lockbox.BankID"
                    + "  , Lockbox.CustomerID"
                    + "  , Lockbox.LockboxID"
                    + "  , Lockbox.SiteCode"
                    + "  , Lockbox.IsCommingled"
                    + "  , SiteCodes.LocalTimeZoneBias"
                    + "  , Batch.IsImageExchange"
                    + "  , Batch.GlobalBatchID"
                    + "  , Batch.Priority"
                    + "  , Batch.BatchID"
                    + "  , Batch.DepositDate"
                    + "  , Batch.ProcessingDate"
                    + "  , Batch.DepositStatus"
                    + "  , Batch.ScanStatus"
                    + "  , Batch.DecisionStatus"
                    + "  , Batch.UseCar"
                    + "  , Lockbox.SiteCode"
                    + " ORDER BY Lockbox.BankID"
                    + "  , Lockbox.CustomerID"
                    + "  , Lockbox.LockboxID"
                    + "  , Batch.Priority"
                    + "  , Batch.ProcessingDate"
                    + "  , Batch.BatchID";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetDecisioningBatch(long globalBatchID) {

            return(" SELECT TOP 1"
                 + "      DecisioningBatches.BatchResetID"
                 + "    , DecisioningBatches.LogonName"
                 + "    , DecisioningBatches.FirstName"
                 + "    , DecisioningBatches.LastName"
                 + "    , DecisioningBatches.BatchBeginWork"
                 + " FROM DecisioningBatches"
                 + " WHERE DecisioningBatches.GlobalBatchID = " + globalBatchID.ToString()
                 + "    AND DecisioningBatches.BatchReset = 0"
                 + "    AND DecisioningBatches.BatchEndWork IS NULL"
                 + " ORDER BY DecisioningBatches.BatchBeginWork DESC");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetBatchDetails(long globalBatchID) {
            
            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT"
                   + "    Lockbox.BankID"
                   + "  , Lockbox.CustomerID"
                   + "  , Lockbox.LockboxID"
                   + "  , Lockbox.SiteCode"
                   + "  , Lockbox.IsCommingled"
                   + "  , SiteCodes.LocalTimeZoneBias"
                   + "  , Batch.IsImageExchange"
                   + "  , Batch.BatchID"
                   + "  , Batch.GlobalBatchID"
                   + "  , Batch.Priority"
                   + "  , Batch.BatchID"
                   + "  , Batch.ProcessingDate"
                   + "  , Batch.DepositStatus"
                   + "  , Batch.ScanStatus"
                   + "  , Batch.DecisionStatus"
                   + "  , Batch.DepositDate"
                   + "  , Batch.DepositStatus"
                   + "  , Batch.DEStatus"
                   + "  , Batch.BatchMode"
                   + "  , Batch.SystemType"
                   + "  , Batch.UseCar"
                   + "  , (SELECT TOP 1"
                   + "         Schedule.ScheduleTime"
                   + "     FROM ScheduleActivities"
                   + "         INNER JOIN schedule ON"
                   + "             ScheduleActivities.ActivityID = Schedule.ActivityID"
                   + "     WHERE Schedule.BankID =  Lockbox.BankID"
                   + "         AND Schedule.CustomerID =  Lockbox.CustomerID"
                   + "         AND Schedule.LockboxID =  Lockbox.LockboxID"
                   + "         AND Schedule.DayOfWeek = (datepart(dw, getdate()) -1)"
                   + "         AND ScheduleActivities.Description = 'OLDDecisionDeadline' ) AS DecisioningDeadline"
                   + "  , Count(*)                          AS ItemsInBatch"
                   + " FROM Bank"
                   + "     INNER JOIN Lockbox"
                   + "         INNER JOIN Batch"
                   + "             INNER JOIN Transactions ON"
                   + "                 Batch.GlobalBatchID = Transactions.GlobalBatchID ON"
                   + "             Lockbox.BankID = Batch.BankID"
                   + "             AND Lockbox.LockboxID = Batch.LockboxID ON"
                   + "         Bank.BankID = Lockbox.BankID"
                   + "     LEFT OUTER JOIN SiteCodes ON"
                   + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                   + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                   + "     AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)"
                   + " GROUP BY"
                   + "    Lockbox.BankID"
                   + "  , Lockbox.CustomerID"
                   + "  , Lockbox.LockboxID"
                   + "  , Lockbox.SiteCode"
                   + "  , Lockbox.IsCommingled"
                   + "  , SiteCodes.LocalTimeZoneBias"
                   + "  , Batch.IsImageExchange"
                   + "  , Batch.BatchID"
                   + "  , Batch.GlobalBatchID"
                   + "  , Batch.Priority"
                   + "  , Batch.BatchID"
                   + "  , Batch.ProcessingDate"
                   + "  , Batch.DepositStatus"
                   + "  , Batch.ScanStatus"
                   + "  , Batch.DecisionStatus"
                   + "  , Batch.DepositDate"
                   + "  , Batch.DepositStatus"
                   + "  , Batch.DEStatus"
                   + "  , Batch.BatchMode"
                   + "  , Batch.SystemType"
                   + "  , Batch.UseCar";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetDESetupFields(long globalBatchID) {
            
            string strSQL;

            strSQL = " SELECT DISTINCT"
                   + "        RTrim(FldName) AS FldName"
                   + "      , RTrim(TableName) AS TableName"
                   + "      , CASE WHEN (DESetupFields.TableName = 'Stubs' OR DESetupFields.TableName = 'Checks') AND (DESetupFields.FldName = 'Amount') THEN 7 ELSE DESetupFields.FldDataTypeEnum END AS FldDataTypeEnum"
                   + "      , CASE WHEN DESetupFields.ReportTitle IS NULL THEN ISNULL(DESetupFields.FldPrompt, DESetupFields.FldName) ELSE DESetupFields.ReportTitle END AS Title"
                   + "      , DESetupFields.DESetupID"
                   + "      , DESetupFields.DocumentType"
                   + "      , DESetupFields.SequenceNumber"
                   + "      , DESetupFields.CheckDigitID"
                   + "      , DESetupFields.ScreenOrder"
                   + "      , DESetupFields.FldLength"
                   + "      , DESetupFields.FldPrompt"
                   + "      , DESetupFields.TypeRequired"
                   + "      , DESetupFields.TypeReadOnly"
                   + "      , DESetupFields.AllowDecisioning"
                   + "      , DESetupFields.DESetupFieldID"
                   + " FROM DESetupFields INNER JOIN Batch ON"
                   + "      DESetupFields.DESetupID = Batch.DESetupID"
                   + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                   + " ORDER By Title ASC";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetTransactions(long globalBatchID) {
            
            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT Transactions.GlobalBatchID"
                   + "      , Transactions.TransactionID"
                   + "      , Transactions.Sequence"
                   + "      , Transactions.TxnMode"
                   + "      , Transactions.DecisionSourceStatus"
                   + "      , Transactions.DestLockboxIdentifier"
                   + "      , Transactions.DestinationLockboxKey"
                   + "      , Transactions.DestinationBatchTypeID"
                   + " FROM Lockbox"
                   + "      INNER JOIN Batch"
                   + "          INNER JOIN Transactions ON"
                   + "              Batch.GlobalBatchID = Transactions.GlobalBatchID ON"
                   + "          Lockbox.BankID = Batch.BankID"
                   + "          AND Lockbox.LockboxID = Batch.LockboxID"
                   + "      LEFT OUTER JOIN SiteCodes ON"
                   + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                   + " WHERE"
                   + "      Batch.GlobalBatchID = " + globalBatchID.ToString()
                   + "     AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)"
                   + " ORDER BY Transactions.Sequence";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <param name="arDESetupFields"></param>
        /// <returns></returns>
        public static string SQL_GetChecks(long globalBatchID, long transactionID, List<cDESetupField> arDESetupFields) {
            
            string strSQL;
            bool bolJoinChecksDE = false;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT Checks.GlobalCheckID"
                   + "      , Checks.CheckSequence"
                   + "      , Checks.BatchSequence"
                   + "      , RTrim(Checks.RT)                  AS RT"
                   + "      , RTrim(Checks.Account)             AS Account"
                   + "      , RTrim(Checks.Serial)              AS Serial"
                   + "      , RTrim(Checks.TransactionCode)     AS TransactionCode"
                   + "      , Checks.Amount"
                   + "      , Checks.PayeeID"
                   + "      , (SELECT TOP 1 RTrim(PayeeName)"
                   + "         FROM Payee"
                   + "         WHERE Payee.BankID = Batch.BankID"
                   + "             AND Payee.LockboxID = Batch.LockboxID"
                   + "             AND Payee.IsValid = 1"
                   + "             AND Checks.PayeeID = Payee.PayeeID) AS PayeeName"
                   + "      , Checks.TraceNumber"
                   + "      , RTrim(Checks.RemitterName)        AS RemitterName"
                   + "      , DecisioningReasons.DecisioningReasonID"
                   + "      , DecisioningReasons.DecisioningReasonDesc";

            foreach(cDESetupField desetupfield in arDESetupFields) {
                switch(desetupfield.TableName.ToLower()) {
                    case("checks"):
                        strSQL += "      , Checks." + desetupfield.FldName + " AS Checks_" + desetupfield.FldName;
                        break;
                    case("checksdataentry"):
                        strSQL += "      , ChecksDataEntry." + desetupfield.FldName + " AS ChecksDataEntry_" + desetupfield.FldName;
                        bolJoinChecksDE = true;
                        break;
                    default:
                        break;
                }
            }

            strSQL+=" FROM Lockbox"
                  + "      INNER JOIN Batch"
                  + "          INNER JOIN Transactions"
                  + "              INNER JOIN Checks";

            if(bolJoinChecksDE) {
                strSQL+="             LEFT OUTER JOIN ChecksDataEntry ON"
                    + "                 Checks.GlobalCheckID = ChecksDataEntry.GlobalCheckID";
            }

            strSQL+="             LEFT OUTER JOIN LockboxDecisioningReasons"
                  + "                 LEFT OUTER JOIN DecisioningReasons ON"
                  + "                     LockboxDecisioningReasons.DecisioningReasonID = DecisioningReasons.DecisioningReasonID ON"
                  + "                 Checks.LockboxDecisioningReasonID = LockboxDecisioningReasons.LockboxDecisioningReasonID ON"
                  + "             Transactions.GlobalBatchID = Checks.GlobalBatchID"
                  + "         AND Transactions.TransactionID = Checks.TransactionID ON"
                  + "     Batch.GlobalBatchID = Transactions.GlobalBatchID ON"
                  + "          Lockbox.BankID = Batch.BankID"
                  + "          AND Lockbox.LockboxID = Batch.LockboxID"
                  + "      LEFT OUTER JOIN SiteCodes ON"
                  + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                  + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                  + "      AND Transactions.TransactionID = " + transactionID.ToString()
                  + "      AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_GetStubs(long globalBatchID, long transactionID) {
            
            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT"
                   + "      DEItemRowData.DEItemRowDataID"
                   + "    , DEItemRowData.RowNo"
                   + "    , Stubs.BatchSequence"
                   + " FROM Lockbox"
                   + "     INNER JOIN Batch"
                   + "         INNER JOIN DEItemRowData ON"
                   + "             Batch.GlobalBatchID = DEItemRowData.GlobalBatchID ON"
                   + "         Lockbox.BankID = Batch.BankID"
                   + "         AND Lockbox.LockboxID = Batch.LockboxID"
                   + "         INNER JOIN Stubs ON Stubs.GlobalStubID = DEItemRowData.GlobalStubID"
                   + "     LEFT OUTER JOIN SiteCodes ON"
                   + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                   + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                   + "      AND DEItemRowData.TransactionID = " + transactionID.ToString()
                   + "      AND DEItemRowData.DocumentType = " + ((int)DocumentTypes.Stub).ToString()
                   + "      AND Batch.DEStatus IN(" + ((int)DataEntryStatusValues.PendingVerify).ToString() 
                   + "                          , " + ((int)DataEntryStatusValues.IncompleteVerify).ToString()
                   + "                          , " + ((int)DataEntryStatusValues.DoneDataEntry).ToString() + ")"
                   + "     AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_GetDocuments(long globalBatchID, long transactionID) {
            
            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                   + " SELECT Documents.GlobalDocumentID"
                   + "      , Documents.DocumentSequence"
                   + "      , Documents.BatchSequence"
                   + "      , Documents.FileDescriptor"
                   + "      , Documents.ImageHeight"
                   + "      , Documents.ImageWidth"
                   + "      , Documents.IsColor"
                   + "      , Documents.TraceNumber"
                   + "      , ISNULL(RTrim(DocumentTypes.Description), 'Unknown') AS Description"
                   + "      , CONVERT(Char(8), Batch.ProcessingDate, 112)         AS PICSDate"
                   + " FROM Lockbox"
                   + "     INNER JOIN Batch"
                   + "     INNER JOIN Transactions ON"
                   + "         Batch.GlobalBatchID = Transactions.GlobalBatchID"
                   + "     INNER JOIN Documents ON"
                   + "             Transactions.GlobalBatchID = Documents.GlobalBatchID"
                   + "         AND Transactions.TransactionID = Documents.TransactionID ON"
                   + "         Lockbox.BankID = Batch.BankID"
                   + "         AND Lockbox.LockboxID = Batch.LockboxID"
                   + "     LEFT OUTER JOIN SiteCodes ON"
                   + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                   + "     LEFT OUTER JOIN DocumentTypes ON"
                   + "         Documents.FileDescriptor = DocumentTypes.FileDescriptor"
                   + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                   + "     AND Transactions.TransactionID = " + transactionID.ToString()
                   + "     AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <param name="docType"></param>
        /// <returns></returns>
        public static string SQL_GetDEItemFields(long globalBatchID, long transactionID, DocumentTypes docType) {
            string strSQL;

           strSQL = SQL_DefineSystemCurrentProcessingDate()
                 + " SELECT"
                 + "      DEItemRowData.DEItemRowDataID"
                 + "    , DEItemRowData.GlobalCheckID"
                 + "    , DEItemFieldData.DEItemFieldDataID"
                 + "    , DEItemFieldData.DESetupFieldID"
                 + "    , DEItemFieldData.ModifiedValue                         AS fldvalue"
                 + "    , DEItemFieldData.FieldDecisionStatus"
                 + "    , LockboxDecisioningReasons.LockboxDecisioningReasonId"
                 + "    , DecisioningReasons.DecisioningReasonDesc"
                 + "    , RTrim(DESetupFields.TableName)                        AS TableName"
                 + "    , RTrim(DESetupFields.FldName)                          AS FldName"
                 + "    , DESetupFields.FldLength"
                 + "    , DESetupFields.FldDataTypeEnum"
                 + "    , CASE WHEN (DESetupFields.TableName = 'Stubs' OR DESetupFields.TableName = 'Checks') AND (DESetupFields.FldName = 'Amount') THEN 7 ELSE DESetupFields.FldDataTypeEnum END AS FldDataTypeEnum"
                 + "    , CASE WHEN DESetupFields.ReportTitle IS NULL THEN ISNULL(DESetupFields.FldPrompt, DESetupFields.FldName) ELSE DESetupFields.ReportTitle END AS Title"
                 + " FROM Lockbox"
                 + "     INNER JOIN Batch"
                 + "          INNER JOIN DEItemRowData"
                 + "              INNER JOIN DEItemFieldData"
                 + "                  LEFT OUTER JOIN LockboxDecisioningReasons"
                 + "                      LEFT OUTER JOIN DecisioningReasons ON"
                 + "                          LockboxDecisioningReasons.DecisioningReasonId = DecisioningReasons.DecisioningReasonId ON"
                 + "                      DEItemFieldData.LockboxDecisioningReasonId = LockboxDecisioningReasons.LockboxDecisioningReasonId ON"
                 + "                    DEItemRowData.DEItemRowDataID = DEItemFieldData.DEItemRowDataID"
                 + "                  INNER JOIN DESetupFields ON"
                 + "                      DEItemFieldData.DESetupFieldID = DESetupFields.DESetupFieldID"
                 + "                  ON Batch.GlobalBatchID = DEItemRowData.GlobalBatchID"
                 + "              ON Lockbox.BankID = Batch.BankID"
                 + "              AND Lockbox.LockboxID = Batch.LockboxID"
                 + "     LEFT OUTER JOIN SiteCodes ON"
                 + "         Lockbox.SiteCode = SiteCodes.SiteCodeID"
                 + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString()
                 + "    AND DEItemRowData.TransactionID = " + transactionID.ToString()
                 + "    AND DEItemRowData.DocumentType = " + ((int)docType).ToString()
                 + "    AND Batch.DEStatus IN(" + ((int)DataEntryStatusValues.PendingVerify).ToString() 
                 + "                        , " + ((int)DataEntryStatusValues.IncompleteVerify).ToString()
                 + "                        , " + ((int)DataEntryStatusValues.DoneDataEntry).ToString() + ")"
                 + "    AND CONVERT(varchar(10), Batch.ProcessingDate, 101) = CONVERT(varchar(10), COALESCE(SiteCodes.CurrentProcessingDate, @SystemCurrentProcessingDate), 101)";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="newDepositStatus"></param>
        /// <returns></returns>
        public static string SQL_UpdateBatchDepositStatus(long globalBatchID, DepositStatusValues newDepositStatus) {

            string strSQL;

            strSQL = " UPDATE Batch"
                   + " SET Batch.DepositStatus = " + ((int)newDepositStatus).ToString()
                   + " WHERE Batch.GlobalBatchID = " + globalBatchID.ToString();

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="GlobalBatchID"></param>
        /// <param name="NumberTransactionsPended"></param>
        /// <param name="LogonName"></param>
        /// <param name="FirstName"></param>
        /// <param name="LastName"></param>
        /// <returns></returns>
        public static string SQL_InsertDecisioningBatches(long GlobalBatchID
                                                        , int NumberTransactionsPended
                                                        , string LogonName
                                                        , string FirstName
                                                        , string LastName) {
            string strSQL;

            strSQL = " INSERT INTO DecisioningBatches"
                   + " (BatchResetID, GlobalBatchID, NumberTransactionsPended, LogonName"
                   + "  , FirstName, LastName, BatchBeginWork, BatchReset)"
                   + " SELECT NewID(), " + GlobalBatchID.ToString() 
                   + ", " + NumberTransactionsPended.ToString() 
                   + ", '" + LogonName + "', '" + FirstName + "', '" + LastName + "', GetDate(), 0";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchResetID"></param>
        /// <returns></returns>
        public static string SQL_ResetDecisioningBatch(Guid batchResetID) {

            string strSQL;

            strSQL = " UPDATE DecisioningBatches"
                   + " SET BatchReset = 1"
                   + "  , BatchEndWork = GetDate()"
                   + " WHERE BatchResetID = '" + batchResetID.ToString() + "'";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="batchResetID"></param>
        /// <returns></returns>
        public static string SQL_CompleteDecisioningBatch(Guid batchResetID) {

            string strSQL;

            strSQL = " UPDATE DecisioningBatches"
                   + " SET BatchEndWork = GetDate()"
                   + " WHERE BatchResetID = '" + batchResetID.ToString() + "'";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <param name="newDecisionSrcStatus"></param>
        /// <returns></returns>
        public static string SQL_UpdateTxnDecisionStatus(long globalBatchID, 
                                                         long transactionID, 
                                                         DecisionSrcStatusTypes newDecisionSrcStatus,
                                                         Guid DestinationLockboxKey) {

            /*string strSQL;

            strSQL = " UPDATE Transactions"
                   + " SET Transactions.DecisionSourceStatus = " + ((int)newDecisionSrcStatus).ToString()
                   + " WHERE Transactions.GlobalBatchID = " + globalBatchID.ToString()
                   + "      AND Transactions.TransactionID = " + transactionID.ToString()
                   + "      AND Transactions.DecisionSourceStatus <> " + ((int)newDecisionSrcStatus).ToString();

            return(strSQL);*/
            return(" EXEC usp_OLDe_UpdateTxnDecisionStatus" +
                   "     @parmGlobalBatchID = " + globalBatchID.ToString() + "," +
                   "     @parmTransactionID = " + transactionID.ToString() + "," +
                   "     @DecisionSrcStatus = " + ((int)newDecisionSrcStatus).ToString() + "," +
                   "     @DestinationLockboxKey = " + (DestinationLockboxKey == Guid.Empty ? "NULL" : "'" + DestinationLockboxKey.ToString() + "'"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deItemRowDataID"></param>
        /// <returns></returns>
        public static string SQL_DeleteStub(Guid deItemRowDataID) {

            return(" DELETE"
                 + " FROM Stubs"
                 + " WHERE GlobalStubID IN("
                 + "     SELECT GlobalStubID" 
                 + "     FROM DEItemRowData"
                 + "     WHERE DEItemRowData.DEItemRowDataID = '" + deItemRowDataID.ToString() + "')");

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deItemRowDataID"></param>
        /// <returns></returns>
        public static string SQL_DeleteVerticalStub(Guid deItemRowDataID) {

            return(" DELETE FROM DEItemRowData WHERE DEItemRowDataID = '" + deItemRowDataID.ToString() + "'");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deItemRowDataID"></param>
        /// <param name="arDEFields"></param>
        /// <param name="arDESetupFields"></param>
        /// <returns></returns>
        public static string SQL_UpdateStubs(Guid deItemRowDataID, List<cDEField> arDEFields, List<cDESetupField> arDESetupFields) {

            string strSQL;
            string strSET = "";

            strSQL = " UPDATE Stubs SET ";
            strSET = BuildDataEntrySetClause("Stubs", arDEFields, arDESetupFields);

            if(strSET.Length > 0) {
                strSQL += strSET;
                strSQL += " FROM Stubs"
                        + "     INNER JOIN DEItemRowData ON"
                        + "         Stubs.GlobalStubID = DEItemRowData.GlobalStubID"
                        + " WHERE DEItemRowData.DEItemRowDataID = '" + deItemRowDataID.ToString() + "'";
            } else {
                strSQL = "";
            }

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="deItemRowDataID"></param>
        /// <param name="arDEFields"></param>
        /// <param name="arDESetupFields"></param>
        /// <returns></returns>
        public static string SQL_UpdateStubsDE(Guid deItemRowDataID, List<cDEField> arDEFields, List<cDESetupField> arDESetupFields) {

            string strSQL;
            string strSET;

            strSQL = " UPDATE StubsDataEntry SET ";
            strSET = BuildDataEntrySetClause("StubsDataEntry", arDEFields, arDESetupFields);

            if(strSET.Length > 0) {
                strSQL += strSET;
                strSQL += " FROM StubsDataEntry"
                        + "     INNER JOIN DEItemRowData ON"
                        + "         StubsDataEntry.GlobalStubID = DEItemRowData.GlobalStubID"
                        + " WHERE DEItemRowData.DEItemRowDataID = '" + deItemRowDataID.ToString() + "'";
            } else {
                strSQL = "";
            }

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="arDEFields"></param>
        /// <param name="arDESetupFields"></param>
        /// <returns></returns>
        private static string BuildDataEntrySetClause(string tableName, List<cDEField> arDEFields, List<cDESetupField> arDESetupFields) {

            string strSET = "";

            foreach(cDEField defield in arDEFields) {
                if(defield.TableName.ToLower().CompareTo(tableName.ToLower()) == 0) {
                    switch(defield.FldAction) {
                        case(FieldActions.Delete):
                            if(strSET.Length > 0) {
                                strSET += "     ,";
                            }
                            strSET += defield.FldName + " = NULL";
                            break;
                        case(FieldActions.Update):
                            foreach(cDESetupField deSetupField in arDESetupFields) {
                                if((deSetupField.TableName.ToLower().CompareTo(defield.TableName.ToLower()) == 0) && 
                                    (deSetupField.FldName.ToLower().CompareTo(defield.FldName.ToLower()) == 0)) {

                                    if(strSET.Length > 0) {
                                        strSET += "     ,";
                                    }

                                    if(defield.FldValue.Trim().Length > 0) {
                                        switch(deSetupField.FldDataTypeEnum) {
                                            case 6:     //  6=float
                                                strSET += defield.FldName + " = " + formatValue(defield.FldValue);
                                                break;
                                            case 7:     //  7=currency
                                                try {
                                                    strSET += defield.FldName + " = " + System.Convert.ToDecimal(defield.FldValue).ToString();
                                                } catch {
                                                    strSET += defield.FldName + " = '" + formatValue(defield.FldValue) + "'";
                                                }
                                                break;
                                            case 11:    // 11=date
                                                strSET += defield.FldName + " = '" + formatValue(defield.FldValue) + "'";
                                                break;
                                            default:    //  1=string
                                                strSET += defield.FldName + " = '" + formatValue(defield.FldValue) + "'";
                                                break;
                                        }
                                    } else {
                                        strSET += defield.FldName + " = NULL";
                                    }
                                }
                            }
                            break;
                    }
                }
            }

            return(strSET);
////return(string.Empty);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stub"></param>
        /// <param name="defield"></param>
        /// <returns></returns>
        public static string SQL_UpdateVertStubDE(long GlobalBatchID, 
                                                  long TransactionID,
                                                  Guid DEItemRowDataID, 
                                                  string TableName, 
                                                  string FldName, 
                                                  string FldValue, 
                                                  FieldDecisionStatusTypes FieldDecisionStatus) {

            string strSQL = " UPDATE DEItemFieldData";

            if(FldValue.Trim().Length > 0) {
                strSQL +=" SET ModifiedValue = '" + formatValue(FldValue) + "'";
            } else {
                strSQL += " SET ModifiedValue = NULL";
            }

            if(FieldDecisionStatus != FieldDecisionStatusTypes.Undefined) {
                strSQL += "    , FieldDecisionStatus = " + ((int)FieldDecisionStatus).ToString();
            }

            strSQL +=" FROM DEItemRowData"
                   + "     INNER JOIN DEItemFieldData"
                   + "         INNER JOIN DESetupFields ON"
                   + "         DEItemFieldData.DESetupFieldID = DESetupFields.DESetupFieldID ON"
                   + "     DEItemRowData.DEItemRowDataID = DEItemFieldData.DEItemRowDataID"
                   + " WHERE DEItemRowData.GlobalBatchID = " + GlobalBatchID.ToString()
                   + "     AND DEItemRowData.TransactionID = " + TransactionID.ToString()
                   + "     AND DESetupFields.TableName = '" + TableName + "'"
                   + "     AND DESetupFields.FldName = '" + FldName + "'"
                   + "     AND DEItemRowData.DEItemRowDataID = '" + DEItemRowDataID.ToString() + "'";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="stub"></param>
        /// <param name="defield"></param>
        /// <returns></returns>
        public static string SQL_InsertVertStubDE(Guid DEItemRowDataID, string TableName, string FldName, string FldValue) {

            string strSQL;

            strSQL = " INSERT INTO DEItemFieldData"
                   + "    (DEItemRowDataID"
                   + "   , DESetupFieldID"
                   + "   , OriginalValue"
                   + "   , ModifiedValue"
                   + "   , VerifiedValue"
                   + "   , ConfidenceValues"
                   + "   , LockboxDecisioningReasonId)"
                   + " SELECT"
                   + "     DEItemRowData.DEItemRowDataID"
                   + "   , DESetupFields.DESetupFieldID"
                   + "   , '" + FldValue + "'"
                   + "   , '" + FldValue + "'"
                   + "   , NULL"
                   + "   , NULL"
                   + "   , NULL"
                   + " FROM DEItemRowData"
                   + "     INNER JOIN Batch"
                   + "         INNER JOIN DESetupFields ON"
                   + "             Batch.DESetupID = DESetupFields.DESetupID ON"
                   + "         DEItemRowData.GlobalBatchID = Batch.GlobalBatchID"
                   + " WHERE DEItemRowDataID = '" + DEItemRowDataID.ToString() + "'"
                   + "     AND DESetupFields.FldName = '" + FldName + "'"    
                   + "     AND DESetupFields.TableName = '" + TableName + "'";

            return(strSQL);
        }

        /// <summary>
        /// Used by Invoice Balancing to ensure GlobalID does not exist in database.
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="fieldName"></param>
        /// <param name="globalID"></param>
        /// <returns></returns>
        public static string SQL_GetNewGlobalID(string tableName, string fieldName, long globalID) {

            string strSQL;

            strSQL = " SELECT " + globalID.ToString() + " FROM " + tableName + " WHERE " + fieldName + "=" + globalID.ToString();

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <param name="globalStubID"></param>
        /// <param name="invoiceAmount"></param>
        /// <returns></returns>
        public static string SQL_InsertStubsBalancingDE(long globalBatchID, long transactionID, long globalStubID, long batchSequence, decimal invoiceAmount) {

            string strSQL;

            strSQL = " INSERT INTO Stubs"
                   + "    (GlobalBatchID"
                   + "   , TransactionID"
                   + "   , GlobalStubID"
                   + "   , OriginalGlobalStubID"
                   + "   , TransactionSequence"
                   + "   , BatchSequence"
                   + "   , StubSequence"
                   + "   , Amount)"
                   + " SELECT"
                   + "    " + globalBatchID.ToString() + ", " + transactionID.ToString() + ", " + globalStubID.ToString() + ", " + globalStubID.ToString()
                   + "   , (SELECT MAX(TransactionSequence)+1 FROM Stubs WHERE GlobalBatchID=" + globalBatchID.ToString() + " AND TransactionID=" + transactionID.ToString() + ")"
                   + "   , " + batchSequence.ToString()
                   + "   , (SELECT MAX(StubSequence)+1 FROM Stubs WHERE GlobalBatchID=" + globalBatchID.ToString() + ")"
                   + "   , " + invoiceAmount.ToString("###############.00");

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidDEItemRowDataID"></param>
        /// <param name="globalBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_InsertVertStubsBalancingRowDE(Guid guidDEItemRowDataID, long globalBatchID, long transactionID, long globalStubID, long batchSequence) {

            string strSQL;

            strSQL = " INSERT INTO DEItemRowData"
                   + "    (DEItemRowDataID"
                   + "   , GlobalBatchID"
                   + "   , TransactionID"
                   + "   , GlobalStubID"
                   + "   , BatchSequence"
                   + "   , ImageSequence"
                   + "   , RowNo)"
                   + " SELECT"
                   + "     '" + guidDEItemRowDataID.ToString() + "'"
                   + "   , " + globalBatchID.ToString() 
                   + "   , " + transactionID.ToString()
                   + "   , " + globalStubID.ToString()
                   + "   , " + batchSequence.ToString()
                   + "   , 0"
                   + "   , (SELECT MAX(RowNo)+1 FROM DEItemRowData WHERE GlobalBatchID=" + globalBatchID + " and TransactionID=" + transactionID + ")";

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DEItemFieldDataID"></param>
        /// <param name="DEItemRowDataID"></param>
        /// <param name="DESetupFieldID"></param>
        /// <param name="invoiceAmount"></param>
        /// <returns></returns>
        public static string SQL_InsertVertStubsBalancingFieldDE(Guid DEItemFieldDataID, Guid DEItemRowDataID, Guid DESetupFieldID, decimal invoiceAmount) {

            string strSQL;

            strSQL = "INSERT INTO DEItemFieldData"
                   + "   (DEItemFieldDataID"
                   + "  , DEItemRowDataID"
                   + "  , DESetupFieldID"
                   + "  , OriginalValue"
                   + "  , ModifiedValue)"
                   + " SELECT"
                   + "    '" + DEItemFieldDataID.ToString() + "'"
                   + "  , '" + DEItemRowDataID.ToString() + "'"
                   + "  , '" + DESetupFieldID.ToString() + "'"
                   + "  , ''"
                   + "  , " + invoiceAmount.ToString("###############.00");

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetBatchMaxBatchSequence(long globalBatchID) {

            string strSQL;

            strSQL = "SELECT"
                    + "     (SELECT MAX(Checks.BatchSequence) FROM Checks WHERE Checks.GlobalBatchID=" + globalBatchID.ToString() + ") as MaxChecksBatchSequence"
                    + "   , (SELECT MAX(Stubs.BatchSequence) FROM Stubs WHERE Stubs.GlobalBatchID=" + globalBatchID.ToString() + ") as MaxStubsBatchSequence"
                    + "   , (SELECT MAX(Documents.BatchSequence) FROM Documents WHERE Documents.GlobalBatchID=" + globalBatchID.ToString() + ") as MaxDocumentsBatchSequence";

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="DEItemRowDataID"></param>
        /// <returns></returns>
        public static string SQL_GetStub(Guid DEItemRowDataID) {

            string strSQL;

            strSQL = "SELECT"
                   + "      DEItemRowData.DEItemRowDataID"
                   + "    , DEItemRowData.RowNo"
                   + "    , Stubs.BatchSequence"
                   + " FROM DEItemRowData"
                   + "     INNER JOIN Stubs ON Stubs.GlobalStubID=DEItemRowData.GlobalStubID"
                   + " WHERE DEItemRowData.DEItemRowDataID = '" + DEItemRowDataID.ToString("B") + "'";

            return (strSQL);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetUnKilledCheckCount(long globalBatchID) {

            string strSQL;

            strSQL = " SELECT Count(*) AS theCount"
                   + " FROM Transactions"
                   + "     INNER JOIN Checks ON"
                   + "         Transactions.GlobalBatchID = Checks.GlobalBatchID"
                   + "         AND Transactions.TransactionID = Checks.TransactionID"
                   + " WHERE Transactions.GlobalBatchID = " + globalBatchID.ToString()
                   + "     AND Checks.killedmethod = 0"
                   + "     AND Transactions.DecisionSourceStatus <> " + ((int)DecisionSrcStatusTypes.Reject).ToString();

            return(strSQL);
        }
        
        public static string SQL_GetRejectedTransactionCount(long globalBatchID) {

            string strSQL;

            strSQL = " SELECT Count(*) AS theCount"
                   + " FROM Transactions"
                   + " WHERE Transactions.GlobalBatchID = " + globalBatchID.ToString()
                   + "     AND Transactions.DecisionSourceStatus = " + ((int)DecisionSrcStatusTypes.Reject).ToString();

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="bankID"></param>
        /// <param name="customerID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="processingDateFrom"></param>
        /// <param name="processingDateTo"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedTransactions(long bankID
                                                       , long customerID
                                                       , long lockboxID
                                                       , DateTime processingDateFrom
                                                       , DateTime processingDateTo) {
            string strSQL;

            strSQL = " SELECT"
                   + "     RejectedBatch.RejectedBatchID"
                   + "   , RejectedBatch.OriginalGlobalBatchID"
                   + "   , RejectedBatch.BankID"
                   + "   , Lockbox.CustomerID"
                   + "   , RejectedBatch.LockboxID"
                   + "   , RejectedBatch.ProcessingDate"
                   + "   , RejectedBatch.BatchID"
                   + "   , RejectedBatch.SystemType"
                   + "   , RejectedBatch.ConsolidationStatus"
                   + "   , RejectedBatch.DepositDate"
                   + "   , RejectedBatch.CreationDate"
                   + "   , RejectedBatch.WorkgroupID"
                   + "   , RejectedBatch.SiteCode"
                   + "   , RejectedBatch.BitonalImageRemovalDate"
                   + "   , RejectedBatch.DataRemovalDate"
                   + "   , RejectedBatch.ModificationDate"
                   + "   , RejectedBatch.GrayScaleImageRemovalDate"
                   + "   , RejectedBatch.ColorImageRemovalDate"
                   + "   , RejectedBatch.CWDBDataRemovalDate"
                   + "   , RejectedBatch.CWDBBitonalImageRemovalDate"
                   + "   , RejectedBatch.CWDBGrayScaleImageRemovalDate"
                   + "   , RejectedBatch.CWDBColorImageRemovalDate"
                   + "   , RejectedBatch.BatchTypeID"
                   + "   , RejectedTransactions.TransactionID"
                   + "   , RejectedTransactions.Sequence"
                   + "   , RejectedTransactions.DecisionSourceStatus"
                   + "   , RejectedTransactions.CreationDate"
                   + "   , RejectedTransactions.ModificationDate"
                   + " FROM Lockbox INNER JOIN RejectedBatch"
                   + "      INNER JOIN RejectedTransactions ON"
                   + "          RejectedBatch.RejectedBatchID = RejectedTransactions.RejectedBatchID ON"
                   + "              Lockbox.BankID = RejectedBatch.BankID"
                   + "              AND Lockbox.LockboxID = RejectedBatch.LockboxID"
                   + "    WHERE Lockbox.BankID = " + bankID.ToString();

            if(customerID > -1) {
                strSQL += "    AND Lockbox.CustomerID = " + customerID.ToString();
            }

            if(lockboxID > -1) {
                strSQL += "    AND Lockbox.LockboxID = " + lockboxID.ToString();
            }

            if(processingDateFrom > DateTime.MinValue) {
                strSQL += "    AND RejectedBatch.ProcessingDate >= '" + processingDateFrom.ToString("d") + " 00:00:00.000'";
            }

            if(processingDateTo > DateTime.MinValue) {
                strSQL += "    AND RejectedBatch.ProcessingDate <= '" + processingDateTo.ToString("d") + " 23:59:59.997'";
            }

            strSQL +=" ORDER BY"
                   + "     RejectedBatch.BankID"
                   + "   , RejectedBatch.LockboxID"
                   + "   , RejectedBatch.DepositDate"
                   + "   , RejectedBatch.BatchID"
                   + "   , RejectedTransactions.TransactionID";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rejectedBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedBatch(Guid rejectedBatchID) {

            string strSQL;

            strSQL = " SELECT"
                   + "   RejectedBatch.RejectedBatchID"
                   + " , RejectedBatch.OriginalGlobalBatchID"
                   + " , RejectedBatch.BankID"
                   + " , Lockbox.CustomerID"
                   + " , RejectedBatch.LockboxID"
                   + " , RejectedBatch.ProcessingDate"
                   + " , RejectedBatch.BatchID"
                   + " , RejectedBatch.SystemType"
                   + " , RejectedBatch.ConsolidationStatus"
                   + " , RejectedBatch.DepositDate"
                   + " , RejectedBatch.CreationDate"
                   + " , RejectedBatch.WorkgroupID"
                   + " , RejectedBatch.SiteCode"
                   + " , RejectedBatch.BitonalImageRemovalDate"
                   + " , RejectedBatch.DataRemovalDate"
                   + " , RejectedBatch.ModificationDate"
                   + " , RejectedBatch.GrayScaleImageRemovalDate"
                   + " , RejectedBatch.ColorImageRemovalDate"
                   + " , RejectedBatch.CWDBDataRemovalDate"
                   + " , RejectedBatch.CWDBBitonalImageRemovalDate"
                   + " , RejectedBatch.CWDBGrayScaleImageRemovalDate"
                   + " , RejectedBatch.CWDBColorImageRemovalDate"
                   + " , RejectedBatch.BatchTypeID"
                   + " FROM"
                   + "     Lockbox INNER JOIN RejectedBatch ON"
                   + "      Lockbox.BankID = RejectedBatch.BankID"
                   + "      AND Lockbox.LockboxID = RejectedBatch.LockboxID"
                   + " WHERE RejectedBatch.RejectedBatchID = '" + rejectedBatchID.ToString() + "'";

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rejectedBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedTransaction(Guid rejectedBatchID, long transactionID) {

            string strSQL;

            strSQL = " SELECT"
                   + "      RejectedBatch.RejectedBatchID"
                   + "    , RejectedTransactions.TransactionID"
                   + "    , RejectedTransactions.Sequence"
                   + "    , RejectedTransactions.DecisionSourceStatus"
                   + "    , RejectedTransactions.CreationDate"
                   + "    , RejectedTransactions.ModificationDate"
                   + " FROM"
                   + "      RejectedBatch"
                   + "      INNER JOIN RejectedTransactions ON "
                   + "          RejectedBatch.RejectedBatchID = RejectedTransactions.RejectedBatchID"
                   + " WHERE RejectedBatch.RejectedBatchID = '" + rejectedBatchID.ToString() + "'"
                   + "      AND RejectedTransactions.TransactionID = " + transactionID.ToString();

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rejectedBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedChecks(Guid rejectedBatchID
                                                 , long transactionID) {
            string strSQL;

            strSQL = " SELECT"
                   + "   RejectedChecks.RejectedBatchID"
                   + " , RejectedChecks.GlobalCheckID"
                   + " , RejectedChecks.TransactionID"
                   + " , RejectedChecks.TransactionSequence"
                   + " , RejectedChecks.BatchSequence"
                   + " , RejectedChecks.CheckSequence"
                   + " , RejectedChecks.RawMICR"
                   + " , RTrim(RejectedChecks.RT)                   AS RT"
                   + " , RTrim(RejectedChecks.Serial)               AS Serial"
                   + " , RTrim(RejectedChecks.Account)              AS Account"
                   + " , RTrim(RejectedChecks.TransactionCode)      AS TransactionCode"
                   + " , RejectedChecks.Amount"
                   + " , RejectedChecks.PayeeID"
                   + " , RejectedChecks.IsForeign"
                   + " , RejectedChecks.IsDeleted"
                   + " , RejectedChecks.RemitterName"
                   + " , RejectedChecks.[Timestamp]"
                   + " , RejectedChecks.TraceNumber"
                   + " , RejectedChecks.IsIRD"
                   + " , RejectedChecks.LockboxDecisioningReasonId"
                   + " FROM"
                   + "      RejectedChecks"
                   + " WHERE RejectedBatchId = '" + rejectedBatchID.ToString() + "'"
                   + "      AND TransactionID = " + transactionID.ToString();

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="rejectedBatchID"></param>
        /// <param name="transactionID"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedDocuments(Guid rejectedBatchID
                                                    , long transactionID) {
            string strSQL;

            strSQL = " SELECT"
                   + "        RejectedBatchID"
                   + "      , TransactionID"
                   + "      , GlobalDocumentID"
                   + "      , TransactionSequence"
                   + "      , DocumentSequence"
                   + "      , FileDescriptor"
                   + "      , BatchSequence"
                   + "      , ImageHeight"
                   + "      , ImageWidth"
                   + "      , IsColor"
                   + "      , TraceNumber"
                   + " FROM"
                   + "      RejectedDocuments"
                   + " WHERE RejectedBatchId = '" + rejectedBatchID.ToString() + "'"
                   + "      AND TransactionID = " + transactionID.ToString();

            return(strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="eventDesc"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="processingDate"></param>
        /// <returns></returns>
        public static string SQL_InsertAuditFile(AuditFileEventTypes eventType
                                               , string eventDesc
                                               , long bankID
                                               , long lockboxID
                                               , long batchID
                                               , DateTime processingDate) {

            return (SQL_InsertAuditFile(eventType, eventDesc, bankID, lockboxID, batchID, null, processingDate));
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventType"></param>
        /// <param name="eventDesc"></param>
        /// <param name="bankID"></param>
        /// <param name="lockboxID"></param>
        /// <param name="batchID"></param>
        /// <param name="item"></param>
        /// <param name="processingDate"></param>
        /// <returns></returns>
        public static string SQL_InsertAuditFile(AuditFileEventTypes eventType
                                               , string eventDesc
                                               , long bankID
                                               , long lockboxID
                                               , long batchID
                                               , long? item
                                               , DateTime processingDate) {
            string strSQL;

            strSQL = " INSERT INTO AuditFile"
                 + "     (AuditFileID"
                 + "    , ModuleName"
                 + "    , Event"
                 + "    , TimeStamp"
                 + "    , WorkStation"
                 + "    , Operator"
                 + "    , BankID"
                 + "    , LockboxID"
                 + "    , Batch"
                 + "    , Item"
                 + "    , Description"
                 + "    , ProcessingDate)"
                 + " VALUES"
                 + "     ('" + Guid.NewGuid().ToString() + "'"
                 + "    , 'OLDecisioningAPI'"
                 + "    , '" + eventType.ToString() + "'"
                 + "    , GetDate()"
                 + "    , NULL"
                 + "    , NULL";

            if (bankID > -1) {
                strSQL += "    , " + bankID.ToString();
            } else {
                strSQL += "    , NULL";
            }

            if (lockboxID > -1) {
                strSQL += "    , " + lockboxID.ToString();
            } else {
                strSQL += "    , NULL";
            }

            strSQL += "    , " + batchID.ToString();

            if (item.HasValue) {
                strSQL += "    , " + item.ToString();
            } else {
                strSQL += "    , NULL";
            }

            strSQL += "    , '" + eventDesc + "'";
            
            if (processingDate > DateTime.MinValue) {
                strSQL += "    , '" + processingDate.ToString("G") + "')";
            } else {
                strSQL += "    , NULL)";
            }

            return (strSQL);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string SQL_GetCurrentProcessingDate() {

            return(" SELECT"
                 + "     CurrentProcessingDate"
                 + " FROM Systems"
                 + " WHERE SystemName = 'CHECKBOX'");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventRuleID"></param>
        /// <param name="globalBatchID"></param>
        /// <param name="processingDate"></param>
        /// <returns></returns>
        public static string SQL_InsertIntoEventLog(Guid eventRuleID
                                                  , long globalBatchID
                                                  , DateTime processingDate) {
            return("INSERT INTO EventLog("
                 + "     EventLogID"
                 + "   , EventRuleID"
                 + "   , EventStatus"
                 + "   , EventData"
                 + "   , ProcessingDate)"
                 + "  VALUES("
                 + "     NEWID()"
                 + "   , '" + eventRuleID.ToString() + "'"
                 + "   , 0"
                 + "   , '<GlobalBatchID>" + globalBatchID.ToString() + "'"
                 + "   , '" + processingDate.ToString() + "')");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="eventName"></param>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetEventRules(string eventName
                                             , long globalBatchID) {

            return(" SELECT"
                 + "    EventRules.EventRuleID"
                 + " FROM EventRules"
                 + "    INNER JOIN Events ON"
                 + "        EventRules.EventID = Events.EventID"
                 + "    INNER JOIN Batch ON"
                 + "        EventRules.BankID = Batch.BankID"
                 + "        AND EventRules.LockboxID = Batch.LockboxID"
                 + " WHERE Events.EventName = '" + eventName + "'"
                 + "    AND Batch.GlobalBatchID = " + globalBatchID.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetPendingTransactionCount(long globalBatchID) {

            return(" SELECT COUNT(*) AS PendingTransactionCount" 
                 + " FROM transactions" 
                 + " WHERE globalbatchid = " + globalBatchID.ToString() 
                 + "     AND DecisionSourceStatus IN(" + ((int)DecisionSrcStatusTypes.PendingDecisionBatchLogon).ToString()
                 + "                               , " + ((int)DecisionSrcStatusTypes.PendingDecisionBatchLogon).ToString() + ")");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetPendingDEItemCount(long globalBatchID) {

            return(" SELECT COUNT(*) AS PendingFieldCount" 
                 + " FROM Transactions" 
                 + "     INNER JOIN DEItemRowData" 
                 + "         INNER JOIN DEItemFieldData" 
                 + "             LEFT OUTER JOIN LockboxDecisioningReasons" 
                 + "                 LEFT OUTER JOIN DecisioningReasons ON" 
                 + "                     LockboxDecisioningReasons.DecisioningReasonId = DecisioningReasons.DecisioningReasonId ON" 
                 + "                 DEItemFieldData.LockboxDecisioningReasonId = LockboxDecisioningReasons.LockboxDecisioningReasonId ON" 
                 + "             DEItemRowData.DEItemRowDataID = DEItemFieldData.DEItemRowDataID ON" 
                 + "        Transactions.GlobalBatchID = DEItemRowData.GlobalBatchID" 
                 + "        AND Transactions.TransactionID = DEItemRowData.TransactionID" 
                 + "    INNER JOIN DESetupFields ON" 
                 + "        DEItemFieldData.DESetupFieldID = DESetupFields.DESetupFieldID" 
                 + " WHERE DEItemRowData.GlobalBatchID = " + globalBatchID.ToString() 
                 + "    AND DEItemFieldData.FieldDecisionStatus = " + ((int)FieldDecisionStatusTypes.PendingDecision).ToString()
                 + "    AND Transactions.DecisionSourceStatus <> " + ((int)DecisionSrcStatusTypes.Reject).ToString())
                 + "    AND DEItemRowData.DocumentType = " + ((int)DocumentTypes.Stub).ToString();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetRejectedDEItemCount(long globalBatchID) {

            return(" SELECT COUNT(*) AS RejectedFieldCount"
                 + " FROM Transactions"
                 + "     INNER JOIN DEItemRowData"
                 + "         INNER JOIN DEItemFieldData"
                 + "             LEFT OUTER JOIN LockboxDecisioningReasons"
                 + "                 LEFT OUTER JOIN DecisioningReasons ON"
                 + "                     LockboxDecisioningReasons.DecisioningReasonId = DecisioningReasons.DecisioningReasonId ON"
                 + "                 DEItemFieldData.LockboxDecisioningReasonId = LockboxDecisioningReasons.LockboxDecisioningReasonId ON"
                 + "             DEItemRowData.DEItemRowDataID = DEItemFieldData.DEItemRowDataID ON"
                 + "         Transactions.GlobalBatchID = DEItemRowData.GlobalBatchID"
                 + "         AND Transactions.TransactionID = DEItemRowData.TransactionID"
                 + "     INNER JOIN DESetupFields ON"
                 + "         DEItemFieldData.DESetupFieldID = DESetupFields.DESetupFieldID"
                 + " WHERE DEItemRowData.GlobalBatchID = " + globalBatchID.ToString()
                 + "    AND DEItemFieldData.FieldDecisionStatus = " + ((int)FieldDecisionStatusTypes.Reject).ToString()
                 + "     AND Transactions.DecisionSourceStatus <> " + ((int)DecisionSrcStatusTypes.Reject).ToString()
                 + "     AND DEItemRowData.DocumentType = " + ((int)DocumentTypes.Stub).ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <returns></returns>
        public static string SQL_GetUndefinedDestLbxCount(long globalBatchID) {

            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT COUNT(*) AS UndefinedDestLbxCount");
            sbSQL.Append(" FROM Lockbox");
            sbSQL.Append("    INNER JOIN Batch");
            sbSQL.Append("        INNER JOIN Transactions ON");
            sbSQL.Append("            Batch.GlobalBatchID = Transactions.GlobalBatchID ON");
            sbSQL.Append("        Lockbox.BankID = Batch.BankID");
            sbSQL.Append("        AND Lockbox.LockboxID = Batch.LockboxID");
            sbSQL.Append(" WHERE");
            sbSQL.Append("    Batch.GlobalBatchID = " + globalBatchID.ToString());
            sbSQL.Append("    AND Lockbox.IsCommingled = 1");
            sbSQL.Append("    AND Transactions.DestinationLockboxKey IS NULL");
            sbSQL.Append("    AND Transactions.DecisionSourceStatus <> " + ((int)DecisionSrcStatusTypes.Reject).ToString());
            sbSQL.Append("    AND Transactions.DecisionSourceStatus > " + ((int)DecisionSrcStatusTypes.NoDecisionRequired).ToString());

            return(sbSQL.ToString());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        /// <param name="decisionStatus"></param>
        /// <returns></returns>
        public static string SQL_UpdateBatchDecisionStatus(long globalBatchID
                                                         , DecisionStatusTypes decisionStatus) {
            return(" UPDATE Batch"
                 + " SET DecisionStatus = " + ((int)decisionStatus).ToString()
                 + " WHERE GlobalBatchID = " + globalBatchID.ToString());
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        private static string formatValue(DateTime theValue) {

            string strRetVal;

            if(theValue==DateTime.MinValue) { 
                strRetVal="NULL"; 
            } else {
                strRetVal="CAST('" + theValue.ToString("MM/dd/yyyy HH:mm:ss") + "' AS DateTime)";
            }
            return(strRetVal);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        private static string formatValue(string theValue) {

            return(theValue.Replace("'", "''"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="theValue"></param>
        /// <returns></returns>
        public static string formatValue(Decimal theValue) {
            return(theValue.ToString("###############.00"));
        }
    }
}
