﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     01/12/2012
*
* Purpose:  
*
* Modification History
* CR 33230 JMC 01/12/2012
*    -New File
* CR 49967 WJS 2/3/2012
 *   - Add DataImportInsertClientSetup, DataImportInsertBatch, DataImportResponseBatch,DataImportResponseClient
 *   - Add xsdversion support
 *   - Add DataImportRequestClientSetup 
 *   - Add DataImportDeleteBatch
* CR 49967 WJS 4/3/2012
 *   - Change names of usp_DataImportQueue to usp_DataImportIntegrationServices
* CR 52261 WJS 5/11/2012
 *   - Add functions DataImportGetItemDataSetupFields,DataImportGetBatchDataSetupFields, DataImportGetDocumentTypes, DataImportGetImageRPSAliasMapping
* CR 52501 WJS 5/22/2012
 *    - Add methods GetBatchDataXSL and GetClientSetupXSL and DataImportGetImageRPSAliasMapping
 * CR 50212 WJS 6/13/2012
 *     - Added details to DataImportImageTransferComplete
* CR 52261 WJS 6/22/2012
*     - Change to allowed DataImportGetImageRPSAliasMapping to take sitebankID/siteLockboxID
* CR 50212 WJS 7/20/2012
 *    - Update DataImageTransferComplete
 *    - Update try/catch logic
 * CR 50212 WJS 8/15/2012
 *    - Change DataImageTransferComplete to pass processingDate and not sourceProcessingDate
* CR 50212 WJS 8/28/2012
 *     - Pass in client code to DataImportResponseBatch and DataImportResponseClient
* CR 55488 WJS 10/18/2012 
 *     - Add call to usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse and usp_UpdateImsInterfaceQueue_ImageImport
 *WI 70412 WJS 12/12/2012
 *      - Add call to usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue
* WI 87184 CRG 02/05/2013 
*     - Remove all SQL string files from the ipoDAL
* WI 87323 CRG 02/05/2013 
*     - Remove SQLDataImport.cs
* WI 90244 CRG 03/19/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// Used to access Data Import records from the database
	/// </summary>
	public class cDataImportDAL : _DALBase {

		/// <summary>
		/// Data Import Queue Status enumeration 
		/// </summary>
		private enum DataImportQueueStatus {
			ReadyToProcess = 10,
			FailedProcessing = 15,
			InProcess = 20,
			Failed = 30,
			CompletedResponseReady = 99,
			ResponseSendWaitingForReceipt = 120,
			ClientUnknownErrorDeadRecord = 145,
			ResponseComplete = 150
		}

		private const string XSD_BATCH = "Data Import Integration Services for Batch Data";
		private const string XSD_CLIENT_SETUP = "Data Import Integration Services for Client Setup";
		private const string XSL_BATCH = "Data Import Integration Services for Batch Data XSL";
		private const string XSL_CLIENT_SETUP = "Data Import Integration Services for Client Setup XSL";

		/// <summary>
		/// Initializes a new instance of the <see cref="cDataImportDAL" /> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cDataImportDAL(string vSiteKey)
			: base(vSiteKey) {

		}

		/// <summary>
		/// Gets the batch data XSD.
		/// </summary>
		/// <param name="xsdVersion">The XSD version.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool GetBatchDataXsd(string xsdVersion, out DataTable dt) {
			return (GetCurrentSchemaRevision(XSD_BATCH, xsdVersion, out dt));
		}

		/// <summary>
		/// Gets the client setup XSD.
		/// </summary>
		/// <param name="xsdVersion">The XSD version.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool GetClientSetupXsd(string xsdVersion, out DataTable dt) {
			return (GetCurrentSchemaRevision(XSD_CLIENT_SETUP, xsdVersion, out dt));
		}

		/// <summary>
		/// Gets the batch data XSL.
		/// </summary>
		/// <param name="xslVersion">The XSL version.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool GetBatchDataXSL(string xslVersion, out DataTable dt) {
			return (GetCurrentSchemaRevision(XSL_BATCH, xslVersion, out dt));
		}

		/// <summary>
		/// Gets the client setup XSL.
		/// </summary>
		/// <param name="xslVersion">The XSL version.</param>
		/// <param name="dt">The data table.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool GetClientSetupXSL(string xslVersion, out DataTable dt) {
			return (GetCurrentSchemaRevision(XSL_CLIENT_SETUP, xslVersion, out dt));
		}

		/// <summary>
		/// Gets the current schema revision.
		/// </summary>
		/// <param name="XsdName">Name of the XSD.</param>
		/// <param name="XSDVersion">The XSD version.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		private bool GetCurrentSchemaRevision(string XsdName, string XSDVersion, out DataTable dt) {
			bool bRetVal = false;
			dt = null;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			try {
				arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmSchemaType", SqlDbType.VarChar, XsdName, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmXSDVersion", SqlDbType.VarChar, XSDVersion, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRetVal = Database.executeProcedure("RecHubSystem.usp_ImportSchemas_Get_BySchemaType", parms, out dt);
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "GetCurrentSchemaRevision(string XsdName, string XSDVersion)");
			}

			return bRetVal;
		}

		/// <summary>
		/// Data import insert client setup.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool DataImportInsertClientSetup(string xml) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Ins_ClientSetup";
			try {
				arParms.Add(BuildParameter("@parmClientSetup", SqlDbType.Xml, xml, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import insert batch.
		/// </summary>
		/// <param name="xml">The XML.</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool DataImportInsertBatch(string xml) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Ins_Batch";
			try {
				arParms.Add(BuildParameter("@parmBatch", SqlDbType.Xml, xml, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import response batch.
		/// </summary>
		/// <param name="sClientProcessCode">The client process code.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool DataImportResponseBatch(string sClientProcessCode, out DataTable dt) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			dt = null;
			const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Get_BatchResponse";
			try {
				//only insert client process code if non-empty otherwise it will default
				if(!String.IsNullOrEmpty(sClientProcessCode)) {
					arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, sClientProcessCode, ParameterDirection.Input));
				}
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import response client.
		/// </summary>
		/// <param name="sClientProcessCode">The client process code.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool DataImportResponseClient(string sClientProcessCode, out DataTable dt) {
			bool bRtnval = false;
			List<SqlParameter> arParms = new List<SqlParameter>();
			SqlParameter[] parms;
			dt = null;
			const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Get_ClientSetupResponse";
			try {
				//only insert client process code if non-empty otherwise it will default
				if(!String.IsNullOrEmpty(sClientProcessCode)) {
					arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, sClientProcessCode, ParameterDirection.Input));
				}
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import set response complete.
		/// </summary>
		/// <param name="parmResponseTrackingID">The parameter response tracking ID.</param>
		/// <param name="clientKnowsAboutRecord">if set to <c>true</c> [client knows about record].</param>
		/// <returns>
		/// results of the store procedure call
		/// </returns>
		public bool DataImportSetResponseComplete(Guid parmResponseTrackingID, bool clientKnowsAboutRecord) {
			bool bRtnval = false;
			List<SqlParameter> arParms = new List<SqlParameter>();
			SqlParameter[] parms;
			DataImportQueueStatus queueStatus = DataImportQueueStatus.ResponseComplete;
			const string PROCNAME = "RecHubSystem.usp_DataImportQueue_Upd_QueueStatus";
			try {
				arParms.Add(BuildParameter("@parmResponseTrackingID", SqlDbType.UniqueIdentifier, parmResponseTrackingID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmEntityTrackingID", SqlDbType.UniqueIdentifier, null, ParameterDirection.Input));
				if(clientKnowsAboutRecord) {
					queueStatus = DataImportQueueStatus.ResponseComplete;
				} else {
					queueStatus = DataImportQueueStatus.ClientUnknownErrorDeadRecord;
				}
				arParms.Add(BuildParameter("@parmQueueStatus", SqlDbType.Int, queueStatus, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import request client setup.
		/// </summary>
		/// <param name="siteBankID">The site bank ID.</param>
		/// <param name="siteLockboxID">The site lockbox ID.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportRequestClientSetup(int siteBankID, int siteLockboxID, out DataTable dt) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			dt = null;
			const string PROCNAME = "RecHubSystem.usp_dimClientAccounts_Get_ClientSetup";
			try {
				arParms.Add(BuildParameter("@parmSiteBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmSiteClientAccountID", SqlDbType.Int, siteLockboxID, ParameterDirection.Input));
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import delete batch.
		/// </summary>
		/// <param name="parmBatchXML">The parameter batch XML.</param>
		/// <param name="rowsDeleted">if set to <c>true</c> [rows deleted].</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportDeleteBatch(string parmBatchXML, out bool rowsDeleted) {
			bool bRtnval = false;
			rowsDeleted = false;
			SqlParameter[] parms;
			SqlParameter parmRowsDeleted;
			List<SqlParameter> arParms = new List<SqlParameter>();
			try {
				arParms.Add(BuildParameter("@parmBatches", SqlDbType.Xml, parmBatchXML, ParameterDirection.Input));
				parmRowsDeleted = BuildParameter("@parmRowsDeleted", SqlDbType.Int, rowsDeleted, ParameterDirection.Output);
				arParms.Add(parmRowsDeleted);
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure("RecHubSystem.usp_DataImportIntegrationServices_DeleteFactRecords", parms);
				if(bRtnval) {
					rowsDeleted = (bool) parmRowsDeleted.Value;
				} else {
					rowsDeleted = false;
				}
			} catch(Exception ex) {
				EventLog.logError(ex, this.GetType().Name, "DataImportDeleteBatch(string parmBatchXML, out bool rowsDeleted)");
			}

			return bRtnval;
		}

		/// <summary>
		/// Data import request document types.
		/// </summary>
		/// <param name="loadDate">The load date.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportRequestDocumentTypes(DateTime? loadDate, out DataTable dt) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			dt = null;
			const string PROCNAME = "RecHubSystem.usp_DataImportIntegrationServices_RequestDocumentTypes";
			try {
				arParms.Add(BuildParameter("@parmLoadDate", SqlDbType.DateTime, loadDate, ParameterDirection.Input));

				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import image transfer complete.
		/// </summary>
		/// <param name="processingDate">The processing date.</param>
		/// <param name="siteBankID">The site bank ID.</param>
		/// <param name="siteLockboxId">The site lockbox id.</param>
		/// <param name="batchID">The batch ID.</param>
		/// <param name="sClientProcessCode">The s client process code.</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportImageTransferComplete(DateTime processingDate, Int32 siteBankID, Int32 siteLockboxId, Int32 batchID, string sClientProcessCode) {
			bool bRtnval = false;

			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubSystem.usp_DataImportIntegrationServices_InsertIMSInterfaceQueue";
			try {
				arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, siteLockboxId, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, sClientProcessCode, ParameterDirection.Input));

				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;

		}

		/// <summary>
		/// Data import get image RPS alias mappings.
		/// </summary>
		/// <param name="siteBankID">The site bank ID.</param>
		/// <param name="siteLockboxId">The site lockbox id.</param>
		/// <param name="modficiationDate">The modification date.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportGetImageRPSAliasMappings(Int32 siteBankID, Int32 siteLockboxId, DateTime? modficiationDate, out DataTable dt) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			const string PROCNAME = "RecHubSystem.usp_dimImageRPSAliasMappings_GetRecords";
			dt = null;
			try {
				arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, siteLockboxId, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmModificationDate", SqlDbType.DateTime, modficiationDate, ParameterDirection.Input));

				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
		}

		/// <summary>
		/// Data import get hyland responses.
		/// </summary>
		/// <param name="sClientProcessCode">The s client process code.</param>
		/// <param name="dt">The data table</param>
		/// <returns>
		/// Result of store procedure call
		/// </returns>
		public bool DataImportGetHylandResponses(string sClientProcessCode, out DataTable dt) {
			bool bRtnval = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms = new List<SqlParameter>();
			dt = null;
			const string PROCNAME = "RecHubSystem.usp_DataImportIntegrationServices_GetImsInterfaceQueueResponse";
			try {
				//only insert client process code if non-empty otherwise it will default
				if(!String.IsNullOrEmpty(sClientProcessCode)) {
					arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, sClientProcessCode, ParameterDirection.Input));
				}

				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(PROCNAME, parms, out dt);
			} catch(Exception ex) {
				EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
				EventLog.logError(ex);
				bRtnval = false;
			}
			return bRtnval;
        }
        public bool DataImportImageUpdateIMSInterfaceQueue(DateTime processingDate, Int32 siteBankID, Int32 siteLockboxId, Int32 batchID, string sClientProcessCode,
                                        int archivedImages, int unArchivedImages)     
        {
            bool bRtnval = false;

            SqlParameter[] parms;
            List<SqlParameter> arParms = new List<SqlParameter>();
            const string PROCNAME = "usp_DataImportIntegrationServices_UpdateIMSInterfaceQueue";
            try {
                arParms.Add(BuildParameter("@parmProcessingDate", SqlDbType.DateTime, processingDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBankID", SqlDbType.Int, siteBankID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmLockboxID", SqlDbType.Int, siteLockboxId, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmBatchID", SqlDbType.Int, batchID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmClientProcessCode", SqlDbType.VarChar, sClientProcessCode, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmArchivedImages", SqlDbType.Int, archivedImages, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmUnArchivedImages", SqlDbType.Int, unArchivedImages, ParameterDirection.Input));

                parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(PROCNAME, parms);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                bRtnval = false;
            }
            return bRtnval;

		}
        
	}
}
