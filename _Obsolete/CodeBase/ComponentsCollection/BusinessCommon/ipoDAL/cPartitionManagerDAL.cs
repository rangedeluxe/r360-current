using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

using WFS.integraPAY.Online.Common;

namespace WFS.integraPAY.Online.DAL {

    public class cPartitionManagerDAL : _DALBase {

        public cPartitionManagerDAL(string vSiteKey) : base(vSiteKey) {

        }
        
        public bool DeletePartitionManager(int iPartitionManagerID){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPartitionManager", SqlDbType.Int, iPartitionManagerID, ParameterDirection.Input));
                parms = arParms.ToArray();
                Database.executeProcedure("usp_DeletePartitionManager",parms);
                bReturn = true;
            }catch(System.Exception ex){
                 EventLog.logEvent("An error occurred while executing usp_DeletePartitionManager " + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);

             }
            return(bReturn);
        }
        public bool AddPartitionManager(string strPartitionIdentifier , string strPartitionFunctionName,string strPartitionSchemaName, string strPartitionSize, string strRangeSettings, string strLastDiskID){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPartitionIdentifier", SqlDbType.VarChar, strPartitionIdentifier, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPartitionFunctionName", SqlDbType.VarChar, strPartitionFunctionName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmPartitionSchemeName", SqlDbType.VarChar, strPartitionSchemaName, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmSizeMB", SqlDbType.Int, strPartitionSize, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRangeSetting", SqlDbType.Int, strRangeSettings, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmLastDiskID", SqlDbType.Int, strLastDiskID, ParameterDirection.Input));
                parms = arParms.ToArray();
                Database.executeProcedure("usp_AddNewPartitionManager", parms);
                bReturn = true;
            }catch(System.Exception ex){
                EventLog.logEvent("An error occurred while executing usp_AddNewPartitionManager " + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
            return bReturn;
        }
        public bool GetPartitionDisks(out DataTable dt){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                Database.executeProcedure("usp_GetPartitionDisks", parms, out dt);
                bReturn = true;
            }catch(System.Exception ex){
                EventLog.logEvent("An error occurred while executing usp_GetPartitionDisks " + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);
               dt = null;
            }
            return bReturn;

        }
        
        public bool GetPartitionEventTracking(out DataTable dt){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                Database.executeProcedure("usp_GetPartitionEventTracking", parms, out dt);
                bReturn = true;
            }catch(System.Exception ex){
                EventLog.logEvent("An error occurred while executing usp_GetPartitionEventTracking " + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);
               dt = null;
            }
            return bReturn;

        }
        public bool DeletePartitionDisks(string strPartitionManagerID, string strDiskOrder){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPartitionManagerID", SqlDbType.Int, strPartitionManagerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDiskOrder", SqlDbType.Int, strDiskOrder, ParameterDirection.Input));
                parms = arParms.ToArray();
                Database.executeProcedure("usp_DeletePartitionDisks",parms);
                bReturn = true;
            }catch(System.Exception ex){
                 EventLog.logEvent("An error occurred while executing usp_DeletePartitionDisks" + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);

             }
            return(bReturn);
        }
        public bool UpdatePartitionDisks(string strPartitionManagerID, string strDiskOrder, string strDiskPath){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPartitionManagerID", SqlDbType.Int, strPartitionManagerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDiskOrder", SqlDbType.Int, strDiskOrder, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDiskPath", SqlDbType.VarChar, strDiskPath, ParameterDirection.Input));
                parms = arParms.ToArray();
                Database.executeProcedure("usp_UpdatePartitionDisks",parms);
                bReturn = true;
            }catch(System.Exception ex){
                 EventLog.logEvent("An error occurred while executing usp_UpdatePartitionDisks" + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);

             }
            return(bReturn);
        }
        public bool AddPartitionDisks(string strPartitionManagerID, string strDiskOrder, string strDiskPath){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmPartitionManagerID", SqlDbType.Int, strPartitionManagerID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDiskOrder", SqlDbType.Int, strDiskOrder, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmDiskPath", SqlDbType.VarChar, strDiskPath, ParameterDirection.Input));
                parms = arParms.ToArray();
                Database.executeProcedure("usp_AddNewPartitionDisks",parms);
                bReturn = true;
            }catch(System.Exception ex){
                 EventLog.logEvent("An error occurred while executing usp_AddNewPartitionDisks" + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);

             }
            return(bReturn);
        }
        public bool GetPartitionManager(out DataTable dt){
            bool bReturn = false;
            try{
                SqlParameter[] parms;
                List<SqlParameter> arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                Database.executeProcedure("usp_GetPartitionManager", parms, out dt);
                bReturn = true;
            }catch(System.Exception ex){
                 EventLog.logEvent("An error occurred while executing usp_GetPartitionManager" + ex.Message
                                , string.Empty
                                , MessageType.Error
                                , MessageImportance.Essential);
                dt = null;
             }
            return(bReturn);
        }
    }
}