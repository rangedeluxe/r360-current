﻿//#define USE_ITEMPROC
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

using WFS.integraPAY.Online.Common;

namespace WFS.integraPAY.Online.DAL {

    internal static class SQLLegacy {

       
        /// <summary>
        /// Image Import Get Check information
        /// </summary>
        /// <param name="bankId"></param>
        /// <param name="lockboxId"></param>
        /// <param name="batchId"></param>
        /// <param name="processingDate"></param>
        /// <returns></returns>
        public static string SQL_ImageImportGetChecks(int bankID, int lockboxID, int batchId, string processingDate)
        {
            return ("EXEC [OLTA].usp_ImageImportGetChecks @parmBankID =" + bankID.ToString() + ", @parmLockboxID = " + lockboxID.ToString() + ",  @parmBatchID = " + batchId.ToString() + ", @parmProcessingDate ='" + processingDate + "'");
        }
           
       /// <summary>
       /// Image Import Get Document Information
       /// </summary>
       /// <param name="bankId"></param>
       /// <param name="lockboxId"></param>
       /// <param name="batchId"></param>
       /// <param name="processingDate"></param>
       /// <returns></returns>
        public static string SQL_ImageImportGetDocuments(int bankID, int lockboxID, int batchId, string processingDate)
        {
            return ("EXEC [OLTA].usp_ImageImportGetDocuments @parmBankID =" + bankID.ToString() + ", @parmLockboxID = " + lockboxID.ToString() + ",  @parmBatchID = " + batchId.ToString() + ", @parmProcessingDate ='" + processingDate + "'");
        }

      

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string SQL_OLTAGetDocumentTypes()
        {
            StringBuilder sbSQL = new StringBuilder();

            sbSQL.Append(" SELECT ");
            sbSQL.Append("     DocumentTypeKey, ");
            sbSQL.Append("     FileDescriptor, ");
            sbSQL.Append("     DocumentTypeDescription AS [Description], ");
            sbSQL.Append("     IMSDocumentType ");
            sbSQL.Append(" FROM ");
            sbSQL.Append("     OLTA.dimDocumentTypes");
           

            return sbSQL.ToString();
        }
    }
}
