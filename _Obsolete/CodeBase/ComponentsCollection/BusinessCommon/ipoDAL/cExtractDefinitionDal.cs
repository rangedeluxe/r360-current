﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WFS.RecHub.Common;
using WFS.RecHub.DAL;
using System.Data.SqlClient;
using System.Xml;
/*******************************************************************************
*
*    Module: cExtractDefinitionDal
*  Filename: cExtractDefinitionDal.cs
*    Author: Eric Smasal
*
* Revisions:
*
* ----------------------
* CR 97662 EAS 05/09/2013
*   -Initial release version.
******************************************************************************/
namespace WFS.RecHub.DAL 
{
	public class cExtractDefinitionDal : _DALBase
    {
        #region Member Variables

        #region Constants

        const string db_Schema = "RecHubConfig";
		const string extracts_GetAfterModDate = "dimExtracts_GetAfterModDate";
		const string extracts_GetDefinitions = "usp_ExtractDefinitions_Get";
		const string extracts_GetByExtractDefID = "usp_ExtractDefinitions_Get_ByExtractDefID";
        const string extracts_UpdateExtractDefFile = "usp_ExtractDefinitions_Upd_ExtractDefFile";
        const string extracts_UpdateExtractName = "usp_ExtractDefinitions_Upd_ExtractName";
        const string extracts_InsertExtractDefFile = "usp_ExtractDefinitions_Ins_ExtractDefFile";
        const string extracts_DeleteByExtractDefID = "usp_ExtractDefinitions_Del_ByExtractDefID";

        #endregion

        #endregion

        #region Constructors

		public cExtractDefinitionDal(string vSiteKey)
			: base(vSiteKey) {
		}

        #endregion

        #region Methods

        #region Public Methods

        #region Extract Retrieval

        public bool GetExtractDefinitions(DateTime? UpdatedSince, out DataTable results)
		{
			bool bolRetVal = false;
			SqlParameter[] parms;
			List<SqlParameter> arParms;
			arParms = new List<SqlParameter>();
			if (UpdatedSince != null) 
				arParms.Add(BuildParameter("@modifiedAfter", SqlDbType.DateTime, UpdatedSince, ParameterDirection.Input));

			parms = arParms.ToArray();
			bolRetVal = Database.executeProcedure(db_Schema + "." + extracts_GetAfterModDate, parms, out results);
			return bolRetVal;
		}

		public bool GetExtractDefinitionsAll(int startRecord, int maxRows, out int totalRows, out DataTable dt)
		{
			bool bRtnval = false;
			dt = null;
			totalRows = 0;
			SqlParameter[] parms;
			SqlParameter parmTotalRecords;

			List<SqlParameter> arParms = new List<SqlParameter>();
			arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, startRecord, ParameterDirection.Input));
			arParms.Add(BuildParameter("@parmRecordsPerPage", SqlDbType.Int, maxRows, ParameterDirection.Input));
			parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
			arParms.Add(parmTotalRecords);

			parms = arParms.ToArray();

			try 
			{
				bRtnval = Database.executeProcedure(db_Schema + "." + extracts_GetDefinitions, parms, out dt);
				
			} 
			catch(Exception ex) 
			{
				EventLog.logError(ex, this.GetType().Name, "GetExtractData(out DataTable dt)");
			}

			if (bRtnval)
			{
				int result;
				if (int.TryParse(parmTotalRecords.Value.ToString(), out result))
					totalRows = result;
			}

			return bRtnval;
		}

		public bool GetExtractDefinitionFile(int extractID, out string extractName, out XmlDocument xmlDocument)
		{
			bool bRtnval = false;
			xmlDocument = null;
			SqlParameter[] parms;
			SqlParameter parmExtractName;

			extractName = null;

			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
				parmExtractName = BuildParameter("@parmExtractName", SqlDbType.VarChar, null, ParameterDirection.Output);
				arParms.Add(parmExtractName);
				
				parms = arParms.ToArray();
				bRtnval = Database.executeProcedure(db_Schema + "." + extracts_GetByExtractDefID, parms, out xmlDocument);

				if (bRtnval)
					extractName = parmExtractName.Value.ToString();

			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "GetExtractDefinition(int extractID, out XmlDocument xmlDocument)");
			}

			return bRtnval;
		}

        #endregion

        #region Extract Update

        public bool UpdateExtractDefinitionFile(int extractID, int fileSize, string extractDefinitionXml)
		{
			bool bRtnval = false;
			SqlParameter[] parms;

			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractDefSizeKb", SqlDbType.Int, fileSize, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractDefinition", SqlDbType.Xml, extractDefinitionXml, ParameterDirection.Input));

				parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_UpdateExtractDefFile, parms);

			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "UpdateExtractDefinitionFile(int extractID, int fileSize, string extractDefinitionXml)");
			}

			return bRtnval;
		}

		public bool UpdateExtractNamesBatch(DataTable dt)
		{
			bool bRtnval = false;

			try
			{
				foreach (DataRow row in dt.Rows)
				{
					UpdateExtractName((int)row["ExtractDefID"], row["ExtractName"].ToString());
				}

				bRtnval = true;
			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "UpdateExtractNamesBatch(DataTable dt)");
			}

			return bRtnval;
		}

		public bool UpdateExtractName(int extractID, string extractName)
		{
			bool bRtnval = false;
			SqlParameter[] parms;

			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractName", SqlDbType.VarChar, extractName, ParameterDirection.Input));
				parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_UpdateExtractName, parms);

			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "UpdateExtractName(int extractID, string extractName)");
			}

			return bRtnval;
		}

        #endregion

        #region Extract Insert

        public bool AddExtractDefinitionFile(string name, int fileSize, string extractDefinitionXml)
		{
			bool bRtnval = false;
			SqlParameter[] parms;
			SqlParameter parmInsertedID;

			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDefSizeKb", SqlDbType.Int, fileSize, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractDefinition", SqlDbType.Xml, extractDefinitionXml, ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmExtractName", SqlDbType.VarChar, name, ParameterDirection.Input));
				parmInsertedID = BuildParameter("@parmExtractInsertedID", SqlDbType.Int, null, ParameterDirection.Output);
				arParms.Add(parmInsertedID);
			   
				parms = arParms.ToArray();

                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_InsertExtractDefFile, parms);

			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "AddExtractDefinitionFile(int fileSize, string extractDefinitionXml)");
			}

			return bRtnval;
		}

        #endregion

        #region Extract Delete

        public bool DeleteExtractDefition(int extractID)
		{
			bool bRtnval = false;
			SqlParameter[] parms;

			try
			{
				List<SqlParameter> arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmExtractDefID", SqlDbType.Int, extractID, ParameterDirection.Input));

				parms = arParms.ToArray();
                bRtnval = Database.executeProcedure(db_Schema + "." + extracts_DeleteByExtractDefID, parms);

			}
			catch (Exception ex)
			{
				EventLog.logError(ex, this.GetType().Name, "DeleteExtractDefition(int extractID)");
			}

			return bRtnval;
        }

        #endregion

        #endregion

        #endregion

    }
}
