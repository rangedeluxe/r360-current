﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Charlie Johnson
* Date:     08/09/2012
*
* Purpose:  Manages data transactions for Notifications
*
* Modification History
*  CR 53253 CEJ 09/08/2012   Create an editor for the dimNotificationFileType table
*  WI 87623 TWE 02/07/2013   
*           Add TimeZoneBias for notification display
*  WI 88768 TWE 02/21/2013
*           Add TimeZoneBias for notification detail display  
* WI 91228 CRG 02/21/2013 
*    Remove Sql Execute from NotificationDAL.cs
*  WI 90134 TWE 3/4/2013   
*           FP:Add TimeZoneBias for notification display
*  WI 90134 WJS 3/4/2013
*           FP: Add TimeZoneBias for notification detail display  
* WI 98695 CRG 04/24/2013
*	FP 2.0 - Create stored procedure usp_dimNotificationFileTypes_Update_ByFileType
* WI 98693 CRG 04/24/2013
*	FP 2.0 - Create stored procedure usp_dimNotificationFileTypes_Insert
* WI 98672 CRG 04/24/2013
*	FP 2.0 - Create stored procedure usp_dimNotificationFileTypes_Get
*******************************************************************************/
namespace WFS.RecHub.DAL {

	/// <summary>
	/// 
	/// </summary>
    public class cNotificationDAL : _DALBase {

		/// <summary>
		/// Initializes a new instance of the <see cref="cNotificationDAL"/> class.
		/// </summary>
		/// <param name="vSiteKey">The v site key.</param>
		public cNotificationDAL(string vSiteKey)
			: base(vSiteKey) {
        }

		/// <summary>
		/// Gets the notification file types.
		/// </summary>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
        public bool GetNotificationFileTypes(out DataTable dt) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Get";
            try {
                arParms = new List<SqlParameter>();
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms, out dt);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

			return (bolRetVal);
        }

		/// <summary>
		/// Inserts the type of the notification file.
		/// </summary>
		/// <param name="fileType">Type of the file.</param>
		/// <param name="fileTypeDesc">The file type desc.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <returns></returns>
        public bool InsertNotificationFileType(string fileType, string fileTypeDesc, out string errorMessage) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Ins";
            string strErrorMessage = string.Empty;
            try {
                arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmFileType", SqlDbType.VarChar, DALLib.SQLEncodeString(fileType), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, DALLib.SQLEncodeString(fileTypeDesc), ParameterDirection.Input));
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
				if(Database.lastException != null) {
                    strErrorMessage = Database.lastException.Message;
                }
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                strErrorMessage = ex.Message;
                bolRetVal = false;
            }

            errorMessage = strErrorMessage;
			return (bolRetVal);
        }

		/// <summary>
		/// Updates the type of the notification file.
		/// </summary>
		/// <param name="fileType">Type of the file.</param>
		/// <param name="fileTypeDesc">The file type desc.</param>
		/// <param name="errorMessage">The error message.</param>
		/// <returns></returns>
        public bool UpdateNotificationFileType(string fileType, string fileTypeDesc, out string errorMessage) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_dimNotificationFileTypes_Upd";
            string strErrorMessage = string.Empty;
            try {
                arParms = new List<SqlParameter>();
				arParms.Add(BuildParameter("@parmFileType", SqlDbType.VarChar, DALLib.SQLEncodeString(fileType), ParameterDirection.Input));
				arParms.Add(BuildParameter("@parmFileTypeDesc", SqlDbType.VarChar, DALLib.SQLEncodeString(fileTypeDesc), ParameterDirection.Input));
                parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms);
				if(Database.lastException != null) {
                    strErrorMessage = Database.lastException.Message;
                }
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                strErrorMessage = ex.Message;
                bolRetVal = false;
            }

            errorMessage = strErrorMessage;
			return (bolRetVal);
        }

        /// <summary>
        /// Returns Notifications using the specified parameters.
        /// </summary>
		/// <param name="userID">The user ID.</param>
		/// <param name="startDate">The start date.</param>
		/// <param name="endDate">The end date.</param>
		/// <param name="notificationFileTypeKey">The notification file type key.</param>
		/// <param name="orderBy">The order by.</param>
		/// <param name="orderDir">The order dir.</param>
		/// <param name="startRecord">The start record.</param>
		/// <param name="maxRows">The max rows.</param>
		/// <param name="totalRecords">The total records.</param>
		/// <param name="dt">The dt.</param>
        /// <returns></returns>
        public bool GetUserNotifications(
            int userID,
            //Guid deliveryMethod,
            DateTime startDate,
            DateTime endDate,
            int notificationFileTypeKey,
            string orderBy,
            string orderDir,
            int startRecord,
            int maxRows,
            int timeZoneBias,
            out int totalRecords,
            out DataTable dt) {


            bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			SqlParameter parmTotalRecords;
			const string PROCNAME = "RecHubData.usp_factNotifications_Get_ByUserID";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmStartDate", SqlDbType.DateTime, startDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmEndDate", SqlDbType.DateTime, endDate, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmNotificationFileTypeKey", SqlDbType.Int, notificationFileTypeKey, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmOrderBy", SqlDbType.VarChar, 20, orderBy));
                arParms.Add(BuildParameter("@parmOrderDir", SqlDbType.VarChar, 4, orderDir));
                arParms.Add(BuildParameter("@parmStartRecord", SqlDbType.Int, startRecord, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmRecordsToReturn", SqlDbType.Int, maxRows, ParameterDirection.Input));
				parmTotalRecords = BuildParameter("@parmTotalRecords", SqlDbType.Int, 0, ParameterDirection.Output);
				arParms.Add(parmTotalRecords);
				arParms.Add(BuildParameter("@parmTimeZoneBias", SqlDbType.Int, timeZoneBias, ParameterDirection.Input));
                parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
				totalRecords = (int) parmTotalRecords.Value;
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                totalRecords = 0;
                bolRetVal = false;
            }

			return (bolRetVal);
        }

        /// <summary>
		/// Validates the notification to user.
        /// </summary>
		/// <param name="userID">The user ID.</param>
		/// <param name="notificationMessageGroup">The notification message group.</param>
		/// <param name="hasAccess">if set to <c>true</c> [has access].</param>
        /// <returns></returns>
		public bool ValidateNotificationToUser(int userID, int notificationMessageGroup, out bool hasAccess) {
            bool bolRetVal = false;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			SqlParameter parmHasAccess;
			const string PROCNAME = "RecHubData.usp_factNotifications_ValidateUserAccess";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmUserID", SqlDbType.Int, userID, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
				parmHasAccess = (BuildParameter("@parmHasAccess", SqlDbType.Int, 0, ParameterDirection.Output));
				arParms.Add(parmHasAccess);
                parms = arParms.ToArray();
                bolRetVal = Database.executeProcedure(PROCNAME, parms);
				hasAccess = (int) parmHasAccess.Value > 0;
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                hasAccess = false;
                bolRetVal = false;
            }

			return (bolRetVal);
        }

		/// <summary>
		/// Gets the notification.
		/// </summary>
		/// <param name="notificationMessageGroup">The notification message group.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetNotification(int notificationMessageGroup, int timeZoneBias, out DataTable dt) {
            bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
                arParms.Add(BuildParameter("@parmTimeZoneBias", SqlDbType.Int, timeZoneBias, ParameterDirection.Input));
                parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

			return (bolRetVal);
        }


		/// <summary>
		/// Gets the notification files.
		/// </summary>
		/// <param name="notificationMessageGroup">The notification message group.</param>
		/// <param name="dt">The dt.</param>
		/// <returns></returns>
		public bool GetNotificationFiles(int notificationMessageGroup, out DataTable dt) {
            bool bolRetVal = false;
			dt = null;
			DataTable tempdt = null;
            SqlParameter[] parms;
            List<SqlParameter> arParms;
			const string PROCNAME = "RecHubData.usp_factNotifications_Get_ByNotificationMessageGroup";
            try {
                arParms = new List<SqlParameter>();
                arParms.Add(BuildParameter("@parmNotificationMessageGroup", SqlDbType.Int, notificationMessageGroup, ParameterDirection.Input));
                parms = arParms.ToArray();
				bolRetVal = Database.executeProcedure(PROCNAME, parms, out tempdt);
				dt = DALLib.ConvertDataTable(ref tempdt);
            } catch(Exception ex) {
                EventLog.logEvent("Unable to execute procedure: " + PROCNAME, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                EventLog.logError(ex);
                dt = null;
                bolRetVal = false;
            }

			return (bolRetVal);
        }
    }
}
