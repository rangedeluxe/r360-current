﻿using System;
using System.Collections.Generic;
using System.Text;

using WFS.integraPAY.Online.Common;

/**********************************************************************************
**********************************************************************************/
namespace WFS.integraPAY.Online.DAL {

    internal static class SQLImageSrv {

        public static string SQL_GetOnlineImageQueue(Guid OnlineImageQueueID) {
            return(" SELECT" + 
                   "     OnlineImageQueueID," + 
                   "     UserID," + 
                   //"     GlobalBatchID," + 
                   //"     TransactionID," + 
                   "     ProcessStatusID," + 
                   "     ProcessStatusText," + 
                   "     UserFileName," + 
                   "     FileSize," + 
                   //"     DisplayScannedCheck," + 
                   "     CreationDate," + 
                   "     ModificationDate," + 
                   "     CreatedBy," + 
                   "     ModifiedBy" + 
                   " FROM OnlineImageQueue" + 
                   " WHERE OnlineImageQueueID = '" + OnlineImageQueueID.ToString() + "'");
        }

        public static string SQL_CreateOnlineImageQueue(Guid OnlineImageQueueID,
                                                        int SessionUserID,
                                                        string UserFileName,
                                                        bool DisplayScannedCheck) {

            return(" INSERT INTO OnlineImageQueue (" + 
                   "     OnlineImageQueueID," + 
                   "     UserID," + 
#if USE_ITEMPROC
                   "     GlobalBatchID," +
                   "     TransactionID," +
#endif
                   "     ProcessStatusID," + 
                   "     UserFileName," + 
                   "     DisplayScannedCheck)" + 
                   " VALUES(" + 
                   "     '" + OnlineImageQueueID.ToString() + "'," + 
                   "     " + SessionUserID + "," + 
#if USE_ITEMPROC
                   "     -1," +
                   "     -1," +
#endif
                   "     " + "1," + 
                   "     " + "'" + UserFileName + "'," + 
                   "     " + (DisplayScannedCheck ? '1' : '0') + ")");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="jobStatus"></param>
        /// <param name="fileSize"></param>
        /// <returns></returns>
        public static string SQL_UpdateJobStatus(Guid jobID, 
                                                 OLFJobStatus jobStatus, 
                                                 long fileSize) {

            string strSQL = " UPDATE"
                          + "    OnlineImageQueue"
                          + " SET"
                          + "    ProcessStatusID=" + ((int)jobStatus).ToString()
                          + "   ,ProcessStatusText='" + jobStatus.ToString() + "'"
                          + "   ,FileSize=" + fileSize.ToString()
                          + "   ,ModificationDate=GetDate()"
                          + " WHERE"
                          + "    OnlineImageQueueID='" + jobID.ToString() + "'";

            return(strSQL);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="jobID"></param>
        /// <param name="errText"></param>
        /// <returns></returns>
        public static string SQL_UpdateJobStatusError(Guid jobID, 
                                                      string errText) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" UPDATE");
            sbSQL.Append("      OnlineImageQueue");
            sbSQL.Append(" SET");
            sbSQL.Append("      ProcessStatusID=" + ((int)OLFJobStatus.Error));
            sbSQL.Append("     ,ProcessStatusText='" + SQLPublic.SQLEncodeString(errText) + "'");
            sbSQL.Append("     ,FileSize=0");
            sbSQL.Append("     ,ModificationDate=GetDate()");
            sbSQL.Append(" WHERE");
            sbSQL.Append("      OnlineImageQueueID='" + jobID.ToString() + "'");

            return(sbSQL.ToString());
        }

        public static string SQL_GetImageDisplayModeForBatch(Guid OLCustomerID, 
                                                             int BankID, 
                                                             int LockboxID, 
                                                             int BatchID, 
                                                             DateTime DepositDate) {

            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT");
            sbSQL.Append(" CASE");
            sbSQL.Append("   WHEN OLLockboxes.CheckImageDisplayMode > 0 THEN OLLockboxes.CheckImageDisplayMode");
            sbSQL.Append("   ELSE OLCustomers.CheckImageDisplayMode");
            sbSQL.Append("  END As CheckImageDisplayMode");
            sbSQL.Append(" ,CASE");
            sbSQL.Append("    WHEN OLLockboxes.DocumentImageDisplayMode > 0 THEN OLLockboxes.DocumentImageDisplayMode");
            sbSQL.Append("    ELSE OLCustomers.DocumentImageDisplayMode");
            sbSQL.Append("  END As DocumentImageDisplayMode");
            sbSQL.Append("  FROM OLLockboxes");
            sbSQL.Append("   INNER JOIN OLCustomers ON");
            sbSQL.Append("       OLCustomers.OLCustomerID = OLLockboxes.OLCustomerID");
            sbSQL.Append("   INNER JOIN dimLockboxes");
            sbSQL.Append("       INNER JOIN factBatchSummary ON");
            sbSQL.Append("           dimLockboxes.LockboxKey = factBatchSummary.LockboxKey ON");
            sbSQL.Append("       OLLockboxes.SiteBankID=dimLockboxes.SiteBankID");
            sbSQL.Append("       AND OLLockboxes.SiteLockboxID=dimLockboxes.SiteLockboxID");
            sbSQL.Append(" WHERE");
            sbSQL.Append("   dimLockboxes.SiteBankID = " + BankID.ToString());
            sbSQL.Append("   AND dimLockboxes.SiteLockboxID = " + LockboxID.ToString());
            sbSQL.Append("   AND factBatchSummary.BatchID = " + BatchID.ToString());
            sbSQL.Append("   AND factBatchSummary.DepositDateKey = " + DepositDate.ToString("yyyyMMdd"));
            sbSQL.Append("   AND OLCustomers.OLCustomerID = '" + OLCustomerID.ToString() + "'");

            return(sbSQL.ToString());
        }

        public static string SQL_GetImageDisplayModeForLockbox(Guid OLCustomerID, 
                                                               Guid OLLockboxID) {
                
            StringBuilder sbSQL = new StringBuilder();
            
            sbSQL.Append(" SELECT");
            sbSQL.Append(" CASE");
            sbSQL.Append("   WHEN OLLockboxes.CheckImageDisplayMode > 0 THEN OLLockboxes.CheckImageDisplayMode");
            sbSQL.Append("   ELSE OLCustomers.CheckImageDisplayMode");
            sbSQL.Append("  END As CheckImageDisplayMode");
            sbSQL.Append(" ,CASE");
            sbSQL.Append("    WHEN OLLockboxes.DocumentImageDisplayMode > 0 THEN OLLockboxes.DocumentImageDisplayMode");
            sbSQL.Append("    ELSE OLCustomers.DocumentImageDisplayMode");
            sbSQL.Append("  END As DocumentImageDisplayMode");
            sbSQL.Append("  FROM OLLockboxes");
            sbSQL.Append("   INNER JOIN OLCustomers ON");
            sbSQL.Append("       OLCustomers.OLCustomerID = OLLockboxes.OLCustomerID");
            sbSQL.Append(" WHERE OLLockboxes.OLLockboxID = '" + OLLockboxID.ToString() + "'");
            sbSQL.Append("   AND OLCustomers.OLCustomerID = '" + OLCustomerID.ToString() + "'");

            return(sbSQL.ToString());
        }
    }
}
