﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2010 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     MM/DD/2010
*
* Purpose:  
*
* Modification History
* CR 28738 JMC 05/03/2010
*     - Added DestinationLockboxKey parameter to UpdateTxnDecisionStatus 
*       function.
* CR 45181 JCS 09/08/2011
*     - Added GetNewGlobalID to return new GlobalID for Online Balancing
*     - Added InsertStubsBalancingDE to create invoice balancing Stubs row
*     - Added InsertVertStubsBalancingRowDE to create invoice balancing
*       DEItemRowData row for invoice amount
*     - Added InsertVertStubsBalancingFieldDE to create invoice balancing
*       DEItemFieldData row for invoice amount
* CR 32602 JCS 10/26/2010
*     - Added GetStub to return specific DEItemRowData vertical Data Entry.
* CR 47719 JCS 11/07/2011
*   -Modified InsertStubsBalancingDE to include input parm BatchSequence.
*   -Modified InsertVertStubsBalancingRowDE to include input parm BatchSequence.
*   -Added GetBatchMaxBatchSequence to return the maximum BatchSequence values.
* CR 32602 JCS 12/20/2011
*   -Added InsertAuditFile overload that includes item parameter.
* WI 90244 CRG 03/19/2013
*	Change the NameSpace, Framework, Using's and Flower Boxes for ipoDAL
******************************************************************************/
namespace WFS.RecHub.DAL {

    public class cDecisioningDAL : _DALBase {

        public cDecisioningDAL(string vSiteKey) : base(vSiteKey) {
        }

        public bool GetBatches(long bankID, long customerID, long lockboxID, DepositStatusValues depositStatus, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetBatches(bankID, customerID, lockboxID, depositStatus), out dt));
        }

        public bool GetDecisioningBatch(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetDecisioningBatch(globalBatchID), out dt));
        }

        public bool GetBatchDetails(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetBatchDetails(globalBatchID), out dt));
        }

        public bool GetDESetupFields(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetDESetupFields(globalBatchID), out dt));
        }

        public bool GetTransactions(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetTransactions(globalBatchID), out dt));
        }

        public bool GetChecks(long globalBatchID, long transactionID, List<cDESetupField> arDESetupFields, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetChecks(globalBatchID, transactionID, arDESetupFields), out dt));
        }

        public bool GetStubs(long globalBatchID, long transactionID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetStubs(globalBatchID, transactionID), out dt));
        }

        public bool GetDocuments(long globalBatchID, long transactionID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetDocuments(globalBatchID, transactionID), out dt));
        }

        public bool GetDEItemFields(long globalBatchID, long transactionID, DocumentTypes docType, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetDEItemFields(globalBatchID, transactionID, docType), out dt));
        }

        public bool UpdateBatchDepositStatus(long globalBatchID, DepositStatusValues newDepositStatus, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_UpdateBatchDepositStatus(globalBatchID, newDepositStatus), out RowsAffected));
        }

        public bool InsertDecisioningBatches(long GlobalBatchID, int NumberTransactionsPended, string LogonName, string FirstName, string LastName, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_InsertDecisioningBatches(GlobalBatchID, NumberTransactionsPended, LogonName, FirstName, LastName), out RowsAffected));
        }

        public bool ResetDecisioningBatch(Guid batchResetID, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_ResetDecisioningBatch(batchResetID), out RowsAffected));
        }

        public bool CompleteDecisioningBatch(Guid batchResetID, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_CompleteDecisioningBatch(batchResetID), out RowsAffected));
        }

        public bool UpdateTxnDecisionStatus(long globalBatchID, long transactionID, DecisionSrcStatusTypes newDecisionSrcStatus, Guid DestinationLockboxKey, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_UpdateTxnDecisionStatus(globalBatchID, transactionID, newDecisionSrcStatus, DestinationLockboxKey), out RowsAffected));
        }

        public bool DeleteStub(Guid deItemRowDataID, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_DeleteStub(deItemRowDataID), out RowsAffected));
        }

        public bool DeleteVerticalStub(Guid deItemRowDataID, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_DeleteVerticalStub(deItemRowDataID), out RowsAffected));
        }

        public bool UpdateStubs(Guid deItemRowDataID, List<cDEField> arDEFields, List<cDESetupField> arDESetupFields, out int RowsAffected) {
        
            string strSQL;
            
            strSQL = SQLDecisioning.SQL_UpdateStubs(deItemRowDataID, arDEFields, arDESetupFields);
            
            if(strSQL.Length > 0) {
                return(ExecuteSQL(strSQL, out RowsAffected));
            } else {
                RowsAffected = 0;
                return(true);
            }
        }

        public bool UpdateStubsDE(Guid deItemRowDataID, List<cDEField> arDEFields, List<cDESetupField> arDESetupFields, out int RowsAffected) {

            string strSQL;
            
            strSQL = SQLDecisioning.SQL_UpdateStubsDE(deItemRowDataID, arDEFields, arDESetupFields);
            
            if(strSQL.Length > 0) {
                return(ExecuteSQL(strSQL, out RowsAffected));
            } else {
                RowsAffected = 0;
                return(true);
            }
        }

        public bool UpdateVertStubDE(long GlobalBatchID, long TransactionID, Guid DEItemRowDataID, string TableName, string FldName, string FldValue, FieldDecisionStatusTypes FieldDecisionStatus, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_UpdateVertStubDE(GlobalBatchID, TransactionID, DEItemRowDataID, TableName, FldName, FldValue, FieldDecisionStatus), out RowsAffected));
        }

        public bool InsertVertStubDE(Guid DEItemRowDataID, string TableName, string FldName, string FldValue, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_InsertVertStubDE(DEItemRowDataID, TableName, FldName, FldValue), out RowsAffected));
        }

        public bool GetNewGlobalID(string tableName, string fieldName, long globalID, out DataTable dt) {
            return (ExecuteSQL(SQLDecisioning.SQL_GetNewGlobalID(tableName, fieldName, globalID), out dt));
        }

        public bool InsertStubsBalancingDE(long globalBatchID, long transactionID, long globalStubID, long batchSequence, decimal invoiceAmount, out int RowsAffected) {
            return (ExecuteSQL (SQLDecisioning.SQL_InsertStubsBalancingDE (globalBatchID, transactionID, globalStubID, batchSequence, invoiceAmount), out RowsAffected));
        }
        
        public bool InsertVertStubsBalancingRowDE(Guid guidDEItemRowDataID, long globalBatchID, long transactionID, long globalStubID, long batchSequence, out int RowsAffected) {
            return (ExecuteSQL(SQLDecisioning.SQL_InsertVertStubsBalancingRowDE(guidDEItemRowDataID, globalBatchID, transactionID, globalStubID, batchSequence), out RowsAffected));
        }

        public bool InsertVertStubsBalancingFieldDE(Guid DEItemFieldDataID, Guid DEItemRowDataID, Guid DESetupFieldID, decimal invoiceAmount, out int RowsAffected) {
            return (ExecuteSQL(SQLDecisioning.SQL_InsertVertStubsBalancingFieldDE(DEItemFieldDataID, DEItemRowDataID, DESetupFieldID, invoiceAmount), out RowsAffected));
        }

        public bool GetBatchMaxBatchSequence(long globalBatchID, out DataTable dt) {
            return (ExecuteSQL(SQLDecisioning.SQL_GetBatchMaxBatchSequence(globalBatchID), out dt));
        }

        public bool GetStub(Guid DEItemRowDataID, out DataTable dt) {
            return (ExecuteSQL (SQLDecisioning.SQL_GetStub (DEItemRowDataID), out dt));
        }

        public bool GetUnKilledCheckCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetUnKilledCheckCount(globalBatchID), out dt));
        }

        public bool GetRejectedTransactionCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedTransactionCount(globalBatchID), out dt));
        }

        public bool GetRejectedTransactions(long bankID, long customerID, long lockboxID, DateTime processingDateFrom, DateTime processingDateTo, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedTransactions(bankID, customerID, lockboxID, processingDateFrom, processingDateTo), out dt));
        }

        public bool GetRejectedBatch(Guid rejectedBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedBatch(rejectedBatchID), out dt));
        }

        public bool GetRejectedTransaction(Guid rejectedBatchID, long transactionID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedTransaction(rejectedBatchID, transactionID), out dt));
        }

        public bool GetRejectedChecks(Guid rejectedBatchID, long transactionID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedChecks(rejectedBatchID, transactionID), out dt));
        }

        public bool GetRejectedDocuments(Guid rejectedBatchID, long transactionID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedDocuments(rejectedBatchID, transactionID), out dt));
        }

        public bool InsertAuditFile(AuditFileEventTypes eventType, string eventDesc, long bankID, long lockboxID, long batchID, DateTime processingDate) {
            return(ExecuteSQL(SQLDecisioning.SQL_InsertAuditFile(eventType, eventDesc, bankID, lockboxID, batchID, processingDate)));
        }

        public bool InsertAuditFile(AuditFileEventTypes eventType, string eventDesc, long bankID, long lockboxID, long batchID, long item, DateTime processingDate) {
            return (ExecuteSQL(SQLDecisioning.SQL_InsertAuditFile(eventType, eventDesc, bankID, lockboxID, batchID, item, processingDate)));
        }

        public bool GetCurrentProcessingDate(out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetCurrentProcessingDate(), out dt));
        }

        public bool InsertIntoEventLog(Guid eventRuleID, long globalBatchID, DateTime processingDate, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_InsertIntoEventLog(eventRuleID, globalBatchID, processingDate), out RowsAffected));
        }

        public bool GetEventRules(string eventName, long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetEventRules(eventName, globalBatchID), out dt));
        }

        public bool GetPendingTransactionCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetPendingTransactionCount(globalBatchID), out dt));
        }

        public bool GetPendingDEItemCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetPendingDEItemCount(globalBatchID), out dt));
        }

        public bool GetRejectedDEItemCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetRejectedDEItemCount(globalBatchID), out dt));
        }

        public bool GetUndefinedDestLbxCount(long globalBatchID, out DataTable dt) {
            return(ExecuteSQL(SQLDecisioning.SQL_GetUndefinedDestLbxCount(globalBatchID), out dt));
        }

        public bool UpdateBatchDecisionStatus(long globalBatchID, DecisionStatusTypes decisionStatus, out int RowsAffected) {
            return(ExecuteSQL(SQLDecisioning.SQL_UpdateBatchDecisionStatus(globalBatchID, decisionStatus), out RowsAffected));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        public bool lockBatch(long globalBatchID) {

            SqlParameter[] parms = new SqlParameter[5];

            parms[0] = new SqlParameter();
            parms[0].ParameterName = "@parmLockKey";
            parms[0].SqlDbType = SqlDbType.VarChar;
            parms[0].Value = globalBatchID.ToString();

            parms[1] = new SqlParameter();
            parms[1].ParameterName = "@parmOwnerID";
            parms[1].SqlDbType = SqlDbType.VarChar;
            parms[1].Value = "OLD API";

            parms[2] = new SqlParameter();
            parms[2].ParameterName = "@parmRowsReturned";
            parms[2].SqlDbType = SqlDbType.Int;
            parms[2].Value = 0;

            parms[3] = new SqlParameter();
            parms[3].ParameterName = "@parmErrorDescription";
            parms[3].SqlDbType = SqlDbType.VarChar;
            parms[3].Value = "";

            parms[4] = new SqlParameter();
            parms[4].ParameterName = "@parmSQLErrorNumber";
            parms[4].SqlDbType = SqlDbType.Int;
            parms[4].Value = 0;
            
            SqlParameter[] returnParms; 
            Database.executeProcedure("proc_DMPLocks_Ins_LockBatch", parms, out returnParms);

            if((int)returnParms[4].Value == 0) {
                return(true);
            } else {
                return(false);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        public void unlockBatch(long globalBatchID) {

            SqlParameter[] parms = new SqlParameter[5];

            parms[0] = new SqlParameter();
            parms[0].ParameterName = "@parmLockKey";
            parms[0].SqlDbType = SqlDbType.VarChar;
            parms[0].Value = globalBatchID.ToString();

            parms[1] = new SqlParameter();
            parms[1].ParameterName = "@parmOwnerID";
            parms[1].SqlDbType = SqlDbType.VarChar;
            parms[1].Value = "OLD API";

            parms[2] = new SqlParameter();
            parms[2].ParameterName = "@parmRowsReturned";
            parms[2].SqlDbType = SqlDbType.Int;
            parms[2].Value = 0;

            parms[3] = new SqlParameter();
            parms[3].ParameterName = "@parmErrorDescription";
            parms[3].SqlDbType = SqlDbType.VarChar;
            parms[3].Value = "";

            parms[4] = new SqlParameter();
            parms[4].ParameterName = "@parmSQLErrorNumber";
            parms[4].SqlDbType = SqlDbType.Int;
            parms[4].Value = 0;
            
            Database.executeProcedure("proc_DMPLocks_Del_UnlockBatch", parms);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="globalBatchID"></param>
        public void setMICRVerifyStatus(long globalBatchID) {

            SqlParameter[] parms = new SqlParameter[4];

            parms[0] = new SqlParameter();
            parms[0].ParameterName = "@parmGlobalBatchID";
            parms[0].SqlDbType = SqlDbType.Int;
            parms[0].Value = globalBatchID.ToString();

            parms[1] = new SqlParameter();
            parms[1].ParameterName = "@parmMICRStatus";
            parms[1].SqlDbType = SqlDbType.Int;
            parms[1].Value = -1;
            parms[1].Direction = System.Data.ParameterDirection.InputOutput;

            parms[2] = new SqlParameter();
            parms[2].ParameterName = "@parmErrorDescription";
            parms[2].SqlDbType = SqlDbType.VarChar;
            parms[2].Size = 2000;
            parms[2].Value = "";
            parms[2].Direction = System.Data.ParameterDirection.InputOutput;

            parms[3] = new SqlParameter();
            parms[3].ParameterName = "@parmSQLErrorNumber";
            parms[3].SqlDbType = SqlDbType.Int;
            parms[3].Value = 0;
            parms[3].Direction = System.Data.ParameterDirection.InputOutput;
            
            Database.executeProcedure("proc_MICR_UPD_Batch_MICRVerifyStatus", parms);

            if((int)parms[3].Value != (int)0) {
                EventLog.logEvent("An error occurred while setting Batch.MICRVerifyStatus: " + parms[2].Value.ToString()
                                , ""
                                , MessageType.Error
                                , MessageImportance.Essential);
            }
        }

        public bool calculateBatch(long globalBatchID, DepositStatusValues depositStatus) {

            bool bolRetVal;

            SqlParameter[] parms = new SqlParameter[2];

            parms[0] = new SqlParameter();
            parms[0].ParameterName = "@GBID";
            parms[0].SqlDbType = SqlDbType.Int;
            parms[0].Value = globalBatchID;

            parms[1] = new SqlParameter();
            parms[1].ParameterName = "@DepositStatus";
            parms[1].SqlDbType = SqlDbType.Int;
            parms[1].Value = (int)depositStatus;

            try {
                Database.executeProcedure("sp_CalculateBatch", parms);
                bolRetVal = true;
            } catch(Exception e) {

                EventLog.logEvent("An error occurred while Executing sp_CalculateBatch: " + e.Message
                                , ""
                                , MessageType.Error
                                , MessageImportance.Essential);

                bolRetVal = false;
            }

            return(bolRetVal);
        }

    }
}
