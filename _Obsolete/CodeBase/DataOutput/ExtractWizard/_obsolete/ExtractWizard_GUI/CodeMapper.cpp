//---------------------------------------------------------------------------
/*
  This file is used in conjunction with the Mapper.ini to translate coded
  fields into user-friendly descriptions.  The codes are listed in
  lbenumtypes.h
*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include "CodeMapper.h"
#include "DMPExceptions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

TCodeMapper::TCodeMapper()
{
  DMPTRY {
    TIniFile *ini = new TIniFile("win.ini");
    String IniPath = ini->ReadString("DMP", "NetParamFName", "");
    if (IniPath.IsEmpty()){
      IniPath = ExtractFilePath(Application->ExeName);
      if (IniPath[IniPath.Length()] == '\\')
        IniPath.Delete(IniPath.Length(),1);
      IniPath = IniPath + ".txt";
      IniPath = ExtractFilePath(IniPath) + "Ini\\";
    }
    FMapFile = IniPath + "Mapper.ini";
    delete ini;
    FCodedFieldList = new TStringList;
    TIniFile *mapIni = new TIniFile(FMapFile);
    mapIni->ReadSections(FCodedFieldList);
    delete mapIni;
  }
  DMPCATCHTHROW("TCodeMapper::TCodeMapper")
}

TCodeMapper::~TCodeMapper()
{
  DMPTRY {
    delete FCodedFieldList;
  }
  DMPCATCHTHROW("TCodeMapper::~TCodeMapper")
}

void __fastcall TCodeMapper::RetrieveCodeValues(String Field, TStrings *str)
{
  DMPTRY {
    TIniFile *ini = new TIniFile(FMapFile);
    ini->ReadSectionValues(Field, str);
    delete ini;
  }
  DMPCATCHTHROW("TCodeMapper::RetrieveCodeValues")
}

int __fastcall TCodeMapper::GetCode(String Field, String Key)
{
  int iResult = -1;
  DMPTRY {
    int Index;
    TIniFile *ini = new TIniFile(FMapFile);
    TStringList *SectionValues = new TStringList;
    RetrieveCodeValues(Field, (TStrings*)SectionValues);
    ReverseList((TStrings*)SectionValues);
    Index = SectionValues->IndexOfName(Key);
    if (Index >= 0)
      iResult = StrToIntDef(SectionValues->Values[SectionValues->Names[Index]], -1);
    delete SectionValues;
    delete ini;
  }
  DMPCATCHTHROW("TCodeMapper::GetCode")
  return iResult;
}

String __fastcall TCodeMapper::GetDescription(String Field, int Key)
{
  String sResult;
  DMPTRY {
    TIniFile *ini = new TIniFile(FMapFile);
    sResult = ini->ReadString(Field, IntToStr(Key), -1);
    delete ini;
  }
  DMPCATCHTHROW("TCodeMapper::GetDescription")
  return sResult;
}

bool __fastcall TCodeMapper::IsCoded(String Field)
{
  bool bResult = false;
  DMPTRY {
    bResult = (FCodedFieldList->IndexOf(Field) >= 0);
  }
  DMPCATCHTHROW("TCodeMapper::IsCoded")
  return bResult;
}

void __fastcall TCodeMapper::ReverseList(TStrings *strList)
{
  DMPTRY {
    String Name, Value;
    for (int i = 0; i < strList->Count; i++){
      Name = strList->Names[i];
      Value = strList->Values[strList->Names[i]];
      strList->Strings[i] = Value + "=" + Name;
    }
  }
  DMPCATCHTHROW("TCodeMapper::ReverseList")
}
