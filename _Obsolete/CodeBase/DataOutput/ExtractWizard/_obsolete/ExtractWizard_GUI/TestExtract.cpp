//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "HandleIni.h"
#include "TestExtract.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DMPADOQuery"
#pragma link "DMPADODataSet"
#pragma resource "*.dfm"
TTestExtractForm *TestExtractForm;
//---------------------------------------------------------------------------
__fastcall TTestExtractForm::TTestExtractForm(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    TIniHandler Ini;
    Ini.Execute();
    if (TestDataSet->Active)
      TestDataSet->Close();
    String ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=True;";
    if (!Ini.Server.IsEmpty())
      ConnectionString += "Data Source=" + Ini.Server + ";";
    ConnectionString += "Initial Catalog=" + Ini.ADODB + ";";
    ConnectionString += "Application Name=" + Application->Title + ";";
    ConnectionString += "User ID=" + Ini.DBUserName +";";
    ConnectionString += "Password=" + Ini.DBPassword + ";";
    TestDataSet->ConnectionString = ConnectionString;
  }
  DMPCATCHTHROW("TTestExtractForm::TTestExtractForm")
}
//---------------------------------------------------------------------------
void __fastcall TTestExtractForm::WordWrapCBClick(TObject *Sender)
{
  DMPTRY {
    TestMemo->WordWrap = WordWrapCB->Checked;
    if (TestMemo->WordWrap)
      TestMemo->ScrollBars = ssVertical;
    else
      TestMemo->ScrollBars = ssBoth;
  }
  DMPCATCHTHROW("TTestExtractForm::WordWrapCBClick")
}

/*
//---------------------------------------------------------------------------
void __fastcall TTestExtractForm::Execute(String SetupFile, String ResultFile)
{
  bool CanShow = true;
  try {
    TestMemo->Lines->LoadFromFile(ResultFile);
  }
  catch(...){
    CanShow = false;
    MessageDlg("Unable to load temporary results file.", mtError, TMsgDlgButtons()<<mbOK, 0);
  }
  if (CanShow){
    WindowState = wsNormal;
    Show();
  }
}      */
//---------------------------------------------------------------------------


void __fastcall TTestExtractForm::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    TestDataSet->Close();
  }
  DMPCATCHTHROW("TTestExtractForm::FormClose")
}
//---------------------------------------------------------------------------

