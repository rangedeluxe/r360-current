object FormatDlg: TFormatDlg
  Left = 789
  Top = 384
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Format'
  ClientHeight = 423
  ClientWidth = 400
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCloseQuery = FormCloseQuery
  PixelsPerInch = 96
  TextHeight = 13
  object PageControl: TPageControl
    Left = 0
    Top = 0
    Width = 400
    Height = 371
    ActivePage = CurrencyTab
    Align = alClient
    TabOrder = 0
    object DateTab: TTabSheet
      Caption = 'Date/Time Formats'
      TabVisible = False
      object Label1: TLabel
        Left = 32
        Top = 51
        Width = 35
        Height = 13
        Caption = 'Format:'
      end
      object Label2: TLabel
        Left = 208
        Top = 51
        Width = 43
        Height = 13
        Caption = 'Example:'
      end
      object Label3: TLabel
        Left = 32
        Top = 16
        Width = 80
        Height = 13
        Caption = 'Selected Format:'
      end
      object Label4: TLabel
        Left = 26
        Top = 314
        Width = 57
        Height = 13
        Caption = 'Test Result:'
      end
      object DateFormatLB: TListBox
        Left = 32
        Top = 72
        Width = 145
        Height = 217
        ItemHeight = 13
        Items.Strings = (
          'm/d/yy'
          'm/d/yyyy'
          'mm/dd/yy'
          'mm/dd/yyyy'
          'yyyymmdd'
          'mmmm d, yyyy'
          'jjj'
          'yyjjj'
          'h:n:s'
          'hh:nn:ss'
          'h:n:s am/pm'
          'hh:nn:ss am/pm'
          'mm/dd/yy hh:nn:ss')
        TabOrder = 0
        OnClick = DateFormatLBClick
      end
      object DateExampleLB: TListBox
        Left = 208
        Top = 72
        Width = 145
        Height = 217
        ItemHeight = 13
        Items.Strings = (
          '7/28/01'
          '7/28/2001'
          '07/28/01'
          '07/28/2001'
          '20010728'
          'July 28, 2001'
          '209'
          '01209'
          '3:14:7'
          '03:14:07'
          '3:14:7 am'
          '03:14:07 am'
          '07/28/01 03:14:07 ')
        TabOrder = 1
        OnClick = DateExampleLBClick
      end
      object DateResultEdit: TEdit
        Left = 90
        Top = 306
        Width = 173
        Height = 21
        ReadOnly = True
        TabOrder = 2
      end
      object DateTestBtn: TButton
        Left = 280
        Top = 302
        Width = 75
        Height = 25
        Caption = 'Test'
        TabOrder = 3
        OnClick = DateTestBtnClick
      end
      object DateFormatEdit: TEdit
        Left = 120
        Top = 8
        Width = 121
        Height = 21
        TabOrder = 4
      end
    end
    object CurrencyTab: TTabSheet
      Caption = 'Currency Formats'
      ImageIndex = 1
      TabVisible = False
      object Label6: TLabel
        Left = 32
        Top = 43
        Width = 35
        Height = 13
        Caption = 'Format:'
      end
      object Label7: TLabel
        Left = 208
        Top = 43
        Width = 43
        Height = 13
        Caption = 'Example:'
      end
      object Label8: TLabel
        Left = 32
        Top = 16
        Width = 80
        Height = 13
        Caption = 'Selected Format:'
      end
      object Label16: TLabel
        Left = 26
        Top = 313
        Width = 54
        Height = 13
        Caption = 'Test Value:'
      end
      object Label10: TLabel
        Left = 26
        Top = 337
        Width = 57
        Height = 13
        Caption = 'Test Result:'
      end
      object FloatFormatLB: TListBox
        Left = 32
        Top = 64
        Width = 145
        Height = 197
        ItemHeight = 13
        Items.Strings = (
          '0'
          '0.00'
          '#,##0'
          '#,##0.00'
          '0*100'
          '#,##0*100')
        TabOrder = 0
        OnClick = FloatFormatLBClick
      end
      object FloatExampleLB: TListBox
        Left = 208
        Top = 64
        Width = 145
        Height = 197
        ItemHeight = 13
        Items.Strings = (
          '12345'
          '12345.67'
          '12,345'
          '12,345.67'
          '1234567'
          '1,234,567')
        TabOrder = 1
        OnClick = FloatExampleLBClick
      end
      object FloatValueEdit: TEdit
        Left = 90
        Top = 307
        Width = 173
        Height = 21
        MaxLength = 12
        TabOrder = 4
      end
      object FloatResultEdit: TEdit
        Left = 90
        Top = 331
        Width = 173
        Height = 21
        ReadOnly = True
        TabOrder = 5
      end
      object TestFloatBtn: TButton
        Left = 280
        Top = 307
        Width = 75
        Height = 25
        Caption = 'Test'
        TabOrder = 6
        OnClick = TestFloatBtnClick
      end
      object FloatFormatEdit: TEdit
        Left = 120
        Top = 8
        Width = 121
        Height = 21
        TabOrder = 7
      end
      object OverpunchPositiveCB: TCheckBox
        Left = 88
        Top = 264
        Width = 217
        Height = 17
        Caption = 'Overpunch Positive Values and Zero'
        TabOrder = 2
        OnClick = OverpunchPositiveCBClick
      end
      object OverpunchNegativeCB: TCheckBox
        Left = 88
        Top = 280
        Width = 217
        Height = 17
        Caption = 'Overpunch Negative Values'
        TabOrder = 3
        OnClick = OverpunchNegativeCBClick
      end
    end
    object AlphaTab: TTabSheet
      Caption = 'AlphaNumeric Formats'
      ImageIndex = 2
      TabVisible = False
      object Label12: TLabel
        Left = 61
        Top = 96
        Width = 35
        Height = 13
        Caption = 'Format:'
      end
      object Label13: TLabel
        Left = 41
        Top = 38
        Width = 329
        Height = 33
        AutoSize = False
        Caption = 
          'Use '#39'%s'#39' to indicate placement of the field value.  For example,' +
          ' the format: '#39'abc%sdef'#39' will print the value HELLO as '#39'abcHELLOd' +
          'ef'#39
        WordWrap = True
      end
      object Label14: TLabel
        Left = 42
        Top = 126
        Width = 54
        Height = 13
        Caption = 'Test Value:'
      end
      object Label15: TLabel
        Left = 42
        Top = 230
        Width = 57
        Height = 13
        Caption = 'Test Result:'
      end
      object FormatEdit: TEdit
        Left = 106
        Top = 88
        Width = 185
        Height = 21
        TabOrder = 0
      end
      object ValueEdit: TEdit
        Left = 106
        Top = 120
        Width = 185
        Height = 21
        TabOrder = 1
      end
      object TestBtn: TButton
        Left = 161
        Top = 174
        Width = 75
        Height = 25
        Caption = 'Test'
        TabOrder = 2
        OnClick = TestBtnClick
      end
      object ResultEdit: TEdit
        Left = 106
        Top = 224
        Width = 185
        Height = 21
        ReadOnly = True
        TabOrder = 3
      end
    end
  end
  object ButtonPanel: TPanel
    Left = 0
    Top = 371
    Width = 400
    Height = 52
    Align = alBottom
    TabOrder = 1
    object OKBtn: TButton
      Left = 112
      Top = 11
      Width = 75
      Height = 25
      Caption = 'OK'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
    object CancelBtn: TButton
      Left = 200
      Top = 11
      Width = 75
      Height = 25
      Cancel = True
      Caption = 'Cancel'
      ModalResult = 2
      TabOrder = 1
    end
  end
end
