//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Results.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TResultForm *ResultForm;
//---------------------------------------------------------------------------
__fastcall TResultForm::TResultForm(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
void __fastcall TResultForm::LoadFile(String FormCaption, String DisplayFile, String Setup)
{
  DMPTRY {
    Caption = FormCaption + DisplayFile;
    SetupFile = Setup;
    ResultsEdit->Clear();
    if (FileExists(DisplayFile))
      ResultsEdit->Lines->LoadFromFile(DisplayFile);
    else
      ShowMessage("Cannot locate the file - " + DisplayFile);
  }
  DMPCATCHTHROW("TResultForm::LoadFile")
}
//(816)891-5014   Mark at WorldSpan
//---------------------------------------------------------------------------
void __fastcall TResultForm::WordWrapCBClick(TObject *Sender)
{
  DMPTRY {
    ResultsEdit->WordWrap = WordWrapCB->Checked;
    if (ResultsEdit->WordWrap)
      ResultsEdit->ScrollBars = ssVertical;
    else
      ResultsEdit->ScrollBars = ssBoth;
  }
  DMPCATCHTHROW("TResultForm::WordWrapCBClick")
}

//---------------------------------------------------------------------------
void __fastcall TResultForm::Print()
{
  DMPTRY {
    ResultsEdit->Print(Caption);
  }
  DMPCATCHTHROW("TResultForm::Print")
}

//---------------------------------------------------------------------------
String __fastcall TResultForm::GetSetupFile()
{
  String sResult;
  DMPTRY {
    sResult = SetupFile;
  }
  DMPCATCHTHROW("TResultForm::GetSetupFile")
  return sResult;
}


//---------------------------------------------------------------------------

