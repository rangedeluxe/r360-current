//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "ExportSentry.h"
#include "ExportAbout.h"
#include "ExportAudit.h"
#include "DMod.h"
#include "Globals.h"
#include "DMPExceptions.h"
USERES("ExtractWizard.res");
USEFORM("DMod.cpp", DataMod); /* TDataModule: File Type */
USEFORM("record.cpp", RecordForm);
USEFORM("Results.cpp", ResultForm);
USEFORM("caption.cpp", CaptionForm);
USEFORM("format.cpp", FormatDlg);
USEUNIT("CodeMapper.cpp");
USEFORM("ImageFileFormatDlg.cpp", ImageFileFormatDialog);
USEUNIT("S:\BorlandCommon\HandleIni.cpp");
USEFORM("MainEW.cpp", MainFormEW);
USEFORM("BatchTrace.cpp", BatchTraceDlg);
USEUNIT("CustomFormat.cpp");
USEUNIT("CustomField.cpp");
USEUNIT("Globals.cpp");
USEUNIT("ImageFileNamingClasses.cpp");
USEUNIT("HelperClasses.cpp");
USEFORM("Params.cpp", ParameterDlg);
USEUNIT("Script.cpp");
USEUNIT("WizardSecurity.cpp");
USEUNIT("S:\BorlandCommon\StringEncoder.cpp");
USEFORM("SetupRpt.cpp", SetupReport); /* TQuickRep: File Type */
USEFORM("ReportPreview.cpp", PreviewForm);
USEFORM("TestExtract.cpp", TestExtractForm);
USEUNIT("Extract.cpp");
USEUNIT("ImageFileEmbeder.cpp");
USEFORM("DMPData.cpp", DMPDataMod); /* TDataModule: File Type */
USEUNIT("DMPADOHelper.cpp");
USEUNIT("TimeStampTrace.cpp");
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
  int iResult = 0;
  HANDLE hMutex = 0;
  DMPException* dmpEx = new DMPException();
  DMPTRY {

    char zIdentifier[] = "DMP Internal Identifier:\t"
                         "Ver 407.1.105.22\t"
                         "MKS Location: CheckBOX_Product/_base/ExtractWizard\t"
                         "MKS Project Node: 1.160.1.3.1.19\t"
                         "Date: 03/16/2007\t"
                         "CR 19345\t"
                         "\0";
    if (zIdentifier[0] == 'A') zIdentifier[0] = 'B'; // This is just to eliminate a warning.

    DMPTRY {
      Globals::m_aboutDll   = new AboutLoader     ("About.dll"     );
      Globals::m_auditDll   = new AuditLoader     ("Audit.dll"     );
      Globals::m_sentryDll  = new SentryLoader    ("Sentry.dll"    );
    }
    DMPCATCHTHROW("Task: Create DLL loader objects")

    DMPTRY {
      Globals::m_aboutDll ->LoadDll();
      Globals::m_auditDll ->LoadDll();
      Globals::m_sentryDll->LoadDll();
    }
    DMPCATCHTHROW("Task: Load DLLs")

    char Op[OPERATORSIZE + 1];
    String System = "CheckBOX";
    if (Globals::m_aboutDll->_CheckRelease()) {
      if (!Globals::m_sentryDll->_Permit(System.c_str(), "ExtractWizard", "Execute", Op)) {
        MessageDlg("You do not have permission to run this application.", mtError, TMsgDlgButtons()<<mbOK, 0);
        iResult = 2;
      }
    }
    else
      iResult = 1;

    DMPException* dmpEx6 = new DMPException();
    DMPException* dmpEx7 = new DMPException();
    DMPTRY {
      if (iResult == 0) {
        DMPException* dmpEx4 = new DMPException();
        DMPException* dmpEx5 = new DMPException();
        DMPTRY {
          Application->Title = "ExtractWizard";
          hMutex = ::CreateMutex(NULL, NULL, "ExtractWizard");
          if (::GetLastError()) {
            Application->Title = "ExtractWizard";
            HWND FirstWnd = ::FindWindow("TApplication", "ExtractWizard");
            if (::IsIconic(FirstWnd))
              ::ShowWindow(FirstWnd, SW_SHOWDEFAULT);
            ::SetForegroundWindow(FirstWnd);
          }
          else {
            DMPException* dmpEx2 = new DMPException();
            DMPException* dmpEx3 = new DMPException();
            DMPTRY {
                //CR20894 MJH 4-30-07: no more need for DMPExtract.dll

                DMPTRY {
                Application->Initialize();
                Application->CreateForm(__classid(TDataMod), &DataMod);
         Application->CreateForm(__classid(TMainFormEW), &MainFormEW);
         Application->CreateForm(__classid(TRecordForm), &RecordForm);
         Application->CreateForm(__classid(TResultForm), &ResultForm);
         Application->CreateForm(__classid(TFormatDlg), &FormatDlg);
         Application->CreateForm(__classid(TCaptionForm), &CaptionForm);
         Application->CreateForm(__classid(TTestExtractForm), &TestExtractForm);
         Application->CreateForm(__classid(TDMPDataMod), &DMPDataMod);
         Application->Run();
              }
              DMPCATCHTHROW("Task: Load forms, run application")
            }
            DMPCATCH_GET("Load forms and run", *dmpEx2)
            if (dmpEx2->Empty) {
              delete dmpEx2;
              if (dmpEx3->Empty)
                delete dmpEx3;
              else
                throw *dmpEx3;
            }
            else {
              delete dmpEx3; // Don't care about this exception (if one occurred) as much as the one preceding it.
              throw *dmpEx2;
            }
          }
        }
        DMPCATCH_GET("Task: Check for running app, run app", *dmpEx4)
        DMPTRY {
          Globals::m_auditDll->CloseAudit();
          Globals::m_sentryDll->_Dismiss();
        }
        DMPCATCH_GET("Task: Call DLL cleanup routines", *dmpEx5)
        if (dmpEx4->Empty) {
          delete dmpEx4;
          if (dmpEx5->Empty)
            delete dmpEx5;
          else
            throw *dmpEx5;
        }
        else {
          delete dmpEx5; // Don't care about this exception (if one occurred) as much as the one preceding it.
          throw *dmpEx4;
        }
      }
    }
    DMPCATCH_GET("Task: Run app, call cleanup", *dmpEx6)
    DMPTRY {
      delete Globals::m_aboutDll ;
      delete Globals::m_auditDll ;
      delete Globals::m_sentryDll;
    }
    DMPCATCH_GET("Task: Free DLLs", *dmpEx7)
    if (dmpEx6->Empty) {
      delete dmpEx6;
      if (dmpEx7->Empty)
        delete dmpEx7;
      else
        throw *dmpEx7;
    }
    else {
      delete dmpEx7; // Don't care about this exception (if one occurred) as much as the one preceding it.
      throw *dmpEx6;
    }
  }
  DMPCATCH_GET("WinMain", *dmpEx)
  if (!dmpEx->Empty) {
    DMPExceptionHelper::LogException(dmpEx->Message);
    ::MessageBox(0, dmpEx->Message.c_str(), String(Application->Title+" Error").c_str(), MB_ICONHAND | MB_OK);
  }
  delete dmpEx;
  if (hMutex)
    ::CloseHandle(hMutex);
  return iResult;
}
//---------------------------------------------------------------------------
