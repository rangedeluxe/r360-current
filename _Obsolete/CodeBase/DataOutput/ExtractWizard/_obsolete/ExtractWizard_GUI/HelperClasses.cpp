//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "HelperClasses.h"
#include "ExportPics.h"
#include "Defines.h"
#include <nt_glue.h>
#include <imglib.h>
#include <img_err.h>
#include <Printers.hpp>
#include <Registry.hpp>
#include "Extract.h"
#include "DMPData.h"
#include "DMPADOHelper.h"
#include <FileCtrl.hpp> //CR15923 6/19/2006 ALH

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------

FieldValue FieldItemHelper::m_fieldVal = FieldValue();

//---------------------------------------------------------------------------

FieldItem::FieldItem(const String& sName)
{
  DMPTRY {
    String sTableAndField = sName;
    int iPos;
    if ((iPos = sName.UpperCase().Pos(" AS ")) != 0) {
      sTableAndField = sName.SubString(1, iPos-1);
      m_sFieldNameAs = sName.SubString(iPos+4, sName.Length()-iPos-3).Trim();
    }
    iPos = sTableAndField.Pos(".");
    if (iPos) {
      m_sTableName = sTableAndField.SubString(1, iPos-1);
      m_sFieldName = sTableAndField.SubString(iPos+1, sTableAndField.Length()-iPos);
    }
    else
      throw DMPException("Field cannot be parsed into table and name: "+sName); // CR 5272 1/07/2004 DJK - All fields must be 'accompanied' by a table name.
  }
  DMPCATCHTHROW("FieldItem::FieldItem (1)")
}

//djkxxxFieldItem::FieldItem(const String& sName, const String& sValue) // This should go away after 'TStringList' normalization...
//djkxxx{
//djkxxx  DMPTRY {
//djkxxx    m_sName = sName;
//djkxxx    m_vValues.push_back(FieldValue(0, sValue));
//djkxxx    ParseForTableAndField();
//djkxxx  }
//djkxxx  DMPCATCHTHROW("FieldItem::FieldItem (2)")
//djkxxx}

bool FieldItem::operator < (const FieldItem& obj) const
{
  bool bResult = false;
  DMPTRY {
    int iComp = m_sTableName.AnsiCompare(obj.m_sTableName);
    if (iComp == 0)
      bResult = (m_sFieldName.AnsiCompareIC(obj.m_sFieldName)  < 0);
    else
      bResult = (iComp < 0);
  }
  DMPCATCHTHROW("FieldItem::operator <")
  return bResult;
}

String FieldItem::GetName() const
{
  String sResult;
  DMPTRY {
    sResult = m_sTableName+"."+m_sFieldName;
    if (m_sFieldNameAs.Length())
      sResult += " As "+m_sFieldNameAs;
  }
  DMPCATCHTHROW("FieldItem::GetName")
  return sResult;
}

void FieldItem::SetValue(const int iRow, const String& sValue)
{
  DMPTRY {
    FieldItemHelper::m_fieldVal.m_iRow = iRow;
    int iInsertPos;
    bool bAlreadyInList = BinarySearch(m_vValues, FieldItemHelper::m_fieldVal, iInsertPos);
    if (bAlreadyInList)
      m_vValues[iInsertPos].m_sValue = sValue;
    else
      m_vValues.insert(m_vValues.begin()+iInsertPos, FieldValue(iRow, sValue));
  }
  DMPCATCHTHROW("FieldItem::SetValue")
}

String FieldItem::GetValue(const int iRow) const
{
  String sResult;
  DMPTRY {
    FieldItemHelper::m_fieldVal.m_iRow = iRow;
    int iFoundPos;
    if (BinarySearch(m_vValues, FieldItemHelper::m_fieldVal, iFoundPos))
      sResult = m_vValues[iFoundPos].m_sValue;
  }
  DMPCATCHTHROW("FieldItem::GetValue")
  return sResult;
}

void FieldItem::AlterTableNameToDataEntry()
{
  DMPTRY {
    if (m_sTableName.AnsiCompareIC("Checks") == 0 || m_sTableName.AnsiCompareIC("Stubs") == 0 || m_sTableName.AnsiCompareIC("Documents") == 0)
      m_sTableName += "DataEntry";
    else
      throw DMPException("Cannot append 'DataEntry' to table name unless table name is 'Checks', 'Stubs', or 'Documents'");
  }
  DMPCATCHTHROW("FieldItem::AlterTableNameToDataEntry")
}

//---------------------------------------------------------------------------

FieldItem& FieldCollection::operator [] (const int iIdx)
{
  FieldItem* fiPtr = 0;
  DMPTRY {
    CheckIdx(iIdx, FieldCount());
    fiPtr = m_vFields.begin() + iIdx;
  }
  DMPCATCHTHROW("FieldCollection::operator [int]")
  return *fiPtr;
}

const FieldItem& FieldCollection::operator [] (const int iIdx) const
{
  const FieldItem* fiPtr = 0;
  DMPTRY {
    CheckIdx(iIdx, FieldCount());
    fiPtr = m_vFields.begin() + iIdx;
  }
  DMPCATCHTHROW("FieldCollection::operator [int] const")
  return *fiPtr;
}

FieldItem& FieldCollection::operator [] (const String& sFieldName)
{
  FieldItem* fiPtr = 0;
  DMPTRY {
    int iIdx = Locate(sFieldName);
    CheckIdx(iIdx, FieldCount());
    fiPtr = m_vFields.begin() + iIdx;
  }
  DMPCATCHTHROW("FieldCollection::operator [String]")
  return *fiPtr;
}

const FieldItem& FieldCollection::operator [] (const String& sFieldName) const
{
  const FieldItem* fiPtr = 0;
  DMPTRY {
    int iIdx = Locate(sFieldName);
    CheckIdx(iIdx, FieldCount());
    fiPtr = m_vFields.begin() + iIdx;
  }
  DMPCATCHTHROW("FieldCollection::operator [String] const")
  return *fiPtr;
}

void FieldCollection::ClearRows()
{
  DMPTRY {
    for (std::vector<FieldItem>::iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt)
      itt->Clear();
  }
  DMPCATCHTHROW("FieldCollection::ClearRows")
}

int FieldCollection::Add(const String& sFieldName, bool bUnique)
{
  int iResult = -1;
  DMPTRY {
    iResult = Add(FieldItem(sFieldName), bUnique);
  }
  DMPCATCHTHROW("FieldCollection::Add (String)")
  return iResult;
}

void FieldCollection::Add(const FieldCollection& fieldCollection)
{
  DMPTRY {
    for (std::vector<FieldItem>::const_iterator itt = fieldCollection.m_vFields.begin(); itt < fieldCollection.m_vFields.end(); ++itt)
      m_vFields.push_back(*itt);
    if (m_bSorted)
      std::sort(m_vFields.begin(), m_vFields.end());
  }
  DMPCATCHTHROW("FieldCollection::Add (FieldCollection)")
}

void FieldCollection::AddRow(const FieldCollection& fieldCollection, const int iIdx)
{
  DMPTRY {
    int iNewRowIdx = RowCount();
    std::vector<FieldItem>::iterator ittDest = m_vFields.begin();
    for (std::vector<FieldItem>::const_iterator itt = fieldCollection.m_vFields.begin(); itt < fieldCollection.m_vFields.end(); ++itt)
      (ittDest++)->SetValue(iNewRowIdx, itt->GetValue(iIdx));
  }
  DMPCATCHTHROW("FieldCollection::AddRow")
}

void FieldCollection::Insert(const int iIdx, const String& sFieldName, bool bUnique)
{
  DMPTRY {
    if (!m_bSorted) {
      CheckIdx(iIdx, FieldCount()+1);
      if (!bUnique || NonBinarySearch(sFieldName) == -1)
        m_vFields.insert(m_vFields.begin()+iIdx, FieldItem(sFieldName));
    }
    else
      throw DMPException("Cannot insert into a sorted collection");
  }
  DMPCATCHTHROW("FieldCollection::Insert")
}

void FieldCollection::InsertCommaDelimitedList(const int iIdx, const String& sListOfFieldNames, bool bUnique)
{
  DMPTRY {
    int iIdxStart = iIdx;
    char* zBuffer = new char[sListOfFieldNames.Length()+1];
    strcpy(zBuffer, sListOfFieldNames.c_str());
    char* zPtr;
    while ((zPtr = strtok(iIdxStart == iIdx ? zBuffer : 0, ",")) != 0) {
      String sField = zPtr;
      Insert(iIdxStart, sField.Trim(), bUnique);
      iIdxStart++;
    }
    delete[] zBuffer;
  }
  DMPCATCHTHROW("FieldCollection::InsertCommaDelimitedList")
}

void FieldCollection::Remove(const int iIdx)
{
  DMPTRY {
    CheckIdx(iIdx, FieldCount());
    m_vFields.erase(m_vFields.begin()+iIdx);
  }
  DMPCATCHTHROW("FieldCollection::Remove")
}

int FieldCollection::Locate(const String& sFieldName) const
{
  int iResult = -1;
  DMPTRY {
    if (m_bSorted) {
      int iWhere;
      if (BinarySearch(m_vFields, FieldItem(sFieldName), iWhere))
        iResult = iWhere;
    }
    else
      iResult = NonBinarySearch(sFieldName);
  }
  DMPCATCHTHROW("FieldCollection::Locate")
  return iResult;
}

int FieldCollection::RowCount() const
{
  int iResult = 0;
  DMPTRY {
    for (std::vector<FieldItem>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt) {
      int iRows = itt->GetMaxRowIdx()+1;
      if (iRows > iResult)
        iResult = iRows;
    }
  }
  DMPCATCHTHROW("FieldCollection::RowCount")
  return iResult;
}

String FieldCollection::AsList() const
{
  String sResult;
  DMPTRY {
    for (std::vector<FieldItem>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt) {
      if (sResult.Length())
        sResult += ", ";
      sResult += itt->GetName();
    }
  }
  DMPCATCHTHROW("FieldCollection::AsList")
  return sResult;
}

String FieldCollection::AsCommaList(const int iRow) const // Debug purposes only
{
  String sResult;
  DMPTRY {
    for (std::vector<FieldItem>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt) {
      if (sResult.Length())
        sResult += ",";
      sResult += itt->GetName()+"="+itt->GetValue(iRow);
    }
  }
  DMPCATCHTHROW("FieldCollection::AsCommaList")
  return sResult;
}

void FieldCollection::CheckIdx(const int iIdx, const int iCount) const
{ // Do not use DMPTRY/CATCH here - it will only confuse things.
  if (iIdx < 0 || iIdx >= iCount)
    throw DMPException("Input argument '"+String(iIdx)+"' is outside of the allowable range [0, "+String(iCount-1)+"]");
}

int FieldCollection::NonBinarySearch(const String& sFieldName) const
{
  int iResult = -1;
  DMPTRY {
    for (std::vector<FieldItem>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt)
      if (itt->GetName().AnsiCompareIC(sFieldName) == 0) {
        iResult = (itt-m_vFields.begin());
        break;
      }
  }
  DMPCATCHTHROW("FieldCollection::NonBinarySearch")
  return iResult;
}

int FieldCollection::Add(const FieldItem& fieldItem, bool bUnique)
{
  int iResult = -1;
  DMPTRY {
    if (m_bSorted) {
      int iInsertPos;
      bool bAlreadyInList = BinarySearch(m_vFields, fieldItem, iInsertPos);
      if (!bUnique || !bAlreadyInList) {
        m_vFields.insert(m_vFields.begin()+iInsertPos, fieldItem);
        iResult = iInsertPos;
      }
    }
    else
      if (!bUnique || NonBinarySearch(fieldItem.GetName()) == -1) {
        m_vFields.push_back(fieldItem);
        iResult = (int) m_vFields.size() - 1;
      }
  }
  DMPCATCHTHROW("FieldCollection::Add (FieldItem)")
  return iResult;
}

//---------------------------------------------------------------------------

FieldCollectionViewer::FieldCollectionViewer(const FieldCollection& fieldCollection)
{
  DMPTRY {
    int iColSpace = 2;
    std::vector<int> vWidths;
    m_iCols = fieldCollection.FieldCount();
    m_iRows = fieldCollection.RowCount() + 2;
    String sRow1, sRow2;
    String sTemp1, sTemp2;
    int iWidthTotal = 0;
    for (int iCol = 0; iCol < m_iCols; ++iCol) {
      int iWidth = 0;
      const FieldItem& fieldItem = fieldCollection[iCol];
      if (fieldItem.GetTableName().Length() > iWidth)
        iWidth = fieldItem.GetTableName().Length();
      if (fieldItem.GetFieldName().Length() > iWidth)
        iWidth = fieldItem.GetFieldName().Length();
      for (int iRow = 0; iRow < (m_iRows-2); ++iRow)
        if (fieldItem.GetValue(iRow).Length() > iWidth)
          iWidth = fieldItem.GetValue(iRow).Length();
      vWidths.push_back(iWidth);
      sTemp1.sprintf("%*s%s%*s", iWidth-fieldItem.GetTableName().Length(), "", fieldItem.GetTableName().c_str(), iColSpace, "");
      sTemp2.sprintf("%*s%s%*s", iWidth-fieldItem.GetFieldName().Length(), "", fieldItem.GetFieldName().c_str(), iColSpace, "");
      sRow1 += sTemp1;
      sRow2 += sTemp2;
      iWidthTotal += iWidth + iColSpace;
    }
    m_zArr = new char*[m_iRows];
    for (int i = 0; i < m_iRows; ++i)
      m_zArr[i] = new char[iWidthTotal+1];
    strcpy(m_zArr[0], sRow1.c_str());
    strcpy(m_zArr[1], sRow2.c_str());
    for (int iRow = 0; iRow < (m_iRows-2); ++iRow) {
      String sRow;
      for (int iCol = 0; iCol < m_iCols; ++iCol) {
        const FieldItem& fieldItem = fieldCollection[iCol];
        sTemp1.sprintf("%*s%s%*s", vWidths[iCol]-fieldItem.GetValue(iRow).Length(), "", fieldItem.GetValue(iRow).c_str(), iColSpace, "");
        sRow += sTemp1;
      }
      strcpy(m_zArr[iRow+2], sRow.c_str());
    }
  }
  DMPCATCHTHROW("FieldCollectionViewer::FieldCollectionViewer")
}

FieldCollectionViewer::~FieldCollectionViewer()
{
  DMPTRY {
    DeleteArray();
  }
  DMPCATCHTHROW("FieldCollectionViewer::~FieldCollectionViewer")
}

FieldCollectionViewer& FieldCollectionViewer::operator = (const FieldCollectionViewer& obj)
{
  DMPTRY {
    m_iRows = obj.m_iRows;
    m_iCols = obj.m_iCols;

    DeleteArray();

    m_zArr = new char*[m_iRows];
    for (int i = 0; i < m_iRows; ++i) {
      m_zArr[i] = new char[strlen(obj.m_zArr[i])+1];
      strcpy(m_zArr[i], obj.m_zArr[i]);
    }
  }
  DMPCATCHTHROW("FieldCollectionViewer::operator =")
  return *this;
}

void FieldCollectionViewer::DeleteArray()
{
  DMPTRY {
    if (m_zArr) {
      for (int i = 0; i < (m_iRows); ++i)
        delete[] m_zArr[i];
      delete[] m_zArr;
    }
    m_zArr = 0;
  }
  DMPCATCHTHROW("FieldCollectionViewer::DeleteArray")
}

//---------------------------------------------------------------------------

int LineCountManager::m_iCounts = 9;

LineCountManager::LineCountManager()
{
  DMPTRY {
    m_iLineCount = 0;
    m_vOrigins = std::vector<int>(m_iCounts, 0);
    m_vSavedCounts = std::vector< std::vector< std::vector<int> > >(m_iCounts, std::vector< std::vector<int> >(0, m_vOrigins));
    m_bSavedValuesUsed = false;
  }
  DMPCATCHTHROW("LineCountManager::LineCountManager")
}

void LineCountManager::Reset(const int iRecordType)
{
  DMPTRY {
    CheckForBoundsViolation(iRecordType-1, m_iCounts);
    for (int i = iRecordType-1; i < m_iCounts; ++i)
      m_vOrigins[i] = m_iLineCount;
  }
  DMPCATCHTHROW("LineCountManager::Reset ("+String(iRecordType)+")")
}

int LineCountManager::GetCount(const int iRecordType, const int iRecordTypeForSaved)
{
  int iResult = 0;
  DMPTRY {
    CheckForBoundsViolation(iRecordType-1, m_iCounts, 1);
    if (iRecordTypeForSaved) {
      m_bSavedValuesUsed = true;
      CheckForBoundsViolation(iRecordTypeForSaved-1, m_iCounts, 2);
      if (m_vSavedCounts[iRecordTypeForSaved-1].size() == 0)
        throw DMPException("There are no saved counts for this record type");
      CheckForBoundsViolation(iRecordType-1, (int) m_vSavedCounts[iRecordTypeForSaved-1][0].size(), 3);
      iResult = m_vSavedCounts[iRecordTypeForSaved-1][0][iRecordType-1];

    }
    else
      iResult = m_iLineCount - m_vOrigins[iRecordType-1] + 1;
  }
  DMPCATCHTHROW("LineCountManager::GetCount ("+String(iRecordType)+", "+String(iRecordTypeForSaved)+")")
  return iResult;
}

void LineCountManager::Initialize()
{
  DMPTRY {
    m_iLineCount == 0;
    Reset(rtFile);
  }
  DMPCATCHTHROW("LineCountManager::Initialize")
}

void LineCountManager::IncCount()
{
  DMPTRY {
    if (m_bSavedValuesUsed)
      m_bSavedValuesUsed = false;
    else
      m_iLineCount++;
  }
  DMPCATCHTHROW("LineCountManager::IncCount")
}

void LineCountManager::EnsureCountsForAfterListEmpty(const int iRecordTypeForSaved) // djk - This is a reality check to ensure something has not broken.
{
  DMPTRY {
    CheckForBoundsViolation(iRecordTypeForSaved-1, m_iCounts);
    if (m_vSavedCounts[iRecordTypeForSaved-1].size())
      throw DMPException("This list should be empty");
  }
  DMPCATCHTHROW("LineCountManager::EnsureCountsForAfterListEmpty ("+String(iRecordTypeForSaved)+")")
}

void LineCountManager::SaveCountsForAfter(const int iRecordTypeForSaved)
{
  DMPTRY {
    CheckForBoundsViolation(iRecordTypeForSaved-1, m_iCounts);
    int iIdx = (int) m_vSavedCounts[iRecordTypeForSaved-1].size();
    m_vSavedCounts[iRecordTypeForSaved-1].push_back(std::vector<int>(iRecordTypeForSaved, 0));
    for (int i = 0; i < iRecordTypeForSaved; ++i)
      m_vSavedCounts[iRecordTypeForSaved-1][iIdx][i] = m_iLineCount -  m_vOrigins[i] + 1;;
    //IncCount();  //CR 9366 01/17/2005 ALH
  }
  DMPCATCHTHROW("LineCountManager::SaveCountsForAfter ("+String(iRecordTypeForSaved)+")")
}

void LineCountManager::ClearOneCountForAfter(const int iRecordTypeForSaved)
{
  DMPTRY {
    CheckForBoundsViolation(iRecordTypeForSaved-1, m_iCounts);
    if (m_vSavedCounts[iRecordTypeForSaved-1].size() == 0)
      throw DMPException("There is no element to remove");
    m_vSavedCounts[iRecordTypeForSaved-1].erase(m_vSavedCounts[iRecordTypeForSaved-1].begin());
  }
  DMPCATCHTHROW("LineCountManager::ClearOneCountForAfter ("+String(iRecordTypeForSaved)+")")
}

void LineCountManager::CheckForBoundsViolation(const int iRecordType, const int iCountLimit, int iIdx) const
{ // DO NOT APPLY DMPCATCH/THROW HERE - IT WILL JUST CONFUSE.
  if (iRecordType < 0 || iRecordType >= iCountLimit) {
    String sError;
    sError.sprintf("Input argument %s'%d' is outside of the allowable range [0, %d]", iIdx == 0 ? String("").c_str() : String("#"+String(iIdx)+" ").c_str(), iRecordType, iCountLimit-1);
    throw DMPException(sError);
  }
}

//---------------------------------------------------------------------------

OutputManager::OutputManager()
{
  DMPTRY {
    Init();
  }
  DMPCATCHTHROW("OutputManager::OutputManager")
}

OutputManager::~OutputManager()
{
  DMPTRY {
    if (m_tmpHtml)
      delete m_tmpHtml;
    if (m_outfile)
      delete m_outfile;
  }
  DMPCATCHTHROW("OutputManager::~OutputManager")
}
//CR 15923 6/16/2006 ALH   - new function to handle relative pathing.
String OutputManager::SetFilePathAndVerifyPathing(String sExtractPath)
{
     TIniFile *ini           = NULL;
        TIniFile *mapIni        = NULL;
        DMPException *E = new DMPException();
        DMPTRY
        {
     if(sExtractPath.SubString(1,1) == ".") //CR 15923 6/16/2006 ALH  ALH if this is a relative path.   //CR17171 6/22/2006 ALH
                {
                    ini = new TIniFile("win.ini");
                    String IniPath = ini->ReadString("DMP", "NetParamFName", "");
                    if (IniPath.IsEmpty())
                    {
                        IniPath = ExtractFilePath(Application->ExeName);
                        if (IniPath[IniPath.Length()] == '\\')
                          IniPath.Delete(IniPath.Length(),1);
                        IniPath = IniPath + ".txt";
                        IniPath = ExtractFilePath(IniPath) + "Ini\\";
                    }
                    String sMapFile = IniPath + "Mapper.ini";
                    if(ini != NULL)
                    {
                        delete ini;
                        ini = NULL;
                    }
                    mapIni = new TIniFile(sMapFile);
                    String sMapperExtractPath = mapIni->ReadString("Wizard", "SetupPath", "");


                    if(!(FindNextSlashFromEndofString(sMapperExtractPath)+1) == sMapperExtractPath.Length())
                    {
                        sMapperExtractPath = sMapperExtractPath + "\\";
                    }

                    sExtractPath = sMapperExtractPath + sExtractPath;  // append paths together.
                    if(mapIni != NULL)
                    {
                        delete mapIni;
                        mapIni = NULL;
                    }
                }
                String sExtractFolder =  sExtractPath.SubString(1,FindNextSlashFromEndofString(sExtractPath));   // get rid of file name.
                if (!DirectoryExists(sExtractFolder))
                        CreateDir(sExtractFolder);     // create folder if it does not exis
        }
        DMPCATCH_GET("OutputManager::SetFilePathAndVerifyPathing", *E)
        if(!E->IsEmpty())
        {
              if(ini != NULL)
              {
                  delete ini;
                  ini = NULL;
              }
              if(mapIni != NULL)
              {
                  delete mapIni;
                  mapIni = NULL;
              }
              throw E;
        }
        else
        {
                delete E;
                E = NULL;
        }
        return sExtractPath;
}

//CR 15923 6/16/2006 ALH   -- new function
int OutputManager::FindNextSlashFromEndofString(String s)
{
        int iIndex = s.Length();
        DMPTRY
        {
                while(s.Length() > 0)
                {
                        String sChar = s.SubString(s.Length(),1);
                        s =  s.SubString(1,s.Length() - 1);
                        iIndex--;
                        if(sChar == "\\")
                                break;
                }
        }
        DMPCATCHTHROW("OutputManager::FindNextSlashFromEndofString")
        return iIndex;
}
bool OutputManager::OpenExtractFiles(bool bIsHtmlTable)
{
  bool bResult = true;
  DMPTRY {
    m_bIsHtmlTable = bIsHtmlTable;

    if (FileExists(m_sTargetFile))
      DeleteFile(m_sTargetFile);

    m_lineCountManager->Initialize();

    DMPTRY {
      m_outfile = new TFileStream(m_sTargetFile, fmCreate|fmShareDenyWrite);
    }
    DMPCATCHTHROW("Task: Create extract file <"+m_sTargetFile+">")

    if (bResult && bIsHtmlTable) {
      DMPTRY {
        m_tmpHtml = new TFileStream(m_sTargetFile + ".tmp", fmCreate);
      }
      DMPCATCHTHROW("Task: Create temporary extract file <" + m_sTargetFile + ".tmp>")
    }
  }
  DMPCATCHTHROW("OutputManager::OpenExtractFiles")
  return bResult;
}

void OutputManager::CloseExtractFiles()
{
  DMPTRY {
    if (m_outfile) {
      delete m_outfile;
      m_outfile = 0;
    }
    if (m_tmpHtml) {
      delete m_tmpHtml;
      m_tmpHtml = 0;
    }
  }
  DMPCATCHTHROW("OutputManager::CloseExtractFiles")
}

void OutputManager::ExportToHtml(const String& sHeaderFile, const String& sFooterFile)
{
  DMPTRY {
    TFileStream *HtmlFile = new TFileStream(m_sTargetFile + ".html", fmCreate|fmShareExclusive);
    if (!sHeaderFile.IsEmpty()){
      TFileStream *HeaderFile = new TFileStream(sHeaderFile, fmOpenRead|fmShareDenyNone);
      HtmlFile->CopyFrom(HeaderFile, 0);
      delete HeaderFile;
    }
    HtmlFile->Seek(0, soFromEnd);
    HtmlFile->Write("<PRE>", 5);
    HtmlFile->CopyFrom(m_bIsHtmlTable ? m_tmpHtml : m_outfile, 0);
    if (!sFooterFile.IsEmpty()){
      TFileStream *FooterFile = new TFileStream(sFooterFile, fmOpenRead|fmShareDenyNone);
      HtmlFile->CopyFrom(FooterFile, 0);
      delete FooterFile;
    }
    delete HtmlFile;
  }
  DMPCATCHTHROW("OutputManager::ExportToHtml")
}

void OutputManager::WriteFile(const String& sText)
{
  DMPTRY {
    m_outfile->Write(sText.c_str(), sText.Length());
    m_lineCountManager->IncCount();
  }
  DMPCATCHTHROW("OutputManager::WriteFile")
}

void OutputManager::WriteHtml(const String& sText)
{
  DMPTRY {
    m_tmpHtml->Write(sText.c_str(), sText.Length());
  }
  DMPCATCHTHROW("OutputManager::WriteHtml")
}

int OutputManager::GetHtmlSize() const
{
  int iResult = 0;
  DMPTRY {
    iResult = m_tmpHtml->Size;
  }
  DMPCATCHTHROW("OutputManager::GetHtmlSize")
  return iResult;
}

int OutputManager::GetFilePos() const
{
  int iResult = 0;
  DMPTRY {
    iResult = m_outfile->Position;
  }
  DMPCATCHTHROW("OutputManager::GetFilePos")
  return iResult;
}

int OutputManager::GetHtmlPos() const
{
  int iResult = 0;
  DMPTRY {
    iResult = m_tmpHtml->Position;
  }
  DMPCATCHTHROW("OutputManager::GetHtmlPos")
  return iResult;
}

void OutputManager::AdjustLastLineFeed(const String& sRecordDelimiter)
{
  DMPTRY {
    if (sRecordDelimiter == "\\n") {
      if (m_outfile->Size >= 2)
        m_outfile->Size = m_outfile->Size-2;
    }
    else
      if (sRecordDelimiter == "\\n\\n")
        if (m_outfile->Size >= 4)
          m_outfile->Size = m_outfile->Size-4;
  }
  DMPCATCHTHROW("OutputManager::AdjustLastLineFeed")
}

void OutputManager::FlipFiles1(TFileStream* tmpFile, TFileStream*& tmpHtmlHeader, const int iPos, const int iPosHtml, const String& sTempHtmlFileName)
{
  DMPTRY {
    CopyTempFile(m_outfile, tmpFile, iPos);
    m_outfile->Seek(iPos, soFromBeginning);
    if (m_bIsHtmlTable){
      tmpHtmlHeader = new TFileStream(sTempHtmlFileName, fmCreate);
      CopyTempFile(m_tmpHtml, tmpHtmlHeader, iPosHtml);
      m_tmpHtml->Seek(iPosHtml, soFromBeginning);
    }
  }
  DMPCATCHTHROW("OutputManager::FlipFiles1")
}

void OutputManager::FlipFiles2(TFileStream* tmpFile, TFileStream* tmpHtmlHeader)
{
  DMPTRY {
    if (m_bIsHtmlTable){
      CopyTempFile(tmpHtmlHeader, m_tmpHtml, 0);
      m_tmpHtml->Seek(0, soFromEnd);
    }
    CopyTempFile(tmpFile, m_outfile, 0);
    m_outfile->Seek(0, soFromEnd);
  }
  DMPCATCHTHROW("OutputManager::FlipFiles2")
}

void OutputManager::CopyTempFile(TFileStream* fsSource, TFileStream* fsDest, const int iStartPos)
{
  DMPTRY {
    MEMORYSTATUS Mem;
    GlobalMemoryStatus(&Mem);
    int iBlockSize = Mem.dwAvailPhys/2;
    int iCopyBytes = fsSource->Position - iStartPos;
    fsSource->Seek(iStartPos, soFromBeginning);
    while (iCopyBytes >= iBlockSize){
      fsDest->CopyFrom(fsSource, iBlockSize);
      iCopyBytes -= iBlockSize;
    }
    if (iCopyBytes)
      fsDest->CopyFrom(fsSource, iCopyBytes);
  }
  DMPCATCHTHROW("OutputManager::CopyTempFile")
}

void OutputManager::Init()
{
  DMPTRY {
    m_bIsHtmlTable = false;
    m_tmpHtml = 0;
    m_outfile = 0;
    m_lineCountManager = 0;
  }
  DMPCATCHTHROW("OutputManager::Init")
}

//---------------------------------------------------------------------------

// CR 13806 10/04/2005 - New class created for handling update of batch table after extract

void ESNBatchUpdate::AddGlobalBatchID(const int iGlobalBatchID)
{
  DMPTRY {
    if (m_iExtractSequenceNumber > 0) {
      if (m_sGlobalBatchIDList.Length())
        m_sGlobalBatchIDList += ",";
      m_sGlobalBatchIDList += String(iGlobalBatchID);
    }
  }
  DMPCATCHTHROW("ESNBatchUpdate::AddGlobalBatchID")
}

void ESNBatchUpdate::UpdateBatchTable(TDMPADOCommand* adoCommand)
{
  DMPTRY {
    if (m_sGlobalBatchIDList.Length() > 0) {
      int iArbitrarySize = 3000;
      String sListToUse;
      while (m_sGlobalBatchIDList.Length()) {
        if (m_sGlobalBatchIDList.Length() > iArbitrarySize) {
          int iPos = iArbitrarySize;
          while (m_sGlobalBatchIDList[iPos] != ',') iPos--;
          sListToUse = m_sGlobalBatchIDList.SubString(1, iPos-1);
          m_sGlobalBatchIDList = m_sGlobalBatchIDList.SubString(iPos+1, m_sGlobalBatchIDList.Length()-iPos);
        }
        else {
          sListToUse = m_sGlobalBatchIDList;
          m_sGlobalBatchIDList = "";
        }
        String sSql;
        sSql.sprintf("Update Batch Set ExtractSequenceNumber = %d Where GlobalBatchID In (%s)", m_iExtractSequenceNumber, sListToUse.c_str());
        adoCommand->CommandText = sSql;
        adoCommand->Execute();
      }
    }
  }
  DMPCATCHTHROW("ESNBatchUpdate::UpdateBatchTable")
}

//---------------------------------------------------------------------------

// CR 5272 1/07/2004 DJK
String TableHelper::GetTable(int iType)
{
  String sResult;
  DMPTRY {
    switch (iType) {
      case rtBank        : sResult = "Bank"        ; break;
      case rtCustomer    : sResult = "Customer"    ; break;
      case rtLockbox     : sResult = "Lockbox"     ; break;
      case rtBatch       : sResult = "Batch"       ; break;
      case rtTransactions: sResult = "Transactions"; break;
      case rtChecks      : sResult = "Checks"      ; break;
      case rtStubs       : sResult = "Stubs"       ; break;
      case rtDocs        : sResult = "Documents"   ; break;
    }
  }
  DMPCATCHTHROW("TableHelper::GetTable")
  return sResult;
}

//---------------------------------------------------------------------------

String StringHelper::GetLastErrorText(int iError)
{
  String sResult;
  DMPTRY {
    void* lpMsgBuf;
    if (iError == -1)
      iError = ::GetLastError();
    ::FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, iError, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
    if (((char*) lpMsgBuf)[0])
      sResult += (char*) lpMsgBuf;
    else
      sResult += "System could not supply a reason for failure";
    ::LocalFree(lpMsgBuf);
  }
  DMPCATCHTHROW("StringHelper::GetLastErrorText")
  return sResult;
}

String StringHelper::GetSnowboundErrorText(const int iError)
{
  String sResult;
  DMPTRY {
    switch (iError) {
      case OUT_OF_MEMORY                : sResult = "OUT_OF_MEMORY: Failed on memory allocation. Problem with a standared memroy allocation";
                                          break;
      case FILE_NOT_FOUND               : sResult = "FILE_NOT_FOUND: open call failed when trying to decompress an image usualy";
                                          break;
      case CORRUPTED_FILE               : sResult = "CORRUPTED_FILE: file format bad, or unreadable. Usualy returned when the decompress gives up on an image";
                                          break;
      case BAD_STRING                   : sResult = "BAD_STRING: string passed in is null or invalid.  May be returned in VBX/OCX products";
                                          break;
      case BAD_RETURN                   : sResult = "BAD_RETURN: Internal VBX/OCX problem";
                                          break;
      case CANT_CREATE_FILE             : sResult = "CANT_CREATE_FILE: fail on saving when attempting to creating a new file.  Also may be returned for .tif planar images since a temp file must be created";
                                          break;
      case FORMAT_NOT_ALLOWED           : sResult = "FORMAT_NOT_ALLOWED: the image was not recognized as an image the library can decompress";
                                          break;
      case NO_BITMAP_FOUND              : sResult = "NO_BITMAP_FOUND: Getobject() call failed to return bitmap header for using DDB functions or may be returned in formats that can contain vector information such as .wpg, .wmf and .pct if not bitmap information is found";
                                          break;
      case DISK_FULL                    : sResult = "DISK_FULL: error writing data to the disk.  Standard file i/o write failed";
                                          break;
      case BAD_DISPLAY_AREA             : sResult = "BAD_DISPLAY_AREA: tried to display with negative coordinates or out of range";
                                          break;
      case PAGE_NOT_FOUND               : sResult = "PAGE_NOT_FOUND: used for multipage file format support when attempting to access a page which does not exist";
                                          break;
      case DISK_READ_ERROR              : sResult = "DISK_READ_ERROR: file format was truncated and tried to read past end of file. Standard read i/o function failed";
                                          break;
      case BAD_HANDLE                   : sResult = "BAD_HANDLE: application passed bad image handle. Not a valid SnowBound library image handle";
                                          break;
      case NO_CLIPBOARD_IMAGE           : sResult = "NO_CLIPBOARD_IMAGE: image not found on clipboard";
                                          break;
      case NO_SCANNER_FOUND             : sResult = "NO_SCANNER_FOUND: Twain scanner driver not installed or not found (TWAIN.DLL)";
                                          break;
      case ERROR_OPENING_SCANNER        : sResult = "ERROR_OPENING_SCANNER: bad scanner driver or driver not configured properly";
                                          break;
      case CANT_FIND_TWAIN_DLL          : sResult = "CANT_FIND_TWAIN_DLL: Twain scanner driver not installed or not found (TWAIN.DLL)";
                                          break;
      case USER_CANCEL                  : sResult = "USER_CANCEL: used for canceling out of low level save or low level decompress. Usualy not an error but terminated a function on purpose";
                                          break;
      case EVAL_TIMEOUT                 : sResult = "EVAL_TIMEOUT: the date on an eval copy of  product has expired";
                                          break;
      case USING_RUNTIME                : sResult = "USING_RUNTIME: vbx/ocx runtime version not allowed for design mode";
                                          break;
      case PIXEL_DEPTH_UNSUPPORTED      : sResult = "PIXEL_DEPTH_UNSUPPORTED: tried to save an image to format not supported the images bits pixel. Also tried to perform an image processing function an an image whose bits per pixel is not allowed";
                                          break;
      case PALETTE_IMAGES_NOT_ALLOWED   : sResult = "PALETTE_IMAGES_NOT_ALLOWED: some image processing operations will not work on palette images";
                                          break;
      case NO_LZW_VERSION               : sResult = "NO_LZW_VERSION: no lzw code or gif in this version";
                                          break;
      case DLL_NOT_LOADED               : sResult = "DLL_NOT_LOADED: dll not loaded for platinum win3.1 version snbd6w31.dll";
                                          break;
      case FORMAT_WILL_NOT_OTFLY        : sResult = "FORMAT_WILL_NOT_OTFLY: format will not support on the fly decompression";
                                          break;
      case NO_TCOLOR_FOUND              : sResult = "NO_TCOLOR_FOUND: no transparancy color information found";
                                          break;
      case COMPRESSION_NOT_SUPPORTED    : sResult = "COMPRESSION_NOT_SUPPORTED: currently not supporting this compression format";
                                          break;
      case NO_MORE_PAGES                : sResult = "NO_MORE_PAGES: no more pages ready in document feeder 2";
                                          break;
      case FEEDER_NOT_READY             : sResult = "FEEDER_NOT_READY: no more pages ready in document feeder 2";
                                          break;
      case NO_DELAY_TIME_FOUND          : sResult = "NO_DELAY_TIME_FOUND: couldn't find graphic extension block in .gif file";
                                          break;
      case TIFF_TAG_NOT_FOUND           : sResult = "TIFF_TAG_NOT_FOUND: couldn't find .tif tag";
                                          break;
      case NOT_A_TILED_IMAGE            : sResult = "NOT_A_TILED_IMAGE: not recognized as a .tiff tiled image";
                                          break;
      case NOT_SUPPORTED_IN_THIS_VERSION: sResult = "NOT_SUPPORTED_IN_THIS_VERSION: you are using a version that does not support this function";
                                          break;
      case AUTOFEED_FAILED              : sResult = "AUTOFEED_FAILED: autofeed fail in twain scanner";
                                          break;
      case NO_FAST_TWAIN_SUPPORTED      : sResult = "NO_FAST_TWAIN_SUPPORTED: twain driver canot do fast transfer";
                                          break;
      case NO_PDF_VERSION               : sResult = "NO_PDF_VERSION: no pdf code in this version";
                                          break;
      case NO_ABIC_VERSION              : sResult = "NO_ABIC_VERSION: no abic plugin code in this version";
                                          break;
      case EXCEPTION_ERROR              : sResult = "EXCEPTION_ERROR: no abic plugin code in this version";
                                          break;
      default                           : sResult = String(iError)+": Next text associated with this error";
    }
  }
  DMPCATCHTHROW("StringHelper::GetSnowboundErrorText")
  return sResult;
}

//---------------------------------------------------------------------------

std::vector<String> FileHelper::FindFiles(const String& sFileName, bool bDirectories) // Wildcards allowed (looks for either files, or directories, but never both at once)
{
  std::vector<String> vResult;
  DMPTRY {
    String sDirName = sFileName.SubString(1, sFileName.LastDelimiter("\\"));
    DWORD dwForbiddenAttributes = FILE_ATTRIBUTE_DIRECTORY | FILE_ATTRIBUTE_OFFLINE | FILE_ATTRIBUTE_SYSTEM | FILE_ATTRIBUTE_TEMPORARY;
    DWORD dwRequiredAttributes = 0;
    if (bDirectories) {
      dwForbiddenAttributes &= ~FILE_ATTRIBUTE_DIRECTORY;
      dwRequiredAttributes  |=  FILE_ATTRIBUTE_DIRECTORY;
    }
    WIN32_FIND_DATA fileData;
    HANDLE hndlSearch = ::FindFirstFile(sFileName.c_str(), &fileData);
    if (hndlSearch != INVALID_HANDLE_VALUE) {
      if (!(fileData.dwFileAttributes & dwForbiddenAttributes) && (dwRequiredAttributes == 0 || (fileData.dwFileAttributes & dwRequiredAttributes)))
        vResult.push_back(fileData.cFileName);
      while (::FindNextFile(hndlSearch, &fileData))
        if (!(fileData.dwFileAttributes & dwForbiddenAttributes) && (dwRequiredAttributes == 0 || (fileData.dwFileAttributes & dwRequiredAttributes)))
          vResult.push_back(fileData.cFileName);
      ::FindClose(hndlSearch);
    }
  }
  DMPCATCHTHROW("FileHelper::FindFiles")
  return vResult;
}

void FileHelper::DeleteFiles(const String& sFileName) // Wildcards allowed
{
  DMPTRY {
    std::vector<String> vFound;

    DMPTRY {
      vFound = FileHelper::FindFiles(sFileName);
    }
    DMPCATCHTHROW("Task: Build list of files that match wilcards that may have been present in input string")

    String sError;
    String sDirName = sFileName.SubString(1, sFileName.LastDelimiter("\\"));
    DMPTRY {
      for (std::vector<String>::iterator itt = vFound.begin(); itt < vFound.end(); ++itt)
        if (::DeleteFile((sDirName+*itt).c_str()) == 0) {
          if (sError.Length())
            sError += "\r\n\r\n";
          sError += "Error deleting file '" + *itt + "':\r\n\r\n" + StringHelper::GetLastErrorText();
        }
    }
    DMPCATCHTHROW("Task: Delete files in list")
    if (sError.Length())
      throw DMPException("Error(s) deleting file(s):\r\n\r\n"+sError);
  }
  DMPCATCHTHROW("FileHelper::DeleteFiles")
}

//---------------------------------------------------------------------------

ImageFileItem::SortingMethod ImageFileItem::m_sortingMethod;
bool                         ImageFileItem::m_bImageFileZeroPad = false;
bool                         ImageFileItem::m_bImageFileBatchTypeFormatFull = false;
String                       ImageFileItem::m_sProcDateFormat;
std::vector<EditToken>       ImageFileItem::m_vTokenSingle   ; // This array defines the layout of the 'Single' file names.
std::vector<EditToken>       ImageFileItem::m_vTokenPerTran  ; // This array defines the layout of the ''Per Transaction' file names.
std::vector<EditToken>       ImageFileItem::m_vTokenPerBatch ; // This array defines the layout of the 'Per Batch' file names.

ImageFileItem::ImageFileItem()
{
  DMPTRY {
    Initialize();
  }
  DMPCATCHTHROW("ImageFileItem::ImageFileItem (default)")
}

ImageFileItem::ImageFileItem(const String& sImageRequestType, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const TDateTime dtProcDate, const int iItem, const String& sType, int iPage)
{
  DMPTRY {
    Initialize();
    m_iPage          = iPage; // CR 9916 10/27/2004 ALH
    m_iBank          = iBank;
    m_iLockbox       = iLockbox;
    m_iBatch         = iBatch;
    m_iBatchTypeCode = iBatchTypeCode;
    m_iTransaction   = iTransaction;
    m_dtProcDate     = dtProcDate;
    m_iItem          = iItem;
    m_sType          = sType;
    m_iImgFieldColSize = 0;
  }
  DMPCATCHTHROW("ImageFileItem::ImageFileItem")
}

ImageFileItem& ImageFileItem::operator = (const ImageFileItem& obj)
{
  DMPTRY {
    m_iBank            = obj.m_iBank;
    m_iLockbox         = obj.m_iLockbox;
    m_iBatch           = obj.m_iBatch;
    m_iBatchTypeCode   = obj.m_iBatchTypeCode;
    m_iTransaction     = obj.m_iTransaction;
    m_dtProcDate       = obj.m_dtProcDate;
    m_iItem            = obj.m_iItem;
    m_sType            = obj.m_sType;
    m_iPage            = obj.m_iPage;
    m_bImageExists     = obj.m_bImageExists;
    m_bSingle          = obj.m_bSingle;
    m_bGroupedByTran   = obj.m_bGroupedByTran;
    m_bGroupedByBatch  = obj.m_bGroupedByBatch;
    m_iImageFileNumber = obj.m_iImageFileNumber;
    m_iImgFieldColSize = obj.m_iImgFieldColSize;
  }
  DMPCATCHTHROW("ImageFileItem::operator =")
  return *this;
}

bool ImageFileItem::operator == (const ImageFileItem& obj) const
{
  bool bResult = false;
  DMPTRY {
    switch (m_sortingMethod) {
      case smDefault : bResult = (
                                  m_iBank                            == obj.m_iBank         &&
                                  m_iLockbox                         == obj.m_iLockbox      &&
                                  m_iBatch                           == obj.m_iBatch        &&
                                  m_dtProcDate                       == obj.m_dtProcDate    &&
                                  m_iItem                            == obj.m_iItem         &&
                                  m_sType.AnsiCompareIC(obj.m_sType) == 0                   &&
                                  m_iPage                            == obj.m_iPage
                                  // m_iBatchTypeCode    // This must not be part of comparison
                                  // m_iTransaction      // This must not be part of comparison
                                  // m_bImageExists      // This must not be part of comparison
                                  // m_bSingle           // This must not be part of comparison
                                  // m_bGroupedByTran    // This must not be part of comparison
                                  // m_bGroupedByBatch   // This must not be part of comparison
                                  // m_iImageFileNumber  // This must not be part of comparison
                                 );
                       break;
      case smSingle  : bResult = (
                                  m_iBank                            == obj.m_iBank         &&
                                  m_iLockbox                         == obj.m_iLockbox      &&
                                  m_iBatch                           == obj.m_iBatch        &&
                                  m_dtProcDate                       == obj.m_dtProcDate    &&
                                  m_iItem                            == obj.m_iItem         &&
                                  m_sType.AnsiCompareIC(obj.m_sType) == 0                   &&
                                  m_bImageExists                     == obj.m_bImageExists  &&
                                  m_bSingle                          == obj.m_bSingle       &&
                                  m_iPage                            == obj.m_iPage
                                  // m_iBatchTypeCode    // This must not be part of comparison
                                  // m_iTransaction      // This must not be part of comparison
                                  // m_bGroupedByTran    // This must not be part of comparison
                                  // m_bGroupedByBatch   // This must not be part of comparison
                                  // m_iImageFileNumber  // This must not be part of comparison
                                 );
                       break;
      case smPerTran :
      case smPerBatch: bResult = (
                                  m_iBank                            == obj.m_iBank          &&
                                  m_iLockbox                         == obj.m_iLockbox       &&
                                  m_iBatch                           == obj.m_iBatch         &&
                                  m_iTransaction                     == obj.m_iTransaction   &&
                                  m_dtProcDate                       == obj.m_dtProcDate     &&
                                  m_iItem                            == obj.m_iItem          &&
                                  m_sType.AnsiCompareIC(obj.m_sType) == 0                    &&
                                  m_bImageExists                     == obj.m_bImageExists   &&
                                  m_bGroupedByTran                   == obj.m_bGroupedByTran &&
                                  m_iPage                            == obj.m_iPage
                                  // m_iBatchTypeCode    // This must not be part of comparison
                                  // m_bSingle           // This must not be part of comparison
                                  // m_bGroupedByBatch   // This must not be part of comparison
                                  // m_iImageFileNumber  // This must not be part of comparison
                                 );
                       break;
      case smImageSize  : bResult = (  //CR 9916 10/27/2004 ALH
                                  m_iBank                            == obj.m_iBank         &&
                                  m_iLockbox                         == obj.m_iLockbox      &&
                                  m_iBatch                           == obj.m_iBatch        &&
                                  m_dtProcDate                       == obj.m_dtProcDate    &&
                                  m_iItem                            == obj.m_iItem         &&
                                  m_sType.AnsiCompareIC(obj.m_sType) == 0                   &&
                                  m_bImageExists                     == obj.m_bImageExists  &&
                                  m_bSingle                          == obj.m_bSingle       &&
                                  m_iPage                            == obj.m_iPage
                                  // m_iBatchTypeCode    // This must not be part of comparison
                                  // m_iTransaction      // This must not be part of comparison
                                  // m_bGroupedByTran    // This must not be part of comparison
                                  // m_bGroupedByBatch   // This must not be part of comparison
                                  // m_iImageFileNumber  // This must not be part of comparison
                                 );
                       break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("ImageFileItem::operator ==")
  return bResult;
}

bool ImageFileItem::operator < (const ImageFileItem& obj) const
{
  bool bResult = false;
  DMPTRY {
    switch (m_sortingMethod) {
      case smDefault : if (m_iBank < obj.m_iBank)
                         bResult = true;
                       else
                         if (m_iBank == obj.m_iBank)
                           if (m_iLockbox < obj.m_iLockbox)
                             bResult = true;
                           else
                             if (m_iLockbox == obj.m_iLockbox)
                               if (m_iBatch < obj.m_iBatch)
                                 bResult = true;
                               else
                                 if (m_iBatch == obj.m_iBatch)
                                   if (m_dtProcDate < obj.m_dtProcDate)
                                     bResult = true;
                                   else
                                     if (m_dtProcDate == obj.m_dtProcDate)
                                       if (m_iItem < obj.m_iItem)
                                         bResult = true;
                                       else
                                         if (m_iItem == obj.m_iItem)
                                           if (m_sType.AnsiCompareIC(obj.m_sType) < 0)
                                             bResult = true;
                                           else
                                             if (m_sType.AnsiCompareIC(obj.m_sType) == 0)
                                                 if(m_iPage < obj.m_iPage)   // CR9916 11/02/2004 ALH
                                                    bResult = true;
                                                 else
                                                    if(m_iPage == obj.m_iPage)
                                                        throw DMPException("Comparison found at least two identical items - all items must be unique (smDefault)");
                       // m_iBatchTypeCode   // This must not be part of comparison
                       // m_iTransaction     // This must not be part of comparison
                       // m_bImageExists     // This must not be part of comparison
                       // m_bSingle          // This must not be part of comparison
                       // m_bGroupedByTran   // This must not be part of comparison
                       // m_bGroupedByBatch  // This must not be part of comparison
                       // m_iImageFileNumber // This must not be part of comparison
                       break;
      case smSingle  : if (m_bImageExists && !obj.m_bImageExists)
                         bResult = true;
                       else
                         if (m_bImageExists == obj.m_bImageExists)
                           if (m_bSingle && !obj.m_bSingle)
                             bResult = true;
                           else
                             if (m_bSingle == obj.m_bSingle)
                               if (m_dtProcDate < obj.m_dtProcDate)
                                 bResult = true;
                               else
                                 if (m_dtProcDate == obj.m_dtProcDate)
                                   if (m_iBank < obj.m_iBank)
                                     bResult = true;
                                   else
                                     if (m_iBank == obj.m_iBank)
                                       if (m_iLockbox < obj.m_iLockbox)
                                         bResult = true;
                                       else
                                         if (m_iLockbox == obj.m_iLockbox)
                                           if (m_iBatch < obj.m_iBatch)
                                             bResult = true;
                                           else
                                             if (m_iBatch == obj.m_iBatch)
                                               if (m_iItem < obj.m_iItem)
                                                 bResult = true;
                                               else
                                                 if (m_iItem == obj.m_iItem)
                                                   if (m_sType.AnsiCompareIC(obj.m_sType) < 0)
                                                     bResult = true;
                                                   else
                                                     if (m_sType.AnsiCompareIC(obj.m_sType) == 0)
                                                       if(m_iPage < obj.m_iPage)  // CR9916 11/02/2004 ALH
                                                          bResult = true;
                                                       else
                                                          if(m_iPage == obj.m_iPage)
                                                             bResult = (this < &obj);
                       // m_iBatchTypeCode   // This must not be part of comparison
                       // m_iTransaction     // This must not be part of comparison
                       // m_bGroupedByTran   // This must not be part of comparison
                       // m_bGroupedByBatch  // This must not be part of comparison
                       // m_iImageFileNumber // This must not be part of comparison
                       break;
      case smPerTran :
      case smPerBatch: if (m_bImageExists && !obj.m_bImageExists)
                         bResult = true;
                       else
                         if (m_bImageExists == obj.m_bImageExists)
                           if (m_bGroupedByTran && !obj.m_bGroupedByTran)
                             bResult = true;
                           else
                             if (m_bGroupedByTran == obj.m_bGroupedByTran)
                               if (m_iBank < obj.m_iBank)
                                 bResult = true;
                               else
                                 if (m_iBank == obj.m_iBank)
                                   if (m_iLockbox < obj.m_iLockbox)
                                     bResult = true;
                                   else
                                     if (m_iLockbox == obj.m_iLockbox)
                                       if (m_iBatch < obj.m_iBatch)
                                         bResult = true;
                                       else
                                         if (m_iBatch == obj.m_iBatch)
                                           if (m_iTransaction < obj.m_iTransaction)
                                             bResult = true;
                                           else
                                             if (m_iTransaction == obj.m_iTransaction)
                                               if (m_dtProcDate < obj.m_dtProcDate)
                                                 bResult = true;
                                               else
                                                 if (m_dtProcDate == obj.m_dtProcDate)
                                                   if (m_iItem < obj.m_iItem)
                                                     bResult = true;
                                                   else
                                                     if (m_iItem == obj.m_iItem)
                                                       if (CompareTypes(m_sType, obj.m_sType) < 0)
                                                         bResult = true;
                                                       else
                                                         if (m_sType.AnsiCompareIC(obj.m_sType) == 0)
                                                             if(m_iPage < obj.m_iPage)     // CR9916 11/02/2004 ALH
                                                                bResult = true;
                                                             else
                                                                if(m_iPage == obj.m_iPage)
                                                                   bResult = (this < &obj);
                       // m_iBatchTypeCode   // This must not be part of comparison
                       // m_bSingle          // This must not be part of comparison
                       // m_bGroupedByBatch  // This must not be part of comparison
                       // m_iImageFileNumber // This must not be part of comparison
                       break;
      case smImageSize : if (m_bImageExists && !obj.m_bImageExists) //CR9916 10/26/2004 ALH
                         bResult = true;
                       else
                         if (m_bImageExists == obj.m_bImageExists)
                           if (m_bSingle && !obj.m_bSingle)
                             bResult = true;
                           else
                             if (m_bSingle == obj.m_bSingle)
                               if (m_dtProcDate < obj.m_dtProcDate)
                                 bResult = true;
                               else
                                 if (m_dtProcDate == obj.m_dtProcDate)
                                   if (m_iBank < obj.m_iBank)
                                     bResult = true;
                                   else
                                     if (m_iBank == obj.m_iBank)
                                       if (m_iLockbox < obj.m_iLockbox)
                                         bResult = true;
                                       else
                                         if (m_iLockbox == obj.m_iLockbox)
                                           if (m_iBatch < obj.m_iBatch)
                                             bResult = true;
                                           else
                                             if (m_iBatch == obj.m_iBatch)
                                               if (m_iItem < obj.m_iItem)
                                                 bResult = true;
                                               else
                                                 if (m_iItem == obj.m_iItem)
                                                   if (m_sType.AnsiCompareIC(obj.m_sType) < 0)
                                                     bResult = true;
                                                   else
                                                     if (m_sType.AnsiCompareIC(obj.m_sType) == 0)
                                                       if(m_iPage < obj.m_iPage)   // CR9916 11/02/2004 ALH
                                                          bResult = true;
                                                       else
                                                          if(m_iPage == obj.m_iPage)
                                                             bResult = (this < &obj);
                       // m_iBatchTypeCode   // This must not be part of comparison
                       // m_iTransaction     // This must not be part of comparison
                       // m_bGroupedByTran   // This must not be part of comparison
                       // m_bGroupedByBatch  // This must not be part of comparison
                       // m_iImageFileNumber // This must not be part of comparison
                       break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("ImageFileItem::operator <")
  return bResult;
}

void ImageFileItem::DetermineExistenceOfImage(PicsLoader* picsDllPtr)
{
  DMPTRY {
    m_bImageExists = (bool) (picsDllPtr->GetSize(m_dtProcDate.FormatString("yyyymmdd").c_str(), m_iBank, m_iLockbox, m_iBatch, m_iItem, m_sType.c_str(), m_iPage));
  }
  DMPCATCHTHROW("ImageFileItem::DetermineExistenceOfImage")
}

String ImageFileItem::GetImageFileName(const String& sImageRequestType, const String& sPathAndRootName, ImageFormat  imageFormat/* djkzzz , bool bCombineProcDates */ )
{
  String sResult;
  DMPTRY {
    if (m_bImageExists) {

      ImageFileGrouping imageFileGrouping;
      DMPTRY {
        // CR 4969 ~ DLAQAB ~ 6-15-2004
        // ImageFileName should be handled just like a SINGLE
        if( sImageRequestType.UpperCase() == "IMAGEFILENAME" ) {
            imageFileGrouping = ifgSingle;
            m_bSingle = true;
        }
        else  // End CR 4969 ~ DLAQAB ~ 6-15-2004
          if (sImageRequestType.UpperCase().Pos("SINGLE") || (sImageRequestType.UpperCase().Pos("IMAGEFILESIZE"))) {  //CR 9916 10/27/2004 ALH - we just want to get the image size adn we only will allow this for single images
            imageFileGrouping = ifgSingle;
            m_bSingle = true;
          }
          else
            if (sImageRequestType.UpperCase().Pos("TRANSACTION")) {
              imageFileGrouping = ifgPerTran;
              m_bGroupedByTran = true;
            }
            else
              if (sImageRequestType.UpperCase().Pos("BATCH")) {
                imageFileGrouping = ifgPerBatch;
                m_bGroupedByBatch = true;
              }
              else
                throw DMPException("Supplied 'sImageFieldName' value ("+sImageRequestType+") cannot be parsed as either a 'Single', 'Transaction', or 'Batch' type");
      }
      DMPCATCHTHROW("Task: Analyze field name to determine type of image grouping required")

      sResult = GetImageFileName(imageFileGrouping, sPathAndRootName, imageFormat/* djkzzz , bCombineProcDates */ );
    }
  }
  DMPCATCHTHROW("ImageFileItem::GetImageFileName")
  return sResult;
}

void ImageFileItem::CreateImageFile(ImageFileGrouping imageFileType, const String& sPathAndRootName, ImageFormat imageFormat/* djkzzz , bool bCombineProcDates*/ , ImageRenderer& imageRenderer, int iPage)
{
  DMPTRY {
    if (m_bImageExists) {
      imageRenderer.RenderImage(m_dtProcDate,
                                m_iBank,
                                m_iLockbox,
                                m_iBatch,
                                m_iItem,
                                m_sType,
                                GetImageFileName(imageFileType, sPathAndRootName, imageFormat, true/* djkzzz , bCombineProcDates */ ),
                                iPage
                                ); //CR 9916 10/27/2004 ALH
    }
    else
      throw DMPException("Cannot create image file - 'm_bImageExists' flag is 'false'");
  }
  DMPCATCHTHROW("ImageFileItem::CreateImageFile")
}

String ImageFileItem::GetImageFileName(ImageFileGrouping imageFileType, const String& sPathAndRootName, ImageFormat imageFormat, bool bRendering/* djkzzz , bool bCombineProcDates */)  //CR9916 11/12/2004 ALH
{
  String sResult;
  DMPTRY {
     if (m_bImageExists) {
      bool bEmbedImage = false;
      String sFileExt = "";
      switch (imageFormat) {
        case ifTIF:  sFileExt += ".Tif"; break;
        case ifPDF:  sFileExt += ".Pdf"; break;
        case ifTIF2: sFileExt += ".Tif"; bEmbedImage = true; break;   //CR 9916 10/14/2004 ALH
        default: throw DMPException("Illegal case");
      }

      sResult = sPathAndRootName;
      std::vector<EditToken>* vPtr;
      switch (imageFileType) {
        case ifgSingle  : vPtr = &m_vTokenSingle;
                          break;
        case ifgPerTran : vPtr = &m_vTokenPerTran;
                          break;
        case ifgPerBatch: vPtr = &m_vTokenPerBatch;
                          break;
        default: throw DMPException("Illegal case");
      }
      String sTemp;
      String sFormatString(m_bImageFileZeroPad ? "%010d" : "%d");
      for (int i = 0; i < (int) vPtr->size(); ++i) {
        EditToken* tokenPtr = vPtr->begin()+i;
        switch (tokenPtr->GetTokenType()) {
          case EditToken::ettText        : sResult += tokenPtr->GetTextInternal();
                                           break;
          case EditToken::ettBank        : sResult += sTemp.sprintf(sFormatString.c_str(), m_iBank);
                                           break;
          case EditToken::ettLockbox     : sResult += sTemp.sprintf(sFormatString.c_str(), m_iLockbox);
                                           break;
          case EditToken::ettBatch       : sResult += sTemp.sprintf(sFormatString.c_str(), m_iBatch);
                                           break;
          case EditToken::ettTransaction : sResult += sTemp.sprintf(sFormatString.c_str(), m_iTransaction);
                                           break;
          case EditToken::ettFileCounter : sResult += sTemp.sprintf(sFormatString.c_str(), m_iImageFileNumber);
                                           break;
          case EditToken::ettBatchType   : sResult += m_bImageFileBatchTypeFormatFull ? sTemp.sprintf(sFormatString.c_str(), m_iBatchTypeCode) : String(m_iBatchTypeCode).SubString(1, 1);
                                           break;
          case EditToken::ettProcDate    : sResult += m_dtProcDate.FormatString(m_sProcDateFormat);
                                           break;
          case EditToken::ettDocumentType: sResult += m_sType;
                                           break;
          default: throw DMPException("Illegal case");
        }
      }
      if (imageFileType == ifgSingle){  //CR 9916 10/29/2004 ALH
        switch (m_iPage){
          case PICS_FRONT:
            sResult += "F";
            break;
          case PICS_BACK:
            sResult += "B";
            break;
          default:
            throw DMPException("TASK: Choose image page = an invalid page was detected. Only PICS_FRONT or PICS_BACK are allowed.");
        }
      }
      sResult += sFileExt;

      if(bEmbedImage && !bRendering){ //CR 9916 10/14/2004 ALH
        String sImageNameSize = "";
        if(ImgFieldColSize == 0)  // should not be zero
        {
           throw DMPException ("Column size cannot be zero.");
        }
        sImageNameSize.sprintf("%05d",ImgFieldColSize);
        sResult = EMBEDTAG + sImageNameSize + sResult;
      }
    }
    
  }
  DMPCATCHTHROW("ImageFileItem::GetImageFileName")
  return sResult;
}

int ImageFileItem::CompareTypes(const String& sType1, const String& sType2) const
{
  int iResult = 0;
  DMPTRY {
    bool b1 = false;
    if (sType1.Length() == 1)
      b1 = sType1[1] == 'c' || sType1[1] == 'C';
    bool b2 = false;
    if (sType2.Length() == 1)
      b2 = sType2[1] == 'c' || sType2[1] == 'C';
    if (b1) {
      if (!b2)
        iResult = -1;
    }
    else
      if (b2)
        iResult = 1;
      else
        iResult = sType1.AnsiCompareIC(sType2);
  }
  DMPCATCHTHROW("ImageFileItem::CompareTypes")
  return iResult;
}

void ImageFileItem::Initialize()
{
  DMPTRY {
    m_iBank = m_iLockbox = m_iBatch = m_iBatchTypeCode = m_iTransaction = m_iItem = m_iImageFileNumber-1;
    m_bImageExists = m_bSingle = m_bGroupedByTran = m_bGroupedByBatch = false;
    m_iPage = PICS_FRONT;
  }
  DMPCATCHTHROW("ImageFileItem::Initialize")
}

//---------------------------------------------------------------------------

ImageProgressReporter::ImageProgressReporter(TImageProgressEvent eventOnImageProgress, int iImageHitCountTotal, int iNumberOfReports)
{
  DMPTRY {
    m_eventOnImageProgress = eventOnImageProgress;
    m_iImageHitCountTotal  = iImageHitCountTotal;
    m_iNumberOfReports     = iNumberOfReports;
    m_iImageHitCounter         = 0;
    m_iImageHitCounterInterval = m_iImageHitCountTotal / m_iNumberOfReports + 1;
    m_iImageHitCounterNext     = m_eventOnImageProgress == 0 ? 0 : m_iImageHitCounterInterval;
    m_bCancelRequested = false;
  }
  DMPCATCHTHROW("ImageProgressReporter::ImageProgressReporter")
}

bool ImageProgressReporter::Hit()
{
  bool bResult = true;
  DMPTRY {
    if (m_eventOnImageProgress) {
      m_iImageHitCounter++;
      if (m_iImageHitCounter >= m_iImageHitCounterNext) {
        m_iImageHitCounterNext += m_iImageHitCounterInterval;
        m_eventOnImageProgress(((double) m_iImageHitCounter)/((double) m_iImageHitCountTotal), m_bCancelRequested);
        bResult = !m_bCancelRequested;
      }
    }
  }
  DMPCATCHTHROW("ImageProgressReporter::Hit")
  return bResult;
}

void ImageProgressReporter::FirstHit()
{
  DMPTRY {
    if (m_eventOnImageProgress)
      m_eventOnImageProgress(0.0, m_bCancelRequested);
  }
  DMPCATCHTHROW("ImageProgressReporter::FirstHit")
}

void ImageProgressReporter::FinalHit()
{
  DMPTRY {
    if (m_eventOnImageProgress)
      m_eventOnImageProgress(1.0, m_bCancelRequested);
  }
  DMPCATCHTHROW("ImageProgressReporter::FinalHit")
}

//---------------------------------------------------------------------------

ImageFileManager::ImageFileManager()
{
  DMPTRY {
    m_iFileCounter      = 0;
    m_imageFormat       = ImageFileItem::ifNone;
//djkzzz    m_bCombineProcDates = false;
    m_picsDll           = 0;
    m_iImgFieldColSize  = 0; // CR9916 11/09/2004 ALH
  }
  DMPCATCHTHROW("ImageFileManager::ImageFileManager")
}

ImageFileManager::~ImageFileManager()
{
  DMPTRY {
  }
  DMPCATCHTHROW("ImageFileManager::~ImageFileManager")
}

void ImageFileManager::Initialize(TImageProgressEvent eventOnImageProgress, PicsLoader* picsDll, const String& sPathAndRootName, ImageFileItem::ImageFormat imageFormat, bool bCombineProcDates, const String& sImageFileProcDateFormat, const bool bImageFileZeroPad, const bool bImageFileBatchTypeFormatFull, const String& sImageFileNameSingle, const String& sImageFileNamePerTran, const String& sImageFileNamePerBatch)
{
  DMPTRY {
    m_sImageErrors = "";
    m_eventOnImageProgress = eventOnImageProgress;
    m_picsDll = picsDll;
    if (m_picsDll == 0)
      throw DMPException("Pointer supplied for 'PicsLoader' is null");
    m_sPathAndRootName = sPathAndRootName;
    m_imageFormat = imageFormat;
//djkzzz    m_bCombineProcDates = bCombineProcDates;
    m_iFileCounter = 0;
    m_vImageFileItems.clear();

    ImageFileItem::m_sortingMethod                 = ImageFileItem::smDefault;
    ImageFileItem::m_bImageFileZeroPad             = bImageFileZeroPad;
    ImageFileItem::m_bImageFileBatchTypeFormatFull = bImageFileBatchTypeFormatFull;

    ImageFileItem::m_sProcDateFormat = sImageFileProcDateFormat;
    DMPTRY {
      String sError = TTokenEdit::ValidateDateFormatString(ImageFileItem::m_sProcDateFormat);
      if (sError.Length())
        ImageFileItem::m_sProcDateFormat = "MMDDYY";
    }
    DMPCATCHTHROW("Task: Validate (correct if necessary) date format string")

    DMPTRY {
      String sSingle   = TTokenManager::ValidateFileNameTemplate(sImageFileNameSingle  , bCombineProcDates, TTokenManager::iftSingle  );
      String sPerTran  = TTokenManager::ValidateFileNameTemplate(sImageFileNamePerTran , bCombineProcDates, TTokenManager::iftPerTran );
      String sPerBatch = TTokenManager::ValidateFileNameTemplate(sImageFileNamePerBatch, bCombineProcDates, TTokenManager::iftPerBatch);
      ImageFileItem::m_vTokenSingle   = TTokenManager::DecodeTokenStringStatic(0, sSingle  );
      ImageFileItem::m_vTokenPerTran  = TTokenManager::DecodeTokenStringStatic(0, sPerTran );
      ImageFileItem::m_vTokenPerBatch = TTokenManager::DecodeTokenStringStatic(0, sPerBatch);
    }
    DMPCATCHTHROW("Task: Validate (correct if necessary) file name templates, decode to array of token definitions")
  }
  DMPCATCHTHROW("ImageFileManager::Initialize")
}

String ImageFileManager::StoreImageRequestAndReturnFileName(const String& sImageRequestType, const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const int iItem, const String& sType, int iPage)
{
  String sResult;
  DMPTRY {

    ImageFileItem ifiPivot(sImageRequestType, iBank, iLockbox, iBatch, iBatchTypeCode, iTransaction, dtProcDate, iItem, sType, iPage);
    int iPos;
    if (!BinarySearch(m_vImageFileItems, ifiPivot, iPos)) {
      ifiPivot.SetImageFileNumber(++m_iFileCounter);
      ifiPivot.DetermineExistenceOfImage(m_picsDll);
      m_vImageFileItems.insert(m_vImageFileItems.begin()+iPos, ifiPivot);
      m_vImageFileItems[iPos].Page = iPage;                             // CR9916 10/25/2004 ALH
      m_vImageFileItems[iPos].ImgFieldColSize = ImgFieldColSize;        // CR9916 11/09/2004 ALH
    }
    sResult = m_vImageFileItems[iPos].GetImageFileName(sImageRequestType, m_sPathAndRootName, m_imageFormat/* djkzzz , m_bCombineProcDates */ );
  }
  DMPCATCHTHROW("ImageFileManager::StoreImageRequestAndReturnFileName")
  return sResult;
}
// CR 9916 10/27/2004 ALH - new function to retrieve image size
String ImageFileManager::ReturnImageSize(const String& sImageRequestType, const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const int iItem, const String& sType, const int iPage)
{
  String sResult = "";

  DMPTRY {
            String sImgSizeTag = "";
            sImgSizeTag.sprintf("%03d",ImgFieldColSize);
            sResult = IMGSIZETAG + sImgSizeTag;
  }
  DMPCATCHTHROW("ImageFileManager::ReturnFileName")
  return sResult;
}

bool ImageFileManager::CreateImageFiles()
{
  DMPException *E = new DMPException();
  bool bRval = true; // assume sucess CR9916 11/03/2004 ALH
 /* for (int loop = 0; loop < (int)m_vImageFileItems.size();loop++)  // Cludgy .. leave for diagnostic purposes just in case.
  {
        if((m_vImageFileItems[loop].Page > PICS_BACK) || (m_vImageFileItems[loop].Page < PICS_FRONT)){
            String sInfo = "";
            sInfo.sprintf("Item %d had a PICS page of %d, which is invalid. Reseting to PICS_FRONT",loop,m_vImageFileItems[loop].Page);
            m_vImageFileItems[loop].Page = PICS_FRONT;
            ::MessageBox(NULL,sInfo.c_str(),"Page Error",MB_OK);
        }
  } */
  DMPTRY {
    // Determine how many calls there will be to create an image (for progress bar, handled in 'ImageProgressReporter' class):
    int iImageHitCountTotal = 0;
    for (std::vector<ImageFileItem>::iterator itt = m_vImageFileItems.begin(); itt < m_vImageFileItems.end(); ++itt)
      iImageHitCountTotal += itt->GetImageHitCount();
    if (iImageHitCountTotal) {
      ImageProgressReporter imageProgress(m_eventOnImageProgress, iImageHitCountTotal, 10000);

      // This is vital when multi-image TIFFs are involved, since it would be possible to inadvertently APPEND to
      //  an old file when the intent was to create a new file.
      FileHelper::DeleteFiles(m_sPathAndRootName+"*.Tif");
      FileHelper::DeleteFiles(m_sPathAndRootName+"*.Pdf");

      ImageRenderer imageRenderer(m_picsDll, m_imageFormat);
      imageProgress.FirstHit();
      if (!imageProgress.GetCancelRequested()) {
        // Create single image files:
        DMPTRY {
          ImageFileItem::m_sortingMethod = ImageFileItem::smSingle;
          std::sort(m_vImageFileItems.begin(), m_vImageFileItems.end());
          for (std::vector<ImageFileItem>::iterator itt = m_vImageFileItems.begin(); itt < m_vImageFileItems.end(); ++itt){
            if (itt->ImageExists() && itt->GetAsSingle()) {
              itt->CreateImageFile(ImageFileItem::ifgSingle, m_sPathAndRootName, m_imageFormat/* djkzzz , m_bCombineProcDates */ , imageRenderer,itt->Page);  //CR 9916 10/27/2004 ALH - add support for front and back images

              if (!imageProgress.Hit())
                break;
            }
            else
              break;
          }
        }
        DMPCATCHTHROW("Task: Generate single image files")
      }

      if (!imageProgress.GetCancelRequested()) {
        // Create image files grouped by TransactionID:
        DMPTRY {
          ImageFileItem::m_sortingMethod = ImageFileItem::smPerTran;
          std::sort(m_vImageFileItems.begin(), m_vImageFileItems.end());
          for (std::vector<ImageFileItem>::iterator itt = m_vImageFileItems.begin(); itt < m_vImageFileItems.end(); ++itt)
            if (itt->ImageExists() && itt->GetAsGroupedByTran()) {
              itt->CreateImageFile(ImageFileItem::ifgPerTran, m_sPathAndRootName, m_imageFormat/* djkzzz , m_bCombineProcDates */ , imageRenderer);
              if (!imageProgress.Hit())
                break;
            }
            else
              break;
        }
        DMPCATCHTHROW("Task: Generate image files grouped by TransactionID")
      }

      if (!imageProgress.GetCancelRequested()) {
        // Create image files grouped by BatchID:
        DMPTRY {
          ImageFileItem::m_sortingMethod = ImageFileItem::smPerBatch;
          std::sort(m_vImageFileItems.begin(), m_vImageFileItems.end());
          for (std::vector<ImageFileItem>::iterator itt = m_vImageFileItems.begin(); itt < m_vImageFileItems.end(); ++itt)
            if (itt->ImageExists() && itt->GetAsGroupedByBatch()) {
              itt->CreateImageFile(ImageFileItem::ifgPerBatch, m_sPathAndRootName, m_imageFormat/* djkzzz , m_bCombineProcDates */ , imageRenderer);
              if (!imageProgress.Hit())
                break;
            }
            else
              break;
        }
        DMPCATCHTHROW("Task: Generate image files grouped by BatchID")
      }
      imageRenderer.m_sImageErrorString = m_sImageErrors;  //CR9916 11/08/2004 ALH

      if (!imageProgress.GetCancelRequested())
        imageProgress.FinalHit();
    }
  }
  DMPCATCHTHROW("ImageFileManager::CreateImageFiles")
  if (!E->IsEmpty())
  {
        bRval = false;
        DMPExceptionHelper::LogException(E->Message);
  }
  delete E;
  E = NULL;
  return bRval;
}

bool ImageFileManager::IsImageFileField(const String& sFieldName)
{
  bool bResult = false;
  DMPTRY {
    //
    // This is meant to be fast:
    // I am looking for a match with one of the following strings:
    //    ImageCheckSingle
    //    ImageCheckPerBatch
    //    ImageCheckPerTransaction
    //    ImageDocumentSingle
    //    ImageDocumentPerBatch
    //    ImageDocumentPerTransaction
    //
    char* zPtr = sFieldName.c_str();
    if (strnicmp(zPtr, "Image", 5) == 0)
      if (strnicmp(zPtr+5, "Check", 5) == 0) {
        if      (stricmp(zPtr+10, "SingleFront"   ) == 0)
          bResult = true;
        else if (stricmp(zPtr+10, "SingleRear"    ) == 0)
          bResult = true;
        else if (stricmp(zPtr+10, "PerBatch"      ) == 0)
          bResult = true;
        else if (stricmp(zPtr+10, "PerTransaction") == 0)
          bResult = true;
      }
      else if (strnicmp(zPtr+5, "File", 4) == 0)   //CR 4969 ~ DLAQAB ~ 3-24-2004
        bResult = true; //ImageFileName would be handled the same way
      else if ((strnicmp(zPtr+5,"FileSize", 8) == 0))// CR 10943 5/4/2005 ALH    - add image size support
        bResult = true;
      else if (strnicmp(zPtr+5, "Document", 8) == 0) {
        if      (stricmp(zPtr+13, "SingleFront"   ) == 0)
          bResult = true;
        else if (stricmp(zPtr+13, "SingleRear"    ) == 0)
          bResult = true;
        else if (stricmp(zPtr+13, "PerBatch"      ) == 0)
          bResult = true;
        else if (stricmp(zPtr+13, "PerTransaction") == 0)
          bResult = true;
      }
      else if (strnicmp(zPtr+5, "Stub", 4) == 0)  // CR 10943 5/4/2005 ALH    - add image size support
        if      (stricmp(zPtr+9, "SingleFront"   ) == 0)
          bResult = true;
        else if (stricmp(zPtr+9, "SingleRear"    ) == 0)
          bResult = true;
        else if (stricmp(zPtr+9, "PerBatch"      ) == 0)
          bResult = true;
        else if (stricmp(zPtr+9, "PerTransaction") == 0)
          bResult = true;
  }
  DMPCATCHTHROW("ImageFileManager::IsImageFileField")
  return bResult;
}

//---------------------------------------------------------------------------

ImageRenderer::ImageRenderer(PicsLoader* picsDll, ImageFileItem::ImageFormat imageFormat)
{

  DMPTRY {
    m_picsDll = picsDll;
    m_imageFormat = imageFormat;
    m_sImageErrorString = "";       // 9916 11/08/2004 ALH
    m_hndlTIFF = ::GlobalAlloc(GHND, 240*10240);
    if (m_hndlTIFF == 0)
      throw DMPException("Insufficient memory to allocate m_hndlTIFF buffer");

    if (imageFormat == ImageFileItem::ifPDF) {
      DMPTRY {
        TPrinter* printer = Printer();
        int iPrinterIndex = -1;
        for (int i = 0; i < printer->Printers->Count; ++i)
          if (printer->Printers->Strings[i] == "Acrobat PDFWriter" /* djkzzz "Win2PDF" */ ) {
            iPrinterIndex = i;
            break;
          }
        printer->PrinterIndex = iPrinterIndex;
        if (iPrinterIndex == -1)
          throw DMPException("'Win2PDF' print driver is not installed");
      }
      DMPCATCHTHROW("Task: Queue up the PDF print driver")
      DMPTRY {
          SetPDFOutputFileName("Dumy.pdf"); // This is here only to inhibit a print-driver dialog from poping up because of 'BeginDoc'.
          Printer()->BeginDoc();
          m_iDevicePixPerInchX = ::GetDeviceCaps(Printer()->Canvas->Handle, LOGPIXELSX);
          m_iDevicePixPerInchY = ::GetDeviceCaps(Printer()->Canvas->Handle, LOGPIXELSY);
          Printer()->Abort();
      }
      DMPCATCHTHROW("Task: Determine PDF print driver's pixel resolution")

    }



    m_dLengthUnitsPerPixX = 254.0/((double) m_iDevicePixPerInchX);
    m_dLengthUnitsPerPixY = 254.0/((double) m_iDevicePixPerInchY);
  }
  DMPCATCHTHROW("ImageRenderer::ImageRenderer")
}

ImageRenderer::~ImageRenderer()
{
  DMPTRY {
    if (m_sLastFileName.Length())
      Printer()->EndDoc();
    ::GlobalFree(m_hndlTIFF);
  }
  DMPCATCHTHROW("ImageRenderer::~ImageRenderer")
}

void ImageRenderer::RenderImage(const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iItem, const String& sType, const String& sTargetFileName, int iPage) //CR9916 10/26/2004 ALH - add support for front and rear images
{
  DMPTRY {
    char* zTiffImage = 0;

    DMPTRY {
      zTiffImage = (char*) ::GlobalLock(m_hndlTIFF);
      if (zTiffImage == 0)
        throw DMPException("'GlobalLock' returned a NULL pointer");
    }
    DMPCATCHTHROW("Task: Obtain memory for TIFF image using 'GlobalLock'")

    int iImageSize;
    DMPTRY {
      iImageSize = m_picsDll->GetItem(TDateTime(dtProcDate).FormatString("yyyymmdd").c_str(), iBank, iLockbox, iBatch, iItem, sType.c_str(), iPage, (BYTE*) zTiffImage);
      if (iImageSize <= 0) {
        String sPICSError = "";
        sPICSError.sprintf("PICS error on call to 'GetItem': for Bank: %d, Lockbox: %d, Batch: %, Item: %d - \n%s",
                                  iBank,
                                  iLockbox,
                                  iBatch,
                                  iItem,
                                  m_picsDll->GetLastPICSError());
                                  m_sImageErrorString += (sPICSError + "\n");
        return;

      }
    }
    DMPCATCHTHROW("Task: Retrieve TIFF image data from PICS Dll")

    int iSnowboundImageHandle;
    DMPTRY {
      iSnowboundImageHandle = IMG_decompress_bitmap_mem(zTiffImage, 0);
      if (iSnowboundImageHandle < 0)
        throw DMPException("Error resulting from call to SnowBound's 'IMG_decompress_bitmap_mem' function:\r\n\r\n"+StringHelper::GetSnowboundErrorText(iSnowboundImageHandle));
    }
    DMPCATCHTHROW("Task: Obtain image handle from SnowBound")

    switch (m_imageFormat) {
      case ImageFileItem::ifTIF: RenderAsTIF(iSnowboundImageHandle, sTargetFileName); break;
      case ImageFileItem::ifPDF: RenderAsPDF(iSnowboundImageHandle, sTargetFileName); break;
      case ImageFileItem::ifTIF2: RenderAsTIF(iSnowboundImageHandle, sTargetFileName); break;
      default: throw DMPException("Illegal case");
    }

    DMPTRY {
      IMG_delete_bitmap(iSnowboundImageHandle);
    }
    DMPCATCHTHROW("Tell SnowBound to delete its copy of the bitmap data")

    DMPTRY {
      if (zTiffImage)
        ::GlobalUnlock(m_hndlTIFF);
    }
    DMPCATCHTHROW("Task: Unlock memory used to hold TIFF images")

  }
  DMPCATCHTHROW("ImageRenderer::RenderImage")
}

// CR 10631 11/15/2004 DJK - First try to save TIF as 'TIFF_G4_FAX'. If error, then try 'TIFF_UNCOMPRESSED'. If still error, throw.
void ImageRenderer::RenderAsTIF(const int iSnowboundImageHandle, const String& sTargetFileName)
{
  DMPTRY {

    //CR 9916 10/15/2004 ALH
    AnsiString sActualFileName = "";
    int iPos = sTargetFileName.UpperCase().Pos(EMBEDTAG);
    if(iPos > 0){  // CR 9916 10/18/2004 ALH remove the embeded file name tag just for when we are getting ready to save the images via Snowboud.
        sActualFileName = sTargetFileName.SubString(TAGSIZE+1,sTargetFileName.Length()-TAGSIZE);
    }
    else{
        sActualFileName =  sTargetFileName;
    }
    int iBitmapImageSize = IMG_save_bitmap(iSnowboundImageHandle, sActualFileName.c_str(), TIFF_G4_FAX);
    if (iBitmapImageSize < 0) {
      iBitmapImageSize = IMG_save_bitmap(iSnowboundImageHandle, sActualFileName.c_str(), TIFF_UNCOMPRESSED);
    }
    if (iBitmapImageSize < 0) {
        String sError = "";
        sError.sprintf("Error resulting from call to SnowBound's 'IMG_save_bitmap' function:\r\n\Attempted file name: %s\r\n\r\n",sActualFileName);
        DMPExceptionHelper::LogException(sError+StringHelper::GetSnowboundErrorText(iBitmapImageSize));
        throw DMPException(sError+StringHelper::GetSnowboundErrorText(iBitmapImageSize));
    }
  }
  DMPCATCHTHROW("ImageRenderer::RenderAsTIF")
}

void ImageRenderer::RenderAsPDF(const int iSnowboundImageHandle, const String& sTargetFileName)
{
  DMPTRY {
    HGLOBAL hndlBitmapMemory;
    char *zBitmapImage;
    long lBitmapImageSize;
    int iWidth, iHeight;

    DMPTRY {
      // Now determine size of bitmap, allocate enough memory to hold the bitmap,
      //   lock the memory in place, and use Snowbound to save the image as a BMP
      //   in that memory.  Then free the Snowbound handle.
      int iWidthTemp, iHeightTemp;
      DMPTRY {
        int iBits;
        IMG_bitmap_info(iSnowboundImageHandle, &iWidthTemp, &iHeightTemp, &iBits);
        hndlBitmapMemory = GlobalAlloc(GHND, (iWidthTemp*iHeightTemp*iBits/8)+10240);    //GW no header 12/28/99
        if (hndlBitmapMemory == 0)
          throw DMPException("Insufficient memory to load image");
      }
      DMPCATCHTHROW("Task: Obtain bitmap dimensions, allocate sufficient memory to contain bitmap")
      DMPTRY {
        zBitmapImage = (char*) GlobalLock(hndlBitmapMemory);
        lBitmapImageSize = IMG_save_bitmap_mem(iSnowboundImageHandle, zBitmapImage, BMP_UNCOMPRESSED);
        if (lBitmapImageSize < 0)
          throw DMPException("Unable to convert to bitmap format");
      }
      DMPCATCHTHROW("Task: Transfer bitmap data from SnowBound buffer to our own buffer")
      DMPTRY {
        iWidth  = 8.0*((double) m_iDevicePixPerInchX);
        iHeight = 8.0*((double) m_iDevicePixPerInchY)*((double) iHeightTemp)/((double) iWidthTemp);
      }
      DMPCATCHTHROW("Task: Determine image dimentions according to print driver pixel resolution")
    }
    DMPCATCHTHROW("Task: Use SnowBound bitmap image handle to obtain bitmap data")

    DMPTRY {
      if (sTargetFileName != m_sLastFileName) {
        if (m_sLastFileName.Length())
          Printer()->EndDoc();

        DMPTRY {
          int iRemember = Printer()->PrinterIndex;
          if (iRemember)
            Printer()->PrinterIndex = 0;
          else
            if (Printer()->Printers->Count > 1)
              Printer()->PrinterIndex = 1;
            else
              Printer()->PrinterIndex = -1;
          Printer()->PrinterIndex = iRemember;
        }
        DMPCATCHTHROW("Task: Unselect, then reselect printer to allow page size to be changed")

        SetPDFOutputFileName(sTargetFileName);

        DMPTRY {
          char ADevice[80];
          char ADriver[80];
          char APort[80];
          THandle hDevMode;

          DMPTRY {
            Printer()->GetPrinter(ADevice, ADriver, APort, hDevMode);
          }
          DMPCATCHTHROW("Task: Get printer information")

          DEVMODE* DeviceMode = (DEVMODE*)::GlobalLock((HGLOBAL)hDevMode);

          DeviceMode->dmFields = DeviceMode->dmFields | DM_PAPERSIZE;
          DeviceMode->dmFields = DeviceMode->dmFields | DM_PAPERWIDTH;
          DeviceMode->dmFields = DeviceMode->dmFields | DM_PAPERLENGTH;
          DeviceMode->dmPaperSize = 0;
          DeviceMode->dmPaperWidth  = /*500*/ (int) (m_dLengthUnitsPerPixX*((double) iWidth ));
          DeviceMode->dmPaperLength = /*500*/ (int) (m_dLengthUnitsPerPixY*((double) iHeight));

          GlobalUnlock((HGLOBAL)hDevMode);
        }
        DMPCATCHTHROW("Task: Setup printer details")

        m_sLastFileName = sTargetFileName;
        Printer()->BeginDoc();
      }
      else
        Printer()->NewPage();
    }
    DMPCATCHTHROW("Task: Manage printing concerns")

    TCanvas* canvas = Printer()->Canvas;

    canvas->Pen->Style = psSolid;
    canvas->Pen->Mode  = pmBlack;
    canvas->Pen->Width = 1;
    canvas->Brush->Style = bsClear;

    // Create a memory stream and load it from the BMP memory image.  Then
    //   unlock and free the global memory handle.
    DMPTRY {
      TMemoryStream* memStream;
      DMPTRY {
        memStream = new TMemoryStream();
      }
      DMPCATCHTHROW("Task: Create 'TMemoryStream' object")
      if (memStream) {
        DMPTRY {
          memStream->Clear();
          memStream->WriteBuffer(zBitmapImage, (int) lBitmapImageSize);
          memStream->Position = 0;
        }
        DMPCATCHTHROW("Task: Copy bitmap data from local buffer into 'stream' object")

        // Create a TBitmap object and load it from the memory stream.  Then
        //   delete the memory stream.
        DMPTRY {
          TRect imageRect(0, 0, /*2362*/ iWidth, /*2362*/ iHeight);

          Graphics::TBitmap* bitmap;
          DMPTRY {
            bitmap = new Graphics::TBitmap();
          }
          DMPCATCHTHROW("Task: Create 'TBitmap' object")
          DMPTRY {
            bitmap->LoadFromStream(memStream);
          }
          DMPCATCHTHROW("Task: Load 'TBitmap' object with bitmap data from stream")
          DMPTRY {
            canvas->StretchDraw(imageRect, bitmap);
          }
          DMPCATCHTHROW("Task: Draw bitmap on page")
          DMPTRY {
            delete bitmap;
          }
          DMPCATCHTHROW("Task: Delete 'TBitmap' object")

          DMPTRY {
            int iWidthOld = canvas->Pen->Width;
            canvas->Pen->Width = (int) (0.013*((double) m_iDevicePixPerInchX));
            canvas->Rectangle(imageRect);
            canvas->Pen->Width = iWidthOld;
          }
          DMPCATCHTHROW("Task: Draw rectangle aroung check image")
        }
        DMPCATCHTHROW("Task: Create 'TBitmap' object, use stream object to transfer bitmap data, draw the bitmap")

        DMPTRY {
          delete memStream;
        }
        DMPCATCHTHROW("Task: Delete 'TMemoryStream' object")
      }
      else
        throw DMPException("Failed to create 'TMemoryStream' object");
    }
    DMPCATCHTHROW("Task: Transfer bitmap data to TBitmap object, draw bitmap")

    DMPTRY {
      GlobalUnlock(hndlBitmapMemory);
    }
    DMPCATCHTHROW("Task: Unlock memory used to hold bitmap data")

    DMPTRY {
      GlobalFree(hndlBitmapMemory);
    }
    DMPCATCHTHROW("Task: Free memory used to hold bitmap data")
  }
  DMPCATCHTHROW("ImageRenderer::RenderAsPDF")
}

void ImageRenderer::SetPDFOutputFileName(const String& sFileName)
{
  DMPTRY {
    TRegistry *reg = new TRegistry();
    reg->RootKey = HKEY_CURRENT_USER;
    if (reg->OpenKey("\\Software\\Adobe\\Acrobat PDFWriter", false)) {
      reg->WriteString ("PDFFileName"  , sFileName);
      reg->WriteBool   ("bExecViewer"  , false          ); // Do not bring up viewer
      reg->CloseKey();
    }
    delete reg;

//djkzzz    TRegistry *reg = new TRegistry();
//djkzzz    reg->RootKey = HKEY_CURRENT_USER;
//djkzzz    if (reg->OpenKey("\\Software\\Dane Prairie Systems\\Win2PDF", false)) {
//djkzzz      reg->WriteString ("PDFFileName", sFileName);
//djkzzz      reg->CloseKey();
//djkzzz    }
//djkzzz    delete reg;
  }
  DMPCATCHTHROW("ImageRenderer::SetPDFOutputFileName")
}

//---------------------------------------------------------------------------

// CR 13806 9/12/2005 DJK - New class added to tally stats for disemination to ExtractAudit and BatchStats tables

void ExtractAuditInfo::Initialize(const String& sOperator, const int iWKID)
{
  DMPTRY {
    Clear();
    m_sOperator = sOperator;
    m_iWKID     = iWKID;
    m_dtStart   = TDateTime::CurrentDateTime();
  }
  DMPCATCHTHROW("ExtractAuditInfo::Initialize")
}

void ExtractAuditInfo::SetExtractResults(const TAggregateList& aggregateFields, const int iExtractRecordCount)
{
  DMPTRY {
    m_bCompleted          = true;
    m_iCheckCount         = (int) (aggregateFields.GetAggregateData("CountChecks"    , rtFile) + 0.5);
    m_iStubCount          = (int) (aggregateFields.GetAggregateData("CountStubs"     , rtFile) + 0.5);
    m_dCheckAmount        =        aggregateFields.GetAggregateData("SumChecksAmount", rtFile);
    m_dStubAmount         =        aggregateFields.GetAggregateData("SumStubsAmount" , rtFile);
    m_iExtractRecordCount = iExtractRecordCount;
  }
  DMPCATCHTHROW("ExtractAuditInfo::SetExtractResults")
}

void ExtractAuditInfo::FlushResultsToDatabase(TADOConnection* adoConnection)
{
  DMPTRY
  {
   int iRetryCount = 0;
   int iWaitCtr    = 0;
   bool bIsDone    = false;
   DMPException *E = new DMPException();
   const iRetryNum = 5;
   //CR 19157 11/21/2006 ALH Begin - add retry logic to updating batch stats.
   do
   {
        DMPTRY
        {
          DMPADOStoredProc sp("proc_EW_Ins_ExtractAudit_InsertNewRow", 2, adoConnection); // CR 14003 9/17/2005 DJK - Incremented version number to '2'
          sp.AddParameter("@parmIsComplete"        , m_bCompleted         );
          sp.AddParameter("@parmExtractCount1"     , m_iCheckCount        );
          sp.AddParameter("@parmExtractCount2"     , m_iStubCount         );
          sp.AddParameter("@parmExtractAmount1"    , m_dCheckAmount       );
          sp.AddParameter("@parmExtractAmount2"    , m_dStubAmount        );
          sp.AddParameter("@parmExtractRecordCount", m_iExtractRecordCount);
          sp.AddParameter("@parmExtractFileName"   , m_sExtractFileName, 256);
          sp.AddParameter("@parmNewExtractAuditID" , "", pdOutput); // CR 14003 9/17/2005 DJK
          sp.ExecProc();
          DMPTRY
          {
            m_sExtractAuditIdGUID = sp.GetParamByName("@parmNewExtractAuditID"); // CR 14003 9/17/2005 DJK
          }
          DMPCATCHTHROW("Task: Retrieve new GUID resulting from insertion")
        }
        DMPCATCH_GET("Task: Insert record into ExtractAudit table", *E)
        if(!E->IsEmpty())
        {
                if(iRetryCount <= iRetryNum)     // try 5 times before failing.
                {
                        DMPExceptionHelper::LogException(E->Message);
                        iRetryCount++;
                        E->Clear();
                        Sleep(1010);  // 1 secon plus 10 millisecond sleep time.
                }
                else
                {
                        throw *E;
                }
        }
        else
        {
             bIsDone = true;
        }
    }
    while(!bIsDone);    // while not done or util we throw an exception.
    // reset counters and done flag
    iRetryCount = 0;
    iWaitCtr    = 0;
    bIsDone     = false;
    E->Clear();
    do
    {
        DMPTRY
        {
          DMPADOStoredProc sp("proc_EW_Ins_BatchStats_InsertNewRow", 1, adoConnection);
          sp.AddParameter("@parmOperator", m_sOperator, 15);
          sp.AddParameter("@parmStation" , m_iWKID);
          sp.AddParameter("@parmStart"   , m_dtStart);
          sp.AddParameter("@parmSeconds" , (int) (24.0*60.0*60.0*(((double) TDateTime::CurrentDateTime())-((double) m_dtStart))+0.5));
          sp.ExecProc();
        }
        DMPCATCH_GET("Task: Insert record into BatchStats table", *E)
        if(!E->IsEmpty())
        {
                if(iRetryCount <= iRetryNum)     // try 5 times before failing.
                {
                        DMPExceptionHelper::LogException(E->Message);
                        iRetryCount++;
                        E->Clear();
                        Sleep(1010); // 1 secon plus 10 millisecond sleep time.
                }
                else
                {
                        throw *E;
                }
        }
        else
        {
             bIsDone = true;
        }
    }
    while(!bIsDone);    // while not done or util we throw an exception.
    E->Clear();
    delete E;
    E = NULL;
    //CR 19157 11/21/2006 ALH End
  }
  DMPCATCHTHROW("ExtractAuditInfo::FlushResultsToDatabase")
}





