//---------------------------------------------------------------------------

#ifndef HelperClassesH
#define HelperClassesH
//---------------------------------------------------------------------------

#include <vector>
#include <Classes.hpp>
#include "DMPExceptions.h"
#include "ImageFileNamingClasses.h"
#include "ExportPics.h"

// NOTE: Tags were changed to be of the same length.  Any new tags must also
//          be this length.  It simplifies the code for searching for the tags.
//          Tag length constant is used for searching for the tags.  MSV 10/17/05
static const String EMBEDTAG            = "<EIMGTAG>";      // CR9916 10/26/2004 ALH
static const String IMGSIZETAG          = "<ISIZTAG>";      // CR9916 11/09/2004 ALH
static const unsigned int IMGTAGLENGTH  = EMBEDTAG.Length();
static const int TAGSIZE                = 14;               // CR9916 10/26/2004 ALH
static const int IMGTAGSIZE             = 12;               // CR9916 11/09/2004 ALH

typedef void __fastcall (__closure *TRecordEvent)(int &Bank, int &Cust, int &Box, int &Batch, int &Item, const char *ProcDate, const char *DepDate);

typedef void __fastcall (__closure *TImageProgressEvent)(double dProgressRatio, bool &bAborted);

typedef void __fastcall (__closure *TCancelEvent)(bool &Aborted);

typedef void __fastcall (__closure *TFinishEvent)(bool Success, const char *ConfirmationFieldsData);

typedef void __fastcall (__closure *TErrorEvent)(const char *ErrorMessage);


template<class T>
  bool BinarySearch(const std::vector<T>& list, const T& pivot, int& iPos) // return 'true' if found, 'false' if not. 'iPos' indicates position if found, or correct insertion point if not found.
  {
    bool bFound = false;
    DMPTRY {
      iPos = 0;
      int iLo = 0;
      int iHi = (int) list.size();
      int i;
      while (iLo < iHi) {
        i = (iLo+iHi)/2;
        if (list[i] == pivot) {
          bFound = true;
          iPos = i;
          break;
        }
        if (list[i] < pivot) {
          iLo = i+1;
          iPos = iLo;
        }
        else {
          iHi = i;
          iPos = iHi;
        }
      }
    }
    DMPCATCHTHROW("BinarySearch (template)")
    return bFound;
  };

class FieldItem;
class ImageFieldInfo;

class FieldValue
{
friend FieldItem;
private:
  int    m_iRow;
  String m_sValue;
public:
  FieldValue() {m_iRow = -1;}
  FieldValue(const FieldValue& obj) {*this = obj;}
  FieldValue(int iRow, const String& sValue) {m_iRow = iRow; m_sValue = sValue;}
  FieldValue& operator  = (const FieldValue& obj) {m_iRow = obj.m_iRow; m_sValue = obj.m_sValue; return *this;}
  bool        operator == (const FieldValue& obj) const {return (m_iRow == obj.m_iRow);}
  bool        operator  < (const FieldValue& obj) const {return (m_iRow  < obj.m_iRow);}
};

class FieldItemHelper
{
friend FieldItem;
  static FieldValue m_fieldVal;
};

// CR 12104 3/30/2005 DJK - Enhanced to handle fields with the 'As' clause in the SQL
class FieldItem
{
private:
  String m_sTableName;
  String m_sFieldName;
  String m_sFieldNameAs;
  std::vector<FieldValue> m_vValues;
public:
  FieldItem() {}
  FieldItem(const FieldItem& obj) {*this = obj;}
  FieldItem(const String& sName);
  FieldItem& operator  = (const FieldItem& obj) {m_sTableName = obj.m_sTableName; m_sFieldName = obj.m_sFieldName; m_sFieldNameAs = obj.m_sFieldNameAs; m_vValues = obj.m_vValues; return *this;}
  bool       operator == (const FieldItem& obj) const {return (m_sTableName.AnsiCompareIC(obj.m_sTableName) == 0 && m_sFieldName.AnsiCompareIC(obj.m_sFieldName) == 0 && m_sFieldNameAs.AnsiCompareIC(obj.m_sFieldNameAs) == 0);}
  bool       operator  < (const FieldItem& obj) const;
  String GetName() const;
  String GetTableName() const {return m_sTableName;}
  String GetFieldName() const {return m_sFieldName;}
  void   SetValue(const int iRow, const String& sValue);
  String GetValue(const int iRow) const;
  void   Clear() {m_vValues.clear();}
  int    GetMaxRowIdx() const {int iResult = -1; if (m_vValues.size()) iResult = m_vValues.back().m_iRow; return iResult;}
  void   AlterTableNameToDataEntry();
};

class FieldCollection
{
private:
  bool m_bSorted;
  std::vector<FieldItem> m_vFields;
public:
  FieldCollection(bool bSorted = true) {m_bSorted = bSorted;}
  FieldCollection(const FieldCollection& obj) {*this = obj;}
  FieldCollection& operator = (const FieldCollection& obj) {m_bSorted = obj.m_bSorted; m_vFields = obj.m_vFields; return *this;}
  FieldItem& operator [] (const int iIdx);
  const FieldItem& operator [] (const int iIdx) const;
  FieldItem& operator [] (const String& sFieldName);
  const FieldItem& operator [] (const String& sFieldName) const;
  void   ClearFields() {m_vFields.clear();}
  void   ClearRows();
  int    Add(const String& sFieldName, bool bUnique = false);
  void   Add(const FieldCollection& fieldCollection);
  void   AddRow(const FieldCollection& fieldCollection, const int iIdx);
  void   Insert(const int iIdx, const String& sFieldName, bool bUnique = false);
  void   InsertCommaDelimitedList(const int iIdx, const String& sListOfFieldNames, bool bUnique = false); // CR 12104 3/30/2005 DJK - Added this 'convenience' function
  void   Remove(const int iIdx);
  int    Locate(const String& sFieldName) const;
  int    FieldCount() const {return (int) m_vFields.size();}
  int    RowCount() const;
  String AsList() const;

  String AsCommaList(const int iRow) const; // Debug purposes only

private:
  void CheckIdx(const int iIdx, const int iCount) const;
  int  NonBinarySearch(const String& sFieldName) const;
  int  Add(const FieldItem& fieldItem, bool bUnique = false);
};

class FieldCollectionViewer // For debugging purposes - this will show you entire contents
{
private:
  int m_iRows;
  int m_iCols;
//  std::vector<String> m_vContents;
  char** m_zArr;
public:
  FieldCollectionViewer() {m_iRows = m_iCols = 0; m_zArr = 0;}
  FieldCollectionViewer(const FieldCollectionViewer& obj) {*this = obj;}
  FieldCollectionViewer(const FieldCollection& fieldCollection);
  ~FieldCollectionViewer();
  FieldCollectionViewer& operator = (const FieldCollectionViewer& obj);
private:
  void DeleteArray();
};

class LineCountManager
{
private:
  static int m_iCounts;
  int m_iLineCount;
  std::vector<int> m_vOrigins; // Counts for rtFile, rtBank, rtCustomer, rtLockbox, rtBatch, rtTransaction, rtCheck, rtStub, rtDocument
  std::vector< std::vector< std::vector<int> > > m_vSavedCounts; // spongy 3-D array.
  bool m_bSavedValuesUsed;
public:
  LineCountManager();
  void Reset(const int iRecordType);
  int  GetCount(const int iRecordType, const int iRecordTypeForSaved = 0);
  void Initialize();
  void IncCount();
  void EnsureCountsForAfterListEmpty(const int iRecordTypeForSaved);
  void SaveCountsForAfter(const int iRecordTypeForSaved);
  void ClearOneCountForAfter(const int iRecordTypeForSaved);
private:
  void CheckForBoundsViolation(const int iRecordType, const int iCountLimit, int iIdx = 0) const;
};

class OutputManager
{
private:
  bool m_bIsHtmlTable;
  String m_sTargetFile;
  TFileStream *m_tmpHtml;
  TFileStream *m_outfile;
  LineCountManager* m_lineCountManager;
public:
  OutputManager();
  OutputManager(const OutputManager& obj) {Init(); *this = obj;}
  ~OutputManager();
  String SetFilePathAndVerifyPathing(String sExtractPath); //CR 15923 6/16/2006 ALH
  int FindNextSlashFromEndofString(String s);              //CR 15923 6/16/2006 ALH
  OutputManager& operator = (const OutputManager& obj) {m_bIsHtmlTable = obj.m_bIsHtmlTable; m_sTargetFile = obj.m_sTargetFile; m_lineCountManager = obj.m_lineCountManager; return *this;}
  void SetLineCountManager(LineCountManager* lineCountManager) {m_lineCountManager = lineCountManager;}
  void SetIsHtmlTable(bool bIsHtmlTable) {m_bIsHtmlTable = bIsHtmlTable;}
  bool OpenExtractFiles(bool bIsHtmlTable);
  void CloseExtractFiles();
  bool GetIsHtmlTable() const {return m_bIsHtmlTable;}
  void SetTargetFile(const String& sTargetFile) {m_sTargetFile = sTargetFile;}
  String GetTargetFile() const {return m_sTargetFile;}
  void ExportToHtml(const String& sHeaderFile, const String& sFooterFile);
  void WriteFile(const String& sText);
  void WriteHtml(const String& sText);
  int GetHtmlSize() const;
  int GetFilePos() const;
  int GetHtmlPos() const;
  void AdjustLastLineFeed(const String& sRecordDelimiter);
  void FlipFiles1(TFileStream* tmpFile, TFileStream*& tmpHtmlHeader, const int iPos, const int iPosHtml, const String& sTempHtmlFileName);
  void FlipFiles2(TFileStream* tmpFile, TFileStream*  tmpHtmlHeader);
private:
  void CopyTempFile(TFileStream* fsSource, TFileStream* fsDest, const int iStartPos);
  void Init();
};

class TDMPADOCommand;

// CR 13806 10/04/2005 - New class created for handling update of batch table after extract
class ESNBatchUpdate
{
private:
  int    m_iExtractSequenceNumber;
  String m_sGlobalBatchIDList;
public:
  ESNBatchUpdate() {m_iExtractSequenceNumber = 0;}
  void SetESN(const int iExtractSequenceNumber) {m_iExtractSequenceNumber = iExtractSequenceNumber; m_sGlobalBatchIDList = "";}
  void AddGlobalBatchID(const int iGlobalBatchID);
  void UpdateBatchTable(TDMPADOCommand* adoCommand);
};

// CR 5272 1/07/2004 DJK
class TableHelper
{
public:
  static String GetTable(int iType);
};

struct StringHelper
{
  static String GetLastErrorText(int iError = -1);
  static String GetSnowboundErrorText(const int iError);
};

struct FileHelper
{
  static std::vector<String> FindFiles  (const String& sFileName, bool bDirectories = false); // Wildcards allowed (looks for either files, or directories, but never both at once)
  static void                DeleteFiles(const String& sFileName); // Wildcards allowed
};

class PicsLoader;
class ImageRenderer;

class ImageFileItem
{
public:
  enum ImageFormat       {ifNone , ifTIF, ifPDF, ifTIF2};
  enum ImageFileGrouping {ifgNone, ifgSingle, ifgPerTran, ifgPerBatch};
  enum SortingMethod     {smNone , smDefault, smSingle, smPerTran, smPerBatch, smImageSize};
private:
  int           m_iBank;
  int           m_iLockbox;
  int           m_iBatch;
  int           m_iTransaction;
  TDateTime     m_dtProcDate;
  int           m_iItem;
  String        m_sType;

  int           m_iBatchTypeCode;

  bool          m_bImageExists;    // i.e. - Pics reports an image.
  bool          m_bSingle;         // i.e. - This image is required as a single.
  bool          m_bGroupedByTran;  // i.e. - This image is required as part of a transaction combo.
  bool          m_bGroupedByBatch; // i.e. - This image is required as part of a batch combo.
  int           m_iPage;           // CR9916 10/27/2004 ALH - add functionality to retrieve backs of images.

  int           m_iImageFileNumber; // This is the number than is appended to the 'single' image files.
  int           m_iImgFieldColSize;  //CR9916 11/09/2004 ALH
public:
  static SortingMethod          m_sortingMethod;
  static bool                   m_bImageFileZeroPad;
  static bool                   m_bImageFileBatchTypeFormatFull;
  static String                 m_sProcDateFormat;
  static std::vector<EditToken> m_vTokenSingle  ; // This array defines the layout of the 'Single' file names.
  static std::vector<EditToken> m_vTokenPerTran ; // This array defines the layout of the 'Per Transaction' file names.
  static std::vector<EditToken> m_vTokenPerBatch; // This array defines the layout of the 'Per Batch' file names.

public:
  ImageFileItem();
  ImageFileItem(const ImageFileItem& obj) {*this = obj;}
  ImageFileItem(const String& sImageRequestType, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const TDateTime dtProcDate, const int iItem, const String& sType, int iPage=PICS_FRONT/* , int iImgFieldColSize*/);
  ImageFileItem& operator =  (const ImageFileItem& obj);
  bool           operator == (const ImageFileItem& obj) const;
  bool           operator <  (const ImageFileItem& obj) const;
  void   SetAsSingle        () {m_bSingle         = true;}
  void   SetAsGroupedByTran () {m_bGroupedByTran  = true;}
  void   SetAsGroupedByBatch() {m_bGroupedByBatch = true;}
  void   SetImageFileNumber(const int iImageFileNumber) {m_iImageFileNumber = iImageFileNumber;}
  bool   GetAsSingle        () {return m_bSingle        ;}
  bool   GetAsGroupedByTran () {return m_bGroupedByTran ;}
  bool   GetAsGroupedByBatch() {return m_bGroupedByBatch;}
  int    GetImageHitCount(){return (m_bImageExists ? ((m_bSingle ? 1 : 0)+ (m_bGroupedByTran ? 1 : 0) + (m_bGroupedByBatch ? 1 : 0)) : 0);};
  void   DetermineExistenceOfImage(PicsLoader* picsDllPtr);
  bool   ImageExists() const {return m_bImageExists;}
  String GetImageFileName(const String& sImageRequestType, const String& sPathAndRootName, ImageFormat imageFormat/* djkzzz , bool bCombineProcDates */ );
  void   CreateImageFile(ImageFileGrouping imageFileType, const String& sPathAndRootName, ImageFormat imageFormat/* djkzzz , bool bCombineProcDates */ , ImageRenderer& imageRenderer, int iPage=PICS_FRONT);
private:
  String GetImageFileName(ImageFileGrouping imageFileType, const String& sPathAndRootName, ImageFormat imageFormat, bool bRendering = false/* djkzzz , bool bCombineProcDates */ );     //CR9916 11/12/2004 ALH
  int CompareTypes(const String& sType1, const String& sType2) const;
  void Initialize();
public:
  __property int Page={read=m_iPage, write=m_iPage};  // CR9916 10/27/2004 ALH - add functionality to retrieve backs of images.
  __property int ImgFieldColSize = {read=m_iImgFieldColSize, write=m_iImgFieldColSize};  // CR9916 11/09/2004 ALH
};

class ImageFileManager
{
private:
  String                     m_sPathAndRootName;
//djkzzz  bool                       m_bCombineProcDates;
  int                        m_iFileCounter; // This is the current upper number that is appended to the 'single; image files.
  std::vector<ImageFileItem> m_vImageFileItems;
  TImageProgressEvent        m_eventOnImageProgress;
  PicsLoader*                m_picsDll; // The actual object is created and destroyed by the 'TExtract' class.
  String                     m_sImageErrors;      //CR9916 11/08/2004 ALH
  int                        m_iImgFieldColSize;  //CR9916 11/09/2004 ALH
public:
  ImageFileItem::ImageFormat m_imageFormat;
  ImageFileManager();
  ~ImageFileManager();
  String GetImageError() { return m_sImageErrors;}; // CR9916 11/08/2004 ALH
  void   Initialize(TImageProgressEvent eventOnImageProgress, PicsLoader* picsDll, const String& sPathAndRootName, ImageFileItem::ImageFormat imageFormat, bool bCombineProcDates, const String& sImageFileProcDateFormat, const bool bImageFileZeroPad, const bool bImageFileBatchTypeFormatFull, const String& sImageFileNameSingle, const String& sImageFileNamePerTran, const String& sImageFileNamePerBatch);
  String StoreImageRequestAndReturnFileName(const String& sImageRequestType, const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const int iItem, const String& sType, int iPage=PICS_FRONT);
  bool   CreateImageFiles();
  static bool IsImageFileField(const String& sFieldName);
  String ReturnImageSize(const String& sImageRequestType, const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iBatchTypeCode, const int iTransaction, const int iItem, const String& sType, const int iPage);// CR9916 10/27/2004 ALH
  __property int ImgFieldColSize = {read=m_iImgFieldColSize, write=m_iImgFieldColSize};  // CR9916 11/09/2004 ALH

};

class ImageProgressReporter
{
private:
  TImageProgressEvent m_eventOnImageProgress;
  int m_iImageHitCountTotal;
  int m_iNumberOfReports;
  int m_iImageHitCounter;
  int m_iImageHitCounterInterval;
  int m_iImageHitCounterNext;
  bool m_bCancelRequested;
public:
  ImageProgressReporter() {m_eventOnImageProgress = 0; m_bCancelRequested = false;}
  ImageProgressReporter(TImageProgressEvent eventOnImageProgress, int iImageHitCountTotal, int iNumberOfReports);
  bool Hit();
  void FirstHit();
  void FinalHit();
  bool GetCancelRequested() const {return m_bCancelRequested;}
};

class ImageRenderer
{
private:
  ImageFileItem::ImageFormat m_imageFormat;
  HGLOBAL                    m_hndlTIFF;
  PicsLoader*                m_picsDll; // The actual object is created and destroyed by the 'TExtract' class.
  String                     m_sLastFileName;
  int                        m_iDevicePixPerInchX;
  int                        m_iDevicePixPerInchY;
  double                     m_dLengthUnitsPerPixX;
  double                     m_dLengthUnitsPerPixY;
public:
  ImageRenderer(PicsLoader* picsDll, ImageFileItem::ImageFormat imageFormat);
  ~ImageRenderer();
  void RenderImage(const TDateTime dtProcDate, const int iBank, const int iLockbox, const int iBatch, const int iItem, const String& sType, const String& sTargetFileName, int iPage=PICS_FRONT);
  String                     m_sImageErrorString;       // 9916 11/08/2004 ALH
private:
  void RenderAsTIF(const int iSnowboundImageHandle, const String& sTargetFileName);
  void RenderAsPDF(const int iSnowboundImageHandle, const String& sTargetFileName);
  void SetPDFOutputFileName(const String& sFileName);
};

class TAggregateList;

// CR 13806 9/12/2005 DJK - New class added to tally stats for disemination to ExtractAudit and BatchStats tables
class ExtractAuditInfo
{
private:
  String    m_sExtractFileName;
  String    m_sOperator;
  bool      m_bCompleted;
  int       m_iCheckCount;
  int       m_iStubCount;
  double    m_dCheckAmount;
  double    m_dStubAmount;
  int       m_iExtractRecordCount;
  TDateTime m_dtStart;
  int       m_iWKID;
  String    m_sExtractAuditIdGUID; // CR 14003 9/17/2005 DJK
public:
  ExtractAuditInfo() {Clear();}
  ExtractAuditInfo(const ExtractAuditInfo& obj) {*this = obj;}
  ExtractAuditInfo& operator = (const ExtractAuditInfo& obj) {m_sExtractFileName = obj.m_sExtractFileName; m_sOperator = obj.m_sOperator; m_bCompleted = obj.m_bCompleted; m_iCheckCount = obj.m_iCheckCount; m_iStubCount = obj.m_iStubCount; m_dCheckAmount = obj.m_dCheckAmount; m_dStubAmount = obj.m_dStubAmount; m_iExtractRecordCount = obj.m_iExtractRecordCount; m_dtStart = obj.m_dtStart; m_iWKID = obj.m_iWKID; return *this;}
  void   Initialize(const String& sOperator, const int iWKID);
  void   SetExtractFileName(const String& sExtractFileName) {m_sExtractFileName = sExtractFileName;}
  void   SetExtractResults(const TAggregateList& aggregateFields, const int iExtractRecordCount);
  void   FlushResultsToDatabase(TADOConnection* adoConnection);
  String GetExtractAuditIdGUID() const {return m_sExtractAuditIdGUID;} // CR 14003 9/17/2005 DJK
private:
  void Clear() {m_sExtractFileName = m_sOperator = m_sExtractAuditIdGUID = ""; m_bCompleted = false; m_iCheckCount = m_iStubCount = m_iExtractRecordCount = m_iWKID = 0; m_dCheckAmount = m_dStubAmount = 0.0; m_dtStart = 0.0;}
};


#endif
