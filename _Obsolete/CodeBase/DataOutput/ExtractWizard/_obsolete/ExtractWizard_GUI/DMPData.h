//---------------------------------------------------------------------------

#ifndef DMPDataH
#define DMPDataH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <Db.hpp>
#include "DMPADOQuery.h"
#include "DMPADOCommand.h"
#include "DMPADODataSet.h"
//---------------------------------------------------------------------------
class TDMPDataMod : public TDataModule
{
__published:	// IDE-managed Components
        TADOConnection *DMPConnection;
  TDMPADODataSet *BankDataSet;
  TDMPADODataSet *CustomerDataSet;
  TDMPADODataSet *LockboxDataSet;
  TDMPADODataSet *BatchDataSet;
  TDMPADODataSet *TranDataSet;
  TDMPADODataSet *ChecksDataSet;
  TDMPADODataSet *StubsDataSet;
  TDMPADODataSet *DocsDataSet;
        TDataSource *BankDS;
        TDataSource *CustomerDS;
        TDataSource *LockboxDS;
        TDataSource *BatchDS;
        TDataSource *TranDS;
        TDataSource *ChecksDS;
        TDataSource *StubsDS;
        TDataSource *DocsDS;
  TDMPADOCommand *ActionCmd;
  TDMPADODataSet *ActionDataSet;
        void __fastcall DataModuleDestroy(TObject *Sender);
        void __fastcall BankDataSetAfterScroll(TDataSet *DataSet);
        void __fastcall CustomerDataSetAfterScroll(TDataSet *DataSet);
        void __fastcall LockboxDataSetAfterScroll(TDataSet *DataSet);
        void __fastcall BatchDataSetAfterScroll(TDataSet *DataSet);
        void __fastcall TranDataSetAfterScroll(TDataSet *DataSet);
  void __fastcall BatchQryAfterOpen(TDataSet *DataSet);
  void __fastcall TranQryAfterOpen(TDataSet *DataSet);
private:	// User declarations
        void __fastcall RequeryChecksStubsDocsOnNewGlobalBatchID();
        void __fastcall RefilterChecksStubsDocsOnNewTransactionID();
public:		// User declarations
        __fastcall TDMPDataMod(TComponent* Owner);
        int RecBank, RecCustomer, RecLockbox, RecBatch, RecBatchTypeCode, RecItem; // CR 5815 03/03/2004 DJK - Added 'RecBatchTypeCode'
        String ProcDateStr;
        String DepDateStr;
        String DatabaseName;
        String ServerName;
        void __fastcall ClearQueries();
        void __fastcall CloseConnection();
        void __fastcall InitializeCounters();
        bool __fastcall IsConnected();
        bool __fastcall OpenConnection(String Server, String Database, String DBUser, String Pswd, int ADOCommandTimeout);
        bool __fastcall OpenDatabase();
        void __fastcall GetFieldNames(String Table, TStrings *Flds); // CR 5272 1/07/2004 DJK
        int  __fastcall GetColumnSQLDataType( String TblName, String FldName ); // CR #14581/15136  12/20/05  MSV
};    
//---------------------------------------------------------------------------
extern PACKAGE TDMPDataMod *DMPDataMod;
//---------------------------------------------------------------------------
#endif
