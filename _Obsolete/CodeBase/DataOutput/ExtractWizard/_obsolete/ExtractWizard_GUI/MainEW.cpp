/******************************************************************************/
/**********                                                          **********/
/******       DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.        ******/
/* Copyright Data Management Products, Inc. ("DMP") 2001.  All Rights       */
/* Reserved.  These materials are unpublished confidential and proprietary    */
/* information of DMP and contain DMP trade secrets.  These materials may not */
/* be used, copied, modified or disclosed except as expressly permitted in    */
/* writing by DMP (see the DMP license agreement for details).  All copies,   */
/* modifications and derivative works of these materials are property of DMP. */
/*                                                                            */
 /******************************************************************************/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include "MainEW.h"
#include "ExportAbout.h"
#include "ExportSentry.h"
#include "HandleIni.h"
#include "Results.h"
#include "Record.h"
#include "lbenumtypes.h"
#include "HandleIni.h"
#include "Globals.h"
#include "DMPExceptions.h"
#include <sstream>
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TMainFormEW *MainFormEW;
//---------------------------------------------------------------------------
__fastcall TMainFormEW::TMainFormEW(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    DMPTRY {
      Application->OnException = GlobalExceptionHandler;
    }
    DMPCATCHTHROW("Task: Establish substitute exception handler routine")
    Module = "ExtractWizard";
    OpenDialog->OnCanClose = NULL;
    char Op[OPERATORSIZE + 1];
    if (Globals::m_sentryDll->_RetrieveOperatorLogon(Op))
      OprID = String(Op);
    if (!OpenDatabase()){
      DoError("Unable to open database.  Application will now close.");
      Close();
      Application->Terminate();
      return;
    }
    if (!GetSetupPath()){
      DoError("Unable to locate setup path in Mapper.ini, [WIZARD], SetupPath=.  Application will now close.");
      Close();
      Application->Terminate();
      return;
    }
    String LicenseFile = ExtractFilePath(Application->ExeName) + "License.dat";
    if (!FileExists(LicenseFile)){
      DoError("Unable to locate license file ("+LicenseFile+").  Application will now close.  Please contact technical support.");
      Close();
      Application->Terminate();
      return;
    }
    Security.ReadMaxExtracts(LicenseFile);
    if (Security.MaxExtracts < 0)
      DoError("Unable to read license file.  Please contact technical support.");
  }
  DMPCATCHTHROW("TMainFormEW::TMainFormEW")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::FormCloseQuery(TObject *Sender, bool &CanClose)
{
    //CR20894 MJH 4-30-2007: RunExtract is now in seperate process, so no need to check if it's running
    CanClose = true;
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    // CR 7411 ~ DLAQAB ~ 3-26-2004
    // Added the if block bellow to Save the changes if any
    if( ID_CANCEL == RecordForm->SaveChangesIfAny(NULL, true) )
    {
      return;
    }
  }
  DMPCATCHTHROW("TMainFormEW::FormClose")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::FormDestroy(TObject *Sender)
{
//
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::New1Click(TObject *Sender)
{
  DMPTRY {
    RecordForm->New();
    RecordForm->PostDLLPathFileNameEdit->Enabled          = false;  //CR10943 03/30/2005 ALH
    RecordForm->DLLBrowsePathFileNameButton->Enabled      = false;  //CR10943 03/30/2005 ALH
    RecordForm->PostDLLPathFileNameEdit->Text             = "";     //CR10943 03/30/2005 ALH
    RecordForm->PostDLLLabel->Enabled                     = false;  //CR10943 03/30/2005 ALH
    ChangeForm(RecordForm);
  }
  DMPCATCHTHROW("TMainFormEW::New1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::Open1Click(TObject *Sender)
{
  DMPTRY {
    // CR 7411 ~ DLAQAB ~ 3-26-2004
    // Added the if block bellow to Save the changes if any
    if( ID_CANCEL == RecordForm->SaveChangesIfAny(NULL, true) )
    {
      return;
    }

    OpenDialog->FileName = "";
    OpenDialog->Filter = "CheckBOX Extract Setup Files (*.cxs)|*.cxs";
    OpenDialog->DefaultExt = "*.cxs";
    OpenDialog->OnCanClose = OpenDialogCanClose;
    if (OpenDialog->Execute())
      LoadSetup(OpenDialog->FileName);
    OpenDialog->OnCanClose = NULL;
  }
  DMPCATCHTHROW("TMainFormEW::Open1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::Save1Click(TObject *Sender)
{
  DMPTRY {
    RecordForm->Save();
  }
  DMPCATCHTHROW("TMainFormEW::Save1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::SaveAs1Click(TObject *Sender)
{
  DMPTRY {
    if (Security.CanSave(SetupPath)){
      if (SaveDialog->Execute())
        RecordForm->SaveAs(SaveDialog->FileName);
    }
    else
      DoError("Unable to save.  Your maximum of " + IntToStr(Security.MaxExtracts) + " extracts has been met.");
  }
  DMPCATCHTHROW("TMainFormEW::SaveAs1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::PrintCxsFileClick(TObject *Sender)
{
  DMPTRY {
    if (PrintDialog->Execute())
      if (ActiveMDIChild == RecordForm)
        RecordForm->Print();
      else
        if (ActiveMDIChild == ResultForm)
          ResultForm->Print();
  }
  DMPCATCHTHROW("TMainFormEW::PrintCxsFileClick")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::PrintSetup1Click(TObject *Sender)
{
  DMPTRY {
    PrinterSetupDialog->Execute();
  }
  DMPCATCHTHROW("TMainFormEW::PrintSetup1Click")
}
//---------------------------------------------------------------------------
// CR 14584 06/01/2006 DJK
void __fastcall TMainFormEW::SetupReportPreviewClick(TObject *Sender)
{
  DMPTRY
  {
    RecordForm->Save();                 //CR 17370 02/09/2007 ALH
    RecordForm->ChangeToGeneralTab();   //CR 17370 02/09/2007 ALH
    RecordForm->SetupReportPreview();
  }
  DMPCATCHTHROW("TMainFormEW::SetupReportPreviewClick")
}
//---------------------------------------------------------------------------
// CR 14584 06/01/2006 DJK
void __fastcall TMainFormEW::SetupReportPrintClick(TObject *Sender)
{
  DMPTRY
  {
    RecordForm->Save();                 //CR 17370 02/09/2007 ALH
    RecordForm->ChangeToGeneralTab();   //CR 17370 02/09/2007 ALH
    RecordForm->SetupReportPrint();
  }
  DMPCATCHTHROW("TMainFormEW::SetupReportPrintClick")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::Exit1Click(TObject *Sender)
{
  DMPTRY {
    Close();
  }
  DMPCATCHTHROW("TMainFormEW::Exit1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::Setup1Click(TObject *Sender)
{
  DMPTRY {
    String FileName;
    OpenDialog->InitialDir = SetupPath;
    //CR20894 MJH 4-30-07: RunForm is no longer available
    if (ActiveMDIChild == ResultForm)
      FileName = ResultForm->GetSetupFile();
    else
      FileName = RecordForm->GetSetupFile();
    if (!FileName.IsEmpty())
      try {
        LoadSetup(FileName);
      }
      catch(...){
        Open1Click(NULL);
      }
    else
      Open1Click(NULL);
  }
  DMPCATCHTHROW("TMainFormEW::Setup1Click")
}
//---------------------------------------------------------------------------

void __fastcall TMainFormEW::Results1Click(TObject *Sender)
{
  DMPTRY {
    //CR20894 MJH 4-30-07: RunForm no longer available.  Browse for results file
    OpenDialog->FileName = "";
    OpenDialog->Filter = "CheckBOX Extract Result Files (*.cxr)|*.cxr|Text files (*.txt)|*.txt|Any file (*.*)|*.*";
    OpenDialog->DefaultExt = "*.cxr";
    if (OpenDialog->Execute()) {
        LoadResults(OpenDialog->FileName, "");
    }
  }
  DMPCATCHTHROW("TMainFormEW::Results1Click")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::LogFile1Click(TObject *Sender)
{
  DMPTRY {
    String LogFile;
    //CR20894 MJH 4-30-07: RunForm no longer available.  Search for log file manually
    OpenDialog->FileName = "";
    OpenDialog->Filter = "CheckBOX Extract Log Files (*.log)|*.log|Text files (*.txt)|*.txt";
    OpenDialog->DefaultExt = "*.log";
    if (OpenDialog->Execute())
      LogFile = OpenDialog->FileName;

    if (!LogFile.IsEmpty()){
      ResultForm->LoadFile("Extract Log - ", LogFile, "");
      ChangeForm(ResultForm);
    }
  }
  DMPCATCHTHROW("TMainFormEW::LogFile1Click")
}


//---------------------------------------------------------------------------
void __fastcall TMainFormEW::Run1Click(TObject *Sender)
{
  DMPTRY {
    bool Proceed = true;
    String SetupFile = RecordForm->GetSetupFile();
    if (ActiveMDIChild == RecordForm)
      Proceed = RecordForm->Save();
    if (Proceed){
      Proceed = Security.CanRun(SetupPath);
      if (!Proceed)
        DoError("The number of extracts in your extract directory exceeds the number permitted by your license (" + IntToStr(Security.MaxExtracts) + ").");
      else {
        SetupFile = RecordForm->GetSetupFile();
        if (SetupFile.IsEmpty()){
          OpenDialog->FileName = "";
          OpenDialog->OnCanClose = OpenDialogCanClose;
          OpenDialog->Filter = "CheckBOX Extract Setup Files (*.cxs)|*.cxs";
          OpenDialog->DefaultExt = "*.cxs";
          if (OpenDialog->Execute())
            SetupFile = OpenDialog->FileName;
          OpenDialog->OnCanClose = NULL;
        }
        if (!SetupFile.IsEmpty()){
          //CR20894 MJH 4-27-07: Now calling .net app, ExtractRun
          //Assuming it's in the current directory
          this->Visible = false; // hide ExtractWizard while ExtractRun is open
          LaunchExtractRun(SetupFile);
          this->Visible = true;  // ExtractRun is closed.  Show ExtractWizard again
        }
      }
    }
  }
  DMPCATCHTHROW("TMainFormEW::Run1Click")
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::LaunchExtractRun(const String& SetupFile)
{
    //CR20894 MJH 4-29-07: Create and launch a new process.  This process will then 'wait' on ExtractRun to return.
    PROCESS_INFORMATION ProcessInfo;
    STARTUPINFO StartupInfo = {0};
    StartupInfo.cb = sizeof(STARTUPINFO);

    // build command line
    std::ostringstream ost;
    ost << "ExtractRun.exe "
        << "/scriptpath:" << SetupFile.c_str()
        << " /test";


    // need to use a c-style string in the process call
    char* cmd = new char[ost.str().size()];
    strcpy(cmd, ost.str().c_str());
    if (CreateProcess(  NULL,
                        cmd,
                        NULL,
                        NULL,
                        FALSE,
                        0,
                        NULL,
                        NULL,
                        &StartupInfo,
                        &ProcessInfo))
    {
        // process started successfully.  Wait for it to return.
        WaitForSingleObject(ProcessInfo.hProcess, INFINITE);
    } else {
        // process didn't start.  Display error
        LPVOID lpMsgBuf;
 
        FormatMessage(  FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
                        NULL,
                        GetLastError(),
                        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                        (LPTSTR) &lpMsgBuf,
                        0,
                        NULL);

        MessageBox(NULL, (LPCTSTR)lpMsgBuf, "ExtractWizard", MB_OK|MB_ICONINFORMATION);
        LocalFree( lpMsgBuf );
    }

    delete[] cmd;
}
//---------------------------------------------------------------------------
void __fastcall TMainFormEW::About1Click(TObject *Sender)
{
  DMPTRY {
    Globals::m_aboutDll->_ShowAbout();
  }
  DMPCATCHTHROW("TMainFormEW::About1Click")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::More1Click(TObject *Sender)
{
  DMPTRY {
    MessageDlg("Server: " + DataMod->ServerName + ", Database: " + DataMod->DatabaseName, mtInformation, TMsgDlgButtons()<<mbOK, 0);
  }
  DMPCATCHTHROW("TMainFormEW::More1Click")
}

//---------------------------------------------------------------------------

void __fastcall TMainFormEW::License1Click(TObject *Sender)
{
  DMPTRY {
    MessageDlg("Maximum number of extracts permitted = " + IntToStr(Security.MaxExtracts), mtInformation, TMsgDlgButtons()<<mbOK, 0);
  }
  DMPCATCHTHROW("TMainFormEW::License1Click")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::GlobalExceptionHandler(System::TObject* Sender, Sysutils::Exception* E)
{
  DMPExceptionHelper::LogException(E->Message);
  Application->MessageBox(E->Message.c_str(), "ExtractWizard Error", MB_OK);
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::ChangeForm(TForm *NewForm)
{
  DMPTRY {
    EnableMenuItems(NewForm);
    SendMessage(Handle, WM_SETREDRAW, 0, 0);
    NewForm->BringToFront();
    ActiveMDIChild->WindowState = wsMaximized;
    SendMessage(Handle, WM_SETREDRAW, 1, 0);
    Invalidate();
    RedrawWindow(Handle, NULL, 0, RDW_INVALIDATE|RDW_ALLCHILDREN);
  }
  DMPCATCHTHROW("TMainFormEW::ChangeForm")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::EnableMenuItems(TForm *NewForm)
{
  DMPTRY {
    Save1           ->Enabled = NewForm == RecordForm;
    SaveAs1         ->Enabled = NewForm == RecordForm;
    PrintCxsFile    ->Enabled = NewForm == RecordForm || NewForm == ResultForm; // CR 14584 06/01/2006 DJK 
    PrintSetupReport->Enabled = NewForm == RecordForm;
    Setup1          ->Enabled = NewForm != RecordForm;
    Run1            ->Enabled = true;  //CR20894 MJH 5-1-07: Always enabled, since when new ExtractRun is open, menu isn't avail
  }
  DMPCATCHTHROW("TMainFormEW::EnableMenuItems")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::LoadSetup(String FileName)
{
  DMPTRY {

    RecordForm->PostDLLPathFileNameEdit->Enabled          = false; //CR10943 03/30/2005 ALH
    RecordForm->DLLBrowsePathFileNameButton->Enabled      = false; //CR10943 03/30/2005 ALH
    RecordForm->PostDLLPathFileNameEdit->Text             = "";    //CR10943 03/30/2005 ALH
    RecordForm->PostDLLLabel->Enabled                     = false; //CR10943 03/30/2005 ALH
    RecordForm->Open(FileName);
    ChangeForm(RecordForm);
  }
  DMPCATCHTHROW("TMainFormEW::LoadSetup")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::LoadResults(String ResultFile, String SetupFile)
{
  DMPTRY {
    ResultForm->LoadFile("Results - ", ResultFile, SetupFile);
    ChangeForm(ResultForm);
  }
  DMPCATCHTHROW("TMainFormEW::LoadResults")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::OnFileSave(TMessage Msg)
{
  DMPTRY {
    SaveAs1Click(NULL);
  }
  DMPCATCHTHROW("TMainFormEW::OnFileSave")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::OnExtractBegun(TMessage Msg)
{
  DMPTRY {
    New1->Enabled = false;
    Open1->Enabled = false;
    View1->Enabled = false;
  }
  DMPCATCHTHROW("TMainFormEW::OnExtractBegun")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::OnExtractComplete(TMessage Msg)
{
  DMPTRY {
/*    String FileName = RunForm->GetResultFile();
    String Setup = RunForm->GetSetupFile();
    LoadResults(FileName, Setup);   */
    New1->Enabled = true;
    Open1->Enabled = true;
    View1->Enabled = true;
  }
  DMPCATCHTHROW("TMainFormEW::OnExtractComplete")
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::DoError(String Error)
{
  DMPTRY {
    MessageDlg(Error, mtError, TMsgDlgButtons()<<mbOK, 0);
  }
  DMPCATCHTHROW("TMainFormEW::DoError")
}

//---------------------------------------------------------------------------
bool __fastcall TMainFormEW::OpenDatabase()
{
  bool bResult = false;
  DMPTRY {
    TIniHandler Ini;
    Ini.Execute();
    WKID = Ini.WorkstationID;
    bResult = DataMod->OpenConnection(Ini.Server, Ini.ADODB, Ini.DBUserName, Ini.DBPassword, Ini.ADOCommandTimeout);
  }
  DMPCATCHTHROW("TMainFormEW::OpenDatabase")
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall TMainFormEW::GetSetupPath()
{
  bool bResult = false;
  DMPTRY {
    TIniHandler Ini;
    Ini.Execute();
    Ini.GetIniFileString("Mapper.ini", "Wizard", "SetupPath", "", SetupPath);
    OpenDialog->InitialDir = SetupPath;
    SaveDialog->InitialDir = SetupPath;
    bResult = !SetupPath.IsEmpty();
  }
  DMPCATCHTHROW("TMainFormEW::GetSetupPath")
  return bResult;
}

//---------------------------------------------------------------------------
void __fastcall TMainFormEW::OpenDialogCanClose(TObject *Sender, bool &CanClose)
{
  DMPTRY {
    String CompPath = ExtractFilePath(OpenDialog->FileName);
    CanClose = CompPath.UpperCase() == SetupPath.UpperCase();
    if (!CanClose)
      ShowMessage("Only extracts in the directory " + SetupPath + " may be opened.");
  }
  DMPCATCHTHROW("TMainFormEW::OpenDialogCanClose")
}
//---------------------------------------------------------------------------

void __fastcall TMainFormEW::SaveDialogCanClose(TObject *Sender, bool &CanClose)
{
  DMPTRY {
    String CompPath = ExtractFilePath(SaveDialog->FileName);
    String CompExt = ExtractFileExt(SaveDialog->FileName);
    bool PathOK = CompPath.UpperCase() == SetupPath.UpperCase();
    bool ExtOK = CompExt.IsEmpty() || CompExt == ".cxs";
    CanClose = PathOK && ExtOK;
    if (!PathOK)
      ShowMessage("Extracts must be saved in the directory " + SetupPath + ".");
    else
      if (!ExtOK)
        ShowMessage("Extracts must be saved with the extension '.cxs'");
  }
  DMPCATCHTHROW("TMainFormEW::SaveDialogCanClose")
}

//---------------------------------------------------------------------------


