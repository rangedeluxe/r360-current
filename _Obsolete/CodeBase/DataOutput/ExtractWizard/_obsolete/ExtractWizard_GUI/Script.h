//---------------------------------------------------------------------------

#ifndef ScriptH
#define ScriptH
//---------------------------------------------------------------------------

#include "HelperClasses.h"

class TRecHelper
{
public:
  static int GenerateNumber     (int iRecordType, int iRecordSubType, int iHeaderTrailerItemIndex, int iRepeatLimit=0, bool bIgnoreOccurs=false);
  static int GetRecordType      (int iValue);
  static int GetRecordSubType   (int iValue); // This is like 'GetRecordType', except that if the type is 'qrtDetail', then the returned type is the 'subtype'
  static int GetHeaderTrailer   (int iValue);
  static int GetRepeatLimitValue(int iValue);
  static int SetRecordDetail    (int iValue, bool bDetail     );
  static int SetRecordSubType   (int iValue, int  iSubType    );
  static int SetHeaderTrailer   (int iValue, int  iItemIndex  );
  static int SetRepeatLimitValue(int iValue, int  iRepeatLimit);
  // CR 8752 7/16/04 MLT - added new static methods for getting/setting IgnoreOccurs flag
  static bool GetIgnoreOccursFlag(int iValue);
  static int SetRecordIgnoreOccurs(int iValue, bool bIgnoreOccurs     );
};

//---------------------------------------------------------------------------

class TRecordFieldItem
{
private:
  int        m_iIniTag; // 'Row1', 'Row2', etc.
  String     m_sFieldName;
  TFieldType m_ftDataType;
  String     m_sDisplayName;
  int        m_iColWidth;
  char       m_cPadChar;
  bool       m_bRightJustify;
  bool       m_bUseQuotes;
  String     m_sFormat;
  int        m_iOccursGroup; // 0:Checks 1:Stubs 2:Docs
  int        m_iOccursCount;

  bool       m_bOccursColumns;

  String     m_sTable; // These are derivative of 'm_sFieldName'.
  String     m_sField; //
public:
  TRecordFieldItem() {Clear();}
  TRecordFieldItem(const TRecordFieldItem& obj) {*this = obj;}
  TRecordFieldItem(const String& sIniRow);
  TRecordFieldItem& operator  = (const TRecordFieldItem& obj);
  bool              operator == (const TRecordFieldItem& obj) const {return (m_iIniTag == obj.m_iIniTag);}
  bool              operator  < (const TRecordFieldItem& obj) const {return (m_iIniTag  < obj.m_iIniTag);}
  String     GetFieldName    () const {return m_sFieldName    ;}
  TFieldType GetDataType     () const {return m_ftDataType    ;}
  String     GetDisplayName  () const {return m_sDisplayName  ;}
  int        GetColWidth     () const {return m_iColWidth     ;}
  char       GetPadChar      () const {return m_cPadChar      ;}
  bool       GetRightJustify () const {return m_bRightJustify ;}
  bool       GetUseQuotes    () const {return m_bUseQuotes    ;}
  String     GetFormat       () const {return m_sFormat       ;}
  int        GetOccursGroup  () const {return m_iOccursGroup  ;}
  int        GetOccursCount  () const {return m_iOccursCount  ;}
  bool       HasOccursColumns() const {return m_bOccursColumns;}
  String     GetTable        () const {return m_sTable        ;}
private:
  void Clear();
};

class TScript;

class TRecordItem
{
friend TScript;
private:
  String m_sName;
  int    m_iType;
  std::vector<TRecordFieldItem> m_vFields;
public:
  TRecordItem() {m_iType = -1;}
  TRecordItem(const TRecordItem& obj) {*this = obj;}
  TRecordItem(const String& sName, const int iType) {m_sName = sName; m_iType = iType;}
  TRecordItem& operator  = (const TRecordItem& obj) {m_sName = obj.m_sName; m_iType = obj.m_iType; m_vFields = obj.m_vFields; return *this;}
  bool         operator == (const TRecordItem& obj) const {return (m_sName.AnsiCompareIC(obj.m_sName) == 0);}
  bool         operator  < (const TRecordItem& obj) const {return (m_sName.AnsiCompareIC(obj.m_sName)  < 0);}
  String GetName() const {return m_sName;}
  int    GetType() const {return m_iType;}
  int    GetFieldCount() const {return (int) m_vFields.size();}
  const TRecordFieldItem& GetField(int iIdx) const;
  void LoadIniData(const String& sIniFile);
private:
  void CheckIdx(const int iIdx, const int iCount) const;
};

class TScript
{
private:
  String FFileName;
  TStringList* m_slParameters;
  std::vector<TRecordItem> m_vRecords, m_vFastSearchTool;
  TRecordItem m_dumyRecord;
  String m_sBatchTraceField;
  bool   m_bDisplayDataOnly;
  String m_sExtractPath;
  String m_sLogFilePath;
  String m_sFieldDelimiter;
  int    m_iHTMLBorderWidth;
  int    m_iHTMLCellSpacing;
  String m_sHTMLFooterFile;
  String m_sHTMLHeaderFile;
  String m_sImagePath;
  String  m_sPostProcDLL;                  // CR 10943 03/30/2005 ALH
  //bool   m_bImageFileFormatAsPDF;        // CR 5815 03/03/2004 DJK
  int    m_iImageFileFormat;               // CR 9916 10/13/2004 ALH - chane to an int instead of bool.
  bool   m_bCombineImagesAccrossProcDates; // CR 5815 03/03/2004 DJK
  String m_sImageFileNameSingle;           // CR 5815 03/03/2004 DJK
  String m_sImageFileNamePerTran;          // CR 5815 03/03/2004 DJK
  String m_sImageFileNamePerBatch;         // CR 5815 03/03/2004 DJK
  String m_sImageFileProcDateFormat;       // CR 5815 03/03/2004 DJK
  bool   m_bImageFileZeroPad;              // CR 5815 03/03/2004 DJK
  bool   m_bImageFileBatchTypeFormatFull;  // CR 5815 03/03/2004 DJK
  String m_sRecordDelimiter;
  bool   m_bReformatHTML;
  bool   m_bUseFieldNames;
  bool   m_bUseHTML;
  bool   m_bUseQuotes;
  bool   m_bNoPad;
  bool   m_bNullAsSpace;
  bool   m_bEmbedImages;

  bool __fastcall HasEntries(int Type, const String& Section);
public:
  TScript(const String& SetupFile);
  ~TScript();
  String __fastcall GetLimits(int Type);
  void __fastcall GetOrder(int Type, FieldCollection& fieldCollection, TStringList* slForAscDesc); // CR 12120 3/30/2005 DJK - Added two last parameters, return void instead of String.
  int __fastcall GetRecordCount() const {return (int) m_vRecords.size();}
  const TRecordItem& __fastcall GetRecord(int iIdx) const;
  int __fastcall LocateRecord(const String& sRecordName) const;
  int __fastcall GetMaxType();
  void __fastcall GetSelectedFieldNames(int Type, FieldCollection& Fields);
  String __fastcall GetTable(int Type, bool bIgnoreDataEntry = true);
private:
  void __fastcall CheckIdx(const int iIdx, const int iCount) const;
public:
  __property String       FileName                      = {read=FFileName                       };
  __property TStringList* Parameters                    = {read=m_slParameters                  };
  __property String       BatchTraceField               = {read=m_sBatchTraceField              };
  __property bool         DisplayDataOnly               = {read=m_bDisplayDataOnly              };
  __property String       ExtractPath                   = {read=m_sExtractPath                  };
  __property String       LogFilePath                   = {read=m_sLogFilePath                  };
  __property String       FieldDelimiter                = {read=m_sFieldDelimiter               };
  __property int          HTMLBorderWidth               = {read=m_iHTMLBorderWidth              };
  __property int          HTMLCellSpacing               = {read=m_iHTMLCellSpacing              };
  __property String       HTMLFooterFile                = {read=m_sHTMLFooterFile               };
  __property String       HTMLHeaderFile                = {read=m_sHTMLHeaderFile               };
  __property String       ImagePath                     = {read=m_sImagePath                    };
  __property int          ImageFileFormatAsPDF          = {read=m_iImageFileFormat              };  // CR 9916 10/13/2004 ALH - chane to an int instead of bool.
  __property bool         CombineImagesAccrossProcDates = {read=m_bCombineImagesAccrossProcDates};
  __property String       ImageFileNameSingle           = {read=m_sImageFileNameSingle          };
  __property String       ImageFileNamePerTran          = {read=m_sImageFileNamePerTran         };
  __property String       ImageFileNamePerBatch         = {read=m_sImageFileNamePerBatch        };
  __property String       ImageFileProcDateFormat       = {read=m_sImageFileProcDateFormat      };
  __property bool         ImageFileZeroPad              = {read=m_bImageFileZeroPad             };
  __property bool         ImageFileBatchTypeFormatFull  = {read=m_bImageFileBatchTypeFormatFull };
  __property String       RecordDelimiter               = {read=m_sRecordDelimiter              };
  __property bool         ReformatHTML                  = {read=m_bReformatHTML                 };
  __property bool         UseFieldNames                 = {read=m_bUseFieldNames                };
  __property bool         UseHTML                       = {read=m_bUseHTML                      };
  __property bool         UseQuotes                     = {read=m_bUseQuotes                    };
  __property bool         NoPad                         = {read=m_bNoPad                        };
  __property bool         NullAsSpace                   = {read=m_bNullAsSpace                  };
  __property bool         EmbedImages                   = {read=m_bEmbedImages                  };
  __property String       sPostProcessingDLL            = {read=m_sPostProcDLL                  };
};


#endif
