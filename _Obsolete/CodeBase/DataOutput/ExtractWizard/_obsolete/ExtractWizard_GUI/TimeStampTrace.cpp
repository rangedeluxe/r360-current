//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TimeStampTrace.h"
#include "DMPExceptions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

TTimeStampTrace::TTimeStampTrace()
{
  DMPTRY {
    FRerunList = "";
    FFieldName = "";
    FBatchList = "";
    FRunStatus = 0;
    FCommand = NULL;
  }
  DMPCATCHTHROW("TTimeStampTrace::TTimeStampTrace (1)")
}

TTimeStampTrace::TTimeStampTrace(String Field, String Runs, TDMPADOCommand *ActionCommand)
{
  DMPTRY {
    TTimeStampTrace();
    FFieldName = Field;
    SetRerunList(Runs);
    FCommand = ActionCommand;
    if (FFieldName.IsEmpty())
      FRunStatus = rsTest;
    else {
      if (FRerunList.IsEmpty())
        FRunStatus = rsFirstRun;
      else
        FRunStatus = rsRerun;
    }
  }
  DMPCATCHTHROW("TTimeStampTrace::TTimeStampTrace (2)")
}

void TTimeStampTrace::SetCommand(TDMPADOCommand* ActionCommand)
{
  DMPTRY {
    FCommand = ActionCommand;
  }
  DMPCATCHTHROW("TTimeStampTrace::SetCommand")
}

void TTimeStampTrace::AddBatch(int GlobalBatchID)
{
  DMPTRY {
    if (!FBatchList.IsEmpty())
      FBatchList += ", ";
    FBatchList += IntToStr(GlobalBatchID);
  }
  DMPCATCHTHROW("TTimeStampTrace::AddBatch")
}

void TTimeStampTrace::SetRerunList(String List)
{
  DMPTRY {
    FRerunList = List;
  }
  DMPCATCHTHROW("TTimeStampTrace::SetRerunList")
}

String TTimeStampTrace::GetWhereClause()
{
  String sResult;
  DMPTRY {
    switch (FRunStatus){
      case rsTest: sResult = ""; break;
      case rsFirstRun: sResult = "Batch.GlobalBatchID in (Select GlobalBatchID from BatchTrace where " +
                  FFieldName + " is NULL)"; break;
      case rsRerun: sResult = "Batch.GlobalBatchID in (Select GlobalBatchID from BatchTrace where " +
                  FFieldName + " in (" + FRerunList + "))"; break;
    }
  }
  DMPCATCHTHROW("TTimeStampTrace::GetWhereClause")
  return sResult;
}

bool TTimeStampTrace::Update()
{
  bool bResult = true;
  DMPTRY {
    if (FRunStatus == rsFirstRun)
      if (!FBatchList.IsEmpty()) {
        FCommand->CommandText = "Update BatchTrace set " + FFieldName + " = " + QuotedStr(Now().DateTimeString()) + " where GlobalBatchID in (" + FBatchList + ")";
        FCommand->Execute();
        FCommand->CommandText = "";
      }
  }
  DMPCATCHTHROW("TTimeStampTrace::Update")
  return bResult;
}


