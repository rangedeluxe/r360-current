//---------------------------------------------------------------------------
#ifndef ResultsH
#define ResultsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TResultForm : public TForm
{
__published:	// IDE-managed Components
        TPanel *Panel1;
        TButton *CloseBtn;
        TRichEdit *ResultsEdit;
        TPanel *Panel2;
        TCheckBox *WordWrapCB;
        void __fastcall WordWrapCBClick(TObject *Sender);
private:	// User declarations
        String SetupFile;
public:		// User declarations
        __fastcall TResultForm(TComponent* Owner);
        String __fastcall GetSetupFile();
        void __fastcall LoadFile(String FormCaption, String DisplayFile, String Setup);
        void __fastcall Print();
};
//---------------------------------------------------------------------------
extern PACKAGE TResultForm *ResultForm;
//---------------------------------------------------------------------------
#endif
