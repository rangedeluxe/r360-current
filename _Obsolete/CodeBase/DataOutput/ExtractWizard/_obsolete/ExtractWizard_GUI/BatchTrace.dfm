object BatchTraceDlg: TBatchTraceDlg
  Left = 372
  Top = 78
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Batch Trace Info'
  ClientHeight = 370
  ClientWidth = 526
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object FieldLbl: TLabel
    Left = 96
    Top = 17
    Width = 45
    Height = 13
    Caption = 'FieldLbl'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object Label1: TLabel
    Left = 19
    Top = 17
    Width = 64
    Height = 13
    Caption = 'Batch Info for'
  end
  object OKBtn: TButton
    Left = 208
    Top = 336
    Width = 75
    Height = 25
    Caption = 'Close'
    Default = True
    ModalResult = 1
    TabOrder = 0
  end
  object InfoGrid: TDBGrid
    Left = 19
    Top = 41
    Width = 485
    Height = 280
    DataSource = BatchDS
    Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgConfirmDelete, dgCancelOnExit]
    ReadOnly = True
    TabOrder = 1
    TitleFont.Charset = DEFAULT_CHARSET
    TitleFont.Color = clWindowText
    TitleFont.Height = -11
    TitleFont.Name = 'MS Sans Serif'
    TitleFont.Style = []
  end
  object TimeQuery: TDMPADODataSet
    Parameters = <>
    Left = 8
    Top = 328
  end
  object TimeDS: TDataSource
    DataSet = TimeQuery
    Left = 40
    Top = 328
  end
  object BatchDS: TDataSource
    DataSet = BatchQuery
    Left = 128
    Top = 328
  end
  object BatchQuery: TDMPADODataSet
    Parameters = <>
    Left = 88
    Top = 328
    object BatchQueryBankID: TIntegerField
      FieldName = 'BankID'
    end
    object BatchQueryLockboxID: TIntegerField
      FieldName = 'LockboxID'
    end
    object BatchQueryBatchID: TIntegerField
      FieldName = 'BatchID'
    end
    object BatchQueryProcessingDate: TDateTimeField
      DisplayWidth = 10
      FieldName = 'ProcessingDate'
    end
    object BatchQueryCheckCount: TIntegerField
      FieldName = 'CheckCount'
    end
    object BatchQueryCheckAmount: TBCDField
      FieldName = 'CheckAmount'
      DisplayFormat = '0.00'
      currency = True
      Precision = 19
    end
  end
end
