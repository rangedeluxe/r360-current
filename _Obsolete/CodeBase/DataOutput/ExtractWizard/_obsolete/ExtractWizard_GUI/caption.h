//---------------------------------------------------------------------------
#ifndef captionH
#define captionH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <Graphics.hpp>

//---------------------------------------------------------------------------
class TCaptionForm : public TForm
{
__published:	// IDE-managed Components
        TImage *Image1;
        TLabel *Label1;
        void __fastcall FormResize(TObject *Sender);
private:	// User declarations
        bool CustomImage;
public:		// User declarations
        __fastcall TCaptionForm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TCaptionForm *CaptionForm;
//---------------------------------------------------------------------------
#endif
