object DMPDataMod: TDMPDataMod
  OldCreateOrder = False
  OnDestroy = DataModuleDestroy
  Left = 576
  Top = 210
  Height = 479
  Width = 741
  object DMPConnection: TADOConnection
    LoginPrompt = False
    Provider = 'SQLOLEDB.1'
    Left = 48
    Top = 16
  end
  object BankDataSet: TDMPADODataSet
    Connection = DMPConnection
    AfterScroll = BankDataSetAfterScroll
    CommandText = 'Select BankID from Bank'
    Parameters = <>
    Left = 16
    Top = 72
  end
  object CustomerDataSet: TDMPADODataSet
    Connection = DMPConnection
    AfterScroll = CustomerDataSetAfterScroll
    CommandText = 
      'Select BankID, CustomerID from Customer'#13#10'where BankID = :BankID'#13 +
      #10
    DataSource = BankDS
    Parameters = <
      item
        Name = 'BankID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 95
    Top = 74
  end
  object LockboxDataSet: TDMPADODataSet
    Connection = DMPConnection
    AfterScroll = LockboxDataSetAfterScroll
    CommandText = 
      'Select Lockbox.CustomerID, Lockbox.BankID, Lockbox.LockboxID, (S' +
      'elect Count(*) From Checks) '#13#10'from Lockbox  '#13#10'where  (BankID = :' +
      'BankID and CustomerID = :CustomerID)  and (LockboxID > 0) '
    DataSource = CustomerDS
    Parameters = <
      item
        Name = 'BankID'
        DataType = ftInteger
        Size = -1
        Value = 0
      end
      item
        Name = 'CustomerID'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    Left = 176
    Top = 72
  end
  object BatchDataSet: TDMPADODataSet
    Connection = DMPConnection
    AfterScroll = BatchDataSetAfterScroll
    CommandText = 
      'Select GlobalBatchID from Batch'#13#10'where BankID = :BankID'#13#10'and Loc' +
      'kboxID = :LockboxID'#13#10
    DataSource = LockboxDS
    Parameters = <
      item
        Name = 'BankID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end
      item
        Name = 'LockboxID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 257
    Top = 72
  end
  object TranDataSet: TDMPADODataSet
    Connection = DMPConnection
    AfterScroll = TranDataSetAfterScroll
    CommandText = 
      'Select TransactionID from Transactions'#13#10'where GlobalBatchID = :G' +
      'lobalBatchID'#13#10
    DataSource = BatchDS
    Parameters = <
      item
        Name = 'GlobalBatchID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 24
    Top = 191
  end
  object ChecksDataSet: TDMPADODataSet
    Connection = DMPConnection
    CommandText = 
      'Select * from Checks, ChecksDataEntry'#13#10'where Checks.GlobalBatchI' +
      'D = :GlobalBatchID'#13#10
    Parameters = <
      item
        Name = 'GlobalBatchID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 97
    Top = 182
  end
  object StubsDataSet: TDMPADODataSet
    Connection = DMPConnection
    CommandText = 
      'Select * from Stubs, StubsDataEntry'#13#10'where Stubs.GlobalBatchID =' +
      ' :GlobalBatchID'#13#10#13#10
    Parameters = <
      item
        Name = 'GlobalBatchID'
        Attributes = [paSigned]
        DataType = ftInteger
        Precision = 10
        Size = 4
        Value = 0
      end>
    Left = 184
    Top = 191
  end
  object DocsDataSet: TDMPADODataSet
    Connection = DMPConnection
    CommandText = 
      'Select * from Documents, DocumentsDataEntry'#13#10'where GlobalBatchID' +
      ' = :GlobalBatchID'#13#10#13#10
    Parameters = <
      item
        Name = 'GlobalBatchID'
        DataType = ftInteger
        Size = -1
        Value = 0
      end>
    Left = 271
    Top = 184
  end
  object BankDS: TDataSource
    DataSet = BankDataSet
    Left = 16
    Top = 120
  end
  object CustomerDS: TDataSource
    DataSet = CustomerDataSet
    Left = 96
    Top = 128
  end
  object LockboxDS: TDataSource
    DataSet = LockboxDataSet
    Left = 176
    Top = 120
  end
  object BatchDS: TDataSource
    DataSet = BatchDataSet
    Left = 256
    Top = 120
  end
  object TranDS: TDataSource
    DataSet = TranDataSet
    Left = 24
    Top = 256
  end
  object ChecksDS: TDataSource
    DataSet = ChecksDataSet
    Left = 104
    Top = 248
  end
  object StubsDS: TDataSource
    DataSet = StubsDataSet
    Left = 184
    Top = 240
  end
  object DocsDS: TDataSource
    DataSet = DocsDataSet
    Left = 264
    Top = 240
  end
  object ActionCmd: TDMPADOCommand
    Connection = DMPConnection
    Parameters = <>
    Left = 135
    Top = 16
  end
  object ActionDataSet: TDMPADODataSet
    Connection = DMPConnection
    Parameters = <>
    Left = 217
    Top = 16
  end
end
