//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ImageFileNamingClasses.h"
#include "DMPExceptions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)



void __fastcall TCaretBlinkTimer::Execute()
{

  m_iLastTickCount = ::GetTickCount();
  while (!Terminated && m_sError.IsEmpty()) {
    ::Sleep(50);
    m_iTickCount = ::GetTickCount();
    Synchronize(CallCaretBlinkTimerEvent);
    m_iLastTickCount = m_iTickCount;
  }
  // djkzzz  - Somehow get the contents of 'm_sError' to exception handler!
}

void __fastcall TCaretBlinkTimer::CallCaretBlinkTimerEvent()
{
  m_sError = m_caretBlinkTimerEvent(m_iTickCount-m_iLastTickCount);
}

//---------------------------------------------------------------------------

TCaretManager::TCaretManager(TTokenEdit* tokenEditPtr, TColor clBackgroundColor)
{
  DMPTRY {
    m_tokenEditPtr      = tokenEditPtr;
    m_clBackgroundColor = clBackgroundColor;
    m_iTicsCounter      = 0;
    m_iBlinkPeriod      = ::GetCaretBlinkTime();
    m_bCaretOn          = false;
    m_bHasFocus         = false;
    m_iCaretPos         = m_tokenEditPtr->GetMinDrawX();
    m_dmpExBlinker      = new DMPException();
    m_threadBlinkTimer  = new TCaretBlinkTimer(&ReactToBlinkTimerEvent);
    m_threadBlinkTimer->Resume();
  }
  DMPCATCHTHROW("TCaretManager::TCaretManager")
}

TCaretManager::~TCaretManager()
{
  DMPTRY {
    if (m_threadBlinkTimer) {
      m_threadBlinkTimer->Terminate();
      m_threadBlinkTimer->WaitFor();
      delete m_threadBlinkTimer;
    }
    delete m_dmpExBlinker;
  }
  DMPCATCHTHROW("TCaretManager::~TCaretManager")
}

void TCaretManager::GotFocus(bool bFocus)
{
  DMPTRY {
    m_bHasFocus = bFocus;
    if (m_bHasFocus)
      ResetBlink();
    else
      EraseCaret();
  }
  DMPCATCHTHROW("TCaretManager::GotFocus")
}

void TCaretManager::SetPosition(int iCaretPos)
{
  DMPTRY {
    EraseCaret();
    m_iCaretPos = iCaretPos;
    ResetBlink();
  }
  DMPCATCHTHROW("TCaretManager::SetPosition")
}

String __fastcall TCaretManager::ReactToBlinkTimerEvent(int iTicsSinceLastCall)
{
  String sResult;
  DMPTRY {
    m_iTicsCounter += iTicsSinceLastCall;
    if (((m_iTicsCounter/m_iBlinkPeriod) % 2) == 0) {
      if (!m_bCaretOn) {
        DrawCaret(true);
        m_bCaretOn = true;
      }
    }
    else
      if (m_bCaretOn) {
        DrawCaret(false);
        m_bCaretOn = false;
      }
  }
  DMPCATCH_GET("TCaretManager::ReactToBlinkTimerEvent", *m_dmpExBlinker)
  if (!m_dmpExBlinker->Empty)
    sResult = m_dmpExBlinker->Message;
  return sResult;
}

void TCaretManager::DrawCaret(bool bOn)
{
  DMPTRY {
    if (m_tokenEditPtr->Showing && m_bHasFocus) {
      TCanvas* canvas = m_tokenEditPtr->Canvas;
      TColor color = canvas->Pen->Color;
      canvas->Pen->Color = bOn ? clWindowText : m_clBackgroundColor;
      canvas->MoveTo(m_iCaretPos - m_tokenEditPtr->GetOffsetDrawX(), 3);
      canvas->LineTo(m_iCaretPos - m_tokenEditPtr->GetOffsetDrawX(), m_tokenEditPtr->Height-5);
      canvas->Pen->Color = color;
    }
  }
  DMPCATCHTHROW("TCaretManager::DrawCaret")
}

void TCaretManager::EraseCaret()
{
  DMPTRY {
    if (m_tokenEditPtr->Showing) {
      TCanvas* canvas = m_tokenEditPtr->Canvas;
      TColor color = canvas->Pen->Color;
      canvas->Pen->Color = m_clBackgroundColor;
      canvas->MoveTo(m_iCaretPos - m_tokenEditPtr->GetOffsetDrawX(), 3);
      canvas->LineTo(m_iCaretPos - m_tokenEditPtr->GetOffsetDrawX(), m_tokenEditPtr->Height-5);
      canvas->Pen->Color = color;
    }
  }
  DMPCATCHTHROW("TCaretManager::EraseCaret")
}

void TCaretManager::ResetBlink()
{
  DMPTRY {
    m_iTicsCounter = 0;
    m_bCaretOn = false;
  }
  DMPCATCHTHROW("TCaretManager::ResetBlink")
}

//---------------------------------------------------------------------------

bool EditToken::m_bSuspendIllegalCharacterCheck = false;

EditToken::EditToken(TTokenEdit* tokenEditPtr, const String& sTextString)
{
  DMPTRY {
    m_tokenEditPtr = tokenEditPtr;
    m_editTokenType = ettText;
    m_sTextString = sTextString;
    if (sTextString.Trim().Length() == 0)
      throw DMPException("Input string is empty");
    else {
      if (!m_bSuspendIllegalCharacterCheck)
        for (int i = 0; i < sTextString.Length(); ++i)
          if (!IsLegalFileNameCharacter(sTextString[i+1]))
            throw DMPException("Input string '"+sTextString+"' contains one or more illegal characters");
      m_bSuspendIllegalCharacterCheck = false;
    }
  }
  DMPCATCHTHROW("EditToken::EditToken")
}

int EditToken::Draw(TCanvas* canvas, const TRect& rect) const
{
  int iResult = 0;
  DMPTRY {
    PrepareCanvas(canvas);

    TRect myRect = rect;
    myRect.Left = myRect.Left - m_tokenEditPtr->GetOffsetDrawX();
    int iX = myRect.Left;
    if (myRect.Left < m_tokenEditPtr->GetMinDrawX())
      myRect.Left = m_tokenEditPtr->GetMinDrawX();

    canvas->TextRect(myRect, iX, rect.Top, GetText());
    iResult = canvas->TextWidth(GetText());
  }
  DMPCATCHTHROW("EditToken::Draw")
  return iResult;
}

int EditToken::GetWidth(TCanvas* canvas) const
{
  int iResult = 0;
  DMPTRY {
    PrepareCanvas(canvas);
    iResult = canvas->TextWidth(GetText());
  }
  DMPCATCHTHROW("EditToken::GetWidth")
  return iResult;
}

int EditToken::GetWidth(TCanvas* canvas, const int iCharIdx) const
{
  int iResult = 0;
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    PrepareCanvas(canvas);
    int iIdx = iCharIdx;
    if (iIdx > m_sTextString.Length())
      iIdx = m_sTextString.Length();
    iResult = canvas->TextWidth(m_sTextString.SubString(1, iIdx));
  }
  DMPCATCHTHROW("EditToken::GetWidth (int)")
  return iResult;
}

int EditToken::GetCharIndex(TCanvas* canvas, const int iPixelsIn) const
{
  int iResult = 0;
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    PrepareCanvas(canvas);
    if (m_sTextString.Length()) {
      iResult = m_sTextString.Length();
      int iPosLast = 0, iPos = 0;
      for (int i = 0; i < m_sTextString.Length(); ++i) {
        iPos += canvas->TextWidth(m_sTextString.SubString(i+1, 1));
        if (iPos >= iPixelsIn) {
          if ((iPixelsIn-iPosLast) < (iPos-iPixelsIn))
            iResult = i;
          else
            iResult = i+1;
          break;
        }
        iPosLast = iPos;
      }
    }
  }
  DMPCATCHTHROW("EditToken::GetCharIndex")
  return iResult;
}

void EditToken::InsertString(const String& sText, const int iInsertionPoint)
{
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    if (iInsertionPoint >= 0 && iInsertionPoint <= m_sTextString.Length())
      m_sTextString.Insert(sText, iInsertionPoint+1);
    else
      throw DMPException("The supplied insertion point '"+String(iInsertionPoint)+"' is outside of the allowable bounds [0, "+String(m_sTextString.Length())+"] for the string '"+m_sTextString+"'");
  }
  DMPCATCHTHROW("EditToken::InsertChar")
}

void EditToken::DeleteChar(const int iDeletionPoint, bool bDontAllowToGoEmpty)
{
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    if (iDeletionPoint >= m_sTextString.Length())
      throw DMPException("The supplied deletion point '"+String(iDeletionPoint)+"' cannot be equal to or greater than the length of the string '"+m_sTextString+"'");
    if (m_sTextString.Length() == 1 && bDontAllowToGoEmpty)
      m_sTextString = "_"; // Can never let two adjascent non-text tokens touch.
    else
      m_sTextString.Delete(iDeletionPoint+1, 1);
  }
  DMPCATCHTHROW("EditToken::DeleteChar")
}

void EditToken::BackspaceChar(const int iBackspacePoint, bool bDontAllowToGoEmpty)
{
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    if (iBackspacePoint <= 0)
      throw DMPException("The supplied deletion point '"+String(iBackspacePoint)+"' cannot be less than or equal to '0'");
    if (m_sTextString.Length() == 1 && bDontAllowToGoEmpty)
      m_sTextString = "_"; // Can never let two adjascent non-text tokens touch.
    else
      m_sTextString.Delete(iBackspacePoint, 1);
  }
  DMPCATCHTHROW("EditToken::BackspaceChar")
}

void EditToken::Merge(const EditToken& obj)
{
  DMPTRY {
    if (m_editTokenType != ettText || obj.m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText', as well as the object referenced in the parameter");
    m_sTextString += obj.m_sTextString;
  }
  DMPCATCHTHROW("EditToken::Merge")
}

EditToken EditToken::Divide(const int iDivisionPoint)
{
  EditToken etResult;
  DMPTRY {
    if (m_editTokenType != ettText)
      throw DMPException("This method can only be called when 'm_editTokenType' is 'ittText'");
    if (iDivisionPoint <= 0 || iDivisionPoint >= m_sTextString.Length())
      throw DMPException("The supplied parameter '"+String(iDivisionPoint)+"' is outside of the allowable range [1, "+String(m_sTextString.Length()-1)+"] for the string '"+m_sTextString+"'");
    etResult = EditToken(m_tokenEditPtr, m_sTextString.SubString(iDivisionPoint+1, m_sTextString.Length()-iDivisionPoint));
    m_sTextString = m_sTextString.SubString(1, iDivisionPoint);
  }
  DMPCATCHTHROW("EditToken::Divide")
  return etResult;
}

String EditToken::EncodeToString() const
{
  String sResult;
  DMPTRY {
    switch (m_editTokenType) {
      case ettNone        : sResult = "<NN>"; break;
      case ettText        : sResult = m_sTextString; break;
      case ettBank        : sResult = "<BK>"; break;
      case ettLockbox     : sResult = "<LB>"; break;
      case ettBatch       : sResult = "<BH>"; break;
      case ettBatchType   : sResult = "<BT>"; break;
      case ettProcDate    : sResult = "<PD>"; break;
      case ettTransaction : sResult = "<TR>"; break;
      case ettDocumentType: sResult = "<DT>"; break;
      case ettFileCounter : sResult = "<FC>"; break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("EditToken::EncodeToString")
  return sResult;
}

// CR 14584 06/01/2006 DJK
String EditToken::AsDisplayed() const
{
  String sResult;
  DMPTRY {
    sResult = GetText();
  }
  DMPCATCHTHROW("EditToken::AsDisplayed")
  return sResult;
}

EditToken EditToken::DecodeString(TTokenEdit* tokenEditPtr, String& sEncodedString)
{
  EditToken etResult;
  DMPTRY {
    if (sEncodedString.Trim().Length()) {
      int iPos1 = sEncodedString.Pos("<");
      if (iPos1) {
        if (iPos1 == 1) {
          int iPos2 = sEncodedString.Pos(">");
          if (iPos2) {
            String sText = sEncodedString.SubString(iPos1+1, iPos2-iPos1-1).Trim();
            if (sText.Length()) {
              EditTokenType editTokenType = ettNone;
              if (sText == "NN")
                editTokenType = ettNone;
              else
                if (sText == "BK")
                  editTokenType = ettBank;
                else
                  if (sText == "LB")
                    editTokenType = ettLockbox;
                  else
                    if (sText == "BH")
                      editTokenType = ettBatch;
                    else
                      if (sText == "BT")
                        editTokenType = ettBatchType;
                      else
                        if (sText == "PD")
                          editTokenType = ettProcDate;
                        else
                          if (sText == "TR")
                            editTokenType = ettTransaction;
                          else
                            if (sText == "DT")
                              editTokenType = ettDocumentType;
                            else
                              if (sText == "FC")
                                editTokenType = ettFileCounter;
                              else
                                throw DMPException("Braced text at front of input string '"+sEncodedString+"' contains an undefined token symbol");
              etResult = EditToken(tokenEditPtr, editTokenType);
              sEncodedString = sEncodedString.SubString(iPos2+1, sEncodedString.Length()-iPos2).Trim();
            }
            else
              throw DMPException("Braced text at front of input string '"+sEncodedString+"' contains only blanks");
          }
          else
            throw DMPException("Input string '"+sEncodedString+"' contains mismatched '<>' braces");
        }
        else {
          String sText = sEncodedString.SubString(1, iPos1-1).Trim();
          if (sText.Length()) {
            if (sText.Pos(">"))
              throw DMPException("Input string '"+sEncodedString+"' contains mismatched '<>' braces");
            else {
              etResult = EditToken(tokenEditPtr, sText);
              sEncodedString = sEncodedString.SubString(iPos1, sEncodedString.Length()-iPos1+1).Trim();
            }
          }
          else
            throw DMPException("Front of input string '"+sEncodedString+"' contains only blanks");
        }
      }
      else
        if (sEncodedString.Pos(">"))
          throw DMPException("Input string '"+sEncodedString+"' contains mismatched '<>' braces");
        else {
          etResult = EditToken(tokenEditPtr, sEncodedString);
          sEncodedString = "";
        }
    }
    else
      throw DMPException("Input string is empty");
  }
  DMPCATCHTHROW("EditToken::DecodeString")
  return etResult;
}

bool EditToken::IsLegalFileNameCharacter(const char cChar)
{
  bool bResult = false;
  DMPTRY {
    if ((unsigned char) cChar >= (unsigned char) 32 && (unsigned char) cChar <= (unsigned char) 126) {
      bResult = true;
      switch (cChar) {
        case '\\':
        case '/' :
        case ':' :
        case '*' :
        case '?' :
        case '\"':
        case '<' :
        case '>' :
        case '|' :
        case ' ' :
        case '.' : bResult = false;
      }
    }
  }
  DMPCATCHTHROW("EditToken::IsLegalFileNameCharacter")
  return bResult;
}

String EditToken::GetText() const
{
  String sResult;
  DMPTRY {
    switch (m_editTokenType) {
      case ettText        : sResult = m_sTextString;
                            break;
      case ettBank        : sResult = "BANK";
                            break;
      case ettLockbox     : sResult = "BOX";
                            break;
      case ettBatch       : sResult = "BATCH";
                            break;
      case ettBatchType   : sResult = "BATCHTYPE["+String(m_tokenEditPtr->GetBatchStatAbbreviated() ? "1" : (m_tokenEditPtr->GetZeroPadded() ? "0F" : "F"))+"]";
                            break;
      case ettProcDate    : sResult = "PDATE["+m_tokenEditPtr->GetDateFormat()+"]";
                            break;
      case ettTransaction : sResult = "TRAN";
                            break;
      case ettDocumentType: sResult = "DOCTYPE";
                            break;
      case ettFileCounter : sResult = "COUNTER";
                            break;
      default: throw DMPException("Illegal case");
    }
    if (m_tokenEditPtr->GetZeroPadded())
      switch (m_editTokenType) {
        case ettBank       :
        case ettLockbox    :
        case ettBatch      :
        case ettTransaction:
        case ettFileCounter: sResult += "[0]"; break;
      }
  }
  DMPCATCHTHROW("EditToken::GetText")
  return sResult;
}

void EditToken::PrepareCanvas(TCanvas* canvas) const
{
  DMPTRY {
    TFontStyles fontStyles;
    if (m_editTokenType == ettText)
      canvas->Font->Color = clWindowText;
    else {
      canvas->Font->Color = clMaroon;
      fontStyles << fsBold;
    }
    canvas->Font->Style = fontStyles;
  }
  DMPCATCHTHROW("EditToken::PrepareCanvas")
}

//---------------------------------------------------------------------------

TokenIndex::TokenIndex(int iIdx, bool bFront, bool bBack, bool bIsText, int iTextPos)
{
  DMPTRY {
    if (iIdx < 0)
      throw DMPException("'iIdx' cannot be negative");
    if (bFront) {
      if (bBack)
        throw DMPException("'bFront' and 'bBack' cannot both be true at the same time");
      if (bIsText && iTextPos != 0)
        throw DMPException("'iTextPos' must be '0' when 'bFront' and 'bIsText' are true");
    }
    else
      if (bBack) {
        if (bIsText && iTextPos == 0)
          throw DMPException("'iTextPos' cannot be '0' when 'bBack' and 'bIsText' are true");
      }
      else
        if (bIsText) {
          if (iTextPos == 0)
            throw DMPException("'iTextPos' cannot be '0' when 'bIsText' is true and 'bFront' amd 'bBack' are both false");
        }
        else
          throw DMPException("'bIsText' must be true when 'bFront' and 'bBack' are both false");

    m_iIdx     = iIdx;
    m_bFront   = bFront;
    m_bBack    = bBack;
    m_bIsText  = bIsText;
    m_iTextPos = iTextPos;
  }
  DMPCATCHTHROW("TokenIndex::TokenIndex")
}

TokenIndex& TokenIndex::operator++()
{
  DMPTRY {
    if (m_bIsText) {
      if (m_bFront) {
        m_bFront = m_bBack = false;
        m_iTextPos = 1; // for the string 'abcdef', this would be caret pos 0 -> '|abcdef', this would be caret pos 1 -> 'a|bcdef', etc.
      }
      else
        if (m_bBack) {
          m_bFront =  true;
          m_bBack  = false;
          ++m_iIdx;
          m_iTextPos = 0;
        }
        else
          ++m_iTextPos;
    }
    else
      if (m_bFront) {
        m_bFront = false;
        m_bBack  =  true;
      }
      else {
        m_bFront =  true;
        m_bBack  = false;
        ++m_iIdx;
      }
  }
  DMPCATCHTHROW("TokenIndex::operator++ (prefix)")
  return *this;
}

TokenIndex TokenIndex::operator++(int)
{
  TokenIndex temp;
  DMPTRY {
    temp = *this;
    ++*this;
  }
  DMPCATCHTHROW("TokenIndex::operator++ (postfix)")
  return temp;
}

TokenIndex& TokenIndex::operator--()
{
  DMPTRY {
    if (m_bIsText) {
      if (m_bBack) {
        m_bFront = m_bBack = false;
        --m_iTextPos;
      }
      else
        if (m_bFront) {
          m_bFront = false;
          m_bBack  =  true;
          --m_iIdx;
          m_iTextPos = -1;
        }
        else
          --m_iTextPos;
    }
    else {
      if (m_bBack) {
        m_bFront =  true;
        m_bBack  = false;
      }
      else {
        m_bFront = false;
        m_bBack  =  true;
        --m_iIdx;
      }
    }
  }
  DMPCATCHTHROW("TokenIndex::operator-- (prefix)")
  return *this;
}

TokenIndex TokenIndex::operator--(int)
{
  TokenIndex temp;
  DMPTRY {
    temp = *this;
    --*this;
  }
  DMPCATCHTHROW("TokenIndex::operator-- (postfix)")
  return temp;
}

//---------------------------------------------------------------------------

TTokenManager::TTokenManager(TTokenEdit* tokenEditPtr)
{
  DMPTRY {
    m_tokenEditPtr = tokenEditPtr;
  }
  DMPCATCHTHROW("TTokenManager::TTokenManager")
}

void TTokenManager::Paint(TCanvas* canvas, const TRect& rect) const
{
  DMPTRY {
    TRect textRect = rect;
    for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end() /*&& textRect.Left < textRect.Right*/; ++itt)
      textRect.Left += itt->Draw(canvas, textRect);
  }
  DMPCATCHTHROW("TTokenManager::Paint")
}

int TTokenManager::DetermineEndPosition(TCanvas* canvas) const
{
  int iResult = m_tokenEditPtr->GetMinDrawX();
  DMPTRY {
    for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end(); ++itt)
      iResult += itt->GetWidth(canvas);
  }
  DMPCATCHTHROW("TTokenManager::DetermineEndPosition")
  return iResult;
}

int TTokenManager::DetermineMoveLeftPosition(TCanvas* canvas, const int iCurrentPos) const
{
  int iResult = 0;
  DMPTRY {
    TokenIndex tokenIndex = CaretPosToIndex(canvas, iCurrentPos, tdLeft);
    tokenIndex--;
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::DetermineMoveLeftPosition")
  return iResult;
}

int TTokenManager::DetermineMoveRightPosition(TCanvas* canvas, const int iCurrentPos) const
{
  int iResult = 0;
  DMPTRY {
    TokenIndex tokenIndex = CaretPosToIndex(canvas, iCurrentPos, tdRight);
    ++tokenIndex;
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::DetermineMoveRightPosition")
  return iResult;
}

int TTokenManager::DetermineClosestPosition(TCanvas* canvas, const int iCurrentPos) const
{
  int iResult = 0;
  DMPTRY {
    TokenIndex tokenIndex = CaretPosToIndex(canvas, iCurrentPos);
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::DetermineClosestPosition")
  return iResult;
}

int TTokenManager::InsertString(TCanvas* canvas, const int iCurrentPos, const String& sText)
{
  int iResult = 0;
  DMPTRY {
    TokenIndex tokenIndex;
    if (m_vTokens.size()) {
      bool bInsertChar = false;
      tokenIndex = CaretPosToIndex(canvas, iCurrentPos);
      if (m_vTokens[tokenIndex.GetIdx()].IsText())
        bInsertChar = true;
      else
        if (tokenIndex.GetFront()) {
          if (tokenIndex.GetIdx() == 0) {
            m_vTokens.insert(m_vTokens.begin(), EditToken(m_tokenEditPtr, sText));
            tokenIndex = TokenIndex(0, false, true, true, sText.Length());
          }
          else {
            tokenIndex = TokenIndex(tokenIndex.GetIdx()-1, false, true, true, m_vTokens[tokenIndex.GetIdx()-1].GetTextLength());
            bInsertChar = true;
          }
        }
        else
          if (tokenIndex.GetIdx() == ((int) m_vTokens.size()-1)) {
            m_vTokens.push_back(EditToken(m_tokenEditPtr, sText));
            tokenIndex = TokenIndex((int) m_vTokens.size()-1, false, true, true, sText.Length());
          }
          else {
            tokenIndex = TokenIndex(tokenIndex.GetIdx()+1, true, false, true, 0);
            bInsertChar = true;
          }
      if (bInsertChar) {
        m_vTokens[tokenIndex.GetIdx()].InsertString(sText, tokenIndex.GetTextPos());
        tokenIndex = TokenIndex(tokenIndex.GetIdx(), false, tokenIndex.GetBack(), true, tokenIndex.GetTextPos()+sText.Length());
      }
    }
    else {
      m_vTokens.push_back(EditToken(m_tokenEditPtr, sText));
      tokenIndex = TokenIndex(0, false, true, true, sText.Length());
    }
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::InsertString")
  return iResult;
}

int TTokenManager::InsertToken(TCanvas* canvas, const int iCurrentPos, const EditToken::EditTokenType editTokenType)
{
  int iResult = 0;
  DMPTRY {
    if (editTokenType == EditToken::ettNone || editTokenType == EditToken::ettText)
      throw DMPException("Neither 'ettNone' nor 'ettText' are allowed here");
    TokenIndex tokenIndex;
    if (m_vTokens.size()) {
      tokenIndex = CaretPosToIndex(canvas, iCurrentPos, tdRight);
      if (m_vTokens[tokenIndex.GetIdx()].IsText()) {
        if (tokenIndex.GetFront()) {
          if (tokenIndex.GetIdx() == 0) { // 2
            m_vTokens.insert(m_vTokens.begin(), EditToken(m_tokenEditPtr, editTokenType));
            tokenIndex = TokenIndex(tokenIndex.GetIdx()+1, true, false, true, 0);
          }
          else { // 7
            m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx(), EditToken(m_tokenEditPtr, editTokenType));
            m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx(), EditToken(m_tokenEditPtr, "_"));
            tokenIndex = TokenIndex(tokenIndex.GetIdx()+2, true, false, true, 0);
          }
        }
        else
          if (tokenIndex.GetBack()) { // 5
            m_vTokens.push_back(EditToken(m_tokenEditPtr, editTokenType));
            tokenIndex = TokenIndex(tokenIndex.GetIdx()+1, false, true, false, 0);
          }
          else { // 4
            m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx()+1, m_vTokens[tokenIndex.GetIdx()].Divide(tokenIndex.GetTextPos()));
            m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx()+1, EditToken(m_tokenEditPtr, editTokenType));
            tokenIndex = TokenIndex(tokenIndex.GetIdx()+2, true, false, true, 0);
          }
      }
      else
        if (tokenIndex.GetFront()) { // 3
          m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx(), EditToken(m_tokenEditPtr, "_"));
          m_vTokens.insert(m_vTokens.begin()+tokenIndex.GetIdx(), EditToken(m_tokenEditPtr, editTokenType));
          tokenIndex = TokenIndex(tokenIndex.GetIdx()+2, true, false, false, 0);
        }
        else { // 6
          m_vTokens.push_back(EditToken(m_tokenEditPtr, "_"));
          m_vTokens.push_back(EditToken(m_tokenEditPtr, editTokenType));
          tokenIndex = TokenIndex(tokenIndex.GetIdx()+2, false, true, false, 0);
        }
    }
    else {
      m_vTokens.push_back(EditToken(m_tokenEditPtr, editTokenType));
      tokenIndex = TokenIndex(0, false, true, false, 0);
    }
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::InsertToken")
  return iResult;
}

int TTokenManager::Backspace(TCanvas* canvas, const int iCurrentPos)
{
  int iResult = 0;
  DMPTRY {
    TokenIndex tokenIndex = CaretPosToIndex(canvas, iCurrentPos, tdLeft);
    if (m_vTokens.size())
      if (tokenIndex.GetIdx() > 0) {
        if (m_vTokens[tokenIndex.GetIdx()].IsText()) {
          int iTextLen = m_vTokens[tokenIndex.GetIdx()].GetTextLength();
          m_vTokens[tokenIndex.GetIdx()].BackspaceChar(tokenIndex.GetTextPos(), tokenIndex.GetIdx() < ((int) m_vTokens.size()-1));
          if (iTextLen != m_vTokens[tokenIndex.GetIdx()].GetTextLength())
            tokenIndex--;
          if (m_vTokens[tokenIndex.GetIdx()].GetTextLength() == 0) {
            if (tokenIndex.GetIdx() < ((int) m_vTokens.size()-1))
              throw DMPException("Cannot let a text token become empty unless it is either at the begging or end of the token array");
            m_vTokens.erase(m_vTokens.begin()+((int) m_vTokens.size()-1));
            tokenIndex = TokenIndex((int) m_vTokens.size()-1, false, true, false, 0);
          }
        }
        else {
          int iTokenIdx = tokenIndex.GetIdx();
          m_vTokens.erase(m_vTokens.begin()+iTokenIdx);
          if (iTokenIdx < (int) m_vTokens.size()) {
            tokenIndex = TokenIndex(iTokenIdx-1, false, false, true, m_vTokens[iTokenIdx-1].GetTextLength());
            m_vTokens[iTokenIdx-1].Merge(m_vTokens[iTokenIdx]);
            m_vTokens.erase(m_vTokens.begin()+iTokenIdx);
          }
          else
            tokenIndex = TokenIndex(iTokenIdx-1, false, true, true, m_vTokens[iTokenIdx-1].GetTextLength());
        }
      }
      else
        if (m_vTokens[0].IsText()) {
          m_vTokens[0].BackspaceChar(tokenIndex.GetTextPos(), false);
          tokenIndex--;
          if (m_vTokens[0].GetTextLength() == 0) {
            m_vTokens.erase(m_vTokens.begin());
            tokenIndex = TokenIndex(0, true, false, false, 0);
          }
        }
        else {
          m_vTokens.erase(m_vTokens.begin());
          tokenIndex = TokenIndex(0, true, false, false, 0);
        }
    iResult = IndexToCaretPos(canvas, tokenIndex);
  }
  DMPCATCHTHROW("TTokenManager::Backspace")
  return iResult;
}

void TTokenManager::Delete(TCanvas* canvas, const int iCurrentPos)
{
  DMPTRY {
    TokenIndex tokenIndex = CaretPosToIndex(canvas, iCurrentPos, tdRight);
    if (tokenIndex.GetIdx() < ((int) m_vTokens.size()-1)) {
      if (m_vTokens[tokenIndex.GetIdx()].IsText()) {
        m_vTokens[tokenIndex.GetIdx()].DeleteChar(tokenIndex.GetTextPos(), tokenIndex.GetIdx() > 0);
        if (m_vTokens[tokenIndex.GetIdx()].GetTextLength() == 0) {
          if (tokenIndex.GetIdx() > 0)
            throw DMPException("Cannot let a text token become empty unless it is either at the begging or end of the token array");
          m_vTokens.erase(m_vTokens.begin());
        }
      }
      else {
        m_vTokens.erase(m_vTokens.begin()+tokenIndex.GetIdx());
        if (tokenIndex.GetIdx() > 0) {
          m_vTokens[tokenIndex.GetIdx()-1].Merge(m_vTokens[tokenIndex.GetIdx()]);
          m_vTokens.erase(m_vTokens.begin()+tokenIndex.GetIdx());
        }
      }
    }
    else
      if (tokenIndex.GetIdx() == ((int) m_vTokens.size()-1) && !tokenIndex.GetBack())
        if (m_vTokens[tokenIndex.GetIdx()].IsText()) {
          m_vTokens[tokenIndex.GetIdx()].DeleteChar(tokenIndex.GetTextPos(), false);
          if (m_vTokens[tokenIndex.GetIdx()].GetTextLength() == 0)
            m_vTokens.erase(m_vTokens.begin()+tokenIndex.GetIdx());
        }
        else
          m_vTokens.erase(m_vTokens.begin()+tokenIndex.GetIdx());
  }
  DMPCATCHTHROW("TTokenManager::Delete")
}

int TTokenManager::Clear()
{
  int iResult = 0;
  DMPTRY {
    m_vTokens.clear();
    iResult = m_tokenEditPtr->GetMinDrawX();
  }
  DMPCATCHTHROW("TTokenManager::Clear")
  return iResult;
}

String TTokenManager::EncodeTokenString() const
{
  String sResult;
  DMPTRY {
    sResult = EncodeTokenStringStatic(m_vTokens);
  }
  DMPCATCHTHROW("TTokenManager::EncodeTokenString")
  return sResult;
}

void TTokenManager::DecodeTokenString(const String& sTokens)
{
  DMPTRY {
    m_vTokens = DecodeTokenStringStatic(m_tokenEditPtr, sTokens);
  }
  DMPCATCHTHROW("TTokenManager::DecodeTokenString")
}

// CR 14584 06/01/2006 DJK
String TTokenManager::GetDisplayString() const
{
  String sResult;
  DMPTRY {
    for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end() /*&& textRect.Left < textRect.Right*/; ++itt)
      sResult += itt->AsDisplayed();
  }
  DMPCATCHTHROW("TTokenManager::GetDisplayString")
  return sResult;
}

String TTokenManager::ValidateFileNameTemplate(const String& sFileName, const bool bCombineProcDates, const ImageFileType imageFileType)
{
  String sResult;
  DMPTRY {
    bool bGenerateDefault = false;

    std::vector<int> vTokenTypesValid     = FetchValidTokenTypeList    (bCombineProcDates, imageFileType);
    std::vector<int> vTokenTypesEssential = FetchEssentialTokenTypeList(bCombineProcDates, imageFileType);

    DMPException* dmpEx = new DMPException();
    std::vector<EditToken> vTokens;
    DMPTRY {
      String sTemp = sFileName;
      vTokens = DecodeTokenStringStatic(0, sTemp);
    }
    DMPCATCH_GET("Task: Decode encoded image file name", *dmpEx)
    bGenerateDefault = (!dmpEx->Empty || vTokens.size() == 0);
    delete dmpEx;

    if (bGenerateDefault)
      sResult = GenerateDefaultEncodedFileName(bCombineProcDates, imageFileType);
    else {
      DMPTRY {
        std::vector<EditToken>::iterator itt = vTokens.begin();
        while (itt < vTokens.end()) {
          if (itt->GetTokenType() != EditToken::ettText) {
            bool bFoundMatch = false;
            for (int j = 0; j < (int) vTokenTypesValid.size(); ++j)
              if (itt->GetTokenType() == vTokenTypesValid[j]) {
                bFoundMatch = true;
                break;
              }
            if (!bFoundMatch) {
              itt = vTokens.erase(itt);
              if (itt > vTokens.begin() && itt < vTokens.end()) { // Merge surrounding text tokens if possible.
                (itt-1)->Merge(*itt);
                itt = vTokens.erase(itt);
              }
            }
          }
          itt++;
        }
      }
      DMPCATCHTHROW("Task: Determine if there are any tokens in the input string that do not belong (remove them if found)")

      DMPTRY {
        std::vector<int> vTokenTypesMissing;
        for (int i = 0; i < (int) vTokenTypesEssential.size(); ++i) {
          bool bFoundMatch = false;
          for (int j = 0; j < (int) vTokens.size(); ++j)
            if (vTokenTypesEssential[i] == vTokens[j].GetTokenType()) {
              bFoundMatch = true;
              break;
            }
          if (!bFoundMatch)
            vTokenTypesMissing.push_back(vTokenTypesEssential[i]);
        }
        for (int i = 0; i < (int) vTokenTypesMissing.size(); ++i) {
          if (vTokens.size())
            if (vTokens.back().GetTokenType() != EditToken::ettText)
              vTokens.push_back(EditToken(0, "_"));
          vTokens.push_back(EditToken(0, (EditToken::EditTokenType) vTokenTypesMissing[i]));
        }
      }
      DMPCATCHTHROW("Task: Make sure all essential tokens are present in the input string (if not, add missing essentials)")

      sResult = EncodeTokenStringStatic(vTokens);
    }
  }
  DMPCATCHTHROW("TTokenManager::ValidateFileNameTemplate")
  return sResult;
}

std::vector<int> TTokenManager::FetchEssentialTokenTypesMissingFromString(const String& sFileName, const bool bCombineProcDates, const ImageFileType imageFileType)
{
  std::vector<int> vResult;
  DMPTRY {
    std::vector<int> vTokenTypesEssential = FetchEssentialTokenTypeList(bCombineProcDates, imageFileType);
    
    std::vector<EditToken> vTokens;
    String sTemp = sFileName;
    vTokens = DecodeTokenStringStatic(0, sTemp);

    DMPTRY {
      for (int i = 0; i < (int) vTokenTypesEssential.size(); ++i) {
        bool bFoundMatch = false;
        for (int j = 0; j < (int) vTokens.size(); ++j)
          if (vTokenTypesEssential[i] == vTokens[j].GetTokenType()) {
            bFoundMatch = true;
            break;
          }
        if (!bFoundMatch)
          vResult.push_back(vTokenTypesEssential[i]);
      }
    }
    DMPCATCHTHROW("Task: Compile list of essential tokens missing from input string")
  }
  DMPCATCHTHROW("FetchEssentialTokenTypesMissingFromString")
  return vResult;
}

std::vector<int> TTokenManager::FetchValidTokenTypeList(const bool bCombineProcDates, const ImageFileType imageFileType)
{
  std::vector<int> vResult;
  DMPTRY {
    switch (imageFileType) {
      case iftSingle  : vResult.push_back(EditToken::ettBank        );
                        vResult.push_back(EditToken::ettLockbox     );
                        vResult.push_back(EditToken::ettBatch       );
                        vResult.push_back(EditToken::ettBatchType   );
                        vResult.push_back(EditToken::ettProcDate    );
                        vResult.push_back(EditToken::ettTransaction );
                        vResult.push_back(EditToken::ettDocumentType);
                        vResult.push_back(EditToken::ettFileCounter );
                        break;
      case iftPerTran : vResult.push_back(EditToken::ettBank        );
                        vResult.push_back(EditToken::ettLockbox     );
                        vResult.push_back(EditToken::ettBatch       );
                        vResult.push_back(EditToken::ettBatchType   );
                        if (!bCombineProcDates)
                          vResult.push_back(EditToken::ettProcDate  );
                        vResult.push_back(EditToken::ettTransaction );
                        break;
      case iftPerBatch: vResult.push_back(EditToken::ettBank        );
                        vResult.push_back(EditToken::ettLockbox     );
                        vResult.push_back(EditToken::ettBatch       );
                        vResult.push_back(EditToken::ettBatchType   );
                        if (!bCombineProcDates)
                          vResult.push_back(EditToken::ettProcDate  );
                        break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("TTokenManager::FetchValidTokenTypeList")
  return vResult;
}

std::vector<int> TTokenManager::FetchEssentialTokenTypeList(const bool bCombineProcDates, const ImageFileType imageFileType)
{
  std::vector<int> vResult;
  DMPTRY {
    switch (imageFileType) {
      case iftSingle  : vResult.push_back(EditToken::ettFileCounter);
                        break;
      case iftPerTran : vResult.push_back(EditToken::ettBank       );
                        vResult.push_back(EditToken::ettLockbox    );
                        vResult.push_back(EditToken::ettBatch      );
                        if (!bCombineProcDates)
                          vResult.push_back(EditToken::ettProcDate );
                        vResult.push_back(EditToken::ettTransaction);
                        break;
      case iftPerBatch: vResult.push_back(EditToken::ettBank       );
                        vResult.push_back(EditToken::ettLockbox    );
                        vResult.push_back(EditToken::ettBatch      );
                        if (!bCombineProcDates)
                          vResult.push_back(EditToken::ettProcDate );
                        break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("TTokenManager::FetchEssentialTokenTypeList")
  return vResult;
}

String TTokenManager::GenerateDefaultEncodedFileName(const bool bCombineProcDates, const ImageFileType imageFileType)
{
  String sResult;
  DMPTRY {
    std::vector<EditToken> vTokens;
    switch (imageFileType) {
      case iftSingle  : vTokens.push_back(EditToken(0, "ExtractImage"));
                        vTokens.push_back(EditToken(0, EditToken::ettFileCounter));
                        break;
      case iftPerTran : vTokens.push_back(EditToken(0, EditToken::ettBank       ));
                        vTokens.push_back(EditToken(0, "_"                      ));
                        vTokens.push_back(EditToken(0, EditToken::ettLockbox    ));
                        vTokens.push_back(EditToken(0, "_"                      ));
                        vTokens.push_back(EditToken(0, EditToken::ettBatch      ));
                        if (!bCombineProcDates) {
                          vTokens.push_back(EditToken(0, "_"                    ));
                          vTokens.push_back(EditToken(0, EditToken::ettProcDate ));
                        }
                        vTokens.push_back(EditToken(0, "_"                      ));
                        vTokens.push_back(EditToken(0, EditToken::ettTransaction));
                        break;
      case iftPerBatch: vTokens.push_back(EditToken(0, EditToken::ettBank       ));
                        vTokens.push_back(EditToken(0, "_"                      ));
                        vTokens.push_back(EditToken(0, EditToken::ettLockbox    ));
                        vTokens.push_back(EditToken(0, "_"                      ));
                        vTokens.push_back(EditToken(0, EditToken::ettBatch      ));
                        if (!bCombineProcDates) {
                          vTokens.push_back(EditToken(0, "_"                    ));
                          vTokens.push_back(EditToken(0, EditToken::ettProcDate ));
                        }
                        break;
      default: throw DMPException("Illegal case");
    }
    sResult = EncodeTokenStringStatic(vTokens);
  }
  DMPCATCHTHROW("TTokenManager::GenerateDefaultEncodedFileName")
  return sResult;
}

void TTokenManager::SingToMe()
{
  DMPTRY {
    ::Beep(1000, 100);
    for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end(); ++itt) {
      ::Beep(5000, 40);
      ::Sleep(90);
    }
  }
  DMPCATCHTHROW("TTokenManager::SingToMe")
}

TokenIndex TTokenManager::CaretPosToIndex(TCanvas* canvas, const int iCurrentPos, const TravelDirection travelDirection) const
{
  TokenIndex tiResult;
  DMPTRY {
    if (iCurrentPos <= m_tokenEditPtr->GetMinDrawX())
      tiResult = TokenIndex(0, true, false, m_vTokens.size() ? m_vTokens[0].IsText() : false, 0);
    else {
      int iLastPos = m_tokenEditPtr->GetMinDrawX();
      int iThisPos = m_tokenEditPtr->GetMinDrawX();
      for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end(); ++itt) {
        iThisPos += itt->GetWidth(canvas);
        if (iThisPos >= iCurrentPos) {
          if (itt->IsText()) {
            int iInternalPosition = itt->GetCharIndex(canvas, iCurrentPos-iLastPos);
            tiResult = TokenIndex(itt-m_vTokens.begin(), iInternalPosition == 0, iInternalPosition >= itt->GetTextLength(), true, iInternalPosition);
          }
          else
            if ((iCurrentPos-iLastPos) < (iThisPos-iCurrentPos))
              tiResult = TokenIndex(itt-m_vTokens.begin(), true, false, false, 0);
            else
              tiResult = TokenIndex(itt-m_vTokens.begin(), false, true, false, 0);
          break;
        }
        iLastPos = iThisPos;
      }
    }
    if (tiResult.GetIdx() == -1) { // Must not have been found yet - beyond end - use last one.
      if (m_vTokens.size()) {
        if (m_vTokens.back().IsText())
          tiResult = TokenIndex((int) m_vTokens.size()-1, false, true, true, m_vTokens.back().GetTextLength());
        else
          tiResult = TokenIndex((int) m_vTokens.size()-1, false, true, false, 0);
      }
      else
        tiResult = TokenIndex(0, true, false, false, 0);
    }
    else  // DID find one - now make as adjustment (bit of a Kludge):
      if (travelDirection == tdRight) {
        if (tiResult.GetBack() && tiResult.GetIdx() < ((int) m_vTokens.size()-1))
          tiResult = TokenIndex(tiResult.GetIdx()+1, true, false, m_vTokens[tiResult.GetIdx()+1].IsText(), 0);
      }
      else
        if (travelDirection == tdLeft)
          if (tiResult.GetFront() && tiResult.GetIdx() > 0) {
            bool bIsText = m_vTokens[tiResult.GetIdx()-1].IsText();
            tiResult = TokenIndex(tiResult.GetIdx()-1, false, true, bIsText, bIsText ? m_vTokens[tiResult.GetIdx()-1].GetTextLength() : 0);
          }
  }
  DMPCATCHTHROW("TTokenManager::CaretPosToIndex")
  return tiResult;
}

int TTokenManager::IndexToCaretPos(TCanvas* canvas, const TokenIndex& tokenIndex) const
{
  int iResult = m_tokenEditPtr->GetMinDrawX();
  DMPTRY {
    if (tokenIndex.GetIdx() >= 0)
      for (std::vector<EditToken>::const_iterator itt = m_vTokens.begin(); itt < m_vTokens.end(); ++itt) {
        if ((itt-m_vTokens.begin()) < tokenIndex.GetIdx())
          iResult += itt->GetWidth(canvas);
        else {
          if (tokenIndex.GetBack())
            iResult += itt->GetWidth(canvas);
          else
            if (!tokenIndex.GetFront()) // Must be a 'text' token
              iResult += itt->GetWidth(canvas, tokenIndex.GetTextPos());
          break;
        }
      }
  }
  DMPCATCHTHROW("TTokenManager::IndexToCaretPos")
  return iResult;
}

String TTokenManager::EncodeTokenStringStatic(const std::vector<EditToken>& vTokens)
{
  String sResult;
  DMPTRY {
    for (std::vector<EditToken>::const_iterator itt = vTokens.begin(); itt < vTokens.end(); ++itt)
      sResult += itt->EncodeToString();
  }
  DMPCATCHTHROW("TTokenManager::EncodeTokenStringStatic")
  return sResult;
}

std::vector<EditToken> TTokenManager::DecodeTokenStringStatic(TTokenEdit* tokenEditPtr, const String& sTokens)
{
  std::vector<EditToken> vTokens;
  DMPTRY {
    String sTemp = sTokens;
    EditToken::EditTokenType lastTokenType = EditToken::ettNone;
    while (sTemp.Length()) {
      EditToken editToken = EditToken::DecodeString(tokenEditPtr, sTemp);
      if (lastTokenType == EditToken::ettNone || (lastTokenType == EditToken::ettText && editToken.GetTokenType() != EditToken::ettText) || (lastTokenType != EditToken::ettText && editToken.GetTokenType() == EditToken::ettText)) {
        vTokens.push_back(editToken);
        lastTokenType = editToken.GetTokenType();
      }
      else
        throw DMPException("Input string '"+sTokens+"' has two tokens next to each other without an intervening text token separator");
    }
  }
  DMPCATCHTHROW("TTokenManager::DecodeTokenStringStatic")
  return vTokens;
}

//---------------------------------------------------------------------------

__fastcall TTokenEdit::TTokenEdit(Classes::TComponent* AOwner)
 : TCustomControl(AOwner)
{
  DMPTRY {
    Width   = 121;
    Height  = 21;
    TabStop = true;
    Cursor  = crIBeam;
    m_iMinDrawX             = 3;
    m_iMaxDrawX             = Width-3;
    m_iOffsetDrawX          = 0;
    m_sDateFormat           = "MMDDYY";
    m_bZeroPadded           = true;
    m_bBatchStatAbbreviated = false;
    FVisibleBorder          = true;
    FBackgroundColor        = clWindow;
    m_wndMethodOriginal = WindowProc;
    WindowProc = HandleWindowsMessage;
    m_tokenManager = new TTokenManager(this);
    m_caretManager = new TCaretManager(this, FBackgroundColor);
  }
  DMPCATCHTHROW("TTokenEdit::TTokenEdit")
}

__fastcall TTokenEdit::~TTokenEdit()
{
  DMPTRY {
    delete m_caretManager;
    delete m_tokenManager;
  }
  DMPCATCHTHROW("TTokenEdit::~TTokenEdit")
}

void TTokenEdit::Clear()
{
  DMPTRY {
    m_caretManager->SetPosition(m_tokenManager->Clear());
    if (!AdjustXOffsetIfNecessary())
      Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::Clear")
}

void TTokenEdit::InsertToken(const EditToken::EditTokenType editTokenType)
{
  DMPTRY {
    int iInsertWhere = Enabled ? m_caretManager->GetPosition() : 10000;
    m_caretManager->SetPosition(m_tokenManager->InsertToken(Canvas, iInsertWhere, editTokenType));
    bool bDidRepaint = false;
    if (Enabled)
      bDidRepaint = AdjustXOffsetIfNecessary();
    if (!bDidRepaint)
      Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::InsertToken")
}

void TTokenEdit::InsertText(const String& sText)
{
  DMPTRY {
    int iInsertWhere = Enabled ? m_caretManager->GetPosition() : 10000;
    m_caretManager->SetPosition(m_tokenManager->InsertString(Canvas, iInsertWhere, sText));
    bool bDidRepaint = false;
    if (Enabled)
      bDidRepaint = AdjustXOffsetIfNecessary();
    if (!bDidRepaint)
      Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::InsertText")
}

String TTokenEdit::ValidateDateFormatString(const String& sFormat)
{
  String sResult;
  DMPTRY {
    String sTemp = sFormat.UpperCase();
    bool bGood = (sTemp.Pos("MM") && sTemp.Pos("DD") && (sTemp.Length() == 6 && sTemp.Pos("YY") || sTemp.Length() == 8 && sTemp.Pos("YYYY")));
    if (!bGood)
      sResult = "Date format '"+sTemp+"' is not valid:\r\n"
                "  - must contain 'DD' for day of month\r\n"
                "  - must contain 'MM' for month\r\n"
                "  - must contain either 'YY' or 'YYYY' for year\r\n"
                "  (cannot contain anything else)";
  }
  DMPCATCHTHROW("TTokenEdit::ValidateDateFormatString")
  return sResult;
}

void __fastcall TTokenEdit::HandleWindowsMessage(Messages::TMessage &Message)
{
  DMPTRY {
    switch (Message.Msg) {
      case WM_LBUTTONDOWN:
          SetFocus();
          m_caretManager->SetPosition(m_tokenManager->DetermineClosestPosition(Canvas, (int) ((Message.LParam) & 0x0000ffff) + m_iOffsetDrawX));
          AdjustXOffsetIfNecessary();
          break;
      case WM_SETFOCUS :
          m_caretManager->SetPosition(m_tokenManager->DetermineEndPosition(Canvas));
          m_caretManager->GotFocus(true);
          break;
      case WM_KILLFOCUS:
          m_caretManager->GotFocus(false);
          break;
      case WM_SIZE     :
          m_iMaxDrawX = (int) ((Message.LParam) & 0x0000ffff) - 3;
          break;
      case CN_KEYDOWN  :
          switch (Message.WParam) {
            case VK_HOME  : m_caretManager->SetPosition(m_iMinDrawX);
                            AdjustXOffsetIfNecessary();
                            Message.Result = 1;
                            break;
            case VK_END   : m_caretManager->SetPosition(m_tokenManager->DetermineEndPosition(Canvas));
                            AdjustXOffsetIfNecessary();
                            Message.Result = 1;
                            break;
            case VK_LEFT  :
            case VK_UP    : m_caretManager->SetPosition(m_tokenManager->DetermineMoveLeftPosition(Canvas, m_caretManager->GetPosition()));
                            AdjustXOffsetIfNecessary();
                            Message.Result = 1;
                            break;
            case VK_RIGHT :
            case VK_DOWN  : m_caretManager->SetPosition(m_tokenManager->DetermineMoveRightPosition(Canvas, m_caretManager->GetPosition()));
                            AdjustXOffsetIfNecessary();
                            Message.Result = 1;
                            break;
            case VK_BACK  : m_caretManager->SetPosition(m_tokenManager->Backspace(Canvas, m_caretManager->GetPosition()));
                            if (!AdjustXOffsetIfNecessary())
                              Repaint();
                            Message.Result = 1;
                            break;
            case VK_DELETE: m_tokenManager->Delete(Canvas, m_caretManager->GetPosition());
                            if (!AdjustXOffsetIfNecessary())
                              Repaint();
                            Message.Result = 1;
                            break;
          }
          break;
      case WM_CHAR     :
          {
            char cChar = Message.WParam;
            if (EditToken::IsLegalFileNameCharacter(cChar)) {
              m_caretManager->SetPosition(m_tokenManager->InsertString(Canvas, m_caretManager->GetPosition(), String(&cChar, 1)));
              if (!AdjustXOffsetIfNecessary())
                Repaint();
            }
          }
          break;
    }
    if (Message.Result == 0)
      TCustomControl::WndProc(Message);
  }
  DMPCATCHTHROW("TTokenEdit::HandleWindowsMessage")
}

void __fastcall TTokenEdit::Paint()
{
  DMPTRY {
    DrawFrame();
    m_tokenManager->Paint(Canvas, TRect(TTokenEdit::m_iMinDrawX, 3, Width-3, Height-5));
  }
  DMPCATCHTHROW("TTokenEdit::Paint")
}

void TTokenEdit::DrawFrame()
{
  DMPTRY {
    Canvas->Brush->Color = FBackgroundColor;
    Canvas->Pen->Color   = FVisibleBorder ? clBtnHighlight : Canvas->Brush->Color;
    Canvas->Rectangle(TRect(0, 0, Width, Height));

    if (FVisibleBorder) {
      Canvas->Pen->Color = clBtnShadow;
      Canvas->MoveTo(0      , Height-2);
      Canvas->LineTo(0      , 0       );
      Canvas->LineTo(Width-1, 0       );

      Canvas->Pen->Color = cl3DLight;
      Canvas->MoveTo(1      , Height-2);
      Canvas->LineTo(Width-2, Height-2);
      Canvas->LineTo(Width-2,        0);

      Canvas->Pen->Color = cl3DDkShadow;
      Canvas->MoveTo(1      , Height-3);
      Canvas->LineTo(1      , 1       );
      Canvas->LineTo(Width-2, 1       );
    }
  }
  DMPCATCHTHROW("TTokenEdit::DrawFrame")
}

bool TTokenEdit::AdjustXOffsetIfNecessary()
{
  bool bResult = false;
  DMPTRY {
    int iOffsetOriginal = m_iOffsetDrawX;
    int iCaretPos = m_caretManager->GetPosition();
    int iViolation = (m_iMinDrawX+3) - (iCaretPos-m_iOffsetDrawX);
    if (iViolation > 0) {
      m_iOffsetDrawX -= iViolation + 35;
      if (m_iOffsetDrawX < 0)
        m_iOffsetDrawX = 0;
    }
    else {
      iViolation = (iCaretPos-m_iOffsetDrawX) - (m_iMaxDrawX-3);
      if (iViolation > 0)
        m_iOffsetDrawX += iViolation + 35;
    }
    bResult = (iOffsetOriginal != m_iOffsetDrawX);
    if (bResult)
      Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::AdjustXOffsetIfNecessary")
  return bResult;
}

void TTokenEdit::SetVisibleBorder(const bool bVal)
{
  DMPTRY {
    FVisibleBorder = bVal;
    Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::SetVisibleBorder")
}

void TTokenEdit::SetBackgroundColor(const TColor clVal)
{
  DMPTRY {
    FBackgroundColor = clVal; 
    m_caretManager->SetBackgroundColor(FBackgroundColor);
    Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::SetBackgroundColor")
}

void TTokenEdit::SetDateFormat(const String& sVal)
{
  DMPTRY {
    DMPTRY {
      String sError = ValidateDateFormatString(sVal);
      if (sError.Length())
        throw DMPException(sError);
    }
    DMPCATCHTHROW("Task: Validate input string")
    m_sDateFormat = sVal;
    Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::SetDateFormat")
}

void TTokenEdit::SetZeroPadded(const bool bVal)
{
  DMPTRY {
    m_bZeroPadded = bVal;
    Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::SetZeroPadded")
}

void TTokenEdit::SetBatchStatAbbreviated(const bool bVal)
{
  DMPTRY {
    m_bBatchStatAbbreviated = !bVal; // The '!' is not a mistake.
    Repaint();
  }
  DMPCATCHTHROW("TTokenEdit::SetBatchStatAbbreviated")
}

String TTokenEdit::GetEncodedString() const
{
  String sResult;
  DMPTRY {
    sResult = m_tokenManager->EncodeTokenString();
  }
  DMPCATCHTHROW("TTokenEdit::GetEncodedString")
  return sResult;
}

void TTokenEdit::SetEncodedString(const String& sEncoded)
{
  DMPTRY {
    m_tokenManager->DecodeTokenString(sEncoded);
    Repaint();
    m_caretManager->SetPosition(m_tokenManager->DetermineEndPosition(Canvas));
  }
  DMPCATCHTHROW("TTokenEdit::SetEncodedString")
}

String TTokenEdit::GetDisplayString() const
{
  String sResult;
  DMPTRY {
    sResult = m_tokenManager->GetDisplayString();
  }
  DMPCATCHTHROW("TTokenEdit::GetDisplayString")
  return sResult;
}


