//---------------------------------------------------------------------------
#ifndef ReportPreviewH
#define ReportPreviewH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ImgList.hpp>
#include <QRPrntr.hpp>
#include <ToolWin.hpp>
#include <Dialogs.hpp>
#include <QRExport.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
// CR 14584 06/01/2006 DJK
class TPreviewForm : public TForm
{
__published:	// IDE-managed Components
        TToolBar *ToolBar1;
        TToolButton *ToolButton1;
        TToolButton *ZoomFit;
        TToolButton *Zoom100;
        TToolButton *ZoomToWidth;
        TToolButton *Separator1;
        TToolButton *FirstPage;
        TToolButton *PreviousPage;
        TToolButton *NextPage;
        TToolButton *LastPage;
        TToolButton *Separator2;
        TToolButton *Print;
        TToolButton *Separator3;
        TToolButton *SaveReport;
        TToolButton *Separator4;
        TSpeedButton *ExitButton;
        TQRPreview *QRPreview1;
        TImageList *Images;
        TStatusBar *StatusBar;
        TQRTextFilter *QRTextFilter1;
        TTimer *ViewTimer;
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
        void __fastcall FormShow(TObject *Sender);
        void __fastcall ZoomFitClick(TObject *Sender);
        void __fastcall Zoom100Click(TObject *Sender);
        void __fastcall ZoomToWidthClick(TObject *Sender);
        void __fastcall FirstPageClick(TObject *Sender);
        void __fastcall PreviousPageClick(TObject *Sender);
        void __fastcall NextPageClick(TObject *Sender);
        void __fastcall LastPageClick(TObject *Sender);
        void __fastcall ExitButtonClick(TObject *Sender);
        void __fastcall FormResize(TObject *Sender);
        void __fastcall SaveReportClick(TObject *Sender);
        void __fastcall PrintSetupClick(TObject *Sender);
        void __fastcall QRPreview1PageAvailable(TObject *Sender,
          int PageNum);
        void __fastcall QRPreview1ProgressUpdate(TObject *Sender,
          int Progress);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall ViewTimerTimer(TObject *Sender);
private:	// User declarations
        bool IsClosing;
    //    TProgressBar* Gauge;
        int LastProgress;
        int CurrentFile;
        TStringList* Files;
        bool Multi;
        int MultiPageNum;
        int TotalPages;
        void __fastcall UpdateInfo();
        void __fastcall SetViewFormat(int iViewFormat);
        int  __fastcall GetViewFormat() const;
public:		// User declarations
        TQRPrinter* QRPrinter;
        __fastcall TPreviewForm(TComponent* Owner);
        void __fastcall Reset(TQRPrinter* aQRPrinter);
        void __fastcall ShowMulti(TStringList* FileList, int Pages);
        __property int              ViewFormat = {write=SetViewFormat};
};
//---------------------------------------------------------------------------
extern PACKAGE TPreviewForm *PreviewForm;
//---------------------------------------------------------------------------
#endif
