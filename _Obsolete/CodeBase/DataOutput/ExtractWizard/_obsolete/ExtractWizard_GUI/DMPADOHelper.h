//---------------------------------------------------------------------------

#ifndef DMPADOH
#define DMPADOH
//---------------------------------------------------------------------------

#include <ADOdb.hpp>
#include <SyncObjs.hpp>
#include <vector>
#include <strstream>
#include "DMPExceptions.h"
#include "HelperClasses.h"


class ProcNameVersion
{
public:
  String m_sName;
  int    m_iVersion;
  bool   m_bNew; // i.e. - Doesn't use those three old mandatory output parameters
public:
  ProcNameVersion() {Init();}
  ProcNameVersion(const ProcNameVersion& obj) {*this = obj;}
  ProcNameVersion(const String& sName) {Init(); m_sName = sName;}
  ProcNameVersion& operator = (const ProcNameVersion& obj) {m_sName = obj.m_sName; m_iVersion = obj.m_iVersion; m_bNew = obj.m_bNew; return *this;}
  bool operator == (const ProcNameVersion& obj) const {return (m_sName.AnsiCompareIC(obj.m_sName) == 0);}
  bool operator  < (const ProcNameVersion& obj) const {return (m_sName.AnsiCompareIC(obj.m_sName)  < 0);}
  void Init() {m_iVersion = -1; m_bNew = true;}
};

class MyVariant1
{
public:
  enum MyVariantType {mvtNone, mvtString, mvtInt, mvtDateTime, mvtBool, mvtDouble};
private:
  MyVariantType m_variantType;
  String        m_sVal;
  int           m_iVal;
  TDateTime     m_dtVal;
  bool          m_bVal;
  double        m_dVal;
public:
  MyVariant1() {m_variantType = mvtNone; m_iVal = -1; m_bVal = false; m_dVal = 0.0;}
  MyVariant1(const MyVariant1& obj) {*this = obj;}
  MyVariant1(const String&    sVal) {m_sVal  =  sVal; m_variantType = mvtString  ;}
  MyVariant1(const int        iVal) {m_iVal  =  iVal; m_variantType = mvtInt     ;}
  MyVariant1(const TDateTime dtVal) {m_dtVal = dtVal; m_variantType = mvtDateTime;}
  MyVariant1(const bool       bVal) {m_bVal  =  bVal; m_variantType = mvtBool    ;}
  MyVariant1(const double     dVal) {m_dVal  =  dVal; m_variantType = mvtDouble  ;}
  MyVariant1& operator = (const MyVariant1& obj) {m_variantType = obj.m_variantType; m_sVal = obj.m_sVal; m_iVal = obj.m_iVal; m_dtVal = obj.m_dtVal; m_bVal = obj.m_bVal; m_dVal = obj.m_dVal; return *this;}
  operator String   () const;
  operator int      () const;
  operator TDateTime() const;
  operator bool     () const;
  operator double   () const;
  MyVariantType GetType() const {return m_variantType;}
};

class MyVariant2
{
private:
  Variant m_variant;
public:
  MyVariant2() {}
  MyVariant2(const MyVariant2& obj) {*this = obj;}
  MyVariant2(const Variant& variant) {m_variant = variant;}
  MyVariant2& operator = (const MyVariant2& obj) {m_variant = obj.m_variant; return *this;}
  operator String   () const;
  operator int      () const;
  operator TDateTime() const;
  operator bool     () const;
  operator double   () const;
  bool IsEmpty() const;
};

class ParameterItem
{
private:
  String              m_sParameterName;
  MyVariant1          m_vtVal;
  int                 m_iParameterLength;
  TParameterDirection m_parameterDirection;
  bool                m_bNewFormalism;
public:
  ParameterItem() {m_iParameterLength = -1; m_parameterDirection = pdInput; m_bNewFormalism = true;}
  ParameterItem(const ParameterItem& obj) {*this = obj;}
  ParameterItem(const String& sParameterName, const MyVariant1& vtVal, const int iParameterLength, const TParameterDirection parameterDirection, const bool bNewFormalism = true) {m_sParameterName = sParameterName; m_vtVal = vtVal; m_iParameterLength = iParameterLength; m_parameterDirection = parameterDirection; m_bNewFormalism = bNewFormalism;}
  ParameterItem& operator = (const ParameterItem& obj) {m_sParameterName = obj.m_sParameterName; m_vtVal = obj.m_vtVal; m_iParameterLength = obj.m_iParameterLength; m_parameterDirection = obj.m_parameterDirection; m_bNewFormalism = obj.m_bNewFormalism; return *this;}
  String                    GetParameterName        () const {return m_sParameterName    ;}
  MyVariant1::MyVariantType GetParameterValueType   () const {return m_vtVal.GetType()   ;}
  MyVariant1                GetParameterValue       () const {return m_vtVal             ;}
  int                       GetParameterLength      () const {return m_iParameterLength  ;}
  TParameterDirection       GetParameterDirection   () const {return m_parameterDirection;}
  bool                      GetParameterNewFormalism() const {return m_bNewFormalism     ;}
};

class PACKAGE TADOStoredProc2 : public TADOStoredProc
{
public:
	__fastcall virtual TADOStoredProc2(Classes::TComponent* AOwner);
public:
  void __fastcall Open(void);
	void __fastcall ExecProc(void);
__published:
	__property DataSource ;
	__property EnableBCD ;
	__property WideString ProcedureName = {read=GetCommandText, write=SetCommandText};
	__property Parameters ;
	__property Prepared ;
};

class DMPADOStoredProc
{
private:
  TADOStoredProc2* m_adoSP;
  int              m_iDeclaredVersion;
  TADOConnection*  m_pConnection;
  bool             m_bProcNameGood;
  bool             m_bNewFormalism;
  static String    m_sMagicParamString;
  static std::vector<ProcNameVersion> m_vProcNameVers;
  std::vector<ParameterItem> m_vParameterItems; // I store parameter info here so I can recreate TADOSToredProc params at any time (for retries)
public:
  static TCriticalSection* m_criticalSection;
  static int               m_iRetryCount;
public:
  DMPADOStoredProc(const String& sName, int iDeclaredVersion, TADOConnection * pConnection);
  ~DMPADOStoredProc();
  void       ExecProc();
  void       Open();
  void       AddParameter(const String& sParamName, const String&    sParamValue , /* for GUIDS */   TParameterDirection paramDirection = pdInput);
  void       AddParameter(const String& sParamName, const String&    sParamValue , int iParamLength, TParameterDirection paramDirection = pdInput);
  void       AddParameter(const String& sParamName, int              iParamValue ,                   TParameterDirection paramDirection = pdInput);
  void       AddParameter(const String& sParamName, const TDateTime& dtParamValue,                   TParameterDirection paramDirection = pdInput);
  void       AddParameter(const String& sParamName, bool             bParamValue ,                   TParameterDirection paramDirection = pdInput);
  void       AddParameter(const String& sParamName, double           dParamValue ,                   TParameterDirection paramDirection = pdInput);
  MyVariant2 GetParamByName(const String& sParamName) const;
  TField*    FieldByName(const String& sFieldName);
  void       First();
  void       Next();
  void       GetFieldNames(std::vector<String>& vList) const;
  __property TField* Fields[int Index] = {read=GetField      };
  __property bool    Eof               = {read=GetEof        };
  __property int     RecordCount       = {read=GetRecordCount};
  __property int     FieldCount        = {read=GetFieldCount };
private:
  TField* GetField(int Index);
  bool    GetEof() const;
  int     GetRecordCount() const;
  int     GetFieldCount() const;
  void    ApplyParameters();
  void    CheckForSPReturnedError();
  String  GetParameters   (bool bInputsOnly = true);
  void    GetParmType     (std::ostrstream& stream, int iP);
  void    GetParmDirection(std::ostrstream& stream, int iP);
  void    GetParmName     (std::ostrstream& stream, int iP);
  void    GetParmValue    (std::ostrstream& stream, int iP);
  std::ostrstream& DoStream(std::ostrstream& stream, const TDateTime& dt);
  static bool DoProcNameCheck(const String& sProcedureName, int iDeclaredVersion);
  static void PopulateProcNamesList(TADOConnection* adoConnection);
};


#endif
