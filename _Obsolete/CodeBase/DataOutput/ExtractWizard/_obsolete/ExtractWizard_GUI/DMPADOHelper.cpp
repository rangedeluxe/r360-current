//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "DMPADOHelper.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)


MyVariant1::operator String() const
{
  DMPTRY {
    if (m_variantType != mvtString)
      throw DMPException("Value is not of this type");
  }
  DMPCATCHTHROW("MyVariant1::operator String")
  return m_sVal;
}

MyVariant1::operator int() const
{
  DMPTRY {
    if (m_variantType != mvtInt)
      throw DMPException("Value is not of this type");
  }
  DMPCATCHTHROW("MyVariant1::operator int")
  return m_iVal;
}

MyVariant1::operator TDateTime() const
{
  DMPTRY {
    if (m_variantType != mvtDateTime)
      throw DMPException("Value is not of this type");
  }
  DMPCATCHTHROW("MyVariant1::operator TDateTime")
  return m_dtVal;
}

MyVariant1::operator bool() const
{
  DMPTRY {
    if (m_variantType != mvtBool)
      throw DMPException("Value is not of this type");
  }
  DMPCATCHTHROW("MyVariant1::operator bool")
  return m_bVal;
}

MyVariant1::operator double() const
{
  DMPTRY {
    if (m_variantType != mvtDouble)
      throw DMPException("Value is not of this type");
  }
  DMPCATCHTHROW("MyVariant1::operator double")
  return m_dVal;
}

//---------------------------------------------------------------------------

MyVariant2::operator String() const
{
  String sResult;
  DMPTRY {
    if (!IsEmpty())
      sResult = (String) m_variant;
  }
  DMPCATCHTHROW("MyVariant2::operator String")
  return sResult;
}

MyVariant2::operator int() const
{
  int iResult = 0;
  DMPTRY {
    if (!IsEmpty())
      iResult = (int) m_variant;
  }
  DMPCATCHTHROW("MyVariant2::operator int")
  return iResult;
}

MyVariant2::operator TDateTime() const
{
  TDateTime dtResult;
  DMPTRY {
    if (!IsEmpty())
      dtResult = (TDateTime) m_variant;
  }
  DMPCATCHTHROW("MyVariant2::operator TDateTime")
  return dtResult;
}

MyVariant2::operator bool() const
{
  bool bResult = false;
  DMPTRY {
    if (!IsEmpty())
      bResult = (bool) m_variant;
  }
  DMPCATCHTHROW("MyVariant2::operator bool")
  return bResult;
}

MyVariant2::operator double() const
{
  double dResult = 0.0;
  DMPTRY {
    if (!IsEmpty())
      dResult = (double) m_variant;
  }
  DMPCATCHTHROW("MyVariant2::operator double")
  return dResult;
}

bool MyVariant2::IsEmpty() const
{
  bool bResult;
  DMPTRY {
    bResult = (m_variant.Type() == varEmpty || m_variant.Type() == varNull);
  }
  DMPCATCHTHROW("MyVariant2::IsEmpty()")
  return bResult;
}

//---------------------------------------------------------------------------

__fastcall TADOStoredProc2::TADOStoredProc2(Classes::TComponent* AOwner)
 : TADOStoredProc(AOwner)
{
}

void __fastcall TADOStoredProc2::Open(void)
{
  if (Connection)
    CommandTimeout = Connection->CommandTimeout;
  TADOStoredProc::Open();
}

void __fastcall TADOStoredProc2::ExecProc(void)
{
  if (Connection)
    CommandTimeout = Connection->CommandTimeout;
  TADOStoredProc::ExecProc();
}

//---------------------------------------------------------------------------

std::vector<ProcNameVersion> DMPADOStoredProc::m_vProcNameVers;
TCriticalSection*            DMPADOStoredProc::m_criticalSection = 0;
int                          DMPADOStoredProc::m_iRetryCount = 3;
String                       DMPADOStoredProc::m_sMagicParamString = "@parmProcVersion_";

DMPADOStoredProc::DMPADOStoredProc(const String& sName, int iDeclaredVersion, TADOConnection* pConnection)
{
  DMPTRY {
    m_adoSP = 0;

    if (m_criticalSection == 0)
      throw DMPException("'DMPADOStoredProc's critical section object needs to be instanciated at some higher level");
    PopulateProcNamesList(pConnection);

    m_adoSP = new TADOStoredProc2(0);
    m_adoSP->Connection    = pConnection;
    m_adoSP->ProcedureName = sName;

    m_iDeclaredVersion = iDeclaredVersion;
    m_bProcNameGood = false;
    m_bNewFormalism = false;

    m_vParameterItems.push_back(ParameterItem("@RETURN_VALUE"                               , (int)      0,   -1, pdReturnValue       ));
    m_vParameterItems.push_back(ParameterItem("@parmRowsReturned"                           , (int)      0,   -1, pdOutput     , false));
    m_vParameterItems.push_back(ParameterItem("@parmErrorDescription"                       , (String)  "", 2000, pdOutput     , false));
    m_vParameterItems.push_back(ParameterItem("@parmSQLErrorNumber"                         , (int)      0,   -1, pdOutput     , false));
    m_vParameterItems.push_back(ParameterItem(m_sMagicParamString+String(m_iDeclaredVersion), (bool) false,   -1, pdOutput            ));
  }
  DMPCATCHTHROW("DMPADOStoredProc::DMPADOStoredProc ("+sName+")")
}

DMPADOStoredProc::~DMPADOStoredProc()
{
  DMPTRY {
    if (m_adoSP) {
      if (m_adoSP->Active)
        m_adoSP->Close();
      delete m_adoSP;
    }
  }
  DMPCATCHTHROW("DMPADOStoredProc::~DMPADOStoredProc")
}

void DMPADOStoredProc::ExecProc()
{
  DMPTRY {
    if (!m_bProcNameGood) {
      m_bNewFormalism = DoProcNameCheck(m_adoSP->ProcedureName+";1", m_iDeclaredVersion);
      m_bProcNameGood = true;
    }

    DMPException* dmpEx = new DMPException();
    int iCntr = 0;
    while (iCntr < m_iRetryCount) {
      DMPTRY {
        ApplyParameters();
        DMPTRY {
          m_adoSP->Prepared = true;
        }
        DMPCATCHTHROW("Task: Set component's 'Prepared' flag")
        DMPTRY {
          m_adoSP->ExecProc();
        }
        DMPCATCHTHROW("Task: Call 'TADOStoredProc::ExecProc()'")
      }
      DMPCATCH_GET("Task: Administer repeatable group of commands", *dmpEx)
      iCntr++;
      if (dmpEx->BaseMessage.Trim().UpperCase().Pos("TIMEOUT EXPIRED") || dmpEx->BaseMessage.Trim().UpperCase().Pos("DEADLOCKED"))
        ::Sleep(100);
      else
        break;
    }
    if (dmpEx->Empty)
      delete dmpEx;
    else {
      String sParams = GetParameters();
      if (sParams.IsEmpty())
        sParams = "\r\nNo input Parameters required for this stored procedure";
      String sEntireError = dmpEx->BaseMessage;
      if (iCntr >= m_iRetryCount)
        sEntireError += " (" + String(iCntr) + " retries)";
      sEntireError += "\r\n" + sParams;
      delete dmpEx;
      throw DMPException(sEntireError);
    }

    CheckForSPReturnedError();
  }
  DMPCATCHTHROW("DMPADOStoredProc::ExecProc ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::Open()
{
  DMPTRY {
    if (!m_bProcNameGood) {
      m_bNewFormalism = DoProcNameCheck(m_adoSP->ProcedureName+";1", m_iDeclaredVersion);
      m_bProcNameGood = true;
    }

    DMPException* dmpEx = new DMPException();
    int iCntr = 0;
    while (iCntr < m_iRetryCount) {
      DMPTRY {
        ApplyParameters();
        DMPTRY {
          m_adoSP->Prepared = true;
        }
        DMPCATCHTHROW("Task: Set component's 'Prepared' flag")
        DMPTRY {
          m_adoSP->Open();
        }
        DMPCATCHTHROW("Task: Call 'TADOStoredProc::Open()'")
      }
      DMPCATCH_GET("Task: Administer repeatable group of commands", *dmpEx)
      iCntr++;
      if (dmpEx->BaseMessage.Trim().UpperCase().Pos("TIMEOUT EXPIRED") || dmpEx->BaseMessage.Trim().UpperCase().Pos("DEADLOCKED"))
        ::Sleep(100);
      else
        break;
    }
    if (dmpEx->Empty)
      delete dmpEx;
    else {
      String sParams = GetParameters();
      if (sParams.IsEmpty())
        sParams = "\r\nNo input Parameters required for this stored procedure";
      String sEntireError = dmpEx->BaseMessage;
      if (iCntr >= m_iRetryCount)
        sEntireError += " (" + String(iCntr) + " retries)";
      sEntireError += "\r\n" + sParams;
      delete dmpEx;
      throw DMPException(sEntireError);
    }

    CheckForSPReturnedError();
  }
  DMPCATCHTHROW("DMPADOStoredProc::Open ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, const String& sParamValue, TParameterDirection paramDirection)
{
  DMPTRY {
    if (paramDirection != pdOutput && (sParamValue.Length() != 38 || sParamValue[1] != '{' || sParamValue[sParamValue.Length()] != '}')) // CR 14003 9/17/2005 DJK - Added 'paramDirection != pdOutput' clause so that output params can be declared without havinig to supply a valid GUID (goal is to 'GET' GUID, not supply it)
      throw DMPException("GUID parameter '"+sParamName+"' supplied a value which does not represent a valid GUID: '"+sParamValue+"'");
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(sParamValue), -1, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (GUID) ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, const String& sParamValue, int iParamLength, TParameterDirection paramDirection)
{
  DMPTRY {
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(sParamValue), iParamLength, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (String) ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, int iParamValue, TParameterDirection paramDirection)
{
  DMPTRY {
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(iParamValue), -1, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (int) ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, const TDateTime& dtParamValue, TParameterDirection paramDirection)
{
  DMPTRY {
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(dtParamValue), -1, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (TDateTime) ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, bool bParamValue, TParameterDirection paramDirection)
{
  DMPTRY {
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(bParamValue), -1, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (bool) ("+m_adoSP->ProcedureName+")")
}

void DMPADOStoredProc::AddParameter(const String& sParamName, double dParamValue, TParameterDirection paramDirection)
{
  DMPTRY {
    m_vParameterItems.insert(m_vParameterItems.end()-4, ParameterItem(sParamName, MyVariant1(dParamValue), -1, paramDirection));
  }
  DMPCATCHTHROW("DMPADOStoredProc::AddParameter (double) ("+m_adoSP->ProcedureName+")")
}

MyVariant2 DMPADOStoredProc::GetParamByName(const String& sParamName) const
{
  MyVariant2 vrntResult;
  DMPTRY {
    vrntResult = m_adoSP->Parameters->ParamByName(sParamName)->Value;
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParamByName")
  return vrntResult;
}

TField* DMPADOStoredProc::FieldByName(const String& sFieldName)
{
  TField* fldResult = 0;
  DMPTRY {
    fldResult = m_adoSP->FieldByName(sFieldName);
  }
  DMPCATCHTHROW("DMPADOStoredProc::FieldByName")
  return fldResult;
}

void DMPADOStoredProc::First()
{
  DMPTRY {
    m_adoSP->First();
  }
  DMPCATCHTHROW("DMPADOStoredProc::First")
}

void DMPADOStoredProc::Next()
{
  DMPTRY {
    m_adoSP->Next();
  }
  DMPCATCHTHROW("DMPADOStoredProc::Next")
}

void DMPADOStoredProc::GetFieldNames(std::vector<String>& vList) const
{
  DMPException* dmpEx = new DMPException();
  TStringList* fieldList = 0;
  DMPTRY {
    vList.clear();
    fieldList = new TStringList();
    m_adoSP->GetFieldNames(fieldList);
    for (int i = 0; i < fieldList->Count; ++i)
      vList.push_back(fieldList->Strings[i]);
    delete fieldList;
    fieldList = 0;
  }
  DMPCATCH_GET("DMPADOStoredProc::GetFieldNames", *dmpEx)
  if (dmpEx->Empty)
    delete dmpEx;
  else {
    try {
      if (fieldList)
        delete fieldList;
    } catch (...) {}
    throw *dmpEx;
  }
}

TField* DMPADOStoredProc::GetField(int Index)
{
  TField* fldResult = 0;
  DMPTRY {
    fldResult = m_adoSP->Fields->Fields[Index];
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetField")
  return fldResult;
}

bool DMPADOStoredProc::GetEof() const
{
  bool bResult;
  DMPTRY {
    bResult = m_adoSP->Eof;
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetEof()")
  return bResult;
}

int DMPADOStoredProc::GetRecordCount() const
{
  int iResult;
  DMPTRY {
    iResult = m_adoSP->RecordCount;
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetRecordCount")
  return iResult;
}

int DMPADOStoredProc::GetFieldCount() const
{
  int iResult;
  DMPTRY {
    iResult = m_adoSP->FieldCount;
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetFieldCount")
  return iResult;
}

void DMPADOStoredProc::ApplyParameters()
{
  DMPTRY {
    m_adoSP->Parameters->Clear();
    for (std::vector<ParameterItem>::const_iterator itt = m_vParameterItems.begin(); itt < m_vParameterItems.end(); ++itt)
      if (!m_bNewFormalism || itt->GetParameterNewFormalism()) {
        TParameter* paramPtr = m_adoSP->Parameters->AddParameter();
        paramPtr->Name      = itt->GetParameterName();
        paramPtr->Direction = itt->GetParameterDirection();
        switch (itt->GetParameterValueType()) {
          case MyVariant1::mvtString  : if (itt->GetParameterLength() == -1) {
                                          paramPtr->DataType = ftGuid;
                                          paramPtr->Value    = (String) itt->GetParameterValue();
                                        }
                                        else {
                                          paramPtr->DataType = ftString;
                                          paramPtr->Size     = itt->GetParameterLength();
                                          paramPtr->Value    = (String) itt->GetParameterValue();
                                        }
                                        break;
          case MyVariant1::mvtInt     : paramPtr->DataType = ftInteger;
                                        paramPtr->Value    = (int) itt->GetParameterValue();
                                        break;
          case MyVariant1::mvtDateTime: paramPtr->DataType = ftDateTime;
                                        paramPtr->Value    = (TDateTime) itt->GetParameterValue();
                                        break;
          case MyVariant1::mvtBool    : paramPtr->DataType = ftBoolean;
                                        paramPtr->Value    = (bool) itt->GetParameterValue();
                                        break;
          case MyVariant1::mvtDouble  : paramPtr->DataType = ftFloat;
                                        paramPtr->Value    = (double) itt->GetParameterValue();
                                        break;
          default: throw DMPException("Undefined value type");
        }
      }
  }
  DMPCATCHTHROW("DMPADOStoredProc::ApplyParameters")
}

void DMPADOStoredProc::CheckForSPReturnedError()
{
  DMPTRY {
    if (0 != (int) m_adoSP->Parameters->Items[0]->Value) {
      int    iParmsCount       = m_adoSP->Parameters->Count;
      int    iReturnedRowCount = m_adoSP->Parameters->Items[iParmsCount-4]->Value;
      String sErrorMessage     = m_adoSP->Parameters->Items[iParmsCount-3]->Value;
      int    iErrorNumber      = m_adoSP->Parameters->Items[iParmsCount-2]->Value;
      String sError;
      sError.sprintf("An Error Occurred in the Stored Procedure: %s\r\n"
                     "\tRows returned    : %d\r\n"
                     "\tError Description: %s\r\n"
                     "\tSQL error number : %d\r\n"
                     "%s",
                     String(m_adoSP->ProcedureName).c_str(),
                     iReturnedRowCount,
                     sErrorMessage.c_str(),
                     iErrorNumber,
                     GetParameters(false).c_str()
                    );
      throw DMPException(sError);
    }
  }
  DMPCATCHTHROW("DMPADOStoredProc::CheckForSPReturnedError")
}

String DMPADOStoredProc::GetParameters(bool bInputsOnly)
{
  String sResult;
  DMPTRY {
    std::ostrstream* pOstr = new std::ostrstream();
    for (int iCurr = 1; iCurr < m_adoSP->Parameters->Count; ++iCurr)
      if (!bInputsOnly || m_adoSP->Parameters->Items[iCurr]->Direction == pdInput || m_adoSP->Parameters->Items[iCurr]->Direction == pdInputOutput) {
        *pOstr << "\r\n" << "Param " << iCurr << ": ";
        GetParmType(*pOstr, iCurr);
        *pOstr << " (";
        GetParmDirection(*pOstr, iCurr);
        *pOstr << ")  -  '";
        GetParmName(*pOstr, iCurr);
        *pOstr << "'\r\n         ";
        if (iCurr < (m_adoSP->Parameters->Count-1))
          GetParmValue(*pOstr, iCurr);
        else
          *pOstr << "Value not used";
      }
    sResult = pOstr->str();
    delete pOstr;
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParameters")
  return sResult;
}

void DMPADOStoredProc::GetParmType(std::ostrstream& stream, int iP)
{
  DMPTRY {
    switch (m_adoSP->Parameters->Items[iP]->DataType)
    {
      case ftString     : stream << "ftString"     ; break;
      case ftSmallint   : stream << "ftSmallint"   ; break;
      case ftInteger    : stream << "ftInteger"    ; break;
      case ftWord       : stream << "ftWord"       ; break;
      case ftBoolean    : stream << "ftBoolean"    ; break;
      case ftFloat      : stream << "ftFloat"      ; break;
      case ftCurrency   : stream << "ftCurrency"   ; break;
      case ftBCD        : stream << "ftBCD"        ; break;
      case ftDate       : stream << "ftDate"       ; break;
      case ftTime       : stream << "ftTime"       ; break;
      case ftDateTime   : stream << "ftDateTime"   ; break;
      case ftBytes      : stream << "ftBytes"      ; break;
      case ftVarBytes   : stream << "ftVarBytes"   ; break;
      case ftAutoInc    : stream << "ftAutoInc"    ; break;
      case ftBlob       : stream << "ftBlob"       ; break;
      case ftMemo       : stream << "ftMemo"       ; break;
      case ftGraphic    : stream << "ftGraphic"    ; break;
      case ftFmtMemo    : stream << "ftFmtMemo"    ; break;
      case ftParadoxOle : stream << "ftParadoxOle" ; break;
      case ftDBaseOle   : stream << "ftDBaseOle"   ; break;
      case ftTypedBinary: stream << "ftTypedBinary"; break;
      case ftCursor     : stream << "ftCursor"     ; break;
      case ftFixedChar  : stream << "ftFixedChar"  ; break;
      case ftWideString : stream << "ftWideString" ; break;
      case ftLargeint   : stream << "ftLargeint"   ; break;
      case ftADT        : stream << "ftADT"        ; break;
      case ftArray      : stream << "ftArray"      ; break;
      case ftReference  : stream << "ftReference"  ; break;
      case ftDataSet    : stream << "ftDataSet"    ; break;
      case ftOraBlob    : stream << "ftOraBlob"    ; break;
      case ftOraClob    : stream << "ftOraClob"    ; break;
      case ftVariant    : stream << "ftVariant"    ; break;
      case ftInterface  : stream << "ftInterface"  ; break;
      case ftIDispatch  : stream << "ftIDispatch"  ; break;
      case ftGuid       : stream << "ftGuid"       ; break;
      case ftUnknown    : stream << "ftUnknown"    ; break;
      default           : stream << "UNDEFINED"    ;
    }
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParmType")
}

void DMPADOStoredProc::GetParmDirection(std::ostrstream& stream, int iP)
{
  DMPTRY {
    switch (m_adoSP->Parameters->Items[iP]->Direction)
    {
      case pdInput:
        stream << "Input";
        break;
      case pdOutput:
        stream << "Output";
        break;
      case pdReturnValue:
        stream << "ReturnValue";
        break;
      case pdInputOutput:
        stream << "InputOutput";
        break;
      case pdUnknown:
        stream << "Unknown";
        break;
      default:
        stream << "UNDEFINED";
    }
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParmDirection")
}

void DMPADOStoredProc::GetParmName(std::ostrstream& stream, int iP)
{
  DMPTRY {
    stream << String(m_adoSP->Parameters->Items[iP]->Name).c_str();
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParmName")
}

void DMPADOStoredProc::GetParmValue(std::ostrstream& stream, int iP)
{
  DMPTRY {
    MyVariant2 variant = m_adoSP->Parameters->Items[iP]->Value;
    if (variant.IsEmpty())
      stream << "NULL";
    else
      switch (m_adoSP->Parameters->Items[iP]->DataType)
      {
        case ftString:
        case ftMemo:
        case ftFmtMemo:
        case ftFixedChar:
        case ftWideString:
        case ftGuid:
          stream << ((String) m_adoSP->Parameters->Items[iP]->Value).c_str();
          break;
        case ftSmallint:
          stream << (short) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftInteger:
        case ftAutoInc:
          stream << (int) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftWord:
          stream << (unsigned int) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftBoolean:
          stream << (bool) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftFloat:
          stream << (double) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftCurrency:
          stream << (double) (Currency) m_adoSP->Parameters->Items[iP]->Value;
          break;
        case ftDate:
        case ftTime:
        case ftDateTime:
          DoStream(stream, (TDateTime) m_adoSP->Parameters->Items[iP]->Value);
          break;
        case ftUnknown:
        case ftBCD:
        case ftBytes:
        case ftVarBytes:
        case ftBlob:
        case ftGraphic:
        case ftParadoxOle:
        case ftDBaseOle:
        case ftTypedBinary:
        case ftCursor:
        case ftLargeint:
        case ftADT:
        case ftArray:
        case ftReference:
        case ftDataSet:
        case ftOraBlob:
        case ftOraClob:
        case ftVariant:
        case ftInterface:
        case ftIDispatch:
          stream << "??";
          break;
        default:
          stream << "UNDEFINED";
      }
  }
  DMPCATCHTHROW("DMPADOStoredProc::GetParmValue")
}

std::ostrstream& DMPADOStoredProc::DoStream(std::ostrstream& stream, const TDateTime& dt)
{
  DMPTRY {
    double dVal = (double) dt;
    if (dVal < 1.0)
      stream << ((TDateTime) dt).FormatString("h:nn:ssam/pm").c_str();
    else {
      stream << ((TDateTime) dt).FormatString("m/d/yyyy").c_str();
      if (fmod(dVal, 1.0))
        stream << " " << ((TDateTime) dt).FormatString("h:nn:ssam/pm").c_str();
    }
  }
  DMPCATCHTHROW("DMPADOStoredProc::DoStream (TDateTime)")
  return stream;
}

bool DMPADOStoredProc::DoProcNameCheck(const String& sProcedureName, int iDeclaredVersion)
{
  m_criticalSection->Enter();
  bool bResult = false;
  DMPException* dmpEx = new DMPException();
  DMPTRY {
    int iWhere;
    if (!BinarySearch(m_vProcNameVers, ProcNameVersion(sProcedureName), iWhere)) // return 'true' if found, 'false' if not. 'iPos' indicates position if found, or correct insertion point if not found.
      throw DMPException("The requested stored procedure name '"+sProcedureName+"' does not exist in the database, or EXECUTE permission to the stored procedure is denied");
    if (m_vProcNameVers[iWhere].m_iVersion != iDeclaredVersion) {
      String sError;
      sError.sprintf("Stored procedure version mis-match. Looking for Ver %d, database has Ver %d\r\n"
                     "Recommendation: Obtain newer version of %s",
                     iDeclaredVersion,
                     m_vProcNameVers[iWhere].m_iVersion,
                     (iDeclaredVersion < m_vProcNameVers[iWhere].m_iVersion ? "application" : "stored procedure")
                    );
      throw DMPException(sError);
    }
    bResult = m_vProcNameVers[iWhere].m_bNew;
  }
  DMPCATCH_GET("DMPADOStoredProc::DoProcNameCheck ("+sProcedureName+")", *dmpEx)
  try {
    m_criticalSection->Leave();
  }
  catch (...) {}
  if (dmpEx->Empty)
    delete dmpEx;
  else
    throw *dmpEx;
  return bResult;
}

void DMPADOStoredProc::PopulateProcNamesList(TADOConnection* adoConnection)
{
  DMPException* dmpEx = new DMPException();
  DMPTRY {
    m_criticalSection->Enter();
    if (m_vProcNameVers.size() == 0) {
      DMPTRY {
        TStringList* slProcNames = new TStringList();
        adoConnection->GetProcedureNames(slProcNames);
        for (int i = 0; i < slProcNames->Count; ++i)
          m_vProcNameVers.push_back(ProcNameVersion(slProcNames->Strings[i]));
        delete slProcNames;
        std::sort(m_vProcNameVers.begin(), m_vProcNameVers.end());
      }
      DMPCATCHTHROW("Task: Obtain list of stored procedures from database")
      DMPTRY {
        TADOQuery* qry = new TADOQuery(0);
        qry->Connection = adoConnection;
        ProcNameVersion pnv;
        int iWhere;
        DMPTRY {
          String sSql;
          sSql.sprintf("Execute sp_sproc_columns @column_name = '%s%'", m_sMagicParamString.c_str());
          qry->SQL->Text = sSql;
          qry->Open();
          qry->First();
          String sTemp;
          std::vector<TField*> vFields;
          vFields.push_back(qry->FieldByName("Procedure_Name"));
          vFields.push_back(qry->FieldByName(   "Column_Name"));
          while (!qry->Eof) {
            pnv.m_sName = vFields[0]->AsString.Trim();
            if (BinarySearch(m_vProcNameVers, pnv, iWhere)) { // return 'true' if found, 'false' if not. 'iPos' indicates position if found, or correct insertion point if not found.
              sTemp = vFields[1]->AsString.Trim();
              m_vProcNameVers[iWhere].m_iVersion = sTemp.SubString(18, sTemp.Length()-17).ToIntDef(-1);
            }
            qry->Next();
          }
          qry->Close();
        }
        DMPCATCHTHROW("Task: Now retrieve stored proc versions, include them in list")
        DMPTRY {
          qry->SQL->Text = "Execute sp_sproc_columns @column_name = '@parmSQLErrorNumber'";
          qry->Open();
          qry->First();
          std::vector<TField*> vFields;
          vFields.push_back(qry->FieldByName("Procedure_Name"));
          while (!qry->Eof) {
            pnv.m_sName = vFields[0]->AsString.Trim();
            if (BinarySearch(m_vProcNameVers, pnv, iWhere)) // return 'true' if found, 'false' if not. 'iPos' indicates position if found, or correct insertion point if not found.
              m_vProcNameVers[iWhere].m_bNew = false;
            qry->Next();
          }
          qry->Close();
        }
        DMPCATCHTHROW("Task: Now retrieve stored proc new/old formalisms, include them in list")
        delete qry;
      }
      DMPCATCHTHROW("Task: Update list of stored procs with information from the database")
    }
  }
  DMPCATCH_GET("DMPADOStoredProc::PopulateProcNamesList", *dmpEx)
  try {
    m_criticalSection->Leave();
  } catch (...) {}
  if (dmpEx->Empty)
    delete dmpEx;
  else
    throw *dmpEx;
}

//---------------------------------------------------------------------------

