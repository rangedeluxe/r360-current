object ParameterDlg: TParameterDlg
  Left = 392
  Top = 210
  ActiveControl = ParamFileEdit
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = 'Parameters'
  ClientHeight = 256
  ClientWidth = 387
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 38
    Top = 6
    Width = 146
    Height = 13
    Caption = 'Enter location of parameter file:'
  end
  object Label3: TLabel
    Left = 94
    Top = 57
    Width = 9
    Height = 13
    Caption = 'or'
  end
  object ParamFileEdit: TEdit
    Left = 38
    Top = 30
    Width = 283
    Height = 21
    TabOrder = 0
  end
  object OKBtn: TButton
    Left = 102
    Top = 219
    Width = 75
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
  end
  object CancelBtn: TButton
    Left = 190
    Top = 219
    Width = 75
    Height = 25
    Cancel = True
    Caption = 'Cancel'
    ModalResult = 2
    TabOrder = 4
  end
  object BrowseBtn: TButton
    Left = 328
    Top = 28
    Width = 30
    Height = 23
    Caption = '...'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Arial'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 1
    OnClick = BrowseBtnClick
  end
  object GridPanel: TPanel
    Left = -4
    Top = 72
    Width = 393
    Height = 147
    BevelOuter = bvNone
    TabOrder = 2
    object Label2: TLabel
      Left = 38
      Top = 5
      Width = 141
      Height = 13
      Caption = 'Fill in parameter values below:'
    end
    object ParamGrid: TAdvStringGrid
      Left = 38
      Top = 24
      Width = 320
      Height = 113
      ColCount = 2
      DefaultColWidth = 100
      DefaultRowHeight = 18
      FixedColor = clWhite
      FixedCols = 1
      RowCount = 2
      FixedRows = 1
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goEditing]
      TabOrder = 0
      Bands.Active = True
      Bands.PrimaryColor = 16777088
      Bands.PrimaryLength = 1
      Bands.SecondaryColor = 15591610
      Bands.SecondaryLength = 1
      Bands.Print = False
      AutoNumAlign = False
      AutoSize = False
      VAlignment = vtaTop
      EnhTextSize = False
      EnhRowColMove = False
      SortFixedCols = False
      SortNormalCellsOnly = False
      SizeWithForm = False
      Multilinecells = False
      SortDirection = sdAscending
      SortFull = True
      SortAutoFormat = True
      SortShow = False
      SortIndexShow = False
      EnableGraphics = False
      EnableHTML = True
      EnableWheel = True
      Flat = False
      SortColumn = 0
      HintColor = clYellow
      SelectionColor = clHighlight
      SelectionTextColor = clHighlightText
      SelectionRectangle = False
      SelectionRTFKeep = False
      HintShowCells = False
      HintShowLargeText = False
      OleAcceptFiles = True
      OleAcceptText = True
      PrintSettings.FooterSize = 0
      PrintSettings.HeaderSize = 0
      PrintSettings.Time = ppNone
      PrintSettings.Date = ppNone
      PrintSettings.DateFormat = 'dd/mm/yyyy'
      PrintSettings.PageNr = ppNone
      PrintSettings.Title = ppNone
      PrintSettings.Font.Charset = DEFAULT_CHARSET
      PrintSettings.Font.Color = clWindowText
      PrintSettings.Font.Height = -11
      PrintSettings.Font.Name = 'MS Sans Serif'
      PrintSettings.Font.Style = []
      PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
      PrintSettings.HeaderFont.Color = clWindowText
      PrintSettings.HeaderFont.Height = -11
      PrintSettings.HeaderFont.Name = 'MS Sans Serif'
      PrintSettings.HeaderFont.Style = []
      PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
      PrintSettings.FooterFont.Color = clWindowText
      PrintSettings.FooterFont.Height = -11
      PrintSettings.FooterFont.Name = 'MS Sans Serif'
      PrintSettings.FooterFont.Style = []
      PrintSettings.Borders = pbNoborder
      PrintSettings.BorderStyle = psSolid
      PrintSettings.Centered = False
      PrintSettings.RepeatFixedRows = False
      PrintSettings.RepeatFixedCols = False
      PrintSettings.LeftSize = 0
      PrintSettings.RightSize = 0
      PrintSettings.ColumnSpacing = 0
      PrintSettings.RowSpacing = 0
      PrintSettings.TitleSpacing = 0
      PrintSettings.Orientation = poPortrait
      PrintSettings.FixedWidth = 0
      PrintSettings.FixedHeight = 0
      PrintSettings.UseFixedHeight = False
      PrintSettings.UseFixedWidth = False
      PrintSettings.FitToPage = fpNever
      PrintSettings.PageNumSep = '/'
      PrintSettings.NoAutoSize = False
      PrintSettings.PrintGraphics = False
      HTMLSettings.Width = 100
      Navigation.AllowInsertRow = False
      Navigation.AllowDeleteRow = False
      Navigation.AdvanceOnEnter = False
      Navigation.AdvanceInsert = False
      Navigation.AutoGotoWhenSorted = False
      Navigation.AutoGotoIncremental = False
      Navigation.AutoComboDropSize = False
      Navigation.AdvanceDirection = adLeftRight
      Navigation.AllowClipboardShortCuts = False
      Navigation.AllowSmartClipboard = False
      Navigation.AllowRTFClipboard = False
      Navigation.AllowClipboardAlways = False
      Navigation.AllowClipboardRowGrow = True
      Navigation.AllowClipboardColGrow = True
      Navigation.AdvanceAuto = False
      Navigation.InsertPosition = pInsertBefore
      Navigation.CursorWalkEditor = False
      Navigation.MoveRowOnSort = False
      Navigation.ImproveMaskSel = False
      Navigation.AlwaysEdit = True
      Navigation.CopyHTMLTagsToClipboard = True
      Navigation.LineFeedOnEnter = False
      ColumnSize.Save = False
      ColumnSize.Stretch = True
      ColumnSize.Location = clRegistry
      CellNode.Color = clSilver
      CellNode.NodeType = cnFlat
      CellNode.NodeColor = clBlack
      SizeWhileTyping.Height = False
      SizeWhileTyping.Width = False
      MaxEditLength = 0
      MouseActions.AllSelect = False
      MouseActions.ColSelect = False
      MouseActions.RowSelect = False
      MouseActions.DirectEdit = False
      MouseActions.DirectComboDrop = False
      MouseActions.DisjunctRowSelect = False
      MouseActions.AllColumnSize = False
      MouseActions.AllRowSize = False
      MouseActions.CaretPositioning = False
      IntelliPan = ipVertical
      URLColor = clBlue
      URLShow = False
      URLFull = False
      URLEdit = False
      ScrollType = ssNormal
      ScrollColor = clNone
      ScrollWidth = 16
      ScrollSynch = False
      ScrollProportional = False
      ScrollHints = shNone
      OemConvert = False
      FixedFooters = 0
      FixedRightCols = 0
      FixedColWidth = 100
      FixedRowHeight = 18
      FixedFont.Charset = DEFAULT_CHARSET
      FixedFont.Color = clWindowText
      FixedFont.Height = -11
      FixedFont.Name = 'MS Sans Serif'
      FixedFont.Style = [fsBold]
      FixedAsButtons = False
      FloatFormat = '%.2f'
      WordWrap = True
      ColumnHeaders.Strings = (
        'Parameter'
        'Value')
      Lookup = False
      LookupCaseSensitive = False
      LookupHistory = False
      ShowSelection = False
      BackGround.Top = 0
      BackGround.Left = 0
      BackGround.Display = bdTile
      BackGround.Cells = bcNormal
      Filter = <>
      ColWidths = (
        100
        214)
      RowHeights = (
        18
        18)
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.txt'
    Filter = 'Text Files (*.txt)|*.txt|All Files (*.*)|*.*'
    Options = [ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 310
    Top = 8
  end
end
