//---------------------------------------------------------------------------
#ifndef definesH
#define definesH
//---------------------------------------------------------------------------
#define ASC "ASC"
#define DESC "DESC"


#define UM_FILESAVE        WM_USER + 102
#define UM_EXTRACTCOMPLETE WM_USER + 103
#define UM_EXTRACTBEGUN    WM_USER + 104
#define UM_APPLRUN         WM_USER + 105

// CR 5272 1/07/2004 DJK - Added table names to go with each field name.
#define BANK_KEY "Bank.BankID"
#define CUSTOMER_KEY "Customer.BankID, Customer.CustomerID"
#define LOCKBOX_KEY "Lockbox.BankID, Lockbox.CustomerID, Lockbox.LockboxID"
#define BATCH_KEY "Batch.BankID, Batch.LockboxID, Batch.BatchID, Batch.GlobalBatchID, Batch.ProcessingDate, Batch.DepositDate, Batch.BatchTypeCode, Batch.CheckCount, Batch.StubCount"  //CR10943 5/18/2005 ALH
#define TRANSACTION_KEY "Transactions.GlobalBatchID, Transactions.TransactionID"
#define CHECK_KEY "Checks.GlobalBatchID, Checks.GlobalCheckID, Checks.TransactionID, Checks.BatchSequence, Checks.Amount, Checks.DEDataKeys, Checks.DEBillingKeys"
//CR 4969 ~ DLAQAB ~ 3-24-2004
//Added D.FileDescriptor
// CR 12104 3/30/2005 DJK - Added 'Documents.BatchSequence'
#define STUB_KEY "Stubs.GlobalBatchID, Stubs.GlobalStubID, Stubs.TransactionID, Stubs.BatchSequence, Stubs.Amount, Stubs.DEDataKeys, Stubs.DEBillingKeys, Documents.FileDescriptor, Documents.BatchSequence As DocBatchSequence"

#define DOCUMENT_KEY "Documents.GlobalBatchID, Documents.GlobalDocumentID, Documents.TransactionID, Documents.BatchSequence, Documents.FileDescriptor"

//CR 4969 ~ DLAQAB ~ 3-24-2004
//Added the line below
//#define STUBS_TABLES_N_JOINS "Documents As D INNER JOIN StubSource ON D.GlobalDocumentID = StubSource.GlobalDocumentID RIGHT OUTER JOIN StubsDataEntry INNER JOIN Stubs ON StubsDataEntry.GlobalStubID = Stubs.GlobalStubID ON StubSource.GlobalStubID = Stubs.GlobalStubID"

//CR 9070 ~ DLAQAB ~ 9-7-2004
//Changed the join on the following line.
// CR 12104 3/30/2005 DJK - Altered the join to pull the same number of rows as the old one (pre CR 4969) did, which still maintaining the features added in CR 4969 (even though I had to fix those as well)
#define STUBS_TABLES_N_JOINS "Stubs Inner Join StubsDataEntry On Stubs.GlobalStubID = StubsDataEntry.GlobalStubID Left Join StubSource On Stubs.GlobalStubID = StubSource.GlobalStubID Left Join Documents On StubSource.GlobalDocumentID = Documents.GlobalDocumentID Left Join DocumentsDataEntry ON Documents.GlobalDocumentID = DocumentsDataEntry.GlobalDocumentID"    //CR 19189 2/26/2007 ALH
//---------------------------------------------------------------------------
enum {goButton=0, goFieldName=1};
enum {goDataType=2, goFieldDesc=3, goColWidth=4, goPadChar=5, goJustify=6, goQuotes = 7, goFormat=8, goOccursGroup=9, goMaxOccurs=10};
enum {goOrder=2};
enum {goOpr=2, goValue=3, goLeftParen=4, goRightParen=5, goRecOpr=6};
enum {rtGeneral, rtFile, rtBank, rtCustomer, rtLockbox, rtBatch, rtTransactions, rtChecks, rtStubs, rtDocs, rtDetail};
enum {rtLayoutCreate, rtOrder, rtLimit};

#endif
 
