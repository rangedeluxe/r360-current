//---------------------------------------------------------------------------

#ifndef recordH
#define recordH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <DBGrids.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <Mask.hpp>
#include "CSPIN.h"
#include "wwdbedit.hpp"
#include "Wwdbspin.hpp"
#include <Dialogs.hpp>
#include "AdvGrid.hpp"
#include <Buttons.hpp>
#include <Menus.hpp>
#include "CodeMapper.h"
#include "CustomField.h"
#include "ImageFileNamingClasses.h" // CR 5815 03/03/2004 DJK

//---------------------------------------------------------------------------
class TRecordForm : public TForm
{
__published:	// IDE-managed Components
        TTabControl *TabControl1;
        TPageControl *PageControl1;
        TTabSheet *GeneralTab;
        TLabel *Label8;
        TLabel *Label9;
        TLabel *Label7;
        TLabel *Label17;
        TLabel *Label10;
        TTreeView *RecordTree;
        TEdit *ExtractPathEdit;
        TButton *BrowseBtn;
        TComboBox *FieldDelimCombo;
        TComboBox *RecordDelimCombo;
        TCheckBox *QuotesCB;
        TComboBox *BatchTraceCombo;
        TButton *DeleteRecordBtn;
        TButton *ClearBtn;
        TEdit *ImagePathEdit;
        TButton *ImagePathBtn;
        TTabSheet *FileHeaderTab;
        TPanel *Panel2;
        TTabSheet *RecordTab;
        TOpenDialog *ExtractPathOpenDialog;
        TPanel *FieldsPanel;
        TLabel *lblDataFields;
        TListBox *FieldLB;
        TPanel *SelectionPanel;
        TPageControl *RecordPageControl;
        TTabSheet *LimitTab;
        TLabel *TypeLbl;
        TAdvStringGrid *LimitGrid;
        TTabSheet *OrderTab;
        TAdvStringGrid *OrderGrid;
        TTabSheet *ExtractTab;
        TLabel *Label27;
        TLabel *Label24;
        TLabel *Label22;
        TAdvStringGrid *FieldGrid;
        TRadioGroup *HeaderFooterRG;
        TButton *TestExtractBtn;
        TButton *RestoreBtn;
        TButton *NewLayoutBtn;
        TListBox *AggregateFieldLB;
        TButton *AddDataBtn;
        TLabel *Label3;
        TListBox *StdFieldLB;
        TBevel *Bevel1;
        TButton *AddAggregateBtn;
        TButton *AddStdBtn;
        TBevel *Bevel2;
        TButton *RemoveBtn;
        TBitBtn *FieldUpBtn;
        TBitBtn *FieldDownBtn;
        TButton *LimitRemoveBtn;
        TLabel *Label21;
        TButton *OrderRemoveBtn;
        TBitBtn *UpBtn;
        TBitBtn *DownBtn;
        TLabel *Label28;
        TLabel *Label26;
        TLabel *Label29;
        TLabel *Label30;
        TButton *RestoreFieldBtn;
        TButton *DeleteLayoutBtn;
        TComboBox *LayoutNameCombo;
        TLabel *Label25;
        TMemo *WhereMemo;
        TButton *ViewWhereBtn;
        TButton *TestWhereBtn;
        TBitBtn *LimitUpBtn;
        TBitBtn *LimitDownBtn;
        TLabel *Label23;
        TStatusBar *HintBar;
        TLabel *Label20;
        TLabel *Label5;
        TLabel *Label13;
        TComboBox *FileLayoutNameCombo;
        TRadioGroup *RecordTypeRG;
        TButton *FileNewBtn;
        TButton *FileTestBtn;
        TButton *FileDeleteBtn;
        TButton *FileRestoreBtn;
        TLabel *Label2;
        TAdvStringGrid *FileGrid;
        TLabel *Label11;
        TListBox *FileStdLB;
        TButton *FileAddBtn;
        TLabel *Label18;
        TButton *FileRemoveBtn;
        TBitBtn *FileUpBtn;
        TBitBtn *FileDownBtn;
        TButton *FileRestoreFieldBtn;
        TBevel *Bevel3;
        TButton *AddBatchTraceBtn;
        TCheckBox *NoPadCB;
        TLabel *NoteLbl;
        TLabel *Label1;
        TEdit *LogPathEdit;
        TButton *LogBrowseBtn;
        TBevel *Bevel4;
        TListBox *FileAggLB;
        TButton *FileAddAggBtn;
        TLabel *Label19;
        TLabel *DisplayLevelLbl;
        TComboBox *DisplayLevelCombo;
        TCheckBox *NullAsSpaceCB;
        TSplitter *Splitter1;
        TEdit *RepeatLimit;
        TLabel *MaxRepeatsLabel;
  TGroupBox *ImageFileFormatsGB;
  TRadioGroup *ImageFileFormatRadioGroup;
  TCheckBox *CombineImagesAcrossProcDatesCheckBox;
  TMemo *Memo1;
  TEdit *eReplaceableImageFileNameSingle;
  TEdit *eReplaceableImageFileNamePerTran;
  TEdit *eReplaceableImageFileNamePerBatch;
  TLabel *Label31;
  TLabel *Label32;
  TLabel *Label33;
  TButton *btnEditImageFileNames;
        // CR 8798 7/16/04 MLT - added IgnoreOccurs CheckBox to the form
        TCheckBox *IgnoreOccurs;
        TCheckBox *UsePostDLLCheckBOX;
        TEdit *PostDLLPathFileNameEdit;
        TButton *DLLBrowsePathFileNameButton;
        TLabel *PostDLLLabel;
        TComboBox *comboBoxJointDetailLevel;
        void __fastcall BrowseBtnClick(TObject *Sender);
        void __fastcall ImagePathBtnClick(TObject *Sender);
        void __fastcall DeleteRecordBtnClick(TObject *Sender);
        void __fastcall ClearBtnClick(TObject *Sender);
        void __fastcall FormCreate(TObject *Sender);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall RecordTreeDblClick(TObject *Sender);
        void __fastcall RecordTreeDragDrop(TObject *Sender, TObject *Source, int X, int Y);
        void __fastcall RecordTreeDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
        void __fastcall RecordTreeEditing(TObject *Sender, TTreeNode *Node, bool &AllowEdit);
        void __fastcall TabControl1Change(TObject *Sender);
        void __fastcall TabControl1Changing(TObject *Sender, bool &AllowChange);
        void __fastcall AddDataBtnClick(TObject *Sender);
        void __fastcall RemoveBtnClick(TObject *Sender);
        void __fastcall GridCanEditCell(TObject *Sender, int aRow, int aCol, bool &canedit);
        void __fastcall FieldLBDblClick(TObject *Sender);
        void __fastcall AggregateFieldLBDblClick(TObject *Sender);
        void __fastcall StdFieldLBDblClick(TObject *Sender);
        void __fastcall GridDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept);
        void __fastcall GridDragDrop(TObject *Sender, TObject *Source, int X, int Y);
        void __fastcall UpBtnClick(TObject *Sender);
        void __fastcall DownBtnClick(TObject *Sender);
        void __fastcall GridClickCell(TObject *Sender, int arow, int acol);
        void __fastcall GridGetEditorType(TObject *Sender, int aCol, int aRow, TEditorType &aEditor);
        void __fastcall GridDblClickCell(TObject *Sender, int aRow, int aCol);
        void __fastcall FieldGridCellValidate(TObject *Sender, int Col, int Row, String &Value, bool &Valid);
        void __fastcall RestoreFieldBtnClick(TObject *Sender);
        void __fastcall NewLayoutBtnClick(TObject *Sender);
        void __fastcall TestExtractBtnClick(TObject *Sender);
        void __fastcall RestoreBtnClick(TObject *Sender);
        void __fastcall FieldGridEllipsClick(TObject *Sender, int aCol, int aRow, String &S);
        void __fastcall HeaderFooterRGClick(TObject *Sender);
        void __fastcall LayoutNameComboChange(TObject *Sender);
        void __fastcall RecordTreeEdited(TObject *Sender, TTreeNode *Node, String &S);
        void __fastcall DeleteLayoutBtnClick(TObject *Sender);
        void __fastcall ListBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
        void __fastcall LimitGridCellValidate(TObject *Sender, int Col, int Row, String &Value, bool &Valid);
        void __fastcall ViewWhereBtnClick(TObject *Sender);
        void __fastcall TestWhereBtnClick(TObject *Sender);
        void __fastcall LimitGridCellChanging(TObject *Sender, int OldRow, int OldCol, int NewRow, int NewCol, bool &Allow);
        void __fastcall FileStdLBDblClick(TObject *Sender);
        void __fastcall ImagePathEditExit(TObject *Sender);
        void __fastcall AddBatchTraceBtnClick(TObject *Sender);
        void __fastcall RecordTreeCustomDrawItem(TCustomTreeView *Sender, TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw);
        void __fastcall LimitGridKeyPress(TObject *Sender, char &Key);
        void __fastcall FileAggLBDblClick(TObject *Sender);
        void __fastcall FieldGridKeyPress(TObject *Sender, char &Key);
        void __fastcall DisplayLevelComboChange(TObject *Sender);
        void __fastcall DisplayLevelComboDropDown(TObject *Sender);
        void __fastcall RepeatLimitExit(TObject *Sender);
  void __fastcall btnEditImageFileNamesClick(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall ImageFileFormatRadioGroupClick(TObject *Sender);
  void __fastcall CombineImagesAcrossProcDatesCheckBoxClick(
          TObject *Sender);
        // CR 8798 7/16/04 MLT - added the following two functions
        void __fastcall IgnoreOccursClick(TObject *Sender);
        void __fastcall RepeatLimitChange(TObject *Sender);
        void __fastcall DocumentChanged(TObject *Sender);       // CR 7411 ~ DLAQAB ~ 3-25-04
        void __fastcall UsePostDLLCheckBOXClick(TObject *Sender);
        void __fastcall DLLBrowsePathFileNameButtonClick(TObject *Sender);
        void __fastcall comboBoxJointDetailLevelChange(TObject *Sender);
private:	// User declarations
        TAggregateList AggregateFieldList;
        TCodeMapper CodeMapper;
        String CurrentRecord;
        String CurrentTable;
        TStringList *FieldAttribList;
        int LastDisplayLevel;
        TInputList Parameters;
        String PicsPath;
        TStringList *Records;
        TRecordCountList RecordCounts;
        String SetupFile;
        TStdFieldList StdFields;
        String TempFile;
        String m_sImageFileNameSingle;                 // CR 5815 03/03/2004 DJK
        String m_sImageFileNamePerTran;                // CR 5815 03/03/2004 DJK
        String m_sImageFileNamePerBatch;               // CR 5815 03/03/2004 DJK
        String m_sImageFileProcDateFormat;             // CR 5815 03/03/2004 DJK
        TTokenEdit* m_teImageFileNameDisplaySingle;    // CR 5815 03/03/2004 DJK
        TTokenEdit* m_teImageFileNameDisplayPerTran;   // CR 5815 03/03/2004 DJK
        TTokenEdit* m_teImageFileNameDisplayPerBatch;  // CR 5815 03/03/2004 DJK
        bool m_bImageFileZeroPad;                      // CR 5815 03/03/2004 DJK
        bool m_bImageFileBatchTypeFormatFull;          // CR 5815 03/03/2004 DJK
        bool m_bEmbedImageInDataFile;                  // CR9916 10/13/2004 ALH

        bool m_bDocumentHasChanged;                    // CR 7411 ~ DLAQAB ~ 3-25-04

        void TestExtract(const String& SetupFile, const String& Record, String& errStr); //CR20894 MJH 4-03-07
        int  m_iLastComboBoxJointDetailLevel;
        void __fastcall AddNameToGrid(TStringList *Selected, TAdvStringGrid *Grid);
        void __fastcall AddDataToGrid(TStringList *Selected, TAdvStringGrid *Grid);
        // CR 8798 7/16/04 MLT - added UsingOccurs parameter
        void __fastcall SetupForOccurs(bool CanHaveOccurs, bool UsingOccurs);
        bool __fastcall CheckDetailAdd();
        bool __fastcall CheckDisplayLevel(int NewIndex);
        void __fastcall ClearRecordTree();
        void __fastcall DeleteLayout(String RecordName);
        void __fastcall DisplayHint(TObject *Sender);
        void __fastcall DoError(String Error);
        void __fastcall DoMessage(String Message);
        void __fastcall EnableGroupBox(TGroupBox *GB, bool IsEnabled);
        void __fastcall FillBatchTraceCombo();
        void __fastcall FillGrid(String Section, TAdvStringGrid *Grid);
        void __fastcall FillOperatorCombo(bool AllOps);
        void __fastcall GetFirstRecord(int RecType);
        String __fastcall GetHint(String Table, String FieldName);
        int  __fastcall GetMinDetailType();
        bool __fastcall GetParameters(String &Text);
        String __fastcall GetWhereClause();
        void __fastcall LoadAttributes(String Field, String &Data);
        void __fastcall LoadFileData();
        void __fastcall LoadGeneralData();
        void __fastcall LoadLimits();
        void __fastcall LoadOrder();
        void __fastcall LoadRecord();
        void __fastcall LoadTableData(int RecType, bool bDetail);
        void __fastcall RemoveQuotes(TAdvStringGrid *Grid, int Col, int Row);
        void __fastcall SetFont(String FontName, int FontSize);
        void __fastcall UnhighlightEdit(int Type);
        void __fastcall WriteCurrentRecord(int Mode);
        void __fastcall WriteGeneralData();
        void __fastcall WriteGridData(String Section, TAdvStringGrid *Grid);
        void __fastcall WriteLimits();
        void __fastcall WriteOrder();
        void __fastcall WriteRecordList();
        void RedrawImageFileNames(); // CR 5815 03/03/2004 DJK
        void ShowDetailComboBox(const bool bShow);
        void DetermineVisibilityOfAggregates();
        int  GetRecordType(const String& sRecordName) const; // CR 14584 06/01/2006 DJK
        void __fastcall PostDLLEnableCheck();
public:		// User declarations
        __fastcall TRecordForm(TComponent* Owner);
        String __fastcall GetSetupFile();
        void __fastcall New();
        void __fastcall Open(String FileName);
        void __fastcall Print();
        bool __fastcall Save();
        void __fastcall SaveAs(String FileName);
        bool __fastcall EvaluateScriptForErrors(const String& TempFile);
        int  __fastcall SaveChangesIfAny(AnsiString strCustomMessage = NULL, bool bShowCancel = false);
        void __fastcall ChangeToGeneralTab(){PageControl1->ActivePage = GeneralTab; LoadGeneralData(); LoadFileData(); LoadRecord();}  //CR 17370 02/09/2007 ALH
        void SetupReportPreview(); // CR 14584 06/01/2006 DJK
        void SetupReportPrint();   // CR 14584 06/01/2006 DJK
        __property String      ScriptFile                   = {read=SetupFile                       }; // CR 14584 06/01/2006 DJK
        __property String      TempScriptFile               = {read=TempFile                        }; // CR 14584 07/07/2006 DJK
        __property TTokenEdit* ImageFileNameDisplaySingle   = {read=m_teImageFileNameDisplaySingle  }; // CR 14584 06/01/2006 DJK
        __property TTokenEdit* ImageFileNameDisplayPerTran  = {read=m_teImageFileNameDisplayPerTran }; // CR 14584 06/01/2006 DJK
        __property TTokenEdit* ImageFileNameDisplayPerBatch = {read=m_teImageFileNameDisplayPerBatch}; // CR 14584 06/01/2006 DJK
        __property int         RecordType[String Index]     = {read=GetRecordType                   }; // CR 14584 06/01/2006 DJK
};
//---------------------------------------------------------------------------
extern PACKAGE TRecordForm *RecordForm;
//---------------------------------------------------------------------------
#endif
