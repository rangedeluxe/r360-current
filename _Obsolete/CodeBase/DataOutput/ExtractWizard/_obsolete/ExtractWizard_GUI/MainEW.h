/******************************************************************************/
/**********                                                          **********/
/******       DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.        ******/
/* Copyright Data Management Products, Inc. ("DMP") 2001.  All Rights       */
/* Reserved.  These materials are unpublished confidential and proprietary    */
/* information of DMP and contain DMP trade secrets.  These materials may not */
/* be used, copied, modified or disclosed except as expressly permitted in    */
/* writing by DMP (see the DMP license agreement for details).  All copies,   */
/* modifications and derivative works of these materials are property of DMP. */
/*                                                                            */
 /******************************************************************************/
//---------------------------------------------------------------------------
#ifndef MainH
#define MainH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Menus.hpp>
#include <Dialogs.hpp>
#include "DMod.h"
#include "defines.h"
#include "WizardSecurity.h"
//---------------------------------------------------------------------------
class TMainFormEW : public TForm
{
__published:	// IDE-managed Components
        TMainMenu *MainMenu1;
        TMenuItem *File1;
        TMenuItem *Exit1;
        TMenuItem *N1;
        TMenuItem *PrintSetup1;
    TMenuItem *PrintCxsFile;
        TMenuItem *N2;
        TMenuItem *SaveAs1;
        TMenuItem *Save1;
        TMenuItem *Open1;
        TMenuItem *New1;
        TMenuItem *Help1;
        TMenuItem *About1;
        TMenuItem *Run1;
        TOpenDialog *OpenDialog;
        TSaveDialog *SaveDialog;
        TPrintDialog *PrintDialog;
        TPrinterSetupDialog *PrinterSetupDialog;
        TMenuItem *View1;
        TMenuItem *Setup1;
        TMenuItem *Results1;
        TMenuItem *LogFile1;
        TMenuItem *More1;
        TMenuItem *License1;
        TMenuItem *PrintSetupReport;
    TMenuItem *SetupReportPreview;
    TMenuItem *SetupReportPrint;
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall FormDestroy(TObject *Sender);
        void __fastcall New1Click(TObject *Sender);
        void __fastcall Open1Click(TObject *Sender);
        void __fastcall Save1Click(TObject *Sender);
        void __fastcall SaveAs1Click(TObject *Sender);
        void __fastcall PrintCxsFileClick(TObject *Sender);
        void __fastcall PrintSetup1Click(TObject *Sender);
        void __fastcall Exit1Click(TObject *Sender);
        void __fastcall About1Click(TObject *Sender);
        void __fastcall Run1Click(TObject *Sender);
        void __fastcall Setup1Click(TObject *Sender);
        void __fastcall Results1Click(TObject *Sender);
        void __fastcall LogFile1Click(TObject *Sender);
        void __fastcall More1Click(TObject *Sender);
        void __fastcall OpenDialogCanClose(TObject *Sender,
          bool &CanClose);
        void __fastcall SaveDialogCanClose(TObject *Sender,
          bool &CanClose);
        void __fastcall License1Click(TObject *Sender);
    void __fastcall SetupReportPreviewClick(TObject *Sender);
    void __fastcall SetupReportPrintClick(TObject *Sender);
private:	// User declarations
        int MaxExtracts;
        String Module;
        String OprID;
        TWizardSecurity Security;
        String SetupPath;
        String System;
        int WKID;
        void __fastcall GlobalExceptionHandler(System::TObject* Sender, Sysutils::Exception* E);
        void __fastcall ChangeForm(TForm *NewForm);
        void __fastcall DoError(String Error);
        void __fastcall EnableMenuItems(TForm *NewForm);
        bool __fastcall GetSetupPath();
        void __fastcall LoadResults(String ResultFile, String SetupFile);
        void __fastcall LoadSetup(String FileName);
        void __fastcall OnExtractBegun(TMessage Msg);
        void __fastcall OnExtractComplete(TMessage Msg);
        void __fastcall OnFileSave(TMessage Msg);
        bool __fastcall OpenDatabase();
        void __fastcall LaunchExtractRun(const String& SetupFile);  //CR20894 MJH 5-1-07
public:		// User declarations
        __fastcall TMainFormEW(TComponent* Owner);
        __property String OperatorID  = {read=OprID}; // CR 14585 06/01/2006 DJK - Audit whenever script saved
        __property int    WorkgroupID = {read=WKID }; // CR 14585 06/01/2006 DJK - Audit whenever script saved
BEGIN_MESSAGE_MAP
  MESSAGE_HANDLER(UM_FILESAVE, TMessage, OnFileSave)
  MESSAGE_HANDLER(UM_EXTRACTBEGUN, TMessage, OnExtractBegun);
  MESSAGE_HANDLER(UM_EXTRACTCOMPLETE, TMessage, OnExtractComplete);
END_MESSAGE_MAP(TForm)
};
//---------------------------------------------------------------------------
extern PACKAGE TMainFormEW *MainFormEW;
//---------------------------------------------------------------------------
#endif
