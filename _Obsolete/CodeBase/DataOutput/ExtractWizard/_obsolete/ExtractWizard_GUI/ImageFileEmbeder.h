// New class - ImageFileEmbeder embeds image data into the extract file if the
// operator (extract writer) so desires.
// CR 9916 10/18/2004 ALH   Class by Aaron L. Hall
#ifndef ImageFileEmbederH
#define ImageFileEmbederH

#include <vcl.h>
#include <vector>

#include "DMPExceptions.h"
#include "HelperClasses.h"
#include "LogHelper.h"

// CR9916 11/15/2004 ALH - new storage class for keeping tracking of image field information
class ImageFieldInfo
{
private:
  String  m_sImageFileName;
  long    m_lColumnSize;
  char    m_cJustifyChar;
  String  m_sPadChar;
public:
  ImageFieldInfo();
  ImageFieldInfo(const ImageFieldInfo &obj);
  ~ImageFieldInfo();
  ImageFieldInfo& operator=(const ImageFieldInfo &obj);
  __property String ImageFileName   = {read=m_sImageFileName,   write=m_sImageFileName};
  __property long ColumnSize        = {read=m_lColumnSize   ,   write=m_lColumnSize};
  __property char JustifyChar       = {read=m_cJustifyChar  ,   write=m_cJustifyChar};
  __property String  PadChar        = {read=m_sPadChar      ,   write=m_sPadChar};
};

class ImageList
{
private:
        std::vector<ImageFieldInfo> m_vIMGlist;
public:
        ImageList();
        ImageList(ImageList *obj);
        ~ImageList();
        void TakeOutTheTrash();
        void AddImage(ImageFieldInfo img);
        ImageFieldInfo GetImageInfo(int iIndex);
        int GetListSize(){return (int)m_vIMGlist.size();};
        __property int ListSize={read = GetListSize};
};

class ImageFileEmbeder
{
 public:
        ImageFileEmbeder();
        ImageFileEmbeder(String sFileName, int lMaxRecordLength);
        ~ImageFileEmbeder();
        void SetDataFileName(String sFileName);
        void SetMaxRecordLength(long lMaxRecordLength);
        void EmbedImages(bool bDeleteImgFiles = false);
        void CleanUpImagesFiles(ImageList *vsImageFileNameList);
        void CleanUpImagesFiles();
        void SetFileNameList(ImageList *vsImageFileNameList);
 private:
        String m_sDataFileName;
        char *m_zTiffImage;
        long m_lTiffSize;
        long m_lMaxRecordLength;
        FILE *SourceFile;
        ImageList m_vsImageFileNameList;
        int m_iImageIndex;
        int m_iImageSizeIndex;
        bool m_bImbededImageData;
        AppLogging  *theLog;
};

#endif
