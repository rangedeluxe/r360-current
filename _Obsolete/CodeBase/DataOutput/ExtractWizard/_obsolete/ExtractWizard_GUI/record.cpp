//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include <Printers.hpp>
#include <FileCtrl.hpp>
#include "record.h"
#include "DMod.h"
#include "dir.h"
#include "dmpdata.h"
#include "extract.h"
#include "format.h"
#include "HandleIni.h"
#include "Defines.h"
#include "ExportSentry.h"
#include "ExportAudit.h"
#include "CustomFormat.h"
#include "Params.h"
#include "MainEW.h"
#include "Script.h"
#include "Globals.h"
#include "HelperClasses.h"
#include "ImageFileFormatDlg.h" // CR 5815 03/03/2004 DJK
#include "SetupRpt.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "CSPIN"
#pragma link "wwdbedit"
#pragma link "Wwdbspin"
#pragma link "AdvGrid"
#pragma resource "*.dfm"
TRecordForm *RecordForm;
//---------------------------------------------------------------------------
__fastcall TRecordForm::TRecordForm(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    m_sImageFileProcDateFormat = "MMDDYY";                   // CR 5815 03/03/2004 DJK
    m_bImageFileZeroPad = false;                             // CR 5815 03/03/2004 DJK
    m_bImageFileBatchTypeFormatFull = true;                  // CR 5815 03/03/2004 DJK
    m_bEmbedImageInDataFile = false;                         // CR 9916 10/13/2004 ALH

    m_teImageFileNameDisplaySingle   = new TTokenEdit(this); // CR 5815 03/03/2004 DJK
    m_teImageFileNameDisplayPerTran  = new TTokenEdit(this); // CR 5815 03/03/2004 DJK
    m_teImageFileNameDisplayPerBatch = new TTokenEdit(this); // CR 5815 03/03/2004 DJK

    char buf[MAXPATH];
    //This cannot happen since Sentry will fail first
    if (!GetTempPath(sizeof(buf), buf))
      ShowMessage("Unable to create temporary setup file");
    else
      TempFile = String(buf) + "tmp.cxs";
    FieldGrid->ColWidths[goDataType] = 0;
    FileGrid->ColWidths[goDataType] = 0;
    Records = new TStringList;
    FieldAttribList = new TStringList;
    Application->OnHint = DisplayHint;
    Application->ShowHint = true;                                                              
    TIniHandler Ini;
    Ini.Execute();
    Ini.GetIniFileString("PICs.ini", "Paths", "Path", "", PicsPath);
    if (!PicsPath.IsEmpty())
      if (PicsPath[PicsPath.Length()] != '\\')
        PicsPath += "\\";
    char Opr[16] = {0};
    Globals::m_sentryDll->_RetrieveOperatorLogon(Opr);
    //Get System here
    AddBatchTraceBtn->Enabled = Globals::m_sentryDll->_Permit("CHECKBOX", "EXTRACTWIZARD", "ADD", Opr);
    PostDLLEnableCheck();

    m_bDocumentHasChanged = false; //CR 7411 ~ DLAQAB ~ 3/25/04
  }
  DMPCATCHTHROW("TRecordForm::TRecordForm")     
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::FormCreate(TObject *Sender)
{
  DMPTRY {
    FillBatchTraceCombo();
    DisplayLevelCombo->ItemIndex = DisplayLevelCombo->Items->Count - 1;
    DisplayLevelCombo->Text = "Detail";
  }
  DMPCATCHTHROW("TRecordForm::FormCreate")
}


//---------------------------------------------------------------------------
void __fastcall TRecordForm::FormDestroy(TObject *Sender)
{
  DMPTRY {
    delete Records;
    delete FieldAttribList;
    while (RecordTree->Items->Count > 0)
      RecordTree->Items->Item[0]->Delete();
    DeleteFile(TempFile);
  }
  DMPCATCHTHROW("TRecordForm::FormDestroy")
}


//---------------------------------------------------------------------------
String __fastcall TRecordForm::GetSetupFile()
{
  String sResult;
  DMPTRY {
    sResult = SetupFile;
  }
  DMPCATCHTHROW("TRecordForm::GetSetupFile")
  return sResult;
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::New()
{
  DMPTRY {
    if( ID_CANCEL == SaveChangesIfAny( NULL, true) )
    {
       return;
    }

    SetupFile = "";
    Caption = TempFile;
    if (FileExists(TempFile))
      DeleteFile(TempFile);
    Records->Clear();
    ClearBtnClick(NULL);
    TabControl1->TabIndex = 0;
    TabControl1Change(NULL);
    RecordDelimCombo->ItemIndex = 0;
    RecordDelimCombo->Text = RecordDelimCombo->Items->Strings[0];
    WhereMemo->Clear();
    m_bDocumentHasChanged = false; //CR 7411 ~ DLAQAB ~ 3/25/04
  }
  DMPCATCHTHROW("TRecordForm::New")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::SetFont(String FontName, int FontSize)
{
  DMPTRY {
    TPrinter* pPrinter = Printer();
    pPrinter->Canvas->Font->Name = FontName;
    pPrinter->Canvas->Font->Size = FontSize;
  }
  DMPCATCHTHROW("TRecordForm::SetFont")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::Print()
{
  DMPTRY {
    TStringList *tmpList = new TStringList;
    tmpList->LoadFromFile(TempFile);
    TPrinter* pPrinter = Printer();
    pPrinter->Orientation = poPortrait;
    pPrinter->Title = SetupFile.IsEmpty()?TempFile:SetupFile;
    int InchPixelsX = GetDeviceCaps(pPrinter->Handle, LOGPIXELSX);
    int InchPixelsY = GetDeviceCaps(pPrinter->Handle, LOGPIXELSY);
    TRect OutRect(InchPixelsX * 1.5, InchPixelsY/2, InchPixelsX * 8, InchPixelsY);
    //Header
    TFont* OldFont = new TFont;
    OldFont->Assign(pPrinter->Canvas->Font);
    SetFont("Arial Bold", 14);
    pPrinter->BeginDoc();
    pPrinter->Canvas->TextRect(OutRect, OutRect.Left, OutRect.Top, SetupFile);
    OutRect.Top = OutRect.Bottom;
    OutRect.Left = InchPixelsX / 2;
    OutRect.Bottom = InchPixelsY * 10;
    SetFont("Arial", 10);
    int LineHeight = pPrinter->Canvas->TextHeight('X'); // + InchPixelsY/10;
    //Body
    for (int i = 0; i < tmpList->Count; i++){
      if (OutRect.Top+ LineHeight < OutRect.Bottom - LineHeight)
        OutRect.Top += LineHeight;
      else {
        pPrinter->NewPage();
        OutRect.Top = InchPixelsX;
      }
      pPrinter->Canvas->TextRect(OutRect, OutRect.Left, OutRect.Top, tmpList->Strings[i]);
    }
    pPrinter->EndDoc();
    pPrinter->Canvas->Font->Assign(OldFont);
    delete OldFont;
    delete tmpList;
  }
  DMPCATCHTHROW("TRecordForm::Print")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::Open(String FileName)
{
  DMPTRY {
    SetupFile = FileName;
    Caption = FileName;
    Records->Clear();
    ClearBtnClick(NULL);
    WhereMemo->Clear();
    TFileStream *SavedStream = new TFileStream(SetupFile, fmOpenRead | fmShareExclusive);
    TFileStream *TempStream  = new TFileStream(TempFile , fmCreate   | fmShareDenyWrite);
    TempStream->CopyFrom(SavedStream, SavedStream->Size);
    if (TempStream)
      delete TempStream;
    if (SavedStream)
      delete SavedStream;
    TabControl1->TabIndex = 0;
    TabControl1Change(NULL);
    m_bDocumentHasChanged = false; //CR 7411 ~ DLAQAB ~ 3/25/04
  }
  DMPCATCHTHROW("TRecordForm::Open")
}


//---------------------------------------------------------------------------
bool __fastcall TRecordForm::Save()
{
  bool bResult = true;
  DMPTRY {
    if (SetupFile.IsEmpty())
      SendMessage(Application->MainForm->Handle, UM_FILESAVE, 0, 0);
    else {
      // CR 7411 ~ DLAQAB ~ 3-31-04
      // Check and make sure Extract File Location, Log File Path and
      // Image File Path TEdits have been filled out by the user.
      // If Log File Path or Image File Path is empty, it will be replaced by Extract File Location
      if( ExtractPathEdit->Text.Trim().IsEmpty() )
      {
         Application->MessageBox( "Extract File Location is required.", "Extract Wizard", MB_OK | MB_ICONSTOP );
         return( false );
      }
      if( LogPathEdit->Text.Trim().IsEmpty() )
      {
        // CR 14602 11/08/2005 - BEGIN
        String sFile = ExtractPathEdit->Text.Trim();
        int iPosDot = sFile.LastDelimiter(".");
        if (iPosDot)
          LogPathEdit->Text = sFile.SubString(1, iPosDot)+"log";
        // CR 14602 11/08/2005 - END
      }
      if( ImagePathEdit->Text.Trim().IsEmpty() )
      {
        // CR 14602 11/08/2005 - BEGIN
        String sPath = ExtractPathEdit->Text.Trim();
        int iPosSlash = sPath.LastDelimiter("\\");
        if (iPosSlash)
          ImagePathEdit->Text = sPath.SubString(1, iPosSlash-1);
        // CR 14602 11/08/2005 - END
      }
      // End CR 7411 ~ DLAQAB

      WriteCurrentRecord(TabControl1->TabIndex);
      bResult = EvaluateScriptForErrors(TempFile);
      if (bResult){
        TFileStream *TempStream = new TFileStream(TempFile , fmOpenRead | fmShareExclusive);
        TFileStream *SaveStream = new TFileStream(SetupFile, fmCreate   | fmShareDenyWrite);
        SaveStream->CopyFrom(TempStream, TempStream->Size);
        if (TempStream)
          delete TempStream;
        if (SaveStream)
          delete SaveStream; 

        // CR 14585 06/01/2006 DJK - Audit whenever script saved
        DMPTRY {
          String sDescription;
          sDescription.sprintf("Extract definition %s updated.", SetupFile.c_str());
          Globals::m_auditDll->Audit("EXTRACT DEFINITION UPDATED",
                                     sDescription.c_str(),
                                     0,
                                     -1,
                                     -1,
                                     -1,
                                     -1,
                                     0,
                                     "EXTRACTWIZARD",
                                     ((TMainFormEW*) (Application->MainForm))->OperatorID.c_str(),
                                     ((TMainFormEW*) (Application->MainForm))->WorkgroupID,
                                     0
                                    );
        }
        DMPCATCHTHROW("Task: Write an audit entry whenever script file saved")
        // CR 14585 06/01/2006 DJK - End change

        m_bDocumentHasChanged = false; //CR 7411 ~ DLAQAB ~ 3/25/04
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::Save")
  return bResult;
}

//---------------------------------------------------------------------------
//
// CR 7411 ~ DLAQAB ~ 3-26-04
//
// The following method checks to see whether ot not it needs to save the
// Extract Script. It does show a messagebox. You would get to choose
// whether or not you want the Cancel button (in addition to Yes and No buttons)
// to be displayed. You also will have a chance to give it a custom message
// (inseated on the default message) to be displayed to the user.
//
// Please note that is there is no need to change the document, ID_NO
// will be returned by default.
//
int __fastcall TRecordForm::SaveChangesIfAny(AnsiString strCustomMessage, bool bShowCancel)
{
    int nAnswer = ID_NO;

    if( true == m_bDocumentHasChanged )
    {
      AnsiString strMessage;
      int nFlags = ( bShowCancel ? MB_YESNOCANCEL | MB_ICONQUESTION : MB_YESNO | MB_ICONQUESTION );

      if( NULL == strCustomMessage )
      {
         strMessage = "Changes have been made to the current extract definition.  Save these changes?" ;
      }
      else
      {
         strMessage = strCustomMessage;
      }

      nAnswer = Application->MessageBox( strMessage.c_str(),
                                             "Extract Wizard",
                                             nFlags );
      if( ID_YES == nAnswer )
      {
        Save();
      }
    }

    return( nAnswer );
}

//---------------------------------------------------------------------------
// CR 14584 06/01/2006 DJK
void TRecordForm::SetupReportPreview()
{
  DMPTRY {
    WriteCurrentRecord(TabControl1->TabIndex);
    TSetupReport* rpt = new TSetupReport(this);
    rpt->RecordForm = this;
    rpt->PreviewModal();
    delete rpt;
  }
  DMPCATCHTHROW("TRecordForm::SetupReportPreview")
}
//---------------------------------------------------------------------------
// CR 14584 06/01/2006 DJK
void TRecordForm::SetupReportPrint()
{
  DMPTRY {
    WriteCurrentRecord(TabControl1->TabIndex);
    TSetupReport* rpt = new TSetupReport(this);
    rpt->RecordForm = this;
    rpt->PrinterSetup();
    rpt->Print();
    delete rpt;
  }
  DMPCATCHTHROW("TRecordForm::SetupReportPrint")
}
//---------------------------------------------------------------------------
void __fastcall TRecordForm::SaveAs(String FileName)
{
  DMPTRY {
    SetupFile = FileName;
    Caption = SetupFile;
    Save();
  }
  DMPCATCHTHROW("TRecordForm::SaveAs")
}

//---------------------------------------------------------------------------
bool __fastcall TRecordForm::EvaluateScriptForErrors(const String& TempFile)
{
  bool bResult = true;
  DMPTRY {
    String Error, Warning;
    TScript Script(TempFile);
    DMPTRY {
      // Test 1: For 'detail-detail' records, either 0 or 2 of occurs-related columns must be filled in
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            if ((field->GetOccursGroup() == -1) != (field->GetOccursCount() == -1))
              Error.sprintf("For the '%s' field in the '%s' layout, '%s' must have a value", field->GetFieldName(), record->GetName(), field->GetOccursGroup() == -1 ? "Occurs Group" : "Occurs Count");
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 1")
    DMPTRY {
      // Test 2: For 'detail-detail' records, only check, stub, or doc fields can have 'occurs'.
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          String FieldCategory;
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            if (field->GetOccursGroup() != -1) {
              FieldCategory = field->GetTable();
              if (FieldCategory.AnsiCompareIC("Checks"            ) != 0 &&
                  FieldCategory.AnsiCompareIC("ChecksDataEntry"   ) != 0 &&
                  FieldCategory.AnsiCompareIC("Stubs"             ) != 0 &&
                  FieldCategory.AnsiCompareIC("StubsDataEntry"    ) != 0 &&
                  FieldCategory.AnsiCompareIC("Documents"         ) != 0 &&
                  FieldCategory.AnsiCompareIC("DocumentsDataEntry") != 0
                 )
                Error.sprintf("The '%s' field in the '%s' layout cannot have an 'Occurs' value since it is not in the 'check', 'stubs' or 'docs' catetory", field->GetFieldName(), record->GetName());
            }
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 2")
    DMPTRY {
      // Test 3: For 'detail-detail' records, if a field has an occur, then all members of that given field type (check, stub, doc) must also have an occur
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          String FieldCategory;
          int ChecksCode = -1, StubsCode = -1, DocsCode = -1;
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            FieldCategory = field->GetTable();
            String ErrorName;
            int Code = field->GetOccursGroup() == -1 ? 0 : 1;
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Checks") == 0 || FieldCategory.AnsiCompareIC("ChecksDataEntry") == 0)){
              if (ChecksCode == -1)
                ChecksCode = Code;
              else
                if (ChecksCode != Code)
                  ErrorName = "checks";
            }
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Stubs") == 0 || FieldCategory.AnsiCompareIC("StubsDataEntry") == 0)){
              if (StubsCode == -1)
                StubsCode = Code;
              else
                if (StubsCode != Code)
                  ErrorName = "stubs";
            }
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Documents") == 0 || FieldCategory.AnsiCompareIC("DocumentsDataEntry") == 0)){
              if (DocsCode == -1)
                DocsCode = Code;
              else
                if (DocsCode != Code)
                  ErrorName = "document";
            }
            if (!ErrorName.IsEmpty())
              Error.sprintf("In the '%s' layout, if one field of type '%s' has an 'Occurs' value, then all '%s' fields must have the same 'Occurs' value", record->GetName(), ErrorName, ErrorName);
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 3")
    DMPTRY {
      // Test 4: For 'detail-detail' records, all members of a given field type (check, stub, or doc) must have same 'Occurs Group' value
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          String FieldCategory;
          int ChecksCode = -1, StubsCode = -1, DocsCode = -1;
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            FieldCategory = field->GetTable();
            String ErrorName;
            int Code = field->GetOccursGroup();
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Checks") == 0 || FieldCategory.AnsiCompareIC("ChecksDataEntry") == 0)){
              if (ChecksCode == -1)
                ChecksCode = Code;
              else
                if (ChecksCode != Code)
                  ErrorName = "checks";
            }
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Stubs") == 0 || FieldCategory.AnsiCompareIC("StubsDataEntry") == 0)){
              if (StubsCode == -1)
                StubsCode = Code;
              else
                if (StubsCode != Code)
                  ErrorName = "stubs";
            }
            if (ErrorName.IsEmpty() && (FieldCategory.AnsiCompareIC("Documents") == 0 || FieldCategory.AnsiCompareIC("DocumentsDataEntry") == 0)){
              if (DocsCode == -1)
                DocsCode = Code;
              else
                if (DocsCode != Code)
                  ErrorName = "document";
            }
            if (!ErrorName.IsEmpty())
              Error.sprintf("In the '%s' layout, all fields of type '%s' must have the same 'Occurs Group' value", record->GetName(), ErrorName);
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 4")
    DMPTRY {
      // Test 5: For 'detail-detail' records, 'occurs' groups of same number must be listed contiguously (unless the occurs count is '1')
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          std::vector<int> vGroupIDs;
          int LastGroupID = -1;
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            int GroupID  = field->GetOccursGroup();
            int GroupCnt = field->GetOccursCount();
            if (GroupID != LastGroupID){
              if (GroupID > -1 && GroupCnt != 1){
                // Check for this Group ID already in list
                for (int k = 0; k < (int) vGroupIDs.size() && Error.IsEmpty(); ++k)
                  if (vGroupIDs[k] == GroupID)
                    Error.sprintf("In the '%s' layout, all fields with 'Occurs Group' of %d and 'Occurs Count' greater than 1 must appear together with no gaps", record->GetName(), GroupID);
                // If not already in list, add to list
                vGroupIDs.push_back(GroupID);
              }
              LastGroupID = GroupID;
            }
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 5")
    DMPTRY {
      // Test 6: For 'detail-detail' records, 'Occurs Group' values cannot be anything other than 0, 1, or 2
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            int GroupID = field->GetOccursGroup();
            if (GroupID != -1 && GroupID != 0 && GroupID != 1 && GroupID != 2)
              Error.sprintf("For the '%s' field in the '%s' layout, the 'Occurs Group' value must be either 0, 1, or 2", field->GetFieldName(), record->GetName());
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 6")
    DMPTRY {
      // Test 7: For 'detail-detail' records, all fields in the same 'occurs' groups must also have the same 'Occurs Count' values
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          std::vector<int> vCountValues;
          vCountValues.push_back(-1);
          vCountValues.push_back(-1);
          vCountValues.push_back(-1);
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            int GroupID     = field->GetOccursGroup();
            int OccursCount = field->GetOccursCount();
            if (GroupID != -1)
              if (vCountValues[GroupID] == -1)
                vCountValues[GroupID] = OccursCount;
              else
                if (vCountValues[GroupID] != OccursCount)
                  Error.sprintf("For the '%s' layout, all fields in 'Occurs Group' %d must have the same 'Occurs Count' value", record->GetName(), GroupID);
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 7")
    DMPTRY {
      // Test 8: For 'detail-detail' records, when more than once field type (check, stub, doc) is present, than there can only be one of the field types that do not have occurs values
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        // CR 7498 7/20/04 MLT - Test 8 does not apply if IgnoreOccurs flag is set
        // (i.e., if IgnoreOccurs checkbox is checked for the script)
        if (!TRecHelper::GetIgnoreOccursFlag(record->GetType()))
        {
          if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
            String FieldCategory;
            int ChecksOccurs = -1, StubsOccurs = -1, DocsOccurs = -1;
            for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
              const TRecordFieldItem* field = &(record->GetField(j));
              FieldCategory = field->GetTable();
              bool Occurs = (field->GetOccursGroup() != -1);
              if (ChecksOccurs == -1 && (FieldCategory.AnsiCompareIC("Checks") == 0 || FieldCategory.AnsiCompareIC("ChecksDataEntry") == 0))
                ChecksOccurs = Occurs ? 1 : 0;
              if (StubsOccurs == -1 && (FieldCategory.AnsiCompareIC("Stubs") == 0 || FieldCategory.AnsiCompareIC("StubsDataEntry") == 0))
                StubsOccurs = Occurs ? 1 : 0;
              if (DocsOccurs == -1 && (FieldCategory.AnsiCompareIC("Documents") == 0 || FieldCategory.AnsiCompareIC("DocumentsDataEntry") == 0))
                DocsOccurs = Occurs ? 1 : 0;
              int NonOccursCount = 0;
              if (ChecksOccurs == 0)
                NonOccursCount++;
              if (StubsOccurs == 0)
                NonOccursCount++;
              if (DocsOccurs == 0)
                NonOccursCount++;
              if (NonOccursCount > 1)
                Error.sprintf("In the '%s' layout, only one set of field types ('check', 'stub, 'document') are allowed to NOT have 'Occurs'", record->GetName());
            }
          }
        }
      }  
    }
    DMPCATCHTHROW("Task: Test 8")
    DMPTRY {
      // Test 9: For 'detail-detail' records, the 'occurs groups'/'table names' associations must be the same accross all records
      TStringList* GroupIDTables = new TStringList();
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            int GroupID = field->GetOccursGroup();
            if (GroupID != -1){
              String GroupTable = field->GetTable();
              // Find this groupID in list:
              int FoundIdx = -1;
              for (int k = 0; k < GroupIDTables->Count; ++k)
                if ((int) (GroupIDTables->Objects[k]) == GroupID){
                  FoundIdx = k;
                  break;
                }
              if (FoundIdx != -1){
                if (GroupIDTables->Strings[FoundIdx] != GroupTable)
                  Error.sprintf("For the '%s' field in the '%s' layout, the 'Occurs Group' value %d must be applied to fields from the '%s' table", field->GetFieldName(), record->GetName(), GroupID, GroupIDTables->Strings[FoundIdx]);
              }
              else{
                GroupIDTables->Add(GroupTable);
                GroupIDTables->Objects[GroupIDTables->Count-1] = (TObject*) GroupID;
              }
              if (Error.IsEmpty()){
                FoundIdx = -1;
                for (int k = 0; k < GroupIDTables->Count; ++k)
                  if (GroupIDTables->Strings[k].AnsiCompareIC(GroupTable) == 0 && (int) (GroupIDTables->Objects[k]) != GroupID){
                    FoundIdx = k;
                    break;
                  }
                if (FoundIdx != -1)
                    Error.sprintf("For the '%s' field in the '%s' layout, the 'Occurs Group' value %d cannot be applied to fields from the '%s' table", field->GetFieldName(), record->GetName(), GroupID, GroupIDTables->Strings[FoundIdx]);
              }
              if (!Error.IsEmpty())
                Error += "\n\nThe association between 'Occurs Group' numbers and the tables they are associated with must be common accross all layouts";
            }
          }
        }
      }
      delete GroupIDTables;
    }
    DMPCATCHTHROW("Task: Test 9")
    DMPTRY {
      // Test 10: The 'Repeat Limit' value must be zero for any records that contain no occurs-related fields
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail)
          if (TRecHelper::GetRepeatLimitValue(record->GetType()) > 0){
            bool OccursArePresent = false;
            for (int j = 0; j < record->GetFieldCount() && !OccursArePresent; ++j){
              const TRecordFieldItem* field = &(record->GetField(j));
              OccursArePresent = (field->GetOccursGroup() != -1);
            }
            if (!OccursArePresent)
              Error.sprintf("In the '%s' layout, 'Repeat Limit' cannot have a value since there are no 'Occurs' fields", record->GetName());
          }
      }
    }
    DMPCATCHTHROW("Task: Test 10")
    bool HasLimit[3];
    HasLimit[0] = HasLimit[1] = HasLimit[2] = false;
    String RecordName[3];
    int RepeatValue[3];
    DMPTRY {
      // Test 11 (Warning only): Warn if the last layout to contain a field with a given 'Occurs Group' has a non-zero repeat limit (this may limit the number of records returned in the extract)
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          int RepeatLimit = TRecHelper::GetRepeatLimitValue(record->GetType());
          for (int j = 0; j < record->GetFieldCount(); ++j) {
            const TRecordFieldItem* field = &(record->GetField(j));
            int OccursID = field->GetOccursGroup();
            if (OccursID >= 0 && OccursID <= 2){
              HasLimit[OccursID] = (RepeatLimit > 0);
              RecordName[OccursID] = record->GetName();
              RepeatValue[OccursID] = RepeatLimit;
            }
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 11")
    DMPTRY {
      // Test 12: For 'detail-detail' records, records extracting images cannot contain Occurs.
      for (int i = 0; i < Script.GetRecordCount() && Error.IsEmpty(); ++i) {
        const TRecordItem* record = &(Script.GetRecord(i));
        if (TRecHelper::GetRecordSubType(record->GetType()) == rtDetail){
          bool bRecordHasOccurs = false;
          String FieldCategory;
          for (int j = 0; j < record->GetFieldCount() && Error.IsEmpty(); ++j){
            const TRecordFieldItem* field = &(record->GetField(j));
            if (field->GetOccursGroup() != -1)
              bRecordHasOccurs = true;
            FieldCategory = field->GetFieldName();
            if (FieldCategory.UpperCase().Pos("IMAGE") > 0 && bRecordHasOccurs)
              Error.sprintf("The '%s' field in the '%s' layout, is not allowed to have occurs in a record with image fields.", field->GetFieldName(), record->GetName());
          }
        }
      }
    }
    DMPCATCHTHROW("Task: Test 12")
    DMPTRY {
      int ProblemIdx = -1;
      if (HasLimit[0])
        ProblemIdx = 0;
      else
        if (HasLimit[1])
          ProblemIdx = 1;
        else
          if (HasLimit[2])
            ProblemIdx = 2;
      if (ProblemIdx > -1)
        Warning.sprintf("The last layout ('%s') to contain a field with 'Occurs Group' of %d has a repeat limit of %d\n\nThis may limit the number of records returned in the extract", RecordName[ProblemIdx], ProblemIdx, RepeatValue[ProblemIdx]);

      if (!Error.IsEmpty()){
        bResult = false;
        ShowMessage(Error);
      }
      else
        if (!Warning.IsEmpty()){
          Warning = "Warning:\n\n"+Warning;
          ShowMessage(Warning);
        }
    }
    DMPCATCHTHROW("Task: Clean up")
  }
  DMPCATCHTHROW("TRecordForm::EvaluateScriptForErrors")
  return bResult;
}

//GENERAL  CONTROLS

//---------------------------------------------------------------------------
void __fastcall TRecordForm::BrowseBtnClick(TObject *Sender)
{
  DMPTRY {
    TEdit *RecipEdit;
    ExtractPathOpenDialog->FileName = "";
    if ((TButton*)Sender == BrowseBtn){
      RecipEdit = ExtractPathEdit;
      ExtractPathOpenDialog->Filter = "CheckBOX Extract Result Files (*.cxr)|*.cxr|Text files (*.txt)|*.txt|All files (*.*)|*.*";
      ExtractPathOpenDialog->DefaultExt = "*.cxr";
    }
    else if ((TButton*)Sender == LogBrowseBtn){
      ExtractPathOpenDialog->Filter = "Log Files (*.log)|*.log|Text files (*.txt)|*.txt";
      ExtractPathOpenDialog->DefaultExt = "*.log";
      RecipEdit = LogPathEdit;
    }
    if (ExtractPathOpenDialog->Execute()){
      RecipEdit->Text = ExtractPathOpenDialog->FileName;
    }
  }
  DMPCATCHTHROW("TRecordForm::BrowseBtnClick")
}
//---------------------------------------------------------------------------
void __fastcall TRecordForm::ImagePathBtnClick(TObject *Sender)
{
  DMPTRY {
    // CR 7361 04/13/2004 DJK - Replaced the guts of this function to accomodate a new way of selecting a directory.
    String sSelection;
    SelectDirectory("Directories", "", sSelection);
    if (!sSelection.IsEmpty())
      ImagePathEdit->Text = sSelection;
    ImagePathEditExit(ImagePathEdit);
  }
  DMPCATCHTHROW("TRecordForm::ImagePathBtnClick")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::ImagePathEditExit(TObject *Sender)
{
  DMPTRY {
    String Path = ImagePathEdit->Text;
    if (!Path.IsEmpty()){
      if (Path[Path.Length()] != '\\')
        Path += "\\";
      if (Path.UpperCase()== PicsPath.UpperCase()){
        DoError("Image path must be different from image path in PICS.ini.");
        ImagePathEdit->SetFocus();
        TabControl1->TabIndex = 0;
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::ImagePathEditExit")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::AddBatchTraceBtnClick(TObject *Sender)
{
  DMPTRY {
    String FieldName = "";
    if (MessageDlg("WARNING: Fields created cannot be changed or removed "
                   "except by a Database Administrator.  Do you wish to continue?", mtConfirmation,
                   TMsgDlgButtons()<<mbYes<<mbNo, 0) == mrYes
                  ) {
      FieldName = InputBox("New Batch Trace Field", "Enter the name of the field to create", "");
      if (!FieldName.IsEmpty()){
        DataMod->DoOwnerAction("Alter Table BatchTrace Add " + FieldName + " DATETIME NULL");
        DoMessage("Field '" + FieldName + "' successfully added to BatchTrace table.");
        FillBatchTraceCombo();
        BatchTraceCombo->ItemIndex = BatchTraceCombo->Items->IndexOf(FieldName);
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::AddBatchTraceBtnClick")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::DeleteRecordBtnClick(TObject *Sender)
{
  DMPTRY {
    if (RecordTree->Selected)
      if (RecordTree->Selected->Level > 1) {
        DeleteLayout(RecordTree->Selected->Text);
        int Index = Records->IndexOfName(RecordTree->Selected->Text);
        if (Index >= 0)
          Records->Delete(Index);
        RecordTree->Selected->Delete();
      }
      else
        MessageBeep(1);
  }
  DMPCATCHTHROW("TRecordForm::DeleteRecordBtnClick")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::ClearRecordTree()
{
  DMPTRY {
    TTreeNode *CurNode = RecordTree->Items->Item[0];
    for (int i = 0; i < CurNode->Count; i++)
      CurNode->Item[i]->DeleteChildren();
  }
  DMPCATCHTHROW("TRecordForm::ClearRecordTree")
}


//---------------------------------------------------------------------------
void __fastcall TRecordForm::ClearBtnClick(TObject *Sender)
{
  DMPTRY {
    ClearRecordTree();
    TIniFile *Ini = new TIniFile(TempFile);
    for (int i = 0; i < Records->Count; i++)
      Ini->EraseSection(Records->Names[i] + "_Grid");
    delete Ini;
    Records->Clear();
  }
  DMPCATCHTHROW("TRecordForm::ClearBtnClick")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::EnableGroupBox(TGroupBox *GB, bool IsEnabled)
{
  DMPTRY {
    for (int i = 0; i < GB->ControlCount; i++)
      GB->Controls[i]->Enabled = IsEnabled;
    GB->Enabled = IsEnabled;
  }
  DMPCATCHTHROW("TRecordForm::EnableGroupBox")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::FillBatchTraceCombo()
{
  DMPTRY {
    DataMod->GetFieldListByType(BatchTraceCombo->Items, "BatchTrace", ftDateTime);
    BatchTraceCombo->Items->Add("");
  }
  DMPCATCHTHROW("TRecordForm::FillBatchTraceCombo")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeDblClick(TObject *Sender)
{
  DMPTRY {
    bool AllowChange;
    TTreeNode *node = RecordTree->Selected;
    if (node->Level > 1){
      TabControl1Changing(NULL, AllowChange);
      String Name = node->Text;
      int Type, Index = Records->IndexOfName(Name);
      if (Index >= 0){
        Type = StrToIntDef(Records->Values[Name],-1);
        Type = TRecHelper::GetRecordType(Type);
        if (Type >= rtGeneral && Type < TabControl1->Tabs->Count){
          TabControl1->TabIndex = Type;
          TabControl1Change(Sender);
          TComboBox *Combo = (Type == rtFile?FileLayoutNameCombo:LayoutNameCombo);
          Index = Combo->Items->IndexOf(Name);
          if (Index >=0){
            Combo->ItemIndex = Index;
            Combo->Text = Name;
          }
          LayoutNameComboChange(Combo);
        }
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeDblClick")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
  DMPTRY {
    if (Source == Sender) {
      TTreeNode *CurNode = RecordTree->GetNodeAt(X, Y);
      if (!CurNode)               //GW 3/6/01
        Accept = false;
      else
        Accept = (CurNode->Parent == RecordTree->Selected->Parent) && (CurNode->Level > 1);
    }
    else
      Accept = false;
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeDragOver")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeDragDrop(TObject *Sender, TObject *Source, int X, int Y)
{
  DMPTRY {
    TTreeNode *MovingNode = RecordTree->Selected;
    TTreeNode *AcceptNode = RecordTree->GetNodeAt(X,Y);
    TNodeAttachMode AttachMode = naInsert;
    if (AcceptNode) {
      if (AcceptNode->AbsoluteIndex > MovingNode->AbsoluteIndex){   //GW 3/6/01
        if (AcceptNode->AbsoluteIndex + 1 < RecordTree->Items->Count){
          if (RecordTree->Items->Item[AcceptNode->AbsoluteIndex + 1]->Level < AcceptNode->Level)
            AttachMode = naAdd;
          else
            AcceptNode = RecordTree->Items->Item[AcceptNode->AbsoluteIndex +1];
        }
        else
          AttachMode = naAdd;
      }
      MovingNode->MoveTo(AcceptNode, AttachMode);
    }
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeDragDrop")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeCustomDrawItem(TCustomTreeView *Sender, TTreeNode *Node, TCustomDrawState State, bool &DefaultDraw)
{
  DMPTRY {
    if (Node->Level > 1)
      if (Node->Selected)
        Sender->Canvas->Font->Color = clWhite;
      else
        Sender->Canvas->Font->Color = clBlue;
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeCustomDrawItem")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeEditing(TObject *Sender, TTreeNode *Node, bool &AllowEdit)
{
  DMPTRY {
    AllowEdit = Node->Level > 1;
    if (AllowEdit)
      CurrentRecord = Node->Text;
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeEditing")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RecordTreeEdited(TObject *Sender, TTreeNode *Node, String &S)
{
  DMPTRY {
    if (S != CurrentRecord){
      TIniFile *Ini = new TIniFile(TempFile);
      TStringList *Lines = new TStringList;
      Ini->ReadSectionValues(CurrentRecord + "_GRID", Lines);
      for (int i = 0; i < Lines->Count; i++)
        Ini->WriteString(S + "_Grid", Lines->Names[i],Lines->Values[Lines->Names[i]]);
      Ini->EraseSection(CurrentRecord + "_Grid");
      CurrentRecord = S;
      delete Lines;
      delete Ini;
    }
  }
  DMPCATCHTHROW("TRecordForm::RecordTreeEdited")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::TabControl1Changing(TObject *Sender, bool &AllowChange)
{
  DMPTRY {
    if (!ComponentState.Contains(csDestroying)){
      WriteCurrentRecord(TabControl1->TabIndex);
      if (TabControl1->TabIndex > rtGeneral){
        UnhighlightEdit(TabControl1->TabIndex);
        WhereMemo->Clear();
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::TabControl1Changing")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::TabControl1Change(TObject *Sender)
{
  DMPTRY {
    switch (TabControl1->TabIndex){
      case rtGeneral: PageControl1->ActivePage = GeneralTab; break;
      case rtFile:    PageControl1->ActivePage = FileHeaderTab; break;
      default:        PageControl1->ActivePage = RecordTab;
                      RecordPageControl->ActivePageIndex = rtLayoutCreate; break;
    }
    LoadRecord();
  }
  DMPCATCHTHROW("TRecordForm::TabControl1Change")
}
//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadRecord()
{
  DMPTRY {
    CurrentTable = "";
    CurrentRecord = "";   //GW 8/9/01
    switch (TabControl1->TabIndex){
      case rtGeneral: LoadGeneralData(); break;
      case rtFile:    LoadFileData(); break;
      default:       HeaderFooterRG->ItemIndex = 0;       //GW 8/21/01
                     if (TabControl1->TabIndex < rtDetail){
                        ShowDetailComboBox(false);
                        SetupForOccurs(false, false);
                        DisplayLevelLbl->Visible = false;
                        DisplayLevelCombo->Visible = false;
                        OrderTab->TabVisible = true;
                        LimitTab->TabVisible = true;
                        LoadTableData(TabControl1->TabIndex, false);
                        // CR 8798 7/19/04 MLT - added IgnoreOccurs checkbox control
                        IgnoreOccurs->Visible = false;
                      }
                      else if (TabControl1->TabIndex == rtDetail){
                        ShowDetailComboBox(true);
                        DisplayLevelLbl->Visible = true;
                        DisplayLevelCombo->Visible = true;
                        OrderTab->TabVisible = false;
                        LimitTab->TabVisible = false;
                        LoadTableData(comboBoxJointDetailLevel->ItemIndex+2, true);
                        DisplayLevelComboChange(NULL);
                      }
                      LoadLimits();        //GW 8/27/01
                      LoadOrder();
                      HeaderFooterRG->Visible = TabControl1->TabIndex <= rtTransactions||TabControl1->TabIndex == rtDetail;
                      DetermineVisibilityOfAggregates();
    }
    GetFirstRecord(TabControl1->TabIndex);
  }
  DMPCATCHTHROW("TRecordForm::LoadRecord")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::WriteCurrentRecord(int Mode)
{
  DMPTRY {
    switch (Mode){
      case rtGeneral: WriteGeneralData(); break;
      case rtFile:    WriteRecordList();
                      WriteGridData(CurrentRecord + "_Grid", FileGrid); break;
      default:        WriteRecordList();
                      WriteLimits();
                      WriteOrder();
                      WriteGridData(CurrentRecord + "_Grid", FieldGrid); break;
    }
    Parameters.EraseParameters(TempFile);
  }
  DMPCATCHTHROW("TRecordForm::WriteCurrentRecord")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::WriteRecordList()
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(TempFile);
    Ini->EraseSection("RECORDS");
    for (int i = 0; i < Records->Count; i++)
      Ini->WriteString("RECORDS", Records->Names[i], Records->Values[Records->Names[i]]);
    delete Ini;
  }
  DMPCATCHTHROW("TRecordForm::WriteRecordList")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadTableData(int RecType, bool bDetail)
{
  DMPTRY {
    String NewCaption;
    AggregateFieldLB->Clear();
    AggregateFieldList.RetrieveNames(AggregateFieldLB, RecType, bDetail);
    if (RecType > rtFile && RecType < rtChecks)
      HeaderFooterRG->Enabled = true;
    StdFieldLB->Clear();
    StdFields.RetrieveNames(StdFieldLB, RecType, bDetail);
    CurrentTable = TableHelper::GetTable(RecType); // CR 5272 1/07/2004 DJK
    if (!CurrentTable.IsEmpty())
      DataMod->LoadFieldNames(CurrentTable, FieldLB->Items, FieldAttribList);
    switch (RecType){
      case rtChecks: DataMod->AppendFieldNames("ChecksDataEntry"   , FieldLB->Items, FieldAttribList); break;
      case rtStubs : DataMod->AppendFieldNames("StubsDataEntry"    , FieldLB->Items, FieldAttribList); break;
      case rtDocs  : DataMod->AppendFieldNames("DocumentsDataEntry", FieldLB->Items, FieldAttribList); break;
      case rtDetail: DataMod->AppendFieldNames("ChecksDataEntry"   , FieldLB->Items, FieldAttribList); break;
    }
  }
  DMPCATCHTHROW("TRecordForm::LoadTableData")
}


//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadLimits()
{
  DMPTRY {
    FillGrid(CurrentTable + "_Limits", LimitGrid);
  }
  DMPCATCHTHROW("TRecordForm::LoadLimits")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::WriteLimits()
{
  DMPTRY {
    WriteGridData(CurrentTable + "_Limits", LimitGrid);
  }
  DMPCATCHTHROW("TRecordForm::WriteLimits")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadOrder()
{
  DMPTRY {
    FillGrid(CurrentTable + "_Order", OrderGrid);
  }
  DMPCATCHTHROW("TRecordForm::LoadOrder")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::WriteOrder()
{
  DMPTRY {
    WriteGridData(CurrentTable + "_Order", OrderGrid);
  }
  DMPCATCHTHROW("TRecordForm::WriteOrder")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadGeneralData()
{
  DMPTRY {
    int Index;
    String Type, Name;
    TTreeNode *ParentNode;
    TIniFile *Ini = new TIniFile(TempFile);
    ExtractPathEdit->Text                         = Ini->ReadString ("GENERAL"   , "ExtractPath"                  , ""   );
    LogPathEdit->Text                             = Ini->ReadString ("GENERAL"   , "LogFilePath"                  , ""   );
    ImagePathEdit->Text                           = Ini->ReadString ("GENERAL"   , "ImagePath"                    , ""   );
    QuotesCB->Checked                             = Ini->ReadBool   ("GENERAL"   , "UseQuotes"                    , false);
    NoPadCB->Checked                              = Ini->ReadBool   ("GENERAL"   , "NoPad"                        , false);
    NullAsSpaceCB->Checked                        = Ini->ReadBool   ("GENERAL"   , "NullAsSpace"                  , false);
    FieldDelimCombo->Text                         = Ini->ReadString ("GENERAL"   , "FieldDelim"                   , ""   );
    String FieldName                              = Ini->ReadString ("GENERAL"   , "TimeStamp"                    , ""   );
    BatchTraceCombo->Text = FieldName;
    BatchTraceCombo->ItemIndex = BatchTraceCombo->Items->IndexOf(FieldName);
    // CR 9916 10/13/2004 ALH This was change to be an integer because there are three choices instead of just two know.
    // 0 = tif, 1 = PDF, 2 = tif format embeded in the data file.
    ImageFileFormatRadioGroup->ItemIndex          = Ini->ReadInteger("IMAGEFILES", "ImageFileFormatAsPDF"         , 0    );  // CR 9916 10/13/2004 ALH
    CombineImagesAcrossProcDatesCheckBox->Checked = Ini->ReadBool   ("IMAGEFILES", "CombineImagesAccrossProcDates", false);
    m_sImageFileNameSingle                        = Ini->ReadString ("IMAGEFILES", "ImageFileNameSingle"          , ""   );
    m_sImageFileNamePerTran                       = Ini->ReadString ("IMAGEFILES", "ImageFileNamePerTran"         , ""   );
    m_sImageFileNamePerBatch                      = Ini->ReadString ("IMAGEFILES", "ImageFileNamePerBatch"        , ""   );
    m_sImageFileProcDateFormat                    = Ini->ReadString ("IMAGEFILES", "ImageFileProcDateFormat"      , "MMDDYY");
    m_bImageFileZeroPad                           = Ini->ReadBool   ("IMAGEFILES", "ImageFileZeroPad"             , false);
    m_bImageFileBatchTypeFormatFull               = Ini->ReadBool   ("IMAGEFILES", "ImageFileBatchTypeFormatFull" , true );
    String sPostProcDLL                           = Ini->ReadString ("POSTDLL"  , "FileName"                      , ""   ); //CR10943 03/30/2005 ALH
    //CR10943 03/30/2005 ALH Begin
    if (!sPostProcDLL.IsEmpty())
    {
        PostDLLPathFileNameEdit->Text = sPostProcDLL;
        UsePostDLLCheckBOX->State = cbChecked;
        PostDLLEnableCheck();
    }
    else
    {
        UsePostDLLCheckBOX->State = cbUnchecked;
        PostDLLEnableCheck();
    }
    //CR10943 03/30/2005 ALH - End

    Index = FieldDelimCombo->Items->IndexOf(FieldDelimCombo->Text);
    if (Index >= 0)
      FieldDelimCombo->ItemIndex = Index;
    RecordDelimCombo->Text                        = Ini->ReadString ("GENERAL", "RecordDelim", "");
    Index = RecordDelimCombo->Items->IndexOf(RecordDelimCombo->Text);
    if (Index >= 0)
      RecordDelimCombo->ItemIndex = Index;
    ClearRecordTree();
    Records->Clear();
    Ini->ReadSectionValues("RECORDS", (TStrings*)Records);
    for (int i = 0; i < Records->Count; i++){
      Name = Records->Names[i];
      int iRawType = StrToIntDef(Records->Values[Name], -1) - 1; // djk 10/2/01
      int RecType = TRecHelper::GetRecordSubType(iRawType); // djk 10/2/01
      ParentNode = RecordTree->Items->Item[0];
      if (TRecHelper::GetHeaderTrailer(iRawType) == 1 && (RecType+1) <= rtTransactions) // djk 10/2/01
        RecType = (ParentNode->Count -1) - RecType; // djk 10/2/01
      if (RecType >= 0 && RecType < ParentNode->Count){
        ParentNode = RecordTree->Items->Item[0]->Item[RecType];
        RecordTree->Items->AddChild(ParentNode, Name);
      }
    }
    delete Ini;
    RecordTree->FullExpand();
    RedrawImageFileNames(); // CR 5815 03/03/2004 DJK
  }
  DMPCATCHTHROW("TRecordForm::LoadGeneralData")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::WriteGeneralData()
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(TempFile);
    //CR20894 MJH 5-1-07: Taking this out. No way to get version from dll, and was told it's not needed.
    //Ini->WriteInteger("VERSION"   , "Version"                      , TRunForm::GetLatestScriptVersion()); // djk 10/23/01
    Ini->WriteString ("GENERAL"   , "ExtractPath"                  , ExtractPathEdit->Text);
    Ini->WriteString ("GENERAL"   , "LogFilePath"                  , LogPathEdit->Text);
    Ini->WriteString ("GENERAL"   , "ImagePath"                    , ImagePathEdit->Text);
    Ini->WriteString ("GENERAL"   , "FieldDelim"                   , FieldDelimCombo->Text);
    Ini->WriteString ("GENERAL"   , "RecordDelim"                  , RecordDelimCombo->Text);
    Ini->WriteBool   ("GENERAL"   , "NoPad"                        , NoPadCB->Checked);
    Ini->WriteBool   ("GENERAL"   , "UseQuotes"                    , QuotesCB->Checked);
    Ini->WriteBool   ("GENERAL"   , "NullAsSpace"                  , NullAsSpaceCB->Checked);
    Ini->WriteString ("GENERAL"   , "TimeStamp"                    , BatchTraceCombo->Text);
    
    // CR 9916 10/13/2004 ALH This was change to be an integer because there are three choices instead of just two know.
    // 0 = NOTE, 1 = TIF, 2 = PDF, 3 = TIF embeded in Images file
    Ini->WriteInteger("IMAGEFILES", "ImageFileFormatAsPDF"         , ImageFileFormatRadioGroup->ItemIndex);  // CR 9916 10/13/2004 ALH

    Ini->WriteBool   ("IMAGEFILES", "CombineImagesAccrossProcDates", CombineImagesAcrossProcDatesCheckBox->Checked);
    Ini->WriteString ("IMAGEFILES", "ImageFileNameSingle"          , m_sImageFileNameSingle);
    Ini->WriteString ("IMAGEFILES", "ImageFileNamePerTran"         , m_sImageFileNamePerTran);
    Ini->WriteString ("IMAGEFILES", "ImageFileNamePerBatch"        , m_sImageFileNamePerBatch);
    Ini->WriteString ("IMAGEFILES", "ImageFileProcDateFormat"      , m_sImageFileProcDateFormat);
    Ini->WriteBool   ("IMAGEFILES", "ImageFileZeroPad"             , m_bImageFileZeroPad);
    Ini->WriteBool   ("IMAGEFILES", "ImageFileBatchTypeFormatFull" , m_bImageFileBatchTypeFormatFull);
    //CR10943 03/30/2005 ALH Begin
    if (UsePostDLLCheckBOX->State == cbChecked)
    {
        Ini->WriteString("POSTDLL" , "FileName"                     , PostDLLPathFileNameEdit->Text);
    }
    else
    {
        Ini->WriteString("POSTDLL" , "FileName"                     , "");
    }
    //CR10943 03/30/2005 ALH - End
    Ini->EraseSection("RECORDS");
    int RecType, Index;
    TTreeNode *CurNode;
    TTreeNode *BaseNode = RecordTree->Items->Item[0];
    for (int i = 0; i < BaseNode->Count; i++){
      CurNode = BaseNode->Item[i];
      for (int j = 0; j < CurNode->Count; j++){
        Index = Records->IndexOfName(CurNode->Item[j]->Text);
        if (Index >= 0)
          RecType = StrToIntDef(Records->Values[Records->Names[Index]],-1);
/*        RecType = i + 1;
        if (RecType > rtDetail)
          RecType = BaseNode->Count - i + 100;     */
        if (RecType >= 0)
          Ini->WriteInteger("RECORDS", CurNode->Item[j]->Text, RecType);
      }
    }
    Records->Clear();
    Ini->ReadSectionValues("RECORDS", (TStrings*)Records);
    delete Ini;
  }
  DMPCATCHTHROW("TRecordForm::WriteGeneralData")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::LoadFileData()
{
  DMPTRY {
    FileStdLB->Clear();
    StdFields.RetrieveNames(FileStdLB, rtFile);
    FileAggLB->Clear();
    AggregateFieldList.RetrieveNames(FileAggLB, rtFile);
    RecordCounts.RetrieveNames(FileAggLB, rtFile);
  }
  DMPCATCHTHROW("TRecordForm::LoadFileData")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RemoveQuotes(TAdvStringGrid *Grid, int Col, int Row)
{
  DMPTRY {
    String tmp = Grid->Cells[Col][Row];
    int i = 1;
    bool Quoted = false;
    if (!tmp.IsEmpty()){
      while (tmp.Length()>0 && (tmp[1] == '\x22' || tmp[i] == '\x27')){
        tmp = tmp.SubString(2, tmp.Length());
        Quoted = true;
      }
      i = tmp.Length();
      while (i > 0 && (tmp[i] == '\x22' || tmp[i] == '\x27')){
        tmp.Delete(tmp.Length(),1);
        Quoted = true;
        i = tmp.Length();
      }
    }
    if (Quoted)
      tmp = QuotedStr(tmp);
    Grid->Cells[Col][Row] = tmp;
  }
  DMPCATCHTHROW("TRecordForm::RemoveQuotes")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FillGrid(String Section, TAdvStringGrid *Grid)
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(TempFile);
    TStringList *Data = new TStringList;
    TStringList *tmpStr = new TStringList;
    String HeaderStr = Grid->Rows[0]->CommaText;
    Grid->Clear();
    Grid->Rows[0]->CommaText = HeaderStr;
    Ini->ReadSectionValues(Section, Data);
    if (Data->Count)
      Grid->RowCount = Data->Count + 1;
    else
      Grid->RowCount = 2;
    for (int i = 0; i < Data->Count; i++){
/*      tmpStr->CommaText = Data->Values[Data->Names[i]];
      tmpStr->Insert(0, "");          */
      TextToList(tmpStr, Data->Values[Data->Names[i]]);
      tmpStr->Insert(0, "");
      Grid->Rows[i+1]->Assign(tmpStr);//Data->Values[Data->Names[i]];
      if (Grid==LimitGrid)
        RemoveQuotes(LimitGrid, goValue, i+1);
    }
    delete tmpStr;
    delete Ini;
    delete Data;
  }
  DMPCATCHTHROW("TRecordForm::FillGrid")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::WriteGridData(String Section, TAdvStringGrid *Grid)
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(TempFile);
    String Key, FieldName;
    TStringList *tmpStr = new TStringList;
    Key = "Row";
    Ini->EraseSection(Section);
    for (int i = 1; i < Grid->RowCount; i++){
      FieldName = Grid->Cells[goFieldName][i];
      if (!FieldName.IsEmpty()){
        tmpStr->Assign(Grid->Rows[i]);
        tmpStr->Delete(0);
        String Text;
        ListToText(tmpStr, Text);
        Ini->WriteString(Section, Key + IntToStr(i), QuotedStr(Text));
//        Ini->WriteString(Section, Key + IntToStr(i), QuotedStr(tmpStr->CommaText));
      }
    }
    delete tmpStr;
    delete Ini;
  }
  DMPCATCHTHROW("TRecordForm::WriteGridData")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::GetFirstRecord(int RecType)
{
  DMPTRY {
    int i, Val, iRawValue;// CR 8798 7/19/04 MLT - moved declaration for iRawValue up to here
    TComboBox* Combo = (RecType == rtFile?FileLayoutNameCombo:LayoutNameCombo);
    TRadioGroup *RG = (RecType == rtFile?RecordTypeRG:HeaderFooterRG);
    CurrentRecord = "";
    Combo->Clear();
    for (i = 0; i < Records->Count; i++)
      if (RecType == TRecHelper::GetRecordType(StrToIntDef(Records->Values[Records->Names[i]], -1))) // djk 10/2/01
        Combo->Items->Add(Records->Names[i]);
    if (Combo->Items->Count){
      CurrentRecord = Combo->Items->Strings[0];
      iRawValue = StrToIntDef(Records->Values[CurrentRecord], -1);
      Combo->ItemIndex = 0;
      Combo->Text = CurrentRecord;
      RG->ItemIndex = TRecHelper::GetHeaderTrailer(iRawValue);
      if (RecType == rtDetail){
        int iSubType = TRecHelper::GetRecordSubType(iRawValue); // djk 10/2/01
        if (iSubType == rtDetail) // djk 10/2/01
        {
          // CR 8798 7/19/04 MLT  - added IgnoreOccurs checkbox
          IgnoreOccurs->Checked = TRecHelper::GetIgnoreOccursFlag(iRawValue);
          DisplayLevelCombo->ItemIndex = DisplayLevelCombo->Items->Count - 1;
        }
        else
          DisplayLevelCombo->ItemIndex = iSubType - 1; // djk 10/2/01
        RepeatLimit->Text = TRecHelper::GetRepeatLimitValue(iRawValue) == 0 ? String("") : String(TRecHelper::GetRepeatLimitValue(iRawValue));
        DisplayLevelComboChange(NULL);
      }
    }
    else
      RG->ItemIndex = 0;
    if (RecType > rtFile){
      FillGrid(CurrentRecord + "_Grid", FieldGrid);
      FieldGrid->Enabled = LayoutNameCombo->Items->Count > 0;
    }
    else if (RecType == rtFile){
      FillGrid(CurrentRecord + "_Grid", FileGrid);
      FileGrid->Enabled = FileLayoutNameCombo->Items->Count > 0;
    }
  }
  DMPCATCHTHROW("TRecordForm::GetFirstRecord")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::LayoutNameComboChange(TObject *Sender)
{
  DMPTRY {
    TComboBox *Combo = (TComboBox*)Sender;
    TAdvStringGrid *Grid = (TabControl1->TabIndex == rtFile?FileGrid:FieldGrid);
    TRadioGroup *RG = (TabControl1->TabIndex == rtFile?RecordTypeRG:HeaderFooterRG);
    if (!CurrentRecord.IsEmpty())
      WriteGridData(CurrentRecord + "_Grid", Grid);
    CurrentRecord = Combo->Text;
    int Val = StrToIntDef(Records->Values[CurrentRecord], -1);
    if (TabControl1->TabIndex == rtDetail){
      int DisplayVal = TRecHelper::GetRecordSubType(Val);
      if (DisplayVal == rtDetail)
      {
        // CR 7498 7/20/04 MLT - added IgnoreOccurs checkbox
        IgnoreOccurs->Checked = TRecHelper::GetIgnoreOccursFlag(Val);
        DisplayLevelCombo->ItemIndex = DisplayLevelCombo->Items->Count -1;
      }
      else
        DisplayLevelCombo->ItemIndex = DisplayVal -1;
      RepeatLimit->Text = TRecHelper::GetRepeatLimitValue(Val) == 0 ? String("") : String(TRecHelper::GetRepeatLimitValue(Val));
    }
    RG->ItemIndex = TRecHelper::GetHeaderTrailer(Val);
    if (TabControl1->TabIndex == rtDetail)
      DisplayLevelComboChange(NULL);
    FillGrid(CurrentRecord + "_Grid", Grid);
  }
  DMPCATCHTHROW("TRecordForm::LayoutNameComboChange")
}

//FIELD INFO

//---------------------------------------------------------------------------

void __fastcall TRecordForm::ListBoxMouseMove(TObject *Sender, TShiftState Shift, int X, int Y)
{
  DMPTRY {
    String TableName;
    TListBox *LB = (TListBox*)Sender;
    if (LB==FieldLB)
      TableName = CurrentTable;
    else
      TableName = "Standard";
    int Index = LB->ItemAtPos(Point(X,Y), true);
    if (Index >= 0)
      LB->Hint = GetHint(TableName, LB->Items->Strings[Index]);
  }
  DMPCATCHTHROW("TRecordForm::ListBoxMouseMove")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::DisplayHint(TObject *Sender)
{
  DMPTRY {
    HintBar->SimpleText = Application->Hint;
  }
  DMPCATCHTHROW("TRecordForm::DisplayHint")
}

//---------------------------------------------------------------------------
String __fastcall TRecordForm::GetHint(String Table, String FieldName)
{
  String sResult;
  DMPTRY {
    TIniHandler Ini;
    try {
      Ini.Execute();
      Ini.GetIniFileString("Hint.ini", Table, FieldName, "", sResult);
    }
    catch(...){}
    if (sResult.IsEmpty())
      sResult = "No information available for this field.";
  }
  DMPCATCHTHROW("TRecordForm::GetHint")
  return sResult;
}

//---------------------------------------------------------------------------//ADDING FIELDS

void __fastcall TRecordForm::AddDataBtnClick(TObject *Sender)
{
  DMPTRY {
    // CR 7411 ~ DLAQAB ~ 3-26-2004
    m_bDocumentHasChanged = true;

    TListBox *lb;
    if (Sender == AddDataBtn)
      lb = FieldLB;
    else if (Sender == AddAggregateBtn)
      lb = AggregateFieldLB;
    else if (Sender == FileAddAggBtn)
      lb = FileAggLB;
    else if (Sender == AddStdBtn)
      lb = StdFieldLB;
    else
      lb = FileStdLB;
    TStringList *SelectedList = new TStringList;
    int i, j;
    int iImageFieldOK = 0;
    bool bDisableOccurs = false;
    for (i = 0, j = 0; i < lb->Items->Count, j < lb->SelCount; i++){
       if (TabControl1->TabIndex == 10)  //CR 10943 5/4/2005 ALH  Begin - disable occurs if we are going to extract images at the joint detail.  TTabcontrol
       {
            if ((!bDisableOccurs) && (IgnoreOccurs->State != cbChecked))
            {
                  for (int k = 0; k < FieldGrid->RowCount; k++)
                  {
                      String sFieldName = FieldGrid->Cells[1][k].UpperCase();
                      if (sFieldName.Pos("IMAGE")> 0)
                      {
                          bDisableOccurs = true;
                          IgnoreOccurs->State = cbChecked;
                          ::MessageBox(NULL, "Image fields and Occurs cannot be used in the same joint detail record.\nOccurs will be disabled.","Extract Wizard - Occurs not allowed", MB_OK | MB_ICONINFORMATION);
                          break;
                      }
                  }
            }
        }                               //CR 10943 5/4/2005 ALH  End
        if (lb->Selected[i]){
          if ((ImageFileFormatRadioGroup->ItemIndex == 0) && (lb->Items->Strings[i].UpperCase().Pos("IMAGE") > 0)){  // CR 9916 11/11/2004 ALH
              iImageFieldOK = 1;
          }
          else if(lb->Items->Strings[i].UpperCase().Pos("IMAGEFILESIZE") > 0){  // CR 9916 11/11/2004 ALH   - allow image sizes only for single images.
                  for (int k = 0; k < FieldGrid->RowCount; k++){
                      if ((FieldGrid->Cells[1][k].UpperCase().Pos("PERBATCH")) || (FieldGrid->Cells[1][k].UpperCase().Pos("PERTRANSACTION"))){
                           iImageFieldOK = 2;
                           break;
                      }
                  }
                  if(iImageFieldOK == 0){      // CR 9916 11/12/2004 ALH
                     SelectedList->Add(lb->Items->Strings[i]);
                  }
          }
          else if((lb->Items->Strings[i].UpperCase().Pos("PERBATCH") > 0) || (lb->Items->Strings[i].UpperCase().Pos("PERTRANSACTION") > 0)){   // CR 9916 11/12/2004 ALH   - allow image sizes only for single images
                  for (int k = 0; k < FieldGrid->RowCount; k++){
                      if (FieldGrid->Cells[1][k].UpperCase().Pos("IMAGEFILESIZE")){
                           iImageFieldOK = 2;
                           break;
                      }
                  }
                  if(iImageFieldOK == 0){      // CR 9916 11/12/2004 ALH
                     SelectedList->Add(lb->Items->Strings[i]);
                  }
          }
          else{
                  SelectedList->Add(lb->Items->Strings[i]);
          }
          j++;
        }
    }
    if (iImageFieldOK == 1){   // CR 9916 11/11/2004 ALH  - don't allow the addtion of image fields if the field is an image field and we set the layout to no image files.
         ::MessageBox(NULL, "You may not add a image field to an extract defined with an image format of 'none.'\nChange the image format on the general tab to allow image fields in this extract.", "Extract Wizard - Invalid Selection", MB_OK | MB_ICONINFORMATION);
    }
    else if (iImageFieldOK == 2){
         ::MessageBox(NULL, "ImageFileSizeFront / ImageFileSizeRear are only supported for single check / document images or embeded tiff images.", "Extract Wizard - Invalid Selection", MB_OK | MB_ICONINFORMATION);
    }
    else{
        if (TabControl1->TabIndex == rtFile){
          if (FileGrid->Enabled)
            AddDataToGrid(SelectedList, FileGrid);
          else
            NewLayoutBtnClick(FileNewBtn);
        }
        else {
          switch (RecordPageControl->ActivePageIndex){
            case rtLayoutCreate: if (FieldGrid->Enabled){
                                   if (TabControl1->TabIndex == rtDetail){
                                     if (CheckDetailAdd())
                                       AddDataToGrid(SelectedList, FieldGrid);
                                     else
                                       DoError("Unable to add fields from the " +
                                              CurrentTable + " table at this display level.");
                                   }
                                   else
                                     AddDataToGrid(SelectedList, FieldGrid);
                                 }
                                 else
                                   NewLayoutBtnClick(NewLayoutBtn);
                                 break;
            case rtOrder: AddNameToGrid(SelectedList, OrderGrid); break;
            case rtLimit: AddNameToGrid(SelectedList, LimitGrid); break;
            default: break;
          }
        }
    }
    delete SelectedList;
  }
  DMPCATCHTHROW("TRecordForm::AddDataBtnClick")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::AddNameToGrid(TStringList *Selected, TAdvStringGrid *Grid)
{
  DMPTRY {
    TStringList *Sel = new TStringList;
    Sel->Assign(Selected);
    //Do not allow dups in order grid
    if (Grid == OrderGrid)
      for (int i = Sel->Count - 1; i >= 0; i--)
        if (Grid->Cols[1]->IndexOf(Sel->Strings[i]) >= 0)
          Sel->Delete(i);
    if (Sel->Count > 0){
      if (Grid->RowCount == 2 && Grid->Cells[1][1].IsEmpty()){
        Grid->Cells[goFieldName][1] = Sel->Strings[0];
        if (Grid == OrderGrid)
          Grid->Cells[goOrder][1] = ASC;
        Sel->Delete(0);
      }
      for (int i = 0; i < Sel->Count;i++){
        Grid->RowCount++;
        Grid->Cells[goFieldName][Grid->RowCount-1] = Sel->Strings[i];
        if (Grid == OrderGrid)
          Grid->Cells[goOrder][Grid->RowCount-1] = ASC;
      }
    }
    delete Sel;
  }
  DMPCATCHTHROW("TRecordForm::AddNameToGrid")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::LoadAttributes(String Field, String &Data)
{
  DMPTRY {
    int index;
    index = FieldAttribList->IndexOfName(Field);
    if (index >= 0){
      Data = FieldAttribList->Values[Field];
      if (TabControl1->TabIndex == rtDetail)
        Data.Insert(CurrentTable + ".", 1);
    }
    else {
      if (AggregateFieldList.IsMember(Field))
        Data = AggregateFieldList.GetAttributes(Field);
      else if (StdFields.IsMember(Field))
        Data = StdFields.GetAttributes(Field);
      else if (RecordCounts.IsMember(Field))
        Data = RecordCounts.GetAttributes(Field);
    }
  }
  DMPCATCHTHROW("TRecordForm::LoadAttributes")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::AddDataToGrid(TStringList *Selected, TAdvStringGrid *Grid)
{
  DMPTRY {
    String Data;
    TStringList *Sel = new TStringList;
    Sel->Assign(Selected);
    for (int j = 0; j < Sel->Count;j++){
      if (!(j == 0 && Grid->RowCount == 2 && Grid->Cells[goFieldName][1].IsEmpty()))
        Grid->RowCount++;
      LoadAttributes(Sel->Strings[j], Data);
      if (!Data.IsEmpty()){
        Data.Insert(",", 1);
        Grid->Rows[Grid->RowCount - 1]->CommaText = Data;
      }
    }
    delete Sel;
  }
  DMPCATCHTHROW("TRecordForm::AddDataToGrid")
}

//---------------------------------------------------------------------------
// CR 8798 7/19/04 MLT - added second parameter for IgnoreOccurs checkbox
void __fastcall TRecordForm::SetupForOccurs(bool CanHaveOccurs, bool UsingOccurs)
{
  DMPTRY {
    if (CanHaveOccurs){
      // djk 12/10/01 - added FieldGrid->ColCount == 9
      // CR 8798 7/19/04 MLT - added UsingOccurs
      if (FieldGrid->ColCount == 9 && UsingOccurs){
        NoteLbl->Top = NoteLbl->Top + 261;
        FieldGrid->InsertCols(9, 2);
        FieldGrid->ColumnHeaders->Add("Occrs Grp");
        FieldGrid->ColumnHeaders->Add("Occrs Cnt");
        FieldGrid->ColWidths[ 9] = 55;
        FieldGrid->ColWidths[10] = 56;
        FieldGrid->Width = FieldGrid->Width + 111;
        MaxRepeatsLabel->Top = FieldGrid->Top;
        MaxRepeatsLabel->Left = FieldGrid->Left+FieldGrid->Width+6;
        RepeatLimit->Top = MaxRepeatsLabel->Top+18;
        RepeatLimit->Left = MaxRepeatsLabel->Left;
        MaxRepeatsLabel->Visible = true;
        RepeatLimit->Visible = true;
      }
      // CR 8798 7/19/04 MLT - added UseOccurs checkbox control
      IgnoreOccurs->Visible = true;
    }
    else{
      if (FieldGrid->ColCount == 11){
        MaxRepeatsLabel->Visible = false;
        RepeatLimit->Visible = false;
        NoteLbl->Top = NoteLbl->Top - 261;
        FieldGrid->RemoveCols(9, 2);
        FieldGrid->Width = FieldGrid->Width - 111;
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::SetupForOccurs")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FieldLBDblClick(TObject *Sender)
{
  DMPTRY {
    AddDataBtnClick(AddDataBtn);
  }
  DMPCATCHTHROW("TRecordForm::FieldLBDblClick")
}
//---------------------------------------------------------------------------


void __fastcall TRecordForm::AggregateFieldLBDblClick(TObject *Sender)
{
  DMPTRY {
    AddDataBtnClick(AddAggregateBtn);
  }
  DMPCATCHTHROW("TRecordForm::AggregateFieldLBDblClick")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::StdFieldLBDblClick(TObject *Sender)
{
  DMPTRY {
    AddDataBtnClick(AddStdBtn);
  }
  DMPCATCHTHROW("TRecordForm::StdFieldLBDblClick")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FileAggLBDblClick(TObject *Sender)
{
  DMPTRY {
    AddDataBtnClick(FileAddAggBtn);
  }
  DMPCATCHTHROW("TRecordForm::FileAggLBDblClick")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FileStdLBDblClick(TObject *Sender)
{
  DMPTRY {
    AddDataBtnClick(FileAddBtn);
  }
  DMPCATCHTHROW("TRecordForm::FileStdLBDblClick")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::GridDragOver(TObject *Sender, TObject *Source, int X, int Y, TDragState State, bool &Accept)
{
  DMPTRY {
    if (dynamic_cast<TListBox*>(Source))
      Accept = ((TWinControl*)Sender)->Enabled;
    else
      Accept = false;
  }
  DMPCATCHTHROW("TRecordForm::GridDragOver")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::GridDragDrop(TObject *Sender, TObject *Source, int X, int Y)
{
  DMPTRY {
    if (Source == FieldLB)
      AddDataBtnClick(AddDataBtn);
    else if (Source == AggregateFieldLB)
      AddDataBtnClick(AddAggregateBtn);
    else if (Source == FileAggLB)
      AddDataBtnClick(FileAddAggBtn);
    else if (Source == StdFieldLB)
      AddDataBtnClick(AddStdBtn);
    else if (Source == FileStdLB)
      AddDataBtnClick(FileAddBtn);
  }
  DMPCATCHTHROW("TRecordForm::GridDragDrop")
}

//REMOVING FIELDS
//---------------------------------------------------------------------------

void __fastcall TRecordForm::RemoveBtnClick(TObject *Sender)
{
  DMPTRY {
    TAdvStringGrid *Grid = FileGrid;
    if (TabControl1->TabIndex != rtFile)
      switch (RecordPageControl->ActivePageIndex){
        case rtLayoutCreate: Grid = FieldGrid; break;
        case rtOrder: Grid = OrderGrid; break;
        case rtLimit: Grid = LimitGrid; break;
        default: Grid = NULL;
      }
    if (Grid){
      if (Grid->RowCount > 2)
        Grid->RemoveRows(Grid->Row, 1);
      else if (Grid->Row == 1)
        Grid->Rows[1]->Clear();
    }
  }
  DMPCATCHTHROW("TRecordForm::RemoveBtnClick")
}

//MOVING FIELDS
//---------------------------------------------------------------------------

void __fastcall TRecordForm::UpBtnClick(TObject *Sender)
{
  DMPTRY {
    TAdvStringGrid *Grid = FileGrid;
    if (TabControl1->TabIndex != rtFile)
      switch (RecordPageControl->ActivePageIndex){
        case rtLayoutCreate: Grid = FieldGrid; break;
        case rtOrder: Grid = OrderGrid; break;
        case rtLimit: Grid = LimitGrid; break;
        default: Grid = NULL;
      }
    if (Grid && Grid->Row > 1){
      Grid->MoveRow(Grid->Row, Grid->Row-1);
      Grid->Row--;
    }
    else
      MessageBeep(MB_ICONEXCLAMATION);
  }
  DMPCATCHTHROW("TRecordForm::UpBtnClick")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::DownBtnClick(TObject *Sender)
{
  DMPTRY {
    TAdvStringGrid *Grid = FileGrid;
    if (TabControl1->TabIndex != rtFile)
      switch (RecordPageControl->ActivePageIndex){
        case rtLayoutCreate: Grid = FieldGrid; break;
        case rtOrder: Grid = OrderGrid; break;
        case rtLimit: Grid = LimitGrid; break;
        default: Grid = NULL;
      }
    if (Grid && Grid->Row <= Grid->RowCount - 2){
      Grid->MoveRow(Grid->Row, Grid->Row+1);
      Grid->Row++;
    }
    else
      MessageBeep(MB_ICONEXCLAMATION);
  }
  DMPCATCHTHROW("TRecordForm::DownBtnClick")
}
//---------------------------------------------------------------------------
void __fastcall TRecordForm::RestoreFieldBtnClick(TObject *Sender)
{
  DMPTRY {
    TAdvStringGrid *Grid = (TabControl1->TabIndex == rtFile?FileGrid:FieldGrid);
    String Field, Data;
    Field = Grid->Cells[goFieldName][Grid->Row];
    if (Field.IsEmpty())
      MessageBeep(MB_ICONEXCLAMATION);
    else {
      LoadAttributes(Field, Data);
      if (!Data.IsEmpty()){
        Data.Insert(",",1);
        Grid->Rows[Grid->Row]->CommaText = Data;
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::RestoreFieldBtnClick")
}

//---------------------------------------------------------------------------

//GRID EDITING
//---------------------------------------------------------------------------
void __fastcall TRecordForm::GridCanEditCell(TObject *Sender, int aRow, int aCol, bool &canedit)
{
  DMPTRY {
    int FirstCol;
    if (Sender == LimitGrid)
      FirstCol = goOpr;
    else
      FirstCol = 3;
    canedit = aRow > 0 && aCol >= FirstCol && !((TAdvStringGrid*)Sender)->Cells[goFieldName][aRow].IsEmpty();
  }
  DMPCATCHTHROW("TRecordForm::GridCanEditCell")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::GridClickCell(TObject *Sender, int arow, int acol)
{
  DMPTRY {
  /*  TAdvStringGrid *Grid = (TAdvStringGrid*)Sender;  //CR 9916 11/11/2004 ALH  This cause behaviour that would
    if (arow == 0)                                                               probably seem strange to most clients
      if (acol > goFieldName){                                                   so on behalf of myself, DJK and MVK, I am
        String Value = Grid->Cells[acol][Grid->Row];                             commenting this out for now.
        for (int i = 1; i < Grid->RowCount; i++)
          Grid->Cells[acol][i] = Value;
      }  */
  }
  DMPCATCHTHROW("TRecordForm::GridClickCell")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::FillOperatorCombo(bool AllOps)
{
  DMPTRY {
    LimitGrid->ClearComboString();
    if (AllOps){
      LimitGrid->AddComboString("=");
      LimitGrid->AddComboString("<>");
      LimitGrid->AddComboString(">");
      LimitGrid->AddComboString(">=");
      LimitGrid->AddComboString("<");
      LimitGrid->AddComboString("<=");
      LimitGrid->AddComboString("between");
      LimitGrid->AddComboString("in");
      LimitGrid->AddComboString("is Null");
      LimitGrid->AddComboString("is not NULL");
    }
    else {
      LimitGrid->AddComboString("AND");
      LimitGrid->AddComboString("AND NOT");
      LimitGrid->AddComboString("OR");
      LimitGrid->AddComboString("OR NOT");
    }
  }
  DMPCATCHTHROW("TRecordForm::FillOperatorCombo")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::GridGetEditorType(TObject *Sender, int aCol, int aRow, TEditorType &aEditor)
{
  DMPTRY {
    if (Sender == LimitGrid){
      if (aCol == goOpr || aCol == goRecOpr){
        aEditor = edComboEdit;
        FillOperatorCombo(aCol == goOpr);
      }
      else if (aCol == goValue){
        if (CodeMapper.IsCoded(LimitGrid->Cells[goFieldName][aRow])){
          aEditor = edComboEdit;
          LimitGrid->ClearComboString();
          CodeMapper.RetrieveCodeValues(LimitGrid->Cells[goFieldName][aRow],LimitGrid->Combobox->Items);
          LimitGrid->Combobox->Style = LayoutNameCombo->Style;     //GW can't get around ambiguity any other way
        }
      }
    }
    else if ((Sender == FieldGrid)||(Sender == FileGrid)){
      if (aCol == goPadChar || aCol == goJustify || aCol == goQuotes)
        aEditor = edUpperCase;
      else if (aCol == goFormat)
        aEditor = edEditBtn;
    }
  }
  DMPCATCHTHROW("TRecordForm::GridGetEditorType")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::GridDblClickCell(TObject *Sender, int aRow, int aCol)
{
  DMPTRY {
    if (aRow > 0){
      if (Sender == OrderGrid){
        //Can't find difference in grids - OnCanEdit does not work for OrderGrid
        if (aCol == goOrder && !OrderGrid->Cells[goFieldName][aRow].IsEmpty())
          OrderGrid->Cells[goOrder][aRow] = OrderGrid->Cells[goOrder][aRow]==ASC?DESC:ASC;
      }
      else if (Sender == FieldGrid){
        if (aCol == goJustify)
          FieldGrid->Cells[goJustify][aRow] = FieldGrid->Cells[goJustify][aRow]=="R"?"L":"R";
        else if (aCol == goQuotes)
          FieldGrid->Cells[goQuotes][aRow] = FieldGrid->Cells[goQuotes][aRow]=="X"?"":"X";
      }
      else if (Sender == FileGrid){
        if (aCol == goJustify)
          FileGrid->Cells[goJustify][aRow] = FileGrid->Cells[goJustify][aRow]=="R"?"L":"R";
        else if (aCol == goQuotes)
          FileGrid->Cells[goQuotes][aRow] = FileGrid->Cells[goQuotes][aRow] == "X"?"":"X";
      }
      else if (Sender == LimitGrid){
        if (aCol == goLeftParen)
          LimitGrid->Cells[goLeftParen][aRow] += "(";
        else if (aCol == goRightParen)
          LimitGrid->Cells[goRightParen][aRow] += ")";
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::GridDblClickCell")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FieldGridEllipsClick(TObject *Sender, int aCol, int aRow, String &S)
{
  DMPTRY {
    TAdvStringGrid *Grid = (TAdvStringGrid*)Sender;
    int GridRecType, RecType;
    GridRecType = StrToIntDef(Grid->Cells[goDataType][aRow],1);
    switch (GridRecType){
      case ftString: RecType = 2; break;
      case ftInteger: RecType = 2; break;
      case ftFloat:
      case ftCurrency:
      case ftBCD: RecType = 1; break;
      case ftDate:
      case ftTime:
      case ftDateTime: RecType = 0; break;
      default: RecType = 2;
    }
    if (FormatDlg->Execute(RecType, S)== mrOk)
      S = FormatDlg->GetFormatString();
  }
  DMPCATCHTHROW("TRecordForm::FieldGridEllipsClick")
}

//---------------------------------------------------------------------------
//Other grids do not need validation of this sort
//NOTE: It is not advisable to check validity of format, since to do so
//would restrict the user to a list or to those formats that can be
//reconverted to the original type, thus mm-dd-yy, while printing
//correctly, would be returned as an invalid date format.  GW
void __fastcall TRecordForm::FieldGridCellValidate(TObject *Sender, int Col, int Row, String &Value, bool &Valid)
{
  DMPTRY {
    TAdvStringGrid *Grid = (TAdvStringGrid*)Sender;
    if (Grid->Cells[goFieldName][Row].IsEmpty())
      Valid = true;
    else {
      switch (Col){
        case goButton: break;
        case goFieldName: break;
        case goFieldDesc: break;
        case goColWidth: Valid = Value.ToIntDef(-1) >= 0; break;
        case goPadChar: Valid = Value.Length()<= 1 || Value.UpperCase()=="SP"; break;
        case goJustify: Valid = (Value.UpperCase() == "R")||(Value.UpperCase() == "L"); break;
        case goQuotes: Valid = (Value.UpperCase() == "X")||(Value.IsEmpty()); break;
        case goFormat: {
                         if (Grid->Cells[goFieldName][Row].AnsiCompareIC("FirstLast") == 0) {
                           String sVal = Value;
                           String sFirst, sMid, sLast;
                           int iPos = sVal.Pos("/");
                           if (iPos) {
                             sFirst = sVal.SubString(1, iPos-1);
                             sVal = sVal.SubString(iPos+1, sVal.Length()-iPos);
                             iPos = sVal.Pos("/");
                             if (iPos) {
                               sMid = sVal.SubString(1, iPos-1);
                               sLast = sVal.SubString(iPos+1, sVal.Length()-iPos);
                             }
                             else
                               Valid = false;
                           }
                           else
                             Valid = false;
                           if (Valid) {
                             if (Value.Pos(",")) {
                               Valid = false;
                               DoMessage("This entry cannot contain commas");
                             }
                             else {
                               int iWidthMax = StrToIntDef(Grid->Cells[goColWidth][Row],0);
                               if (sFirst.Length() > iWidthMax || sMid.Length() > iWidthMax || sLast.Length() > iWidthMax) {
                                 Valid = false;
                                 DoMessage("None of the three entries may be greater than the column width");
                               }
                             }
                           }
                           else
                             DoMessage("The required form of the entry is   First/Mid/Last  ");
                         }
                         else {
                           if (Value.Length() > StrToIntDef(Grid->Cells[goColWidth][Row],0))
                             DoMessage("The width of the formatted data (" + IntToStr(Value.Length()) + ") is greater than the column width.");
                         }
                       } break;
        case goOccursGroup: Valid = (Value.Trim().IsEmpty())||(Value.ToIntDef(-1) >= 0); break;
        case goMaxOccurs: Valid = (Value.Trim().IsEmpty())||(Value.ToIntDef(-1) >= 1); break;
      }
    }
    if (!Valid)
      DoError(QuotedStr(Value) + " is not a valid entry.");
  }
  DMPCATCHTHROW("TRecordForm::FieldGridCellValidate")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::FieldGridKeyPress(TObject *Sender, char &Key)
{
  DMPTRY {
    TAdvStringGrid *Grid = (TAdvStringGrid*)Sender;
    if ((Key >= ' ') && (Key <= 'z'))
      if (Grid->Col == goQuotes)
        Key = 'X';
  }
  DMPCATCHTHROW("TRecordForm::FieldGridKeyPress")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::LimitGridCellValidate(TObject *Sender, int Col, int Row, String &Value, bool &Valid)
{
  DMPTRY {
    if (Col == goLeftParen||Col == goRightParen){
      char ValidChar;
      if (Col == goLeftParen)
        ValidChar = '(';
      else if (Col == goRightParen)
        ValidChar = ')';
      String CompStr = CompStr.StringOfChar(ValidChar, Value.Length());
      Valid = Value == CompStr;
    }
  }
  DMPCATCHTHROW("TRecordForm::LimitGridCellValidate")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::LimitGridKeyPress(TObject *Sender, char &Key)
{
  DMPTRY {
    if ((Key >= ' ') && (Key <= 'z'))
      if (LimitGrid->Col == goLeftParen)
        Key = '(';
      else if (LimitGrid->Col == goRightParen)
        Key = ')';
  }
  DMPCATCHTHROW("TRecordForm::LimitGridKeyPress")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::UnhighlightEdit(int Type)
{
  DMPTRY {
    if (Type == rtFile){
      FileGrid->HideInplaceEdit();
      FileGrid->Col = goFieldName;
    }
    else {
      FieldGrid->Col = goFieldName;
      FieldGrid->HideInplaceEdit();
      LimitGrid->Col = goFieldName;
      LimitGrid->HideInplaceEdit();
      OrderGrid->Col = goFieldName;
      OrderGrid->HideInplaceEdit();
    }
  }
  DMPCATCHTHROW("TRecordForm::UnhighlightEdit")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::HeaderFooterRGClick(TObject *Sender)
{
  DMPTRY {
    int Index, Value;
    TRadioGroup *RG = (TRadioGroup*)Sender;
    if (!CurrentRecord.IsEmpty()){
      Index = Records->IndexOfName(CurrentRecord);
      if (Index >= 0){
        Value = StrToIntDef(Records->Values[Records->Names[Index]],0);
        Records->Values[Records->Names[Index]] = TRecHelper::SetHeaderTrailer(Value, RG->ItemIndex); // djk 10/2/01
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::HeaderFooterRGClick")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::DisplayLevelComboDropDown(TObject *Sender)
{
  DMPTRY {
    LastDisplayLevel = DisplayLevelCombo->ItemIndex;
  }
  DMPCATCHTHROW("TRecordForm::DisplayLevelComboDropDown")
}

//---------------------------------------------------------------------------
int __fastcall TRecordForm::GetMinDetailType()
{
  int iResult = rtFile;
  DMPTRY {
    int CurType;
    for (int i = 1; i < FieldGrid->RowCount; i++){
      CurType = rtFile;
      if (FieldGrid->Cells[1][i].Pos("Bank."))
        CurType  = rtBank;
      else if (FieldGrid->Cells[1][i].Pos("Customer."))
        CurType = rtCustomer;
      else if (FieldGrid->Cells[1][i].Pos("Lockbox."))
        CurType = rtLockbox;
      else if (FieldGrid->Cells[1][i].Pos("Batch."))
        CurType = rtBatch;
      else if (FieldGrid->Cells[1][i].Pos("Transactions."))
        CurType = rtTransactions;
      else if (FieldGrid->Cells[1][i].Pos("Checks."))
        CurType = rtDetail;
      else if (FieldGrid->Cells[1][i].Pos("Stubs."))
        CurType = rtDetail;
      else if (FieldGrid->Cells[1][i].Pos("Documents."))
        CurType = rtDetail;
      if (CurType > iResult)
        iResult = CurType;
    }
  }
  DMPCATCHTHROW("TRecordForm::GetMinDetailType")
  return iResult;
}

//---------------------------------------------------------------------------
bool __fastcall TRecordForm::CheckDisplayLevel(int NewIndex)
{
  bool bResult = false;
  DMPTRY {
    int NewType = NewIndex + 1;
    if (NewType == DisplayLevelCombo->Items->Count)
      NewType = rtDetail;
    bResult = (NewType >= GetMinDetailType());
  }
  DMPCATCHTHROW("TRecordForm::CheckDisplayLevel")
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall TRecordForm::CheckDetailAdd()
{
  bool bResult = false;
  DMPTRY {
    int TableType, CurType = DisplayLevelCombo->ItemIndex + 1;
    if (CurType == DisplayLevelCombo->Items->Count)
      CurType = rtDetail;
    TableType = rtFile;
    if (CurrentTable.UpperCase()=="BANK")
      TableType = rtBank;
    else if (CurrentTable.UpperCase()=="CUSTOMER")
      TableType = rtCustomer;
    else if (CurrentTable.UpperCase()=="LOCKBOX")
      TableType = rtLockbox;
    else if (CurrentTable.UpperCase()=="BATCH")
      TableType = rtBatch;
    else if (CurrentTable.UpperCase()=="TRANSACTIONS")
      TableType = rtTransactions;
    else if (CurrentTable.UpperCase()=="CHECKS")
      TableType = rtChecks;
    else if (CurrentTable.UpperCase()=="STUBS")
      TableType = rtStubs;
    else if (CurrentTable.UpperCase()=="DOCUMENTS")
      TableType = rtDocs;
    bResult = (CurType >= TableType);
  }
  DMPCATCHTHROW("TRecordForm::CheckDetailAdd")
  return bResult;
}


//---------------------------------------------------------------------------

void __fastcall TRecordForm::DisplayLevelComboChange(TObject *Sender)
{
  DMPTRY {
    int Index, Value, NewValue;
    if (!CurrentRecord.IsEmpty()){
      if (!CheckDisplayLevel(DisplayLevelCombo->ItemIndex)){
        DisplayLevelCombo->ItemIndex = LastDisplayLevel;
        DoError("You may not choose a display level higher than the fields you have "
          "already selected.");
      }
      else {
        HeaderFooterRG->Enabled = DisplayLevelCombo->ItemIndex < DisplayLevelCombo->Items->Count-1;
        if (!HeaderFooterRG->Enabled)           //GW 8/21/01
          HeaderFooterRG->ItemIndex = 0;
        if (DisplayLevelCombo->ItemIndex == DisplayLevelCombo->Items->Count -1)
          NewValue = rtDetail;
        else
          NewValue = DisplayLevelCombo->ItemIndex + 1;
        if (HeaderFooterRG->Enabled)
          NewValue = TRecHelper::SetHeaderTrailer(NewValue, HeaderFooterRG->ItemIndex == 1);
        NewValue = TRecHelper::SetRecordDetail(NewValue, true);
        NewValue = TRecHelper::SetRepeatLimitValue(NewValue, RepeatLimit->Text.Trim().IsEmpty() ? 0 : RepeatLimit->Text.Trim().ToInt());
        // CR 8798 7/19/04 MLT - added IgnoreOccurs checkbox to allow user to bypass the occcurs logic for a given detail record type
        NewValue = TRecHelper::SetRecordIgnoreOccurs(NewValue, IgnoreOccurs->Checked);
        Index = Records->IndexOfName(CurrentRecord);
        if (Index >= 0)
          Records->Values[Records->Names[Index]] = NewValue;
      }
    }
    // CR 8798 7/19/04 MLT - added IgnoreOccurs parameter
    SetupForOccurs(DisplayLevelCombo->ItemIndex == DisplayLevelCombo->Items->Count-1, !IgnoreOccurs->Checked);
    DetermineVisibilityOfAggregates();
  }
  DMPCATCHTHROW("TRecordForm::DisplayLevelComboChange")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::NewLayoutBtnClick(TObject *Sender)
{
  DMPTRY {
    TComboBox *Combo;
    TAdvStringGrid *Grid;
    if (TabControl1->TabIndex == rtFile){
      Combo = FileLayoutNameCombo;
      Grid = FileGrid;
    }
    else {
      Combo = LayoutNameCombo;
      Grid = FieldGrid;
    }
    String NewName = InputBox("Layout", "Enter a name for the new layout", "");
    if (!NewName.IsEmpty()){
      int Index = Records->IndexOfName(NewName);
      if (Index >= 0)
        DoError("Cannot create layout.  A layout with this name already exists.");
      else {
        WriteGridData(CurrentRecord + "_Grid", Grid);
        int RecType = TabControl1->TabIndex;
        if (RecType == rtDetail){
          if (DisplayLevelCombo->ItemIndex < DisplayLevelCombo->Items->Count -1)
            RecType = DisplayLevelCombo->ItemIndex + 1;
          RecType = TRecHelper::SetRecordDetail(RecType, true);
        }
        RecType = TRecHelper::SetHeaderTrailer(RecType, HeaderFooterRG->ItemIndex);
        Records->Add(NewName + "=" + IntToStr(RecType));
        CurrentRecord = NewName;
        TIniFile *Ini = new TIniFile(TempFile);
        Ini->WriteInteger("RECORDS", NewName, RecType);
        delete Ini;
        Combo->Items->Add(NewName);
        Combo->ItemIndex = Combo->Items->IndexOf(NewName);
        FillGrid(CurrentRecord + "_Grid", Grid);
        Grid->Enabled = true;
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::NewLayoutBtnClick")
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::TestExtractBtnClick(TObject *Sender)
{
  DMPTRY {
    String Junk;
    WriteCurrentRecord(TabControl1->TabIndex);
    if (GetParameters(Junk))   {
      //CR20894 MJH 4-3-07: Moved all TestExtract code into this exe
      TExtract extract;
      AnsiString errStr;
      TestExtract(TempFile, CurrentRecord, errStr);
      if (!errStr.IsEmpty()) {
        throw DMPException(errStr);
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::TestExtractBtnClick")
}
//---------------------------------------------------------------------------
//CR20894 MJH 4-03-07: Moved and modified function from DMPExtract.dll
void TRecordForm::TestExtract(const String& SetupFile, const String& Record, String& errStr)
{
    DMPDataMod = 0;
    DMPException* dmpEx1 = new DMPException();
    DMPTRY {
      TExtract Extract;
      DMPDataMod = new TDMPDataMod(0);
      if (DMPDataMod->IsConnected()) {
        Extract.DoTest(SetupFile, Record);
        errStr = Extract.m_sLastExceptionError;
      } else {
        ShowMessage("Unable to connect to database.");
      }
    }
    DMPCATCH_GET("Task: Load data module and run test", *dmpEx1)
    DMPException* dmpEx2 = new DMPException();
    DMPTRY {
      if (DMPDataMod) {
        if (DMPDataMod->IsConnected())
          DMPDataMod->CloseConnection();
        delete DMPDataMod;
      }
    }
    DMPCATCH_GET("Task: Release database connection", *dmpEx2)
    if (!dmpEx1->Empty) {
      delete dmpEx2;
      throw *dmpEx1;
    }
    else {
      delete dmpEx1;
      if (!dmpEx2->Empty)
        throw *dmpEx2;
      else
        delete dmpEx2;
    }
}
//---------------------------------------------------------------------------

void __fastcall TRecordForm::RestoreBtnClick(TObject *Sender)
{
  DMPTRY {
    if (!SetupFile.IsEmpty()){
      TIniFile *SetupIni = new TIniFile(SetupFile);
      TStringList *Lines = new TStringList;
      TIniFile *Ini = new TIniFile(TempFile);
      SetupIni->ReadSectionValues(CurrentRecord + "_Grid", Lines);
      for (int i = 0; i < Lines->Count; i++)
        Ini->WriteString(CurrentRecord + "_Grid", Lines->Names[i], Lines->Values[Lines->Names[i]]);
      delete Ini;
      delete Lines;
      delete SetupIni;
    }
    TAdvStringGrid *Grid = (TabControl1->TabIndex == rtFile?FileGrid:FieldGrid);
    FillGrid(CurrentRecord + "_Grid", Grid);
  }
  DMPCATCHTHROW("TRecordForm::RestoreBtnClick")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::DeleteLayoutBtnClick(TObject *Sender)
{
  DMPTRY {
    DeleteLayout(CurrentRecord);
    WriteRecordList();
    GetFirstRecord(TabControl1->TabIndex);
  }
  DMPCATCHTHROW("TRecordForm::DeleteLayoutBtnClick")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::DeleteLayout(String RecordName)
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(TempFile);
    Ini->EraseSection(RecordName + "_Grid");
    delete Ini;
    int Index = Records->IndexOfName(RecordName);
    if (Index >= 0)
      Records->Delete(Index);
  }
  DMPCATCHTHROW("TRecordForm::DeleteLayout")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::ViewWhereBtnClick(TObject *Sender)
{
  DMPTRY {
    WhereMemo->Clear();
    WhereMemo->Text = GetWhereClause();
  }
  DMPCATCHTHROW("TRecordForm::ViewWhereBtnClick")
}

//---------------------------------------------------------------------------
// CR#15135 12/20/05 MSV  - For column types -1, 1, 9, 10, 11, and 12 (basically 
//                          text and datetime types) we need to include single 
//                          quotes around the value.
String __fastcall TRecordForm::GetWhereClause()
{
  String sResult,sTable,sFldDelim;
  DMPTRY {
    switch (TabControl1->TabIndex){
      case rtChecks:    sTable = "Checks";      break;
      case rtStubs:     sTable = "Stubs";       break;
      case rtDocs:      sTable = "Documents";   break;
      default:          sTable = CurrentTable;
    }
    if (!LimitGrid->Cells[goFieldName][1].IsEmpty()) {
      sResult = " where ";
      for (int i = 1; i < LimitGrid->RowCount; i++){
        int columntype = DataMod->GetColumnSQLDataType(sTable,LimitGrid->Cells[goFieldName][i]);
        if (columntype == 0) {
            switch (TabControl1->TabIndex) {
                case rtChecks: 
                  columntype = DataMod->GetColumnSQLDataType("ChecksDataEntry",LimitGrid->Cells[goFieldName][i]);
                  break;
                case rtStubs:
                  columntype = DataMod->GetColumnSQLDataType("StubsDataEntry",LimitGrid->Cells[goFieldName][i]);
                  break;
                case rtDocs:
                  columntype = DataMod->GetColumnSQLDataType("DocumentsDataEntry",LimitGrid->Cells[goFieldName][i]);
                  break;
            }               
        }
        sFldDelim = "";
        if ( (columntype == -1) || (columntype == 1) || ((columntype >= 9) && (columntype <= 12)) )
          sFldDelim = "'";
        sResult += LimitGrid->Cells[goLeftParen][i] + LimitGrid->Cells[goFieldName][i] + " " + 
                   LimitGrid->Cells[goOpr][i] + " " + sFldDelim + LimitGrid->Cells[goValue][i] + sFldDelim + 
                   LimitGrid->Cells[goRightParen][i] + " " + LimitGrid->Cells[goRecOpr][i] + " ";
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::GetWhereClause")
  return sResult;
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::TestWhereBtnClick(TObject *Sender)
{
  DMPTRY {
    TDMPADODataSet *DataSet = DataMod->ActionDataSet;
    String TableName, Where;
    switch (TabControl1->TabIndex){
      case rtChecks: TableName = "Checks, ChecksDataEntry"; break;
      case rtStubs:  TableName = "Stubs, StubsDataEntry"; break;
      case rtDocs:   TableName = "Documents, DocumentsDataEntry"; break;
  //    case rtDetail: TableName = "Checks, ChecksDataEntry, Stubs, StubsDataEntry, Documents, DocumentsDataEntry"; break;
      default: TableName = CurrentTable;
    }
    Where = GetWhereClause();
    if (Where.IsEmpty())
      ShowMessage("Nothing to test.");
    else {
      if (Where.Pos("@")){
        if(!GetParameters(Where))
          return;
      }
      DataSet->CommandText = "Select * from " + TableName + Where + " and 1 = 2";
      try {
        DataSet->Open();
        DoMessage("The statement is valid.");
      }
      catch(...){
        DoError("Select * from " + TableName + Where + " is not a valid SQL statement");
      }
      DataSet->Close();
    }
  }
  DMPCATCHTHROW("TRecordForm::TestWhereBtnClick")
}

//---------------------------------------------------------------------------
bool __fastcall TRecordForm::GetParameters(String &Text)
{
  bool bResult = true;
  DMPTRY {
    TStringList *ValList = new TStringList;
    if (TabControl1->TabIndex == rtFile)
      ValList->Assign(FileGrid->Cols[goFormat]);
    else
      ValList->Assign(FieldGrid->Cols[goFormat]);
    ValList->Delete(0);
    Parameters.EraseParameters(TempFile);
    Parameters.Clear();
    Parameters.AddFieldsFromList(ValList);
    ValList->Clear();
    if (TabControl1->TabIndex > rtFile){
      ValList->Assign(LimitGrid->Cols[goValue]);
      ValList->Delete(0);
      Parameters.AddFieldsFromList(ValList);
    }
    String tmp = "";
    ValList->Clear();
    Parameters.RetrieveNames(ValList);
    if (ValList->Count){
      ParameterDlg = new TParameterDlg(this);
      if (ParameterDlg->Execute(ValList, tmp, false) != mrOk)
        bResult = false;
      delete ParameterDlg;
      if (bResult){
        Parameters.SetFieldValues(ValList);
        Parameters.WriteParameters(TempFile);
        if (TabControl1->TabIndex > rtFile)
          Parameters.SubstituteParameters(Text, true);
      }
    }
    else
      bResult = true;
    delete ValList;
  }
  DMPCATCHTHROW("TRecordForm::GetParameters")
  return bResult;
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::LimitGridCellChanging(TObject *Sender, int OldRow, int OldCol, int NewRow, int NewCol, bool &Allow)
{
  DMPTRY {
    if (OldCol == goValue){
      String Value = LimitGrid->Cells[goValue][OldRow];
      String FieldName = LimitGrid->Cells[goFieldName][OldRow];
      if (CodeMapper.IsCoded(FieldName) && Value.Pos("="))
        LimitGrid->Cells[goValue][OldRow] = Value.SubString(1, Value.Pos("=")-1);
    }
  }
  DMPCATCHTHROW("TRecordForm::LimitGridCellChanging")
}


//---------------------------------------------------------------------------
//GENERAL FUNCTIONALITY
//---------------------------------------------------------------------------
void __fastcall TRecordForm::DoError(String Error)
{
  DMPTRY {
    MessageDlg(Error, mtError, TMsgDlgButtons()<<mbOK, 0);
  }
  DMPCATCHTHROW("TRecordForm::DoError")
}

//---------------------------------------------------------------------------
void __fastcall TRecordForm::DoMessage(String Message)
{
  DMPTRY {
    MessageDlg(Message, mtInformation, TMsgDlgButtons()<<mbOK, 0);
  }
  DMPCATCHTHROW("TRecordForm::DoMessage")
}

//---------------------------------------------------------------------------

void __fastcall TRecordForm::RepeatLimitExit(TObject *Sender)
{
  DMPTRY {
    String TempString = RepeatLimit->Text.Trim();
    String TempString2;
    if (!TempString.IsEmpty()){
      for (int i = 0; i < TempString.Length(); ++i)
        if (TempString[i+1] >= '0' && TempString[i+1] <= '9')
          TempString2 += TempString[i+1];
      TempString = TempString2;
      if (!TempString.IsEmpty())
        TempString = String(TempString.ToInt());
    }
    if (!TempString.IsEmpty())
      if (TempString == "0")
        TempString = "";
    RepeatLimit->Text = TempString;
    if (!CurrentRecord.IsEmpty()){
      int Index = Records->IndexOfName(CurrentRecord);
      if (Index >= 0){
        int Value = StrToIntDef(Records->Values[Records->Names[Index]],0);
        Records->Values[Records->Names[Index]] = TRecHelper::SetRepeatLimitValue(Value, TempString.IsEmpty() ? 0 : TempString.ToInt());
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::RepeatLimitExit")
}
//---------------------------------------------------------------------------

// CR 5815 03/03/2004 DJK
void __fastcall TRecordForm::btnEditImageFileNamesClick(TObject *Sender)
{
  DMPTRY {
    TImageFileFormatDialog* dlg = new TImageFileFormatDialog(this);

    dlg->ProcDateFormat      = m_sImageFileProcDateFormat;
    dlg->ZeroPad             = m_bImageFileZeroPad;
    dlg->BatchTypeFormatFull = m_bImageFileBatchTypeFormatFull;
    dlg->CombineProcDates    = CombineImagesAcrossProcDatesCheckBox->Checked;
    dlg->FileNameSingle      = m_sImageFileNameSingle;
    dlg->FileNamePerTran     = m_sImageFileNamePerTran;
    dlg->FileNamePerBatch    = m_sImageFileNamePerBatch;

    if (dlg->ShowModal() == mrOk) {
      m_sImageFileProcDateFormat      = dlg->ProcDateFormat     ;
      m_bImageFileZeroPad             = dlg->ZeroPad            ;
      m_bImageFileBatchTypeFormatFull = dlg->BatchTypeFormatFull;
      m_sImageFileNameSingle          = dlg->FileNameSingle     ;
      m_sImageFileNamePerTran         = dlg->FileNamePerTran    ;
      m_sImageFileNamePerBatch        = dlg->FileNamePerBatch   ;
      RedrawImageFileNames();
    }

    delete dlg;
  }
  DMPCATCHTHROW("TRecordForm::btnEditImageFileNamesClick")
}
//---------------------------------------------------------------------------

// CR 5815 03/03/2004 DJK
void __fastcall TRecordForm::FormShow(TObject *Sender)
{
  DMPTRY {
    m_iLastComboBoxJointDetailLevel = 0;
    comboBoxJointDetailLevel->ItemIndex = m_iLastComboBoxJointDetailLevel;
    ShowDetailComboBox(false);

    for (int i = 0; i < 3; ++i) {
      TEdit*      edit;
      TTokenEdit* tokenEdit;
      switch (i) {
        case 0: edit      = eReplaceableImageFileNameSingle;
                tokenEdit = m_teImageFileNameDisplaySingle;
                break;
        case 1: edit      = eReplaceableImageFileNamePerTran;
                tokenEdit = m_teImageFileNameDisplayPerTran;
                break;
        case 2: edit      = eReplaceableImageFileNamePerBatch;
                tokenEdit = m_teImageFileNameDisplayPerBatch;
                break;
      }
      edit->Visible = false;
      tokenEdit->Parent          = edit->Parent;
      tokenEdit->Left            = edit->Left;
      tokenEdit->Top             = edit->Top;
      tokenEdit->Width           = edit->Width;
      tokenEdit->Anchors         = edit->Anchors;
      tokenEdit->VisibleBorder   = false;
      tokenEdit->BackgroundColor = clBtnFace;
      tokenEdit->Visible         = true;
    }

    m_sImageFileNameSingle   = TTokenManager::GenerateDefaultEncodedFileName(CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftSingle  );
    m_sImageFileNamePerTran  = TTokenManager::GenerateDefaultEncodedFileName(CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftPerTran );
    m_sImageFileNamePerBatch = TTokenManager::GenerateDefaultEncodedFileName(CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftPerBatch);

    RedrawImageFileNames();
  }
  DMPCATCHTHROW("TRecordForm::FormShow")
}
//---------------------------------------------------------------------------

// CR 5815 03/03/2004 DJK
void TRecordForm::RedrawImageFileNames()
{
  DMPTRY {
    m_teImageFileNameDisplaySingle  ->ProcDateFormat      = m_sImageFileProcDateFormat;
    m_teImageFileNameDisplayPerTran ->ProcDateFormat      = m_sImageFileProcDateFormat;
    m_teImageFileNameDisplayPerBatch->ProcDateFormat      = m_sImageFileProcDateFormat;
    m_teImageFileNameDisplaySingle  ->ZeroPad             = m_bImageFileZeroPad;
    m_teImageFileNameDisplayPerTran ->ZeroPad             = m_bImageFileZeroPad;
    m_teImageFileNameDisplayPerBatch->ZeroPad             = m_bImageFileZeroPad;
    m_teImageFileNameDisplaySingle  ->BatchTypeFormatFull = m_bImageFileBatchTypeFormatFull;
    m_teImageFileNameDisplayPerTran ->BatchTypeFormatFull = m_bImageFileBatchTypeFormatFull;
    m_teImageFileNameDisplayPerBatch->BatchTypeFormatFull = m_bImageFileBatchTypeFormatFull;

    m_sImageFileNameSingle   = TTokenManager::ValidateFileNameTemplate(m_sImageFileNameSingle  , CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftSingle  );
    m_sImageFileNamePerTran  = TTokenManager::ValidateFileNameTemplate(m_sImageFileNamePerTran , CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftPerTran );
    m_sImageFileNamePerBatch = TTokenManager::ValidateFileNameTemplate(m_sImageFileNamePerBatch, CombineImagesAcrossProcDatesCheckBox->Checked, TTokenManager::iftPerBatch);

    m_teImageFileNameDisplaySingle  ->EncodedString = m_sImageFileNameSingle  ;
    m_teImageFileNameDisplayPerTran ->EncodedString = m_sImageFileNamePerTran ;
    m_teImageFileNameDisplayPerBatch->EncodedString = m_sImageFileNamePerBatch;

    String sExtension = "";

    //CR9916 10/13/2004 ALH
     if (ImageFileFormatRadioGroup->ItemIndex != 0){  //CR9916 10/13/2004 ALH
        int iImageFormat =  ImageFileFormatRadioGroup->ItemIndex;
        switch  (iImageFormat){  //CR9916 10/13/2004 ALH
            case 0:
                    sExtension = "";
            break;
            case 1:
                    sExtension = ".Tif";
            break;
            case 2:
                    sExtension = ".Pdf";
            break;
            case 3:
                    sExtension = ".Tif";
                    m_bEmbedImageInDataFile = true;
            break;
            default:
                    throw DMPException("Illegal Case selecting image format.");
        }

        EditToken::SuspendIllegalCharacterCheck();
        m_teImageFileNameDisplaySingle  ->InsertText(sExtension);
        EditToken::SuspendIllegalCharacterCheck();
        m_teImageFileNameDisplayPerTran ->InsertText(sExtension);
        EditToken::SuspendIllegalCharacterCheck();
        m_teImageFileNameDisplayPerBatch->InsertText(sExtension);
    }
    else {
        m_teImageFileNameDisplaySingle->EncodedString   = "NONE";
        m_teImageFileNameDisplayPerTran->EncodedString  = "NONE";
        m_teImageFileNameDisplayPerBatch->EncodedString = "NONE";
    }
  }
  DMPCATCHTHROW("TRecordForm::RedrawImageFileNames")
}

void TRecordForm::ShowDetailComboBox(const bool bShow)
{
  DMPTRY {
    bool bShowing = comboBoxJointDetailLevel->Height > 0;
    lblDataFields->Left  = 1;
    lblDataFields->Top   = 1;
    lblDataFields->Width = FieldsPanel->Width-2;
    comboBoxJointDetailLevel->Left  = 1;
    comboBoxJointDetailLevel->Top   = lblDataFields->Top + lblDataFields->Height;
    comboBoxJointDetailLevel->Width = FieldsPanel->Width-2;
    if (bShowing != bShow)
      comboBoxJointDetailLevel->Height = bShow ? 21 : 0;
    FieldLB->Left   = 1;
    FieldLB->Top    = comboBoxJointDetailLevel->Top + comboBoxJointDetailLevel->Height;
    FieldLB->Width  = FieldsPanel->Width-2;
    FieldLB->Height = AddDataBtn->Top - FieldLB->Top - 5;
  }
  DMPCATCHTHROW("TRecordForm::ShowDetailComboBox")
}

void TRecordForm::DetermineVisibilityOfAggregates()
{
  DMPTRY {
    // CR22051: Making Aggregates listbox visible and available also for the Transactions table.
    bool bVisible = comboBoxJointDetailLevel->Height == 0 || ((comboBoxJointDetailLevel->ItemIndex + 2) >= rtTransactions && DisplayLevelCombo->ItemIndex == (DisplayLevelCombo->Items->Count-1));
    Label22         ->Visible = bVisible;
    AggregateFieldLB->Visible = bVisible;
    AddAggregateBtn ->Visible = bVisible;
  }
  DMPCATCHTHROW("TRecordForm::DetermineVisibilityOfAggregates")
}

// CR 14584 06/01/2006 DJK
int TRecordForm::GetRecordType(const String& sRecordName) const
{
  int iResult;
  DMPTRY {
    int iIndex = Records->IndexOfName(sRecordName);
    if (iIndex == -1)
      throw DMPException("The string '"+sRecordName+"' could not be found in the 'Records' array");
    iResult = StrToIntDef(Records->Values[sRecordName],-1);
  }
  DMPCATCHTHROW("TRecordForm::GetRecord")
  return iResult;
}

// CR 5815 03/03/2004 DJK
void __fastcall TRecordForm::ImageFileFormatRadioGroupClick(TObject *Sender)
{
  DMPTRY {
    RedrawImageFileNames();
  }
  DMPCATCHTHROW("TRecordForm::ImageFileFormatRadioGroupClick")
}
//---------------------------------------------------------------------------

// CR 5815 03/03/2004 DJK
void __fastcall TRecordForm::CombineImagesAcrossProcDatesCheckBoxClick(TObject *Sender)
{
  DMPTRY {
    RedrawImageFileNames();
  }
  DMPCATCHTHROW("TRecordForm::CombineImagesAcrossProcDatesCheckBoxClick")
}
//---------------------------------------------------------------------------
// CR 7411 ~ DLAQAB ~ 3-25-2004
void __fastcall TRecordForm::DocumentChanged(TObject *Sender)
{
   m_bDocumentHasChanged = true;
}
//---------------------------------------------------------------------------
// CR 8798 7/19/04 MLT - added IgnoreOccurs checkbox to allow user
// to bypass the occcurs logic for a given detail record type
void __fastcall TRecordForm::IgnoreOccursClick(TObject *Sender)
{
  DMPTRY
  {
    if (!IgnoreOccurs->Checked && FieldGrid->ColCount == 9)
    {
      FieldGrid->InsertCols(9, 2);
      NoteLbl->Top = NoteLbl->Top + 261;
      FieldGrid->ColumnHeaders->Add("Occrs Grp");
      FieldGrid->ColumnHeaders->Add("Occrs Cnt");
      FieldGrid->ColWidths[ 9] = 55;
      FieldGrid->ColWidths[10] = 56;
      FieldGrid->Width = FieldGrid->Width + 111;
      MaxRepeatsLabel->Top = FieldGrid->Top;
      MaxRepeatsLabel->Left = FieldGrid->Left+FieldGrid->Width+6;
      RepeatLimit->Top = MaxRepeatsLabel->Top+18;
      RepeatLimit->Left = MaxRepeatsLabel->Left;
      MaxRepeatsLabel->Visible = true;
      RepeatLimit->Visible = true;
    }
    else
    {
      if (FieldGrid->ColCount == 11)
      {
        RepeatLimit->Text = "";
        MaxRepeatsLabel->Visible = false;
        RepeatLimit->Visible = false;
        FieldGrid->RemoveCols(9, 2);
        FieldGrid->Width = FieldGrid->Width - 111;
        NoteLbl->Top = NoteLbl->Top - 261;

        for (int i = 1; i < FieldGrid->RowCount; i++)
        {
          FieldGrid->Cells[9][i] = "";
          FieldGrid->Cells[10][i] = "";
        }
      }
    }
    if (!CurrentRecord.IsEmpty()){
      int Index = Records->IndexOfName(CurrentRecord);
      if (Index >= 0){
        int Value = StrToIntDef(Records->Values[Records->Names[Index]],0);
        Records->Values[Records->Names[Index]] = TRecHelper::SetRecordIgnoreOccurs(Value, IgnoreOccurs->Checked);
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::IgnoreOccursClick")
}

// CR 7498 7/20/04 MLT - filled in implementation for this function,
// using same code as in RepeatLimitExit
void __fastcall TRecordForm::RepeatLimitChange(TObject *Sender)
{
  DMPTRY {
    String TempString = RepeatLimit->Text.Trim();
    String TempString2;
    if (!TempString.IsEmpty()){
      for (int i = 0; i < TempString.Length(); ++i)
        if (TempString[i+1] >= '0' && TempString[i+1] <= '9')
          TempString2 += TempString[i+1];
      TempString = TempString2;
      if (!TempString.IsEmpty())
        TempString = String(TempString.ToInt());
    }
    if (!TempString.IsEmpty())
      if (TempString == "0")
        TempString = "";
    RepeatLimit->Text = TempString;
    if (!CurrentRecord.IsEmpty()){
      int Index = Records->IndexOfName(CurrentRecord);
      if (Index >= 0){
        int Value = StrToIntDef(Records->Values[Records->Names[Index]],0);
        Records->Values[Records->Names[Index]] = TRecHelper::SetRepeatLimitValue(Value, TempString.IsEmpty() ? 0 : TempString.ToInt());
      }
    }
  }
  DMPCATCHTHROW("TRecordForm::RepeatLimitChange")
}
//CR10943 03/30/2005 ALH  - New Function
//---------------------------------------------------------------------------
void __fastcall TRecordForm::UsePostDLLCheckBOXClick(TObject *Sender)
{
        DMPTRY
        {
                PostDLLEnableCheck();
        }
        DMPCATCHTHROW("TRecordForm::UsePostDLLCheckBOXClick")
}
//CR10943 03/30/2005 ALH  - New Function
//---------------------------------------------------------------------------
void __fastcall TRecordForm::PostDLLEnableCheck()
{
        DMPTRY
        {
                if (UsePostDLLCheckBOX->State == cbChecked)
                {
                       PostDLLPathFileNameEdit->Enabled          = true;
                       DLLBrowsePathFileNameButton->Enabled      = true;
                       PostDLLLabel->Enabled                     = true;
                }
                else
                {
                       PostDLLPathFileNameEdit->Enabled          = false;
                       DLLBrowsePathFileNameButton->Enabled      = false;
                       PostDLLLabel->Enabled                     = false;
                }
        }
        DMPCATCHTHROW("TRecordForm::PostDLLEnableCheck")
}
//CR10943 03/30/2005 ALH  - New Function
//---------------------------------------------------------------------------
void __fastcall TRecordForm::DLLBrowsePathFileNameButtonClick(TObject *Sender)
{
        DMPTRY
        {
              TOpenDialog* OpenFileDlg = new TOpenDialog(this);
              if (OpenFileDlg->Execute())
                PostDLLPathFileNameEdit->Text = OpenFileDlg->FileName;
              delete OpenFileDlg;
              ImagePathEditExit(ImagePathEdit);
        }
        DMPCATCHTHROW("TRecordForm::DLLBrowsePathFileNameButtonClick")
}
//---------------------------------------------------------------------------
void __fastcall TRecordForm::comboBoxJointDetailLevelChange(TObject *Sender)
{
  DMPTRY {
    int iType = comboBoxJointDetailLevel->ItemIndex+2;
    if ((DisplayLevelCombo->ItemIndex < DisplayLevelCombo->Items->Count -1) && (DisplayLevelCombo->ItemIndex + 1 < iType)){
      DoError("You must select fields from tables higher than or equal to the display level.");
      comboBoxJointDetailLevel->ItemIndex = m_iLastComboBoxJointDetailLevel;
    }
    else {
      LoadTableData(iType, true);
      m_iLastComboBoxJointDetailLevel = iType-2;
      DetermineVisibilityOfAggregates();
    }
  }
  DMPCATCHTHROW("TRecordForm::comboBoxJointDetailLevelChange")
}
//---------------------------------------------------------------------------


