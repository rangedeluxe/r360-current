//----------------------------------------------------------------------------
#ifndef SetupRptH
#define SetupRptH
//----------------------------------------------------------------------------
#include <vcl\Classes.hpp>
#include <vcl\Controls.hpp>
#include <vcl\StdCtrls.hpp>
#include <vcl\Forms.hpp>
#include <vcl\QuickRpt.hpp>
#include <vcl\QRCtrls.hpp>
#include <Qrctrls.hpp>
#include <ExtCtrls.hpp>
#include <ADODB.hpp>
#include <Db.hpp>
#include "Record.h"
//----------------------------------------------------------------------------

// CR 14584 06/01/2006 DJK

class RecordData
{
private:
  String m_sRecordDataName;
  String m_sOrderDataName;
  String m_sLimitDataName;
public:
  RecordData(const String& sRecordDataName, const String& sOrderDataName, const String& sLimitDataName) {m_sRecordDataName = sRecordDataName; m_sOrderDataName = sOrderDataName; m_sLimitDataName = sLimitDataName;}
  __property String RecordDataName = {read=m_sRecordDataName};
  __property String OrderDataName  = {read=m_sOrderDataName };
  __property String LimitDataName  = {read=m_sLimitDataName };
};

class TSetupReport : public TQuickRep
{
__published:
    TQRBand *QRBand_Title;
    TQRLabel *lblTitle;
    TQRShape *QRShape1;
    TQRLabel *QRLabel1;
    TQRLabel *lblExtractFileLocation;
    TQRLabel *QRLabel2;
    TQRLabel *lblLogFilePath;
    TQRLabel *QRLabel3;
    TQRLabel *lblImageFilePath;
    TQRLabel *QRLabel4;
    TQRLabel *lblPostProcessingDLL;
    TQRLabel *QRLabel10;
    TQRLabel *lblWriteTimestampTo;
    TQRShape *QRShape2;
    TQRLabel *QRLabel5;
    TQRLabel *lblImageFileFormat;
    TQRLabel *QRLabel6;
    TQRLabel *lblCombineImagesFromMultipleProcessingDates;
    TQRLabel *QRLabel7;
    TQRLabel *QRLabel8;
    TQRLabel *QRLabel9;
    TQRLabel *lblFileNameSingle;
    TQRLabel *lblFileNamePerTran;
    TQRLabel *lblFileNamePerBatch;
    TQRShape *QRShape4;
    TQRLabel *QRLabel15;
    TQRLabel *QRLabel16;
    TQRLabel *QRLabel17;
    TQRLabel *QRLabel18;
    TQRLabel *QRLabel19;
    TQRLabel *lblFieldDelimiter;
    TQRLabel *lblRecordDelimiter;
    TQRLabel *lblOmitPadding;
    TQRLabel *lblEncloseFieldDataInQuotes;
    TQRLabel *lblShowNullAsSpace;
    TQRStringsBand *QRStringsBand_RecordSummary;
    TQRMemo *memoRecords;
    TQRBand *PageFooterBand1;
    TQRSysData *QRSysData1;
    TQRLabel *QRLabel20;
    TQRLabel *QRLabel21;
    TQRStringsBand *QRStringsBand_Records;
    TQRLabel *QRLabel22;
    TQRLabel *lblRecord;
    TQRStringsBand *QRStringsBand_NormalRecordFields;
    TQRShape *boxFieldName;
    TQRShape *boxDisplayName;
    TQRShape *boxColumnWidth;
    TQRShape *boxPadChar;
    TQRShape *boxJustify;
    TQRShape *boxQuotes;
    TQRShape *boxFormat;
    TQRLabel *lblFieldName;
    TQRLabel *lblDisplayName;
    TQRLabel *lblColumnWidth;
    TQRLabel *lblPadChar;
    TQRLabel *lblJustify;
    TQRLabel *lblQuotes;
    TQRLabel *lblFormat;
    TQRStringsBand *QRStringsBand_DetailRecordFields;
    TQRStringsBand *QRStringsBand_NormalRecord;
    TQRStringsBand *QRStringsBand_DetailRecord;
    TQRShape *box_D_FieldName;
    TQRShape *box_D_DisplayName;
    TQRShape *box_D_ColWidth;
    TQRShape *box_D_PadChar;
    TQRShape *box_D_Justify;
    TQRShape *box_D_Quotes;
    TQRShape *box_D_Format;
    TQRShape *box_D_OccrsGrp;
    TQRShape *box_D_OccrsCnt;
    TQRLabel *lbl_D_FieldName;
    TQRLabel *lbl_D_DisplayName;
    TQRLabel *lbl_D_ColumnWidth;
    TQRLabel *lbl_D_PadChar;
    TQRLabel *lbl_D_Justify;
    TQRLabel *lbl_D_Quotes;
    TQRLabel *lbl_D_Format;
    TQRLabel *lbl_D_OccrsGrp;
    TQRLabel *lbl_D_OccrsCnt;
    TQRShape *QRShape5;
    TQRShape *QRShape6;
    TQRShape *QRShape7;
    TQRShape *QRShape8;
    TQRShape *QRShape9;
    TQRShape *QRShape10;
    TQRShape *QRShape11;
    TQRLabel *QRLabel25;
    TQRLabel *QRLabel40;
    TQRLabel *QRLabel26;
    TQRLabel *QRLabel27;
    TQRLabel *QRLabel28;
    TQRLabel *QRLabel29;
    TQRLabel *QRLabel30;
    TQRShape *QRShape12;
    TQRShape *QRShape13;
    TQRShape *QRShape14;
    TQRShape *QRShape15;
    TQRShape *QRShape16;
    TQRShape *QRShape17;
    TQRShape *QRShape18;
    TQRShape *QRShape19;
    TQRShape *QRShape20;
    TQRLabel *QRLabel23;
    TQRLabel *QRLabel32;
    TQRLabel *QRLabel33;
    TQRLabel *QRLabel34;
    TQRLabel *QRLabel35;
    TQRLabel *QRLabel36;
    TQRLabel *QRLabel37;
    TQRLabel *QRLabel38;
    TQRLabel *QRLabel39;
    TQRLabel *lblIgnoreOccursColumns_Label;
    TQRLabel *lblIgnoreOccursColumns;
    TQRStringsBand *QRStringsBand_OrderData;
    TQRStringsBand *QRStringsBand_OrderDataFields;
    TQRStringsBand *QRStringsBand_LimitData;
    TQRStringsBand *QRStringsBand_LimitDataFields;
    TQRShape *QRShape21;
    TQRShape *QRShape22;
    TQRLabel *QRLabel14;
    TQRLabel *QRLabel24;
    TQRShape *QRShape23;
    TQRShape *QRShape24;
    TQRShape *QRShape25;
    TQRShape *QRShape26;
    TQRShape *QRShape27;
    TQRShape *QRShape28;
    TQRLabel *QRLabel31;
    TQRLabel *QRLabel41;
    TQRLabel *QRLabel42;
    TQRLabel *QRLabel43;
    TQRLabel *QRLabel44;
    TQRLabel *QRLabel45;
    TQRShape *box_OD_FieldName;
    TQRShape *box_OD_AscDesc;
    TQRLabel *lbl_OD_FieldName;
    TQRLabel *lbl_OD_AscDesc;
    TQRShape *box_LD_FieldName;
    TQRShape *box_LD_LogicalOpr;
    TQRShape *box_LD_Value;
    TQRShape *box_LD_Left;
    TQRShape *box_LD_Right;
    TQRShape *box_LD_OprOnNextRow;
    TQRLabel *lbl_LD_FieldName;
    TQRLabel *lbl_LD_LogicalOpr;
    TQRLabel *lbl_LD_Value;
    TQRLabel *lbl_LD_Left;
    TQRLabel *lbl_LD_Right;
    TQRLabel *lbl_LD_OprOnNextRow;
    TQRLabel *QRLabel46;
    TQRLabel *QRLabel47;
    TQRStringsBand *QRStringsBand_Records_Sub2;
    TQRStringsBand *QRStringsBand_Records_Sub3;
    TQRLabel *lblMaxRepeats_Label;
    TQRLabel *lblMaxRepeats;
    TQRStringsBand *QRStringsBand_Records_Sub1;
    TQRStringsBand *QRStringsBand_Records_Sub4;
    TQRLabel *QRLabel48;
    TQRLabel *lblLevel;
    void __fastcall QuickRepPreview(TObject *Sender);
    void __fastcall QuickRepBeforePrint(TCustomQuickRep *Sender, bool &PrintReport);
    void __fastcall QRStringsBand_RecordsBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_NormalRecordFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_NormalRecordBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_DetailRecordBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_DetailRecordFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_OrderDataBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_LimitDataBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_OrderDataFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
    void __fastcall QRStringsBand_LimitDataFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand);
private:
    TRecordForm* m_recordForm;
    void RecursivelyDetermineWhichRecordsUseOrderLimits(TTreeNode* currentNode, TStringList* slOrderLimitsUsedByRecords);
    void RecursivelySetupRecords(TStrings* strings, TTreeNode* currentNode, TStringList* slOrderLimitsUsedOverall, const TStringList* slOrderLimitsUsedByRecords);
    bool DetermineExistenceOfOrderLimitData(const int iRecordSubType, String& sOrderDataName, String& sLimitDataName, TStringList* slOrderLimitDataTablesUsed, const TStringList* slOrderLimitDataToAvoid);
    void DetermineLevelNameAndSubType(const String& sRecordHeaderName, String& sRecordDataName, int& iSubType);
    void RecursivelyPopulateMemo(TQRMemo* memoRecords, TTreeNode* currentNode);
    String       m_sRecordName;
    int          m_iRecordTypeSpecific;
    int          m_iRecordTypeGeneral;
    int          m_iRecordSubType;
    TStringList* m_slRecordData;
    TStringList* m_slOrderData;
    TStringList* m_slLimitData;
    TIniFile*    m_scriptFile;
public:
   __fastcall TSetupReport::TSetupReport(TComponent* Owner);
   __fastcall TSetupReport::~TSetupReport();
   __property TRecordForm* RecordForm = {write=m_recordForm};
};
//----------------------------------------------------------------------------
extern TSetupReport *SetupReport;
//----------------------------------------------------------------------------
#endif