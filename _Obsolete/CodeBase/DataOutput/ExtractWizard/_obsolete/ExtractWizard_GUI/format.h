//---------------------------------------------------------------------------

#ifndef formatH
#define formatH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TFormatDlg : public TForm
{
__published:	// IDE-managed Components
        TPageControl *PageControl;
        TPanel *ButtonPanel;
        TTabSheet *DateTab;
        TTabSheet *CurrencyTab;
        TTabSheet *AlphaTab;
        TListBox *DateFormatLB;
        TLabel *Label1;
        TLabel *Label2;
        TListBox *DateExampleLB;
        TLabel *Label3;
        TLabel *Label6;
        TListBox *FloatFormatLB;
        TLabel *Label7;
        TListBox *FloatExampleLB;
        TLabel *Label8;
        TLabel *Label12;
        TEdit *FormatEdit;
        TLabel *Label13;
        TLabel *Label14;
        TEdit *ValueEdit;
        TButton *TestBtn;
        TLabel *Label15;
        TEdit *ResultEdit;
        TLabel *Label16;
        TEdit *FloatValueEdit;
        TLabel *Label10;
        TEdit *FloatResultEdit;
        TButton *TestFloatBtn;
        TLabel *Label4;
        TEdit *DateResultEdit;
        TButton *DateTestBtn;
        TButton *OKBtn;
        TButton *CancelBtn;
        TEdit *DateFormatEdit;
        TEdit *FloatFormatEdit;
    TCheckBox *OverpunchPositiveCB;
    TCheckBox *OverpunchNegativeCB;
        void __fastcall FormCloseQuery(TObject *Sender, bool &CanClose);
        void __fastcall DateTestBtnClick(TObject *Sender);
        void __fastcall DateFormatLBClick(TObject *Sender);
        void __fastcall TestFloatBtnClick(TObject *Sender);
        void __fastcall FloatFormatLBClick(TObject *Sender);
        void __fastcall TestBtnClick(TObject *Sender);
        void __fastcall FloatExampleLBClick(TObject *Sender);
        void __fastcall DateExampleLBClick(TObject *Sender);
    void __fastcall FloatSetOverpunch( void );
    void __fastcall FloatGetOverpunch( void );
    void __fastcall OverpunchPositiveCBClick(TObject *Sender);
    void __fastcall OverpunchNegativeCBClick(TObject *Sender);
private:	// User declarations
        String Format;
        void __fastcall ClearAll();
public:		// User declarations
        __fastcall TFormatDlg(TComponent* Owner);
        TModalResult __fastcall Execute(int Type, String FormatStr);
        String __fastcall GetFormatString();
};
//---------------------------------------------------------------------------
extern PACKAGE TFormatDlg *FormatDlg;
//---------------------------------------------------------------------------
#endif
