//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "HandleIni.h"
#include "BatchTrace.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DMPADOQuery"
#pragma link "DMPADODataSet"
#pragma resource "*.dfm"
TBatchTraceDlg *BatchTraceDlg;
//---------------------------------------------------------------------------
__fastcall TBatchTraceDlg::TBatchTraceDlg(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    TIniHandler Ini;
    Ini.Execute();
    if (TimeQuery->Active)
      TimeQuery->Close();
    String ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=True;";
    if (!Ini.Server.IsEmpty())
      ConnectionString += "Data Source=" + Ini.Server + ";";
    ConnectionString += "Initial Catalog=" + Ini.ADODB + ";";
    ConnectionString += "Application Name=" + Application->Title + ";";
    ConnectionString += "User ID=" + Ini.DBUserName +";";
    ConnectionString += "Password=" + Ini.DBPassword + ";";
    TimeQuery->ConnectionString = ConnectionString;
    BatchQuery->ConnectionString = ConnectionString;
  }
  DMPCATCHTHROW("TBatchTraceDlg::TBatchTraceDlg")
}

//---------------------------------------------------------------------------
void __fastcall TBatchTraceDlg::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    BatchQuery->Close();
    TimeQuery->Close();
  }
  DMPCATCHTHROW("TBatchTraceDlg::FormClose")
}

//---------------------------------------------------------------------------
void __fastcall TBatchTraceDlg::MakeTimeStampList(String FieldName, TStrings *Items)
{
  DMPTRY {
    Items->Clear();
    TimeQuery->CommandText = "Select distinct " + FieldName + " from BatchTrace where " + FieldName + " is not NULL order by " + FieldName + " DESC";
    TimeQuery->Open();
    while (!TimeQuery->Eof){
      Items->Add(TimeQuery->FieldByName(FieldName)->AsString);
      TimeQuery->Next();
    }
    TimeQuery->Close();
    if (Items->Count == 1 && Items->Strings[0].IsEmpty())
      Items->Delete(0);
  }
  DMPCATCHTHROW("TBatchTraceDlg::MakeTimeStampList")
}

//---------------------------------------------------------------------------
TModalResult __fastcall TBatchTraceDlg::Execute(String FieldName, String tsTime)
{
  TModalResult mrResult;
  DMPTRY {
    FieldLbl->Caption = FieldName + " " + tsTime;
    BatchQuery->CommandText = "Select BankID, LockboxID, BatchID, ProcessingDate, CheckCount, "
          "CheckAmount from Batch where GlobalBatchID in (Select GlobalBatchID from BatchTrace "
          "where " + FieldName + " = " + QuotedStr(tsTime) + ") order by BankID, LockboxID, BatchID";
    BatchQuery->Open();
    mrResult = ShowModal();
  }
  DMPCATCHTHROW("TBatchTraceDlg::Execute")
  return mrResult;
}


//---------------------------------------------------------------------------
