//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "ImageFileFormatDlg.h"
#include "DMPExceptions.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TImageFileFormatDialog *ImageFileFormatDialog;
//---------------------------------------------------------------------------
__fastcall TImageFileFormatDialog::TImageFileFormatDialog(TComponent* Owner)
  : TForm(Owner)
{
  DMPTRY {
    FProcDateFormat      = "MMDDYY";
    FZeroPad             = false;
    FBatchTypeFormatFull = true;
    FCombineProcDates    = false;
    m_vTokenEdits.push_back(new TTokenEdit(this));
    m_vTokenEdits.push_back(new TTokenEdit(this));
    m_vTokenEdits.push_back(new TTokenEdit(this));
  }
  DMPCATCHTHROW("TImageFileFormatDialog::TImageFileFormatDialog")
}
//---------------------------------------------------------------------------

String TImageFileFormatDialog::EditTokenTypeToString(const EditToken::EditTokenType editTokenType) const
{
  String sResult;
  DMPTRY {
    switch (editTokenType) {
      case EditToken::ettBank        : sResult = "Bank"           ; break;
      case EditToken::ettLockbox     : sResult = "Lockbox"        ; break;
      case EditToken::ettBatch       : sResult = "Batch"          ; break;
      case EditToken::ettBatchType   : sResult = "Batch Type"     ; break;
      case EditToken::ettProcDate    : sResult = "Processing Date"; break;
      case EditToken::ettTransaction : sResult = "Transaction"    ; break;
      case EditToken::ettDocumentType: sResult = "Document Type"  ; break;
      case EditToken::ettFileCounter : sResult = "File Counter"   ; break;
      default: throw DMPException("Illegal case");
    }
  }
  DMPCATCHTHROW("TImageFileFormatDialog::EditTokenTypeToString")
  return sResult;
}
//---------------------------------------------------------------------------

EditToken::EditTokenType TImageFileFormatDialog::StringToEditTokenType(const String& sEditTokenTypeName) const
{
  EditToken::EditTokenType ettResult = EditToken::ettNone;
  DMPTRY {
    if (sEditTokenTypeName == "Bank")
      ettResult = EditToken::ettBank;
    else
      if (sEditTokenTypeName == "Lockbox")
        ettResult = EditToken::ettLockbox;
      else
        if (sEditTokenTypeName == "Batch")
          ettResult = EditToken::ettBatch;
        else
          if (sEditTokenTypeName == "Batch Type")
            ettResult = EditToken::ettBatchType;
          else
            if (sEditTokenTypeName == "Processing Date")
              ettResult = EditToken::ettProcDate;
            else
              if (sEditTokenTypeName == "Transaction")
                ettResult = EditToken::ettTransaction;
              else
                if (sEditTokenTypeName == "Document Type")
                  ettResult = EditToken::ettDocumentType;
                else
                  if (sEditTokenTypeName == "File Counter")
                    ettResult = EditToken::ettFileCounter;
                  else
                    throw DMPException("Unrecognized edit token type name '"+sEditTokenTypeName+"'");
  }
  DMPCATCHTHROW("TImageFileFormatDialog::StringToEditTokenType")
  return ettResult;
}
//---------------------------------------------------------------------------

void TImageFileFormatDialog::PopulateListBox(TListBox* listBox, const std::vector<int>& vTokenTypes)
{
  DMPTRY {
    listBox->Clear();
    for (int i = 0; i < (int) vTokenTypes.size(); ++i)
      listBox->Items->Add(EditTokenTypeToString((EditToken::EditTokenType) vTokenTypes[i]));
    listBox->ItemIndex = 0;
  }
  DMPCATCHTHROW("TImageFileFormatDialog::PopulateListBox")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::pnlButtonsResize(TObject *Sender)
{
  DMPTRY {
    int iWidthToUse = (pnlButtons->Width - btnOk->Width - btnCancel->Width - 13)/2;
    btnOk->Left = iWidthToUse;
    btnCancel->Left = pnlButtons->Width - iWidthToUse - btnCancel->Width;
    int iTopToUse = (pnlButtons->Height - btnOk->Height)/2;
    btnOk->Top = iTopToUse;
    btnCancel->Top = iTopToUse;
  }
  DMPCATCHTHROW("TImageFileFormatDialog::pnlButtonsResize")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::FormShow(TObject *Sender)
{
  DMPTRY {
    DMPTRY {
      FProcDateFormat = FProcDateFormat.UpperCase();
      if (TTokenEdit::ValidateDateFormatString(FProcDateFormat).Length())
        FProcDateFormat = "MMDDYY";
      FFileNameSingle   = TTokenManager::ValidateFileNameTemplate(FFileNameSingle  , FCombineProcDates, TTokenManager::iftSingle  );
      FFileNamePerTran  = TTokenManager::ValidateFileNameTemplate(FFileNamePerTran , FCombineProcDates, TTokenManager::iftPerTran );
      FFileNamePerBatch = TTokenManager::ValidateFileNameTemplate(FFileNamePerBatch, FCombineProcDates, TTokenManager::iftPerBatch);
    }
    DMPCATCHTHROW("Task: Validate some of the inputs, fix if necessary")

    std::vector<TEdit*) vEdits;
    vEdits.push_back(Edit1);
    vEdits.push_back(Edit2);
    vEdits.push_back(Edit3);

    for (int i = 0; i < (int) m_vTokenEdits.size(); ++i) {
      vEdits[i]->Visible = false;
      m_vTokenEdits[i]->Parent      = vEdits[i]->Parent;
      m_vTokenEdits[i]->Left        = vEdits[i]->Left;
      m_vTokenEdits[i]->Top         = vEdits[i]->Top;
      m_vTokenEdits[i]->Width       = vEdits[i]->Width;
      m_vTokenEdits[i]->Anchors     = vEdits[i]->Anchors;
      m_vTokenEdits[i]->ProcDateFormat      = FProcDateFormat;
      m_vTokenEdits[i]->ZeroPad             = FZeroPad;
      m_vTokenEdits[i]->BatchTypeFormatFull = FBatchTypeFormatFull;
      m_vTokenEdits[i]->Visible     = true;
    }

    m_vTokenEdits[0]->EncodedString = FFileNameSingle;
    m_vTokenEdits[1]->EncodedString = FFileNamePerTran;
    m_vTokenEdits[2]->EncodedString = FFileNamePerBatch;

    m_vTokenEdits[0]->SetFocus();

    cbZeroPadNumbers ->Checked   = FZeroPad;
    eProcDateFormat  ->Text      = FProcDateFormat;
    rgBatchTypeFormat->ItemIndex = FBatchTypeFormatFull ? 0 : 1;

    std::vector<int> vTokens;

    vTokens = TTokenManager::FetchValidTokenTypeList(FCombineProcDates, TTokenManager::iftSingle);
    PopulateListBox(lbTokens1, vTokens);

    vTokens = TTokenManager::FetchValidTokenTypeList(FCombineProcDates, TTokenManager::iftPerTran);
    PopulateListBox(lbTokens2, vTokens);

    vTokens = TTokenManager::FetchValidTokenTypeList(FCombineProcDates, TTokenManager::iftPerBatch);
    PopulateListBox(lbTokens3, vTokens);
  }
  DMPCATCHTHROW("TImageFileFormatDialog::FormShow")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::InsertToken(TObject *Sender)
{
  DMPTRY {
    String sName = dynamic_cast<TWinControl*>(Sender)->Name;
    TListBox*   listBox = 0;
    TTokenEdit* tokenEdit = 0;
    if (sName == "lbTokens1" || sName == "btnAddItem1") {
      listBox   = lbTokens1;
      tokenEdit = m_vTokenEdits[0];
    }
    else
      if (sName == "lbTokens2" || sName == "btnAddItem2") {
        listBox   = lbTokens2;
        tokenEdit = m_vTokenEdits[1];
      }
      else
        if (sName == "lbTokens3" || sName == "btnAddItem3") {
          listBox   = lbTokens3;
          tokenEdit = m_vTokenEdits[2];
        }
    if (tokenEdit) {
      tokenEdit->InsertToken(StringToEditTokenType(listBox->Items->Strings[listBox->ItemIndex]));
      tokenEdit->SetFocus();
    }
    else
      throw DMPException("Method called by undefined component");
  }
  DMPCATCHTHROW("TImageFileFormatDialog::InsertToken")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::pcFileFormatsChange(TObject *Sender)
{
  DMPTRY {
    if (pcFileFormats->ActivePage == tsSingle)
      m_vTokenEdits[0]->SetFocus();
    else
      if (pcFileFormats->ActivePage == tsPerTran)
        m_vTokenEdits[1]->SetFocus();
      else
        if (pcFileFormats->ActivePage == tsPerBatch)
          m_vTokenEdits[2]->SetFocus();
  }
  DMPCATCHTHROW("TImageFileFormatDialog::pcFileFormatsChange")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::btnApplyDefaultClick(TObject *Sender)
{
  DMPTRY {
    String sName = dynamic_cast<TWinControl*>(Sender)->Name;
    if (sName == "btnApplyDefault1") {
      m_vTokenEdits[0]->EncodedString = TTokenManager::GenerateDefaultEncodedFileName(FCombineProcDates, TTokenManager::iftSingle);
      m_vTokenEdits[0]->SetFocus();
    }
    else
      if (sName == "btnApplyDefault2") {
        m_vTokenEdits[1]->EncodedString = TTokenManager::GenerateDefaultEncodedFileName(FCombineProcDates, TTokenManager::iftPerTran);
        m_vTokenEdits[1]->SetFocus();
      }
      else
        if (sName == "btnApplyDefault3") {
          m_vTokenEdits[2]->EncodedString = TTokenManager::GenerateDefaultEncodedFileName(FCombineProcDates, TTokenManager::iftPerBatch);
          m_vTokenEdits[2]->SetFocus();
        }
        else
          throw DMPException("Method called by undefined component");
  }
  DMPCATCHTHROW("TImageFileFormatDialog::btnApplyDefaultClick")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::cbZeroPadNumbersClick(TObject *Sender)
{
  DMPTRY {
    FZeroPad = cbZeroPadNumbers->Checked;
    for (int i = 0; i < (int) m_vTokenEdits.size(); ++i)
      m_vTokenEdits[i]->ZeroPad = FZeroPad;
  }
  DMPCATCHTHROW("TImageFileFormatDialog::cbZeroPadNumbersClick")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::rgBatchTypeFormatClick(TObject *Sender)
{
  DMPTRY {
    FBatchTypeFormatFull = (rgBatchTypeFormat->ItemIndex == 0);
    for (int i = 0; i < (int) m_vTokenEdits.size(); ++i)
      m_vTokenEdits[i]->BatchTypeFormatFull = FBatchTypeFormatFull;
  }
  DMPCATCHTHROW("TImageFileFormatDialog::rgBatchTypeFormatClick")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::eProcDateFormatExit(TObject *Sender)
{
  DMPTRY {
    String sNew = eProcDateFormat->Text.Trim();
    if (FProcDateFormat != sNew) {
      String sProblem = TTokenEdit::ValidateDateFormatString(sNew);
      if (sProblem.Length()) {
        ::MessageBox(Handle, sProblem.c_str(), "Problem With Format String", MB_OK | MB_ICONINFORMATION);
        eProcDateFormat->Text = FProcDateFormat;
      }
      else {
        FProcDateFormat = sNew;
        for (int i = 0; i < (int) m_vTokenEdits.size(); ++i)
          m_vTokenEdits[i]->ProcDateFormat = sNew;
      }
    }
  }
  DMPCATCHTHROW("TImageFileFormatDialog::eProcDateFormatExit")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::eProcDateFormatKeyPress(TObject *Sender, char &Key)
{
  DMPTRY {
    if ((int) Key == VK_RETURN)
      eProcDateFormatExit(Sender);
  }
  DMPCATCHTHROW("TImageFileFormatDialog::eProcDateFormatKeyPress")
}
//---------------------------------------------------------------------------

void __fastcall TImageFileFormatDialog::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    if (ModalResult == mrOk) {
      DMPTRY {
        for (int i = 0; i < (int) m_vTokenEdits.size(); ++i) {
          TTokenManager::ImageFileType imageFileType;
          TTabSheet* tabSheet;
          switch (i) {
            case 0: imageFileType = TTokenManager::iftSingle;
                    tabSheet      = tsSingle;
                    break;
            case 1: imageFileType = TTokenManager::iftPerTran;
                    tabSheet      = tsPerTran;
                    break;
            case 2: imageFileType = TTokenManager::iftPerBatch;
                    tabSheet      = tsPerBatch;
                    break;
            default: throw DMPException("Illegal case");
          }
          std::vector<int> vTokenTypes = TTokenManager::FetchEssentialTokenTypesMissingFromString(m_vTokenEdits[i]->EncodedString, FCombineProcDates, imageFileType);
          String sMissingTokenTypes;
          for (int j = 0; j < (int) vTokenTypes.size(); ++j)
            sMissingTokenTypes += "\r\n   - " + EditTokenTypeToString((EditToken::EditTokenType) vTokenTypes[j]);
          if (sMissingTokenTypes.Length()) {
            pcFileFormats->ActivePage = tabSheet;
            Application->ProcessMessages();
            ::MessageBox(Handle, String("The file name is missing the following required item"+String(vTokenTypes.size() > 1 ? "s" : "")+":"+sMissingTokenTypes).c_str(), "Missing Information", MB_OK | MB_ICONINFORMATION);
            Action = caNone;
            break;
          }
        }
      }
      DMPCATCHTHROW("Task: Validate file names")

      if (Action != caNone) {
        FFileNameSingle   = m_vTokenEdits[0]->EncodedString;
        FFileNamePerTran  = m_vTokenEdits[1]->EncodedString;
        FFileNamePerBatch = m_vTokenEdits[2]->EncodedString;
      }
    }
  }
  DMPCATCHTHROW("TImageFileFormatDialog::FormClose")
}
//---------------------------------------------------------------------------

