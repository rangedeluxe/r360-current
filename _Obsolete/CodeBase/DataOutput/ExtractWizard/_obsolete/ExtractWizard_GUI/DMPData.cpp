//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include "HandleIni.h"
#include "DMPData.h"
#include <math.h>
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DMPADOCommand"
#pragma link "DMPADODataSet"
#pragma resource "*.dfm"
TDMPDataMod *DMPDataMod;
//---------------------------------------------------------------------------
__fastcall TDMPDataMod::TDMPDataMod(TComponent* Owner)
        : TDataModule(Owner)
{
  DMPTRY {
    OpenDatabase();
  }
  DMPCATCHTHROW("TDMPDataMod::TDMPDataMod")
}

//---------------------------------------------------------------------------
bool __fastcall TDMPDataMod::OpenDatabase()
{
  bool bResult = false;
  DMPTRY {
    TIniHandler Ini;
    Ini.Execute();
    OpenConnection(Ini.Server, Ini.ADODB,Ini.DBUserName, Ini.DBPassword, Ini.ADOCommandTimeout);
    bResult = DMPConnection->Connected;
  }
  DMPCATCHTHROW("TDMPDataMod::OpenDatabase")
  return bResult;
}

//---------------------------------------------------------------------------
bool __fastcall TDMPDataMod::OpenConnection(String Server, String Database, String DBUser, String Pswd, int ADOCommandTimeout)
{
  bool bResult = false;
  DMPTRY {
    if (DMPConnection->Connected)
      DMPConnection->Close();
    String ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;";
    if (!Server.IsEmpty())
      ConnectionString += "Data Source=" + Server + ";";
    ConnectionString += "Initial Catalog=" + Database + ";";
    ConnectionString += "Application Name=" + Application->Title + ";";
    ConnectionString += "User ID=" + DBUser +";";
    ConnectionString += "Password=" + Pswd + ";";
    DMPConnection->ConnectionString = ConnectionString;
    DMPConnection->CommandTimeout = ADOCommandTimeout;
    DMPConnection->Open();      //DBUser, Pswd);
    DatabaseName = Database;
    ServerName = Server;
    bResult = DMPConnection->Connected;
  }
  DMPCATCHTHROW("TDMPDataMod::OpenConnection")
  return bResult;
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::CloseConnection()
{
  DMPTRY {
    DMPConnection->Close();
  }
  DMPCATCHTHROW("TDMPDataMod::CloseConnection")
}

//---------------------------------------------------------------------------
bool __fastcall TDMPDataMod::IsConnected()
{
  bool bResult = false;
  DMPTRY {
    bResult = DMPConnection->Connected;
  }
  DMPCATCHTHROW("TDMPDataMod::IsConnected")
  return bResult;
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::ClearQueries()
{
  DMPTRY {
    for (int i = 0; i < ComponentCount; i++)
      if (dynamic_cast<TDMPADODataSet*>(Components[i])){
        ((TDMPADODataSet*)Components[i])->Close();
        ((TDMPADODataSet*)Components[i])->CommandText = "";
      }
  }
  DMPCATCHTHROW("TDMPDataMod::ClearQueries")
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::RequeryChecksStubsDocsOnNewGlobalBatchID()
{
  DMPTRY {
    int iGlobalBatchID = BatchDataSet->FieldByName("GlobalBatchID")->AsInteger;
    if (ChecksDataSet->Active) {
      ChecksDataSet->Close();
      ChecksDataSet->Parameters->ParamByName("GlobalBatchID")->Value = iGlobalBatchID;
      ChecksDataSet->Open();
    }
    if (StubsDataSet->Active) {
      StubsDataSet->Close();
      StubsDataSet->Parameters->ParamByName("GlobalBatchID")->Value = iGlobalBatchID;
      StubsDataSet->Open();
    }
    if (DocsDataSet->Active) {
      DocsDataSet->Close();
      DocsDataSet->Parameters->ParamByName("GlobalBatchID")->Value = iGlobalBatchID;
      DocsDataSet->Open();
    }
  }
  DMPCATCHTHROW("TDMPDataMod::RequeryChecksStubsDocsOnNewGlobalBatchID")
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::RefilterChecksStubsDocsOnNewTransactionID()
{
  String sTransactionID;
  DMPTRY {
    DMPTRY {
      sTransactionID = TranDataSet->FieldByName("TransactionID")->AsString.Trim();
    }
    DMPCATCHTHROW("Task: Retrieve TransactionID From TranQey dataset: <"+sTransactionID+">")
    if (!sTransactionID.IsEmpty()) {
      if (ChecksDataSet->Active) {
        ChecksDataSet->Filter = "TransactionID = " + sTransactionID;
        if (!ChecksDataSet->Filtered)
          ChecksDataSet->Filtered = true;
      }
      if (StubsDataSet->Active) {
        StubsDataSet->Filter = "TransactionID = " + sTransactionID;
        if (!StubsDataSet->Filtered)
          StubsDataSet->Filtered = true;
      }
      if (DocsDataSet->Active) {
        DocsDataSet->Filter = "TransactionID = " + sTransactionID;
        if (!DocsDataSet->Filtered)
          DocsDataSet->Filtered = true;
      }
    }
  }
  DMPCATCHTHROW("TDMPDataMod::RefilterChecksStubsDocsOnNewTransactionID <"+sTransactionID+">")
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::DataModuleDestroy(TObject *Sender)
{
  //CloseConnection();
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::BankDataSetAfterScroll(TDataSet *DataSet)
{
  DMPTRY {
    RecBank = DataSet->FieldByName("BankID")->AsInteger;
  }
  DMPCATCHTHROW("TDMPDataMod::BankQryAfterScroll")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::CustomerDataSetAfterScroll(TDataSet *DataSet)
{
  DMPTRY {
    RecCustomer = DataSet->FieldByName("CustomerID")->AsInteger;
  }
  DMPCATCHTHROW("TDMPDataMod::CustomerQryAfterScroll")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::LockboxDataSetAfterScroll(TDataSet *DataSet)
{
  DMPTRY {
    RecLockbox = DataSet->FieldByName("LockboxID")->AsInteger;
  }
  DMPCATCHTHROW("TDMPDataMod::LockboxQryAfterScroll")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::BatchDataSetAfterScroll(TDataSet *DataSet)
{
  DMPTRY {

    RequeryChecksStubsDocsOnNewGlobalBatchID();

    RecBatch = DataSet->FieldByName("BatchID")->AsInteger;
    RecBatchTypeCode = DataSet->FieldByName("BatchTypeCode")->AsInteger; // CR 5815 03/03/2004 DJK
    if (DataSet->FieldByName("ProcessingDate")->IsNull)
      ProcDateStr = "";
      ProcDateStr = DataSet->FieldByName("ProcessingDate")->AsDateTime.FormatString("mm/dd/yy");
    if (DataSet->FieldByName("DepositDate")->IsNull)
      DepDateStr = "";
    else {
      TDateTime dtDep = DataSet->FieldByName("DepositDate")->AsDateTime;
      DepDateStr = dtDep.FormatString("mm/dd/yy");
      if (fmod((double) dtDep, 1.0) > 1.0/24.0/60.0/60.0/2.0) // if we are more than half a second past midnight...
        DepDateStr += " "+dtDep.FormatString("hh:nn");
    }
  }
  DMPCATCHTHROW("TDMPDataMod::BatchQryAfterScroll")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::TranDataSetAfterScroll(TDataSet *DataSet)
{
  DMPTRY {
    RecItem = DataSet->FieldByName("TransactionID")->AsInteger;

    RefilterChecksStubsDocsOnNewTransactionID();
  }
  DMPCATCHTHROW("TDMPDataMod::TranQryAfterScroll")
}

//---------------------------------------------------------------------------
void __fastcall TDMPDataMod::InitializeCounters()
{
  DMPTRY {
    RecBank = RecCustomer = RecLockbox = RecBatch = RecBatchTypeCode = RecItem = -1;
    ProcDateStr = DepDateStr = "";
  }
  DMPCATCHTHROW("TDMPDataMod::InitializeCounters")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::BatchQryAfterOpen(TDataSet *DataSet)
{
  DMPTRY {
    RequeryChecksStubsDocsOnNewGlobalBatchID();
  }
  DMPCATCHTHROW("TDMPDataMod::BatchQryAfterOpen")
}
//---------------------------------------------------------------------------

void __fastcall TDMPDataMod::TranQryAfterOpen(TDataSet *DataSet)
{
  DMPTRY {
    RefilterChecksStubsDocsOnNewTransactionID();
  }
  DMPCATCHTHROW("TDMPDataMod::TranQryAfterOpen")
}
//---------------------------------------------------------------------------
// CR 5272 1/07/2004 DJK
void __fastcall TDMPDataMod::GetFieldNames(String Table, TStrings *Flds)
{
  DMPTRY {
    Flds->Clear();
    ActionDataSet->CommandText = "Select * from " + Table + " where 1 = 2";
    ActionDataSet->Open();
    ActionDataSet->GetFieldNames(Flds);
    ActionDataSet->Close();
  }
  DMPCATCHTHROW("TDMPDataMod::LoadFieldNames")
}
//---------------------------------------------------------------------------
// CR #14581/15136  12/20/05  MSV
int  __fastcall TDMPDataMod::GetColumnSQLDataType( String TblName, String FldName )
{
    int rval;

    try {
        TADODataSet *cmd = new TADODataSet(this);
        cmd->Connection = DMPConnection;
        cmd->CommandType = cmdText;
        cmd->CommandText = "EXEC sp_columns @table_name='" + TblName + "', @column_name='" + FldName + "'";
        cmd->Open();
        rval = cmd->FieldByName("SQL_DATA_TYPE")->AsInteger;
        cmd->Close();
        delete cmd;
    }
    catch (...) {
        rval = 0;
    }
    return(rval);
}
