object ResultForm: TResultForm
  Left = 191
  Top = 141
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Extract Results'
  ClientHeight = 453
  ClientWidth = 688
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 452
    Width = 688
    Height = 1
    Align = alBottom
    TabOrder = 0
    object CloseBtn: TButton
      Left = 328
      Top = 8
      Width = 75
      Height = 25
      Caption = 'Close'
      Default = True
      ModalResult = 1
      TabOrder = 0
    end
  end
  object ResultsEdit: TRichEdit
    Left = 0
    Top = 31
    Width = 688
    Height = 421
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 1
    WantTabs = True
    WordWrap = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 31
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object WordWrapCB: TCheckBox
      Left = 8
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Wrap Text'
      TabOrder = 0
      OnClick = WordWrapCBClick
    end
  end
end
