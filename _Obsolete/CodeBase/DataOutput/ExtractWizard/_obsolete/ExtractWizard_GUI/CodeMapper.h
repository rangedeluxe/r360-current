//---------------------------------------------------------------------------

#ifndef CodeMapperH
#define CodeMapperH
//---------------------------------------------------------------------------
class TCodeMapper
{
  private:
    String FMapFile;
    TStringList *FCodedFieldList;
  protected:
    void __fastcall ReverseList(TStrings *strList);
  public:
    TCodeMapper();
    ~TCodeMapper();
    int __fastcall GetCode(String Field, String Key);
    String __fastcall GetDescription(String Field, int Key);
    bool __fastcall IsCoded(String Field);
    void __fastcall RetrieveCodeValues(String Field, TStrings *str);
};


#endif
