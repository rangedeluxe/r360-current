//---------------------------------------------------------------------------

#ifndef ImageFileFormatDlgH
#define ImageFileFormatDlgH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <ComCtrls.hpp>
#include "ImageFileNamingClasses.h"
//---------------------------------------------------------------------------
class TImageFileFormatDialog : public TForm
{
__published:	// IDE-managed Components
  TPanel *pnlButtons;
  TPanel *pnlMain;
  TButton *btnOk;
  TButton *btnCancel;
  TPageControl *pcFileFormats;
  TTabSheet *tsSingle;
  TTabSheet *tsPerTran;
  TTabSheet *tsPerBatch;
  TCheckBox *cbZeroPadNumbers;
  TRadioGroup *rgBatchTypeFormat;
  TLabel *lblProcDateFormat;
  TEdit *eProcDateFormat;
  TLabel *lblProcDateHelper;
  TLabel *Label1;
  TEdit *Edit1;
  TListBox *lbTokens1;
  TButton *btnAddItem1;
  TButton *btnApplyDefault1;
  TLabel *Label2;
  TEdit *Edit2;
  TListBox *lbTokens2;
  TButton *btnAddItem2;
  TButton *btnApplyDefault2;
  TLabel *Label3;
  TEdit *Edit3;
  TListBox *lbTokens3;
  TButton *btnAddItem3;
  TButton *btnApplyDefault3;
  void __fastcall pnlButtonsResize(TObject *Sender);
  void __fastcall FormShow(TObject *Sender);
  void __fastcall InsertToken(TObject *Sender);
  void __fastcall pcFileFormatsChange(TObject *Sender);
  void __fastcall btnApplyDefaultClick(TObject *Sender);
  void __fastcall cbZeroPadNumbersClick(TObject *Sender);
  void __fastcall rgBatchTypeFormatClick(TObject *Sender);
  void __fastcall eProcDateFormatExit(TObject *Sender);
  void __fastcall eProcDateFormatKeyPress(TObject *Sender, char &Key);
  void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
  String                   EditTokenTypeToString(const EditToken::EditTokenType editTokenType     ) const;
  EditToken::EditTokenType StringToEditTokenType(const String&                  sEditTokenTypeName) const;
  void                     PopulateListBox(TListBox* listBox, const std::vector<int>& vTokenTypes);
  std::vector<TTokenEdit*> m_vTokenEdits;
  String FProcDateFormat;
  bool   FZeroPad;
  bool   FBatchTypeFormatFull;
  bool   FCombineProcDates;
  String FFileNameSingle;
  String FFileNamePerTran;
  String FFileNamePerBatch;
public:		// User declarations
  __fastcall TImageFileFormatDialog(TComponent* Owner);
  __property String ProcDateFormat      = {read=FProcDateFormat,      write=FProcDateFormat     };
  __property bool   ZeroPad             = {read=FZeroPad            , write=FZeroPad            };
  __property bool   BatchTypeFormatFull = {read=FBatchTypeFormatFull, write=FBatchTypeFormatFull};
  __property bool   CombineProcDates    = {                           write=FCombineProcDates   };
  __property String FileNameSingle      = {read=FFileNameSingle     , write=FFileNameSingle     };
  __property String FileNamePerTran     = {read=FFileNamePerTran    , write=FFileNamePerTran    };
  __property String FileNamePerBatch    = {read=FFileNamePerBatch   , write=FFileNamePerBatch   };
};
//---------------------------------------------------------------------------
extern PACKAGE TImageFileFormatDialog *ImageFileFormatDialog;
//---------------------------------------------------------------------------
#endif
