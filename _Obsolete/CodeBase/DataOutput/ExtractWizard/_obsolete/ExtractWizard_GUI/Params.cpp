//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Params.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "AdvGrid"
#pragma resource "*.dfm"
TParameterDlg *ParameterDlg;
//---------------------------------------------------------------------------
__fastcall TParameterDlg::TParameterDlg(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
TModalResult __fastcall TParameterDlg::Execute(TStringList *Params, String &ParamFile, bool ShowFileEdit)
{
  TModalResult mrResult;
  DMPTRY {
    ParameterList = Params;
    ParamFilePtr = &ParamFile;
    ParamGrid->RowCount = ParameterList->Count + 1;
    for (int i = 1; i < ParamGrid->RowCount; i++)
      ParamGrid->Cells[0][i] = ParameterList->Strings[i-1];
    ShowEdit(ShowFileEdit);
    mrResult = ShowModal();
  }
  DMPCATCHTHROW("TParameterDlg::Execute")
  return mrResult;
}
//---------------------------------------------------------------------------
void __fastcall TParameterDlg::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    *ParamFilePtr = ParamFileEdit->Text;
    ParameterList->Clear();
    for (int i = 1; i < ParamGrid->RowCount; i++)
      ParameterList->Add(ParamGrid->Cells[0][i] + "=" + ParamGrid->Cells[1][i]);
    *ParamFilePtr = ParamFileEdit->Text;
  }
  DMPCATCHTHROW("TParameterDlg::FormClose")
}

//---------------------------------------------------------------------------
void __fastcall TParameterDlg::ShowEdit(bool DoShow)
{
  DMPTRY {
    Label1->Visible = DoShow;
    Label3->Visible = DoShow;
    ParamFileEdit->Visible = DoShow;
    BrowseBtn->Visible = DoShow;
    GridPanel->Top = (DoShow ? 72 : 0);
  }
  DMPCATCHTHROW("TParameterDlg::ShowEdit")
}
//---------------------------------------------------------------------------
void __fastcall TParameterDlg::BrowseBtnClick(TObject *Sender)
{
  DMPTRY {
    if (OpenDialog->Execute())
      ParamFileEdit->Text = OpenDialog->FileName;
  }
  DMPCATCHTHROW("TParameterDlg::BrowseBtnClick")
}
//---------------------------------------------------------------------------

