//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop
#include <stdio.h>
#include "DMod.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "DMPADOQuery"
#pragma link "DMPADOCommand"
#pragma link "DMPADODataSet"
#pragma resource "*.dfm"
TDataMod *DataMod;
//---------------------------------------------------------------------------
__fastcall TDataMod::TDataMod(TComponent* Owner)
        : TDataModule(Owner)
{
}

//---------------------------------------------------------------------------
bool __fastcall TDataMod::OpenConnection(String Server, String Database, String DBUser, String Pswd, int ADOCommandTimeout)
{
  bool bResult = false;
  DMPTRY {
    if (DMPConnection->Connected)
      DMPConnection->Close();
    String ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;";
    if (!Server.IsEmpty())
      ConnectionString += "Data Source=" + Server + ";";
    ConnectionString += "Initial Catalog=" + Database + ";";
    ConnectionString += "Application Name=" + Application->Title + ";";
    DMPConnection->ConnectionString = ConnectionString;
    DMPConnection->CommandTimeout = ADOCommandTimeout;
    DMPConnection->Open(DBUser, Pswd);
    ServerName = Server;
    DatabaseName = Database;
    CommandTimeout = ADOCommandTimeout;
    bResult = DMPConnection->Connected;
  }
  DMPCATCHTHROW("TDataMod::OpenConnection")
  return bResult;
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::CloseConnection()
{
  DMPTRY {
    DMPConnection->Close();
  }
  DMPCATCHTHROW("TDataMod::CloseConnection")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::DataModuleDestroy(TObject *Sender)
{
  DMPTRY {
    DMPConnection->Close();
  }
  DMPCATCHTHROW("TDataMod::DataModuleDestroy")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::DoAction(String Action)
{
  DMPTRY {
    ActionCommand->CommandText = Action;
    ActionCommand->Execute();
    ActionCommand->CommandText = "";
  }
  DMPCATCHTHROW("TDataMod::DoAction")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::DoOwnerAction(String Action)
{
  DMPTRY {
    String ConnectionString = "Provider=SQLOLEDB.1;Persist Security Info=False;";
    ConnectionString += "Data Source=" + ServerName + ";";
    ConnectionString += "Initial Catalog=" + DatabaseName + ";";
    ConnectionString += "Application Name=" + Application->Title + ";";
    ConnectionString += "User ID=IPAdmin;Password=123456";
    ActionCommand->ConnectionString = ConnectionString;
    ActionCommand->CommandTimeout = CommandTimeout;
    ActionCommand->CommandText = Action;
    ActionCommand->Execute();
    ActionCommand->CommandText = "";
    ActionCommand->ConnectionString = "";
    ActionCommand->Connection = DMPConnection;
  }
  DMPCATCHTHROW("TDataMod::DoOwnerAction")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::Audit(String Event, String Description, int BankID, int LockboxID, int Batch, int Item, String Module, String OperatorID, int WKID)
{
  DMPTRY {
    String Action = "Insert into AuditFile (Event, Description, BankID, LockboxID, Batch, Item, ModuleName, Operator, WorkStation, TimeStamp) "
                    "Values (" + QuotedStr(Event)    + ", " + QuotedStr(Description) + ", " + IntToStr(BankID) + ", "
                               + IntToStr(LockboxID) + ", " + IntToStr(Batch)        + ", " + IntToStr(Item)   + ", "
                               + QuotedStr(Module)   + ", " + QuotedStr(OperatorID)  + ", " + IntToStr(WKID)   + ", "
                               + QuotedStr(TDateTime().CurrentDateTime().DateTimeString()) + ")";
    DoAction(Action);
  }
  DMPCATCHTHROW("TDataMod::Audit")
}

//---------------------------------------------------------------------------
int __fastcall TDataMod::Exists(String SelectStmt)
{
  int iResult = 0;
  DMPTRY {
    ActionDataSet->CommandText = SelectStmt;
    ActionDataSet->Open();
    iResult = ActionDataSet->RecordCount;
    ActionDataSet->Close();
  }
  DMPCATCHTHROW("TDataMod::Exists")
  return iResult;
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::GetFieldListByType(TStrings* str, String Table, TFieldType DataType)
{
  DMPTRY {
    ActionDataSet->CommandText = "Select * from " + Table + " where 1 = 2";
    ActionDataSet->Open();
    str->Clear();
    for (int i = 0; i < ActionDataSet->FieldCount; i++){
      if (ActionDataSet->Fields->Fields[i]->DataType == DataType)
        str->Add(ActionDataSet->Fields->Fields[i]->FieldName);
    }
    ActionDataSet->Close();
  }
  DMPCATCHTHROW("TDataMod::GetFieldListByType")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::LoadFieldNames(String Table, TStrings *Flds, TStringList *Attribs)
{
  DMPTRY {
    Flds->Clear();
    Attribs->Clear();
    ActionDataSet->CommandText = "Select * from " + Table + " where 1 = 2";
    ActionDataSet->Open();
    ActionDataSet->GetFieldNames(Flds);
    char CurrentField[255];
    for (int i = 0; i < ActionDataSet->FieldCount; i++){
      TField *fld = ActionDataSet->Fields->Fields[i];
      memset(CurrentField, '\x0', 255);
      sprintf(CurrentField, "%s=%s,%d,%s,%d,0,%c",
              fld->FieldName.c_str(),
              fld->FieldName.c_str(),
              fld->DataType,
              fld->DisplayLabel.c_str(),
              fld->DisplayWidth,
              (fld->DataType == ftString || fld->DataType == ftDateTime || fld->DataType == ftMemo) ? 'L' : 'R'
             );
      Attribs->Add(String(CurrentField));
    }
    ActionDataSet->Close();
  }
  DMPCATCHTHROW("TDataMod::LoadFieldNames")
}

//---------------------------------------------------------------------------
void __fastcall TDataMod::AppendFieldNames(String Table, TStrings *Flds, TStringList *Attribs)
{
  DMPTRY {
    char CurrentField[255];
    TStringList *tmp = new TStringList;
    ActionDataSet->CommandText = "Select * from " + Table + " where 1 = 2";
    ActionDataSet->Open();
    ActionDataSet->GetFieldNames(tmp);
    for (int i = 0; i < tmp->Count; i++)
      if (Flds->IndexOf(tmp->Strings[i]) == -1) {
        Flds->Append(tmp->Strings[i]);
        TField *fld = ActionDataSet->FindField(tmp->Strings[i]);
        if (!fld)
          continue;
        memset(CurrentField, '\x0', 255);
        sprintf(CurrentField, "%s=%s,%d,%s,%d,0,%c",
                fld->FieldName.c_str(),
                fld->FieldName.c_str(),
                fld->DataType,
                fld->DisplayLabel.c_str(),
                fld->DisplayWidth,
                (fld->DataType == ftString || fld->DataType == ftDateTime || fld->DataType == ftMemo) ? 'L' : 'R'
               );
        Attribs->Add(String(CurrentField));
      }
    ActionDataSet->Close();
    delete tmp;
  }
  DMPCATCHTHROW("TDataMod::AppendFieldNames")
}
//---------------------------------------------------------------------------
// CR #14581  12/20/05  MSV
int  __fastcall TDataMod::GetColumnSQLDataType( String TblName, String FldName )
{
    int rval;

    try {
        TADODataSet *cmd = new TADODataSet(this);
        cmd->Connection = DMPConnection;
        cmd->CommandType = cmdText;
        cmd->CommandText = "EXEC sp_columns @table_name='" + TblName + "', @column_name='" + FldName + "'";
        cmd->Open();
        rval = cmd->FieldByName("SQL_DATA_TYPE")->AsInteger;
        cmd->Close();
        delete cmd;
    }
    catch(...) {
        rval = 0;
    }
    return(rval);
}
//---------------------------------------------------------------------------
