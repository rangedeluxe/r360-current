//---------------------------------------------------------------------------

#ifndef CustomFieldH
#define CustomFieldH
#include <ADODB.hpp>
#include "defines.h"
#include <vector>
#include <StdCtrls.hpp>
#include "DMPADODataSet.h"
//---------------------------------------------------------------------------

enum {atCountChecks, atCountStubs, atCountDocs, atCountTransactions,
        atSumChecksAmount, atSumStubsAmount, atSumChecksDEData,
        atSumChecksDEBilling, atSumStubsDEData, atSumStubsDEBilling};

class ValidationFlags
{
private:
  std::vector<bool> m_vFlags;
  static int m_iArrSize;
public:
  ValidationFlags() {}
  ValidationFlags(const ValidationFlags& obj) {*this = obj;}
  ValidationFlags(const String& sFlags);
  ValidationFlags& operator = (const ValidationFlags& obj) {m_vFlags = obj.m_vFlags; return *this;}
  bool IsValid(int iRecordType, bool bDetail = false) const;
};

class FieldDefinition
{
public:
  String          m_sName;
  TFieldType      m_fieldType;
  String          m_sAttributes;
  ValidationFlags m_flags;
  FieldDefinition() {m_fieldType = ftUnknown;}
  FieldDefinition(const FieldDefinition& obj) {*this = obj;}
  FieldDefinition(const String& sName, TFieldType fieldType, const String& sAttributes, const String& sFlags);
  FieldDefinition& operator = (const FieldDefinition& obj) {m_sName = obj.m_sName; m_fieldType = obj.m_fieldType; m_sAttributes = obj.m_sAttributes; m_flags = obj.m_flags; return *this;}
  bool IsValid(int iRecordType, bool bDetail = false) const {return m_flags.IsValid(iRecordType, bDetail);}
};

class TCustomField
{
protected:
  std::vector<FieldDefinition> m_vFields;
public:
  TCustomField();
  ~TCustomField();
  String __fastcall GetAttributes(const String& sFieldName) const;
  bool __fastcall IsMember(const String& sFieldName) const;
  TFieldType GetFieldType(const String& sFieldName) const;
  virtual void __fastcall RetrieveNames(TListBox* listBox, int iRecordType, bool bDetail = false) const;
protected:
  int GetFieldIdx(const String& sFieldName) const;
};

class TAggregateList : public TCustomField
{
private:
  std::vector< std::vector<double> > m_vAggregateCounts; // CR 13806 9/09/2005 DJK - I converted this from a 2-D double array to a 2-D 'active' vector array - Attempts to access memory beyond the bound of the array will now throw exceptions (before they would just do silent damage).
public:
  TAggregateList();
  void __fastcall ClearAggregateCounts();
  void __fastcall ClearAggregateValues(int Type);
  double __fastcall GetAggregateData(String FieldName, int Type) const;
  int __fastcall GetQueryType(String FieldName) const;
  void __fastcall SetAggregateValues(TDMPADODataSet *DataSet, int Type);
  // CR 13806 9/09/2005 DJK - Added the following method
  void __fastcall SetCheckStubAggregateValues(const int iCheckCount, const int iStubCount, const double dCheckDollars, const double dStubDollars);
};

class TStdFieldList:public TCustomField
{
public:
  TStdFieldList();
};

class TRecordCountList:public TCustomField
{
   private:
   protected:
   public:
     TRecordCountList();
     int __fastcall  GetType(String FieldName);
};


class TInputList
{
  private:
    TStringList *FieldList;
  public:
    TInputList();
    ~TInputList();
    void __fastcall Add(String FieldName);
    void __fastcall AddFieldsFromList(TStringList *FldList);
    void __fastcall Clear();
    void __fastcall RetrieveList(TStrings* Fields);
    void __fastcall RetrieveNames(TStrings *Fields);
    String __fastcall GetAttributes(String FieldName);
    bool __fastcall IsMember(String FieldName);
    void __fastcall EraseParameters(String FileName);
    void __fastcall SetFieldValues(TStringList *Str);
    void __fastcall SubstituteParameters(String &Text, bool UseQuotes);
    void __fastcall WriteParameters(String FileName);
};

//////////////////////////////////////////////////////////////////////////////////

class TScript;

class TFieldCounter
{
private:
  int m_iScriptNum;
  int m_iFieldType; // File, Bank, Customer, etc.
  int m_iCounter;
  int m_iRecCount;
  String m_sFirstTag, m_sMidTag, m_sLastTag;
public:
  TFieldCounter() {Init();}
  TFieldCounter(const TFieldCounter& obj) {*this = obj;}
  TFieldCounter(int iScriptNum, int iFieldType, const String& sFirstTag, const String& sMidTag, const String& sLastTag) {Init(); m_iScriptNum = iScriptNum; m_iFieldType = iFieldType; m_sFirstTag = sFirstTag; m_sMidTag = sMidTag; m_sLastTag = sLastTag;}
  TFieldCounter& operator = (const TFieldCounter& obj) {m_iScriptNum = obj.m_iScriptNum; m_iFieldType = obj.m_iFieldType; m_iCounter = obj.m_iCounter; m_iRecCount = obj.m_iRecCount; m_sFirstTag = obj.m_sFirstTag; m_sMidTag = obj.m_sMidTag; m_sLastTag = obj.m_sLastTag; return *this;}
  void ResetCounter() {m_iCounter = 1;}
  void IncCount    () {m_iCounter++;}
  void SetRecCount (int iRecCount) {m_iRecCount = iRecCount;}
  int    GetScriptNum() const {return m_iScriptNum;}
  int    GetType     () const {return m_iFieldType;}
  int    GetCount    () const {return m_iCounter  ;}
  String GetTag      () const {return m_iCounter >= m_iRecCount ? m_sLastTag : (m_iCounter == 1 ? m_sFirstTag : m_sMidTag);}
private:
  void Init() {m_iScriptNum = m_iFieldType = -1; m_iCounter = 1; m_iRecCount = 0;}
};

class TCounterFieldsManager
{
private:
  std::vector<TFieldCounter> m_vCounters;
  std::vector<TFieldCounter>::iterator itt;
public:
  TCounterFieldsManager() {}
  void   Initialize(TScript* Script);
  void   ResetCounters(int FieldType);
  void   SetRecordCount(int FieldType, int RecordCount);
  void   SetRecordCountByScriptNum(int ScriptNum, int RecordCount);
  int    GetCounter(int ScriptNum);
  void   IncrementCounter(int ScriptNum);
  String GetFirstLastTag(int ScriptNum);
private:
  TFieldCounter* GetElement(int ScriptNum);
};


#endif
