//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include "CustomField.h"
#include "Script.h"
#include "CustomFormat.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------

#pragma package(smart_init)


int ValidationFlags::m_iArrSize = 17;

ValidationFlags::ValidationFlags(const String& sFlags)
{
  DMPTRY {
    m_vFlags = std::vector<bool>(m_iArrSize, false);
    bool bError = true;
    if (sFlags.Length() == m_iArrSize) {
      bError = false;
      for (int i = 0; i < m_iArrSize && !bError; ++i)
        if (sFlags[i+1] == '-')
          m_vFlags[i] = false;
        else
          if (sFlags[i+1] == '1')
            m_vFlags[i] = true;
          else
            bError = true;
    }
    if (bError)
      throw DMPException("There must be "+String(m_iArrSize)+" characters ('-' or '1') in this string");
  }
  DMPCATCHTHROW("ValidationFlags::ValidationFlags (String)")
}

bool ValidationFlags::IsValid(int iRecordType, bool bDetail) const
{
  bool bResult = false;
  DMPTRY {
    bool bError;
    if (bDetail)
      bError = (iRecordType < rtBank || iRecordType >= rtDetail);
    else
      bError = (iRecordType < rtFile || iRecordType >= rtDetail);
    if (bError)
      throw DMPException("Illegal set of inputs");
    bResult = m_vFlags[iRecordType + (bDetail ? 7 : -1)];
  }
  DMPCATCHTHROW("ValidationFlags::IsValid")
  return bResult;
}

//---------------------------------------------------------------------------

FieldDefinition::FieldDefinition(const String& sName, TFieldType fieldType, const String& sAttributes, const String& sFlags)
{
  DMPTRY {
    m_sName = sName;
    m_fieldType = fieldType;
    m_sAttributes = sAttributes;
    m_flags = ValidationFlags(sFlags);
  }
  DMPCATCHTHROW("FieldDefinition::FieldDefinition")
}

//---------------------------------------------------------------------------

TCustomField::TCustomField()
{
  DMPTRY {
  }
  DMPCATCHTHROW("TCustomField::TCustomField")
}

TCustomField::~TCustomField()
{
  DMPTRY {
  }
  DMPCATCHTHROW("TCustomField::~TCustomField")
}

String __fastcall TCustomField::GetAttributes(const String& sFieldName) const
{
  String sResult;
  DMPTRY {
    int iIdx = GetFieldIdx(sFieldName);
    if (iIdx >= 0) {
      FieldDefinition* fPtr = const_cast<FieldDefinition*>(m_vFields.begin()+iIdx);
      sResult = fPtr->m_sName+","+String((int) (fPtr->m_fieldType))+","+fPtr->m_sAttributes;
    }
  }
  DMPCATCHTHROW("TCustomField::GetAttributes")
  return sResult;
}

bool __fastcall TCustomField::IsMember(const String& sFieldName) const
{
  bool bResult = false;
  DMPTRY {
    bResult = (GetFieldIdx(sFieldName) >= 0);
  }
  DMPCATCHTHROW("TCustomField::IsMember")
  return bResult;
}

TFieldType TCustomField::GetFieldType(const String& sFieldName) const
{
  TFieldType ftResult = ftUnknown;
  DMPTRY {
    int iIdx = GetFieldIdx(sFieldName);
    if (iIdx >= 0)
      ftResult = m_vFields[iIdx].m_fieldType;
  }
  DMPCATCHTHROW("TCustomField::GetFieldType")
  return ftResult;
}

void __fastcall TCustomField::RetrieveNames(TListBox* listBox, int iRecordType, bool bDetail) const
{
  DMPTRY {
    for (std::vector<FieldDefinition>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt)
      if (itt->IsValid(iRecordType, bDetail))
        listBox->Items->Add(itt->m_sName);
  }
  DMPCATCHTHROW("TCustomField::RetrieveNames")
}

int TCustomField::GetFieldIdx(const String& sFieldName) const
{
  int iResult = -1;
  DMPTRY {
    for (std::vector<FieldDefinition>::const_iterator itt = m_vFields.begin(); itt < m_vFields.end(); ++itt)
      if (itt->m_sName.AnsiCompareIC(sFieldName) == 0) {
        iResult = itt - m_vFields.begin();
        break;
      }
  }
  DMPCATCHTHROW("TCustomField::GetFieldIdx")
  return iResult;
}

//---------------------------------------------------------------------------

TAggregateList::TAggregateList()
 :TCustomField()
{
  DMPTRY {
    // CR22051: Modified CountChecks, CountStubs, SumChecksAmount, and SumStubsAmount to make them available to Joint Transactions.
    
//                                                                                                                          dddddddd
//                                                                                                                 FBCLBTCSDBCLBTCSD
    m_vFields.push_back(FieldDefinition("CountChecks"           , ftInteger , "Count(Checks),10,0,R"            , "111111111----1111"));
    m_vFields.push_back(FieldDefinition("CountStubs"            , ftInteger , "Count(Stubs),10,0,R"             , "111111111----1111"));
    m_vFields.push_back(FieldDefinition("CountDocuments"        , ftInteger , "Count(Documents),10,0,R"         , "111111-----------"));
    m_vFields.push_back(FieldDefinition("CountTransactions"     , ftInteger , "Count(Transactions),10,0,R"      , "111111-----------"));
    m_vFields.push_back(FieldDefinition("SumChecksAmount"       , ftCurrency, "Sum(Checks.Amount),10,0,R"       , "111111111----1111"));
    m_vFields.push_back(FieldDefinition("SumStubsAmount"        , ftCurrency, "Sum(Stubs.Amount),10,0,R"        , "111111111----1111"));
    m_vFields.push_back(FieldDefinition("SumChecksDEDataKeys"   , ftInteger , "Sum(Checks.DEDataKeys),10,0,R"   , "111111-----------"));
    m_vFields.push_back(FieldDefinition("SumChecksDEBillingKeys", ftInteger , "Sum(Checks.DEBillingKeys),10,0,R", "111111-----------"));
    m_vFields.push_back(FieldDefinition("SumStubsDEDataKeys"    , ftInteger , "Sum(Stubs.DEDataKeys),10,0,R"    , "111111-----------"));
    m_vFields.push_back(FieldDefinition("SumStubsDEBillingKeys" , ftInteger , "Sum(Stubs.DEBillingKeys),10,0,R" , "111111-----------"));

    std::vector<double> vDoubles10 = std::vector<double>(10);
    for (int i = 0; i <= rtDocs; ++i)
      m_vAggregateCounts.push_back(vDoubles10);

    ClearAggregateCounts();
  }
  DMPCATCHTHROW("TAggregateList::TAggregateList")
}

int __fastcall TAggregateList::GetQueryType(String FieldName) const
{
  int iResult = -1;
  DMPTRY {
    switch (GetFieldIdx(FieldName)) {
      case 0: iResult = rtChecks; break;
      case 1: iResult = rtStubs; break;
      case 2: iResult = rtDocs; break;
      case 3: iResult = rtTransactions; break;
      case 4: iResult = rtChecks; break;
      case 5: iResult = rtStubs; break;
      case 6: iResult = rtChecks; break;
      case 7: iResult = rtChecks; break;
      case 8: iResult = rtStubs; break;
      case 9: iResult = rtStubs; break;
      default: throw DMPException("FieldName '"+FieldName+"' not found in list");
    }
  }
  DMPCATCHTHROW("TAggregateList::GetQueryType")
  return iResult;
}


void __fastcall TAggregateList::ClearAggregateCounts()
{
  DMPTRY {
    for (int i = rtGeneral; i <= rtStubs; i++)
      for (int j = 0; j < 10; j++)
        m_vAggregateCounts[i][j] = 0;
  }
  DMPCATCHTHROW("TAggregateList::ClearAggregateCounts")
}

void __fastcall TAggregateList::ClearAggregateValues(int Type)
{
  DMPTRY {
    for (int i = 0; i < 10; i++)
      m_vAggregateCounts[Type][i] = 0;
  }
  DMPCATCHTHROW("TAggregateList::ClearAggregateValues")
}

void __fastcall TAggregateList::SetAggregateValues(TDMPADODataSet *DataSet, int Type)
{
  DMPTRY {
    // CR 13806 9/09/2005 DJK - Even though 'i' can go up to 'rtStubs', we only set values up to 'rtTransactions' ('rtChecks' and 'rtStubs' are handled differently, in the new method 'SetCheckStubAggregateValues')
    for (int i = rtFile; i <= rtTransactions; i++) // DJK - Do not let the aggregates extend into Checks or Stubs - they are handled differently there.
      switch (Type){
        case rtTransactions: m_vAggregateCounts[i][atCountTransactions ]++;
                             break;
        case rtChecks:       m_vAggregateCounts[i][atCountChecks       ]++;
                             m_vAggregateCounts[i][atSumChecksDEData   ] += DataSet->FieldByName("DEDataKeys"   )->AsInteger;
                             m_vAggregateCounts[i][atSumChecksDEBilling] += DataSet->FieldByName("DEBillingKeys")->AsInteger;
                             m_vAggregateCounts[i][atSumChecksAmount   ] += DataSet->FieldByName("Amount"       )->AsFloat;
                             break;
        case rtStubs:        m_vAggregateCounts[i][atCountStubs        ]++;
                             m_vAggregateCounts[i][atSumStubsDEData    ] += DataSet->FieldByName("DEDataKeys")->AsInteger;
                             m_vAggregateCounts[i][atSumStubsDEBilling ] += DataSet->FieldByName("DEBillingKeys")->AsInteger;
                             m_vAggregateCounts[i][atSumStubsAmount    ] += DataSet->FieldByName("Amount")->AsFloat;
                             break;
        case rtDocs:         m_vAggregateCounts[i][atCountDocs         ]++;
                             break;
      }
  }
  DMPCATCHTHROW("TAggregateList::SetAggregateValues")
}

double __fastcall TAggregateList::GetAggregateData(String FieldName, int Type) const
{
  double dResult = 0.0;
  DMPTRY {
    int AggType = GetFieldIdx(FieldName);
    dResult = m_vAggregateCounts[Type][AggType];
  }
  DMPCATCHTHROW("TAggregateList::GetAggregateData")
  return dResult;
}

// CR 13806 9/09/2005 DJK - This new method is the only one that allows counts in the 'rtChecks' and 'rtStubs' range
void __fastcall TAggregateList::SetCheckStubAggregateValues(const int iCheckCount, const int iStubCount, const double dCheckDollars, const double dStubDollars)
{
  DMPTRY {
    for (int i = rtChecks; i <= rtDocs; i++) {
      m_vAggregateCounts[i][atCountChecks    ] = (double) iCheckCount;
      m_vAggregateCounts[i][atCountStubs     ] = (double) iStubCount;
      m_vAggregateCounts[i][atSumChecksAmount] = dCheckDollars;
      m_vAggregateCounts[i][atSumStubsAmount ] = dStubDollars;
    }
  }
  DMPCATCHTHROW("TAggregateList::SetCheckStubAggregateValues")
}

/*
String __fastcall TAggregateList::GetCheckAggregateSQL(int Type)
{
  String Base = "Select Count(*) CountChecks, Sum(Amount) SumCheckAmount, Sum(DEDataKeys) SumCheckDEData,"
        "Sum(DEBillingKeys) SumCheckDEBilling from Checks, ChecksDataEntry "
        "where ChecksDataEntry.GlobalCheckID = Checks.GlobalCheckID "
        "and ChecksDataEntry.TransactionID = Checks.TransactionID and "
        "ChecksDataEntry.GlobalBatchID = Checks.GlobalBatchID ";
  String Where = "";
  switch (Type){
    case rtBank: Where = " (and Checks.GlobalBatchID in (Select GlobalBatchID from "
        "Batch where BankID = :BankID))"; break;
    case rtCustomer: Where = "and (Checks.GlobalBatchID in (Select GlobalBatchID from "
        "Batch where BankID = :BankID and LockboxID in (Select LockboxID from "
        "Customer where BankID = :BankID2 and CustomerID = :CustomerID))"; break;
    case rtLockbox: Where = " and (Checks.GlobalBatchID in (Select GlobalBatchID from "
        "Batch where BankID = :BankID and LockboxID = :LockboxID))"; break;
    case rtBatch: Where = " and (Checks.GlobalBatchID = :GlobalBatchID)"; break;
    case rtTransactions: Where = " (and Checks.TransactionID = :TransactionID "
        "and Checks.GlobalBatchID = :GlobalBatchID)"; break;
  }
  return Base + Where;
}   */

//---------------------------------------------------------------------------
TStdFieldList::TStdFieldList()
 :TCustomField()
{
  DMPTRY {                                                                                                                 //FBCLBTCSDBCLBTCSD
    m_vFields.push_back(FieldDefinition("Filler"                     , ftString  , "Filler,10,SP,L"                       , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("Static"                     , ftString  , "Static,10,SP,L"                       , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("Date"                       , ftDateTime, "Date,8,0,L,,mm/dd/yy"                 , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("Time"                       , ftDateTime, "Time,10,0,L,,hh:nn:ss"                , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("CurrentProcessingDate"      , ftDateTime, "CurrentProcessingDate,8,0,L,,mm/dd/yy", "11111111111111111"));
    m_vFields.push_back(FieldDefinition("ProcessingRunDate"          , ftDateTime, "ProcessingRunDate,8,0,L,,mm/dd/yy"    , "1111-----111-----"));
    m_vFields.push_back(FieldDefinition("ImageCheckSingleFront"      , ftString  , "ImageCheckSingleFront,60,SP,L"        , "------1-------1--")); //CR9916 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageCheckSingleRear"       , ftString  , "ImageCheckSingleRear,60,SP,L"         , "------1-------1--")); //CR9916 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageCheckPerBatch"         , ftString  , "ImageCheckPerBatch,60,SP,L"           , "------1----------"));
    m_vFields.push_back(FieldDefinition("ImageCheckPerTransaction"   , ftString  , "ImageCheckPerTransaction,60,SP,L"     , "------1----------"));
    m_vFields.push_back(FieldDefinition("ImageDocumentSingleFront"   , ftString  , "ImageDocumentSingleFront,60,SP,L"     , "--------1-------1")); //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageDocumentSingleRear"    , ftString  , "ImageDocumentSingleRear,60,SP,L"      , "--------1-------1")); //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageStubSingleFront"       , ftString  , "ImageStubSingleFront,60,SP,L"         , "-------1-------1-")); //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageStubSingleRear"        , ftString  , "ImageStubSingleRear,60,SP,L"          , "-------1-------1-")); //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageDocumentPerBatch"      , ftString  , "ImageDocumentPerBatch,60,SP,L"        , "--------1--------"));
    m_vFields.push_back(FieldDefinition("ImageDocumentPerTransaction", ftString  , "ImageDocumentPerTransaction,60,SP,L"  , "--------1--------"));
   //CR 4969 ~ DLAQAB ~ 3/22/2004
   //The following lines adds ImageFileName to Stubs Tab.
    m_vFields.push_back(FieldDefinition("ImageFileName"              , ftString  , "ImageFileName,255,SP,L"               , "-------1---------"));

    m_vFields.push_back(FieldDefinition("ImageFileSizeFrontC"        , ftString  , "ImageFileSizeFrontC,32,0,R"           , "------1-------1--"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageFileSizeRearC"         , ftString  , "ImageFileSizeRearC,32,0,R"            , "------1-------1--"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageFileSizeFrontS"        , ftString  , "ImageFileSizeFrontS,32,0,R"           , "-------1-------1-"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageFileSizeRearS"         , ftString  , "ImageFileSizeRearS,32,0,R"            , "-------1-------1-"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageFileSizeFrontD"        , ftString  , "ImageFileSizeFrontD,32,0,R"           , "--------1-------1"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("ImageFileSizeRearD"         , ftString  , "ImageFileSizeRearD,32,0,R"            , "--------1-------1"));  //10943 10/26/2004 ALH
    m_vFields.push_back(FieldDefinition("BatchTotalDocsCount"        , ftString  , "BatchTotalDocsCount,6,0,R"            , "----1-------1----"));  //10943 5/10/2005 ALH
    m_vFields.push_back(FieldDefinition("JointDetailSequence"        , ftString  , "JointDetailSequence,4,0,R"            , "--------------111"));  //12916 6/27/2005 ALH
    m_vFields.push_back(FieldDefinition("Counter"                    , ftInteger , "Counter,4,0,R"                        , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("FirstLast"                  , ftInteger , "FirstLast,10,SP,L,,First/Mid/Last"    , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterFile"            , ftInteger , "LineCounterFile,5,0,R"                , "11111111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterBank"            , ftInteger , "LineCounterBank,5,0,R"                , "-1111111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterCustomer"        , ftInteger , "LineCounterCustomer,5,0,R"            , "--111111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterLockbox"         , ftInteger , "LineCounterLockbox,5,0,R"             , "---11111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterBatch"           , ftInteger , "LineCounterBatch,5,0,R"               , "----1111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterTransaction"     , ftInteger , "LineCounterTransaction,5,0,R"         , "-----111111111111"));
    m_vFields.push_back(FieldDefinition("LineCounterCheck"           , ftInteger , "LineCounterCheck,5,0,R"               , "------1----------"));
    m_vFields.push_back(FieldDefinition("LineCounterStub"            , ftInteger , "LineCounterStub,5,0,R"                , "-------1---------"));
    m_vFields.push_back(FieldDefinition("LineCounterDocument"        , ftInteger , "LineCounterDocument,5,0,R"            , "--------1--------"));
  }
  DMPCATCHTHROW("TStdFieldList::TStdFieldList")
}

//---------------------------------------------------------------------------

TRecordCountList::TRecordCountList()
 :TCustomField()
{
  DMPTRY {
//                                                                                                           dddddddd
//                                                                                                  FBCLBTCSDBCLBTCSD
    m_vFields.push_back(FieldDefinition("RecordCountTotal"   , ftInteger, "TotalRecords,15,0,R"  , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountFile"    , ftInteger, "FileRecords,15,0,R"   , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountBank"    , ftInteger, "BankRecords,15,0,R"   , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountCustomer", ftInteger, "CustRecords,15,0,R"   , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountLockbox" , ftInteger, "LockboxRecords,15,0,R", "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountBatch"   , ftInteger, "BatchRecords,15,0,R"  , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountTran"    , ftInteger, "TranRecords,15,0,R"   , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountCheck"   , ftInteger, "CheckRecords,15,0,R"  , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountStub"    , ftInteger, "StubRecords,15,0,R"   , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountDoc"     , ftInteger, "DocRecords,15,0,R"    , "1----------------"));
    m_vFields.push_back(FieldDefinition("RecordCountDetail"  , ftInteger, "DetailRecords,15,0,R" , "1----------------"));
  }
  DMPCATCHTHROW("TRecordCountList::TRecordCountList")
}

//---------------------------------------------------------------------------

int __fastcall TRecordCountList::GetType(String FieldName)
{
  int iResult = 0;
  DMPTRY {
    iResult = GetFieldIdx(FieldName);
  }
  DMPCATCHTHROW("TRecordCountList::GetType")
  return iResult;
}

//---------------------------------------------------------------------------

TInputList::TInputList()
{
  DMPTRY {
    FieldList = new TStringList();
  }
  DMPCATCHTHROW("TInputList::TInputList")
}

TInputList::~TInputList()
{
  DMPTRY {
    delete FieldList;
  }
  DMPCATCHTHROW("TInputList::~TInputList")
}

void __fastcall TInputList::Add(String FieldName)
{
  DMPTRY {
    if (!IsMember(FieldName))
      FieldList->Add(FieldName + "=");
  }
  DMPCATCHTHROW("TInputList::Add")
}

void __fastcall TInputList::AddFieldsFromList(TStringList *FldList)
{
  DMPTRY {
    String Value, tmp;
    for (int i = 0; i < FldList->Count; i++){
      Value = FldList->Strings[i];
      tmp = "";
      if (Value.Pos("@")){
        tmp = Value.SubString(Value.Pos("@")+ 1, Value.Length());
        if (tmp.Pos(" "))
          tmp = tmp.SubString(1, tmp.Pos(" ") -1);
      }
      if (!tmp.IsEmpty())
        Add(tmp);
    }
  }
  DMPCATCHTHROW("TInputList::AddFieldsFromList")
}

void __fastcall TInputList::Clear()
{
  DMPTRY {
    FieldList->Clear();
  }
  DMPCATCHTHROW("TInputList::Clear")
}

void __fastcall TInputList::RetrieveList(TStrings* Fields)
{
  DMPTRY {
    Fields->Clear();
    Fields->Assign(FieldList);
  }
  DMPCATCHTHROW("TInputList::RetrieveList")
}

void __fastcall TInputList::RetrieveNames(TStrings *Fields)
{
  DMPTRY {
    Fields->Clear();
    for (int i = 0; i < FieldList->Count; i++)
      Fields->Add(FieldList->Names[i]);
  }
  DMPCATCHTHROW("TInputList::RetrieveNames")
}

String __fastcall TInputList::GetAttributes(String FieldName)
{
  String sResult;
  DMPTRY {
    sResult = FieldList->Values[FieldName];
  }
  DMPCATCHTHROW("TInputList::GetAttributes")
  return sResult;
}

bool __fastcall TInputList::IsMember(String FieldName)
{
  bool bResult = false;
  DMPTRY {
    bResult = (FieldList->IndexOfName(FieldName) >= 0);
  }
  DMPCATCHTHROW("TInputList::IsMember")
  return bResult;
}

void __fastcall TInputList::EraseParameters(String FileName)
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(FileName);
    Ini->EraseSection("PARAMETERS");
    delete Ini;
  }
  DMPCATCHTHROW("TInputList::EraseParameters")
}

void __fastcall TInputList::SetFieldValues(TStringList *Str)
{
  DMPTRY {
    FieldList->Assign(Str);
  }
  DMPCATCHTHROW("TInputList::SetFieldValues")
}

void __fastcall TInputList::SubstituteParameters(String &Text, bool UseQuotes)
{
  DMPTRY {
    String Param, TextRead = "", TextLeft = Text;
    int Index;
    while (TextLeft.Pos("@")){
      Index = TextLeft.Pos("@");
      TextRead += TextLeft.SubString(1, Index - 1);
      Param = TextLeft.SubString(Index + 1, TextLeft.Length());
      if (Param.Pos(" "))
        Param = Param.SubString(1, Param.Pos(" ") -1);
      TextLeft = TextLeft.SubString(Index + Param.Length() + 1, TextLeft.Length());
      Param = GetAttributes(Param);
      if (UseQuotes)
        Param = QuotedStr(Param);
      TextRead += Param;
    }
    if (!TextLeft.IsEmpty())
      TextRead += TextLeft;
    Text = TextRead;
  }
  DMPCATCHTHROW("TInputList::SubstituteParameters")
}

void __fastcall TInputList::WriteParameters(String FileName)
{
  DMPTRY {
    TIniFile *Ini = new TIniFile(FileName);
    for (int i = 0; i < FieldList->Count; i++)
      Ini->WriteString("PARAMETERS", FieldList->Names[i], FieldList->Values[FieldList->Names[i]]);
    delete Ini;
  }
  DMPCATCHTHROW("TInputList::WriteParameters")
}

/////////////////////////////////////////////////////////////////////////////////

void TCounterFieldsManager::Initialize(TScript* Script)
{
  DMPTRY {
    m_vCounters.clear();
    String sFirst, sMid, sLast;
    for (int i = 0; i < Script->GetRecordCount(); ++i) {
      int iType = TRecHelper::GetRecordSubType(Script->GetRecord(i).GetType());
      const TRecordItem* record = &(Script->GetRecord(i));
      bool bGotCounter   = false;
      bool bGotFirstLast = false;
      sFirst = sMid = sLast = "";
      for (int j = 0; j < record->GetFieldCount(); ++j) {
        const TRecordFieldItem* field = &(record->GetField(j));
        if (field->GetFieldName().AnsiCompareIC("Counter") == 0 && !bGotCounter)
          bGotCounter = true;
        else
          if (field->GetFieldName().AnsiCompareIC("FirstLast") == 0 && !bGotFirstLast) {
            if (field->HasOccursColumns()) {
              String sTemp = field->GetFormat();
              int iPos = sTemp.Pos("/");
              if (iPos) {
                sFirst = sTemp.SubString(1, iPos-1);
                sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);
              }
              else {
                sFirst = sTemp;
                sTemp = "";
              }
              iPos = sTemp.Pos("/");
              if (iPos) {
                sMid = sTemp.SubString(1, iPos-1);
                sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);
              }
              else {
                sMid = sTemp;
                sTemp = "";
              }
              sLast = sTemp;
            }
            bGotFirstLast = true;;
          }
      }
      if (bGotCounter || bGotFirstLast)
        m_vCounters.push_back(TFieldCounter(i, iType, sFirst, sMid, sLast));
    }
  }
  DMPCATCHTHROW("TCounterFieldsManager::Initialize")
}

void TCounterFieldsManager::ResetCounters(int FieldType)
{
  DMPTRY {
    int iType = TRecHelper::GetRecordSubType(FieldType);
    for (itt = m_vCounters.begin(); itt < m_vCounters.end(); ++itt)
      if (itt->GetType() == iType)
        itt->ResetCounter();
  }
  DMPCATCHTHROW("TCounterFieldsManager::ResetCounters")
}

void TCounterFieldsManager::SetRecordCount(int FieldType, int RecordCount)
{
  DMPTRY {
    int iType = TRecHelper::GetRecordSubType(FieldType);
    for (itt = m_vCounters.begin(); itt < m_vCounters.end(); ++itt)
      if (itt->GetType() == iType)
        itt->SetRecCount(RecordCount);
  }
  DMPCATCHTHROW("TCounterFieldsManager::SetRecordCount")
}

void TCounterFieldsManager::SetRecordCountByScriptNum(int ScriptNum, int RecordCount)
{
  DMPTRY {
    TFieldCounter* ptr = GetElement(ScriptNum);
    if (ptr)
      ptr->SetRecCount(RecordCount);
  }
  DMPCATCHTHROW("TCounterFieldsManager::SetRecordCountByScriptNum")
}

int TCounterFieldsManager::GetCounter(int ScriptNum)
{
  int iResult = 0;
  DMPTRY {
    TFieldCounter* ptr = GetElement(ScriptNum);
    if (ptr)
      iResult = ptr->GetCount();
  }
  DMPCATCHTHROW("TCounterFieldsManager::GetCounter")
  return iResult;
}

void TCounterFieldsManager::IncrementCounter(int ScriptNum)
{
  DMPTRY {
    TFieldCounter* ptr = GetElement(ScriptNum);
    if (ptr)
      ptr->IncCount();
  }
  DMPCATCHTHROW("TCounterFieldsManager::IncrementCounter")
}

String TCounterFieldsManager::GetFirstLastTag(int ScriptNum)
{
  String sResult;
  DMPTRY {
    TFieldCounter* ptr = GetElement(ScriptNum);
    if (ptr)
      sResult = ptr->GetTag();
  }
  DMPCATCHTHROW("TCounterFieldsManager::GetFirstLastTag")
  return sResult;
}

TFieldCounter* TCounterFieldsManager::GetElement(int ScriptNum)
{
  TFieldCounter* pResult = 0;
  DMPTRY {
    for (itt = m_vCounters.begin(); itt < m_vCounters.end(); ++itt)
      if (itt->GetScriptNum() == ScriptNum) {
        pResult = itt;
        break;
      }
  }
  DMPCATCHTHROW("TCounterFieldsManager::GetElement")
  return pResult;
}

