//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <time.h>
#include "CustomFormat.h"
#include "DMPExceptions.h"
#include "HandleIni.h"
#include "IniFiles.hpp"
#include "CustomField.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

// CR 6454 1/08/2004 DJK - Modified function to handle 'zero-padded' negative numbers properly.
void Pad(String &sValue, const bool bMayHaveToMoveNegative, const char cPadChar, const int iWidth, const bool bRightJustify, const bool bUseQuotes)
{
  DMPTRY {
    bool bHandleNegative = bMayHaveToMoveNegative && bRightJustify && cPadChar == '0' && sValue.Length() && sValue[1] == '-';
    int iTmpWidth = iWidth;
    if (bHandleNegative) {
      iTmpWidth -= 1;
      sValue.Delete(1, 1);
    }
    if (bUseQuotes)
      iTmpWidth -= 2;
    if (iWidth > 0){       //0 means no pad; don't want data to trim to 0
      if (bRightJustify){
        while (sValue.Length() < iTmpWidth)
          sValue.Insert(String(cPadChar), 1);
        while (sValue.Length() > iTmpWidth)
          sValue.Delete(1, 1);
      }
      else {
        while (sValue.Length() < iTmpWidth)
          sValue += cPadChar;
        sValue = sValue.SubString(1, iTmpWidth);
      }
    }
    if (bHandleNegative)
      sValue.Insert("-", 1);
    if (bUseQuotes)
      sValue = AnsiQuotedStr(sValue, '"');
  }
  DMPCATCHTHROW("Pad")
}

//---------------------------------------------------------------------------
void DoFormatAlpha(String FormatStr, String Input, String &Value)
{
  DMPTRY {
    if (FormatStr.IsEmpty())
      return;
    if (Input.IsEmpty())
      Input = " ";
    TVarRec args[1] = {Input};
    Value = Format(FormatStr, args, 0);
    Value = Value.Trim();
  }
  DMPCATCHTHROW("DoFormatAlpha")
}

//---------------------------------------------------------------------------
void DoFormatFloat(String Str, double FloatValue, String &Value)
{
    bool bOverpunchPositive = false;
    bool bOverpunchNegative = false;

    DMPTRY {
        if (Str.IsEmpty())
            return;

        if (Str.Pos("{")) {
            bOverpunchPositive = true;
            Str = Str.Delete(Str.Pos("{"),1);
        }
        if (Str.Pos("}")) {
            bOverpunchNegative = true;
            Str = Str.Delete(Str.Pos("}"),1);
        }
        if (Str.Pos("*100")) {
            FloatValue *= 100;       //= (__int64)(FloatValue *100);
            Str = Str.SubString(1, Str.Pos("*100")-1);
        }
        Value = FormatFloat(Str, FloatValue);
        if (bOverpunchPositive && (FloatValue >= 0.0)) {
            char c = Value[Value.Length()];
            switch (c) {
                case '0':   c = '{';    break;
                case '1':   c = 'A';    break;
                case '2':   c = 'B';    break;
                case '3':   c = 'C';    break;
                case '4':   c = 'D';    break;
                case '5':   c = 'E';    break;
                case '6':   c = 'F';    break;
                case '7':   c = 'G';    break;
                case '8':   c = 'H';    break;
                case '9':   c = 'I';    break;
            }
            Value[Value.Length()] = c;
        }
        if (bOverpunchNegative && (FloatValue < 0.0)) {
            Value = FormatFloat(Str, -1.0*FloatValue);
            char c = Value[Value.Length()];
            switch (c) {
                case '0':   c = '}';    break;
                case '1':   c = 'J';    break;
                case '2':   c = 'K';    break;
                case '3':   c = 'L';    break;
                case '4':   c = 'M';    break;
                case '5':   c = 'N';    break;
                case '6':   c = 'O';    break;
                case '7':   c = 'P';    break;
                case '8':   c = 'Q';    break;
                case '9':   c = 'R';    break;
            }
            Value[Value.Length()] = c;
        }
    }
    DMPCATCHTHROW("DoFormatFloat")
}

//---------------------------------------------------------------------------
void DoFormatDateTime(String Str, TDateTime aDate, String &Value)
{
  DMPTRY {
    if (Str.IsEmpty())
      return;
    if (Str.Pos("jjj"))
      Value = FormatJulian(Str, aDate);
    else
      Value = aDate.FormatString(Str);
  }
  DMPCATCHTHROW("DoFormatDateTime")
}

//-----------------------------------------------------------------------------
//Precondition - there is a 'j' in FormatStr
String FormatJulian(String FormatStr, TDateTime DateVal)
{
  String sResult;
  DMPTRY {
    char s[4];
    memset(s, '\x0', 4);
    struct tm tmDate;
    unsigned short yr, mo, dy;
    DateVal.DecodeDate(&yr, &mo,&dy);
    tmDate.tm_year = yr - 1900;
    tmDate.tm_mon  = mo - 1;
    tmDate.tm_mday = dy;
    tmDate.tm_hour = 0;
    tmDate.tm_min  = 0;
    tmDate.tm_sec  = 1;
    tmDate.tm_isdst = -1;
    mktime(&tmDate);
    strftime(s, 4, "%j", &tmDate);
    if (FormatStr[1] == 'j'){
      String TempFormat = FormatStr.SubString(4, FormatStr.Length());
      sResult = TempFormat.IsEmpty()?String(s):String(s) + DateVal.FormatString(TempFormat);
    }
    else {
      String TempFormat = FormatStr.SubString(1, FormatStr.Pos("jjj")-1);
      sResult = TempFormat.IsEmpty()?String(s):DateVal.FormatString(TempFormat) + String(s);
    }
  }
  DMPCATCHTHROW("FormatJulian")
  return sResult;
}

//---------------------------------------------------------------------------
void __fastcall TextToList(TStringList *Str, String Text)
{
  DMPTRY {
    int Index;
    String tmp;
    Str->Clear();
    if (Text[1] == '\"' || Text[1] == '\'')
      Text.Delete(1,1);
    if (Text[Text.Length()] == '\"' || Text[Text.Length()] == '\'')
      Text.Delete(Text.Length(), 1);
    bool DoneParsing = false;
    while (!DoneParsing){
      Index = Text.Pos("|");
      if (Text.Length() > 1){
        if ((Text[1] == '(')&&(Text[2] != '|')){
          if (Text.Pos(")|"))
            Index = Text.Pos(")|") + 1;
        }
      }
      if (Index){
        Str->Add(Text.SubString(1, Index -1));
        Text = Text.SubString(Index + 1, Text.Length());
      }
      else{
        Str->Add(Text);
        DoneParsing = true;
      }
    }
  }
  DMPCATCHTHROW("TextToList")
}

//---------------------------------------------------------------------------
void __fastcall ListToText(TStringList *Str, String &Text)
{
  DMPTRY {
    Text = "";
    for (int i = 0; i < Str->Count; i++){
      if (!Text.IsEmpty())
        Text += "|";
      Text += Str->Strings[i];
    }
  }
  DMPCATCHTHROW("ListToText")
}

//---------------------------------------------------------------------------
//  std::vector<IntWithString> m_vFields;
void ConfirmationFields::LoadFieldsFromIniFile(const TAggregateList& aggregates, const TRecordCountList& recordCounts)
{
  DMPTRY {
     m_vFields.clear();

    TStringList* fields = new TStringList();

    TIniHandler iniHandler;
    iniHandler.GetWinInfo();
    TIniFile* ini = new TIniFile(iniHandler.IniPath+"Mapper.ini");
    ini->ReadSection("ConfirmationFields", fields);
    delete ini;

    for (int i = 0; i < fields->Count; ++i) {
      String sFieldName = fields->Strings[i];
      TFieldType fieldType = aggregates.GetFieldType(sFieldName);
      if (fieldType == ftUnknown)
        fieldType = recordCounts.GetFieldType(sFieldName);
      m_vFields.push_back(ConfirmationField(sFieldName, fieldType));
    }

    delete fields;
  }
  DMPCATCHTHROW("ConfirmationFields::LoadFieldsFromIniFile")
}

String ConfirmationFields::GetFieldListString(int iIdx) const
{
  String sResult;
  DMPTRY {
    if (iIdx >= 0 && iIdx < (int) m_vFields.size()) {
      sResult = m_vFields[iIdx].m_sName + "|" + String((int) (m_vFields[iIdx].m_fieldType)) + "|x|20|SP|L||";
      if (m_vFields[iIdx].m_fieldType == ftCurrency)
        sResult += "#,##0.00";
    }
    else {
      String sError;
      sError.sprintf("Argument <%d> outside of allowable range [0, %d]", iIdx, (int) m_vFields.size()-1);
      throw DMPException(sError);
    }
  }
  DMPCATCHTHROW("ConfirmationFields::GetFieldListString")
  return sResult;
}

void ConfirmationFields::SetData(int iIdx, const String& sData)
{
  DMPTRY {
    if (iIdx >= 0 && iIdx < (int) m_vFields.size())
      m_vFields[iIdx].m_sData = sData;
    else {
      String sError;
      sError.sprintf("Argument <%d> outside of allowable range [0, %d]", iIdx, (int) m_vFields.size()-1);
      throw DMPException(sError);
    }
  }
  DMPCATCHTHROW("ConfirmationFields::SetNumber")
}

String ConfirmationFields::ReturnData() const
{
  String sResult;
  DMPTRY {
    for (int i = 0; i < (int) m_vFields.size(); ++i) {
      if (!sResult.IsEmpty())
        sResult += ";";
      sResult += m_vFields[i].m_sName+";"+m_vFields[i].m_sData.Trim();
    }
  }
  DMPCATCHTHROW("ConfirmationFields::ReturnData")
  return sResult;
}



