//---------------------------------------------------------------------------

#ifndef GlobalsH
#define GlobalsH
//---------------------------------------------------------------------------

class AboutLoader;
class AuditLoader;
class SentryLoader;
class DMPExtractLoader;

class Globals
{
public:
  static AboutLoader*      m_aboutDll;
  static AuditLoader*      m_auditDll;
  static SentryLoader*     m_sentryDll;
};

#endif
