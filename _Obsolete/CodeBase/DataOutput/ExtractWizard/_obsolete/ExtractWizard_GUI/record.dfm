object RecordForm: TRecordForm
  Left = 28
  Top = 146
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Setup'
  ClientHeight = 724
  ClientWidth = 1054
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIChild
  OldCreateOrder = False
  Position = poDefault
  Visible = True
  WindowState = wsMaximized
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object TabControl1: TTabControl
    Left = 0
    Top = 0
    Width = 1054
    Height = 20
    Align = alTop
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    RaggedRight = True
    Style = tsFlatButtons
    TabOrder = 0
    Tabs.Strings = (
      'General'
      'File'
      'Bank'
      'Customer'
      'Lockbox'
      'Batch'
      'Transaction'
      'Check'
      'Stub'
      'Document'
      'Joint Detail')
    TabIndex = 0
    TabWidth = 80
    OnChange = TabControl1Change
    OnChanging = TabControl1Changing
  end
  object PageControl1: TPageControl
    Left = 0
    Top = 20
    Width = 1054
    Height = 704
    ActivePage = GeneralTab
    Align = alClient
    TabOrder = 1
    object GeneralTab: TTabSheet
      Caption = 'General'
      TabVisible = False
      object Bevel4: TBevel
        Left = 544
        Top = 14
        Width = 385
        Height = 113
      end
      object Label8: TLabel
        Left = 544
        Top = 162
        Width = 108
        Height = 13
        Caption = 'Current Extract Layout:'
      end
      object Label9: TLabel
        Left = 24
        Top = 23
        Width = 102
        Height = 13
        Alignment = taRightJustify
        Caption = 'Extract File Location: '
      end
      object Label6: TLabel
        Left = 586
        Top = 50
        Width = 68
        Height = 13
        Caption = 'Field Delimiter:'
      end
      object Label7: TLabel
        Left = 573
        Top = 88
        Width = 81
        Height = 13
        Caption = 'Record Delimiter:'
      end
      object Label17: TLabel
        Left = 15
        Top = 371
        Width = 160
        Height = 13
        Alignment = taRightJustify
        Caption = 'At completion, write timestamp to :'
        WordWrap = True
      end
      object Label10: TLabel
        Left = 47
        Top = 90
        Width = 79
        Height = 13
        Alignment = taRightJustify
        Caption = 'Image  File Path:'
      end
      object Label1: TLabel
        Left = 61
        Top = 55
        Width = 65
        Height = 13
        Alignment = taRightJustify
        Caption = 'Log File Path:'
      end
      object PostDLLLabel: TLabel
        Left = 29
        Top = 138
        Width = 97
        Height = 13
        Alignment = taRightJustify
        Caption = 'Post DLL File Name:'
      end
      object RecordTree: TTreeView
        Left = 544
        Top = 192
        Width = 385
        Height = 497
        DragMode = dmAutomatic
        Indent = 19
        ShowLines = False
        TabOrder = 15
        OnCustomDrawItem = RecordTreeCustomDrawItem
        OnDblClick = RecordTreeDblClick
        OnDragDrop = RecordTreeDragDrop
        OnDragOver = RecordTreeDragOver
        OnEdited = RecordTreeEdited
        OnEditing = RecordTreeEditing
        Items.Data = {
          010000001F0000000000000000000000FFFFFFFFFFFFFFFF0000000010000000
          064C61796F757425000000010000000000000006000000FFFFFFFF0000000000
          0000000C46696C65204865616465727325000000010000000000000000000000
          FFFFFFFF00000000000000000C42616E6B204865616465727329000000010000
          000000000000000000FFFFFFFF000000000000000010437573746F6D65722048
          65616465727328000000010000000000000001000000FFFFFFFF000000000000
          00000F4C6F636B626F7820486561646572732600000001000000000000000200
          0000FFFFFFFF00000000000000000D426174636820486561646572732C000000
          010000000000000003000000FFFFFFFF0000000000000000135472616E736163
          74696F6E204865616465727326000000010000000000000004000000FFFFFFFF
          00000000000000000D436865636B205265636F72647328000000010000000000
          000005000000FFFFFFFF00000000000000000F496E766F696365205265636F72
          6473290000000000000000000000FFFFFFFFFFFFFFFF00000000000000001044
          6F63756D656E74205265636F7264732D0000000000000000000000FFFFFFFFFF
          FFFFFF0000000000000000144A6F696E742044657461696C205265636F726473
          2D000000010000000000000003000000FFFFFFFF000000000000000014547261
          6E73616374696F6E20547261696C657273270000000100000000000000020000
          00FFFFFFFF00000000000000000E426174636820547261696C65727329000000
          010000000000000001000000FFFFFFFF0000000000000000104C6F636B626F78
          20547261696C6572732A000000010000000000000000000000FFFFFFFF000000
          000000000011437573746F6D657220547261696C657273260000000100000000
          00000000000000FFFFFFFF00000000000000000D42616E6B20547261696C6572
          7326000000010000000000000006000000FFFFFFFF00000000000000000D4669
          6C6520547261696C657273}
      end
      object ExtractPathEdit: TEdit
        Left = 128
        Top = 15
        Width = 361
        Height = 21
        TabOrder = 0
      end
      object BrowseBtn: TButton
        Left = 500
        Top = 14
        Width = 30
        Height = 23
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 1
        OnClick = BrowseBtnClick
      end
      object FieldDelimCombo: TComboBox
        Left = 663
        Top = 42
        Width = 57
        Height = 21
        ItemHeight = 13
        TabOrder = 8
        Items.Strings = (
          ','
          ';'
          ':'
          '|'
          '!'
          '@'
          '#'
          '%')
      end
      object RecordDelimCombo: TComboBox
        Left = 663
        Top = 80
        Width = 57
        Height = 21
        ItemHeight = 13
        TabOrder = 9
        Items.Strings = (
          '\n'
          '\n\n'
          '\t'
          '\t\t'
          ','
          ';'
          ':'
          '|'
          '!'
          '@'
          '#'
          '%')
      end
      object QuotesCB: TCheckBox
        Left = 766
        Top = 77
        Width = 153
        Height = 17
        Caption = 'Enclose field data in quotes'
        TabOrder = 11
      end
      object BatchTraceCombo: TComboBox
        Left = 178
        Top = 371
        Width = 263
        Height = 21
        Style = csDropDownList
        ItemHeight = 13
        TabOrder = 6
      end
      object DeleteRecordBtn: TButton
        Left = 661
        Top = 153
        Width = 129
        Height = 25
        Caption = 'Delete Current Record'
        TabOrder = 14
        OnClick = DeleteRecordBtnClick
      end
      object ClearBtn: TButton
        Left = 797
        Top = 153
        Width = 129
        Height = 25
        Caption = 'Clear All Records'
        TabOrder = 13
        OnClick = ClearBtnClick
      end
      object ImagePathEdit: TEdit
        Left = 128
        Top = 82
        Width = 361
        Height = 21
        TabOrder = 4
        OnExit = ImagePathEditExit
      end
      object ImagePathBtn: TButton
        Left = 500
        Top = 82
        Width = 30
        Height = 23
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 5
        OnClick = ImagePathBtnClick
      end
      object AddBatchTraceBtn: TButton
        Left = 447
        Top = 369
        Width = 75
        Height = 25
        Caption = 'Add Field'
        TabOrder = 7
        OnClick = AddBatchTraceBtnClick
      end
      object NoPadCB: TCheckBox
        Left = 766
        Top = 46
        Width = 92
        Height = 17
        Caption = 'Omit padding'
        TabOrder = 10
      end
      object LogPathEdit: TEdit
        Left = 128
        Top = 47
        Width = 361
        Height = 21
        TabOrder = 2
        OnExit = ImagePathEditExit
      end
      object LogBrowseBtn: TButton
        Left = 500
        Top = 47
        Width = 30
        Height = 23
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
        OnClick = BrowseBtnClick
      end
      object NullAsSpaceCB: TCheckBox
        Left = 766
        Top = 108
        Width = 153
        Height = 17
        Caption = 'Show NULL as space'
        TabOrder = 12
      end
      object ImageFileFormatsGB: TGroupBox
        Left = 16
        Top = 168
        Width = 513
        Height = 193
        TabOrder = 16
        object Label31: TLabel
          Left = 11
          Top = 122
          Width = 82
          Height = 13
          Caption = 'File Name: Single'
        end
        object Label32: TLabel
          Left = 11
          Top = 145
          Width = 94
          Height = 13
          Caption = 'File Name: Per Tran'
        end
        object Label33: TLabel
          Left = 11
          Top = 168
          Width = 100
          Height = 13
          Caption = 'File Name: Per Batch'
        end
        object ImageFileFormatRadioGroup: TRadioGroup
          Left = 11
          Top = 8
          Width = 182
          Height = 97
          Caption = 'Image File Format'
          ItemIndex = 0
          Items.Strings = (
            'None'
            'Single/Multi Page TIFF'
            'Acrobat PDF'
            'Embed TIFF Image In Data File')
          TabOrder = 0
          OnClick = ImageFileFormatRadioGroupClick
        end
        object CombineImagesAcrossProcDatesCheckBox: TCheckBox
          Left = 197
          Top = 57
          Width = 16
          Height = 17
          TabOrder = 1
          OnClick = CombineImagesAcrossProcDatesCheckBoxClick
        end
        object Memo1: TMemo
          Left = 216
          Top = 31
          Width = 195
          Height = 82
          BorderStyle = bsNone
          Color = clBtnFace
          Lines.Strings = (
            'For extracts that span multiple processing '
            'dates, allow images from multiple '
            'processing dates to be combined into '
            'individual batch/transaction image files')
          TabOrder = 2
        end
        object eReplaceableImageFileNameSingle: TEdit
          Left = 116
          Top = 119
          Width = 385
          Height = 21
          TabOrder = 3
          Text = 'eReplaceableImageFileNameSingle'
        end
        object eReplaceableImageFileNamePerTran: TEdit
          Left = 116
          Top = 142
          Width = 385
          Height = 21
          TabOrder = 4
          Text = 'eReplaceableImageFileNamePerTran'
        end
        object eReplaceableImageFileNamePerBatch: TEdit
          Left = 116
          Top = 165
          Width = 385
          Height = 21
          TabOrder = 5
          Text = 'eReplaceableImageFileNamePerBatch'
        end
        object btnEditImageFileNames: TButton
          Left = 416
          Top = 44
          Width = 83
          Height = 25
          Caption = 'File Names...'
          TabOrder = 6
          OnClick = btnEditImageFileNamesClick
        end
      end
      object UsePostDLLCheckBOX: TCheckBox
        Left = 128
        Top = 112
        Width = 209
        Height = 17
        BiDiMode = bdLeftToRight
        Caption = 'Use Post Processing DLL'
        ParentBiDiMode = False
        TabOrder = 17
        OnClick = UsePostDLLCheckBOXClick
      end
      object PostDLLPathFileNameEdit: TEdit
        Left = 128
        Top = 129
        Width = 361
        Height = 21
        TabOrder = 18
      end
      object DLLBrowsePathFileNameButton: TButton
        Left = 500
        Top = 130
        Width = 30
        Height = 23
        Caption = '...'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Arial'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 19
        OnClick = DLLBrowsePathFileNameButtonClick
      end
    end
    object FileHeaderTab: TTabSheet
      Caption = 'File Info'
      ImageIndex = 1
      TabVisible = False
      object Bevel3: TBevel
        Left = 4
        Top = 368
        Width = 677
        Height = 241
      end
      object Label2: TLabel
        Left = 5
        Top = 120
        Width = 940
        Height = 27
        AutoSize = False
        Caption = 
          'Click on the column header to repeat the current data for each f' +
          'ield.  Use '#39'SP'#39' to indicate a space as pad char.  Enter the text' +
          ' for static fields in the '#39'Format'#39' column.'
        Layout = tlBottom
        WordWrap = True
      end
      object Label11: TLabel
        Left = 251
        Top = 384
        Width = 73
        Height = 13
        Caption = 'Standard Fields'
      end
      object Label18: TLabel
        Left = 425
        Top = 409
        Width = 194
        Height = 56
        AutoSize = False
        Caption = 
          'Drag and drop field names on the desired grid or use the buttons' +
          ' to select.  Drag the row button on the grid to change order or ' +
          'use the up/down buttons.'
        WordWrap = True
      end
      object Label19: TLabel
        Left = 59
        Top = 384
        Width = 79
        Height = 13
        Caption = 'Aggregate Fields'
      end
      object Panel2: TPanel
        Left = 0
        Top = 0
        Width = 1046
        Height = 113
        Align = alTop
        TabOrder = 0
        object Label5: TLabel
          Left = 6
          Top = 4
          Width = 779
          Height = 21
          AutoSize = False
          Caption = 
            'Extract layouts define the data that will be printed in the extr' +
            'act.  You may create as many layouts as you wish per record type' +
            ', or none at all.'
          WordWrap = True
        end
        object Label13: TLabel
          Left = 11
          Top = 47
          Width = 72
          Height = 13
          Caption = 'Current Layout:'
        end
        object FileLayoutNameCombo: TComboBox
          Left = 11
          Top = 67
          Width = 181
          Height = 21
          Style = csDropDownList
          ItemHeight = 13
          TabOrder = 0
          OnChange = LayoutNameComboChange
        end
        object RecordTypeRG: TRadioGroup
          Left = 208
          Top = 47
          Width = 185
          Height = 41
          Caption = 'RecordType'
          Columns = 2
          ItemIndex = 0
          Items.Strings = (
            'Header'
            'Trailer')
          TabOrder = 1
          OnClick = HeaderFooterRGClick
        end
        object FileNewBtn: TButton
          Left = 406
          Top = 33
          Width = 145
          Height = 25
          Caption = 'New Layout'
          TabOrder = 2
          OnClick = NewLayoutBtnClick
        end
        object FileTestBtn: TButton
          Left = 406
          Top = 64
          Width = 145
          Height = 25
          Caption = 'Test Layout'
          TabOrder = 3
          OnClick = TestExtractBtnClick
        end
        object FileDeleteBtn: TButton
          Left = 558
          Top = 33
          Width = 145
          Height = 25
          Caption = 'Delete Layout'
          TabOrder = 4
          OnClick = DeleteLayoutBtnClick
        end
        object FileRestoreBtn: TButton
          Left = 558
          Top = 64
          Width = 145
          Height = 25
          Caption = 'Restore Original Layout'
          TabOrder = 5
          OnClick = RestoreBtnClick
        end
      end
      object FileGrid: TAdvStringGrid
        Left = 4
        Top = 154
        Width = 964
        Height = 190
        ColCount = 9
        DefaultRowHeight = 18
        FixedCols = 1
        RowCount = 2
        FixedRows = 1
        Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowMoving, goEditing, goTabs]
        TabOrder = 1
        OnDragDrop = GridDragDrop
        OnDragOver = GridDragOver
        OnKeyPress = FieldGridKeyPress
        Bands.Active = False
        Bands.PrimaryColor = clInactiveCaptionText
        Bands.PrimaryLength = 1
        Bands.SecondaryColor = clWindow
        Bands.SecondaryLength = 1
        Bands.Print = False
        AutoNumAlign = False
        AutoSize = False
        VAlignment = vtaTop
        EnhTextSize = False
        EnhRowColMove = True
        SortFixedCols = False
        SortNormalCellsOnly = False
        SizeWithForm = False
        Multilinecells = False
        OnClickCell = GridClickCell
        OnDblClickCell = GridDblClickCell
        OnCanEditCell = GridCanEditCell
        OnCellValidate = FieldGridCellValidate
        SortDirection = sdAscending
        OnGetEditorType = GridGetEditorType
        OnEllipsClick = FieldGridEllipsClick
        SortFull = True
        SortAutoFormat = True
        SortShow = False
        SortIndexShow = False
        EnableGraphics = True
        EnableHTML = True
        EnableWheel = True
        Flat = False
        SortColumn = 0
        HintColor = clYellow
        SelectionColor = clHighlight
        SelectionTextColor = clHighlightText
        SelectionRectangle = False
        SelectionRTFKeep = False
        HintShowCells = False
        HintShowLargeText = False
        OleAcceptFiles = True
        OleAcceptText = True
        PrintSettings.FooterSize = 0
        PrintSettings.HeaderSize = 0
        PrintSettings.Time = ppNone
        PrintSettings.Date = ppNone
        PrintSettings.DateFormat = 'dd/mm/yyyy'
        PrintSettings.PageNr = ppNone
        PrintSettings.Title = ppNone
        PrintSettings.Font.Charset = DEFAULT_CHARSET
        PrintSettings.Font.Color = clWindowText
        PrintSettings.Font.Height = -11
        PrintSettings.Font.Name = 'MS Sans Serif'
        PrintSettings.Font.Style = []
        PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
        PrintSettings.HeaderFont.Color = clWindowText
        PrintSettings.HeaderFont.Height = -11
        PrintSettings.HeaderFont.Name = 'MS Sans Serif'
        PrintSettings.HeaderFont.Style = []
        PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
        PrintSettings.FooterFont.Color = clWindowText
        PrintSettings.FooterFont.Height = -11
        PrintSettings.FooterFont.Name = 'MS Sans Serif'
        PrintSettings.FooterFont.Style = []
        PrintSettings.Borders = pbNoborder
        PrintSettings.BorderStyle = psSolid
        PrintSettings.Centered = False
        PrintSettings.RepeatFixedRows = False
        PrintSettings.RepeatFixedCols = False
        PrintSettings.LeftSize = 0
        PrintSettings.RightSize = 0
        PrintSettings.ColumnSpacing = 0
        PrintSettings.RowSpacing = 0
        PrintSettings.TitleSpacing = 0
        PrintSettings.Orientation = poPortrait
        PrintSettings.FixedWidth = 0
        PrintSettings.FixedHeight = 0
        PrintSettings.UseFixedHeight = False
        PrintSettings.UseFixedWidth = False
        PrintSettings.FitToPage = fpNever
        PrintSettings.PageNumSep = '/'
        PrintSettings.NoAutoSize = False
        PrintSettings.PrintGraphics = False
        HTMLSettings.Width = 100
        Navigation.AllowInsertRow = False
        Navigation.AllowDeleteRow = False
        Navigation.AdvanceOnEnter = False
        Navigation.AdvanceInsert = False
        Navigation.AutoGotoWhenSorted = False
        Navigation.AutoGotoIncremental = False
        Navigation.AutoComboDropSize = False
        Navigation.AdvanceDirection = adLeftRight
        Navigation.AllowClipboardShortCuts = False
        Navigation.AllowSmartClipboard = False
        Navigation.AllowRTFClipboard = False
        Navigation.AllowClipboardAlways = False
        Navigation.AllowClipboardRowGrow = True
        Navigation.AllowClipboardColGrow = True
        Navigation.AdvanceAuto = False
        Navigation.InsertPosition = pInsertBefore
        Navigation.CursorWalkEditor = False
        Navigation.MoveRowOnSort = False
        Navigation.ImproveMaskSel = False
        Navigation.AlwaysEdit = False
        Navigation.CopyHTMLTagsToClipboard = True
        Navigation.LineFeedOnEnter = False
        ColumnSize.Save = False
        ColumnSize.Stretch = True
        ColumnSize.Location = clRegistry
        CellNode.Color = clSilver
        CellNode.NodeType = cnFlat
        CellNode.NodeColor = clBlack
        SizeWhileTyping.Height = False
        SizeWhileTyping.Width = False
        MaxEditLength = 0
        MouseActions.AllSelect = False
        MouseActions.ColSelect = False
        MouseActions.RowSelect = False
        MouseActions.DirectEdit = False
        MouseActions.DirectComboDrop = False
        MouseActions.DisjunctRowSelect = False
        MouseActions.AllColumnSize = False
        MouseActions.AllRowSize = False
        MouseActions.CaretPositioning = False
        IntelliPan = ipVertical
        URLColor = clBlue
        URLShow = False
        URLFull = False
        URLEdit = False
        ScrollType = ssNormal
        ScrollColor = clNone
        ScrollWidth = 16
        ScrollSynch = False
        ScrollProportional = False
        ScrollHints = shNone
        OemConvert = False
        FixedFooters = 0
        FixedRightCols = 0
        FixedColWidth = 15
        FixedRowHeight = 18
        FixedFont.Charset = DEFAULT_CHARSET
        FixedFont.Color = clWindowText
        FixedFont.Height = -11
        FixedFont.Name = 'MS Sans Serif'
        FixedFont.Style = []
        FixedAsButtons = False
        FloatFormat = '%.2f'
        WordWrap = True
        ColumnHeaders.Strings = (
          ''
          'Field Name'
          'Data Type'
          'Display Name'
          'Column Width'
          'Pad Char'
          'Justify (L/R)'
          'Quotes'
          'Format')
        Lookup = False
        LookupCaseSensitive = False
        LookupHistory = False
        ShowSelection = False
        RowIndicator.Data = {
          76010000424D7601000000000000760000002800000020000000100000000100
          04000000000000010000120B0000120B00001000000000000000000000000000
          800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
          FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
          33333FF3333333333333447333333333333377FFF33333333333744473333333
          333337773FF3333333333444447333333333373F773FF3333333334444447333
          33333373F3773FF3333333744444447333333337F333773FF333333444444444
          733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
          999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
          33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
          333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
          33333777333333333333CC333333333333337733333333333333}
        BackGround.Top = 0
        BackGround.Left = 0
        BackGround.Display = bdTile
        BackGround.Cells = bcNormal
        Filter = <>
        ColWidths = (
          15
          121
          5
          120
          76
          51
          63
          63
          421)
        RowHeights = (
          18
          18)
      end
      object FileStdLB: TListBox
        Left = 212
        Top = 404
        Width = 172
        Height = 160
        DragMode = dmAutomatic
        IntegralHeight = True
        ItemHeight = 13
        MultiSelect = True
        Sorted = True
        TabOrder = 4
        OnDblClick = FileStdLBDblClick
        OnMouseMove = ListBoxMouseMove
      end
      object FileAddBtn: TButton
        Left = 213
        Top = 569
        Width = 173
        Height = 25
        Caption = 'Add Field'
        TabOrder = 5
        OnClick = AddDataBtnClick
      end
      object FileRemoveBtn: TButton
        Left = 429
        Top = 470
        Width = 173
        Height = 25
        Caption = 'Remove Field'
        TabOrder = 6
        OnClick = RemoveBtnClick
      end
      object FileUpBtn: TBitBtn
        Left = 429
        Top = 498
        Width = 83
        Height = 27
        Caption = 'Up '
        TabOrder = 7
        OnClick = UpBtnClick
        Glyph.Data = {
          C2070000424DC20700000000000036000000280000001B000000170000000100
          1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF536CCD2344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          A9B5E62344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C09CAA
          E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E623
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000AEBAE7ECEEF9FFFFFFFFFFFFFFFF
          FFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C02344C023
          44C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          91A1DF000000647BD22344C06179D19DACE3DEE3F5FFFFFFA9B5E62344C02344
          C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
          44C09CAAE2FFFFFFDBE1F498A8E15C74D02344C09DACE3000000FFFFFF7287D6
          2344C02344C02344C05770CFA9B5E62344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C02344C08FA0DF5770CF2344C023
          44C02344C07B8FD9FFFFFF000000FFFFFFFFFFFF5972CF2344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C02344C05E76D0FFFFFFFFFFFF00
          0000FFFFFFFFFFFFF8F9FD2344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C02344C02344C0FCFCFEFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFDF
          E3F52344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C0E5E9F7FFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFBEC8EC2344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C0C5CEEEFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A2B0E4
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFF7A8ED92344C02344C02344C02344C02344C02344C02344C02344C023
          44C02344C02344C02344C02344C07F92DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E76D02344
          C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C061
          79D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFE2344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C0FEFEFEFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFE6E9F72344C02344C02344C02344C02344C02344C02344C02344
          C02344C0E8ECF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7CFEF
          2344C02344C02344C02344C02344C02344C02344C0C9D1EFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4B2E52344C02344C02344C0
          2344C02344C0A7B4E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF8295DB2344C02344C02344C08496DBFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFF647BD22344C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF000000}
      end
      object FileDownBtn: TBitBtn
        Left = 519
        Top = 498
        Width = 83
        Height = 27
        Cancel = True
        Caption = 'Down '
        TabOrder = 8
        OnClick = DownBtnClick
        Glyph.Data = {
          C2070000424DC20700000000000036000000280000001B000000170000000100
          1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FF536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF657CD22344C0647BD2FFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF8496DB2344C02344C02344C08295DBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7B4E52344C02344C02344C0
          2344C02344C0A4B2E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFC8D0EF2344C02344C02344C02344C02344C02344C02344C0C7CFEF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8ECF82344C02344C023
          44C02344C02344C02344C02344C02344C02344C0E6E9F7FFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFEFEFE2344C02344C02344C02344C02344C02344C02344C023
          44C02344C02344C02344C0FCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6179D12344
          C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C05E
          76D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFF7E92DA2344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C02344C07A8ED9FFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2B0E4
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000FFFFFFFFFFFFFFFFFFFFFFFFC5CEEE2344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C0BEC8ECFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE5
          E9F72344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C0DFE3F5FFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFCFCFE2344C02344C02344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C0F8F9FDFFFFFFFFFFFF000000FFFF
          FFFFFFFF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C05972CFFFFFFFFFFFFF000000FFFFFF7B8FD92344C02344C02344
          C05770CF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
          44C02344C02344C02344C02344C0627AD25770CF2344C02344C02344C07287D6
          FFFFFF0000009DACE32344C05C74D098A8E1DBE1F4FFFFFF9CAAE22344C02344
          C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
          44C0A9B5E6FFFFFFDEE3F59DACE36179D12344C0FFFFFF000000FFFFFFE4E8F7
          FFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344
          C02344C02344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFF
          FFFFFFFFFFECEEF9FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          9CAAE22344C02344C02344C02344C02344C02344C02344C02344C02344C02344
          C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
          0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C0
          2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A9B5
          E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344C02344C0
          2344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE223
          44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
          2344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C023
          44C02344C02344C02344C02344C02344C02344C02344C02344C0FFFFFFFFFFFF
          FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFFFFFFFFFF536CCD536CCD536CCD536CCD536CCD536CCD536CCD536CCD53
          6CCD536CCD536CCD536CCD536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
          FFFFFF000000}
      end
      object FileRestoreFieldBtn: TButton
        Left = 430
        Top = 530
        Width = 173
        Height = 25
        Caption = 'Restore Default Field Data'
        TabOrder = 9
        OnClick = RestoreFieldBtnClick
      end
      object FileAggLB: TListBox
        Left = 19
        Top = 404
        Width = 172
        Height = 160
        DragMode = dmAutomatic
        IntegralHeight = True
        ItemHeight = 13
        MultiSelect = True
        Sorted = True
        TabOrder = 2
        OnDblClick = FileAggLBDblClick
        OnMouseMove = ListBoxMouseMove
      end
      object FileAddAggBtn: TButton
        Left = 20
        Top = 569
        Width = 171
        Height = 25
        Caption = 'Add Field'
        TabOrder = 3
        OnClick = AddDataBtnClick
      end
    end
    object RecordTab: TTabSheet
      Caption = 'Record'
      ImageIndex = 2
      TabVisible = False
      object Splitter1: TSplitter
        Left = 169
        Top = 0
        Width = 3
        Height = 694
        Cursor = crHSplit
        Beveled = True
        ResizeStyle = rsUpdate
      end
      object FieldsPanel: TPanel
        Left = 0
        Top = 0
        Width = 169
        Height = 694
        Align = alLeft
        Constraints.MaxWidth = 300
        Constraints.MinWidth = 40
        TabOrder = 0
        object lblDataFields: TLabel
          Left = 1
          Top = 1
          Width = 53
          Height = 13
          Alignment = taCenter
          AutoSize = False
          Caption = 'Data Fields'
          Layout = tlBottom
        end
        object FieldLB: TListBox
          Left = 1
          Top = 35
          Width = 167
          Height = 563
          DragMode = dmAutomatic
          IntegralHeight = True
          ItemHeight = 13
          MultiSelect = True
          ParentShowHint = False
          ShowHint = False
          Sorted = True
          TabOrder = 1
          OnDblClick = FieldLBDblClick
          OnMouseMove = ListBoxMouseMove
        end
        object AddDataBtn: TButton
          Left = 8
          Top = 610
          Width = 153
          Height = 25
          Anchors = [akLeft, akTop, akRight]
          Caption = 'Add Field'
          TabOrder = 2
          OnClick = AddDataBtnClick
        end
        object comboBoxJointDetailLevel: TComboBox
          Left = 1
          Top = 14
          Width = 167
          Height = 21
          ItemHeight = 13
          TabOrder = 0
          Text = 'comboBoxJointDetailLevel'
          OnChange = comboBoxJointDetailLevelChange
          Items.Strings = (
            'Bank'
            'Customer'
            'Lockbox'
            'Batch'
            'Transactions'
            'Checks'
            'Stubs'
            'Documents')
        end
      end
      object SelectionPanel: TPanel
        Left = 172
        Top = 0
        Width = 874
        Height = 694
        Align = alClient
        TabOrder = 1
        object RecordPageControl: TPageControl
          Left = 1
          Top = 1
          Width = 872
          Height = 673
          ActivePage = ExtractTab
          Align = alClient
          TabOrder = 0
          object ExtractTab: TTabSheet
            Caption = 'Create Layouts'
            ImageIndex = 2
            object Bevel2: TBevel
              Left = 8
              Top = 392
              Width = 608
              Height = 227
            end
            object Label27: TLabel
              Left = 6
              Top = 4
              Width = 779
              Height = 21
              AutoSize = False
              Caption = 
                'Extract layouts define the data that will be printed in the extr' +
                'act.  You may create as many layouts as you wish per record type' +
                ', or none at all.'
              WordWrap = True
            end
            object Label24: TLabel
              Left = 391
              Top = 425
              Width = 194
              Height = 56
              AutoSize = False
              Caption = 
                'Drag and drop field names on the desired grid or use the buttons' +
                ' to select.  Drag the row button on the grid to change order or ' +
                'use the up/down buttons.'
              WordWrap = True
            end
            object Label22: TLabel
              Left = 59
              Top = 400
              Width = 79
              Height = 13
              Caption = 'Aggregate Fields'
            end
            object Label3: TLabel
              Left = 241
              Top = 400
              Width = 73
              Height = 13
              Caption = 'Standard Fields'
            end
            object Bevel1: TBevel
              Left = 0
              Top = 88
              Width = 809
              Height = 10
              Shape = bsBottomLine
            end
            object Label30: TLabel
              Left = 6
              Top = 112
              Width = 700
              Height = 13
              Caption = 
                'Click on the column header to repeat the current data for each f' +
                'ield.  Use '#39'SP'#39' to indicate a space as pad char.  A width of zer' +
                'o indicates no padding.'
            end
            object Label25: TLabel
              Left = 11
              Top = 72
              Width = 72
              Height = 13
              Caption = 'Current Layout:'
            end
            object NoteLbl: TLabel
              Left = 628
              Top = 132
              Width = 177
              Height = 83
              AutoSize = False
              Caption = 
                'NOTE: Aggregate field values and  field values for detail record' +
                's will be represented by placeholder values when testing layouts' +
                ', since these fields are dependent on data from other levels.'
              WordWrap = True
            end
            object DisplayLevelLbl: TLabel
              Left = 13
              Top = 41
              Width = 66
              Height = 13
              Caption = 'Display Level:'
            end
            object MaxRepeatsLabel: TLabel
              Left = 625
              Top = 229
              Width = 66
              Height = 13
              Caption = 'Max Repeats:'
              Visible = False
            end
            object FieldGrid: TAdvStringGrid
              Left = 8
              Top = 139
              Width = 608
              Height = 251
              ColCount = 9
              DefaultRowHeight = 18
              FixedCols = 1
              RowCount = 2
              FixedRows = 1
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowMoving, goEditing, goTabs]
              ParentShowHint = False
              ShowHint = True
              TabOrder = 7
              OnDragDrop = GridDragDrop
              OnDragOver = GridDragOver
              OnKeyPress = FieldGridKeyPress
              Bands.Active = False
              Bands.PrimaryColor = clInactiveCaptionText
              Bands.PrimaryLength = 1
              Bands.SecondaryColor = clWindow
              Bands.SecondaryLength = 1
              Bands.Print = False
              AutoNumAlign = False
              AutoSize = False
              VAlignment = vtaTop
              EnhTextSize = False
              EnhRowColMove = True
              SortFixedCols = False
              SortNormalCellsOnly = False
              SizeWithForm = False
              Multilinecells = False
              OnClickCell = GridClickCell
              OnDblClickCell = GridDblClickCell
              OnCanEditCell = GridCanEditCell
              OnCellValidate = FieldGridCellValidate
              SortDirection = sdAscending
              OnGetEditorType = GridGetEditorType
              OnEllipsClick = FieldGridEllipsClick
              SortFull = True
              SortAutoFormat = True
              SortShow = False
              SortIndexShow = False
              EnableGraphics = True
              EnableHTML = True
              EnableWheel = True
              Flat = False
              SortColumn = 0
              HintColor = clYellow
              SelectionColor = clHighlight
              SelectionTextColor = clHighlightText
              SelectionRectangle = False
              SelectionRTFKeep = False
              HintShowCells = False
              HintShowLargeText = False
              OleAcceptFiles = True
              OleAcceptText = True
              PrintSettings.FooterSize = 0
              PrintSettings.HeaderSize = 0
              PrintSettings.Time = ppNone
              PrintSettings.Date = ppNone
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.PageNr = ppNone
              PrintSettings.Title = ppNone
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'MS Sans Serif'
              PrintSettings.Font.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'MS Sans Serif'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'MS Sans Serif'
              PrintSettings.FooterFont.Style = []
              PrintSettings.Borders = pbNoborder
              PrintSettings.BorderStyle = psSolid
              PrintSettings.Centered = False
              PrintSettings.RepeatFixedRows = False
              PrintSettings.RepeatFixedCols = False
              PrintSettings.LeftSize = 0
              PrintSettings.RightSize = 0
              PrintSettings.ColumnSpacing = 0
              PrintSettings.RowSpacing = 0
              PrintSettings.TitleSpacing = 0
              PrintSettings.Orientation = poPortrait
              PrintSettings.FixedWidth = 0
              PrintSettings.FixedHeight = 0
              PrintSettings.UseFixedHeight = False
              PrintSettings.UseFixedWidth = False
              PrintSettings.FitToPage = fpNever
              PrintSettings.PageNumSep = '/'
              PrintSettings.NoAutoSize = False
              PrintSettings.PrintGraphics = False
              HTMLSettings.Width = 100
              Navigation.AllowInsertRow = False
              Navigation.AllowDeleteRow = False
              Navigation.AdvanceOnEnter = False
              Navigation.AdvanceInsert = False
              Navigation.AutoGotoWhenSorted = False
              Navigation.AutoGotoIncremental = False
              Navigation.AutoComboDropSize = False
              Navigation.AdvanceDirection = adLeftRight
              Navigation.AllowClipboardShortCuts = False
              Navigation.AllowSmartClipboard = False
              Navigation.AllowRTFClipboard = False
              Navigation.AllowClipboardAlways = False
              Navigation.AllowClipboardRowGrow = True
              Navigation.AllowClipboardColGrow = True
              Navigation.AdvanceAuto = False
              Navigation.InsertPosition = pInsertBefore
              Navigation.CursorWalkEditor = False
              Navigation.MoveRowOnSort = False
              Navigation.ImproveMaskSel = False
              Navigation.AlwaysEdit = False
              Navigation.CopyHTMLTagsToClipboard = True
              Navigation.LineFeedOnEnter = False
              ColumnSize.Save = False
              ColumnSize.Stretch = True
              ColumnSize.Location = clRegistry
              CellNode.Color = clSilver
              CellNode.NodeType = cnFlat
              CellNode.NodeColor = clBlack
              SizeWhileTyping.Height = False
              SizeWhileTyping.Width = False
              MaxEditLength = 0
              MouseActions.AllSelect = False
              MouseActions.ColSelect = False
              MouseActions.RowSelect = False
              MouseActions.DirectEdit = False
              MouseActions.DirectComboDrop = False
              MouseActions.DisjunctRowSelect = False
              MouseActions.AllColumnSize = False
              MouseActions.AllRowSize = False
              MouseActions.CaretPositioning = False
              IntelliPan = ipVertical
              URLColor = clBlue
              URLShow = False
              URLFull = False
              URLEdit = False
              ScrollType = ssNormal
              ScrollColor = clNone
              ScrollWidth = 16
              ScrollSynch = False
              ScrollProportional = False
              ScrollHints = shNone
              OemConvert = False
              FixedFooters = 0
              FixedRightCols = 0
              FixedColWidth = 15
              FixedRowHeight = 18
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWindowText
              FixedFont.Height = -11
              FixedFont.Name = 'MS Sans Serif'
              FixedFont.Style = []
              FixedAsButtons = False
              FloatFormat = '%.2f'
              WordWrap = True
              ColumnHeaders.Strings = (
                ''
                'Field Name'
                'Data Type'
                'Display Name'
                'Column Width'
                'Pad Char'
                'Justify'
                'Quotes'
                'Format')
              Lookup = False
              LookupCaseSensitive = False
              LookupHistory = False
              ShowSelection = False
              RowIndicator.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33333FF3333333333333447333333333333377FFF33333333333744473333333
                333337773FF3333333333444447333333333373F773FF3333333334444447333
                33333373F3773FF3333333744444447333333337F333773FF333333444444444
                733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
                999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
                33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
                333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
                33333777333333333333CC333333333333337733333333333333}
              BackGround.Top = 0
              BackGround.Left = 0
              BackGround.Display = bdTile
              BackGround.Cells = bcNormal
              Filter = <>
              ColWidths = (
                15
                121
                5
                120
                76
                51
                35
                40
                116)
              RowHeights = (
                18
                18)
            end
            object HeaderFooterRG: TRadioGroup
              Left = 296
              Top = 25
              Width = 185
              Height = 60
              Caption = 'RecordType'
              Columns = 2
              ItemIndex = 0
              Items.Strings = (
                'Header'
                'Trailer')
              TabOrder = 2
              OnClick = HeaderFooterRGClick
            end
            object TestExtractBtn: TButton
              Left = 494
              Top = 60
              Width = 145
              Height = 25
              Caption = 'Test Layout'
              TabOrder = 4
              OnClick = TestExtractBtnClick
            end
            object RestoreBtn: TButton
              Left = 646
              Top = 60
              Width = 145
              Height = 25
              Caption = 'Restore Original Layout'
              TabOrder = 6
              OnClick = RestoreBtnClick
            end
            object NewLayoutBtn: TButton
              Left = 494
              Top = 29
              Width = 145
              Height = 25
              Caption = 'New Layout'
              TabOrder = 3
              OnClick = NewLayoutBtnClick
            end
            object AggregateFieldLB: TListBox
              Left = 19
              Top = 420
              Width = 172
              Height = 160
              DragMode = dmAutomatic
              IntegralHeight = True
              ItemHeight = 13
              MultiSelect = True
              Sorted = True
              TabOrder = 8
              OnDblClick = AggregateFieldLBDblClick
              OnMouseMove = ListBoxMouseMove
            end
            object StdFieldLB: TListBox
              Left = 202
              Top = 420
              Width = 172
              Height = 160
              DragMode = dmAutomatic
              IntegralHeight = True
              ItemHeight = 13
              MultiSelect = True
              Sorted = True
              TabOrder = 10
              OnDblClick = StdFieldLBDblClick
              OnMouseMove = ListBoxMouseMove
            end
            object AddAggregateBtn: TButton
              Left = 20
              Top = 585
              Width = 171
              Height = 25
              Caption = 'Add Field'
              TabOrder = 9
              OnClick = AddDataBtnClick
            end
            object AddStdBtn: TButton
              Left = 203
              Top = 585
              Width = 173
              Height = 25
              Caption = 'Add Field'
              TabOrder = 11
              OnClick = AddDataBtnClick
            end
            object RemoveBtn: TButton
              Left = 395
              Top = 486
              Width = 173
              Height = 25
              Caption = 'Remove Field'
              TabOrder = 12
              OnClick = RemoveBtnClick
            end
            object FieldUpBtn: TBitBtn
              Left = 395
              Top = 514
              Width = 83
              Height = 27
              Caption = 'Up '
              TabOrder = 13
              OnClick = UpBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF536CCD2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                A9B5E62344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C09CAA
                E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E623
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000AEBAE7ECEEF9FFFFFFFFFFFFFFFF
                FFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                91A1DF000000647BD22344C06179D19DACE3DEE3F5FFFFFFA9B5E62344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C09CAAE2FFFFFFDBE1F498A8E15C74D02344C09DACE3000000FFFFFF7287D6
                2344C02344C02344C05770CFA9B5E62344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C08FA0DF5770CF2344C023
                44C02344C07B8FD9FFFFFF000000FFFFFFFFFFFF5972CF2344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C05E76D0FFFFFFFFFFFF00
                0000FFFFFFFFFFFFF8F9FD2344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0FCFCFEFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFDF
                E3F52344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0E5E9F7FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFBEC8EC2344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C0C5CEEEFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A2B0E4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF7A8ED92344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C07F92DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E76D02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C061
                79D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFE2344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C0FEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFE6E9F72344C02344C02344C02344C02344C02344C02344C02344
                C02344C0E8ECF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7CFEF
                2344C02344C02344C02344C02344C02344C02344C0C9D1EFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4B2E52344C02344C02344C0
                2344C02344C0A7B4E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF8295DB2344C02344C02344C08496DBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF647BD22344C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
            object FieldDownBtn: TBitBtn
              Left = 485
              Top = 514
              Width = 83
              Height = 27
              Cancel = True
              Caption = 'Down '
              TabOrder = 14
              OnClick = DownBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF657CD22344C0647BD2FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF8496DB2344C02344C02344C08295DBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7B4E52344C02344C02344C0
                2344C02344C0A4B2E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFC8D0EF2344C02344C02344C02344C02344C02344C02344C0C7CFEF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8ECF82344C02344C023
                44C02344C02344C02344C02344C02344C02344C0E6E9F7FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFE2344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C0FCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6179D12344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C05E
                76D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7E92DA2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C07A8ED9FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2B0E4
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFC5CEEE2344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C0BEC8ECFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE5
                E9F72344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0DFE3F5FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFCFCFE2344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0F8F9FDFFFFFFFFFFFF000000FFFF
                FFFFFFFF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C05972CFFFFFFFFFFFFF000000FFFFFF7B8FD92344C02344C02344
                C05770CF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C0627AD25770CF2344C02344C02344C07287D6
                FFFFFF0000009DACE32344C05C74D098A8E1DBE1F4FFFFFF9CAAE22344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C0A9B5E6FFFFFFDEE3F59DACE36179D12344C0FFFFFF000000FFFFFFE4E8F7
                FFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFF
                FFFFFFFFFFECEEF9FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                9CAAE22344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A9B5
                E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE223
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF536CCD536CCD536CCD536CCD536CCD536CCD536CCD536CCD53
                6CCD536CCD536CCD536CCD536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
            object RestoreFieldBtn: TButton
              Left = 396
              Top = 546
              Width = 173
              Height = 25
              Caption = 'Restore Default Field Data'
              TabOrder = 15
              OnClick = RestoreFieldBtnClick
            end
            object DeleteLayoutBtn: TButton
              Left = 646
              Top = 29
              Width = 145
              Height = 25
              Caption = 'Delete Layout'
              TabOrder = 5
              OnClick = DeleteLayoutBtnClick
            end
            object LayoutNameCombo: TComboBox
              Left = 104
              Top = 64
              Width = 181
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 1
              OnChange = LayoutNameComboChange
            end
            object DisplayLevelCombo: TComboBox
              Left = 104
              Top = 33
              Width = 181
              Height = 21
              Style = csDropDownList
              ItemHeight = 13
              TabOrder = 0
              OnChange = DisplayLevelComboChange
              OnDropDown = DisplayLevelComboDropDown
              Items.Strings = (
                'File'
                'Bank'
                'Customer'
                'Lockbox'
                'Batch'
                'Transactions'
                'Detail')
            end
            object RepeatLimit: TEdit
              Left = 624
              Top = 248
              Width = 38
              Height = 21
              MaxLength = 3
              TabOrder = 16
              Visible = False
              OnChange = RepeatLimitChange
              OnExit = RepeatLimitExit
            end
            object IgnoreOccurs: TCheckBox
              Left = 632
              Top = 544
              Width = 129
              Height = 17
              Caption = 'Ignore Occurs Columns'
              TabOrder = 17
              OnClick = IgnoreOccursClick
            end
          end
          object OrderTab: TTabSheet
            Caption = 'Order Data'
            ImageIndex = 1
            object Label21: TLabel
              Left = 592
              Top = 86
              Width = 193
              Height = 56
              AutoSize = False
              Caption = 
                'Drag and drop field names on the grid or use the button to selec' +
                't.  Drag the row button on the grid to change order or use the u' +
                'p/down buttons.'
              WordWrap = True
            end
            object Label28: TLabel
              Left = 6
              Top = 4
              Width = 525
              Height = 28
              AutoSize = False
              Caption = 
                'Use the area below to create an optional  "order by" clause at t' +
                'his level.  You may define only one order per level (bank, custo' +
                'mer, lockbox, etc.) .'
              WordWrap = True
            end
            object Label29: TLabel
              Left = 6
              Top = 48
              Width = 570
              Height = 27
              AutoSize = False
              Caption = 
                'Click on the column header to repeat the current data for each f' +
                'ield.  Double click the '#39'Asc/Desc'#39' column to toggle between asce' +
                'nding and descending.'
              WordWrap = True
            end
            object OrderGrid: TAdvStringGrid
              Left = 0
              Top = 88
              Width = 583
              Height = 231
              ColCount = 3
              DefaultRowHeight = 18
              FixedCols = 1
              RowCount = 2
              FixedRows = 1
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowMoving, goEditing, goTabs]
              TabOrder = 0
              OnDragDrop = GridDragDrop
              OnDragOver = GridDragOver
              Bands.Active = False
              Bands.PrimaryColor = clInactiveCaptionText
              Bands.PrimaryLength = 1
              Bands.SecondaryColor = clWindow
              Bands.SecondaryLength = 1
              Bands.Print = False
              AutoNumAlign = False
              AutoSize = False
              VAlignment = vtaTop
              EnhTextSize = False
              EnhRowColMove = True
              SortFixedCols = False
              SortNormalCellsOnly = False
              SizeWithForm = False
              Multilinecells = False
              OnClickCell = GridClickCell
              OnDblClickCell = GridDblClickCell
              OnCanEditCell = GridCanEditCell
              SortDirection = sdAscending
              OnGetEditorType = GridGetEditorType
              SortFull = True
              SortAutoFormat = True
              SortShow = False
              SortIndexShow = False
              EnableGraphics = True
              EnableHTML = True
              EnableWheel = True
              Flat = False
              SortColumn = 0
              HintColor = clYellow
              SelectionColor = clHighlight
              SelectionTextColor = clHighlightText
              SelectionRectangle = False
              SelectionRTFKeep = False
              HintShowCells = False
              HintShowLargeText = False
              OleAcceptFiles = True
              OleAcceptText = True
              PrintSettings.FooterSize = 0
              PrintSettings.HeaderSize = 0
              PrintSettings.Time = ppNone
              PrintSettings.Date = ppNone
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.PageNr = ppNone
              PrintSettings.Title = ppNone
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'MS Sans Serif'
              PrintSettings.Font.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'MS Sans Serif'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'MS Sans Serif'
              PrintSettings.FooterFont.Style = []
              PrintSettings.Borders = pbNoborder
              PrintSettings.BorderStyle = psSolid
              PrintSettings.Centered = False
              PrintSettings.RepeatFixedRows = False
              PrintSettings.RepeatFixedCols = False
              PrintSettings.LeftSize = 0
              PrintSettings.RightSize = 0
              PrintSettings.ColumnSpacing = 0
              PrintSettings.RowSpacing = 0
              PrintSettings.TitleSpacing = 0
              PrintSettings.Orientation = poPortrait
              PrintSettings.FixedWidth = 0
              PrintSettings.FixedHeight = 0
              PrintSettings.UseFixedHeight = False
              PrintSettings.UseFixedWidth = False
              PrintSettings.FitToPage = fpNever
              PrintSettings.PageNumSep = '/'
              PrintSettings.NoAutoSize = False
              PrintSettings.PrintGraphics = False
              HTMLSettings.Width = 100
              Navigation.AllowInsertRow = False
              Navigation.AllowDeleteRow = False
              Navigation.AdvanceOnEnter = False
              Navigation.AdvanceInsert = False
              Navigation.AutoGotoWhenSorted = False
              Navigation.AutoGotoIncremental = False
              Navigation.AutoComboDropSize = False
              Navigation.AdvanceDirection = adLeftRight
              Navigation.AllowClipboardShortCuts = False
              Navigation.AllowSmartClipboard = False
              Navigation.AllowRTFClipboard = False
              Navigation.AllowClipboardAlways = False
              Navigation.AllowClipboardRowGrow = True
              Navigation.AllowClipboardColGrow = True
              Navigation.AdvanceAuto = False
              Navigation.InsertPosition = pInsertBefore
              Navigation.CursorWalkEditor = False
              Navigation.MoveRowOnSort = False
              Navigation.ImproveMaskSel = False
              Navigation.AlwaysEdit = False
              Navigation.CopyHTMLTagsToClipboard = True
              Navigation.LineFeedOnEnter = False
              ColumnSize.Save = False
              ColumnSize.Stretch = True
              ColumnSize.Location = clRegistry
              CellNode.Color = clSilver
              CellNode.NodeType = cnFlat
              CellNode.NodeColor = clBlack
              SizeWhileTyping.Height = False
              SizeWhileTyping.Width = False
              MaxEditLength = 0
              MouseActions.AllSelect = False
              MouseActions.ColSelect = False
              MouseActions.RowSelect = False
              MouseActions.DirectEdit = False
              MouseActions.DirectComboDrop = False
              MouseActions.DisjunctRowSelect = False
              MouseActions.AllColumnSize = False
              MouseActions.AllRowSize = False
              MouseActions.CaretPositioning = False
              IntelliPan = ipVertical
              URLColor = clBlue
              URLShow = False
              URLFull = False
              URLEdit = False
              ScrollType = ssNormal
              ScrollColor = clNone
              ScrollWidth = 16
              ScrollSynch = False
              ScrollProportional = False
              ScrollHints = shNone
              OemConvert = False
              FixedFooters = 0
              FixedRightCols = 0
              FixedColWidth = 15
              FixedRowHeight = 18
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWindowText
              FixedFont.Height = -11
              FixedFont.Name = 'MS Sans Serif'
              FixedFont.Style = []
              FixedAsButtons = False
              FloatFormat = '%.2f'
              WordWrap = True
              ColumnHeaders.Strings = (
                ''
                'Field Name'
                'Asc/Desc')
              Lookup = False
              LookupCaseSensitive = False
              LookupHistory = False
              ShowSelection = False
              RowIndicator.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33333FF3333333333333447333333333333377FFF33333333333744473333333
                333337773FF3333333333444447333333333373F773FF3333333334444447333
                33333373F3773FF3333333744444447333333337F333773FF333333444444444
                733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
                999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
                33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
                333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
                33333777333333333333CC333333333333337733333333333333}
              BackGround.Top = 0
              BackGround.Left = 0
              BackGround.Display = bdTile
              BackGround.Cells = bcNormal
              Filter = <>
              ColWidths = (
                15
                433
                112)
              RowHeights = (
                18
                18)
            end
            object OrderRemoveBtn: TButton
              Left = 595
              Top = 150
              Width = 173
              Height = 25
              Caption = 'Remove Current Field'
              TabOrder = 1
              OnClick = RemoveBtnClick
            end
            object UpBtn: TBitBtn
              Left = 595
              Top = 178
              Width = 83
              Height = 27
              Caption = 'Up '
              TabOrder = 2
              OnClick = UpBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF536CCD2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                A9B5E62344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C09CAA
                E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E623
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000AEBAE7ECEEF9FFFFFFFFFFFFFFFF
                FFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                91A1DF000000647BD22344C06179D19DACE3DEE3F5FFFFFFA9B5E62344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C09CAAE2FFFFFFDBE1F498A8E15C74D02344C09DACE3000000FFFFFF7287D6
                2344C02344C02344C05770CFA9B5E62344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C08FA0DF5770CF2344C023
                44C02344C07B8FD9FFFFFF000000FFFFFFFFFFFF5972CF2344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C05E76D0FFFFFFFFFFFF00
                0000FFFFFFFFFFFFF8F9FD2344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0FCFCFEFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFDF
                E3F52344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0E5E9F7FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFBEC8EC2344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C0C5CEEEFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A2B0E4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF7A8ED92344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C07F92DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E76D02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C061
                79D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFE2344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C0FEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFE6E9F72344C02344C02344C02344C02344C02344C02344C02344
                C02344C0E8ECF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7CFEF
                2344C02344C02344C02344C02344C02344C02344C0C9D1EFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4B2E52344C02344C02344C0
                2344C02344C0A7B4E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF8295DB2344C02344C02344C08496DBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF647BD22344C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
            object DownBtn: TBitBtn
              Left = 685
              Top = 178
              Width = 83
              Height = 27
              Cancel = True
              Caption = 'Down '
              TabOrder = 3
              OnClick = DownBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF657CD22344C0647BD2FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF8496DB2344C02344C02344C08295DBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7B4E52344C02344C02344C0
                2344C02344C0A4B2E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFC8D0EF2344C02344C02344C02344C02344C02344C02344C0C7CFEF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8ECF82344C02344C023
                44C02344C02344C02344C02344C02344C02344C0E6E9F7FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFE2344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C0FCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6179D12344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C05E
                76D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7E92DA2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C07A8ED9FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2B0E4
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFC5CEEE2344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C0BEC8ECFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE5
                E9F72344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0DFE3F5FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFCFCFE2344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0F8F9FDFFFFFFFFFFFF000000FFFF
                FFFFFFFF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C05972CFFFFFFFFFFFFF000000FFFFFF7B8FD92344C02344C02344
                C05770CF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C0627AD25770CF2344C02344C02344C07287D6
                FFFFFF0000009DACE32344C05C74D098A8E1DBE1F4FFFFFF9CAAE22344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C0A9B5E6FFFFFFDEE3F59DACE36179D12344C0FFFFFF000000FFFFFFE4E8F7
                FFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFF
                FFFFFFFFFFECEEF9FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                9CAAE22344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A9B5
                E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE223
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF536CCD536CCD536CCD536CCD536CCD536CCD536CCD536CCD53
                6CCD536CCD536CCD536CCD536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
          end
          object LimitTab: TTabSheet
            Caption = 'Limit Data'
            object TypeLbl: TLabel
              Left = 6
              Top = 4
              Width = 525
              Height = 28
              AutoSize = False
              Caption = 
                'Use the area below to create an optional  "where" clause to limi' +
                't the selection of records at this level.  You may define only o' +
                'ne set of limits per level (bank, customer, lockbox, etc.) .'
              WordWrap = True
            end
            object Label26: TLabel
              Left = 6
              Top = 48
              Width = 570
              Height = 27
              AutoSize = False
              Caption = 
                'Click on the column header to repeat the current data for each f' +
                'ield.'
              Layout = tlBottom
              WordWrap = True
            end
            object Label23: TLabel
              Left = 592
              Top = 86
              Width = 193
              Height = 56
              AutoSize = False
              Caption = 
                'Drag and drop field names on the grid or use the button to selec' +
                't.  Drag the row button on the grid to change order or use the u' +
                'p/down buttons.'
              WordWrap = True
            end
            object Label20: TLabel
              Left = 8
              Top = 352
              Width = 110
              Height = 13
              Caption = '"Where clause" as text'
            end
            object LimitGrid: TAdvStringGrid
              Left = 0
              Top = 88
              Width = 583
              Height = 251
              ColCount = 7
              DefaultRowHeight = 18
              FixedCols = 1
              RowCount = 2
              FixedRows = 1
              Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goColSizing, goRowMoving, goEditing, goTabs]
              TabOrder = 0
              OnDragDrop = GridDragDrop
              OnDragOver = GridDragOver
              OnKeyPress = LimitGridKeyPress
              Bands.Active = False
              Bands.PrimaryColor = clInactiveCaptionText
              Bands.PrimaryLength = 1
              Bands.SecondaryColor = clWindow
              Bands.SecondaryLength = 1
              Bands.Print = False
              AutoNumAlign = False
              AutoSize = False
              VAlignment = vtaTop
              EnhTextSize = False
              EnhRowColMove = True
              SortFixedCols = False
              SortNormalCellsOnly = False
              SizeWithForm = False
              Multilinecells = False
              OnCellChanging = LimitGridCellChanging
              OnClickCell = GridClickCell
              OnDblClickCell = GridDblClickCell
              OnCanEditCell = GridCanEditCell
              OnCellValidate = LimitGridCellValidate
              SortDirection = sdAscending
              OnGetEditorType = GridGetEditorType
              SortFull = True
              SortAutoFormat = True
              SortShow = False
              SortIndexShow = False
              EnableGraphics = True
              EnableHTML = True
              EnableWheel = True
              Flat = False
              SortColumn = 0
              HintColor = clYellow
              SelectionColor = clHighlight
              SelectionTextColor = clHighlightText
              SelectionRectangle = False
              SelectionRTFKeep = False
              HintShowCells = False
              HintShowLargeText = False
              OleAcceptFiles = True
              OleAcceptText = True
              PrintSettings.FooterSize = 0
              PrintSettings.HeaderSize = 0
              PrintSettings.Time = ppNone
              PrintSettings.Date = ppNone
              PrintSettings.DateFormat = 'dd/mm/yyyy'
              PrintSettings.PageNr = ppNone
              PrintSettings.Title = ppNone
              PrintSettings.Font.Charset = DEFAULT_CHARSET
              PrintSettings.Font.Color = clWindowText
              PrintSettings.Font.Height = -11
              PrintSettings.Font.Name = 'MS Sans Serif'
              PrintSettings.Font.Style = []
              PrintSettings.HeaderFont.Charset = DEFAULT_CHARSET
              PrintSettings.HeaderFont.Color = clWindowText
              PrintSettings.HeaderFont.Height = -11
              PrintSettings.HeaderFont.Name = 'MS Sans Serif'
              PrintSettings.HeaderFont.Style = []
              PrintSettings.FooterFont.Charset = DEFAULT_CHARSET
              PrintSettings.FooterFont.Color = clWindowText
              PrintSettings.FooterFont.Height = -11
              PrintSettings.FooterFont.Name = 'MS Sans Serif'
              PrintSettings.FooterFont.Style = []
              PrintSettings.Borders = pbNoborder
              PrintSettings.BorderStyle = psSolid
              PrintSettings.Centered = False
              PrintSettings.RepeatFixedRows = False
              PrintSettings.RepeatFixedCols = False
              PrintSettings.LeftSize = 0
              PrintSettings.RightSize = 0
              PrintSettings.ColumnSpacing = 0
              PrintSettings.RowSpacing = 0
              PrintSettings.TitleSpacing = 0
              PrintSettings.Orientation = poPortrait
              PrintSettings.FixedWidth = 0
              PrintSettings.FixedHeight = 0
              PrintSettings.UseFixedHeight = False
              PrintSettings.UseFixedWidth = False
              PrintSettings.FitToPage = fpNever
              PrintSettings.PageNumSep = '/'
              PrintSettings.NoAutoSize = False
              PrintSettings.PrintGraphics = False
              HTMLSettings.Width = 100
              Navigation.AllowInsertRow = False
              Navigation.AllowDeleteRow = False
              Navigation.AdvanceOnEnter = False
              Navigation.AdvanceInsert = False
              Navigation.AutoGotoWhenSorted = False
              Navigation.AutoGotoIncremental = False
              Navigation.AutoComboDropSize = True
              Navigation.AdvanceDirection = adLeftRight
              Navigation.AllowClipboardShortCuts = False
              Navigation.AllowSmartClipboard = False
              Navigation.AllowRTFClipboard = False
              Navigation.AllowClipboardAlways = False
              Navigation.AllowClipboardRowGrow = True
              Navigation.AllowClipboardColGrow = True
              Navigation.AdvanceAuto = False
              Navigation.InsertPosition = pInsertBefore
              Navigation.CursorWalkEditor = False
              Navigation.MoveRowOnSort = False
              Navigation.ImproveMaskSel = False
              Navigation.AlwaysEdit = False
              Navigation.CopyHTMLTagsToClipboard = True
              Navigation.LineFeedOnEnter = False
              ColumnSize.Save = False
              ColumnSize.Stretch = True
              ColumnSize.Location = clRegistry
              CellNode.Color = clSilver
              CellNode.NodeType = cnFlat
              CellNode.NodeColor = clBlack
              SizeWhileTyping.Height = False
              SizeWhileTyping.Width = False
              MaxEditLength = 0
              MouseActions.AllSelect = False
              MouseActions.ColSelect = False
              MouseActions.RowSelect = True
              MouseActions.DirectEdit = False
              MouseActions.DirectComboDrop = True
              MouseActions.DisjunctRowSelect = False
              MouseActions.AllColumnSize = False
              MouseActions.AllRowSize = False
              MouseActions.CaretPositioning = False
              IntelliPan = ipVertical
              URLColor = clBlue
              URLShow = False
              URLFull = False
              URLEdit = False
              ScrollType = ssNormal
              ScrollColor = clNone
              ScrollWidth = 16
              ScrollSynch = False
              ScrollProportional = False
              ScrollHints = shNone
              OemConvert = False
              FixedFooters = 0
              FixedRightCols = 0
              FixedColWidth = 15
              FixedRowHeight = 18
              FixedFont.Charset = DEFAULT_CHARSET
              FixedFont.Color = clWindowText
              FixedFont.Height = -11
              FixedFont.Name = 'MS Sans Serif'
              FixedFont.Style = []
              FixedAsButtons = False
              FloatFormat = '%.2f'
              WordWrap = True
              ColumnHeaders.Strings = (
                ''
                'Field Name'
                'Logical Opr'
                'Value'
                '(Left'
                'Right)'
                'Opr on Next Row')
              Lookup = False
              LookupCaseSensitive = False
              LookupHistory = False
              ShowSelection = False
              RowIndicator.Data = {
                76010000424D7601000000000000760000002800000020000000100000000100
                04000000000000010000120B0000120B00001000000000000000000000000000
                800000800000008080008000000080008000808000007F7F7F00BFBFBF000000
                FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
                33333FF3333333333333447333333333333377FFF33333333333744473333333
                333337773FF3333333333444447333333333373F773FF3333333334444447333
                33333373F3773FF3333333744444447333333337F333773FF333333444444444
                733333373F3333773FF333334444444444733FFF7FFFFFFF77FF999999999999
                999977777777777733773333CCCCCCCCCC3333337333333F7733333CCCCCCCCC
                33333337F3333F773333333CCCCCCC3333333337333F7733333333CCCCCC3333
                333333733F77333333333CCCCC333333333337FF7733333333333CCC33333333
                33333777333333333333CC333333333333337733333333333333}
              BackGround.Top = 0
              BackGround.Left = 0
              BackGround.Display = bdTile
              BackGround.Cells = bcNormal
              Filter = <>
              ColWidths = (
                15
                120
                61
                195
                43
                48
                74)
              RowHeights = (
                18
                18)
            end
            object LimitRemoveBtn: TButton
              Left = 595
              Top = 150
              Width = 173
              Height = 25
              Caption = 'Remove Current Field'
              TabOrder = 1
              OnClick = RemoveBtnClick
            end
            object WhereMemo: TMemo
              Left = 0
              Top = 376
              Width = 585
              Height = 121
              ReadOnly = True
              ScrollBars = ssVertical
              TabOrder = 6
            end
            object ViewWhereBtn: TButton
              Left = 595
              Top = 208
              Width = 173
              Height = 25
              Caption = 'View as Text'
              TabOrder = 4
              OnClick = ViewWhereBtnClick
            end
            object TestWhereBtn: TButton
              Left = 595
              Top = 236
              Width = 173
              Height = 25
              Caption = 'Validate'
              TabOrder = 5
              OnClick = TestWhereBtnClick
            end
            object LimitUpBtn: TBitBtn
              Left = 595
              Top = 178
              Width = 83
              Height = 27
              Caption = 'Up '
              TabOrder = 2
              OnClick = UpBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF536CCD2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                A9B5E62344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C09CAA
                E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E623
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA9B5E62344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C09CAAE2FFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000AEBAE7ECEEF9FFFFFFFFFFFFFFFF
                FFFFFFFFA9B5E62344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                91A1DF000000647BD22344C06179D19DACE3DEE3F5FFFFFFA9B5E62344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C09CAAE2FFFFFFDBE1F498A8E15C74D02344C09DACE3000000FFFFFF7287D6
                2344C02344C02344C05770CFA9B5E62344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C08FA0DF5770CF2344C023
                44C02344C07B8FD9FFFFFF000000FFFFFFFFFFFF5972CF2344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C05E76D0FFFFFFFFFFFF00
                0000FFFFFFFFFFFFF8F9FD2344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0FCFCFEFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFDF
                E3F52344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0E5E9F7FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFBEC8EC2344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C0C5CEEEFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A2B0E4
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFF7A8ED92344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C07F92DAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF5E76D02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C061
                79D1FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFCFCFE2344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C0FEFEFEFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFE6E9F72344C02344C02344C02344C02344C02344C02344C02344
                C02344C0E8ECF8FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC7CFEF
                2344C02344C02344C02344C02344C02344C02344C0C9D1EFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA4B2E52344C02344C02344C0
                2344C02344C0A7B4E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF8295DB2344C02344C02344C08496DBFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFF647BD22344C0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
            object LimitDownBtn: TBitBtn
              Left = 685
              Top = 178
              Width = 83
              Height = 27
              Cancel = True
              Caption = 'Down '
              TabOrder = 3
              OnClick = DownBtnClick
              Glyph.Data = {
                C2070000424DC20700000000000036000000280000001B000000170000000100
                1800000000008C07000000000000000000000000000000000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FF536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF657CD22344C0647BD2FFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF8496DB2344C02344C02344C08295DBFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7B4E52344C02344C02344C0
                2344C02344C0A4B2E5FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFC8D0EF2344C02344C02344C02344C02344C02344C02344C0C7CFEF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE8ECF82344C02344C023
                44C02344C02344C02344C02344C02344C02344C0E6E9F7FFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFEFEFE2344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C0FCFCFEFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF6179D12344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C05E
                76D0FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFF7E92DA2344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C07A8ED9FFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA2B0E4
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C09CAAE2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFC5CEEE2344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C0BEC8ECFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFE5
                E9F72344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C0DFE3F5FFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFCFCFE2344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0F8F9FDFFFFFFFFFFFF000000FFFF
                FFFFFFFF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C05972CFFFFFFFFFFFFF000000FFFFFF7B8FD92344C02344C02344
                C05770CF5E76D02344C02344C02344C02344C02344C02344C02344C02344C023
                44C02344C02344C02344C02344C0627AD25770CF2344C02344C02344C07287D6
                FFFFFF0000009DACE32344C05C74D098A8E1DBE1F4FFFFFF9CAAE22344C02344
                C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C023
                44C0A9B5E6FFFFFFDEE3F59DACE36179D12344C0FFFFFF000000FFFFFFE4E8F7
                FFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344
                C02344C02344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFF
                FFFFFFFFFFECEEF9FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                9CAAE22344C02344C02344C02344C02344C02344C02344C02344C02344C02344
                C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF00
                0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C0
                2344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0A9B5
                E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C02344C02344C02344C0
                2344C02344C02344C02344C02344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE223
                44C02344C02344C02344C02344C02344C02344C02344C02344C02344C02344C0
                2344C02344C0A9B5E6FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF9CAAE22344C02344C02344C02344C023
                44C02344C02344C02344C02344C02344C02344C02344C02344C0FFFFFFFFFFFF
                FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFFFFFFFFFF536CCD536CCD536CCD536CCD536CCD536CCD536CCD536CCD53
                6CCD536CCD536CCD536CCD536CCDFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
                FFFFFF000000}
            end
          end
        end
        object HintBar: TStatusBar
          Left = 1
          Top = 674
          Width = 872
          Height = 19
          Panels = <>
          SimplePanel = False
        end
      end
    end
  end
  object ExtractPathOpenDialog: TOpenDialog
    DefaultExt = '*.cxr'
    Filter = 
      'CheckBOX Extract Result Files (*.cxr)|*.cxr|Text files (*.txt)|*' +
      '.txt|All files (*.*)|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofEnableSizing]
    Left = 22
    Top = 82
  end
end
