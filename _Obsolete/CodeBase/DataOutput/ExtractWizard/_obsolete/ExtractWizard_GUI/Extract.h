//---------------------------------------------------------------------------

#ifndef ExtractH
#define ExtractH
//---------------------------------------------------------------------------

#include "dir.h"
#include "CustomField.h"
#include "DMPData.h"
#include "TestExtract.h"
#include "Defines.h"
#include "TimeStampTrace.h"
#include "Script.h"
#include <vector>
#include "CustomFormat.h"
#include "HelperClasses.h"
#include "ImageFileEmbeder.h"
#include "LogHelper.h" // CR 16217 06/12/2006 DJK

struct SAttrib
{
  String FieldName;
  TFieldType DataType;
  String DisplayName;
  int Width;
  char FillChar;
  bool RightJustify;
  bool UseQuotes;
  String Format;
  int OccursGroup; // 0:Checks 1:Stubs 2:Docs
  int OccursCount;
  SAttrib() {Clear();}
  SAttrib(const SAttrib& obj) {*this = obj;}
  void Clear() {FieldName = ""; DataType = ftUnknown; DisplayName = ""; Width = 0; FillChar = ' '; RightJustify = false; UseQuotes = false; Format = ""; OccursGroup = -1; OccursCount = 0;}
  SAttrib& operator = (const SAttrib& obj) {FieldName = obj.FieldName; DataType = obj.DataType; DisplayName = obj.DisplayName; Width = obj.Width; FillChar = obj.FillChar; RightJustify = obj.RightJustify; UseQuotes = obj.UseQuotes; Format = obj.Format; OccursGroup = obj.OccursGroup; OccursCount = obj.OccursCount; return *this;}
};

typedef std::vector<SAttrib> SAttribArr;
typedef std::vector<SAttribArr> SAttribArrArr;

class LogFile32Loader;
class PicsLoader;

class TExtract
{
  public:
    String m_sLastExceptionError;
  private:
    std::vector<int>    m_vBatSeqList;                  // CR 16503 10/03/2006 ALH
    std::vector<String> m_vDocumentFileDescriptors;     // CR 16503 10/03/2006 ALH
    ImageList *m_vIMGlist;
    long m_lJointDetailSequenceNumb;
    bool m_bIsCancelled;
    TAggregateList m_aggregateFields;
    TStdFieldList m_standardFields;
    TRecordCountList m_recordCounts;
    TInputList m_inputList;
    TTimeStampTrace* m_batchTrace;
    ESNBatchUpdate m_esnBatchUpdate; // CR 13806 10/04/2005 DJK
    String m_sScriptFile;
    TDateTime m_dtCurrentProcDate;    //as per Date.ini
    TDateTime m_dtProcRunDate; // as entered in Run dialog, after some processing
    FieldCollection m_detailRecFields;
    String FImagePath;
    String FBank, FCustomer, FLockbox, FBatch, FProcDate, FDepDate, FOperatorID; // CR 13806 9/12/2005 DJK - Added 'FOperatorID'
    int FHandle;
    int FCallerThreadID;
    int FExtractSequenceNumber; // CR 13806 10/04/2005 DJK
    TCancelEvent FOnCancel;
    TRecordEvent FOnRecord;
    TImageProgressEvent FOnImageProgress; // CR 5815 03/03/2004 DJK
    TFinishEvent FOnFinish;
    TErrorEvent FOnError;
    bool m_bHasAggregateHeaders;
    bool m_bUseADT;
    ImageFileManager m_imageFileManager; // CR 5815 03/03/2004 DJK
    int m_iLastScriptNum;
    int m_iLastType;
    int m_iMaxType;
    TList* m_tlDataSetList;
    int RecordCountArray[rtDetail + 1];
    TCounterFieldsManager m_counterFields;
    int m_iScriptNum;
    OutputManager m_outputMan;
    TScript* m_script;
    String m_sTempDir;
    String m_sTempFileName;
    String m_sTempHtmlFileName;
    TTestExtractForm *m_frmTestExtractForm;
    // LogFile32Loader* m_logFile32Dll; // CR 16217 06/12/2006 DJK - Remove this reference (no longer using this old-style logging)
    AppLogging* m_logFile; // CR 16217 06/12/2006 DJK - Add reference to standard logging class.
    PicsLoader* m_picsDll;
    ConfirmationFields m_confirmationFields;
    LineCountManager m_lineCounter;
    bool m_bDoSpecialChecksTally;        // CR 13806 9/09/2005 DJK
    bool m_bDoSpecialStubsTally;         // CR 13806 9/09/2005 DJK
    int m_iWKID;                         // CR 13806 9/12/2005 DJK
    ExtractAuditInfo m_extractAuditInfo; // CR 13806 9/12/2005 DJK

    bool m_bEmbedImageInDataFile;               //CR 9916 11/05/2004 ALH
    bool m_bExtractImages;                      //CR 9916 11/11/2004 ALH
    void EmbedImagesIntoDataFile();
    void AddParams(const int Type, String &WhereClause);
    void AddModifiedParams(const int Type, String &WhereClause);
    String GetDepDateSQL(const String& sDepDate);
    String GetDepDateSQL(const String& sDepDate1, const String& sDepDate2);
    String GetCookieValue(const String& Cookie);
    void Cleanup(bool Success);
    void ClearDetailField(const int Type);
    void CloseQueries();
    void DefineDataSet(const int Type);
    bool ThereAreOccursRecords();
    bool ThisIsAnOccursRecord(const int ScriptNum);
    void DoExtract();
    void OptionallyTallyCheckAndStubCountsAndAmountsForTransaction(); // CR 13806 9/09/2005 DJK
    int CalculateHowManyOfThisRecToGoOut(const int ScriptNum, const int ChecksCount, const int StubsCount, const int DocsCount);
    void DoRecordAfter(const int Type, const int Pos, const int HtmlPos);
    void DoRecordBefore(const int Type, int &Pos, int &HtmlPos);
    void FillDetailFields();
    void FillDataSetList();
    void LogMessage(const String& Msg);
    void WriteToLogFile(const String& Msg);
    void FlipFiles(const String& FileName, const String& HtmlFileName, const int Type, const int Pos, const int HtmlPos);
    void FormatAggregateData(const int Level, SAttrib *FieldAttrib, String &Data);
    void FormatAggregateData(const int Level, const TRecordFieldItem* field, String &Data);
    void FormatData(TDMPADODataSet* dataSet, const TRecordFieldItem* field, String &Data);
    void FormatDetailData(const TRecordFieldItem* field, String &Data);
    void FormatRecordCountData(SAttrib *FieldAttrib, String &Data);
    void FormatRecordCountData(const TRecordFieldItem* field, String &Data);
    void FormatStandardData(TDMPADODataSet* dataSet, const TRecordFieldItem* field, String &Data, const int ScriptNum, const int iType = -1, const bool bBeingInserted = false);
    TDMPADODataSet* GetDataSetForImgsAtJointDetail(int iRecordType); //CR 10943 4/29/2005 ALH
    int ReturnTypeOfImageInformation(String sScriptFieldName);  //CR 10943 5/4/2005 ALH
    String GetCountOfChecksAndDocsForBatch(); //CR 10943 5/10/2005 ALH
    String IncrementJointDetailSequence();    // CR  6/27/20005 ALH
    String GetImageData(TDMPADODataSet* Qry, const TRecordFieldItem* field, String Data, const int iScriptNum, const int iType); //CR 10943 5/4/2005 ALH
    void GetAttributes(SAttrib *FieldAttrib, const String& Attrib);
    TDataSource* GetDataSource(const int Type);
    void GetDetailData(const int Type, TDMPADODataSet *dataSet, FieldCollection& fieldCollection, const int iRow);
    void GetDetailParams(bool &HasChecks, bool &HasStubs, bool &HasDocs);
    String GetKey(const int Type);
    int  GetNextScript(const int Start, const int Type);
    void GetParallelDetailData(const int Type, FieldCollection& fieldCollection, const int RecsThisPass);
    String GetTemporaryPath();
    int GetRecordCountSum();
    bool IsValid(const String& ValidChars, const String& Value);
    void LoadDataSet(const String& sTable, const String& sRootTable, const String& sKey, TDMPADODataSet *dataSet, const int iType, bool bIsTest); // CR 12104 3/30/2005 DJK - Added new (2nd) parameter
    bool OpenQueries();
    void ParseTargetFileName();
    void PrepareQueries();
    void HandleHTMLOuput(bool IsData, const int ScriptNum, const String& Data, const TRecordFieldItem* field, String& HeaderLine, String& HtmlLine);
    void PrintDetailOccursRecords(const FieldCollection& fieldCollection, const int ScriptNum, const int ChecksCountTotal, const int StubsCountTotal, const int DocsCountTotal, int& ChecksCount, int& StubsCount, int& DocsCount);
    void PrintDetailRecords(const FieldCollection& fieldCollection, const int iRow, const int ScriptNum, const int iType = -1, const bool bBeingInserted = false);
    void PrintRecord(TDMPADODataSet *dataSet, const int Index, const int Type, const bool bBeingInserted = false);
    void PrintRecordsOfType(const int Type, const bool bBeingInserted = false);
    void SetBank(const String& BankID);
    void SetCustomer(const String& CustID);
    void SetLockbox(const String& LockboxID);
    void SetStandardFields();
    void SetBatch(const String& BatchID);
    void SetProcDate(const String& DateStr);
    void SetDepDate(const String& DepStr);
    void TestDetailRecord(const int Index);
    void UpdateBatchTrace();
    void DoFinalStep();
    void DoConfirmationFieldsTally();
    int GetDetailType(const String sTableName);
    String GetTargetFile() const {return m_outputMan.GetTargetFile();}
    void SetTargetFile(const String& sTargetFile) {m_outputMan.SetTargetFile(sTargetFile);}
    void SendTargetFileNameViaThreadMessages(int iCallerThreadID, const String& sTargetFile);
    void EnsureCorrectDataEntryTableNames(FieldCollection& fieldList); // CR 5272 1/07/2004 DJK
    void VerifyData(String &Data, int iFieldSize);
    String GetExtractAuditIdGUID() const; // CR 14003 9/17/2005 DJK - Added this function
    void RunPostProcessingDLL();   //CR10943 03/30/2005 ALH - New Function for hooking in a post processing DLL
    String GenerateUniqueTempFileName(String sFileType);  //CR 18447 10/06/02006 ALH
    void  CleanUpTempFiles();                             //CR 18447 10/06/02006 ALH
  public:
    TExtract();
    ~TExtract();
    void DoTest(const String& Setup, const String& RecordName);
    void RemoveTestForm();
    void Run(const String& Setup, const String& TimeStampField, const String& TimeStamps);
    __property String Bank = {read=FBank, write=SetBank};
    __property String Customer = {read=FCustomer, write=SetCustomer};
    __property String Lockbox = {read=FLockbox, write=SetLockbox};
    __property String Batch = {read=FBatch, write=SetBatch};
    __property int ExtractSequenceNumber = {read=FExtractSequenceNumber, write=FExtractSequenceNumber}; // CR 13806 10/04/2005 DJK
    __property int CallerHandle = {read=FHandle, write=FHandle};
    __property int CallerThreadID = {read=FCallerThreadID, write=FCallerThreadID};
    __property String ProcDate = {read=FProcDate, write=SetProcDate};
    __property String DepDate = {read=FDepDate, write=SetDepDate};
    __property String OperatorID = {read=FOperatorID, write=FOperatorID}; // CR 13806 9/12/2005 DJK
    __property String TargetFile = {read=GetTargetFile, write=SetTargetFile};
    __property String ImagePath = {read=FImagePath, write=FImagePath};
    __property String ExtractAuditIdGUID = {read=GetExtractAuditIdGUID}; // CR 14003 9/17/2005 DJK - Added this property
    __property TCancelEvent OnCancel={read=FOnCancel, write=FOnCancel};
    __property TErrorEvent OnError={read=FOnError, write=FOnError};
    __property TFinishEvent OnFinish={read=FOnFinish, write=FOnFinish};
    __property TRecordEvent OnRecord={read=FOnRecord, write=FOnRecord};
    __property TImageProgressEvent OnImageProgress={read=FOnImageProgress, write=FOnImageProgress}; // CR 5815 03/03/2004 DJK

};
class CustomDLLObject
{
  private:
    String sDllName;
    String sError;
    HMODULE hMod;
  public:
    CustomDLLObject(const String& dllName)
    {
      m_sDDLFunctionName = GetFunctionName(dllName);
      sDllName = dllName;
      hMod = 0;
    }
    ~CustomDLLObject()
    {
      if (hMod)
        FreeLibrary(hMod);
    }

    void LoadDll()
    {
      if (!hMod) {
        hMod = LoadLibrary(sDllName.c_str());

        if (!hMod) {
          sError = "DLL '" + sDllName + "' failed to load";
          void* lpMsgBuf;

          FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS, NULL, GetLastError(), MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPTSTR) &lpMsgBuf, 0, NULL);
          sError += "\n\n";

          if (((char*) lpMsgBuf)[0])
            sError += (char*) lpMsgBuf;

          else
            sError += "System could not supply a reason for failure";

          LocalFree( lpMsgBuf );
          throw DMPException(sError);
        }
      }
    }
    void FreeDll()
    {
      if (hMod)
        FreeLibrary(hMod);
      hMod = 0;
    }
    bool IsDllLoaded()
    {
      return (hMod != 0);
    }

  private:
    ProcAddressHolder pahCallToFunction;
    String m_sDDLFunctionName;

    String GetFunctionName(String dllName){
        String sRval = dllName;
        int iSlashPos = 0;
        int iStrSize  = 0;
                while (sRval.Pos("\\")){
                     iSlashPos = sRval.Pos("\\");
                     iStrSize = sRval.Length();
                     sRval.Delete(1,iSlashPos);
                }
                if (sRval.Pos(".") > -1)
                        sRval.Delete(sRval.Pos("."),sRval.Length());
        return sRval;
    }

  public:

    bool CallToFunction(char *P1, char *P2,char *P3)
    {
      if (hMod) {
        typedef bool (*lpCallToFunction)(char*,char* ,char*);
        String s_ShowAbout = m_sDDLFunctionName;
        if (pahCallToFunction.farProc == 0)
          pahCallToFunction.farProc = GetProcAddress(hMod, s_ShowAbout.c_str());
        if (pahCallToFunction.farProc == 0) {
          s_ShowAbout = "_" + s_ShowAbout;
          pahCallToFunction.farProc = GetProcAddress(hMod, s_ShowAbout.c_str());
        }
        lpCallToFunction useCallToFunction = (lpCallToFunction) pahCallToFunction.farProc;
        if (useCallToFunction) {
          return useCallToFunction(P1,P2,P3);
        }
        else {
          sError = "Could not find address for function '" + String("CallToFunction") + "' in DLL '" + sDllName + "'";
          throw DMPException(sError);
        }
      }
      else {
        sError = "Cannot call function '" + String("CallToFunction") + "' in DLL '" + sDllName + "' when DLL not loaded";
        throw DMPException(sError);
      }
    }
};
#endif
