//----------------------------------------------------------------------------
#include <vcl\vcl.h>
#pragma hdrstop

#include "SetupRpt.h"
#include <IniFiles.hpp>
#include "ReportPreview.h"
#include "Script.h"
#include "CustomFormat.h"
#include "DMPExceptions.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////
//                                                                                                   //
// CR 14584 - The code for this report is not simple and needs some explanation.                     //
//            Please look for the document "ExtractWizard SetupReport Documentation.doc" which will  //
//              contain an in-depth explanation of this module. It's possible it will not be in MKS  //
//              by the time this node is checkpointed, but it will get in shortly thereafter.        //
//                                                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////////

//----------------------------------------------------------------------------
#pragma resource "*.dfm"
TSetupReport *SetupReport;
//----------------------------------------------------------------------------
__fastcall TSetupReport::TSetupReport(TComponent* Owner)
    : TQuickRep(Owner)
{
  DMPTRY {
    m_slRecordData = new TStringList();
    m_slOrderData  = new TStringList();
    m_slLimitData  = new TStringList();
    m_scriptFile   = 0;
  }
  DMPCATCHTHROW("TSetupReport::TSetupReport")
}
//----------------------------------------------------------------------------
__fastcall TSetupReport::~TSetupReport()
{
  DMPTRY {
    delete m_slRecordData;
    delete m_slOrderData;
    delete m_slLimitData;
    if (m_scriptFile)
      delete m_scriptFile;
  }
  DMPCATCHTHROW("TSetupReport::~TSetupReport")
}
//----------------------------------------------------------------------------
void TSetupReport::RecursivelyDetermineWhichRecordsUseOrderLimits(TTreeNode* currentNode, TStringList* slOrderLimitsUsedByRecords)
{
  DMPTRY {
    if (currentNode->Level == 2) {
      int iRecordTypeSpecific = m_recordForm->RecordType[currentNode->Text];
      int iRecordSubType      = TRecHelper::GetRecordSubType(iRecordTypeSpecific);
      String sOrderDataName, sLimitDataName;
      DetermineExistenceOfOrderLimitData(iRecordSubType, sOrderDataName, sLimitDataName, slOrderLimitsUsedByRecords, 0);
    }
    for (int i = 0; i < currentNode->Count; ++i)
      RecursivelyDetermineWhichRecordsUseOrderLimits(currentNode->Item[i], slOrderLimitsUsedByRecords);
  }
  DMPCATCHTHROW("TSetupReport::RecursivelyDetermineWhichRecordsUseOrderLimits")
}
//----------------------------------------------------------------------------
void TSetupReport::RecursivelySetupRecords(TStrings* strings, TTreeNode* currentNode, TStringList* slOrderLimitsUsedOverall, const TStringList* slOrderLimitsUsedByRecords)
{
  DMPTRY {
    String sBanner;
    String sRecordDataName;
    String sOrderDataName;
    String sLimitDataName;
    if (currentNode->Level == 2) {
      sBanner = currentNode->Parent->Text;
      int iRecordTypeSpecific = m_recordForm->RecordType[currentNode->Text];
      int iRecordTypeGeneral  = TRecHelper::GetRecordType   (iRecordTypeSpecific);
      int iRecordSubType      = TRecHelper::GetRecordSubType(iRecordTypeSpecific);
      if (iRecordTypeGeneral == rtDetail && iRecordSubType != rtDetail) // If 'iRecordSubType == rtDetail' then the extra text '(Joint Detail)' is redundant!
        sBanner += " (Joint Detail)";
      sBanner += " - " + currentNode->Text;
      sRecordDataName = currentNode->Text;
      DetermineExistenceOfOrderLimitData(iRecordSubType, sOrderDataName, sLimitDataName, slOrderLimitsUsedOverall, 0);
      strings->AddObject(sBanner, (TObject*) (new RecordData(sRecordDataName, sOrderDataName, sLimitDataName)));
    }
    else {
      if (currentNode->Level == 1 && currentNode->Count == 0) {
        // In this case there will be no defined record for this level, BUT THERE MIGHT STILL BE ORDER/LIMIT INFO - Check for it!
        int iSubType;
        DetermineLevelNameAndSubType(currentNode->Text, sBanner, iSubType);
        if (iSubType != -1) {
          if (DetermineExistenceOfOrderLimitData(iSubType, sOrderDataName, sLimitDataName, slOrderLimitsUsedOverall, slOrderLimitsUsedByRecords))
            strings->AddObject(sBanner, (TObject*) (new RecordData("", sOrderDataName, sLimitDataName)));
        }
      }
      for (int i = 0; i < currentNode->Count; ++i)
        RecursivelySetupRecords(strings, currentNode->Item[i], slOrderLimitsUsedOverall, slOrderLimitsUsedByRecords);
    }
  }
  DMPCATCHTHROW("TSetupReport::RecursivelySetupRecords")
}
//----------------------------------------------------------------------------
bool TSetupReport::DetermineExistenceOfOrderLimitData(const int iRecordSubType, String& sOrderDataName, String& sLimitDataName, TStringList* slOrderLimitDataTablesUsed, const TStringList* slOrderLimitDataToAvoid)
{
  bool bResult = false;
  DMPTRY {
    sOrderDataName = sLimitDataName = "";
    String sCurrentDBTable;
    switch (iRecordSubType) {
      case rtBank        : sCurrentDBTable = "Bank"        ; break;
      case rtCustomer    : sCurrentDBTable = "Customer"    ; break;
      case rtLockbox     : sCurrentDBTable = "Lockbox"     ; break;
      case rtBatch       : sCurrentDBTable = "Batch"       ; break;
      case rtTransactions: sCurrentDBTable = "Transactions"; break;
      case rtChecks      : sCurrentDBTable = "Checks"      ; break;
      case rtStubs       : sCurrentDBTable = "Stubs"       ; break;
      case rtDocs        : sCurrentDBTable = "Documents"   ; break;
    }
    if (sCurrentDBTable.Length())
      if (slOrderLimitDataToAvoid == 0 || slOrderLimitDataToAvoid->IndexOf(sCurrentDBTable) == -1)
        if (slOrderLimitDataTablesUsed->IndexOf(sCurrentDBTable) == -1) { // i.e. - This is the first time we have dealt with this table name for Order/Limit information.
          slOrderLimitDataTablesUsed->Add(sCurrentDBTable);
          if (m_scriptFile->SectionExists(sCurrentDBTable+"_Order"))
            sOrderDataName = sCurrentDBTable+"_Order";
          if (m_scriptFile->SectionExists(sCurrentDBTable+"_Limits"))
            sLimitDataName = sCurrentDBTable+"_Limits";
          bResult = sOrderDataName.Length() || sLimitDataName.Length();
        }
  }
  DMPCATCHTHROW("TSetupReport::DetermineExistenceOfOrderLimitData")
  return bResult;
}
//----------------------------------------------------------------------------
void TSetupReport::DetermineLevelNameAndSubType(const String& sRecordHeaderName, String& sRecordDataName, int& iSubType)
{
  DMPTRY {
    iSubType = -1;
    int iPosSpace = sRecordHeaderName.Pos(" ");
    if (iPosSpace == 0)
      throw DMPException("Could not find space in name - should be there");
    sRecordDataName = sRecordHeaderName.SubString(1, iPosSpace-1);
    String sBaseName = sRecordDataName.UpperCase();
    if (sBaseName.Pos("BANK"))
      iSubType = rtBank;
    else if (sBaseName.Pos("CUSTOMER"))
      iSubType = rtCustomer;
    else if (sBaseName.Pos("LOCKBOX"))
      iSubType = rtLockbox;
    else if (sBaseName.Pos("BATCH"))
      iSubType = rtBatch;
    else if (sBaseName.Pos("TRANSACTION"))
      iSubType = rtTransactions;
    else if (sBaseName.Pos("CHECK"))
      iSubType = rtChecks;
    else if (sBaseName.Pos("INVOICE") || sBaseName.Pos("STUB"))
      iSubType = rtStubs;
    else if (sBaseName.Pos("DOCUMENT"))
      iSubType = rtDocs;
  }
  DMPCATCHTHROW("TSetupReport::DetermineLevelNameAndSubType")
}
//----------------------------------------------------------------------------
void TSetupReport::RecursivelyPopulateMemo(TQRMemo* memoRecords, TTreeNode* currentNode)
{
  DMPTRY {
    String sIndent;
    sIndent.sprintf("%*s", 6*currentNode->Level, " ");
    memoRecords->Lines->Add(sIndent+currentNode->Text);
    for (int i = 0; i < currentNode->Count; ++i)
      RecursivelyPopulateMemo(memoRecords, currentNode->Item[i]);
  }
  DMPCATCHTHROW("TSetupReport::RecursivelyPopulateMemo")
}
//----------------------------------------------------------------------------
void __fastcall TSetupReport::QuickRepPreview(TObject *Sender)
{
  DMPTRY {
    TPreviewForm* previewForm = new TPreviewForm(this);
    previewForm->Reset(QRPrinter);
    previewForm->ShowModal();
    delete previewForm; // Remember to comment out this line if you decide to invoke the window using 'Show' rather than 'ShowModal'.
  }
  DMPCATCHTHROW("TSetupReport::QuickRepPreview")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QuickRepBeforePrint(TCustomQuickRep *Sender, bool &PrintReport)
{
  DMPTRY {
    if (m_scriptFile == 0) // This method can be called multiple times after constructor (for instance - if you print from within 'Preview' window) - so don't just blindly instanciate 'TIniFile'.
      m_scriptFile = new TIniFile(m_recordForm->TempScriptFile);

    lblTitle->Caption = "ExtractWizard Setup Report - " + m_recordForm->ScriptFile;

    // Populate all controls on the 'Title' band (QRBand_Title):

    lblExtractFileLocation->Caption = m_recordForm->ExtractPathEdit->Text;
    lblLogFilePath        ->Caption = m_recordForm->LogPathEdit    ->Text;
    lblImageFilePath      ->Caption = m_recordForm->ImagePathEdit  ->Text;
    lblPostProcessingDLL  ->Caption = m_recordForm->UsePostDLLCheckBOX->Checked ? m_recordForm->PostDLLPathFileNameEdit->Text : String("None");

    lblImageFileFormat                         ->Caption = m_recordForm->ImageFileFormatRadioGroup->Items->Strings[m_recordForm->ImageFileFormatRadioGroup->ItemIndex];
    lblCombineImagesFromMultipleProcessingDates->Caption = m_recordForm->CombineImagesAcrossProcDatesCheckBox->Checked ? "Yes" : "No";
    lblFileNameSingle                          ->Caption = m_recordForm->ImageFileNameDisplaySingle  ->DisplayString;
    lblFileNamePerTran                         ->Caption = m_recordForm->ImageFileNameDisplayPerTran ->DisplayString;
    lblFileNamePerBatch                        ->Caption = m_recordForm->ImageFileNameDisplayPerBatch->DisplayString;

    lblWriteTimestampTo->Caption = m_recordForm->BatchTraceCombo->ItemIndex == -1 ? String("None") : m_recordForm->BatchTraceCombo->Items->Strings[m_recordForm->BatchTraceCombo->ItemIndex];

    //CR20894 MJH 5-07-07: Removed HTML associated fields

    lblFieldDelimiter          ->Caption = m_recordForm->FieldDelimCombo ->ItemIndex == -1 ? String("None") : m_recordForm->FieldDelimCombo ->Items->Strings[m_recordForm->FieldDelimCombo ->ItemIndex];
    lblRecordDelimiter         ->Caption = m_recordForm->RecordDelimCombo->ItemIndex == -1 ? String("None") : m_recordForm->RecordDelimCombo->Items->Strings[m_recordForm->RecordDelimCombo->ItemIndex];
    lblOmitPadding             ->Caption = m_recordForm->NoPadCB      ->Checked ? "Yes" : "No";
    lblEncloseFieldDataInQuotes->Caption = m_recordForm->QuotesCB     ->Checked ? "Yes" : "No";
    lblShowNullAsSpace         ->Caption = m_recordForm->NullAsSpaceCB->Checked ? "Yes" : "No";

    // Setup 'momeRecords' control:
    QRStringsBand_RecordSummary->Items->Clear();
    QRStringsBand_RecordSummary->Items->Add("0");
    memoRecords->Lines->Clear();
    RecursivelyPopulateMemo(memoRecords, m_recordForm->RecordTree->Items->Item[0]);

    // Setup 'Items' propety of 'QRStringsBand_Records' band (so it will know how many times to 'fire'):
    QRStringsBand_Records->Items->Clear();
    TStringList* slOrderLimitsUsedByRecords = new TStringList();
    RecursivelyDetermineWhichRecordsUseOrderLimits(m_recordForm->RecordTree->Items->Item[0], slOrderLimitsUsedByRecords);
    TStringList* slOrderLimitsUsedOverall = new TStringList();
    RecursivelySetupRecords(QRStringsBand_Records->Items, m_recordForm->RecordTree->Items->Item[0], slOrderLimitsUsedOverall, slOrderLimitsUsedByRecords);
    delete slOrderLimitsUsedOverall;
    delete slOrderLimitsUsedByRecords;
  }
  DMPCATCHTHROW("TSetupReport::QuickRepBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_RecordsBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    RecordData* recordData = (RecordData*) (QRStringsBand_Records->Items->Objects[QRStringsBand_Records->Index]);

    // We start off 'turning off' these bands:
    QRStringsBand_Records_Sub1->Items->Clear();
    QRStringsBand_Records_Sub2->Items->Clear();
    QRStringsBand_Records_Sub3->Items->Clear();
    QRStringsBand_Records_Sub4->Items->Clear();
    QRStringsBand_NormalRecord->Items->Clear();
    QRStringsBand_DetailRecord->Items->Clear();
    QRStringsBand_OrderData->Items->Clear();
    QRStringsBand_LimitData->Items->Clear();

    if (recordData->RecordDataName.Length()) {
      m_sRecordName         = recordData->RecordDataName;
      m_iRecordTypeSpecific = m_recordForm->RecordType[m_sRecordName];
      m_iRecordTypeGeneral  = TRecHelper::GetRecordType   (m_iRecordTypeSpecific);
      m_iRecordSubType      = TRecHelper::GetRecordSubType(m_iRecordTypeSpecific);

      // Add band that displays 'Record' and 'Ignore Occurs':
      DMPTRY {
        QRStringsBand_Records_Sub1->Items->Add("0");

        lblRecord->Caption = QRStringsBand_Records->Item;

        lblIgnoreOccursColumns_Label->Enabled = m_iRecordTypeGeneral == rtDetail;
        lblIgnoreOccursColumns      ->Enabled = lblIgnoreOccursColumns_Label->Enabled;

        if (lblIgnoreOccursColumns->Enabled)
          lblIgnoreOccursColumns->Caption = TRecHelper::GetIgnoreOccursFlag(m_iRecordTypeSpecific) ? "Yes" : "No";
      }
      DMPCATCHTHROW("Task: Populate 'Record' band")

      // Add this band only if we are going to display the 'Repeats' information:
      DMPTRY {
        if (m_iRecordSubType == rtDetail && !TRecHelper::GetIgnoreOccursFlag(m_iRecordTypeSpecific)) {
          QRStringsBand_Records_Sub2->Items->Add("0");
          lblMaxRepeats->Caption = String(TRecHelper::GetRepeatLimitValue(m_iRecordTypeSpecific));
        }
      }
      DMPCATCHTHROW("Task: Populate 'Repeats' band")

      // This is just a 'spacer' band - it should be added when there is a record.
      QRStringsBand_Records_Sub3->Items->Add("0");

      if (m_iRecordSubType == rtDetail && !TRecHelper::GetIgnoreOccursFlag(m_iRecordTypeSpecific))
        QRStringsBand_DetailRecord->Items->Add("0");
      else
        QRStringsBand_NormalRecord->Items->Add("0");
    }
    else {
      QRStringsBand_Records_Sub4->Items->Add("0");
      lblLevel->Caption = QRStringsBand_Records->Item;
    }

    m_slOrderData->Clear();
    m_slLimitData->Clear();

    if (recordData->OrderDataName.Length()) {
      m_scriptFile->ReadSectionValues(recordData->OrderDataName, m_slOrderData);
      QRStringsBand_OrderData->Items->Add("0");
    }

    if (recordData->LimitDataName.Length()) {
      m_scriptFile->ReadSectionValues(recordData->LimitDataName, m_slLimitData);
      QRStringsBand_LimitData->Items->Add("0");
    }
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_RecordsBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_NormalRecordBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    if (m_iRecordSubType == rtGeneral)
      throw DMPException("This should never happen - if so, then there is a programming problem");

    m_slRecordData->Clear();
    m_scriptFile->ReadSectionValues(m_sRecordName+"_Grid", m_slRecordData);
    QRStringsBand_NormalRecordFields->Items->Clear();
    for (int i = 0; i < m_slRecordData->Count; ++i)
      QRStringsBand_NormalRecordFields->Items->Add(String(i));
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_NormalRecordBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_NormalRecordFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    TStringList *slFields = new TStringList();
    TextToList(slFields, m_slRecordData->Values["Row"+String(StrToInt(QRStringsBand_NormalRecordFields->Item)+1)]);
    lblFieldName  ->Caption = slFields->Strings[0];
    lblDisplayName->Caption = slFields->Strings[2];
    lblColumnWidth->Caption = slFields->Strings[3];
    lblPadChar    ->Caption = slFields->Strings[4];
    lblJustify    ->Caption = slFields->Strings[5];
    lblQuotes     ->Caption = slFields->Strings[6];
    lblFormat     ->Caption = slFields->Strings[7];
    delete slFields;
  }
  DMPCATCHTHROW("QRStringsBand_NormalRecordFieldsBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_DetailRecordBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    if (m_iRecordSubType == rtGeneral)
      throw DMPException("This should never happen - if so, then there is a programming problem");

    m_slRecordData->Clear();
    m_scriptFile->ReadSectionValues(m_sRecordName+"_Grid", m_slRecordData);
    QRStringsBand_DetailRecordFields->Items->Clear();
    for (int i = 0; i < m_slRecordData->Count; ++i)
      QRStringsBand_DetailRecordFields->Items->Add(String(i));
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_DetailRecordBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_DetailRecordFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    TStringList *slFields = new TStringList();
    TextToList(slFields, m_slRecordData->Values["Row"+String(StrToInt(QRStringsBand_DetailRecordFields->Item)+1)]);
    lbl_D_FieldName  ->Caption = slFields->Strings[0];
    lbl_D_DisplayName->Caption = slFields->Strings[2];
    lbl_D_ColumnWidth->Caption = slFields->Strings[3];
    lbl_D_PadChar    ->Caption = slFields->Strings[4];
    lbl_D_Justify    ->Caption = slFields->Strings[5];
    lbl_D_Quotes     ->Caption = slFields->Strings[6];
    lbl_D_Format     ->Caption = slFields->Strings[7];
    lbl_D_OccrsGrp   ->Caption = slFields->Strings[8];
    lbl_D_OccrsCnt   ->Caption = slFields->Strings[9];
    delete slFields;
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_DetailRecordFieldsBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_OrderDataBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    QRStringsBand_OrderDataFields->Items->Clear();
    for (int i = 0; i < m_slOrderData->Count; ++i)
      QRStringsBand_OrderDataFields->Items->Add(String(i));
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_OrderDataBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_OrderDataFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    TStringList *slFields = new TStringList();
    TextToList(slFields, m_slOrderData->Values["Row"+String(StrToInt(QRStringsBand_OrderDataFields->Item)+1)]);
    lbl_OD_FieldName->Caption = slFields->Strings[0];
    lbl_OD_AscDesc  ->Caption = slFields->Strings[1];
    delete slFields;
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_OrderDataFieldsBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_LimitDataBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    QRStringsBand_LimitDataFields->Items->Clear();
    for (int i = 0; i < m_slLimitData->Count; ++i)
      QRStringsBand_LimitDataFields->Items->Add(String(i));
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_LimitDataBeforePrint")
}
//---------------------------------------------------------------------------
void __fastcall TSetupReport::QRStringsBand_LimitDataFieldsBeforePrint(TQRCustomBand *Sender, bool &PrintBand)
{
  DMPTRY {
    TStringList *slFields = new TStringList();
    TextToList(slFields, m_slLimitData->Values["Row"+String(StrToInt(QRStringsBand_LimitDataFields->Item)+1)]);
    lbl_LD_FieldName   ->Caption = slFields->Strings[0];
    lbl_LD_LogicalOpr  ->Caption = slFields->Strings[1];
    lbl_LD_Value       ->Caption = slFields->Strings[2];
    lbl_LD_Left        ->Caption = slFields->Strings[3];
    lbl_LD_Right       ->Caption = slFields->Strings[4];
    lbl_LD_OprOnNextRow->Caption = slFields->Strings[5];
    delete slFields;
  }
  DMPCATCHTHROW("TSetupReport::QRStringsBand_LimitDataFieldsBeforePrint")
}
//---------------------------------------------------------------------------

