//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "WizardSecurity.h"
#include "StringEncoder.h"
#include "DMPExceptions.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

TWizardSecurity::TWizardSecurity()
{
  FMaxExtracts = 0;
}

//---------------------------------------------------------------------------
void TWizardSecurity::ReadMaxExtracts(String FilePath)
{
  DMPTRY {
    String NumStr, DecodeString = ReadSecurityFile(FilePath);
    StringEncoder Encoder("0123456789", 10);
    if (Encoder.VerifyDecodable(DecodeString)){
      NumStr = Encoder.Decode(DecodeString);
      NumStr = NumStr.SubString(7, NumStr.Length());
    }
    FMaxExtracts = StrToIntDef(NumStr, -1);
  }
  DMPCATCHTHROW("TWizardSecurity::ReadMaxExtracts")
}

//---------------------------------------------------------------------------
String TWizardSecurity::ReadSecurityFile(String FileName)
{
  String sResult;
  DMPTRY {
    char Buffer[10] = {'0'};
    TFileStream *in = new TFileStream(FileName, fmOpenRead);
    in->Read(Buffer, sizeof Buffer);
    delete in;
    sResult = String(Buffer);
  }
  DMPCATCHTHROW("TWizardSecurity::ReadSecurityFile")
  return sResult;
}

//---------------------------------------------------------------------------
bool TWizardSecurity::CanRun(String FilePath)
{
  bool bResult = false;
  DMPTRY {
    int FileCount = 0;
    TSearchRec SearchRec;
    int Attributes = faAnyFile;
    if (FindFirst(FilePath + "*.cxs", Attributes, SearchRec) == 0){
      do{
        if ((SearchRec.Attr & Attributes) == SearchRec.Attr)
          FileCount++;
      } while (FindNext(SearchRec) == 0);
      FindClose(SearchRec);
    }
    bResult = (FileCount <= FMaxExtracts);
  }
  DMPCATCHTHROW("TWizardSecurity::CanRun")
  return bResult;
}

//---------------------------------------------------------------------------
bool TWizardSecurity::CanSave(String FilePath)
{
  bool bResult = false;
  DMPTRY {
    int FileCount = 0;
    TSearchRec SearchRec;
    int Attributes = faAnyFile;
    if (FindFirst(FilePath + "*.cxs", Attributes, SearchRec) == 0){
      do{
        if ((SearchRec.Attr & Attributes) == SearchRec.Attr)
          FileCount++;
      } while (FindNext(SearchRec) == 0);
      FindClose(SearchRec);
    }
    bResult = (FileCount < FMaxExtracts);
  }
  DMPCATCHTHROW("TWizardSecurity::CanSave")
  return bResult;
}

//---------------------------------------------------------------------------
