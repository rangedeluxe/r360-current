//---------------------------------------------------------------------------

#ifndef WizardSecurityH
#define WizardSecurityH
//---------------------------------------------------------------------------
class TWizardSecurity
{
private:
  int FMaxExtracts;
  String ReadSecurityFile(String FileName);
public:
  TWizardSecurity();
  bool CanRun(String FilePath);
  bool CanSave(String FilePath);
  void ReadMaxExtracts(String FilePath);
  __property int MaxExtracts={read = FMaxExtracts};
};
#endif
