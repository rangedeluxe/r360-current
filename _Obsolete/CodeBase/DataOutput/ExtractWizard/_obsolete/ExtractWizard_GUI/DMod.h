//---------------------------------------------------------------------------
#ifndef DModH
#define DModH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Db.hpp>
#include <DBTables.hpp>
#include <ADODB.hpp>
#include "DMPADOQuery.h"
#include "DMPADOCommand.h"
#include "DMPADODataSet.h"

//---------------------------------------------------------------------------
class TDataMod : public TDataModule
{
__published:	// IDE-managed Components
        TADOConnection *DMPConnection;
  TADOCommand *ActionCommand;
  TDMPADODataSet *ActionDataSet;
        void __fastcall DataModuleDestroy(TObject *Sender);
private:	// User declarations

public:		// User declarations
        String ServerName;
        String DatabaseName;
        int CommandTimeout;
        __fastcall TDataMod(TComponent* Owner);
        void __fastcall AppendFieldNames(String Table, TStrings *Flds, TStringList *Attribs);
        void __fastcall Audit(String Event, String Description, int BankID,
                int LockboxID, int Batch, int Item, String Module, String OperatorID, int WKID);
        void __fastcall CloseConnection();
        void __fastcall DoAction(String Action);
        void __fastcall DoOwnerAction(String Action);
        int  __fastcall Exists(String SelectStmt);
        void __fastcall GetFieldListByType(TStrings* str, String Table, TFieldType DataType);
        bool __fastcall OpenConnection(String Server, String Database, String DBUser, String Pswd, int ADOCommandTimeout);
        void __fastcall LoadFieldNames(String Table, TStrings *Flds, TStringList *Attribs);
        int  __fastcall GetColumnSQLDataType( String TblName, String FldName ); // CR #14581  12/20/05  MSV
};    
//---------------------------------------------------------------------------
extern PACKAGE TDataMod *DataMod;
//---------------------------------------------------------------------------
#endif
