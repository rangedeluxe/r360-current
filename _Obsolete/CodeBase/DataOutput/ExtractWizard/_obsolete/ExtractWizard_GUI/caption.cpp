//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "caption.h"
#include "HandleIni.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TCaptionForm *CaptionForm;
//---------------------------------------------------------------------------
__fastcall TCaptionForm::TCaptionForm(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    CustomImage = false;
    TIniHandler Ini;
    Ini.Execute();
    String ImageName = "";
    Ini.GetIniFileString("Logo.ini", "LOGO", "LogoPath", "", ImageName);
    TColor BackColor = (TColor)Ini.GetIniFileInt("Logo.ini", "LOGO", "BackgroundColor", -1);
    if (!ImageName.IsEmpty()){
      try {
        CustomImage = true;
        Image1->Picture->LoadFromFile(ImageName);
        Color = BackColor;
        Label1->Font->Color = BackColor==clBlack?clWhite:clBlack;
      }
      catch(...){
      }
    }
  }
  DMPCATCHTHROW("TCaptionForm::TCaptionForm")
}
//---------------------------------------------------------------------------
void __fastcall TCaptionForm::FormResize(TObject *Sender)
{
  DMPTRY {
    Image1->Left = (Width - Image1->Width)/2;
    Image1->Top = ((Height - Image1->Height)/2) - 20;
    Label1->Left = Image1->Left + (Image1->Width/5);
    if (CustomImage)
      Label1->Top = Image1->Top + Image1->Height + 5;
    else
      Label1->Top = Image1->Top + Image1->Height - (Image1->Height/5);
  }
  DMPCATCHTHROW("TCaptionForm::FormResize")
}
//---------------------------------------------------------------------------

