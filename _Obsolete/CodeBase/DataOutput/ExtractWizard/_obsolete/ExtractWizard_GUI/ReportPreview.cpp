//---------------------------------------------------------------------------
#include <vcl.h>
#include <Qrctrls.hpp>
#include <QuickRpt.hpp>
#include <qrexport.hpp>
#pragma hdrstop

#include "ReportPreview.h"
#include <typeinfo>
#include "DMPExceptions.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TPreviewForm *PreviewForm;
//---------------------------------------------------------------------------
__fastcall TPreviewForm::TPreviewForm(TComponent* Owner)
        : TForm(Owner)
{
  DMPTRY {
    ViewTimer->Enabled = false;
    Reset(0);
    Files = new TStringList();
  }
  DMPCATCHTHROW("TPreviewForm::TPreviewForm")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::Reset(TQRPrinter* aQRPrinter)
{
  DMPTRY {
    IsClosing = false;
    QRPrinter = aQRPrinter;
    QRPreview1->QRPrinter = aQRPrinter;
    if ((QRPrinter) && (!QRPrinter->Title.IsEmpty()))
      Caption = QRPrinter->Title;
    LastProgress = 0;
  }
  DMPCATCHTHROW("TPreviewForm::Reset")
}

//---------------------------------------------------------------------------
void __fastcall TPreviewForm::FormCreate(TObject *Sender)
{
  DMPTRY {
    HorzScrollBar->Tracking = true;
    VertScrollBar->Tracking = true;
  }
  DMPCATCHTHROW("TPreviewForm::FormCreate")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::FormClose(TObject *Sender, TCloseAction &Action)
{
  DMPTRY {
    IsClosing = true;
    QRPreview1->QRPrinter->ClosePreview(this);
    QRPrinter->Cleanup();
    Action = caFree;
  }
  DMPCATCHTHROW("TPreviewForm::FormClose")
}

//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormDestroy(TObject *Sender)
{
  DMPTRY {
    delete Files;
  }
  DMPCATCHTHROW("TPreviewForm::FormDestroy")
}

//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormKeyDown(TObject *Sender, WORD &Key, TShiftState Shift)
{
  DMPTRY {
    switch (Key){
      case VK_NEXT:
        if (Shift.Contains(ssCtrl))
          LastPageClick(this);
        else
          NextPageClick(this);
        break;
      case VK_PRIOR:
        if (Shift.Contains(ssCtrl))
          FirstPageClick(this);
        else
          PreviousPageClick(this);
        break;
      case VK_HOME : FirstPageClick(this); break;
      case VK_END : LastPageClick(this); break;
    }
  }
  DMPCATCHTHROW("TPreviewForm::FormKeyDown")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::FormShow(TObject *Sender)
{
  DMPTRY {
    TForm::Show();
    UpdateInfo();
    ViewTimer->Enabled = true;
  }
  DMPCATCHTHROW("TPreviewForm::FormShow")
}

//---------------------------------------------------------------------------
void __fastcall TPreviewForm::ShowMulti(TStringList* FileList, int Pages)
{
  DMPTRY {
    Multi = true;
    Files->Assign(FileList);
    QRPrinter->Load(Files->Strings[0]);
    CurrentFile = 0;
    QRPreview1->PageNumber = 1;
    QRPreview1->PreviewImage->PageNumber = 1;
    QRPreview1->ZoomToWidth();
    SaveReport->Enabled = false;
    TotalPages = Pages;
    MultiPageNum = 1;
    FormShow(this);
  }
  DMPCATCHTHROW("TPreviewForm::ShowMulti")
}

//---------------------------------------------------------------------------
void __fastcall TPreviewForm::UpdateInfo()
{
  DMPTRY {
    int iPageCurrent, iPageMax;

    if ((Multi)&&(TotalPages > 0)) {
      iPageCurrent = MultiPageNum;
      iPageMax     = TotalPages;
    }
    else {
      iPageCurrent = QRPreview1->PageNumber;
      iPageMax     = QRPreview1->QRPrinter->PageCount;
    }

    StatusBar->Panels->Items[1]->Text = "Page " + IntToStr(iPageCurrent) + " of " + IntToStr(iPageMax);

    FirstPage   ->Enabled = (iPageCurrent > 1       );
    PreviousPage->Enabled = (iPageCurrent > 1       );
    NextPage    ->Enabled = (iPageCurrent < iPageMax);
    LastPage    ->Enabled = (iPageCurrent < iPageMax);
  }
  DMPCATCHTHROW("TPreviewForm::UpdateInfo")
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::SetViewFormat(int iViewFormat)
{
  DMPTRY {
    switch (iViewFormat) {
      case  1: ZoomFit    ->Down = true;
               break;
      case  2: Zoom100    ->Down = true;
               break;
      case  3:
      default: ZoomToWidth->Down = true;
               break;
    }
  }
  DMPCATCHTHROW("TPreviewForm::SetViewFormat")
}
//---------------------------------------------------------------------------

int __fastcall TPreviewForm::GetViewFormat() const
{
  int iResult = 3;
  DMPTRY {
    if (ZoomFit->Down)
      iResult = 1;
    else
      if (Zoom100->Down)
        iResult = 2;
      else
        iResult = 3;
  }
  DMPCATCHTHROW("TPreviewForm::GetViewFormat")
  return iResult;
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::ZoomFitClick(TObject *Sender)
{
  DMPTRY {
    Application->ProcessMessages();
    QRPreview1->ZoomToFit();
  }
  DMPCATCHTHROW("TPreviewForm::ZoomFitClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::Zoom100Click(TObject *Sender)
{
  DMPTRY {
    Application->ProcessMessages();
    QRPreview1->Zoom = 100;
  }
  DMPCATCHTHROW("TPreviewForm::Zoom100Click")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::ZoomToWidthClick(TObject *Sender)
{
  DMPTRY {
    Application->ProcessMessages();
    QRPreview1->ZoomToWidth();
  }
  DMPCATCHTHROW("TPreviewForm::ZoomToWidthClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::FirstPageClick(TObject *Sender)
{
  DMPTRY {
    if (Multi){
      if (CurrentFile != 0){
        CurrentFile = 0;
        QRPrinter->Load(Files->Strings[0]);
      }
      MultiPageNum = 1;
    }
    QRPreview1->PageNumber = 1;
    QRPreview1->PreviewImage->PageNumber = 1;
    UpdateInfo();
  }
  DMPCATCHTHROW("TPreviewForm::FirstPageClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::PreviousPageClick(TObject *Sender)
{
  DMPTRY {
    if (Multi){
      if ((QRPreview1->PageNumber == 1)&&(CurrentFile > 0)){
          QRPrinter->Load(Files->Strings[--CurrentFile]);
          QRPreview1->PageNumber = QRPrinter->PageCount;
          QRPreview1->PreviewImage->PageNumber = QRPrinter->PageCount;
      }
      else
        QRPreview1->PageNumber = QRPreview1->PageNumber - 1;
      if (MultiPageNum > 1)
        MultiPageNum--;
    }
    else
      QRPreview1->PageNumber = QRPreview1->PageNumber - 1;
    UpdateInfo();
  }
  DMPCATCHTHROW("TPreviewForm::PreviousPageClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::NextPageClick(TObject *Sender)
{
  DMPTRY {
    if (Multi) {
      if ((QRPreview1->PageNumber == QRPrinter->PageCount)&&(CurrentFile < Files->Count - 1)) {
        QRPrinter->Load(Files->Strings[++CurrentFile]);
        QRPreview1->PageNumber = 1;
        QRPreview1->PreviewImage->PageNumber = 1;
      }
      else
         QRPreview1->PageNumber = QRPreview1->PageNumber + 1;
      if (MultiPageNum < TotalPages)
        MultiPageNum++;
    }
    else
      QRPreview1->PageNumber = QRPreview1->PageNumber + 1;
    UpdateInfo();
  }
  DMPCATCHTHROW("TPreviewForm::NextPageClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::LastPageClick(TObject *Sender)
{
  DMPTRY {
    if (Multi) {
      if (CurrentFile < Files->Count - 1) {
        CurrentFile = Files->Count - 1;
        QRPrinter->Load(Files->Strings[CurrentFile]);
      }
      MultiPageNum = TotalPages;
    }
    QRPreview1->PageNumber = QRPrinter->PageCount;
    QRPreview1->PreviewImage->PageNumber = QRPrinter->PageCount;
    UpdateInfo();
  }
  DMPCATCHTHROW("TPreviewForm::LastPageClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::ExitButtonClick(TObject *Sender)
{
  DMPTRY {
    IsClosing = true;
    Close();
  }
  DMPCATCHTHROW("TPreviewForm::ExitButtonClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::FormResize(TObject *Sender)
{
  DMPTRY {
    if (!IsClosing)
      QRPreview1->UpdateZoom();
  }
  DMPCATCHTHROW("TPreviewForm::FormResize")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::SaveReportClick(TObject *Sender)
{
  DMPTRY {
    TSaveDialog* SD = new TSaveDialog(this);
    SD->Filter = QRExportFilterLibrary->SaveDialogFilterString;
    SD->DefaultExt = cQRPDefaultExt;
    if (SD->Execute()) {
      if (SD->FilterIndex == 1)
        QRPrinter->Save(SD->FileName);
      else {
        TQRExportFilter* aExportFilter;
        TQRExportFilterLibraryEntry* LibEntry = (TQRExportFilterLibraryEntry*)QRExportFilterLibrary->Filters->Items[SD->FilterIndex - 2];
        if (LibEntry->ExportFilterClass->ClassNameIs("TQRAsciiExportFilter"))
          aExportFilter = new TQRAsciiExportFilter(SD->FileName);
        else if (LibEntry->ExportFilterClass->ClassNameIs("TQRHTMLDocumentFilter"))
          aExportFilter = new TQRHTMLDocumentFilter(SD->FileName);
        else if (LibEntry->ExportFilterClass->ClassNameIs("TQRCommaSeparatedFilter"))
          aExportFilter = new TQRCommaSeparatedFilter(SD->FileName);
        else
          aExportFilter = new TQRExportFilter(SD->FileName);

        QRPrinter->ExportToFilter(aExportFilter);
        delete aExportFilter;
      }
    }
    delete SD;
  }
  DMPCATCHTHROW("TPreviewForm::SaveReportClick")
}

//---------------------------------------------------------------------------

void __fastcall TPreviewForm::PrintSetupClick(TObject *Sender)
{
  DMPTRY {
    QRPreview1->QRPrinter->PrintSetup();
    if (Multi){
      for (int i = 0; i < Files->Count; i++){
        QRPrinter->Load(Files->Strings[i]);
        QRPrinter->Print();
      }
    }
    else
      QRPreview1->QRPrinter->Print();
  }
  DMPCATCHTHROW("TPreviewForm::PrintSetupClick")
}
//---------------------------------------------------------------------------
void __fastcall TPreviewForm::QRPreview1PageAvailable(TObject *Sender, int PageNum)
{
  DMPTRY {
    UpdateInfo();
  }
  DMPCATCHTHROW("TPreviewForm::QRPreview1PageAvailable")
}
//---------------------------------------------------------------------------

//int Progress = (DetailNumber*100) div RecCount per QuickRpt
//returns negative??/undefined?? number with large number of records
//can't find type too small     GW
void __fastcall TPreviewForm::QRPreview1ProgressUpdate(TObject *Sender, int Progress)
{
  DMPTRY {
    if (Progress >= LastProgress + 5) {
      StatusBar->Panels->Items[0]->Text = IntToStr(Progress)+'%';
      LastProgress = Progress;
    }
    if ((Progress == 0) || (Progress == 100))
      StatusBar->Panels->Items[0]->Text = "";
  }
  DMPCATCHTHROW("TPreviewForm::QRPreview1ProgressUpdate")
}

//---------------------------------------------------------------------------

void __fastcall TPreviewForm::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  DMPTRY {
//djk123    CanClose = QRPrinter->AvailablePages == QRPrinter->PageCount;
  }
  DMPCATCHTHROW("TPreviewForm::FormCloseQuery")
}
//---------------------------------------------------------------------------

void __fastcall TPreviewForm::ViewTimerTimer(TObject *Sender)
{
  DMPTRY {
    ViewTimer->Enabled = false;
    switch (GetViewFormat()) {
      case  1: ZoomFit    ->OnClick(this);
               break;
      case  2: Zoom100    ->OnClick(this);
               break;
      case  3:
      default: ZoomToWidth->OnClick(this);
               break;
    }
    Refresh();
  }
  DMPCATCHTHROW("TPreviewForm::ViewTimerTimer")
}
//---------------------------------------------------------------------------

