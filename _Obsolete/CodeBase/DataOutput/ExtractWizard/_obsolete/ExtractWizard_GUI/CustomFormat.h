//---------------------------------------------------------------------------

#ifndef CustomFormatH
#define CustomFormatH
//---------------------------------------------------------------------------

#include <vector>
#include <DB.hpp>


void CorrectSingleQuotes(String &Value);
void DoFormatAlpha(String FormatStr, String Input, String &Value);
void DoFormatFloat(String Str, double FloatValue, String &Value);
void DoFormatDateTime(String Str, TDateTime aDate, String &Value);
String FormatJulian(String FormatStr, TDateTime DateVal);
void __fastcall ListToText(TStringList *Str, String &Text);
void Pad(String &sValue, const bool bMayHaveToMoveNegative, const char cPadChar, const int iWidth, const bool bRightJustify, const bool bUseQuotes); // CR 6454 1/08/2004 DJK - Modified function to handle 'zero-padded' negative numbers properly.
void __fastcall TextToList(TStringList *Str, String Text);

class ConfirmationField
{
public:
  String m_sName;
  String m_sData;
  TFieldType m_fieldType;
  ConfirmationField() {m_fieldType = ftUnknown;}   // '-99' used as tag...
  ConfirmationField(const ConfirmationField& obj) {*this = obj;}
  ConfirmationField(const String& sName, TFieldType fieldType) {m_sName = sName; m_fieldType = fieldType; if (m_fieldType == ftUnknown) m_sData = "<Invalid field>";}
  ConfirmationField& operator = (const ConfirmationField& obj) {m_sName = obj.m_sName; m_sData = obj.m_sData; m_fieldType = obj.m_fieldType; return *this;}
};

class TAggregateList;
class TRecordCountList;

class ConfirmationFields
{
private:
  std::vector<ConfirmationField> m_vFields;
public:
  ConfirmationFields() {}
  void   LoadFieldsFromIniFile(const TAggregateList& aggregates, const TRecordCountList& recordCounts);
  int    GetCount() const {return (int) m_vFields.size();}
  String GetFieldListString(int iIdx) const;
  void   SetData(int iIdx, const String& sData);
  String ReturnData() const;
};

#endif
