object TestExtractForm: TTestExtractForm
  Left = 364
  Top = 146
  Width = 696
  Height = 480
  Caption = 'Test Extract'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  PixelsPerInch = 96
  TextHeight = 13
  object TestMemo: TMemo
    Left = 0
    Top = 312
    Width = 688
    Height = 141
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Lucida Console'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssVertical
    TabOrder = 0
    WordWrap = False
  end
  object Panel2: TPanel
    Left = 0
    Top = 284
    Width = 688
    Height = 28
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    object Label3: TLabel
      Left = 8
      Top = 13
      Width = 226
      Height = 13
      Caption = 'The current layout produces this sample extract.'
    end
    object WordWrapCB: TCheckBox
      Left = 584
      Top = 8
      Width = 97
      Height = 17
      Caption = 'Wrap Text'
      TabOrder = 0
      OnClick = WordWrapCBClick
    end
  end
  object SQLPanel: TPanel
    Left = 0
    Top = 0
    Width = 688
    Height = 284
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 2
    object Label1: TLabel
      Left = 16
      Top = 16
      Width = 249
      Height = 13
      Caption = 'The current layout uses the following SQL statement:'
    end
    object Label2: TLabel
      Left = 16
      Top = 155
      Width = 578
      Height = 13
      Caption = 
        'The records below represent a sample resulting record set, but w' +
        'ithout including any variables in the SQL statement above.'
    end
    object SQLMemo: TMemo
      Left = 16
      Top = 32
      Width = 641
      Height = 114
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object TestGrid: TDBGrid
      Left = 16
      Top = 176
      Width = 641
      Height = 105
      DataSource = TestDS
      ReadOnly = True
      TabOrder = 1
      TitleFont.Charset = DEFAULT_CHARSET
      TitleFont.Color = clWindowText
      TitleFont.Height = -11
      TitleFont.Name = 'MS Sans Serif'
      TitleFont.Style = []
    end
  end
  object TestDataSet: TDMPADODataSet
    Parameters = <>
    Left = 488
  end
  object TestDS: TDataSource
    DataSet = TestDataSet
    Left = 528
  end
end
