object MainFormEW: TMainFormEW
  Left = 384
  Top = 113
  Width = 696
  Height = 480
  Caption = 'Extract Wizard'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  FormStyle = fsMDIForm
  Menu = MainMenu1
  OldCreateOrder = False
  WindowState = wsMaximized
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object MainMenu1: TMainMenu
    Left = 4
    Top = 400
    object File1: TMenuItem
      Tag = 1
      Caption = '&File'
      object New1: TMenuItem
        Caption = '&New Setup'
        OnClick = New1Click
      end
      object Open1: TMenuItem
        Caption = '&Open Setup...'
        OnClick = Open1Click
      end
      object Save1: TMenuItem
        Caption = '&Save'
        Enabled = False
        OnClick = Save1Click
      end
      object SaveAs1: TMenuItem
        Caption = 'Save &As...'
        Enabled = False
        OnClick = SaveAs1Click
      end
      object N2: TMenuItem
        Caption = '-'
      end
      object PrintSetupReport: TMenuItem
        Caption = 'Set&up Report'
        Enabled = False
        object SetupReportPreview: TMenuItem
          Caption = 'P&review...'
          OnClick = SetupReportPreviewClick
        end
        object SetupReportPrint: TMenuItem
          Caption = '&Print...'
          OnClick = SetupReportPrintClick
        end
      end
      object PrintCxsFile: TMenuItem
        Caption = 'Print &Contents of CXS File...'
        Enabled = False
        OnClick = PrintCxsFileClick
      end
      object PrintSetup1: TMenuItem
        Caption = 'P&rinter Setup...'
        OnClick = PrintSetup1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'E&xit'
        OnClick = Exit1Click
      end
    end
    object View1: TMenuItem
      Caption = '&View'
      object Setup1: TMenuItem
        Caption = '&Setup'
        Enabled = False
        OnClick = Setup1Click
      end
      object Results1: TMenuItem
        Caption = '&Results'
        OnClick = Results1Click
      end
      object LogFile1: TMenuItem
        Caption = '&Log File'
        OnClick = LogFile1Click
      end
    end
    object Run1: TMenuItem
      Caption = '&Run'
      OnClick = Run1Click
    end
    object Help1: TMenuItem
      Caption = '&Help'
      object About1: TMenuItem
        Caption = '&About...'
        OnClick = About1Click
      end
      object More1: TMenuItem
        Caption = '&More'
        ShortCut = 16461
        Visible = False
        OnClick = More1Click
      end
      object License1: TMenuItem
        Caption = '&License'
        OnClick = License1Click
      end
    end
  end
  object OpenDialog: TOpenDialog
    DefaultExt = '*.dex'
    Filter = 'CheckBOX Setup Files (*.dex)|*.dex|Any file|*.*'
    Options = [ofHideReadOnly, ofPathMustExist, ofFileMustExist, ofEnableSizing]
    Left = 40
    Top = 400
  end
  object SaveDialog: TSaveDialog
    DefaultExt = '*.cxs'
    Filter = 'CheckBOX Extract Setup Files (*.cxs)|*.cxs'
    Options = [ofOverwritePrompt, ofHideReadOnly, ofNoChangeDir, ofPathMustExist, ofEnableSizing]
    OnCanClose = SaveDialogCanClose
    Left = 80
    Top = 400
  end
  object PrintDialog: TPrintDialog
    Left = 120
    Top = 400
  end
  object PrinterSetupDialog: TPrinterSetupDialog
    Left = 152
    Top = 400
  end
end
