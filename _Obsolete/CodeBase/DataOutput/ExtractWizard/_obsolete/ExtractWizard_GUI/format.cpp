//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <sysutils.hpp>
#include "format.h"
#include "CustomFormat.h"
#include "DMPExceptions.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TFormatDlg *FormatDlg;
//---------------------------------------------------------------------------
__fastcall TFormatDlg::TFormatDlg(TComponent* Owner)
        : TForm(Owner)
{
}

//---------------------------------------------------------------------------
TModalResult __fastcall TFormatDlg::Execute(int Type, String FormatStr)
{
  TModalResult mrResult;
  DMPTRY {
    if (Type < 0 || Type >= PageControl->PageCount)
      throw DMPException("Invalid data type");
    PageControl->ActivePageIndex = Type;
    ClearAll();
    switch (Type){
      case  0: Caption = "Format Date/Time"; 
               DateFormatEdit ->Text = FormatStr; 
               break;
      case  1: Caption = "Format Currency"; 
               FloatFormatEdit->Text = FormatStr; 
               FloatGetOverpunch();
               break;
      case  2: Caption = "Format AlphaNumeric"; 
               FormatEdit->Text = FormatStr; 
               break;
      default: Caption = "Format";
    }
    mrResult = ShowModal();
  }
  DMPCATCHTHROW("TFormatDlg::Execute")
  return mrResult;
}

//---------------------------------------------------------------------------
String __fastcall TFormatDlg::GetFormatString()
{
  return Format;
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::FormCloseQuery(TObject *Sender, bool &CanClose)
{
  DMPTRY {
    switch (PageControl->ActivePageIndex){
      case 0: Format = DateFormatEdit->Text; break;
      case 1: Format = FloatFormatEdit->Text; break;
      case 2: Format = FormatEdit->Text; break;
      default: Format = "";
    }
  }
  DMPCATCHTHROW("TFormatDlg::FormCloseQuery")
}

//---------------------------------------------------------------------------
void __fastcall TFormatDlg::ClearAll()
{
  DMPTRY {
    FormatEdit->Clear();
    ValueEdit->Clear();
    ResultEdit->Clear();
    DateResultEdit->Clear();
    DateFormatEdit->Clear();
    FloatFormatEdit->Clear();
    FloatValueEdit->Clear();
    FloatResultEdit->Clear();
    DateFormatLB->ItemIndex = -1;
    DateExampleLB->ItemIndex = -1;
    FloatFormatLB->ItemIndex = -1;
    FloatExampleLB->ItemIndex = -1;
  }
  DMPCATCHTHROW("TFormatDlg::ClearAll")
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::DateTestBtnClick(TObject *Sender)
{
  String Result;
  try {
    DoFormatDateTime(DateFormatEdit->Text, Now(), Result);
    DateResultEdit->Text = Result;
  }
  catch(...){
    MessageDlg("Invalid date format!", mtError, TMsgDlgButtons()<<mbOK, 0);
    DateResultEdit->Clear();
  }
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::DateFormatLBClick(TObject *Sender)
{
  DMPTRY {
    DateExampleLB->ItemIndex = DateFormatLB->ItemIndex;
    DateFormatEdit->Text = DateFormatLB->Items->Strings[DateFormatLB->ItemIndex];
  }
  DMPCATCHTHROW("TFormatDlg::DateFormatLBClick")
}

//---------------------------------------------------------------------------

void __fastcall TFormatDlg::DateExampleLBClick(TObject *Sender)
{
  DMPTRY {
    DateFormatLB->ItemIndex = DateExampleLB->ItemIndex;
    DateFormatEdit->Text = DateFormatLB->Items->Strings[DateFormatLB->ItemIndex];
  }
  DMPCATCHTHROW("TFormatDlg::DateExampleLBClick")
}

//---------------------------------------------------------------------------
void __fastcall TFormatDlg::TestFloatBtnClick(TObject *Sender)
{
  DMPTRY {
    double FloatVal;
    bool CanContinue = true;
    String Result, Format = FloatFormatEdit->Text;
    try {
      FloatVal = StrToFloat(FloatValueEdit->Text);
    }
    catch(...){
      CanContinue = false;
      MessageDlg(QuotedStr(FloatValueEdit->Text) + " is not a valid float value",
          mtError, TMsgDlgButtons()<<mbOK, 0);
    }
    if (CanContinue){
      try {
        DoFormatFloat(Format, FloatVal, Result);
        FloatResultEdit->Text = Result;
      }
      catch(...){
         MessageDlg("Invalid format!", mtError, TMsgDlgButtons()<<mbOK, 0);
         FloatResultEdit->Clear();
      }
    }
    else
      FloatResultEdit->Clear();
  }
  DMPCATCHTHROW("TFormatDlg::TestFloatBtnClick")
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::FloatFormatLBClick(TObject *Sender)
{
  DMPTRY {
    FloatExampleLB->ItemIndex = FloatFormatLB->ItemIndex;
    FloatFormatEdit->Text = FloatFormatLB->Items->Strings[FloatFormatLB->ItemIndex];
    FloatSetOverpunch();
  }
  DMPCATCHTHROW("TFormatDlg::FloatFormatLBClick")
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::FloatExampleLBClick(TObject *Sender)
{
  DMPTRY {
    FloatFormatLB->ItemIndex = FloatExampleLB->ItemIndex;
    FloatFormatEdit->Text = FloatFormatLB->Items->Strings[FloatFormatLB->ItemIndex];
    FloatSetOverpunch();
  }
  DMPCATCHTHROW("TFormatDlg::FloatExampleLBClick")
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::TestBtnClick(TObject *Sender)
{
  DMPTRY {
    String Result;
    String Format = FormatEdit->Text;
    if (Format.Pos("%")){
      if (Format[Format.Pos("%")+1] != 's'){
        ShowMessage("Only \"%s\" is allowed");
        FormatEdit->SetFocus();
        return;
      }
    }
    DoFormatAlpha(FormatEdit->Text, ValueEdit->Text, Result);
    ResultEdit->Text = Result;
  }
  DMPCATCHTHROW("TFormatDlg::TestBtnClick")
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::FloatSetOverpunch( void )
{
    AnsiString sFormat = FloatFormatEdit->Text;
  
    while (sFormat.Pos("{"))
        sFormat.Delete(sFormat.Pos("{"),1);
    while (sFormat.Pos("}"))
        sFormat.Delete(sFormat.Pos("}"),1);
    if (OverpunchPositiveCB->Checked)
      sFormat += "{";
    if (OverpunchNegativeCB->Checked)
      sFormat += "}";
    FloatFormatEdit->Text = sFormat;
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::FloatGetOverpunch( void )
{
    AnsiString sFormat = FloatFormatEdit->Text;
    OverpunchPositiveCB->Checked = (sFormat.Pos("{") != 0);
    OverpunchNegativeCB->Checked = (sFormat.Pos("}") != 0);
    FloatSetOverpunch();
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::OverpunchPositiveCBClick(TObject *Sender)
{
    FloatSetOverpunch();
}
//---------------------------------------------------------------------------
void __fastcall TFormatDlg::OverpunchNegativeCBClick(TObject *Sender)
{
    FloatSetOverpunch();
}
//---------------------------------------------------------------------------

