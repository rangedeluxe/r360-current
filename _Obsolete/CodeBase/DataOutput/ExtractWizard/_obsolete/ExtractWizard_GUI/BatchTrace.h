//---------------------------------------------------------------------------

#ifndef BatchTraceH
#define BatchTraceH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ADODB.hpp>
#include <Db.hpp>
#include <DBCtrls.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include "DMPADOQuery.h"
#include "DMPADODataSet.h"
//---------------------------------------------------------------------------
class TBatchTraceDlg : public TForm
{
__published:	// IDE-managed Components
        TDMPADODataSet *TimeQuery;
        TDataSource *TimeDS;
        TButton *OKBtn;
        TDBGrid *InfoGrid;
        TLabel *FieldLbl;
        TLabel *Label1;
        TDataSource *BatchDS;
        TDMPADODataSet *BatchQuery;
        TIntegerField *BatchQueryBankID;
        TIntegerField *BatchQueryLockboxID;
        TIntegerField *BatchQueryBatchID;
        TDateTimeField *BatchQueryProcessingDate;
        TIntegerField *BatchQueryCheckCount;
        TBCDField *BatchQueryCheckAmount;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TBatchTraceDlg(TComponent* Owner);
        void __fastcall MakeTimeStampList(String FieldName, TStrings *Items);
        TModalResult __fastcall Execute(String FieldName, String tsTime);
};
//---------------------------------------------------------------------------
extern PACKAGE TBatchTraceDlg *BatchTraceDlg;
//---------------------------------------------------------------------------
#endif
