//---------------------------------------------------------------------------

#ifndef TimeStampTraceH
#define TimeStampTraceH
//---------------------------------------------------------------------------
#include <adodb.hpp>
#include "DMPADOCommand.h"

enum {rsTest, rsFirstRun, rsRerun};


class TTimeStampTrace
{
  private:
    String FBatchList;
    String FRerunList;    //Individual elements must be contained in quotes
    String FFieldName;
    int FRunStatus;
    TDMPADOCommand *FCommand;
  protected:
  public:
    TTimeStampTrace();
    TTimeStampTrace(String Field, String Runs, TDMPADOCommand *ActionCommand);
    void AddBatch(int GlobalBatchID);
    String GetWhereClause();
    void SetCommand(TDMPADOCommand* ActionCommand);
    void SetRerunList(String List);
    bool Update();
    __property String FieldName = {read=FFieldName, write=FFieldName};
    __property String RerunList = {read=FRerunList, write=SetRerunList};
    __property int RunStatus = {read=FRunStatus};
};

#endif
