//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include "Defines.h"
#include "Script.h"
#include "CustomFormat.h"
#include "DMPData.h"

//---------------------------------------------------------------------------

#pragma package(smart_init)

//---------------------------------------------------------------------------
// CR 8752 7/16/04 MLT - added bIgnoreOccurs parameter
int TRecHelper::GenerateNumber(int iRecordType, int iRecordSubType, int iHeaderTrailerItemIndex, int iRepeatLimit, bool bIgnoreOccurs)
{
  int iResult = iRecordType;
  DMPTRY {
    if (iResult == rtDetail)
      iResult = 1000 + iRecordSubType;
    if (iHeaderTrailerItemIndex == 1)
      iResult += 100;
    iResult += 10000*iRepeatLimit;
    // CR 8752 7/16/04 MLT - if record value is 6 digits with first digit on left a 1, then IgnoreOccurs flag is true
    if (bIgnoreOccurs)
      iResult += 100000;
  }
  DMPCATCHTHROW("TRecHelper::GenerateNumber")
  return iResult;
}

int TRecHelper::GetRecordType(int iValue)
{
  int iResult = rtDetail;
  DMPTRY {
    iValue = iValue % 10000; // Strip off 'occurs' value
    if (iValue < 1000)
      iResult = iValue % 100;
  }
  DMPCATCHTHROW("TRecHelper::GetRecordType")
  return iResult;
}

int TRecHelper::GetRecordSubType(int iValue) // This is like 'GetRecordType', except that if the type is 'rtDetail', then the returned type is the 'subtype'
{
  int iResult = 0;
  DMPTRY {
    iResult = iValue % 100;
  }
  DMPCATCHTHROW("TRecHelper::GetRecordSubType")
  return iResult;
}

int TRecHelper::GetHeaderTrailer(int iValue)
{
  int iResult;
  DMPTRY {
    iResult = (iValue % 1000) / 100;
    if (iResult > 1)
      iResult = 1;
    else
      if (iResult < 0)
        iResult = 0;
  }
  DMPCATCHTHROW("TRecHelper::GetHeaderTrailer")
  return iResult;
}

int TRecHelper::GetRepeatLimitValue(int iValue)
{
  int iResult;
  DMPTRY {
    // CR 8752 7/16/04 MLT - strip off new IgnoreOccurs flag (leftmost digit when value is 6 digits long)
    iResult = (iValue % 100000)/10000;
  }
  DMPCATCHTHROW("TRecHelper::GetRepeatLimitValue")
  return iResult;
}

// CR 8752 7/16/04 MLT - added new static method
bool TRecHelper::GetIgnoreOccursFlag(int iValue)
{
  int iResult;
  DMPTRY {
    iResult = iValue/100000;
  }
  DMPCATCHTHROW("TRecHelper::GetIgnoreOccursFlag")
  return iResult;
}

int TRecHelper::SetRecordDetail(int iValue, bool bDetail)
{
  int iResult = 0;
  DMPTRY {
    // CR 8752 7/16/04 MLT - add logic to handle IgnoreOccurs flag
    bool bIgnoreOccurs = iValue/100000 > 0;
    iValue = iValue % 100000;
    int iUpper = iValue / 10000;
    int iLower = iValue % 1000;
    iResult = 10000*iUpper+iLower+(bDetail ? 1000 : 0)+(bIgnoreOccurs ? 100000 : 0);
  }
  DMPCATCHTHROW("TRecHelper::SetRecordDetail")
  return iResult;
}

// CR 8752 7/16/04 MLT - add new method to set the IgnoreOccurs flag
int TRecHelper::SetRecordIgnoreOccurs(int iValue, bool bIgnoreOccurs)
{
  int iResult = 0;
  DMPTRY {
    iValue = iValue % 100000;           // strip off previous IgnoreOccurs flag
    iResult = iValue+(bIgnoreOccurs ? 100000 : 0); // add new setting for flag
  }
  DMPCATCHTHROW("TRecHelper::SetRecordIgnoreOccurs")
  return iResult;
}

int TRecHelper::SetRecordSubType(int iValue, int iSubType)
{
  int iResult;
  DMPTRY {
    iResult = iValue - (iValue % 100) + iSubType;
  }
  DMPCATCHTHROW("TRecHelper::SetRecordSubType")
  return iResult;
}

int TRecHelper::SetHeaderTrailer(int iValue, int iItemIndex)
{
  int iResult = iValue;
  DMPTRY {
    if (iItemIndex > 1)
      iItemIndex = 1;
    else
      if (iItemIndex < 0)
        iItemIndex = 0;
    if (iItemIndex == 0) {
      if (GetHeaderTrailer(iValue))
        iResult -= 100;
    }
    else
      if (GetHeaderTrailer(iValue) == 0)
        iResult += 100;
  }
  DMPCATCHTHROW("TRecHelper::SetHeaderTrailer")
  return iResult;
}

int TRecHelper::SetRepeatLimitValue(int iValue, int iRepeatLimit)
{
  int iResult;
  DMPTRY {
    iResult = 10000*iRepeatLimit + (iValue % 10000);
  }
  DMPCATCHTHROW("TRecHelper::SetRepeatLimitValue")
  return iResult;
}

//---------------------------------------------------------------------------

TRecordFieldItem::TRecordFieldItem(const String& sIniRow)
{
  DMPTRY {
    Clear();

    int iPos = sIniRow.Pos("=");
    m_iIniTag = sIniRow.SubString(4, iPos-4).Trim().ToIntDef(-1);
    String sTemp = sIniRow.SubString(iPos+1, sIniRow.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_sFieldName = sTemp.SubString(1, iPos-1).Trim();
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_ftDataType = (TFieldType) sTemp.SubString(1, iPos-1).Trim().ToIntDef(-1);
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_sDisplayName = sTemp.SubString(1, iPos-1).Trim();
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_iColWidth = sTemp.SubString(1, iPos-1).Trim().ToIntDef(-1);
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    String sPadChar = sTemp.SubString(1, iPos-1).Trim();
    if (sPadChar.AnsiCompareIC("SP") && sPadChar.Length())
      m_cPadChar = sPadChar[1];
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_bRightJustify = (sTemp.SubString(1, iPos-1).Trim().AnsiCompareIC("R") == 0);
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    m_bUseQuotes = (sTemp.SubString(1, iPos-1).Trim().Length() > 0);
    sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

    iPos = sTemp.Pos("|");
    if (iPos) {
      m_bOccursColumns = true;

      m_sFormat = sTemp.SubString(1, iPos-1).Trim();
      sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

      iPos = sTemp.Pos("|");
      m_iOccursGroup = sTemp.SubString(1, iPos-1).Trim().ToIntDef(-1);
      sTemp = sTemp.SubString(iPos+1, sTemp.Length()-iPos);

      m_iOccursCount = sTemp.Trim().ToIntDef(-1);
    }
    else
      m_sFormat = sTemp.Trim();

    iPos = m_sFieldName.Pos(".");
    if (iPos) {
      m_sTable = m_sFieldName.SubString(1, iPos-1);
      m_sField = m_sFieldName.SubString(iPos+1, m_sFieldName.Length()-iPos);
    }
    else
      m_sField = m_sFieldName;
  }
  DMPCATCHTHROW("TRecordFieldItem::TRecordFieldItem")
}

TRecordFieldItem& TRecordFieldItem::operator = (const TRecordFieldItem& obj)
{
  DMPTRY {
    m_iIniTag        = obj.m_iIniTag;
    m_sFieldName     = obj.m_sFieldName;
    m_ftDataType     = obj.m_ftDataType;
    m_sDisplayName   = obj.m_sDisplayName;
    m_iColWidth      = obj.m_iColWidth;
    m_cPadChar       = obj.m_cPadChar;
    m_bRightJustify  = obj.m_bRightJustify;
    m_bUseQuotes     = obj.m_bUseQuotes;
    m_sFormat        = obj.m_sFormat;
    m_iOccursGroup   = obj.m_iOccursGroup;
    m_iOccursCount   = obj.m_iOccursCount;
    m_bOccursColumns = obj.m_bOccursColumns;
    m_sTable         = obj.m_sTable;
    m_sField         = obj.m_sField;
  }
  DMPCATCHTHROW("TRecordFieldItem::operator =")
  return *this;
}

void TRecordFieldItem::Clear()
{
  DMPTRY {
    m_iIniTag        = 0;
    m_sFieldName     = "";
    m_ftDataType     = ftUnknown;
    m_sDisplayName   = "";
    m_iColWidth      = 0;
    m_cPadChar       = ' ';
    m_bRightJustify  = false;
    m_bUseQuotes     = false;
    m_sFormat        = "";
    m_iOccursGroup   = -1;
    m_iOccursCount   = -1;
    m_bOccursColumns = false;
    m_sTable         = "";
    m_sField         = "";
  }
  DMPCATCHTHROW("TRecordFieldItem::Clear")
}

//---------------------------------------------------------------------------

const TRecordFieldItem& TRecordItem::GetField(int iIdx) const
{
  const TRecordFieldItem* fiPtr = 0;
  DMPTRY {
    CheckIdx(iIdx, GetFieldCount());
    fiPtr = m_vFields.begin() + iIdx;
  }
  DMPCATCHTHROW("TRecordItem::GetField")
  return *fiPtr;
}

void TRecordItem::LoadIniData(const String& sIniFile)
{
  DMPTRY {
    TStringList* slTemp = new TStringList();
    TIniFile *Ini = new TIniFile(sIniFile);
    Ini->ReadSectionValues(m_sName + "_GRID", (TStrings*) slTemp);
    delete Ini;
    for (int i = 0; i < slTemp->Count; ++i)
      m_vFields.push_back(TRecordFieldItem(slTemp->Strings[i]));
    delete slTemp;
    std::sort(m_vFields.begin(), m_vFields.end());
  }
  DMPCATCHTHROW("TRecordItem::LoadIniData")
}

void TRecordItem::CheckIdx(const int iIdx, const int iCount) const
{ // Do not use DMPTRY/CATCH here - it will only confuse things.
  if (iIdx < 0 || iIdx >= iCount)
    throw DMPException("Input argument '"+String(iIdx)+"' is outside of the allowable range [0, "+String(iCount-1)+"]");
}

//---------------------------------------------------------------------------

TScript::TScript(const String& SetupFile)
{
  DMPTRY {
    FFileName = SetupFile;
    TStringList* tmp = new TStringList;
    TIniFile *Ini = new TIniFile(SetupFile);
    m_sExtractPath                   = Ini->ReadString ("GENERAL"   , "ExtractPath"                  , ""   );
    // CR 7411 ~ DLAQAB ~ 3-31-2004
    // Added the check for m_sExtractPath and changed the default values for
    // m_sLogFilePath and m_sImagePath to m_sExtractPath
    while( m_sExtractPath.Trim().IsEmpty() )
    {
        if( !InputQuery( "Extract", "Extract File Path is required...", m_sExtractPath ) )
        {
           throw DMPException("Cannot Proceed without Extract File Path");
        }
    }
    m_sLogFilePath                   = Ini->ReadString ("GENERAL"   , "LogFilePath"                  , m_sExtractPath   );
    m_sImagePath                     = Ini->ReadString ("GENERAL"   , "ImagePath"                    , m_sExtractPath   );
    // End CR 7411 ~ DLAQAB ~ 3-31-2004
    m_bUseQuotes                     = Ini->ReadBool   ("GENERAL"   , "UseQuotes"                    , false);
    m_bNoPad                         = Ini->ReadBool   ("GENERAL"   , "NoPad"                        , false);
    m_bNullAsSpace                   = Ini->ReadBool   ("GENERAL"   , "NullAsSpace"                  , false);
    m_sBatchTraceField               = Ini->ReadString ("GENERAL"   , "TimeStamp"                    , ""   );
    m_sFieldDelimiter                = Ini->ReadString ("GENERAL"   , "FieldDelim"                   , ""   );
    m_sRecordDelimiter               = Ini->ReadString ("GENERAL"   , "RecordDelim"                  , ""   );
    m_iImageFileFormat               = Ini->ReadInteger("IMAGEFILES", "ImageFileFormatAsPDF"         , 0    ); // CR 9916 10/13/2004 ALH - chane to an int instead of bool.
    m_bCombineImagesAccrossProcDates = Ini->ReadBool   ("IMAGEFILES", "CombineImagesAccrossProcDates", false);
    m_sImageFileNameSingle           = Ini->ReadString ("IMAGEFILES", "ImageFileNameSingle"          , ""   );
    m_sImageFileNamePerTran          = Ini->ReadString ("IMAGEFILES", "ImageFileNamePerTran"         , ""   );
    m_sImageFileNamePerBatch         = Ini->ReadString ("IMAGEFILES", "ImageFileNamePerBatch"        , ""   );
    m_sImageFileProcDateFormat       = Ini->ReadString ("IMAGEFILES", "ImageFileProcDateFormat"      , "MMDDYY");
    m_bImageFileZeroPad              = Ini->ReadBool   ("IMAGEFILES", "ImageFileZeroPad"             , false);
    m_bImageFileBatchTypeFormatFull  = Ini->ReadBool   ("IMAGEFILES", "ImageFileBatchTypeFormatFull" , true );
    m_bUseHTML                       = Ini->ReadBool   ("HTML"      , "UseHTML"                      , false);
    m_sHTMLHeaderFile                = Ini->ReadString ("HTML"      , "HeaderFile"                   , ""   );
    m_sHTMLFooterFile                = Ini->ReadString ("HTML"      , "FooterFile"                   , ""   );
    m_bReformatHTML                  = Ini->ReadBool   ("HTML"      , "ReformatHTML"                 , false);
    m_iHTMLBorderWidth               = Ini->ReadInteger("HTML"      , "BorderWidth"                  , 0    );
    m_iHTMLCellSpacing               = Ini->ReadInteger("HTML"      , "CellSpacing"                  , 2    );
    m_bUseFieldNames                 = Ini->ReadBool   ("HTML"      , "UseFieldNames"                , false);
    m_bDisplayDataOnly               = Ini->ReadBool   ("HTML"      , "DisplayDataOnly"              , true );
    m_sPostProcDLL                   = Ini->ReadString ("POSTDLL"   , "FileName"                     , ""   ); //CR10943 03/30/2005 ALH

    if(m_iImageFileFormat == 3){
        m_bEmbedImages = true;
    }     
    else{
        m_bEmbedImages = false;
    }

    m_slParameters = new TStringList();
    Ini->ReadSectionValues("PARAMETERS", m_slParameters);
    Ini->ReadSectionValues("RECORDS", (TStrings*) tmp);
    delete Ini;
    for (int i = 0; i < tmp->Count; ++i) {
      TRecordItem record(tmp->Names[i], tmp->Values[tmp->Names[i]].ToIntDef(-1));
      record.LoadIniData(SetupFile);
      m_vRecords.push_back(record);
      m_vFastSearchTool.push_back(TRecordItem(tmp->Names[i], i));
    }
    std::sort(m_vFastSearchTool.begin(), m_vFastSearchTool.end());
    delete tmp;
  }
  DMPCATCHTHROW("TScript::TScript")
}

TScript::~TScript()
{
  DMPTRY {
    delete m_slParameters;
  }
  DMPCATCHTHROW("TScript::~TScript")
}

const TRecordItem& __fastcall TScript::GetRecord(int iIdx) const
{
  const TRecordItem* riPtr = 0;
  DMPTRY {
    CheckIdx(iIdx, GetRecordCount());
    riPtr = m_vRecords.begin() + iIdx;
  }
  DMPCATCHTHROW("TScript::GetRecord")
  return *riPtr;
}

int __fastcall TScript::LocateRecord(const String& sRecordName) const
{
  int iResult = -1;
  DMPTRY {
    int iPos;
    if (BinarySearch(m_vFastSearchTool, TRecordItem(sRecordName, -1), iPos))
      iResult = m_vFastSearchTool[iPos].GetType();
  }
  DMPCATCHTHROW("TScript::LocateRecord")
  return iResult;
}

int __fastcall TScript::GetMaxType()
{
  int iResult = rtGeneral;
  DMPTRY {
    int i;
    for (i = 0; i < (int) m_vRecords.size(); i++){
      int CompType = TRecHelper::GetRecordType(m_vRecords[i].GetType()); // djk 10/2/01
      if (CompType > iResult)
        iResult = CompType;
    }
    i = rtDocs;
    while (i > rtGeneral && i > iResult){
      if (HasEntries(i, "_Limits") || HasEntries(i, "_Order"))
        iResult = i;
      i--;
    }
  }
  DMPCATCHTHROW("TScript::GetMaxType")
  return iResult;
}

// CR 5272 1/07/2004 DJK 
//     Altered to guarantee that every field is matched with the appropriate table name (the string '-----'
//     is used as a 'stand-in' for non-DB fields, which fields get removed before any SQL statements are built.
void __fastcall TScript::GetSelectedFieldNames(int Type, FieldCollection& Fields)
{
  DMPTRY {
    String sFieldName;
    String sTableName = TableHelper::GetTable(TRecHelper::GetRecordSubType(Type));
    Fields.ClearFields();
    for (int i = 0; i < (int) m_vRecords.size(); i++)
      if (TRecHelper::GetRecordType(m_vRecords[i].GetType()) == Type) {
        TRecordItem* recordPtr = &(m_vRecords[i]);
        for (int j = 0; j < recordPtr->GetFieldCount(); ++j) {
          sFieldName = recordPtr->GetField(j).GetFieldName();
          if (Type > rtTransactions) {
            String ID;
            switch (Type) {
              case rtChecks: ID = "GLOBALCHECKID"   ; break;
              case rtStubs : ID = "GLOBALSTUBID"    ; break;
              case rtDocs  : ID = "GLOBALDOCUMENTID"; break;
            }
            if (sFieldName.AnsiCompareIC("GLOBALBATCHID") == 0 || sFieldName.AnsiCompareIC("TRANSACTIONID") == 0)
              sFieldName = sTableName+"."+sFieldName;
            else
              if (sFieldName.AnsiCompareIC(ID) == 0)
                sFieldName = sTableName+"."+ID;
          }
          if (sFieldName.Pos(".") == 0)
            if (sTableName.Length() == 0)
              sFieldName = "-----."+sFieldName;
            else
              sFieldName = sTableName+"."+sFieldName;
          Fields.Add(sFieldName, true);
        }
      }
  }
  DMPCATCHTHROW("TScript::GetSelectedFieldNames")
}

String __fastcall TScript::GetTable(int Type, bool bIgnoreDataEntry)
{
  String sResult;
  DMPTRY {
    switch (Type){
      case rtBank        : sResult = "Bank"        ; break;
      case rtCustomer    : sResult = "Customer"    ; break;
      case rtLockbox     : sResult = "Lockbox"     ; break;
      case rtBatch       : sResult = "Batch"       ; break;
      case rtTransactions: sResult = "Transactions"; break;
      case rtChecks      : sResult = bIgnoreDataEntry ? "Checks"    : "Checks, ChecksDataEntry"      ; break;
      case rtStubs       : sResult = bIgnoreDataEntry ? "Stubs"     : "Stubs, StubsDataEntry"        ; break;
      case rtDocs        : sResult = bIgnoreDataEntry ? "Documents" : "Documents, DocumentsDataEntry"; break;
      default            : sResult = "";
    }
  }
  DMPCATCHTHROW("TScript::GetTable")
  return sResult;
}

bool __fastcall TScript::HasEntries(int Type, const String& Section) //xxx
{
  bool bResult = false;
  DMPTRY {
    String Table = GetTable(Type);
    if (!Table.IsEmpty()) {
      TStringList *LimitList = new TStringList;
      TIniFile *Ini = new TIniFile(FFileName);
      Ini->ReadSectionValues(Table + Section, LimitList);
      bResult = LimitList->Count > 0;
      delete LimitList;
      delete Ini;
    }
  }
  DMPCATCHTHROW("TScript::HasEntries")
  return bResult;
}

// CR#14581 12/20/05 MSV  - For column types -1, 1, 9, 10, 11, and 12 (basically 
//                          text and datetime types) we need to include single 
//                          quotes around the value.
String __fastcall TScript::GetLimits(int Type)
{
  String sResult,sTable,sFldDelim;
  DMPTRY {
    TStringList *LimitList = new TStringList;
    TStringList *tmpRow = new TStringList;
    sTable = GetTable(Type);
    TIniFile *Ini = new TIniFile(FFileName);
    Ini->ReadSectionValues(sTable + "_Limits", LimitList);
    for (int i = 0; i < LimitList->Count; i++){
      TextToList(tmpRow, LimitList->Values[LimitList->Names[i]]);
      tmpRow->Insert(0, "");
      while (tmpRow->Count <= goRecOpr)
        tmpRow->Add("");
      int columntype = DMPDataMod->GetColumnSQLDataType(sTable,tmpRow->Strings[goFieldName]);
      if (columntype == 0) {  // if not in Checks, Stubs, Documents, look in associated DE table
        switch (Type) {
          case rtChecks: columntype = DMPDataMod->GetColumnSQLDataType("ChecksDataEntry",tmpRow->Strings[goFieldName]);     break;
          case rtStubs:  columntype = DMPDataMod->GetColumnSQLDataType("StubsDataEntry",tmpRow->Strings[goFieldName]);      break;
          case rtDocs:   columntype = DMPDataMod->GetColumnSQLDataType("DocumentsDataEntry",tmpRow->Strings[goFieldName]);  break;
        }
      }
      sFldDelim = "";
      if ( (columntype == -1) || (columntype == 1) || ((columntype >= 9) && (columntype <= 12)) )
        sFldDelim = "'";
      sResult += tmpRow->Strings[goLeftParen] + tmpRow->Strings[goFieldName] + " " + 
                 tmpRow->Strings[goOpr] + " " + sFldDelim + tmpRow->Strings[goValue] + sFldDelim + 
                 tmpRow->Strings[goRightParen] + " " + tmpRow->Strings[goRecOpr] + " ";
    }
    if (!sResult.IsEmpty()) {
      sResult.Insert(" where (", 1);
      sResult.Insert(")", sResult.Length()-1); // djk - 2/12/02
    }
    delete Ini;
    delete tmpRow;
    delete LimitList;
  }
  DMPCATCHTHROW("TScript::GetLimits")
  return sResult;
}

// CR 12120 3/30/2005 DJK - Added two last parameters, return void instead of String. Function modifed accordingly.
void __fastcall TScript::GetOrder(int Type, FieldCollection& fieldCollection, TStringList* slForAscDesc)
{
  DMPTRY {
    String Table = GetTable(Type);
    TStringList *OrderList = new TStringList;
    TStringList *tmpRow = new TStringList;
    TIniFile *Ini = new TIniFile(FFileName);
    Ini->ReadSectionValues(Table + "_Order", OrderList);
    for (int i = 0; i < OrderList->Count; i++){
      TextToList(tmpRow, OrderList->Values[OrderList->Names[i]]);
      fieldCollection.Add(Table+"."+tmpRow->Strings[0]);
      slForAscDesc->Add(tmpRow->Strings[1]);
    }
    delete tmpRow;
    delete OrderList;
    delete Ini;
  }
  DMPCATCHTHROW("TScript::GetOrder")
}

void __fastcall TScript::CheckIdx(const int iIdx, const int iCount) const
{ // Do not use DMPTRY/CATCH here - it will only confuse things.
  if (iIdx < 0 || iIdx >= iCount)
    throw DMPException("Input argument '"+String(iIdx)+"' is outside of the allowable range [0, "+String(iCount-1)+"]");
}


