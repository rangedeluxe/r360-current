//---------------------------------------------------------------------------

#ifndef TestExtractH
#define TestExtractH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <ExtCtrls.hpp>
#include <DBGrids.hpp>
#include <Grids.hpp>
#include <ADODB.hpp>
#include <Db.hpp>
#include "DMPADOQuery.h"
#include "DMPADODataSet.h"
//---------------------------------------------------------------------------
class TTestExtractForm : public TForm
{
__published:	// IDE-managed Components
        TMemo *TestMemo;
        TPanel *Panel2;
        TCheckBox *WordWrapCB;
        TPanel *SQLPanel;
        TLabel *Label1;
        TMemo *SQLMemo;
        TLabel *Label2;
        TDBGrid *TestGrid;
        TLabel *Label3;
  TDMPADODataSet *TestDataSet;
        TDataSource *TestDS;
        void __fastcall WordWrapCBClick(TObject *Sender);
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:	// User declarations
public:		// User declarations
        __fastcall TTestExtractForm(TComponent* Owner);
   //     void __fastcall Execute(String FileName);
};
//---------------------------------------------------------------------------
extern PACKAGE TTestExtractForm *TestExtractForm;
//---------------------------------------------------------------------------
#endif
