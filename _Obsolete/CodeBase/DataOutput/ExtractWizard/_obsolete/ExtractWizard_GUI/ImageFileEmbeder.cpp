// New class - ImageFileEmbeder embeds image data into the extract file if the
// operator (extract writer) so desires.
// CR 9916 10/18/2004 ALH   Class by Aaron L. Hall
#include <io.h>
#include <fcntl.h>
#include <vcl.h>
#pragma hdrstop
#include "ImageFileEmbeder.h"
#include "HelperClasses.h"


ImageFileEmbeder::ImageFileEmbeder()
{
        DMPTRY
        {
           m_sDataFileName      = "";
           m_zTiffImage         = NULL;
           m_lMaxRecordLength   = 0;
           m_lTiffSize          = 0;
           SourceFile           = NULL;
           m_iImageIndex        = 0;
           m_iImageSizeIndex    = 0;
           m_bImbededImageData  = false;
           theLog = new AppLogging("DMPExtractDLL-ImageEmbeddingLOG.log", true, true, 2);
           theLog->AddLine("\n******************************** DMPExtract.dll is now embeding images ************************************");
        }
        DMPCATCHTHROW("ImageFileEmbeder::ImageFileEmbeder")
}

ImageFileEmbeder::ImageFileEmbeder(String sFileName, int lMaxRecordLength)
{
        DMPTRY
        {
             SetDataFileName(sFileName);
             m_zTiffImage       = NULL;
             m_lMaxRecordLength = lMaxRecordLength;
             m_lTiffSize        = 0;
             SourceFile         = NULL;
             m_iImageIndex      = 0;
             m_bImbededImageData= false;
        }
        DMPCATCHTHROW("ImageFileEmbeder::ImageFileEmbeder(sFileName)")
}

ImageFileEmbeder::~ImageFileEmbeder()
{
        DMPTRY
        {
           m_sDataFileName = "";
            if (m_zTiffImage != NULL)
            {
                    delete []m_zTiffImage;
                    m_zTiffImage = NULL;
            }
        }
        DMPCATCHTHROW("ImageFileEmbeder::~ImageFileEmbeder")
}

void ImageFileEmbeder::SetDataFileName(String sFileName)
{
        DMPTRY
        {
            m_sDataFileName = sFileName;
            theLog->AddLine("Data file Name is %s",m_sDataFileName);
        }
        DMPCATCHTHROW("ImageFileEmbeder::SetDataFileName")
}

// CR#13656 10/18/05 MSV - This method was basically completely rewritten to 
//                          be more efficient in how it searched for the embedded
//                          image & size tags.
void  ImageFileEmbeder::EmbedImages(bool bDeleteImgFiles)
{
    DMPTRY {
        String  sTempFileName;
        FILE    *TempFile;
        char    tc,zBuff[256],*pEmbedTag,*pImgSizeTag;
        int     indx;
        theLog->AddLine("Entering ImageFileEmbeder::EmbedImages function.");
        // pointers to the embedded tag strings, to make some statements simpler
        pEmbedTag = EMBEDTAG.c_str();
        pImgSizeTag = IMGSIZETAG.c_str();

        // open the source and temporary files for the tag replacement process
        sTempFileName.sprintf("%s.TMP",m_sDataFileName);
        if ( (SourceFile = fopen(m_sDataFileName.c_str(), "rb")) == NULL )
            throw(DMPException("Error opening file for embedded image replacement.",0));
        theLog->AddLine("Opening SourceFile: %s",m_sDataFileName);
        if ( (TempFile   = fopen(sTempFileName.c_str()  , "wb")) == NULL )
            throw(DMPException("Error opening temporary file for embedded image replacement.",0));
        theLog->AddLine("Opening SourceFile: %s",sTempFileName);
        String sFileName = "";
        theLog->AddLine("Outputting image file name in the image file list:");
        for (int i = 0; i < m_vsImageFileNameList.GetListSize();i++)
        {
            sFileName = m_vsImageFileNameList.GetImageInfo(i).ImageFileName.c_str();
            theLog->AddLine("Image number %d is '%s'",i,sFileName.c_str());
        }
        DMPTRY {
            // scan through the source file....
            while (!feof(SourceFile)) {
                // read a character
                if (fread(&tc,sizeof(tc),1,SourceFile) != sizeof(tc))
                    continue;
                // if not the first character of one of the tags, move on
                if ((tc != pEmbedTag[0]) && (tc != pImgSizeTag[0]))
                    fwrite(&tc,sizeof(tc),1,TempFile);
                // otherwise see if we've found a tag
                else {
                    memset(zBuff,0,sizeof(zBuff));
                    zBuff[0] = tc;
                    indx = 1;
                    theLog->AddLine("Entering  Image Embeding While Loop");
                    while ( ( (strnicmp(zBuff,pEmbedTag,indx) == 0) || (strnicmp(zBuff,pImgSizeTag,indx) == 0) ) &&
                            (strlen(zBuff) < IMGTAGLENGTH) )
                        fread(&zBuff[indx++],sizeof(zBuff[0]),1,SourceFile);
                    // if we've found the image size tag, handle it...
                    if (stricmp(zBuff,pImgSizeTag) == 0) {
                        theLog->AddLine("An Embeded Image has been found.");
                        // get the size of the output field (next 3 chars tell us)
                        memset(zBuff,0,sizeof(zBuff));
                        fread(zBuff,sizeof(zBuff[0]),3,SourceFile);
                        int iOutputSize = atoi(zBuff);
                        // get the rest of the output field from the input file (and discard it)
                        fread(zBuff,sizeof(zBuff[0]),iOutputSize,SourceFile);
                        // get the name of the image file
                        String sFileName = m_vsImageFileNameList.GetImageInfo(m_iImageSizeIndex).ImageFileName;
                        theLog->AddLine("Image size tag found, Retrieving image size for image: '%s'", sFileName);
                        if (sFileName.UpperCase().Pos(EMBEDTAG) > 0)
                            sFileName = sFileName.SubString(sFileName.UpperCase().Pos(EMBEDTAG)+TAGSIZE,sFileName.Length());
                        // get the size of the image, store in 'zBuff'
                        memset(zBuff,0,sizeof(zBuff));
                        int fhandle = open(sFileName.c_str(),O_RDONLY,O_BINARY);
                        if (fhandle < 0)
                            sprintf(zBuff,"%0*ld",iOutputSize,0);
                        else {
                            sprintf(zBuff,"%0*ld",iOutputSize,filelength(fhandle));
                            theLog->AddLine("The Image Exists.");
                            close(fhandle);
                        }
                        // write the file size to the output file
                        fwrite(zBuff,sizeof(zBuff[0]),iOutputSize,TempFile);
                        m_iImageSizeIndex ++;
                    }
                    // if we've found the embedded image tag, handle it...
                    else if (stricmp(zBuff,pEmbedTag) == 0) {
                        m_bImbededImageData = true;
                        // get the size of the field containing the file name (next 5 chars tell us)
                        memset(zBuff,0,sizeof(zBuff));
                        fread(zBuff,sizeof(zBuff[0]),5,SourceFile);
                        int iFileNameSize = atoi(zBuff);
                        // get the image file name itself, and trim leading/trailing blanks
                        memset(zBuff,0,sizeof(zBuff));
                        fread(zBuff,sizeof(zBuff[0]),iFileNameSize,SourceFile);
                        strcpy(zBuff,AnsiString(zBuff).Trim().c_str());
                        theLog->AddLine("Embeded Image tag found, Image is '%s'.",zBuff);
                        if (FileExists(zBuff)) { //CR16190 3/22/2006  ALH
                            // copy the image data into the output file
                            theLog->AddLine("Confirmation, Image '%s', exists.",zBuff);
                            FILE *ImageFile = fopen(zBuff,"rb");
                            int iImageFileSize = filelength(_fileno(ImageFile));
                            char *sFileBuffer = new char[iImageFileSize];
                            fread(sFileBuffer,sizeof(sFileBuffer[0]),iImageFileSize,ImageFile);
                            fwrite(sFileBuffer,sizeof(sFileBuffer[0]),iImageFileSize,TempFile);
                            theLog->AddLine("Image has been written to the data file, image size is: %d",iImageFileSize);
                            fclose(ImageFile);
                            delete sFileBuffer;
                        }
                        else
                        {
                             theLog->AddLine("File Exists Function for file '%s', returned false, the file does not exist.",zBuff);
                        }
                        m_iImageIndex++;
                        theLog->AddLine("Image embeded is number %d",m_iImageIndex);
                    }
                    // if we haven't found a tag (false alarm), write out what we
                    //  read in and move on
                    else {
                        // If we get here, we've seen something that looks like the
                        //  beginning of a tag, but isn't.  We just read the character
                        //  that tells us it isn't a tag.  Because this character could
                        //  be the START of a tag, we must "un-get" this character
                        tc = zBuff[strlen(zBuff)-1];
                        zBuff[strlen(zBuff)-1] = '\0';
                        ungetc(tc,SourceFile);
                        fwrite(&zBuff[0],sizeof(zBuff[0]),strlen(zBuff),TempFile);
                    }
                }
            }
            fclose(SourceFile);
            fclose(TempFile);
            DeleteFile(m_sDataFileName);
            theLog->AddLine("Deleting source file.");
            RenameFile(sTempFileName, m_sDataFileName);
            theLog->AddLine("Renaming temp file '%s' to '%s'",sTempFileName,m_sDataFileName);
        }
        DMPCATCHTHROW("Error converting embedded image tags to image data in output file.");

        if (bDeleteImgFiles)
            CleanUpImagesFiles();
    }
    DMPCATCHTHROW("ImageFileEmbeder::EmbedImages")
}

void ImageFileEmbeder::SetMaxRecordLength(long lMaxRecordLength)
{
        DMPTRY
        {
              m_lMaxRecordLength = lMaxRecordLength;
        }
        DMPCATCHTHROW("ImageFileEmbeder::SetMaxRecordLength")
}

void ImageFileEmbeder::CleanUpImagesFiles(ImageList *vsImageFileNameList)
{
        DMPTRY
        {
                if (m_bImbededImageData)
                {
                      String sFileName;
                      int iArsize = (int)vsImageFileNameList->ListSize;
                      for(int l = 0; l < iArsize; l++)
                      {
                              sFileName       = vsImageFileNameList->GetImageInfo(l).ImageFileName.c_str();
                              int iEbedTagPos = sFileName.UpperCase().Pos(EMBEDTAG);
                              sFileName       = sFileName.SubString(iEbedTagPos+TAGSIZE, sFileName.Length());
                              if(FileExists(sFileName.c_str()))
                              {
                                  DeleteFile(sFileName.c_str());
                              }
                      }
                 }
        }
        DMPCATCHTHROW("ImageFileEmbeder::CleanUpImagesFiles(ImageList *vsImageFileNameList)")
}

void ImageFileEmbeder::CleanUpImagesFiles()
{
        DMPTRY
        {
                if (m_bImbededImageData)
                {
                      String sFileName;
                      int iArsize = m_vsImageFileNameList.ListSize;
                      for(int l = 0; l < iArsize; l++)
                      {
                              sFileName       = m_vsImageFileNameList.GetImageInfo(l).ImageFileName.c_str();
                              int iEbedTagPos = sFileName.UpperCase().Pos(EMBEDTAG);
                              sFileName       = sFileName.SubString(iEbedTagPos+TAGSIZE, sFileName.Length());
                              if(FileExists(sFileName.c_str()))
                              {
                                  DeleteFile(sFileName.c_str());
                              }
                      }
                 }
        }
        DMPCATCHTHROW("ImageFileEmbeder::CleanUpImagesFiles()")
}

void ImageFileEmbeder::SetFileNameList(ImageList *vsImageFileNameList)
{
        DMPTRY
        {
               m_vsImageFileNameList.TakeOutTheTrash();

               for (int i = 0; i < vsImageFileNameList->ListSize; i++)
               {
                    m_vsImageFileNameList.AddImage(vsImageFileNameList->GetImageInfo(i));
               }
        }
        DMPCATCHTHROW("ImageFileEmbeder::SetFileNameList")
}

// ImageFilePathList class
ImageList::ImageList()
{
        DMPTRY
        {
             TakeOutTheTrash();
        }
        DMPCATCHTHROW("ImageList::ImageList")
}

ImageList::ImageList(ImageList *obj)
{
        DMPTRY
        {
                this->m_vIMGlist = obj->m_vIMGlist;
        }
        DMPCATCHTHROW("ImageList::ImageList(ImageList *obj)")
}

ImageList::~ImageList()
{
        DMPTRY
        {
            TakeOutTheTrash();
        }
        DMPCATCHTHROW("ImageList::~ImageList")
}

void ImageList::TakeOutTheTrash()
{
        DMPTRY
        {
                 m_vIMGlist.clear();
                 m_vIMGlist.resize(0);
        }
        DMPCATCHTHROW("ImageList::TakeOutTheTrash")
}

void ImageList::AddImage(ImageFieldInfo img)
{
        DMPTRY
        {
                m_vIMGlist.push_back(img);
        }
        DMPCATCHTHROW("ImageList::AddImage")
}

ImageFieldInfo ImageList::GetImageInfo(int iIndex)
{
        ImageFieldInfo Rval;
        DMPTRY
        {
                if(iIndex > (int)m_vIMGlist.size()-1)
                {
                        String sErrMsg = "";
                        sErrMsg.sprintf("TASK: Get the path from the list; ImageFieldList index is out of bounds.\n The index passed was %d, the maximum index of the array is: %d",iIndex,m_vIMGlist.size()-1);
                        throw DMPException(sErrMsg.c_str());
                }
                else
                {
                        Rval = m_vIMGlist[iIndex];
                }
        }
        DMPCATCHTHROW("ImageList::GetImageInfo")
        return Rval;
}

// CR9916 11/15/2004 ALH - new storage class for keeping tracking of image field information
ImageFieldInfo::ImageFieldInfo()
{
        DMPTRY
        {
               m_sImageFileName = "";
               m_lColumnSize    = 0;
               m_cJustifyChar   = 0;
               m_sPadChar       = 0;
        }
        DMPCATCHTHROW("ImageFieldInfo::ImageFieldInfo")
}

ImageFieldInfo::ImageFieldInfo(const ImageFieldInfo &obj)
{
        DMPTRY
        {
                this->m_sImageFileName   = obj.m_sImageFileName;
                this->m_lColumnSize      = obj.m_lColumnSize;
                this->m_cJustifyChar     = obj.m_cJustifyChar;
                this->m_sPadChar         = obj.m_sPadChar;
        }
        DMPCATCHTHROW("ImageFieldInfo::ImageFieldInfo(const ImageFieldInfo &obj)")
}

ImageFieldInfo::~ImageFieldInfo()
{
        DMPTRY
        {
             // hasta lavista baby!
        }
        DMPCATCHTHROW("ImageFieldInfo::~ImageFieldInfo()")
}

ImageFieldInfo& ImageFieldInfo::operator=(const ImageFieldInfo &obj)
{
        DMPTRY
        {
                this->m_sImageFileName   = obj.m_sImageFileName;
                this->m_lColumnSize      = obj.m_lColumnSize;
                this->m_cJustifyChar     = obj.m_cJustifyChar;
                this->m_sPadChar         = obj.m_sPadChar;
        }
        DMPCATCHTHROW("ImageFieldInfo::operator=")
        return *this;
}




