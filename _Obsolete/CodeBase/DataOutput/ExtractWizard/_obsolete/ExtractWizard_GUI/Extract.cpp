//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop
#include <IniFiles.hpp>
#include <FileCtrl.hpp>
#include "HandleIni.h"
#include "Extract.h"
#include "ExportPics.h"
#include "CustomFormat.h"
// #include "ExportLogFile32.h" // CR 16217 06/12/2006 DJK - This include file no longer required
#include "ImageFileEmbeder.h"
#include "DMPADOHelper.h"
//---------------------------------------------------------------------------
#include <dir.h>


// parameter command line execution:     R:\WizardScripts\EmbedExtractTest.cxs /P9/10/2004 /B50 /T30

#pragma package(smart_init)

TExtract::TExtract()
{
  DMPTRY {
    m_vIMGlist = NULL; //CR9916 04/26/2005 ALH
    m_frmTestExtractForm = NULL;
    FOnRecord = NULL;
    FOnImageProgress = NULL; // CR 5815 03/03/2004 DJK
    FOnCancel = NULL;
    FOnFinish = NULL;
    FOnError = NULL;
    m_bIsCancelled = false;
    m_script = NULL;
    FBank = "";
    FCustomer = "";
    FLockbox = "";
    FBatch = "";
    FProcDate = "";
    FDepDate = "";
    FHandle = 0;
    FCallerThreadID = 0;
    FExtractSequenceNumber = 0;
    m_tlDataSetList = new TList;
    m_sTempDir = GetTemporaryPath();
    //CR 18447 10/06/02006 ALH  - Begin
    m_sTempFileName = GenerateUniqueTempFileName("DMPExtract");
    m_sTempHtmlFileName = GenerateUniqueTempFileName("DMPExtractTable");
    //CR 18447 10/06/02006 ALH  - End
    m_batchTrace = NULL;
    TIniHandler Ini;
    Ini.Execute();
    m_iWKID = Ini.WorkstationID; // CR 13806 9/12/2005 DJK
    m_bUseADT = Ini.GetIniFileBool("Dates.ini", "DATES", "m_bUseADT", false);
    // m_logFile32Dll = 0; // CR 16217 06/12/2006 DJK - Remove this reference (no longer using this old-style logging)
    m_logFile = 0; // CR 16217 06/12/2006 DJK
    m_picsDll = 0;
    m_outputMan.SetLineCountManager(&m_lineCounter);
    m_bEmbedImageInDataFile = false;
    m_bExtractImages = false;  //CR9916 11/11/2004 ALH
    m_lJointDetailSequenceNumb = 0;  //CR12916  6/27/2005 ALH
  }
  DMPCATCHTHROW("TExtract::TExtract")
}

TExtract::~TExtract()
{
  DMPTRY {
    delete m_script;
    delete m_tlDataSetList;
    if(m_vIMGlist != NULL)   //CR9916 04/26/2005 ALH
    {
        delete m_vIMGlist;
    }
    m_vIMGlist = NULL;
  }
  DMPCATCHTHROW("TExtract::~TExtract")
}

//CR 18447 10/06/02006 ALH generate unique temp file names.
String TExtract::GenerateUniqueTempFileName(String sFileType)
{
        String sRval     = "";
        String sBaseName = "";
        DMPTRY
        {
               if(FExtractSequenceNumber <= 0)
               {
                     FExtractSequenceNumber = rand();
               }
               do
                {
                    srand((unsigned)time(0));
                    int irandom_integer = rand();
                    sBaseName.sprintf("%s_%d_%d.tmp",sFileType,irandom_integer,FExtractSequenceNumber);
                    sRval = m_sTempDir + sBaseName;
                }while(FileExists(sRval.c_str()));
        }
        DMPCATCHTHROW("TExtract::GenerateUniqueTempFileName()")
        return sRval;
}
//CR 18447 10/06/02006 ALH clean up temp files.
void TExtract::CleanUpTempFiles()
{
        DMPTRY
        {
              if(FileExists(m_sTempFileName))
              {
                      DeleteFile(m_sTempFileName);
              }
              if(FileExists(m_sTempHtmlFileName))
              {
                      DeleteFile(m_sTempHtmlFileName);
              }
        }
        DMPCATCHTHROW("TExtract::CleanUpTempFiles()")
}

bool TExtract::IsValid(const String& ValidChars, const String& Value)
{
  bool bResult = true;
  DMPTRY {
    for (int i = 1; i <= Value.Length() && bResult; i++)
      bResult = ValidChars.Pos(String(Value[i]));
  }
  DMPCATCHTHROW("TExtract::IsValid")
  return bResult;
}

void TExtract::SetBank(const String& BankID)
{
  DMPTRY {
    if (BankID.IsEmpty())
      FBank = BankID;
    else {
      if (!IsValid("0123456789-,", BankID))
        throw DMPException("Invalid bank id");
      FBank = BankID;
    }
  }
  DMPCATCHTHROW("TExtract::SetBank")
}

void TExtract::SetCustomer(const String& CustID)
{
  DMPTRY {
    if (CustID.IsEmpty())
      FCustomer = CustID;
    else {
      if (!IsValid("0123456789-,", CustID))
        throw DMPException("Invalid customer id");
      FCustomer = CustID;
    }
  }
  DMPCATCHTHROW("TExtract::SetCustomer")
}

void TExtract::SetLockbox(const String& LockboxID)
{
  DMPTRY {
    if (LockboxID.IsEmpty())
      FLockbox = LockboxID;
    else {
      if (!IsValid("0123456789-,", LockboxID))
        throw DMPException("Invalid lockbox id");
      FLockbox = LockboxID;
    }
  }
  DMPCATCHTHROW("TExtract::SetLockbox")
}

void TExtract::SetBatch(const String& BatchID)
{
  DMPTRY {
    if (BatchID.IsEmpty())
      FBatch = BatchID;
    else {
      if (!IsValid("0123456789-,", BatchID))
        throw DMPException("Invalid batch id");
      FBatch = BatchID;
    }
  }
  DMPCATCHTHROW("TExtract::SetBatch")
}

void TExtract::SetProcDate(const String& DateStr)
{
  DMPTRY {
    if (DateStr.IsEmpty())
      FProcDate = DateStr;
    else
      if (DateStr.Pos(",")) {
        TStringList *tmp = new TStringList;
        tmp->CommaText = DateStr;
        for (int i = 0; i < tmp->Count; i++) {
          TDateTime dtProcDate(tmp->Strings[i]);
        }
        delete tmp;
        FProcDate = DateStr;
      }
      else
        if (DateStr.Pos("-")) {
          TDateTime dtProcDate(DateStr.SubString(1, DateStr.Pos("-")-1));
          TDateTime dtProcDate2(DateStr.SubString(DateStr.Pos("-") + 1, DateStr.Length()));
          FProcDate = DateStr;
        }
        else {
          TDateTime dtProcDate(DateStr);
          FProcDate = DateStr;
        }
  }
  DMPCATCHTHROW("TExtract::SetProcDate")
}

void TExtract::SetDepDate(const String& DepStr)
{
  DMPTRY {
    if (DepStr.IsEmpty())
      FDepDate = DepStr;
    else {
      if (DepStr.Pos(",")){
        TStringList *tmp = new TStringList;
        tmp->CommaText = DepStr;
        for (int i = 0; i < tmp->Count; i++) {
          TDateTime dtDepDate(tmp->Strings[i]);
        }
        delete tmp;
        FDepDate = DepStr;
      }
      else if (DepStr.Pos("-")){
        TDateTime dtDepDate(DepStr.SubString(1, DepStr.Pos("-")-1));
        TDateTime dtDepDate2(DepStr.SubString(DepStr.Pos("-") + 1, DepStr.Length()));
        FDepDate = DepStr;
      }
      else {
        TDateTime dtDepDate(DepStr);
        FDepDate = DepStr;
      }
    }
  }
  DMPCATCHTHROW("TExtract::SetDepDate")
}

void TExtract::ParseTargetFileName()
{
  DMPTRY {
    String StaticPath, RemPath, Cookie;
    StaticPath = "";
    RemPath = m_outputMan.GetTargetFile();
    while (RemPath.Pos("@")){
      int Index = RemPath.Pos("@");
      StaticPath += RemPath.SubString(1, Index - 1);
      if (RemPath.Pos("]"))
        Cookie = RemPath.SubString(Index + 1, RemPath.Pos("]") - Index);
      else
        Cookie = RemPath.SubString(Index + 1, RemPath.Length());
      StaticPath += GetCookieValue(Cookie);
      if (RemPath.Length()> Cookie.Length())
        RemPath = RemPath.SubString(Index + 1 + Cookie.Length(), RemPath.Length());
      else
        RemPath = "";
    }
    m_outputMan.SetTargetFile(StaticPath + RemPath);
    StaticPath = ExtractFilePath(m_outputMan.GetTargetFile());
    if (!DirectoryExists(StaticPath))
      CreateDir(StaticPath);
  }
  DMPCATCHTHROW("TExtract::ParseTargetFileName")
}

String TExtract::GetCookieValue(const String& Cookie)
{
  String sResult;
  DMPTRY {
    String Exp, Format;
    if (Cookie.Pos("[")){
      int Index = Cookie.Pos("[");
      if (Cookie.Pos("]"))
        Format = Cookie.SubString(Index + 1, Cookie.Pos("]")-Index-1);
      else
        Format = Cookie.SubString(Index + 1, Cookie.Length());
      Exp = Cookie.SubString(1, Index - 1);
    }
    else {
      Exp = Cookie;
      Format = "";
    }
    if (Exp.UpperCase() == "DATE"){
      if (Format.IsEmpty())
        sResult = Date().FormatString("mmddyy");
      else
        DoFormatDateTime(Format, Date(), sResult);
    }
    else if (Exp.UpperCase() == "TIME"){
      if (Format.IsEmpty())
        sResult = Time().FormatString("hhnnss");
      else
        DoFormatDateTime(Format, Time(), sResult);
    }
    else if (Exp.UpperCase() == "CURRENTPROCESSINGDATE"){
      if (Format.IsEmpty())
        sResult = m_dtCurrentProcDate.FormatString("mmddyy");
      else
        DoFormatDateTime(Format, m_dtCurrentProcDate, sResult);
    }
    else if (Exp.UpperCase() == "PROCESSINGRUNDATE"){
      if (Format.IsEmpty())
        sResult = m_dtProcRunDate.FormatString("mmddyy");
      else
        DoFormatDateTime(Format, m_dtProcRunDate, sResult);
    }
    else if (Exp.UpperCase() == "BANK"){
      if (Format.IsEmpty())
        sResult = FBank;
      else
        DoFormatAlpha(Format, FBank, sResult);
    }
    else if (Exp.UpperCase() == "CUSTOMER"){
      if (Format.IsEmpty())
        sResult = FCustomer;
      else
        DoFormatAlpha(Format, FCustomer, sResult);
    }
    else if (Exp.UpperCase() == "LOCKBOX"){
      if (Format.IsEmpty())
        sResult = FLockbox;
      else
        DoFormatAlpha(Format, FLockbox, sResult);
    }
    else if (Exp.UpperCase() == "BATCH"){
      if (Format.IsEmpty())
        sResult = FBatch;
      else
        DoFormatAlpha(Format, FBatch, sResult);
    }
    else if (Exp.UpperCase() == "BATCHPROCDATE"){
      if (Format.IsEmpty())
        sResult = FProcDate;
      else
        DoFormatDateTime(Format, TDateTime(FBatch), sResult);
    }
  }
  DMPCATCHTHROW("TExtract::GetCookieValue")
  return sResult;
}

void TExtract::SetStandardFields()
{
  DMPTRY {
    DMPTRY {
      String sDate = FProcDate;
      if (sDate.IsEmpty())
        sDate = FDepDate;
      int iPos = sDate.Pos(",");
      if (iPos)
        sDate = sDate.SubString(1, iPos-2);
      iPos = sDate.Pos("-");
      if (iPos)
        sDate = sDate.SubString(1, iPos-2);
      m_dtProcRunDate = TDateTime(sDate);
    }
    DMPCATCHTHROW("Task: Convert 'FProcDate' string into TDateTime value")

    TIniHandler Ini;
    Ini.Execute();
    Ini.GetProcDate(m_dtCurrentProcDate);
    //CR 15923 6/16/2006 ALH   - Begin
    if (m_outputMan.GetTargetFile().IsEmpty())
    {
        m_outputMan.SetTargetFile(m_outputMan.SetFilePathAndVerifyPathing(m_script->ExtractPath));
    }
    else
    {
        m_outputMan.SetTargetFile(m_outputMan.SetFilePathAndVerifyPathing(m_outputMan.GetTargetFile()));
    }
    //CR 15923 6/16/2006 ALH   - End
    if (m_outputMan.GetTargetFile().IsEmpty())
      m_outputMan.SetTargetFile(m_script->ExtractPath);
    if (m_outputMan.GetTargetFile().Pos("@"))
      ParseTargetFileName();
    if (FHandle != 0){
      COPYDATASTRUCT CDS;
      CDS.dwData = 19950207;
      CDS.cbData = m_outputMan.GetTargetFile().Length()+1;
      CDS.lpData = new char[CDS.cbData];
      memset(CDS.lpData, '\x0', sizeof(CDS.lpData));
      strcpy((char*)CDS.lpData, m_outputMan.GetTargetFile().c_str());
      SendMessage((HWND)FHandle, WM_COPYDATA, 0, (long)&CDS);
      delete [] CDS.lpData;
    }
    if (FCallerThreadID != 0)
      SendTargetFileNameViaThreadMessages(FCallerThreadID, m_outputMan.GetTargetFile());
    if (FImagePath.IsEmpty())
      FImagePath = m_script->ImagePath;
    if (FImagePath.IsEmpty()) { // djk 10/30/01
      FImagePath = m_script->ExtractPath;
      FImagePath = FImagePath.SubString(1, FImagePath.LastDelimiter("\\"));
    }
    if (!FImagePath.IsEmpty()){
      if (FImagePath[FImagePath.Length()] != '\\')
        FImagePath =  FImagePath + "\\";
    }

    //CR xxxx 6/21/2006 ALH
     FImagePath =  m_outputMan.SetFilePathAndVerifyPathing(FImagePath);
    // CR 5815 03/03/2004 DJK
    ImageFileItem::ImageFormat iFileType = ImageFileItem::ifNone;

    switch (m_script->ImageFileFormatAsPDF) {
        case 0:
            iFileType = ImageFileItem::ifNone;
            break;
        case 1:
            iFileType = ImageFileItem::ifTIF;
            break;
        case 2:
            iFileType = ImageFileItem::ifPDF;
            break;
        case 3:
            iFileType = ImageFileItem::ifTIF2;
            break;
        default:
            throw DMPException("Illegal case, setting image format.");
    }

    if(iFileType != ImageFileItem::ifNone ){ // CR 9916 11/11/2004 ALH - if extract is not set to do image files, then don't do the image stuff.
        m_imageFileManager.Initialize(FOnImageProgress,
                                      m_picsDll,
                                      FImagePath,
                                      //m_script->ImageFileFormatAsPDF ? ImageFileItem::ifPDF : ImageFileItem::ifTIF,
                                      iFileType,
                                      m_script->CombineImagesAccrossProcDates,
                                      m_script->ImageFileProcDateFormat,
                                      m_script->ImageFileZeroPad,
                                      m_script->ImageFileBatchTypeFormatFull,
                                      m_script->ImageFileNameSingle,
                                      m_script->ImageFileNamePerTran,
                                      m_script->ImageFileNamePerBatch
                                     );
                                     m_bExtractImages = true;
    }
    else if (iFileType == ImageFileItem::ifNone){
        m_bExtractImages = false;   // CR 9916 11/11/2004 ALH
    }
    m_inputList.SetFieldValues(m_script->Parameters);
  }
  DMPCATCHTHROW("TExtract::SetStandardFields")
}


void TExtract::Run(const String& Setup, const String& TimeStampField, const String& TimeStamps)
{
  DMPTRY {
    m_lJointDetailSequenceNumb = 0;  //CR12916  6/27/2005 ALH
    DMPException* dmpEx1 = new DMPException();
    DMPTRY {
      m_extractAuditInfo.Initialize(FOperatorID, m_iWKID); // CR 13806 9/12/2005 DJK

      // CR 16217 06/12/2006 DJK - Switch from old logging dll to standard logging class:
      // DMPTRY {
      //   m_logFile32Dll = new LogFile32Loader("LogFile32.Dll");
      //   m_logFile32Dll->LoadDll();
      // }
      // DMPCATCHTHROW("Task: Instanciate logging class")

      DMPTRY {
        m_picsDll = new PicsLoader("Pics.Dll");
        m_picsDll->LoadDll();
      }
      DMPCATCHTHROW("Task: Load Pics DLL")

      m_confirmationFields.LoadFieldsFromIniFile(m_aggregateFields, m_recordCounts);
      //CR 18447 10/10/02006 ALH     begin
      m_sTempFileName = GenerateUniqueTempFileName("DMPExtract");
      m_sTempHtmlFileName = GenerateUniqueTempFileName("DMPExtractTable");
      //CR 18447 10/10/02006 ALH     end
      m_bHasAggregateHeaders  = false;
      m_bDoSpecialChecksTally = false; // CR 13806 9/09/2005 DJK
      m_bDoSpecialStubsTally  = false; // CR 13806 9/09/2005 DJK
      TFileStream *in = new TFileStream(Setup, fmOpenRead);
      TFileStream *out = new TFileStream(m_sTempFileName, fmCreate);        //CR 18447 10/09/02006 ALH
      out->CopyFrom(in, 0);
      delete in;
      delete out;
      m_sScriptFile = m_sTempFileName;          //CR 18447 10/09/02006 ALH
      m_script = new TScript(m_sScriptFile);
      String LogFile = m_script->LogFilePath;
      if (LogFile.IsEmpty())
        LogFile = ExtractFilePath(Setup) + "DMPExtract.log";

      // m_logFile32Dll->SetLogFileName(LogFile.c_str()); // CR 16217 06/12/2006 DJK
      // CR 16217 06/12/2006 DJK - Get log file retention setting from 'Mapper.ini', use it in instanciating AppLogging class.
      DMPTRY {
        int iLogRetentionDays;
        DMPTRY {
          TIniHandler iniHandler;
          iniHandler.GetWinInfo();
          TIniFile* ini = new TIniFile(iniHandler.IniPath+"Mapper.ini");
          iLogRetentionDays = ini->ReadInteger("Wizard", "LogRetentionDays", 30);
          delete ini;
        }
        DMPCATCHTHROW("Task: Retrieve log file retention days from 'Mapper.ini'")
        LogFile = m_outputMan.SetFilePathAndVerifyPathing(LogFile);
        m_logFile = new AppLogging(LogFile, true, true, iLogRetentionDays);
      }
      DMPCATCHTHROW("Task: Instanciate 'm_logFile' object")

      String LogData = "Extract - " + Setup + " started";

      // m_logFile32Dll->WriteLog(LogData.c_str()); // CR 16217 06/12/2006 DJK
      WriteToLogFile(LogData.c_str()); // CR 16217 06/12/2006 DJK

      LogData = "Server: " + DMPDataMod->ServerName + " , Database: " + DMPDataMod->DatabaseName;

      // m_logFile32Dll->WriteLog(LogData.c_str()); // CR 16217 06/12/2006 DJK
      WriteToLogFile(LogData.c_str()); // CR 16217 06/12/2006 DJK

      SetStandardFields();
      // CR 7411 ~ DLAQAB ~ 3-31-2004
      if (m_outputMan.GetTargetFile().IsEmpty())
      {
         m_outputMan.SetTargetFile(m_script->ExtractPath);
      }
      // End CR 7411 ~ DLAQAB ~ 3-31-2004
      if (!m_outputMan.OpenExtractFiles(m_script->UseHTML && m_script->ReformatHTML)) {
        Cleanup(false);
        return;
      }
      m_extractAuditInfo.SetExtractFileName(m_outputMan.GetTargetFile()); // CR 13806 9/12/2005 DJK
      m_detailRecFields.ClearFields();
      m_iMaxType = m_script->GetMaxType();
      if (m_iMaxType >= rtDetail)
        FillDetailFields();
      m_batchTrace = new TTimeStampTrace(TimeStampField, TimeStamps, DMPDataMod->ActionCmd);
      m_esnBatchUpdate.SetESN(FExtractSequenceNumber);
      PrepareQueries();
      if (!OpenQueries()){
        Cleanup(false);
        return;
      }
      m_aggregateFields.ClearAggregateCounts();
      FillDataSetList();
      bool Success = true;
      DMPException* dmpEx1 = new DMPException();
      DMPTRY {
        DoExtract();
        if (m_script->UseHTML)
          m_outputMan.ExportToHtml(m_script->HTMLHeaderFile, m_script->HTMLFooterFile);
        if (!m_bIsCancelled) {
          m_batchTrace->Update();
          m_esnBatchUpdate.UpdateBatchTable(DMPDataMod->ActionCmd);
        }
      }
      DMPCATCH_GET("Task: Generate extract", *dmpEx1)
      DMPException* dmpEx2 = new DMPException();
      DMPTRY {
        Cleanup(Success);
      }
      DMPCATCH_GET("Task: Do cleanup", *dmpEx2)
      if (!dmpEx1->Empty) {
        delete dmpEx2;
        throw *dmpEx1;
      }
      else {
        delete dmpEx1;
        if (!dmpEx2->Empty)
          throw *dmpEx2;
        else
          delete dmpEx2;
      }
    }
    DMPCATCH_GET("Task: Run extract", *dmpEx1)
    DMPException* dmpEx2 = new DMPException();
    DMPTRY {
      // CR 16217 06/12/2006 DJK - Switch from old logging dll to standard logging class:
      // if (m_logFile32Dll) {
      //   delete m_logFile32Dll;
      //   m_logFile32Dll = 0;
      // }
      delete m_logFile; // CR 16217 06/12/2006 DJK
      m_logFile = 0;    // CR 16217 06/12/2006 DJK
      if (m_picsDll) {
        delete m_picsDll;
        m_picsDll = 0;
      }
    }
    DMPCATCH_GET("Task: Unload AppLoging class, Pics DLL", *dmpEx2)
    if (!dmpEx1->Empty) {
      delete dmpEx2;
      throw *dmpEx1;
    }
    else {
      delete dmpEx1;
      if (!dmpEx2->Empty)
        throw *dmpEx2;
      else
        delete dmpEx2;
    }
  }
  DMPCATCHTHROW("TExtract::Run")
}

void TExtract::PrepareQueries()
{
  DMPTRY {
    DMPDataMod->ClearQueries();
    for (int i = rtFile; i <= m_iMaxType; i++)
      DefineDataSet(i);
  }
  DMPCATCHTHROW("TExtract::PrepareQueries")
}

// CR 12104 3/30/2005 DJK - Added new (2nd) parameter to all calls to 'LoadDataSet'
void TExtract::DefineDataSet(const int Type)
{
  DMPTRY {
    switch (Type){
      case rtFile        : LoadDataSet("File"                         , ""            , ""             , NULL                       , Type, false); break;
      case rtBank        : LoadDataSet("Bank"                         , "Bank"        , BANK_KEY       , DMPDataMod->BankDataSet    , Type, false); break;
      case rtCustomer    : LoadDataSet("Customer"                     , "Customer"    , CUSTOMER_KEY   , DMPDataMod->CustomerDataSet, Type, false); break;
      case rtLockbox     : LoadDataSet("Lockbox"                      , "Lockbox"     , LOCKBOX_KEY    , DMPDataMod->LockboxDataSet , Type, false); break;
      case rtBatch       : LoadDataSet("Batch"                        , "Batch"       , BATCH_KEY      , DMPDataMod->BatchDataSet   , Type, false); break;
      case rtTransactions: LoadDataSet("Transactions"                 , "Transactions", TRANSACTION_KEY, DMPDataMod->TranDataSet    , Type, false); break;
      case rtChecks      : LoadDataSet("Checks, ChecksDataEntry"      , "Checks"      , CHECK_KEY      , DMPDataMod->ChecksDataSet  , Type, false); break;
      //case rtStubs     : LoadDataSet("Stubs,StubsDataEntry"         ,                 STUB_KEY       , DMPDataMod->StubsDataSet   , Type, false); break;
      // CR 4969 ~ DLAQAB ~ 3-24-2004
      // Relaced STUBS_TABLES_N_JOINS for string "Stubs,StubsDataEntry" in line below
      case rtStubs       : LoadDataSet(STUBS_TABLES_N_JOINS           , "Stubs"       , STUB_KEY       , DMPDataMod->StubsDataSet   , Type, false); break;
      case rtDocs        : LoadDataSet("Documents, DocumentsDataEntry", "Documents"   , DOCUMENT_KEY   , DMPDataMod->DocsDataSet    , Type, false); break;
    }
  }
  DMPCATCHTHROW("TExtract::DefineDataSet")
}

// CR 5272 1/07/2004 DJK - Fixed (was broken before)
void TExtract::FillDetailFields()
{
  DMPTRY {
    m_script->GetSelectedFieldNames(rtDetail, m_detailRecFields);
    int iCntr = 0, iCount = m_detailRecFields.FieldCount();
    while (iCntr < iCount) {
      // CR 13806 10/04/2005 - START - Check for aggregate fields in detail fields, set appropriate flags if found
      if (m_aggregateFields.IsMember(m_detailRecFields[iCntr].GetFieldName())) {
        int AggregateType = m_aggregateFields.GetQueryType(m_detailRecFields[iCntr].GetFieldName());
        if (AggregateType == rtChecks)
          m_bDoSpecialChecksTally = true;
        if (AggregateType == rtStubs)
          m_bDoSpecialStubsTally = true;
      }
      // CR 13806 10/04/2005 - END
      if (m_standardFields.IsMember(m_detailRecFields[iCntr].GetFieldName())) {
        m_detailRecFields.Remove(iCntr);
        iCount--;
      }
      else
        iCntr++;
    }
  }
  DMPCATCHTHROW("TExtract::FillDetailFields")
}

// CR 5272 1/07/2004 DJK - Change made to accomodate new scheme of all field names being 'accompanied' by table names as well.
// CR 12104 3/30/2005 DJK - I added the new parameter 'sRootTable', and I made other changes as well
void TExtract::LoadDataSet(const String& sTable, const String& sRootTable, const String& sKey, TDMPADODataSet *dataSet, const int iType, bool bIsTest)
{
  DMPTRY {
    FieldCollection fieldList(false); // not sorted...

    m_script->GetSelectedFieldNames(iType, fieldList);

    FieldCollection footerFieldList(false); // not sorted...
    if (iType < rtChecks)
      m_script->GetSelectedFieldNames(TRecHelper::SetHeaderTrailer(iType, 1), footerFieldList);
    for (int i = fieldList.FieldCount() - 1; i >= 0; i--){
      if (m_aggregateFields.IsMember(fieldList[i].GetFieldName())){
        int AggregateType = m_aggregateFields.GetQueryType(fieldList[i].GetFieldName());
        if (AggregateType > m_iMaxType)
          m_iMaxType = AggregateType;
        // CR 13806 9/09/2005 DJK - START - If there are any aggregate fields for 'rtChecks', 'rtStubs' or 'rtDocs', then they must be the new fields added for this CR (before this CR Checks and Stubs could not have aggregate fields), so set the two new flags accordingly.
        if (iType == rtChecks || iType == rtStubs || iType == rtDocs) {
          if (AggregateType == rtChecks)
            m_bDoSpecialChecksTally = true;
          if (AggregateType == rtStubs)
            m_bDoSpecialStubsTally = true;
        }
        // CR 13806 9/09/2005 DJK - END
        m_bHasAggregateHeaders = true;
        fieldList.Remove(i);
      }
      else if (m_standardFields.IsMember(fieldList[i].GetFieldName()))
        fieldList.Remove(i);
      else if (m_recordCounts.IsMember(fieldList[i].GetFieldName())){
        m_bHasAggregateHeaders = true;
        fieldList.Remove(i);
      }
    }
    for (int i = footerFieldList.FieldCount() - 1; i >= 0; i--){
      if (m_aggregateFields.IsMember(footerFieldList[i].GetFieldName())){
        int AggregateType = m_aggregateFields.GetQueryType(footerFieldList[i].GetFieldName());
        if (AggregateType > m_iMaxType)
          m_iMaxType = AggregateType;
        footerFieldList.Remove(i);
      }
      else if (m_standardFields.IsMember(footerFieldList[i].GetFieldName()))
        footerFieldList.Remove(i);
      else if (fieldList.Locate(footerFieldList[i].GetFieldName()) != -1)
        footerFieldList.Remove(i);
    }
    fieldList.Add(footerFieldList);
    // CR 16503 10/3/2006  ALH - BEGIN
    if (iType > rtFile){
      for (int i = 0; i < m_detailRecFields.FieldCount(); i++){
        String CompTable;
        int idx = sTable.Pos(",");
        if (sTable.Pos(",") > 0)    //CR18855 11/03/2006 ALH   - more explicit
          CompTable = sTable.SubString(1, sTable.Pos(",")-1);
        else
          CompTable = sTable;
        //CR18855 11/03/2006 ALH
        //change to use Pos instead of compare, becase table name may be part of larger FROM statement.
        //also change the order so that Pos is being executed on CompTable since it usually
        //the larger string and there will be a greater chance for the GetTableName() string
        //to be smaller and thus exist in the larger string.
         if (CompTable.Pos(m_detailRecFields[i].GetTableName()) > 0)      //CR18855 11/03/2006 ALH
          fieldList.Add(m_detailRecFields[i].GetName(), true); // Add only if unique.
      }
      // CR 16503 10/3/2006  ALH - END
      if(sTable.UpperCase().Pos("CHECKS") > 0)
        m_detailRecFields.Add("Checks.BatchSequence", true); // Add only if unique.     // CR 16503 10/3/2006  ALH
      if(sTable.UpperCase().Pos("STUBS") > 0)
        m_detailRecFields.Add("Stubs.BatchSequence", true); // Add only if unique.      // CR 16503 10/3/2006  ALH
      if(sTable.UpperCase().Pos("DOCUMENTS") > 0)
        m_detailRecFields.Add("Documents.BatchSequence", true); // Add only if unique.  // CR 16503 10/3/2006  ALH
      if(sTable.UpperCase().Pos("DOCUMENTS") > 0)
        m_detailRecFields.Add("Documents.FileDescriptor", true); // Add only if unique. // CR 16503 10/3/2006  ALH

      String Where = m_script->GetLimits(iType);
      m_inputList.SubstituteParameters(Where, true);

      fieldList.InsertCommaDelimitedList(0, sKey, true);

      if (DMPDataMod)
        EnsureCorrectDataEntryTableNames(fieldList);

      String sSql = "Select " + fieldList.AsList() + " from " + sTable + " ";
      if (!bIsTest)
        AddParams(iType, Where);
      else if (iType > rtTransactions)
        AddModifiedParams(iType, Where);
      if (iType == rtBatch && m_batchTrace){
        String TraceWhere = m_batchTrace->GetWhereClause();
        if (!TraceWhere.IsEmpty())
          Where = Where + " and " + TraceWhere;
      }
      if (!Where.IsEmpty())
        sSql += Where + " ";

      //if (!Order.IsEmpty())  // CR 12120 3/30/2005 DJK - Line removed.
      //  sSql += Order + " "; // CR 12120 3/30/2005 DJK - Line removed.

      // CR 12120 3/30/2005 DJK - Following block added:
      DMPTRY {
        FieldCollection fieldListForOrderBy(false); // not sorted...
        TStringList* slForAscDesc = new TStringList();

        m_script->GetOrder(iType, fieldListForOrderBy, slForAscDesc);
        if (fieldListForOrderBy.FieldCount() != slForAscDesc->Count)
          throw DMPException("The array sizes must match");

        if (DMPDataMod)
          EnsureCorrectDataEntryTableNames(fieldListForOrderBy);

        if (fieldListForOrderBy.FieldCount()) {
          String sOrderBy = "Order By ";
          for (int i = 0; i < fieldListForOrderBy.FieldCount(); i++) {
            if (i)
              sOrderBy += ", ";
            sOrderBy += fieldListForOrderBy[i].GetName() + " " + slForAscDesc->Strings[i];
          }
          sSql += sOrderBy + " ";
        }

        delete slForAscDesc;
      }
      DMPCATCHTHROW("Task: Generate 'Order By' clause and add it to SQL string")

      if (sSql.Length())
        dataSet->CommandText = sSql;
      // m_logFile32Dll->WriteLog(sSql.c_str()); // CR 16217 06/12/2006 DJK
      WriteToLogFile(sSql.c_str()); // CR 16217 06/12/2006 DJK
    }
  }
  DMPCATCHTHROW("TExtract::LoadDataSet")
}

bool TExtract::ThereAreOccursRecords()
{
  bool bResult = false;
  DMPTRY {
    int iScriptNum = GetNextScript(0, rtDetail);
    while (iScriptNum >= 0) {
      if (ThisIsAnOccursRecord(iScriptNum))
        bResult = true;
      iScriptNum = GetNextScript(iScriptNum + 1, rtDetail);
    }
  }
  DMPCATCHTHROW("TExtract::ThereAreOccursRecords")
  return bResult;
}

bool TExtract::ThisIsAnOccursRecord(const int iScriptNum)
{
  bool bResult = false;
  DMPTRY {
    const TRecordItem* record = &(m_script->GetRecord(iScriptNum));
    for (int i = 0; i < record->GetFieldCount() && !bResult; ++i)
      // CR 8752 7/16/04 MLT - added IgnoreOccurs flag to each detail record
      // so user can override any occurs settings for the record's fields
      if (record->GetField(i).GetOccursGroup() != -1 && !TRecHelper::GetIgnoreOccursFlag(record->GetType()))
        bResult = true;
  }
  DMPCATCHTHROW("TExtract::ThisIsAnOccursRecord")
  return bResult;
}

void TExtract::AddParams(const int Type, String &WhereClause)
{
  DMPTRY {
    TStringList *tmp;
    if (!WhereClause.IsEmpty())
      WhereClause += " and ";
    else
      WhereClause += " where ";
    switch (Type){
      case rtBank:     if (FBank.IsEmpty())
                         WhereClause += " (BankID > 0)";
                       else if (FBank.Pos(","))
                         WhereClause += " (BankID in (" + FBank + "))";
                       else if (FBank.Pos("-"))
                         WhereClause += " (BankID between " + FBank.SubString(1, FBank.Pos("-") - 1)
                          + " and " + FBank.SubString(FBank.Pos("-") + 1, FBank.Length()) + ")";
                       else
                         WhereClause += " (BankID=" + FBank + ")";
                       break;
      case rtCustomer: WhereClause += " BankID = :BankID ";
                       if (FCustomer.IsEmpty())   // user did not enter a CustomerID
                       {
                         // CR 7938 6/11/04 MLT - if customers are not specified but lockboxes and banks are,
                         // limit customers to those associated with the banks and lockboxes
                         
                         String SubWhereClause;
                         //bank
                         if (FBank.IsEmpty())
                           SubWhereClause += " (BankID > 0) ";
                         else if (FBank.Pos(","))
                           SubWhereClause += " (BankID in (" + FBank + ")) ";
                         else if (FBank.Pos("-"))
                           SubWhereClause += " (BankID between " + FBank.SubString(1, FBank.Pos("-") - 1)
                            + " and " + FBank.SubString(FBank.Pos("-") + 1, FBank.Length()) + ") ";
                         else
                           SubWhereClause += " (BankID=" + FBank + ") ";
                         //lockbox
                         if (FLockbox.IsEmpty())
                           SubWhereClause += " and (LockboxID > 0) ";
                         else {
                           if (FLockbox.Pos(","))
                             SubWhereClause += " and (LockboxID in (" + FLockbox + ")) ";
                           else if (FLockbox.Pos("-"))
                             SubWhereClause += " and (LockboxID between " + FLockbox.SubString(1, FLockbox.Pos("-")-1)
                                  + " and " + FLockbox.SubString(FLockbox.Pos("-")+1, FLockbox.Length()) + ") ";
                           else
                             SubWhereClause += " and (LockboxID=" + FLockbox + ") ";
                         }
                         WhereClause += " and (CustomerID in (Select CustomerID From Lockbox Where " + SubWhereClause + "))";
                       }
                       else     // user entered a CustomerID
                       {
                         if (FCustomer.Pos(","))
                           WhereClause += " and (CustomerID in (" + FCustomer + "))";
                         else if (FCustomer.Pos("-"))
                           WhereClause += " and (CustomerID between " + FCustomer.SubString(1, FCustomer.Pos("-") -1)
                                  + " and " + FCustomer.SubString( FCustomer.Pos("-") + 1, FCustomer.Length()) + ")";
                         else
                           WhereClause += " and (CustomerID=" + FCustomer + ")";
                       }
                       break;
      case rtLockbox:  WhereClause += " (BankID = :BankID and CustomerID = :CustomerID) ";
                       if (FLockbox.IsEmpty())
                         WhereClause += " and (LockboxID > 0)";
                       else {
                         if (FLockbox.Pos(","))
                           WhereClause += " and (LockboxID in (" + FLockbox + "))";
                         else if (FLockbox.Pos("-"))
                           WhereClause += " and (LockboxID between " + FLockbox.SubString(1, FLockbox.Pos("-")-1)
                                  + " and " + FLockbox.SubString(FLockbox.Pos("-")+1, FLockbox.Length()) + ")";
                         else
                           WhereClause += " and (LockboxID=" + FLockbox + ")";
                       }
                       break;
      case rtBatch:    WhereClause += " (BankID = :BankID and LockboxID = :LockboxID) ";
                       if (!FBatch.IsEmpty()){
                         if (FBatch.Pos(","))
                           WhereClause += " and (BatchID in (" + FBatch + "))";
                         else if (FBatch.Pos("-"))
                           WhereClause += " and (BatchID between " + FBatch.SubString(1, FBatch.Pos("-") -1) +
                                  " and " + FBatch.SubString(FBatch.Pos("-")+1, FBatch.Length()) + ")";
                         else
                           WhereClause += " and (BatchID=" + FBatch + ")";
                       }
                       if (!FProcDate.IsEmpty()){
                         if (FProcDate.Pos(",")){
                           tmp = new TStringList;
                           tmp->CommaText = FProcDate;
                           WhereClause += " and (ProcessingDate in (";
                           for (int i = 0; i < tmp->Count - 1; i++ )
                             WhereClause += QuotedStr(tmp->Strings[i]) + ",";
                           WhereClause += QuotedStr(tmp->Strings[tmp->Count-1]) + "))";
                           delete tmp;
                         }
                         else if (FProcDate.Pos("-"))
                           WhereClause += " and (ProcessingDate between " + QuotedStr(FProcDate.SubString(1, FProcDate.Pos("-") -1))
                                  + " and " + QuotedStr(FProcDate.SubString(FProcDate.Pos("-") + 1, FProcDate.Length())) + ")";
                         else
                           WhereClause += " and (ProcessingDate=" + QuotedStr(FProcDate) + ")";
                       }
                       if (!FDepDate.IsEmpty()){
                         if (FDepDate.Pos(",")) { // djk - 2/12/02 - 'CommaText' doesn't work for this one, as it also goes off on spaces, wrecking dates which also have times - I use 'strtok' instead.
                           char *zPtr, *zDepDate = new char[FDepDate.Length()+1];
                           strcpy(zDepDate, FDepDate.c_str());
                           bool bFirstToken = true;
                           while ((zPtr = strtok(bFirstToken ? zDepDate : 0, ",")) != 0) {
                             WhereClause += " and ("+GetDepDateSQL(zPtr)+")";
                             bFirstToken = false;
                           }
                           delete[] zDepDate;
                         }
                         else if (FDepDate.Pos("-"))
                           WhereClause += " and ("+GetDepDateSQL(FDepDate.SubString(1, FDepDate.Pos("-") -1), FDepDate.SubString(FDepDate.Pos("-") + 1, FDepDate.Length()))+")";
                         else
                           WhereClause += " and ("+GetDepDateSQL(FDepDate)+")";
                       }
                       break;
      case rtTransactions:  WhereClause += " (GlobalBatchID = :GlobalBatchID) "; break;
      case rtChecks: WhereClause += " (Checks.GlobalBatchID    = :GlobalBatchID) And (ChecksDataEntry.GlobalCheckID       = Checks.GlobalCheckID      ) "; break;
      case rtStubs : WhereClause += " (Stubs.GlobalBatchID     = :GlobalBatchID) "                                                                       ; break; // CR 12104 3/30/2005 DJK - Modified 'where' clause
      case rtDocs  : WhereClause += " (Documents.GlobalBatchID = :GlobalBatchID) And (DocumentsDataEntry.GlobalDocumentID = Documents.GlobalDocumentID And (Documents.BatchSequence NOT IN (Select Distinct Stubs.BatchSequence FROM Stubs WHERE Stubs.GlobalBatchID = Documents.GlobalBatchID))) "; break;
    }
    // m_logFile32Dll->WriteLog(WhereClause.c_str()); // CR 16217 06/12/2006 DJK
    WriteToLogFile(WhereClause.c_str()); // CR 16217 06/12/2006 DJK
  }
  DMPCATCHTHROW("TExtract::AddParams")
}

void TExtract::AddModifiedParams(const int Type, String &WhereClause)
{
  DMPTRY {
    if (Type >= rtChecks){
      if (!WhereClause.IsEmpty())
        WhereClause += " and ";
      else
        WhereClause += " where ";
    }
    switch (Type){
      case rtChecks: WhereClause += " (ChecksDataEntry.GlobalCheckID       = Checks.GlobalCheckID      ) "; break;
      case rtStubs : WhereClause += " (StubsDataEntry.GlobalStubID         = Stubs.GlobalStubID        ) "; break;
      case rtDocs  : WhereClause += " (DocumentsDataEntry.GlobalDocumentID = Documents.GlobalDocumentID) "; break;
    }
    // m_logFile32Dll->WriteLog(WhereClause.c_str()); // CR 16217 06/12/2006 DJK
    WriteToLogFile(WhereClause.c_str()); // CR 16217 06/12/2006 DJK
  }
  DMPCATCHTHROW("TExtract::AddModifiedParams")
}

String TExtract::GetDepDateSQL(const String& sDepDate)
{
  String sResult;
  DMPTRY {
    TDateTime dtTemp(sDepDate);
    if (m_bUseADT && fmod((double) dtTemp, 1.0) > 1.0/24.0/60.0/60.0/2.0)
      sResult = "DepositDate = " + QuotedStr(dtTemp.FormatString("m/d/yyyy h:nn:ss"));
    else
      sResult = "DepositDate between "+QuotedStr(TDateTime(floor((double) dtTemp)).FormatString("m/d/yyyy h:nn:ss"))+" and "+QuotedStr(TDateTime(floor((double) dtTemp)+1.0-1.0/24.0/60.0/60.0/2.0).FormatString("m/d/yyyy h:nn:ss"));
  }
  DMPCATCHTHROW("TExtract::GetDepDateSQL")
  return sResult;
}

String TExtract::GetDepDateSQL(const String& sDepDate1, const String& sDepDate2)
{
  String sResult;
  DMPTRY {
    TDateTime dtTemp1(sDepDate1);
    TDateTime dtTemp2(sDepDate2);
    if (!m_bUseADT || fmod((double) dtTemp1, 1.0) < 1.0/24.0/60.0/60.0/2.0)
      dtTemp1 = TDateTime(floor((double) dtTemp1));
    if (!m_bUseADT || fmod((double) dtTemp2, 1.0) < 1.0/24.0/60.0/60.0/2.0)
      dtTemp2 = TDateTime(floor((double) dtTemp2)+1.0-1.0/24.0/60.0/60.0/2.0);
    sResult = "DepositDate between "+QuotedStr(dtTemp1.FormatString("m/d/yyyy h:nn:ss"))+" and "+QuotedStr(dtTemp2.FormatString("m/d/yyyy h:nn:ss"));
  }
  DMPCATCHTHROW("TExtract::GetDepDateSQL")
  return sResult;
}

bool TExtract::OpenQueries()
{
  bool bResult = false;
  DMPTRY {
    /////// djk /////// - These must be opened first, so that when BatchQry is opened, and the 'OnOpen' event fires, these dependent queries can react.
    if (m_iMaxType >= rtChecks){
      DMPTRY {
        DMPDataMod->ChecksDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Checks query")
    }
    if (m_iMaxType >= rtStubs){
      DMPTRY {
        DMPDataMod->StubsDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Stubs query")
    }
    if (m_iMaxType >= rtDocs){
      DMPTRY {
        DMPDataMod->DocsDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Documents query")
    }
    /////// djk ///////

    if (m_iMaxType >= rtBank){
      DMPTRY {
        DMPDataMod->BankDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Bank query")
    }
    if (m_iMaxType >= rtCustomer){
      DMPTRY {
        DMPDataMod->CustomerDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Customer query")
    }
    if (m_iMaxType >= rtLockbox){
      DMPTRY {
        DMPDataMod->LockboxDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Lockbox query")
    }
    if (m_iMaxType >= rtBatch){
      DMPTRY {
        DMPDataMod->BatchDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Batch query")
    }
    if (m_iMaxType >= rtTransactions){
      DMPTRY {
        DMPDataMod->TranDataSet->Open();
      }
      DMPCATCHTHROW("Task: Open Transaction query")
    }
    bResult = true;
  }
  DMPCATCHTHROW("TExtract::OpenQueries")
  return bResult;
}

void TExtract::CloseQueries()
{
  DMPTRY {
    DMPDataMod->DocsDataSet->Close();
    DMPDataMod->StubsDataSet->Close();
    DMPDataMod->ChecksDataSet->Close();
    DMPDataMod->TranDataSet->Close();
    DMPDataMod->BatchDataSet->Close();
    DMPDataMod->LockboxDataSet->Close();
    DMPDataMod->CustomerDataSet->Close();
    DMPDataMod->BankDataSet->Close();
    DMPDataMod->InitializeCounters();
  }
  DMPCATCHTHROW("TExtract::CloseQueries")
}

void TExtract::FillDataSetList()
{
  DMPTRY {
    while (m_tlDataSetList->Count)
      m_tlDataSetList->Delete(0);
    m_tlDataSetList->Add(NULL);
    m_tlDataSetList->Add(NULL);
    m_tlDataSetList->Add(DMPDataMod->BankDataSet);
    m_tlDataSetList->Add(DMPDataMod->CustomerDataSet);
    m_tlDataSetList->Add(DMPDataMod->LockboxDataSet);
    m_tlDataSetList->Add(DMPDataMod->BatchDataSet);
    m_tlDataSetList->Add(DMPDataMod->TranDataSet);
    m_tlDataSetList->Add(DMPDataMod->ChecksDataSet);
    m_tlDataSetList->Add(DMPDataMod->StubsDataSet);
    m_tlDataSetList->Add(DMPDataMod->DocsDataSet);
  }
  DMPCATCHTHROW("TExtract::FillDataSetList")
}

//---------------------------------------------------------------------------
void TExtract::DoExtract()
{
  DMPException *E = new DMPException();   //CR 18447 10/06/02006 ALH
  DMPTRY
  {
    m_iLastScriptNum = 0;
    if (m_vIMGlist == NULL)
    {
        m_vIMGlist = new ImageList();    //CR9916 04/26/2005 ALH
    }
    m_iLastType = 0;
    int FilePos[rtChecks] = {0};      //Insertion points for extract headers
    int HtmlFilePos[rtChecks] = {0};  //Insertion points for html headers
    for (int i = 0; i <= rtDetail; i++)
      RecordCountArray[i] = 0;
    m_counterFields.Initialize(m_script);
    bool DoDetail = m_iMaxType >= rtDetail;
    bool DoChecks = false, DoStubs = false, DoDocs = false;
    bool Occurs = ThereAreOccursRecords();
    if (DoDetail)
      GetDetailParams(DoChecks, DoStubs, DoDocs);
     if (m_outputMan.GetIsHtmlTable()){
      m_outputMan.WriteHtml("<table border=" + IntToStr(m_script->HTMLBorderWidth) + " cellspacing=" + IntToStr(m_script->HTMLCellSpacing) + " width=\"100%\">");
      for (int i = 0; i < rtChecks; i++)
        HtmlFilePos[i] = m_outputMan.GetHtmlSize();
    }
    int CurType = rtFile;
    DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);
    while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){
      DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);
      while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){
        DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);
        while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){
          DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);
          while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){        //Batch
            DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);

            if (FOnRecord)
               FOnRecord(DMPDataMod->RecBank, DMPDataMod->RecCustomer, DMPDataMod->RecLockbox, DMPDataMod->RecBatch, DMPDataMod->RecItem, DMPDataMod->ProcDateStr.c_str(), DMPDataMod->DepDateStr.c_str());

            if (FOnCancel)
              FOnCancel(m_bIsCancelled);
            if (m_bIsCancelled){
              // m_logFile32Dll->WriteLog("User cancelled"); // CR 16217 06/12/2006 DJK
              WriteToLogFile("User cancelled"); // CR 16217 06/12/2006 DJK
              return;
            }
            if (m_batchTrace->RunStatus == rsFirstRun)
              m_batchTrace->AddBatch(((TDMPADODataSet*)m_tlDataSetList->Items[rtBatch])->FieldByName("GlobalBatchID")->AsInteger);
            m_esnBatchUpdate.AddGlobalBatchID(((TDMPADODataSet*)m_tlDataSetList->Items[rtBatch])->FieldByName("GlobalBatchID")->AsInteger);

            FieldCollection detailRecs(m_detailRecFields);

            while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){       //transaction
               m_vBatSeqList.clear();               // CR 16503 10/03/2006 ALH - clear the sequence list.
               m_vDocumentFileDescriptors.clear();  // CR 16503 10/03/2006 ALH - clear out old fild descriptors

              // CR 13806 9/09/2005 DJK
              OptionallyTallyCheckAndStubCountsAndAmountsForTransaction();

              detailRecs.ClearRows();
              DoRecordBefore(CurType++, FilePos[CurType], HtmlFilePos[CurType]);
              int ChecksCountTotal = 0, StubsCountTotal = 0, DocsCountTotal = 0;

              while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){       //checks
                DoRecordBefore(CurType, FilePos[CurType], HtmlFilePos[CurType]);
                if (DoDetail && DoChecks){
                  detailRecs.AddRow(m_detailRecFields, 0);
                  // CR 16503 10/03/2006 ALH - Begin
                  String Data =  "";
                  int Index = detailRecs.Locate("Checks.BatchSequence");
                  if (Index < 0)
                    Data = "";
                  else
                    Data = detailRecs[Index].GetValue(ChecksCountTotal);
                  m_vBatSeqList.push_back(atoi(Data.c_str()));
                  m_vDocumentFileDescriptors.push_back("C");    // CR 16503 10/03/2006 ALH
                  // CR 16503 10/03/2006 ALH - END
                  ChecksCountTotal++;
                }
                ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
              }

              ClearDetailField(CurType);
              CurType++;
              int DetailRecCount = 0;
              while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){   //stubs
                DoRecordBefore(CurType, FilePos[CurType], HtmlFilePos[CurType]);
                if (DoDetail && DoStubs){
                  GetParallelDetailData(CurType, detailRecs, DetailRecCount++);
                // CR 16503 10/03/2006 ALH - Begin
                String Data =  "";
                int Index = detailRecs.Locate("Stubs.BatchSequence");
                if (Index < 0)
                  Data = "";
                else
                  Data = detailRecs[Index].GetValue(StubsCountTotal);
                m_vBatSeqList.push_back(atoi(Data.c_str()));  // CR 16503 10/03/2006 ALH
                m_vDocumentFileDescriptors.push_back("S");    // CR 16503 10/03/2006 ALH
                // CR 16503 10/03/2006 ALH - END
                StubsCountTotal++;
              }
              ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
              }

              ClearDetailField(CurType);
              CurType++;
              DetailRecCount = 0;
               int Index = 0;       // CR 16503 10/03/2006 ALH 
              while ((!((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Eof)){  //documents
                DoRecordBefore(CurType, FilePos[CurType], HtmlFilePos[CurType]);
                if (DoDetail && DoDocs){
                  GetParallelDetailData(CurType, detailRecs, DetailRecCount++);
                // CR 16503 10/03/2006 ALH - Begin
                String Data =  "";
                Index = detailRecs.Locate("Documents.BatchSequence");
                if (Index < 0)
                  Data = "";
                else
                  Data = detailRecs[Index].GetValue(DocsCountTotal);
                m_vBatSeqList.push_back(atoi(Data.c_str()));
                Data = "UT";  // initialize to an untyped document.
                Index = detailRecs.Locate("Documents.FileDescriptor");
                if (Index >= 0)
                  Data = detailRecs[Index].GetValue(DocsCountTotal);
                m_vDocumentFileDescriptors.push_back(Data.c_str());
                // CR 16503 10/03/2006 ALH - END
                  DocsCountTotal++;
                }
                ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
              }

              ClearDetailField(CurType);
              CurType++;                      //detail
              if (detailRecs.RowCount() == 0)
                detailRecs.AddRow(m_detailRecFields, 0);
              int RealType = TRecHelper::SetRecordDetail(rtDetail, true);
              if (Occurs){
                int ChecksCount = 0, StubsCount = 0, DocsCount = 0;
                if (detailRecs.RowCount()){
                  int iScriptNum = GetNextScript(0, RealType);
                  while (iScriptNum >= 0){
                    PrintDetailOccursRecords(detailRecs, iScriptNum, ChecksCountTotal, StubsCountTotal, DocsCountTotal, ChecksCount, StubsCount, DocsCount);
                    iScriptNum = GetNextScript(iScriptNum + 1, RealType);
                  }
                }
              }
              else{
                if (ChecksCountTotal || StubsCountTotal || DocsCountTotal) {
                  int iScriptNum = GetNextScript(0, RealType);    //GW
                  while (iScriptNum >= 0){
                    int HowManyRecsOut = CalculateHowManyOfThisRecToGoOut(iScriptNum, ChecksCountTotal, StubsCountTotal, DocsCountTotal);
                    m_counterFields.SetRecordCount(rtDetail, HowManyRecsOut);
                    for (int i = 0; i < HowManyRecsOut; i++){
                      // CR 16503 10/03/2006 ALH   - Begin
                      PrintDetailRecords(detailRecs, i, iScriptNum);  // CR 16503 10/03/2006 ALH
                      RecordCountArray[CurType]++;
                      std::vector<int>::iterator intITT = m_vBatSeqList.begin();
                      m_vBatSeqList.erase(intITT);            // CR 16503 10/03/2006 ALH
                      std::vector<String>::iterator stringITT = m_vDocumentFileDescriptors.begin();
                      m_vDocumentFileDescriptors.erase(stringITT); // CR 16503 10/03/2006 ALH
                      // CR 16503 10/03/2006 ALH   - End
                    }
                    iScriptNum = GetNextScript(iScriptNum + 1, RealType);
                  }
                }
              }
              CurType = rtTransactions;
              DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
              ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
            }         //end Tran
            CurType--;
            DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
            ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
          }     //end Batch
          CurType--;
          DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
          ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
        }//end Box
        CurType--;
        DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
        ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
      }//end Customer
      CurType--;
      DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
      ((TDMPADODataSet*)(m_tlDataSetList->Items[CurType]))->Next();
    }  //end Bank
    CurType--;
    DoRecordAfter(CurType, FilePos[CurType], HtmlFilePos[CurType]);
    DoFinalStep();
    if (m_outputMan.GetIsHtmlTable())
      m_outputMan.WriteHtml("\n</table>");
    String sImageError = "";
    if (m_bExtractImages){
        if (!m_imageFileManager.CreateImageFiles()){ // CR 5815 03/03/2004 DJK
            if (m_script->EmbedImages) {
                ImageFileEmbeder IFE;
                IFE.CleanUpImagesFiles(m_vIMGlist);
            }
            sImageError = m_imageFileManager.GetImageError();
            sImageError.sprintf("TASK: Creating Image Files after extract. \n%s",sImageError);
            throw DMPException(sImageError);
        }
        else if (m_script->EmbedImages) {     // CR#13656 10/18/05 MSV - only check for embedded
            EmbedImagesIntoDataFile();        //    image tags if extract requires it
            sImageError = m_imageFileManager.GetImageError();
        }
        if ((sImageError != "") && (sImageError != ""))  //CR9916 11/08/2004 ALH
        {
           ::MessageBox(NULL, sImageError.c_str(), "Image Extraction Error", MB_OK | MB_ICONERROR);

        }
    }

    //CR10943 03/30/2005 ALH - Begin
    //CR 19345 03/16/2007 ALH   - moved post processing dll to cleanup.
    if(m_vIMGlist != NULL)   //CR9916 04/26/2005 ALH
    {
        delete m_vIMGlist;
    }
    m_vIMGlist = NULL;
    //CR10943 03/30/2005 ALH - End
    //CR 18447 10/06/02006 ALH - begin
    CleanUpTempFiles();
  }
  DMPCATCH_GET("TExtract::DoExtract", *E)
  if(!E->IsEmpty())
  {
      CleanUpTempFiles();
      throw *E;
  }
  //CR 18447 10/06/02006 ALH - end
}

// CR 13806 9/09/2005 DJK - Added this new method - If either of the flags indicate the need for this special tally, then execute the required stored procedure and post the results to the 'm_aggregateFields' object.
void TExtract::OptionallyTallyCheckAndStubCountsAndAmountsForTransaction()
{
  DMPTRY {
    if (m_bDoSpecialChecksTally || m_bDoSpecialStubsTally) {
      int iCheckCount, iStubCount;
      double dCheckDollars, dStubDollars;

      DMPTRY {
        DMPADOStoredProc sp("proc_EW_Sel_ChecksStubs_GetCountsAndDollarTotals", 2, DMPDataMod->DMPConnection);
        sp.AddParameter("@parmGlobalBatchID", DMPDataMod->BatchDataSet->FieldByName("GlobalBatchID")->AsInteger);
        sp.AddParameter("@parmTransactionID", DMPDataMod->TranDataSet ->FieldByName("TransactionID")->AsInteger);
        sp.AddParameter("@parmDoCheckTally" , m_bDoSpecialChecksTally);
        sp.AddParameter("@parmDoStubTally"  , m_bDoSpecialStubsTally);
        sp.AddParameter("@parmCheckCount"   , (int   ) 0  , pdOutput);
        sp.AddParameter("@parmCheckDollars" , (double) 0.0, pdOutput);
        sp.AddParameter("@parmStubCount"    , (int   ) 0  , pdOutput);
        sp.AddParameter("@parmStubDollars"  , (double) 0.0, pdOutput);
        sp.ExecProc();
        DMPTRY {
          iCheckCount   = sp.GetParamByName("@parmCheckCount"  );
          iStubCount    = sp.GetParamByName("@parmStubCount"   );
          dCheckDollars = sp.GetParamByName("@parmCheckDollars");
          dStubDollars  = sp.GetParamByName("@parmStubDollars" );
        }
        DMPCATCHTHROW("Task: Retrieve data from 'output' parameters")
      }
      DMPCATCHTHROW("Task: Fetch check/stub counts and dollar totals for a given transaction using stored procedure")

      m_aggregateFields.SetCheckStubAggregateValues(iCheckCount, iStubCount, dCheckDollars, dStubDollars);
    }
  }
  DMPCATCHTHROW("TExtract::OptionallyTallyCheckAndStubCountsAndAmountsForTransaction")
}

// CR 9916 10/18/2004 ALH  - new FUNCTION for embeding immages into file.
void TExtract::EmbedImagesIntoDataFile()
{
    DMPTRY{
        // m_logFile32Dll->WriteLog("Embedding images..."); // CR 16217 06/12/2006 DJK
        WriteToLogFile("Embedding images..."); // CR 16217 06/12/2006 DJK
        ImageFileEmbeder IFE;
        IFE.SetDataFileName(TargetFile);
        IFE.SetFileNameList(m_vIMGlist);
        m_outputMan.CloseExtractFiles();
        IFE.EmbedImages(m_bExtractImages);
    }
    DMPCATCHTHROW("TExtract::EmbedImagesIntoDataFile")
}

//CR10943 03/30/2005 ALH - New Function for hooking in a post processing DLL
void TExtract::RunPostProcessingDLL()
{
        char *zError = NULL;
        char *zParms = NULL;
        CustomDLLObject *customDLL = new CustomDLLObject(m_script->sPostProcessingDLL);
        DMPException *E = new DMPException();
        DMPTRY
        {
             FILE *fptr = fopen(TargetFile.c_str(),"rb");
	     if(fptr != NULL)
	     {
                  fclose(fptr);
                  DMPTRY //CR10943 05/12/2005 ALH
                  {
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBank]))     ->First();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtCustomer])) ->First();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtLockbox]))  ->First();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBatch]))    ->First();

                      Bank            =  ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBank]))    ->FieldByName("BankID")->AsString;
                      Customer        =  ((TDMPADODataSet*)(m_tlDataSetList->Items[rtCustomer]))->FieldByName("CustomerID")->AsString;
                      Lockbox         =  ((TDMPADODataSet*)(m_tlDataSetList->Items[rtLockbox])) ->FieldByName("LockboxID")->AsString;
                      Batch           =  ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBatch]))   ->FieldByName("BatchID")->AsString;
                      if(ProcDate.IsEmpty())
                      {
                              ProcDate = DepDate;
                      }

                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBank]))     ->Last();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtCustomer])) ->Last();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtLockbox]))  ->Last();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBatch]))    ->Last();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBank]))     ->Close();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtCustomer])) ->Close();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtLockbox]))  ->Close();
                      ((TDMPADODataSet*)(m_tlDataSetList->Items[rtBatch]))    ->Close();
                  }
                  DMPCATCHTHROW("TASK: Re - populate member variabeles with values from actual record sets.")
                  String sError = "";
                  if(FileExists(m_script->sPostProcessingDLL.c_str()))  // make sure the dll exists.
                  {
                              customDLL->LoadDll();
                              if (!customDLL->IsDllLoaded())
                              {
                                      sError.sprintf("TASK LOAD DLL: Unable to load %s, post processing DLL.",m_script->sPostProcessingDLL.c_str());
                                      throw DMPException(sError.c_str());
                              }
                              else
                              {
                                      String sParms = "";
                                      sParms.sprintf("<BANKID>%s<CUSTOMERID>%s<LOCKBOXID>%s<BATCHID>%s<PROCDATE>%s",Bank.c_str(),Customer.c_str(),Lockbox.c_str(),Batch.c_str(),ProcDate.c_str());
                                      zError = new char[4024];
                                      zParms = new char[1048];
                                      memset(zError,0,4024);
                                      memset(zParms,0,1048);
                                      strcpy(zParms,sParms.c_str());
                                      if(!customDLL->CallToFunction(m_script->ExtractPath.c_str(),zError,zParms))
                                      {
                                          sError.sprintf("TASK Run custom post processing dll.  The DLL did NOT complete succesfully.\nError: %s",zError);
                                          throw DMPException(sError.c_str());
                                      }
                              }
                  }
                  else
                  {
                      sError.sprintf("The custom DLL: '%s', could not be found.",m_script->sPostProcessingDLL.c_str());
                      throw DMPException(sError.c_str());
                  }
             }
        }
        DMPCATCH_GET("TExtract::RunPostProcessingDLL()",*E)
        if (!E->IsEmpty())
        {
            if(zError != NULL)
            {
                delete []zError;
            }
            if (zParms != NULL)
            {
                delete []zParms;
            }
            if (customDLL != NULL)
            {
                delete customDLL;
            }
            DMPExceptionHelper::LogException(E->Message.c_str());
            throw DMPException(E->Message.c_str());
        }
            if(zError != NULL)
            {
                delete []zError;
            }
            if (zParms != NULL)
            {
                delete []zParms;
            }
            if (customDLL != NULL)
            {
                delete customDLL;
            }
        delete E;
        E= NULL;
}

int TExtract::CalculateHowManyOfThisRecToGoOut(const int iScriptNum, const int ChecksCount, const int StubsCount, const int DocsCount)
{
  int iResult = 0;
  DMPTRY {
    String TableName;
    bool ContainsChecks = false, ContainsStubs = false, ContainsDocuments = false;
    const TRecordItem* record = &(m_script->GetRecord(iScriptNum));
    for (int i = 0; i < record->GetFieldCount(); ++i) {
      TableName = record->GetField(i).GetTable();
      if (TableName.AnsiCompareIC("Checks") == 0 || TableName.AnsiCompareIC("ChecksDataEntry") == 0)
        ContainsChecks = true;
      else
        if (TableName.AnsiCompareIC("Stubs") == 0 || TableName.AnsiCompareIC("StubsDataEntry") == 0)
          ContainsStubs = true;
        else
          if (TableName.AnsiCompareIC("Documents") == 0 || TableName.AnsiCompareIC("DocumentsDataEntry") == 0)
            ContainsDocuments = true;
      if (ContainsChecks && ContainsStubs && ContainsDocuments)
        break;
    }
    if (ContainsChecks)
      if (ChecksCount > iResult)
        iResult = ChecksCount;
    if (ContainsStubs)
      if (StubsCount > iResult)
        iResult = StubsCount;
    if (ContainsDocuments)
      if (DocsCount > iResult)
        iResult = DocsCount;
  }
  DMPCATCHTHROW("TExtract::CalculateHowManyOfThisRecToGoOut")
  return iResult;
}

void TExtract::DoRecordBefore(const int Type, int &Pos, int &HtmlPos)
{
  DMPTRY {

    m_lineCounter.Reset(Type);

    if (Type == rtFile) {
      // This is the only place where 'RecordCountArray[1]' should be assigned.
      for (int i = 0; i < m_script->GetRecordCount(); ++i) {
        String RecType = m_script->GetRecord(i).GetType();
        if (TRecHelper::GetRecordType(StrToIntDef(RecType, -1)) == rtFile)
          RecordCountArray[rtFile]++;
      }
    }

//    if (FOnRecord)
//      FOnRecord(DMPDataMod->RecBank, DMPDataMod->RecCustomer, DMPDataMod->RecLockbox, DMPDataMod->RecBatch, DMPDataMod->RecItem, DMPDataMod->ProcDateStr.c_str(), DMPDataMod->DepDateStr.c_str());
    if (Type < rtTransactions) {
      m_counterFields.ResetCounters(Type+1);
      TDMPADODataSet* qry = (TDMPADODataSet*) (m_tlDataSetList->Items[Type+1]);
      m_counterFields.SetRecordCount(Type+1, qry->Active ? qry->RecordCount : 0);
    }
    else
      if (Type == rtTransactions) {
        TDMPADODataSet* qry;
        qry = (TDMPADODataSet*) (m_tlDataSetList->Items[rtChecks]);
        m_counterFields.ResetCounters(rtChecks);
        m_counterFields.SetRecordCount(rtChecks, qry->Active ? qry->RecordCount : 0);
        qry = (TDMPADODataSet*) (m_tlDataSetList->Items[rtStubs]);
        m_counterFields.ResetCounters(rtStubs);
        m_counterFields.SetRecordCount(rtStubs, qry->Active ? qry->RecordCount : 0);
        qry = (TDMPADODataSet*) (m_tlDataSetList->Items[rtDocs]);
        m_counterFields.ResetCounters(rtDocs);
        m_counterFields.SetRecordCount(rtDocs, qry->Active ? qry->RecordCount : 0);
        m_counterFields.ResetCounters(rtDetail);
      }
    if (m_iMaxType >= rtDetail && Type < rtStubs)
      GetDetailData(Type, (TDMPADODataSet*)m_tlDataSetList->Items[TRecHelper::GetRecordSubType(Type)], m_detailRecFields, 0);
    if (Type >= rtTransactions && Type < rtDetail)
      m_aggregateFields.SetAggregateValues((TDMPADODataSet*)m_tlDataSetList->Items[Type], Type);
    if (m_bHasAggregateHeaders && Type < rtChecks){
      int iScriptNum = -1;
      m_lineCounter.EnsureCountsForAfterListEmpty(Type);
      while ((iScriptNum = GetNextScript(iScriptNum + 1, Type)) >= 0)
        m_lineCounter.SaveCountsForAfter(Type);
      Pos = m_outputMan.GetFilePos();
      if (m_outputMan.GetIsHtmlTable())
        HtmlPos = m_outputMan.GetHtmlPos();
    }
    else
      PrintRecordsOfType(Type);
  }
  DMPCATCHTHROW("TExtract::DoRecordBefore")
}

void TExtract::DoRecordAfter(const int Type, const int Pos, const int HtmlPos)
{
  DMPTRY {
    PrintRecordsOfType(TRecHelper::SetHeaderTrailer(Type, 1));
    if (m_bHasAggregateHeaders)
      FlipFiles(m_sTempFileName, m_sTempHtmlFileName, Type, Pos, HtmlPos);
    if (Type == 1) {
      DoConfirmationFieldsTally();
      m_extractAuditInfo.SetExtractResults(m_aggregateFields, GetRecordCountSum()); // CR 13806 9/12/2005 DJK
    }
    m_aggregateFields.ClearAggregateValues(Type);
  }
  DMPCATCHTHROW("TExtract::DoRecordAfter")
}

int TExtract::GetNextScript(const int Start, const int Type)
{
  int iResult = -1;
  DMPTRY {
    int i = Start;
    bool Found = false;
    while (i < m_script->GetRecordCount() && !Found){
      int RecType = m_script->GetRecord(i).GetType();
      if (TRecHelper::GetRecordSubType(RecType) == TRecHelper::GetRecordSubType(Type) && TRecHelper::GetHeaderTrailer(RecType) == TRecHelper::GetHeaderTrailer(Type)){
        Found = true;
        iResult = i;
      }
      i++;
    }
  }
  DMPCATCHTHROW("TExtract::GetNextScript")
  return iResult;
}

void TExtract::PrintRecordsOfType(const int Type, const bool bBeingInserted)
{
  DMPTRY {
    int RealType, m_iScriptNum = GetNextScript(0, Type);
    while (m_iScriptNum >= 0){
      RealType = m_script->GetRecord(m_iScriptNum).GetType();
      int iBaseType = TRecHelper::GetRecordSubType(Type);
      if (TRecHelper::GetRecordType(RealType) == rtDetail)
        PrintDetailRecords(m_detailRecFields, 0, m_iScriptNum, iBaseType, bBeingInserted); //CR10943 04/28/2005 ALH
      else // DJK 09/23/2005 - I added this back in - Aaron took it out for CR 10943 with no explanation - it needs to be here!
        PrintRecord((TDMPADODataSet*)m_tlDataSetList->Items[iBaseType], m_iScriptNum, iBaseType, bBeingInserted);
      if (iBaseType > rtFile)
        RecordCountArray[iBaseType]++;
      if (bBeingInserted)
        m_lineCounter.ClearOneCountForAfter(Type);
      m_iLastType = Type;
      m_iScriptNum = GetNextScript(m_iScriptNum + 1, Type);
    }
  }
  DMPCATCHTHROW("TExtract::PrintRecordsOfType")
}


void TExtract::PrintRecord(TDMPADODataSet * Qry, const int Index, const int Type, const bool bBeingInserted)
{
  DMPTRY {
    String Line = "", Data = "", HeaderLine = "<tr>", HtmlLine = "<tr>";
    bool IsData;
    const TRecordItem* record = &(m_script->GetRecord(Index));
    for (int i = 0; i < record->GetFieldCount(); i++) {
      const TRecordFieldItem* field = &(record->GetField(i));
      IsData = true;
      if (m_aggregateFields.IsMember(field->GetFieldName()))
        FormatAggregateData(Type, field, Data);
      else if (m_standardFields.IsMember(field->GetFieldName())){
        IsData = false;
        FormatStandardData(Qry, field, Data, Index, Type, bBeingInserted);
      }
      else if (m_recordCounts.IsMember(field->GetFieldName())){
        IsData = false;
        FormatRecordCountData(field, Data);
      }
      else
        FormatData(Qry, field, Data);
      if (i > 0 && i < record->GetFieldCount())  //GW 8/10/01 was Count-1
        Line += m_script->FieldDelimiter;
      Line += Data;
      if (m_outputMan.GetIsHtmlTable()){
        if (m_script->UseFieldNames){
          if (Type != m_iLastType||m_iScriptNum != m_iLastScriptNum){    //was if (Qry && ...
            if (IsData){
              String Display = field->GetDisplayName();
              Pad(Display, false, ' ', m_script->NoPad ? 0 : field->GetColWidth(), false, false); // CR 6454 1/08/2004 DJK
              HeaderLine += "<td nowrap>" + Display + "</td>";
            }
            else if (!m_script->DisplayDataOnly)
              HeaderLine +="<td nowrap><br></td>";
          }
        }
        if (!m_script->DisplayDataOnly || (m_script->DisplayDataOnly && IsData)){
          if (Data.IsEmpty())
            HtmlLine += "<td nowrap><br></td>";
          else
            HtmlLine += "<td nowrap>" + Data + "</td>";
        }
      }
    }
    m_counterFields.IncrementCounter(Index);
    if (m_script->RecordDelimiter == "\\n")
      Line += "\r\n";
    else if (m_script->RecordDelimiter == "\\n\\n")
      Line += "\r\n\r\n";
    else
      Line += m_script->RecordDelimiter;
    m_iLastScriptNum = m_iScriptNum;
    m_outputMan.WriteFile(Line);
    if (m_outputMan.GetIsHtmlTable()){
      if (HeaderLine.Length()> 4)
        m_outputMan.WriteHtml(HeaderLine + "</tr>");
      m_outputMan.WriteHtml(HtmlLine + "</tr>");
    }
  }
  DMPCATCHTHROW("TExtract::PrintRecord")
}

void TExtract::PrintDetailRecords(const FieldCollection& fieldCollection, const int iRow, const int iScriptNum, const int iType, const bool bBeingInserted) //FullType
{
  DMPTRY {
    String Line = "", Data = "", HeaderLine = "<tr>", HtmlLine = "<tr>";
    bool IsData;
    int Index;
    const TRecordItem* record = &(m_script->GetRecord(iScriptNum));
    for (int i = 0; i < record->GetFieldCount(); i++){
      const TRecordFieldItem* field = &(record->GetField(i));
      IsData = true;
      if (m_aggregateFields.IsMember(field->GetFieldName())) // CR 13806 10/04/2005 - Handle aggregate fields found in detail records
        FormatAggregateData(rtChecks, field, Data);          // CR 13806 10/04/2005 
      else                                                   // CR 13806 10/04/2005
        if (m_standardFields.IsMember(field->GetFieldName())) {
          IsData = false;
          FormatStandardData(NULL, field, Data, iScriptNum, iType, bBeingInserted);
        }
        else {
          Index = fieldCollection.Locate(field->GetFieldName());  //was tmpStr
          if (Index < 0)
            Data = "";
          else
            Data = fieldCollection[Index].GetValue(iRow);
          FormatDetailData(field, Data);
        }
      if (i > 0 && i < record->GetFieldCount())   //GW 8/10/01 was Count-1
        Line += m_script->FieldDelimiter;
      Line += Data;
      if (m_outputMan.GetIsHtmlTable())
        HandleHTMLOuput(IsData, iScriptNum, Data, field, HeaderLine, HtmlLine);
    }
    m_counterFields.IncrementCounter(iScriptNum);
    if (m_script->RecordDelimiter == "\\n")
      Line += "\r\n";
    else if (m_script->RecordDelimiter == "\\n\\n")
      Line += "\r\n\r\n";
    else
      Line += m_script->RecordDelimiter;
    m_outputMan.WriteFile(Line);
    if (m_outputMan.GetIsHtmlTable()){
      if (HeaderLine.Length()> 4)
        m_outputMan.WriteHtml(HeaderLine + "</tr>");
      m_outputMan.WriteHtml(HtmlLine + "</tr>");
    }
  }
  DMPCATCHTHROW("TExtract::PrintDetailRecords")
}

// Variant #2: Same a Variant #1, except:
//               Any 'non-Occurs' fields will have their first, second, etc. rows read (instead of always just their first) until the last row is reached, after which just
//               that last row will be used.
void TExtract::PrintDetailOccursRecords(const FieldCollection& fieldCollection, const int iScriptNum, const int ChecksCountTotal, const int StubsCountTotal, const int DocsCountTotal, int& ChecksCount, int& StubsCount, int& DocsCount)
{
  DMPTRY {
    if (ThisIsAnOccursRecord(iScriptNum)){
      const TRecordItem* record = &(m_script->GetRecord(iScriptNum));
      int iRecordCount = 0;
      int iRecordCountTotal = TRecHelper::GetRepeatLimitValue(m_script->GetRecord(iScriptNum).GetType());
      // Build array of groups ('non-group' fields are considered individual groups) - each group will then be represented by an array or one or more fields
      bool bObligatory = false; // ie - Found fields of one of the three magic types (check, stub, doc) NOT in a group - This means there MUST be at least ONE record, even if not warranted by the 'occurs' rules.
      std::vector<const TRecordFieldItem*> fieldArr;
      std::vector< std::vector<const TRecordFieldItem*> > fieldArrArr;
      int iChecksCountTotalLocal = ChecksCountTotal;
      int iStubsCountTotalLocal  = StubsCountTotal;
      int iDocsCountTotalLocal   = DocsCountTotal;
      std::vector<int> vOccursRatchets;
      vOccursRatchets.push_back(0); // How many check records get chewed up by this record
      vOccursRatchets.push_back(0); // How many stub  records get chewed up by this record
      vOccursRatchets.push_back(0); // How many dec   records get chewed up by this record

      DMPTRY {
        int CurrentGroup = -1;
        for (int i = 0; i < record->GetFieldCount(); i++){
          const TRecordFieldItem* field = &(record->GetField(i));
          int iFieldType = GetDetailType(field->GetTable());
          if (iFieldType != -1) {
            if (field->GetOccursGroup() == -1)
              bObligatory = true;
            else
              vOccursRatchets[iFieldType] = field->GetOccursCount();
          }
          if (field->GetOccursGroup() == -1 || field->GetOccursGroup() != CurrentGroup){
            if (fieldArr.size())
              fieldArrArr.push_back(fieldArr);
            fieldArr.clear();
            CurrentGroup = field->GetOccursGroup();
          }
          fieldArr.push_back(field);
        }
        fieldArrArr.push_back(fieldArr);
        if (vOccursRatchets[0] == 0)
          iChecksCountTotalLocal = 0;
        if (vOccursRatchets[1] == 0)
          iStubsCountTotalLocal = 0;
        if (vOccursRatchets[2] == 0)
          iDocsCountTotalLocal = 0;
      }
      DMPCATCHTHROW("Task: Setup flags and counters")

      m_counterFields.SetRecordCount(rtDetail, 999999); // djk - At this point we don't know (and we won't know until we are on the last one), so for now make count big.
      bool FirstLoop = true;
      bool AllDone = false;
      while (!AllDone) {
        AllDone = (ChecksCount >= iChecksCountTotalLocal && StubsCount >= iStubsCountTotalLocal && DocsCount >= iDocsCountTotalLocal) && !(FirstLoop && bObligatory);
        if (!AllDone)
          AllDone = (iRecordCountTotal != 0 && iRecordCount >= iRecordCountTotal);
        if (!AllDone) {

          DMPTRY {
            bool bIsLastRecordForFunction = iRecordCountTotal != 0 && (iRecordCount+1) >= iRecordCountTotal;
            if (!bIsLastRecordForFunction)
              bIsLastRecordForFunction = (ChecksCount + vOccursRatchets[0]) >= iChecksCountTotalLocal && (StubsCount + vOccursRatchets[1]) >= iStubsCountTotalLocal && (DocsCount + vOccursRatchets[2]) >= iDocsCountTotalLocal;
            if (bIsLastRecordForFunction)
              m_counterFields.SetRecordCountByScriptNum(iScriptNum, iRecordCount+1);
          }
          DMPCATCHTHROW("Task: If will be last record put out during this function call, then set 'm_counterFields' value")

          String Line = "", Data = "", HeaderLine = "<tr>", HtmlLine = "<tr>";
          int iCheckRowsUsed = 0, iStubRowsUsed = 0, iDocRowsUsed = 0;
          for (int i = 0; i < (int) fieldArrArr.size(); i++){
            bool bIsData = true;
            if (m_aggregateFields.IsMember(fieldArrArr[i][0]->GetFieldName())) {  // CR 13806 10/04/2005 - Handle aggregate fields found in detail records
              FormatAggregateData(rtChecks, fieldArrArr[i][0], Data);
              if (i > 0 && i < record->GetFieldCount())   //GW 8/10/01 was Count-1
                Line += m_script->FieldDelimiter;
              Line += Data;
              if (m_outputMan.GetIsHtmlTable())
                HandleHTMLOuput(bIsData, iScriptNum, Data, fieldArrArr[i][0], HeaderLine, HtmlLine);
            }
            else
              if (m_standardFields.IsMember(fieldArrArr[i][0]->GetFieldName())){
                bIsData = false;
                FormatStandardData(NULL, fieldArrArr[i][0], Data, iScriptNum);
                if (i > 0 && i < record->GetFieldCount())   //GW 8/10/01 was Count-1
                  Line += m_script->FieldDelimiter;
                Line += Data;
                if (m_outputMan.GetIsHtmlTable())
                  HandleHTMLOuput(bIsData, iScriptNum, Data, fieldArrArr[i][0], HeaderLine, HtmlLine);
              }
              else{
                int iDetailType = GetDetailType(fieldArrArr[i][0]->GetTable());
                if (iDetailType != -1) {
                  int iCount, *iRowsUsed = 0;
                  const int* CountTotal = 0;
                  switch (iDetailType) {
                    case 0: iCount = ChecksCount;
                            CountTotal = &ChecksCountTotal;
                            iRowsUsed = &iCheckRowsUsed;
                            break;
                    case 1: iCount = StubsCount;
                            CountTotal = &StubsCountTotal;
                            iRowsUsed = &iStubRowsUsed;
                            break;
                    case 2: iCount = DocsCount;
                            CountTotal = &DocsCountTotal;
                            iRowsUsed = &iDocRowsUsed;
                  }
                  int iReps = fieldArrArr[i][0]->GetOccursCount();
                  bool bHasOccurs = true;
                  if (fieldArrArr[i][0]->GetOccursGroup() == -1) {
                    iReps = 1;
                    bHasOccurs = false;
                  }
                  *iRowsUsed = iReps;
                  for (int j = 0; j < iReps; ++j){
                    for (int k = 0; k < (int) fieldArrArr[i].size(); ++k){
                      int iRowToUse = iCount;
                      if (iRowToUse >= *CountTotal)
                        if (bHasOccurs)
                          iRowToUse = -1;
                        else
                          iRowToUse = *CountTotal-1;
                      if (iRowToUse == -1)
                        Data = String::StringOfChar(' ', fieldArrArr[i][k]->GetColWidth());
                      else {
                        Data = fieldCollection[fieldArrArr[i][k]->GetFieldName()].GetValue(iRowToUse);
                        FormatDetailData(fieldArrArr[i][k], Data);
                      }
                      if (i > 0 && i < record->GetFieldCount())   //GW 8/10/01 was Count-1
                        Line += m_script->FieldDelimiter;
                      Line += Data;
                      if (m_outputMan.GetIsHtmlTable())
                        HandleHTMLOuput(bIsData, iScriptNum, Data, fieldArrArr[i][k], HeaderLine, HtmlLine);
                    }
                    iCount++;
                  }
                }
                else{
                  int iIndex = fieldCollection.Locate(fieldArrArr[i][0]->GetFieldName());
                  if (iIndex < 0)
                    Data = "";
                  else
                    Data = fieldCollection[iIndex].GetValue(0);
                  FormatDetailData(fieldArrArr[i][0], Data);
                  if (i > 0 && i < record->GetFieldCount())   //GW 8/10/01 was Count-1
                    Line += m_script->FieldDelimiter;
                  Line += Data;
                  if (m_outputMan.GetIsHtmlTable())
                    HandleHTMLOuput(bIsData, iScriptNum, Data, fieldArrArr[i][0], HeaderLine, HtmlLine);
                }
              }
          }
          // Now increment counters:
          ChecksCount += iCheckRowsUsed;
          StubsCount  += iStubRowsUsed;
          DocsCount   += iDocRowsUsed;

          if (m_script->RecordDelimiter == "\\n")
            Line += "\r\n";
          else if (m_script->RecordDelimiter == "\\n\\n")
            Line += "\r\n\r\n";
          else
            Line += m_script->RecordDelimiter;
          m_outputMan.WriteFile(Line);
          if (m_outputMan.GetIsHtmlTable()){
            if (HeaderLine.Length()> 4)
              m_outputMan.WriteHtml(HeaderLine + "</tr>");
            m_outputMan.WriteHtml(HtmlLine + "</tr>");
          }
          iRecordCount++;
          RecordCountArray[rtDetail]++;
          m_counterFields.IncrementCounter(iScriptNum);
          FirstLoop = false;
        } // If (!AllDone)
      } // While
    }
    else{
      if (ChecksCountTotal || StubsCountTotal || DocsCountTotal) {
        int HowManyRecsOut = CalculateHowManyOfThisRecToGoOut(iScriptNum, ChecksCountTotal, StubsCountTotal, DocsCountTotal);
        m_counterFields.SetRecordCount(rtDetail, HowManyRecsOut);
        for (int i = 0; i < HowManyRecsOut; i++){
          PrintDetailRecords(fieldCollection, i, iScriptNum);  //CR 10943 4/28/2005 ALH
          RecordCountArray[rtDetail]++;
        }
      }
  // This is how it WAS before Friday 3/1/02
  //    TStringList *tmpStr = new TStringList;
  //    for (int i = 0; i < DetailRecs->Count; i++){
  //      tmpStr->Clear();
  //      tmpStr->CommaText = DetailRecs->Strings[i];
  //      PrintDetailRecords(tmpStr, iScriptNum);
  //      RecordCountArray[rtDetail]++;
  //    }
  //    delete tmpStr;

    }
  }
  DMPCATCHTHROW("TExtract::PrintDetailOccursRecords")
}

void TExtract::HandleHTMLOuput(bool IsData, const int iScriptNum, const String& Data, const TRecordFieldItem* field, String& HeaderLine, String& HtmlLine)
{
  DMPTRY {
    if (m_script->UseFieldNames){
      if (rtDetail != m_iLastType || iScriptNum != m_iLastScriptNum) {    //was if (Qry && ...
        if (IsData){
          String Display = field->GetDisplayName();
          Pad(Display, false, ' ', m_script->NoPad ? 0 : field->GetColWidth(), false, false); // CR 6454 1/08/2004 DJK
          HeaderLine += "<td nowrap>" + Display + "</td>";
        }
        else if (!m_script->DisplayDataOnly)
          HeaderLine +="<td nowrap><br></td>";
      }
    }
    if (!m_script->DisplayDataOnly || (m_script->DisplayDataOnly && IsData)){
      if (Data.IsEmpty())
        HtmlLine += "<td nowrap><br></td>";
      else
        HtmlLine += "<td nowrap>" + Data + "</td>";
    }
  }
  DMPCATCHTHROW("TExtract::HandleHTMLOuput (TRecordFieldItem*)")
}

void TExtract::GetParallelDetailData(const int Type, FieldCollection& fieldCollection, const int RecsThisPass)
{
  DMPTRY {
    if (fieldCollection.RowCount() > RecsThisPass)
      GetDetailData(Type, (TDMPADODataSet*)(m_tlDataSetList->Items[Type]), fieldCollection, RecsThisPass);
    else {
      GetDetailData(Type, (TDMPADODataSet*)(m_tlDataSetList->Items[Type]), m_detailRecFields, 0);
      fieldCollection.AddRow(m_detailRecFields, 0);
    }
  }
  DMPCATCHTHROW("TExtract::GetParallelDetailData")
}

//NOTE: Precondition: the list is sorted
void TExtract::GetDetailData(const int Type, TDMPADODataSet *Qry, FieldCollection& fieldCollection, const int iRow)
{
  DMPTRY {
    String sTable = m_script->GetTable(Type);
    if (sTable.IsEmpty())
      return;
    bool Found = false;
    int Start, i = 0;
    while (i < fieldCollection.FieldCount() && !Found){
      if (fieldCollection[i].GetTableName().AnsiCompareIC(sTable) == 0) {
        Found = true;
        Start = i;
      }
      i++;
    }
    if (Found){
      i = Start;
      while (i < fieldCollection.FieldCount() && Found){
        if (fieldCollection[i].GetTableName().AnsiCompareIC(sTable) == 0)
          fieldCollection[i].SetValue(iRow, Qry->FieldByName(fieldCollection[i].GetFieldName())->AsString.Trim());
        else
          Found = false;
        i++;
      }
    }
  }
  DMPCATCHTHROW("TExtract::GetDetailData")
}

void TExtract::ClearDetailField(const int Type)
{
  DMPTRY {
    String sTable = m_script->GetTable(Type);
    if (sTable.IsEmpty())
      return;
    bool Found = false;
    int Start, i = 0;
    while (i < m_detailRecFields.FieldCount() && !Found) {
      if (m_detailRecFields[i].GetTableName().AnsiCompareIC(sTable) == 0) {
        Found = true;
        Start = i;
      }
      i++;
    }
    if (Found){
      i = Start;
      while (i < m_detailRecFields.FieldCount() && Found) {
        if (m_detailRecFields[i].GetTableName().AnsiCompareIC(sTable) == 0)
          m_detailRecFields[i].Clear();
        else
          Found = false;
        i++;
      }
    }
  }
  DMPCATCHTHROW("TExtract::ClearDetailField")
}

void TExtract::GetDetailParams(bool &HasChecks, bool &HasStubs, bool &HasDocs)
{
  DMPTRY {
    String TableName;
    HasChecks = HasStubs = HasDocs = false;
    FieldCollection fieldList;
    m_script->GetSelectedFieldNames(rtDetail, fieldList);
    for (int i = 0; i < fieldList.FieldCount(); i++){
      TableName = fieldList[i].GetTableName();
      if (TableName.AnsiCompareIC("CHECKS") == 0)
        HasChecks = true;
      else if (TableName.AnsiCompareIC("STUBS") == 0)
        HasStubs = true;
      else if (TableName.AnsiCompareIC("DOCUMENTS") == 0)
        HasDocs = true;
      if (HasChecks && HasStubs && HasDocs)
        break;
    }
  }
  DMPCATCHTHROW("TExtract::GetDetailParams")
}

void TExtract::GetAttributes(SAttrib *FieldAttrib, const String& Attrib)
{
  DMPTRY {
    TStringList *AttribList = new TStringList;
  //  AttribList->CommaText = Attrib;
    TextToList(AttribList, Attrib);
    while (AttribList->Count < goFormat)
      AttribList->Add("");
    FieldAttrib->Clear();
    FieldAttrib->FieldName = AttribList->Strings[goFieldName - 1];
    FieldAttrib->DataType = (TFieldType) StrToIntDef(AttribList->Strings[goDataType -1],0);
    FieldAttrib->DisplayName = AttribList->Strings[goFieldDesc -1];
    FieldAttrib->Width = StrToIntDef(AttribList->Strings[goColWidth -1],0);
    String Filler = AttribList->Strings[goPadChar -1];
    if (Filler.UpperCase() == "SP")
      FieldAttrib->FillChar = ' ';
    else if (Filler.Length())
      FieldAttrib->FillChar = Filler[1];
    else
      FieldAttrib->FillChar = ' ';  //GW cannot use NULL; will not display in RichTextEdit
    FieldAttrib->RightJustify = AttribList->Strings[goJustify - 1].UpperCase() == "R";
    FieldAttrib->UseQuotes = !AttribList->Strings[goQuotes -1].Trim().IsEmpty();
    FieldAttrib->Format = AttribList->Strings[goFormat -1];
    if (AttribList->Count >= goOccursGroup)
      if (!AttribList->Strings[goOccursGroup-1].Trim().IsEmpty())
        FieldAttrib->OccursGroup = AttribList->Strings[goOccursGroup-1].ToInt();
    if (AttribList->Count >= goMaxOccurs)
      if (!AttribList->Strings[goMaxOccurs-1].Trim().IsEmpty())
        FieldAttrib->OccursCount = AttribList->Strings[goMaxOccurs-1].ToInt();
    delete AttribList;
  }
  DMPCATCHTHROW("TExtract::GetAttributes")
}

void TExtract::FormatAggregateData(const int Level, SAttrib *FieldAttrib, String &Data) // djkdjk - slated for removal.
{
  DMPTRY {
    Data = "";
    bool bIsNumeric = false; // CR 6454 1/08/2004 DJK
    switch (FieldAttrib->DataType){
      case ftSmallint:
      case ftInteger :
      case ftWord    :
      case ftFloat   :
      case ftCurrency:
      case ftBCD     :
      case ftLargeint: bIsNumeric = true;
    }
    double Value = m_aggregateFields.GetAggregateData(FieldAttrib->FieldName, Level);
    switch (FieldAttrib->DataType){
      case ftString:
      case ftInteger: if (FieldAttrib->Format.IsEmpty())
                        Data = IntToStr((int)Value);
                      else
                        DoFormatAlpha(FieldAttrib->Format, IntToStr((int)Value), Data);
                      break;
      case ftFloat:
      case ftCurrency:
      case ftBCD:     if (FieldAttrib->Format.IsEmpty())
                        DoFormatFloat("0.00", Value, Data);
                      else
                        DoFormatFloat(FieldAttrib->Format, Value, Data);
                      break;
    }
    Pad(Data, bIsNumeric, FieldAttrib->FillChar, m_script->NoPad?0:FieldAttrib->Width, FieldAttrib->RightJustify, m_script->UseQuotes||FieldAttrib->UseQuotes); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatAggregateData (SAttrib*)")
}

void TExtract::FormatAggregateData(const int Level, const TRecordFieldItem* field, String &Data)
{
  DMPTRY {
    Data = "";
    bool bIsNumeric = false; // CR 6454 1/08/2004 DJK
    switch (field->GetDataType()){
      case ftSmallint:
      case ftInteger :
      case ftWord    :
      case ftFloat   :
      case ftCurrency:
      case ftBCD     :
      case ftLargeint: bIsNumeric = true;
    }
    double Value = m_aggregateFields.GetAggregateData(field->GetFieldName(), Level);
    switch (field->GetDataType()) {
      case ftString:
      case ftInteger: if (field->GetFormat().IsEmpty())
                        Data = IntToStr((int)Value);
                      else
                        DoFormatAlpha(field->GetFormat(), IntToStr((int)Value), Data);
                      break;
      case ftFloat:
      case ftCurrency:
      case ftBCD:     if (field->GetFormat().IsEmpty())
                        DoFormatFloat("0.00", Value, Data);
                      else
                        DoFormatFloat(field->GetFormat(), Value, Data);
                      break;
    }
    Pad(Data, bIsNumeric, field->GetPadChar(), m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes()); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatAggregateData (TRecordFieldItem*)")
}

int TExtract::GetRecordCountSum()
{
  int iResult = 0;
  DMPTRY {
    for (int i = 0; i <= rtDetail; i++)
      if (i != rtFile) // djk - 1/23/02 - These are totalled separately below:
        iResult += RecordCountArray[i];
    // Now add in the sum of all File-level header and footer records  // djk - 12/18/01
    for (int i = 0; i < m_script->GetRecordCount(); ++i)
      if (TRecHelper::GetRecordType(m_script->GetRecord(i).GetType()) == rtFile)
        iResult++;
  }
  DMPCATCHTHROW("TExtract::GetRecordCountSum")
  return iResult;
}

void TExtract::FormatRecordCountData(SAttrib *FieldAttrib, String &Data) //djkdjk - Due for termination
{
  DMPTRY {
    int Value, Type = m_recordCounts.GetType(FieldAttrib->FieldName);
    switch (Type){
      case -1: Value = 0; break;
      case 0: Value = GetRecordCountSum(); break;
      default: Value = RecordCountArray[Type];
    }
    if (FieldAttrib->Format.IsEmpty())
      Data = IntToStr(Value);
    else
      DoFormatAlpha(FieldAttrib->Format, IntToStr(Value), Data);
    Pad(Data, false, FieldAttrib->FillChar, m_script->NoPad?0:FieldAttrib->Width, FieldAttrib->RightJustify, m_script->UseQuotes||FieldAttrib->UseQuotes); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatRecordCountData (SAttrib*)")
}

void TExtract::FormatRecordCountData(const TRecordFieldItem* field, String &Data)
{
  DMPTRY {
    int Value, Type = m_recordCounts.GetType(field->GetFieldName());
    switch (Type){
      case -1: Value = 0; break;
      case 0: Value = GetRecordCountSum(); break;
      default: Value = RecordCountArray[Type];
    }
    if (field->GetFormat().IsEmpty())
      Data = IntToStr(Value);
    else
      DoFormatAlpha(field->GetFormat(), IntToStr(Value), Data);
    Pad(Data, false, field->GetPadChar(), m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes()); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatRecordCountData (TRecordFieldItem*)")
}

void TExtract::FormatStandardData(TDMPADODataSet* Qry, const TRecordFieldItem* field, String &Data, const int iScriptNum, const int iType, const bool bBeingInserted)
{
  DMPTRY
  {
    //  Data = m_standardFields.GetDefaultValue(FieldAttrib->FieldName);
    if (field->GetFieldName().AnsiCompareIC("FILLER") == 0)
      Data = Data.StringOfChar(field->GetPadChar(), field->GetColWidth());
    else if (field->GetFieldName().AnsiCompareIC("STATIC") == 0)
    {
      Data = field->GetFormat();
      if (Data.Pos("@"))
        m_inputList.SubstituteParameters(Data, false);
    }
    else if (field->GetFieldName().AnsiCompareIC("COUNTER") == 0)
      Data = String(m_counterFields.GetCounter(iScriptNum));
    else if (field->GetFieldName().AnsiCompareIC("FIRSTLAST") == 0)
      Data = m_counterFields.GetFirstLastTag(iScriptNum);
    else if (field->GetFieldName().AnsiCompareIC("DATE") == 0)
    {
      if (field->GetFormat().IsEmpty())
        Data = Date().FormatString("mm/dd/yy");
      else
        DoFormatDateTime(field->GetFormat(), Date(), Data);
    }
    else if (field->GetFieldName().AnsiCompareIC("TIME") == 0)
    {
      if (field->GetFormat().IsEmpty())
        Data = Time().FormatString("hh:nn:ss");
      else
        DoFormatDateTime(field->GetFormat(), Time(), Data);
    }
    else if (field->GetFieldName().AnsiCompareIC("CURRENTPROCESSINGDATE") == 0)
    {
      if (field->GetFormat().IsEmpty())
        Data = m_dtCurrentProcDate.FormatString("mm/dd/yy");
      else
        DoFormatDateTime(field->GetFormat(), m_dtCurrentProcDate, Data);
    }
    else if (field->GetFieldName().AnsiCompareIC("PROCESSINGRUNDATE") == 0)
    {
      if (field->GetFormat().IsEmpty())
        Data = m_dtProcRunDate.FormatString("mm/dd/yy");
      else
        DoFormatDateTime(field->GetFormat(), m_dtProcRunDate, Data);
    }
    else if (ImageFileManager::IsImageFileField(field->GetFieldName()) && m_bExtractImages)
    {
       // CR9916 10/27/2004 ALH  CR10943 5/6/2005 ALH   // CR 5815 03/03/2004 DJK - Modified the contents of this 'if' block.
       Data = GetImageData(Qry, field, Data, iScriptNum, iType);  //CR 10943 5/4/2005 ALH
    }
    else if (ImageFileManager::IsImageFileField(field->GetFieldName()) && !m_bExtractImages)
    {
        Data = "Non Image Extract";
    }
    else if (field->GetFieldName().AnsiCompareIC("BATCHTOTALDOCSCOUNT") == 0) // CR 10943 5/10/2005 ALH
    {
           Data = GetCountOfChecksAndDocsForBatch();
    }
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERFILE") == 0)
      Data = String(m_lineCounter.GetCount(rtFile, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERBANK") == 0)
      Data = String(m_lineCounter.GetCount(rtBank, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERCUSTOMER") == 0)
      Data = String(m_lineCounter.GetCount(rtCustomer, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERLOCKBOX") == 0)
      Data = String(m_lineCounter.GetCount(rtLockbox, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERBATCH") == 0)
      Data = String(m_lineCounter.GetCount(rtBatch, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERTRANSACTION") == 0)
      Data = String(m_lineCounter.GetCount(rtTransactions, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERCHECK") == 0)
      Data = String(m_lineCounter.GetCount(rtChecks, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERSTUB") == 0)
      Data = String(m_lineCounter.GetCount(rtStubs, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("LINECOUNTERDOCUMENT") == 0)
      Data = String(m_lineCounter.GetCount(rtDocs, bBeingInserted ? iType : 0));
    else if (field->GetFieldName().AnsiCompareIC("JOINTDETAILSEQUENCE") == 0)  // CR 12916  6/27/2005 ALH
      Data = IncrementJointDetailSequence();  
    if(field->GetFieldName().UpperCase().Pos("IMAGE") && m_bExtractImages)  // CR9916 11/10/2004 ALH
    {
        // CR#13656 10/18/05 MSV - If embedding images, always left-justify (to make some later code easier)
        //                          Incidentally, we're fixing a bug here that padded image file names
        //                          with extra space for tags even if embedding was not used.
        if (!(m_script->EmbedImages))       
            Pad(Data, false, field->GetPadChar(), m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes());
        else if ((!field->GetFieldName().UpperCase().Pos("SIZE")) && field->GetFieldName().UpperCase().Pos("CHECK") || field->GetFieldName().UpperCase().Pos("DOCUMENT") || field->GetFieldName().UpperCase().Pos("STUB"))
            Pad(Data, false, field->GetPadChar(), m_script->NoPad ? 0 : (field->GetColWidth() + TAGSIZE), false, m_script->UseQuotes || field->GetUseQuotes());   // add the tag size to the pad, so that we don't truncate
        else
            Pad(Data, false, field->GetPadChar(), m_script->NoPad ? 0 : (field->GetColWidth()+ IMGTAGSIZE), false, m_script->UseQuotes || field->GetUseQuotes()); // add the tag size to the pad, so that we don't truncate
    }
    else
    {
        Pad(Data, false, field->GetPadChar(), m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes()); // CR 6454 1/08/2004 DJK
    }
  }
  DMPCATCHTHROW("TExtract::FormatStandardData (TRecordFieldItem*)")
}

// CR12916   6/27/2005 ALH - new Function
String TExtract::IncrementJointDetailSequence()
{
        String sData = "";
        DMPTRY
        {
                m_lJointDetailSequenceNumb ++;
                sData.sprintf("%d",m_lJointDetailSequenceNumb);
        }
        DMPCATCHTHROW("TExtract::IncrementJointDetailSequence()")
        return sData;
}

// CR 10943 5/3/2005 ALH
String TExtract::GetImageData(TDMPADODataSet* Qry, const TRecordFieldItem* field, String Data, const int iScriptNum, const int iType)
{
        DMPTRY
        {
                int iSeq   = 0;
                String sFD = "C";  // default to check.
                int iImgInfoType =  ReturnTypeOfImageInformation(field->GetFieldName());
                TLocateOptions Opts;
               // CR 16503 10/03/2006 ALH  - modified each case to pull sequence number and file descriptor from the
                // vectors if the Qry object is NULL.
                if (DMPDataMod)
                {
                    ImageFieldInfo ImgInfo;
                    switch (iImgInfoType)
                    {
                            case 0:  //IMAGECHECKSINGLEFRONT
                                DMPTRY
                                {
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtChecks);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_FRONT
                                                                            );

                                    VerifyData(Data,field->GetColWidth());
                                    ImgInfo.ImageFileName   = Data;
                                    ImgInfo.ColumnSize      = field->GetColWidth();
                                    ImgInfo.PadChar         = field->GetPadChar();
                                    ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                    m_vIMGlist->AddImage(ImgInfo);
                                }
                                DMPCATCHTHROW("TASK: Get single front check image information.")
                                break;
                            case 1:  //IMAGECHECKSINGLEREAR
                                DMPTRY
                                {
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtChecks);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_BACK
                                                                            );


                                    VerifyData(Data,field->GetColWidth());
                                    ImgInfo.ImageFileName   = Data;
                                    ImgInfo.ColumnSize      = field->GetColWidth();
                                    ImgInfo.PadChar         = field->GetPadChar();
                                    ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                    m_vIMGlist->AddImage(ImgInfo);
                                }
                                DMPCATCHTHROW("TASK: Get single rear check image information.")
                                break;
                            case 2: //IMAGECHECKPERBATCH
                            case 3: // IMAGECHECKPERTRANSACTION
                                DMPTRY
                                {
                                     if (Qry == NULL)
                                     {
                                              Qry  = GetDataSetForImgsAtJointDetail(rtChecks);
                                              sFD  = m_vDocumentFileDescriptors[0];
                                              iSeq = m_vBatSeqList[0];
                                     }
                                     else
                                     {
                                               if (Qry->FindField("FileDescriptor"))
                                               {
                                                    sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                               }
                                               iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                     }
                                     if ((field->GetFieldName().UpperCase().Pos("IMAGECHECK") > 0)
                                       || (field->GetFieldName().UpperCase().Pos("IMAGEDOCUMENT") == 0))
                                       {
                                                m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                                Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                        TDateTime(DMPDataMod->ProcDateStr),
                                                                                        DMPDataMod->RecBank,
                                                                                        DMPDataMod->RecLockbox,
                                                                                        DMPDataMod->RecBatch,
                                                                                        DMPDataMod->RecBatchTypeCode,
                                                                                        Qry->FieldByName("TransactionID")->AsInteger,
                                                                                        iSeq,
                                                                                        sFD
                                                                                        );
                                                VerifyData(Data,field->GetColWidth());
                                                ImgInfo.ImageFileName   = Data;
                                                ImgInfo.ColumnSize      = field->GetColWidth();
                                                ImgInfo.PadChar         = field->GetPadChar();
                                                ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                                m_vIMGlist->AddImage(ImgInfo);
                                       }
                                       else
                                       {
                                                Data                    = "";
                                                VerifyData(Data,field->GetColWidth());
                                                ImgInfo.ImageFileName   = Data;
                                                ImgInfo.ColumnSize      = field->GetColWidth();
                                                ImgInfo.PadChar         = field->GetPadChar();
                                                ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                                m_vIMGlist->AddImage(ImgInfo);
                                       }
                                }
                                DMPCATCHTHROW("TASK: Get check image information per transaction or per batch.")
                                break;
                            case 4: //IMAGESTUBSINGLEFRONT
                                DMPTRY
                                {
                                           if (Qry == NULL)
                                           {
                                                    Qry  = GetDataSetForImgsAtJointDetail(rtStubs);
                                                    sFD  = m_vDocumentFileDescriptors[0];
                                                    iSeq = m_vBatSeqList[0];
                                           }
                                           else
                                           {
                                                     if (Qry->FindField("FileDescriptor"))
                                                     {
                                                          sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                                     }
                                                     iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                           }
                                           m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                           Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                    TDateTime(DMPDataMod->ProcDateStr),
                                                                                    DMPDataMod->RecBank,
                                                                                    DMPDataMod->RecLockbox,
                                                                                    DMPDataMod->RecBatch,
                                                                                    DMPDataMod->RecBatchTypeCode,
                                                                                    Qry->FieldByName("TransactionID")->AsInteger,
                                                                                    iSeq,
                                                                                    sFD,
                                                                                    PICS_FRONT
                                                                                    );
                                          VerifyData(Data,field->GetColWidth());
                                          ImgInfo.ImageFileName   = Data;
                                          ImgInfo.ColumnSize      = field->GetColWidth();
                                          ImgInfo.PadChar         = field->GetPadChar();
                                          ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                          m_vIMGlist->AddImage(ImgInfo);

                                }
                                DMPCATCHTHROW("TASK: Get single front stub image information.")
                                break;
                            case 5: //IMAGESTUBSINGLEREAR
                                DMPTRY
                                {
                                         if (Qry == NULL)
                                         {
                                                  Qry  = GetDataSetForImgsAtJointDetail(rtStubs);
                                                  sFD  = m_vDocumentFileDescriptors[0];
                                                  iSeq = m_vBatSeqList[0];
                                         }
                                         else
                                         {
                                                   if (Qry->FindField("FileDescriptor"))
                                                   {
                                                        sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                                   }
                                                   iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                         }
                                         m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                         Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                  TDateTime(DMPDataMod->ProcDateStr),
                                                                                  DMPDataMod->RecBank,
                                                                                  DMPDataMod->RecLockbox,
                                                                                  DMPDataMod->RecBatch,
                                                                                  DMPDataMod->RecBatchTypeCode,
                                                                                  Qry->FieldByName("TransactionID")->AsInteger,
                                                                                  iSeq,
                                                                                  sFD,
                                                                                  PICS_BACK
                                                                                  );

                                            VerifyData(Data,field->GetColWidth());
                                            ImgInfo.ImageFileName   = Data;
                                            ImgInfo.ColumnSize      = field->GetColWidth();
                                            ImgInfo.PadChar         = field->GetPadChar();
                                            ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                            m_vIMGlist->AddImage(ImgInfo);
                                }
                                DMPCATCHTHROW("TASK: Get single rear stub image information.")
                                break;
                            case 6: //IMAGEDOCUMENTSINGLEFRONT
                                DMPTRY
                                {
                                    if (Qry == NULL)
                                    {
                                              Qry  = GetDataSetForImgsAtJointDetail(rtDocs);
                                              sFD  = m_vDocumentFileDescriptors[0];
                                              iSeq = m_vBatSeqList[0];
                                    }
                                    else
                                    {
                                               if (Qry->FindField("FileDescriptor"))
                                               {
                                                    sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                               }
                                               iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                    }
                                    if (sFD != "S")
                                    {
                                         m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                         Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                  TDateTime(DMPDataMod->ProcDateStr),
                                                                                  DMPDataMod->RecBank,
                                                                                  DMPDataMod->RecLockbox,
                                                                                  DMPDataMod->RecBatch,
                                                                                  DMPDataMod->RecBatchTypeCode,
                                                                                  Qry->FieldByName("TransactionID")->AsInteger,
                                                                                  iSeq,
                                                                                  sFD,
                                                                                  PICS_FRONT
                                                                                  );
                                          VerifyData(Data,field->GetColWidth());
                                          ImgInfo.ImageFileName   = Data;
                                          ImgInfo.ColumnSize      = field->GetColWidth();
                                          ImgInfo.PadChar         = field->GetPadChar();
                                          ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                          m_vIMGlist->AddImage(ImgInfo);
                                    }
                                    else
                                    {
                                        Data = NULL;
                                    }
                                }
                                DMPCATCHTHROW("TASK: Get single front Document image information.")
                                break;
                            case 7: //IMAGEDOCUMENTSINGLEREAR
                                DMPTRY
                                {
                                     if (Qry == NULL)
                                     {
                                              Qry  = GetDataSetForImgsAtJointDetail(rtDocs);
                                              sFD  = m_vDocumentFileDescriptors[0];
                                              iSeq = m_vBatSeqList[0];
                                     }
                                     else
                                     {
                                               if (Qry->FindField("FileDescriptor"))
                                               {
                                                    sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                               }
                                               iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                     }
                                     if (sFD != "S")
                                    {
                                           m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                           Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                    TDateTime(DMPDataMod->ProcDateStr),
                                                                                    DMPDataMod->RecBank,
                                                                                    DMPDataMod->RecLockbox,
                                                                                    DMPDataMod->RecBatch,
                                                                                    DMPDataMod->RecBatchTypeCode,
                                                                                    Qry->FieldByName("TransactionID")->AsInteger,
                                                                                    iSeq,
                                                                                    sFD,
                                                                                    PICS_BACK
                                                                                    );
                                           VerifyData(Data,field->GetColWidth());
                                           ImgInfo.ImageFileName   = Data;
                                           ImgInfo.ColumnSize      = field->GetColWidth();
                                           ImgInfo.PadChar         = field->GetPadChar();
                                           ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                           m_vIMGlist->AddImage(ImgInfo);
                                    }
                                    else
                                    {
                                        Data = NULL;
                                    }
                                }
                                DMPCATCHTHROW("TASK: Get single rear document image information.")
                                break;
                            case 8: //IMAGEDOCUMENTPERBATCH
                            case 9: //IMAGEDOCUMENTPERTRANSACTION
                                DMPTRY
                                {
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtDocs);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD
                                                                            );
                                    VerifyData(Data,field->GetColWidth());
                                    ImgInfo.ImageFileName   = Data;
                                    ImgInfo.ColumnSize      = field->GetColWidth();
                                    ImgInfo.PadChar         = field->GetPadChar();
                                    ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                    m_vIMGlist->AddImage(ImgInfo);
                                }
                                DMPCATCHTHROW("TASK: Get document image information per transaction or per batch..")
                                break;
                            case 10: //IMAGEFILESIZEFRONTC
                               DMPTRY
                               {
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtChecks);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_FRONT
                                                                            );
                              }
                              DMPCATCHTHROW("TASK: Get single front check image size information.")
                                break;
                            case 11: //IMAGEFILESIZEREARC
                                DMPTRY
                                {
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtChecks);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_BACK
                                                                            );
                                }
                                DMPCATCHTHROW("TASK: Get single rear check image size information.")
                                break;
                            case 12: //IMAGEFILESIZEFRONTS
                                DMPTRY
                                {
                                   sFD = "S";
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtStubs);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_FRONT
                                                                            );

                                }
                                DMPCATCHTHROW("TASK: Get single front stub image size information.")
                                break;
                            case 13: //IMAGEFILESIZEREARS
                                DMPTRY
                                {
                                   sFD = "S";
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtStubs);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_BACK
                                                                            );
                                }
                                DMPCATCHTHROW("TASK: Get single rear stub image size information.")
                                break;
                            case 14: //IMAGEFILESIZEFRONTD
                                DMPTRY
                                {
                                   sFD = "UT";
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtDocs);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_FRONT
                                                                            );

                                }
                                DMPCATCHTHROW("TASK: Get single front document image size information.")
                                break;
                            case 15: //IMAGEFILESIZEREARD
                                DMPTRY
                                {
                                   sFD = "UT";
                                   if (Qry == NULL)
                                   {
                                            Qry  = GetDataSetForImgsAtJointDetail(rtDocs);
                                            sFD  = m_vDocumentFileDescriptors[0];
                                            iSeq = m_vBatSeqList[0];
                                   }
                                   else
                                   {
                                             if (Qry->FindField("FileDescriptor"))
                                             {
                                                  sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                             }
                                             iSeq = Qry->FieldByName("BatchSequence")->AsInteger;
                                   }
                                   m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                   Data = m_imageFileManager.ReturnImageSize(field->GetFieldName(),
                                                                            TDateTime(DMPDataMod->ProcDateStr),
                                                                            DMPDataMod->RecBank,
                                                                            DMPDataMod->RecLockbox,
                                                                            DMPDataMod->RecBatch,
                                                                            DMPDataMod->RecBatchTypeCode,
                                                                            Qry->FieldByName("TransactionID")->AsInteger,
                                                                            iSeq,
                                                                            sFD,
                                                                            PICS_BACK
                                                                            );
                                }
                                DMPCATCHTHROW("TASK: Get single rear document image size information.")
                                break;
                            case 16: //ANY Other Imagefield
                                DMPTRY
                                {
                                     if(Qry != NULL)
                                    {
                                          if (Qry->FindField("FileDescriptor"))
                                          {
                                              sFD = Qry->FieldByName("FileDescriptor")->AsString.Trim();
                                          }
                                          m_imageFileManager.ImgFieldColSize = field->GetColWidth();
                                          Data = m_imageFileManager.StoreImageRequestAndReturnFileName(field->GetFieldName(),
                                                                                             TDateTime(DMPDataMod->ProcDateStr),
                                                                                             DMPDataMod->RecBank,
                                                                                             DMPDataMod->RecLockbox,
                                                                                             DMPDataMod->RecBatch,
                                                                                             DMPDataMod->RecBatchTypeCode,
                                                                                             Qry->FieldByName("TransactionID")->AsInteger,
                                                                                             Qry->FieldByName("BatchSequence")->AsInteger,
                                                                                             sFD
                                                                                            );

                                          VerifyData(Data,field->GetColWidth());
                                          ImgInfo.ImageFileName   = Data;
                                          ImgInfo.ColumnSize      = field->GetColWidth();
                                          ImgInfo.PadChar         = field->GetPadChar();
                                          ImgInfo.JustifyChar     = field->GetRightJustify()==true ? 'R' : 'L';
                                          m_vIMGlist->AddImage(ImgInfo);
                                    }
                                    else
                                    {
                                          Data = "";
                                    }
                                }
                                DMPCATCHTHROW("TASK: Get other image information.")
                                break;
                            default: // bad field def, field is classified as img field, but not actually img field.
                                    throw DMPException("Illegal case, a valid image field or image related field was not found.");
                    }
                }
                else
                {
                        DoFormatAlpha(field->GetFormat(), Data, Data);
                }
                if (Data.Length())
                  DoFormatAlpha(field->GetFormat(), Data, Data);
        }
        DMPCATCHTHROW("TExtract::GetImageData")
        return Data;
}

int TExtract::ReturnTypeOfImageInformation(String sScriptFieldName)
{
        int iType = -1;
        DMPTRY
        {
            if(sScriptFieldName.AnsiCompareIC("IMAGECHECKSINGLEFRONT") == 0)
            {
                 iType = 0;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGECHECKSINGLEREAR") == 0)
            {
                 iType = 1;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGECHECKPERBATCH") == 0)
            {
                 iType = 2;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGECHECKPERTRANSACTION") == 0)
            {
                 iType = 3;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGESTUBSINGLEFRONT") == 0)
            {
                 iType = 4;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGESTUBSINGLEREAR") == 0)
            {
                 iType = 5;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEDOCUMENTSINGLEFRONT") == 0)
            {
                 iType = 6;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEDOCUMENTSINGLEREAR") == 0)
            {
                 iType = 7;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEDOCUMENTPERBATCH") == 0)
            {
                 iType = 8;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEDOCUMENTPERTRANSACTION") == 0)
            {
                 iType = 9;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEFRONTC") == 0)
            {
                 iType = 10;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEREARC") == 0)
            {
                 iType = 11;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEFRONTS") == 0)
            {
                 iType = 12;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEREARS") == 0)
            {
                 iType = 13;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEFRONTD") == 0)
            {
                 iType = 14;
            }
            else if(sScriptFieldName.AnsiCompareIC("IMAGEFILESIZEREARD") == 0)
            {
                 iType = 15;
            }
            else
            {
                 if (sScriptFieldName.UpperCase().Pos("IMAGE") > 0)
                 {
                        iType = 16;
                 }
            }
        }
        DMPCATCHTHROW("TExtract::ReturnTypeOfImageInformation")
        return iType;
}     

// CR 10943 4/29/2005 ALH - New  function
TDMPADODataSet* TExtract::GetDataSetForImgsAtJointDetail(int iRecordType)
{
        TDMPADODataSet *QryDs;
        DMPTRY
        {
             QryDs = (TDMPADODataSet*) (m_tlDataSetList->Items[iRecordType]);
        }
        DMPCATCHTHROW("TExtract::GetDataSetForImgsAtJointDetail()")
        return QryDs;
}

void TExtract::VerifyData(String &Data, int iFieldSize)
{
        DMPTRY
        {
                if((Data == NULL || (Data == "")) && (m_imageFileManager.m_imageFormat == ImageFileItem::ifTIF2))
                {
                      String sImageNameSize = "";
                      sImageNameSize.sprintf("%05d",iFieldSize);
                      Data = EMBEDTAG + sImageNameSize;
                }
        }
        DMPCATCHTHROW("TExtract::VerifyData")
}

// CR 14003 9/17/2005 DJK - Added this function
String TExtract::GetExtractAuditIdGUID() const
{
  String sResult;
  DMPTRY {
    sResult = m_extractAuditInfo.GetExtractAuditIdGUID();
  }
  DMPCATCHTHROW("TExtract::GetExtractAuditIdGUID")
  return sResult;
}

// CR 10943 5/10/2005  ALH
// CR 14062 9/22/2005  ALH
String TExtract::GetCountOfChecksAndDocsForBatch()
{
        String Data = "";
        DMPException *E = new DMPException();
        TDMPADODataSet* BatchSet        =  GetDataSetForImgsAtJointDetail(rtBatch);
        DMPTRY
        {
                int iGBI = 0;
                DMPTRY
                {
                        iGBI =  BatchSet->FieldByName("GlobalBatchID")->AsInteger;
                }
                DMPCATCHTHROW("TASK: Get GlobalBatchID from Batch Data Set.")
                DMPADOStoredProc sp("proc_EW_Sel_ChecksStubsDocuments_GetCountOfDocumentsForBatch", 1, DMPDataMod->DMPConnection); // CR 14003 9/17/2005 DJK - Incremented version number to '2'
                DMPTRY
                {
                        sp.AddParameter("@parmGlobalBatchID" , iGBI);
                        sp.AddParameter("@parmTotalDocsOut" , (int) 0 , pdOutput);
                }
                DMPCATCHTHROW("TASK: Supply stored procedure parameters.")
                DMPTRY
                {
                        sp.ExecProc();
                }
                DMPCATCHTHROW("TASK: Execute proc_EW_GetCountOfDocumentsForBatch stored procedure.")

                int iDocs = sp.GetParamByName("@parmTotalDocsOut");

                Data.sprintf("%d",iDocs);
        }
        DMPCATCH_GET("TExtract::GetCountOfChecksAndDocsForBatch()", *E)
        if(!E->IsEmpty())
        {
            throw DMPException(E->Message);
        }
        delete E;
        E = NULL;
        return Data;
}

void TExtract::FormatData(TDMPADODataSet *Qry, const TRecordFieldItem* field, String &Data)
{
  DMPTRY {
    double Val;
    bool bIsNumeric = false; // CR 6454 1/08/2004 DJK
    switch (field->GetDataType()){
      case ftSmallint:
      case ftInteger :
      case ftWord    :
      case ftFloat   :
      case ftCurrency:
      case ftBCD     :
      case ftLargeint: bIsNumeric = true;
    }
    bool IsNull = Qry->FieldByName(field->GetFieldName())->IsNull;
    if (field->GetFormat().IsEmpty())
      Data = Qry->FieldByName(field->GetFieldName())->AsString.Trim();
    else {
      switch (field->GetDataType()) {
        case ftString:
        case ftInteger:  DoFormatAlpha(field->GetFormat(), Qry->FieldByName(field->GetFieldName())->AsString.Trim(), Data);
                         break;
        case ftFloat:
        case ftCurrency:
        case ftBCD:      if (IsNull && m_script->NullAsSpace)
                           Data = "";
                         else {
                           Val = Qry->FieldByName(field->GetFieldName())->AsFloat;
                           if (field->GetFormat().IsEmpty())      //GW 7/20/01
                             DoFormatFloat("0.00", Val, Data);
                           else
                             DoFormatFloat(field->GetFormat(), Val, Data);
                         } break;
        case ftDate:
        case ftTime:
        case ftDateTime: if (IsNull  && m_script->NullAsSpace)
                           Data = "";
                         else
                           DoFormatDateTime(field->GetFormat(), Qry->FieldByName(field->GetFieldName())->AsDateTime, Data);
                         break;
      }
    }
    char FillChar;
    if (m_script->NullAsSpace && IsNull)
      FillChar ='\x20';
    else
      FillChar = field->GetPadChar();
    Pad(Data, bIsNumeric, FillChar, m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes()); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatData (TRecordFieldItem*)")
}

void TExtract::FormatDetailData(const TRecordFieldItem* field, String &Data) 
{
  DMPTRY {
    double Val;
    bool bIsNumeric = false; // CR 6454 1/08/2004 DJK
    switch (field->GetDataType()){
      case ftSmallint:
      case ftInteger :
      case ftWord    :
      case ftFloat   :
      case ftCurrency:
      case ftBCD     :
      case ftLargeint: bIsNumeric = true;
    }
    bool IsNull = Data.IsEmpty();
    if (!field->GetFormat().IsEmpty()){
      switch (field->GetDataType()) {
        case ftString:
        case ftInteger:  DoFormatAlpha(field->GetFormat(), Data, Data);
                         break;
        case ftFloat:
        case ftCurrency:
        case ftBCD:      if (IsNull && m_script->NullAsSpace)
                           Data = "";
                         else {
                            Val = (Data.IsEmpty() ? 0 : StrToFloat(Data));
                            if (field->GetFormat().IsEmpty())     //GW 7/20/01
                              DoFormatFloat("0.00", Val, Data);
                            else
                              DoFormatFloat(field->GetFormat(), Val, Data);
                         } break;
        case ftDate:
        case ftTime:
        case ftDateTime: if (IsNull && m_script->NullAsSpace)
                           Data = "";
                         else {
                           if (Data.IsEmpty())
                             Data = "0";
                           else
                             DoFormatDateTime(field->GetFormat(), TDateTime(Data), Data);
                         } break;
      }
    }
    char FillChar;
    if (m_script->NullAsSpace && IsNull)
      FillChar = '\x20';
    else
      FillChar = field->GetPadChar();
    Pad(Data, bIsNumeric, FillChar, m_script->NoPad ? 0 : field->GetColWidth(), field->GetRightJustify(), m_script->UseQuotes || field->GetUseQuotes()); // CR 6454 1/08/2004 DJK
  }
  DMPCATCHTHROW("TExtract::FormatDetailData (TRecordFieldItem*)")
}

void TExtract::DoTest(const String& Setup, const String& RecordName)
{
  DMPTRY {
    // CR 16217 06/12/2006 DJK - Switch from old logging dll to standard logging class:
    // m_logFile32Dll = new LogFile32Loader("LogFile32.Dll");  // CR#14581 12/20/05 MSV
    // m_logFile32Dll->LoadDll();                              // CR#14581 12/20/05 MSV

    bool CanContinue = true;
    int Index, Type;
    if (!m_frmTestExtractForm)
      m_frmTestExtractForm = new TTestExtractForm(NULL);
    String ResultFile = ExtractFilePath(Setup) + "Temp.cxr";
    m_sScriptFile = Setup;     //Do NOT call cleanup - it will delete the script file
    m_script = new TScript(m_sScriptFile);
    m_outputMan.SetIsHtmlTable(false);
    Index = m_script->LocateRecord(RecordName);
    if (Index >= 0)
      Type = m_script->GetRecord(Index).GetType();
    else
      CanContinue = false;
    Type = TRecHelper::GetRecordType(Type);
    CanContinue = CanContinue && (Type >= 0 && Type <= rtDetail);
    if (!CanContinue)
      ShowMessage("Unable to locate record");
    else {
      OutputManager outputBack = m_outputMan;

      m_outputMan.SetTargetFile(ResultFile);
      DMPTRY {
        m_outputMan.OpenExtractFiles(false);
      }
      DMPCATCHTHROW("Task: Create temporary extract file")
      if (CanContinue){
//djk 9/15/02        SetStandardFields();
        String Table = m_script->GetTable(Type, false);
        String sRootTable = m_script->GetTable(Type, true);
        if (Table.IsEmpty()) {
          m_frmTestExtractForm->SQLMemo->Clear();
          if (Type == rtDetail)
            TestDetailRecord(Index);
          else
            PrintRecord(NULL, Index, Type);
        }
        else {
          DMPTRY {
            m_frmTestExtractForm->TestDataSet->Close();
            m_frmTestExtractForm->TestDataSet->CommandText = "";
            String Key = GetKey(Type);
            LoadDataSet(Table,sRootTable,Key, m_frmTestExtractForm->TestDataSet, Type, false); // CR 12104 1/25/2005 DJK - Altered function call to conform to new form
//djkdjk            m_frmTestExtractForm->SQLMemo->Lines->Assign(m_frmTestExtractForm->TestQry->SQL);
            m_frmTestExtractForm->SQLMemo->Text = m_frmTestExtractForm->TestDataSet->CommandText;
            m_frmTestExtractForm->TestDataSet->CommandText = "";
            LoadDataSet(Table, sRootTable, Key, m_frmTestExtractForm->TestDataSet, Type, true); // CR 12104 1/25/2005 DJK - Altered function call to conform to new form
            m_frmTestExtractForm->TestDataSet->Open();
            for (int i = 0; i < 5 && !m_frmTestExtractForm->TestDataSet->Eof; i++, m_frmTestExtractForm->TestDataSet->Next())
              PrintRecord(m_frmTestExtractForm->TestDataSet, Index, Type);
            m_frmTestExtractForm->TestDataSet->First();
          }
          DMPCATCHTHROW("Task: Open test query")
        }
      }
      m_outputMan.CloseExtractFiles();

      m_outputMan = outputBack;
    }
    if (CanContinue){
      m_frmTestExtractForm->TestMemo->Lines->LoadFromFile(ResultFile);
      m_frmTestExtractForm->Show();
    }
    if (FileExists(ResultFile))
      DeleteFile(ResultFile);
    FImagePath = "";

    // CR 16217 06/12/2006 DJK - Switch from old logging dll to standard logging class:
    // if (m_logFile32Dll) {       // CR#14581 12/20/05 MSV
    //   delete m_logFile32Dll;    // CR#14581 12/20/05 MSV
    //   m_logFile32Dll = 0;       // CR#14581 12/20/05 MSV
    // }
  }
  DMPCATCHTHROW("TExtract::DoTest")
}

// CR 5272 1/07/2004 DJK - Altered to ignore fields not 'accompanied' by table names.
void TExtract::TestDetailRecord(const int Index)
{
  DMPTRY {
    SAttrib Attrib;
    FillDetailFields();
    const TRecordItem* record = &(m_script->GetRecord(Index));
    for (int i = 0; i < record->GetFieldCount(); i++) {
      const TRecordFieldItem* field = &(record->GetField(i));
      int iFieldNum = -1;
      if (field->GetFieldName().Pos("."))
        iFieldNum = m_detailRecFields.Locate(field->GetFieldName());
      if (iFieldNum >= 0){
        String Value;
        switch (field->GetDataType()) {
          case ftString:   Value = "Data"; break;
          case ftInteger:  Value = "999"; break;
          case ftFloat:
          case ftCurrency:
          case ftBCD:      Value = "123.45"; break;
          case ftDate:     Value = "07/28/01"; break;
          case ftTime:     Value = "13:07:22"; break;
          case ftDateTime: Value = "07/28/01 13:07:22";break;
        }
        m_detailRecFields[iFieldNum].SetValue(0, Value);
      }
    }
    PrintDetailRecords(m_detailRecFields, 0, Index); //CR10943 4/28/2005 ALH
  }
  DMPCATCHTHROW("TExtract::TestDetailRecord")
}

String TExtract::GetKey(const int Type)
{
  String sResult;
  DMPTRY {
    switch (Type){
      case rtBank: sResult = BANK_KEY; break;
      case rtCustomer: sResult = CUSTOMER_KEY; break;
      case rtLockbox: sResult = LOCKBOX_KEY; break;
      case rtBatch: sResult = BATCH_KEY; break;
      case rtTransactions: sResult = TRANSACTION_KEY; break;
      case rtChecks: sResult = CHECK_KEY; break;
      case rtStubs: sResult = STUB_KEY; break;
      case rtDocs: sResult = DOCUMENT_KEY; break;
    }
  }
  DMPCATCHTHROW("TExtract::GetKey")
  return sResult;
}


TDataSource* TExtract::GetDataSource(const int Type)
{
  TDataSource* dsResult = 0;
  DMPTRY {
    switch (Type){
      case rtBank: dsResult = DMPDataMod->BankDS; break;
      case rtCustomer: dsResult = DMPDataMod->CustomerDS; break;
      case rtLockbox: dsResult = DMPDataMod->LockboxDS; break;
      case rtBatch: dsResult = DMPDataMod->BatchDS; break;
      case rtTransactions: dsResult = DMPDataMod->TranDS; break;
      case rtChecks: dsResult = DMPDataMod->ChecksDS; break;
      case rtStubs: dsResult = DMPDataMod->StubsDS; break;
      case rtDocs: dsResult = DMPDataMod->DocsDS; break;
    }
  }
  DMPCATCHTHROW("TExtract::GetDataSource")
  return dsResult;
}

void TExtract::RemoveTestForm()
{
  DMPTRY {
    if (m_frmTestExtractForm)
      delete m_frmTestExtractForm;
  }
  DMPCATCHTHROW("TExtract::RemoveTestForm")
}

//File handling

void TExtract::DoFinalStep()
{
  DMPTRY {
    m_outputMan.AdjustLastLineFeed(m_script->RecordDelimiter);
  }
  DMPCATCHTHROW("TExtract::DoFinalStep")
}

void TExtract::DoConfirmationFieldsTally()
{
  DMPTRY {
    SAttrib Attrib;
    int iCount = m_confirmationFields.GetCount();
    for (int i = 0; i < iCount; i++){
      GetAttributes(&Attrib, m_confirmationFields.GetFieldListString(i));
      String sData;
      if (m_aggregateFields.IsMember(Attrib.FieldName))
        FormatAggregateData(1, &Attrib, sData);
      else
        if (m_recordCounts.IsMember(Attrib.FieldName))
          FormatRecordCountData(&Attrib, sData);
      if (!sData.IsEmpty())
        m_confirmationFields.SetData(i, sData);
    }
  }
  DMPCATCHTHROW("TExtract::DoConfirmationFieldsTally")
}

int TExtract::GetDetailType(const String sTableName)
{
  int iResult = -1;
  DMPTRY {
    if (sTableName.AnsiCompareIC("Checks") == 0)
      iResult = 0;
    else
      if (sTableName.AnsiCompareIC("Stubs") == 0)
        iResult = 1;
      else
        if (sTableName.AnsiCompareIC("Documents") == 0)
          iResult = 2;
  }
  DMPCATCHTHROW("TExtract::GetDetailType")
  return iResult;
}

void TExtract::SendTargetFileNameViaThreadMessages(int iCallerThreadID, const String& sTargetFile)
{
  DMPTRY {
    unsigned int ulProxyMessageID;

    DMPTRY {
      String sReturnThreadMessage = "WM_DMP_EXTRACTRUNTOCENDS_NEWEXTRACTNAME"; // This MUST match the name used in DeliveryServerService.exe
      ulProxyMessageID = ::RegisterWindowMessage(sReturnThreadMessage.c_str());
      if (ulProxyMessageID == 0)
        throw DMPException("Could not register custom message '"+sReturnThreadMessage+"'");
    }
    DMPCATCHTHROW("Task: Establish message ID")

    DMPTRY {
      int iMessageCount = sTargetFile.Length()/6;
      if ((sTargetFile.Length() % 6) != 0 || iMessageCount == 0)
        iMessageCount++;
      DMPTRY {
        int iWParam = 0;
        int iLParam = iMessageCount;
        ::PostThreadMessage(iCallerThreadID, ulProxyMessageID, iWParam, iLParam);
      }
      DMPCATCHTHROW("Task: First, send message count")
      char* zBuff = new char[iMessageCount*6];
      memset(zBuff, 0, iMessageCount*6);
      strcpy(zBuff, sTargetFile.c_str());
      int iMessCntr = 0;
      while (iMessCntr < iMessageCount) {
        char* zPtr = zBuff + 6*iMessCntr;
        ++iMessCntr;
        DMPTRY {
          int iWParam = (iMessCntr << 16) | (((int) zPtr[0]) << 8) | ((int) zPtr[1]);
          int iLParam = (((int) zPtr[2]) << 24) | (((int) zPtr[3]) << 16) | (((int) zPtr[4]) << 8) | ((int) zPtr[5]);
          ::PostThreadMessage(iCallerThreadID, ulProxyMessageID, iWParam, iLParam);
        }
        DMPCATCHTHROW("Task: Encode data into two 32-bit pieces, send back to calling app")
      }
      delete[] zBuff;
    }
    DMPCATCHTHROW("Task: Send back string in a series of messages (6 chars per message)")
  }
  DMPCATCHTHROW("TExtract::SendTargetFileNameViaThreadMessages")
}

// CR 5272 1/07/2004 DJK
//     This function looks for any 'Checks', 'Stubs' or 'Documents' fields and determines
//     if they are in fact from the corresponding 'ChecksDataEntry', 'StubsDataEntry', or
//     'DocumentsDataEntry' tables. If so, then the corresponding table name is altered
//     accordingly. This is required since in the extract script files, and internally, 
//     the fields for the 'Checks' and 'ChecksDataEntry' tables are all grouped together
//     uder the 'Chekcks' heading (same for stubs and documents). Since the queries now 
//     require that all fields be prefixed with table names, this function must be called
//     before the SQL statements are assigned to the various query components.
// CR 12104 3/30/2005 DJK
//     I altered this function so that we can mix fields from the Checks, Stubs, and Documents
//     tables together in the fieldList. This became a new requirement due to the new Stubs
//     SQL, which grabs the 'BatchSequence' field from the Documents table.
void TExtract::EnsureCorrectDataEntryTableNames(FieldCollection& fieldList)
{
  DMPTRY {
    TStringList* slFields;
    TStringList* slFieldsChecks = 0;
    TStringList* slFieldsStubs  = 0;
    TStringList* slFieldsDocs   = 0;
    for (int i = 0; i < fieldList.FieldCount(); ++i) {
      slFields = 0;
      if (fieldList[i].GetTableName().AnsiCompareIC("Checks") == 0) {
        if (slFieldsChecks == 0) {
          slFieldsChecks = new TStringList();
          DMPDataMod->GetFieldNames("ChecksDataEntry", slFieldsChecks);
        }
        slFields = slFieldsChecks;
      }
      else
        if (fieldList[i].GetTableName().AnsiCompareIC("Stubs") == 0) {
          if (slFieldsStubs == 0) {
            slFieldsStubs = new TStringList();
            DMPDataMod->GetFieldNames("StubsDataEntry", slFieldsStubs);
          }
          slFields = slFieldsStubs;
        }
        else
          if (fieldList[i].GetTableName().AnsiCompareIC("Documents") == 0) {
            if (slFieldsDocs == 0) {
              slFieldsDocs = new TStringList();
              DMPDataMod->GetFieldNames("DocumentsDataEntry", slFieldsDocs);
            }
            slFields = slFieldsDocs;
          }
      if (slFields) {
        String sField = fieldList[i].GetFieldName();
        for (int j = 0; j < slFields->Count; ++j)
          if (slFields->Strings[j].AnsiCompareIC(sField) == 0) {
            fieldList[i].AlterTableNameToDataEntry();
            break;
          }
      }
    }
    if (slFieldsChecks)
      delete slFieldsChecks;
    if (slFieldsStubs)
      delete slFieldsStubs;
    if (slFieldsDocs)
      delete slFieldsDocs;
  }
  DMPCATCHTHROW("TExtract::EnsureCorrectDataEntryTableNames")
}

void TExtract::FlipFiles(const String& FileName, const String& HtmlFileName, const int Type, const int Pos, const int HtmlPos)
{
  DMPTRY {
    TFileStream *tmpFile = new TFileStream(FileName, fmCreate);
    TFileStream *tmpHtmlHeader = 0;

    m_outputMan.FlipFiles1(tmpFile, tmpHtmlHeader, Pos, HtmlPos, m_sTempHtmlFileName);
    PrintRecordsOfType(Type, true); // This is being inserted as if it were the first record of its category (header)
    m_outputMan.FlipFiles2(tmpFile, tmpHtmlHeader);

    if (tmpHtmlHeader)
      delete tmpHtmlHeader;
    delete tmpFile;
  }
  DMPCATCHTHROW("TExtract::FlipFiles")
}

String TExtract::GetTemporaryPath()
{
  String sResult;
  DMPTRY {
    char buf[MAXPATH] = {0};
    if (!GetTempPath(sizeof(buf), buf)){
      LogMessage("Unable to locate temporary directory.");
      sResult = ExtractFileDir(m_outputMan.GetTargetFile());
    }
    else
      sResult = String(buf);
  }
  DMPCATCHTHROW("TExtract::GetTemporaryPath")
  return sResult;
}

void TExtract::Cleanup(bool Success)
{
  DMPTRY {
    m_extractAuditInfo.FlushResultsToDatabase(DMPDataMod->DMPConnection); // CR 13806 9/12/2005 DJK
    //CR 19345 03/16/2007 ALH BEGIN
    m_outputMan.CloseExtractFiles();
    if (!m_script->sPostProcessingDLL.IsEmpty()) {
        RunPostProcessingDLL();
    }
    //CR 19345 03/16/2007 ALH END
    CloseQueries();
    m_outputMan.CloseExtractFiles();
    if (m_batchTrace)
      delete m_batchTrace;
    if (FileExists(m_outputMan.GetTargetFile() + ".tmp" ))   //Temporary HTML table
      DeleteFile(m_outputMan.GetTargetFile() + ".tmp");
    if (FileExists(m_sTempFileName))
      DeleteFile(m_sTempFileName);
    if (FileExists(m_sTempHtmlFileName))
      DeleteFile(m_sTempHtmlFileName);
    if (FileExists(m_sScriptFile))
      DeleteFile(m_sScriptFile);
    String LogData = Success?"Success ":"Failure ";
    LogData += "writing extract to " + m_outputMan.GetTargetFile() + ".\r\n\r\n";
    // m_logFile32Dll->WriteLog(LogData.c_str()); // CR 16217 06/12/2006 DJK
    WriteToLogFile(LogData.c_str()); // CR 16217 06/12/2006 DJK
    m_outputMan.SetTargetFile("");
    FImagePath = "";
    if (FOnFinish)
      FOnFinish(Success, m_confirmationFields.ReturnData().c_str());
  }
  DMPCATCHTHROW("TExtract::Cleanup")
}

void TExtract::LogMessage(const String& Msg)
{
  DMPTRY {
    if (FOnError)
      FOnError(Msg.c_str());
    // m_logFile32Dll->WriteLog(Msg.c_str()); // CR 16217 06/12/2006 DJK
    WriteToLogFile(Msg.c_str()); // CR 16217 06/12/2006 DJK
  }
  DMPCATCHTHROW("TExtract::LogMessage")
}

void TExtract::WriteToLogFile(const String& Msg)
{
  DMPTRY {
    if (m_logFile)
      m_logFile->AddLine(Msg.c_str());
  }
  DMPCATCHTHROW("TExtract::WriteToLogFile")
}



