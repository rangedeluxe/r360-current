//---------------------------------------------------------------------------

#ifndef ImageFileNamingClassesH
#define ImageFileNamingClassesH
//---------------------------------------------------------------------------

#include <ExtCtrls.hpp>
#include <vector>


class TCaretBlinkTimer : public TThread
{
public:
  typedef String __fastcall (__closure *TCaretBlinkTimerEvent)(int iTicsSinceLastCall);
private:
  int m_iLastTickCount;
  int m_iTickCount;
  TCaretBlinkTimerEvent m_caretBlinkTimerEvent;
  String m_sError;
public:
  TCaretBlinkTimer(TCaretBlinkTimerEvent caretBlinkTimerEvent) : TThread(true) {m_caretBlinkTimerEvent = caretBlinkTimerEvent;}
private:
  void __fastcall Execute();
  void __fastcall CallCaretBlinkTimerEvent();
};

class TTokenEdit;
class DMPException;

class TCaretManager
{
private:
  TCaretBlinkTimer* m_threadBlinkTimer;
  TTokenEdit*       m_tokenEditPtr;
  DMPException*     m_dmpExBlinker;
  int               m_iTicsCounter;
  int               m_iBlinkPeriod;
  bool              m_bCaretOn;
  bool              m_bHasFocus;
  int               m_iCaretPos;
  TColor            m_clBackgroundColor;
public:
  TCaretManager(TTokenEdit* tokenEditPtr, TColor clBackgroundColor);
  ~TCaretManager();
  void GotFocus(bool bFocus);
  void SetPosition(int iCaretPos);
  int  GetPosition() const {return m_iCaretPos;}
  void SetBackgroundColor(TColor clBackgroundColor) {m_clBackgroundColor = clBackgroundColor;}
private:
  String __fastcall ReactToBlinkTimerEvent(int iTicsSinceLastCall);
  void DrawCaret(bool bOn);
  void EraseCaret();
  void ResetBlink();
};

class EditToken
{
public:
  enum EditTokenType {ettNone, ettText, ettBank, ettLockbox, ettBatch, ettBatchType, ettProcDate, ettTransaction, ettDocumentType, ettFileCounter};
private:
  EditTokenType m_editTokenType;
  String        m_sTextString;
  TTokenEdit*   m_tokenEditPtr;
  static bool m_bSuspendIllegalCharacterCheck;
public:
  EditToken() {m_editTokenType = ettNone; m_tokenEditPtr = 0;}
  EditToken(const EditToken& obj) {*this = obj;}
  EditToken(TTokenEdit* tokenEditPtr, EditTokenType editTokenType) {m_tokenEditPtr = tokenEditPtr; m_editTokenType = editTokenType; if (m_editTokenType == ettText) m_sTextString = "_";}
  EditToken(TTokenEdit* tokenEditPtr, const String& sTextString);
  EditToken& operator = (const EditToken& obj) {m_editTokenType = obj.m_editTokenType; m_sTextString = obj.m_sTextString; m_tokenEditPtr = obj.m_tokenEditPtr; return *this;}
  int  Draw(TCanvas* canvas, const TRect& rect) const;
  int  GetWidth(TCanvas* canvas) const;
  int  GetWidth(TCanvas* canvas, const int iCharIdx) const;
  int  GetCharIndex(TCanvas* canvas, const int iPixelsIn) const;
  void InsertString(const String& sText, const int iInsertionPoint);
  void DeleteChar(const int iDeletionPoint, bool bDontAllowToGoEmpty);
  void BackspaceChar(const int iBackspacePoint, bool bDontAllowToGoEmpty);
  void Merge(const EditToken& obj);
  EditToken Divide(const int iDivisionPoint);
  bool IsText () const {return (m_editTokenType == ettText);}
  int  GetTextLength() const {return m_sTextString.Length();}
  String GetTextInternal() {return m_sTextString;}
  EditTokenType GetTokenType() const {return m_editTokenType;}
  String EncodeToString() const;
  String AsDisplayed() const; // CR 14584 06/01/2006 DJK
  static EditToken DecodeString(TTokenEdit* tokenEditPtr, String& sEncodedString);
  static bool IsLegalFileNameCharacter(const char cChar);
  static void SuspendIllegalCharacterCheck() {m_bSuspendIllegalCharacterCheck = true;}
private:
  String GetText() const;
  void PrepareCanvas(TCanvas* canvas) const;
};

class TokenIndex
{
private:
  int  m_iIdx;
  bool m_bFront;
  bool m_bBack;
  bool m_bIsText;  // This is true only for token of type 'ettText'
  int  m_iTextPos; // This applies only for token of type 'ettText'
public:
  TokenIndex() {m_iIdx = m_iTextPos = -1; m_bFront = m_bBack = m_bIsText = true;}
  TokenIndex(const TokenIndex& obj) {*this = obj;}
  TokenIndex(int iIdx, bool bFront, bool bBack, bool bIsText, int iTextPos);
  TokenIndex& operator = (const TokenIndex& obj) {m_iIdx = obj.m_iIdx; m_bFront = obj.m_bFront; m_bBack = obj.m_bBack; m_bIsText = obj.m_bIsText; m_iTextPos = obj.m_iTextPos; return *this;}
  TokenIndex& operator++();    // Prefix  operator
  TokenIndex  operator++(int); // Postfix operator
  TokenIndex& operator--();    // Prefix  operator
  TokenIndex  operator--(int); // Postfix operator
  int  GetIdx    () const {return m_iIdx    ;}
  bool GetFront  () const {return m_bFront  ;}
  bool GetBack   () const {return m_bBack   ;}
  int  GetTextPos() const {return m_iTextPos;}
};

class TTokenManager
{
public:
  enum ImageFileType {iftNone, iftSingle, iftPerTran, iftPerBatch};
private:
  enum TravelDirection {tdNone, tdLeft, tdRight};
  std::vector<EditToken> m_vTokens;
  TTokenEdit* m_tokenEditPtr;
public:
  TTokenManager(TTokenEdit* tokenEditPtr);
  void   Paint(TCanvas* canvas, const TRect& rect) const;
  int    DetermineEndPosition      (TCanvas* canvas                       ) const;
  int    DetermineMoveLeftPosition (TCanvas* canvas, const int iCurrentPos) const;
  int    DetermineMoveRightPosition(TCanvas* canvas, const int iCurrentPos) const;
  int    DetermineClosestPosition  (TCanvas* canvas, const int iCurrentPos) const;
  int    InsertString              (TCanvas* canvas, const int iCurrentPos, const String& sText);
  int    InsertToken               (TCanvas* canvas, const int iCurrentPos, const EditToken::EditTokenType editTokenType);
  int    Backspace                 (TCanvas* canvas, const int iCurrentPos);
  void   Delete                    (TCanvas* canvas, const int iCurrentPos);
  int    Clear();
  String EncodeTokenString() const;
  void   DecodeTokenString(const String& sTokens);
  String GetDisplayString() const; // CR 14584 06/01/2006 DJK

  static String           ValidateFileNameTemplate                 (const String& sFileName, const bool bCombineProcDates, const ImageFileType imageFileType);
  static std::vector<int> FetchEssentialTokenTypesMissingFromString(const String& sFileName, const bool bCombineProcDates, const ImageFileType imageFileType);
  static std::vector<int> FetchValidTokenTypeList                  (const bool bCombineProcDates, const ImageFileType imageFileType);
  static std::vector<int> FetchEssentialTokenTypeList              (const bool bCombineProcDates, const ImageFileType imageFileType);
  static String           GenerateDefaultEncodedFileName           (const bool bCombineProcDates, const ImageFileType imageFileType);

void SingToMe();

private:
  TokenIndex CaretPosToIndex(TCanvas* canvas, const int iCurrentPos, const TravelDirection travelDirection = tdNone) const;
  int        IndexToCaretPos(TCanvas* canvas, const TokenIndex& tokenIndex) const;
  static String                 EncodeTokenStringStatic(const std::vector<EditToken>& vTokens);
public:
  static std::vector<EditToken> DecodeTokenStringStatic(TTokenEdit* tokenEditPtr, const String& sTokens);
};

class TTokenEdit : public TCustomControl
{
friend TCaretManager;
private:
  TCaretManager* m_caretManager;
  TTokenManager* m_tokenManager;
  TWndMethod     m_wndMethodOriginal;
  int     m_iMinDrawX;
  int     m_iMaxDrawX;
  int     m_iOffsetDrawX;
  String  m_sDateFormat;
  bool    m_bZeroPadded;
  bool    m_bBatchStatAbbreviated;
  bool    FVisibleBorder;
  TColor  FBackgroundColor;
public:
  __fastcall TTokenEdit(Classes::TComponent* AOwner);
  __fastcall ~TTokenEdit();
  void Clear();
  void InsertToken(const EditToken::EditTokenType editTokenType);
  void InsertText(const String& sText);
  int    GetMinDrawX            () const {return m_iMinDrawX            ;}
  int    GetOffsetDrawX         () const {return m_iOffsetDrawX         ;}
  String GetDateFormat          () const {return m_sDateFormat          ;}
  bool   GetZeroPadded          () const {return m_bZeroPadded          ;}
  bool   GetBatchStatAbbreviated() const {return m_bBatchStatAbbreviated;}
  static String ValidateDateFormatString(const String& sFormat);
private:
  void __fastcall HandleWindowsMessage(Messages::TMessage &Message);
  void __fastcall Paint();
  void DrawFrame();
  bool AdjustXOffsetIfNecessary(); // Returns 'true' if an offset adjustment was necessary, resulting in a repaint.
  void SetVisibleBorder       (const bool     bVal);
  void SetBackgroundColor     (const TColor  clVal);
  void SetDateFormat          (const String&  sVal);
  void SetZeroPadded          (const bool     bVal);
  void SetBatchStatAbbreviated(const bool     bVal);
  String GetEncodedString() const;
  void   SetEncodedString(const String& sEncoded);
  String GetDisplayString() const; // CR 14584 06/01/2006 DJK
public:
  __property bool   VisibleBorder       = {read=FVisibleBorder  , write=SetVisibleBorder       };
  __property TColor BackgroundColor     = {read=FBackgroundColor, write=SetBackgroundColor     };
  __property String ProcDateFormat      = {                       write=SetDateFormat          };
  __property bool   ZeroPad             = {                       write=SetZeroPadded          };
  __property bool   BatchTypeFormatFull = {                       write=SetBatchStatAbbreviated};
  __property String EncodedString       = {read=GetEncodedString, write=SetEncodedString       };
  __property String DisplayString       = {read=GetDisplayString                               }; // CR 14584 06/01/2006 DJK
};


#endif

