//---------------------------------------------------------------------------

#ifndef ParamsH
#define ParamsH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "AdvGrid.hpp"
#include <Grids.hpp>
#include <Dialogs.hpp>
#include <ExtCtrls.hpp>
//---------------------------------------------------------------------------
class TParameterDlg : public TForm
{
__published:	// IDE-managed Components
        TLabel *Label1;
        TEdit *ParamFileEdit;
        TButton *OKBtn;
        TButton *CancelBtn;
        TButton *BrowseBtn;
        TOpenDialog *OpenDialog;
        TLabel *Label3;
        TPanel *GridPanel;
        TLabel *Label2;
        TAdvStringGrid *ParamGrid;
        void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
        void __fastcall BrowseBtnClick(TObject *Sender);
private:	// User declarations
        TStringList *ParameterList;
        String *ParamFilePtr;
        void __fastcall ShowEdit(bool DoShow);
public:		// User declarations
        __fastcall TParameterDlg(TComponent* Owner);
        TModalResult __fastcall Execute(TStringList *Params,
                String &ParamFile, bool ShowFileEdit = true);
};
//---------------------------------------------------------------------------
extern PACKAGE TParameterDlg *ParameterDlg;
//---------------------------------------------------------------------------
#endif
