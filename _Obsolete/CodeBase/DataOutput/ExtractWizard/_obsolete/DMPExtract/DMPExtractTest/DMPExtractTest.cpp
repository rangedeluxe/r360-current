#include "windows.h"
#include <stdio.h>
#import "ExtractAPI.tlb" named_guids

int main(int argc, char* argv[])
{
    printf("Starting...\n");

    HRESULT hRes = S_OK;
    CoInitialize(NULL);
    ExtractAPI::IMyManagedInterface *pManagedInterface = NULL;

    hRes = CoCreateInstance(ExtractAPI::CLSID_cExtract, NULL, CLSCTX_INPROC_SERVER, 
    ExtractAPI::IID_IMyManagedInterface, reinterpret_cast<void**> (&pManagedInterface));

    if (S_OK == hRes)
    {
        printf("Initializing Parms...\n");

        _bstr_t bstrSetupFile(L"C:\\Checkbox\\Extracts\\joel_extract_9.cxs");
        _bstr_t bstrTraceField(""); 
        _bstr_t bstrRerunList("");
        _bstr_t bstrTargetFile(L"C:\\Checkbox\\Data\\Extract_Results\\joel_extract_9.txt");
        _bstr_t bstrImageFile(L"C:\\Checkbox\\Data\\Extract_Results");
        _bstr_t bstrBankID = SysAllocString(L"7000");
        _bstr_t bstrCustomerID = SysAllocString(L"100");
        _bstr_t bstrLockboxID = SysAllocString(L"104");
        _bstr_t bstrBatchID = SysAllocString(L"13025");
        _bstr_t bstrProcessingDateString = SysAllocString(L"10/13/2005");
        _bstr_t bstrDepositDateString = SysAllocString(L"10/18/2005");
        _bstr_t bstrOperatorID = SysAllocString(L"studboy");
        int intExtractSequenceNumber = 1;

        BSTR ExtractAuditID;
        BSTR LastErrorMessage;

        printf("Running Extract...\n");

        long retVal =0;
        hRes = pManagedInterface->raw_RunExtract(bstrSetupFile, 
                                                 bstrTraceField, 
                                                 bstrRerunList, 
                                                 bstrTargetFile, 
                                                 bstrImageFile, 
                                                 bstrBankID, 
                                                 bstrCustomerID, 
                                                 bstrLockboxID, 
                                                 bstrBatchID, 
                                                 bstrProcessingDateString, 
                                                 bstrDepositDateString, 
                                                 bstrOperatorID,
                                                 intExtractSequenceNumber,
                                                 &ExtractAuditID,
                                                 &LastErrorMessage,
                                                 &retVal);

        _bstr_t bstrExtractAuditID = ExtractAuditID;
        _bstr_t bstrLastErrorMessage = LastErrorMessage;


        printf("\nThe value returned by the dll is %ld\n",retVal);

        printf("\nThe Extract Audit ID returned by the dll is %s\n",(LPCTSTR)bstrExtractAuditID);

        printf("\nThe Extract Audit ID returned by the dll is %s\n",bstrExtractAuditID);

        SysFreeString(ExtractAuditID);
        SysFreeString(LastErrorMessage);

        pManagedInterface->Release();
    }

    CoUninitialize();

    printf("Finished...");

    return 0;
}
