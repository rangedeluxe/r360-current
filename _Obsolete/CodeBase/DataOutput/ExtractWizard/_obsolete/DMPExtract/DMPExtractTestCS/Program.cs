using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;

namespace DMPExtractTestCS
{
    class Program
    {

        [DllImport("DMPExtract.dll")]
        public static extern string GetExtractAuditID(IntPtr Ptr);

        [DllImport("DMPExtract.dll")]
        public static extern int SetExtractParams(string TargetFile, 
                                                  string ImageFile, 
                                                  string Bank, 
                                                  string Cust, 
                                                  string Box, 
                                                  string Batch, 
                                                  string DateStr, 
                                                  string DepStr, 
                                                  string OperatorID, 
                                                  int ExtractSequenceNumber, 
                                                  int Handle, 
                                                  int CallerThreadID); // this is the C++ function exposed by the dll
   
        [DllImport("DMPExtract.dll")]
        public static extern int RunExtract(string SetupFile, 
                                            string TraceField, 
                                            string RerunList); // this is the C++ function exposed by the dll

        [DllImport("DMPExtract.dll")]
        public static extern int TestExtract(string SetupFile, 
                                             string Record); // this is the C++ function exposed by the dll

        [DllImport("DMPExtract.dll")]
        public static extern int GetExtractAuditGUID( StringBuilder zGUID, int iLen);

        [DllImport("DMPExtract.dll")]
        public static extern void GetLastErrorMessage( StringBuilder zError, int iLen);

        static void Main(string[] args) {

            string strExtractPath = string.Empty;
            string strTargetFile = string.Empty;
            string strImageFile = string.Empty;
            string strBank = string.Empty;
            string strCustomer = string.Empty;
            string strLockbox = string.Empty;
            string strBatch = string.Empty;
            string strProcessingDate = string.Empty;
            string strDepositDate = string.Empty;
            string strOperator = "Console";

            foreach(string arg in args) {
                if(arg.StartsWith("extractpath=") && arg.Length > ("extractpath=").Length) {
                    strExtractPath = arg.Substring(("extractpath=").Length);
                } else if(arg.StartsWith("targetfile=") && arg.Length > ("targetfile=").Length) {
                    strTargetFile = arg.Substring(("targetfile=").Length);
                } else if(arg.StartsWith("imagefile=") && arg.Length > ("imagefile=").Length) {
                    strImageFile = arg.Substring(("imagefile=").Length);
                } else if(arg.StartsWith("bank=") && arg.Length > ("bank=").Length) {
                    strBank = arg.Substring(("bank=").Length);
                } else if(arg.StartsWith("customer=") && arg.Length > ("customer=").Length) {
                    strCustomer = arg.Substring(("customer=").Length);
                } else if(arg.StartsWith("lockbox=") && arg.Length > ("lockbox=").Length) {
                    strLockbox = arg.Substring(("lockbox=").Length);
                } else if(arg.StartsWith("batch=") && arg.Length > ("batch=").Length) {
                    strBatch = arg.Substring(("batch=").Length);
                } else if(arg.StartsWith("procdate=") && arg.Length > ("procdate=").Length) {
                    strProcessingDate = arg.Substring(("procdate=").Length);
                } else if(arg.StartsWith("depdate=") && arg.Length > ("depdate=").Length) {
                    strDepositDate = arg.Substring(("depdate=").Length);
                } else if(arg.StartsWith("operator=") && arg.Length > ("operator=").Length) {
                    strOperator = arg.Substring(("operator=").Length);
                }
            }

            if(strExtractPath.Length == 0||strTargetFile.Length == 0||strImageFile.Length ==0||(strProcessingDate.Length==0 &&strDepositDate.Length==0)) {
                Usage();
            } else {
                _RunExtract(strExtractPath,
                            strTargetFile, 
                            strImageFile, 
                            strBank, 
                            strCustomer, 
                            strLockbox, 
                            strBatch, 
                            strProcessingDate, 
                            strDepositDate,
                            strOperator);
            }
        }

        private static void _RunExtract(string ExtractPath,
                                        string TargetFile,
                                        string ImageFile,
                                        string Bank,
                                        string Customer,
                                        string Lockbox,
                                        string Batch,
                                        string ProcessingDate,
                                        string DepositDate,
                                        string Operator) {

            string strGuid = string.Empty;
            string strLastErrorMsg = string.Empty;

            SetExtractParams(TargetFile, 
                             ImageFile, 
                             Bank, 
                             Customer, 
                             Lockbox, 
                             Batch, 
                             ProcessingDate, 
                             DepositDate, 
                             Operator, 
                             0, 
                             0, 
                             0);
            
            RunExtract(ExtractPath, "", "");
            
            int intRetVal = GetExtractAuditID(ref strGuid);
            GetLastErrorMsg(ref strLastErrorMsg);

            Console.WriteLine("\nRetVal: " + intRetVal.ToString() + "\n");
            Console.WriteLine("\nGuid: " + strGuid + "\n");
        }

 	    private static int GetExtractAuditID(ref string Result) {

			int intResult = -1;
			StringBuilder sbString = new StringBuilder(38);
			intResult = GetExtractAuditGUID(sbString, 38);		
			Result = sbString.ToString();

			return intResult;
		}

 	    private static void GetLastErrorMsg(ref string Result) {

			StringBuilder sbString = new StringBuilder(10000);
			GetLastErrorMessage(sbString, 10000);		
			Result = sbString.ToString();
		}

        private static void Usage() {

            Console.WriteLine(@"************************************************************************");
            Console.WriteLine(@"*                                                                      *");
            Console.WriteLine(@"* Usage:                                                               *");
            Console.WriteLine(@"*                                                                      *");
            Console.WriteLine(@"* DMPExtractTestCS extractpath=<CXS FILE PATH>                         *"); 
            Console.WriteLine(@"*                  targetfile=<TARGET FILE>                            *"); 
            Console.WriteLine(@"*                  imagefile=<IMAGE FILE>                              *");
            Console.WriteLine(@"*                  bank=<BANK ID> (comma delimited)                    *");
            Console.WriteLine(@"*                  customer=<CUSTOMER ID> (comma delimited)            *");
            Console.WriteLine(@"*                  lockbox=<LOCKBOX ID> (comma delimited)              *");
            Console.WriteLine(@"*                  batch=<BATCH ID> (comma delimited)                  *");
            Console.WriteLine(@"*                  procdate=<PROCESSING DATE> (separated by '-')       *");
            Console.WriteLine(@"*                  depdate=<DEPOSIT DATE> (separated by '-')           *");
            Console.WriteLine(@"*                  operator=<OPERATOR ID>                              *");
            Console.WriteLine(@"*                                                                      *");
            Console.WriteLine(@"* Example:                                                             *");
            Console.WriteLine(@"*                                                                      *");
            Console.WriteLine(@"* DMPExtractTestCS extractpath=C:\Checkbox\Extracts\joel_extract_9.cxs *"); 
            Console.WriteLine(@"*                  targetfile=C:\Checkbox\Data\Extract_Results         *"); 
            Console.WriteLine(@"*                  imagefile=C:\Checkbox\Data\Extract_Results          *");
            Console.WriteLine(@"*                  bank=7000                                           *");
            Console.WriteLine(@"*                  customer=100                                        *");
            Console.WriteLine(@"*                  lockbox=l104                                        *");
            Console.WriteLine(@"*                  batch=13025                                         *");
            Console.WriteLine(@"*                  procdate=10/13/2005-10/13/2005                      *");
            Console.WriteLine(@"*                  depdate=10/18/2005-10/18/2005                       *");
            Console.WriteLine(@"*                  operator=studboy                                    *");
            Console.WriteLine(@"*                                                                      *");
            Console.WriteLine(@"************************************************************************");
        }

    }
}
