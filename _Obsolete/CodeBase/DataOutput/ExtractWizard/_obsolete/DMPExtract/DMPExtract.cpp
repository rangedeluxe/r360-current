// DMPExtract.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "DMPExtract.h"

#import "ExtractAPI.tlb" named_guids

class TExtract {

public:

    //TExtract();
    //~TExtract();

    LPSTR TargetFile;
    LPSTR ImageFile;
    LPSTR BankID;
    LPSTR CustomerID;
    LPSTR LockboxID;
    LPSTR BatchID;
    LPSTR ProcessingDateString;
    LPSTR DepositDateString;
    LPSTR OperatorID;
    int ExtractSequenceNumber;
    int Handle;
    int CallerThreadID;
    LPSTR ExtractAuditIdGUID;
    LPSTR LastErrorMessage;
    BOOL HasBeenInitialized;
};

TExtract _Extract;

/*extern "C" int SetExtractOnRecord(TRecordEvent Event) {

    return 0;
}

extern "C" int SetExtractOnImageProgress(TImageProgressEvent Event) {

    return 0;
}

extern "C" int SetExtractOnCancel(TCancelEvent Event) {

    return 0;
}

extern "C" int SetExtractOnFinish(TFinishEvent Event) {

    return 0;
}

extern "C" int SetExtractOnError(TErrorEvent Event) {

    return 0;
}*/

/*
extern "C" DMPEXTRACT_API int* GetDllPtr() {

    return(int*)&_Extract;
}

extern "C" DMPEXTRACT_API LPSTR GetBankID(int* ExtractPointer) {

    TExtract *Extract = (TExtract*)ExtractPointer;

    return Extract->BankID;
}

extern "C" DMPEXTRACT_API LPSTR GetExtractAuditID(int* ExtractPointer) {

    TExtract *Extract = (TExtract*)ExtractPointer;

    return Extract->ExtractAuditIdGUID;
}
*/

extern "C" DMPEXTRACT_API int SetExtractParams(LPSTR TargetFile, 
                                               LPSTR ImageFile, 
                                               LPSTR Bank, 
                                               LPSTR Cust, 
                                               LPSTR Box, 
                                               LPSTR Batch, 
                                               LPSTR DateStr, 
                                               LPSTR DepStr, 
                                               LPSTR OperatorID, 
                                               int ExtractSequenceNumber, 
                                               int Handle, 
                                               int CallerThreadID) {

    _Extract.TargetFile = new char[strlen(TargetFile) + 1];
    strcpy_s(_Extract.TargetFile, strlen(TargetFile) + 1, TargetFile);

    _Extract.ImageFile = new char[strlen(ImageFile) + 1];
    strcpy_s(_Extract.ImageFile, strlen(ImageFile) + 1, ImageFile);

    _Extract.BankID = new char[strlen(Bank) + 1];
    strcpy_s(_Extract.BankID, strlen(Bank) + 1, Bank);

    _Extract.CustomerID = new char[strlen(Cust) + 1];
    strcpy_s(_Extract.CustomerID, strlen(Cust) + 1, Cust);

    _Extract.LockboxID = new char[strlen(Box) + 1];
    strcpy_s(_Extract.LockboxID, strlen(Box) + 1, Box);

    _Extract.BatchID = new char[strlen(Batch) + 1];
    strcpy_s(_Extract.BatchID, strlen(Batch) + 1, Batch);

    _Extract.ProcessingDateString = new char[strlen(DateStr) + 1];
    strcpy_s(_Extract.ProcessingDateString, strlen(DateStr) + 1, DateStr);

    _Extract.DepositDateString = new char[strlen(DepStr) + 1];
    strcpy_s(_Extract.DepositDateString, strlen(DepStr) + 1, DepStr);

    _Extract.OperatorID = new char[strlen(OperatorID) + 1];
    strcpy_s(_Extract.OperatorID, strlen(OperatorID) + 1, OperatorID);

    _Extract.ExtractSequenceNumber = ExtractSequenceNumber;

    _Extract.LastErrorMessage = new char[strlen(" ") + 1];
    strcpy_s(_Extract.LastErrorMessage, strlen(" ") + 1, " ");

    //_Extract.Handle = new char[strlen(Handle) + 1];
    //strcpy(_Extract.Handle, Handle);

    //_Extract.CallerThreadID = new char[strlen(CallerThreadID) + 1];
    //strcpy(_Extract.CallerThreadID, CallerThreadID);

    _Extract.HasBeenInitialized = TRUE;

    return 1;
}

extern "C" DMPEXTRACT_API int RunExtract(LPSTR SetupFile, 
                                         LPSTR TraceField, 
                                         LPSTR RerunList) {

    HRESULT hRes = S_OK;

    if(!_Extract.HasBeenInitialized) {
        printf("Extract Paramaters have not been initialized.");
        return(0);
    }

    CoInitialize(NULL);
    ExtractAPI::IMyManagedInterface *pManagedInterface = NULL;

    hRes = CoCreateInstance(ExtractAPI::CLSID_cExtract, NULL, CLSCTX_INPROC_SERVER, 
    ExtractAPI::IID_IMyManagedInterface, reinterpret_cast<void**> (&pManagedInterface));

    if (S_OK == hRes)
    {
        _bstr_t bstrSetupFile(SetupFile);
        _bstr_t bstrTraceField(TraceField); 
        _bstr_t bstrRerunList(RerunList);
        _bstr_t bstrTargetFile(_Extract.TargetFile);
        _bstr_t bstrImageFile(_Extract.ImageFile);
        _bstr_t bstrBankID(_Extract.BankID);
        _bstr_t bstrCustomerID(_Extract.CustomerID);
        _bstr_t bstrLockboxID(_Extract.LockboxID);
        _bstr_t bstrBatchID(_Extract.BatchID);
        _bstr_t bstrProcessingDateString(_Extract.ProcessingDateString);
        _bstr_t bstrDepositDateString(_Extract.DepositDateString);
        _bstr_t bstrOperatorID(_Extract.OperatorID);

        BSTR ExtractAuditID;
        BSTR LastErrorMessage;

        long retVal =0;
        hRes = pManagedInterface->raw_RunExtract(bstrSetupFile,
                                                 bstrTraceField, 
                                                 bstrRerunList,
                                                 bstrTargetFile, 
                                                 bstrImageFile,
                                                 bstrBankID, 
                                                 bstrCustomerID, 
                                                 bstrLockboxID, 
                                                 bstrBatchID, 
                                                 bstrProcessingDateString, 
                                                 bstrDepositDateString,
                                                 bstrOperatorID,
                                                 _Extract.ExtractSequenceNumber,
                                                 &ExtractAuditID,
                                                 &LastErrorMessage,
                                                 &retVal);

        _bstr_t bstrExtractAuditID = ExtractAuditID;
        _bstr_t bstrLastErrorMessage = LastErrorMessage;

        SysFreeString(ExtractAuditID);
        SysFreeString(LastErrorMessage);

        _Extract.ExtractAuditIdGUID = new char[strlen(bstrExtractAuditID) + 1];
        strcpy_s(_Extract.ExtractAuditIdGUID, strlen(bstrExtractAuditID) + 1, bstrExtractAuditID);

        _Extract.LastErrorMessage = new char[strlen(bstrLastErrorMessage) + 1];
        strcpy_s(_Extract.LastErrorMessage, strlen(bstrLastErrorMessage) + 1, bstrLastErrorMessage);

        printf("\nThe value returned by the dll is %ld\n", retVal);

        printf("\nThe Extract Audit ID returned by the dll is %s\n", (LPSTR)bstrExtractAuditID);

        printf("\nThe Last Error Message returned by the dll is %s\n", (LPSTR)bstrLastErrorMessage);

        pManagedInterface->Release();
    }

    CoUninitialize();
    return 0;
}

extern "C" int DMPEXTRACT_API TestExtract(LPSTR SetupFile, LPSTR Record) {

    HRESULT hRes = S_OK;

    CoInitialize(NULL);
    ExtractAPI::IMyManagedInterface *pManagedInterface = NULL;

    hRes = CoCreateInstance(ExtractAPI::CLSID_cExtract, NULL, CLSCTX_INPROC_SERVER, 
    ExtractAPI::IID_IMyManagedInterface, reinterpret_cast<void**> (&pManagedInterface));

    if (S_OK == hRes)
    {
        _bstr_t bstrSetupFile(SetupFile);
        _bstr_t bstrRecord(Record);

        long retVal =0;
        hRes = pManagedInterface->raw_TestExtract(bstrSetupFile,
                                                  bstrRecord, 
                                                  &retVal);

        printf("\nThe value returned by the dll is %ld\n",retVal);
        pManagedInterface->Release();
    }

    CoUninitialize();
    return 0;
}

extern "C" int DMPEXTRACT_API GetExtractAuditGUID(char* zGUID, int iLen) {

    if(!_Extract.HasBeenInitialized) {
        printf("Extract Parameters have not been initialized.");
        return(0);
    } else {
        strcpy_s(zGUID, iLen + 1, _Extract.ExtractAuditIdGUID);
        zGUID[iLen + 1] = 0;
        return(1);
    }
}

extern "C" int DMPEXTRACT_API CloseTestExtract() {

    return 1;
}

extern "C" void DMPEXTRACT_API GetLastErrorMessage(char* zError, int iLen) {

    if(!_Extract.HasBeenInitialized) {
        printf("Extract Parameters have not been initialized.");
    } else {
        strcpy_s(zError, iLen + 1, _Extract.LastErrorMessage);
        zError[iLen + 1] = 0;
    }
}
