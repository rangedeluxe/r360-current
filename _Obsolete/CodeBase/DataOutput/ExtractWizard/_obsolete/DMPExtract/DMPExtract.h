// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DMPEXTRACT_EXPORTS
// symbol defined on the command line. this symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DMPEXTRACT_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DMPEXTRACT_EXPORTS
#define DMPEXTRACT_API __declspec(dllexport)
#else
#define DMPEXTRACT_API __declspec(dllimport)
#endif

// This class is exported from the DMPExtract.dll
class DMPEXTRACT_API CDMPExtract {
public:
	CDMPExtract(void);
};

extern DMPEXTRACT_API int nDMPExtract;

DMPEXTRACT_API int fnDMPExtract(void);
