namespace DMP.Delivery.Extract.ExtractWizard
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.TreeNode treeNode1 = new System.Windows.Forms.TreeNode("File Headers");
            System.Windows.Forms.TreeNode treeNode2 = new System.Windows.Forms.TreeNode("Bank Headers");
            System.Windows.Forms.TreeNode treeNode3 = new System.Windows.Forms.TreeNode("Customer Headers");
            System.Windows.Forms.TreeNode treeNode4 = new System.Windows.Forms.TreeNode("Lockbox Headers");
            System.Windows.Forms.TreeNode treeNode5 = new System.Windows.Forms.TreeNode("Batch Headers");
            System.Windows.Forms.TreeNode treeNode6 = new System.Windows.Forms.TreeNode("Transaction Headers");
            System.Windows.Forms.TreeNode treeNode7 = new System.Windows.Forms.TreeNode("Check Records");
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Invoice Records");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Document Records");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Joint Detail Records");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Transaction Trailers");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Batch Trailers");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Lockbox Trailers");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Customer Trailers");
            System.Windows.Forms.TreeNode treeNode15 = new System.Windows.Forms.TreeNode("Bank Trailers");
            System.Windows.Forms.TreeNode treeNode16 = new System.Windows.Forms.TreeNode("File Trailers");
            System.Windows.Forms.TreeNode treeNode17 = new System.Windows.Forms.TreeNode("Layout", new System.Windows.Forms.TreeNode[] {
            treeNode1,
            treeNode2,
            treeNode3,
            treeNode4,
            treeNode5,
            treeNode6,
            treeNode7,
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13,
            treeNode14,
            treeNode15,
            treeNode16});
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.newSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.closeSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.printToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.printerSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.setupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.resultsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logFileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.runToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.licenseToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tpJointDetail = new System.Windows.Forms.TabPage();
            this.tabControl10 = new System.Windows.Forms.TabControl();
            this.tabPage39 = new System.Windows.Forms.TabPage();
            this.label57 = new System.Windows.Forms.Label();
            this.cboJointDetailDisplayLayouts = new System.Windows.Forms.ComboBox();
            this.textBox119 = new System.Windows.Forms.TextBox();
            this.label56 = new System.Windows.Forms.Label();
            this.panel20 = new System.Windows.Forms.Panel();
            this.textBox108 = new System.Windows.Forms.TextBox();
            this.button180 = new System.Windows.Forms.Button();
            this.button181 = new System.Windows.Forms.Button();
            this.button182 = new System.Windows.Forms.Button();
            this.button183 = new System.Windows.Forms.Button();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.rdoJointDetailRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoJointDetailRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label52 = new System.Windows.Forms.Label();
            this.cboJointDetailLayouts = new System.Windows.Forms.ComboBox();
            this.lvJointDetailFields = new System.Windows.Forms.ListView();
            this.columnHeader107 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader108 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader109 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader110 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader111 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader112 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader113 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader118 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader119 = new System.Windows.Forms.ColumnHeader();
            this.textBox109 = new System.Windows.Forms.TextBox();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.textBox120 = new System.Windows.Forms.TextBox();
            this.textBox110 = new System.Windows.Forms.TextBox();
            this.button184 = new System.Windows.Forms.Button();
            this.button185 = new System.Windows.Forms.Button();
            this.button186 = new System.Windows.Forms.Button();
            this.button187 = new System.Windows.Forms.Button();
            this.button188 = new System.Windows.Forms.Button();
            this.label53 = new System.Windows.Forms.Label();
            this.listBox29 = new System.Windows.Forms.ListBox();
            this.panel21 = new System.Windows.Forms.Panel();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.button198 = new System.Windows.Forms.Button();
            this.lstJointDetailColumns = new System.Windows.Forms.ListBox();
            this.label55 = new System.Windows.Forms.Label();
            this.splitter9 = new System.Windows.Forms.Splitter();
            this.tpDocument = new System.Windows.Forms.TabPage();
            this.tabControl9 = new System.Windows.Forms.TabControl();
            this.tabPage36 = new System.Windows.Forms.TabPage();
            this.panel18 = new System.Windows.Forms.Panel();
            this.textBox97 = new System.Windows.Forms.TextBox();
            this.button161 = new System.Windows.Forms.Button();
            this.button162 = new System.Windows.Forms.Button();
            this.button163 = new System.Windows.Forms.Button();
            this.button164 = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.cboDocumentLayouts = new System.Windows.Forms.ComboBox();
            this.lvDocumentFields = new System.Windows.Forms.ListView();
            this.columnHeader96 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader97 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader98 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader99 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader100 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader101 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader102 = new System.Windows.Forms.ColumnHeader();
            this.textBox98 = new System.Windows.Forms.TextBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.textBox99 = new System.Windows.Forms.TextBox();
            this.button165 = new System.Windows.Forms.Button();
            this.button166 = new System.Windows.Forms.Button();
            this.button167 = new System.Windows.Forms.Button();
            this.button168 = new System.Windows.Forms.Button();
            this.button169 = new System.Windows.Forms.Button();
            this.button170 = new System.Windows.Forms.Button();
            this.label49 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.listBox26 = new System.Windows.Forms.ListBox();
            this.listBox27 = new System.Windows.Forms.ListBox();
            this.tabPage37 = new System.Windows.Forms.TabPage();
            this.textBox100 = new System.Windows.Forms.TextBox();
            this.textBox101 = new System.Windows.Forms.TextBox();
            this.textBox102 = new System.Windows.Forms.TextBox();
            this.button171 = new System.Windows.Forms.Button();
            this.button172 = new System.Windows.Forms.Button();
            this.button173 = new System.Windows.Forms.Button();
            this.lvDocumentsOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader103 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader104 = new System.Windows.Forms.ColumnHeader();
            this.tabPage38 = new System.Windows.Forms.TabPage();
            this.textBox103 = new System.Windows.Forms.TextBox();
            this.textBox104 = new System.Windows.Forms.TextBox();
            this.button174 = new System.Windows.Forms.Button();
            this.button175 = new System.Windows.Forms.Button();
            this.textBox105 = new System.Windows.Forms.TextBox();
            this.textBox106 = new System.Windows.Forms.TextBox();
            this.textBox107 = new System.Windows.Forms.TextBox();
            this.button176 = new System.Windows.Forms.Button();
            this.button177 = new System.Windows.Forms.Button();
            this.button178 = new System.Windows.Forms.Button();
            this.listView27 = new System.Windows.Forms.ListView();
            this.columnHeader105 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader106 = new System.Windows.Forms.ColumnHeader();
            this.panel19 = new System.Windows.Forms.Panel();
            this.button179 = new System.Windows.Forms.Button();
            this.lstDocumentColumns = new System.Windows.Forms.ListBox();
            this.label51 = new System.Windows.Forms.Label();
            this.splitter8 = new System.Windows.Forms.Splitter();
            this.tpStub = new System.Windows.Forms.TabPage();
            this.tabControl8 = new System.Windows.Forms.TabControl();
            this.tabPage33 = new System.Windows.Forms.TabPage();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.panel16 = new System.Windows.Forms.Panel();
            this.textBox86 = new System.Windows.Forms.TextBox();
            this.button142 = new System.Windows.Forms.Button();
            this.button143 = new System.Windows.Forms.Button();
            this.button144 = new System.Windows.Forms.Button();
            this.button145 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.cboStubLayouts = new System.Windows.Forms.ComboBox();
            this.lvStubFields = new System.Windows.Forms.ListView();
            this.columnHeader85 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader86 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader87 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader88 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader89 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader90 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader91 = new System.Windows.Forms.ColumnHeader();
            this.textBox87 = new System.Windows.Forms.TextBox();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.textBox88 = new System.Windows.Forms.TextBox();
            this.button146 = new System.Windows.Forms.Button();
            this.button147 = new System.Windows.Forms.Button();
            this.button148 = new System.Windows.Forms.Button();
            this.button149 = new System.Windows.Forms.Button();
            this.button150 = new System.Windows.Forms.Button();
            this.button151 = new System.Windows.Forms.Button();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.listBox23 = new System.Windows.Forms.ListBox();
            this.listBox24 = new System.Windows.Forms.ListBox();
            this.tabPage34 = new System.Windows.Forms.TabPage();
            this.textBox89 = new System.Windows.Forms.TextBox();
            this.textBox90 = new System.Windows.Forms.TextBox();
            this.textBox91 = new System.Windows.Forms.TextBox();
            this.button152 = new System.Windows.Forms.Button();
            this.button153 = new System.Windows.Forms.Button();
            this.button154 = new System.Windows.Forms.Button();
            this.lvStubsOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader92 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader93 = new System.Windows.Forms.ColumnHeader();
            this.tabPage35 = new System.Windows.Forms.TabPage();
            this.textBox92 = new System.Windows.Forms.TextBox();
            this.textBox93 = new System.Windows.Forms.TextBox();
            this.button155 = new System.Windows.Forms.Button();
            this.button156 = new System.Windows.Forms.Button();
            this.textBox94 = new System.Windows.Forms.TextBox();
            this.textBox95 = new System.Windows.Forms.TextBox();
            this.textBox96 = new System.Windows.Forms.TextBox();
            this.button157 = new System.Windows.Forms.Button();
            this.button158 = new System.Windows.Forms.Button();
            this.button159 = new System.Windows.Forms.Button();
            this.listView24 = new System.Windows.Forms.ListView();
            this.columnHeader94 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader95 = new System.Windows.Forms.ColumnHeader();
            this.panel17 = new System.Windows.Forms.Panel();
            this.button160 = new System.Windows.Forms.Button();
            this.lstStubColumns = new System.Windows.Forms.ListBox();
            this.label47 = new System.Windows.Forms.Label();
            this.splitter7 = new System.Windows.Forms.Splitter();
            this.tpCheck = new System.Windows.Forms.TabPage();
            this.tabControl7 = new System.Windows.Forms.TabControl();
            this.tabPage30 = new System.Windows.Forms.TabPage();
            this.panel14 = new System.Windows.Forms.Panel();
            this.textBox75 = new System.Windows.Forms.TextBox();
            this.button123 = new System.Windows.Forms.Button();
            this.button124 = new System.Windows.Forms.Button();
            this.button125 = new System.Windows.Forms.Button();
            this.button126 = new System.Windows.Forms.Button();
            this.label40 = new System.Windows.Forms.Label();
            this.cboCheckLayouts = new System.Windows.Forms.ComboBox();
            this.lvCheckFields = new System.Windows.Forms.ListView();
            this.columnHeader74 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader75 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader76 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader77 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader78 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader79 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader80 = new System.Windows.Forms.ColumnHeader();
            this.textBox76 = new System.Windows.Forms.TextBox();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.textBox77 = new System.Windows.Forms.TextBox();
            this.button127 = new System.Windows.Forms.Button();
            this.button128 = new System.Windows.Forms.Button();
            this.button129 = new System.Windows.Forms.Button();
            this.button130 = new System.Windows.Forms.Button();
            this.button131 = new System.Windows.Forms.Button();
            this.button132 = new System.Windows.Forms.Button();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.listBox20 = new System.Windows.Forms.ListBox();
            this.listBox21 = new System.Windows.Forms.ListBox();
            this.tabPage31 = new System.Windows.Forms.TabPage();
            this.textBox78 = new System.Windows.Forms.TextBox();
            this.textBox79 = new System.Windows.Forms.TextBox();
            this.textBox80 = new System.Windows.Forms.TextBox();
            this.button133 = new System.Windows.Forms.Button();
            this.button134 = new System.Windows.Forms.Button();
            this.button135 = new System.Windows.Forms.Button();
            this.lvChecksOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader81 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader82 = new System.Windows.Forms.ColumnHeader();
            this.tabPage32 = new System.Windows.Forms.TabPage();
            this.textBox81 = new System.Windows.Forms.TextBox();
            this.textBox82 = new System.Windows.Forms.TextBox();
            this.button136 = new System.Windows.Forms.Button();
            this.button137 = new System.Windows.Forms.Button();
            this.textBox83 = new System.Windows.Forms.TextBox();
            this.textBox84 = new System.Windows.Forms.TextBox();
            this.textBox85 = new System.Windows.Forms.TextBox();
            this.button138 = new System.Windows.Forms.Button();
            this.button139 = new System.Windows.Forms.Button();
            this.button140 = new System.Windows.Forms.Button();
            this.listView21 = new System.Windows.Forms.ListView();
            this.columnHeader83 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader84 = new System.Windows.Forms.ColumnHeader();
            this.panel15 = new System.Windows.Forms.Panel();
            this.button141 = new System.Windows.Forms.Button();
            this.lstCheckColumns = new System.Windows.Forms.ListBox();
            this.label43 = new System.Windows.Forms.Label();
            this.splitter6 = new System.Windows.Forms.Splitter();
            this.tpTransaction = new System.Windows.Forms.TabPage();
            this.tabControl6 = new System.Windows.Forms.TabControl();
            this.tabPage27 = new System.Windows.Forms.TabPage();
            this.panel12 = new System.Windows.Forms.Panel();
            this.textBox64 = new System.Windows.Forms.TextBox();
            this.button104 = new System.Windows.Forms.Button();
            this.button105 = new System.Windows.Forms.Button();
            this.button106 = new System.Windows.Forms.Button();
            this.button107 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.rdoTransactionRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoTransactionRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label36 = new System.Windows.Forms.Label();
            this.cboTransactionLayouts = new System.Windows.Forms.ComboBox();
            this.lvTransactionFields = new System.Windows.Forms.ListView();
            this.columnHeader63 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader64 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader65 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader66 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader67 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader68 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader69 = new System.Windows.Forms.ColumnHeader();
            this.textBox65 = new System.Windows.Forms.TextBox();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.textBox66 = new System.Windows.Forms.TextBox();
            this.button108 = new System.Windows.Forms.Button();
            this.button109 = new System.Windows.Forms.Button();
            this.button110 = new System.Windows.Forms.Button();
            this.button111 = new System.Windows.Forms.Button();
            this.button112 = new System.Windows.Forms.Button();
            this.button113 = new System.Windows.Forms.Button();
            this.label37 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.listBox17 = new System.Windows.Forms.ListBox();
            this.listBox18 = new System.Windows.Forms.ListBox();
            this.tabPage28 = new System.Windows.Forms.TabPage();
            this.textBox67 = new System.Windows.Forms.TextBox();
            this.textBox68 = new System.Windows.Forms.TextBox();
            this.textBox69 = new System.Windows.Forms.TextBox();
            this.button114 = new System.Windows.Forms.Button();
            this.button115 = new System.Windows.Forms.Button();
            this.button116 = new System.Windows.Forms.Button();
            this.lvTransactionsOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader70 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader71 = new System.Windows.Forms.ColumnHeader();
            this.tabPage29 = new System.Windows.Forms.TabPage();
            this.textBox70 = new System.Windows.Forms.TextBox();
            this.textBox71 = new System.Windows.Forms.TextBox();
            this.button117 = new System.Windows.Forms.Button();
            this.button118 = new System.Windows.Forms.Button();
            this.textBox72 = new System.Windows.Forms.TextBox();
            this.textBox73 = new System.Windows.Forms.TextBox();
            this.textBox74 = new System.Windows.Forms.TextBox();
            this.button119 = new System.Windows.Forms.Button();
            this.button120 = new System.Windows.Forms.Button();
            this.button121 = new System.Windows.Forms.Button();
            this.listView18 = new System.Windows.Forms.ListView();
            this.columnHeader72 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader73 = new System.Windows.Forms.ColumnHeader();
            this.panel13 = new System.Windows.Forms.Panel();
            this.button122 = new System.Windows.Forms.Button();
            this.lstTransactionColumns = new System.Windows.Forms.ListBox();
            this.label39 = new System.Windows.Forms.Label();
            this.splitter5 = new System.Windows.Forms.Splitter();
            this.tpBatch = new System.Windows.Forms.TabPage();
            this.tabControl5 = new System.Windows.Forms.TabControl();
            this.tabPage24 = new System.Windows.Forms.TabPage();
            this.panel10 = new System.Windows.Forms.Panel();
            this.textBox53 = new System.Windows.Forms.TextBox();
            this.button85 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.button87 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.rdoBatchRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoBatchRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label32 = new System.Windows.Forms.Label();
            this.cboBatchLayouts = new System.Windows.Forms.ComboBox();
            this.lvBatchFields = new System.Windows.Forms.ListView();
            this.columnHeader52 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader53 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader54 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader55 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader56 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader57 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader58 = new System.Windows.Forms.ColumnHeader();
            this.textBox54 = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.textBox55 = new System.Windows.Forms.TextBox();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button91 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.button93 = new System.Windows.Forms.Button();
            this.button94 = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.listBox14 = new System.Windows.Forms.ListBox();
            this.listBox15 = new System.Windows.Forms.ListBox();
            this.tabPage25 = new System.Windows.Forms.TabPage();
            this.textBox56 = new System.Windows.Forms.TextBox();
            this.textBox57 = new System.Windows.Forms.TextBox();
            this.textBox58 = new System.Windows.Forms.TextBox();
            this.button95 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.button97 = new System.Windows.Forms.Button();
            this.lvBatchOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader59 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader60 = new System.Windows.Forms.ColumnHeader();
            this.tabPage26 = new System.Windows.Forms.TabPage();
            this.textBox59 = new System.Windows.Forms.TextBox();
            this.textBox60 = new System.Windows.Forms.TextBox();
            this.button98 = new System.Windows.Forms.Button();
            this.button99 = new System.Windows.Forms.Button();
            this.textBox61 = new System.Windows.Forms.TextBox();
            this.textBox62 = new System.Windows.Forms.TextBox();
            this.textBox63 = new System.Windows.Forms.TextBox();
            this.button100 = new System.Windows.Forms.Button();
            this.button101 = new System.Windows.Forms.Button();
            this.button102 = new System.Windows.Forms.Button();
            this.listView15 = new System.Windows.Forms.ListView();
            this.columnHeader61 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader62 = new System.Windows.Forms.ColumnHeader();
            this.panel11 = new System.Windows.Forms.Panel();
            this.button103 = new System.Windows.Forms.Button();
            this.lstBatchColumns = new System.Windows.Forms.ListBox();
            this.label35 = new System.Windows.Forms.Label();
            this.splitter4 = new System.Windows.Forms.Splitter();
            this.tpLockbox = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage21 = new System.Windows.Forms.TabPage();
            this.panel8 = new System.Windows.Forms.Panel();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.button66 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.button68 = new System.Windows.Forms.Button();
            this.button69 = new System.Windows.Forms.Button();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.rdoLockboxRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoLockboxRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label28 = new System.Windows.Forms.Label();
            this.cboLockboxLayouts = new System.Windows.Forms.ComboBox();
            this.lvLockboxFields = new System.Windows.Forms.ListView();
            this.columnHeader41 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader42 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader43 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader44 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader45 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader46 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader47 = new System.Windows.Forms.ColumnHeader();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.button70 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.button75 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.listBox11 = new System.Windows.Forms.ListBox();
            this.listBox12 = new System.Windows.Forms.ListBox();
            this.tabPage22 = new System.Windows.Forms.TabPage();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.button76 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.lvLockboxOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader48 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader49 = new System.Windows.Forms.ColumnHeader();
            this.tabPage23 = new System.Windows.Forms.TabPage();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.button79 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.textBox52 = new System.Windows.Forms.TextBox();
            this.button81 = new System.Windows.Forms.Button();
            this.button82 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.listView12 = new System.Windows.Forms.ListView();
            this.columnHeader50 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader51 = new System.Windows.Forms.ColumnHeader();
            this.panel9 = new System.Windows.Forms.Panel();
            this.button84 = new System.Windows.Forms.Button();
            this.lstLockboxColumns = new System.Windows.Forms.ListBox();
            this.label31 = new System.Windows.Forms.Label();
            this.splitter3 = new System.Windows.Forms.Splitter();
            this.tpCustomer = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage18 = new System.Windows.Forms.TabPage();
            this.panel6 = new System.Windows.Forms.Panel();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.button47 = new System.Windows.Forms.Button();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.button50 = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.rdoCustomerRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoCustomerRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label24 = new System.Windows.Forms.Label();
            this.cboCustomerLayouts = new System.Windows.Forms.ComboBox();
            this.lvCustomerFields = new System.Windows.Forms.ListView();
            this.columnHeader30 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader31 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader32 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader33 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader34 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader35 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader36 = new System.Windows.Forms.ColumnHeader();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.button51 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.listBox8 = new System.Windows.Forms.ListBox();
            this.listBox9 = new System.Windows.Forms.ListBox();
            this.tabPage19 = new System.Windows.Forms.TabPage();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.lvCustomerOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader37 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader38 = new System.Windows.Forms.ColumnHeader();
            this.tabPage20 = new System.Windows.Forms.TabPage();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.button60 = new System.Windows.Forms.Button();
            this.button61 = new System.Windows.Forms.Button();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.listView9 = new System.Windows.Forms.ListView();
            this.columnHeader39 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader40 = new System.Windows.Forms.ColumnHeader();
            this.panel7 = new System.Windows.Forms.Panel();
            this.button65 = new System.Windows.Forms.Button();
            this.lstCustomerColumns = new System.Windows.Forms.ListBox();
            this.label27 = new System.Windows.Forms.Label();
            this.splitter2 = new System.Windows.Forms.Splitter();
            this.tpBank = new System.Windows.Forms.TabPage();
            this.panel3 = new System.Windows.Forms.Panel();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.panel4 = new System.Windows.Forms.Panel();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.rdoBankRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoBankRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.cboBankLayouts = new System.Windows.Forms.ComboBox();
            this.lvBankFields = new System.Windows.Forms.ListView();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.listBox5 = new System.Windows.Forms.ListBox();
            this.listBox4 = new System.Windows.Forms.ListBox();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.lvBankOrderByColumns = new System.Windows.Forms.ListView();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.button28 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.listView3 = new System.Windows.Forms.ListView();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader18 = new System.Windows.Forms.ColumnHeader();
            this.panel2 = new System.Windows.Forms.Panel();
            this.button14 = new System.Windows.Forms.Button();
            this.lstBankColumns = new System.Windows.Forms.ListBox();
            this.label17 = new System.Windows.Forms.Label();
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.tpFile = new System.Windows.Forms.TabPage();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.btnRemoveField = new System.Windows.Forms.Button();
            this.btnMoveFieldDown = new System.Windows.Forms.Button();
            this.btnMoveFieldUp = new System.Windows.Forms.Button();
            this.btnRestoreDefaultFieldData = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.lvFileFields = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.rdoFileRecordTypeTrailer = new System.Windows.Forms.RadioButton();
            this.rdoFileRecordTypeHeader = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.cboFileLayouts = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.tpGeneral = new System.Windows.Forms.TabPage();
            this.grpExtractLayouts = new System.Windows.Forms.GroupBox();
            this.tvGeneralLayout = new System.Windows.Forms.TreeView();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.btnDeleteCurrentRecord = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.grpExtractImageOptions = new System.Windows.Forms.GroupBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.chkCombineImagesAcrossProcessingDates = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.rdoImageFileFormatEmbeddedTIFF = new System.Windows.Forms.RadioButton();
            this.rdoImageFileFormatPDF = new System.Windows.Forms.RadioButton();
            this.rdoImageFileFormatSingleMultiTIFF = new System.Windows.Forms.RadioButton();
            this.rdoImageFileFormatNone = new System.Windows.Forms.RadioButton();
            this.grpExtractRecordOptions = new System.Windows.Forms.GroupBox();
            this.cboRecordDelimiter = new System.Windows.Forms.ComboBox();
            this.cboFieldDelimiter = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.chkShowNULLAsSpace = new System.Windows.Forms.CheckBox();
            this.chkEncloseFieldDataInQuotes = new System.Windows.Forms.CheckBox();
            this.chkOmitPadding = new System.Windows.Forms.CheckBox();
            this.grpExtractFileOptions = new System.Windows.Forms.GroupBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.txtImageFilePath = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.chkUsePostProcessingDLL = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.txtLogFilePath = new System.Windows.Forms.TextBox();
            this.txtPostProcessingDLLFileName = new System.Windows.Forms.TextBox();
            this.txtExtractFilePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabMain = new System.Windows.Forms.TabControl();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.panel5 = new System.Windows.Forms.Panel();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.radioButton9 = new System.Windows.Forms.RadioButton();
            this.radioButton10 = new System.Windows.Forms.RadioButton();
            this.label21 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.listView4 = new System.Windows.Forms.ListView();
            this.columnHeader19 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader20 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader21 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader22 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader23 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader24 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader25 = new System.Windows.Forms.ColumnHeader();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.button33 = new System.Windows.Forms.Button();
            this.button34 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button38 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.listBox6 = new System.Windows.Forms.ListBox();
            this.listBox7 = new System.Windows.Forms.ListBox();
            this.tabPage16 = new System.Windows.Forms.TabPage();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.button39 = new System.Windows.Forms.Button();
            this.button40 = new System.Windows.Forms.Button();
            this.button41 = new System.Windows.Forms.Button();
            this.listView5 = new System.Windows.Forms.ListView();
            this.columnHeader26 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader27 = new System.Windows.Forms.ColumnHeader();
            this.tabPage17 = new System.Windows.Forms.TabPage();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.button42 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.button44 = new System.Windows.Forms.Button();
            this.button45 = new System.Windows.Forms.Button();
            this.button46 = new System.Windows.Forms.Button();
            this.listView6 = new System.Windows.Forms.ListView();
            this.columnHeader28 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader29 = new System.Windows.Forms.ColumnHeader();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.menuStrip1.SuspendLayout();
            this.tpJointDetail.SuspendLayout();
            this.tabControl10.SuspendLayout();
            this.tabPage39.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.panel21.SuspendLayout();
            this.tpDocument.SuspendLayout();
            this.tabControl9.SuspendLayout();
            this.tabPage36.SuspendLayout();
            this.groupBox25.SuspendLayout();
            this.tabPage37.SuspendLayout();
            this.tabPage38.SuspendLayout();
            this.panel19.SuspendLayout();
            this.tpStub.SuspendLayout();
            this.tabControl8.SuspendLayout();
            this.tabPage33.SuspendLayout();
            this.groupBox23.SuspendLayout();
            this.tabPage34.SuspendLayout();
            this.tabPage35.SuspendLayout();
            this.panel17.SuspendLayout();
            this.tpCheck.SuspendLayout();
            this.tabControl7.SuspendLayout();
            this.tabPage30.SuspendLayout();
            this.groupBox21.SuspendLayout();
            this.tabPage31.SuspendLayout();
            this.tabPage32.SuspendLayout();
            this.panel15.SuspendLayout();
            this.tpTransaction.SuspendLayout();
            this.tabControl6.SuspendLayout();
            this.tabPage27.SuspendLayout();
            this.groupBox18.SuspendLayout();
            this.groupBox19.SuspendLayout();
            this.tabPage28.SuspendLayout();
            this.tabPage29.SuspendLayout();
            this.panel13.SuspendLayout();
            this.tpBatch.SuspendLayout();
            this.tabControl5.SuspendLayout();
            this.tabPage24.SuspendLayout();
            this.groupBox16.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.tabPage25.SuspendLayout();
            this.tabPage26.SuspendLayout();
            this.panel11.SuspendLayout();
            this.tpLockbox.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage21.SuspendLayout();
            this.groupBox14.SuspendLayout();
            this.groupBox15.SuspendLayout();
            this.tabPage22.SuspendLayout();
            this.tabPage23.SuspendLayout();
            this.panel9.SuspendLayout();
            this.tpCustomer.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage18.SuspendLayout();
            this.groupBox12.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tabPage19.SuspendLayout();
            this.tabPage20.SuspendLayout();
            this.panel7.SuspendLayout();
            this.tpBank.SuspendLayout();
            this.panel3.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tpFile.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tpGeneral.SuspendLayout();
            this.grpExtractLayouts.SuspendLayout();
            this.grpExtractImageOptions.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.grpExtractRecordOptions.SuspendLayout();
            this.grpExtractFileOptions.SuspendLayout();
            this.tabMain.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tabPage16.SuspendLayout();
            this.tabPage17.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.viewToolStripMenuItem,
            this.runToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1008, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newSetupToolStripMenuItem,
            this.openSetupToolStripMenuItem,
            this.closeSetupToolStripMenuItem,
            this.toolStripSeparator1,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripMenuItem1,
            this.printToolStripMenuItem,
            this.printerSetupToolStripMenuItem,
            this.toolStripMenuItem2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(35, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // newSetupToolStripMenuItem
            // 
            this.newSetupToolStripMenuItem.Image = global::DMP.Delivery.Extract.ExtractWizard.Properties.Resources.DocumentHS;
            this.newSetupToolStripMenuItem.Name = "newSetupToolStripMenuItem";
            this.newSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.newSetupToolStripMenuItem.Text = "&New Setup";
            this.newSetupToolStripMenuItem.Click += new System.EventHandler(this.newSetupToolStripMenuItem_Click);
            // 
            // openSetupToolStripMenuItem
            // 
            this.openSetupToolStripMenuItem.Image = global::DMP.Delivery.Extract.ExtractWizard.Properties.Resources.openHS;
            this.openSetupToolStripMenuItem.Name = "openSetupToolStripMenuItem";
            this.openSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.openSetupToolStripMenuItem.Text = "&Open Setup...";
            this.openSetupToolStripMenuItem.Click += new System.EventHandler(this.openSetupToolStripMenuItem_Click);
            // 
            // closeSetupToolStripMenuItem
            // 
            this.closeSetupToolStripMenuItem.Name = "closeSetupToolStripMenuItem";
            this.closeSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.closeSetupToolStripMenuItem.Text = "&Close Setup";
            this.closeSetupToolStripMenuItem.Click += new System.EventHandler(this.closeSetupToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(157, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Image = global::DMP.Delivery.Extract.ExtractWizard.Properties.Resources.saveHS;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As...";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.saveAsToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(157, 6);
            // 
            // printToolStripMenuItem
            // 
            this.printToolStripMenuItem.Image = global::DMP.Delivery.Extract.ExtractWizard.Properties.Resources.PrintHS;
            this.printToolStripMenuItem.Name = "printToolStripMenuItem";
            this.printToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.printToolStripMenuItem.Text = "Print...";
            this.printToolStripMenuItem.Click += new System.EventHandler(this.printToolStripMenuItem_Click);
            // 
            // printerSetupToolStripMenuItem
            // 
            this.printerSetupToolStripMenuItem.Name = "printerSetupToolStripMenuItem";
            this.printerSetupToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.printerSetupToolStripMenuItem.Text = "Printer Setup...";
            this.printerSetupToolStripMenuItem.Click += new System.EventHandler(this.printerSetupToolStripMenuItem_Click);
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(157, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(160, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // viewToolStripMenuItem
            // 
            this.viewToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.setupToolStripMenuItem,
            this.resultsToolStripMenuItem,
            this.logFileToolStripMenuItem});
            this.viewToolStripMenuItem.Name = "viewToolStripMenuItem";
            this.viewToolStripMenuItem.Size = new System.Drawing.Size(41, 20);
            this.viewToolStripMenuItem.Text = "&View";
            // 
            // setupToolStripMenuItem
            // 
            this.setupToolStripMenuItem.Enabled = false;
            this.setupToolStripMenuItem.Name = "setupToolStripMenuItem";
            this.setupToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.setupToolStripMenuItem.Text = "Setup";
            this.setupToolStripMenuItem.Click += new System.EventHandler(this.setupToolStripMenuItem_Click);
            // 
            // resultsToolStripMenuItem
            // 
            this.resultsToolStripMenuItem.Name = "resultsToolStripMenuItem";
            this.resultsToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.resultsToolStripMenuItem.Text = "Results";
            this.resultsToolStripMenuItem.Click += new System.EventHandler(this.resultsToolStripMenuItem_Click);
            // 
            // logFileToolStripMenuItem
            // 
            this.logFileToolStripMenuItem.Name = "logFileToolStripMenuItem";
            this.logFileToolStripMenuItem.Size = new System.Drawing.Size(121, 22);
            this.logFileToolStripMenuItem.Text = "Log File";
            this.logFileToolStripMenuItem.Click += new System.EventHandler(this.logFileToolStripMenuItem_Click);
            // 
            // runToolStripMenuItem
            // 
            this.runToolStripMenuItem.Name = "runToolStripMenuItem";
            this.runToolStripMenuItem.Size = new System.Drawing.Size(38, 20);
            this.runToolStripMenuItem.Text = "&Run";
            this.runToolStripMenuItem.Click += new System.EventHandler(this.runToolStripMenuItem_Click);
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem,
            this.licenseToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(40, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.aboutToolStripMenuItem.Text = "About...";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.aboutToolStripMenuItem_Click);
            // 
            // licenseToolStripMenuItem
            // 
            this.licenseToolStripMenuItem.Name = "licenseToolStripMenuItem";
            this.licenseToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.licenseToolStripMenuItem.Text = "License";
            this.licenseToolStripMenuItem.Click += new System.EventHandler(this.licenseToolStripMenuItem_Click);
            // 
            // tpJointDetail
            // 
            this.tpJointDetail.Controls.Add(this.tabControl10);
            this.tpJointDetail.Controls.Add(this.panel21);
            this.tpJointDetail.Location = new System.Drawing.Point(4, 22);
            this.tpJointDetail.Name = "tpJointDetail";
            this.tpJointDetail.Size = new System.Drawing.Size(1000, 618);
            this.tpJointDetail.TabIndex = 10;
            this.tpJointDetail.Text = "Joint Detail";
            this.tpJointDetail.UseVisualStyleBackColor = true;
            // 
            // tabControl10
            // 
            this.tabControl10.Controls.Add(this.tabPage39);
            this.tabControl10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl10.Location = new System.Drawing.Point(185, 0);
            this.tabControl10.Name = "tabControl10";
            this.tabControl10.SelectedIndex = 0;
            this.tabControl10.Size = new System.Drawing.Size(815, 618);
            this.tabControl10.TabIndex = 2;
            // 
            // tabPage39
            // 
            this.tabPage39.Controls.Add(this.label57);
            this.tabPage39.Controls.Add(this.cboJointDetailDisplayLayouts);
            this.tabPage39.Controls.Add(this.textBox119);
            this.tabPage39.Controls.Add(this.label56);
            this.tabPage39.Controls.Add(this.panel20);
            this.tabPage39.Controls.Add(this.textBox108);
            this.tabPage39.Controls.Add(this.button180);
            this.tabPage39.Controls.Add(this.button181);
            this.tabPage39.Controls.Add(this.button182);
            this.tabPage39.Controls.Add(this.button183);
            this.tabPage39.Controls.Add(this.groupBox26);
            this.tabPage39.Controls.Add(this.label52);
            this.tabPage39.Controls.Add(this.cboJointDetailLayouts);
            this.tabPage39.Controls.Add(this.lvJointDetailFields);
            this.tabPage39.Controls.Add(this.textBox109);
            this.tabPage39.Controls.Add(this.groupBox27);
            this.tabPage39.Location = new System.Drawing.Point(4, 22);
            this.tabPage39.Name = "tabPage39";
            this.tabPage39.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage39.Size = new System.Drawing.Size(807, 592);
            this.tabPage39.TabIndex = 0;
            this.tabPage39.Text = "Create Layouts";
            this.tabPage39.UseVisualStyleBackColor = true;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(3, 52);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(73, 13);
            this.label57.TabIndex = 24;
            this.label57.Text = "Display Level:";
            // 
            // cboJointDetailDisplayLayouts
            // 
            this.cboJointDetailDisplayLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJointDetailDisplayLayouts.FormattingEnabled = true;
            this.cboJointDetailDisplayLayouts.Location = new System.Drawing.Point(88, 49);
            this.cboJointDetailDisplayLayouts.Name = "cboJointDetailDisplayLayouts";
            this.cboJointDetailDisplayLayouts.Size = new System.Drawing.Size(156, 21);
            this.cboJointDetailDisplayLayouts.TabIndex = 23;
            // 
            // textBox119
            // 
            this.textBox119.Location = new System.Drawing.Point(717, 189);
            this.textBox119.Name = "textBox119";
            this.textBox119.Size = new System.Drawing.Size(43, 20);
            this.textBox119.TabIndex = 22;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(716, 173);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(73, 13);
            this.label56.TabIndex = 21;
            this.label56.Text = "Max Repeats:";
            // 
            // panel20
            // 
            this.panel20.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel20.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel20.ForeColor = System.Drawing.SystemColors.Control;
            this.panel20.Location = new System.Drawing.Point(6, 103);
            this.panel20.Name = "panel20";
            this.panel20.Size = new System.Drawing.Size(782, 1);
            this.panel20.TabIndex = 20;
            // 
            // textBox108
            // 
            this.textBox108.BackColor = System.Drawing.SystemColors.Window;
            this.textBox108.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox108.Location = new System.Drawing.Point(6, 13);
            this.textBox108.Name = "textBox108";
            this.textBox108.ReadOnly = true;
            this.textBox108.Size = new System.Drawing.Size(763, 13);
            this.textBox108.TabIndex = 19;
            this.textBox108.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button180
            // 
            this.button180.Location = new System.Drawing.Point(568, 45);
            this.button180.Name = "button180";
            this.button180.Size = new System.Drawing.Size(130, 23);
            this.button180.TabIndex = 18;
            this.button180.Text = "Delete Layout";
            this.button180.UseVisualStyleBackColor = true;
            // 
            // button181
            // 
            this.button181.Location = new System.Drawing.Point(568, 74);
            this.button181.Name = "button181";
            this.button181.Size = new System.Drawing.Size(130, 23);
            this.button181.TabIndex = 17;
            this.button181.Text = "Restore Original Layout";
            this.button181.UseVisualStyleBackColor = true;
            // 
            // button182
            // 
            this.button182.Location = new System.Drawing.Point(432, 45);
            this.button182.Name = "button182";
            this.button182.Size = new System.Drawing.Size(130, 23);
            this.button182.TabIndex = 16;
            this.button182.Text = "New Layout";
            this.button182.UseVisualStyleBackColor = true;
            // 
            // button183
            // 
            this.button183.Location = new System.Drawing.Point(432, 74);
            this.button183.Name = "button183";
            this.button183.Size = new System.Drawing.Size(130, 23);
            this.button183.TabIndex = 15;
            this.button183.Text = "Test Layout";
            this.button183.UseVisualStyleBackColor = true;
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.rdoJointDetailRecordTypeTrailer);
            this.groupBox26.Controls.Add(this.rdoJointDetailRecordTypeHeader);
            this.groupBox26.Location = new System.Drawing.Point(248, 45);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(146, 52);
            this.groupBox26.TabIndex = 14;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "Record Type";
            // 
            // rdoJointDetailRecordTypeTrailer
            // 
            this.rdoJointDetailRecordTypeTrailer.AutoSize = true;
            this.rdoJointDetailRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoJointDetailRecordTypeTrailer.Name = "rdoJointDetailRecordTypeTrailer";
            this.rdoJointDetailRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoJointDetailRecordTypeTrailer.TabIndex = 1;
            this.rdoJointDetailRecordTypeTrailer.TabStop = true;
            this.rdoJointDetailRecordTypeTrailer.Text = "Trailer";
            this.rdoJointDetailRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoJointDetailRecordTypeHeader
            // 
            this.rdoJointDetailRecordTypeHeader.AutoSize = true;
            this.rdoJointDetailRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoJointDetailRecordTypeHeader.Name = "rdoJointDetailRecordTypeHeader";
            this.rdoJointDetailRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoJointDetailRecordTypeHeader.TabIndex = 0;
            this.rdoJointDetailRecordTypeHeader.TabStop = true;
            this.rdoJointDetailRecordTypeHeader.Text = "Header";
            this.rdoJointDetailRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(3, 79);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(79, 13);
            this.label52.TabIndex = 13;
            this.label52.Text = "Current Layout:";
            // 
            // cboJointDetailLayouts
            // 
            this.cboJointDetailLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboJointDetailLayouts.FormattingEnabled = true;
            this.cboJointDetailLayouts.Location = new System.Drawing.Point(88, 76);
            this.cboJointDetailLayouts.Name = "cboJointDetailLayouts";
            this.cboJointDetailLayouts.Size = new System.Drawing.Size(156, 21);
            this.cboJointDetailLayouts.TabIndex = 12;
            // 
            // lvJointDetailFields
            // 
            this.lvJointDetailFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader107,
            this.columnHeader108,
            this.columnHeader109,
            this.columnHeader110,
            this.columnHeader111,
            this.columnHeader112,
            this.columnHeader113,
            this.columnHeader118,
            this.columnHeader119});
            this.lvJointDetailFields.FullRowSelect = true;
            this.lvJointDetailFields.GridLines = true;
            this.lvJointDetailFields.Location = new System.Drawing.Point(8, 173);
            this.lvJointDetailFields.Name = "lvJointDetailFields";
            this.lvJointDetailFields.Size = new System.Drawing.Size(704, 169);
            this.lvJointDetailFields.TabIndex = 11;
            this.lvJointDetailFields.UseCompatibleStateImageBehavior = false;
            this.lvJointDetailFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader107
            // 
            this.columnHeader107.Text = "Field Name";
            this.columnHeader107.Width = 105;
            // 
            // columnHeader108
            // 
            this.columnHeader108.Text = "Display Name";
            this.columnHeader108.Width = 96;
            // 
            // columnHeader109
            // 
            this.columnHeader109.Text = "Column Width";
            this.columnHeader109.Width = 85;
            // 
            // columnHeader110
            // 
            this.columnHeader110.Text = "Pad Char";
            this.columnHeader110.Width = 59;
            // 
            // columnHeader111
            // 
            this.columnHeader111.Text = "Justify";
            this.columnHeader111.Width = 45;
            // 
            // columnHeader112
            // 
            this.columnHeader112.Text = "Quotes";
            this.columnHeader112.Width = 69;
            // 
            // columnHeader113
            // 
            this.columnHeader113.Text = "Format";
            this.columnHeader113.Width = 87;
            // 
            // columnHeader118
            // 
            this.columnHeader118.Text = "Occurs Grp";
            this.columnHeader118.Width = 78;
            // 
            // columnHeader119
            // 
            this.columnHeader119.Text = "Occurs Cnt";
            this.columnHeader119.Width = 76;
            // 
            // textBox109
            // 
            this.textBox109.BackColor = System.Drawing.SystemColors.Window;
            this.textBox109.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox109.Location = new System.Drawing.Point(6, 153);
            this.textBox109.Name = "textBox109";
            this.textBox109.ReadOnly = true;
            this.textBox109.Size = new System.Drawing.Size(677, 13);
            this.textBox109.TabIndex = 10;
            this.textBox109.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox27
            // 
            this.groupBox27.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox27.Controls.Add(this.checkBox6);
            this.groupBox27.Controls.Add(this.textBox120);
            this.groupBox27.Controls.Add(this.textBox110);
            this.groupBox27.Controls.Add(this.button184);
            this.groupBox27.Controls.Add(this.button185);
            this.groupBox27.Controls.Add(this.button186);
            this.groupBox27.Controls.Add(this.button187);
            this.groupBox27.Controls.Add(this.button188);
            this.groupBox27.Controls.Add(this.label53);
            this.groupBox27.Controls.Add(this.listBox29);
            this.groupBox27.Location = new System.Drawing.Point(6, 348);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(782, 238);
            this.groupBox27.TabIndex = 0;
            this.groupBox27.TabStop = false;
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Location = new System.Drawing.Point(643, 147);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(136, 17);
            this.checkBox6.TabIndex = 15;
            this.checkBox6.Text = "Ignore Occurs Columns";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // textBox120
            // 
            this.textBox120.BackColor = System.Drawing.SystemColors.Window;
            this.textBox120.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox120.Location = new System.Drawing.Point(611, 50);
            this.textBox120.Multiline = true;
            this.textBox120.Name = "textBox120";
            this.textBox120.ReadOnly = true;
            this.textBox120.Size = new System.Drawing.Size(168, 86);
            this.textBox120.TabIndex = 14;
            this.textBox120.Text = "NOTE: Aggregate field values and field values for detail records will be represen" +
                "ted by placeholder values when testing layouts, since these fields are dependent" +
                " on data from other levels.";
            // 
            // textBox110
            // 
            this.textBox110.BackColor = System.Drawing.SystemColors.Window;
            this.textBox110.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox110.Location = new System.Drawing.Point(404, 50);
            this.textBox110.Multiline = true;
            this.textBox110.Name = "textBox110";
            this.textBox110.ReadOnly = true;
            this.textBox110.Size = new System.Drawing.Size(202, 62);
            this.textBox110.TabIndex = 13;
            this.textBox110.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button184
            // 
            this.button184.Location = new System.Drawing.Point(404, 118);
            this.button184.Name = "button184";
            this.button184.Size = new System.Drawing.Size(202, 23);
            this.button184.TabIndex = 12;
            this.button184.Text = "Remove Field";
            this.button184.UseVisualStyleBackColor = true;
            // 
            // button185
            // 
            this.button185.Location = new System.Drawing.Point(510, 147);
            this.button185.Name = "button185";
            this.button185.Size = new System.Drawing.Size(96, 23);
            this.button185.TabIndex = 11;
            this.button185.Text = "Down";
            this.button185.UseVisualStyleBackColor = true;
            // 
            // button186
            // 
            this.button186.Location = new System.Drawing.Point(404, 147);
            this.button186.Name = "button186";
            this.button186.Size = new System.Drawing.Size(96, 23);
            this.button186.TabIndex = 10;
            this.button186.Text = "Up";
            this.button186.UseVisualStyleBackColor = true;
            // 
            // button187
            // 
            this.button187.Location = new System.Drawing.Point(404, 176);
            this.button187.Name = "button187";
            this.button187.Size = new System.Drawing.Size(202, 23);
            this.button187.TabIndex = 9;
            this.button187.Text = "Restore Default Field Data";
            this.button187.UseVisualStyleBackColor = true;
            // 
            // button188
            // 
            this.button188.Location = new System.Drawing.Point(211, 202);
            this.button188.Name = "button188";
            this.button188.Size = new System.Drawing.Size(177, 23);
            this.button188.TabIndex = 6;
            this.button188.Text = "Add Field";
            this.button188.UseVisualStyleBackColor = true;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(208, 23);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(80, 13);
            this.label53.TabIndex = 4;
            this.label53.Text = "Standard Fields";
            // 
            // listBox29
            // 
            this.listBox29.FormattingEnabled = true;
            this.listBox29.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterFile",
            "ProcessingRunDate",
            "Static",
            "Time"});
            this.listBox29.Location = new System.Drawing.Point(211, 39);
            this.listBox29.Name = "listBox29";
            this.listBox29.Size = new System.Drawing.Size(177, 160);
            this.listBox29.TabIndex = 1;
            // 
            // panel21
            // 
            this.panel21.Controls.Add(this.comboBox14);
            this.panel21.Controls.Add(this.button198);
            this.panel21.Controls.Add(this.lstJointDetailColumns);
            this.panel21.Controls.Add(this.label55);
            this.panel21.Controls.Add(this.splitter9);
            this.panel21.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel21.Location = new System.Drawing.Point(0, 0);
            this.panel21.Name = "panel21";
            this.panel21.Size = new System.Drawing.Size(185, 618);
            this.panel21.TabIndex = 1;
            // 
            // comboBox14
            // 
            this.comboBox14.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Items.AddRange(new object[] {
            "Bank",
            "Customer",
            "Lockbox",
            "Batch",
            "Transactions",
            "Checks",
            "Stubs",
            "Documents"});
            this.comboBox14.Location = new System.Drawing.Point(6, 26);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(167, 21);
            this.comboBox14.TabIndex = 2;
            // 
            // button198
            // 
            this.button198.Location = new System.Drawing.Point(8, 515);
            this.button198.Name = "button198";
            this.button198.Size = new System.Drawing.Size(75, 23);
            this.button198.TabIndex = 3;
            this.button198.Text = "Add Field";
            this.button198.UseVisualStyleBackColor = true;
            // 
            // lstJointDetailColumns
            // 
            this.lstJointDetailColumns.FormattingEnabled = true;
            this.lstJointDetailColumns.Location = new System.Drawing.Point(8, 50);
            this.lstJointDetailColumns.Name = "lstJointDetailColumns";
            this.lstJointDetailColumns.Size = new System.Drawing.Size(165, 459);
            this.lstJointDetailColumns.TabIndex = 2;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(5, 10);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(60, 13);
            this.label55.TabIndex = 1;
            this.label55.Text = "Data Fields";
            // 
            // splitter9
            // 
            this.splitter9.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter9.Location = new System.Drawing.Point(182, 0);
            this.splitter9.Name = "splitter9";
            this.splitter9.Size = new System.Drawing.Size(3, 618);
            this.splitter9.TabIndex = 0;
            this.splitter9.TabStop = false;
            // 
            // tpDocument
            // 
            this.tpDocument.Controls.Add(this.tabControl9);
            this.tpDocument.Controls.Add(this.panel19);
            this.tpDocument.Location = new System.Drawing.Point(4, 22);
            this.tpDocument.Name = "tpDocument";
            this.tpDocument.Size = new System.Drawing.Size(1000, 618);
            this.tpDocument.TabIndex = 9;
            this.tpDocument.Text = "Document";
            this.tpDocument.UseVisualStyleBackColor = true;
            // 
            // tabControl9
            // 
            this.tabControl9.Controls.Add(this.tabPage36);
            this.tabControl9.Controls.Add(this.tabPage37);
            this.tabControl9.Controls.Add(this.tabPage38);
            this.tabControl9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl9.Location = new System.Drawing.Point(185, 0);
            this.tabControl9.Name = "tabControl9";
            this.tabControl9.SelectedIndex = 0;
            this.tabControl9.Size = new System.Drawing.Size(815, 618);
            this.tabControl9.TabIndex = 2;
            // 
            // tabPage36
            // 
            this.tabPage36.Controls.Add(this.panel18);
            this.tabPage36.Controls.Add(this.textBox97);
            this.tabPage36.Controls.Add(this.button161);
            this.tabPage36.Controls.Add(this.button162);
            this.tabPage36.Controls.Add(this.button163);
            this.tabPage36.Controls.Add(this.button164);
            this.tabPage36.Controls.Add(this.label48);
            this.tabPage36.Controls.Add(this.cboDocumentLayouts);
            this.tabPage36.Controls.Add(this.lvDocumentFields);
            this.tabPage36.Controls.Add(this.textBox98);
            this.tabPage36.Controls.Add(this.groupBox25);
            this.tabPage36.Location = new System.Drawing.Point(4, 22);
            this.tabPage36.Name = "tabPage36";
            this.tabPage36.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage36.Size = new System.Drawing.Size(807, 592);
            this.tabPage36.TabIndex = 0;
            this.tabPage36.Text = "Create Layouts";
            this.tabPage36.UseVisualStyleBackColor = true;
            // 
            // panel18
            // 
            this.panel18.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel18.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel18.ForeColor = System.Drawing.SystemColors.Control;
            this.panel18.Location = new System.Drawing.Point(6, 103);
            this.panel18.Name = "panel18";
            this.panel18.Size = new System.Drawing.Size(782, 1);
            this.panel18.TabIndex = 20;
            // 
            // textBox97
            // 
            this.textBox97.BackColor = System.Drawing.SystemColors.Window;
            this.textBox97.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox97.Location = new System.Drawing.Point(6, 13);
            this.textBox97.Name = "textBox97";
            this.textBox97.ReadOnly = true;
            this.textBox97.Size = new System.Drawing.Size(763, 13);
            this.textBox97.TabIndex = 19;
            this.textBox97.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button161
            // 
            this.button161.Location = new System.Drawing.Point(568, 45);
            this.button161.Name = "button161";
            this.button161.Size = new System.Drawing.Size(130, 23);
            this.button161.TabIndex = 18;
            this.button161.Text = "Delete Layout";
            this.button161.UseVisualStyleBackColor = true;
            // 
            // button162
            // 
            this.button162.Location = new System.Drawing.Point(568, 74);
            this.button162.Name = "button162";
            this.button162.Size = new System.Drawing.Size(130, 23);
            this.button162.TabIndex = 17;
            this.button162.Text = "Restore Original Layout";
            this.button162.UseVisualStyleBackColor = true;
            // 
            // button163
            // 
            this.button163.Location = new System.Drawing.Point(432, 45);
            this.button163.Name = "button163";
            this.button163.Size = new System.Drawing.Size(130, 23);
            this.button163.TabIndex = 16;
            this.button163.Text = "New Layout";
            this.button163.UseVisualStyleBackColor = true;
            // 
            // button164
            // 
            this.button164.Location = new System.Drawing.Point(432, 74);
            this.button164.Name = "button164";
            this.button164.Size = new System.Drawing.Size(130, 23);
            this.button164.TabIndex = 15;
            this.button164.Text = "Test Layout";
            this.button164.UseVisualStyleBackColor = true;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(3, 45);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(79, 13);
            this.label48.TabIndex = 13;
            this.label48.Text = "Current Layout:";
            // 
            // cboDocumentLayouts
            // 
            this.cboDocumentLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboDocumentLayouts.FormattingEnabled = true;
            this.cboDocumentLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboDocumentLayouts.Name = "cboDocumentLayouts";
            this.cboDocumentLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboDocumentLayouts.TabIndex = 12;
            // 
            // lvDocumentFields
            // 
            this.lvDocumentFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader96,
            this.columnHeader97,
            this.columnHeader98,
            this.columnHeader99,
            this.columnHeader100,
            this.columnHeader101,
            this.columnHeader102});
            this.lvDocumentFields.FullRowSelect = true;
            this.lvDocumentFields.GridLines = true;
            this.lvDocumentFields.Location = new System.Drawing.Point(8, 173);
            this.lvDocumentFields.Name = "lvDocumentFields";
            this.lvDocumentFields.Size = new System.Drawing.Size(651, 169);
            this.lvDocumentFields.TabIndex = 11;
            this.lvDocumentFields.UseCompatibleStateImageBehavior = false;
            this.lvDocumentFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader96
            // 
            this.columnHeader96.Text = "Field Name";
            this.columnHeader96.Width = 106;
            // 
            // columnHeader97
            // 
            this.columnHeader97.Text = "Display Name";
            this.columnHeader97.Width = 110;
            // 
            // columnHeader98
            // 
            this.columnHeader98.Text = "Column Width";
            this.columnHeader98.Width = 85;
            // 
            // columnHeader99
            // 
            this.columnHeader99.Text = "Pad Char";
            this.columnHeader99.Width = 59;
            // 
            // columnHeader100
            // 
            this.columnHeader100.Text = "Justify";
            this.columnHeader100.Width = 69;
            // 
            // columnHeader101
            // 
            this.columnHeader101.Text = "Quotes";
            this.columnHeader101.Width = 69;
            // 
            // columnHeader102
            // 
            this.columnHeader102.Text = "Format";
            this.columnHeader102.Width = 148;
            // 
            // textBox98
            // 
            this.textBox98.BackColor = System.Drawing.SystemColors.Window;
            this.textBox98.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox98.Location = new System.Drawing.Point(6, 153);
            this.textBox98.Name = "textBox98";
            this.textBox98.ReadOnly = true;
            this.textBox98.Size = new System.Drawing.Size(677, 13);
            this.textBox98.TabIndex = 10;
            this.textBox98.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox25
            // 
            this.groupBox25.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox25.Controls.Add(this.textBox99);
            this.groupBox25.Controls.Add(this.button165);
            this.groupBox25.Controls.Add(this.button166);
            this.groupBox25.Controls.Add(this.button167);
            this.groupBox25.Controls.Add(this.button168);
            this.groupBox25.Controls.Add(this.button169);
            this.groupBox25.Controls.Add(this.button170);
            this.groupBox25.Controls.Add(this.label49);
            this.groupBox25.Controls.Add(this.label50);
            this.groupBox25.Controls.Add(this.listBox26);
            this.groupBox25.Controls.Add(this.listBox27);
            this.groupBox25.Location = new System.Drawing.Point(6, 348);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(782, 238);
            this.groupBox25.TabIndex = 0;
            this.groupBox25.TabStop = false;
            // 
            // textBox99
            // 
            this.textBox99.BackColor = System.Drawing.SystemColors.Window;
            this.textBox99.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox99.Location = new System.Drawing.Point(419, 56);
            this.textBox99.Multiline = true;
            this.textBox99.Name = "textBox99";
            this.textBox99.ReadOnly = true;
            this.textBox99.Size = new System.Drawing.Size(202, 62);
            this.textBox99.TabIndex = 13;
            this.textBox99.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button165
            // 
            this.button165.Location = new System.Drawing.Point(419, 124);
            this.button165.Name = "button165";
            this.button165.Size = new System.Drawing.Size(202, 23);
            this.button165.TabIndex = 12;
            this.button165.Text = "Remove Field";
            this.button165.UseVisualStyleBackColor = true;
            // 
            // button166
            // 
            this.button166.Location = new System.Drawing.Point(525, 153);
            this.button166.Name = "button166";
            this.button166.Size = new System.Drawing.Size(96, 23);
            this.button166.TabIndex = 11;
            this.button166.Text = "Down";
            this.button166.UseVisualStyleBackColor = true;
            // 
            // button167
            // 
            this.button167.Location = new System.Drawing.Point(419, 153);
            this.button167.Name = "button167";
            this.button167.Size = new System.Drawing.Size(96, 23);
            this.button167.TabIndex = 10;
            this.button167.Text = "Up";
            this.button167.UseVisualStyleBackColor = true;
            // 
            // button168
            // 
            this.button168.Location = new System.Drawing.Point(419, 182);
            this.button168.Name = "button168";
            this.button168.Size = new System.Drawing.Size(202, 23);
            this.button168.TabIndex = 9;
            this.button168.Text = "Restore Default Field Data";
            this.button168.UseVisualStyleBackColor = true;
            // 
            // button169
            // 
            this.button169.Location = new System.Drawing.Point(211, 202);
            this.button169.Name = "button169";
            this.button169.Size = new System.Drawing.Size(177, 23);
            this.button169.TabIndex = 6;
            this.button169.Text = "Add Field";
            this.button169.UseVisualStyleBackColor = true;
            // 
            // button170
            // 
            this.button170.Location = new System.Drawing.Point(16, 202);
            this.button170.Name = "button170";
            this.button170.Size = new System.Drawing.Size(177, 23);
            this.button170.TabIndex = 5;
            this.button170.Text = "Add Field";
            this.button170.UseVisualStyleBackColor = true;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(208, 23);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(80, 13);
            this.label49.TabIndex = 4;
            this.label49.Text = "Standard Fields";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(13, 23);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(86, 13);
            this.label50.TabIndex = 3;
            this.label50.Text = "Aggregate Fields";
            // 
            // listBox26
            // 
            this.listBox26.FormattingEnabled = true;
            this.listBox26.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "ImageDocumentPerBatch",
            "ImageDocumentPerTransaction",
            "ImageDocumentSingleFront",
            "ImageDocumentSingleRear",
            "ImageFileSizeFrontD",
            "ImageFileSizeRearD",
            "LineCounterBank",
            "LineCounterBatch",
            "LineCounterCustomer",
            "LineCounterDocument",
            "LineCounterFile",
            "LineCounterLockbox",
            "LineCounterTransaction",
            "Static",
            "Time"});
            this.listBox26.Location = new System.Drawing.Point(211, 39);
            this.listBox26.Name = "listBox26";
            this.listBox26.Size = new System.Drawing.Size(177, 160);
            this.listBox26.TabIndex = 1;
            // 
            // listBox27
            // 
            this.listBox27.FormattingEnabled = true;
            this.listBox27.Items.AddRange(new object[] {
            "CountChecks",
            "CountStubs",
            "SumChecksAmount",
            "SumStubsAmount"});
            this.listBox27.Location = new System.Drawing.Point(16, 39);
            this.listBox27.Name = "listBox27";
            this.listBox27.Size = new System.Drawing.Size(177, 160);
            this.listBox27.TabIndex = 0;
            // 
            // tabPage37
            // 
            this.tabPage37.Controls.Add(this.textBox100);
            this.tabPage37.Controls.Add(this.textBox101);
            this.tabPage37.Controls.Add(this.textBox102);
            this.tabPage37.Controls.Add(this.button171);
            this.tabPage37.Controls.Add(this.button172);
            this.tabPage37.Controls.Add(this.button173);
            this.tabPage37.Controls.Add(this.lvDocumentsOrderByColumns);
            this.tabPage37.Location = new System.Drawing.Point(4, 22);
            this.tabPage37.Name = "tabPage37";
            this.tabPage37.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage37.Size = new System.Drawing.Size(807, 592);
            this.tabPage37.TabIndex = 1;
            this.tabPage37.Text = "Order Data";
            this.tabPage37.UseVisualStyleBackColor = true;
            // 
            // textBox100
            // 
            this.textBox100.BackColor = System.Drawing.SystemColors.Window;
            this.textBox100.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox100.Location = new System.Drawing.Point(12, 54);
            this.textBox100.Multiline = true;
            this.textBox100.Name = "textBox100";
            this.textBox100.ReadOnly = true;
            this.textBox100.Size = new System.Drawing.Size(473, 33);
            this.textBox100.TabIndex = 20;
            this.textBox100.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox101
            // 
            this.textBox101.BackColor = System.Drawing.SystemColors.Window;
            this.textBox101.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox101.Location = new System.Drawing.Point(12, 12);
            this.textBox101.Multiline = true;
            this.textBox101.Name = "textBox101";
            this.textBox101.ReadOnly = true;
            this.textBox101.Size = new System.Drawing.Size(473, 33);
            this.textBox101.TabIndex = 19;
            this.textBox101.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox102
            // 
            this.textBox102.BackColor = System.Drawing.SystemColors.Window;
            this.textBox102.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox102.Location = new System.Drawing.Point(492, 99);
            this.textBox102.Multiline = true;
            this.textBox102.Name = "textBox102";
            this.textBox102.ReadOnly = true;
            this.textBox102.Size = new System.Drawing.Size(202, 62);
            this.textBox102.TabIndex = 18;
            this.textBox102.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button171
            // 
            this.button171.Location = new System.Drawing.Point(492, 167);
            this.button171.Name = "button171";
            this.button171.Size = new System.Drawing.Size(202, 23);
            this.button171.TabIndex = 17;
            this.button171.Text = "Remove Field";
            this.button171.UseVisualStyleBackColor = true;
            // 
            // button172
            // 
            this.button172.Location = new System.Drawing.Point(598, 196);
            this.button172.Name = "button172";
            this.button172.Size = new System.Drawing.Size(96, 23);
            this.button172.TabIndex = 16;
            this.button172.Text = "Down";
            this.button172.UseVisualStyleBackColor = true;
            // 
            // button173
            // 
            this.button173.Location = new System.Drawing.Point(492, 196);
            this.button173.Name = "button173";
            this.button173.Size = new System.Drawing.Size(96, 23);
            this.button173.TabIndex = 15;
            this.button173.Text = "Up";
            this.button173.UseVisualStyleBackColor = true;
            // 
            // lvDocumentsOrderByColumns
            // 
            this.lvDocumentsOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader103,
            this.columnHeader104});
            this.lvDocumentsOrderByColumns.FullRowSelect = true;
            this.lvDocumentsOrderByColumns.GridLines = true;
            this.lvDocumentsOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvDocumentsOrderByColumns.Name = "lvDocumentsOrderByColumns";
            this.lvDocumentsOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvDocumentsOrderByColumns.TabIndex = 12;
            this.lvDocumentsOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvDocumentsOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader103
            // 
            this.columnHeader103.Text = "Field Name";
            this.columnHeader103.Width = 345;
            // 
            // columnHeader104
            // 
            this.columnHeader104.Text = "Asc/Desc";
            this.columnHeader104.Width = 120;
            // 
            // tabPage38
            // 
            this.tabPage38.Controls.Add(this.textBox103);
            this.tabPage38.Controls.Add(this.textBox104);
            this.tabPage38.Controls.Add(this.button174);
            this.tabPage38.Controls.Add(this.button175);
            this.tabPage38.Controls.Add(this.textBox105);
            this.tabPage38.Controls.Add(this.textBox106);
            this.tabPage38.Controls.Add(this.textBox107);
            this.tabPage38.Controls.Add(this.button176);
            this.tabPage38.Controls.Add(this.button177);
            this.tabPage38.Controls.Add(this.button178);
            this.tabPage38.Controls.Add(this.listView27);
            this.tabPage38.Location = new System.Drawing.Point(4, 22);
            this.tabPage38.Name = "tabPage38";
            this.tabPage38.Size = new System.Drawing.Size(807, 592);
            this.tabPage38.TabIndex = 2;
            this.tabPage38.Text = "Limit Data";
            this.tabPage38.UseVisualStyleBackColor = true;
            // 
            // textBox103
            // 
            this.textBox103.BackColor = System.Drawing.SystemColors.Window;
            this.textBox103.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox103.Location = new System.Drawing.Point(12, 360);
            this.textBox103.Multiline = true;
            this.textBox103.Name = "textBox103";
            this.textBox103.Size = new System.Drawing.Size(473, 18);
            this.textBox103.TabIndex = 31;
            this.textBox103.Text = "\"Where clause\" as text";
            // 
            // textBox104
            // 
            this.textBox104.Location = new System.Drawing.Point(12, 384);
            this.textBox104.Multiline = true;
            this.textBox104.Name = "textBox104";
            this.textBox104.ReadOnly = true;
            this.textBox104.Size = new System.Drawing.Size(473, 123);
            this.textBox104.TabIndex = 30;
            // 
            // button174
            // 
            this.button174.Location = new System.Drawing.Point(492, 225);
            this.button174.Name = "button174";
            this.button174.Size = new System.Drawing.Size(202, 23);
            this.button174.TabIndex = 29;
            this.button174.Text = "View as Text";
            this.button174.UseVisualStyleBackColor = true;
            // 
            // button175
            // 
            this.button175.Location = new System.Drawing.Point(492, 254);
            this.button175.Name = "button175";
            this.button175.Size = new System.Drawing.Size(202, 23);
            this.button175.TabIndex = 28;
            this.button175.Text = "Validate";
            this.button175.UseVisualStyleBackColor = true;
            // 
            // textBox105
            // 
            this.textBox105.BackColor = System.Drawing.SystemColors.Window;
            this.textBox105.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox105.Location = new System.Drawing.Point(12, 66);
            this.textBox105.Multiline = true;
            this.textBox105.Name = "textBox105";
            this.textBox105.ReadOnly = true;
            this.textBox105.Size = new System.Drawing.Size(473, 18);
            this.textBox105.TabIndex = 27;
            this.textBox105.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox106
            // 
            this.textBox106.BackColor = System.Drawing.SystemColors.Window;
            this.textBox106.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox106.Location = new System.Drawing.Point(12, 12);
            this.textBox106.Multiline = true;
            this.textBox106.Name = "textBox106";
            this.textBox106.ReadOnly = true;
            this.textBox106.Size = new System.Drawing.Size(473, 33);
            this.textBox106.TabIndex = 26;
            this.textBox106.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox107
            // 
            this.textBox107.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox107.Location = new System.Drawing.Point(492, 99);
            this.textBox107.Multiline = true;
            this.textBox107.Name = "textBox107";
            this.textBox107.Size = new System.Drawing.Size(202, 62);
            this.textBox107.TabIndex = 25;
            this.textBox107.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button176
            // 
            this.button176.Location = new System.Drawing.Point(492, 167);
            this.button176.Name = "button176";
            this.button176.Size = new System.Drawing.Size(202, 23);
            this.button176.TabIndex = 24;
            this.button176.Text = "Remove Field";
            this.button176.UseVisualStyleBackColor = true;
            // 
            // button177
            // 
            this.button177.Location = new System.Drawing.Point(598, 196);
            this.button177.Name = "button177";
            this.button177.Size = new System.Drawing.Size(96, 23);
            this.button177.TabIndex = 23;
            this.button177.Text = "Down";
            this.button177.UseVisualStyleBackColor = true;
            // 
            // button178
            // 
            this.button178.Location = new System.Drawing.Point(492, 196);
            this.button178.Name = "button178";
            this.button178.Size = new System.Drawing.Size(96, 23);
            this.button178.TabIndex = 22;
            this.button178.Text = "Up";
            this.button178.UseVisualStyleBackColor = true;
            // 
            // listView27
            // 
            this.listView27.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader105,
            this.columnHeader106});
            this.listView27.FullRowSelect = true;
            this.listView27.GridLines = true;
            this.listView27.Location = new System.Drawing.Point(12, 90);
            this.listView27.Name = "listView27";
            this.listView27.Size = new System.Drawing.Size(473, 227);
            this.listView27.TabIndex = 21;
            this.listView27.UseCompatibleStateImageBehavior = false;
            this.listView27.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader105
            // 
            this.columnHeader105.Text = "Field Name";
            this.columnHeader105.Width = 345;
            // 
            // columnHeader106
            // 
            this.columnHeader106.Text = "Asc/Desc";
            this.columnHeader106.Width = 120;
            // 
            // panel19
            // 
            this.panel19.Controls.Add(this.button179);
            this.panel19.Controls.Add(this.lstDocumentColumns);
            this.panel19.Controls.Add(this.label51);
            this.panel19.Controls.Add(this.splitter8);
            this.panel19.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel19.Location = new System.Drawing.Point(0, 0);
            this.panel19.Name = "panel19";
            this.panel19.Size = new System.Drawing.Size(185, 618);
            this.panel19.TabIndex = 1;
            // 
            // button179
            // 
            this.button179.Location = new System.Drawing.Point(8, 515);
            this.button179.Name = "button179";
            this.button179.Size = new System.Drawing.Size(75, 23);
            this.button179.TabIndex = 3;
            this.button179.Text = "Add Field";
            this.button179.UseVisualStyleBackColor = true;
            // 
            // lstDocumentColumns
            // 
            this.lstDocumentColumns.FormattingEnabled = true;
            this.lstDocumentColumns.Location = new System.Drawing.Point(8, 24);
            this.lstDocumentColumns.Name = "lstDocumentColumns";
            this.lstDocumentColumns.Size = new System.Drawing.Size(165, 485);
            this.lstDocumentColumns.TabIndex = 2;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(5, 10);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(60, 13);
            this.label51.TabIndex = 1;
            this.label51.Text = "Data Fields";
            // 
            // splitter8
            // 
            this.splitter8.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter8.Location = new System.Drawing.Point(182, 0);
            this.splitter8.Name = "splitter8";
            this.splitter8.Size = new System.Drawing.Size(3, 618);
            this.splitter8.TabIndex = 0;
            this.splitter8.TabStop = false;
            // 
            // tpStub
            // 
            this.tpStub.Controls.Add(this.tabControl8);
            this.tpStub.Controls.Add(this.panel17);
            this.tpStub.Location = new System.Drawing.Point(4, 22);
            this.tpStub.Name = "tpStub";
            this.tpStub.Size = new System.Drawing.Size(1000, 618);
            this.tpStub.TabIndex = 8;
            this.tpStub.Text = "Stub";
            this.tpStub.UseVisualStyleBackColor = true;
            // 
            // tabControl8
            // 
            this.tabControl8.Controls.Add(this.tabPage33);
            this.tabControl8.Controls.Add(this.tabPage34);
            this.tabControl8.Controls.Add(this.tabPage35);
            this.tabControl8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl8.Location = new System.Drawing.Point(185, 0);
            this.tabControl8.Name = "tabControl8";
            this.tabControl8.SelectedIndex = 0;
            this.tabControl8.Size = new System.Drawing.Size(815, 618);
            this.tabControl8.TabIndex = 2;
            // 
            // tabPage33
            // 
            this.tabPage33.Controls.Add(this.checkBox7);
            this.tabPage33.Controls.Add(this.panel16);
            this.tabPage33.Controls.Add(this.textBox86);
            this.tabPage33.Controls.Add(this.button142);
            this.tabPage33.Controls.Add(this.button143);
            this.tabPage33.Controls.Add(this.button144);
            this.tabPage33.Controls.Add(this.button145);
            this.tabPage33.Controls.Add(this.label44);
            this.tabPage33.Controls.Add(this.cboStubLayouts);
            this.tabPage33.Controls.Add(this.lvStubFields);
            this.tabPage33.Controls.Add(this.textBox87);
            this.tabPage33.Controls.Add(this.groupBox23);
            this.tabPage33.Location = new System.Drawing.Point(4, 22);
            this.tabPage33.Name = "tabPage33";
            this.tabPage33.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage33.Size = new System.Drawing.Size(807, 592);
            this.tabPage33.TabIndex = 0;
            this.tabPage33.Text = "Create Layouts";
            this.tabPage33.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(47, 84);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(186, 17);
            this.checkBox7.TabIndex = 21;
            this.checkBox7.Text = "Include rows with all NULL values";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // panel16
            // 
            this.panel16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel16.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel16.ForeColor = System.Drawing.SystemColors.Control;
            this.panel16.Location = new System.Drawing.Point(6, 103);
            this.panel16.Name = "panel16";
            this.panel16.Size = new System.Drawing.Size(782, 1);
            this.panel16.TabIndex = 20;
            // 
            // textBox86
            // 
            this.textBox86.BackColor = System.Drawing.SystemColors.Window;
            this.textBox86.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox86.Location = new System.Drawing.Point(6, 13);
            this.textBox86.Name = "textBox86";
            this.textBox86.ReadOnly = true;
            this.textBox86.Size = new System.Drawing.Size(763, 13);
            this.textBox86.TabIndex = 19;
            this.textBox86.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button142
            // 
            this.button142.Location = new System.Drawing.Point(568, 45);
            this.button142.Name = "button142";
            this.button142.Size = new System.Drawing.Size(130, 23);
            this.button142.TabIndex = 18;
            this.button142.Text = "Delete Layout";
            this.button142.UseVisualStyleBackColor = true;
            // 
            // button143
            // 
            this.button143.Location = new System.Drawing.Point(568, 74);
            this.button143.Name = "button143";
            this.button143.Size = new System.Drawing.Size(130, 23);
            this.button143.TabIndex = 17;
            this.button143.Text = "Restore Original Layout";
            this.button143.UseVisualStyleBackColor = true;
            // 
            // button144
            // 
            this.button144.Location = new System.Drawing.Point(432, 45);
            this.button144.Name = "button144";
            this.button144.Size = new System.Drawing.Size(130, 23);
            this.button144.TabIndex = 16;
            this.button144.Text = "New Layout";
            this.button144.UseVisualStyleBackColor = true;
            // 
            // button145
            // 
            this.button145.Location = new System.Drawing.Point(432, 74);
            this.button145.Name = "button145";
            this.button145.Size = new System.Drawing.Size(130, 23);
            this.button145.TabIndex = 15;
            this.button145.Text = "Test Layout";
            this.button145.UseVisualStyleBackColor = true;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(3, 45);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(79, 13);
            this.label44.TabIndex = 13;
            this.label44.Text = "Current Layout:";
            // 
            // cboStubLayouts
            // 
            this.cboStubLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboStubLayouts.FormattingEnabled = true;
            this.cboStubLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboStubLayouts.Name = "cboStubLayouts";
            this.cboStubLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboStubLayouts.TabIndex = 12;
            // 
            // lvStubFields
            // 
            this.lvStubFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader85,
            this.columnHeader86,
            this.columnHeader87,
            this.columnHeader88,
            this.columnHeader89,
            this.columnHeader90,
            this.columnHeader91});
            this.lvStubFields.FullRowSelect = true;
            this.lvStubFields.GridLines = true;
            this.lvStubFields.Location = new System.Drawing.Point(8, 173);
            this.lvStubFields.Name = "lvStubFields";
            this.lvStubFields.Size = new System.Drawing.Size(652, 169);
            this.lvStubFields.TabIndex = 11;
            this.lvStubFields.UseCompatibleStateImageBehavior = false;
            this.lvStubFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader85
            // 
            this.columnHeader85.Text = "Field Name";
            this.columnHeader85.Width = 120;
            // 
            // columnHeader86
            // 
            this.columnHeader86.Text = "Display Name";
            this.columnHeader86.Width = 120;
            // 
            // columnHeader87
            // 
            this.columnHeader87.Text = "Column Width";
            this.columnHeader87.Width = 85;
            // 
            // columnHeader88
            // 
            this.columnHeader88.Text = "Pad Char";
            this.columnHeader88.Width = 59;
            // 
            // columnHeader89
            // 
            this.columnHeader89.Text = "Justify";
            this.columnHeader89.Width = 45;
            // 
            // columnHeader90
            // 
            this.columnHeader90.Text = "Quotes";
            this.columnHeader90.Width = 69;
            // 
            // columnHeader91
            // 
            this.columnHeader91.Text = "Format";
            this.columnHeader91.Width = 150;
            // 
            // textBox87
            // 
            this.textBox87.BackColor = System.Drawing.SystemColors.Window;
            this.textBox87.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox87.Location = new System.Drawing.Point(6, 153);
            this.textBox87.Name = "textBox87";
            this.textBox87.ReadOnly = true;
            this.textBox87.Size = new System.Drawing.Size(677, 13);
            this.textBox87.TabIndex = 10;
            this.textBox87.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox23
            // 
            this.groupBox23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox23.Controls.Add(this.textBox88);
            this.groupBox23.Controls.Add(this.button146);
            this.groupBox23.Controls.Add(this.button147);
            this.groupBox23.Controls.Add(this.button148);
            this.groupBox23.Controls.Add(this.button149);
            this.groupBox23.Controls.Add(this.button150);
            this.groupBox23.Controls.Add(this.button151);
            this.groupBox23.Controls.Add(this.label45);
            this.groupBox23.Controls.Add(this.label46);
            this.groupBox23.Controls.Add(this.listBox23);
            this.groupBox23.Controls.Add(this.listBox24);
            this.groupBox23.Location = new System.Drawing.Point(6, 348);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(782, 238);
            this.groupBox23.TabIndex = 0;
            this.groupBox23.TabStop = false;
            // 
            // textBox88
            // 
            this.textBox88.BackColor = System.Drawing.SystemColors.Window;
            this.textBox88.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox88.Location = new System.Drawing.Point(419, 56);
            this.textBox88.Multiline = true;
            this.textBox88.Name = "textBox88";
            this.textBox88.ReadOnly = true;
            this.textBox88.Size = new System.Drawing.Size(202, 62);
            this.textBox88.TabIndex = 13;
            this.textBox88.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button146
            // 
            this.button146.Location = new System.Drawing.Point(419, 124);
            this.button146.Name = "button146";
            this.button146.Size = new System.Drawing.Size(202, 23);
            this.button146.TabIndex = 12;
            this.button146.Text = "Remove Field";
            this.button146.UseVisualStyleBackColor = true;
            // 
            // button147
            // 
            this.button147.Location = new System.Drawing.Point(525, 153);
            this.button147.Name = "button147";
            this.button147.Size = new System.Drawing.Size(96, 23);
            this.button147.TabIndex = 11;
            this.button147.Text = "Down";
            this.button147.UseVisualStyleBackColor = true;
            // 
            // button148
            // 
            this.button148.Location = new System.Drawing.Point(419, 153);
            this.button148.Name = "button148";
            this.button148.Size = new System.Drawing.Size(96, 23);
            this.button148.TabIndex = 10;
            this.button148.Text = "Up";
            this.button148.UseVisualStyleBackColor = true;
            // 
            // button149
            // 
            this.button149.Location = new System.Drawing.Point(419, 182);
            this.button149.Name = "button149";
            this.button149.Size = new System.Drawing.Size(202, 23);
            this.button149.TabIndex = 9;
            this.button149.Text = "Restore Default Field Data";
            this.button149.UseVisualStyleBackColor = true;
            // 
            // button150
            // 
            this.button150.Location = new System.Drawing.Point(211, 202);
            this.button150.Name = "button150";
            this.button150.Size = new System.Drawing.Size(177, 23);
            this.button150.TabIndex = 6;
            this.button150.Text = "Add Field";
            this.button150.UseVisualStyleBackColor = true;
            // 
            // button151
            // 
            this.button151.Location = new System.Drawing.Point(16, 202);
            this.button151.Name = "button151";
            this.button151.Size = new System.Drawing.Size(177, 23);
            this.button151.TabIndex = 5;
            this.button151.Text = "Add Field";
            this.button151.UseVisualStyleBackColor = true;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(208, 23);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(80, 13);
            this.label45.TabIndex = 4;
            this.label45.Text = "Standard Fields";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(13, 23);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(86, 13);
            this.label46.TabIndex = 3;
            this.label46.Text = "Aggregate Fields";
            // 
            // listBox23
            // 
            this.listBox23.FormattingEnabled = true;
            this.listBox23.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "ImageFileName",
            "ImageFileSizeFrontS",
            "ImageFileSizeRearS",
            "ImageStubSingleFront",
            "ImageStubSingleRear",
            "LineCounterBank",
            "LineCounterBatch",
            "LineCounterCustomer",
            "LineCounterFile",
            "LineCounterLockbox",
            "LineCounterStub",
            "LineCounterTransaction",
            "Static",
            "Time"});
            this.listBox23.Location = new System.Drawing.Point(211, 39);
            this.listBox23.Name = "listBox23";
            this.listBox23.Size = new System.Drawing.Size(177, 160);
            this.listBox23.TabIndex = 1;
            // 
            // listBox24
            // 
            this.listBox24.FormattingEnabled = true;
            this.listBox24.Items.AddRange(new object[] {
            "CountChecks",
            "CountStubs",
            "SumChecksAmount",
            "SumStubsAmount"});
            this.listBox24.Location = new System.Drawing.Point(16, 39);
            this.listBox24.Name = "listBox24";
            this.listBox24.Size = new System.Drawing.Size(177, 160);
            this.listBox24.TabIndex = 0;
            // 
            // tabPage34
            // 
            this.tabPage34.Controls.Add(this.textBox89);
            this.tabPage34.Controls.Add(this.textBox90);
            this.tabPage34.Controls.Add(this.textBox91);
            this.tabPage34.Controls.Add(this.button152);
            this.tabPage34.Controls.Add(this.button153);
            this.tabPage34.Controls.Add(this.button154);
            this.tabPage34.Controls.Add(this.lvStubsOrderByColumns);
            this.tabPage34.Location = new System.Drawing.Point(4, 22);
            this.tabPage34.Name = "tabPage34";
            this.tabPage34.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage34.Size = new System.Drawing.Size(807, 592);
            this.tabPage34.TabIndex = 1;
            this.tabPage34.Text = "Order Data";
            this.tabPage34.UseVisualStyleBackColor = true;
            // 
            // textBox89
            // 
            this.textBox89.BackColor = System.Drawing.SystemColors.Window;
            this.textBox89.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox89.Location = new System.Drawing.Point(12, 54);
            this.textBox89.Multiline = true;
            this.textBox89.Name = "textBox89";
            this.textBox89.ReadOnly = true;
            this.textBox89.Size = new System.Drawing.Size(473, 33);
            this.textBox89.TabIndex = 20;
            this.textBox89.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox90
            // 
            this.textBox90.BackColor = System.Drawing.SystemColors.Window;
            this.textBox90.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox90.Location = new System.Drawing.Point(12, 12);
            this.textBox90.Multiline = true;
            this.textBox90.Name = "textBox90";
            this.textBox90.ReadOnly = true;
            this.textBox90.Size = new System.Drawing.Size(473, 33);
            this.textBox90.TabIndex = 19;
            this.textBox90.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox91
            // 
            this.textBox91.BackColor = System.Drawing.SystemColors.Window;
            this.textBox91.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox91.Location = new System.Drawing.Point(492, 99);
            this.textBox91.Multiline = true;
            this.textBox91.Name = "textBox91";
            this.textBox91.ReadOnly = true;
            this.textBox91.Size = new System.Drawing.Size(202, 62);
            this.textBox91.TabIndex = 18;
            this.textBox91.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button152
            // 
            this.button152.Location = new System.Drawing.Point(492, 167);
            this.button152.Name = "button152";
            this.button152.Size = new System.Drawing.Size(202, 23);
            this.button152.TabIndex = 17;
            this.button152.Text = "Remove Field";
            this.button152.UseVisualStyleBackColor = true;
            // 
            // button153
            // 
            this.button153.Location = new System.Drawing.Point(598, 196);
            this.button153.Name = "button153";
            this.button153.Size = new System.Drawing.Size(96, 23);
            this.button153.TabIndex = 16;
            this.button153.Text = "Down";
            this.button153.UseVisualStyleBackColor = true;
            // 
            // button154
            // 
            this.button154.Location = new System.Drawing.Point(492, 196);
            this.button154.Name = "button154";
            this.button154.Size = new System.Drawing.Size(96, 23);
            this.button154.TabIndex = 15;
            this.button154.Text = "Up";
            this.button154.UseVisualStyleBackColor = true;
            // 
            // lvStubsOrderByColumns
            // 
            this.lvStubsOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader92,
            this.columnHeader93});
            this.lvStubsOrderByColumns.FullRowSelect = true;
            this.lvStubsOrderByColumns.GridLines = true;
            this.lvStubsOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvStubsOrderByColumns.Name = "lvStubsOrderByColumns";
            this.lvStubsOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvStubsOrderByColumns.TabIndex = 12;
            this.lvStubsOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvStubsOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader92
            // 
            this.columnHeader92.Text = "Field Name";
            this.columnHeader92.Width = 345;
            // 
            // columnHeader93
            // 
            this.columnHeader93.Text = "Asc/Desc";
            this.columnHeader93.Width = 120;
            // 
            // tabPage35
            // 
            this.tabPage35.Controls.Add(this.textBox92);
            this.tabPage35.Controls.Add(this.textBox93);
            this.tabPage35.Controls.Add(this.button155);
            this.tabPage35.Controls.Add(this.button156);
            this.tabPage35.Controls.Add(this.textBox94);
            this.tabPage35.Controls.Add(this.textBox95);
            this.tabPage35.Controls.Add(this.textBox96);
            this.tabPage35.Controls.Add(this.button157);
            this.tabPage35.Controls.Add(this.button158);
            this.tabPage35.Controls.Add(this.button159);
            this.tabPage35.Controls.Add(this.listView24);
            this.tabPage35.Location = new System.Drawing.Point(4, 22);
            this.tabPage35.Name = "tabPage35";
            this.tabPage35.Size = new System.Drawing.Size(807, 592);
            this.tabPage35.TabIndex = 2;
            this.tabPage35.Text = "Limit Data";
            this.tabPage35.UseVisualStyleBackColor = true;
            // 
            // textBox92
            // 
            this.textBox92.BackColor = System.Drawing.SystemColors.Window;
            this.textBox92.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox92.Location = new System.Drawing.Point(12, 360);
            this.textBox92.Multiline = true;
            this.textBox92.Name = "textBox92";
            this.textBox92.Size = new System.Drawing.Size(473, 18);
            this.textBox92.TabIndex = 31;
            this.textBox92.Text = "\"Where clause\" as text";
            // 
            // textBox93
            // 
            this.textBox93.Location = new System.Drawing.Point(12, 384);
            this.textBox93.Multiline = true;
            this.textBox93.Name = "textBox93";
            this.textBox93.ReadOnly = true;
            this.textBox93.Size = new System.Drawing.Size(473, 123);
            this.textBox93.TabIndex = 30;
            // 
            // button155
            // 
            this.button155.Location = new System.Drawing.Point(492, 225);
            this.button155.Name = "button155";
            this.button155.Size = new System.Drawing.Size(202, 23);
            this.button155.TabIndex = 29;
            this.button155.Text = "View as Text";
            this.button155.UseVisualStyleBackColor = true;
            // 
            // button156
            // 
            this.button156.Location = new System.Drawing.Point(492, 254);
            this.button156.Name = "button156";
            this.button156.Size = new System.Drawing.Size(202, 23);
            this.button156.TabIndex = 28;
            this.button156.Text = "Validate";
            this.button156.UseVisualStyleBackColor = true;
            // 
            // textBox94
            // 
            this.textBox94.BackColor = System.Drawing.SystemColors.Window;
            this.textBox94.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox94.Location = new System.Drawing.Point(12, 66);
            this.textBox94.Multiline = true;
            this.textBox94.Name = "textBox94";
            this.textBox94.ReadOnly = true;
            this.textBox94.Size = new System.Drawing.Size(473, 18);
            this.textBox94.TabIndex = 27;
            this.textBox94.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox95
            // 
            this.textBox95.BackColor = System.Drawing.SystemColors.Window;
            this.textBox95.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox95.Location = new System.Drawing.Point(12, 12);
            this.textBox95.Multiline = true;
            this.textBox95.Name = "textBox95";
            this.textBox95.ReadOnly = true;
            this.textBox95.Size = new System.Drawing.Size(473, 33);
            this.textBox95.TabIndex = 26;
            this.textBox95.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox96
            // 
            this.textBox96.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox96.Location = new System.Drawing.Point(492, 99);
            this.textBox96.Multiline = true;
            this.textBox96.Name = "textBox96";
            this.textBox96.Size = new System.Drawing.Size(202, 62);
            this.textBox96.TabIndex = 25;
            this.textBox96.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button157
            // 
            this.button157.Location = new System.Drawing.Point(492, 167);
            this.button157.Name = "button157";
            this.button157.Size = new System.Drawing.Size(202, 23);
            this.button157.TabIndex = 24;
            this.button157.Text = "Remove Field";
            this.button157.UseVisualStyleBackColor = true;
            // 
            // button158
            // 
            this.button158.Location = new System.Drawing.Point(598, 196);
            this.button158.Name = "button158";
            this.button158.Size = new System.Drawing.Size(96, 23);
            this.button158.TabIndex = 23;
            this.button158.Text = "Down";
            this.button158.UseVisualStyleBackColor = true;
            // 
            // button159
            // 
            this.button159.Location = new System.Drawing.Point(492, 196);
            this.button159.Name = "button159";
            this.button159.Size = new System.Drawing.Size(96, 23);
            this.button159.TabIndex = 22;
            this.button159.Text = "Up";
            this.button159.UseVisualStyleBackColor = true;
            // 
            // listView24
            // 
            this.listView24.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader94,
            this.columnHeader95});
            this.listView24.FullRowSelect = true;
            this.listView24.GridLines = true;
            this.listView24.Location = new System.Drawing.Point(12, 90);
            this.listView24.Name = "listView24";
            this.listView24.Size = new System.Drawing.Size(473, 227);
            this.listView24.TabIndex = 21;
            this.listView24.UseCompatibleStateImageBehavior = false;
            this.listView24.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader94
            // 
            this.columnHeader94.Text = "Field Name";
            this.columnHeader94.Width = 345;
            // 
            // columnHeader95
            // 
            this.columnHeader95.Text = "Asc/Desc";
            this.columnHeader95.Width = 120;
            // 
            // panel17
            // 
            this.panel17.Controls.Add(this.button160);
            this.panel17.Controls.Add(this.lstStubColumns);
            this.panel17.Controls.Add(this.label47);
            this.panel17.Controls.Add(this.splitter7);
            this.panel17.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel17.Location = new System.Drawing.Point(0, 0);
            this.panel17.Name = "panel17";
            this.panel17.Size = new System.Drawing.Size(185, 618);
            this.panel17.TabIndex = 1;
            // 
            // button160
            // 
            this.button160.Location = new System.Drawing.Point(8, 515);
            this.button160.Name = "button160";
            this.button160.Size = new System.Drawing.Size(75, 23);
            this.button160.TabIndex = 3;
            this.button160.Text = "Add Field";
            this.button160.UseVisualStyleBackColor = true;
            // 
            // lstStubColumns
            // 
            this.lstStubColumns.FormattingEnabled = true;
            this.lstStubColumns.Location = new System.Drawing.Point(8, 24);
            this.lstStubColumns.Name = "lstStubColumns";
            this.lstStubColumns.Size = new System.Drawing.Size(165, 485);
            this.lstStubColumns.TabIndex = 2;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(5, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(60, 13);
            this.label47.TabIndex = 1;
            this.label47.Text = "Data Fields";
            // 
            // splitter7
            // 
            this.splitter7.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter7.Location = new System.Drawing.Point(182, 0);
            this.splitter7.Name = "splitter7";
            this.splitter7.Size = new System.Drawing.Size(3, 618);
            this.splitter7.TabIndex = 0;
            this.splitter7.TabStop = false;
            // 
            // tpCheck
            // 
            this.tpCheck.Controls.Add(this.tabControl7);
            this.tpCheck.Controls.Add(this.panel15);
            this.tpCheck.Location = new System.Drawing.Point(4, 22);
            this.tpCheck.Name = "tpCheck";
            this.tpCheck.Size = new System.Drawing.Size(1000, 618);
            this.tpCheck.TabIndex = 7;
            this.tpCheck.Text = "Check";
            this.tpCheck.UseVisualStyleBackColor = true;
            // 
            // tabControl7
            // 
            this.tabControl7.Controls.Add(this.tabPage30);
            this.tabControl7.Controls.Add(this.tabPage31);
            this.tabControl7.Controls.Add(this.tabPage32);
            this.tabControl7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl7.Location = new System.Drawing.Point(185, 0);
            this.tabControl7.Name = "tabControl7";
            this.tabControl7.SelectedIndex = 0;
            this.tabControl7.Size = new System.Drawing.Size(815, 618);
            this.tabControl7.TabIndex = 2;
            // 
            // tabPage30
            // 
            this.tabPage30.Controls.Add(this.panel14);
            this.tabPage30.Controls.Add(this.textBox75);
            this.tabPage30.Controls.Add(this.button123);
            this.tabPage30.Controls.Add(this.button124);
            this.tabPage30.Controls.Add(this.button125);
            this.tabPage30.Controls.Add(this.button126);
            this.tabPage30.Controls.Add(this.label40);
            this.tabPage30.Controls.Add(this.cboCheckLayouts);
            this.tabPage30.Controls.Add(this.lvCheckFields);
            this.tabPage30.Controls.Add(this.textBox76);
            this.tabPage30.Controls.Add(this.groupBox21);
            this.tabPage30.Location = new System.Drawing.Point(4, 22);
            this.tabPage30.Name = "tabPage30";
            this.tabPage30.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage30.Size = new System.Drawing.Size(807, 592);
            this.tabPage30.TabIndex = 0;
            this.tabPage30.Text = "Create Layouts";
            this.tabPage30.UseVisualStyleBackColor = true;
            // 
            // panel14
            // 
            this.panel14.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel14.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel14.ForeColor = System.Drawing.SystemColors.Control;
            this.panel14.Location = new System.Drawing.Point(6, 103);
            this.panel14.Name = "panel14";
            this.panel14.Size = new System.Drawing.Size(782, 1);
            this.panel14.TabIndex = 20;
            // 
            // textBox75
            // 
            this.textBox75.BackColor = System.Drawing.SystemColors.Window;
            this.textBox75.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox75.Location = new System.Drawing.Point(6, 13);
            this.textBox75.Name = "textBox75";
            this.textBox75.ReadOnly = true;
            this.textBox75.Size = new System.Drawing.Size(763, 13);
            this.textBox75.TabIndex = 19;
            this.textBox75.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button123
            // 
            this.button123.Location = new System.Drawing.Point(568, 45);
            this.button123.Name = "button123";
            this.button123.Size = new System.Drawing.Size(130, 23);
            this.button123.TabIndex = 18;
            this.button123.Text = "Delete Layout";
            this.button123.UseVisualStyleBackColor = true;
            // 
            // button124
            // 
            this.button124.Location = new System.Drawing.Point(568, 74);
            this.button124.Name = "button124";
            this.button124.Size = new System.Drawing.Size(130, 23);
            this.button124.TabIndex = 17;
            this.button124.Text = "Restore Original Layout";
            this.button124.UseVisualStyleBackColor = true;
            // 
            // button125
            // 
            this.button125.Location = new System.Drawing.Point(432, 45);
            this.button125.Name = "button125";
            this.button125.Size = new System.Drawing.Size(130, 23);
            this.button125.TabIndex = 16;
            this.button125.Text = "New Layout";
            this.button125.UseVisualStyleBackColor = true;
            // 
            // button126
            // 
            this.button126.Location = new System.Drawing.Point(432, 74);
            this.button126.Name = "button126";
            this.button126.Size = new System.Drawing.Size(130, 23);
            this.button126.TabIndex = 15;
            this.button126.Text = "Test Layout";
            this.button126.UseVisualStyleBackColor = true;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(3, 45);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(79, 13);
            this.label40.TabIndex = 13;
            this.label40.Text = "Current Layout:";
            // 
            // cboCheckLayouts
            // 
            this.cboCheckLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCheckLayouts.FormattingEnabled = true;
            this.cboCheckLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboCheckLayouts.Name = "cboCheckLayouts";
            this.cboCheckLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboCheckLayouts.TabIndex = 12;
            // 
            // lvCheckFields
            // 
            this.lvCheckFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader74,
            this.columnHeader75,
            this.columnHeader76,
            this.columnHeader77,
            this.columnHeader78,
            this.columnHeader79,
            this.columnHeader80});
            this.lvCheckFields.FullRowSelect = true;
            this.lvCheckFields.GridLines = true;
            this.lvCheckFields.Location = new System.Drawing.Point(8, 173);
            this.lvCheckFields.Name = "lvCheckFields";
            this.lvCheckFields.Size = new System.Drawing.Size(653, 169);
            this.lvCheckFields.TabIndex = 11;
            this.lvCheckFields.UseCompatibleStateImageBehavior = false;
            this.lvCheckFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader74
            // 
            this.columnHeader74.Text = "Field Name";
            this.columnHeader74.Width = 120;
            // 
            // columnHeader75
            // 
            this.columnHeader75.Text = "Display Name";
            this.columnHeader75.Width = 120;
            // 
            // columnHeader76
            // 
            this.columnHeader76.Text = "Column Width";
            this.columnHeader76.Width = 85;
            // 
            // columnHeader77
            // 
            this.columnHeader77.Text = "Pad Char";
            this.columnHeader77.Width = 59;
            // 
            // columnHeader78
            // 
            this.columnHeader78.Text = "Justify";
            this.columnHeader78.Width = 45;
            // 
            // columnHeader79
            // 
            this.columnHeader79.Text = "Quotes";
            this.columnHeader79.Width = 69;
            // 
            // columnHeader80
            // 
            this.columnHeader80.Text = "Format";
            this.columnHeader80.Width = 150;
            // 
            // textBox76
            // 
            this.textBox76.BackColor = System.Drawing.SystemColors.Window;
            this.textBox76.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox76.Location = new System.Drawing.Point(6, 153);
            this.textBox76.Name = "textBox76";
            this.textBox76.ReadOnly = true;
            this.textBox76.Size = new System.Drawing.Size(677, 13);
            this.textBox76.TabIndex = 10;
            this.textBox76.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox21
            // 
            this.groupBox21.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox21.Controls.Add(this.textBox77);
            this.groupBox21.Controls.Add(this.button127);
            this.groupBox21.Controls.Add(this.button128);
            this.groupBox21.Controls.Add(this.button129);
            this.groupBox21.Controls.Add(this.button130);
            this.groupBox21.Controls.Add(this.button131);
            this.groupBox21.Controls.Add(this.button132);
            this.groupBox21.Controls.Add(this.label41);
            this.groupBox21.Controls.Add(this.label42);
            this.groupBox21.Controls.Add(this.listBox20);
            this.groupBox21.Controls.Add(this.listBox21);
            this.groupBox21.Location = new System.Drawing.Point(6, 348);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(782, 238);
            this.groupBox21.TabIndex = 0;
            this.groupBox21.TabStop = false;
            // 
            // textBox77
            // 
            this.textBox77.BackColor = System.Drawing.SystemColors.Window;
            this.textBox77.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox77.Location = new System.Drawing.Point(419, 56);
            this.textBox77.Multiline = true;
            this.textBox77.Name = "textBox77";
            this.textBox77.ReadOnly = true;
            this.textBox77.Size = new System.Drawing.Size(202, 62);
            this.textBox77.TabIndex = 13;
            this.textBox77.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button127
            // 
            this.button127.Location = new System.Drawing.Point(419, 124);
            this.button127.Name = "button127";
            this.button127.Size = new System.Drawing.Size(202, 23);
            this.button127.TabIndex = 12;
            this.button127.Text = "Remove Field";
            this.button127.UseVisualStyleBackColor = true;
            // 
            // button128
            // 
            this.button128.Location = new System.Drawing.Point(525, 153);
            this.button128.Name = "button128";
            this.button128.Size = new System.Drawing.Size(96, 23);
            this.button128.TabIndex = 11;
            this.button128.Text = "Down";
            this.button128.UseVisualStyleBackColor = true;
            // 
            // button129
            // 
            this.button129.Location = new System.Drawing.Point(419, 153);
            this.button129.Name = "button129";
            this.button129.Size = new System.Drawing.Size(96, 23);
            this.button129.TabIndex = 10;
            this.button129.Text = "Up";
            this.button129.UseVisualStyleBackColor = true;
            // 
            // button130
            // 
            this.button130.Location = new System.Drawing.Point(419, 182);
            this.button130.Name = "button130";
            this.button130.Size = new System.Drawing.Size(202, 23);
            this.button130.TabIndex = 9;
            this.button130.Text = "Restore Default Field Data";
            this.button130.UseVisualStyleBackColor = true;
            // 
            // button131
            // 
            this.button131.Location = new System.Drawing.Point(211, 202);
            this.button131.Name = "button131";
            this.button131.Size = new System.Drawing.Size(177, 23);
            this.button131.TabIndex = 6;
            this.button131.Text = "Add Field";
            this.button131.UseVisualStyleBackColor = true;
            // 
            // button132
            // 
            this.button132.Location = new System.Drawing.Point(16, 202);
            this.button132.Name = "button132";
            this.button132.Size = new System.Drawing.Size(177, 23);
            this.button132.TabIndex = 5;
            this.button132.Text = "Add Field";
            this.button132.UseVisualStyleBackColor = true;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(208, 23);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(80, 13);
            this.label41.TabIndex = 4;
            this.label41.Text = "Standard Fields";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(13, 23);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(86, 13);
            this.label42.TabIndex = 3;
            this.label42.Text = "Aggregate Fields";
            // 
            // listBox20
            // 
            this.listBox20.FormattingEnabled = true;
            this.listBox20.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "ImageCheckPerBatch",
            "ImageCheckPerTransaction",
            "ImageCheckSingleFront",
            "ImageCheckSingleRear",
            "ImageFileSizeFrontC",
            "ImageFileSizeRearC",
            "LineCounterBank",
            "LineCounterBatch",
            "LineCounterCheck",
            "LineCounterCustomer",
            "LineCounterFile",
            "LineCounterLockbox",
            "LineCounterTransaction",
            "Static",
            "Time"});
            this.listBox20.Location = new System.Drawing.Point(211, 39);
            this.listBox20.Name = "listBox20";
            this.listBox20.Size = new System.Drawing.Size(177, 160);
            this.listBox20.TabIndex = 1;
            // 
            // listBox21
            // 
            this.listBox21.FormattingEnabled = true;
            this.listBox21.Items.AddRange(new object[] {
            "CountChecks",
            "CountStubs",
            "SumChecksAmount",
            "SumStubsAmount"});
            this.listBox21.Location = new System.Drawing.Point(16, 39);
            this.listBox21.Name = "listBox21";
            this.listBox21.Size = new System.Drawing.Size(177, 160);
            this.listBox21.TabIndex = 0;
            // 
            // tabPage31
            // 
            this.tabPage31.Controls.Add(this.textBox78);
            this.tabPage31.Controls.Add(this.textBox79);
            this.tabPage31.Controls.Add(this.textBox80);
            this.tabPage31.Controls.Add(this.button133);
            this.tabPage31.Controls.Add(this.button134);
            this.tabPage31.Controls.Add(this.button135);
            this.tabPage31.Controls.Add(this.lvChecksOrderByColumns);
            this.tabPage31.Location = new System.Drawing.Point(4, 22);
            this.tabPage31.Name = "tabPage31";
            this.tabPage31.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage31.Size = new System.Drawing.Size(807, 592);
            this.tabPage31.TabIndex = 1;
            this.tabPage31.Text = "Order Data";
            this.tabPage31.UseVisualStyleBackColor = true;
            // 
            // textBox78
            // 
            this.textBox78.BackColor = System.Drawing.SystemColors.Window;
            this.textBox78.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox78.Location = new System.Drawing.Point(12, 54);
            this.textBox78.Multiline = true;
            this.textBox78.Name = "textBox78";
            this.textBox78.ReadOnly = true;
            this.textBox78.Size = new System.Drawing.Size(473, 33);
            this.textBox78.TabIndex = 20;
            this.textBox78.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox79
            // 
            this.textBox79.BackColor = System.Drawing.SystemColors.Window;
            this.textBox79.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox79.Location = new System.Drawing.Point(12, 12);
            this.textBox79.Multiline = true;
            this.textBox79.Name = "textBox79";
            this.textBox79.ReadOnly = true;
            this.textBox79.Size = new System.Drawing.Size(473, 33);
            this.textBox79.TabIndex = 19;
            this.textBox79.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox80
            // 
            this.textBox80.BackColor = System.Drawing.SystemColors.Window;
            this.textBox80.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox80.Location = new System.Drawing.Point(492, 99);
            this.textBox80.Multiline = true;
            this.textBox80.Name = "textBox80";
            this.textBox80.ReadOnly = true;
            this.textBox80.Size = new System.Drawing.Size(202, 62);
            this.textBox80.TabIndex = 18;
            this.textBox80.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button133
            // 
            this.button133.Location = new System.Drawing.Point(492, 167);
            this.button133.Name = "button133";
            this.button133.Size = new System.Drawing.Size(202, 23);
            this.button133.TabIndex = 17;
            this.button133.Text = "Remove Field";
            this.button133.UseVisualStyleBackColor = true;
            // 
            // button134
            // 
            this.button134.Location = new System.Drawing.Point(598, 196);
            this.button134.Name = "button134";
            this.button134.Size = new System.Drawing.Size(96, 23);
            this.button134.TabIndex = 16;
            this.button134.Text = "Down";
            this.button134.UseVisualStyleBackColor = true;
            // 
            // button135
            // 
            this.button135.Location = new System.Drawing.Point(492, 196);
            this.button135.Name = "button135";
            this.button135.Size = new System.Drawing.Size(96, 23);
            this.button135.TabIndex = 15;
            this.button135.Text = "Up";
            this.button135.UseVisualStyleBackColor = true;
            // 
            // lvChecksOrderByColumns
            // 
            this.lvChecksOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader81,
            this.columnHeader82});
            this.lvChecksOrderByColumns.FullRowSelect = true;
            this.lvChecksOrderByColumns.GridLines = true;
            this.lvChecksOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvChecksOrderByColumns.Name = "lvChecksOrderByColumns";
            this.lvChecksOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvChecksOrderByColumns.TabIndex = 12;
            this.lvChecksOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvChecksOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader81
            // 
            this.columnHeader81.Text = "Field Name";
            this.columnHeader81.Width = 345;
            // 
            // columnHeader82
            // 
            this.columnHeader82.Text = "Asc/Desc";
            this.columnHeader82.Width = 120;
            // 
            // tabPage32
            // 
            this.tabPage32.Controls.Add(this.textBox81);
            this.tabPage32.Controls.Add(this.textBox82);
            this.tabPage32.Controls.Add(this.button136);
            this.tabPage32.Controls.Add(this.button137);
            this.tabPage32.Controls.Add(this.textBox83);
            this.tabPage32.Controls.Add(this.textBox84);
            this.tabPage32.Controls.Add(this.textBox85);
            this.tabPage32.Controls.Add(this.button138);
            this.tabPage32.Controls.Add(this.button139);
            this.tabPage32.Controls.Add(this.button140);
            this.tabPage32.Controls.Add(this.listView21);
            this.tabPage32.Location = new System.Drawing.Point(4, 22);
            this.tabPage32.Name = "tabPage32";
            this.tabPage32.Size = new System.Drawing.Size(807, 592);
            this.tabPage32.TabIndex = 2;
            this.tabPage32.Text = "Limit Data";
            this.tabPage32.UseVisualStyleBackColor = true;
            // 
            // textBox81
            // 
            this.textBox81.BackColor = System.Drawing.SystemColors.Window;
            this.textBox81.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox81.Location = new System.Drawing.Point(12, 360);
            this.textBox81.Multiline = true;
            this.textBox81.Name = "textBox81";
            this.textBox81.Size = new System.Drawing.Size(473, 18);
            this.textBox81.TabIndex = 31;
            this.textBox81.Text = "\"Where clause\" as text";
            // 
            // textBox82
            // 
            this.textBox82.Location = new System.Drawing.Point(12, 384);
            this.textBox82.Multiline = true;
            this.textBox82.Name = "textBox82";
            this.textBox82.ReadOnly = true;
            this.textBox82.Size = new System.Drawing.Size(473, 123);
            this.textBox82.TabIndex = 30;
            // 
            // button136
            // 
            this.button136.Location = new System.Drawing.Point(492, 225);
            this.button136.Name = "button136";
            this.button136.Size = new System.Drawing.Size(202, 23);
            this.button136.TabIndex = 29;
            this.button136.Text = "View as Text";
            this.button136.UseVisualStyleBackColor = true;
            // 
            // button137
            // 
            this.button137.Location = new System.Drawing.Point(492, 254);
            this.button137.Name = "button137";
            this.button137.Size = new System.Drawing.Size(202, 23);
            this.button137.TabIndex = 28;
            this.button137.Text = "Validate";
            this.button137.UseVisualStyleBackColor = true;
            // 
            // textBox83
            // 
            this.textBox83.BackColor = System.Drawing.SystemColors.Window;
            this.textBox83.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox83.Location = new System.Drawing.Point(12, 66);
            this.textBox83.Multiline = true;
            this.textBox83.Name = "textBox83";
            this.textBox83.ReadOnly = true;
            this.textBox83.Size = new System.Drawing.Size(473, 18);
            this.textBox83.TabIndex = 27;
            this.textBox83.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox84
            // 
            this.textBox84.BackColor = System.Drawing.SystemColors.Window;
            this.textBox84.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox84.Location = new System.Drawing.Point(12, 12);
            this.textBox84.Multiline = true;
            this.textBox84.Name = "textBox84";
            this.textBox84.ReadOnly = true;
            this.textBox84.Size = new System.Drawing.Size(473, 33);
            this.textBox84.TabIndex = 26;
            this.textBox84.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox85
            // 
            this.textBox85.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox85.Location = new System.Drawing.Point(492, 99);
            this.textBox85.Multiline = true;
            this.textBox85.Name = "textBox85";
            this.textBox85.Size = new System.Drawing.Size(202, 62);
            this.textBox85.TabIndex = 25;
            this.textBox85.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button138
            // 
            this.button138.Location = new System.Drawing.Point(492, 167);
            this.button138.Name = "button138";
            this.button138.Size = new System.Drawing.Size(202, 23);
            this.button138.TabIndex = 24;
            this.button138.Text = "Remove Field";
            this.button138.UseVisualStyleBackColor = true;
            // 
            // button139
            // 
            this.button139.Location = new System.Drawing.Point(598, 196);
            this.button139.Name = "button139";
            this.button139.Size = new System.Drawing.Size(96, 23);
            this.button139.TabIndex = 23;
            this.button139.Text = "Down";
            this.button139.UseVisualStyleBackColor = true;
            // 
            // button140
            // 
            this.button140.Location = new System.Drawing.Point(492, 196);
            this.button140.Name = "button140";
            this.button140.Size = new System.Drawing.Size(96, 23);
            this.button140.TabIndex = 22;
            this.button140.Text = "Up";
            this.button140.UseVisualStyleBackColor = true;
            // 
            // listView21
            // 
            this.listView21.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader83,
            this.columnHeader84});
            this.listView21.FullRowSelect = true;
            this.listView21.GridLines = true;
            this.listView21.Location = new System.Drawing.Point(12, 90);
            this.listView21.Name = "listView21";
            this.listView21.Size = new System.Drawing.Size(473, 227);
            this.listView21.TabIndex = 21;
            this.listView21.UseCompatibleStateImageBehavior = false;
            this.listView21.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader83
            // 
            this.columnHeader83.Text = "Field Name";
            this.columnHeader83.Width = 345;
            // 
            // columnHeader84
            // 
            this.columnHeader84.Text = "Asc/Desc";
            this.columnHeader84.Width = 120;
            // 
            // panel15
            // 
            this.panel15.Controls.Add(this.button141);
            this.panel15.Controls.Add(this.lstCheckColumns);
            this.panel15.Controls.Add(this.label43);
            this.panel15.Controls.Add(this.splitter6);
            this.panel15.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel15.Location = new System.Drawing.Point(0, 0);
            this.panel15.Name = "panel15";
            this.panel15.Size = new System.Drawing.Size(185, 618);
            this.panel15.TabIndex = 1;
            // 
            // button141
            // 
            this.button141.Location = new System.Drawing.Point(8, 515);
            this.button141.Name = "button141";
            this.button141.Size = new System.Drawing.Size(75, 23);
            this.button141.TabIndex = 3;
            this.button141.Text = "Add Field";
            this.button141.UseVisualStyleBackColor = true;
            // 
            // lstCheckColumns
            // 
            this.lstCheckColumns.FormattingEnabled = true;
            this.lstCheckColumns.Location = new System.Drawing.Point(8, 24);
            this.lstCheckColumns.Name = "lstCheckColumns";
            this.lstCheckColumns.Size = new System.Drawing.Size(165, 485);
            this.lstCheckColumns.TabIndex = 2;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(5, 10);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(60, 13);
            this.label43.TabIndex = 1;
            this.label43.Text = "Data Fields";
            // 
            // splitter6
            // 
            this.splitter6.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter6.Location = new System.Drawing.Point(182, 0);
            this.splitter6.Name = "splitter6";
            this.splitter6.Size = new System.Drawing.Size(3, 618);
            this.splitter6.TabIndex = 0;
            this.splitter6.TabStop = false;
            // 
            // tpTransaction
            // 
            this.tpTransaction.Controls.Add(this.tabControl6);
            this.tpTransaction.Controls.Add(this.panel13);
            this.tpTransaction.Location = new System.Drawing.Point(4, 22);
            this.tpTransaction.Name = "tpTransaction";
            this.tpTransaction.Size = new System.Drawing.Size(1000, 618);
            this.tpTransaction.TabIndex = 6;
            this.tpTransaction.Text = "Transaction";
            this.tpTransaction.UseVisualStyleBackColor = true;
            // 
            // tabControl6
            // 
            this.tabControl6.Controls.Add(this.tabPage27);
            this.tabControl6.Controls.Add(this.tabPage28);
            this.tabControl6.Controls.Add(this.tabPage29);
            this.tabControl6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl6.Location = new System.Drawing.Point(185, 0);
            this.tabControl6.Name = "tabControl6";
            this.tabControl6.SelectedIndex = 0;
            this.tabControl6.Size = new System.Drawing.Size(815, 618);
            this.tabControl6.TabIndex = 2;
            // 
            // tabPage27
            // 
            this.tabPage27.Controls.Add(this.panel12);
            this.tabPage27.Controls.Add(this.textBox64);
            this.tabPage27.Controls.Add(this.button104);
            this.tabPage27.Controls.Add(this.button105);
            this.tabPage27.Controls.Add(this.button106);
            this.tabPage27.Controls.Add(this.button107);
            this.tabPage27.Controls.Add(this.groupBox18);
            this.tabPage27.Controls.Add(this.label36);
            this.tabPage27.Controls.Add(this.cboTransactionLayouts);
            this.tabPage27.Controls.Add(this.lvTransactionFields);
            this.tabPage27.Controls.Add(this.textBox65);
            this.tabPage27.Controls.Add(this.groupBox19);
            this.tabPage27.Location = new System.Drawing.Point(4, 22);
            this.tabPage27.Name = "tabPage27";
            this.tabPage27.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage27.Size = new System.Drawing.Size(807, 592);
            this.tabPage27.TabIndex = 0;
            this.tabPage27.Text = "Create Layouts";
            this.tabPage27.UseVisualStyleBackColor = true;
            // 
            // panel12
            // 
            this.panel12.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel12.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel12.ForeColor = System.Drawing.SystemColors.Control;
            this.panel12.Location = new System.Drawing.Point(6, 103);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(782, 1);
            this.panel12.TabIndex = 20;
            // 
            // textBox64
            // 
            this.textBox64.BackColor = System.Drawing.SystemColors.Window;
            this.textBox64.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox64.Location = new System.Drawing.Point(6, 13);
            this.textBox64.Name = "textBox64";
            this.textBox64.ReadOnly = true;
            this.textBox64.Size = new System.Drawing.Size(763, 13);
            this.textBox64.TabIndex = 19;
            this.textBox64.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button104
            // 
            this.button104.Location = new System.Drawing.Point(568, 45);
            this.button104.Name = "button104";
            this.button104.Size = new System.Drawing.Size(130, 23);
            this.button104.TabIndex = 18;
            this.button104.Text = "Delete Layout";
            this.button104.UseVisualStyleBackColor = true;
            // 
            // button105
            // 
            this.button105.Location = new System.Drawing.Point(568, 74);
            this.button105.Name = "button105";
            this.button105.Size = new System.Drawing.Size(130, 23);
            this.button105.TabIndex = 17;
            this.button105.Text = "Restore Original Layout";
            this.button105.UseVisualStyleBackColor = true;
            // 
            // button106
            // 
            this.button106.Location = new System.Drawing.Point(432, 45);
            this.button106.Name = "button106";
            this.button106.Size = new System.Drawing.Size(130, 23);
            this.button106.TabIndex = 16;
            this.button106.Text = "New Layout";
            this.button106.UseVisualStyleBackColor = true;
            // 
            // button107
            // 
            this.button107.Location = new System.Drawing.Point(432, 74);
            this.button107.Name = "button107";
            this.button107.Size = new System.Drawing.Size(130, 23);
            this.button107.TabIndex = 15;
            this.button107.Text = "Test Layout";
            this.button107.UseVisualStyleBackColor = true;
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.rdoTransactionRecordTypeTrailer);
            this.groupBox18.Controls.Add(this.rdoTransactionRecordTypeHeader);
            this.groupBox18.Location = new System.Drawing.Point(248, 45);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(146, 52);
            this.groupBox18.TabIndex = 14;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Record Type";
            // 
            // rdoTransactionRecordTypeTrailer
            // 
            this.rdoTransactionRecordTypeTrailer.AutoSize = true;
            this.rdoTransactionRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoTransactionRecordTypeTrailer.Name = "rdoTransactionRecordTypeTrailer";
            this.rdoTransactionRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoTransactionRecordTypeTrailer.TabIndex = 1;
            this.rdoTransactionRecordTypeTrailer.TabStop = true;
            this.rdoTransactionRecordTypeTrailer.Text = "Trailer";
            this.rdoTransactionRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoTransactionRecordTypeHeader
            // 
            this.rdoTransactionRecordTypeHeader.AutoSize = true;
            this.rdoTransactionRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoTransactionRecordTypeHeader.Name = "rdoTransactionRecordTypeHeader";
            this.rdoTransactionRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoTransactionRecordTypeHeader.TabIndex = 0;
            this.rdoTransactionRecordTypeHeader.TabStop = true;
            this.rdoTransactionRecordTypeHeader.Text = "Header";
            this.rdoTransactionRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(3, 45);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 13);
            this.label36.TabIndex = 13;
            this.label36.Text = "Current Layout:";
            // 
            // cboTransactionLayouts
            // 
            this.cboTransactionLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTransactionLayouts.FormattingEnabled = true;
            this.cboTransactionLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboTransactionLayouts.Name = "cboTransactionLayouts";
            this.cboTransactionLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboTransactionLayouts.TabIndex = 12;
            // 
            // lvTransactionFields
            // 
            this.lvTransactionFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader63,
            this.columnHeader64,
            this.columnHeader65,
            this.columnHeader66,
            this.columnHeader67,
            this.columnHeader68,
            this.columnHeader69});
            this.lvTransactionFields.FullRowSelect = true;
            this.lvTransactionFields.GridLines = true;
            this.lvTransactionFields.Location = new System.Drawing.Point(8, 173);
            this.lvTransactionFields.Name = "lvTransactionFields";
            this.lvTransactionFields.Size = new System.Drawing.Size(653, 169);
            this.lvTransactionFields.TabIndex = 11;
            this.lvTransactionFields.UseCompatibleStateImageBehavior = false;
            this.lvTransactionFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader63
            // 
            this.columnHeader63.Text = "Field Name";
            this.columnHeader63.Width = 120;
            // 
            // columnHeader64
            // 
            this.columnHeader64.Text = "Display Name";
            this.columnHeader64.Width = 120;
            // 
            // columnHeader65
            // 
            this.columnHeader65.Text = "Column Width";
            this.columnHeader65.Width = 85;
            // 
            // columnHeader66
            // 
            this.columnHeader66.Text = "Pad Char";
            this.columnHeader66.Width = 59;
            // 
            // columnHeader67
            // 
            this.columnHeader67.Text = "Justify";
            this.columnHeader67.Width = 45;
            // 
            // columnHeader68
            // 
            this.columnHeader68.Text = "Quotes";
            this.columnHeader68.Width = 69;
            // 
            // columnHeader69
            // 
            this.columnHeader69.Text = "Format";
            this.columnHeader69.Width = 150;
            // 
            // textBox65
            // 
            this.textBox65.BackColor = System.Drawing.SystemColors.Window;
            this.textBox65.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox65.Location = new System.Drawing.Point(6, 153);
            this.textBox65.Name = "textBox65";
            this.textBox65.ReadOnly = true;
            this.textBox65.Size = new System.Drawing.Size(677, 13);
            this.textBox65.TabIndex = 10;
            this.textBox65.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox19
            // 
            this.groupBox19.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox19.Controls.Add(this.textBox66);
            this.groupBox19.Controls.Add(this.button108);
            this.groupBox19.Controls.Add(this.button109);
            this.groupBox19.Controls.Add(this.button110);
            this.groupBox19.Controls.Add(this.button111);
            this.groupBox19.Controls.Add(this.button112);
            this.groupBox19.Controls.Add(this.button113);
            this.groupBox19.Controls.Add(this.label37);
            this.groupBox19.Controls.Add(this.label38);
            this.groupBox19.Controls.Add(this.listBox17);
            this.groupBox19.Controls.Add(this.listBox18);
            this.groupBox19.Location = new System.Drawing.Point(6, 348);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(782, 238);
            this.groupBox19.TabIndex = 0;
            this.groupBox19.TabStop = false;
            // 
            // textBox66
            // 
            this.textBox66.BackColor = System.Drawing.SystemColors.Window;
            this.textBox66.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox66.Location = new System.Drawing.Point(419, 56);
            this.textBox66.Multiline = true;
            this.textBox66.Name = "textBox66";
            this.textBox66.ReadOnly = true;
            this.textBox66.Size = new System.Drawing.Size(202, 62);
            this.textBox66.TabIndex = 13;
            this.textBox66.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button108
            // 
            this.button108.Location = new System.Drawing.Point(419, 124);
            this.button108.Name = "button108";
            this.button108.Size = new System.Drawing.Size(202, 23);
            this.button108.TabIndex = 12;
            this.button108.Text = "Remove Field";
            this.button108.UseVisualStyleBackColor = true;
            // 
            // button109
            // 
            this.button109.Location = new System.Drawing.Point(525, 153);
            this.button109.Name = "button109";
            this.button109.Size = new System.Drawing.Size(96, 23);
            this.button109.TabIndex = 11;
            this.button109.Text = "Down";
            this.button109.UseVisualStyleBackColor = true;
            // 
            // button110
            // 
            this.button110.Location = new System.Drawing.Point(419, 153);
            this.button110.Name = "button110";
            this.button110.Size = new System.Drawing.Size(96, 23);
            this.button110.TabIndex = 10;
            this.button110.Text = "Up";
            this.button110.UseVisualStyleBackColor = true;
            // 
            // button111
            // 
            this.button111.Location = new System.Drawing.Point(419, 182);
            this.button111.Name = "button111";
            this.button111.Size = new System.Drawing.Size(202, 23);
            this.button111.TabIndex = 9;
            this.button111.Text = "Restore Default Field Data";
            this.button111.UseVisualStyleBackColor = true;
            // 
            // button112
            // 
            this.button112.Location = new System.Drawing.Point(211, 202);
            this.button112.Name = "button112";
            this.button112.Size = new System.Drawing.Size(177, 23);
            this.button112.TabIndex = 6;
            this.button112.Text = "Add Field";
            this.button112.UseVisualStyleBackColor = true;
            // 
            // button113
            // 
            this.button113.Location = new System.Drawing.Point(16, 202);
            this.button113.Name = "button113";
            this.button113.Size = new System.Drawing.Size(177, 23);
            this.button113.TabIndex = 5;
            this.button113.Text = "Add Field";
            this.button113.UseVisualStyleBackColor = true;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(208, 23);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(80, 13);
            this.label37.TabIndex = 4;
            this.label37.Text = "Standard Fields";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 23);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(86, 13);
            this.label38.TabIndex = 3;
            this.label38.Text = "Aggregate Fields";
            // 
            // listBox17
            // 
            this.listBox17.FormattingEnabled = true;
            this.listBox17.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterBatch",
            "LineCounterCustomer",
            "LineCounterFile",
            "LineCounterLockbox",
            "LineCounterTransaction",
            "Static",
            "Time"});
            this.listBox17.Location = new System.Drawing.Point(211, 39);
            this.listBox17.Name = "listBox17";
            this.listBox17.Size = new System.Drawing.Size(177, 160);
            this.listBox17.TabIndex = 1;
            // 
            // listBox18
            // 
            this.listBox18.FormattingEnabled = true;
            this.listBox18.Items.AddRange(new object[] {
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox18.Location = new System.Drawing.Point(16, 39);
            this.listBox18.Name = "listBox18";
            this.listBox18.Size = new System.Drawing.Size(177, 160);
            this.listBox18.TabIndex = 0;
            // 
            // tabPage28
            // 
            this.tabPage28.Controls.Add(this.textBox67);
            this.tabPage28.Controls.Add(this.textBox68);
            this.tabPage28.Controls.Add(this.textBox69);
            this.tabPage28.Controls.Add(this.button114);
            this.tabPage28.Controls.Add(this.button115);
            this.tabPage28.Controls.Add(this.button116);
            this.tabPage28.Controls.Add(this.lvTransactionsOrderByColumns);
            this.tabPage28.Location = new System.Drawing.Point(4, 22);
            this.tabPage28.Name = "tabPage28";
            this.tabPage28.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage28.Size = new System.Drawing.Size(807, 592);
            this.tabPage28.TabIndex = 1;
            this.tabPage28.Text = "Order Data";
            this.tabPage28.UseVisualStyleBackColor = true;
            // 
            // textBox67
            // 
            this.textBox67.BackColor = System.Drawing.SystemColors.Window;
            this.textBox67.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox67.Location = new System.Drawing.Point(12, 54);
            this.textBox67.Multiline = true;
            this.textBox67.Name = "textBox67";
            this.textBox67.ReadOnly = true;
            this.textBox67.Size = new System.Drawing.Size(473, 33);
            this.textBox67.TabIndex = 20;
            this.textBox67.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox68
            // 
            this.textBox68.BackColor = System.Drawing.SystemColors.Window;
            this.textBox68.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox68.Location = new System.Drawing.Point(12, 12);
            this.textBox68.Multiline = true;
            this.textBox68.Name = "textBox68";
            this.textBox68.ReadOnly = true;
            this.textBox68.Size = new System.Drawing.Size(473, 33);
            this.textBox68.TabIndex = 19;
            this.textBox68.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox69
            // 
            this.textBox69.BackColor = System.Drawing.SystemColors.Window;
            this.textBox69.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox69.Location = new System.Drawing.Point(492, 99);
            this.textBox69.Multiline = true;
            this.textBox69.Name = "textBox69";
            this.textBox69.ReadOnly = true;
            this.textBox69.Size = new System.Drawing.Size(202, 62);
            this.textBox69.TabIndex = 18;
            this.textBox69.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button114
            // 
            this.button114.Location = new System.Drawing.Point(492, 167);
            this.button114.Name = "button114";
            this.button114.Size = new System.Drawing.Size(202, 23);
            this.button114.TabIndex = 17;
            this.button114.Text = "Remove Field";
            this.button114.UseVisualStyleBackColor = true;
            // 
            // button115
            // 
            this.button115.Location = new System.Drawing.Point(598, 196);
            this.button115.Name = "button115";
            this.button115.Size = new System.Drawing.Size(96, 23);
            this.button115.TabIndex = 16;
            this.button115.Text = "Down";
            this.button115.UseVisualStyleBackColor = true;
            // 
            // button116
            // 
            this.button116.Location = new System.Drawing.Point(492, 196);
            this.button116.Name = "button116";
            this.button116.Size = new System.Drawing.Size(96, 23);
            this.button116.TabIndex = 15;
            this.button116.Text = "Up";
            this.button116.UseVisualStyleBackColor = true;
            // 
            // lvTransactionsOrderByColumns
            // 
            this.lvTransactionsOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader70,
            this.columnHeader71});
            this.lvTransactionsOrderByColumns.FullRowSelect = true;
            this.lvTransactionsOrderByColumns.GridLines = true;
            this.lvTransactionsOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvTransactionsOrderByColumns.Name = "lvTransactionsOrderByColumns";
            this.lvTransactionsOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvTransactionsOrderByColumns.TabIndex = 12;
            this.lvTransactionsOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvTransactionsOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader70
            // 
            this.columnHeader70.Text = "Field Name";
            this.columnHeader70.Width = 345;
            // 
            // columnHeader71
            // 
            this.columnHeader71.Text = "Asc/Desc";
            this.columnHeader71.Width = 120;
            // 
            // tabPage29
            // 
            this.tabPage29.Controls.Add(this.textBox70);
            this.tabPage29.Controls.Add(this.textBox71);
            this.tabPage29.Controls.Add(this.button117);
            this.tabPage29.Controls.Add(this.button118);
            this.tabPage29.Controls.Add(this.textBox72);
            this.tabPage29.Controls.Add(this.textBox73);
            this.tabPage29.Controls.Add(this.textBox74);
            this.tabPage29.Controls.Add(this.button119);
            this.tabPage29.Controls.Add(this.button120);
            this.tabPage29.Controls.Add(this.button121);
            this.tabPage29.Controls.Add(this.listView18);
            this.tabPage29.Location = new System.Drawing.Point(4, 22);
            this.tabPage29.Name = "tabPage29";
            this.tabPage29.Size = new System.Drawing.Size(807, 592);
            this.tabPage29.TabIndex = 2;
            this.tabPage29.Text = "Limit Data";
            this.tabPage29.UseVisualStyleBackColor = true;
            // 
            // textBox70
            // 
            this.textBox70.BackColor = System.Drawing.SystemColors.Window;
            this.textBox70.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox70.Location = new System.Drawing.Point(12, 360);
            this.textBox70.Multiline = true;
            this.textBox70.Name = "textBox70";
            this.textBox70.Size = new System.Drawing.Size(473, 18);
            this.textBox70.TabIndex = 31;
            this.textBox70.Text = "\"Where clause\" as text";
            // 
            // textBox71
            // 
            this.textBox71.Location = new System.Drawing.Point(12, 384);
            this.textBox71.Multiline = true;
            this.textBox71.Name = "textBox71";
            this.textBox71.ReadOnly = true;
            this.textBox71.Size = new System.Drawing.Size(473, 123);
            this.textBox71.TabIndex = 30;
            // 
            // button117
            // 
            this.button117.Location = new System.Drawing.Point(492, 225);
            this.button117.Name = "button117";
            this.button117.Size = new System.Drawing.Size(202, 23);
            this.button117.TabIndex = 29;
            this.button117.Text = "View as Text";
            this.button117.UseVisualStyleBackColor = true;
            // 
            // button118
            // 
            this.button118.Location = new System.Drawing.Point(492, 254);
            this.button118.Name = "button118";
            this.button118.Size = new System.Drawing.Size(202, 23);
            this.button118.TabIndex = 28;
            this.button118.Text = "Validate";
            this.button118.UseVisualStyleBackColor = true;
            // 
            // textBox72
            // 
            this.textBox72.BackColor = System.Drawing.SystemColors.Window;
            this.textBox72.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox72.Location = new System.Drawing.Point(12, 66);
            this.textBox72.Multiline = true;
            this.textBox72.Name = "textBox72";
            this.textBox72.ReadOnly = true;
            this.textBox72.Size = new System.Drawing.Size(473, 18);
            this.textBox72.TabIndex = 27;
            this.textBox72.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox73
            // 
            this.textBox73.BackColor = System.Drawing.SystemColors.Window;
            this.textBox73.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox73.Location = new System.Drawing.Point(12, 12);
            this.textBox73.Multiline = true;
            this.textBox73.Name = "textBox73";
            this.textBox73.ReadOnly = true;
            this.textBox73.Size = new System.Drawing.Size(473, 33);
            this.textBox73.TabIndex = 26;
            this.textBox73.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox74
            // 
            this.textBox74.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox74.Location = new System.Drawing.Point(492, 99);
            this.textBox74.Multiline = true;
            this.textBox74.Name = "textBox74";
            this.textBox74.Size = new System.Drawing.Size(202, 62);
            this.textBox74.TabIndex = 25;
            this.textBox74.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button119
            // 
            this.button119.Location = new System.Drawing.Point(492, 167);
            this.button119.Name = "button119";
            this.button119.Size = new System.Drawing.Size(202, 23);
            this.button119.TabIndex = 24;
            this.button119.Text = "Remove Field";
            this.button119.UseVisualStyleBackColor = true;
            // 
            // button120
            // 
            this.button120.Location = new System.Drawing.Point(598, 196);
            this.button120.Name = "button120";
            this.button120.Size = new System.Drawing.Size(96, 23);
            this.button120.TabIndex = 23;
            this.button120.Text = "Down";
            this.button120.UseVisualStyleBackColor = true;
            // 
            // button121
            // 
            this.button121.Location = new System.Drawing.Point(492, 196);
            this.button121.Name = "button121";
            this.button121.Size = new System.Drawing.Size(96, 23);
            this.button121.TabIndex = 22;
            this.button121.Text = "Up";
            this.button121.UseVisualStyleBackColor = true;
            // 
            // listView18
            // 
            this.listView18.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader72,
            this.columnHeader73});
            this.listView18.FullRowSelect = true;
            this.listView18.GridLines = true;
            this.listView18.Location = new System.Drawing.Point(12, 90);
            this.listView18.Name = "listView18";
            this.listView18.Size = new System.Drawing.Size(473, 227);
            this.listView18.TabIndex = 21;
            this.listView18.UseCompatibleStateImageBehavior = false;
            this.listView18.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader72
            // 
            this.columnHeader72.Text = "Field Name";
            this.columnHeader72.Width = 345;
            // 
            // columnHeader73
            // 
            this.columnHeader73.Text = "Asc/Desc";
            this.columnHeader73.Width = 120;
            // 
            // panel13
            // 
            this.panel13.Controls.Add(this.button122);
            this.panel13.Controls.Add(this.lstTransactionColumns);
            this.panel13.Controls.Add(this.label39);
            this.panel13.Controls.Add(this.splitter5);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel13.Location = new System.Drawing.Point(0, 0);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(185, 618);
            this.panel13.TabIndex = 1;
            // 
            // button122
            // 
            this.button122.Location = new System.Drawing.Point(8, 515);
            this.button122.Name = "button122";
            this.button122.Size = new System.Drawing.Size(75, 23);
            this.button122.TabIndex = 3;
            this.button122.Text = "Add Field";
            this.button122.UseVisualStyleBackColor = true;
            // 
            // lstTransactionColumns
            // 
            this.lstTransactionColumns.FormattingEnabled = true;
            this.lstTransactionColumns.Location = new System.Drawing.Point(8, 24);
            this.lstTransactionColumns.Name = "lstTransactionColumns";
            this.lstTransactionColumns.Size = new System.Drawing.Size(165, 485);
            this.lstTransactionColumns.TabIndex = 2;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(5, 10);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(60, 13);
            this.label39.TabIndex = 1;
            this.label39.Text = "Data Fields";
            // 
            // splitter5
            // 
            this.splitter5.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter5.Location = new System.Drawing.Point(182, 0);
            this.splitter5.Name = "splitter5";
            this.splitter5.Size = new System.Drawing.Size(3, 618);
            this.splitter5.TabIndex = 0;
            this.splitter5.TabStop = false;
            // 
            // tpBatch
            // 
            this.tpBatch.Controls.Add(this.tabControl5);
            this.tpBatch.Controls.Add(this.panel11);
            this.tpBatch.Location = new System.Drawing.Point(4, 22);
            this.tpBatch.Name = "tpBatch";
            this.tpBatch.Size = new System.Drawing.Size(1000, 618);
            this.tpBatch.TabIndex = 5;
            this.tpBatch.Text = "Batch";
            this.tpBatch.UseVisualStyleBackColor = true;
            // 
            // tabControl5
            // 
            this.tabControl5.Controls.Add(this.tabPage24);
            this.tabControl5.Controls.Add(this.tabPage25);
            this.tabControl5.Controls.Add(this.tabPage26);
            this.tabControl5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl5.Location = new System.Drawing.Point(185, 0);
            this.tabControl5.Name = "tabControl5";
            this.tabControl5.SelectedIndex = 0;
            this.tabControl5.Size = new System.Drawing.Size(815, 618);
            this.tabControl5.TabIndex = 2;
            // 
            // tabPage24
            // 
            this.tabPage24.Controls.Add(this.panel10);
            this.tabPage24.Controls.Add(this.textBox53);
            this.tabPage24.Controls.Add(this.button85);
            this.tabPage24.Controls.Add(this.button86);
            this.tabPage24.Controls.Add(this.button87);
            this.tabPage24.Controls.Add(this.button88);
            this.tabPage24.Controls.Add(this.groupBox16);
            this.tabPage24.Controls.Add(this.label32);
            this.tabPage24.Controls.Add(this.cboBatchLayouts);
            this.tabPage24.Controls.Add(this.lvBatchFields);
            this.tabPage24.Controls.Add(this.textBox54);
            this.tabPage24.Controls.Add(this.groupBox17);
            this.tabPage24.Location = new System.Drawing.Point(4, 22);
            this.tabPage24.Name = "tabPage24";
            this.tabPage24.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage24.Size = new System.Drawing.Size(807, 592);
            this.tabPage24.TabIndex = 0;
            this.tabPage24.Text = "Create Layouts";
            this.tabPage24.UseVisualStyleBackColor = true;
            // 
            // panel10
            // 
            this.panel10.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel10.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel10.ForeColor = System.Drawing.SystemColors.Control;
            this.panel10.Location = new System.Drawing.Point(6, 103);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(782, 1);
            this.panel10.TabIndex = 20;
            // 
            // textBox53
            // 
            this.textBox53.BackColor = System.Drawing.SystemColors.Window;
            this.textBox53.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox53.Location = new System.Drawing.Point(6, 13);
            this.textBox53.Name = "textBox53";
            this.textBox53.ReadOnly = true;
            this.textBox53.Size = new System.Drawing.Size(763, 13);
            this.textBox53.TabIndex = 19;
            this.textBox53.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button85
            // 
            this.button85.Location = new System.Drawing.Point(568, 45);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(130, 23);
            this.button85.TabIndex = 18;
            this.button85.Text = "Delete Layout";
            this.button85.UseVisualStyleBackColor = true;
            // 
            // button86
            // 
            this.button86.Location = new System.Drawing.Point(568, 74);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(130, 23);
            this.button86.TabIndex = 17;
            this.button86.Text = "Restore Original Layout";
            this.button86.UseVisualStyleBackColor = true;
            // 
            // button87
            // 
            this.button87.Location = new System.Drawing.Point(432, 45);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(130, 23);
            this.button87.TabIndex = 16;
            this.button87.Text = "New Layout";
            this.button87.UseVisualStyleBackColor = true;
            // 
            // button88
            // 
            this.button88.Location = new System.Drawing.Point(432, 74);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(130, 23);
            this.button88.TabIndex = 15;
            this.button88.Text = "Test Layout";
            this.button88.UseVisualStyleBackColor = true;
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.rdoBatchRecordTypeTrailer);
            this.groupBox16.Controls.Add(this.rdoBatchRecordTypeHeader);
            this.groupBox16.Location = new System.Drawing.Point(248, 45);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(146, 52);
            this.groupBox16.TabIndex = 14;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "Record Type";
            // 
            // rdoBatchRecordTypeTrailer
            // 
            this.rdoBatchRecordTypeTrailer.AutoSize = true;
            this.rdoBatchRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoBatchRecordTypeTrailer.Name = "rdoBatchRecordTypeTrailer";
            this.rdoBatchRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoBatchRecordTypeTrailer.TabIndex = 1;
            this.rdoBatchRecordTypeTrailer.TabStop = true;
            this.rdoBatchRecordTypeTrailer.Text = "Trailer";
            this.rdoBatchRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoBatchRecordTypeHeader
            // 
            this.rdoBatchRecordTypeHeader.AutoSize = true;
            this.rdoBatchRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoBatchRecordTypeHeader.Name = "rdoBatchRecordTypeHeader";
            this.rdoBatchRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoBatchRecordTypeHeader.TabIndex = 0;
            this.rdoBatchRecordTypeHeader.TabStop = true;
            this.rdoBatchRecordTypeHeader.Text = "Header";
            this.rdoBatchRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(3, 45);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(79, 13);
            this.label32.TabIndex = 13;
            this.label32.Text = "Current Layout:";
            // 
            // cboBatchLayouts
            // 
            this.cboBatchLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBatchLayouts.FormattingEnabled = true;
            this.cboBatchLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboBatchLayouts.Name = "cboBatchLayouts";
            this.cboBatchLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboBatchLayouts.TabIndex = 12;
            // 
            // lvBatchFields
            // 
            this.lvBatchFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader52,
            this.columnHeader53,
            this.columnHeader54,
            this.columnHeader55,
            this.columnHeader56,
            this.columnHeader57,
            this.columnHeader58});
            this.lvBatchFields.FullRowSelect = true;
            this.lvBatchFields.GridLines = true;
            this.lvBatchFields.Location = new System.Drawing.Point(8, 173);
            this.lvBatchFields.Name = "lvBatchFields";
            this.lvBatchFields.Size = new System.Drawing.Size(651, 169);
            this.lvBatchFields.TabIndex = 11;
            this.lvBatchFields.UseCompatibleStateImageBehavior = false;
            this.lvBatchFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader52
            // 
            this.columnHeader52.Text = "Field Name";
            this.columnHeader52.Width = 120;
            // 
            // columnHeader53
            // 
            this.columnHeader53.Text = "Display Name";
            this.columnHeader53.Width = 120;
            // 
            // columnHeader54
            // 
            this.columnHeader54.Text = "Column Width";
            this.columnHeader54.Width = 85;
            // 
            // columnHeader55
            // 
            this.columnHeader55.Text = "Pad Char";
            this.columnHeader55.Width = 59;
            // 
            // columnHeader56
            // 
            this.columnHeader56.Text = "Justify";
            this.columnHeader56.Width = 44;
            // 
            // columnHeader57
            // 
            this.columnHeader57.Text = "Quotes";
            this.columnHeader57.Width = 69;
            // 
            // columnHeader58
            // 
            this.columnHeader58.Text = "Format";
            this.columnHeader58.Width = 150;
            // 
            // textBox54
            // 
            this.textBox54.BackColor = System.Drawing.SystemColors.Window;
            this.textBox54.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox54.Location = new System.Drawing.Point(6, 153);
            this.textBox54.Name = "textBox54";
            this.textBox54.ReadOnly = true;
            this.textBox54.Size = new System.Drawing.Size(677, 13);
            this.textBox54.TabIndex = 10;
            this.textBox54.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox17
            // 
            this.groupBox17.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox17.Controls.Add(this.textBox55);
            this.groupBox17.Controls.Add(this.button89);
            this.groupBox17.Controls.Add(this.button90);
            this.groupBox17.Controls.Add(this.button91);
            this.groupBox17.Controls.Add(this.button92);
            this.groupBox17.Controls.Add(this.button93);
            this.groupBox17.Controls.Add(this.button94);
            this.groupBox17.Controls.Add(this.label33);
            this.groupBox17.Controls.Add(this.label34);
            this.groupBox17.Controls.Add(this.listBox14);
            this.groupBox17.Controls.Add(this.listBox15);
            this.groupBox17.Location = new System.Drawing.Point(6, 348);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(782, 238);
            this.groupBox17.TabIndex = 0;
            this.groupBox17.TabStop = false;
            // 
            // textBox55
            // 
            this.textBox55.BackColor = System.Drawing.SystemColors.Window;
            this.textBox55.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox55.Location = new System.Drawing.Point(419, 56);
            this.textBox55.Multiline = true;
            this.textBox55.Name = "textBox55";
            this.textBox55.ReadOnly = true;
            this.textBox55.Size = new System.Drawing.Size(202, 62);
            this.textBox55.TabIndex = 13;
            this.textBox55.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button89
            // 
            this.button89.Location = new System.Drawing.Point(419, 124);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(202, 23);
            this.button89.TabIndex = 12;
            this.button89.Text = "Remove Field";
            this.button89.UseVisualStyleBackColor = true;
            // 
            // button90
            // 
            this.button90.Location = new System.Drawing.Point(525, 153);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(96, 23);
            this.button90.TabIndex = 11;
            this.button90.Text = "Down";
            this.button90.UseVisualStyleBackColor = true;
            // 
            // button91
            // 
            this.button91.Location = new System.Drawing.Point(419, 153);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(96, 23);
            this.button91.TabIndex = 10;
            this.button91.Text = "Up";
            this.button91.UseVisualStyleBackColor = true;
            // 
            // button92
            // 
            this.button92.Location = new System.Drawing.Point(419, 182);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(202, 23);
            this.button92.TabIndex = 9;
            this.button92.Text = "Restore Default Field Data";
            this.button92.UseVisualStyleBackColor = true;
            // 
            // button93
            // 
            this.button93.Location = new System.Drawing.Point(211, 202);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(177, 23);
            this.button93.TabIndex = 6;
            this.button93.Text = "Add Field";
            this.button93.UseVisualStyleBackColor = true;
            // 
            // button94
            // 
            this.button94.Location = new System.Drawing.Point(16, 202);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(177, 23);
            this.button94.TabIndex = 5;
            this.button94.Text = "Add Field";
            this.button94.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(208, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(80, 13);
            this.label33.TabIndex = 4;
            this.label33.Text = "Standard Fields";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 23);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(86, 13);
            this.label34.TabIndex = 3;
            this.label34.Text = "Aggregate Fields";
            // 
            // listBox14
            // 
            this.listBox14.FormattingEnabled = true;
            this.listBox14.Items.AddRange(new object[] {
            "BatchTotalDocsCount",
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterBatch",
            "LineCounterCustomer",
            "LineCounterFile",
            "LineCounterLockbox",
            "Static",
            "Time"});
            this.listBox14.Location = new System.Drawing.Point(211, 39);
            this.listBox14.Name = "listBox14";
            this.listBox14.Size = new System.Drawing.Size(177, 160);
            this.listBox14.TabIndex = 1;
            // 
            // listBox15
            // 
            this.listBox15.FormattingEnabled = true;
            this.listBox15.Items.AddRange(new object[] {
            "CountChecks",
            "CountDetail",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox15.Location = new System.Drawing.Point(16, 39);
            this.listBox15.Name = "listBox15";
            this.listBox15.Size = new System.Drawing.Size(177, 160);
            this.listBox15.TabIndex = 0;
            // 
            // tabPage25
            // 
            this.tabPage25.Controls.Add(this.textBox56);
            this.tabPage25.Controls.Add(this.textBox57);
            this.tabPage25.Controls.Add(this.textBox58);
            this.tabPage25.Controls.Add(this.button95);
            this.tabPage25.Controls.Add(this.button96);
            this.tabPage25.Controls.Add(this.button97);
            this.tabPage25.Controls.Add(this.lvBatchOrderByColumns);
            this.tabPage25.Location = new System.Drawing.Point(4, 22);
            this.tabPage25.Name = "tabPage25";
            this.tabPage25.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage25.Size = new System.Drawing.Size(807, 592);
            this.tabPage25.TabIndex = 1;
            this.tabPage25.Text = "Order Data";
            this.tabPage25.UseVisualStyleBackColor = true;
            // 
            // textBox56
            // 
            this.textBox56.BackColor = System.Drawing.SystemColors.Window;
            this.textBox56.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox56.Location = new System.Drawing.Point(12, 54);
            this.textBox56.Multiline = true;
            this.textBox56.Name = "textBox56";
            this.textBox56.ReadOnly = true;
            this.textBox56.Size = new System.Drawing.Size(473, 33);
            this.textBox56.TabIndex = 20;
            this.textBox56.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox57
            // 
            this.textBox57.BackColor = System.Drawing.SystemColors.Window;
            this.textBox57.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox57.Location = new System.Drawing.Point(12, 12);
            this.textBox57.Multiline = true;
            this.textBox57.Name = "textBox57";
            this.textBox57.ReadOnly = true;
            this.textBox57.Size = new System.Drawing.Size(473, 33);
            this.textBox57.TabIndex = 19;
            this.textBox57.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox58
            // 
            this.textBox58.BackColor = System.Drawing.SystemColors.Window;
            this.textBox58.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox58.Location = new System.Drawing.Point(492, 99);
            this.textBox58.Multiline = true;
            this.textBox58.Name = "textBox58";
            this.textBox58.ReadOnly = true;
            this.textBox58.Size = new System.Drawing.Size(202, 62);
            this.textBox58.TabIndex = 18;
            this.textBox58.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button95
            // 
            this.button95.Location = new System.Drawing.Point(492, 167);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(202, 23);
            this.button95.TabIndex = 17;
            this.button95.Text = "Remove Field";
            this.button95.UseVisualStyleBackColor = true;
            // 
            // button96
            // 
            this.button96.Location = new System.Drawing.Point(598, 196);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(96, 23);
            this.button96.TabIndex = 16;
            this.button96.Text = "Down";
            this.button96.UseVisualStyleBackColor = true;
            // 
            // button97
            // 
            this.button97.Location = new System.Drawing.Point(492, 196);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(96, 23);
            this.button97.TabIndex = 15;
            this.button97.Text = "Up";
            this.button97.UseVisualStyleBackColor = true;
            // 
            // lvBatchOrderByColumns
            // 
            this.lvBatchOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader59,
            this.columnHeader60});
            this.lvBatchOrderByColumns.FullRowSelect = true;
            this.lvBatchOrderByColumns.GridLines = true;
            this.lvBatchOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvBatchOrderByColumns.Name = "lvBatchOrderByColumns";
            this.lvBatchOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvBatchOrderByColumns.TabIndex = 12;
            this.lvBatchOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvBatchOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader59
            // 
            this.columnHeader59.Text = "Field Name";
            this.columnHeader59.Width = 345;
            // 
            // columnHeader60
            // 
            this.columnHeader60.Text = "Asc/Desc";
            this.columnHeader60.Width = 120;
            // 
            // tabPage26
            // 
            this.tabPage26.Controls.Add(this.textBox59);
            this.tabPage26.Controls.Add(this.textBox60);
            this.tabPage26.Controls.Add(this.button98);
            this.tabPage26.Controls.Add(this.button99);
            this.tabPage26.Controls.Add(this.textBox61);
            this.tabPage26.Controls.Add(this.textBox62);
            this.tabPage26.Controls.Add(this.textBox63);
            this.tabPage26.Controls.Add(this.button100);
            this.tabPage26.Controls.Add(this.button101);
            this.tabPage26.Controls.Add(this.button102);
            this.tabPage26.Controls.Add(this.listView15);
            this.tabPage26.Location = new System.Drawing.Point(4, 22);
            this.tabPage26.Name = "tabPage26";
            this.tabPage26.Size = new System.Drawing.Size(807, 592);
            this.tabPage26.TabIndex = 2;
            this.tabPage26.Text = "Limit Data";
            this.tabPage26.UseVisualStyleBackColor = true;
            // 
            // textBox59
            // 
            this.textBox59.BackColor = System.Drawing.SystemColors.Window;
            this.textBox59.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox59.Location = new System.Drawing.Point(12, 360);
            this.textBox59.Multiline = true;
            this.textBox59.Name = "textBox59";
            this.textBox59.Size = new System.Drawing.Size(473, 18);
            this.textBox59.TabIndex = 31;
            this.textBox59.Text = "\"Where clause\" as text";
            // 
            // textBox60
            // 
            this.textBox60.Location = new System.Drawing.Point(12, 384);
            this.textBox60.Multiline = true;
            this.textBox60.Name = "textBox60";
            this.textBox60.ReadOnly = true;
            this.textBox60.Size = new System.Drawing.Size(473, 123);
            this.textBox60.TabIndex = 30;
            // 
            // button98
            // 
            this.button98.Location = new System.Drawing.Point(492, 225);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(202, 23);
            this.button98.TabIndex = 29;
            this.button98.Text = "View as Text";
            this.button98.UseVisualStyleBackColor = true;
            // 
            // button99
            // 
            this.button99.Location = new System.Drawing.Point(492, 254);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(202, 23);
            this.button99.TabIndex = 28;
            this.button99.Text = "Validate";
            this.button99.UseVisualStyleBackColor = true;
            // 
            // textBox61
            // 
            this.textBox61.BackColor = System.Drawing.SystemColors.Window;
            this.textBox61.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox61.Location = new System.Drawing.Point(12, 66);
            this.textBox61.Multiline = true;
            this.textBox61.Name = "textBox61";
            this.textBox61.ReadOnly = true;
            this.textBox61.Size = new System.Drawing.Size(473, 18);
            this.textBox61.TabIndex = 27;
            this.textBox61.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox62
            // 
            this.textBox62.BackColor = System.Drawing.SystemColors.Window;
            this.textBox62.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox62.Location = new System.Drawing.Point(12, 12);
            this.textBox62.Multiline = true;
            this.textBox62.Name = "textBox62";
            this.textBox62.ReadOnly = true;
            this.textBox62.Size = new System.Drawing.Size(473, 33);
            this.textBox62.TabIndex = 26;
            this.textBox62.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox63
            // 
            this.textBox63.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox63.Location = new System.Drawing.Point(492, 99);
            this.textBox63.Multiline = true;
            this.textBox63.Name = "textBox63";
            this.textBox63.Size = new System.Drawing.Size(202, 62);
            this.textBox63.TabIndex = 25;
            this.textBox63.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button100
            // 
            this.button100.Location = new System.Drawing.Point(492, 167);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(202, 23);
            this.button100.TabIndex = 24;
            this.button100.Text = "Remove Field";
            this.button100.UseVisualStyleBackColor = true;
            // 
            // button101
            // 
            this.button101.Location = new System.Drawing.Point(598, 196);
            this.button101.Name = "button101";
            this.button101.Size = new System.Drawing.Size(96, 23);
            this.button101.TabIndex = 23;
            this.button101.Text = "Down";
            this.button101.UseVisualStyleBackColor = true;
            // 
            // button102
            // 
            this.button102.Location = new System.Drawing.Point(492, 196);
            this.button102.Name = "button102";
            this.button102.Size = new System.Drawing.Size(96, 23);
            this.button102.TabIndex = 22;
            this.button102.Text = "Up";
            this.button102.UseVisualStyleBackColor = true;
            // 
            // listView15
            // 
            this.listView15.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader61,
            this.columnHeader62});
            this.listView15.FullRowSelect = true;
            this.listView15.GridLines = true;
            this.listView15.Location = new System.Drawing.Point(12, 90);
            this.listView15.Name = "listView15";
            this.listView15.Size = new System.Drawing.Size(473, 227);
            this.listView15.TabIndex = 21;
            this.listView15.UseCompatibleStateImageBehavior = false;
            this.listView15.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader61
            // 
            this.columnHeader61.Text = "Field Name";
            this.columnHeader61.Width = 345;
            // 
            // columnHeader62
            // 
            this.columnHeader62.Text = "Asc/Desc";
            this.columnHeader62.Width = 120;
            // 
            // panel11
            // 
            this.panel11.Controls.Add(this.button103);
            this.panel11.Controls.Add(this.lstBatchColumns);
            this.panel11.Controls.Add(this.label35);
            this.panel11.Controls.Add(this.splitter4);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel11.Location = new System.Drawing.Point(0, 0);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(185, 618);
            this.panel11.TabIndex = 1;
            // 
            // button103
            // 
            this.button103.Location = new System.Drawing.Point(8, 515);
            this.button103.Name = "button103";
            this.button103.Size = new System.Drawing.Size(75, 23);
            this.button103.TabIndex = 3;
            this.button103.Text = "Add Field";
            this.button103.UseVisualStyleBackColor = true;
            // 
            // lstBatchColumns
            // 
            this.lstBatchColumns.FormattingEnabled = true;
            this.lstBatchColumns.Location = new System.Drawing.Point(8, 24);
            this.lstBatchColumns.Name = "lstBatchColumns";
            this.lstBatchColumns.Size = new System.Drawing.Size(165, 485);
            this.lstBatchColumns.TabIndex = 2;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(5, 10);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(60, 13);
            this.label35.TabIndex = 1;
            this.label35.Text = "Data Fields";
            // 
            // splitter4
            // 
            this.splitter4.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter4.Location = new System.Drawing.Point(182, 0);
            this.splitter4.Name = "splitter4";
            this.splitter4.Size = new System.Drawing.Size(3, 618);
            this.splitter4.TabIndex = 0;
            this.splitter4.TabStop = false;
            // 
            // tpLockbox
            // 
            this.tpLockbox.Controls.Add(this.tabControl4);
            this.tpLockbox.Controls.Add(this.panel9);
            this.tpLockbox.Location = new System.Drawing.Point(4, 22);
            this.tpLockbox.Name = "tpLockbox";
            this.tpLockbox.Size = new System.Drawing.Size(1000, 618);
            this.tpLockbox.TabIndex = 4;
            this.tpLockbox.Text = "Lockbox";
            this.tpLockbox.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage21);
            this.tabControl4.Controls.Add(this.tabPage22);
            this.tabControl4.Controls.Add(this.tabPage23);
            this.tabControl4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl4.Location = new System.Drawing.Point(185, 0);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(815, 618);
            this.tabControl4.TabIndex = 2;
            // 
            // tabPage21
            // 
            this.tabPage21.Controls.Add(this.panel8);
            this.tabPage21.Controls.Add(this.textBox42);
            this.tabPage21.Controls.Add(this.button66);
            this.tabPage21.Controls.Add(this.button67);
            this.tabPage21.Controls.Add(this.button68);
            this.tabPage21.Controls.Add(this.button69);
            this.tabPage21.Controls.Add(this.groupBox14);
            this.tabPage21.Controls.Add(this.label28);
            this.tabPage21.Controls.Add(this.cboLockboxLayouts);
            this.tabPage21.Controls.Add(this.lvLockboxFields);
            this.tabPage21.Controls.Add(this.textBox43);
            this.tabPage21.Controls.Add(this.groupBox15);
            this.tabPage21.Location = new System.Drawing.Point(4, 22);
            this.tabPage21.Name = "tabPage21";
            this.tabPage21.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage21.Size = new System.Drawing.Size(807, 592);
            this.tabPage21.TabIndex = 0;
            this.tabPage21.Text = "Create Layouts";
            this.tabPage21.UseVisualStyleBackColor = true;
            // 
            // panel8
            // 
            this.panel8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.panel8.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel8.ForeColor = System.Drawing.SystemColors.Control;
            this.panel8.Location = new System.Drawing.Point(6, 103);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(782, 1);
            this.panel8.TabIndex = 20;
            // 
            // textBox42
            // 
            this.textBox42.BackColor = System.Drawing.SystemColors.Window;
            this.textBox42.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox42.Location = new System.Drawing.Point(6, 13);
            this.textBox42.Name = "textBox42";
            this.textBox42.ReadOnly = true;
            this.textBox42.Size = new System.Drawing.Size(763, 13);
            this.textBox42.TabIndex = 19;
            this.textBox42.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button66
            // 
            this.button66.Location = new System.Drawing.Point(568, 45);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(130, 23);
            this.button66.TabIndex = 18;
            this.button66.Text = "Delete Layout";
            this.button66.UseVisualStyleBackColor = true;
            // 
            // button67
            // 
            this.button67.Location = new System.Drawing.Point(568, 74);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(130, 23);
            this.button67.TabIndex = 17;
            this.button67.Text = "Restore Original Layout";
            this.button67.UseVisualStyleBackColor = true;
            // 
            // button68
            // 
            this.button68.Location = new System.Drawing.Point(432, 45);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(130, 23);
            this.button68.TabIndex = 16;
            this.button68.Text = "New Layout";
            this.button68.UseVisualStyleBackColor = true;
            // 
            // button69
            // 
            this.button69.Location = new System.Drawing.Point(432, 74);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(130, 23);
            this.button69.TabIndex = 15;
            this.button69.Text = "Test Layout";
            this.button69.UseVisualStyleBackColor = true;
            // 
            // groupBox14
            // 
            this.groupBox14.Controls.Add(this.rdoLockboxRecordTypeTrailer);
            this.groupBox14.Controls.Add(this.rdoLockboxRecordTypeHeader);
            this.groupBox14.Location = new System.Drawing.Point(248, 45);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(146, 52);
            this.groupBox14.TabIndex = 14;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Record Type";
            // 
            // rdoLockboxRecordTypeTrailer
            // 
            this.rdoLockboxRecordTypeTrailer.AutoSize = true;
            this.rdoLockboxRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoLockboxRecordTypeTrailer.Name = "rdoLockboxRecordTypeTrailer";
            this.rdoLockboxRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoLockboxRecordTypeTrailer.TabIndex = 1;
            this.rdoLockboxRecordTypeTrailer.TabStop = true;
            this.rdoLockboxRecordTypeTrailer.Text = "Trailer";
            this.rdoLockboxRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoLockboxRecordTypeHeader
            // 
            this.rdoLockboxRecordTypeHeader.AutoSize = true;
            this.rdoLockboxRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoLockboxRecordTypeHeader.Name = "rdoLockboxRecordTypeHeader";
            this.rdoLockboxRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoLockboxRecordTypeHeader.TabIndex = 0;
            this.rdoLockboxRecordTypeHeader.TabStop = true;
            this.rdoLockboxRecordTypeHeader.Text = "Header";
            this.rdoLockboxRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 45);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(79, 13);
            this.label28.TabIndex = 13;
            this.label28.Text = "Current Layout:";
            // 
            // cboLockboxLayouts
            // 
            this.cboLockboxLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboLockboxLayouts.FormattingEnabled = true;
            this.cboLockboxLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboLockboxLayouts.Name = "cboLockboxLayouts";
            this.cboLockboxLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboLockboxLayouts.TabIndex = 12;
            // 
            // lvLockboxFields
            // 
            this.lvLockboxFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader41,
            this.columnHeader42,
            this.columnHeader43,
            this.columnHeader44,
            this.columnHeader45,
            this.columnHeader46,
            this.columnHeader47});
            this.lvLockboxFields.FullRowSelect = true;
            this.lvLockboxFields.GridLines = true;
            this.lvLockboxFields.Location = new System.Drawing.Point(8, 173);
            this.lvLockboxFields.Name = "lvLockboxFields";
            this.lvLockboxFields.Size = new System.Drawing.Size(651, 169);
            this.lvLockboxFields.TabIndex = 11;
            this.lvLockboxFields.UseCompatibleStateImageBehavior = false;
            this.lvLockboxFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader41
            // 
            this.columnHeader41.Text = "Field Name";
            this.columnHeader41.Width = 120;
            // 
            // columnHeader42
            // 
            this.columnHeader42.Text = "Display Name";
            this.columnHeader42.Width = 120;
            // 
            // columnHeader43
            // 
            this.columnHeader43.Text = "Column Width";
            this.columnHeader43.Width = 85;
            // 
            // columnHeader44
            // 
            this.columnHeader44.Text = "Pad Char";
            this.columnHeader44.Width = 59;
            // 
            // columnHeader45
            // 
            this.columnHeader45.Text = "Justify";
            this.columnHeader45.Width = 44;
            // 
            // columnHeader46
            // 
            this.columnHeader46.Text = "Quotes";
            this.columnHeader46.Width = 69;
            // 
            // columnHeader47
            // 
            this.columnHeader47.Text = "Format";
            this.columnHeader47.Width = 150;
            // 
            // textBox43
            // 
            this.textBox43.BackColor = System.Drawing.SystemColors.Window;
            this.textBox43.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox43.Location = new System.Drawing.Point(6, 153);
            this.textBox43.Name = "textBox43";
            this.textBox43.ReadOnly = true;
            this.textBox43.Size = new System.Drawing.Size(677, 13);
            this.textBox43.TabIndex = 10;
            this.textBox43.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox15
            // 
            this.groupBox15.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox15.Controls.Add(this.textBox44);
            this.groupBox15.Controls.Add(this.button70);
            this.groupBox15.Controls.Add(this.button71);
            this.groupBox15.Controls.Add(this.button72);
            this.groupBox15.Controls.Add(this.button73);
            this.groupBox15.Controls.Add(this.button74);
            this.groupBox15.Controls.Add(this.button75);
            this.groupBox15.Controls.Add(this.label29);
            this.groupBox15.Controls.Add(this.label30);
            this.groupBox15.Controls.Add(this.listBox11);
            this.groupBox15.Controls.Add(this.listBox12);
            this.groupBox15.Location = new System.Drawing.Point(6, 348);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(782, 238);
            this.groupBox15.TabIndex = 0;
            this.groupBox15.TabStop = false;
            // 
            // textBox44
            // 
            this.textBox44.BackColor = System.Drawing.SystemColors.Window;
            this.textBox44.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox44.Location = new System.Drawing.Point(419, 56);
            this.textBox44.Multiline = true;
            this.textBox44.Name = "textBox44";
            this.textBox44.ReadOnly = true;
            this.textBox44.Size = new System.Drawing.Size(202, 62);
            this.textBox44.TabIndex = 13;
            this.textBox44.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button70
            // 
            this.button70.Location = new System.Drawing.Point(419, 124);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(202, 23);
            this.button70.TabIndex = 12;
            this.button70.Text = "Remove Field";
            this.button70.UseVisualStyleBackColor = true;
            // 
            // button71
            // 
            this.button71.Location = new System.Drawing.Point(525, 153);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(96, 23);
            this.button71.TabIndex = 11;
            this.button71.Text = "Down";
            this.button71.UseVisualStyleBackColor = true;
            // 
            // button72
            // 
            this.button72.Location = new System.Drawing.Point(419, 153);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(96, 23);
            this.button72.TabIndex = 10;
            this.button72.Text = "Up";
            this.button72.UseVisualStyleBackColor = true;
            // 
            // button73
            // 
            this.button73.Location = new System.Drawing.Point(419, 182);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(202, 23);
            this.button73.TabIndex = 9;
            this.button73.Text = "Restore Default Field Data";
            this.button73.UseVisualStyleBackColor = true;
            // 
            // button74
            // 
            this.button74.Location = new System.Drawing.Point(211, 202);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(177, 23);
            this.button74.TabIndex = 6;
            this.button74.Text = "Add Field";
            this.button74.UseVisualStyleBackColor = true;
            // 
            // button75
            // 
            this.button75.Location = new System.Drawing.Point(16, 202);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(177, 23);
            this.button75.TabIndex = 5;
            this.button75.Text = "Add Field";
            this.button75.UseVisualStyleBackColor = true;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(208, 23);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(80, 13);
            this.label29.TabIndex = 4;
            this.label29.Text = "Standard Fields";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(13, 23);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(86, 13);
            this.label30.TabIndex = 3;
            this.label30.Text = "Aggregate Fields";
            // 
            // listBox11
            // 
            this.listBox11.FormattingEnabled = true;
            this.listBox11.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterCustomer",
            "LineCounterLockbox",
            "LineCounterFile",
            "ProcessingRunDate",
            "Static",
            "Time"});
            this.listBox11.Location = new System.Drawing.Point(211, 39);
            this.listBox11.Name = "listBox11";
            this.listBox11.Size = new System.Drawing.Size(177, 160);
            this.listBox11.TabIndex = 1;
            // 
            // listBox12
            // 
            this.listBox12.FormattingEnabled = true;
            this.listBox12.Items.AddRange(new object[] {
            "CountBatches",
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox12.Location = new System.Drawing.Point(16, 39);
            this.listBox12.Name = "listBox12";
            this.listBox12.Size = new System.Drawing.Size(177, 160);
            this.listBox12.TabIndex = 0;
            // 
            // tabPage22
            // 
            this.tabPage22.Controls.Add(this.textBox45);
            this.tabPage22.Controls.Add(this.textBox46);
            this.tabPage22.Controls.Add(this.textBox47);
            this.tabPage22.Controls.Add(this.button76);
            this.tabPage22.Controls.Add(this.button77);
            this.tabPage22.Controls.Add(this.button78);
            this.tabPage22.Controls.Add(this.lvLockboxOrderByColumns);
            this.tabPage22.Location = new System.Drawing.Point(4, 22);
            this.tabPage22.Name = "tabPage22";
            this.tabPage22.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage22.Size = new System.Drawing.Size(807, 592);
            this.tabPage22.TabIndex = 1;
            this.tabPage22.Text = "Order Data";
            this.tabPage22.UseVisualStyleBackColor = true;
            // 
            // textBox45
            // 
            this.textBox45.BackColor = System.Drawing.SystemColors.Window;
            this.textBox45.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox45.Location = new System.Drawing.Point(12, 54);
            this.textBox45.Multiline = true;
            this.textBox45.Name = "textBox45";
            this.textBox45.ReadOnly = true;
            this.textBox45.Size = new System.Drawing.Size(473, 33);
            this.textBox45.TabIndex = 20;
            this.textBox45.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox46
            // 
            this.textBox46.BackColor = System.Drawing.SystemColors.Window;
            this.textBox46.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox46.Location = new System.Drawing.Point(12, 12);
            this.textBox46.Multiline = true;
            this.textBox46.Name = "textBox46";
            this.textBox46.ReadOnly = true;
            this.textBox46.Size = new System.Drawing.Size(473, 33);
            this.textBox46.TabIndex = 19;
            this.textBox46.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox47
            // 
            this.textBox47.BackColor = System.Drawing.SystemColors.Window;
            this.textBox47.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox47.Location = new System.Drawing.Point(492, 99);
            this.textBox47.Multiline = true;
            this.textBox47.Name = "textBox47";
            this.textBox47.ReadOnly = true;
            this.textBox47.Size = new System.Drawing.Size(202, 62);
            this.textBox47.TabIndex = 18;
            this.textBox47.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button76
            // 
            this.button76.Location = new System.Drawing.Point(492, 167);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(202, 23);
            this.button76.TabIndex = 17;
            this.button76.Text = "Remove Field";
            this.button76.UseVisualStyleBackColor = true;
            // 
            // button77
            // 
            this.button77.Location = new System.Drawing.Point(598, 196);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(96, 23);
            this.button77.TabIndex = 16;
            this.button77.Text = "Down";
            this.button77.UseVisualStyleBackColor = true;
            // 
            // button78
            // 
            this.button78.Location = new System.Drawing.Point(492, 196);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(96, 23);
            this.button78.TabIndex = 15;
            this.button78.Text = "Up";
            this.button78.UseVisualStyleBackColor = true;
            // 
            // lvLockboxOrderByColumns
            // 
            this.lvLockboxOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader48,
            this.columnHeader49});
            this.lvLockboxOrderByColumns.FullRowSelect = true;
            this.lvLockboxOrderByColumns.GridLines = true;
            this.lvLockboxOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvLockboxOrderByColumns.Name = "lvLockboxOrderByColumns";
            this.lvLockboxOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvLockboxOrderByColumns.TabIndex = 12;
            this.lvLockboxOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvLockboxOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader48
            // 
            this.columnHeader48.Text = "Field Name";
            this.columnHeader48.Width = 345;
            // 
            // columnHeader49
            // 
            this.columnHeader49.Text = "Asc/Desc";
            this.columnHeader49.Width = 120;
            // 
            // tabPage23
            // 
            this.tabPage23.Controls.Add(this.textBox48);
            this.tabPage23.Controls.Add(this.textBox49);
            this.tabPage23.Controls.Add(this.button79);
            this.tabPage23.Controls.Add(this.button80);
            this.tabPage23.Controls.Add(this.textBox50);
            this.tabPage23.Controls.Add(this.textBox51);
            this.tabPage23.Controls.Add(this.textBox52);
            this.tabPage23.Controls.Add(this.button81);
            this.tabPage23.Controls.Add(this.button82);
            this.tabPage23.Controls.Add(this.button83);
            this.tabPage23.Controls.Add(this.listView12);
            this.tabPage23.Location = new System.Drawing.Point(4, 22);
            this.tabPage23.Name = "tabPage23";
            this.tabPage23.Size = new System.Drawing.Size(807, 592);
            this.tabPage23.TabIndex = 2;
            this.tabPage23.Text = "Limit Data";
            this.tabPage23.UseVisualStyleBackColor = true;
            // 
            // textBox48
            // 
            this.textBox48.BackColor = System.Drawing.SystemColors.Window;
            this.textBox48.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox48.Location = new System.Drawing.Point(12, 360);
            this.textBox48.Multiline = true;
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(473, 18);
            this.textBox48.TabIndex = 31;
            this.textBox48.Text = "\"Where clause\" as text";
            // 
            // textBox49
            // 
            this.textBox49.Location = new System.Drawing.Point(12, 384);
            this.textBox49.Multiline = true;
            this.textBox49.Name = "textBox49";
            this.textBox49.ReadOnly = true;
            this.textBox49.Size = new System.Drawing.Size(473, 123);
            this.textBox49.TabIndex = 30;
            // 
            // button79
            // 
            this.button79.Location = new System.Drawing.Point(492, 225);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(202, 23);
            this.button79.TabIndex = 29;
            this.button79.Text = "View as Text";
            this.button79.UseVisualStyleBackColor = true;
            // 
            // button80
            // 
            this.button80.Location = new System.Drawing.Point(492, 254);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(202, 23);
            this.button80.TabIndex = 28;
            this.button80.Text = "Validate";
            this.button80.UseVisualStyleBackColor = true;
            // 
            // textBox50
            // 
            this.textBox50.BackColor = System.Drawing.SystemColors.Window;
            this.textBox50.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox50.Location = new System.Drawing.Point(12, 66);
            this.textBox50.Multiline = true;
            this.textBox50.Name = "textBox50";
            this.textBox50.ReadOnly = true;
            this.textBox50.Size = new System.Drawing.Size(473, 18);
            this.textBox50.TabIndex = 27;
            this.textBox50.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox51
            // 
            this.textBox51.BackColor = System.Drawing.SystemColors.Window;
            this.textBox51.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox51.Location = new System.Drawing.Point(12, 12);
            this.textBox51.Multiline = true;
            this.textBox51.Name = "textBox51";
            this.textBox51.ReadOnly = true;
            this.textBox51.Size = new System.Drawing.Size(473, 33);
            this.textBox51.TabIndex = 26;
            this.textBox51.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox52
            // 
            this.textBox52.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox52.Location = new System.Drawing.Point(492, 99);
            this.textBox52.Multiline = true;
            this.textBox52.Name = "textBox52";
            this.textBox52.Size = new System.Drawing.Size(202, 62);
            this.textBox52.TabIndex = 25;
            this.textBox52.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button81
            // 
            this.button81.Location = new System.Drawing.Point(492, 167);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(202, 23);
            this.button81.TabIndex = 24;
            this.button81.Text = "Remove Field";
            this.button81.UseVisualStyleBackColor = true;
            // 
            // button82
            // 
            this.button82.Location = new System.Drawing.Point(598, 196);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(96, 23);
            this.button82.TabIndex = 23;
            this.button82.Text = "Down";
            this.button82.UseVisualStyleBackColor = true;
            // 
            // button83
            // 
            this.button83.Location = new System.Drawing.Point(492, 196);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(96, 23);
            this.button83.TabIndex = 22;
            this.button83.Text = "Up";
            this.button83.UseVisualStyleBackColor = true;
            // 
            // listView12
            // 
            this.listView12.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader50,
            this.columnHeader51});
            this.listView12.FullRowSelect = true;
            this.listView12.GridLines = true;
            this.listView12.Location = new System.Drawing.Point(12, 90);
            this.listView12.Name = "listView12";
            this.listView12.Size = new System.Drawing.Size(473, 227);
            this.listView12.TabIndex = 21;
            this.listView12.UseCompatibleStateImageBehavior = false;
            this.listView12.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader50
            // 
            this.columnHeader50.Text = "Field Name";
            this.columnHeader50.Width = 345;
            // 
            // columnHeader51
            // 
            this.columnHeader51.Text = "Asc/Desc";
            this.columnHeader51.Width = 120;
            // 
            // panel9
            // 
            this.panel9.Controls.Add(this.button84);
            this.panel9.Controls.Add(this.lstLockboxColumns);
            this.panel9.Controls.Add(this.label31);
            this.panel9.Controls.Add(this.splitter3);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel9.Location = new System.Drawing.Point(0, 0);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(185, 618);
            this.panel9.TabIndex = 1;
            // 
            // button84
            // 
            this.button84.Location = new System.Drawing.Point(8, 515);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(75, 23);
            this.button84.TabIndex = 3;
            this.button84.Text = "Add Field";
            this.button84.UseVisualStyleBackColor = true;
            // 
            // lstLockboxColumns
            // 
            this.lstLockboxColumns.FormattingEnabled = true;
            this.lstLockboxColumns.Location = new System.Drawing.Point(8, 24);
            this.lstLockboxColumns.Name = "lstLockboxColumns";
            this.lstLockboxColumns.Size = new System.Drawing.Size(165, 485);
            this.lstLockboxColumns.TabIndex = 2;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(5, 10);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(60, 13);
            this.label31.TabIndex = 1;
            this.label31.Text = "Data Fields";
            // 
            // splitter3
            // 
            this.splitter3.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter3.Location = new System.Drawing.Point(182, 0);
            this.splitter3.Name = "splitter3";
            this.splitter3.Size = new System.Drawing.Size(3, 618);
            this.splitter3.TabIndex = 0;
            this.splitter3.TabStop = false;
            // 
            // tpCustomer
            // 
            this.tpCustomer.Controls.Add(this.tabControl3);
            this.tpCustomer.Controls.Add(this.panel7);
            this.tpCustomer.Location = new System.Drawing.Point(4, 22);
            this.tpCustomer.Name = "tpCustomer";
            this.tpCustomer.Size = new System.Drawing.Size(1000, 618);
            this.tpCustomer.TabIndex = 3;
            this.tpCustomer.Text = "Customer";
            this.tpCustomer.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage18);
            this.tabControl3.Controls.Add(this.tabPage19);
            this.tabControl3.Controls.Add(this.tabPage20);
            this.tabControl3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl3.Location = new System.Drawing.Point(185, 0);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(815, 618);
            this.tabControl3.TabIndex = 2;
            // 
            // tabPage18
            // 
            this.tabPage18.Controls.Add(this.panel6);
            this.tabPage18.Controls.Add(this.textBox31);
            this.tabPage18.Controls.Add(this.button47);
            this.tabPage18.Controls.Add(this.button48);
            this.tabPage18.Controls.Add(this.button49);
            this.tabPage18.Controls.Add(this.button50);
            this.tabPage18.Controls.Add(this.groupBox12);
            this.tabPage18.Controls.Add(this.label24);
            this.tabPage18.Controls.Add(this.cboCustomerLayouts);
            this.tabPage18.Controls.Add(this.lvCustomerFields);
            this.tabPage18.Controls.Add(this.textBox32);
            this.tabPage18.Controls.Add(this.groupBox13);
            this.tabPage18.Location = new System.Drawing.Point(4, 22);
            this.tabPage18.Name = "tabPage18";
            this.tabPage18.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage18.Size = new System.Drawing.Size(807, 592);
            this.tabPage18.TabIndex = 0;
            this.tabPage18.Text = "Create Layouts";
            this.tabPage18.UseVisualStyleBackColor = true;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel6.ForeColor = System.Drawing.SystemColors.Control;
            this.panel6.Location = new System.Drawing.Point(6, 103);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(692, 1);
            this.panel6.TabIndex = 20;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.SystemColors.Window;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox31.Location = new System.Drawing.Point(6, 13);
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.Size = new System.Drawing.Size(763, 13);
            this.textBox31.TabIndex = 19;
            this.textBox31.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button47
            // 
            this.button47.Location = new System.Drawing.Point(568, 45);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(130, 23);
            this.button47.TabIndex = 18;
            this.button47.Text = "Delete Layout";
            this.button47.UseVisualStyleBackColor = true;
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(568, 74);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(130, 23);
            this.button48.TabIndex = 17;
            this.button48.Text = "Restore Original Layout";
            this.button48.UseVisualStyleBackColor = true;
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(432, 45);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(130, 23);
            this.button49.TabIndex = 16;
            this.button49.Text = "New Layout";
            this.button49.UseVisualStyleBackColor = true;
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(432, 74);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(130, 23);
            this.button50.TabIndex = 15;
            this.button50.Text = "Test Layout";
            this.button50.UseVisualStyleBackColor = true;
            // 
            // groupBox12
            // 
            this.groupBox12.Controls.Add(this.rdoCustomerRecordTypeTrailer);
            this.groupBox12.Controls.Add(this.rdoCustomerRecordTypeHeader);
            this.groupBox12.Location = new System.Drawing.Point(248, 45);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(146, 52);
            this.groupBox12.TabIndex = 14;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Record Type";
            // 
            // rdoCustomerRecordTypeTrailer
            // 
            this.rdoCustomerRecordTypeTrailer.AutoSize = true;
            this.rdoCustomerRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoCustomerRecordTypeTrailer.Name = "rdoCustomerRecordTypeTrailer";
            this.rdoCustomerRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoCustomerRecordTypeTrailer.TabIndex = 1;
            this.rdoCustomerRecordTypeTrailer.TabStop = true;
            this.rdoCustomerRecordTypeTrailer.Text = "Trailer";
            this.rdoCustomerRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoCustomerRecordTypeHeader
            // 
            this.rdoCustomerRecordTypeHeader.AutoSize = true;
            this.rdoCustomerRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoCustomerRecordTypeHeader.Name = "rdoCustomerRecordTypeHeader";
            this.rdoCustomerRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoCustomerRecordTypeHeader.TabIndex = 0;
            this.rdoCustomerRecordTypeHeader.TabStop = true;
            this.rdoCustomerRecordTypeHeader.Text = "Header";
            this.rdoCustomerRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(3, 45);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(79, 13);
            this.label24.TabIndex = 13;
            this.label24.Text = "Current Layout:";
            // 
            // cboCustomerLayouts
            // 
            this.cboCustomerLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboCustomerLayouts.FormattingEnabled = true;
            this.cboCustomerLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboCustomerLayouts.Name = "cboCustomerLayouts";
            this.cboCustomerLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboCustomerLayouts.TabIndex = 12;
            // 
            // lvCustomerFields
            // 
            this.lvCustomerFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader30,
            this.columnHeader31,
            this.columnHeader32,
            this.columnHeader33,
            this.columnHeader34,
            this.columnHeader35,
            this.columnHeader36});
            this.lvCustomerFields.FullRowSelect = true;
            this.lvCustomerFields.GridLines = true;
            this.lvCustomerFields.Location = new System.Drawing.Point(8, 173);
            this.lvCustomerFields.Name = "lvCustomerFields";
            this.lvCustomerFields.Size = new System.Drawing.Size(652, 169);
            this.lvCustomerFields.TabIndex = 11;
            this.lvCustomerFields.UseCompatibleStateImageBehavior = false;
            this.lvCustomerFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader30
            // 
            this.columnHeader30.Text = "Field Name";
            this.columnHeader30.Width = 120;
            // 
            // columnHeader31
            // 
            this.columnHeader31.Text = "Display Name";
            this.columnHeader31.Width = 120;
            // 
            // columnHeader32
            // 
            this.columnHeader32.Text = "Column Width";
            this.columnHeader32.Width = 85;
            // 
            // columnHeader33
            // 
            this.columnHeader33.Text = "Pad Char";
            this.columnHeader33.Width = 59;
            // 
            // columnHeader34
            // 
            this.columnHeader34.Text = "Justify";
            this.columnHeader34.Width = 44;
            // 
            // columnHeader35
            // 
            this.columnHeader35.Text = "Quotes";
            this.columnHeader35.Width = 69;
            // 
            // columnHeader36
            // 
            this.columnHeader36.Text = "Format";
            this.columnHeader36.Width = 150;
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.SystemColors.Window;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox32.Location = new System.Drawing.Point(6, 153);
            this.textBox32.Name = "textBox32";
            this.textBox32.ReadOnly = true;
            this.textBox32.Size = new System.Drawing.Size(677, 13);
            this.textBox32.TabIndex = 10;
            this.textBox32.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.textBox33);
            this.groupBox13.Controls.Add(this.button51);
            this.groupBox13.Controls.Add(this.button52);
            this.groupBox13.Controls.Add(this.button53);
            this.groupBox13.Controls.Add(this.button54);
            this.groupBox13.Controls.Add(this.button55);
            this.groupBox13.Controls.Add(this.button56);
            this.groupBox13.Controls.Add(this.label25);
            this.groupBox13.Controls.Add(this.label26);
            this.groupBox13.Controls.Add(this.listBox8);
            this.groupBox13.Controls.Add(this.listBox9);
            this.groupBox13.Location = new System.Drawing.Point(6, 348);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(786, 238);
            this.groupBox13.TabIndex = 0;
            this.groupBox13.TabStop = false;
            // 
            // textBox33
            // 
            this.textBox33.BackColor = System.Drawing.SystemColors.Window;
            this.textBox33.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox33.Location = new System.Drawing.Point(419, 56);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.ReadOnly = true;
            this.textBox33.Size = new System.Drawing.Size(202, 62);
            this.textBox33.TabIndex = 13;
            this.textBox33.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(419, 124);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(202, 23);
            this.button51.TabIndex = 12;
            this.button51.Text = "Remove Field";
            this.button51.UseVisualStyleBackColor = true;
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(525, 153);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(96, 23);
            this.button52.TabIndex = 11;
            this.button52.Text = "Down";
            this.button52.UseVisualStyleBackColor = true;
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(419, 153);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(96, 23);
            this.button53.TabIndex = 10;
            this.button53.Text = "Up";
            this.button53.UseVisualStyleBackColor = true;
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(419, 182);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(202, 23);
            this.button54.TabIndex = 9;
            this.button54.Text = "Restore Default Field Data";
            this.button54.UseVisualStyleBackColor = true;
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(211, 202);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(177, 23);
            this.button55.TabIndex = 6;
            this.button55.Text = "Add Field";
            this.button55.UseVisualStyleBackColor = true;
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(16, 202);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(177, 23);
            this.button56.TabIndex = 5;
            this.button56.Text = "Add Field";
            this.button56.UseVisualStyleBackColor = true;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(208, 23);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(80, 13);
            this.label25.TabIndex = 4;
            this.label25.Text = "Standard Fields";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(13, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(86, 13);
            this.label26.TabIndex = 3;
            this.label26.Text = "Aggregate Fields";
            // 
            // listBox8
            // 
            this.listBox8.FormattingEnabled = true;
            this.listBox8.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterCustomer",
            "LineCounterFile",
            "ProcessingRunDate",
            "Static",
            "Time"});
            this.listBox8.Location = new System.Drawing.Point(211, 39);
            this.listBox8.Name = "listBox8";
            this.listBox8.Size = new System.Drawing.Size(177, 160);
            this.listBox8.TabIndex = 1;
            // 
            // listBox9
            // 
            this.listBox9.FormattingEnabled = true;
            this.listBox9.Items.AddRange(new object[] {
            "CountBatches",
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox9.Location = new System.Drawing.Point(16, 39);
            this.listBox9.Name = "listBox9";
            this.listBox9.Size = new System.Drawing.Size(177, 160);
            this.listBox9.TabIndex = 0;
            // 
            // tabPage19
            // 
            this.tabPage19.Controls.Add(this.textBox34);
            this.tabPage19.Controls.Add(this.textBox35);
            this.tabPage19.Controls.Add(this.textBox36);
            this.tabPage19.Controls.Add(this.button57);
            this.tabPage19.Controls.Add(this.button58);
            this.tabPage19.Controls.Add(this.button59);
            this.tabPage19.Controls.Add(this.lvCustomerOrderByColumns);
            this.tabPage19.Location = new System.Drawing.Point(4, 22);
            this.tabPage19.Name = "tabPage19";
            this.tabPage19.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage19.Size = new System.Drawing.Size(807, 592);
            this.tabPage19.TabIndex = 1;
            this.tabPage19.Text = "Order Data";
            this.tabPage19.UseVisualStyleBackColor = true;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.SystemColors.Window;
            this.textBox34.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox34.Location = new System.Drawing.Point(12, 54);
            this.textBox34.Multiline = true;
            this.textBox34.Name = "textBox34";
            this.textBox34.ReadOnly = true;
            this.textBox34.Size = new System.Drawing.Size(473, 33);
            this.textBox34.TabIndex = 20;
            this.textBox34.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox35
            // 
            this.textBox35.BackColor = System.Drawing.SystemColors.Window;
            this.textBox35.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox35.Location = new System.Drawing.Point(12, 12);
            this.textBox35.Multiline = true;
            this.textBox35.Name = "textBox35";
            this.textBox35.ReadOnly = true;
            this.textBox35.Size = new System.Drawing.Size(473, 33);
            this.textBox35.TabIndex = 19;
            this.textBox35.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox36
            // 
            this.textBox36.BackColor = System.Drawing.SystemColors.Window;
            this.textBox36.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox36.Location = new System.Drawing.Point(492, 99);
            this.textBox36.Multiline = true;
            this.textBox36.Name = "textBox36";
            this.textBox36.ReadOnly = true;
            this.textBox36.Size = new System.Drawing.Size(202, 62);
            this.textBox36.TabIndex = 18;
            this.textBox36.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(492, 167);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(202, 23);
            this.button57.TabIndex = 17;
            this.button57.Text = "Remove Field";
            this.button57.UseVisualStyleBackColor = true;
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(598, 196);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(96, 23);
            this.button58.TabIndex = 16;
            this.button58.Text = "Down";
            this.button58.UseVisualStyleBackColor = true;
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(492, 196);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(96, 23);
            this.button59.TabIndex = 15;
            this.button59.Text = "Up";
            this.button59.UseVisualStyleBackColor = true;
            // 
            // lvCustomerOrderByColumns
            // 
            this.lvCustomerOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader37,
            this.columnHeader38});
            this.lvCustomerOrderByColumns.FullRowSelect = true;
            this.lvCustomerOrderByColumns.GridLines = true;
            this.lvCustomerOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvCustomerOrderByColumns.Name = "lvCustomerOrderByColumns";
            this.lvCustomerOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvCustomerOrderByColumns.TabIndex = 12;
            this.lvCustomerOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvCustomerOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader37
            // 
            this.columnHeader37.Text = "Field Name";
            this.columnHeader37.Width = 345;
            // 
            // columnHeader38
            // 
            this.columnHeader38.Text = "Asc/Desc";
            this.columnHeader38.Width = 120;
            // 
            // tabPage20
            // 
            this.tabPage20.Controls.Add(this.textBox37);
            this.tabPage20.Controls.Add(this.textBox38);
            this.tabPage20.Controls.Add(this.button60);
            this.tabPage20.Controls.Add(this.button61);
            this.tabPage20.Controls.Add(this.textBox39);
            this.tabPage20.Controls.Add(this.textBox40);
            this.tabPage20.Controls.Add(this.textBox41);
            this.tabPage20.Controls.Add(this.button62);
            this.tabPage20.Controls.Add(this.button63);
            this.tabPage20.Controls.Add(this.button64);
            this.tabPage20.Controls.Add(this.listView9);
            this.tabPage20.Location = new System.Drawing.Point(4, 22);
            this.tabPage20.Name = "tabPage20";
            this.tabPage20.Size = new System.Drawing.Size(807, 592);
            this.tabPage20.TabIndex = 2;
            this.tabPage20.Text = "Limit Data";
            this.tabPage20.UseVisualStyleBackColor = true;
            // 
            // textBox37
            // 
            this.textBox37.BackColor = System.Drawing.SystemColors.Window;
            this.textBox37.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox37.Location = new System.Drawing.Point(12, 360);
            this.textBox37.Multiline = true;
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(473, 18);
            this.textBox37.TabIndex = 31;
            this.textBox37.Text = "\"Where clause\" as text";
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(12, 384);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.Size = new System.Drawing.Size(473, 123);
            this.textBox38.TabIndex = 30;
            // 
            // button60
            // 
            this.button60.Location = new System.Drawing.Point(492, 225);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(202, 23);
            this.button60.TabIndex = 29;
            this.button60.Text = "View as Text";
            this.button60.UseVisualStyleBackColor = true;
            // 
            // button61
            // 
            this.button61.Location = new System.Drawing.Point(492, 254);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(202, 23);
            this.button61.TabIndex = 28;
            this.button61.Text = "Validate";
            this.button61.UseVisualStyleBackColor = true;
            // 
            // textBox39
            // 
            this.textBox39.BackColor = System.Drawing.SystemColors.Window;
            this.textBox39.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox39.Location = new System.Drawing.Point(12, 66);
            this.textBox39.Multiline = true;
            this.textBox39.Name = "textBox39";
            this.textBox39.ReadOnly = true;
            this.textBox39.Size = new System.Drawing.Size(473, 18);
            this.textBox39.TabIndex = 27;
            this.textBox39.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox40
            // 
            this.textBox40.BackColor = System.Drawing.SystemColors.Window;
            this.textBox40.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox40.Location = new System.Drawing.Point(12, 12);
            this.textBox40.Multiline = true;
            this.textBox40.Name = "textBox40";
            this.textBox40.ReadOnly = true;
            this.textBox40.Size = new System.Drawing.Size(473, 33);
            this.textBox40.TabIndex = 26;
            this.textBox40.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox41
            // 
            this.textBox41.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox41.Location = new System.Drawing.Point(492, 99);
            this.textBox41.Multiline = true;
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(202, 62);
            this.textBox41.TabIndex = 25;
            this.textBox41.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(492, 167);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(202, 23);
            this.button62.TabIndex = 24;
            this.button62.Text = "Remove Field";
            this.button62.UseVisualStyleBackColor = true;
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(598, 196);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(96, 23);
            this.button63.TabIndex = 23;
            this.button63.Text = "Down";
            this.button63.UseVisualStyleBackColor = true;
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(492, 196);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(96, 23);
            this.button64.TabIndex = 22;
            this.button64.Text = "Up";
            this.button64.UseVisualStyleBackColor = true;
            // 
            // listView9
            // 
            this.listView9.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader39,
            this.columnHeader40});
            this.listView9.FullRowSelect = true;
            this.listView9.GridLines = true;
            this.listView9.Location = new System.Drawing.Point(12, 90);
            this.listView9.Name = "listView9";
            this.listView9.Size = new System.Drawing.Size(473, 227);
            this.listView9.TabIndex = 21;
            this.listView9.UseCompatibleStateImageBehavior = false;
            this.listView9.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader39
            // 
            this.columnHeader39.Text = "Field Name";
            this.columnHeader39.Width = 345;
            // 
            // columnHeader40
            // 
            this.columnHeader40.Text = "Asc/Desc";
            this.columnHeader40.Width = 120;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.button65);
            this.panel7.Controls.Add(this.lstCustomerColumns);
            this.panel7.Controls.Add(this.label27);
            this.panel7.Controls.Add(this.splitter2);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel7.Location = new System.Drawing.Point(0, 0);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(185, 618);
            this.panel7.TabIndex = 1;
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(8, 515);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(75, 23);
            this.button65.TabIndex = 3;
            this.button65.Text = "Add Field";
            this.button65.UseVisualStyleBackColor = true;
            // 
            // lstCustomerColumns
            // 
            this.lstCustomerColumns.FormattingEnabled = true;
            this.lstCustomerColumns.Location = new System.Drawing.Point(8, 24);
            this.lstCustomerColumns.Name = "lstCustomerColumns";
            this.lstCustomerColumns.Size = new System.Drawing.Size(165, 485);
            this.lstCustomerColumns.TabIndex = 2;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(5, 10);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(60, 13);
            this.label27.TabIndex = 1;
            this.label27.Text = "Data Fields";
            // 
            // splitter2
            // 
            this.splitter2.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter2.Location = new System.Drawing.Point(182, 0);
            this.splitter2.Name = "splitter2";
            this.splitter2.Size = new System.Drawing.Size(3, 618);
            this.splitter2.TabIndex = 0;
            this.splitter2.TabStop = false;
            // 
            // tpBank
            // 
            this.tpBank.Controls.Add(this.panel3);
            this.tpBank.Controls.Add(this.panel2);
            this.tpBank.Location = new System.Drawing.Point(4, 22);
            this.tpBank.Name = "tpBank";
            this.tpBank.Size = new System.Drawing.Size(1000, 618);
            this.tpBank.TabIndex = 2;
            this.tpBank.Text = "Bank";
            this.tpBank.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.tabControl2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(185, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(815, 618);
            this.panel3.TabIndex = 1;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage12);
            this.tabControl2.Controls.Add(this.tabPage13);
            this.tabControl2.Controls.Add(this.tabPage14);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl2.Location = new System.Drawing.Point(0, 0);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(815, 618);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.panel4);
            this.tabPage12.Controls.Add(this.textBox11);
            this.tabPage12.Controls.Add(this.button17);
            this.tabPage12.Controls.Add(this.button18);
            this.tabPage12.Controls.Add(this.button19);
            this.tabPage12.Controls.Add(this.button20);
            this.tabPage12.Controls.Add(this.groupBox9);
            this.tabPage12.Controls.Add(this.label20);
            this.tabPage12.Controls.Add(this.cboBankLayouts);
            this.tabPage12.Controls.Add(this.lvBankFields);
            this.tabPage12.Controls.Add(this.textBox10);
            this.tabPage12.Controls.Add(this.groupBox8);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(807, 592);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "Create Layouts";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel4.ForeColor = System.Drawing.SystemColors.Control;
            this.panel4.Location = new System.Drawing.Point(6, 103);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(692, 1);
            this.panel4.TabIndex = 20;
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.SystemColors.Window;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Location = new System.Drawing.Point(6, 13);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(763, 13);
            this.textBox11.TabIndex = 19;
            this.textBox11.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(568, 45);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(130, 23);
            this.button17.TabIndex = 18;
            this.button17.Text = "Delete Layout";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(568, 74);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(130, 23);
            this.button18.TabIndex = 17;
            this.button18.Text = "Restore Original Layout";
            this.button18.UseVisualStyleBackColor = true;
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(432, 45);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(130, 23);
            this.button19.TabIndex = 16;
            this.button19.Text = "New Layout";
            this.button19.UseVisualStyleBackColor = true;
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(432, 74);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(130, 23);
            this.button20.TabIndex = 15;
            this.button20.Text = "Test Layout";
            this.button20.UseVisualStyleBackColor = true;
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.rdoBankRecordTypeTrailer);
            this.groupBox9.Controls.Add(this.rdoBankRecordTypeHeader);
            this.groupBox9.Location = new System.Drawing.Point(248, 45);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(146, 52);
            this.groupBox9.TabIndex = 14;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Record Type";
            // 
            // rdoBankRecordTypeTrailer
            // 
            this.rdoBankRecordTypeTrailer.AutoSize = true;
            this.rdoBankRecordTypeTrailer.Location = new System.Drawing.Point(81, 19);
            this.rdoBankRecordTypeTrailer.Name = "rdoBankRecordTypeTrailer";
            this.rdoBankRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoBankRecordTypeTrailer.TabIndex = 1;
            this.rdoBankRecordTypeTrailer.TabStop = true;
            this.rdoBankRecordTypeTrailer.Text = "Trailer";
            this.rdoBankRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoBankRecordTypeHeader
            // 
            this.rdoBankRecordTypeHeader.AutoSize = true;
            this.rdoBankRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoBankRecordTypeHeader.Name = "rdoBankRecordTypeHeader";
            this.rdoBankRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoBankRecordTypeHeader.TabIndex = 0;
            this.rdoBankRecordTypeHeader.TabStop = true;
            this.rdoBankRecordTypeHeader.Text = "Header";
            this.rdoBankRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(3, 45);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(79, 13);
            this.label20.TabIndex = 13;
            this.label20.Text = "Current Layout:";
            // 
            // cboBankLayouts
            // 
            this.cboBankLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBankLayouts.FormattingEnabled = true;
            this.cboBankLayouts.Location = new System.Drawing.Point(6, 60);
            this.cboBankLayouts.Name = "cboBankLayouts";
            this.cboBankLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboBankLayouts.TabIndex = 12;
            // 
            // lvBankFields
            // 
            this.lvBankFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader8,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.lvBankFields.FullRowSelect = true;
            this.lvBankFields.GridLines = true;
            this.lvBankFields.Location = new System.Drawing.Point(8, 173);
            this.lvBankFields.Name = "lvBankFields";
            this.lvBankFields.Size = new System.Drawing.Size(652, 169);
            this.lvBankFields.TabIndex = 11;
            this.lvBankFields.UseCompatibleStateImageBehavior = false;
            this.lvBankFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Field Name";
            this.columnHeader8.Width = 120;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Display Name";
            this.columnHeader9.Width = 120;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Column Width";
            this.columnHeader10.Width = 85;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Pad Char";
            this.columnHeader11.Width = 59;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Justify";
            this.columnHeader12.Width = 45;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Quotes";
            this.columnHeader13.Width = 69;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Format";
            this.columnHeader14.Width = 150;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.SystemColors.Window;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Location = new System.Drawing.Point(6, 153);
            this.textBox10.Name = "textBox10";
            this.textBox10.ReadOnly = true;
            this.textBox10.Size = new System.Drawing.Size(677, 13);
            this.textBox10.TabIndex = 10;
            this.textBox10.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.textBox9);
            this.groupBox8.Controls.Add(this.button10);
            this.groupBox8.Controls.Add(this.button11);
            this.groupBox8.Controls.Add(this.button12);
            this.groupBox8.Controls.Add(this.button13);
            this.groupBox8.Controls.Add(this.button16);
            this.groupBox8.Controls.Add(this.button15);
            this.groupBox8.Controls.Add(this.label19);
            this.groupBox8.Controls.Add(this.label18);
            this.groupBox8.Controls.Add(this.listBox5);
            this.groupBox8.Controls.Add(this.listBox4);
            this.groupBox8.Location = new System.Drawing.Point(6, 348);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(784, 238);
            this.groupBox8.TabIndex = 0;
            this.groupBox8.TabStop = false;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.SystemColors.Window;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox9.Location = new System.Drawing.Point(419, 56);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(202, 62);
            this.textBox9.TabIndex = 13;
            this.textBox9.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(419, 124);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(202, 23);
            this.button10.TabIndex = 12;
            this.button10.Text = "Remove Field";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(525, 153);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(96, 23);
            this.button11.TabIndex = 11;
            this.button11.Text = "Down";
            this.button11.UseVisualStyleBackColor = true;
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(419, 153);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(96, 23);
            this.button12.TabIndex = 10;
            this.button12.Text = "Up";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(419, 182);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(202, 23);
            this.button13.TabIndex = 9;
            this.button13.Text = "Restore Default Field Data";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(211, 202);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(177, 23);
            this.button16.TabIndex = 6;
            this.button16.Text = "Add Field";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(16, 202);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(177, 23);
            this.button15.TabIndex = 5;
            this.button15.Text = "Add Field";
            this.button15.UseVisualStyleBackColor = true;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(208, 23);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(80, 13);
            this.label19.TabIndex = 4;
            this.label19.Text = "Standard Fields";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(13, 23);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(86, 13);
            this.label18.TabIndex = 3;
            this.label18.Text = "Aggregate Fields";
            // 
            // listBox5
            // 
            this.listBox5.FormattingEnabled = true;
            this.listBox5.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterFile",
            "ProcessingRunDate",
            "Static",
            "Time"});
            this.listBox5.Location = new System.Drawing.Point(211, 39);
            this.listBox5.Name = "listBox5";
            this.listBox5.Size = new System.Drawing.Size(177, 160);
            this.listBox5.TabIndex = 1;
            // 
            // listBox4
            // 
            this.listBox4.FormattingEnabled = true;
            this.listBox4.Items.AddRange(new object[] {
            "CountBatches",
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox4.Location = new System.Drawing.Point(16, 39);
            this.listBox4.Name = "listBox4";
            this.listBox4.Size = new System.Drawing.Size(177, 160);
            this.listBox4.TabIndex = 0;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.textBox14);
            this.tabPage13.Controls.Add(this.textBox13);
            this.tabPage13.Controls.Add(this.textBox12);
            this.tabPage13.Controls.Add(this.button21);
            this.tabPage13.Controls.Add(this.button22);
            this.tabPage13.Controls.Add(this.button23);
            this.tabPage13.Controls.Add(this.lvBankOrderByColumns);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(807, 592);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "Order Data";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // textBox14
            // 
            this.textBox14.BackColor = System.Drawing.SystemColors.Window;
            this.textBox14.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox14.Location = new System.Drawing.Point(12, 54);
            this.textBox14.Multiline = true;
            this.textBox14.Name = "textBox14";
            this.textBox14.ReadOnly = true;
            this.textBox14.Size = new System.Drawing.Size(473, 33);
            this.textBox14.TabIndex = 20;
            this.textBox14.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.SystemColors.Window;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox13.Location = new System.Drawing.Point(12, 12);
            this.textBox13.Multiline = true;
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(473, 33);
            this.textBox13.TabIndex = 19;
            this.textBox13.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.SystemColors.Window;
            this.textBox12.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox12.Location = new System.Drawing.Point(492, 99);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.ReadOnly = true;
            this.textBox12.Size = new System.Drawing.Size(202, 62);
            this.textBox12.TabIndex = 18;
            this.textBox12.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(492, 167);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(202, 23);
            this.button21.TabIndex = 17;
            this.button21.Text = "Remove Field";
            this.button21.UseVisualStyleBackColor = true;
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(598, 196);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(96, 23);
            this.button22.TabIndex = 16;
            this.button22.Text = "Down";
            this.button22.UseVisualStyleBackColor = true;
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(492, 196);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(96, 23);
            this.button23.TabIndex = 15;
            this.button23.Text = "Up";
            this.button23.UseVisualStyleBackColor = true;
            // 
            // lvBankOrderByColumns
            // 
            this.lvBankOrderByColumns.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader15,
            this.columnHeader16});
            this.lvBankOrderByColumns.FullRowSelect = true;
            this.lvBankOrderByColumns.GridLines = true;
            this.lvBankOrderByColumns.Location = new System.Drawing.Point(12, 90);
            this.lvBankOrderByColumns.Name = "lvBankOrderByColumns";
            this.lvBankOrderByColumns.Size = new System.Drawing.Size(473, 227);
            this.lvBankOrderByColumns.TabIndex = 12;
            this.lvBankOrderByColumns.UseCompatibleStateImageBehavior = false;
            this.lvBankOrderByColumns.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Field Name";
            this.columnHeader15.Width = 345;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Asc/Desc";
            this.columnHeader16.Width = 120;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.textBox19);
            this.tabPage14.Controls.Add(this.textBox18);
            this.tabPage14.Controls.Add(this.button28);
            this.tabPage14.Controls.Add(this.button27);
            this.tabPage14.Controls.Add(this.textBox15);
            this.tabPage14.Controls.Add(this.textBox16);
            this.tabPage14.Controls.Add(this.textBox17);
            this.tabPage14.Controls.Add(this.button24);
            this.tabPage14.Controls.Add(this.button25);
            this.tabPage14.Controls.Add(this.button26);
            this.tabPage14.Controls.Add(this.listView3);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(807, 592);
            this.tabPage14.TabIndex = 2;
            this.tabPage14.Text = "Limit Data";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.SystemColors.Window;
            this.textBox19.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox19.Location = new System.Drawing.Point(12, 360);
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(473, 18);
            this.textBox19.TabIndex = 31;
            this.textBox19.Text = "\"Where clause\" as text";
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(12, 384);
            this.textBox18.Multiline = true;
            this.textBox18.Name = "textBox18";
            this.textBox18.ReadOnly = true;
            this.textBox18.Size = new System.Drawing.Size(473, 123);
            this.textBox18.TabIndex = 30;
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(492, 225);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(202, 23);
            this.button28.TabIndex = 29;
            this.button28.Text = "View as Text";
            this.button28.UseVisualStyleBackColor = true;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(492, 254);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(202, 23);
            this.button27.TabIndex = 28;
            this.button27.Text = "Validate";
            this.button27.UseVisualStyleBackColor = true;
            // 
            // textBox15
            // 
            this.textBox15.BackColor = System.Drawing.SystemColors.Window;
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Location = new System.Drawing.Point(12, 66);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.ReadOnly = true;
            this.textBox15.Size = new System.Drawing.Size(473, 18);
            this.textBox15.TabIndex = 27;
            this.textBox15.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox16
            // 
            this.textBox16.BackColor = System.Drawing.SystemColors.Window;
            this.textBox16.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox16.Location = new System.Drawing.Point(12, 12);
            this.textBox16.Multiline = true;
            this.textBox16.Name = "textBox16";
            this.textBox16.ReadOnly = true;
            this.textBox16.Size = new System.Drawing.Size(473, 33);
            this.textBox16.TabIndex = 26;
            this.textBox16.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox17
            // 
            this.textBox17.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox17.Location = new System.Drawing.Point(492, 99);
            this.textBox17.Multiline = true;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(202, 62);
            this.textBox17.TabIndex = 25;
            this.textBox17.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(492, 167);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(202, 23);
            this.button24.TabIndex = 24;
            this.button24.Text = "Remove Field";
            this.button24.UseVisualStyleBackColor = true;
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(598, 196);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(96, 23);
            this.button25.TabIndex = 23;
            this.button25.Text = "Down";
            this.button25.UseVisualStyleBackColor = true;
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(492, 196);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(96, 23);
            this.button26.TabIndex = 22;
            this.button26.Text = "Up";
            this.button26.UseVisualStyleBackColor = true;
            // 
            // listView3
            // 
            this.listView3.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader17,
            this.columnHeader18});
            this.listView3.FullRowSelect = true;
            this.listView3.GridLines = true;
            this.listView3.Location = new System.Drawing.Point(12, 90);
            this.listView3.Name = "listView3";
            this.listView3.Size = new System.Drawing.Size(473, 227);
            this.listView3.TabIndex = 21;
            this.listView3.UseCompatibleStateImageBehavior = false;
            this.listView3.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Field Name";
            this.columnHeader17.Width = 345;
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "Asc/Desc";
            this.columnHeader18.Width = 120;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.button14);
            this.panel2.Controls.Add(this.lstBankColumns);
            this.panel2.Controls.Add(this.label17);
            this.panel2.Controls.Add(this.splitter1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(185, 618);
            this.panel2.TabIndex = 0;
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(8, 573);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 3;
            this.button14.Text = "Add Field";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // lstBankColumns
            // 
            this.lstBankColumns.FormattingEnabled = true;
            this.lstBankColumns.Location = new System.Drawing.Point(8, 24);
            this.lstBankColumns.Name = "lstBankColumns";
            this.lstBankColumns.Size = new System.Drawing.Size(165, 537);
            this.lstBankColumns.TabIndex = 2;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(5, 10);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(60, 13);
            this.label17.TabIndex = 1;
            this.label17.Text = "Data Fields";
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Right;
            this.splitter1.Location = new System.Drawing.Point(182, 0);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(3, 618);
            this.splitter1.TabIndex = 0;
            this.splitter1.TabStop = false;
            // 
            // tpFile
            // 
            this.tpFile.Controls.Add(this.panel1);
            this.tpFile.Controls.Add(this.groupBox7);
            this.tpFile.Controls.Add(this.lvFileFields);
            this.tpFile.Controls.Add(this.textBox7);
            this.tpFile.Controls.Add(this.button7);
            this.tpFile.Controls.Add(this.button6);
            this.tpFile.Controls.Add(this.button5);
            this.tpFile.Controls.Add(this.button4);
            this.tpFile.Controls.Add(this.groupBox6);
            this.tpFile.Controls.Add(this.label13);
            this.tpFile.Controls.Add(this.cboFileLayouts);
            this.tpFile.Controls.Add(this.textBox6);
            this.tpFile.Location = new System.Drawing.Point(4, 22);
            this.tpFile.Name = "tpFile";
            this.tpFile.Padding = new System.Windows.Forms.Padding(3);
            this.tpFile.Size = new System.Drawing.Size(1000, 618);
            this.tpFile.TabIndex = 1;
            this.tpFile.Text = "File";
            this.tpFile.UseVisualStyleBackColor = true;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.ForeColor = System.Drawing.SystemColors.Control;
            this.panel1.Location = new System.Drawing.Point(10, 101);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(876, 1);
            this.panel1.TabIndex = 11;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label16);
            this.groupBox7.Controls.Add(this.label15);
            this.groupBox7.Controls.Add(this.textBox8);
            this.groupBox7.Controls.Add(this.btnRemoveField);
            this.groupBox7.Controls.Add(this.btnMoveFieldDown);
            this.groupBox7.Controls.Add(this.btnMoveFieldUp);
            this.groupBox7.Controls.Add(this.btnRestoreDefaultFieldData);
            this.groupBox7.Controls.Add(this.button9);
            this.groupBox7.Controls.Add(this.button8);
            this.groupBox7.Controls.Add(this.listBox2);
            this.groupBox7.Controls.Add(this.listBox1);
            this.groupBox7.Location = new System.Drawing.Point(26, 281);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(953, 257);
            this.groupBox7.TabIndex = 10;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "groupBox7";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(215, 26);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(80, 13);
            this.label16.TabIndex = 10;
            this.label16.Text = "Standard Fields";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 26);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(86, 13);
            this.label15.TabIndex = 9;
            this.label15.Text = "Aggregate Fields";
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(430, 60);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(202, 62);
            this.textBox8.TabIndex = 8;
            this.textBox8.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // btnRemoveField
            // 
            this.btnRemoveField.Location = new System.Drawing.Point(430, 128);
            this.btnRemoveField.Name = "btnRemoveField";
            this.btnRemoveField.Size = new System.Drawing.Size(202, 23);
            this.btnRemoveField.TabIndex = 7;
            this.btnRemoveField.Text = "Remove Field";
            this.btnRemoveField.UseVisualStyleBackColor = true;
            // 
            // btnMoveFieldDown
            // 
            this.btnMoveFieldDown.Location = new System.Drawing.Point(536, 157);
            this.btnMoveFieldDown.Name = "btnMoveFieldDown";
            this.btnMoveFieldDown.Size = new System.Drawing.Size(96, 23);
            this.btnMoveFieldDown.TabIndex = 6;
            this.btnMoveFieldDown.Text = "Down";
            this.btnMoveFieldDown.UseVisualStyleBackColor = true;
            // 
            // btnMoveFieldUp
            // 
            this.btnMoveFieldUp.Location = new System.Drawing.Point(430, 157);
            this.btnMoveFieldUp.Name = "btnMoveFieldUp";
            this.btnMoveFieldUp.Size = new System.Drawing.Size(96, 23);
            this.btnMoveFieldUp.TabIndex = 5;
            this.btnMoveFieldUp.Text = "Up";
            this.btnMoveFieldUp.UseVisualStyleBackColor = true;
            // 
            // btnRestoreDefaultFieldData
            // 
            this.btnRestoreDefaultFieldData.Location = new System.Drawing.Point(430, 186);
            this.btnRestoreDefaultFieldData.Name = "btnRestoreDefaultFieldData";
            this.btnRestoreDefaultFieldData.Size = new System.Drawing.Size(202, 23);
            this.btnRestoreDefaultFieldData.TabIndex = 4;
            this.btnRestoreDefaultFieldData.Text = "Restore Default Field Data";
            this.btnRestoreDefaultFieldData.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(16, 221);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(178, 23);
            this.button9.TabIndex = 3;
            this.button9.Text = "Add Field";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(218, 221);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(178, 23);
            this.button8.TabIndex = 2;
            this.button8.Text = "Add Field";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // listBox2
            // 
            this.listBox2.FormattingEnabled = true;
            this.listBox2.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Data",
            "Filler",
            "FirstLast",
            "LineCounterFile",
            "ProcessingRunData",
            "Static",
            "Time"});
            this.listBox2.Location = new System.Drawing.Point(218, 42);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(178, 173);
            this.listBox2.TabIndex = 1;
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Items.AddRange(new object[] {
            "CountBatches",
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "RecordCountBank",
            "RecordCountBatch",
            "RecordCountBatchDetail",
            "RecordCountCheck",
            "RecordCountCustomer",
            "RecordCountDetail",
            "RecordCountDoc",
            "RecordCountFile",
            "RecordCountLockbox",
            "RecordCountStub",
            "RecordCountTotal",
            "RecordCountTran",
            "SumChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDDataKeys",
            "SumStubsAmount",
            "SumStubsDEBillingKeys",
            "SumStubsDEDataKeys"});
            this.listBox1.Location = new System.Drawing.Point(16, 42);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(178, 173);
            this.listBox1.TabIndex = 0;
            // 
            // lvFileFields
            // 
            this.lvFileFields.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7});
            this.lvFileFields.FullRowSelect = true;
            this.lvFileFields.GridLines = true;
            this.lvFileFields.Location = new System.Drawing.Point(26, 178);
            this.lvFileFields.Name = "lvFileFields";
            this.lvFileFields.Size = new System.Drawing.Size(932, 97);
            this.lvFileFields.TabIndex = 9;
            this.lvFileFields.UseCompatibleStateImageBehavior = false;
            this.lvFileFields.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Field Name";
            this.columnHeader1.Width = 120;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Display Name";
            this.columnHeader2.Width = 120;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Column Width";
            this.columnHeader3.Width = 85;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Pad Char";
            this.columnHeader4.Width = 59;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Justify(L/R)";
            this.columnHeader5.Width = 69;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Quotes";
            this.columnHeader6.Width = 69;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Format";
            this.columnHeader7.Width = 320;
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.SystemColors.Window;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Location = new System.Drawing.Point(26, 9);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(763, 13);
            this.textBox7.TabIndex = 8;
            this.textBox7.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(638, 41);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(130, 23);
            this.button7.TabIndex = 7;
            this.button7.Text = "Delete Layout";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(638, 70);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(130, 23);
            this.button6.TabIndex = 6;
            this.button6.Text = "Restore Original Layout";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(502, 41);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(130, 23);
            this.button5.TabIndex = 5;
            this.button5.Text = "New Layout";
            this.button5.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(502, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(130, 23);
            this.button4.TabIndex = 4;
            this.button4.Text = "Test Layout";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.rdoFileRecordTypeTrailer);
            this.groupBox6.Controls.Add(this.rdoFileRecordTypeHeader);
            this.groupBox6.Location = new System.Drawing.Point(268, 41);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(200, 52);
            this.groupBox6.TabIndex = 3;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Record Type";
            // 
            // rdoFileRecordTypeTrailer
            // 
            this.rdoFileRecordTypeTrailer.AutoSize = true;
            this.rdoFileRecordTypeTrailer.Location = new System.Drawing.Point(106, 19);
            this.rdoFileRecordTypeTrailer.Name = "rdoFileRecordTypeTrailer";
            this.rdoFileRecordTypeTrailer.Size = new System.Drawing.Size(54, 17);
            this.rdoFileRecordTypeTrailer.TabIndex = 1;
            this.rdoFileRecordTypeTrailer.TabStop = true;
            this.rdoFileRecordTypeTrailer.Text = "Trailer";
            this.rdoFileRecordTypeTrailer.UseVisualStyleBackColor = true;
            // 
            // rdoFileRecordTypeHeader
            // 
            this.rdoFileRecordTypeHeader.AutoSize = true;
            this.rdoFileRecordTypeHeader.Location = new System.Drawing.Point(15, 19);
            this.rdoFileRecordTypeHeader.Name = "rdoFileRecordTypeHeader";
            this.rdoFileRecordTypeHeader.Size = new System.Drawing.Size(60, 17);
            this.rdoFileRecordTypeHeader.TabIndex = 0;
            this.rdoFileRecordTypeHeader.TabStop = true;
            this.rdoFileRecordTypeHeader.Text = "Header";
            this.rdoFileRecordTypeHeader.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(23, 41);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(79, 13);
            this.label13.TabIndex = 2;
            this.label13.Text = "Current Layout:";
            // 
            // cboFileLayouts
            // 
            this.cboFileLayouts.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFileLayouts.FormattingEnabled = true;
            this.cboFileLayouts.Location = new System.Drawing.Point(26, 56);
            this.cboFileLayouts.Name = "cboFileLayouts";
            this.cboFileLayouts.Size = new System.Drawing.Size(227, 21);
            this.cboFileLayouts.TabIndex = 1;
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.SystemColors.Window;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Location = new System.Drawing.Point(26, 158);
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.Size = new System.Drawing.Size(763, 13);
            this.textBox6.TabIndex = 0;
            this.textBox6.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.  Enter the text for static fields in the \'Format" +
                "\' column.";
            // 
            // tpGeneral
            // 
            this.tpGeneral.Controls.Add(this.grpExtractLayouts);
            this.tpGeneral.Controls.Add(this.grpExtractImageOptions);
            this.tpGeneral.Controls.Add(this.grpExtractRecordOptions);
            this.tpGeneral.Controls.Add(this.grpExtractFileOptions);
            this.tpGeneral.Location = new System.Drawing.Point(4, 22);
            this.tpGeneral.Name = "tpGeneral";
            this.tpGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tpGeneral.Size = new System.Drawing.Size(1000, 618);
            this.tpGeneral.TabIndex = 0;
            this.tpGeneral.Text = "General";
            this.tpGeneral.UseVisualStyleBackColor = true;
            // 
            // grpExtractLayouts
            // 
            this.grpExtractLayouts.Controls.Add(this.tvGeneralLayout);
            this.grpExtractLayouts.Controls.Add(this.btnClearAll);
            this.grpExtractLayouts.Controls.Add(this.btnDeleteCurrentRecord);
            this.grpExtractLayouts.Controls.Add(this.label14);
            this.grpExtractLayouts.Location = new System.Drawing.Point(537, 120);
            this.grpExtractLayouts.Name = "grpExtractLayouts";
            this.grpExtractLayouts.Size = new System.Drawing.Size(443, 406);
            this.grpExtractLayouts.TabIndex = 1;
            this.grpExtractLayouts.TabStop = false;
            // 
            // tvGeneralLayout
            // 
            this.tvGeneralLayout.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tvGeneralLayout.Location = new System.Drawing.Point(11, 65);
            this.tvGeneralLayout.Name = "tvGeneralLayout";
            treeNode1.Name = "nodeFileHeaders";
            treeNode1.Text = "File Headers";
            treeNode2.Name = "nodeBankHeaders";
            treeNode2.Text = "Bank Headers";
            treeNode3.Name = "nodeCustomerHeaders";
            treeNode3.Text = "Customer Headers";
            treeNode4.Name = "nodeLockboxHeaders";
            treeNode4.Text = "Lockbox Headers";
            treeNode5.Name = "nodeBatchHeaders";
            treeNode5.Text = "Batch Headers";
            treeNode6.Name = "nodeTransactionHeaders";
            treeNode6.Text = "Transaction Headers";
            treeNode7.Name = "nodeCheckRecords";
            treeNode7.Text = "Check Records";
            treeNode8.Name = "nodeInvoiceRecords";
            treeNode8.Text = "Invoice Records";
            treeNode9.Name = "nodeDocumentRecords";
            treeNode9.Text = "Document Records";
            treeNode10.Name = "nodeJointDetailRecords";
            treeNode10.Text = "Joint Detail Records";
            treeNode11.Name = "nodTransactionTrailers";
            treeNode11.Text = "Transaction Trailers";
            treeNode12.Name = "nodeBatchTrailers";
            treeNode12.Text = "Batch Trailers";
            treeNode13.Name = "nodeLockboxTrailers";
            treeNode13.Text = "Lockbox Trailers";
            treeNode14.Name = "nodeCustomerTrailers";
            treeNode14.Text = "Customer Trailers";
            treeNode15.Name = "nodeBankTrailers";
            treeNode15.Text = "Bank Trailers";
            treeNode16.Name = "nodeFileTrailers";
            treeNode16.Text = "File Trailers";
            treeNode17.Name = "nodeLayout";
            treeNode17.Text = "Layout";
            this.tvGeneralLayout.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode17});
            this.tvGeneralLayout.Size = new System.Drawing.Size(422, 335);
            this.tvGeneralLayout.TabIndex = 3;
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(276, 24);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(75, 23);
            this.btnClearAll.TabIndex = 2;
            this.btnClearAll.Text = "Clear All Records";
            this.btnClearAll.UseVisualStyleBackColor = true;
            // 
            // btnDeleteCurrentRecord
            // 
            this.btnDeleteCurrentRecord.Location = new System.Drawing.Point(132, 24);
            this.btnDeleteCurrentRecord.Name = "btnDeleteCurrentRecord";
            this.btnDeleteCurrentRecord.Size = new System.Drawing.Size(138, 23);
            this.btnDeleteCurrentRecord.TabIndex = 1;
            this.btnDeleteCurrentRecord.Text = "Delete Current Record";
            this.btnDeleteCurrentRecord.UseVisualStyleBackColor = true;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 29);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Current Extract Layout:";
            // 
            // grpExtractImageOptions
            // 
            this.grpExtractImageOptions.Controls.Add(this.textBox5);
            this.grpExtractImageOptions.Controls.Add(this.label12);
            this.grpExtractImageOptions.Controls.Add(this.label11);
            this.grpExtractImageOptions.Controls.Add(this.label10);
            this.grpExtractImageOptions.Controls.Add(this.label9);
            this.grpExtractImageOptions.Controls.Add(this.label8);
            this.grpExtractImageOptions.Controls.Add(this.label7);
            this.grpExtractImageOptions.Controls.Add(this.button1);
            this.grpExtractImageOptions.Controls.Add(this.chkCombineImagesAcrossProcessingDates);
            this.grpExtractImageOptions.Controls.Add(this.groupBox3);
            this.grpExtractImageOptions.Location = new System.Drawing.Point(21, 207);
            this.grpExtractImageOptions.Name = "grpExtractImageOptions";
            this.grpExtractImageOptions.Size = new System.Drawing.Size(499, 245);
            this.grpExtractImageOptions.TabIndex = 0;
            this.grpExtractImageOptions.TabStop = false;
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.SystemColors.Window;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Location = new System.Drawing.Point(232, 56);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.ReadOnly = true;
            this.textBox5.Size = new System.Drawing.Size(170, 72);
            this.textBox5.TabIndex = 10;
            this.textBox5.Text = "For extracts that span multiple processing dates, allow images from multiple proc" +
                "essing dates to be combined into individual batch/transaction image files";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(15, 151);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 13);
            this.label12.TabIndex = 8;
            this.label12.Text = "File Name: Single";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(16, 176);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(101, 13);
            this.label11.TabIndex = 7;
            this.label11.Text = "File Name: Per Tran";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(15, 202);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(107, 13);
            this.label10.TabIndex = 6;
            this.label10.Text = "File Name: Per Batch";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(128, 151);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 5;
            this.label9.Text = "NONE";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(128, 176);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 4;
            this.label8.Text = "NONE";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(128, 202);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(38, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "NONE";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(408, 88);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "File Names...";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // chkCombineImagesAcrossProcessingDates
            // 
            this.chkCombineImagesAcrossProcessingDates.AutoSize = true;
            this.chkCombineImagesAcrossProcessingDates.Location = new System.Drawing.Point(211, 90);
            this.chkCombineImagesAcrossProcessingDates.Name = "chkCombineImagesAcrossProcessingDates";
            this.chkCombineImagesAcrossProcessingDates.Size = new System.Drawing.Size(15, 14);
            this.chkCombineImagesAcrossProcessingDates.TabIndex = 1;
            this.chkCombineImagesAcrossProcessingDates.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.rdoImageFileFormatEmbeddedTIFF);
            this.groupBox3.Controls.Add(this.rdoImageFileFormatPDF);
            this.groupBox3.Controls.Add(this.rdoImageFileFormatSingleMultiTIFF);
            this.groupBox3.Controls.Add(this.rdoImageFileFormatNone);
            this.groupBox3.Location = new System.Drawing.Point(18, 26);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(187, 110);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Image File Format";
            // 
            // rdoImageFileFormatEmbeddedTIFF
            // 
            this.rdoImageFileFormatEmbeddedTIFF.AutoSize = true;
            this.rdoImageFileFormatEmbeddedTIFF.Location = new System.Drawing.Point(12, 85);
            this.rdoImageFileFormatEmbeddedTIFF.Name = "rdoImageFileFormatEmbeddedTIFF";
            this.rdoImageFileFormatEmbeddedTIFF.Size = new System.Drawing.Size(166, 17);
            this.rdoImageFileFormatEmbeddedTIFF.TabIndex = 3;
            this.rdoImageFileFormatEmbeddedTIFF.Text = "Embed TIFF Image in data file";
            this.rdoImageFileFormatEmbeddedTIFF.UseVisualStyleBackColor = true;
            // 
            // rdoImageFileFormatPDF
            // 
            this.rdoImageFileFormatPDF.AutoSize = true;
            this.rdoImageFileFormatPDF.Location = new System.Drawing.Point(11, 62);
            this.rdoImageFileFormatPDF.Name = "rdoImageFileFormatPDF";
            this.rdoImageFileFormatPDF.Size = new System.Drawing.Size(86, 17);
            this.rdoImageFileFormatPDF.TabIndex = 2;
            this.rdoImageFileFormatPDF.Text = "Acrobat PDF";
            this.rdoImageFileFormatPDF.UseVisualStyleBackColor = true;
            // 
            // rdoImageFileFormatSingleMultiTIFF
            // 
            this.rdoImageFileFormatSingleMultiTIFF.AutoSize = true;
            this.rdoImageFileFormatSingleMultiTIFF.Location = new System.Drawing.Point(11, 39);
            this.rdoImageFileFormatSingleMultiTIFF.Name = "rdoImageFileFormatSingleMultiTIFF";
            this.rdoImageFileFormatSingleMultiTIFF.Size = new System.Drawing.Size(134, 17);
            this.rdoImageFileFormatSingleMultiTIFF.TabIndex = 1;
            this.rdoImageFileFormatSingleMultiTIFF.Text = "Single/Multi Page TIFF";
            this.rdoImageFileFormatSingleMultiTIFF.UseVisualStyleBackColor = true;
            // 
            // rdoImageFileFormatNone
            // 
            this.rdoImageFileFormatNone.AutoSize = true;
            this.rdoImageFileFormatNone.Checked = true;
            this.rdoImageFileFormatNone.Location = new System.Drawing.Point(12, 16);
            this.rdoImageFileFormatNone.Name = "rdoImageFileFormatNone";
            this.rdoImageFileFormatNone.Size = new System.Drawing.Size(51, 17);
            this.rdoImageFileFormatNone.TabIndex = 0;
            this.rdoImageFileFormatNone.TabStop = true;
            this.rdoImageFileFormatNone.Text = "None";
            this.rdoImageFileFormatNone.UseVisualStyleBackColor = true;
            // 
            // grpExtractRecordOptions
            // 
            this.grpExtractRecordOptions.Controls.Add(this.cboRecordDelimiter);
            this.grpExtractRecordOptions.Controls.Add(this.cboFieldDelimiter);
            this.grpExtractRecordOptions.Controls.Add(this.label6);
            this.grpExtractRecordOptions.Controls.Add(this.label5);
            this.grpExtractRecordOptions.Controls.Add(this.chkShowNULLAsSpace);
            this.grpExtractRecordOptions.Controls.Add(this.chkEncloseFieldDataInQuotes);
            this.grpExtractRecordOptions.Controls.Add(this.chkOmitPadding);
            this.grpExtractRecordOptions.Location = new System.Drawing.Point(537, 9);
            this.grpExtractRecordOptions.Name = "grpExtractRecordOptions";
            this.grpExtractRecordOptions.Size = new System.Drawing.Size(443, 95);
            this.grpExtractRecordOptions.TabIndex = 0;
            this.grpExtractRecordOptions.TabStop = false;
            // 
            // cboRecordDelimiter
            // 
            this.cboRecordDelimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboRecordDelimiter.FormattingEnabled = true;
            this.cboRecordDelimiter.Items.AddRange(new object[] {
            "\\n",
            "\\n\\n",
            "\\t",
            "\\t\\t",
            ",",
            ";",
            ":",
            "|",
            "!",
            "@",
            "#",
            "%"});
            this.cboRecordDelimiter.Location = new System.Drawing.Point(107, 59);
            this.cboRecordDelimiter.Name = "cboRecordDelimiter";
            this.cboRecordDelimiter.Size = new System.Drawing.Size(56, 21);
            this.cboRecordDelimiter.TabIndex = 6;
            // 
            // cboFieldDelimiter
            // 
            this.cboFieldDelimiter.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboFieldDelimiter.FormattingEnabled = true;
            this.cboFieldDelimiter.Items.AddRange(new object[] {
            ",",
            ";",
            ":",
            "|",
            "!",
            "@",
            "#",
            "%"});
            this.cboFieldDelimiter.Location = new System.Drawing.Point(107, 22);
            this.cboFieldDelimiter.Name = "cboFieldDelimiter";
            this.cboFieldDelimiter.Size = new System.Drawing.Size(56, 21);
            this.cboFieldDelimiter.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(26, 25);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Field Delimiter:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 64);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(88, 13);
            this.label5.TabIndex = 3;
            this.label5.Text = "Record Delimiter:";
            // 
            // chkShowNULLAsSpace
            // 
            this.chkShowNULLAsSpace.AutoSize = true;
            this.chkShowNULLAsSpace.Location = new System.Drawing.Point(192, 67);
            this.chkShowNULLAsSpace.Name = "chkShowNULLAsSpace";
            this.chkShowNULLAsSpace.Size = new System.Drawing.Size(130, 17);
            this.chkShowNULLAsSpace.TabIndex = 2;
            this.chkShowNULLAsSpace.Text = "Show NULL as space";
            this.chkShowNULLAsSpace.UseVisualStyleBackColor = true;
            // 
            // chkEncloseFieldDataInQuotes
            // 
            this.chkEncloseFieldDataInQuotes.AutoSize = true;
            this.chkEncloseFieldDataInQuotes.Location = new System.Drawing.Point(192, 44);
            this.chkEncloseFieldDataInQuotes.Name = "chkEncloseFieldDataInQuotes";
            this.chkEncloseFieldDataInQuotes.Size = new System.Drawing.Size(163, 17);
            this.chkEncloseFieldDataInQuotes.TabIndex = 1;
            this.chkEncloseFieldDataInQuotes.Text = "Enclose Field Data in Quotes";
            this.chkEncloseFieldDataInQuotes.UseVisualStyleBackColor = true;
            // 
            // chkOmitPadding
            // 
            this.chkOmitPadding.AutoSize = true;
            this.chkOmitPadding.Location = new System.Drawing.Point(192, 19);
            this.chkOmitPadding.Name = "chkOmitPadding";
            this.chkOmitPadding.Size = new System.Drawing.Size(89, 17);
            this.chkOmitPadding.TabIndex = 0;
            this.chkOmitPadding.Text = "Omit Padding";
            this.chkOmitPadding.UseVisualStyleBackColor = true;
            // 
            // grpExtractFileOptions
            // 
            this.grpExtractFileOptions.Controls.Add(this.comboBox1);
            this.grpExtractFileOptions.Controls.Add(this.txtImageFilePath);
            this.grpExtractFileOptions.Controls.Add(this.button2);
            this.grpExtractFileOptions.Controls.Add(this.chkUsePostProcessingDLL);
            this.grpExtractFileOptions.Controls.Add(this.label54);
            this.grpExtractFileOptions.Controls.Add(this.txtLogFilePath);
            this.grpExtractFileOptions.Controls.Add(this.txtPostProcessingDLLFileName);
            this.grpExtractFileOptions.Controls.Add(this.txtExtractFilePath);
            this.grpExtractFileOptions.Controls.Add(this.label4);
            this.grpExtractFileOptions.Controls.Add(this.label3);
            this.grpExtractFileOptions.Controls.Add(this.label2);
            this.grpExtractFileOptions.Controls.Add(this.label1);
            this.grpExtractFileOptions.Location = new System.Drawing.Point(21, 9);
            this.grpExtractFileOptions.Name = "grpExtractFileOptions";
            this.grpExtractFileOptions.Size = new System.Drawing.Size(499, 192);
            this.grpExtractFileOptions.TabIndex = 0;
            this.grpExtractFileOptions.TabStop = false;
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "",
            "TypeOneCutoff",
            "IBMLFEI",
            "ImageExchangeExtract"});
            this.comboBox1.Location = new System.Drawing.Point(176, 158);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(217, 21);
            this.comboBox1.TabIndex = 4;
            // 
            // txtImageFilePath
            // 
            this.txtImageFilePath.Location = new System.Drawing.Point(127, 75);
            this.txtImageFilePath.Name = "txtImageFilePath";
            this.txtImageFilePath.Size = new System.Drawing.Size(266, 20);
            this.txtImageFilePath.TabIndex = 8;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(408, 158);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Add Field";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // chkUsePostProcessingDLL
            // 
            this.chkUsePostProcessingDLL.AutoSize = true;
            this.chkUsePostProcessingDLL.Location = new System.Drawing.Point(127, 113);
            this.chkUsePostProcessingDLL.Name = "chkUsePostProcessingDLL";
            this.chkUsePostProcessingDLL.Size = new System.Drawing.Size(147, 17);
            this.chkUsePostProcessingDLL.TabIndex = 7;
            this.chkUsePostProcessingDLL.Text = "Use Post Processing DLL";
            this.chkUsePostProcessingDLL.UseVisualStyleBackColor = true;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 161);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(164, 13);
            this.label54.TabIndex = 2;
            this.label54.Text = "At completion, write timestamp to:";
            // 
            // txtLogFilePath
            // 
            this.txtLogFilePath.Location = new System.Drawing.Point(127, 49);
            this.txtLogFilePath.Name = "txtLogFilePath";
            this.txtLogFilePath.Size = new System.Drawing.Size(266, 20);
            this.txtLogFilePath.TabIndex = 6;
            // 
            // txtPostProcessingDLLFileName
            // 
            this.txtPostProcessingDLLFileName.Location = new System.Drawing.Point(127, 130);
            this.txtPostProcessingDLLFileName.Name = "txtPostProcessingDLLFileName";
            this.txtPostProcessingDLLFileName.Size = new System.Drawing.Size(266, 20);
            this.txtPostProcessingDLLFileName.TabIndex = 5;
            // 
            // txtExtractFilePath
            // 
            this.txtExtractFilePath.Location = new System.Drawing.Point(127, 23);
            this.txtExtractFilePath.Name = "txtExtractFilePath";
            this.txtExtractFilePath.Size = new System.Drawing.Size(266, 20);
            this.txtExtractFilePath.TabIndex = 4;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(15, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(106, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Extract File Location:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(49, 52);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Log File Path:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Enabled = false;
            this.label2.Location = new System.Drawing.Point(17, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(104, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Post DLL File Name:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(38, 78);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Image File Path:";
            // 
            // tabMain
            // 
            this.tabMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.tabMain.Controls.Add(this.tpGeneral);
            this.tabMain.Controls.Add(this.tpFile);
            this.tabMain.Controls.Add(this.tpBank);
            this.tabMain.Controls.Add(this.tpCustomer);
            this.tabMain.Controls.Add(this.tpLockbox);
            this.tabMain.Controls.Add(this.tpBatch);
            this.tabMain.Controls.Add(this.tpTransaction);
            this.tabMain.Controls.Add(this.tpCheck);
            this.tabMain.Controls.Add(this.tpStub);
            this.tabMain.Controls.Add(this.tpDocument);
            this.tabMain.Controls.Add(this.tpJointDetail);
            this.tabMain.Location = new System.Drawing.Point(2, 27);
            this.tabMain.Name = "tabMain";
            this.tabMain.SelectedIndex = 0;
            this.tabMain.Size = new System.Drawing.Size(1008, 644);
            this.tabMain.TabIndex = 0;
            this.tabMain.SelectedIndexChanged += new System.EventHandler(this.tabMain_SelectedIndexChanged);
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.panel5);
            this.tabPage15.Controls.Add(this.textBox20);
            this.tabPage15.Controls.Add(this.button29);
            this.tabPage15.Controls.Add(this.button30);
            this.tabPage15.Controls.Add(this.button31);
            this.tabPage15.Controls.Add(this.button32);
            this.tabPage15.Controls.Add(this.groupBox10);
            this.tabPage15.Controls.Add(this.label21);
            this.tabPage15.Controls.Add(this.comboBox5);
            this.tabPage15.Controls.Add(this.listView4);
            this.tabPage15.Controls.Add(this.textBox21);
            this.tabPage15.Controls.Add(this.groupBox11);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage15.Size = new System.Drawing.Size(704, 521);
            this.tabPage15.TabIndex = 0;
            this.tabPage15.Text = "Create Layout";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel5.ForeColor = System.Drawing.SystemColors.Control;
            this.panel5.Location = new System.Drawing.Point(6, 103);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(692, 1);
            this.panel5.TabIndex = 20;
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.SystemColors.Window;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox20.Location = new System.Drawing.Point(6, 13);
            this.textBox20.Name = "textBox20";
            this.textBox20.ReadOnly = true;
            this.textBox20.Size = new System.Drawing.Size(763, 13);
            this.textBox20.TabIndex = 19;
            this.textBox20.Text = "Extract layouts define the data that will be printed in the extract.  You may cre" +
                "ate as many layouts as you wish per record type, or none at all.";
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(568, 45);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(130, 23);
            this.button29.TabIndex = 18;
            this.button29.Text = "Delete Layout";
            this.button29.UseVisualStyleBackColor = true;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(568, 74);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(130, 23);
            this.button30.TabIndex = 17;
            this.button30.Text = "Restore Original Layout";
            this.button30.UseVisualStyleBackColor = true;
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(432, 45);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(130, 23);
            this.button31.TabIndex = 16;
            this.button31.Text = "New Layout";
            this.button31.UseVisualStyleBackColor = true;
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(432, 74);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(130, 23);
            this.button32.TabIndex = 15;
            this.button32.Text = "Test Layout";
            this.button32.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.radioButton9);
            this.groupBox10.Controls.Add(this.radioButton10);
            this.groupBox10.Location = new System.Drawing.Point(248, 45);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(146, 52);
            this.groupBox10.TabIndex = 14;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Record Type";
            // 
            // radioButton9
            // 
            this.radioButton9.AutoSize = true;
            this.radioButton9.Location = new System.Drawing.Point(81, 19);
            this.radioButton9.Name = "radioButton9";
            this.radioButton9.Size = new System.Drawing.Size(54, 17);
            this.radioButton9.TabIndex = 1;
            this.radioButton9.TabStop = true;
            this.radioButton9.Text = "Trailer";
            this.radioButton9.UseVisualStyleBackColor = true;
            // 
            // radioButton10
            // 
            this.radioButton10.AutoSize = true;
            this.radioButton10.Location = new System.Drawing.Point(15, 19);
            this.radioButton10.Name = "radioButton10";
            this.radioButton10.Size = new System.Drawing.Size(60, 17);
            this.radioButton10.TabIndex = 0;
            this.radioButton10.TabStop = true;
            this.radioButton10.Text = "Header";
            this.radioButton10.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(3, 45);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(79, 13);
            this.label21.TabIndex = 13;
            this.label21.Text = "Current Layout:";
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(6, 60);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(227, 21);
            this.comboBox5.TabIndex = 12;
            // 
            // listView4
            // 
            this.listView4.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader22,
            this.columnHeader23,
            this.columnHeader24,
            this.columnHeader25});
            this.listView4.Location = new System.Drawing.Point(6, 173);
            this.listView4.Name = "listView4";
            this.listView4.Size = new System.Drawing.Size(692, 97);
            this.listView4.TabIndex = 11;
            this.listView4.UseCompatibleStateImageBehavior = false;
            this.listView4.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "Field Name";
            this.columnHeader19.Width = 120;
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "Display Name";
            this.columnHeader20.Width = 120;
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Column Width";
            this.columnHeader21.Width = 85;
            // 
            // columnHeader22
            // 
            this.columnHeader22.Text = "Pad Char";
            this.columnHeader22.Width = 59;
            // 
            // columnHeader23
            // 
            this.columnHeader23.Text = "Justify(L/R)";
            this.columnHeader23.Width = 69;
            // 
            // columnHeader24
            // 
            this.columnHeader24.Text = "Quotes";
            this.columnHeader24.Width = 69;
            // 
            // columnHeader25
            // 
            this.columnHeader25.Text = "Format";
            this.columnHeader25.Width = 150;
            // 
            // textBox21
            // 
            this.textBox21.BackColor = System.Drawing.SystemColors.Window;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox21.Location = new System.Drawing.Point(6, 153);
            this.textBox21.Name = "textBox21";
            this.textBox21.ReadOnly = true;
            this.textBox21.Size = new System.Drawing.Size(677, 13);
            this.textBox21.TabIndex = 10;
            this.textBox21.Text = "Click on the column header to repeat the current data for each field.  Use \'SP\' t" +
                "o indicate a space as pad char.";
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.textBox22);
            this.groupBox11.Controls.Add(this.button33);
            this.groupBox11.Controls.Add(this.button34);
            this.groupBox11.Controls.Add(this.button35);
            this.groupBox11.Controls.Add(this.button36);
            this.groupBox11.Controls.Add(this.button37);
            this.groupBox11.Controls.Add(this.button38);
            this.groupBox11.Controls.Add(this.label22);
            this.groupBox11.Controls.Add(this.label23);
            this.groupBox11.Controls.Add(this.listBox6);
            this.groupBox11.Controls.Add(this.listBox7);
            this.groupBox11.Location = new System.Drawing.Point(6, 276);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(692, 238);
            this.groupBox11.TabIndex = 0;
            this.groupBox11.TabStop = false;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.SystemColors.Window;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox22.Location = new System.Drawing.Point(419, 56);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.ReadOnly = true;
            this.textBox22.Size = new System.Drawing.Size(202, 62);
            this.textBox22.TabIndex = 13;
            this.textBox22.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(419, 124);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(202, 23);
            this.button33.TabIndex = 12;
            this.button33.Text = "Remove Field";
            this.button33.UseVisualStyleBackColor = true;
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(525, 153);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(96, 23);
            this.button34.TabIndex = 11;
            this.button34.Text = "Down";
            this.button34.UseVisualStyleBackColor = true;
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(419, 153);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(96, 23);
            this.button35.TabIndex = 10;
            this.button35.Text = "Up";
            this.button35.UseVisualStyleBackColor = true;
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(419, 182);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(202, 23);
            this.button36.TabIndex = 9;
            this.button36.Text = "Restore Default Field Data";
            this.button36.UseVisualStyleBackColor = true;
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(211, 202);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(177, 23);
            this.button37.TabIndex = 6;
            this.button37.Text = "Add Field";
            this.button37.UseVisualStyleBackColor = true;
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(16, 202);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(177, 23);
            this.button38.TabIndex = 5;
            this.button38.Text = "Add Field";
            this.button38.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(199, 23);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(80, 13);
            this.label22.TabIndex = 4;
            this.label22.Text = "Standard Fields";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(13, 23);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(86, 13);
            this.label23.TabIndex = 3;
            this.label23.Text = "Aggregate Fields";
            // 
            // listBox6
            // 
            this.listBox6.FormattingEnabled = true;
            this.listBox6.Items.AddRange(new object[] {
            "Counter",
            "CurrentProcessingDate",
            "Date",
            "Filler",
            "FirstLast",
            "LineCounterBank",
            "LineCounterFile",
            "ProcessingRunDate",
            "Static",
            "Time"});
            this.listBox6.Location = new System.Drawing.Point(211, 39);
            this.listBox6.Name = "listBox6";
            this.listBox6.Size = new System.Drawing.Size(177, 160);
            this.listBox6.TabIndex = 1;
            // 
            // listBox7
            // 
            this.listBox7.FormattingEnabled = true;
            this.listBox7.Items.AddRange(new object[] {
            "CountBatches",
            "CountChecks",
            "CountDocuments",
            "CountStubs",
            "CountTransactions",
            "SubChecksAmount",
            "SumChecksDEBillingKeys",
            "SumChecksDEDataKeys",
            "SubStubsAmount",
            "SubStubsDEBillingKeys",
            "SubStubsDEDataKeys"});
            this.listBox7.Location = new System.Drawing.Point(16, 39);
            this.listBox7.Name = "listBox7";
            this.listBox7.Size = new System.Drawing.Size(177, 160);
            this.listBox7.TabIndex = 0;
            // 
            // tabPage16
            // 
            this.tabPage16.Controls.Add(this.textBox23);
            this.tabPage16.Controls.Add(this.textBox24);
            this.tabPage16.Controls.Add(this.textBox25);
            this.tabPage16.Controls.Add(this.button39);
            this.tabPage16.Controls.Add(this.button40);
            this.tabPage16.Controls.Add(this.button41);
            this.tabPage16.Controls.Add(this.listView5);
            this.tabPage16.Location = new System.Drawing.Point(4, 22);
            this.tabPage16.Name = "tabPage16";
            this.tabPage16.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage16.Size = new System.Drawing.Size(704, 521);
            this.tabPage16.TabIndex = 1;
            this.tabPage16.Text = "Order Data";
            this.tabPage16.UseVisualStyleBackColor = true;
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.SystemColors.Window;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox23.Location = new System.Drawing.Point(12, 54);
            this.textBox23.Multiline = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.ReadOnly = true;
            this.textBox23.Size = new System.Drawing.Size(473, 33);
            this.textBox23.TabIndex = 20;
            this.textBox23.Text = "Click on the column header to repeat the current data for each field.  Double cli" +
                "ck the \'Asc/Desc\' column to toggle between ascending and descending.";
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox24.Location = new System.Drawing.Point(12, 12);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.ReadOnly = true;
            this.textBox24.Size = new System.Drawing.Size(473, 33);
            this.textBox24.TabIndex = 19;
            this.textBox24.Text = "Use the area below to create an optional \"order by\" clause at the level.  You may" +
                " define only one order per level (bank, customer, lockbox, etc.).";
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.SystemColors.Window;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox25.Location = new System.Drawing.Point(492, 99);
            this.textBox25.Multiline = true;
            this.textBox25.Name = "textBox25";
            this.textBox25.ReadOnly = true;
            this.textBox25.Size = new System.Drawing.Size(202, 62);
            this.textBox25.TabIndex = 18;
            this.textBox25.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(492, 167);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(202, 23);
            this.button39.TabIndex = 17;
            this.button39.Text = "Remove Field";
            this.button39.UseVisualStyleBackColor = true;
            // 
            // button40
            // 
            this.button40.Location = new System.Drawing.Point(598, 196);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(96, 23);
            this.button40.TabIndex = 16;
            this.button40.Text = "Down";
            this.button40.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(492, 196);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(96, 23);
            this.button41.TabIndex = 15;
            this.button41.Text = "Up";
            this.button41.UseVisualStyleBackColor = true;
            // 
            // listView5
            // 
            this.listView5.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader26,
            this.columnHeader27});
            this.listView5.Location = new System.Drawing.Point(12, 90);
            this.listView5.Name = "listView5";
            this.listView5.Size = new System.Drawing.Size(473, 227);
            this.listView5.TabIndex = 12;
            this.listView5.UseCompatibleStateImageBehavior = false;
            this.listView5.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader26
            // 
            this.columnHeader26.Text = "Field Name";
            this.columnHeader26.Width = 345;
            // 
            // columnHeader27
            // 
            this.columnHeader27.Text = "Asc/Desc";
            this.columnHeader27.Width = 120;
            // 
            // tabPage17
            // 
            this.tabPage17.Controls.Add(this.textBox26);
            this.tabPage17.Controls.Add(this.textBox27);
            this.tabPage17.Controls.Add(this.button42);
            this.tabPage17.Controls.Add(this.button43);
            this.tabPage17.Controls.Add(this.textBox28);
            this.tabPage17.Controls.Add(this.textBox29);
            this.tabPage17.Controls.Add(this.textBox30);
            this.tabPage17.Controls.Add(this.button44);
            this.tabPage17.Controls.Add(this.button45);
            this.tabPage17.Controls.Add(this.button46);
            this.tabPage17.Controls.Add(this.listView6);
            this.tabPage17.Location = new System.Drawing.Point(4, 22);
            this.tabPage17.Name = "tabPage17";
            this.tabPage17.Size = new System.Drawing.Size(704, 521);
            this.tabPage17.TabIndex = 2;
            this.tabPage17.Text = "Limit Data";
            this.tabPage17.UseVisualStyleBackColor = true;
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.SystemColors.Window;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox26.Location = new System.Drawing.Point(12, 360);
            this.textBox26.Multiline = true;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(473, 18);
            this.textBox26.TabIndex = 31;
            this.textBox26.Text = "\"Where clause\" as text";
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(12, 384);
            this.textBox27.Multiline = true;
            this.textBox27.Name = "textBox27";
            this.textBox27.ReadOnly = true;
            this.textBox27.Size = new System.Drawing.Size(473, 123);
            this.textBox27.TabIndex = 30;
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(492, 225);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(202, 23);
            this.button42.TabIndex = 29;
            this.button42.Text = "View as Text";
            this.button42.UseVisualStyleBackColor = true;
            // 
            // button43
            // 
            this.button43.Location = new System.Drawing.Point(492, 254);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(202, 23);
            this.button43.TabIndex = 28;
            this.button43.Text = "Validate";
            this.button43.UseVisualStyleBackColor = true;
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.SystemColors.Window;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox28.Location = new System.Drawing.Point(12, 66);
            this.textBox28.Multiline = true;
            this.textBox28.Name = "textBox28";
            this.textBox28.ReadOnly = true;
            this.textBox28.Size = new System.Drawing.Size(473, 18);
            this.textBox28.TabIndex = 27;
            this.textBox28.Text = "Click on the column header to repeat the current data for each field.";
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.SystemColors.Window;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox29.Location = new System.Drawing.Point(12, 12);
            this.textBox29.Multiline = true;
            this.textBox29.Name = "textBox29";
            this.textBox29.ReadOnly = true;
            this.textBox29.Size = new System.Drawing.Size(473, 33);
            this.textBox29.TabIndex = 26;
            this.textBox29.Text = "Use the area below to create an optional \"where\" clause to limit the selection of" +
                " records at this level.  You may define only one set of limits per level(bank, c" +
                "ustomer, lockbox. etc.).";
            // 
            // textBox30
            // 
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox30.Location = new System.Drawing.Point(492, 99);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(202, 62);
            this.textBox30.TabIndex = 25;
            this.textBox30.Text = "Drag and drop field names on the desired grid or use the buttons to select.  Drag" +
                " the row button on the grid to change order or use the up/down buttons.";
            // 
            // button44
            // 
            this.button44.Location = new System.Drawing.Point(492, 167);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(202, 23);
            this.button44.TabIndex = 24;
            this.button44.Text = "Remove Field";
            this.button44.UseVisualStyleBackColor = true;
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(598, 196);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(96, 23);
            this.button45.TabIndex = 23;
            this.button45.Text = "Down";
            this.button45.UseVisualStyleBackColor = true;
            // 
            // button46
            // 
            this.button46.Location = new System.Drawing.Point(492, 196);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(96, 23);
            this.button46.TabIndex = 22;
            this.button46.Text = "Up";
            this.button46.UseVisualStyleBackColor = true;
            // 
            // listView6
            // 
            this.listView6.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader28,
            this.columnHeader29});
            this.listView6.Location = new System.Drawing.Point(12, 90);
            this.listView6.Name = "listView6";
            this.listView6.Size = new System.Drawing.Size(473, 227);
            this.listView6.TabIndex = 21;
            this.listView6.UseCompatibleStateImageBehavior = false;
            this.listView6.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader28
            // 
            this.columnHeader28.Text = "Field Name";
            this.columnHeader28.Width = 345;
            // 
            // columnHeader29
            // 
            this.columnHeader29.Text = "Asc/Desc";
            this.columnHeader29.Width = 120;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 670);
            this.Controls.Add(this.tabMain);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "frmMain";
            this.Text = "Extract Wizard";
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tpJointDetail.ResumeLayout(false);
            this.tabControl10.ResumeLayout(false);
            this.tabPage39.ResumeLayout(false);
            this.tabPage39.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.panel21.ResumeLayout(false);
            this.panel21.PerformLayout();
            this.tpDocument.ResumeLayout(false);
            this.tabControl9.ResumeLayout(false);
            this.tabPage36.ResumeLayout(false);
            this.tabPage36.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            this.tabPage37.ResumeLayout(false);
            this.tabPage37.PerformLayout();
            this.tabPage38.ResumeLayout(false);
            this.tabPage38.PerformLayout();
            this.panel19.ResumeLayout(false);
            this.panel19.PerformLayout();
            this.tpStub.ResumeLayout(false);
            this.tabControl8.ResumeLayout(false);
            this.tabPage33.ResumeLayout(false);
            this.tabPage33.PerformLayout();
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            this.tabPage34.ResumeLayout(false);
            this.tabPage34.PerformLayout();
            this.tabPage35.ResumeLayout(false);
            this.tabPage35.PerformLayout();
            this.panel17.ResumeLayout(false);
            this.panel17.PerformLayout();
            this.tpCheck.ResumeLayout(false);
            this.tabControl7.ResumeLayout(false);
            this.tabPage30.ResumeLayout(false);
            this.tabPage30.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            this.tabPage31.ResumeLayout(false);
            this.tabPage31.PerformLayout();
            this.tabPage32.ResumeLayout(false);
            this.tabPage32.PerformLayout();
            this.panel15.ResumeLayout(false);
            this.panel15.PerformLayout();
            this.tpTransaction.ResumeLayout(false);
            this.tabControl6.ResumeLayout(false);
            this.tabPage27.ResumeLayout(false);
            this.tabPage27.PerformLayout();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            this.tabPage28.ResumeLayout(false);
            this.tabPage28.PerformLayout();
            this.tabPage29.ResumeLayout(false);
            this.tabPage29.PerformLayout();
            this.panel13.ResumeLayout(false);
            this.panel13.PerformLayout();
            this.tpBatch.ResumeLayout(false);
            this.tabControl5.ResumeLayout(false);
            this.tabPage24.ResumeLayout(false);
            this.tabPage24.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.tabPage25.ResumeLayout(false);
            this.tabPage25.PerformLayout();
            this.tabPage26.ResumeLayout(false);
            this.tabPage26.PerformLayout();
            this.panel11.ResumeLayout(false);
            this.panel11.PerformLayout();
            this.tpLockbox.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage21.ResumeLayout(false);
            this.tabPage21.PerformLayout();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            this.tabPage22.ResumeLayout(false);
            this.tabPage22.PerformLayout();
            this.tabPage23.ResumeLayout(false);
            this.tabPage23.PerformLayout();
            this.panel9.ResumeLayout(false);
            this.panel9.PerformLayout();
            this.tpCustomer.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage18.ResumeLayout(false);
            this.tabPage18.PerformLayout();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tabPage19.ResumeLayout(false);
            this.tabPage19.PerformLayout();
            this.tabPage20.ResumeLayout(false);
            this.tabPage20.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.tpBank.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage12.PerformLayout();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.tabPage13.ResumeLayout(false);
            this.tabPage13.PerformLayout();
            this.tabPage14.ResumeLayout(false);
            this.tabPage14.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tpFile.ResumeLayout(false);
            this.tpFile.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tpGeneral.ResumeLayout(false);
            this.grpExtractLayouts.ResumeLayout(false);
            this.grpExtractLayouts.PerformLayout();
            this.grpExtractImageOptions.ResumeLayout(false);
            this.grpExtractImageOptions.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.grpExtractRecordOptions.ResumeLayout(false);
            this.grpExtractRecordOptions.PerformLayout();
            this.grpExtractFileOptions.ResumeLayout(false);
            this.grpExtractFileOptions.PerformLayout();
            this.tabMain.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.tabPage15.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.tabPage16.ResumeLayout(false);
            this.tabPage16.PerformLayout();
            this.tabPage17.ResumeLayout(false);
            this.tabPage17.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem runToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.TabPage tpJointDetail;
        private System.Windows.Forms.TabPage tpDocument;
        private System.Windows.Forms.TabPage tpStub;
        private System.Windows.Forms.TabPage tpCheck;
        private System.Windows.Forms.TabPage tpTransaction;
        private System.Windows.Forms.TabPage tpBatch;
        private System.Windows.Forms.TabPage tpLockbox;
        private System.Windows.Forms.TabPage tpCustomer;
        private System.Windows.Forms.TabPage tpBank;
        private System.Windows.Forms.TabPage tpFile;
        private System.Windows.Forms.TabPage tpGeneral;
        private System.Windows.Forms.GroupBox grpExtractLayouts;
        private System.Windows.Forms.TreeView tvGeneralLayout;
        private System.Windows.Forms.Button btnClearAll;
        private System.Windows.Forms.Button btnDeleteCurrentRecord;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox grpExtractImageOptions;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox chkCombineImagesAcrossProcessingDates;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton rdoImageFileFormatEmbeddedTIFF;
        private System.Windows.Forms.RadioButton rdoImageFileFormatPDF;
        private System.Windows.Forms.RadioButton rdoImageFileFormatSingleMultiTIFF;
        private System.Windows.Forms.RadioButton rdoImageFileFormatNone;
        private System.Windows.Forms.GroupBox grpExtractRecordOptions;
        private System.Windows.Forms.ComboBox cboRecordDelimiter;
        private System.Windows.Forms.ComboBox cboFieldDelimiter;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkShowNULLAsSpace;
        private System.Windows.Forms.CheckBox chkEncloseFieldDataInQuotes;
        private System.Windows.Forms.CheckBox chkOmitPadding;
        private System.Windows.Forms.GroupBox grpExtractFileOptions;
        private System.Windows.Forms.TextBox txtImageFilePath;
        private System.Windows.Forms.CheckBox chkUsePostProcessingDLL;
        private System.Windows.Forms.TextBox txtLogFilePath;
        private System.Windows.Forms.TextBox txtPostProcessingDLLFileName;
        private System.Windows.Forms.TextBox txtExtractFilePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabControl tabMain;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button btnRemoveField;
        private System.Windows.Forms.Button btnMoveFieldDown;
        private System.Windows.Forms.Button btnMoveFieldUp;
        private System.Windows.Forms.Button btnRestoreDefaultFieldData;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.ListBox listBox2;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.ListView lvFileFields;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton rdoFileRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoFileRecordTypeHeader;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ComboBox cboFileLayouts;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.ListBox listBox5;
        private System.Windows.Forms.ListBox listBox4;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ListBox lstBankColumns;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.ListView lvBankFields;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.RadioButton rdoBankRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoBankRecordTypeHeader;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.ComboBox cboBankLayouts;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.ListView lvBankOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.ListView listView3;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.TabControl tabControl10;
        private System.Windows.Forms.TabPage tabPage39;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.TextBox textBox108;
        private System.Windows.Forms.Button button180;
        private System.Windows.Forms.Button button181;
        private System.Windows.Forms.Button button182;
        private System.Windows.Forms.Button button183;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.RadioButton rdoJointDetailRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoJointDetailRecordTypeHeader;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.ComboBox cboJointDetailLayouts;
        private System.Windows.Forms.ListView lvJointDetailFields;
        private System.Windows.Forms.ColumnHeader columnHeader107;
        private System.Windows.Forms.ColumnHeader columnHeader108;
        private System.Windows.Forms.ColumnHeader columnHeader109;
        private System.Windows.Forms.ColumnHeader columnHeader110;
        private System.Windows.Forms.ColumnHeader columnHeader111;
        private System.Windows.Forms.ColumnHeader columnHeader112;
        private System.Windows.Forms.ColumnHeader columnHeader113;
        private System.Windows.Forms.TextBox textBox109;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.TextBox textBox110;
        private System.Windows.Forms.Button button184;
        private System.Windows.Forms.Button button185;
        private System.Windows.Forms.Button button186;
        private System.Windows.Forms.Button button187;
        private System.Windows.Forms.Button button188;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.ListBox listBox29;
        private System.Windows.Forms.Panel panel21;
        private System.Windows.Forms.Button button198;
        private System.Windows.Forms.ListBox lstJointDetailColumns;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Splitter splitter9;
        private System.Windows.Forms.TabControl tabControl9;
        private System.Windows.Forms.TabPage tabPage36;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.TextBox textBox97;
        private System.Windows.Forms.Button button161;
        private System.Windows.Forms.Button button162;
        private System.Windows.Forms.Button button163;
        private System.Windows.Forms.Button button164;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.ComboBox cboDocumentLayouts;
        private System.Windows.Forms.ListView lvDocumentFields;
        private System.Windows.Forms.ColumnHeader columnHeader96;
        private System.Windows.Forms.ColumnHeader columnHeader97;
        private System.Windows.Forms.ColumnHeader columnHeader98;
        private System.Windows.Forms.ColumnHeader columnHeader99;
        private System.Windows.Forms.ColumnHeader columnHeader100;
        private System.Windows.Forms.ColumnHeader columnHeader101;
        private System.Windows.Forms.ColumnHeader columnHeader102;
        private System.Windows.Forms.TextBox textBox98;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.TextBox textBox99;
        private System.Windows.Forms.Button button165;
        private System.Windows.Forms.Button button166;
        private System.Windows.Forms.Button button167;
        private System.Windows.Forms.Button button168;
        private System.Windows.Forms.Button button169;
        private System.Windows.Forms.Button button170;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ListBox listBox26;
        private System.Windows.Forms.ListBox listBox27;
        private System.Windows.Forms.TabPage tabPage37;
        private System.Windows.Forms.TextBox textBox100;
        private System.Windows.Forms.TextBox textBox101;
        private System.Windows.Forms.TextBox textBox102;
        private System.Windows.Forms.Button button171;
        private System.Windows.Forms.Button button172;
        private System.Windows.Forms.Button button173;
        private System.Windows.Forms.ListView lvDocumentsOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader103;
        private System.Windows.Forms.ColumnHeader columnHeader104;
        private System.Windows.Forms.TabPage tabPage38;
        private System.Windows.Forms.TextBox textBox103;
        private System.Windows.Forms.TextBox textBox104;
        private System.Windows.Forms.Button button174;
        private System.Windows.Forms.Button button175;
        private System.Windows.Forms.TextBox textBox105;
        private System.Windows.Forms.TextBox textBox106;
        private System.Windows.Forms.TextBox textBox107;
        private System.Windows.Forms.Button button176;
        private System.Windows.Forms.Button button177;
        private System.Windows.Forms.Button button178;
        private System.Windows.Forms.ListView listView27;
        private System.Windows.Forms.ColumnHeader columnHeader105;
        private System.Windows.Forms.ColumnHeader columnHeader106;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Button button179;
        private System.Windows.Forms.ListBox lstDocumentColumns;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Splitter splitter8;
        private System.Windows.Forms.TabControl tabControl8;
        private System.Windows.Forms.TabPage tabPage33;
        private System.Windows.Forms.Panel panel16;
        private System.Windows.Forms.TextBox textBox86;
        private System.Windows.Forms.Button button142;
        private System.Windows.Forms.Button button143;
        private System.Windows.Forms.Button button144;
        private System.Windows.Forms.Button button145;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.ComboBox cboStubLayouts;
        private System.Windows.Forms.ListView lvStubFields;
        private System.Windows.Forms.ColumnHeader columnHeader85;
        private System.Windows.Forms.ColumnHeader columnHeader86;
        private System.Windows.Forms.ColumnHeader columnHeader87;
        private System.Windows.Forms.ColumnHeader columnHeader88;
        private System.Windows.Forms.ColumnHeader columnHeader89;
        private System.Windows.Forms.ColumnHeader columnHeader90;
        private System.Windows.Forms.ColumnHeader columnHeader91;
        private System.Windows.Forms.TextBox textBox87;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.TextBox textBox88;
        private System.Windows.Forms.Button button146;
        private System.Windows.Forms.Button button147;
        private System.Windows.Forms.Button button148;
        private System.Windows.Forms.Button button149;
        private System.Windows.Forms.Button button150;
        private System.Windows.Forms.Button button151;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ListBox listBox23;
        private System.Windows.Forms.ListBox listBox24;
        private System.Windows.Forms.TabPage tabPage34;
        private System.Windows.Forms.TextBox textBox89;
        private System.Windows.Forms.TextBox textBox90;
        private System.Windows.Forms.TextBox textBox91;
        private System.Windows.Forms.Button button152;
        private System.Windows.Forms.Button button153;
        private System.Windows.Forms.Button button154;
        private System.Windows.Forms.ListView lvStubsOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader92;
        private System.Windows.Forms.ColumnHeader columnHeader93;
        private System.Windows.Forms.TabPage tabPage35;
        private System.Windows.Forms.TextBox textBox92;
        private System.Windows.Forms.TextBox textBox93;
        private System.Windows.Forms.Button button155;
        private System.Windows.Forms.Button button156;
        private System.Windows.Forms.TextBox textBox94;
        private System.Windows.Forms.TextBox textBox95;
        private System.Windows.Forms.TextBox textBox96;
        private System.Windows.Forms.Button button157;
        private System.Windows.Forms.Button button158;
        private System.Windows.Forms.Button button159;
        private System.Windows.Forms.ListView listView24;
        private System.Windows.Forms.ColumnHeader columnHeader94;
        private System.Windows.Forms.ColumnHeader columnHeader95;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Button button160;
        private System.Windows.Forms.ListBox lstStubColumns;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Splitter splitter7;
        private System.Windows.Forms.TabControl tabControl7;
        private System.Windows.Forms.TabPage tabPage30;
        private System.Windows.Forms.Panel panel14;
        private System.Windows.Forms.TextBox textBox75;
        private System.Windows.Forms.Button button123;
        private System.Windows.Forms.Button button124;
        private System.Windows.Forms.Button button125;
        private System.Windows.Forms.Button button126;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.ComboBox cboCheckLayouts;
        private System.Windows.Forms.ListView lvCheckFields;
        private System.Windows.Forms.ColumnHeader columnHeader74;
        private System.Windows.Forms.ColumnHeader columnHeader75;
        private System.Windows.Forms.ColumnHeader columnHeader76;
        private System.Windows.Forms.ColumnHeader columnHeader77;
        private System.Windows.Forms.ColumnHeader columnHeader78;
        private System.Windows.Forms.ColumnHeader columnHeader79;
        private System.Windows.Forms.ColumnHeader columnHeader80;
        private System.Windows.Forms.TextBox textBox76;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.TextBox textBox77;
        private System.Windows.Forms.Button button127;
        private System.Windows.Forms.Button button128;
        private System.Windows.Forms.Button button129;
        private System.Windows.Forms.Button button130;
        private System.Windows.Forms.Button button131;
        private System.Windows.Forms.Button button132;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.ListBox listBox20;
        private System.Windows.Forms.ListBox listBox21;
        private System.Windows.Forms.TabPage tabPage31;
        private System.Windows.Forms.TextBox textBox78;
        private System.Windows.Forms.TextBox textBox79;
        private System.Windows.Forms.TextBox textBox80;
        private System.Windows.Forms.Button button133;
        private System.Windows.Forms.Button button134;
        private System.Windows.Forms.Button button135;
        private System.Windows.Forms.ListView lvChecksOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader81;
        private System.Windows.Forms.ColumnHeader columnHeader82;
        private System.Windows.Forms.TabPage tabPage32;
        private System.Windows.Forms.TextBox textBox81;
        private System.Windows.Forms.TextBox textBox82;
        private System.Windows.Forms.Button button136;
        private System.Windows.Forms.Button button137;
        private System.Windows.Forms.TextBox textBox83;
        private System.Windows.Forms.TextBox textBox84;
        private System.Windows.Forms.TextBox textBox85;
        private System.Windows.Forms.Button button138;
        private System.Windows.Forms.Button button139;
        private System.Windows.Forms.Button button140;
        private System.Windows.Forms.ListView listView21;
        private System.Windows.Forms.ColumnHeader columnHeader83;
        private System.Windows.Forms.ColumnHeader columnHeader84;
        private System.Windows.Forms.Panel panel15;
        private System.Windows.Forms.Button button141;
        private System.Windows.Forms.ListBox lstCheckColumns;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Splitter splitter6;
        private System.Windows.Forms.TabControl tabControl6;
        private System.Windows.Forms.TabPage tabPage27;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.TextBox textBox64;
        private System.Windows.Forms.Button button104;
        private System.Windows.Forms.Button button105;
        private System.Windows.Forms.Button button106;
        private System.Windows.Forms.Button button107;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.RadioButton rdoTransactionRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoTransactionRecordTypeHeader;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.ComboBox cboTransactionLayouts;
        private System.Windows.Forms.ListView lvTransactionFields;
        private System.Windows.Forms.ColumnHeader columnHeader63;
        private System.Windows.Forms.ColumnHeader columnHeader64;
        private System.Windows.Forms.ColumnHeader columnHeader65;
        private System.Windows.Forms.ColumnHeader columnHeader66;
        private System.Windows.Forms.ColumnHeader columnHeader67;
        private System.Windows.Forms.ColumnHeader columnHeader68;
        private System.Windows.Forms.ColumnHeader columnHeader69;
        private System.Windows.Forms.TextBox textBox65;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.TextBox textBox66;
        private System.Windows.Forms.Button button108;
        private System.Windows.Forms.Button button109;
        private System.Windows.Forms.Button button110;
        private System.Windows.Forms.Button button111;
        private System.Windows.Forms.Button button112;
        private System.Windows.Forms.Button button113;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ListBox listBox17;
        private System.Windows.Forms.ListBox listBox18;
        private System.Windows.Forms.TabPage tabPage28;
        private System.Windows.Forms.TextBox textBox67;
        private System.Windows.Forms.TextBox textBox68;
        private System.Windows.Forms.TextBox textBox69;
        private System.Windows.Forms.Button button114;
        private System.Windows.Forms.Button button115;
        private System.Windows.Forms.Button button116;
        private System.Windows.Forms.ListView lvTransactionsOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader70;
        private System.Windows.Forms.ColumnHeader columnHeader71;
        private System.Windows.Forms.TabPage tabPage29;
        private System.Windows.Forms.TextBox textBox70;
        private System.Windows.Forms.TextBox textBox71;
        private System.Windows.Forms.Button button117;
        private System.Windows.Forms.Button button118;
        private System.Windows.Forms.TextBox textBox72;
        private System.Windows.Forms.TextBox textBox73;
        private System.Windows.Forms.TextBox textBox74;
        private System.Windows.Forms.Button button119;
        private System.Windows.Forms.Button button120;
        private System.Windows.Forms.Button button121;
        private System.Windows.Forms.ListView listView18;
        private System.Windows.Forms.ColumnHeader columnHeader72;
        private System.Windows.Forms.ColumnHeader columnHeader73;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Button button122;
        private System.Windows.Forms.ListBox lstTransactionColumns;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Splitter splitter5;
        private System.Windows.Forms.TabControl tabControl5;
        private System.Windows.Forms.TabPage tabPage24;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.TextBox textBox53;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.RadioButton rdoBatchRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoBatchRecordTypeHeader;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox cboBatchLayouts;
        private System.Windows.Forms.ListView lvBatchFields;
        private System.Windows.Forms.ColumnHeader columnHeader52;
        private System.Windows.Forms.ColumnHeader columnHeader53;
        private System.Windows.Forms.ColumnHeader columnHeader54;
        private System.Windows.Forms.ColumnHeader columnHeader55;
        private System.Windows.Forms.ColumnHeader columnHeader56;
        private System.Windows.Forms.ColumnHeader columnHeader57;
        private System.Windows.Forms.ColumnHeader columnHeader58;
        private System.Windows.Forms.TextBox textBox54;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.TextBox textBox55;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ListBox listBox14;
        private System.Windows.Forms.ListBox listBox15;
        private System.Windows.Forms.TabPage tabPage25;
        private System.Windows.Forms.TextBox textBox56;
        private System.Windows.Forms.TextBox textBox57;
        private System.Windows.Forms.TextBox textBox58;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.ListView lvBatchOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader59;
        private System.Windows.Forms.ColumnHeader columnHeader60;
        private System.Windows.Forms.TabPage tabPage26;
        private System.Windows.Forms.TextBox textBox59;
        private System.Windows.Forms.TextBox textBox60;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.TextBox textBox61;
        private System.Windows.Forms.TextBox textBox62;
        private System.Windows.Forms.TextBox textBox63;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Button button101;
        private System.Windows.Forms.Button button102;
        private System.Windows.Forms.ListView listView15;
        private System.Windows.Forms.ColumnHeader columnHeader61;
        private System.Windows.Forms.ColumnHeader columnHeader62;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Button button103;
        private System.Windows.Forms.ListBox lstBatchColumns;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Splitter splitter4;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage21;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.RadioButton rdoLockboxRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoLockboxRecordTypeHeader;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ComboBox cboLockboxLayouts;
        private System.Windows.Forms.ListView lvLockboxFields;
        private System.Windows.Forms.ColumnHeader columnHeader41;
        private System.Windows.Forms.ColumnHeader columnHeader42;
        private System.Windows.Forms.ColumnHeader columnHeader43;
        private System.Windows.Forms.ColumnHeader columnHeader44;
        private System.Windows.Forms.ColumnHeader columnHeader45;
        private System.Windows.Forms.ColumnHeader columnHeader46;
        private System.Windows.Forms.ColumnHeader columnHeader47;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ListBox listBox11;
        private System.Windows.Forms.ListBox listBox12;
        private System.Windows.Forms.TabPage tabPage22;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.ListView lvLockboxOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader48;
        private System.Windows.Forms.ColumnHeader columnHeader49;
        private System.Windows.Forms.TabPage tabPage23;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.TextBox textBox51;
        private System.Windows.Forms.TextBox textBox52;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.ListView listView12;
        private System.Windows.Forms.ColumnHeader columnHeader50;
        private System.Windows.Forms.ColumnHeader columnHeader51;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.ListBox lstLockboxColumns;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Splitter splitter3;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage18;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.RadioButton rdoCustomerRecordTypeTrailer;
        private System.Windows.Forms.RadioButton rdoCustomerRecordTypeHeader;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox cboCustomerLayouts;
        private System.Windows.Forms.ListView lvCustomerFields;
        private System.Windows.Forms.ColumnHeader columnHeader30;
        private System.Windows.Forms.ColumnHeader columnHeader31;
        private System.Windows.Forms.ColumnHeader columnHeader32;
        private System.Windows.Forms.ColumnHeader columnHeader33;
        private System.Windows.Forms.ColumnHeader columnHeader34;
        private System.Windows.Forms.ColumnHeader columnHeader35;
        private System.Windows.Forms.ColumnHeader columnHeader36;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ListBox listBox8;
        private System.Windows.Forms.ListBox listBox9;
        private System.Windows.Forms.TabPage tabPage19;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.ListView lvCustomerOrderByColumns;
        private System.Windows.Forms.ColumnHeader columnHeader37;
        private System.Windows.Forms.ColumnHeader columnHeader38;
        private System.Windows.Forms.TabPage tabPage20;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Button button60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.ListView listView9;
        private System.Windows.Forms.ColumnHeader columnHeader39;
        private System.Windows.Forms.ColumnHeader columnHeader40;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.ListBox lstCustomerColumns;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Splitter splitter2;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.RadioButton radioButton9;
        private System.Windows.Forms.RadioButton radioButton10;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.ListView listView4;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.ColumnHeader columnHeader22;
        private System.Windows.Forms.ColumnHeader columnHeader23;
        private System.Windows.Forms.ColumnHeader columnHeader24;
        private System.Windows.Forms.ColumnHeader columnHeader25;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ListBox listBox6;
        private System.Windows.Forms.ListBox listBox7;
        private System.Windows.Forms.TabPage tabPage16;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.ListView listView5;
        private System.Windows.Forms.ColumnHeader columnHeader26;
        private System.Windows.Forms.ColumnHeader columnHeader27;
        private System.Windows.Forms.TabPage tabPage17;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.ListView listView6;
        private System.Windows.Forms.ColumnHeader columnHeader28;
        private System.Windows.Forms.ColumnHeader columnHeader29;
        private System.Windows.Forms.ColumnHeader columnHeader118;
        private System.Windows.Forms.ColumnHeader columnHeader119;
        private System.Windows.Forms.TextBox textBox119;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.TextBox textBox120;
        private System.Windows.Forms.ToolStripMenuItem newSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem printToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem printerSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem setupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem resultsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logFileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem licenseToolStripMenuItem;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox cboJointDetailDisplayLayouts;
        private System.Windows.Forms.ToolStripMenuItem closeSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;

    }
}

