using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using DMP.DmpSentry;

/*******************************************************************************
*
*    Module: Program
*  Filename: Program.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 04/09/2007
*   -Initial release version.
******************************************************************************/
namespace DMP.Delivery.Extract.ExtractWizard
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
			bool bolSuccess;
			string strOperatorID;

			bolSuccess = CollectLogin(out strOperatorID);

			if(bolSuccess) {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new frmMain());
			} else {
			    //          hitting 'Cancel' on the Sentry dialog should not result in
			    //          an error in here.  it should be no message, just cancelled.
			    //          But, have to get more return options back from Sentry 
			    //          Authorize call than a bool to get more info than success/fail.

			    StringBuilder   sbLoginFailMsg  = new StringBuilder();

			    sbLoginFailMsg.Append("You are not authorized to use Extract Wizard.\n\n");
			    sbLoginFailMsg.Append("Administer the Operator Permissions setup in System\n");
			    sbLoginFailMsg.Append("Maintenance to gain authorization.");
 
                //m_profilerManager.LogMessage(sbLoginFailMsg.ToString(),LogLevelEnum.Normal);      // CR 16848 9/14/06 MLT - only use one log
			    MessageBox.Show(sbLoginFailMsg.ToString(), "Sentry Error");  
            }
        }

		private static bool CollectLogin(out string OperatorID)
		{
			bool bolRetVal = false;
			string strOperatorID;
			
			bolRetVal = Sentry.Permit("CHECKBOX", "EXTRACTWIZARD", "EXECUTE", out strOperatorID);  // CR 15921 5/25/06 MLT - need to use Permit not Authorize, wasn't logging on the operator and wasn't returning the operatorid
			if(bolRetVal) {
				OperatorID = strOperatorID;
			} else {
			    OperatorID = string.Empty;
			}

			// clean up Sentry object
			Sentry.Dismiss();

			return bolRetVal;
		}
    }
}