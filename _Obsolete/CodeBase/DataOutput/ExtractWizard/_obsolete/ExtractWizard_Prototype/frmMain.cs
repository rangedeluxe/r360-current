using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

using DMP.Delivery.Extract.ExtractAPI;
using DMP.Delivery.Extract.ExtractRun;

using DMP.Delivery.Common;

/*******************************************************************************
*
*    Module: Extract Wizard
*  Filename: ExtractWizard.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 03/23/2007
*   -Initial release version.
******************************************************************************/
namespace DMP.Delivery.Extract.ExtractWizard {

    public partial class frmMain : Form {

        public frmMain() {
            InitializeComponent();

            SetFormState(true);
        }

        #region Object Properties
        private cExtractSettings _Settings = null;
        private cDatabase2 _Database = null;

        private cDatabase2 Database {
            get {
                if(_Database == null) {
                    _Database = new cDatabase2(Settings.ConnectionString);
                }
                return(_Database);
            }
        }

        private cExtractSettings Settings {
            get {
                if(_Settings == null) {
                    _Settings = new cExtractSettings();
                }
                return(_Settings);
            }
        }

        private string _InputFileName = string.Empty;
        
        private string InputFileName {
            get {
                return(_InputFileName);
            }
            set {
                _InputFileName = value;
            }
        }
        #endregion 


        #region Build Instructions object from form values
        private cExtractInstructions GetInstructionsFromForm() {
            
            cExtractInstructions instructions = new cExtractInstructions();
            //cLayout layout;
            //cGridItem griditem;
            //int intTemp;

            instructions.ExtractPath = this.txtExtractFilePath.Text;
            instructions.LogFilePath = this.txtLogFilePath.Text;
            instructions.ImagePath = this.txtImageFilePath.Text;
            //this.chkUsePostProcessingDLL.Checked = instructions.PostProcDLLFileName.Length > 0
            instructions.PostProcDLLFileName = this.txtPostProcessingDLLFileName.Text;
            //this.rdoImageFileFormatNone.Checked = instructions.
            //this.rdoImageFileFormatNone;
            //this.rdoImageFileFormatPDF;
            //this.rdoImageFileFormatSingleMultiTIFF;
            //this.rdoImageFileFormatEmbeddedTIFF;
            //this.chkCombineImagesAcrossProcessingDates;
            //this.cboFieldDelimiter;

            instructions.FieldDelim = this.cboFieldDelimiter.Text;
            instructions.RecordDelim = this.cboRecordDelimiter.Text;

            instructions.NoPad = this.chkOmitPadding.Checked;
            instructions.UseQuotes = this.chkEncloseFieldDataInQuotes.Checked;
            instructions.NullAsSpace = this.chkShowNULLAsSpace.Checked;
            
            //foreach(string strLayout in this.cboFileLayouts.Items) {

            //    layout = new cLayout();
            //    layout.LayoutName = strLayout;

            //    if(this.rdoFileRecordTypeTrailer.Checked) {
            //        layout.LayoutType = LayoutTypeEnum.Trailer;
            //    } else {
            //        layout.LayoutType = LayoutTypeEnum.Header;
            //    }

            //    foreach(ListView.ListViewItemCollection col in this.lvFileFields.Items) {
            //        griditem = new cGridItem();
            //        griditem.FieldName = col[0].Text;
            //        griditem.DisplayName = col[1].Text;

            //        if(int.TryParse(col[2].Text, out intTemp)) {
            //            griditem.ColumnWidth = intTemp;
            //        //} else {
            //        }

            //        if(col[3].Text.Length > 0) {
            //            griditem.PadChar = col[3].Text[0];
            //        //} else {
            //        //    griditem.PadChar = '0';
            //        }

            //        if(col[4].Text.Length > 0 && col[4].Text[0] == 'L') {
            //            griditem.Justification = JustifyEnum.Left;
            //        //} else {
            //        //    griditem.Justification = JustifyEnum.Right;
            //        }

            //        if(col[5].Text.Length > 0 && (col[5].Text.Substring(0, 1).ToLower() == "x" || col[5].Text.Substring(0, 1).ToLower() == "y")) {
            //            griditem.Quotes = true;
            //        } else {
            //            griditem.Quotes = false;
            //        }

            //        griditem.Format = col[6].Text;
            //    }
            //}

            GetLayoutsFromForm(this.cboFileLayouts, LayoutLevelEnum.File, this.lvFileFields.Items, instructions);
            GetLayoutsFromForm(this.cboBankLayouts, LayoutLevelEnum.Bank, this.lvBankFields.Items, instructions);
            GetLayoutsFromForm(this.cboCustomerLayouts, LayoutLevelEnum.Customer, this.lvCustomerFields.Items, instructions);
            GetLayoutsFromForm(this.cboLockboxLayouts, LayoutLevelEnum.Lockbox, this.lvLockboxFields.Items, instructions);
            GetLayoutsFromForm(this.cboBatchLayouts, LayoutLevelEnum.Batch, this.lvBatchFields.Items, instructions);
            GetLayoutsFromForm(this.cboTransactionLayouts, LayoutLevelEnum.Transaction, this.lvTransactionFields.Items, instructions);
            GetLayoutsFromForm(this.cboCheckLayouts, LayoutLevelEnum.Check, this.lvCheckFields.Items, instructions);
            GetLayoutsFromForm(this.cboStubLayouts, LayoutLevelEnum.Stub, this.lvStubFields.Items, instructions);
            GetLayoutsFromForm(this.cboDocumentLayouts, LayoutLevelEnum.Document, this.lvDocumentFields.Items, instructions);
            //GetLayoutsFromForm(this.cboJointDetailDisplayLayouts, LayoutLevelEnum.JointDetail, this.lvJointDetailFields.Items);

            return(instructions);
        }

        private void GetLayoutsFromForm(ComboBox LayoutsCombo, 
                                        LayoutLevelEnum LayoutLevel, 
                                        ListView.ListViewItemCollection Items, 
                                        cExtractInstructions Instructions) {

            cLayout layout;
            cGridItem griditem;
            int intTemp;

            foreach(cLayout objLayout in LayoutsCombo.Items) {

                layout = new cLayout(Instructions);
                layout.LayoutName = objLayout.LayoutName;

//TODO: 
                //layout.LayoutLevel = ????;

//TODO: THIS NEEDS TO BE STORED IN THE GRID -- NOT THE OPTION BUTTONS!!!
                if(this.rdoFileRecordTypeTrailer.Checked) {
                    layout.LayoutType = LayoutTypeEnum.Trailer;
                } else {
                    layout.LayoutType = LayoutTypeEnum.Header;
                }

                foreach(ListViewItem col in Items) {

                    griditem = new cGridItem(layout);
                    griditem.FieldName = col.Text;
                    griditem.DisplayName = col.SubItems[0].Text;

                    if(int.TryParse(col.SubItems[1].Text, out intTemp)) {
                        griditem.ColumnWidth = intTemp;
                    //} else {
                    }

                    if(col.SubItems[2].Text.Length > 0) {
                        griditem.PadChar = col.SubItems[2].Text[0];
                    //} else {
                    //    griditem.PadChar = '0';
                    }

                    if(col.SubItems[3].Text.Length > 0 && col.SubItems[3].Text[0] == 'L') {
                        griditem.Justification = JustifyEnum.Left;
                    //} else {
                    //    griditem.Justification = JustifyEnum.Right;
                    }

                    if(col.SubItems[4].Text.Length > 0 && (col.SubItems[4].Text.Substring(0, 1).ToLower() == "x" || col.SubItems[4].Text.Substring(0, 1).ToLower() == "y")) {
                        griditem.Quotes = true;
                    } else {
                        griditem.Quotes = false;
                    }

                    griditem.Format = col.SubItems[5].Text;
                }
            }
        }
        #endregion 


        #region Load Instructions File into form
        private void LoadInstructions(string FileName) {

            cExtractInstructions instructions = new cExtractInstructions();
            instructions.ExceptionOccurred += OnExceptionOccurred;
            instructions.OutputStatusMsg += OnOutputStatusMessage;           
            
            if(instructions.ParseInstructionsFile(FileName)) {

                this.txtExtractFilePath.Text = instructions.ExtractPath;
                this.txtLogFilePath.Text = instructions.LogFilePath;
                this.txtImageFilePath.Text = instructions.ImagePath;
                //this.chkUsePostProcessingDLL.Checked = instructions.PostProcDLLFileName.Length > 0
                this.txtPostProcessingDLLFileName.Text = instructions.PostProcDLLFileName;
                //this.rdoImageFileFormatNone.Checked = instructions.
                //this.rdoImageFileFormatNone;
                //this.rdoImageFileFormatPDF;
                //this.rdoImageFileFormatSingleMultiTIFF;
                //this.rdoImageFileFormatEmbeddedTIFF;
                //this.chkCombineImagesAcrossProcessingDates;
                //this.cboFieldDelimiter;

                int iPos;

                iPos = this.cboFieldDelimiter.FindString(instructions.FieldDelim);
                if(iPos >= 0) {
                    this.cboFieldDelimiter.SelectedIndex = iPos;
                } else {
                    this.cboRecordDelimiter.SelectedIndex = this.cboRecordDelimiter.FindString("|");
                }

                iPos = this.cboRecordDelimiter.FindString(instructions.RecordDelim);
                if(iPos >= 0) {
                    this.cboRecordDelimiter.SelectedIndex = iPos;
                } else {
                    this.cboRecordDelimiter.SelectedIndex = this.cboRecordDelimiter.FindString("\n");
                }
                this.chkOmitPadding.Checked = instructions.NoPad;
                this.chkEncloseFieldDataInQuotes.Checked = instructions.UseQuotes;
                this.chkShowNULLAsSpace.Checked = instructions.NullAsSpace;

                /*tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileTrailers"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankTrailers"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerTrailers"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchTrailers"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeTransactionHeaders"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodTransactionTrailers"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCheckRecords"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeInvoiceRecords"].Nodes.Clear();
                tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeDocumentRecords"].Nodes.Clear();*/
                
                LoadLayouts(instructions.FileLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileTrailers"],
                            this.cboFileLayouts,
                            this.rdoFileRecordTypeHeader,
                            this.rdoFileRecordTypeTrailer,
                            this.lvFileFields);

                LoadLayouts(instructions.BankLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankTrailers"],
                            this.cboBankLayouts,
                            this.rdoBankRecordTypeHeader,
                            this.rdoBankRecordTypeTrailer,
                            this.lvBankFields);

                LoadLayouts(instructions.CustomerLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerTrailers"],
                            this.cboCustomerLayouts,
                            this.rdoCustomerRecordTypeHeader,
                            this.rdoCustomerRecordTypeTrailer,
                            this.lvCustomerFields);

                LoadLayouts(instructions.LockboxLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxHeaders"],
                            this.cboLockboxLayouts,
                            this.rdoLockboxRecordTypeHeader,
                            this.rdoLockboxRecordTypeTrailer,
                            this.lvLockboxFields);

                LoadLayouts(instructions.BatchLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchTrailers"],
                            this.cboBatchLayouts,
                            this.rdoBatchRecordTypeHeader,
                            this.rdoBatchRecordTypeTrailer,
                            this.lvBatchFields);

                LoadLayouts(instructions.TransactionLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeTransactionHeaders"],
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodTransactionTrailers"],
                            this.cboTransactionLayouts,
                            this.rdoTransactionRecordTypeHeader,
                            this.rdoTransactionRecordTypeTrailer,
                            this.lvTransactionFields);

                LoadLayouts(instructions.CheckLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCheckRecords"],
                            this.cboCheckLayouts,
                            this.lvCheckFields);

                LoadLayouts(instructions.StubLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeInvoiceRecords"],
                            this.cboStubLayouts,
                            this.lvStubFields);

                LoadLayouts(instructions.DocumentLayouts, 
                            tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeDocumentRecords"],
                            this.cboDocumentLayouts,
                            this.lvDocumentFields);

                LoadOrderByColumns(instructions.BankOrderByColumns, this.lvBankOrderByColumns);
                LoadOrderByColumns(instructions.CustomerOrderByColumns, this.lvCustomerOrderByColumns);
                LoadOrderByColumns(instructions.LockboxOrderByColumns, this.lvLockboxOrderByColumns);
                LoadOrderByColumns(instructions.BatchOrderByColumns, this.lvBatchOrderByColumns);
                LoadOrderByColumns(instructions.TransactionsOrderByColumns, this.lvTransactionsOrderByColumns);
                LoadOrderByColumns(instructions.ChecksOrderByColumns, this.lvChecksOrderByColumns);
                LoadOrderByColumns(instructions.StubsOrderByColumns, this.lvStubsOrderByColumns);
                LoadOrderByColumns(instructions.DocumentsOrderByColumns, this.lvDocumentsOrderByColumns);
                
                tvGeneralLayout.Nodes["nodeLayout"].ExpandAll();

                this.Text = "Extract Wizard - " + InputFileName; 
            }
        }

        private void LoadOrderByColumns(cOrderByColumn[] orderbycolumns, ListView listview) {
        
            string[] strColumns;
        
            foreach(cOrderByColumn orderbycolumn in orderbycolumns) {

                strColumns = new string[] { 
                    orderbycolumn.ColumnName, 
                    orderbycolumn.OrderByDir.ToString()
                };

                listview.Items.Add(new ListViewItem(strColumns));
            }
        }

        private void LoadLayouts(cLayout[] Layouts, 
                                 TreeNode HeaderNode,
                                 TreeNode TrailerNode, 
                                 ComboBox LayoutsCombo, 
                                 RadioButton RecordTypeHeaderOption, 
                                 RadioButton RecordTypeTrailerOption,
                                 ListView lvGridItems) {

            bool bolIsFirst = true;
            string[] strGridItems;

            HeaderNode.Nodes.Clear();
            TrailerNode.Nodes.Clear();

            foreach(cLayout layout in Layouts) {
                               
                if(layout.LayoutType == LayoutTypeEnum.Header) {
                    HeaderNode.Nodes.Add(BuildTreeNode(layout.LayoutName));
                } else {
                    TrailerNode.Nodes.Add(BuildTreeNode(layout.LayoutName));
                }

                LayoutsCombo.Items.Add(layout);

                if(bolIsFirst) {
                    RecordTypeHeaderOption.Checked = (layout.LayoutType == LayoutTypeEnum.Header);
                    RecordTypeTrailerOption.Checked = (layout.LayoutType == LayoutTypeEnum.Trailer);
                    LayoutsCombo.SelectedIndex = LayoutsCombo.Items.Count -1;

                    foreach(cGridItem griditem in layout.GridItems) {

                        strGridItems = new string[] { 
                            griditem.FieldName, 
                            griditem.DisplayName, 
                            griditem.ColumnWidth.ToString(), 
                            griditem.PadChar.ToString(), 
                            griditem.Justification.ToString(), 
                            griditem.Quotes.ToString(), 
                            griditem.Format
                        };

                        lvGridItems.Items.Add(new ListViewItem(strGridItems));
                    }
                }

                bolIsFirst = false;
            }
        }

        private void LoadLayouts(cLayout[] Layouts, 
                                 TreeNode HeaderNode,
                                 ComboBox LayoutsCombo,
                                 ListView lvGridItems) {

            bool bolIsFirst = true;
            string[] strGridItems;

            HeaderNode.Nodes.Clear();

            foreach(cLayout layout in Layouts) {

                HeaderNode.Nodes.Add(BuildTreeNode(layout.LayoutName));

                LayoutsCombo.Items.Add(layout);

                if(bolIsFirst) {
                    LayoutsCombo.SelectedIndex = LayoutsCombo.Items.Count -1;

                    foreach(cGridItem griditem in layout.GridItems) {

                        strGridItems = new string[] {
                            griditem.FieldName.ToString(), 
                            griditem.DisplayName.ToString(), 
                            griditem.ColumnWidth.ToString(), 
                            griditem.PadChar.ToString(), 
                            griditem.Justification.ToString(), 
                            griditem.Quotes.ToString(), 
                            griditem.Format.ToString()};

                        lvGridItems.Items.Add(new ListViewItem(strGridItems));
                    }
                }

                bolIsFirst = false;
            }
        }
        
        private TreeNode BuildTreeNode(string Text) {
            TreeNode newNode = new TreeNode(Text);
            newNode.ForeColor = Color.Blue;
            return(newNode);
        }
        #endregion


        #region Tool Strip Menu item Events.

        //
        // File Menu Items
        //
        private void newSetupToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void openSetupToolStripMenuItem_Click(object sender, EventArgs e) {
            
            this.openFileDialog1.Filter = "IntegraPAY Extract Script Files (*.cxs)|*.cxs";
            this.openFileDialog1.FileName = "";
            this.openFileDialog1.InitialDirectory = Settings.SetupPath;
            DialogResult dr = this.openFileDialog1.ShowDialog();
            
            if(this.openFileDialog1.FileName.Length > 0 && File.Exists(this.openFileDialog1.FileName)) {
                InputFileName = this.openFileDialog1.FileName;
                LoadInstructions(InputFileName);

                SetFormState(false);
            }
        }

        private void closeSetupToolStripMenuItem_Click(object sender, EventArgs e) {

            cExtractInstructions currentInstructionsObject;
            cExtractInstructions fileInstructionsObject;
            bool bolPromptForSave = false;
            string strSaveFileName;
            bool bolCancel = false;

            currentInstructionsObject = GetInstructionsFromForm();

            if(this.InputFileName.Length > 0 && File.Exists(this.InputFileName)) {
                fileInstructionsObject = new cExtractInstructions();
                fileInstructionsObject.ExceptionOccurred += OnExceptionOccurred;
                fileInstructionsObject.OutputStatusMsg += OnOutputStatusMessage;
                fileInstructionsObject.ParseInstructionsFile(this.InputFileName);
                bolPromptForSave = (currentInstructionsObject.GetHashCode() == fileInstructionsObject.GetHashCode());
            }

            if(this.InputFileName.Length > 0) {
                strSaveFileName = this.InputFileName;
            } else {
                strSaveFileName = "ExtractInstructions1.cxs";
            }

            if(bolPromptForSave) {
                DialogResult result = MessageBox.Show("Do you want to save the changes to " + strSaveFileName + "?", "Extract Wizard", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);

                if(result == DialogResult.Yes) {
                    currentInstructionsObject.SaveAsCxs(this.InputFileName);
                } else if(DialogResult == DialogResult.Cancel) {
                    bolCancel = true;
                }
            }

            if(!bolCancel) {
                ResetForm();
                SetFormState(true);
            }
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e) {

            cExtractInstructions currentInstructionsObject;
            //bool bolPromptForSave = false;
            string strSaveFileName;
            //string strFileName;

            currentInstructionsObject = GetInstructionsFromForm();

            if(this.InputFileName.Length > 0) {
                strSaveFileName = this.InputFileName;
            } else {
                strSaveFileName = "ExtractInstructions1.cxs";
                //bolPromptForSave = true;
            }

             currentInstructionsObject.SaveAsCxs(this.InputFileName);
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void printToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void printerSetupToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) {

            bool bolCancel = false;

            //TODO: Determine if a file is loaded, but not saved yet(NEW Mode)
            //cExtractInstructions objFileInstructions = LoadInputFile(this.InputFileName);
            //if(objFileInstructions.GetHashCode() != this.GetInstructionsObject().GetHashCode()) {
            //    switch(PromptUser) {
            //        case Save:
            //            bolCancel = !Save();
            //            break;
            //        case DoNotSave:
            //            break;
            //        case Cancel:
            //            bolCancel = true;
            //            break;
            //    }
            //}

            if(!bolCancel) {
                this.Close();
            }
        }




        //
        // View Menu Items
        //
        private void setupToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void resultsToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }

        private void logFileToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }




        //
        // Run Menu Items
        //
        private void runToolStripMenuItem_Click(object sender, EventArgs e) {

            cExtractParms Parms = new cExtractParms();           

            if(this.InputFileName.Length > 0) {
                Parms.SetupFile = this.InputFileName;
            } else {
                this.openFileDialog1.Filter = "cxs input files|*.cxs";
                this.openFileDialog1.FileName = "";
                this.openFileDialog1.InitialDirectory = Settings.SetupPath;
                DialogResult dr = this.openFileDialog1.ShowDialog();
                
                if(this.openFileDialog1.FileName.Length > 0 && File.Exists(this.openFileDialog1.FileName)) {
                    Parms.SetupFile = this.openFileDialog1.FileName;
                }
            }
            
            if(Parms.SetupFile.Length > 0) {

                LoadInstructions(Parms.SetupFile);
                SetFormState(false);

                frmExtractRun objExtractRun = new frmExtractRun(Parms, true);
                objExtractRun.SetFormPositions(false);
                objExtractRun.ShowDialog(this);
            }
        }

        //
        // Help Menu Items
        //
        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAbout about = new frmAbout();
            about.ShowDialog();
            about.Dispose();
        }

        private void licenseToolStripMenuItem_Click(object sender, EventArgs e) {
            MessageBox.Show("Not implemented!");
        }
        #endregion 


        #region Accessed Tab logic - Available database fields are loaded as a tab is accessed.

        private bool[] _AccessedTabs = {false, false, false, false, false, false, false, false, false, false, false};

        private void tabMain_SelectedIndexChanged(object sender, EventArgs e) {

            if(!_AccessedTabs[tabMain.SelectedIndex]) {
                switch(tabMain.SelectedIndex) {
                    case 2:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Bank"), this.lstBankColumns);
                        break;
                    case 3:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Customer"), this.lstCustomerColumns);
                        break;
                    case 4:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Lockbox"), this.lstLockboxColumns);
                        break;
                    case 5:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Batch"), this.lstBatchColumns);
                        break;
                    case 6:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Transactions"), this.lstTransactionColumns);
                        break;
                    case 7:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Checks"), this.lstCheckColumns);
                        break;
                    case 8:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Stubs"), this.lstStubColumns);
                        break;
                    case 9:
                        LoadDatabaseColumns(SQL_Public.SQL_DescribeTable("Documents"), this.lstDocumentColumns);
                        break;
                }

                _AccessedTabs[tabMain.SelectedIndex] = true;
            }
        }

        private void LoadDatabaseColumns(string SourceSQL, ListBox ColumnsListBox) {

            DataTable dt;
            Database.CreateDataTable(SourceSQL, out dt);

            cDatabase2.SortDataTable(dt, "ColumnName");

            foreach(DataRow dr in dt.Rows) {
                ColumnsListBox.Items.Add(dr["ColumnName"]);
            }
        }
        #endregion


        #region Enable / Disable Form controls depending on if a layout has been loaded.

        private void ResetForm() {
        
            this.lvFileFields.Items.Clear();
            this.lvBankFields.Items.Clear();
            this.lvCustomerFields.Items.Clear();
            this.lvLockboxFields.Items.Clear();
            this.lvBatchFields.Items.Clear();
            this.lvTransactionFields.Items.Clear();
            this.lvCheckFields.Items.Clear();
            this.lvStubFields.Items.Clear();
            this.lvDocumentFields.Items.Clear();
            
            this.txtExtractFilePath.Text = "";
            this.txtImageFilePath.Text = "";
            this.txtLogFilePath.Text = "";
            this.txtPostProcessingDLLFileName.Text = "";
            
            this.chkCombineImagesAcrossProcessingDates.Checked = false;
            this.chkEncloseFieldDataInQuotes.Checked = false;
            this.chkOmitPadding.Checked = false;
            this.chkShowNULLAsSpace.Checked = false;
            this.chkUsePostProcessingDLL.Checked = false;
            
            this.cboFieldDelimiter.SelectedIndex = this.cboFieldDelimiter.FindString("|");
            this.cboRecordDelimiter.SelectedIndex = this.cboRecordDelimiter.FindString("\n");

            this.cboFileLayouts.Items.Clear();
            this.cboFileLayouts.Text = "";
            this.cboBankLayouts.Items.Clear();
            this.cboBankLayouts.Text = "";
            this.cboCustomerLayouts.Items.Clear();
            this.cboCustomerLayouts.Text = "";
            this.cboLockboxLayouts.Items.Clear();
            this.cboLockboxLayouts.Text = "";
            this.cboBatchLayouts.Items.Clear();
            this.cboBatchLayouts.Text = "";
            this.cboTransactionLayouts.Items.Clear();
            this.cboTransactionLayouts.Text = "";
            this.cboCheckLayouts.Items.Clear();
            this.cboCheckLayouts.Text = "";
            this.cboStubLayouts.Items.Clear();
            this.cboStubLayouts.Text = "";
            this.cboDocumentLayouts.Items.Clear();
            this.cboDocumentLayouts.Text = "";
            
            this.rdoFileRecordTypeHeader.Checked = true;
            this.rdoBankRecordTypeHeader.Checked = true;
            this.rdoCustomerRecordTypeHeader.Checked = true;
            this.rdoLockboxRecordTypeHeader.Checked = true;
            this.rdoBatchRecordTypeHeader.Checked = true;
            this.rdoTransactionRecordTypeHeader.Checked = true;
            this.rdoBatchRecordTypeHeader.Checked = true;
            this.rdoTransactionRecordTypeHeader.Checked = true;
            
            this.rdoImageFileFormatNone.Checked = true;
            
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeTransactionHeaders"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCheckRecords"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeInvoiceRecords"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeDocumentRecords"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeJointDetailRecords"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodTransactionTrailers"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBatchTrailers"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeLockboxTrailers"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeCustomerTrailers"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeBankTrailers"].Nodes.Clear();
            this.tvGeneralLayout.Nodes["nodeLayout"].Nodes["nodeFileTrailers"].Nodes.Clear();
            //this.tvGeneralLayout.Nodes["nodeLayout"].Nodes.Clear();
            
            this.lvBankOrderByColumns.Items.Clear();
            this.lvCustomerOrderByColumns.Items.Clear();
            this.lvLockboxOrderByColumns.Items.Clear();
            this.lvBatchOrderByColumns.Items.Clear();
            this.lvTransactionsOrderByColumns.Items.Clear();
            this.lvChecksOrderByColumns.Items.Clear();
            this.lvStubsOrderByColumns.Items.Clear();
            this.lvDocumentsOrderByColumns.Items.Clear();
            this.lvJointDetailFields.Items.Clear();
        }

        private void SetFormState(bool Locked) {

            DisableTabPageControls(this.tpGeneral.Controls, Locked);
            DisableTabPageControls(this.tpFile.Controls, Locked);
            DisableTabPageControls(this.tpBank.Controls, Locked);
            DisableTabPageControls(this.tpCustomer.Controls, Locked);
            DisableTabPageControls(this.tpLockbox.Controls, Locked);
            DisableTabPageControls(this.tpBatch.Controls, Locked);
            DisableTabPageControls(this.tpTransaction.Controls, Locked);
            DisableTabPageControls(this.tpCheck.Controls, Locked);
            DisableTabPageControls(this.tpStub.Controls, Locked);
            DisableTabPageControls(this.tpDocument.Controls, Locked);
            DisableTabPageControls(this.tpJointDetail.Controls, Locked);

            // File Menu
            //this.fileToolStripMenuItem.Enabled;
            //this.newSetupToolStripMenuItem;
            //this.openSetupToolStripMenuItem;
            this.saveToolStripMenuItem.Enabled = !Locked;
            this.saveAsToolStripMenuItem.Enabled = !Locked;
            this.printToolStripMenuItem.Enabled = !Locked;
            this.printerSetupToolStripMenuItem.Enabled = !Locked;
            //this.exitToolStripMenuItem;

            // View Menu
            //this.viewToolStripMenuItem;
            this.setupToolStripMenuItem.Enabled = !Locked;
            this.resultsToolStripMenuItem.Enabled = !Locked;
            this.logFileToolStripMenuItem.Enabled = !Locked;

            // Run Menu
            //this.runToolStripMenuItem;

            // Help Menu
            //this.helpToolStripMenuItem;
            //this.aboutToolStripMenuItem;
            //this.licenseToolStripMenuItem;
        }
        
        private void DisableTabPageControls(Control.ControlCollection Controls, bool Locked) {
            foreach(Control control in Controls) {
                control.Enabled = !Locked;
            }
        }
        #endregion 


        private void frmMain_Load(object sender, EventArgs e)
        {

        }

        private void OnOutputStatusMessage(string StatusMsg) {
            //TODO: Add this item to the form.
            //this.toolStripStatusMsg.Text = StatusMsg;
            Application.DoEvents();
        }

        private void OnExceptionOccurred(object Sender, System.Exception e) {

            MessageBox.Show("An Exception Occurred " + e.StackTrace.TrimStart() + "\n\n" + e.Message, "Exception Occurred", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

    }
}


