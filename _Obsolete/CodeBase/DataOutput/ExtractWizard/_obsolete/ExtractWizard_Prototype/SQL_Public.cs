using System;
using System.Collections.Generic;
using System.Text;

/*******************************************************************************
*
*    Module: SQL_Public
*  Filename: SQL_Public.cs
*    Author: Joel Caples
*
* Revisions:
*
* ----------------------
* CR 19480 JMC 04/09/2007
*   -Initial release version.
******************************************************************************/
namespace DMP.Delivery.Extract.ExtractWizard
{
    internal static class SQL_Public {

        public static string SQL_DescribeTable(string tableName) {

            StringBuilder sb = new StringBuilder();

            sb.Append(" SELECT");
            sb.Append("    LEFT(OBJECT_NAME(c.id),30) AS 'TableName',");
            sb.Append("    LEFT(c.name,30) AS 'ColumnName',");
            sb.Append("    CAST(c.colid AS int) AS 'ColumnOrdinal',");
            sb.Append("    UPPER(LEFT(t.name,16)) AS 'Type',");
            sb.Append("    CASE WHEN t.name LIKE '%char%' THEN c.length ELSE c.prec END AS 'Length'");  // ,
            sb.Append(" FROM syscolumns c WITH (NOLOCK)");
            sb.Append(" INNER JOIN systypes t WITH (NOLOCK) ON");
            sb.Append("    c.xusertype = t.xusertype");
            sb.Append(" INNER JOIN sysobjects o WITH (NOLOCK) ON");
            sb.Append("    c.id = o.id");
            sb.Append(" WHERE o.xtype = 'U' AND o.name = ('" + tableName + "')");

            return(sb.ToString());
        }
    }
}
