﻿namespace IntegrationTestFramework
{
    /// <summary>
    /// High-level interface to R360 UI that's not specific to a particular page.
    /// </summary>
    public static class Ui
    {
        private static Browser _browser;

        internal static Browser Browser
        {
            get
            {
                if (_browser == null)
                {
                    _browser = new Browser(Configuration.CreateWebDriver());
                    InitializeBrowser();
                }
                return _browser;
            }
        }

        public static void Close()
        {
            if (_browser != null)
            {
                _browser.Dispose();
            }
            _browser = null;
        }
        private static void InitializeBrowser()
        {
            Pages.Login.GoTo();
            Pages.Login.LogIn(Configuration.LoginEntity, Configuration.LoginName, Configuration.LoginPassword);
        }
        public static void SaveScreenshot(string testName)
        {
            if (_browser != null)
            {
                _browser.SaveScreenshot(testName);
            }
        }
    }
}