﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntegrationTestFramework
{
    public class DatePicker
    {
        public static void SelectDate(IWebElement datePickerDiv, string date)
        {

            var elements = date.Split('/');
            var expYear = elements[2];
            var expMonth = Convert.ToInt32(elements[0]);
            var expDay = elements[1];

            //select the given year
            var yearSel = new SelectElement(datePickerDiv.FindElement(By.ClassName("ui-datepicker-year")));
            yearSel.SelectByValue(expYear);

            //select the given month
            var monthSel = new SelectElement(datePickerDiv.FindElement(By.ClassName("ui-datepicker-month")));
            monthSel.SelectByValue(Convert.ToString(expMonth - 1));

            //get the table
            var calTable = datePickerDiv.FindElement(By.ClassName("ui-datepicker-calendar"));
            //click on the correct/given cell/date
            calTable.FindElement(By.LinkText(expDay)).Click();
        }
    }
}