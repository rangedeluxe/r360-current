﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;
using System.Text.RegularExpressions;
using NUnit.Framework;

namespace IntegrationTestFramework
{
    internal static class WebElementExtensions
    {
        public static IWebElement FindElement(this IWebElement element, By by, TimeSpan timeout)
        {
            return Ui.Browser.Wait(timeout, () => element.FindElement(by), by.ToString());
        }
        public static bool HasClass(this IWebElement element, string className)
        {
            return Regex.IsMatch(element.GetAttribute("class"), string.Format("\\b{0}\\b", className));
        }
        public static IWebElement SelectAll(this IWebElement element)
        {
            Ui.Browser.CreateActions()
                .Click(element)
                .KeyDown(Keys.Control)
                .SendKeys("a")
                .KeyUp(Keys.Control)
                .Perform();
            return element;
        }
        public static IWebElement SingleDisplayed(this IEnumerable<IWebElement> elements,
            string elementDescription)
        {
            var result = elements.SingleDisplayedOrDefault(elementDescription);
            if (result == null)
            {
                var pageLocation = Pages.Navigation.GetPageLocation();
                Assert.Fail("Couldn't find " + elementDescription + " on page " + pageLocation);
            }
            return result;
        }
        public static IWebElement SingleDisplayedOrDefault(this IEnumerable<IWebElement> elements,
            string elementDescription)
        {
            var displayedElements = elements.Where(e => e.Displayed).ToList();
            if (displayedElements.Count == 0)
                return null;

            if (displayedElements.Count > 1)
            {
                var pageLocation = Pages.Navigation.GetPageLocation();
                var message = "Found more than one " + elementDescription + " on page " + pageLocation;
                Assert.Fail(message);
            }

            return displayedElements.Single();
        }
    }
}