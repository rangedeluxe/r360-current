﻿using System;
using System.Collections.ObjectModel;
using System.Drawing.Imaging;
using System.Linq;
using System.Threading;
using IntegrationUtilities;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.Extensions;
using OpenQA.Selenium.Support.UI;

namespace IntegrationTestFramework
{
    internal class Browser : IDisposable
    {
        private readonly IWebDriver _driver;

        public Browser(IWebDriver driver)
        {
            _driver = driver;
        }

        public string Title
        {
            get { return _driver.Title; }
        }

        /// <summary>
        /// Use JavaScript to deselect all items in a &lt;select&gt;. Significantly
        /// faster than doing the same thing with <see cref="SelectElement.DeselectAll"/>.
        /// </summary>
        /// <param name="element">The &lt;select&gt; element.</param>
        public void ClearListBoxSelections(IWebElement element)
        {
            const string script = @"
                var options = arguments[0].options;
                for (var i = 0; i < options.length; ++i) {
                    options[i].selected = false;
                }
            ";
            ExecuteJavaScript(script, element);
        }
        public Actions CreateActions()
        {
            return new Actions(_driver);
        }
        public void Dispose()
        {
            _driver.Navigate().GoToUrl(Configuration.LogoutUrl);
            _driver.Dispose();
        }
        private void ExecuteJavaScript(string script, params object[] args)
        {
            var javaScriptExecutor = (IJavaScriptExecutor) _driver;
            javaScriptExecutor.ExecuteScript(script, args);
        }
        public void ExpectSpinners(Spinners spinners = null)
        {
            var expectedSpinners = spinners ?? Spinners.Default;
            expectedSpinners.WaitFor(_driver);
        }
        /// <summary>
        /// Returns a single element. Throws if the element doesn't exist.
        /// </summary>
        /// <param name="by">The locating mechanism to use.</param>
        /// <returns>The element that was found.</returns>
        /// <remarks>
        /// If you don't want an exception, use <see cref="FindElements(OpenQA.Selenium.By)"/>
        /// and call FirstOrDefault on the result.
        /// </remarks>
        /// <exception cref="NoSuchElementException">If no element matches the criteria.</exception>
        public IWebElement FindElement(By by)
        {
            return _driver.FindElement(by);
        }
        /// <summary>
        /// Returns a single element, waiting if it doesn't exist yet.
        /// Throws if the timeout expires and the element still doesn't exist.
        /// </summary>
        /// <param name="by">The locating mechanism to use.</param>
        /// <param name="timeout">How long to wait for an element to exist.</param>
        /// <remarks>
        /// If you don't want an exception, use <see cref="FindElements(OpenQA.Selenium.By, System.TimeSpan)"/>
        /// and call FirstOrDefault on the result.
        /// </remarks>
        /// <exception cref="WebDriverTimeoutException">
        /// If the timeout expires and the element still doesn't exist.
        /// </exception>
        public IWebElement FindElement(By by, TimeSpan timeout)
        {
            return Wait(timeout, () => FindElement(by), by.ToString());
        }
        /// <summary>
        /// Returns a list of matching elements, which may be an empty list.
        /// </summary>
        /// <param name="by">The locating mechanism to use.</param>
        /// <returns>A list of matching elements, which may be empty.</returns>
        public ReadOnlyCollection<IWebElement> FindElements(By by)
        {
            return _driver.FindElements(by);
        }
        /// <summary>
        /// Returns a list of matching elements, waiting up to <see cref="timeout"/>
        /// for any to appear. If the timeout elapses, returns an empty list.
        /// </summary>
        /// <param name="by">The locating mechanism to use.</param>
        /// <param name="timeout">How long to wait for elements to exist.</param>
        /// <returns>A list of matching elements, or an empty list if the timeout expires.</returns>
        public ReadOnlyCollection<IWebElement> FindElements(By by, TimeSpan timeout)
        {
            var endTime = DateTime.Now + timeout;
            do
            {
                var elements = FindElements(by);
                if (elements.Any())
                {
                    return elements;
                }
                Thread.Sleep(TimeSpan.FromMilliseconds(500));
            } while (DateTime.Now <= endTime);

            return new ReadOnlyCollection<IWebElement>(new IWebElement[] {});
        }
        public void GoTo(Uri url)
        {
            _driver.Navigate().GoToUrl(url);
        }
        public void SaveScreenshot(string testName)
        {
            var fileName = AttachmentUtilities.TestNameToFileBaseName(testName) + ".png";
            _driver.TakeScreenshot().SaveAsFile(fileName, ImageFormat.Png);
        }
        /// <summary>
        /// Waits until the given condition returns a Boolean true or a non-null reference.
        /// Throws if the timeout expires and the condition still hasn't succeeded.
        /// </summary>
        /// <typeparam name="T">Either System.Bool or a reference type.</typeparam>
        /// <param name="timeout">How long to wait.</param>
        /// <param name="condition">
        /// The condition to wait for. The function waits until this returns
        /// true or non-null, or <see cref="timeout"/> has elapsed.
        /// </param>
        /// <param name="message">Text to be included in the exception message if we time out.</param>
        /// <returns>The first true or non-null return value from the <see cref="condition"/> callback.</returns>
        /// <exception cref="WebDriverTimeoutException">
        /// If the timeout elapses and <see cref="condition"/> hasn't yet returned
        /// true or non-null.
        /// </exception>
        public T Wait<T>(TimeSpan timeout, Func<T> condition, string message)
        {
            var wait = new WebDriverWait(_driver, timeout) {Message = message};
            return wait.Until(driver => condition());
        }
    }
}