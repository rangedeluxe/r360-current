﻿using IntegrationTestFramework.PageObjects;

namespace IntegrationTestFramework
{
    public static class Pages
    {
        public static AdvancedSearchPage AdvancedSearch
        {
            get { return new AdvancedSearchPage(); }
        }
        public static AdvancedSearchResultsPage AdvancedSearchResults
        {
            get { return new AdvancedSearchResultsPage(); }
        }
        public static Page Current
        {
            get
            {
                // This doesn't necessarily need to include every possible page,
                // just those that override members from Page to request special
                // behavior for common methods.
                switch (Navigation.GetPageLocation())
                {
                    case PageLocations.AdvancedSearch:
                        return AdvancedSearch;
                    case PageLocations.BankMaintenance:
                        return BankMaintenance;
                    default:
                        return new Page();
                }
            }
        }
        public static ReceivablesSummaryPage ReceivablesSummary
        {
            get { return new ReceivablesSummaryPage(); }
        }
        public static LoginPage Login
        {
            get { return new LoginPage(); }
        }
        public static PageNavigation Navigation
        {
            get { return new PageNavigation(); }
        }
        public static UserManagementPage UserManagement
        {
            get { return new UserManagementPage(); }
        }

        public static BankMaintenancePage BankMaintenance
        {
            get { return new BankMaintenancePage(); }
        }
    }
}