﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;

namespace IntegrationTestFramework
{
    internal class Spinners
    {
        public static readonly Spinners Default = new Spinners(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(1));

        private readonly TimeSpan _initialWait;
        private readonly TimeSpan _quietPeriod;

        public Spinners(TimeSpan initialWait, TimeSpan quietPeriod)
        {
            _initialWait = initialWait;
            _quietPeriod = quietPeriod;
        }

        public void WaitFor(IWebDriver driver)
        {
            var expiration = DateTime.Now + _initialWait;
            while (true)
            {
                Thread.Sleep(250);

                var spinners = driver.FindElements(By.ClassName("spinDiv"));
                if (spinners.Any())
                {
                    expiration = DateTime.Now + _quietPeriod;
                }
                else if (DateTime.Now > expiration)
                {
                    break;
                }
            }
        }
    }
}