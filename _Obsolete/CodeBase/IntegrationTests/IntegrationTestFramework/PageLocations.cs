﻿namespace IntegrationTestFramework
{
    public static class PageLocations
    {
        public const string AdvancedSearch = "Advanced Search";
        public const string AdvancedSearchResults = "Advanced Search Results";
        public const string Dashboard = "Dashboard/Receivables Summary";
        public const string UserManagement = "Security Administration/Users";
        public const string BankMaintenance = "Bank Maintenance";
    }
}