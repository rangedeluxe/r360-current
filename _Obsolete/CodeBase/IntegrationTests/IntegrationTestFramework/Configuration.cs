﻿using System;
using System.Configuration;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace IntegrationTestFramework
{
    /// <summary>
    /// Top-level configuration information that may later be context-sensitive
    /// (depending on which browser/VM is running tests).
    /// </summary>
    internal static class Configuration
    {
        public static Uri AdvancedSearchUrl
        {
            get { return new Uri(ConfigurationManager.AppSettings["R360.AdvancedSearch.Uri"]); }
        }

        public static Uri InitialUrl
        {
            get { return new Uri(ConfigurationManager.AppSettings["R360.Login.Uri"]); }
        }

        public static Uri LogoutUrl
        {
            get { return new Uri(ConfigurationManager.AppSettings["R360.Logout.Uri"]); }
        }

        public static string LoginEntity
        {
            get { return ConfigurationManager.AppSettings["R360.Login.Entity"]; }
        }

        public static string LoginName
        {
            get { return ConfigurationManager.AppSettings["R360.Login.User"]; }
        }

        public static string LoginPassword
        {
            get { return ConfigurationManager.AppSettings["R360.Login.Password"]; }
        }

        public static IWebDriver CreateWebDriver()
        {
            var browserName = Environment.GetEnvironmentVariable("IntegrationBrowser") ?? "";
            switch (browserName.ToLowerInvariant())
            {
                case "chrome":
                    return new ChromeDriver();

                case "ie":
                    return new InternetExplorerDriver();

                case "firefox":
                case "":
                case null:
                    return new FirefoxDriver();

                default:
                    throw new InvalidOperationException("Unknown browser name: " + browserName);
            }
        }
    }
}