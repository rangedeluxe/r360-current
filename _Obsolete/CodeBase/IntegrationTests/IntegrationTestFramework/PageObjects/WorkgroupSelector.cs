﻿using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public class WorkgroupSelector : FilterableComboBoxBase<WorkgroupSelector.SelectionPath>
    {
        public class SelectionPath : ISelection
        {
            private readonly IReadOnlyList<string> _pathSegments;

            public SelectionPath(string selection)
            {
                _pathSegments = selection.Split('\\');
            }

            public string FilterText
            {
                get { return _pathSegments.Last(); }
            }
            public string ParentPath
            {
                get { return string.Join(" / ", _pathSegments.Take(_pathSegments.Count - 1)); }
            }

            public bool MatchesSelectorText(string text)
            {
                var firstLine = text.Split('\r', '\n').First();
                return firstLine.EndsWith(": " + FilterText);
            }
            public override string ToString()
            {
                return FilterText;
            }
        }

        public WorkgroupSelector(IWebElement selector)
            : base(selector)
        {
        }

        private bool CanDropDown
        {
            get { return Selector.FindElements(By.ClassName("jqx-expander")).Any(); }
        }
        protected override bool IsDroppedDown
        {
            get { return Selector.FindElement(By.ClassName("jqx-widget-content")).Displayed; }
        }

        protected override SelectionPath CreateSelection(string selection)
        {
            return new SelectionPath(selection);
        }
        private void DropDown()
        {
            Selector.Click();
            Browser.Wait(TimeSpan.FromSeconds(5), () => IsDroppedDown, "Waiting for workgroup selector to drop down");
        }
        protected override void EnterFilterMode()
        {
            DropDown();
            ResetFilter();
            OpenModalFilterPanel();
        }
        protected override bool FilterResultMatches(IWebElement filterResult, SelectionPath selection)
        {
            var labels = filterResult.FindElements(By.TagName("td"));
            return
                labels.Count == 2 &&
                labels[0].Text == selection.ParentPath &&
                labels[1].Text == selection.FilterText;
        }
        private void OpenModalFilterPanel()
        {
            Selector.FindElement(By.ClassName("entitySelect")).Click();
        }
        private void ResetFilter()
        {
            var resetButton = Selector.FindElements(By.Id("filterClear")).FirstOrDefault();
            if (resetButton != null && resetButton.Displayed)
            {
                resetButton.Click();
            }
        }
        protected override void WaitForDropdownToClose()
        {
            if (CanDropDown)
            {
                base.WaitForDropdownToClose();
            }
        }
    }
}