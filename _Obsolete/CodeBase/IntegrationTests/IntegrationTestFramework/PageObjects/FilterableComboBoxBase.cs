using System;
using System.Collections.Generic;
using System.Linq;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public abstract class FilterableComboBoxBase<TSelection> : UiObject
        where TSelection : FilterableComboBoxBase<TSelection>.ISelection
    {
        public interface ISelection
        {
            string FilterText { get; }

            bool MatchesSelectorText(string text);
        }

        protected FilterableComboBoxBase(IWebElement selector)
        {
            Selector = selector;
        }

        protected abstract bool IsDroppedDown { get; }
        public IWebElement Selector { get; private set; }

        protected abstract TSelection CreateSelection(string selection);
        protected abstract void EnterFilterMode();
        protected abstract bool FilterResultMatches(IWebElement filterResult, TSelection selection);
        protected IWebElement FindFilterResult(TSelection selection)
        {
            return Browser.Wait(
                TimeSpan.FromSeconds(5),
                () =>
                {
                    var results = FindFilterResults();
                    return results.FirstOrDefault(element => FilterResultMatches(element, selection));
                },
                string.Format("Waiting for filter result '{0}'", selection));
        }
        protected IReadOnlyList<IWebElement> FindFilterResults()
        {
            var modalFilterPanel = FindModalFilterPanel();
            var resultContainer = modalFilterPanel.FindElement(By.ClassName("select2-results"));
            var results = resultContainer.FindElements(By.TagName("li"));
            return results;
        }
        protected IWebElement FindFilterTextBox()
        {
            var modalFilterPanel = FindModalFilterPanel();
            return modalFilterPanel.FindElement(By.ClassName("select2-input"));
        }
        protected IWebElement FindModalFilterPanel()
        {
            return Browser.FindElement(By.Id("select2-drop"), TimeSpan.FromSeconds(10));
        }
        public void Select(string selection)
        {
            var selectionPath = CreateSelection(selection);

            EnterFilterMode();
            SendKeysToFilterTextBox(selectionPath.FilterText);
            WaitForFilterResults();
            SelectFilterResult(selectionPath);
        }
        protected void SelectFilterResult(TSelection selection)
        {
            FindFilterResult(selection).Click();
            WaitForDropdownToClose();
            Browser.Wait(
                TimeSpan.FromSeconds(5),
                () => selection.MatchesSelectorText(Selector.Text),
                string.Format("Waiting for selection to change to '{0}' (was '{1}')",
                    selection.FilterText, Selector.Text));
        }
        protected void SendKeysToFilterTextBox(string text)
        {
            var filterTextBox = FindFilterTextBox();
            filterTextBox.SendKeys(text);
        }
        protected virtual void WaitForDropdownToClose()
        {
            Browser.Wait(TimeSpan.FromSeconds(5), () => !IsDroppedDown, "Waiting for dropdown to close");
        }
        protected void WaitForFilterResults()
        {
            var filterTextBox = FindFilterTextBox();
            Browser.Wait(
                TimeSpan.FromSeconds(10),
                () => !filterTextBox.GetAttribute("class").Contains("select2-active"),
                "Waiting for filter results");
        }
    }
}