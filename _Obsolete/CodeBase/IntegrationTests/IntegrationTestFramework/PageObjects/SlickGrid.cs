using System;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using System.Linq;
using NUnit.Framework;

namespace IntegrationTestFramework.PageObjects
{
    public class SlickGrid : GridBase
    {
        private bool _showingAllRows;

        protected IWebElement Pager { get; set; }
        public SlickGrid(IWebElement grid) : base(grid)
        {
        }
        public SlickGrid(IWebElement grid, IWebElement pager) : base(grid)
        {
            Pager = pager;
        }

        public void ClickUpdateButtonOnRow(string cellHeader, string cellContent)
        {
            var matchingCell = GetCellWithText(cellHeader, cellContent);
            if (matchingCell != null)
            {
                var row = matchingCell.FindElement(By.XPath(".."));
                var cells = row.FindElements(By.ClassName("slick-cell"));
                var editButton = cells.ElementAt(0).FindElements(By.ClassName("tableIcon")).SingleDisplayed("anchor");
                editButton.Click();
            }
            else
            {
                Assert.Fail("Row with text:{0} was not found", cellContent);
            }

        }

        public bool HasCellWithText(string cellHeader, string cellContent)
        {
            return GetCellWithText(cellHeader, cellContent) != null;
        }

        protected IWebElement GetCellWithText(string cellHeader, string cellContent)
        {
            var headers = Grid.FindElement(By.ClassName("slick-header-columns")).
                               FindElements(By.ClassName("slick-header-column"));

            var selectedHeader = headers.Where(h => h.Text == cellHeader).SingleDisplayed("header");
            var headerpos = headers.IndexOf(selectedHeader);

            var rows = Grid.FindElement(By.ClassName("grid-canvas")).FindElements(By.ClassName("slick-row"));
            return rows.Select(row => row.FindElements(By.ClassName("slick-cell"))).
                        Select(cells => cells.ElementAtOrDefault(headerpos)).
                        FirstOrDefault(elementAtHeaderPosition => elementAtHeaderPosition != null && 
                                        elementAtHeaderPosition.Text == cellContent);
        }

        public override void ClickCheckBoxInGridHeader()
        {
            var checkboxes = Grid.FindElements(By.CssSelector(".slick-header input[type=checkbox]"));
            var checkbox = checkboxes.SingleDisplayed("grid header checkbox");
            checkbox.Click();
        }
        protected override ReadOnlyCollection<IWebElement> GetColumnHeaders()
        {
            return Grid.FindElements(By.ClassName("slick-header-column"));
        }
        private void ExpandPagerSettings(IWebElement pagerSettingsElement)
        {
            var expandedContents = pagerSettingsElement.FindElement(By.ClassName("slick-pager-settings-expanded"));
            if (!expandedContents.Displayed)
            {
                var expandButton = pagerSettingsElement.FindElement(By.ClassName("ui-icon-container"));
                Browser.Wait(TimeSpan.FromSeconds(5), () => expandButton.Displayed,
                    "Waiting for pager-settings expand button to show");
                expandButton.Click();
            }
        }
        public override int RecordCount()
        {
            ShowAllRows();

            var pagerStatus = Pager.FindElement(By.ClassName("slick-pager-status"), TimeSpan.FromSeconds(5));
            var pagerStatusText = pagerStatus.Text;
            var match = Regex.Match(pagerStatusText, @"^Showing all (\d+) rows$");
            if (!match.Success)
            {
                throw new InvalidOperationException("Could not parse pager status text: " + pagerStatusText);
            }

            return int.Parse(match.Groups[1].Value);
        }
        private void ShowAllRows()
        {
            if (_showingAllRows)
                return;

            var pagerSettingsElement = Pager.FindElement(By.ClassName("slick-pager-settings"), TimeSpan.FromSeconds(5));
            ExpandPagerSettings(pagerSettingsElement);
            SetRowsPerPage(pagerSettingsElement, "All");

            _showingAllRows = true;
        }
        private void SetRowsPerPage(IWebElement pagerSettingsElement, string selection)
        {
            pagerSettingsElement.FindElement(By.LinkText(selection), TimeSpan.FromSeconds(5)).Click();
        }
    }
}