﻿using NUnit.Framework;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public class AdvancedSearchResultsPage : UiObject
    {
        private SimpleGrid _grid;

        public SimpleGrid Grid
        {
            get
            {
                if (_grid == null)
                {
                    Assert.That(IsAt(), Is.True, "Expected to be on the Search Results page");
                    _grid = new SimpleGrid(Browser.FindElement(By.CssSelector("table.grid")));
                }
                return _grid;
            }
        }

        public bool IsAt()
        {
            return Browser.Title == "Search";
        }
    }
}