﻿using System;
using System.Linq;
using JetBrains.Annotations;
using NUnit.Framework;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.Members)]
    public class PageNavigation : UiObject
    {
        private void ClickSubMenuItem(string parentText, string childText)
        {
            var menuBar = Browser.FindElement(By.Id("recTabs"), TimeSpan.FromSeconds(5));
            var menuItems = menuBar.FindElements(By.TagName("li"));
            var parentMenuItem = menuItems.First(menuItem => menuItem.Text == parentText);
            parentMenuItem.Click();

            var childMenuItem = parentMenuItem.FindElement(By.LinkText(childText));
            childMenuItem.Click();
        }
        /// <summary>
        /// Gets the page location string. This is mainly based on panel headers and
        /// the selected tab (if there's a tab strip), plus extra logic to resolve ambiguities.
        /// The return values correspond to constants in <see cref="PageLocations"/>.
        /// </summary>
        public string GetPageLocation()
        {
            var headerElements = Browser.FindElements(By.CssSelector(".portlet-text,#TabMenu>.active"));
            var headers = string.Join("/", headerElements.Select(element => element.Text));

            if (headers == PageLocations.AdvancedSearch)
            {
                // Advanced Search Results has an .account-heading span, Advanced Search doesn't
                var accountHeadings = Browser.FindElements(By.ClassName("account-heading"));
                if (accountHeadings.Any())
                {
                    headers = PageLocations.AdvancedSearchResults;
                }
            }
            return headers;
        }
        public void GoToAdvancedSearch()
        {
            ClickSubMenuItem("Search", "Advanced Search");
            Browser.ExpectSpinners();
        }
        public void GoToUserManagement()
        {
            ClickSubMenuItem("Admin", "Users");
            Browser.ExpectSpinners();
        }
        public void ShouldBeAt(string expectedPageLocation)
        {
            var actualPageLocation = GetPageLocation();
            Assert.That(actualPageLocation, Is.EqualTo(expectedPageLocation), "Page location");
        }        
        public void GoToBankMaintenance()
        {
            ClickSubMenuItem("Admin", "Banks");
            Browser.ExpectSpinners();
        }
    }
}