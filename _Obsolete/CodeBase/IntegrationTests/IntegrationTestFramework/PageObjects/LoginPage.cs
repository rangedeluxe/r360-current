﻿using System;
using JetBrains.Annotations;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.WithMembers)]
    public class LoginPage : UiObject
    {
        public void GoTo()
        {
            Browser.GoTo(Configuration.InitialUrl);
        }
        public void LogIn(string entity, string userName, string password)
        {
            Browser.FindElement(By.Id("Entity"), TimeSpan.FromSeconds(30)).SendKeys(entity);
            Browser.FindElement(By.Id("UserName")).SendKeys(userName);
            Browser.FindElement(By.Id("Password")).SendKeys(password);
            Browser.FindElement(By.ClassName("saveButton")).Click();

            Browser.ExpectSpinners(new Spinners(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(5)));
        }
    }
}