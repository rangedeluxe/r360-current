﻿using System;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public class UserManagementPage : UiObject
    {
        public GridBase Grid { get; private set; }

        public UserManagementPage()
        {
            Grid = new SlickGrid(Browser.FindElement(By.Id("userGrid"), TimeSpan.FromSeconds(10)));
        }
    }
}