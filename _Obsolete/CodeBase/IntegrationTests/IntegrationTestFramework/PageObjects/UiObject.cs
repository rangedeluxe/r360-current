﻿using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public class UiObject
    {
        internal Browser Browser
        {
            get { return Ui.Browser; }
        }
        public string ErrorText
        {
            get
            {
                var errors = Browser.FindElements(By.ClassName("toast-type-error"))
                    .Select(toastItem => toastItem.Text)
                    .ToList();
                return string.Join("\r\n", errors);
            }
        }

        public void CheckForErrorBoxes()
        {
            Assert.That(ErrorText, Is.EqualTo(""), "Error toasts");
        }
    }
}