using System;
using System.Collections.ObjectModel;
using JetBrains.Annotations;
using NUnit.Framework;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public abstract class GridBase : UiObject
    {
        protected GridBase(IWebElement grid)
        {
            Grid = grid;
        }

        protected IWebElement Grid { get; private set; }

        public abstract void ClickCheckBoxInGridHeader();
        public void ClickColumnHeader(string headerText)
        {
            GetColumnHeaderCell(headerText).Click();
        }
        protected abstract ReadOnlyCollection<IWebElement> GetColumnHeaders();
        [NotNull]
        private IWebElement GetColumnHeaderCell(string headerText)
        {
            return GetColumnHeaderCellAndIndex(headerText).Item1;
        }
        [NotNull]
        private Tuple<IWebElement, int> GetColumnHeaderCellAndIndex(string headerText)
        {
            var columnAndIndex = TryGetColumnHeaderCellAndIndex(headerText);
            if (columnAndIndex == null)
                Assert.Fail("Could not find column with header text " + headerText);
            return columnAndIndex;
        }
        public abstract int RecordCount();
        [CanBeNull]
        private Tuple<IWebElement, int> TryGetColumnHeaderCellAndIndex(string headerText)
        {
            var headers = GetColumnHeaders();
            for (var i = 0; i < headers.Count; i++)
            {
                var header = headers[i];
                if (header.Text.Trim().Equals(headerText.Trim(), StringComparison.InvariantCultureIgnoreCase))
                    return Tuple.Create(header, i);
            }
            return null;
        }
        protected int? TryGetColumnIndex(string headerText)
        {
            var columnAndIndex = TryGetColumnHeaderCellAndIndex(headerText);
            return columnAndIndex != null ? columnAndIndex.Item2 : (int?) null;
        }
    }
}