using System;
using System.Linq;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    public class FilterableComboBox : FilterableComboBoxBase<FilterableComboBox.StringSelection>
    {
        public class StringSelection : ISelection
        {
            public StringSelection(string filterText)
            {
                FilterText = filterText;
            }

            public string FilterText { get; private set; }

            public bool MatchesSelectorText(string text)
            {
                return FilterText == text;
            }
        }

        public FilterableComboBox(IWebElement selector)
            : base(selector)
        {
        }

        protected override bool IsDroppedDown
        {
            get { return Browser.FindElements(By.Id("select2-drop")).Any(); }
        }
        protected override StringSelection CreateSelection(string selection)
        {
            return new StringSelection(selection);
        }
        protected override void EnterFilterMode()
        {
            Selector.Click();
            Browser.Wait(TimeSpan.FromSeconds(5), () => IsDroppedDown, "Waiting for combo box to drop down");
        }
        protected override bool FilterResultMatches(IWebElement filterResult, StringSelection selection)
        {
            return filterResult.Text == selection.FilterText;
        }
    }
}