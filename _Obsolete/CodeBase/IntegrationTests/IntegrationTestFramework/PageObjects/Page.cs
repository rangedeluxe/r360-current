﻿using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace IntegrationTestFramework.PageObjects
{
    public class Page : UiObject
    {
        protected virtual bool EntitySelectionCausesSpinner
        {
            get { return true; }
        }
        public GridBase Grid
        {
            get
            {
                var allGrids = ModalRoot.FindElements(By.CssSelector("table.grid,.grid-container"));
                if (allGrids.Count <= 0)
                {
                    allGrids = ModalRoot.FindElements(By.ClassName("slickStyle"));
                }

                var grid = allGrids.SingleDisplayed("grid");
                if (grid.HasClass("grid-container") || grid.HasClass(("slickStyle")))
                {
                    //the pager is sometimes the grid's sibling 
                    //and not inside the grid
                    IWebElement pager = null;
                    try
                    {
                        pager = grid.FindElement(By.ClassName("slick-pager"), TimeSpan.FromSeconds(5));
                    }
                    catch (WebDriverTimeoutException e)
                    {
                        
                        pager = grid.FindElement(By.XPath("following-sibling::*"), TimeSpan.FromSeconds(5));
                    }
                    
                    return new SlickGrid(grid, pager);
                }
                return new SimpleGrid(grid);
            }
        }
        public IWebElement ModalRoot
        {
            get
            {
                var allModalDialogs = Browser.FindElements(By.ClassName("modal-dialog"));
                var dialog = allModalDialogs.SingleDisplayedOrDefault("modal dialog");
                return dialog ?? Browser.FindElement(By.TagName("body"));
            }
        }

        public void ClickButton(string text, bool includeLinks = false)
        {
            // When we show and hide modal dialogs, buttons can get destroyed and
            // re-created. (Maybe a Knockout thing?) If this happens, just retry.
            Browser.Wait(TimeSpan.FromSeconds(10), () =>
            {
                try
                {
                    var button = GetButton(text, includeLinks);
                    if (button.GetAttribute("disabled") != null)
                    {
                        Assert.Fail("Cannot click " + text + " button because it is disabled");
                    }
                    button.Click();
                    return true;
                }
                catch (StaleElementReferenceException)
                {
                    return false; // retry
                }
            }, "Too many StaleElementReferenceExceptions looking for button " + text);
        }

        private IWebElement GetButton(string text, bool includeLinks = false)
        {
            var buttons = ModalRoot.FindElements(By.TagName("button")).ToList();
            if (includeLinks)
            {
                var links = ModalRoot.FindElements(By.TagName("a"));
                buttons = buttons.Concat(links).ToList(); 
            }

            var matchingButtons = buttons.Where(b =>
                b.Text.Trim().Equals(text.Trim(), StringComparison.InvariantCultureIgnoreCase));

            var button = matchingButtons.SingleDisplayed(text + " button");
            return button;
        }

        public void SetTextBoxText(string text, string textboxLabel)
        {
            var matchingTextBoxes = ModalRoot.FindElements(By.XPath( string.Format("//label[contains(.,'{0}')]/following::input[1]", textboxLabel)));
            var textBox = matchingTextBoxes.SingleDisplayed(textboxLabel + " textbox");
            textBox.Clear();
            textBox.SendKeys(text);
        }

        private IWebElement GetEntitySelectorElement()
        {
            var elements = Browser.FindElements(By.CssSelector("#entityTree,#treeBase"), TimeSpan.FromSeconds(10));
            if (elements.Count == 0)
                Assert.Fail("Could not find any entity or workgroup selectors on the page");
            if (elements.Count > 1)
                Assert.Fail("Too many entity or workgroup selectors");
            return elements.Single();
        }
        public void SelectEntityOrWorkgroup(string entityPath)
        {
            var entitySelectorElement = GetEntitySelectorElement();
            var entitySelector = new WorkgroupSelector(entitySelectorElement);
            entitySelector.Select(entityPath);
            CheckForErrorBoxes();

            if (EntitySelectionCausesSpinner)
            {
                Browser.ExpectSpinners();
            }
        }

        public void ModalWithLegendHasLoaded(string legendText)
        {
            Browser.ExpectSpinners();
            var legend = ModalRoot.FindElements(By.TagName("Legend"));
            Assert.That(legend.SingleDisplayed("Legend").Text, Is.EqualTo(legendText),
                "Modal not found");
        }
    }
}