﻿using JetBrains.Annotations;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace IntegrationTestFramework.PageObjects
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.WithMembers)]
    public class ReceivablesSummaryPage : UiObject
    {
        private string _batchResultsTableID = "batchResultsTable";
        private string _ascendingSortIconClass = "sorting_asc";
        private string _descendingSortIconClass = "sorting_desc";
        private string _disabled = "sorting_disabled";
        private string _dimmedSortIconClass = "sorting";

        public void ShouldBeAt()
        {
            Assert.That(Browser.Title,Is.EqualTo("RECEIVABLES 360"), "Browser Title");
            //Assert.That(Browser.Title, Is.EqualTo("Dev-R360"), "Browser title");
        }

        public void HasHeaderWithDescendingSortIcon(string columnHeader)
        {
            var headers = GetReceivablesSummaryHeaderRow();
            var sortedCol = headers.First(header => header.Text == columnHeader);
            Assert.That(sortedCol.HasClass(_descendingSortIconClass), Is.True, "Header doesn't have desc icon");
        }

        public void HasHeaderWithAscendingSortIcon(string columnHeader)
        {
            var headers = GetReceivablesSummaryHeaderRow();
            var sortedCol = headers.First(header => header.Text == columnHeader);
            Assert.That(sortedCol.HasClass(_ascendingSortIconClass), Is.True, "Header doesn't have asc icon");
        }

        public void AllHeadersHaveDimmedSortIconExcept(string column)
        {
            var headers = GetReceivablesSummaryHeaderRow();
            var headerWithIcon = headers.Where(header => header.Text != column && header.HasClass(_dimmedSortIconClass));
            Assert.That(headerWithIcon.Count(), Is.EqualTo(headers.Count - 1), "Header count without sort icon ");
        }

        public void SortByColumn(string columnHeader)
        {
            var headers = GetReceivablesSummaryHeaderRow();
            var headerElement = headers.First(header => header.Text == columnHeader);
            headerElement.Click();
        }

        private ReadOnlyCollection<IWebElement> GetReceivablesSummaryHeaderRow()
        {
            return Browser.FindElement(By.Id(_batchResultsTableID), TimeSpan.FromSeconds(5)).
                FindElement(By.TagName("thead")).FindElement(By.TagName("tr")).FindElements(By.TagName("th"));
        }
    }
}
