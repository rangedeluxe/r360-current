﻿using System;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    /// <summary>
    /// For simple grids, like Advanced Search results, that don't use either SlickGrid or DataGrid.
    /// </summary>
    public class SimpleGrid : GridBase
    {
        private static string _classNameGridHeader = ".grid-header";
        private static string _classNameGridPager = "grid-pager";
        private static string _classNameGridCell = ".grid-cell";
        private static string _classNameRows = ".oddrow,.evenrow";

        public SimpleGrid(IWebElement grid) : base(grid)
        {
        }

        public override void ClickCheckBoxInGridHeader()
        {
            var selector = "." + _classNameGridPager + " input[type=checkbox]";
            var checkboxes = Grid.FindElements(By.CssSelector(selector));
            var checkbox = checkboxes.SingleDisplayed("grid header checkbox");
            checkbox.Click();
        }
        public override int RecordCount()
        {
            int count;

            // Get the Pager Bar and find the Record count string
            var pager = Grid.FindElement(By.ClassName(_classNameGridPager));
            var spans = pager.FindElements(By.TagName("span"));
            var textData = spans[1].Text;
            // grab the text after the last space, should be the record count
            var total = textData.Substring(textData.LastIndexOf(new String(' ', 1), StringComparison.Ordinal));

            // Make sure it can be cast to an int
            Int32.TryParse(total, out count);
            return count;
        }

        private bool CanParseDouble(string value)
        {
            return TryParseDouble(value).HasValue;
        }
        public bool ColumnDataEqualTo(string columnName, string columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => cols[(int)i].Text == columnValue);
        }

        public bool ColumnDataEqualTo(string columnName, decimal columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => Convert.ToDecimal(cols[(int)i].Text.Remove(0,1)) == columnValue);
        }

        public bool ColumnDataEqualTo(string columnName, float columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => Math.Abs(Convert.ToSingle(cols[(int)i].Text) - columnValue) < 2);
        }

        public bool ColumnDataEqualTo(string columnName, DateTime columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => Convert.ToDateTime(cols[(int)i].Text, new CultureInfo("en-US")) == columnValue);
        }

        public bool ColumnDataContains(string columnName, string columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => cols[(int)i].Text.Contains(columnValue));
        }

        public bool ColumnDataBeginsWith(string columnName, string columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => cols[(int)i].Text.StartsWith(columnValue));
        }

        public bool ColumnDataEndsWith(string columnName, string columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return
                rows.Select(row => row.FindElements(By.CssSelector(_classNameGridCell)))
                    .All(cols => cols[(int)i].Text.EndsWith(columnValue));
        }

        public bool ColumnDataGreaterThan(string columnName, int columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();


            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue > Convert.ToInt32(cols[(int)i].Text));
        }

        public bool ColumnDataLessThan(string columnName, int columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();


            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue < Convert.ToInt32(cols[(int)i].Text));
        }

        public bool ColumnDataGreaterThan(string columnName, long columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue > Convert.ToInt64(cols[(int)i].Text));
        }

        public bool ColumnDataLessThan(string columnName, long columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue < Convert.ToInt64(cols[(int)i].Text));
        }

        public bool ColumnDataGreaterThan(string columnName, float columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue > Convert.ToSingle(cols[(int)i].Text));
        }

        public bool ColumnDataLessThan(string columnName, float columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue < Convert.ToSingle(cols[(int)i].Text));
        }

        public bool ColumnDataGreaterThan(string columnName, decimal columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue > Convert.ToDecimal(cols[(int)i].Text.Remove(0, 1)));
        }

        public bool ColumnDataLessThan(string columnName, decimal columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue < Convert.ToDecimal(cols[(int)i].Text.Remove(0, 1)));
        }

        public bool ColumnDataGreaterThan(string columnName, DateTime columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue > Convert.ToDateTime(cols[(int)i].Text,  new CultureInfo("en-US")));
        }
        public bool ColumnDataLessThan(string columnName, DateTime columnValue)
        {
            var i = TryGetColumnIndex(columnName);
            if (!i.HasValue) return false;
            var rows = GetRows();
            return !(from row in rows select row.FindElements(By.CssSelector(_classNameGridCell)) into cols from col in cols select cols).Any(cols => columnValue < Convert.ToDateTime(cols[(int)i].Text, new CultureInfo("en-US")));
        }
        public void ExpectEveryCellInColumn(string columnName, string comparison, string value)
        {
            switch (comparison.ToLowerInvariant())
            {
                case "equals":
                case "equal to":
                {
                    var valueIsNumeric = CanParseDouble(value);
                    foreach (var cell in GetCellsInColumn(columnName))
                    {
                        if (valueIsNumeric && CanParseDouble(cell.Text))
                        {
                            Assert.That(
                                ParseDouble(value),
                                Is.EqualTo(ParseDouble(cell.Text)),
                                $"Cell in column '{columnName}'");
                        }
                        else
                        {
                            Assert.That(value, Is.EqualTo(cell.Text), $"Cell in column '{columnName}'");
                        }
                    }
                    break;
                }
                case "greater than":
                case "is greater than":
                    ExpectEveryNumberInColumn(columnName, Is.GreaterThan(ParseDouble(value)));
                    break;
                case "less than":
                case "is less than":
                    ExpectEveryNumberInColumn(columnName, Is.LessThan(ParseDouble(value)));
                    break;
                case "begins with":
                    ExpectEveryStringInColumn(columnName, Is.StringStarting(value));
                    break;
                case "contains":
                    ExpectEveryStringInColumn(columnName, Is.StringContaining(value));
                    break;
                case "ends with":
                    ExpectEveryStringInColumn(columnName, Is.StringEnding(value));
                    break;
                default:
                    Assert.Fail("Unrecognized comparison operator '{0}'", comparison);
                    return;
            }
        }
        private void ExpectEveryCellInColumn(string columnName, Func<string, object> transformCellValue, IResolveConstraint expectation)
        {
            var cells = GetCellsInColumn(columnName);
            foreach (var cell in cells)
            {
                var cellValue = transformCellValue(cell.Text);
                Assert.That(cellValue, expectation, "Cell in column '" + columnName + "'");
            }
        }
        private void ExpectEveryNumberInColumn(string columnName, IResolveConstraint expectation)
        {
            ExpectEveryCellInColumn(columnName, cell => ParseDouble(cell), expectation);
        }
        private void ExpectEveryStringInColumn(string columnName, IResolveConstraint expectation)
        {
            ExpectEveryCellInColumn(columnName, cell => cell, expectation);
        }
        private ReadOnlyCollection<IWebElement> GetCellsInColumn(string columnName)
        {
            var columnIndex = TryGetColumnIndex(columnName);
            if (columnIndex == null)
            {
                return new ReadOnlyCollection<IWebElement>(new IWebElement[] {});
            }

            var cssSelector = string.Format("{0}:nth-child({1})", _classNameGridCell, columnIndex + 1);
            var cells = Grid.FindElements(By.CssSelector(cssSelector));
            if (!cells.Any())
            {
                Assert.Fail("Could not find any cells in column '" + columnName + "'");
            }
            return cells;
        }
        protected override ReadOnlyCollection<IWebElement> GetColumnHeaders()
        {
            return Grid.FindElements(By.CssSelector(_classNameGridHeader));
        }
        private ReadOnlyCollection<IWebElement> GetRows()
        {
            return Grid.FindElements(By.CssSelector(_classNameRows));
        }
        private double ParseDouble(string value)
        {
            var result = TryParseDouble(value);
            if (result == null)
            {
                var message = string.Format("Could not convert '{0}' to a number", value);
                throw new InvalidOperationException(message);
            }
            return result.Value;
        }
        private double? TryParseDouble(string value)
        {
            var valueToParse = value.Replace("$", "");

            double result;
            var success = double.TryParse(valueToParse, out result);
            return success ? result : (double?) null;
        }
    }
}
