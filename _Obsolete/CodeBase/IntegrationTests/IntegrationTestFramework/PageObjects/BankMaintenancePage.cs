﻿using JetBrains.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium;

namespace IntegrationTestFramework.PageObjects
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.WithMembers)]
    public class BankMaintenancePage : Page
    {

        public BankMaintenancePage() { }

        public int GetRowCount()
        {
            Browser.ExpectSpinners();
            return Grid.RecordCount();
        }

        public void SetEntity(string entity)
        {
            //can't use id, dropdown generated on the fly
            var comboBox = new FilterableComboBox(Browser.FindElement(By.ClassName("select2-container")));
            comboBox.Select(entity);
            CheckForErrorBoxes();
        }

        public bool BankHasName(string bankName)
        {
            Browser.ExpectSpinners();
            return ((SlickGrid)Grid).HasCellWithText("Name", bankName);
        }

        public void ClickUpdateButtonOnGrid(string content, string gridHeader)
        {
            ((SlickGrid)Grid).ClickUpdateButtonOnRow(gridHeader, content);
            Browser.ExpectSpinners();
        }
    }
}
