﻿using System.Linq;
using JetBrains.Annotations;
using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Support.UI;

namespace IntegrationTestFramework.PageObjects
{
    [UsedImplicitly(ImplicitUseKindFlags.Assign, ImplicitUseTargetFlags.WithMembers)]
    public class AdvancedSearchPage : Page
    {
        protected override bool EntitySelectionCausesSpinner
        {
            get { return false; }
        }

        public void ClickSearch()
        {
            var searchButton = Browser.FindElement(By.Name("cmdSearch"));
            searchButton.Click();
            CheckForErrorBoxes();
        }
        private IWebElement GetDatePickerTextBox(string datePickerId)
        {
            var datePicker = Browser.FindElement(By.Id(datePickerId));
            return datePicker.FindElement(By.TagName("input"));
        }
        private IWebElement GetDatePickerCalendar(string datePickerId)
        {
            var start = Browser.FindElement(By.Id(datePickerId));
            start.FindElement(By.TagName("button")).Click();
            return Browser.FindElement(By.Id("ui-datepicker-div"));
        }

        private IWebElement GetPaymentTypeTextBox(string paymentTypeId)
        {
            var ddList = Browser.FindElement(By.Id(paymentTypeId));
            return ddList.FindElement(By.TagName("input"));
        }
        public void GoTo()
        {
            Browser.GoTo(Configuration.AdvancedSearchUrl);
            Browser.ExpectSpinners();
        }
        public bool HasNoResults()
        {
            var errorDetailElement = Browser
                .FindElements(By.ClassName("errordetail"), TimeSpan.FromSeconds(5))
                .FirstOrDefault();
            return
                errorDetailElement != null &&
                errorDetailElement.Text == "No records were found matching the criteria specified.";
        }
        public bool IsAt()
        {
            return Browser.Title == "Advanced Search";
        }
        public void SetDepositDate(string startDate, string endDate = null)
        {
            // GetDatePickerTextBox("StartDatePicker").SelectAll().SendKeys(startDate);
            // GetDatePickerTextBox("EndDatePicker").SelectAll().SendKeys(endDate ?? startDate);
            endDate = endDate ?? startDate;

            var startCal = GetDatePickerCalendar("StartDatePicker");
            DatePicker.SelectDate(startCal, startDate);

            var endCal = GetDatePickerCalendar("EndDatePicker");
            DatePicker.SelectDate(endCal, endDate);

            CheckForErrorBoxes();
        }
        public void SetPaymentSource(string paymentSource)
        {
            var comboBox = new FilterableComboBox(Browser.FindElement(By.Id("s2id_PaymentSourceSelect")));
            comboBox.Select(paymentSource);

            CheckForErrorBoxes();
        }
        public void SetPaymentType(string paymentType)
        {
            var comboBox = new FilterableComboBox(Browser.FindElement(By.Id("s2id_PaymentTypeSelect")));
            comboBox.Select(paymentType);

            CheckForErrorBoxes();
        }

        public void SetAdvancedFind(int idx, string name, string op, string val)
        {
            var selector = Browser.FindElement(By.Name("selField" + idx));
            selector.SelectAll().SendKeys(name);

            selector = Browser.FindElement(By.Name("selOperator" + idx));
            selector.SelectAll().SendKeys(op);

            selector = Browser.FindElement(By.Name("searchFieldValue" + idx));
            selector.SelectAll().SendKeys(val);
        }

        public void SelectDisplayField(string name)
        {
            var element = Browser.FindElement(By.Name("selAvailable"));
            Browser.ClearListBoxSelections(element);

            var selector = new SelectElement(element);
            selector.SelectByText(name);

            var button  = Browser.FindElement(By.Name("cmdMoveSingleAvailable"));
            button.Click();
        }
    }
}