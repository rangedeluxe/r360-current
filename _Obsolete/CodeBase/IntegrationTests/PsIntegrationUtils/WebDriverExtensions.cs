﻿using System;
using System.Linq;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace PsIntegrationUtils
{
    public static class WebDriverExtensions
    {
        public static object ExecuteJavaScript(this IWebDriver driver, string script, params object[] args)
        {
            var javaScriptExecutor = (IJavaScriptExecutor) driver;
            return javaScriptExecutor.ExecuteScript(script, args);
        }
        /// <summary>
        /// Waits until the given condition returns a Boolean true or a non-null reference.
        /// Throws if the timeout expires and the condition still hasn't succeeded.
        /// </summary>
        /// <typeparam name="T">Either System.Bool or a reference type.</typeparam>
        /// <param name="driver">The <see cref="IWebDriver"/> instance.</param>
        /// <param name="timeout">How long to wait.</param>
        /// <param name="condition">
        /// The condition to wait for. The function waits until this returns
        /// true or non-null, or <see cref="timeout"/> has elapsed.
        /// </param>
        /// <param name="message">Text to be included in the exception message if we time out.</param>
        /// <returns>The first true or non-null return value from the <see cref="condition"/> callback.</returns>
        /// <exception cref="WebDriverTimeoutException">
        /// If the timeout elapses and <see cref="condition"/> hasn't yet returned
        /// true or non-null.
        /// </exception>
        public static T Wait<T>(this IWebDriver driver, TimeSpan timeout, Func<T> condition, string message)
        {
            var wait = new WebDriverWait(driver, timeout) { Message = message };
            return wait.Until(d => condition());
        }
        public static void WaitForDashboardSpinners(this IWebDriver driver)
        {
            driver.WaitForSpinner(TimeSpan.FromSeconds(15), TimeSpan.FromSeconds(5));
        }
        public static void WaitForSpinner(this IWebDriver driver, TimeSpan? initialWait = null, TimeSpan? quietPeriod = null)
        {
            var expiration = DateTime.Now + (initialWait ?? TimeSpan.FromSeconds(15));
            while (true)
            {
                Thread.Sleep(250);

                var spinners = driver.FindElements(By.ClassName("spinDiv"));
                if (spinners.Any())
                {
                    expiration = DateTime.Now + (quietPeriod ?? TimeSpan.FromSeconds(1));
                }
                else if (DateTime.Now > expiration)
                {
                    break;
                }
            }
        }
    }
}