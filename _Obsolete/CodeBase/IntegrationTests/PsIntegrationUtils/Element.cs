﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using JetBrains.Annotations;
using OpenQA.Selenium;

namespace PsIntegrationUtils
{
    public class Element
    {
        public Element(Config config, IWebDriver webDriver, IWebElement webElement)
        {
            Config = config;
            WebDriver = webDriver;
            WebElement = webElement;

            StyleOverride = new ElementStyleOverride(webDriver, webElement);
        }

        private Config Config { get; set; }
        private ElementStyleOverride StyleOverride { get; set; }
        private IWebDriver WebDriver { get; set; }
        private IWebElement WebElement { get; set; }
        private string YouAreHere
        {
            get
            {
                if (WebElement.TagName.Equals("body", StringComparison.InvariantCultureIgnoreCase))
                    return "";
                return ToString() + ": ";
            }
        }

        private event EventHandler BeforeClick = delegate { };

        [PublicAPI]
        public Element Button(string buttonText, FindElementOptions options = FindElementOptions.None)
        {
            var trueButton = ".//button" + "[normalize-space(.)=" + QuoteForXPath(buttonText) + "]";
            var inputButton = ".//input[@type='button'][@value=" + QuoteForXPath(buttonText) + "]";
            return XPath(trueButton + "|" + inputButton, options);
        }
        [PublicAPI]
        public IReadOnlyList<Element> Buttons(string buttonText, FindElementOptions options = FindElementOptions.None)
        {
            var trueButton = ".//button" + "[normalize-space(.)=" + QuoteForXPath(buttonText) + "]";
            var inputButton = ".//input[@type='button'][@value=" + QuoteForXPath(buttonText) + "]";
            return XPaths(trueButton + "|" + inputButton, options);
        }
        [PublicAPI]
        public Element Class(string className, FindElementOptions options = FindElementOptions.None)
        {
            return FindElement(By.ClassName(className), options);
        }
        [PublicAPI]
        public void Click()
        {
            BeforeClick(this, EventArgs.Empty);
            WebElement.Click();
        }
        private Element CreateElement(IWebElement webElement)
        {
            return new Element(Config, WebDriver, webElement);
        }
        [PublicAPI]
        public Element Css(string css, FindElementOptions options = FindElementOptions.None)
        {
            return FindElement(By.CssSelector(css), options);
        }
        [PublicAPI]
        public IReadOnlyList<Element> Csses(string css,
            FindElementOptions options = FindElementOptions.None)
        {
            return FindAll(By.CssSelector(css), options);
        }
        [PublicAPI]
        public void Enter(string value)
        {
            WebElement.SendKeys(value);
        }
        private IReadOnlyList<Element> FindAll(By by, FindElementOptions options)
        {
            var webElements = FindWebElements(by, options);
            return webElements.Select(CreateElement).ToList();
        }
        private Element FindElement(By by, FindElementOptions options)
        {
            var elements = FindWebElements(by, options);
            if (elements.Count > 1)
            {
                var message =
                    YouAreHere + "Found more than one \"" + by + "\"\r\n" +
                    string.Join("\r\n", elements.Select(element => element.GetPreviewText()));
                throw new TestFailureException(message);
            }
            if (elements.Count == 0)
            {
                if (options.HasFlag(FindElementOptions.Optional))
                    return null;
                throw new TestFailureException(YouAreHere + "Did not find any \"" + by + "\"");
            }
            return CreateElement(elements.Single());
        }
        /// <summary>
        /// Returns a list of matching elements. If we're in a running test (instead
        /// of interactive mode), wait the default timeout for any elements to appear.
        /// If the timeout elapses, returns an empty list.
        /// </summary>
        /// <param name="by">The locating mechanism to use.</param>
        /// <param name="options">Options that affect the search, e.g. whether to include hidden elements.</param>
        /// <returns>A list of matching elements, or an empty list if the timeout expires.</returns>
        private IReadOnlyList<IWebElement> FindWebElements(By by, FindElementOptions options)
        {
            var endTime = DateTime.Now + Config.ElementTimeout;
            do
            {
                var elementQuery = WebElement.FindElements(by).AsEnumerable();
                if (!options.HasFlag(FindElementOptions.IncludeHidden))
                {
                    elementQuery = elementQuery.Where(e => e.Displayed);
                }

                var elements = elementQuery.ToList();
                if (elements.Any())
                {
                    return elements;
                }

                if (options.HasFlag(FindElementOptions.NoWait))
                {
                    break;
                }

                Thread.Sleep(TimeSpan.FromMilliseconds(500));
            } while (DateTime.Now <= endTime);

            return new IWebElement[] {};
        }
        [PublicAPI]
        public string GetAttribute(string attributeName)
        {
            return WebElement.GetAttribute(attributeName);
        }
        public bool HasClass(string className)
        {
            return WebElement.HasClass(className);
        }
        [PublicAPI]
        public Element Id(string id, FindElementOptions options = FindElementOptions.None)
        {
            return FindElement(By.Id(id), options);
        }
        [PublicAPI]
        public bool IsEnabled()
        {
            return WebElement.Enabled;
        }
        [PublicAPI]
        public Element Labeled(string labelText, FindElementOptions options = FindElementOptions.None)
        {
            // Using <label for="...">
            var forHtmlLabel =
                ".//*[@id=//label[.=" + QuoteForXPath(labelText) + "]/@for]";
            // Using hubLabel and formfield, e.g. Advanced Search
            var forHubLabel =
                ".//td[@class='hubLabel'][starts-with(normalize-space(.)," + QuoteForXPath(labelText) + ")]" +
                "/following-sibling::td[1]";

            return XPath(forHtmlLabel + "|" + forHubLabel);
        }
        [PublicAPI]
        public Element MenuItem(string menuItemText, string subMenuItemText = null)
        {
            var menuItem = XPath(".//nav/ul/li[a=" + QuoteForXPath(menuItemText) + "]");

            if (string.IsNullOrEmpty(subMenuItemText))
                return menuItem;

            var subMenuItem = menuItem.FindElement(
                By.XPath("./ul/li[a=" + QuoteForXPath(subMenuItemText) + "]"),
                FindElementOptions.IncludeHidden);
            subMenuItem.BeforeClick += (sender, e) =>
            {
                if (!subMenuItem.WebElement.Displayed)
                    menuItem.Click();
            };
            return subMenuItem;
        }
        [PublicAPI]
        public void OverrideStyle(string name, string value)
        {
            StyleOverride.SetStyle(name, value);
        }
        [PublicAPI]
        public string PageTitle()
        {
            var csses = new[]
            {
                // Panel titles, not including supplementary text like the
                // "(Showing All Entities)" after "Receivables Summary"
                ".portlet-text",

                // Dialog title
                ".modal-title",

                // Active tab
                ".nav-tabs li.active",
            };
            var elements = Csses(string.Join(",", csses));
            return string.Join(" / ", elements.Select(element => element.Text()));
        }
        private string QuoteForXPath(string value)
        {
            var hasSingleQuote = value.Contains("'");
            var hasDoubleQuote = value.Contains("\"");
            if (hasSingleQuote && hasDoubleQuote)
            {
                return "concat('" + value.Replace("'", "', \"'\"', '") + "')";
            }
            return hasSingleQuote
                ? "\"" + value + "\""
                : "'" + value + "'";
        }
        [PublicAPI]
        public string Text()
        {
            return WebElement.Text;
        }
        public override string ToString()
        {
            return WebElement.GetPreviewText();
        }
        [PublicAPI]
        public Element XPath(string xpath, FindElementOptions options = FindElementOptions.None)
        {
            return FindElement(By.XPath(xpath), options);
        }
        [PublicAPI]
        public IReadOnlyList<Element> XPaths(string xpath,
            FindElementOptions options = FindElementOptions.None)
        {
            return FindAll(By.XPath(xpath), options);
        }
    }
}