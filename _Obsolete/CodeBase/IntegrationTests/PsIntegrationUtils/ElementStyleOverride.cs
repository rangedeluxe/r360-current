﻿using System;
using System.Collections.Generic;
using OpenQA.Selenium;

namespace PsIntegrationUtils
{
    public class ElementStyleOverride
    {
        private readonly IWebDriver _driver;
        private readonly IWebElement _element;
        private bool _isOriginalStyleInitialized;
        private string _originalStyle;
        private readonly IDictionary<string, string> _overriddenStyles;

        public ElementStyleOverride(IWebDriver driver, IWebElement element)
        {
            _driver = driver;
            _element = element;
            _overriddenStyles = new Dictionary<string, string>(StringComparer.InvariantCultureIgnoreCase);
        }

        public void SetStyle(string name, string value)
        {
            if (!_isOriginalStyleInitialized)
            {
                _originalStyle = _element.GetAttribute("style");
                _isOriginalStyleInitialized = true;
            }

            if (string.IsNullOrEmpty(value))
                _overriddenStyles.Remove(name);
            else
                _overriddenStyles[name] = value;

            var newStyle = _originalStyle;
            foreach (var pair in _overriddenStyles)
            {
                newStyle += ";" + pair.Key + ":" + pair.Value;
            }
            _element.SetAttribute(_driver, "style", newStyle);
        }
    }
}