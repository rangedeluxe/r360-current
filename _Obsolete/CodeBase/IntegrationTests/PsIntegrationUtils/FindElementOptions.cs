using System;

namespace PsIntegrationUtils
{
    [Flags]
    public enum FindElementOptions
    {
        None = 0,
        /// <summary>
        /// Includes hidden elements in the search. By default, hidden elements are filtered out.
        /// </summary>
        IncludeHidden = 1,
        /// <summary>
        /// Returns null if the element is not found. This will still wait the default timeout
        /// for the element to appear, unless otherwise specified (e.g. by <see cref="NoWait"/>.
        /// </summary>
        Optional = 2,
        /// <summary>
        /// Skips the default timeout; returns immediately if the element cannot be found.
        /// </summary>
        NoWait = 4,
    }
}