﻿using System;

namespace PsIntegrationUtils
{
    public class TestFailureException : Exception
    {
        public TestFailureException(string message) : base(message)
        {
        }
        public TestFailureException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}