﻿using System;
using System.Collections;
using JetBrains.Annotations;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Edge;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;
using OpenQA.Selenium.Remote;

namespace PsIntegrationUtils
{
    [PublicAPI]
    public class Session
    {
        public Session(IDictionary psConfig, string browser)
        {
            Config = new Config(psConfig);
            WebDriver = CreateDriver(browser);
            WebDriver.Navigate().GoToUrl(Config.LoginUrl);
        }

        private Config Config { get; set; }
        private IWebDriver WebDriver { get; set; }

        public void Close()
        {
            var logoutUrl = Config.LogoutUrl;
            if (logoutUrl != null)
                WebDriver.Navigate().GoToUrl(logoutUrl);

            WebDriver.Close();
        }
        private RemoteWebDriver CreateDriver(string browser)
        {
            switch ((browser ?? "").ToLowerInvariant())
            {
                case "chrome":
                    return new ChromeDriver();
                case "ie":
                    return new InternetExplorerDriver();
                case "edge":
                    return new EdgeDriver();
                default:
                    return new FirefoxDriver();
            }
        }
        private Element GetBody()
        {
            return new Element(Config, WebDriver, WebDriver.FindElement(By.TagName("body")));
        }
        public Element GetPage()
        {
            // When a modal opens, the body gets a "modal-open" class, but the ".modal.fade.in" div
            // doesn't appear immediately; so if we have "body.modal-open" but no ".modal.fade.in",
            // retry until we get a consistent state.
            //
            // After we've opened our dialog, the "modal-open" class gets removed from the body.
            // (This doesn't appear to be normal Bootstrap behavior. Maybe it's Framework doing this?)
            //
            // When a modal closes, "body.modal-open" is already gone, and ".modal.fade.in" goes away
            // immediately (the "in" class is removed).
            var result = WebDriver.Wait(Config.DefaultElementTimeout, () =>
            {
                var body = GetBody();
                var dialog = body.Css(".modal.fade.in", FindElementOptions.Optional | FindElementOptions.NoWait);
                if (body.HasClass("modal-open") && dialog == null)
                {
                    // Retry
                    return null;
                }
                return dialog ?? body;
            }, "Waiting for modal open/close to settle into a consistent state");
            return result;
        }
        public void WaitForSpinner(TimeSpan? initialWait = null, TimeSpan? quietPeriod = null)
        {
            WebDriver.WaitForSpinner(initialWait, quietPeriod);
        }
    }
}