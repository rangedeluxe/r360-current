﻿using System.Text.RegularExpressions;
using OpenQA.Selenium;

namespace PsIntegrationUtils
{
    public static class WebElementExtensions
    {
        private static string GetAttributePreviewText(IWebElement element, string attributeName)
        {
            var value = element.GetAttribute(attributeName);
            return string.IsNullOrEmpty(value)
                ? ""
                : string.Format(" {0}=\"{1}\"", attributeName, value);
        }
        public static string GetPreviewText(this IWebElement element)
        {
            return
                "<" + element.TagName +
                GetAttributePreviewText(element, "disabled") +
                GetAttributePreviewText(element, "id") +
                GetAttributePreviewText(element, "name") +
                GetAttributePreviewText(element, "class") +
                "...>";
        }
        public static bool HasClass(this IWebElement element, string className)
        {
            return Regex.IsMatch(element.GetAttribute("class"), string.Format("\\b{0}\\b", className));
        }
        public static void SetAttribute(this IWebElement element, IWebDriver webDriver, string name, string value)
        {
            webDriver.ExecuteJavaScript("arguments[0].setAttribute(arguments[1], arguments[2]);", element, name, value);
        }
    }
}