﻿using System;
using System.Collections;

namespace PsIntegrationUtils
{
    public class Config
    {
        private readonly IDictionary _dictionary;

        public Config(IDictionary dictionary)
        {
            _dictionary = dictionary;
        }

        public TimeSpan DefaultElementTimeout
        {
            get { return (TimeSpan) _dictionary["DefaultElementTimeout"]; }
        }
        public TimeSpan ElementTimeout
        {
            get { return IsRunningTest ? DefaultElementTimeout : TimeSpan.Zero; }
        }
        private bool IsRunningTest
        {
            get { return !string.IsNullOrEmpty(RunningTestCase); }
        }
        public Uri LoginUrl
        {
            get { return new Uri((string) _dictionary["LoginUrl"]); }
        }
        public Uri LogoutUrl
        {
            get
            {
                var uriString = (string) _dictionary["LogoutUrl"];
                return uriString != null ? new Uri(uriString) : null;
            }
        }
        private string RunningTestCase
        {
            get { return (string) _dictionary["RunningTestCase"]; }
        }
    }
}