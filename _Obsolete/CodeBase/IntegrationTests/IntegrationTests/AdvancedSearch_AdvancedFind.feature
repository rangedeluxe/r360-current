﻿Feature: AdvancedSearch_AdvancedFind
    In order to avoid releasing broken code
    As a member of the R360 team
    I want to know whether Advanced Find still works in Advanced Search

Scenario Template: Advanced Find
    Given I am on the Advanced Search page
    When I select workgroup '<Workgroup>'
    And I set deposit date to '10/16/2015'
    And I set advanced find to '<Field>' '<Operator>' '<Value>'
    And I select display field '<Field>'
    And I click search
    Then all search results have '<Field>' <Operator> '<Value>'

    Examples:
    | Workgroup                               | Field                 | Operator        | Value            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Account Number  | Equals          | 7505234438876021 |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Account Number  | Contains        | 7505234438876021 |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Account Number  | Begins With     | 7505             |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Account Number  | Ends With       | 6021             |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount          | Equals          | 83.71            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount          | Is Greater Than | 83.71            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount          | Is Less Than    | 83.71            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount Due      | Equals          | 526.83           |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount Due      | Is Greater Than | 526.83           |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [INV].Amount Due      | Is Less Than    | 526.83           |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].Courtesy Amount | Equals          | 48.67            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].Courtesy Amount | Is Greater Than | 48.67            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].Courtesy Amount | Is Less Than    | 48.67            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].CC2 Field       | Equals          | 48.67            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].CC2 Field       | Is Greater Than | 48.67            |
    | WFS\Mosinee Bank\DevCompany\303030 - 30 | [PMT].CC2 Field       | Is Less Than    | 48.67            |
