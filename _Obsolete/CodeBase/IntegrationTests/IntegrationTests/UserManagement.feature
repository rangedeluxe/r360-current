﻿Feature: User Management
    In order to provide feedback to Kevin
    As a Selenium developer
    I want to know how much time it will take to develop a framework QA can use to test User Management

Scenario: Open User Management
    When I am on the User Management page
    #And I select entity 'WFS\Mosinee Bank'
    #And I click the 'Add' button
    #And I click the 'Cancel' button
    And I click the checkbox in the grid header
    #Then I am on the User Management page
    #Then there are 14 rows in the grid
    Then I see an error containing 'cannot be deleted'
    #Then I do not see any errors
