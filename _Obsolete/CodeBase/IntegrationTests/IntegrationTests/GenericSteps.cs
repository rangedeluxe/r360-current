﻿using IntegrationTestFramework;
using JetBrains.Annotations;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
    [Binding, UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class GenericSteps
    {
        [When(@"I click the checkbox in the grid header")]
        public void WhenIClickTheCheckboxInTheGridHeader()
        {
            Pages.Current.Grid.ClickCheckBoxInGridHeader();
        }
        [When(@"I click the '(.*)' button")]
        [Then(@"I click the '(.*)' button")]
        public void WhenIClickTheButton(string text)
        {
            Pages.Current.ClickButton(text, true);
        }

        [When(@"I click the '(.*)' column header")]
        public void WhenIClickTheColumnHeader(string headerText)
        {
            Pages.Current.Grid.ClickColumnHeader(headerText);
        }
        [Given(@"I selected workgroup '(.*)'")]
        [When(@"I select workgroup '(.*)'")]
        [When(@"I select entity '(.*)'")]
        public void WhenISelectEntity(string entityPath)
        {
            Pages.Current.SelectEntityOrWorkgroup(entityPath);
        }

        [Then(@"I do not see any errors")]
        public void ThenIDoNotSeeAnyErrors()
        {
            Pages.Current.CheckForErrorBoxes();
        }
        [Then(@"I see an error containing '(.*)'")]
        public void ThenISeeAnErrorContaining(string substring)
        {
            Assert.That(Pages.Current.ErrorText, Is.StringContaining(substring), "Text of error box(es)");
        }
        [Then(@"there are (\d+) rows in the grid")]
        public void ThenThereAreRowsInTheGrid(int expectedRowCount)
        {
            Assert.That(Pages.Current.Grid.RecordCount(), Is.EqualTo(expectedRowCount), "Grid row count");
        }

        [Then(@"I enter '(.*)' for '(.*)' textbox")]
        [When(@"I enter '(.*)' for '(.*)' textbox")]
        public void ThenIEnterFor(string text, string textboxLabel)
        {
            Pages.Current.SetTextBoxText(text, textboxLabel);
        }

        [Then(@"I can see the '(.*)' modal")]
        public void ThenICanSeeTheModal(string legendText)
        {
            Pages.Current.ModalWithLegendHasLoaded(legendText);
        }

    }
}
