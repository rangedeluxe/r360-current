﻿using IntegrationTestFramework;
using JetBrains.Annotations;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
    [Binding, UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class UserManagementSteps
    {
        [When(@"I am on the User Management page")]
        public void WhenIAmOnTheUserManagementPage()
        {
            Pages.Navigation.GoToUserManagement();
        }

        [Then(@"I am on the User Management page")]
        public void ThenIAmOnTheUserManagementPage()
        {
            Pages.Navigation.ShouldBeAt(PageLocations.UserManagement);
        }
    }
}
