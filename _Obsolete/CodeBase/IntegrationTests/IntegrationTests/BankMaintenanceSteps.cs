﻿using IntegrationTestFramework;
using NUnit.Framework;
using System;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
    [Binding]
    public class BankMaintenanceSteps
    {
        private int _initialRowCount;
        [When(@"I am on the Bank Maintenance page")]
        public void GivenIAmOnTheBankMaintenancePage()
        {
            Pages.Navigation.GoToBankMaintenance();
            _initialRowCount =  Pages.BankMaintenance.GetRowCount();
        }
        
        [Then(@"there is one more row in the grid")]
        public void ThenThereIsOneMoreRowInTheGrid()
        {
            Assert.That(Pages.BankMaintenance.GetRowCount(), Is.EqualTo(_initialRowCount + 1), "Row count mismatch after add");
        }

        [Then(@"a row in the grid has the name '(.*)'")]
        public void ThenARowInTheGridHasTheName(string bankName)
        {
            Assert.That(Pages.BankMaintenance.BankHasName(bankName), Is.EqualTo(true), "Updated bank not found");
        }


        [Then(@"I set Entity to '(.*)'")]
        public void ThenISetEntityTo(string Entity)
        {
            Pages.BankMaintenance.SetEntity(Entity);
        }

        [When(@"I click the Update button on the grids row with content '(.*)' for the '(.*)' column")]
        public void WhenIClickTheUpdateButtonOnTheGridsRowWithContentForTheColumn(string content, string gridHeader)
        {
            Pages.BankMaintenance.ClickUpdateButtonOnGrid(content, gridHeader);
        }

    }
}
