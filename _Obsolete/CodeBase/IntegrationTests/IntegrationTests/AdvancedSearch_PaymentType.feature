﻿Feature: AdvancedSearch_PaymentType
	In order to avoid releasing broken code
    As a member of the R360 team
    I want to know whether Payment Type still works in Advanced Search

Scenario Template: Search by payment type, results found
    Given I am on the Advanced Search page
    When I select workgroup '<Workgroup>'
    And I set deposit date to '<Deposit Date>'
    And I set payment type to '<Payment Type>'
    And I click search
    Then all search results have 'Payment Type' equal to '<Payment Type>'

    Examples:
    | Workgroup                                    | Deposit Date | Payment Type |
    | WFS\Mosinee Bank\DevCompany\303030 - 30      | 10/16/2015   | Check        |

Scenario: Search by payment type, no results found
    Given I am on the Advanced Search page
    When I select workgroup 'WFS\Mosinee Bank\DevCompany\303030 - 30'
    And I set deposit date to '10/16/2015'
    And I set payment type to 'Check'
    And I click search
    Then there are no search results
