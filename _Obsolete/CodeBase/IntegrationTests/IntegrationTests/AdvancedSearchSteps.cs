﻿using IntegrationTestFramework;
using JetBrains.Annotations;
using NUnit.Framework;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
    [Binding, UsedImplicitly(ImplicitUseTargetFlags.WithMembers)]
    public class AdvancedSearchSteps
    {
        [AfterScenario]
        public void AfterScenario()
        {
            if (ScenarioContext.Current.TestError != null)
            {
                Ui.SaveScreenshot(TestContext.CurrentContext.Test.FullName);
            }
        }
        [AfterFeature]
        public static void AfterFeature()
        {
            Ui.Close();
        }

        [Given(@"I am on the Advanced Search page")]
        public void GivenIAmOnTheAdvancedSearchPage()
        {
            Pages.AdvancedSearch.GoTo();
        }

        [When(@"I click search")]
        public void WhenIClickSearch()
        {
            Pages.AdvancedSearch.ClickSearch();
        }
        [When(@"I select display field '(.*)'")]
        public void WhenISelectDisplayField(string displayField)
        {
            Pages.AdvancedSearch.SelectDisplayField(displayField);
        }
        [When(@"I set advanced find to '(.*)' '(.*)' '(.*)'")]
        public void WhenISetAdvancedFindTo(string searchField, string comparison, string value)
        {
            Pages.AdvancedSearch.SetAdvancedFind(1, searchField, comparison, value);
        }
        [Given(@"I set deposit date to '(.*)'")]
        [When(@"I set deposit date to '(.*)'")]
        public void WhenISetDepositDateTo(string depositDate)
        {
            Pages.AdvancedSearch.SetDepositDate(depositDate);
        }
        [Given(@"I set payment source to '(.*)'")]
        [When(@"I set payment source to '(.*)'")]
        public void WhenISetPaymentSourceTo(string paymentSource)
        {
            Pages.AdvancedSearch.SetPaymentSource(paymentSource);
        }
        [When(@"I set payment type to '(.*)'")]
        public void WhenISetPaymentTypeTo(string paymentType)
        {
            Pages.AdvancedSearch.SetPaymentType(paymentType);
        }

        [Then(@"all search results have '(.*)' ([\w ]+) '(.*)'")]
        public void ThenAllSearchResultsHave(string columnHeader, string comparison, string value)
        {
            Pages.AdvancedSearchResults.Grid.ExpectEveryCellInColumn(columnHeader, comparison, value);
        }
        [Then(@"I am on the Advanced Search Results page")]
        public void ThenIAmOnTheAdvancedSearchResultsPage()
        {
            Assert.IsTrue(Pages.AdvancedSearchResults.IsAt(), "Expected to be on the Advanced Search Results page");
        }
        [Then(@"there are no search results")]
        public void ThenThereAreNoSearchResults()
        {
            Assert.That(Pages.AdvancedSearch.HasNoResults(), Is.True, "Expected search to return no results");
        }
    }
}
