﻿Feature: ReceivablesSummary
   In order to avoid releasing broken code
   As a member of the R360 team
   I want to know whether Receivables Summary displays 
   the correct icons on column headers

Scenario: Initial state of sorting icons
	When I am on the dashboard page
	Then the 'Workgroup' column has an ascending sort icon 
	And  the rest of the columns have a dimmed sort icon

Scenario: Pick Payment Source to sort by
	When I am on the dashboard page
	And I click the 'Payment Source' column
	Then the column has a descending sort icon 
	And  the rest of the columns have a dimmed sort icon

Scenario: Switch sort direction of selected column
	When I click the 'Payment Source' column
	Then the column has an ascending sort icon
	And the rest of the columns have a dimmed sort icon
	