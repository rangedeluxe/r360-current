﻿using IntegrationTestFramework;
using System;
using TechTalk.SpecFlow;

namespace IntegrationTests
{
    [Binding]
    public class ReceivablesSummarySteps
    {
        private string _sortedColumn;
        [When(@"I am on the dashboard page")]
        public void WhenIAmOnTheDashboardPage()
        {
            Pages.ReceivablesSummary.ShouldBeAt();
        }

        [Then(@"the '(.*)' column has an ascending sort icon")]
        public void ThenTheColumnHasAAscendingSortIcon(string columnHeader)
        {
            _sortedColumn = columnHeader;
            Pages.ReceivablesSummary.HasHeaderWithAscendingSortIcon(columnHeader);
        }

        [Then(@"the column has an ascending sort icon")]
        public void ThenTheColumnHasAnAscendingSortIcon()
        {
            Pages.ReceivablesSummary.HasHeaderWithAscendingSortIcon(_sortedColumn);
        }

        [Then(@"the column has a descending sort icon")]
        public void ThenTheColumnHasADescendingSortIcon()
        {
            Pages.ReceivablesSummary.HasHeaderWithDescendingSortIcon(_sortedColumn);
        }

        [Then(@"the rest of the columns have a dimmed sort icon")]
        public void ThenTheRestOfTheColumnsHaveADimmedSortIcon()
        {
            Pages.ReceivablesSummary.AllHeadersHaveDimmedSortIconExcept(_sortedColumn);
        }

        [When(@"I click the '(.*)' column")]
        public void WhenIClickTheColumn(string columnHeader)
        {
            _sortedColumn = columnHeader;
            Pages.ReceivablesSummary.SortByColumn(columnHeader);           
        }

    }
}
