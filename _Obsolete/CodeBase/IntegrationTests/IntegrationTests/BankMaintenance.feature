﻿Feature: BankMaintenance
    In order to avoid releasing broken code
    As a member of the R360 team
    I want to know whether I can add and Edit Banks

Scenario: Add Bank to Bank Maintenance
	When  I am on the Bank Maintenance page
	And I click the 'Add' button
	Then I can see the 'Add Bank' modal
	And I set Entity to 'Mosinee Bank' 
	And I enter '1015' for 'ID' textbox
	And I enter 'Integration testing' for 'Name' textbox
	And I click the 'Save' button
	Then there is one more row in the grid

Scenario: Edit a Bank in Bank Maintenance
	When  I am on the Bank Maintenance page
	And I click the Update button on the grids row with content 'Integration testing' for the 'Name' column
	Then I can see the 'Edit Bank' modal
	And I enter 'Edited Integration testing bank' for 'Name' textbox
	And I click the 'Save' button
	Then a row in the grid has the name 'Edited Integration testing bank' 


Scenario: Add existing Bank to Bank Maintenance
	When I am on the Bank Maintenance page
	And I click the 'Add' button
	Then I can see the 'Add Bank' modal	
	Then I set Entity to 'Mosinee Bank' 
	Then I enter '1015' for 'ID' textbox
	And I enter 'repeated id' for 'Name' textbox
	And I click the 'Save' button
	Then I see an error containing 'Cannot Add ID 1015'