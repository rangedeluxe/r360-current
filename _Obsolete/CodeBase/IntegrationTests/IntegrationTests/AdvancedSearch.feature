﻿Feature: AdvancedSearch
    In order to avoid releasing broken code
    As a member of the R360 team
    I want to know whether Advanced Search still works

Background:
    Given I am on the Advanced Search page
    And I selected workgroup 'WFS\Mosinee Bank\DevCompany\303030 - 30'
    And I set deposit date to '10/16/2015'

Scenario: Advanced Search - Michael's Scenario 1
    When I set payment source to 'ImageRPS'
    And I click search
    Then all search results have 'Payment Source' equal to 'ImageRPS'

Scenario: Advanced Search - Michael's Scenario 2
    When I set advanced find to '[INV].Amount Due' 'Is Greater Than' '400.00'
    And I select display field '[INV].Amount Due'
    And I click search
    Then all search results have '[INV].Amount Due' greater than '400'
