﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace IntegrationUtilities
{
    public static class AttachmentUtilities
    {
        public static string TestNameToFileBaseName(string testName)
        {
            using (var sha1 = new SHA1Managed())
            {
                var bytes = Encoding.UTF8.GetBytes(testName);
                var hash = sha1.ComputeHash(bytes);
                // Slashes obviously aren't valid inside a URL path segment, and
                // IIS panics if it sees an encoded '+' in the path because, if
                // you weren't careful with your encoding/decoding, you could
                // double-decode that to a space and maybe introduce security bugs.
                // So, don't use either of those; replace with innocuous alternatives.
                var baseName = Convert.ToBase64String(hash)
                    .Replace("/", "_")
                    .Replace("+", "-");
                return baseName;
            }
        }
    }
}
