# Helper functions

function ConvertTo-XPathQuoted($Value) {
	$HasSingleQuote = $Value.Contains("'")
	$HasDoubleQuote = $Value.Contains('"')
	if ($HasSingleQuote -and $HasDoubleQuote)
	{
		return "concat('" + $Value.Replace("'", "', `"'`", '") + "')";
	} elseif ($HasSingleQuote) {
		return '"' + $Value + '"'
	} else {
		return "'" + $Value + "'"
	}
}

# Main commands

function Click($Element) {
	$Element.Click()
}

function Enter($Element, $Value) {
	$Element.Enter($Value)
}

function EnterAdvancedFind($SearchField, $Comparison, $Value) {
    $FormSearchFields = XPaths ".//select[starts-with(@name, 'selField')]"
    $EmptyFields = $FormSearchFields | Where { ([int] $_.GetAttribute('selectedIndex')) -le 0 }
    $FirstEmptyField = $EmptyFields | Select -First 1
    $FieldNumber = $FirstEmptyField.GetAttribute('name') -replace '[a-z]', ''

    Enter (XPath ".//select[@name='selField$FieldNumber']") $SearchField
    Enter (XPath ".//select[@name='selOperator$FieldNumber']") $Comparison
    Enter (XPath ".//input[@name='searchFieldValue$FieldNumber']") $Value
}

function EnterDate($Element, [DateTime]$Value) {
    Click $Element.Css("button")
    $Calendar = Css "#ui-datepicker-div"
    Enter $Calendar.Css(".ui-datepicker-year") $Value.Year
    Enter $Calendar.Css(".ui-datepicker-month") $Value.ToString("MMM")
    Click $Calendar.XPath(".//a[.='$($Value.Day)']")
}

function EnterWorkgroup($Element, $Value) {
	$Segments = $Value.Split("\")
	$LastSegment = $Segments | Select -Last 1
	$Ancestry = ($Segments | Select -First ($Segments.Count - 1)) -join " / "

	Click $Element.Class("jqx-expander-header")
	Click $Element.Class("select2-container")

	# The search box and search results are in their own layer, not children of the workgroup selector
	Enter (Class "select2-input") $LastSegment
	Click (Wait {
		$XPath = ".//li[contains(concat(' ', @class, ' '), ' select2-result ')]" +
			"[normalize-space(.//tr[1]/td)=$(ConvertTo-XPathQuoted $Ancestry)]" +
			"[normalize-space(.//tr[2]//b)=$(ConvertTo-XPathQuoted $LastSegment)]"
		XPath $XPath
	})

	Wait { Assert-Equal $Element.Text() "Selected: $LastSegment" }
}

function Show($Elements) {
	if ($Elements -eq $Null) {
		throw "No elements to show"
	}

	@("red", "white", "red", "white", "red") | ForEach {
		$Color = $_
		$Elements | ForEach {
			$_.OverrideStyle("background", $Color);
		}
		Sleep -Milliseconds 400
	}
	# Leave it on the final color for a little extra time
	Sleep -Milliseconds 400
	$Elements | ForEach {
		$_.OverrideStyle("background", $Null);
	}
}

# Keep retrying, up to the default element timeout, as long as the condition proc
# throws an exception or returns $False.
function Wait {
	param (
		[ScriptBlock] $ConditionProc,
		[switch] $NoErrorOnTimeout,
		[TimeSpan] $Timeout = $Config.DefaultElementTimeout
	)

	$WaitUntil = [System.DateTime]::Now + $Timeout
	while ($True) {
		try {
			$Result = & $ConditionProc
			if ($Result -eq $Null) {
				return $Result
			} elseif ($Result.Count -eq 0) {
				return $Result
			} elseif ($Result.Count -gt 1) {
				return $Result
			} elseif ($Result[0] -ne $False) {
				return $Result
			}

			if ([System.DateTime]::Now -gt $WaitUntil) {
				if ($NoErrorOnTimeout) {
					return
				} else {
					throw "Retry timeout expired in {$ConditionProc}"
				}
			}
		} catch {
			if ([System.DateTime]::Now -gt $WaitUntil) {
				throw "Retry timeout expired: $_"
			}
		}
		Sleep -Milliseconds 500
	}
}

function WaitForSpinner {
	param (
		[TimeSpan] $AppearTimeout = $Config.WaitSpinnerAppearTimeout,
		[TimeSpan] $DisappearTimeout = $Config.WaitSpinnerDisappearTimeout,
		[string] $Message = "Waiting for load spinner"
	)
	Write-Progress -Id 34 $Message -Status "Waiting for load spinner to appear"
	Wait { (Csses ".spinDiv").Count -gt 0 } -Timeout $AppearTimeout -NoErrorOnTimeout | Out-Null

	Write-Progress -Id 34 $Message -Status "Waiting for load spinner to disappear"
	Wait { (Csses ".spinDiv").Count -eq 0 } -Timeout $DisappearTimeout | Out-Null

	Write-Progress -Id 34 $Message -Completed
}
