$script:AutoLoginNeeded = $True

function Enable-AutoLogin {
	$script:AutoLoginNeeded = $True
}

function Disable-AutoLogin {
	$script:AutoLoginNeeded = $False
}

function Get-AutoLoginEnabled {
	$script:AutoLoginNeeded
}

function Invoke-AutoLogin {
	if (Get-AutoLoginEnabled) {
		Invoke-Login
	}
}

function Invoke-Login {
	param (
		[string] $EntityName = $Config.LoginEntity,
		[string] $LoginName = $Config.LoginName,
		[string] $Password = $Config.Password
	)

	Disable-AutoLogin
	Enter (Labeled "Entity Name") $EntityName
	Enter (Labeled "Login Name") $LoginName
	Enter (Labeled "Password") $Password
	Click (Button "Sign In")

	$LoginErrors = Csses "div.validation-summary-errors li" "NoWait" | ForEach { $_.Text() }
	if ($LoginErrors) {
		throw "Login failed: $($LoginErrors -join ""`r`n"")"
	}

	WaitForSpinner -Message "First load spinner"
	WaitForSpinner -Message "Second load spinner" -AppearTimeout $Config.DashboardSecondWaitSpinnerAppearTimeout
}

New-Alias LogIn Invoke-Login

Export-ModuleMember -Function * -Alias *
