function Get-CurrentPage {
	Invoke-AutoLogin
	(Get-Browser).GetPage()
}

function Button($ButtonText) {
	(Get-CurrentPage).Button($ButtonText)
}
function Buttons($ButtonText) {
	(Get-CurrentPage).Buttons($ButtonText)
}
function CellsInColumn($Column) {
    if ($Column -is [String]) {
        $Column = GridHeader $Column
    }
    $Result = XPaths ".//tr[@class='oddrow' or @class='evenrow']/td[$($Column.ColumnNumber)]"
    if ($Result.Count -eq 0) {
        Write-Error "Could not find any cells in column '$($Column.Text())'"
    }
    $Result
}
function Class($Class) {
	(Get-CurrentPage).Class($Class)
}
function Css($Css) {
	(Get-CurrentPage).Css($Css)
}
function Csses($Css, $Options = "None") {
	(Get-CurrentPage).Csses($Css, $Options)
}
function GridHeader($Text) {
    $Result = GridHeaders | Where { $_.Text() -eq $Text }
    if ($Result.Count -eq 0) {
        Write-Error "Could not find grid header '$Text'"
        return
    }
    if ($Result.Count -gt 1) {
        Write-Error "Found more than one grid header '$Text'"
        return
    }
    $Result
}
function GridHeaders() {
    $ColumnNumber = 1
    Csses ".grid-header" | ForEach {
        $_ | Add-Member -MemberType NoteProperty -Name Element -Value $_.ToString()
        $_ | Add-Member -MemberType NoteProperty -Name ColumnNumber -Value $ColumnNumber
        $ColumnNumber += 1
        $_
    }
}
function Id($Id) {
	(Get-CurrentPage).Id($Id)
}
function Labeled($LabelText) {
	(Get-CurrentPage).Labeled($LabelText)
}
function MenuItem($MenuItemText, $SubMenuItemText) {
	(Get-CurrentPage).MenuItem($MenuItemText, $SubMenuItemText)
}
function PageTitle {
	(Get-CurrentPage).PageTitle()
}
function XPath($XPath) {
	(Get-CurrentPage).XPath($XPath)
}
function XPaths($XPath) {
	(Get-CurrentPage).XPaths($XPath)
}
