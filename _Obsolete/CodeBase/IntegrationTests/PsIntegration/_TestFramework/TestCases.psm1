$script:TestCases = @()
$script:TestResults = @()

# Helper functions

function IsCSharpTestFailureException($Exception) {
	$Exception -is [PsIntegrationUtils.TestFailureException]
}

function ConvertTo-RelativePath($Path) {
	Push-Location (Join-Path $PSScriptRoot "..")
	$Result = (Resolve-Path -Relative $Path) -replace "^\.\\", ""
	Pop-Location
	$Result
}

# Test scripts

function Get-TestScript($Root = (Join-Path $PSScriptRoot "..")) {
	$ExcludedRelativePaths = @("Interactive.ps1", "Run.ps1")

	$Subdirectories = Get-ChildItem $Root -Directory -Exclude _*
	$Subdirectories | ForEach {
		Get-TestScript $_
	}

	Get-ChildItem $Root -File -Filter *.ps1 | Where {
		# Filter out anything whose relative path is in $ExcludedRelativePaths
		!($ExcludedRelativePaths -eq (ConvertTo-RelativePath $_.FullName))
	}
}

function Import-TestScript {
	$script:TestCases = @()
	Get-TestScript | ForEach {
		# Load the file inside its own isolated module
		$ModuleCode = "Export-ModuleMember; . ""$($_.FullName)"""
		$ModuleBlock = [ScriptBlock]::Create($ModuleCode)
		New-Module $ModuleBlock | Out-Null
	}
}

# Test cases

function Add-TestCase($Name, $Proc) {
	$CallingFileName = ConvertTo-RelativePath $MyInvocation.PSCommandPath
	$TestCase = New-Object PSObject
	# Add properties in the same order we want them to appear in grid output
	$TestCase | Add-Member -MemberType NoteProperty -Name Name -Value $Name
	$TestCase | Add-Member -MemberType NoteProperty -Name File -Value $CallingFileName
	# Hide Proc inside a method instead of a property, so it doesn't show up in PowerShell output
	# by default; but don't actually bind Proc as the method body itself, because then it loses
	# some context, and error stack traces no longer reflect the original filename and line numbers.
	# Instead, just make a method that returns the $Proc ScriptBlock in its original form.
	$TestCase | Add-Member -MemberType ScriptMethod -Name GetProc -Value { $Proc }.GetNewClosure()
	$script:TestCases += @($TestCase)
}

function Get-TestCase {
	$script:TestCases
}

# Tests (i.e., a particular test case in a particular browser)

function Get-Test {
	$Browsers = $Config.Browsers
	if (!$Browsers) {
		$Browsers = @("FireFox")
	}

	Get-TestCase | ForEach {
		$TestCase = $_
		$Config.Browsers | ForEach {
			$Browser = $_
			$Test = New-Object PSObject
			$Test | Add-Member -MemberType NoteProperty -Name Name -Value $TestCase.Name
			$Test | Add-Member -MemberType NoteProperty -Name Browser -Value $Browser
			$Test | Add-Member -MemberType NoteProperty -Name File -Value $TestCase.File
			$Test | Add-Member -MemberType ScriptMethod -Name GetProc -Value { $TestCase.GetProc() }.GetNewClosure()
			$Test
		}
	}
}

function Invoke-Test($Test) {
	$Result = New-Object PSObject
	$Result | Add-Member -NotePropertyName Name -NotePropertyValue $Test.Name
	$Result | Add-Member -NotePropertyName Browser -NotePropertyValue $Test.Browser
	$Result | Add-Member -NotePropertyName File -NotePropertyValue $Test.File
	$Result | Add-Member -NotePropertyName Result -NotePropertyValue "Undetermined"
	Open-Browser $Test.Browser
	try {
		$Config.RunningTestCase = $Test.Name
		& $Test.GetProc()
		$Result.Result = "Passed"
	} catch {
		$Result.Result = "Failed"
		Write-Host "Test ""$($Test.Name)"" in $($Test.Browser) FAILED with message:" -ForegroundColor Red
		if (IsCSharpTestFailureException $_.Exception.InnerException) {
			Write-Host "    $($_.Exception.InnerException.Message)" -ForegroundColor Red
		} else {
			Write-Host "    $_" -ForegroundColor Red
		}
		if ($_.ScriptStackTrace -match "at <ScriptBlock>, (.*): line (\d+)") {
			$FileName = $Matches[1]
			$LineNumber = [int] $Matches[2]
			$Source = (Get-Content $FileName)[$LineNumber - 1].Trim()
			Write-Host "at $FileName line $($LineNumber):" -ForegroundColor Red
			Write-Host "    $Source" -ForegroundColor Red
			Write-Host "    $('~' * $Source.Length)" -ForegroundColor Red
			Write-Host
		}
	} finally {
		$Config.RunningTestCase = $Null
		Close-Browser
	}
	$Result
}

function Start-Test([switch] $Choose) {
	Import-TestScript

	$script:TestResults = @()
	$Tests = Get-Test
	if ($Choose) {
		[array] $Tests = $Tests | Out-GridView -PassThru
	}

	$Results = @()
	$Tests | ForEach {
		Write-Progress -Activity "Running tests" `
			-Status "Running test $($Results.Count + 1) of $($Tests.Count)" `
			-PercentComplete ($Results.Count * 100 / $Tests.Count) `
			-CurrentOperation "Test '$($_.Name)' in $($_.Browser)"
		$Results += (Invoke-Test $_)
	}

	Write-Progress -Activity "Running tests" -Completed
	$script:TestResults = $Results
	$Results
}

function Get-TestResult {
	$script:TestResults
}

# Exports

New-Alias TestCase Add-TestCase

Export-ModuleMember -Function Get-TestScript, Import-TestScript, Add-TestCase, Get-TestCase, Get-Test, Start-Test, Get-TestResult
Export-ModuleMember -Alias *
