$UnquotedTypes = @(
	[System.Boolean]
	[System.Byte]
	[System.SByte]
	[System.Int16]
	[System.UInt16]
	[System.Int32]
	[System.UInt32]
	[System.Int64]
	[System.UInt64]
	[System.Single]
	[System.Double]
	[System.Enum]
)

function ConvertTo-Display($Value) {
	if ($Value -eq $Null) {
		"(null)"
	} elseif ($Value -is [System.DateTime]) {
		if ($Value.TimeOfDay -eq [System.TimeSpan]::Zero) {
			$Value.ToString("yyyy-MM-dd")
		} else {
			$Value.ToString("yyyy-MM-dd hh:mm:sstt").ToLowerInvariant()
		}
	} elseif ($UnquotedTypes | Where { $Value -is $_ }) {
		[string] $Value
	} else {
		"`"$($Value)`""
	}
}

function Test-Equal($A, $B) {
	# PowerShell's "-eq" operator is case-insensitive, and thinks "5" -eq 5.
	# We don't want either of those things. Use default .NET equality instead.
	[System.Object]::Equals($A, $B)
}

function Assert-That($Success, $ComparisonDescription, $Actual, $Expected) {
	if (!$Success) {
		if ($ComparisonDescription) {
			$Prefix = $ComparisonDescription + " "
		} else {
			$Prefix = ""
		}
		throw "Expected: $Prefix$(ConvertTo-Display $Expected)`r`n But was: $(ConvertTo-Display $Actual)"
	}
}

function Assert-Equal($Actual, $Expected) {
	Assert-That (Test-Equal $Actual $Expected) "" $Actual $Expected
}
function Assert-NotEqual($Actual, $Expected) {
	Assert-That (!(Test-Equal $Actual $Expected)) "not equal to" $Actual $Expected
}

$FunctionExports = Get-Command Assert-* -Module Assertions |
	ForEach { $_.Name } |
	Where { $_ -ne "Assert-That" }
Export-ModuleMember -Function $FunctionExports
