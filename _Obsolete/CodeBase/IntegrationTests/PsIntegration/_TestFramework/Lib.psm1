$ErrorActionPreference = "Stop"

$Config = @{}
. (Join-Path $PSScriptRoot "..\_Config\Config.ps1")

# Window dressing

function Prompt {
	Write-Host "Selenium " -NoNewLine -ForegroundColor Green
	return "$(Get-Location)> "
}

# Test infrastructure

$AssemblyPath = Join-Path $PSScriptRoot "bin\PSIntegrationUtils.dll"
[System.Reflection.Assembly]::LoadFrom($AssemblyPath) | Out-Null

@(
	"Browsers.psm1"
	"TestCases.psm1"
	"Commands.psm1"
	"Assertions.psm1"
	"Locators.psm1"
	"Login.psm1"
) | ForEach { Import-Module (Join-Path $PSScriptRoot $_) }

Export-ModuleMember -Variable Config -Function * -Alias *
