Register-EngineEvent PowerShell.Exiting -Action {
	Close-Browser
} | Out-Null

# Exported functions

function Open-Browser([ValidateSet("FireFox", "Chrome", "IE", "Edge")] $BrowserType) {
	Close-Browser
	Enable-AutoLogin
	$script:CurrentBrowser = New-Object PsIntegrationUtils.Session @($Config, $BrowserType)
}

function Close-Browser {
	if ($script:CurrentBrowser) {
		try { $script:CurrentBrowser.Close() } catch {}
		$script:CurrentBrowser = $Null
	}
}

function Get-Browser {
	$script:CurrentBrowser
}
