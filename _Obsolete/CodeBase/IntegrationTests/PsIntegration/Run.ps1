param (
	[switch] $List,
	[switch] $Choose
)

$ErrorActionPreference = "Stop"

# Run in a new PowerShell instance so the DLLs get unlocked when we're done.
PowerShell -NoProfile {
	$ErrorActionPreference = "Stop"

	Import-Module .\_TestFramework\Lib.psm1

	$List = $Args[0]
	$Choose = $Args[1]
	if ($List) {
		Import-TestScript
		Get-Test
	} else {
		Start-Test -Choose:$Choose
	}
} -Args @([bool] $List, [bool] $Choose)