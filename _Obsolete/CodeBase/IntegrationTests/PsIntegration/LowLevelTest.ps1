TestCase "Advanced Search (low-level)" {
	$Search = (XPath ".//nav/ul/li[a='Search']")
	Click $Search
	$AdvancedSearch = $Search.XPath("./ul/li[a='Advanced Search']")
	Click $AdvancedSearch

	Wait { (Csses ".spinDiv").Count -gt 0 } -Timeout $Config.WaitSpinnerAppearTimeout -NoErrorOnTimeout | Out-Null
	Wait { (Csses ".spinDiv").Count -eq 0 } -Timeout $Config.WaitSpinnerDisappearTimeout | Out-Null

	$wg = (XPath ".//td[@class='hubLabel'][starts-with(normalize-space(.),'Workgroup')]/following-sibling::td[1]")
	Click $wg.Class("jqx-expander-header")
	Click $wg.Class("select2-container")

	# The search box and search results are in their own layer, not children of the workgroup selector
	Enter (Class "select2-input") "228"
	Click (Wait { XPath "//li[contains(concat(' ', @class, ' '), ' select2-result ')][normalize-space(.//tr[1]/td)='WFS / Michael Bank / Michael Inc'][normalize-space(.//tr[2]//b)='228 - Sailu WG for Org Id test']" })

	Wait { Assert-Equal $wg.Text() "Selected: 228 - Sailu WG for Org Id test" }

    Click (Css "#StartDatePicker button")
    $Calendar = Css "#ui-datepicker-div"
    Enter $Calendar.Css(".ui-datepicker-year") "2016"
    Enter $Calendar.Css(".ui-datepicker-month") "Feb"
    Click $Calendar.XPath(".//a[.='24']")

    Click (Css "#EndDatePicker button")
    $Calendar = Css "#ui-datepicker-div"
    Enter $Calendar.Css(".ui-datepicker-year") "2016"
    Enter $Calendar.Css(".ui-datepicker-month") "Feb"
    Click $Calendar.XPath(".//a[.='24']")

    Enter (XPath ".//select[@name='selField1']") "Account Number"
    Enter (XPath ".//select[@name='selOperator1']") "Equals"
    Enter (XPath ".//input[@name='searchFieldValue1']") "12345"

    Click (XPath "(.//input[@value='Search'])[1]")

    $AccountNumberCells = XPaths ".//tr[@class='oddrow' or @class='evenrow']/td[9]"
    Assert-NotEqual $AccountNumberCells.Count 0
    $AccountNumberCells | ForEach {
        Assert-Equal $_.Text() "12345"
    }
}
