param(
	[ValidateSet("FireFox", "Chrome", "IE", "Edge")] [string] $Browser = "FireFox",
	[switch] $NoBrowser = $False,
	[switch] $NoLogin = $False,
	[switch] $Console = $False,
	[ScriptBlock] $Run
)

$ErrorActionPreference = "Stop"

if (Get-ChildItem ../*.sln) {
	Write-Progress "Building" -Status "Building C# solution"
	pushd
	cd ..
	$MSBuildDir = (Get-ItemProperty HKLM:\SOFTWARE\Wow6432Node\Microsoft\MSBuild\ToolsVersions\14.0).MSBuildToolsPath
	$MSBuildPath = Join-Path $MSBuildDir "msbuild.exe"
	& $MSBuildPath /v:q /nologo
	Write-Progress "Building" -Completed
	popd
	if ($LastExitCode -ne 0) {
		Exit
	}
}

$Commands = @()
$Commands += "`$ErrorActionPreference = 'Stop'"
$Commands += "Import-Module '$(Join-Path $PSScriptRoot '_TestFramework\Lib.psm1')'"
if (!$NoBrowser) {
	$Commands += "Write-Progress 'Loading' -Status 'Opening browser'"
	$Commands += "Open-Browser $Browser"
	if (!$NoLogin) {
		$Commands += "Write-Progress 'Loading' -Status 'Logging in'"
		$Commands += "LogIn"
	}
	$Commands += "Write-Progress 'Loading' -Completed"
}
$Commands += $Run
$CommandLine = $Commands -join "; "

# Launch a new copy of PowerShell and let it load and lock the DLLs.
# User can "Exit" to unlock the DLLs and go back to the main PowerShell prompt.
if ($Console) {
	PowerShell -NoProfile -NoExit $CommandLine
} else {
	$IseProfilePath = $Profile -replace 'PowerShell_profile', 'PowerShellISE_profile'
	$IseProfileContent = Get-Content $IseProfilePath -ErrorAction Ignore
	if (!($IseProfileContent -match 'env:IseStartupCommand')) {
		$IseProfileContent = @(
			'if ($env:IseStartupCommand) {'
			'    Invoke-Expression $env:IseStartupCommand'
			'}'
		) + $IseProfileContent
		Set-Content $IseProfilePath $IseProfileContent
	}
	$env:IseStartupCommand = $CommandLine
	ise
	Remove-Item env:IseStartupCommand
}
