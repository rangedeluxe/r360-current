TestCase "Advanced Search (high-level)" {
	Click (MenuItem "Search" "Advanced Search")
	WaitForSpinner
	EnterWorkgroup (Labeled "Workgroup") "WFS\Michael Bank\Michael Inc\228 - Sailu WG for Org Id test"
    EnterDate (Id "StartDatePicker") "2016-02-24"
    EnterDate (Id "EndDatePicker") "2016-02-24"
    EnterAdvancedFind "Account Number" "Equals" "12345"
    Click (Buttons "Search" | Select -First 1)
    CellsInColumn "Account Number" | ForEach {
        Assert-Equal $_.Text() "12345"
    }
}
