﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationExcelTests.models
{
    public class AdvancedSearchModel
    {
        public AdvancedSearchModel()
        {
            _displayFields = new List<string>();
            _advanceFindList = new List<AdvancedFindModel>();
        }

        public string TestName { get; set; }
        public string Workgroup { get; set; }
        public string DepositDate { get; set; }
        public string PaymentType { get; set; }
        public string Expected { get; set; }

        private List<string> _displayFields; 
        public List<string> DisplayFields { get; set; }
        public void AddDisplayField(string name)
        {
            DisplayFields.Add(name);
        }

        private List<AdvancedFindModel> _advanceFindList;
        public List<AdvancedFindModel> AdvancedFindList { get; set; }
        public void AddAdvancedFind(AdvancedFindModel avdFind)
        {
            AdvancedFindList.Add(avdFind);
        }

        public override string ToString()
        {
            return String.Format("Test - {0} for Workgroup {1} on DepositDate {2}", TestName, Workgroup, DepositDate);
        }
    }
}
