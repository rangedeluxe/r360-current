﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IntegrationExcelTests.models
{
    public enum DataTypes
    {
        String,
        Float,
        Currency,
        Date
    }

    public class AdvancedFindModel
    {
        public int Index { get; set; }
        public string Name { get; set; }
        public string Op { get; set; }
        public string Val { get; set; }
        public DataTypes DataType { get; set; }

        public override string ToString()
        {
            return String.Format("Index: {0} Name: {1} Op: {2} Val: {3} DataType {4}", Index, Name, Op, Val, DataType);
        }
    }
}
