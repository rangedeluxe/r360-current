﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using IntegrationExcelTests.models;
using IntegrationExcelTests.utility;
using IntegrationTestFramework;
using NUnit.Framework;

namespace IntegrationExcelTests
{

    public class AdvancedSearchTests
    {

        public void AfterTest()
        {
            // Ui.Close();
        }

        private IEnumerable<AdvancedSearchModel> GetPaymentTypeTestData()
        {
            var reader = new CsvReader("AdvancedSearch_paymentType.csv", true);
            var tests = reader.ReadPaymentType();
            return tests;
        }

        [Test, TestCaseSource("GetPaymentTypeTestData")]
        public void TestAdvancedSearch_PaymentType(AdvancedSearchModel test)
            {
                Pages.AdvancedSearch.GoTo();
                Pages.AdvancedSearch.SelectEntityOrWorkgroup(test.Workgroup);
                Pages.AdvancedSearch.SetDepositDate(test.DepositDate);
                Pages.AdvancedSearch.SetPaymentType(test.PaymentType);
                Pages.AdvancedSearch.ClickSearch();
                if (test.Expected.Equals("no results", StringComparison.InvariantCultureIgnoreCase))
                {
                    Assert.IsTrue(
                        Pages.AdvancedSearch.IsAt(),
                        "Expected to stay on the Advanced Search page");
                    Assert.IsTrue(
                        Pages.AdvancedSearch.HasNoResults(),
                        "Expected 'no records' error message to be displayed");
                }
                else
                {
                    Assert.IsTrue(Pages.AdvancedSearchResults.IsAt(), "Expected to stay on the Advanced Search page");
                    Assert.IsTrue(Pages.AdvancedSearchResults.Grid.ColumnDataEqualTo("Payment Type", test.PaymentType),
                        "Expected All Column Data to Equal" + test.PaymentType);
                    Assert.IsTrue(Pages.AdvancedSearchResults.Grid.RecordCount() > 0,
                        "Expect RecordCount to be greater than 0");
                }
            }

        private IEnumerable<AdvancedSearchModel> GetAdvancedFindestData()
        {
            var reader = new CsvReader("AdvancedSearch_advancedFind.csv", true);
            var tests = reader.ReadAdvancedFind();
            return tests;
        }

        [Test, TestCaseSource("GetAdvancedFindestData")]
        public void TestAdvancedSearch_AdvancedFind(AdvancedSearchModel test)
            {
                Trace.WriteLine(String.Format("Starting Test : {0} with Parameters Workgroup: {1} DepositDate: {2} AdvancedFind: {3} DisplayField: {4}", test.TestName, test.Workgroup, test.DepositDate, test.AdvancedFindList[0], test.DisplayFields[0]));
                var adv = test.AdvancedFindList[0];
                var display = test.DisplayFields[0];

                Pages.AdvancedSearch.GoTo();
                Pages.AdvancedSearch.SelectEntityOrWorkgroup(test.Workgroup);
                Pages.AdvancedSearch.SetDepositDate(test.DepositDate);
                Pages.AdvancedSearch.SetAdvancedFind(adv.Index, adv.Name, adv.Op, adv.Val);
                Pages.AdvancedSearch.SelectDisplayField(display);
                Pages.AdvancedSearch.ClickSearch();
                Assert.IsTrue(
                    Pages.AdvancedSearchResults.IsAt(),
                    "Expected to move to the Search Results page");
                Assert.IsTrue(ValidateDataByCondition(adv.Op, adv.Name, adv.Val, adv.DataType),
                    "Expected Data Matches Advanced Find Operations.");

                Trace.WriteLine("Finished Test");
            }

        private bool ValidateDataByCondition(string condition, string column, string expected, DataTypes dataType)
        {
            switch (condition)
            {
                case "Equals":
                    switch (dataType)
                    {
                        case DataTypes.Currency:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataEqualTo(column, Convert.ToDecimal(expected));
                        case DataTypes.Float:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataEqualTo(column, Convert.ToSingle(expected));
                        case DataTypes.Date:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataEqualTo(column,
                                Convert.ToDateTime(expected, new CultureInfo("en-US")));
                        case DataTypes.String:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataEqualTo(column, expected);
                    }
                    break;
                case "Contains":
                    return Pages.AdvancedSearchResults.Grid.ColumnDataContains(column, expected);
                case "Begins With":
                    return Pages.AdvancedSearchResults.Grid.ColumnDataBeginsWith(column, expected);
                case "Ends With":
                    return Pages.AdvancedSearchResults.Grid.ColumnDataEndsWith(column, expected);
                case "Is Greater Than":
                    switch (dataType)
                    {
                        case DataTypes.Currency:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataGreaterThan(column, Convert.ToDecimal(expected));
                        case DataTypes.Float:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataGreaterThan(column, Convert.ToSingle(expected));
                        case DataTypes.Date:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataGreaterThan(column,
                                Convert.ToDateTime(expected, new CultureInfo("en-US")));
                    }
                    break;
                case "Is Less Than":
                    switch (dataType)
                    {
                        case DataTypes.Currency:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataLessThan(column, Convert.ToDecimal(expected));
                        case DataTypes.Float:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataLessThan(column, Convert.ToSingle(expected));
                        case DataTypes.Date:
                            return Pages.AdvancedSearchResults.Grid.ColumnDataLessThan(column,
                                Convert.ToDateTime(expected, new CultureInfo("en-US")));
                    }
                    break;
                default:
                    return false;
            }
            return false;
        }
    }
}