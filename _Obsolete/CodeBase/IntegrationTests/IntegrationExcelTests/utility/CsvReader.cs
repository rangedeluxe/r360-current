﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using IntegrationExcelTests.models;

namespace IntegrationExcelTests.utility
{
    public class CsvReader
    {
        public CsvReader(string testFile, bool containsHeaders)
        {
            FileName = testFile;
            ContainsHeaders = containsHeaders;
        }

        public string FileName { get; set; }
        public bool ContainsHeaders { get; set; }

        public IEnumerable<AdvancedSearchModel> ReadPaymentType()
        {
            var allLines = File.ReadAllLines(Path.Combine("data", FileName));
            var list = allLines.ToList();

            if (ContainsHeaders)
            {
                list.RemoveAt(0);
            }

            var query = from line in list
                let data = line.Split(',')
                select new AdvancedSearchModel
                {
                    TestName = data[0],
                    Workgroup = data[1],
                    DepositDate = data[2],
                    PaymentType = data[3],
                    Expected = data[4]
                };

            return query;
        }

        public IEnumerable<AdvancedSearchModel> ReadAdvancedFind()
        {
            var allLines = File.ReadAllLines(Path.Combine("data", FileName));
            var list = allLines.ToList();

            if (ContainsHeaders)
            {
                list.RemoveAt(0);
            }

            var query = from line in list
                let data = line.Split(',')
                select new AdvancedSearchModel
                {
                    TestName = data[0],
                    Workgroup = data[1],
                    DepositDate = data[2],
                    AdvancedFindList =
                        new List<AdvancedFindModel>
                        {
                            new AdvancedFindModel {Index = 1, Name = data[3], Op = data[4], Val = data[5], DataType = (DataTypes)Enum.Parse(typeof(DataTypes), data[7])}
                        },
                    DisplayFields = new List<string> {data[6]},
                    Expected = data[8]
                };

            return query;
        }
    }
}