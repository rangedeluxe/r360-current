﻿using System.Web.Mvc;
using System.Web.Routing;

namespace IntegrationPortal
{
    public static class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "TestRun",
                url: "TestRun/{dateTimeString}",
                defaults: new {controller = "Home", action = "TestRun"});

            routes.MapRoute(
                name: "Screenshot",
                url: "Screenshot/{dateTimeString}/{hash}/{browserName}",
                defaults: new {controller = "Home", action = "Screenshot"});

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
