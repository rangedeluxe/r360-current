﻿using System.IO;

namespace IntegrationPortal.Models
{
    public class BrowserTestRunIdentifier
    {
        public BrowserTestRunIdentifier(TestRunIdentifier testRunIdentifier, string browserName)
        {
            TestRunIdentifier = testRunIdentifier;
            BrowserName = browserName;
        }

        public string BrowserName { get; private set; }
        private string BrowserTestResultsDirectory
        {
            get { return Path.Combine(TestRunIdentifier.TestResultsDirectory, BrowserName); }
        }
        private TestRunIdentifier TestRunIdentifier { get; set; }

        public string GetScreenshotPathFromHash(string hash)
        {
            return Path.Combine(BrowserTestResultsDirectory, hash + ".png");
        }
    }
}