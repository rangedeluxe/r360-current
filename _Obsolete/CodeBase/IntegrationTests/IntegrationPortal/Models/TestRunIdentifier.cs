﻿using System;
using System.Globalization;
using System.IO;
using IntegrationPortal.Properties;

namespace IntegrationPortal.Models
{
    public class TestRunIdentifier
    {
        public TestRunIdentifier(string dateTimeString)
        {
            DateTime = DateTime.ParseExact(dateTimeString, "yyyyMMdd_HHmmss", CultureInfo.InvariantCulture);
        }

        public DateTime DateTime { get; private set; }
        public string DateTimeString
        {
            get { return DateTime.ToString("yyyyMMdd_HHmmss"); }
        }
        public string TestResultsDirectory
        {
            get { return Path.Combine(Settings.Default.BaseTestResultsDirectory, DateTimeString); }
        }
        public string Title
        {
            get { return DateTime.ToString(CultureInfo.InvariantCulture); }
        }
    }
}