﻿using System.Collections.Generic;
using System.Linq;

namespace IntegrationPortal.Models
{
    public class TestRunDetail
    {
        private readonly IReadOnlyList<BrowserTestRunDetail> _browserDetails;
        private readonly TestRunIdentifier _identifier;

        public TestRunDetail(TestRunIdentifier identifier, IEnumerable<BrowserTestRunDetail> browserDetails)
        {
            _identifier = identifier;
            _browserDetails = browserDetails.ToList();
        }

        public string DateTimeString
        {
            get { return _identifier.DateTimeString; }
        }
        public IEnumerable<TestDetail> Failures
        {
            get { return _browserDetails.SelectMany(browser => browser.Failures); }
        }
        public string SummaryBackgroundClass
        {
            get
            {
                if (TestFailCount > 0)
                {
                    return "bg-danger";
                }
                if (TestNotRunCount > 0)
                {
                    return "bg-warning";
                }
                return "bg-success";
            }
        }
        public int TestCount
        {
            get { return _browserDetails.Sum(browser => browser.TestCount); }
        }
        public int TestFailCount
        {
            get { return _browserDetails.Sum(browser => browser.TestFailCount); }
        }
        public int TestNotRunCount
        {
            get { return _browserDetails.Sum(browser => browser.TestNotRunCount); }
        }
        public string Title
        {
            get { return _identifier.Title; }
        }
    }
}