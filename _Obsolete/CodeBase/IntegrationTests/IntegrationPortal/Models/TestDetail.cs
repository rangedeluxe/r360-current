﻿using System.IO;
using IntegrationUtilities;

namespace IntegrationPortal.Models
{
    public class TestDetail
    {
        public TestDetail(BrowserTestRunIdentifier identifier, string name, string failureMessage)
        {
            Identifier = identifier;
            Name = name;
            FailureMessage = failureMessage;
        }

        public string BrowserName
        {
            get { return Identifier.BrowserName; }
        }
        public string FailureMessage { get; private set; }
        public string Hash
        {
            get { return AttachmentUtilities.TestNameToFileBaseName(Name); }
        }
        public bool HasScreenShot
        {
            get
            {
                var screenShotPath = Identifier.GetScreenshotPathFromHash(Hash);
                return File.Exists(screenShotPath);
            }
        }
        private BrowserTestRunIdentifier Identifier { get; set; }
        public string Name { get; private set; }
    }
}