using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;

namespace IntegrationPortal.Models
{
    public class BrowserTestRunDetail
    {
        private readonly BrowserTestRunIdentifier _identifier;
        private readonly XElement _root;

        public BrowserTestRunDetail(BrowserTestRunIdentifier identifier, XDocument document)
        {
            _identifier = identifier;
            _root = document.Root;
        }

        public IEnumerable<TestDetail> Failures
        {
            get
            {
                return
                    from testCase in _root.Descendants("test-case")
                    let name = testCase.Attribute("name").Value
                    let failureElement = testCase.Element("failure")
                    where failureElement != null
                    let messageElement = failureElement.Element("message")
                    where messageElement != null
                    select new TestDetail(_identifier, name, messageElement.Value);
            }
        }
        public int TestCount
        {
            get { return GetTestCount("total"); }
        }
        public int TestFailCount
        {
            get { return GetTestCount("errors") + GetTestCount("failures"); }
        }
        public int TestNotRunCount
        {
            get
            {
                return GetTestCount("not-run") +
                       GetTestCount("inconclusive") +
                       GetTestCount("ignored") +
                       GetTestCount("skipped") +
                       GetTestCount("invalid");
            }
        }

        private int GetTestCount(string attributeName)
        {
            return int.Parse(_root.Attribute(attributeName).Value);
        }
    }
}