﻿using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using IntegrationPortal.Models;
using IntegrationPortal.Properties;
using JetBrains.Annotations;

namespace IntegrationPortal.Controllers
{
    public class HomeController : Controller
    {
        [AssertionMethod]
        private void ValidateBrowserName(string browserName)
        {
            if (!Regex.IsMatch(browserName, @"^\w+$"))
            {
                throw new HttpException(400, "Bad Request");
            }
        }
        [AssertionMethod]
        private static void ValidateDateTimeString(string dateTimeString)
        {
            if (!Regex.IsMatch(dateTimeString, @"^\w+$"))
            {
                throw new HttpException(400, "Bad Request");
            }
        }
        [AssertionMethod]
        private static void ValidateHash(string hash)
        {
            if (!Regex.IsMatch(hash, @"^[A-Za-z0-9_=-]+$"))
            {
                throw new HttpException(400, "Bad Request");
            }
        }

        public ActionResult Index()
        {
            var baseTestResultsDirectory = Settings.Default.BaseTestResultsDirectory;
            var dates =
                from directoryFullName in Directory.GetDirectories(baseTestResultsDirectory)
                let directoryBaseName = Path.GetFileName(directoryFullName)
                let identifier = new TestRunIdentifier(directoryBaseName)
                orderby identifier.DateTime descending
                group identifier by identifier.DateTime.Date;
            return View(dates);
        }

        public ActionResult TestRun(string dateTimeString)
        {
            ValidateDateTimeString(dateTimeString);

            var testRunIdentifier = new TestRunIdentifier(dateTimeString);
            var testRunDirectory = testRunIdentifier.TestResultsDirectory;
            var browserTestRuns =
                from browserTestRunDirectory in Directory.GetDirectories(testRunDirectory)
                let browserName = Path.GetFileName(browserTestRunDirectory)
                let browserTestRunIdentifier = new BrowserTestRunIdentifier(testRunIdentifier, browserName)
                let browserTestResultPath = Path.Combine(browserTestRunDirectory, "TestResult.xml")
                let content = XDocument.Load(browserTestResultPath)
                select new BrowserTestRunDetail(browserTestRunIdentifier, content);
            var model = new TestRunDetail(testRunIdentifier, browserTestRuns);
            return View(model);
        }

        public ActionResult Screenshot(string dateTimeString, string hash, string browserName)
        {
            ValidateDateTimeString(dateTimeString);
            ValidateHash(hash);
            ValidateBrowserName(browserName);

            var testRunIdentifier = new TestRunIdentifier(dateTimeString);
            var browserTestRunIdentifier = new BrowserTestRunIdentifier(testRunIdentifier, browserName);
            var path = browserTestRunIdentifier.GetScreenshotPathFromHash(hash);
            if (!System.IO.File.Exists(path))
            {
                return HttpNotFound();
            }

            return File(path, "image/png");
        }
    }
}