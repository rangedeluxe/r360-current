# This script runs the Selenium integration tests, then saves the test results on
# a network share and sends a success/failure e-mail.
#
# Usage:
# First build the IntegrationTests solution.
# Then, either:
#   - Browse out to the $ArtifactDirectory and enter your QALabs credentials into
#     Windows Explorer, then run this script from its location in version control;
#   - Or copy this script, the NUnit runner distributables, and the contents of the
#     IntegrationTests project's bin/Release directory all into the same directory
#     on a VM in qalabs.nwk, and then run this script from there.

param (
    [string] $EmailList,
    [string[]] $Browsers
)

$ErrorActionPreference = 'Stop'

$DateTimeString = Get-Date -Format yyyyMMdd_HHmmss
$ArtifactDirectory = Join-Path "\\RecHubIntWeb01.qalabs.nwk\IntegrationTestResults" $DateTimeString

# Running NUnit

function FindInPath($FileName) {
    $Paths = @(".") + $env:Path.Split(";")
    $Paths | %{ Join-Path $_ $FileName } | ?{ Test-Path $_ }
}

function GetBrowsers {
    if ($Browsers) {
        $Browsers
    } else {
        "Firefox"
        if (FindInPath "ChromeDriver.exe") {
            "Chrome"
        }
        if (FindInPath "IEDriverServer.exe") {
            "IE"
        }
    }
}

function GetNewestFile($Potentials) {
    $ExistingPaths = $Potentials | ?{ Test-Path $_ }
    $FileInfos = $ExistingPaths | %{ Get-Item $_ }
    $NewestFilesFirst = $FileInfos | Sort-Object LastWriteTime -Descending
    $NewestFile = $NewestFilesFirst | Select-Object -First 1
    if (!$NewestFile) {
        throw "Could not find file: " + (Split-Path -Leaf $Potentials[0])
    }
    $NewestFile
}

function NUnitExePath {
    $NUnitExeFileName = "nunit-console.exe"
    GetNewestFile @(
        $NUnitExeFileName
        Join-Path "packages/NUnit.Runners.2.6.4/tools" $NUnitExeFileName
    )
}

function GetTestDllPath($BaseName) {
    $DllFileName = $BaseName + ".dll"
    GetNewestFile @(
        $DllFileName
        Join-Path "$BaseName/bin/Debug" $DllFileName
        Join-Path "$BaseName/bin/Release" $DllFileName
    )
}

function TestDllPaths {
    GetTestDllPath "IntegrationTests"
    GetTestDllPath "IntegrationExcelTests"
}

function RunNUnit {
    $NUnitExe = NUnitExePath
    $Args = (TestDllPaths | %{ '"' + $_ + '"' }) -join " "

    Write-Host "Starting NUnit..." -ForegroundColor Cyan
    # Force NUnit to run in .NET 4.x even though NUnit 2.x is built for .NET 3.5
    $env:COMPLUS_Version = "v4.0.30319"
    try {
        $Process = Start-Process $NUnitExe -Args $Args -NoNewWindow -PassThru
        Wait-Process -InputObject $Process
    } finally {
        Remove-Item env:\COMPLUS_Version
    }
}

# Gathering test artifacts

function GetArtifacts {
    Get-ChildItem -Filter TestResult.xml
    TestDllPaths | %{
        Get-ChildItem (Split-Path $_ -Parent) -Filter *.png
    }
}

function RemoveLocalArtifacts {
    GetArtifacts | Remove-Item
}

function SaveArtifacts($BrowserName) {
    $BrowserArtifactDirectory = Join-Path $ArtifactDirectory $BrowserName
    Write-Host "Saving artifacts to $BrowserArtifactDirectory..." -ForegroundColor Cyan
    New-Item $BrowserArtifactDirectory -Type Directory -Force | Out-Null
    GetArtifacts | %{ Copy-Item $_.FullName $BrowserArtifactDirectory }
}

# Building e-mail

function GetTestResults {
    $Total = 0
    $Failed = 0
    $NotRun = 0

    Get-ChildItem $ArtifactDirectory | %{
        [xml] $TestResultXml = Get-Content (Join-Path $_.FullName "TestResult.xml")
        $Stats = $TestResultXml["test-results"]
        $Total += [int] $Stats.total
        $Failed += ([int] $Stats.errors) + ([int] $Stats.failures)
        $NotRun += ([int] $Stats["not-run"]) + ([int] $Stats.inconclusive) + ([int] $Stats.ignored) + ([int] $Stats.skipped) + ([int] $Stats.invalid)
    }

    @{
        Total = $Total
        Failed = $Failed
        NotRun = $NotRun
    }
}

function GetEmailRecipients {
    if ($EmailList) {
        $EmailList.Split(',')
    } else {
        @("R360DevelopmentTeam@wausaufs.com")
    }
}

function GetEmailSubject($TestResults) {
    if ($TestResults.Failed) {
        "R360 current integration tests FAILED"
    } elseif ($TestResults.NotRun) {
        "R360 current integration tests partially passed"
    } else {
        "R360 current integration tests passed"
    }
}

function GetEmailHeaderLine($TestResults) {
    $SummaryLine = "<p>`n"

    $SummaryLine += "Summary: $($TestResults.Total) test"
    if ($TestResults.Total -ne 1) {
        $SummaryLine += "s"
    }
    $SummaryLine += ",`n"

    if ($TestResults.Failed) {
        $SummaryLine += "<span style='color: red'>"
    } else {
        $SummaryLine += "<span>"
    }
    $SummaryLine += "$($TestResults.Failed) failed</span>,`n"

    if ($TestResults.NotRun) {
        $SummaryLine += "<span style='color: #FF9900'>"
    } else {
        $SummaryLine += "<span>"
    }
    $SummaryLine += "$($TestResults.NotRun) not run</span>`n"
    $SummaryLine += "</p>`n"

    $SummaryLine
}

function GetEmailSubheaderLine($TestResults) {
    if ($TestResults.Failed -or $TestResults.NotRun) {
        ""
    } else {
        "<p style='color: #090'>All tests passed</p>`n"
    }
}

function GetEmailArtifactLink {
    "<p><a href='http://r360labs.qalabs.nwk/IntegrationPortal/TestRun/$DateTimeString'>Show details</a></p>`n"
}

function GetEmailBody($TestResults) {
    $FontStyle = "font-family: Segoe UI; font-size: 80%"
    $Body = "<html><body style='$FontStyle'>`n"

    $Body += GetEmailHeaderLine $TestResults
    $Body += GetEmailSubheaderLine $TestResults
    $Body += GetEmailArtifactLink

    $Body += "</body></html>"
    $Body
}

function SendEmailNotification {
    $TestResults = GetTestResults

    $Recipients = GetEmailRecipients
    $Body = GetEmailBody $TestResults

    $Message = New-Object System.Net.Mail.MailMessage
    $Message.Subject = GetEmailSubject $TestResults
    $Message.From = New-Object System.Net.Mail.MailAddress("R360 Integration Tests R360DevelopmentTeam@wausaufs.com")
    foreach($Recipient in $Recipients) {
        $Message.To.Add($Recipient)
    }
    $Message.IsBodyHTML = $True
    $Message.Body = $Body
    $Smtp = New-Object Net.Mail.SmtpClient("owa.wausaufs.net")

    Write-Host "Sending e-mail notification: $($Message.Subject)" -ForegroundColor Cyan
    $Smtp.Send($Message)
}

GetBrowsers | %{
    Write-Host "Running tests in browser: $_" -ForegroundColor Cyan
    $env:IntegrationBrowser = $_
    RemoveLocalArtifacts
    RunNUnit
    SaveArtifacts $_
    Write-Host
}
SendEmailNotification