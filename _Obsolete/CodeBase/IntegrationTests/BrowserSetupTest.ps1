param (
    [string] $Browser
)

$ErrorActionPreference = 'Stop'

function GetNewestFile($Potentials) {
    $ExistingPaths = $Potentials | ?{ Test-Path $_ }
    $FileInfos = $ExistingPaths | %{ Get-Item $_ }
    $NewestFilesFirst = $FileInfos | Sort-Object LastWriteTime -Descending
    $NewestFile = $NewestFilesFirst | Select-Object -First 1
    if (!$NewestFile) {
        throw "Could not find file: " + (Split-Path -Leaf $Potentials[0])
    }
    $NewestFile
}

function GetSeleniumDllPath {
    $SeleniumDllFileName = "WebDriver.dll"
    GetNewestFile @(
        $SeleniumDllFileName
        Join-Path "IntegrationTests/bin/Debug" $SeleniumDllFileName
        Join-Path "IntegrationTests/bin/Release" $SeleniumDllFileName
    )
}

function GetIntegrationTestFrameworkDllPath {
    $IntegrationTestFrameworkDllFileName = "IntegrationTestFramework.dll"
    GetNewestFile @(
        $IntegrationTestFrameworkDllFileName
        Join-Path "IntegrationTests/bin/Debug" $IntegrationTestFrameworkDllFileName
        Join-Path "IntegrationTests/bin/Release" $IntegrationTestFrameworkDllFileName
    )
}

Add-Type -Path (GetSeleniumDllPath)
Add-Type -Path (GetIntegrationTestFrameworkDllPath)

function CreateDriver {
    switch ($Browser) {
        "Firefox" { New-Object OpenQA.Selenium.Firefox.FirefoxDriver }
        "Chrome" { New-Object OpenQA.Selenium.Chrome.ChromeDriver }
        "IE" { New-Object OpenQA.Selenium.IE.InternetExplorerDriver }
    }
}

$FrameworkAssembly = [IntegrationTestFramework.Ui].Assembly
$ConfigurationType = $FrameworkAssembly.GetType("IntegrationTestFramework.Configuration") # internal visibility
$Url = $ConfigurationType.GetField("InitialUrl").GetValue($Null)

$Driver = CreateDriver
if (!$Driver) {
    Write-Host "Usage: $($MyInvocation.ScriptName) <browsername>"
    Write-Host "where <browsername> is 'Firefox', 'Chrome', or 'IE'"
} else {
    $Driver.Navigate().GoToURL($Url)
}
