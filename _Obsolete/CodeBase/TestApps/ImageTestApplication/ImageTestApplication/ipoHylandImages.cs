using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Hyland.Public;
using System.Data;
using System.Data.OleDb;
using System.Xml;
using System.Drawing;
using System.Drawing.Imaging;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2011 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* CR 32562 JMC 03/07/2011 
*    -Modified GetImage() method to accept IItem interface instead of cImage object.
*    -Modified GetRelativeImagePath() method to accept IItem interface instead of cImage object.
*    -Modified GetHylandDocument() method to accept IItem interface instead of cImage object.
* CR 47093 JNE 10/4/2011
*    -Enhanced logging while in verbose mode for ipoHylandImages
* CR 47984 WJS 11/8/2011
*	 -Add support to get all images needed for Hyland upfront. Store items in a list
* CR 48387 WJS 11/17/2011
*	 -Add ability for single image to work again
*	- Check batch sequence to ensure you are actually displaying right image
* CR 48475 WJS 11/24/2011
*	- Support for image output path to be passed in
*	- Write out images on getRelative path call to image output path
*   - Create common GetImageData method
 *   CR 49190 WJS  1/13/2012
 *   - FP: Don't only use batchsequence for view all. Use isCheck in addition.
******************************************************************************/
namespace WFS.integraPAY.Online.Common
{

   
  
    /// <summary></summary>
    public interface IItem
    {

        /// <summary></summary>
        int BatchSiteCode
        {
            get;
        }

        /// <summary></summary>
        int BankID
        {
            get;
        }

        /// <summary></summary>
        int LockboxID
        {
            get;
        }

        /// <summary></summary>
        int ProcessingDateKey
        {
            get;
        }

        /// <summary></summary>
        string PICSDate
        {
            get;
        }

        /// <summary></summary>
        DateTime DepositDate
        {
            get;
        }

        /// <summary></summary>
        int BatchID
        {
            get;
        }

        /// <summary></summary>
        int TransactionID
        {
            get;
        }

        /// <summary></summary>
        int TxnSequence
        {
            get;
        }

        /// <summary></summary>
        int BatchSequence
        {
            get;
        }

        /// <summary></summary>
        string FileDescriptor
        {
            get;
        }

        /// <summary></summary>
        LockboxColorMode DefaultColorMode
        {
            get;
        }
    }
    public class HylandDocument
    {
        private long m_lExternalDocumentID = -1;
        private long m_lFileSize = -1;
        private string m_sSide = string.Empty;
        private string m_sColorMode = string.Empty;
        private int m_iPage = -1;
        private int m_iBatchSequence = -1;
        private bool m_bIsCheck = false;

     

        public HylandDocument()
        {

        }

        public HylandDocument(long lExternalDocumentID, long lFileSize, string sSide, string sColorMode, int iPage, bool bIsCheck)
        {
            m_lExternalDocumentID = lExternalDocumentID;
            m_lFileSize = lFileSize;
            m_sSide = sSide;
            m_sColorMode = sColorMode;
            m_iPage = iPage;
            m_bIsCheck = bIsCheck;
        }
        
        public HylandDocument(int iBatchSequence, long lExternalDocumentID, long lFileSize, string sSide, string sColorMode, int iPage, bool bIsCheck) :
                this(lExternalDocumentID, lFileSize,  sSide,  sColorMode,  iPage, bIsCheck)
        {
            m_iBatchSequence = iBatchSequence;
        }

        public int BatchSequence
        {
            get { return (m_iBatchSequence); }
            set { m_iBatchSequence = value; }
        }
        public long ExternalDocumentID
        {
            get { return (m_lExternalDocumentID); }
            set { m_lExternalDocumentID = value; }
        }

        public long FileSize
        {
            get { return (m_lFileSize); }
            set { m_lFileSize = value; }
        }

        public string Side
        {
            get { return (m_sSide); }
            set { m_sSide = value; }
        }

        public string ColorMode
        {
            get { return (m_sColorMode); }
            set { m_sColorMode = value; }
        }

        public int Page
        {
            get { return (m_iPage); }
            set { m_iPage = value; }
        }

        public bool IsCheck
        {
            get { return (m_bIsCheck); }
            set { m_bIsCheck = value; }
        }
    }

    public class ipoHylandImages 
    {
        // Create a request list.
        private IRequestList _requestList = null;
        private string m_sWebServiceURL = string.Empty;
        private string m_sessionID = string.Empty;
        private List<HylandDocument> m_listHylandDocuments;

        private string m_sOutputRootPath = string.Empty;
        ////private bool m_bIsConnected = false;

        /// <summary>
        /// constructor of the Hyland interface object
        /// </summary>
        public ipoHylandImages(string sSiteKey, 
                               string sWebServiceURL, 
                               string sDatasource, 
                               string sLicenseType, 
                               string sUserName, 
                               string sPassword) {

            string strEventMessage = string.Empty;

            try
            {
                _requestList = (IRequestList)Factory.CreateObject(typeof(IRequestList));
                m_listHylandDocuments = new List<HylandDocument>();
                

                m_sWebServiceURL = sWebServiceURL;               

                IRequest request = (IRequest)Factory.CreateObject(typeof(IRequest));

                // Specify the "OnBase" service provider to handle the request.
                request.ServiceProvider = "OnBase";

                // Set the request type to "Connect".
                request.Name = "Connect";

                // Load the OnBase login credentials to the request.
                request.AddParameter("datasource", sDatasource);
                request.AddParameter("username", sUserName);
                request.AddParameter("password", sPassword);
                request.AddParameter("licenseType", sLicenseType);

                //Add Logging in Verbose mode
                strEventMessage = "Loading ipoHylandImages object. Request object inputs:  ServiceProvider='OnBase', Name='Connect', DataSource='"+sDatasource;
                strEventMessage += "', username='"+sUserName+"', licenseType='"+sLicenseType+"' ";
               

                // Create a request list.  
                IRequestList requestList = (IRequestList)Factory.CreateObject(typeof(IRequestList));

                // Next, add the loaded request to the request list.
                requestList.Add(request);

                // Create the service client and add the URL for the location of the Service.asmx file.
                ISoapServiceClient onbase = (ISoapServiceClient)Factory.CreateObject(typeof(ISoapServiceClient));
                onbase.URL = m_sWebServiceURL;

                // Execute the request list and obtain the responses.
                IResponseList responseList = onbase.Execute(requestList);

                // Obtain the first response.
                IResponse response = (IResponse)responseList[0];

                // Check for errors on the server side by using the Validate() call on the response.
                // If an error is found, Validate() will throw a ServiceException.
                response.Validate();

                // Generate a parameter list from the response.
                IParameterList parameterList = response.Parameters;

                // Access the sessionID that was generated.  The sessionID is a parameter in the response list.
                m_sessionID = parameterList.Values["sessionID"].ToString();

                //Add Logging in Verbose mode
               // strEventMessage = "Loaded Hyland Object and received a successful response.  SiteKey = "+siteKey+", WebServiceURL = "+m_sWebServiceURL+", SessionID = "+m_sessionID;
              //  eventLog.logEvent(strEventMessage,"ipoHylandImages", MessageImportance.Verbose);

                ////m_bIsConnected = true;
            }
            catch (Hyland.Public.ServiceException ex)
            {
              //  eventLog.logError(ex);
                throw new Exception(ex.Error.ToString());
            }
            catch (Exception ex)
            {
              //  eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
        }
        
        /// <summary>
        /// Function used to get the path (without the IMG directory)
        /// for a specified image and desired color mode.
        /// </summary>
        /// <param name="imageRequest">Image object to be located.</param>
        /// <param name="iPage">Page (0=Front, 1=Back)</param>
        /// <returns>Path of image relative to the Image root path</returns>
     /*   public string GetRelativeImagePath(IItem imageRequest, int iPage)
        {
            StringBuilder sbRet = new StringBuilder();
            string strEventMessage = string.Empty;
            string completeTempFile = string.Empty;
            try
            {
                // we want to return
                // PICSDATE\BANK\LOCKBOX\BATCH_BATCHSEQUENCE_FILEDESCRIPTOR_FRONTBACK.TIF
                // ProcDate = 5/18/2009
                // Bank = 53
                // Lockbox = 123
                // Batch = 100
                // BatchSequence = 1
                // FileDesc = "C"
                // FrontBack = Front Bitonal = "F" or Front Color = "FC" (for example)
                // 20090518\53\123\100_1_C_F.TIF
                // this is all based on the desired lockbox color mode

                HylandDocument hdDocument = null;

                bool bRet = true;
               // bool bRet = GetHylandDocument(imageRequest, iPage, out hdDocument);                

                if (bRet)
                {
                    byte[] ImageBytes;
                    bRet = GetImageData(hdDocument, out ImageBytes);

                    

                    string sFrontBack = string.Empty;
                    string sColor = string.Empty;

                    sbRet.AppendFormat("{0}_", imageRequest.PICSDate);
                    sbRet.AppendFormat("{0}_", imageRequest.BankID);
                    sbRet.AppendFormat("{0}_", imageRequest.LockboxID);
                    sbRet.AppendFormat("{0}_", imageRequest.BatchID);
                    sbRet.AppendFormat("{0}_", imageRequest.BatchSequence);
                    sbRet.AppendFormat("{0}_", imageRequest.FileDescriptor);

                    if(hdDocument.Side.ToUpper() == "FRONT")
                        sFrontBack = "F";

                    if(hdDocument.ColorMode.ToUpper() == "GRAYSCALE")
                        sColor = "G";
                    else if(hdDocument.ColorMode.ToUpper() == "COLOR")
                        sColor = "C";

                    sbRet.AppendFormat("{0}{1}.TIF", sFrontBack, sColor.Length > 0 ? sColor : "");

                    //Add Logging in Verbose mode

                    completeTempFile = Path.Combine(m_sOutputRootPath, sbRet.ToString());
                    strEventMessage = "GetRelativeImagePath method:  Path=" + completeTempFile.ToString() + "'";
                  //  eventLog.logEvent(strEventMessage, "ipoHylandImages", MessageImportance.Verbose);
                    File.WriteAllBytes(completeTempFile, ImageBytes);
                }
            }
            catch (System.Exception ex)
            {
                completeTempFile = string.Empty;
                //eventLog.logError(ex);
            }

            return sbRet.ToString();
        }

        */

        public void AddItemToRequestList(HylandDocument hdDocument)
        {
            // Create a new request.
            IRequest request = (IRequest)Factory.CreateObject(typeof(IRequest));

            // Specify the "OnBase" service provider to handle the request.
            request.ServiceProvider = "OnBase";

            IAuthenticationToken authenticationToken = (IAuthenticationToken)Factory.CreateObject(typeof(IAuthenticationToken));
            authenticationToken.AddParameter("sessionID", m_sessionID);

            // Load the authentication token on the request ...
            request.AuthenticationToken = authenticationToken;

            //  Set the request type to "GetDocumentData".
            request.Name = "GetDocumentData";

            // Set the following parameters as shown below to return the OnBase document
            // via a specified format.
            request.AddParameter("docID", hdDocument.ExternalDocumentID.ToString());
            // CR 28966 MEH 02-09-2010 Image backs displaying image fronts when pulling from Hyland                      
            request.AddParameter("contentType", "image/tiff");
            request.AddParameter("pageRange", hdDocument.Page.ToString());


            // Next, add the loaded request to the request list.
            _requestList.Add(request);


        }
        public void ClearRequestList()
        {
            _requestList.Clear();
        }
        public bool RunMultipleImages(int numberItems, out byte[][] ImageBytes)
        {
            bool bRet = false;
            string strEventMessage = string.Empty;
            try
            {
                ImageBytes = new byte[numberItems][];
                //Add Logging in Verbose mode
               

                // Create the service client and add the URL for the location of the Service.asmx file.
                ISoapServiceClient onbase = (ISoapServiceClient)Factory.CreateObject(typeof(ISoapServiceClient));
                onbase.URL = m_sWebServiceURL;

               
                // Execute the request list and obtain the responses.
                IResponseList responseList = onbase.Execute(_requestList);

                // Obtain the first response.
                int count = responseList.Count;
                for (int i = 0; i < count; i++)
                {
                    ImageBytes[i] = null;
                    IResponse response = (IResponse)responseList[i];
                    // Use the "AccessElement" call to lock onto the "documentResult" element.
                    IElement element = response.Parameters.AccessElement("documentResult");

                    // Convert the base64 string (within the DocumentData node) to a byte array.
                    // This contains all of the document data information that we need to view the document. 
                    // From here we can stream the data to a browser if desired to view the document in a web 
                    // application.
                    byte[] byteData = null;
                    byteData = System.Convert.FromBase64String(element.Properties["DocumentData"] as string);

                    ImageBytes[i] = byteData;
                    strEventMessage = "GetImage object:  Successfully retrieved Image";
                }

                bRet = true;

            }
            catch (Hyland.Public.ServiceException ex)
            {
                ImageBytes = null;
                //  eventLog.logError(ex);
                bRet = false;
            }
            return bRet;
        }
        public bool GetImageData(HylandDocument hdDocument, out byte[] ImageBytes)
        {
            bool bRet = false;
            string strEventMessage = string.Empty;
            try
            {

                IRequestList requestList = (IRequestList)Factory.CreateObject(typeof(IRequestList));
                //Add Logging in Verbose mode
                strEventMessage = "GetImage object: retrieves image using document id. Request object inputs:  ServiceProvider = 'OnBase', AuthenticationTokenParameter = 'sessionID', SessionID = '" + m_sessionID;
                strEventMessage += "', Request Name='GetDocumentData', docID='" + hdDocument.ExternalDocumentID.ToString() + "', contentType='image/tiff', pageRange='" + hdDocument.Page.ToString() + "' ";
              //  eventLog.logEvent(strEventMessage, "ipoHylandImages", MessageImportance.Verbose);

                // Create the service client and add the URL for the location of the Service.asmx file.
                ISoapServiceClient onbase = (ISoapServiceClient)Factory.CreateObject(typeof(ISoapServiceClient));
                onbase.URL = m_sWebServiceURL;

                // Create a new request.
                IRequest request = (IRequest)Factory.CreateObject(typeof(IRequest));

                // Specify the "OnBase" service provider to handle the request.
                request.ServiceProvider = "OnBase";

                IAuthenticationToken authenticationToken = (IAuthenticationToken)Factory.CreateObject(typeof(IAuthenticationToken));
                authenticationToken.AddParameter("sessionID", m_sessionID);

                // Load the authentication token on the request ...
                request.AuthenticationToken = authenticationToken;

                //  Set the request type to "GetDocumentData".
                request.Name = "GetDocumentData";

                // Set the following parameters as shown below to return the OnBase document
                // via a specified format.
                request.AddParameter("docID", hdDocument.ExternalDocumentID.ToString());
                // CR 28966 MEH 02-09-2010 Image backs displaying image fronts when pulling from Hyland                      
                request.AddParameter("contentType", "image/tiff");
                request.AddParameter("pageRange", hdDocument.Page.ToString());


                // Next, add the loaded request to the request list.
                requestList.Add(request);
                // Execute the request list and obtain the responses.
                IResponseList responseList = onbase.Execute(requestList);

                // Obtain the first response.
                IResponse response = (IResponse)responseList[0];

                // Check for errors on the server side by using the Validate() call on the response.
                // If an error is found, Validate() will throw a ServiceException.
                response.Validate();

                // Use the "AccessElement" call to lock onto the "documentResult" element.
                IElement element = response.Parameters.AccessElement("documentResult");

                // Convert the base64 string (within the DocumentData node) to a byte array.
                // This contains all of the document data information that we need to view the document. 
                // From here we can stream the data to a browser if desired to view the document in a web 
                // application.
                byte[] byteData = null;
                byteData = System.Convert.FromBase64String(element.Properties["DocumentData"] as string);

                ImageBytes = byteData;
                strEventMessage = "GetImage object:  Successfully retrieved Image";
              //  eventLog.logEvent(strEventMessage, "ipoHylandImages", MessageImportance.Verbose);
                bRet = true;

            }
            catch (Hyland.Public.ServiceException ex)
            {
                ImageBytes = null;
              //  eventLog.logError(ex);
                bRet = false;
            }
            return bRet;
        }

        /// <summary>
        /// Function used to get the image from the Hyland
        /// image storage area.
        /// </summary>
        /// <param name="imageRequest">Image object to be located.</param>
        /// <param name="iPage">Page (0=Front, 1=Back)</param>
        /// <returns>Memory stream of the image</returns>
   /*     public bool GetImage(IItem imageRequest, int iPage, out byte[] ImageBytes)
        {
            bool bRet = true;
            bool bIsCheck  = false;
            string strEventMessage = string.Empty;
            try
            {
                List<HylandDocument> filteredlistHylandDocuments = new List<HylandDocument>();
                HylandDocument hdDocument = null;
                //check the batch Sequence
                if (m_listHylandDocuments.Count == 0)
                {
                    bRet = GetHylandDocument(imageRequest, iPage, out hdDocument);
                }
                else       
                {
                    bIsCheck = imageRequest is cCheck ? true : false;
                    filteredlistHylandDocuments = m_listHylandDocuments.Where(item => item.BatchSequence == imageRequest.BatchSequence && item.IsCheck == bIsCheck).ToList<HylandDocument>();
                    if (filteredlistHylandDocuments.Count > 0)
                    {
                        bRet = GetHylandDocument(filteredlistHylandDocuments,imageRequest.DefaultColorMode, iPage, out hdDocument);
                    }
                  
                }

                if (bRet)
                {
                    bRet= GetImageData(hdDocument, out ImageBytes);
                   
                } else {
                    ImageBytes = null;
                    strEventMessage = "GetHylandDocument method did not return successfully.";
                //    eventLog.logEvent(strEventMessage,"ipoHylandImages", MessageImportance.Verbose);

                }
            }
            catch (System.Exception ex)
            {
                ImageBytes = null;
                bRet = false;
              //  eventLog.logError(ex);
            }            

            return bRet;
        }*/

      /*  public bool GetAllHylandDocumentsForABatch(IItem imageRequest )
        {
            bool bRet = true;

            DataTable dt = new DataTable();
            try
            {
                m_listHylandDocuments = new List<HylandDocument>();
                string sImageInfoXML = string.Empty;

                HylandDocument objHylandDoc;
        */
                // get the XML from the factChecks/factDocuments table     
          /*      bRet = ItemProcDAL.GetImageInfo(imageRequest.BankID,
                                                imageRequest.LockboxID,
                                                imageRequest.ProcessingDateKey,
                                                imageRequest.BatchID,
                                                -1,
                                                Convert.ToInt32(imageRequest.DepositDate.ToString("yyyyMMdd")),
                                                TableTypes.Checks,
                                                out dt);

                if (bRet && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        objHylandDoc = new HylandDocument((int) dr["BatchSequence"],
                                                            (int)dr["ExternalDocumentID"],
                                                                         (int)dr["FileSize"],
                                                                         dr["Side"].ToString(),
                                                                         dr["ColorMode"].ToString(),
                                                                         (int)dr["Page"],
                                                                        true);   // it is check
                        m_listHylandDocuments.Add(objHylandDoc);
                    }
                }
                bRet = ItemProcDAL.GetImageInfo(imageRequest.BankID,
                                               imageRequest.LockboxID,
                                               imageRequest.ProcessingDateKey,
                                               imageRequest.BatchID,
                                               -1,
                                               Convert.ToInt32(imageRequest.DepositDate.ToString("yyyyMMdd")),
                                               TableTypes.Documents,
                                               out dt);
                if (bRet && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        objHylandDoc = new HylandDocument((int)dr["BatchSequence"], 
                                                            (int)dr["ExternalDocumentID"],
                                                                         (int)dr["FileSize"],
                                                                         dr["Side"].ToString(),
                                                                         dr["ColorMode"].ToString(),
                                                                         (int)dr["Page"],
                                                                         false);
                        m_listHylandDocuments.Add(objHylandDoc);
                    }
                }
                
            }
            catch (System.Exception ex)
            {
                bRet = false;
                //eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            finally {
                if (dt != null) {
                    dt.Dispose();
                    dt = null;
                }
            }
            return bRet;
        }

        private bool GetHylandDocument(List<HylandDocument> searchHylandDocList, LockboxColorMode DefaultColorMode, int iFrontBack, out HylandDocument hdDocument)
        {
            bool bRet = true;

            hdDocument = new HylandDocument();
            try
            {
                List<HylandDocument> filteredlistHylandDocuments = new List<HylandDocument>();
                string sMySide = string.Empty;

                switch (iFrontBack)
                {
                    case 0:
                    //case cipoImages.PAGE_FRONT:
                        sMySide = "FRONT";
                        break;
                    case 1:
                    //case cipoImages.PAGE_BACK:
                        sMySide = "REAR";
                        break;
                }

                switch (DefaultColorMode)
                {
                    case LockboxColorMode.COLOR_MODE_BITONAL:
                        // LINQ!  
                        filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "BITONAL").ToList<HylandDocument>();
                        if (filteredlistHylandDocuments.Count > 0)
                            hdDocument = filteredlistHylandDocuments[0];
                        else
                        {
                            filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "GRAYSCALE").ToList<HylandDocument>();
                            if (filteredlistHylandDocuments.Count > 0)
                                hdDocument = filteredlistHylandDocuments[0];
                            else
                            {
                                filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "COLOR").ToList<HylandDocument>();
                                if (filteredlistHylandDocuments.Count > 0)
                                    hdDocument = filteredlistHylandDocuments[0];
                                else
                                {
                                    // can't find it
                                    bRet = false;
                                }
                            }
                        }
                        break;
                    case LockboxColorMode.COLOR_MODE_GRAYSCALE:
                        filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "GRAYSCALE").ToList<HylandDocument>();
                        if (filteredlistHylandDocuments.Count > 0)
                            hdDocument = filteredlistHylandDocuments[0];
                        else
                        {
                            filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "COLOR").ToList<HylandDocument>();
                            if (filteredlistHylandDocuments.Count > 0)
                                hdDocument = filteredlistHylandDocuments[0];
                            else
                            {
                                filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "BITONAL").ToList<HylandDocument>();
                                if (filteredlistHylandDocuments.Count > 0)
                                    hdDocument = filteredlistHylandDocuments[0];
                                else
                                {
                                    // can't find it
                                    bRet = false;
                                }
                            }
                        }
                        break;
                    case LockboxColorMode.COLOR_MODE_COLOR:
                        filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "COLOR").ToList<HylandDocument>();
                        if (filteredlistHylandDocuments.Count > 0)
                            hdDocument = filteredlistHylandDocuments[0];
                        else
                        {
                            filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "GRAYSCALE").ToList<HylandDocument>();
                            if (filteredlistHylandDocuments.Count > 0)
                                hdDocument = filteredlistHylandDocuments[0];
                            else
                            {
                                filteredlistHylandDocuments = searchHylandDocList.Where(item => item.Side.ToUpper() == sMySide &&
                                                            item.ColorMode.ToUpper() == "BITONAL").ToList<HylandDocument>();
                                if (filteredlistHylandDocuments.Count > 0)
                                    hdDocument = filteredlistHylandDocuments[0];
                                else
                                {
                                    // can't find it
                                    bRet = false;
                                }
                            }
                        }
                        break;
                }
            }
            catch (System.Exception ex)
            {
                bRet = false;
              //  eventLog.logError(ex);
                throw new Exception(ex.Message);
            }
            return bRet;
        }

        private bool GetHylandDocument(IItem imageRequest, int iFrontBack, out HylandDocument hdDocument)
        {
            bool bRet = true;

            hdDocument = new HylandDocument();
            DataTable dt = new DataTable();
            try
            {
                // iFrontBack = 0 means front
                // iFrontBack = 1 means rear

                // use the ipoDAL to get the XML from ImageInfoXML for the check/document
                string sImageInfoXML = string.Empty;
                List<HylandDocument> listHylandDocuments = new List<HylandDocument>();
                HylandDocument objHylandDoc;

                // get the XML from the factChecks/factDocuments table     
              /*  bRet = ItemProcDAL.GetImageInfo(imageRequest.BankID, 
                                                imageRequest.LockboxID,
                                                imageRequest.ProcessingDateKey,
                                                imageRequest.BatchID,
                                                imageRequest.BatchSequence,
                                                Convert.ToInt32(imageRequest.DepositDate.ToString("yyyyMMdd")),
                                                (imageRequest is cCheck ? TableTypes.Checks : TableTypes.Documents), 
                                                out dt);                

                if(bRet && dt.Rows.Count > 0)
                {

                    foreach(DataRow dr in dt.Rows) {
                        objHylandDoc = new HylandDocument((int)dr["ExternalDocumentID"],
                                                                         (int)dr["FileSize"],
                                                                         dr["Side"].ToString(),
                                                                         dr["ColorMode"].ToString(),
                                                                         (int)dr["Page"],
                                                                        imageRequest is cCheck ? true: false);
                        listHylandDocuments.Add(objHylandDoc);
                    }

                    bRet = GetHylandDocument(listHylandDocuments, imageRequest.DefaultColorMode, iFrontBack, out hdDocument);
                 
                } // if(bRet) // could not get the ImageInfoXML from the database*/
          /*  }
            catch (System.Exception ex)
            {
                bRet = false;
          //      eventLog.logError(ex);
                throw new Exception(ex.Message);                
            }
            finally {
                if (dt != null) {
                    dt.Dispose();
                    dt = null;
                }
            }

            return bRet;
        }*/
        
        public void Dispose()
        {
            string strEventMessage = string.Empty;
            try
            {
                // Create a new request.
                IRequest request = (IRequest)Factory.CreateObject(typeof(IRequest));

                // Specify the "OnBase" service provider to handle the request.
                request.ServiceProvider = "OnBase";

                // Set the request type to "Disconnect".
                request.Name = "Disconnect";

                // Load the sessionID to disconnect from (hard coded here for sake of example). 
                request.AddParameter("sessionID", m_sessionID);

                // Create a request list.
                IRequestList requestList = (IRequestList)Factory.CreateObject(typeof(IRequestList));

                // Next, add the loaded request to the request list.
                requestList.Add(request);
              
                //Add Logging in Verbose mode
                strEventMessage = "Hyland object dispose method. Request object inputs:  ServiceProvider = 'OnBase',  Request Name='Disconnect', SessionID = '"+m_sessionID+"'";
              //  eventLog.logEvent(strEventMessage,"ipoHylandImages", MessageImportance.Verbose);

                // Create the service client and add the URL for the location of the Service.asmx file.
                ISoapServiceClient onbase = (ISoapServiceClient)Factory.CreateObject(typeof(ISoapServiceClient));
                onbase.URL = m_sWebServiceURL;

                // Process the disconnect request.
                IResponseList responseList = onbase.Execute(requestList);

                // Obtain the first response.
                IResponse response = (IResponse)responseList[0];

                // Check for errors on the server side by using the Validate() call on the response.
                // If an error is found, Validate() will throw a ServiceException.
                response.Validate();
                //Add Logging in Verbose mode
                strEventMessage = "Hyland object dispose method. Dispose Successful";
             //   eventLog.logEvent(strEventMessage,"ipoHylandImages", MessageImportance.Verbose);
                ////m_bIsConnected = false;
            }
            catch (Hyland.Public.ServiceException ex)
            {
              //  eventLog.logError(ex);
                throw new Exception(ex.Error.ToString());
            }
        }

        public void SetOutputImagePath(string outputImagePath)
        {
            m_sOutputRootPath = outputImagePath;
        }
  
    }
}
