﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using WFS.integraPAY.Online.Common;

namespace ImageTestApplication
{
    public partial class Form1 : Form
    {
        private const int MAXTOSEND = 30;
        public Form1()
        {
            InitializeComponent();
           
        }

        private void OKBtn_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            byte[][] ImageBytes2;
            List<string> filenames = new List<string>();
            int numItems = 0;
            int allAtOnceCount = 0;
            string sUserName, sPassword;
            cCrypto3DES cryp3DES = new cCrypto3DES();
            string sDatasource = ipoINILib.IniReadValue("Hyland", "Datasource");
            cryp3DES.Decrypt(ipoINILib.IniReadValue("Hyland", "Username"), out sUserName);
            cryp3DES.Decrypt(ipoINILib.IniReadValue("Hyland", "Password"), out sPassword);
            string sLicenseType = ipoINILib.IniReadValue("Hyland", "LicenseType");
            string sWebServiceURL = ipoINILib.IniReadValue("Hyland", "WebServiceURL");
             ipoHylandImages  hylandImage = new ipoHylandImages("IPONLINE", sWebServiceURL, sDatasource, sLicenseType, sUserName, sPassword);
            //ipoHylandImages  hylandImage = new ipoHylandImages(
            using (StreamReader sr = new StreamReader("../../HylandInputDoc.txt"))
            {
                 while (sr.Peek() >= 0)
                 {
                     bool bRet = false;
                     string buff = sr.ReadLine();
                     if (buff.Length > 0)
                     {
                         string[] imageLineDocument = buff.Split(';');
                         long hylandDocument = Convert.ToInt64(imageLineDocument[0]);
                         long fileSize = Convert.ToInt64(imageLineDocument[1]);
                         int page = Convert.ToInt16(imageLineDocument[2]);
                         string sSize = imageLineDocument[3];
                         string sColorMode = imageLineDocument[4];
                         string image = imageLineDocument[5];
                         HylandDocument hyDoc = new HylandDocument(hylandDocument, fileSize, sSize, sColorMode, page, true);
                         if (OneAtATime.Checked)
                         {
                              byte[] ImageBytes;
                              numItems++;
                              bRet = hylandImage.GetImageData(hyDoc, out ImageBytes);
                              File.WriteAllBytes(image, ImageBytes);
                         }
                         else if (allAtOnce.Checked)
                         {
                             filenames.Add(image);
                             numItems++;
                             allAtOnceCount++;
                             hylandImage.AddItemToRequestList(hyDoc);
                             if (allAtOnceCount > MAXTOSEND)
                             {
                                 hylandImage.RunMultipleImages(allAtOnceCount, out ImageBytes2);
                                 string[] fileArray = filenames.ToArray();
                                 for (int i = 0; i < filenames.Count; i++)
                                 {
                                     File.WriteAllBytes(fileArray[i], ImageBytes2[i]);
                                 }
                                 allAtOnceCount = 0;
                                 filenames.Clear();
                                 hylandImage.ClearRequestList();
                             }
                         }
                         else
                         {
                             MessageBox.Show("Nothing checked. terminating");
                             return;

                         }
                       
                     }
                     
                }
                 if (allAtOnce.Checked)
                 {
                     hylandImage.RunMultipleImages(allAtOnceCount, out ImageBytes2);
                     string[] fileArray = filenames.ToArray();
                     for (int i = 0; i < filenames.Count; i++)
                     {
                         File.WriteAllBytes(fileArray[i], ImageBytes2[i]);
                     }
                 }
            }
            hylandImage.Dispose();
            DateTime endTime = DateTime.Now;
            TimeSpan span = endTime.Subtract(startTime);
            MessageBox.Show("total Items: " + numItems.ToString() + " total Seconds: " + span.TotalSeconds.ToString());
        }

        private void Cancel_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
