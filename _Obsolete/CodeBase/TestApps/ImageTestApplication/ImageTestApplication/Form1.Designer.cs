﻿namespace ImageTestApplication
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.OKBtn = new System.Windows.Forms.Button();
            this.Cancel = new System.Windows.Forms.Button();
            this.allAtOnce = new System.Windows.Forms.RadioButton();
            this.OneAtATime = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // OKBtn
            // 
            this.OKBtn.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.OKBtn.Location = new System.Drawing.Point(36, 135);
            this.OKBtn.Name = "OKBtn";
            this.OKBtn.Size = new System.Drawing.Size(75, 23);
            this.OKBtn.TabIndex = 0;
            this.OKBtn.Text = "OK";
            this.OKBtn.UseVisualStyleBackColor = true;
            this.OKBtn.Click += new System.EventHandler(this.OKBtn_Click);
            // 
            // Cancel
            // 
            this.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Cancel.Location = new System.Drawing.Point(165, 135);
            this.Cancel.Name = "Cancel";
            this.Cancel.Size = new System.Drawing.Size(75, 23);
            this.Cancel.TabIndex = 1;
            this.Cancel.Text = "Cancel";
            this.Cancel.UseVisualStyleBackColor = true;
            this.Cancel.Click += new System.EventHandler(this.Cancel_Click);
            // 
            // allAtOnce
            // 
            this.allAtOnce.AutoSize = true;
            this.allAtOnce.Checked = true;
            this.allAtOnce.Location = new System.Drawing.Point(97, 59);
            this.allAtOnce.Name = "allAtOnce";
            this.allAtOnce.Size = new System.Drawing.Size(77, 17);
            this.allAtOnce.TabIndex = 2;
            this.allAtOnce.TabStop = true;
            this.allAtOnce.Tag = "Allatonce";
            this.allAtOnce.Text = "All at Once";
            this.allAtOnce.UseVisualStyleBackColor = true;
            // 
            // OneAtATime
            // 
            this.OneAtATime.AutoSize = true;
            this.OneAtATime.Location = new System.Drawing.Point(97, 94);
            this.OneAtATime.Name = "OneAtATime";
            this.OneAtATime.Size = new System.Drawing.Size(92, 17);
            this.OneAtATime.TabIndex = 3;
            this.OneAtATime.Tag = "Oneatatime";
            this.OneAtATime.Text = "One at a Time";
            this.OneAtATime.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(247, 180);
            this.Controls.Add(this.OneAtATime);
            this.Controls.Add(this.allAtOnce);
            this.Controls.Add(this.Cancel);
            this.Controls.Add(this.OKBtn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Image Test Application";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OKBtn;
        private System.Windows.Forms.Button Cancel;
        private System.Windows.Forms.RadioButton allAtOnce;
        private System.Windows.Forms.RadioButton OneAtATime;
    }
}

