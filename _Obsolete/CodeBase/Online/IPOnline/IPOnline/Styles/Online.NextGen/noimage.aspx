﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
  <title>Image Unavailable</title>
    <link rel="stylesheet" type="text/css" href="stylesheet.css" />
</head>
<body>
    <br /><br />
    <h3 style="text-align: center;">Image Unavailable</h3>
    <p style="text-align: center;">
        The image you requested is not available at this time.
    </p>
</body>
</html>
