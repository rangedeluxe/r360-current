<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>
  <xsl:output method="html"/>

  <xsl:template name="Title">
    Query Summary
  </xsl:template>

  <xsl:template name="PageTitle">
    <span class="contenttitle">Query Summary</span>
    <!-- span class="contentsubtitle">&nbsp;</span -->
  </xsl:template>

  <xsl:template match="Page">
    <div class="page-portlet-view">
      <form method="post" onSubmit="return false;">
        <xsl:attribute name="name">frmMaintainQueries</xsl:attribute>

        <xsl:if test="Form[@Name='frmMaintainQueries']">
          <xsl:apply-templates select="Form[@Name = 'frmMaintainQueries']" mode="frmMaintainQueries"/>
        </xsl:if>

        <xsl:if test="Recordset[@Name='GetSavedQueries']">
          <xsl:apply-templates select="Recordset[@Name = 'GetSavedQueries']" mode="GetSavedQueries"/>
        </xsl:if>
      </form>
    </div>
  </xsl:template>

  <xsl:template match="Form" mode="frmMaintainQueries">
    <table align="center" width="98%" cellpadding="3" cellspacing="0" border="0">
      <input type="hidden" name="txtAction" value=""/>
      <input type="hidden" name="txtPredefSearchID" value=""/>
      <input type="hidden" name="txtPredefSearchID_Uncheck" value=""/>
    </table>
  </xsl:template>

  <xsl:template match="Recordset" mode="GetSavedQueries">
    <xsl:variable name="startrecord">
      <xsl:value-of select="../PageInfo/@StartRecord"/>
    </xsl:variable>

    <input type="hidden" name="txtStart">
      <xsl:attribute name="value">
        <xsl:value-of select="$startrecord" />
      </xsl:attribute>
    </input>
    <p/>
    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td>
          &nbsp;
        </td>
      </tr>
      <tr>
        <td align="left" valign="top">
          <table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid">
            <tr>
              <td class="grid-header grid-header-left">Query</td>
              <td class="grid-header">Query Description</td>
              <td class="grid-header cell-center-align">Default</td>
              <td class="grid-header cell-center-align">Edit</td>
              <td class="grid-header cell-center-align">Delete</td>
            </tr>

            <xsl:for-each select="Record">
              <tr>
                <td class="grid-cell grid-cell-left">
                  <xsl:value-of select="@Name"/>
                </td>
                <td class="grid-cell">
                  <xsl:value-of select="@Description"/>
                </td>
                <td class="grid-cell cell-center-align">
                  <input type="radio" name="IsDefaultGroup">
                    <xsl:attribute name="id">rb_<xsl:value-of select="@PredefSearchID"/>
                    </xsl:attribute>
                    <xsl:if test = "@IsDefault = 'True'">
                      <xsl:attribute name="CHECKED">CHECKED</xsl:attribute>
                    </xsl:if>
                    <xsl:attribute name="onClick">checkbtnclick(this)</xsl:attribute>
                  </input>
                </td>
                <td class="grid-cell cell-center-align">
                  <a class="showTip" title="Edit">
                    <xsl:attribute name="href">lockboxsearch.aspx?Action=selsavequery&amp;PreDefSearchID=<xsl:value-of select="@PredefSearchID"/></xsl:attribute>
                    <span style="display:block;"><i class="fa fa-search"></i></span>
                  </a>
                </td>
                <td class="grid-cell cell-center-align">
                  <a class="showTip" href="#" title="Delete">
                    <xsl:attribute name="onClick">deleteclick('<xsl:value-of select="@PredefSearchID"/>')</xsl:attribute>
                    <span style="display:block;"><i class="fa fa-times icon-large"></i></span>
                  </a>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>

      <xsl:call-template name="PaginationLinks">
        <xsl:with-param name="controlname">document.forms['frmMaintainQueries'].txtAction</xsl:with-param>
        <xsl:with-param name="startrecord">
          <xsl:value-of select="$startrecord" />
        </xsl:with-param>
        <xsl:with-param name="colSpan">5</xsl:with-param>
      </xsl:call-template>

      <tr>
        <td colspan="5" style="height: 3px;">&nbsp;</td>
      </tr>

    </table>
    <script language="javascript">
      <xsl:comment>
        // Hide scripts from older browsers
        var RadioInputStateCollection = new Array();
        window.onload = function () {GetPageFormRadioState();}
        function GetPageFormRadioState(){
        var InputCollection = document.getElementsByTagName("INPUT");
        for (var i = 0; i&lt;InputCollection.length; i++) {
        if (InputCollection[i].type == "radio"){
        if (InputCollection[i].checked){
        RadioInputStateCollection[InputCollection[i].name] = InputCollection[i].id;
        }
        }
        }
        }
        function checkbtnclick(sender){
        var originStateRadioID = RadioInputStateCollection[sender.name];
        //Code here to call maintenance query with new,old guid
        document.forms['frmMaintainQueries'].txtPredefSearchID.value = sender.id.substr(3);
        if (typeof originStateRadioID != "undefined"){
        document.forms['frmMaintainQueries'].txtPredefSearchID_Uncheck.value = originStateRadioID.substr(3);
        }
        document.forms['frmMaintainQueries'].txtAction.value = 'update';
        InsertSecurityToken(document.forms['frmMaintainQueries'].txtAction);
        document.forms['frmMaintainQueries'].submit();
        }
        function deleteclick(PredefSearchID){
        document.forms['frmMaintainQueries'].txtPredefSearchID.value = PredefSearchID;
        document.forms['frmMaintainQueries'].txtAction.value = 'delete';
        InsertSecurityToken(document.forms['frmMaintainQueries'].txtAction);
        document.forms['frmMaintainQueries'].submit();
        }
        //Done with hiding

        $(document).ready(function() {
        $('.showTip').tooltip();
        });
      </xsl:comment>
    </script>
  </xsl:template>



</xsl:stylesheet>
