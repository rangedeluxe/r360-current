<?xml version="1.0" encoding="UTF-8" ?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 <xsl:template name="js-escape-quote">
  <xsl:param name="text" />
  <xsl:variable name="tmp">
   <xsl:call-template name="replace-substring">
    <xsl:with-param name="from" select="'&quot;'" />
    <xsl:with-param name="to">\"</xsl:with-param>
    <xsl:with-param name="value">
     <xsl:call-template name="replace-substring">
      <xsl:with-param name="from">&apos;</xsl:with-param>
      <xsl:with-param name="to">\'</xsl:with-param>
      <xsl:with-param name="value" select="$text" />
     </xsl:call-template>
    </xsl:with-param>
   </xsl:call-template>
  </xsl:variable>
  <xsl:value-of select="$tmp" />
 </xsl:template>

 <xsl:template name="replace-substring">
  <xsl:param name="value" />
  <xsl:param name="from" />
  <xsl:param name="to" />
  <xsl:choose>
   <xsl:when test="contains($value, $from)">
    <xsl:value-of select="substring-before($value, $from)" />
    <xsl:value-of select="$to" />
    <xsl:call-template name="replace-substring">
     <xsl:with-param name="value" select="substring-after($value, $from)" />
     <xsl:with-param name="from" select="$from" />
     <xsl:with-param name="to" select="$to" />
    </xsl:call-template>
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="$value" />
   </xsl:otherwise>
  </xsl:choose>
 </xsl:template>

		<xsl:template name="to-lower">
			<xsl:param name="Value" />
			<xsl:value-of select="translate($Value,'ABCDEFGHIJKLMNOPQRSTUVWXYZ','abcdefghijklmnopqrstuvwxyz')" />
		</xsl:template>

		<xsl:template name="to-upper">
			<xsl:param name="Value" />
			<xsl:value-of select="translate($Value,'abcdefghijklmnopqrstuvwxyz','ABCDEFGHIJKLMNOPQRSTUVWXYZ')" />
		</xsl:template>

		<xsl:template name="string-compare">
			<xsl:param name="Value1" />
			<xsl:param name="Value2" />
			<xsl:variable name="Value1LCase"><xsl:call-template name="to-lower"><xsl:with-param name="Value" select="$Value1" /></xsl:call-template></xsl:variable>
			<xsl:variable name="Value2LCase"><xsl:call-template name="to-lower"><xsl:with-param name="Value" select="$Value2" /></xsl:call-template></xsl:variable>
			<xsl:choose>
				<xsl:when test="Value1LCase = Value2LCase">1</xsl:when>
				<xsl:otherwise>0</xsl:otherwise>
			</xsl:choose>
		</xsl:template>

</xsl:stylesheet>
