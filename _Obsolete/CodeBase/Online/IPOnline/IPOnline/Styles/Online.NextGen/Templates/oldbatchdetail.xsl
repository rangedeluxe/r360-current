<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
    Decisioning Batch Detail
  </xsl:template>

  <xsl:template name="PageTitle">
    <span class="contenttitle">Decisioning Batch Detail</span>
    <br/>
    <span class="contentsubtitle">
      Workgroup: <xsl:value-of select="/Page/Batch/LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=/Page/Batch/BankID and @LockboxID=/Page/Batch/LockboxID]/@LongName" />
    </span>
  </xsl:template>

  <xsl:variable name="DecisionSourceStatusNotRequired">0</xsl:variable>
  <xsl:variable name="DecisionSourceStatusPendingDecBatLogon">1</xsl:variable>
  <xsl:variable name="DecisionSourceStatusPendingDecTDE">2</xsl:variable>
  <xsl:variable name="DecisionSourceStatusAccepted">3</xsl:variable>
  <xsl:variable name="DecisionSourceStatusRejected">4</xsl:variable>

  <xsl:variable name="GlobalBatchID">
    <xsl:value-of select="/Page/FormFields/GlobalBatchID" />
  </xsl:variable>
  <xsl:variable name="DepositStatus">
    <xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/DepositStatus" />
  </xsl:variable>
  <xsl:variable name="LogonName">
    <xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/LogonName" />
  </xsl:variable>

  <xsl:variable name="pagemode">
    <xsl:choose>
      <xsl:when test="$DepositStatus='240' or ($DepositStatus='241' and /Page/UserInfo/User/@ID=$LogonName)">edit</xsl:when>
      <xsl:otherwise>view</xsl:otherwise>
    </xsl:choose>
  </xsl:variable>

  <xsl:template match="Page">

    <xsl:variable name="BankID">
      <xsl:value-of select="/Page/Batch/BankID" />
    </xsl:variable>
    <xsl:variable name="CustomerID">
      <xsl:value-of select="/Page/Batch/CustomerID" />
    </xsl:variable>
    <xsl:variable name="LockboxID">
      <xsl:value-of select="/Page/Batch/LockboxID" />
    </xsl:variable>
    <xsl:variable name="startrecord">
      <xsl:value-of select="/Page/PageInfo/@StartRecord"/>
    </xsl:variable>

    <form method="post" name="frmOLD">
      <input type="hidden" name="txtAction" />
      <input type="hidden" name="txtStart">
        <xsl:attribute name="value">
          <xsl:value-of select="$startrecord" />
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtBankID">
        <xsl:attribute name="value">
          <xsl:value-of select="/Page/Batch/BankID" />
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtCustomerID">
        <xsl:attribute name="value">
          <xsl:value-of select="/Page/Batch/CustomerID" />
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtLockboxID">
        <xsl:attribute name="value">
          <xsl:value-of select="/Page/Batch/LockboxID" />
        </xsl:attribute>
      </input>

      <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
        <tr>
          <td colspan="2">
            <table width="100%" cellpadding="5" cellspacing="0" border="0">
              <tr>
                <td class="reportcaption">
                  <xsl:variable name="LongName">
                    <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@LongName"/>
                  </xsl:variable>
                  Workgroup:&nbsp;<xsl:value-of select="$LockboxID" /><xsl:if test="string-length($LongName) > 0">
                    &nbsp;-&nbsp;<xsl:value-of select="$LongName" />
                  </xsl:if><br/>
                  Deposit Date:&nbsp;<xsl:value-of select="/Page/Batch/DepositDate"/><br />
                  Batch:&nbsp;<xsl:value-of select="/Page/Batch/BatchID" /><br />
                  Deposit Status:&nbsp;
                  <xsl:choose>
                    <xsl:when test="/Page/Batch/DepositStatus = '240'">Pending Decision</xsl:when>
                    <xsl:when test="/Page/Batch/DepositStatus = '241'">In-Decision</xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="/Page/Batch/DepositStatus" />
                    </xsl:otherwise>
                  </xsl:choose>
                  <br />
                  Decisioning Deadline: <xsl:value-of select="/Page/Batch/AdjustedDecisioningDeadline" /><br />
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <xsl:call-template name="ShowingResultsLabel">
              <xsl:with-param name="startrecord">
                <xsl:value-of select="$startrecord" />
              </xsl:with-param>
            </xsl:call-template>
          </td>
          <td align="right" class="pagelinks">
            <a>
              <xsl:attribute name="href">
                oldbatches.aspx?OLLockboxID=<xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@OLLockboxID" />
              </xsl:attribute>Select Batch
            </a>
          </td>
        </tr>
        <tr>
          <td align="left" valign="top" colspan="2">
            <xsl:for-each select="Batch">
              <table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
                <tr>
                  <td class="reportheading" vAlign="top" align="left">Transaction</td>
                  <td class="reportheading" vAlign="top" align="right">Payment Amount</td>
                  <td class="reportheading" vAlign="top" align="left"></td>
                  <td class="reportheading" vAlign="top" align="left"></td>
                  <td class="reportheading" vAlign="top" align="left"></td>
                  <td class="reportheading" vAlign="top" align="left"></td>
                  <td class="reportheading" vAlign="top" align="left">R/T</td>
                  <td class="reportheading" vAlign="top" align="left">Account</td>
                  <td class="reportheading" vAlign="top" align="left">Serial</td>
                  <td class="reportheading" vAlign="top" align="left">Payer</td>
                  <td class="reportheading" vAlign="top" align="left">Decision Status</td>
                  <td class="reportheading" vAlign="top" align="left">
                  </td>
                </tr>
                <xsl:for-each select="Transactions">
                  <xsl:variable name="txnpos">
                    <xsl:value-of select="position()" />
                  </xsl:variable>
                  <xsl:choose>
                    <xsl:when test="count(Checks) = 0">
                      <tr>
                        <xsl:if test='$txnpos mod 2 = 0'>
                          <xsl:attribute name="class">evenrow</xsl:attribute>
                        </xsl:if>
                        <xsl:if test='$txnpos mod 2 != 0'>
                          <xsl:attribute name="class">oddrow</xsl:attribute>
                        </xsl:if>
                        <td class="reportdata" align="left" valign="top">
                          <a title="View Transaction Detail">
                            <xsl:attribute name="href">
                              oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="../GlobalBatchID" />&amp;transaction=<xsl:value-of select="TransactionID" />
                            </xsl:attribute>
                            <xsl:value-of select="TransactionID" />
                          </a>
                        </td>
                        <td class="reportdata" vAlign="top" align="right">&nbsp;</td>
                        <td class="reportdata" vAlign="top" align="left" width="16">&nbsp;</td>
                        <td class="reportdata" vAlign="top" align="left" width="16"></td>
                        <td class="reportdata" vAlign="top" align="left" width="16"></td>
                        <td class="reportdata" vAlign="top" align="left" width="16"></td>
                        <td class="reportdata" vAlign="top" align="left"></td>
                        <td class="reportdata" vAlign="top" align="left"></td>
                        <td class="reportdata" vAlign="top" align="left"></td>
                        <td class="reportdata" vAlign="top" align="left"></td>
                        <td class="reportdata" vAlign="top" align="left">
                          <xsl:choose>
                            <xsl:when test="DecisionSourceStatus=$DecisionSourceStatusNotRequired">
                              Decision Not Required
                            </xsl:when>
                            <xsl:when test="DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon">
                              Pending Decision
                            </xsl:when>
                            <xsl:when test="DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE">
                              Pending Decision
                            </xsl:when>
                            <xsl:when test="DecisionSourceStatus=$DecisionSourceStatusAccepted">
                              Accepted
                            </xsl:when>
                            <xsl:when test="DecisionSourceStatus=$DecisionSourceStatusRejected">
                              Rejected
                            </xsl:when>
                            <xsl:otherwise>
                              &nbsp;
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                        <td class="reportdata" vAlign="top" align="right">
                          <a title="View Transaction Detail">
                            <xsl:attribute name="href">
                              oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="../GlobalBatchID" />&amp;transaction=<xsl:value-of select="TransactionID" />
                            </xsl:attribute>
                            <img src="{$brandtemplatepath}/images/cbo_icon_batchdetail.jpg" border="0" />
                          </a>
                        </td>
                      </tr>
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:for-each select="Checks">
                        <tr>
                          <xsl:if test='$txnpos mod 2 = 0'>
                            <xsl:attribute name="class">evenrow</xsl:attribute>
                          </xsl:if>
                          <xsl:if test='$txnpos mod 2 != 0'>
                            <xsl:attribute name="class">oddrow</xsl:attribute>
                          </xsl:if>
                          <td class="reportdata" align="left" valign="top">
                            <xsl:choose>
                              <xsl:when test="position() = 1">
                                <a title="View Transaction Detail">
                                  <xsl:attribute name="href">
                                    oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="../../GlobalBatchID" />&amp;transaction=<xsl:value-of select="../TransactionID" />
                                  </xsl:attribute>
                                  <xsl:value-of select="../TransactionID" />
                                </a>
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                          <td class="reportdata" vAlign="top" align="right">
                            <xsl:value-of select="format-number(./Amount, '$ ###,###.00')" />
                          </td>
                          <td class="reportdata" vAlign="top" align="left" width="16">
                            <a title="View Payment Image">
                              <xsl:attribute name="onclick">
                                docView('imagepdf.aspx?bank=<xsl:value-of select="../../BankID"/>&amp;lbx=<xsl:value-of select="../../LockboxID"/>&amp;batch=<xsl:value-of select="../../BatchID"/>&amp;date=<xsl:value-of select="../../DepositDate"/>&amp;picsdate=<xsl:value-of select="../../PICSDate"/>&amp;txn=<xsl:value-of select="../TransactionID"/>&amp;item=<xsl:value-of select="./BatchSequence"/>&amp;type=c&amp;page=-1');return false;
                              </xsl:attribute>
                              <xsl:attribute name="href">
                                imagepdf.aspx?bank=<xsl:value-of select="../../BankID"/>&amp;lbx=<xsl:value-of select="../../LockboxID"/>&amp;batch=<xsl:value-of select="../../BatchID"/>&amp;date=<xsl:value-of select="../../DepositDate"/>&amp;picsdate=<xsl:value-of select="../../PICSDate"/>&amp;txn=<xsl:value-of select="../TransactionID"/>&amp;item=<xsl:value-of select="./BatchSequence"/>&amp;type=c&amp;page=-1
                              </xsl:attribute>
                              <img src="{$brandtemplatepath}/Images/cbo_icon_check.gif" border="0" />
                            </a>
                          </td>
                          <td class="reportdata" vAlign="top" align="left" width="16"></td>
                          <td class="reportdata" vAlign="top" align="left" width="16"></td>
                          <td class="reportdata" vAlign="top" align="left" width="16"></td>
                          <td class="reportdata" vAlign="top" align="left">
                            <xsl:value-of select="RT" />
                          </td>
                          <td class="reportdata" vAlign="top" align="left">
                            <xsl:value-of select="Account" />
                          </td>
                          <td class="reportdata" vAlign="top" align="left">
                            <xsl:value-of select="Serial" />
                          </td>
                          <td class="reportdata" vAlign="top" align="left">
                            <xsl:value-of select="RemitterName" />
                          </td>

                          <td class="reportdata" vAlign="top" align="left">
                            <xsl:choose>
                              <xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusNotRequired">
                                Decision Not Required
                              </xsl:when>
                              <xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon">
                                Pending Decision
                              </xsl:when>
                              <xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE">
                                Pending Decision
                              </xsl:when>
                              <xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusAccepted">
                                Accepted
                              </xsl:when>
                              <xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusRejected">
                                Rejected
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>

                          <td class="reportdata" vAlign="top" align="right">
                            <a title="View Transaction Detail">
                              <xsl:attribute name="href">
                                oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="../../GlobalBatchID" />&amp;transaction=<xsl:value-of select="../TransactionID" />
                              </xsl:attribute>
                              <img src="{$brandtemplatepath}/images/cbo_icon_batchdetail.jpg" border="0" />
                            </a>
                          </td>
                        </tr>
                      </xsl:for-each>
                    </xsl:otherwise>
                  </xsl:choose>
                </xsl:for-each>
              </table>
            </xsl:for-each>
          </td>
        </tr>
      </table>


      <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td align="right" valign="bottom" class="pagelinks">
            <br />
            <xsl:call-template name="PaginationLinks">
              <xsl:with-param name="controlname">document.forms['frmOLD'].txtAction</xsl:with-param>
              <xsl:with-param name="startrecord">
                <xsl:value-of select="$startrecord" />
              </xsl:with-param>
            </xsl:call-template>
          </td>
        </tr>
      </table>

      <br />

      <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td align="center">
            <xsl:choose>
              <xsl:when test="/Page/Batch/DepositStatus=240">
                <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='modify']">
                  <input type="submit" value="Check Out" name="btnCheckOut" />
                </xsl:if>
              </xsl:when>
              <xsl:when test="/Page/Batch/DepositStatus=241">
                <xsl:if test="(count(/Page/Batch/Transactions[DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon or DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE]) = 0) and ($pagemode='edit')">
                  <input type="submit" value="Complete" name="btnComplete" />
                </xsl:if>
                <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='reset']">
                  <input type="submit" value="Reset" name="btnReset" />
                </xsl:if>
              </xsl:when>
            </xsl:choose>
          </td>
        </tr>
      </table>

    </form>

    <p></p>
  </xsl:template>

</xsl:stylesheet>
