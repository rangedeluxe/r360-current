<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#0171;">
  <!ENTITY raquo "&#0187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">


<xsl:template name="recordsperpage">
    <xsl:choose>
        <xsl:when test="./@DisplayRecords">
            <xsl:value-of select="./@DisplayRecords"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="/Page/PageInfo/@DisplayRecords"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="recordcount">
    <xsl:choose>
        <xsl:when test="./@TotalRecords">
            <xsl:value-of select="./@TotalRecords"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="/Page/PageInfo/@TotalRecords"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="previousstartrecord">
    <xsl:choose>
        <xsl:when test="./@PreviousStartRecord">
            <xsl:value-of select="./@PreviousStartRecord"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="/Page/PageInfo/@PreviousStartRecord"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="nextstartrecord">
    <xsl:choose>
        <xsl:when test="./@NextStartRecord">
            <xsl:value-of select="./@NextStartRecord"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="/Page/PageInfo/@NextStartRecord"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="laststartrecord">
    <xsl:choose>
        <xsl:when test="./@LastStartRecord">
            <xsl:value-of select="./@LastStartRecord"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:value-of select="/Page/PageInfo/@LastStartRecord"/>
        </xsl:otherwise>
    </xsl:choose>
</xsl:template>

<xsl:template name="ShowingResultsLabel">
	<xsl:param name="startrecord"/>

	<xsl:variable name="recordsperpage"><xsl:call-template name="recordsperpage" /></xsl:variable>
	<xsl:variable name="recordcount"><xsl:call-template name="recordcount" /></xsl:variable>
	<xsl:variable name="previousstartrecord"><xsl:call-template name="previousstartrecord" /></xsl:variable>
	<xsl:variable name="nextstartrecord"><xsl:call-template name="nextstartrecord" /></xsl:variable>
	<xsl:variable name="laststartrecord"><xsl:call-template name="laststartrecord" /></xsl:variable>

    <table width="100%">
		<tr>
			<td align="left" valign="top" class="recordcount">
				<xsl:choose>
					<xsl:when test="number($recordcount) &lt; 1">
                        &nbsp;
					</xsl:when>
					<xsl:when test="number($startrecord) + number($recordsperpage) > number($recordcount)">
						Showing Results <xsl:value-of select="$startrecord" /> - <xsl:value-of select="$recordcount" />
						&nbsp;of&nbsp;<xsl:value-of select="$recordcount" />
					</xsl:when>
					<xsl:otherwise>
						Showing Results <xsl:value-of select="$startrecord" /> - <xsl:value-of select="number($startrecord) + number($recordsperpage) - 1" />
						&nbsp;of&nbsp;<xsl:value-of select="$recordcount" />
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
    </table>
</xsl:template>

<xsl:template name="NoResultsLabel">
    There are no records to display.
</xsl:template>

<xsl:template name="PaginationLinks">
	<xsl:param name="controlname"/>
	<xsl:param name="startrecordcontrolname"/>
	<xsl:param name="startrecord"/>
	<xsl:param name="confirmationmsg"/>
	<xsl:param name="confirmationscript"/>
	<xsl:param name="linkurl"/>
	<xsl:param name="colSpan" />

	<xsl:variable name="recordsperpage"><xsl:call-template name="recordsperpage" /></xsl:variable>
	<xsl:variable name="recordcount"><xsl:call-template name="recordcount" /></xsl:variable>
	<xsl:variable name="previousstartrecord"><xsl:call-template name="previousstartrecord" /></xsl:variable>
	<xsl:variable name="nextstartrecord"><xsl:call-template name="nextstartrecord" /></xsl:variable>
	<xsl:variable name="laststartrecord"><xsl:call-template name="laststartrecord" /></xsl:variable>

    <script language="JavaScript" src="{$brandtemplatepath}/js/pagination.js"></script>

	<tr class="grid-pager">
		<td>
			<xsl:attribute name="colspan">
				<xsl:value-of select="$colSpan"/>
			</xsl:attribute>
			<span style="margin-left: 3px; dispay: inline-block;">
				<xsl:choose>
					<xsl:when test="number($startrecord)-number($recordsperpage)>=1">
						<a>
							<xsl:choose>
								<xsl:when test="($confirmationscript='' or $confirmationmsg='') and string-length($linkurl) = 0">
									<xsl:choose>
										<xsl:when test="string-length($startrecordcontrolname) = 0">
											<xsl:attribute name="href">
												javascript:navigateRS(<xsl:value-of select="$controlname" />, 'first','1');
											</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="href">
												javascript:navigateRS2(<xsl:value-of select="$controlname" />, <xsl:value-of select="$startrecordcontrolname" />, 'first','1');
											</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="string-length($linkurl) &gt; 0">
									<xsl:attribute name="href">
										<xsl:value-of select="$linkurl"/>?start=1
									</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="href">
										javascript:if(!<xsl:value-of select="$confirmationscript" /> || '<xsl:value-of select="$confirmationmsg" />'=='' || confirm('<xsl:value-of select="$confirmationmsg" />')) { navigateRS(<xsl:value-of select="$controlname" />, 'first','1'); }
									</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:attribute name="title">View First Page</xsl:attribute>
							<i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						</a>
						<a>
							<xsl:choose>
								<xsl:when test="($confirmationscript='' or $confirmationmsg='') and string-length($linkurl) = 0">
									<xsl:choose>
										<xsl:when test="string-length($startrecordcontrolname) = 0">
											<xsl:attribute name="href">
												javascript:navigateRS(<xsl:value-of select="$controlname" />, 'previous','<xsl:value-of select="$previousstartrecord" />');
											</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="href">
												javascript:navigateRS2(<xsl:value-of select="$controlname" />, <xsl:value-of select="$startrecordcontrolname" />, 'previous','<xsl:value-of select="$previousstartrecord" />');
											</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="string-length($linkurl) &gt; 0">
									<xsl:attribute name="href">
										<xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="$previousstartrecord"/>
									</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="href">
										javascript:if(!<xsl:value-of select="$confirmationscript" /> || '<xsl:value-of select="$confirmationmsg" />'=='' || confirm('<xsl:value-of select="$confirmationmsg" />')) { navigateRS(<xsl:value-of select="$controlname" />, 'previous','<xsl:value-of select="$previousstartrecord" />'); }
									</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:attribute name="title">View Previous Page</xsl:attribute>
							<i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<i class="ui-icon ui-icon-seek-first ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						<i class="ui-icon ui-icon-seek-prev ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
					</xsl:otherwise>
				</xsl:choose>

				<xsl:choose>
					<xsl:when test="number($startrecord) + number($recordsperpage) - 1 &lt; number($recordcount)">
						<a>
							<xsl:choose>
								<xsl:when test="($confirmationscript='' or $confirmationmsg='') and string-length($linkurl) = 0">
									<xsl:choose>
										<xsl:when test="string-length($startrecordcontrolname) = 0">
											<xsl:attribute name="href">
												javascript:navigateRS(<xsl:value-of select="$controlname" />, 'next','<xsl:value-of select="$nextstartrecord" />');
											</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="href">
												javascript:navigateRS2(<xsl:value-of select="$controlname" />, <xsl:value-of select="$startrecordcontrolname" />, 'next','<xsl:value-of select="$nextstartrecord" />');
											</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="string-length($linkurl) &gt; 0">
									<xsl:attribute name="href">
										<xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="$nextstartrecord"/>
									</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="href">
										javascript:if(!<xsl:value-of select="$confirmationscript" /> || '<xsl:value-of select="$confirmationmsg" />'=='' || confirm('<xsl:value-of select="$confirmationmsg" />')) { navigateRS(<xsl:value-of select="$controlname" />, 'next','<xsl:value-of select="$nextstartrecord" />'); }
									</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:attribute name="title">View Next Page</xsl:attribute>
							<i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						</a>
						<a>
							<xsl:choose>
								<xsl:when test="($confirmationscript='' or $confirmationmsg='') and string-length($linkurl) = 0">
									<xsl:choose>
										<xsl:when test="string-length($startrecordcontrolname) = 0">
											<xsl:attribute name="href">
												javascript:navigateRS(<xsl:value-of select="$controlname" />, 'last','<xsl:value-of select="$laststartrecord" />');
											</xsl:attribute>
										</xsl:when>
										<xsl:otherwise>
											<xsl:attribute name="href">
												javascript:navigateRS2(<xsl:value-of select="$controlname" />, <xsl:value-of select="$startrecordcontrolname" />, 'last','<xsl:value-of select="$laststartrecord" />');
											</xsl:attribute>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:when>
								<xsl:when test="string-length($linkurl) &gt; 0">
									<xsl:attribute name="href">
										<xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="$laststartrecord"/>
									</xsl:attribute>
								</xsl:when>
								<xsl:otherwise>
									<xsl:attribute name="href">
										javascript:if(!<xsl:value-of select="$confirmationscript" /> || '<xsl:value-of select="$confirmationmsg" />'=='' || confirm('<xsl:value-of select="$confirmationmsg" />')) { navigateRS(<xsl:value-of select="$controlname" />, 'last','<xsl:value-of select="$laststartrecord" />'); }
									</xsl:attribute>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:attribute name="title">View Last Page</xsl:attribute>
							<i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						</a>
					</xsl:when>
					<xsl:otherwise>
						<i class="ui-icon ui-icon-seek-next ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
						<i class="ui-icon ui-icon-seek-end ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<span style="dispay: inline-block; vertical-align: middle; font-size: 0.7em; margin-left: 5px;">
				<xsl:choose>
					<xsl:when test="number($recordcount) &lt; 1">
						&nbsp;
					</xsl:when>
					<xsl:when test="number($startrecord) + number($recordsperpage) > number($recordcount)">
						Showing Results <xsl:value-of select="$startrecord" /> - <xsl:value-of select="$recordcount" />
						&nbsp;of&nbsp;<xsl:value-of select="$recordcount" />
					</xsl:when>
					<xsl:otherwise>
						Showing Results <xsl:value-of select="$startrecord" /> - <xsl:value-of select="number($startrecord) + number($recordsperpage) - 1" />
						&nbsp;of&nbsp;<xsl:value-of select="$recordcount" />
					</xsl:otherwise>
				</xsl:choose>
			</span>
			<!--<div style="display: inline-block; float: right; vertical-align: middle; margin-right: 5px; margin-top: 3px;">
				<i class="ui-icon ui-icon-lightbulb ui-state-disabled" style="border: 1px solid gray" ></i>
			</div>-->
		</td>
	</tr>       
	
</xsl:template>


</xsl:stylesheet>

  
