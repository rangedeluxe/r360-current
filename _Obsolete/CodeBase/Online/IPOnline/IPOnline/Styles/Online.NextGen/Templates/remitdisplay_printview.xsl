<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incCustomBranding.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	Batch Detail
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Batch Detail</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>

<xsl:template match="Page">

	<xsl:if test="Recordset[@Name='BatchDetail']">
		<xsl:apply-templates select="Recordset[@Name = 'BatchDetail']" mode="BatchDetail"/>
	</xsl:if>

</xsl:template>


<xsl:template match="Recordset" mode="BatchDetail">

	<xsl:if test="Record">
		<xsl:apply-templates select="Record" mode="BatchDetail"/>
	</xsl:if>

</xsl:template>


<xsl:template match="Record" mode="BatchDetail">
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
		</head>
		<body>
			<div id="container">
				<div id="header">
					<h1>
						<p/>
						Batch Detail
					</h1>
				</div>
				<div id="subheader">
					<h1>
						<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">
							Bank ID:&nbsp;<xsl:value-of select="/Page/Recordset[@Name='BatchDetail']/PageInfo/@BankID"/><br/>
						</xsl:if>
            Workgroup:&nbsp;&nbsp;<xsl:value-of select="@LongName"/><br/>
						Deposit Date:&nbsp;<xsl:value-of select="@DepositDate"/><br/>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
       Account Site Code:&nbsp;<xsl:value-of select="@SiteCode"/>
      </xsl:if>
     </h1>
				</div>
				<div id="content">
					<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
							<xsl:for-each select="Recordset[@Name='BatchDetails']">
								<xsl:apply-templates select="." mode="BatchDetails"/>
							</xsl:for-each>
							<xsl:if test="../PageInfo[@DisplayPageTotals='Y']">
								<xsl:apply-templates select="../PageInfo" mode="PageTotals"/>
							</xsl:if>
					</table>
				</div>
			</div>
		</body>
	</html>

</xsl:template>


<xsl:template match="Recordset" mode="BatchDetails">
  <xsl:variable name="viewallurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>
  <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/Recordset[@Name = 'BatchDetail']/Record/@DisplayBatchID"/></xsl:variable>
	<tr>
	 <xsl:if test="$displaybatchid = 'True'">
		 <td class="reportheading" align="left" valign="top"><xsl:call-template name="batchidlabel" /></td>
  </xsl:if>
  <td class="reportheading" align="left" valign="top"><xsl:call-template name="batchnumberlabel" /></td>
  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
   <td class="reportheading" align="left" valign="top">Batch Site Code</td>
  </xsl:if>
  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
   <td class="reportheading" align="left" valign="top"><xsl:call-template name="batchcueidlabel" /></td>
  </xsl:if>
  <td class="reportheading" align="left" valign="top">Transaction</td>
		<td class="reportheading" align="right" valign="top">Payment Amount</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
		<td class="reportheading" align="left" valign="top">R/T</td>
		<td class="reportheading" align="left" valign="top">Account Number</td>
		<td class="reportheading" align="left" valign="top">Check/Trace/Ref Number</td>
		<td class="reportheading" align="left" valign="top">Payer</td>
    <td class="reportheading" align="left" valign="top">DDA</td>
		<td class="reportheading" align="left" valign="top">&nbsp;</td>
	</tr>

	<xsl:for-each select="Record">
		<tr>
			<xsl:if test='position() mod 2 = 0'>
				<xsl:attribute name="class">evenrow</xsl:attribute>
			</xsl:if>
			<xsl:if test='position() mod 2 != 0'>
				<xsl:attribute name="class">oddrow</xsl:attribute>
			</xsl:if>

   <xsl:if test="$displaybatchid = 'True'">
    <td class="reportdata" align="left" valign="top">				
     <xsl:choose>
					 <xsl:when test="position() = 1">
						 <xsl:value-of select="@SourceBatchID"/>
					 </xsl:when>
					 <xsl:otherwise>
						 &nbsp;
					 </xsl:otherwise>
				 </xsl:choose>
    </td>
   </xsl:if>

			<td class="reportdata" align="left" valign="top">
				<xsl:choose>
					<xsl:when test="position() = 1">
						<xsl:value-of select="@BatchNumber"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
   <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
		<td class="reportdata" align="left" valign="top">
			<xsl:choose>
				<xsl:when test="position() = 1">
					<xsl:value-of select="@BatchSiteCode"/>
				</xsl:when>
				<xsl:otherwise>
					&nbsp;
				</xsl:otherwise>
			</xsl:choose>     
		</td>
   </xsl:if>
   <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
    <td class="reportdata" align="left" valign="top">
    <xsl:choose>
     <xsl:when test="@BatchCueID = -1">
      &nbsp;
     </xsl:when>
     <xsl:otherwise>
		 <xsl:choose>
			 <xsl:when test="position() = 1">
				 <xsl:value-of select="@BatchCueID"/>
			 </xsl:when>
			 <xsl:otherwise>&nbsp;</xsl:otherwise>
		 </xsl:choose>
     </xsl:otherwise>
    </xsl:choose>
    </td>
   </xsl:if>
   <xsl:variable name="transactiondetailurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/></xsl:variable>
			<xsl:variable name="imageurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;item=<xsl:value-of select="@BatchSequence"/>&amp;type=c</xsl:variable>
			<xsl:variable name="addremitterurl">lckbx=<xsl:value-of select="../../@OLLockboxID"/>&amp;bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;item=<xsl:value-of select="@BatchSequence"/>&amp;type=c&amp;rt=<xsl:value-of select="@RT"/>&amp;acct=<xsl:value-of select="@Account"/></xsl:variable>

			<td class="reportdata" align="left" valign="top">
					<xsl:value-of select="@TxnSequence"/>
			</td>
			<td class="reportdata" align="right" valign="top">
				<xsl:choose>
					<xsl:when test="string-length(@Amount) &gt; 0">
						$<xsl:value-of select="format-number(@Amount, '#,##0.00')"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="reportdata" align="left" valign="top" width="16">&nbsp;</td>
			<td class="reportdata" align="left" valign="top" width="16">&nbsp;</td>
			<td align="center" valign="top" class="reportdata" width="16">&nbsp;</td>
			<td class="reportdata" align="left" valign="top" width="16">&nbsp;</td>
			<td class="reportdata" align="left" valign="top" width="16">&nbsp;</td>
			<td class="reportdata" align="left" valign="top">
				<xsl:choose>
					<xsl:when test="string-length(@RT) &gt; 0">
						<xsl:value-of select="@RT"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="reportdata" align="left" valign="top">
				<xsl:choose>
					<xsl:when test="string-length(@Account) &gt; 0">
						<xsl:value-of select="@Account"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="reportdata" align="left" valign="top">
				<xsl:choose>
					<xsl:when test="string-length(@Serial) &gt; 0">
						<xsl:value-of select="@Serial"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td class="reportdata" align="left" valign="top">
				<xsl:choose>
					<xsl:when test="string-length(@RemitterName) &gt; 0">
						<xsl:value-of select="@RemitterName"/>
					</xsl:when>
					<xsl:otherwise>
						&nbsp;
					</xsl:otherwise>
				</xsl:choose>
			</td>
      <td class="reportdata" align="left" valign="top">
        <xsl:choose>
          <xsl:when test="string-length(@DDA) &gt; 0">
            <xsl:value-of select="@DDA"/>
          </xsl:when>
          <xsl:otherwise>
            &nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </td>
			<td class="reportdata" align="right" valign="top">&nbsp;</td>
		</tr>
	</xsl:for-each>

	<xsl:if test="@DisplayBatchTotals = 'Y'">
		<tr>
   <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
    <td class="reportfooter" align="right" valign="top"></td>
   </xsl:if>
	  <xsl:if test="$displaybatchid = 'True'">
		  <td class="reportfooter" align="left" valign="top"></td>
   </xsl:if>
			<td class="reportfooter" align="right" valign="top" colspan="2"><xsl:call-template name="batchnumberlabel" />&nbsp;<xsl:value-of select="@BatchNumber"/>&nbsp;Total:</td>
			<td class="reportfooter" align="right" valign="top">$<xsl:value-of select="format-number(@BatchTotal, '#,##0.00')"/></td>
			<td class="reportfooter" align="left" valign="top">
			</td>
			<td class="reportfooter" align="left" valign="top" colspan="10">&nbsp;</td>
		</tr>
	</xsl:if>

</xsl:template>


<xsl:template match="PageInfo" mode="PageTotals">

	<tr>
  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
   <td class="reportfooter" align="right" valign="top"></td>
  </xsl:if>
  <td class="reportfooter" align="right" valign="top" colspan="2">Total For All Batches:</td>
		<td class="reportfooter" align="right" valign="top">$<xsl:value-of select="format-number(@BatchGrandTotal, '#,##0.00')"/></td>
		<td class="reportfooter" align="left" valign="top" colspan="10">&nbsp;</td>
	</tr>

</xsl:template>




</xsl:stylesheet>
