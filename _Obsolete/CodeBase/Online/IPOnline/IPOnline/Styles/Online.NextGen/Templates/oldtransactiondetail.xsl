<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "&#169;">
	<!ENTITY middot "&#183;">
	<!ENTITY laquo "&#0171;">
	<!ENTITY raquo "&#0187;">
	<!ENTITY bull "&#8226;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html"/>

<xsl:variable name="FldTypeString">1</xsl:variable>
<xsl:variable name="FldTypeFloat">6</xsl:variable>
<xsl:variable name="FldTypeCurrency">7</xsl:variable>
<xsl:variable name="FldTypeDate">11</xsl:variable>

<xsl:variable name="FieldDecisionStatusNoneRequired">0</xsl:variable>
<xsl:variable name="FieldDecisionStatusPending">1</xsl:variable>
<xsl:variable name="FieldDecisionStatusAccepted">2</xsl:variable>
<xsl:variable name="FieldDecisionStatusRejected">3</xsl:variable>

<xsl:variable name="DecisionSourceStatusNotRequired">0</xsl:variable>
<xsl:variable name="DecisionSourceStatusPendingDecBatLogon">1</xsl:variable>
<xsl:variable name="DecisionSourceStatusPendingDecTDE">2</xsl:variable>
<xsl:variable name="DecisionSourceStatusAccepted">3</xsl:variable>
<xsl:variable name="DecisionSourceStatusRejected">4</xsl:variable>

<xsl:variable name="InvoiceBalancingNotRequired">0</xsl:variable>
<xsl:variable name="InvoiceBalancingDesired">1</xsl:variable>
<xsl:variable name="InvoiceBalancingRequired">2</xsl:variable>

<xsl:variable name="InvoiceBalancingActionUndefined">0</xsl:variable>
<xsl:variable name="InvoiceBalancingActionSaveUnbalanced">1</xsl:variable>
<xsl:variable name="InvoiceBalancingActionForceBalance">2</xsl:variable>

<xsl:variable name="GlobalBatchID"><xsl:value-of select="/Page/FormFields/GlobalBatchID" /></xsl:variable>
<xsl:variable name="TransactionID"><xsl:value-of select="/Page/FormFields/TransactionID" /></xsl:variable>
<xsl:variable name="DepositStatus"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/DepositStatus" /></xsl:variable>
<xsl:variable name="DecisionSourceStatus"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/Transactions[TransactionID=$TransactionID]/DecisionSourceStatus" /></xsl:variable>
<xsl:variable name="DestLoxkboxIdentifier"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/Transactions[TransactionID=$TransactionID]/DestLoxkboxIdentifier" /></xsl:variable>
<xsl:variable name="DestinationLockboxKey"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/Transactions[TransactionID=$TransactionID]/DestinationLockboxKey" /></xsl:variable>
<xsl:variable name="DestinationBatchTypeID"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/Transactions[TransactionID=$TransactionID]/DestinationBatchTypeID" /></xsl:variable>
<xsl:variable name="LogonName"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/LogonName" /></xsl:variable>

<xsl:variable name="BankID"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/BankID" /></xsl:variable>
<xsl:variable name="CustomerID"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/CustomerID" /></xsl:variable>
<xsl:variable name="LockboxID"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/LockboxID" /></xsl:variable>

<xsl:variable name="pagemode">
	<xsl:choose>
		<xsl:when test="($DepositStatus='240') or ($DepositStatus='241' and /Page/UserInfo/User/@ID=$LogonName) and /Page/UserInfo/Permissions/Permission[@Script='oldtransactiondetail.aspx' and @Mode='modify']">edit</xsl:when>
		<xsl:otherwise>view</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="txnlabel">gbid<xsl:value-of select="$GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" /></xsl:variable>

<xsl:template name="Title">
	Decisioning Transaction Detail
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Decisioning Transaction Detail</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>

<xsl:template match="Page">
	<xsl:apply-templates select="Batch" />
</xsl:template>

<xsl:template match="Batch">
	 
	<form method="post" name="frmOLD">

	<input type="hidden" name="hidBankID"><xsl:attribute name="value"><xsl:value-of select="BankID" /></xsl:attribute></input>
	<input type="hidden" name="hidCustomerID"><xsl:attribute name="value"><xsl:value-of select="CustomerID" /></xsl:attribute></input>
	<input type="hidden" name="hidLockboxID"><xsl:attribute name="value"><xsl:value-of select="LockboxID" /></xsl:attribute></input>
	<input type="hidden" name="hidGlobalBatchID"><xsl:attribute name="value"><xsl:value-of select="GlobalBatchID" /></xsl:attribute></input>
	<input type="hidden" name="hidTransactionID"><xsl:attribute name="value"><xsl:value-of select="$TransactionID" /></xsl:attribute></input>

	<input type="hidden" name="txtCheckAction" />
	<input type="hidden" name="txtStubAction" />
	<input type="hidden" name="txtDocumentAction" />
	<input type="hidden" name="txtAction" />

	<input type="hidden" name="txtCheckStartRecord">
		<xsl:attribute name="value"><xsl:value-of select="Transactions/CheckInfo/@StartRecord" /></xsl:attribute>
	</input>
	<input type="hidden" name="txtStubStartRecord">
		<xsl:attribute name="value"><xsl:value-of select="Transactions/StubInfo/@StartRecord" /></xsl:attribute>
	</input>
	<input type="hidden" name="txtDocumentStartRecord">
		<xsl:attribute name="value"><xsl:value-of select="Transactions/DocumentInfo/@StartRecord" /></xsl:attribute>
	</input>

	<input type="hidden" name="txtNextTxnID" />

	<table width="98%" align="center" cellpadding="5" cellspacing="0" border="0">
		<tr>
		<td align="left" class="reportcaption">
			<xsl:variable name="LongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@LongName"/></xsl:variable>
      Workgroup:&nbsp;<xsl:value-of select="LockboxID" /><xsl:if test="string-length($LongName) > 0">&nbsp;-&nbsp;<xsl:value-of select="$LongName" /></xsl:if><br/>
			Deposit Date:&nbsp;<xsl:value-of select="DepositDate" /><br/>
			Batch:&nbsp;<xsl:value-of select="BatchID" /><br/>
			Deposit Status:&nbsp;
			<xsl:choose>
				<xsl:when test="DepositStatus = '240'">Pending Decision</xsl:when>
				<xsl:when test="DepositStatus = '241'">In-Decision</xsl:when>
				<xsl:otherwise><xsl:value-of select="/Page/Batch/DepositStatus" /></xsl:otherwise>
			</xsl:choose>
			<br />
			Decisioning Deadline: <xsl:value-of select="/Page/Batch/AdjustedDecisioningDeadline" /><br />
			Transaction:&nbsp;<xsl:value-of select="Transactions[TransactionID=/Page/FormFields/TransactionID]/Sequence" /> of <xsl:value-of select="count(Transactions)" /><br/>
		</td>
		</tr>
	</table>

	<xsl:call-template name="TxnCommands" />

	<hr />

	<table cellspacing="5" cellpadding="5" border="0" align="center" width="100%">
		<tr>
		<td align="center">
			<xsl:apply-templates select="Transactions" />
		</td>
		</tr>
	</table>

	<br />

	<xsl:call-template name="TxnCommands" />

	<!-- invoice balancing option for the Online Lockbox -->
	<xsl:variable name="LockboxBalancingOption"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@InvoiceBalancingOption" /></xsl:variable>
	<input type="hidden">
		<xsl:attribute name="name">txtBalancingOption_<xsl:value-of select="$txnlabel" /></xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select="$LockboxBalancingOption" /></xsl:attribute>
	</input>
	
	<!-- invoice balancing action chosen by user -->
	<input type="hidden">
		<xsl:attribute name="name">txtBalancingAction_<xsl:value-of select="$txnlabel" /></xsl:attribute>
		<xsl:attribute name="value"><xsl:value-of select ="$InvoiceBalancingNotRequired" /></xsl:attribute>
	</input>
		
	<script language="javascript">
		var bolForceBalancing = false;
		
		function updatetxn(nexttxnid, formaction) {

			var bolRetVal;
			var arrField
			var bolInvoiceBalancing;

			if(document.forms['frmOLD'].rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" /> == undefined) {
				bolRetVal = true;
			} else if(document.forms['frmOLD'].rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" />[0].checked) {
				
				bolRetVal = true;
				for(var i=0;i&lt;document.forms['frmOLD'].elements.length;++i) {
					arrField = document.forms['frmOLD'].elements[i].name.split('_');
					if(arrField[0] == 'rdoStubFieldDecisionStatus') {
						if(document.forms['frmOLD'].elements[document.forms['frmOLD'].elements[i].name][1].checked) {
							bolRetVal = false;
						}
					}
				}
				
				if(!bolRetVal) {
					bolRetVal = confirm('Rejecting any Data Entry item will cause the Transaction to be rejected.	Mark Transaction as rejected and continue?');

					if(bolRetVal) {
						document.forms['frmOLD'].rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" />[1].checked = true;
					}
				}
				
				// use invoice balancing option defined
				if (document.forms['frmOLD'].txtBalancingOption_<xsl:value-of select="$txnlabel" /> != undefined) {
					if (document.forms['frmOLD'].txtBalancingOption_<xsl:value-of select="$txnlabel" />.value > 0) {
						bolInvoiceBalancing = true;
					}
				}

				// invoice balancing will not be performed if rejecting item
				if (document.forms['frmOLD'].rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" />[1].checked == true) {
					bolInvoiceBalancing = false;
				}
			} else {
				bolRetVal = true;
			}

			// check if invoice balancing necessary
			if (bolRetVal &amp;&amp; bolInvoiceBalancing) {
				bolRetVal = invoiceBalancing()
			}

			if(bolRetVal) {
				if (bolForceBalancing) {
					// keep user on current transaction to review force balancing row; formaction 'complete' not allowed to continue when balancing.
				document.forms['frmOLD'].txtAction.value = 'updatetxn';
					nexttxnid = document.forms['frmOLD'].hidTransactionID.value;
				} else {
					document.forms['frmOLD'].txtAction.value = formaction;
					if (nexttxnid == null)
						document.forms['frmOLD'].txtNextTxnID.value = document.forms['frmOLD'].hidTransactionID.value;
					else
				document.forms['frmOLD'].txtNextTxnID.value = nexttxnid;
				}
				document.forms['frmOLD'].submit();
			}
		}

		function imgStubFieldDecisionStatus_OnClick(txtStubFieldDecisionStatus, imgStubFieldDecisionStatus) {

			switch(document.getElementById(txtStubFieldDecisionStatus).value) {
				case '<xsl:value-of select="$FieldDecisionStatusAccepted" />':
					document.getElementById(txtStubFieldDecisionStatus).value='<xsl:value-of select="$FieldDecisionStatusRejected" />';
					document.getElementById(imgStubFieldDecisionStatus).src='<xsl:value-of select="$brandtemplatepath" />/images/reject.gif';
					document.getElementById(imgStubFieldDecisionStatus).title='Click to reset this item';
					break;
				case '<xsl:value-of select="$FieldDecisionStatusRejected" />':
					document.getElementById(txtStubFieldDecisionStatus).value='<xsl:value-of select="$FieldDecisionStatusPending" />';
					document.getElementById(imgStubFieldDecisionStatus).src='<xsl:value-of select="$brandtemplatepath" />/images/cbx_off.gif';
					document.getElementById(imgStubFieldDecisionStatus).title='Click to accept this item';
					break;
				case '<xsl:value-of select="$FieldDecisionStatusPending" />':
				default:
					document.getElementById(txtStubFieldDecisionStatus).value='<xsl:value-of select="$FieldDecisionStatusAccepted" />';
					document.getElementById(imgStubFieldDecisionStatus).src='<xsl:value-of select="$brandtemplatepath" />/images/accept.gif';
					document.getElementById(imgStubFieldDecisionStatus).title='Click to reject this item';
					break;
			}
		}
		
		//
		function invoiceBalancing () {
		
			var amtChecks = 0;
			var amtInvoices = 0;
			var bolDesiredRsp = null;
			var strDesiredMesg;
			var bolBalanceRsp = false;
			var strBalanceMesg;
			
			amtChecks = calcCheckAmountTotal();
			amtInvoices = calcInvoiceAmountTotal();
			
			<!-- alert ('Payment Amount Total: ' + amtChecks + ';  Invoice Amount Total: ' + amtInvoices); -->
			
			if (amtChecks != amtInvoices) {
				switch (document.forms['frmOLD'].txtBalancingOption_<xsl:value-of select="$txnlabel" />.value) {
					case '<xsl:value-of select="$InvoiceBalancingDesired" />':
							strDesiredMesg = 'Invoice amounts for this Transaction are out of balance.\n\n';
							strDesiredMesg = strDesiredMesg + '\t&bull; Press OK to perform invoice balancing on this Transaction.\n';
							strDesiredMesg = strDesiredMesg + '\t&bull; Press Cancel to continue without balancing.';
							bolDesiredRsp = confirm (strDesiredMesg);
							if (bolDesiredRsp) {
								strBalanceMesg = 'Invoice amounts for this Transaction are out of balance.\n\n';
								strBalanceMesg = strBalanceMesg + '\t&bull; Press OK to create a new invoice balancing row.\n';
								strBalanceMesg = strBalanceMesg + '\t&bull; Press Cancel to return to the Decisioning Transaction Detail page and correct the out of balance condition.';
								bolBalanceRsp = confirm (strBalanceMesg);
							}
							break;
					case '<xsl:value-of select="$InvoiceBalancingRequired" />':
							strBalanceMesg = 'Invoice amounts for this Transaction are out of balance.\n\n';
							strBalanceMesg = strBalanceMesg + '\t&bull; Press OK to create a new invoice balancing row.\n';
							strBalanceMesg = strBalanceMesg + '\t&bull; Press Cancel to return to the Decisioning Transaction Detail page and correct the out of balance condition.';
							bolBalanceRsp = confirm (strBalanceMesg);
							break;
				}
			} else {
				return (true);
			}
			
			if (bolDesiredRsp == false) {
				document.forms['frmOLD'].txtBalancingAction_<xsl:value-of select="$txnlabel" />.value = '<xsl:value-of select ="$InvoiceBalancingActionSaveUnbalanced" />';
				return (true);
			} else {
				if (bolBalanceRsp) {
					document.forms['frmOLD'].txtBalancingAction_<xsl:value-of select="$txnlabel" />.value = '<xsl:value-of select ="$InvoiceBalancingActionForceBalance" />';
					bolForceBalancing = true;
					return (true);
				} else {
					// return to page for user manual balance
					return (false);
				}
			}
			
		}
		
		// calculate check amount totals
		function calcCheckAmountTotal () {
			var amtChecks = 0;
			var tmpCheckAmt = 0;
			
			for (var i=0;i&lt;document.forms['frmOLD'].elements.length;++i) {
				arrField = document.forms['frmOLD'].elements[i].name.split('_');
				
				if (arrField.length == 5) {
					if ((arrField[3] == 'checks') &amp;&amp; (arrField[4] == 'Amount')) {
						tmpCheckAmt = stripCurrencyCharacters(document.forms['frmOLD'].elements[document.forms['frmOLD'].elements[i].name].value);
						if (isNumeric(tmpCheckAmt))
							amtChecks = amtChecks + parseFloat(tmpCheckAmt);
					}
				}
				
			}
			return (amtChecks.toFixed(2));
		}
		
		
		// calculate invoice amount totals
		function calcInvoiceAmountTotal () {
			var amtInvoices = 0;
			var tmpInvoiceAmt = 0;
			var amtPaginatedTotal = 0;
			
			for (var i=0;i&lt;document.forms['frmOLD'].elements.length;++i) {
				arrField = document.forms['frmOLD'].elements[i].name.split('_');
				var arrDeleteStub;
				var strDeleteStub;
				
				if (arrField.length == 5) {
					if ((arrField[3] == 'stubs') &amp;&amp; (arrField[4] == 'Amount')) {
						arrDeleteStub = arrField.slice(0,3);
						strDeleteStubChkBx = arrDeleteStub.join('_') + '_chkDeleteStub';
						
						// check balance of fields not marked for deletion
						if (document.forms['frmOLD'].elements[document.forms['frmOLD'].elements[strDeleteStubChkBx].name].checked == false) {
							tmpInvoiceAmt = stripCurrencyCharacters(document.forms['frmOLD'].elements[document.forms['frmOLD'].elements[i].name].value);
							if (isNumeric(tmpInvoiceAmt))
								amtInvoices = amtInvoices + parseFloat(tmpInvoiceAmt);
						}
						
					} else if ((arrField[2] == 'invoicebalancing') &amp;&amp; (arrField[3] == 'amount') &amp;&amp; (arrField[4] == 'paginated')) {
						// account for invoice amounts not displayed due to pagination
						tmpInvoiceAmt = stripCurrencyCharacters(document.forms['frmOLD'].elements[document.forms['frmOLD'].elements[i].name].value);
						if (isNumeric(tmpInvoiceAmt))
							amtPaginatedTotal = parseFloat(tmpInvoiceAmt);
					}
					
				}
			}
			
			amtInvoices = amtInvoices + amtPaginatedTotal;
			
			return (amtInvoices.toFixed(2));
		}
		
		//
		function stripCurrencyCharacters(str) {
  str = str.replace('$','');
  str = str.replace(/,/gi,'');	
  return str;
 } 

	</script>
	
	</form>

	<hr />

</xsl:template>


<xsl:template match="Transactions">
	 
	<xsl:variable name="CheckStartRecord"><xsl:value-of select="CheckInfo/@StartRecord" /></xsl:variable>
	<xsl:variable name="StubStartRecord"><xsl:value-of select="StubInfo/@StartRecord" /></xsl:variable>
	<xsl:variable name="DocumentStartRecord"><xsl:value-of select="DocumentInfo/@StartRecord" /></xsl:variable>

	<xsl:if test="TransactionID = $TransactionID">

	<xsl:if test="$pagemode='edit' and $DepositStatus='241' and $DecisionSourceStatus!=$DecisionSourceStatusNotRequired">
		<table width="98%">
		<tr>
			<td class="reportcaption">Transaction Decision:&nbsp;
			<span class="reportdata">
				<xsl:choose>
				<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusNotRequired">
				No Decision Required
				</xsl:when>
				<xsl:otherwise>
					<input type="radio" value="Accept">
						<xsl:attribute name="name">rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" /></xsl:attribute>
						<xsl:if test="DecisionSourceStatus = $DecisionSourceStatusAccepted">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input> Accept
					<input type="radio" value="Reject">
						<xsl:attribute name="name">rdoTxnDecisionStatus_<xsl:value-of select="$txnlabel" /></xsl:attribute>
						<xsl:if test="DecisionSourceStatus = $DecisionSourceStatusRejected">
							<xsl:attribute name="checked">checked</xsl:attribute>
						</xsl:if>
					</input> Reject
				</xsl:otherwise>
				</xsl:choose>
			</span>
			</td>
		</tr>
		</table>
	</xsl:if>

	<xsl:variable name="IsCommingled"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@IsCommingled"/></xsl:variable>
	<xsl:if test="$IsCommingled = '1'">
		<xsl:call-template name="DestLockbox" />
	</xsl:if>

	<xsl:call-template name="Checks" />
	<xsl:call-template name="Stubs" />
	<xsl:call-template name="Documents" />

	</xsl:if>

</xsl:template>


<xsl:template name="DestLockbox">
	<table width="98%">
	<tr>
		<td class="reportcaption">
      Destination Workgroup:
      <xsl:choose>
			<xsl:when test="$DepositStatus='241' and $pagemode='edit' and $DecisionSourceStatus!=$DecisionSourceStatusNotRequired">
				<select>
					<xsl:attribute name="name">selDestLockbox_<xsl:value-of select="$txnlabel" /></xsl:attribute>
					<option value="[All]">&lt;-- Choose Destination Workgroup --&gt;</option>
					<xsl:for-each select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record">
					<option>
						<xsl:attribute name="value"><xsl:value-of select="@SiteLockboxKey"/></xsl:attribute>
						<xsl:if test="$DestinationLockboxKey = @SiteLockboxKey">
							<xsl:attribute name="selected">selected</xsl:attribute>
						</xsl:if>
						<xsl:value-of select="@LockboxID" /> - <xsl:value-of select="@LongName"/> - <xsl:value-of select="@DestLockboxIdentifierLabel"/>
					</option>
					</xsl:for-each>
				</select>
			</xsl:when>
			<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record/@SiteLockboxKey = $DestinationLockboxKey">
					<xsl:variable name="DestLockboxID"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@LockboxID"/></xsl:variable>
					<xsl:variable name="DestLockboxIdentifierLabel"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@DestLockboxIdentifierLabel"/></xsl:variable>
					<xsl:variable name="DestLockboxLongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@LongName"/></xsl:variable>
					<xsl:value-of select="$DestLockboxID"/> - <xsl:value-of select="$DestLockboxLongName"/> - <xsl:value-of select="$DestLockboxIdentifierLabel"/>
				</xsl:when>
				<xsl:otherwise>Undefined</xsl:otherwise>
			</xsl:choose>
			</xsl:otherwise>
		</xsl:choose>
		</td>
	</tr>
	</table>
</xsl:template>


<xsl:template name="Checks">
	<table width="98%">
		<tr>
			<td class="reportcaption">
				Checks:
			</td>
		</tr>
		<tr>
			<td class="recordcount">
				<xsl:call-template name="CheckResultsLabel" />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td class="reportheading" align="left">Payment Amount</td>
						<td class="reportheading" align="left"></td>
						<td class="reportheading" align="left">R/T</td>
						<td class="reportheading" align="left">Account</td>
						<td class="reportheading" align="left">Serial</td>
						<td class="reportheading" align="left">Payee</td>
						<td class="reportheading" align="left">Payer</td>
						<xsl:for-each select="../DESetupField">
							<xsl:if test="TableName='checksdataentry'">
								<td class="reportheading"><xsl:value-of select="FldName" /></td>
							</xsl:if>
						</xsl:for-each>
						<td class="reportheading">Decisioning Reason</td>
					</tr>
					<xsl:for-each select="Checks">

						<xsl:variable name="GlobalCheckID">
							<xsl:choose>
								<xsl:when test="@GlobalCheckID"><xsl:value-of select="@GlobalCheckID" /></xsl:when>
								<xsl:otherwise><xsl:value-of select="GlobalCheckID" /></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<!-- form field name for hidden Checks.Amount -->
						<xsl:variable name="checklabel">
							<xsl:choose>
								<xsl:when test="Amount">gbid<xsl:value-of select="../../GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" />_checkid<xsl:value-of select="$GlobalCheckID" />_checks_Amount<xsl:text></xsl:text>
							</xsl:when>
								<xsl:otherwise>gbid0_txnid0_checkid</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<tr>
							<xsl:if test='position() mod 2 = 0'>
								<xsl:attribute name="class">evenrow</xsl:attribute>
							</xsl:if>
							<xsl:if test='position() mod 2 != 0'>
								<xsl:attribute name="class">oddrow</xsl:attribute>
							</xsl:if>

							<!-- hidden form field for Checks.Amount -->
							<td class="reportdata" align="left">
								<xsl:value-of select="format-number(./Amount, '$ ###,###.00')" />
								<input type="hidden">
									<xsl:attribute name="name"><xsl:value-of select="$checklabel" /></xsl:attribute>
									<xsl:attribute name="value"><xsl:value-of select="Amount" /></xsl:attribute>
								</input>
							</td>
							
							<td class="reportdata" vAlign="top" align="left" width="16">
								<a title="View Payment Image">
									<xsl:attribute name="onclick">docView('imagepdf.aspx?bank=<xsl:value-of select="../../BankID"/>&amp;lbx=<xsl:value-of select="../../LockboxID"/>&amp;batch=<xsl:value-of select="../../BatchID"/>&amp;date=<xsl:value-of select="../../DepositDate"/>&amp;picsdate=<xsl:value-of select="../../PICSDate"/>&amp;txn=<xsl:value-of select="../TransactionID"/>&amp;item=<xsl:value-of select="./BatchSequence"/>&amp;type=c&amp;page=-1');return false;</xsl:attribute>
									<xsl:attribute name="href">imagepdf.aspx?bank=<xsl:value-of select="../../BankID"/>&amp;lbx=<xsl:value-of select="../../LockboxID"/>&amp;batch=<xsl:value-of select="../../BatchID"/>&amp;date=<xsl:value-of select="../../DepositDate"/>&amp;picsdate=<xsl:value-of select="../../PICSDate"/>&amp;txn=<xsl:value-of select="../TransactionID"/>&amp;item=<xsl:value-of select="./BatchSequence"/>&amp;type=c&amp;page=-1</xsl:attribute>
									<img src="{$brandtemplatepath}/Images/cbo_icon_check.gif" border="0" />
								</a>
							</td>
							<td class="reportdata" align="left"><xsl:value-of select="RT" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="Account" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="Serial" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="PayeeName" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="RemitterName" /></td>


							<xsl:for-each select="../../DESetupField">

								<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
								<xsl:variable name="FldName"><xsl:value-of select="FldName" /></xsl:variable>
								<xsl:variable name="FldDataTypeEnum"><xsl:value-of select="FldDataTypeEnum" /></xsl:variable>
								<xsl:variable name="FldLength"><xsl:value-of select="FldLength" /></xsl:variable>
								<xsl:variable name="DESetupFieldID"><xsl:value-of select="@DESetupFieldID" /></xsl:variable>
								<xsl:variable name="FldData"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/Checks[GlobalCheckID=$GlobalCheckID]/Field[DESetupFieldID=$DESetupFieldID]/FldData" /></xsl:variable>

								<xsl:if test="$TableName='checksdataentry'">
									<td class="reportdata">
 										<xsl:choose>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeString">
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:when>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeDate">
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:when>
												<xsl:when test="number($FldDataTypeEnum) = $FldTypeFloat">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test="string(number($FldData)) != 'NaN'">
													<xsl:value-of select="format-number($FldData, '#,##0.00')"/>
												</xsl:if>
											</xsl:when>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeCurrency">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test="string(number($FldData)) != 'NaN'">
													$<xsl:value-of select="format-number($FldData, '#,##0.00')"/>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:otherwise>
										</xsl:choose>
									</td>
								</xsl:if>
							</xsl:for-each>

							<td class="reportdata"><xsl:value-of select="DecisioningReasonDesc" /></td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>		 		
		<tr>
			<td colspan="3">
				<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<xsl:call-template name="CheckPaginationLinks" />
						</td>
					</tr>
				</table>
			</td>
		</tr>		 		
	</table>
</xsl:template>

<xsl:template name="Stubs">

	<!-- form field name for hidden Invoice Amount Totals -->
	<xsl:variable name="invoiceamountlabel">
		<xsl:choose>
			<xsl:when test="../Transactions[TransactionID=$TransactionID][InvoiceAmountPaginated]">gbid<xsl:value-of select="../GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" />_invoicebalancing_amount_paginated</xsl:when>
			<xsl:otherwise>gbid0_txnid0_invoicebalancing_amount_paginated</xsl:otherwise>
		</xsl:choose>
	</xsl:variable>

	<table width="98%">
		<tr>
			<td class="reportcaption">
				<br />
				Data Entry:
				<input type="hidden">
					<xsl:attribute name="name"><xsl:value-of select="$invoiceamountlabel" /></xsl:attribute>
					<xsl:attribute name="value"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/InvoiceAmountPaginated" /></xsl:attribute>
				</input>
			</td>
		</tr>
		<xsl:if test="string-length(/Page/FormFields/DEItemRowDataID) = 0">
			<tr>
				<td class="recordcount">
					<xsl:call-template name="StubResultsLabel" />
				</td>
			</tr>
		</xsl:if>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="0" cellspacing="0" border="0" class="reportmain">
					<tr>
						<xsl:if test="$DepositStatus='241' and $pagemode='edit' and $DecisionSourceStatus!=$DecisionSourceStatusNotRequired">
							<td class="reportheading">Delete</td>
						</xsl:if>
						<td class="reportheading">Row No</td>
						<xsl:for-each select="../DESetupField">
							<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
							<xsl:if test="$TableName='stubs' or $TableName='stubsdataentry'">
								<td class="reportheading">
									<xsl:value-of select="Title" />
								</td>
							</xsl:if>
						</xsl:for-each>
					</tr>
					<xsl:for-each select="Stubs">
						<xsl:variable name="DEItemRowDataID">
							<xsl:choose>
								<xsl:when test="@DEItemRowDataID"><xsl:value-of select="@DEItemRowDataID" /></xsl:when>
								<xsl:otherwise><xsl:value-of select="DEItemRowDataID" /></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<xsl:variable name="stublabel">gbid<xsl:value-of select="../../GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" />_deitemrowdataid<xsl:value-of select="$DEItemRowDataID" />_</xsl:variable>

						<tr>
						<xsl:if test='position() mod 2 = 0'>
							<xsl:attribute name="class">evenrow</xsl:attribute>
						</xsl:if>
						<xsl:if test='position() mod 2 != 0'>
							<xsl:attribute name="class">oddrow</xsl:attribute>
						</xsl:if>

							<xsl:if test="$DepositStatus='241' and $pagemode='edit' and $DecisionSourceStatus!=$DecisionSourceStatusNotRequired">
								<td class="reportdata">
									<input type="checkbox">
										<xsl:attribute name="name"><xsl:value-of select="$stublabel" />chkDeleteStub</xsl:attribute>
									</input>
								</td>
							</xsl:if>
							<td class="reportdata" valign="top"><xsl:value-of select="RowNo" /></td>

							<xsl:for-each select="../../DESetupField">

								<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
								<xsl:variable name="FldName"><xsl:value-of select="FldName" /></xsl:variable>
								<xsl:variable name="FldDataTypeEnum"><xsl:value-of select="FldDataTypeEnum" /></xsl:variable>
								<xsl:variable name="FldLength"><xsl:value-of select="FldLength" /></xsl:variable>
								<xsl:variable name="DESetupFieldID"><xsl:value-of select="@DESetupFieldID" /></xsl:variable>
								<xsl:variable name="FldData"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/Stubs[DEItemRowDataID=$DEItemRowDataID]/Field[DESetupFieldID=$DESetupFieldID]/FldData" /></xsl:variable>
								<xsl:variable name="FieldDecisionStatus"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/Stubs[DEItemRowDataID=$DEItemRowDataID]/Field[DESetupFieldID=$DESetupFieldID]/FieldDecisionStatus" /></xsl:variable>
								<xsl:variable name="DecisioningReasonDesc"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/Stubs[DEItemRowDataID=$DEItemRowDataID]/Field[DESetupFieldID=$DESetupFieldID]/DecisioningReasonDesc" /></xsl:variable>

								<xsl:if test="$TableName='stubs' or $TableName='stubsdataentry'">
									<td class="reportdata" vAlign="top">
										<xsl:choose>
											<xsl:when test="$DepositStatus='241' and $pagemode='edit' and $DecisionSourceStatus!=$DecisionSourceStatusNotRequired">
												<input type="text">
													<xsl:attribute name="name"><xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
													<xsl:attribute name="size">
														<xsl:value-of select="$FldLength" />
													</xsl:attribute>
													<xsl:attribute name="maxlength">
														<xsl:value-of select="$FldLength" />
													</xsl:attribute>
													<xsl:attribute name="value">
														<xsl:choose>
															<xsl:when test="number($FldDataTypeEnum) = $FldTypeCurrency">
																<xsl:if test="string(number($FldData)) != 'NaN'">
																	<xsl:value-of select="format-number($FldData, '#,##0.00')" />
																</xsl:if>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="$FldData" />
															</xsl:otherwise>
														</xsl:choose>
													</xsl:attribute>
												</input>
												<xsl:choose>
													<xsl:when test="$FieldDecisionStatus=$FieldDecisionStatusNoneRequired or string-length($FieldDecisionStatus)=0">&nbsp;</xsl:when>
													<xsl:otherwise>
														<!--
														<a>
															<xsl:attribute name="href">javascript:imgStubFieldDecisionStatus_OnClick('txtStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" />', 'imgStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" />');</xsl:attribute>
															<img border="0">
																<xsl:attribute name="name">imgStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
																<xsl:attribute name="id">imgStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
																<xsl:attribute name="src">
																	<xsl:choose>
																		<xsl:when test="$FieldDecisionStatus=$FieldDecisionStatusAccepted"><xsl:value-of select="$brandtemplatepath" />/images/accept.gif</xsl:when>
																		<xsl:when test="$FieldDecisionStatus=$FieldDecisionStatusRejected"><xsl:value-of select="$brandtemplatepath" />/images/reject.gif</xsl:when>
																		<xsl:otherwise><xsl:value-of select="$brandtemplatepath" />/images/cbx_off.gif</xsl:otherwise>
																	</xsl:choose>
																</xsl:attribute>
																<xsl:attribute name="title">
																	<xsl:choose>
																		<xsl:when test="$FieldDecisionStatus=$FieldDecisionStatusAccepted">Click to reject this item</xsl:when>
																		<xsl:when test="$FieldDecisionStatus=$FieldDecisionStatusRejected">Click to reset this item</xsl:when>
																		<xsl:otherwise>Click to accept this item</xsl:otherwise>
																	</xsl:choose>
																</xsl:attribute>
															</img>
														</a>
														<input type="hidden">
															<xsl:attribute name="name">txtStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
															<xsl:attribute name="id">txtStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
															<xsl:attribute name="value"><xsl:value-of select="$FieldDecisionStatus" /></xsl:attribute>
														</input>
														-->
														
														<input type="hidden">
															<xsl:attribute name="name">txtStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
															<xsl:attribute name="id">txtStubFieldDecisionStatus_<xsl:value-of select="$stublabel" /><xsl:value-of select="$TableName" />_<xsl:value-of select="$FldName" /></xsl:attribute>
															<xsl:attribute name="value">_TXN_</xsl:attribute>
														</input>
													</xsl:otherwise>
												</xsl:choose>
											</xsl:when>
											<xsl:otherwise>
												<xsl:choose>
													<xsl:when test="number($FldDataTypeEnum) = $FldTypeCurrency">
														<xsl:if test="string(number($FldData)) != 'NaN'">
															<xsl:value-of select="format-number($FldData, '#,##0.00')" />
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$FldData" />
													</xsl:otherwise>
												</xsl:choose>
											</xsl:otherwise>
										</xsl:choose>
										<br />
										<xsl:value-of select="$DecisioningReasonDesc" />
									</td>
								</xsl:if>
							</xsl:for-each>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<xsl:call-template name="StubPaginationLinks" />
						</td>
					</tr>
				</table>
			</td>
		</tr>		 		
	</table>
</xsl:template>

<xsl:template name="Documents">
	<table width="98%">
		<tr>
			<td class="reportcaption">
				<br />
				Documents:
			</td>
		</tr>
		<tr>
			<td class="recordcount">
				<xsl:call-template name="DocumentResultsLabel" />
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
					<tr>
						<td class="reportheading">Document</td>
						<td class="reportheading">Description</td>
						<td width="66%" align="left" class="reportheading">&nbsp;</td>
					</tr>
					<xsl:for-each select="Documents">

						<xsl:variable name="GlobalDocumentID">
							<xsl:choose>
								<xsl:when test="@GlobalDocumentID"><xsl:value-of select="@GlobalDocumentID" /></xsl:when>
								<xsl:otherwise><xsl:value-of select="GlobalDocumentID" /></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>

						<xsl:variable name="documentlabel">gbid<xsl:value-of select="../../GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" />_docid<xsl:value-of select="$GlobalDocumentID" />_</xsl:variable>
						<xsl:variable name="imageurl">bank=<xsl:value-of select="../../BankID"/>&amp;lbx=<xsl:value-of select="../../LockboxID"/>&amp;batch=<xsl:value-of select="../../BatchID"/>&amp;date=<xsl:value-of select="../../DepositDate"/>&amp;picsdate=<xsl:value-of select="../../PICSDate"/>&amp;txn=<xsl:value-of select="../TransactionID"/>&amp;item=<xsl:value-of select="./BatchSequence"/>&amp;page=-1</xsl:variable>
						<tr>
							<xsl:if test='position() mod 2 = 0'>
								<xsl:attribute name="class">evenrow</xsl:attribute>
							</xsl:if>
							<xsl:if test='position() mod 2 != 0'>
								<xsl:attribute name="class">oddrow</xsl:attribute>
							</xsl:if>

							<td class="reportdata"><xsl:value-of select="DocumentSequence" /></td>
							<td class="reportdata"><xsl:value-of select="Description" /></td>

							<td align="left" class="reportdata">
								<a>
									<xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
									<xsl:attribute name="title">View <xsl:value-of select="Fld[@ID='Description']"/></xsl:attribute>
									<xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
									<img src="{$brandtemplatepath}/Images/cbo_icon_document.gif" border="0"/>
								</a>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
					<tr>
						<td>
							<xsl:call-template name="DocumentPaginationLinks" />
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</xsl:template>


<xsl:template name="CheckResultsLabel">
	<xsl:for-each select="CheckInfo">
	<xsl:call-template name="ShowingResultsLabel">
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
	</xsl:call-template>
	</xsl:for-each>
</xsl:template>

<xsl:template name="StubResultsLabel">
	<xsl:for-each select="StubInfo">
	<xsl:call-template name="ShowingResultsLabel">
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
	</xsl:call-template>
	</xsl:for-each>
</xsl:template>

<xsl:template name="DocumentResultsLabel">
	<xsl:for-each select="DocumentInfo">
	<xsl:call-template name="ShowingResultsLabel">
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
	</xsl:call-template>
	</xsl:for-each>
</xsl:template>

<xsl:template name="CheckPaginationLinks">
	<xsl:for-each select="CheckInfo">
	<td align="right" valign="bottom" class="pagelinks">
	<br />
	<xsl:call-template name="PaginationLinks">
		<xsl:with-param name="controlname">document.forms['frmOLD'].txtCheckAction</xsl:with-param>
		<xsl:with-param name="startrecordcontrolname">document.forms['frmOLD'].txtCheckStartRecord</xsl:with-param>
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
	</xsl:call-template>
	</td>
	</xsl:for-each>
</xsl:template>

<xsl:template name="StubPaginationLinks">
	<xsl:for-each select="StubInfo">
	<td align="right" valign="bottom" class="pagelinks">
	<br />
	<xsl:call-template name="PaginationLinks">
		<xsl:with-param name="controlname">document.forms['frmOLD'].txtStubAction</xsl:with-param>
		<xsl:with-param name="startrecordcontrolname">document.forms['frmOLD'].txtStubStartRecord</xsl:with-param>
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
	</xsl:call-template>
	</td>
	</xsl:for-each>
</xsl:template>

<xsl:template name="DocumentPaginationLinks">
	<xsl:for-each select="DocumentInfo">
	<td align="right" valign="bottom" class="pagelinks">
		<br />
		<xsl:call-template name="PaginationLinks">
		<xsl:with-param name="controlname">document.forms['frmOLD'].txtDocumentAction</xsl:with-param>
		<xsl:with-param name="startrecordcontrolname">document.forms['frmOLD'].txtDocumentStartRecord</xsl:with-param>
		<xsl:with-param name="startrecord"><xsl:value-of select="@StartRecord" /></xsl:with-param>
		</xsl:call-template>
	</td>
	</xsl:for-each>
</xsl:template>

<xsl:template name="TxnCommands">
	<table cellspacing="5" cellpadding="0" border="0" width="98%">
		<tr>
			<td class="reportcaption" align="center" valign="bottom">
				<xsl:if test="$pagemode='edit'">
					<xsl:choose>
						<xsl:when test="$DepositStatus=240">
							<xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='modify'] and $DepositStatus='240'">
								<input type="submit" value="Check Out" name="btnCheckOut" />&nbsp;
							</xsl:if>
						</xsl:when>
						<xsl:otherwise>
							<input type="button" name="btnUpdate" value=" Save ">
								<xsl:attribute name="onclick">javascript:updatetxn('<xsl:value-of select="$TransactionID" />','updatetxn');</xsl:attribute>
							</input>&nbsp;
							<xsl:if test="count(/Page/Batch/Transactions[DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon or DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE]) = 0">
								<input type="button" value="Complete" name="btnComplete" onclick="javascript:updatetxn(null,'complete');" />&nbsp;
							</xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:if>
				<xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='reset'] and $DepositStatus='241'">
					<input type="submit" value="Reset" name="btnReset" />&nbsp;
				</xsl:if>
			</td>
			<td align="right" class="pagelinks">
				<a><xsl:attribute name="href">oldbatches.aspx?OLLockboxID=<xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@OLLockboxID" /></xsl:attribute>Select Batch</a>
				&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
				<a><xsl:attribute name="href">oldbatchdetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="/Page/FormFields/GlobalBatchID" /></xsl:attribute>View Batch</a>

				<xsl:for-each select="Transactions">
					<xsl:if test="TransactionID = $TransactionID">

						<xsl:variable name="previouspos"><xsl:value-of select="position() -1" /></xsl:variable>
						<xsl:variable name="nextpos"><xsl:value-of select="position() +1" /></xsl:variable>

						<xsl:variable name="previoustransaction">
							<xsl:if test="../Transactions[1]/TransactionID != ./TransactionID">
								<xsl:value-of select="../Transactions[number($previouspos)]/TransactionID" />
							</xsl:if>
						</xsl:variable>
						<xsl:variable name="nexttransaction">
							<xsl:if test="../Transactions[number($nextpos)]/TransactionID != ./TransactionID">
								<xsl:value-of select="../Transactions[number($nextpos)]/TransactionID" />
							</xsl:if>
						</xsl:variable>

						&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;

						<xsl:choose>
							<xsl:when test="$previoustransaction = ''">
								<span class="pagelinkssymbolsoff">&laquo;</span>&nbsp;
								Previous Transaction
							</xsl:when>
							<xsl:otherwise>
								<a title="Save current Transaction and view previous"><xsl:attribute name="href">javascript:updatetxn('<xsl:value-of select="$previoustransaction" />','updatetxn');</xsl:attribute>
									<span class="pagelinkssymbolson">&laquo;</span>&nbsp;
									Previous Transaction</a>
							</xsl:otherwise>
						</xsl:choose>
						&nbsp;&nbsp;|&nbsp;&nbsp;
						<xsl:choose>
							<xsl:when test="$nexttransaction = ''">
								Next Transaction<span class="pagelinkssymbolsoff">&raquo;</span>
							</xsl:when>
							<xsl:otherwise>
								<a title="Save current Transaction and view next"><xsl:attribute name="href">javascript:updatetxn('<xsl:value-of select="$nexttransaction" />','updatetxn');</xsl:attribute>
									Next Transaction
								</a><span class="pagelinkssymbolson">&raquo;</span>
							</xsl:otherwise>
						</xsl:choose>

					</xsl:if>

				</xsl:for-each>
			</td>
		</tr>
	</table>
</xsl:template>

<xsl:template name="DisplayMessages">
	<xsl:param name="type"/>
	<xsl:param name="caption">Messages</xsl:param>
	<xsl:param name="icon"><xsl:value-of select="$brandtemplatepath" />/Images/critical.gif</xsl:param>

	<table border="1" cellpadding="0" cellspacing="0" width="400" align="center" class="errormain" >
		<tr>
			<td>
			<table border="0" cellpadding="5" cellspacing="0" width="400" align="center" class="errorsub">
				<tr>
					<td align="center" valign="middle" class="errortitle" colspan="2"><xsl:value-of select="$caption"/></td>
				</tr>
				<tr align="center" valign="middle">
					<td>
						<img src="{$brandtemplatepath}/Images/critical.gif">
							<xsl:attribute name="src"><xsl:value-of select="$icon"/></xsl:attribute>
						</img>
					</td>
					<td align="left" valign="top" width="320" class="errordetail">
						<menu>
							<xsl:for-each select="Message[@Type=$type]">
								<!--
								<xsl:choose>
									<xsl:when test="@MsgEnum='DecisionDeadlineWarning'">
										<li>CUSTOM WARNING MESSAGE HERE</li>
									</xsl:when>
									<xsl:when test="@MsgEnum='DecisionDeadlineExpired'">
										<li>CUSTOM WARNING MESSAGE HERE</li>
									</xsl:when>
									<xsl:otherwise>
										<li><xsl:value-of select="@Text"/></li>
									</xsl:otherwise>
								</xsl:choose>
								-->
								<li><xsl:value-of select="@Text"/></li>
							</xsl:for-each>
						</menu>
					</td>
				</tr>
			</table>
			</td>
		</tr>
	</table>
</xsl:template>

</xsl:stylesheet>
