<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "<xsl:text disable-output-escaping='yes'>&amp;copy;</xsl:text>">
	<!ENTITY middot "<xsl:text disable-output-escaping='yes'>&amp;middot;</xsl:text>">
	<!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="incCustomBranding.xsl"/>
	<xsl:include href="incMessages.xsl"/>

	<xsl:output method="html"/>


<xsl:template match="/">

  <xsl:choose>
 <xsl:when test="/Page/FormFields/FinishedNowCloseMe='1'">
				<script language="javascript">
				 parent.window.opener.window.focus();
				 parent.window.opener.window.location.reload();
				 parent.window.close();
				</script>
 </xsl:when>
 <xsl:otherwise>
   <xsl:apply-templates select="Page" />
 </xsl:otherwise>
  </xsl:choose>

</xsl:template>


<xsl:template match="Page">

<html>
<head>

	<title>Request Complete</title>
	<link rel="stylesheet" text="text/css" href="{$brandtemplatepath}/stylesheet.css" />

	<script language="javascript" src="{$brandtemplatepath}/js/scriptlib.js" />
	<script language="javascript" src="{$brandtemplatepath}/js/openwindow.js" />
	<script language="javascript" src="{$brandtemplatepath}/js/menu.js" />

	<script language="javascript" src="{$brandtemplatepath}/js/AnchorPosition.js" />
	<script language="javascript" src="{$brandtemplatepath}/js/PopupWindow.js" />
	<script language="JavaScript" src="{$brandtemplatepath}/js/browserdetect.js" />

</head>
<body>

	<xsl:apply-templates select="/Page/Messages"/>

  <form name="frmMain" method="POST">

 <input type="hidden" name="txtAction" value="" />

 <input type="hidden" name="txtDisplayMode">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/DisplayMode" />
   </xsl:attribute>
 </input>

 <input type="hidden" name="txtLockboxRemitterID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/LockboxRemitterID" />
   </xsl:attribute>
 </input>

 <input type="hidden" name="txtOLLockboxID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/OLLockboxID" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtBankID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/BankID" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtLockboxID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/LockboxID" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtBatchID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/BatchID" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtDepositDate">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/DepositDate" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtTransactionID">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/TransactionID" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtBatchSequence">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/BatchSequence" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtRoutingNumber">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/RoutingNumber" />
   </xsl:attribute>
 </input>
 <input type="hidden" name="txtAccount">
   <xsl:attribute name="value">
  <xsl:value-of select="/Page/FormFields/AccountNumber" />
   </xsl:attribute>
 </input>


	  <table align="center" width="400" border="1" cellpadding="2" cellspacing="0">
		  <tr>
			  <td align="center" class="errortitle"><xsl:value-of select="FormTitle" /></td>
		  </tr>
		  <tr>
			  <td>
				  <table align="center" width="100%" border="0" cellpadding="2" cellspacing="0">
					  <tr>
     <td align="right" class="formlabel">Workgroup:&nbsp;</td>
						  <td align="left" class="formfield">
    <xsl:value-of select="/Page/FormFields/LockboxID" />
						  </td>
					  </tr>
					  <tr>
						  <td align="right" class="formlabel">R/T:&nbsp;</td>
						  <td align="left" class="formfield">
    <xsl:value-of select="/Page/FormFields/RoutingNumber" />
						  </td>
					  </tr>
					  <tr>
						  <td align="right" class="formlabel">Account Number:&nbsp;</td>
						  <td align="left" class="formfield">
    <xsl:value-of select="/Page/FormFields/AccountNumber" />
						  </td>
					  </tr>
					  <tr>
						  <td align="right" class="formlabel">Payer:&nbsp;</td>
						  <td align="left" class="formfield">
				  <input type="text" name="txtRemitterName" size="20" maxlength="60">		
						   <xsl:attribute name="value">
						  <xsl:value-of select="/Page/FormFields/RemitterName" />
      </xsl:attribute>
						 </input>
						  </td>
					  </tr>
					  <tr>
						  <td align="center" colspan="2">
				  <input type="button" name="cmdUpdate" class="formfield" value="Submit" onClick="formSubmit(this, 'txtAction')" />&nbsp;
				  <input type="button" name="cmdCancel" class="formfield" value="Cancel" onClick="parent.window.opener.window.focus();parent.window.close();" />
						  </td>
					  </tr>
				  </table>
			  </td>
		  </tr>
	  </table>
</form>

	<script language="JavaScript">
		<xsl:comment>
 //Hide script from older browsers
 //Set form field validation requirements
 with (document.forms["frmMain"]){
	 if (elements["txtRoutingNumber"]){
		 txtRoutingNumber.isRequired = true;
		 txtRoutingNumber.fieldname = 'R/T';
	 }
	 if (elements["txtAccount"]){
		 txtAccount.isRequired = true;
		 txtAccount.fieldname = 'Account Number';
	 }
	 if (elements["txtRemitterName"]){
		 txtRemitterName.isRequired = true;
		 txtRemitterName.fieldname = 'Payer';
	 }

 }
 //End hiding-->
		</xsl:comment>
	</script>

  <script language="JavaScript1.2" type="text/javascript">
	  initFocusHandler();
	  initFormFocus();
  </script>

</body>
</html>

</xsl:template>

</xsl:stylesheet> 

