<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html" indent="no"/>

<xsl:template name="Title">
	Rejected Decisioning Transactions
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Rejected Decisioning Transactions</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">

 <xsl:variable name="startrecord"><xsl:value-of select="PageInfo/@StartRecord"/></xsl:variable>
 <xsl:variable name="OLLockboxID">
  <xsl:choose>
   <xsl:when test="count(/Page/Recordset[@Name='UserLockboxes']/Record) = 1">
    <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@OLLockboxID" />
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="/Page/FormFields/OLLockboxID" />
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>

 <form method="post" name="frmOLD">
  <input type="hidden" name="txtAction" />
  <input type="hidden" name="txtStart">
   <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
  </input>

  <div id="divCalendar" style="position: absolute; background-color: white; visibility: hidden;" />

	 <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0">
   <tr>
    <td>
				 <table cellpadding="5" cellspacing="0" border="0">
					 <tr>
						 <td class="formlabel">Date Range:</td>
						 <td class="formlabel">Workgroup:</td>
						 <td>&nbsp;</td>
					 </tr>
					 <tr>
						 <td class="formlabel">
        <input type="text" name="txtStartDate" maxLength="10" size="25" class="formfield">
         <xsl:attribute name="value"><xsl:value-of select="/Page/FormFields/StartDate" /></xsl:attribute>
        </input>
        <a href="#" name="ancStartDate" id="ancStartDate" onClick="getDateFor(document.forms['frmOLD'].txtStartDate, 'ancStartDate', 'divCalendar', false);return false;"><img src="{$brandtemplatepath}/Images/Calendar.jpg" border="0" align="top" alt="Select Date" WIDTH="25" HEIGHT="26"/></a>
        &nbsp;-&nbsp;
        <input type="text" name="txtEndDate" maxLength="10" size="25" class="formfield">
         <xsl:attribute name="value"><xsl:value-of select="/Page/FormFields/EndDate" /></xsl:attribute>
        </input>
        <a href="#" name="ancEndDate" id="ancEndDate" onClick="getDateFor(document.forms['frmOLD'].txtEndDate, 'ancEndDate', 'divCalendar', false);return false;"><img src="{$brandtemplatepath}/Images/Calendar.jpg" border="0" align="top" alt="Select Date" WIDTH="25" HEIGHT="26"/></a>
						 </td>
       <td class="formfield">
       <xsl:choose>
        <xsl:when test="count(/Page/Recordset[@Name='UserLockboxes']/Record)=1">
         <input type="hidden" name="cboOLLockbox">
          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@OLLockboxID" /></xsl:attribute>
          <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@LongName" />
         </input>
        </xsl:when>
        <xsl:otherwise>
         <select name="cboOLLockbox" class="formfield" onchange="this.form.submit();">
          <option value="-1">&lt;-- Select workgroup --&gt;</option>
          <xsl:for-each select="/Page/Recordset[@Name='UserLockboxes']/Record">
           <option>
            <xsl:attribute name="value"><xsl:value-of select="@OLLockboxID" /></xsl:attribute>
            <xsl:if test="@OLLockboxID = $OLLockboxID">
             <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@LockboxID" /> - <xsl:value-of select="@LongName" />
           </option>
          </xsl:for-each>
         </select>
        </xsl:otherwise>
       </xsl:choose>
       </td>
						 <td align="center">
        <input class="formfield" onclick="formSubmit(this, 'txtAction');" type="button" value="Go" name="cmdSubmit" />
						 </td>
					 </tr>
     </table>
    </td>
   </tr>
  </table>
  
  <table onmouseover="popMenu();" cellSpacing="0" cellPadding="0" width="98%" align="center" border="0">
   <tbody>
    <tr>
     <td class="contentbreak"><img height="1" src="{$brandtemplatepath}/Images/shim.gif" width="1" border="0" /></td>
    </tr>
   </tbody>
  </table>

  <script language="JavaScript">

   //Set form field validation requirements
   with (document.forms["frmMain"]){
    if (elements["txtStartDate"]){
     txtStartDate.isDate = true;
     txtStartDate.isRequired = true;
     txtStartDate.fieldname = 'Start Date';
    }
    if (elements["txtEndDate"]){
     txtEndDate.isDate = true;
     txtEndDate.isRequired = true;
     txtEndDate.fieldname = 'End Date';
    }
   }
  </script>
  <p></p>
  <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
   <tr>
    <td>
     <table width="100%" cellpadding="5" cellspacing="0" border="0">
      <tr>
       <td class="reportcaption">
         Workgroup: <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@OLLockboxID=$OLLockboxID]/@LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@OLLockboxID=$OLLockboxID]/@LongName" /><br />
       </td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td>
			  <xsl:call-template name="ShowingResultsLabel">
				  <xsl:with-param name="startrecord"><xsl:value-of select="$startrecord" /></xsl:with-param>
			  </xsl:call-template>
    </td>
   </tr>
   <tr>
    <td align="left" valign="top">
     <table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
      <tr class="oddrow">
       <td class="reportheading">Workgroup</td>
       <td class="reportheading">Deposit Date</td>
       <td class="reportheading">Processing Date</td>
       <td class="reportheading">Batch ID</td>
       <td class="reportheading">Transaction ID</td>
       <td class="reportheading"></td>
      </tr>

      <xsl:if test="count(RespRejectedTransactions/Batches/Batch/Transactions) = 0">
       <tr class="evenrow">
        <td class="reportdata" colspan="6" align="center">
         <xsl:call-template name="NoResultsLabel" />
        </td>
       </tr>
      </xsl:if>


      <xsl:for-each select="RespRejectedTransactions/Batches/Batch/Transactions">

													<tr>

														<xsl:if test='position() mod 2 = 0'>
																<xsl:attribute name="class">evenrow</xsl:attribute>
														</xsl:if>
														<xsl:if test='position() mod 2 != 0'>
															<xsl:attribute name="class">oddrow</xsl:attribute>
														</xsl:if>

       <td class="reportdata"><xsl:value-of select="../LockboxID" />&nbsp;&middot;&nbsp;<xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@OLLockboxID=$OLLockboxID]/@LongName" /></td>
       <td class="reportdata"><xsl:value-of select="../DepositDate" /></td>
       <td class="reportdata"><xsl:value-of select="../ProcessingDate" /></td>
       <td class="reportdata"><xsl:value-of select="../BatchID" /></td>
       <td class="reportdata"><xsl:value-of select="TransactionID" /></td>
       <td class="reportdata" align="right">
        <a>
          <xsl:attribute name="href">oldrejectedtransactiondetail.aspx?rejectbatchid=<xsl:value-of select="../RejectedBatchID" />&amp;transactionid=<xsl:value-of select="TransactionID" /></xsl:attribute>
 															<img border="0" src="{$brandtemplatepath}/Images/cbo_icon_batchdetail.jpg" width="16" height="16" alt="View Batch Detail"/>
        </a>
       </td>
      </tr>
      </xsl:for-each>

     </table>
    </td>
   </tr>
  </table>
  <p></p>
  <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
		   <td align="right" valign="bottom" class="pagelinks">
			   <br />
				  <xsl:call-template name="PaginationLinks">
					  <xsl:with-param name="controlname">document.forms['frmOLD'].txtAction</xsl:with-param>
					  <xsl:with-param name="startrecord"><xsl:value-of select="$startrecord" /></xsl:with-param>
				  </xsl:call-template>
		   </td>
	   </tr>
  </table>
  <p></p>

 </form>
</xsl:template>

</xsl:stylesheet>
