<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
 <!ENTITY nbsp "&#160;">
 <!ENTITY copy "&#169;">
 <!ENTITY middot "&#183;">
 <!ENTITY laquo "&#171;">
 <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:include href="incCustomBranding.xsl"/>

	<xsl:output method="html"/>

 <xsl:template name="Title">
  Property Management Account Detail
 </xsl:template>

 <xsl:template name="PageTitle">
  <span class="contenttitle">Property Management Account Detail</span>
  <br/>
  <span class="contentsubtitle">&nbsp;(Individual Management Company HOA Deposit Detail)</span>
 </xsl:template>

 <xsl:template match="Page">
  <html>
   <head>
    <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
    <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/scriptlib.js" />
   </head>
   <body>
    <div id="container">
     <div id="header">
      <h1>
       <p/>
       Property Management Account Detail
      </h1>
     </div>
     <div id="subheader">
      <h1>
      </h1>
     </div>
     <div id="content">
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
       <xsl:if test="Recordset[@Name='HOAPMADetailHeader']">
        <xsl:apply-templates select="Recordset[@Name = 'HOAPMADetailHeader']" mode="HOAPMADetailHeader"/>
       </xsl:if>
       <xsl:if test="Recordset[@Name='HOAPMADetailData']">
        <xsl:apply-templates select="Recordset[@Name = 'HOAPMADetailData']" mode="HOAPMADetailData"/>
       </xsl:if>
       <xsl:if test="Recordset[@Name='HOAPMADetailFooter']">
        <xsl:apply-templates select="Recordset[@Name = 'HOAPMADetailFooter']" mode="HOAPMADetailFooter"/>
       </xsl:if>
      </table>
     </div>
     <div id="footer">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  <xsl:variable name="printviewurl">haveparameters=yes&amp;reportname=HOAPMADetail&amp;customer=<xsl:value-of select="Inputs/@CustomerID"/>&amp;lbx=<xsl:value-of select="Inputs/@LockboxID"/>&amp;date=<xsl:value-of select="Inputs/@DepositDate"/>&amp;HOA_Number=<xsl:value-of select="Inputs/@HOA_Number"/></xsl:variable>
				  <a>
					  <xsl:attribute name="href">JavaScript:newPopup('HOAReports.aspx?<xsl:value-of select="$printviewurl"/>&amp;printview=1');</xsl:attribute>
					  <nobr>Printer-Friendly Version</nobr>
				  </a>
     </div>

    </div>
   </body>
  </html>

 </xsl:template>

 <xsl:template match="Recordset" mode="HOAPMADetailHeader">
  <xsl:for-each select="Record">
   <tr>
    <td align="left"><h1>Property Manager: <xsl:value-of select="@Lockbox_Name"/></h1></td>
    <td align="right"><h1>Deposit Date: <xsl:value-of select="@DepositDate"/></h1></td>
   </tr>
   <tr>
    <td align="left"><h1>Workgroup Number: <xsl:value-of select="@Lockbox_Number"/></h1></td>
   </tr>
   <tr>
    <td align="left"><h1>HOA: <xsl:value-of select="@HOA_NAME"/></h1></td>
   </tr>
   <tr>
    <td align="left"><h1>HOA Number: <xsl:value-of select="@AccountNumber"/></h1></td>
   </tr>
   <tr>
    <td align="left"><h1>Bank Account Number: <xsl:value-of select="@HOA_DDANO"/></h1></td>
   </tr>
  </xsl:for-each>
 </xsl:template>

 <xsl:template match="Recordset" mode="HOAPMADetailData">
  <tr>
   <td align="left" valign="top" colspan="3">
    <table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
     <tr>
      <td class="hoareportheading" align="left" valign="top">Batch Number</td>
      <td class="hoareportheading" align="left" valign="top">Transaction Number</td>
      <td class="hoareportheading" align="right" valign="top">Payer Account</td>
      <td class="hoareportheading" align="right" valign="top">Check/Trace/Ref Number</td>
      <td class="hoareportheading" align="right" valign="top">Payment Amount</td>
      <td class="hoareportheading" align="right" valign="top">Stub Amount</td>
     </tr>
      <xsl:for-each select="Record">
      <tr>
       <xsl:if test='position() mod 2 = 0'>
        <xsl:attribute name="class">evenrow</xsl:attribute>
       </xsl:if>
       <xsl:if test='position() mod 2 != 0'>
        <xsl:attribute name="class">oddrow</xsl:attribute>
       </xsl:if>

       <td class="reportdata" align="left" valign="top"><xsl:value-of select="@BatchNumber"/></td>
       <td class="reportdata" align="left" valign="top"><xsl:value-of select="@TransactionNumber"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="@RemitterAccount"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="@CheckNumber"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="format-number(@CheckAmount,'#.00')"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="format-number(@StubAmount,'#.00')"/></td>
      </tr>
     </xsl:for-each>
    </table>
   </td>
  </tr>
 </xsl:template>

 <xsl:template match="Recordset" mode="HOAPMADetailFooter">
  <tr><td class="hoareportfooter" colspan="3" >&nbsp;</td></tr>
  <xsl:for-each select="Record">
   <tr>
    <td align="left"><h1>GroupTotal: </h1></td>
    <td align="right"><h1>Totals: <xsl:value-of select="format-number(@Check_Total,'#.00')"/></h1></td>
    <td align="right"><h1><xsl:value-of select="format-number(@Stub_Total,'#.00')"/></h1></td>
   </tr>
   <tr>
    <td align="left"><h1> </h1></td>
    <td align="right"><h1>Counts: <xsl:value-of select="@Check_Cnt"/></h1></td>
    <td align="right"><h1><xsl:value-of select="@Stub_Cnt"/></h1></td>
   </tr>
  </xsl:for-each>
 </xsl:template>

</xsl:stylesheet>
