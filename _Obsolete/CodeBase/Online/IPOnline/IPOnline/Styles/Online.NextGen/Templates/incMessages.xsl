<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>

<xsl:template match="Messages">
	<div class="page-portlet-view">
		<xsl:if test="Message[@Type='Error']">
			<br/>
			<xsl:call-template name="DisplayMessages">
				<xsl:with-param name="caption">Error</xsl:with-param>
				<xsl:with-param name="type">Error</xsl:with-param>
				<xsl:with-param name="icon">&lt;i class="icon-remove-sign icon-3x"&gt;&lt;/i&gt;</xsl:with-param>
			</xsl:call-template>
		</xsl:if>


		<xsl:if test="Message[@Type='Information']">
			<br/>
			<xsl:call-template name="DisplayMessages">
				<xsl:with-param name="caption">Information</xsl:with-param>
				<xsl:with-param name="type">Information</xsl:with-param>
				<xsl:with-param name="icon">&lt;i class="icon-info-sign icon-3x"&gt;&lt;/i&gt;</xsl:with-param>
			</xsl:call-template>
		</xsl:if>

		<xsl:if test="Message[@Type='Warning']">
			<br/>
			<xsl:call-template name="DisplayMessages">
				<xsl:with-param name="caption">Warning</xsl:with-param>
				<xsl:with-param name="type">Warning</xsl:with-param>
				<xsl:with-param name="icon">&lt;i class="icon-warning-sign icon-3x"&gt;&lt;/i&gt;</xsl:with-param>
			</xsl:call-template>
		</xsl:if>
	</div>
</xsl:template>

<xsl:template name="DisplayMessages">
 <xsl:param name="type"/>
 <xsl:param name="caption">Messages</xsl:param>
 <xsl:param name="icon">&lt;i class="icon-remove-sign icon-3x"&gt;&lt;/i&gt;</xsl:param>

 <table border="1" cellpadding="0" cellspacing="0" width="400" align="center" class="errormain" >
  <tr>
   <td>
   <table border="0" cellpadding="5" cellspacing="0" width="400" align="center" class="errorsub">
    <tr>
     <td align="center" valign="middle" class="errortitle" colspan="2"><xsl:value-of select="$caption"/></td>
    </tr>
    <tr align="center" valign="middle">
     <td>
      <xsl:value-of select="$icon" disable-output-escaping="yes"/>
     </td>
     <td align="left" valign="top" width="320" class="errordetail">
      <menu>
       <xsl:for-each select="Message[@Type=$type]">
        <li><xsl:value-of disable-output-escaping="yes" select="@Text"/></li>
       </xsl:for-each>
      </menu>
     </td>
    </tr>
   </table>
   </td>
  </tr>
 </table>
</xsl:template>

</xsl:stylesheet>
