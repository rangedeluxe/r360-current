<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
    <!ENTITY copy "&#169;">
    <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:import href="incPagination.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	Account Summary
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Account Summary</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">
	<form method="post" onSubmit="return false;">
		
		<xsl:attribute name="name">frmDeposits</xsl:attribute>

		<xsl:if test="Recordset[@Name='DailyDeposits']">
			<xsl:apply-templates select="Recordset[@Name = 'DailyDeposits']" mode="DailyDeposits"/>
		</xsl:if>
		<p/>
		<xsl:if test="Form[@Name='frmDeposits']">
			<xsl:apply-templates select="Form[@Name = 'frmDeposits']" mode="frmDeposits"/>
		</xsl:if>
		
	</form>
</xsl:template>


<xsl:template match="Form" mode="frmDeposits">
    <table align="center" width="98%" cellpadding="3" cellspacing="0" border="0">
		<input type="hidden" name="txtAction" value=""/>
		<tr>
			<td align="right" valign="top">


				<div class="input-append" style="margin-left: 5px;">
					<input name="txtDepositDate" type="text" class="input-small" id="StartDatePicker">
						<xsl:attribute name="value">
							<xsl:value-of select="Field[@Name='txtDepositDate']/@Value"/>
						</xsl:attribute>
					</input>
					<a class="btn dpButton icon-no-text" id="StartDateButton">
						<i class="icon-calendar"></i>
					</a>
					&nbsp;&nbsp;
					<input type="button" name="cmdSubmit" class="btn" value="Go" onClick="formSubmit(this, 'txtAction');" style="margin-left: 10px;"/>
				</div>
			</td>
		</tr>
	</table>

	<script language="JavaScript">
		<xsl:comment>
		//Hide script from older browsers
		//Set form field validation requirements
		with (document.forms["frmDeposits"]){
			 if (elements["txtDepositDate"]){
				  txtDepositDate.isDate = true;
				  txtDepositDate.isRequired = true;
				  txtDepositDate.fieldname = 'Deposit Date';
			 }
		//End hiding
		}
		</xsl:comment>
	</script>
</xsl:template>


<xsl:template match="Recordset" mode="DailyDeposits">

    <xsl:variable name="startrecord"><xsl:value-of select="../PageInfo/@StartRecord"/></xsl:variable>

    <input type="hidden" name="txtStart">
        <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
    </input>

	<p/>
	
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td class="">
							<h5 style="display: inline-block">Deposit Date:</h5>
                		    <xsl:if test="../Form[@Name='frmDeposits']/Field[@Name='txtDepositDate']">
								<h5 style="display: inline-block">
									<xsl:value-of select="../Form[@Name='frmDeposits']/Field[@Name='txtDepositDate']/@Value"/>
								</h5>
                		    </xsl:if>
                	    </td>
                        <xsl:variable name="reportlinkurl">haveparameters=no&amp;reportname=HOAAllPMASummary&amp;date=<xsl:value-of select="../Form[@Name='frmDeposits']/Field[@Name='txtDepositDate']/@Value"/></xsl:variable>
                        <xsl:if test="Record[@HOA='True']">
                           <td class="reportdata" align="right" valign="top">
                                <a title="View Property Management Account Summary">
                                    <xsl:attribute name="href">JavaScript:newPopup('HOAReports.aspx?+<xsl:value-of select="$reportlinkurl"/>');</xsl:attribute>
                                    PMA Summary Report <br/> All Companies
                                </a>
                            </td>
                        </xsl:if>
                    </tr>
				</table>
			</td>
		</tr>
		<tr>
			<td align="left" valign="top">
				<table class="grid" width="100%" cellpadding="5" cellspacing="0" border="0">
				  <tr>
						<td class="grid-header">Account</td>
                        <td class="grid-header">&nbsp;</td>
                        <td class="grid-header">&nbsp;</td>
						<td class="grid-header cell-right-align">Batch Count</td>
						<td class="grid-header cell-right-align">Transaction Count</td>
						<td class="grid-header cell-right-align">Deposit Total</td>
						<td class="grid-header">&nbsp;</td>
				  </tr>

				  <xsl:for-each select="Record">
				  <tr>
						<xsl:variable name="totals"><xsl:value-of select="@DepositTotal"/></xsl:variable>

						<xsl:if test="@BatchCount = 0">
							 <td class="grid-cell"><xsl:value-of select="@LockboxID"/>&nbsp;&middot;&nbsp;<xsl:value-of select="@LockboxName"/></td>
							 <td class="grid-cell">&nbsp;</td>
							 <xsl:choose>
								<xsl:when test="string(@ExceedsViewingDays) = 'True' or string(@Released) != 'True'">
                                    <td class="grid-cell"></td>
									<td class="grid-cell cell-right-align" valign="top">N/A</td>
									<td class="grid-cell cell-right-align">N/A</td>
									<td class="grid-cell cell-right-align">N/A</td>
								</xsl:when>
								<xsl:otherwise>
                                    <td class="grid-cell"></td>
									<td class="grid-cell cell-right-align"><xsl:value-of select="@BatchCount"/></td>
									<td class="grid-cell cell-right-align"><xsl:value-of select="@TransactionCount"/></td>
									<td class="grid-cell cell-right-align">$<xsl:value-of select="format-number(@DepositTotal,'#,##0.00')"/></td>
								</xsl:otherwise>
							 </xsl:choose>
							 
							 <td class="grid-cell">&nbsp;</td>
						</xsl:if>
					  
						<xsl:if test="@BatchCount > 0">
							 <xsl:variable name="linkurl">lckbx=<xsl:value-of select="@OLLockboxID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>

							 <td class="grid-cell">
								  <a title="View Batch Summary">
										<xsl:attribute name="href">batchsummary.aspx?<xsl:value-of select="$linkurl"/></xsl:attribute>
										<xsl:value-of select="@LockboxID"/>&nbsp;&middot;&nbsp;<xsl:value-of select="@LockboxName"/>
								  </a>
							 </td>
                            <xsl:variable name="reportlinkurl">haveparameters=yes&amp;reportname=HOAPMASummary&amp;customer=<xsl:value-of select="@CustomerID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="@HOA='True'">
                                    <td class="reportdata" align="right" valign="top">
                                        <a title="View Property Management Account Summary">
                                            <xsl:attribute name="href">JavaScript:newPopup('HOAReports.aspx?<xsl:value-of select="$reportlinkurl"/>');</xsl:attribute>
                                            PMA Summary Report
                                        </a>
                                    </td>
                                 </xsl:when>
                                 <xsl:otherwise>
                                    <td class="grid-cell"></td>
                                 </xsl:otherwise>
                             </xsl:choose>
                             <xsl:variable name="reportdetailurl">haveparameters=no&amp;reportname=HOAPMADetail&amp;customer=<xsl:value-of select="@CustomerID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>
                             <xsl:choose>
                                <xsl:when test="@HOA='True'">
                                    <td class="reportdata" align="right" valign="top">
                                        <a title="View Property Management Account Summary">
                                            <xsl:attribute name="href">JavaScript:newPopup('HOAReports.aspx?<xsl:value-of select="$reportdetailurl"/>');</xsl:attribute>
                                            PMA Detail Report
                                        </a>
                                    </td>
                                 </xsl:when>
                                 <xsl:otherwise>
                                    <td class="grid-cell"></td>
                                 </xsl:otherwise>
                             </xsl:choose>
							 <td class="grid-cell cell-right-align"><xsl:value-of select="@BatchCount"/></td>
							 <td class="grid-cell cell-right-align"><xsl:value-of select="@TransactionCount"/></td>
							 <td class="grid-cell cell-right-align">$<xsl:value-of select="format-number(@DepositTotal,'#,##0.00')"/></td>
							 <td class="grid-cell cell-center-align">
								  <a title="View Batch Detail">
										<xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/></xsl:attribute>
									  <i class="icon-search"></i>
								  </a>
							 </td>
						</xsl:if>
				  </tr>
				  </xsl:for-each>
				</table>
			</td>
		</tr>
		
		<xsl:call-template name="PaginationLinks">
			<xsl:with-param name="controlname">document.forms['frmDeposits'].txtAction</xsl:with-param>
			<xsl:with-param name="startrecord">
				<xsl:value-of select="$startrecord" />
			</xsl:with-param>
			<xsl:with-param name="colSpan">7</xsl:with-param>
		</xsl:call-template>

		<tr>
		  <td>
			  <br />
        <table width="100%">
          <tr>
            <td align="left" valign="bottom">
              <a href="JavaScript:newPopup('dailydeposits.aspx?date=' + document.forms.frmDeposits.txtDepositDate.value + '&amp;printview=1');">
				  <i class="icon-print"></i>
				  &nbsp;Printer-Friendly Version
			  </a>
            </td>
		    <td align="right" valign="bottom" class="pagelinks">
				&nbsp;
		    </td>
          </tr>
        </table>
      </td>
    </tr>
 	</table>
	<script language="JavaScript">

		$(document).ready(function() {


			//$.extend($.datepicker, { _checkOffset: function (inst, offset, isFixed) { return offset } });

			var dp = $('#StartDatePicker');

			dp.wfsDatePicker({
				constrainInput: true,
				showOn: "button"
			});
			
			//$('#StartDateButton').click(function () {

			//$('#StartDatePicker').datepicker("show");
		});

	</script>
	
</xsl:template>


</xsl:stylesheet>
