<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "<xsl:text disable-output-escaping='yes'>&amp;copy;</xsl:text>">
	<!ENTITY middot "<xsl:text disable-output-escaping='yes'>&amp;middot;</xsl:text>">
	<!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incMessages.xsl"/>
<xsl:include href="incCustomBranding.xsl"/>

<xsl:output method="html"/>

<xsl:template match="/">

<html>
<head>
	<title>Request Complete</title>
	<link rel="stylesheet" text="text/css" href="{$brandtemplatepath}/stylesheet.css" />
</head>
<body>

  <xsl:apply-templates select="/Page/Messages"/>

	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle" class="brandheaderstyle">
			<td align="left" colspan="2"><img src="{$brandtemplatepath}/images/Mast-Header.jpg" border="0"><xsl:attribute name="alt"><xsl:call-template name="appname" /></xsl:attribute></img></td>
		</tr>
		<tr>
			<td align="left" bgcolor="#000000" colspan="2"><img src="{$brandtemplatepath}/images/shim.gif" border="0" width="1" height="1" /></td>
		</tr>
		<tr>
			<td align="left" class="contenttitle" colspan="2"><img src="{$brandtemplatepath}/images/shim.gif" border="0" width="1" height="10" /></td>
		</tr>
		<tr>
			<td align="left" class="contenttitle"><img src="{$brandtemplatepath}/images/shim.gif" border="0" width="1" height="10" /></td>
			<td align="left" valign="top" class="contenttitle">Request Complete</td>
		</tr>
		<tr>
			<td align="left" class="contenttitle" colspan="2"><img src="{$brandtemplatepath}/images/shim.gif" border="0" width="1" height="10" /></td>
		</tr>
		<tr>
			<td align="left" bgcolor="#000000" colspan="2"><img src="{$brandtemplatepath}/images/shim.gif" border="0" width="1" height="1" /></td>
		</tr>
	</table>

	<br />

	<table border="0" cellpadding="5" cellspacing="0">

 FileSize: <xsl:value-of select="/Page/FormFields/FileSize" /><br />
 FileSize Div:<xsl:value-of select="/Page/FormFields/FileSize div (1024 * 1024)" /><br />

	  <xsl:choose>
	 <xsl:when test="/Page/FormFields/FileSize div (1024 * 1024) &gt; 50">
			  <tr>
				  <td align="left" valign="top" class="reportdata">
					  Your request is complete. For best results, please retrieve the file as follows:
				  </td>
			  </tr>
			  <tr>
				  <td align="left" valign="top" class="reportdata">
					  <ol class="reportdata">
						  <li>Using your mouse, please 'right click' the link below.</li>
						  <li>Click 'save' from the menu that displays (Note: The name of the 'save' option varies depending on your browser version).</li>
					  </ol>
				  </td>
			  </tr>
			  <tr>
				  <td align="left" valign="top" class="reportdata">
					  <ul>
						  <li>
						 <a>
						   <xsl:attribute name="href"><xsl:value-of select="/Page/FormFields/FileUrl" /></xsl:attribute>
						   <xsl:attribute name="onClick">alert('Please \'right click\' on the link as noted.');return false;</xsl:attribute>
						   <xsl:value-of select="/Page/FormFields/UserFileName" />
						 </a>
						  </li>
					  </ul>
				  </td>
			  </tr>
	 </xsl:when>
	 <xsl:otherwise>
			  <tr>
				  <td align="left" valign="top" class="reportdata">
					  Your request is complete.  Please click the link below to retrieve the file.
				  </td>
			  </tr>
			  <tr>
				  <td align="left" valign="top" class="reportdata">
					  <ul>
						  <li>
						 <a>
						   <xsl:attribute name="href"><xsl:value-of select="/Page/FormFields/FileUrl" /></xsl:attribute>
						   <xsl:value-of select="/Page/FormFields/UserFileName" />
						 </a>
						  </li>
					  </ul>
				  </td>
			  </tr>
	 </xsl:otherwise>
	  </xsl:choose>
	</table>

	<br />

	<table border="0" cellpadding="2" width="100%">
		<tr>
			<td align="left" class="copyright"><xsl:call-template name="copyright" /></td>
		</tr>
	</table>

</body>
</html>

</xsl:template>


<xsl:template name="PageTitle">
	<span class="contenttitle"><xsl:call-template name="appname" /></span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">
	<h3>You should not be seeing this template.</h3>
	TODO: Modify this template to display generic error message.
	<p/>
</xsl:template>

</xsl:stylesheet>
