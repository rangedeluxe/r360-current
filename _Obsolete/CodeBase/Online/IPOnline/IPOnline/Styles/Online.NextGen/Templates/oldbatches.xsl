<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
 Decisioning Batches
</xsl:template>

<xsl:template name="PageTitle">
 <span class="contenttitle">Decisioning Batches</span><br/>
 <span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">

 <xsl:variable name="startrecord"><xsl:value-of select="PageInfo/@StartRecord"/></xsl:variable>
 <xsl:variable name="OLLockboxID">
  <xsl:choose>
   <xsl:when test="count(/Page/Recordset[@Name='UserLockboxes']/Record) = 1">
    <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@OLLockboxID" />
   </xsl:when>
   <xsl:otherwise>
    <xsl:value-of select="/Page/FormFields/OLLockboxID" />
   </xsl:otherwise>
  </xsl:choose>
 </xsl:variable>

 <form method="post" name="frmOLD">
  <input type="hidden" name="txtAction" />
  <input type="hidden" name="txtBankID" />
  <input type="hidden" name="txtCustomerID" />
  <input type="hidden" name="txtLockboxID" />
  <input type="hidden" name="txtGlobalBatchID" />
  <input type="hidden" name="txtStart">
   <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
  </input>
  <table cellspacing="0" cellpadding="0" width="98%" align="center" border="0">
   <tr>
    <td>
     <table cellSpacing="0" cellPadding="5" border="0">
      <tr>
       <td class="formlabel">Workgroup:</td>
       <td class="formfield">
       <xsl:choose>
        <xsl:when test="count(/Page/Recordset[@Name='UserLockboxes']/Record)=1">
         <input type="hidden" name="cboOLLockbox">
          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@OLLockboxID" /></xsl:attribute>
          <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record/@LongName" />
         </input>
        </xsl:when>
        <xsl:otherwise>
         <select name="cboOLLockbox" class="formfield" onchange="this.form.submit();">
          <option value="[All]">&lt;-- All workgroups --&gt;</option>
          <xsl:for-each select="/Page/Recordset[@Name='UserLockboxes']/Record">
           <option>
            <xsl:attribute name="value"><xsl:value-of select="@OLLockboxID" /></xsl:attribute>
            <xsl:if test="@OLLockboxID = $OLLockboxID">
             <xsl:attribute name="selected">selected</xsl:attribute>
            </xsl:if>
            <xsl:value-of select="@LockboxID" /> - <xsl:value-of select="@LongName" />
           </option>
          </xsl:for-each>
         </select>
        </xsl:otherwise>
       </xsl:choose>
       </td>
      </tr>
      <tr>
       <td class="formlabel">Status:</td>
       <td class="formfield">
        <select name="selBatchMode" class="formfield" onchange="this.form.submit();">
         <option value="2">
          <xsl:if test="/Page/FormFields/BatchMode != '0' and /Page/FormFields/BatchMode != '1'">
           <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
          [All]
         </option>
         <option value="0">
          <xsl:if test="/Page/FormFields/BatchMode = '0'">
           <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
          Pending
         </option>
         <option value="1">
          <xsl:if test="/Page/FormFields/BatchMode = '1'">
           <xsl:attribute name="selected">selected</xsl:attribute>
          </xsl:if>
          In-Decision
         </option>
        </select>
       </td>
      </tr>
     </table>
    </td>
   </tr>
  </table>

  <p></p>

  <table onmouseover="popMenu();" cellSpacing="0" cellPadding="0" width="98%" align="center" border="0">
   <tbody>
    <tr>
     <td class="contentbreak"><img height="1" src="{$brandtemplatepath}/Images/shim.gif" width="1" border="0" /></td>
    </tr>
   </tbody>
  </table>

  <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
   <tr>
    <td>
     <table width="100%" cellpadding="5" cellspacing="0" border="0">
      <tr>
       <td class="reportcaption">
        <xsl:choose>
         <xsl:when test="string-length($OLLockboxID)=0">
           All Workgroups
         </xsl:when>
         <xsl:otherwise>
          <xsl:variable name="LongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@OLLockboxID=$OLLockboxID]/@LongName" /></xsl:variable>
           Workgroup: <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@OLLockboxID=$OLLockboxID]/@LockboxID" /><xsl:if test="string-length($LongName) > 0"> - <xsl:value-of select="$LongName" /></xsl:if><br />
         </xsl:otherwise>
        </xsl:choose>
       </td>
      </tr>
     </table>
    </td>
   </tr>
   <tr>
    <td>
     <xsl:call-template name="ShowingResultsLabel">
      <xsl:with-param name="startrecord"><xsl:value-of select="$startrecord" /></xsl:with-param>
     </xsl:call-template>
    </td>
   </tr>
   <tr>
    <td align="left" valign="top">
     <table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
      <tr>
       <td class="reportheading" align="left">Workgroup</td>
       <td class="reportheading" align="right">Deposit Date</td>
       <td class="reportheading" align="right">Batch ID</td>
       <td class="reportheading" align="left">Deposit Status</td>
       <td class="reportheading" align="right">Decisioning Deadline</td>
       <td class="reportheading" align="left">User</td>
       <td class="reportheading" align="right">Transaction Count</td>
       <td class="reportheading" align="right"></td>
      </tr>

      <xsl:if test="count(Batches/Batch) = 0">
       <tr class="evenrow">
        <td class="reportdata" colspan="8" align="center">
         <xsl:call-template name="NoResultsLabel" />
        </td>
       </tr>
      </xsl:if>

      <xsl:for-each select="Batches/Batch">
      <tr>

       <xsl:variable name="BankID"><xsl:value-of select="./BankID" /></xsl:variable>
       <xsl:variable name="CustomerID"><xsl:value-of select="./CustomerID" /></xsl:variable>
       <xsl:variable name="LockboxID"><xsl:value-of select="./LockboxID" /></xsl:variable>

       <xsl:if test='position() mod 2 = 0'>
        <xsl:attribute name="class">evenrow</xsl:attribute>
       </xsl:if>
       <xsl:if test='position() mod 2 != 0'>
        <xsl:attribute name="class">oddrow</xsl:attribute>
       </xsl:if>

       <td class="reportdata" align="left">
															<xsl:variable name="LongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@LongName" /></xsl:variable>
															<xsl:value-of select="LockboxID" /><xsl:if test="string-length($LongName) > 0">&nbsp; - &nbsp;<xsl:value-of select="$LongName" /></xsl:if>
														</td>
       <td class="reportdata" align="right"><xsl:value-of select="DepositDate" /></td>
       <td class="reportdata" align="right">
        <a><xsl:attribute name="href">oldbatchdetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="GlobalBatchID" /></xsl:attribute>
         <xsl:value-of select="BatchID" /></a>
       </td>
       <td class="reportdata" align="left">
        <xsl:choose>
         <xsl:when test="DepositStatus = 240">Pending Decision</xsl:when>
         <xsl:when test="DepositStatus = 241">In-Decision</xsl:when>
        </xsl:choose>
       </td>
       <td class="reportdata" align="right"><xsl:value-of select="AdjustedDecisioningDeadline" /></td>
       <td class="reportdata" align="left"><xsl:value-of select="FirstName" />&nbsp;<xsl:value-of select="LastName" /></td>
       <td class="reportdata" vAlign="top" align="right"><xsl:value-of select="ItemsInBatch" /></td>
       <td class="reportdata" align="right">
        <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='modify']">
         <xsl:choose>
          <xsl:when test="DepositStatus = 240">
           <a>
            <xsl:attribute name="href">javascript:checkoutbatch('<xsl:value-of select="$BankID" />', '<xsl:value-of select="$CustomerID" />', '<xsl:value-of select="$LockboxID" />', '<xsl:value-of select="GlobalBatchID" />');</xsl:attribute>
            <small>Check-Out</small>
           </a>&nbsp;&nbsp;
          </xsl:when>
          <xsl:when test="DepositStatus = 241">
           <a>
            <xsl:attribute name="href">javascript:resetbatch('<xsl:value-of select="$BankID" />', '<xsl:value-of select="$CustomerID" />', '<xsl:value-of select="$LockboxID" />', '<xsl:value-of select="GlobalBatchID" />');</xsl:attribute>
            <small>Reset</small>
           </a>&nbsp;&nbsp;
          </xsl:when>
         </xsl:choose>
        </xsl:if>
        <a>
         <xsl:attribute name="href">oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="GlobalBatchID" /></xsl:attribute>
         <img border="0" src="{$brandtemplatepath}/Images/cbo_icon_batchdetail.jpg" width="16" height="16" alt="View Decisioning Batch Detail"/>
        </a>
       </td>
      </tr>
      </xsl:for-each>
     </table>
    </td>
   </tr>
  </table>

  <p></p>

  <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
    <tr>
     <td align="right" valign="bottom" class="pagelinks">
      <br />
      <xsl:call-template name="PaginationLinks">
       <xsl:with-param name="controlname">document.forms['frmOLD'].txtAction</xsl:with-param>
       <xsl:with-param name="startrecord"><xsl:value-of select="$startrecord" /></xsl:with-param>
      </xsl:call-template>
     </td>
    </tr>
  </table>

  <p></p>

 </form>
 
 <script language="javascript">
  function checkoutbatch(bankid, customerid, lockboxid, globalbatchid) {
   //alert('check out: ' + globalbatchid);
   document.forms['frmOLD'].txtAction.value = 'checkout';
   document.forms['frmOLD'].txtBankID.value = bankid;
   document.forms['frmOLD'].txtCustomerID.value = customerid;
   document.forms['frmOLD'].txtLockboxID.value = lockboxid;
   document.forms['frmOLD'].txtGlobalBatchID.value = globalbatchid;
   document.forms['frmOLD'].submit();
  }

  function resetbatch(bankid, customerid, lockboxid, globalbatchid) {
   //alert('reset: ' + globalbatchid);
   document.forms['frmOLD'].txtAction.value = 'reset';
   document.forms['frmOLD'].txtBankID.value = bankid;
   document.forms['frmOLD'].txtCustomerID.value = customerid;
   document.forms['frmOLD'].txtLockboxID.value = lockboxid;
   document.forms['frmOLD'].txtGlobalBatchID.value = globalbatchid;
   document.forms['frmOLD'].submit();
  }
 </script>

</xsl:template>

</xsl:stylesheet>
