<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
 <!ENTITY nbsp "&#160;">
 <!ENTITY copy "&#169;">
 <!ENTITY middot "&#183;">
 <!ENTITY laquo "&#171;">
 <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="incStyle.xsl"/>
	<xsl:include href="incCommon.xsl"/>
	<xsl:include href="incMessages.xsl"/>

	<xsl:output method="html"/>

 <xsl:template name="Title">
  Property Management Account Parameters
 </xsl:template>

 <xsl:template name="PageTitle">
  <span class="contenttitle">Property Management Account Parameters</span>
  <br/>
  <span class="contentsubtitle">&nbsp;</span>
 </xsl:template>

 <xsl:template match="Page">
  <xsl:variable name="vReportName"><xsl:value-of select="Parameters/@ReportName" /></xsl:variable>
  <html>
   <head>
    <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
   </head>
   <body>
    <form method="post">
	    <xsl:attribute name="name">frmMain</xsl:attribute>
     <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
        <tr>
       <td align="right" class="formlabel">&nbsp;&nbsp;</td>
       <xsl:choose>
        <xsl:when test = "$vReportName = 'HOAPMADetail'">
         <td class="formlabel">HOA PMA Detail Report </td>
        </xsl:when>
        <xsl:when test = "$vReportName = 'HOAAllPMASummary'">
         <td class="formlabel">HOA PMA All Summary Report </td>
        </xsl:when>
        <xsl:otherwise></xsl:otherwise>
       </xsl:choose>
        </tr>
        <tr><td>&nbsp;</td></tr> 
        <tr>
		     <td align="right" class="formlabel">Deposit Date:&nbsp;&nbsp;</td>
       <td class="formfield"><xsl:value-of select="Parameters/@DepositDate"/></td>
      </tr>
      <tr><td>&nbsp;</td></tr>   
      <tr> 
			    <td align="right" class="formlabel">Customer:&nbsp;&nbsp;</td>
       <xsl:choose>
        <xsl:when test="Parameters/@ReportName = 'HOAPMADetail'">
          <td class="formfield"><xsl:value-of select="Parameters/@CustomerID"/></td>
        </xsl:when>
        <xsl:otherwise>
         <td class="formfield">
          <xsl:choose>
										 <xsl:when test="count(/Page/Recordset[@Name='HOAPMACustomers']/Record)=1">
											 <input type="hidden" name="txtCustomer">
												 <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='HOAPMACustomers']/Record/@Customer_ID" /></xsl:attribute>
												 <xsl:value-of select="/Page/Recordset[@Name='HOAPMACustomers']/Record/@Customer_ID" /> - <xsl:value-of select="/Page/Recordset[@Name='HOAPMACustomers']/Record/@Customer_Name" />
											 </input>
										 </xsl:when>
										 <xsl:otherwise>
			         <select name="txtCustomer" class="formfield" style="width:200px" onChange="">
								     <xsl:for-each select="/Page/Recordset[@Name='HOAPMACustomers']/Record">
									     <option>
										     <xsl:attribute name="value"><xsl:value-of select="@Customer_ID" /></xsl:attribute>
										     <xsl:value-of select="@Customer_ID" /> - <xsl:value-of select="@Customer_Name" />
									     </option>
								     </xsl:for-each>
							     </select>
           </xsl:otherwise>
          </xsl:choose>
         </td>
        </xsl:otherwise>
       </xsl:choose>
		    </tr>
      <tr><td>&nbsp;</td></tr>
      <xsl:if test = "Parameters/@ReportName = 'HOAPMADetail'">
       <tr>
			     <td align="right" class="formlabel">Workgroup:&nbsp;&nbsp;</td>
        <td class="formfield"><xsl:value-of select="Parameters/@LockboxID"/></td>
	      </tr>
       <tr><td>&nbsp;</td></tr>
       <tr>
        <td align="right" class="formlabel">HOA Name:&nbsp;&nbsp;</td>
        <td class="formfield">
			      <select name="txtHOAName" class="formfield" style="width:200px" onChange="">
								  <xsl:for-each select="/Page/Recordset[@Name='HOAPMAHOANames']/Record">
									  <option>
										  <xsl:attribute name="value"><xsl:value-of select="@HOA_Number" /></xsl:attribute>
										  <xsl:value-of select="@HOA_Number" /> - <xsl:value-of select="@HOA_Name" />
									  </option>
								  </xsl:for-each>
							  </select>
        </td>
       </tr>
      </xsl:if>
      <tr><td>&nbsp;</td></tr>
      <tr>
       <td>&nbsp;</td>
       <td align="left">
        <input type="button" name="cmdSubmit" value="Run Report" class="formfield" onClick="javascript:RunReport('{$vReportName}') "/>
       </td>
      </tr>
     </table>
    </form>
   </body>
  </html>
  <script language="javascript">
			<xsl:comment>
    //Hide script from older browsers
    function RunReport(reportname){
     var linkurl = document.URL;
     var finallinkurl = linkurl.replace("no","yes");
     var concatlink;

     if (reportname == 'HOAPMADetail'){
      concatlink = '&amp;HOA_Number='+document.forms['frmMain'].txtHOAName.value;
     }
     if (reportname == 'HOAAllPMASummary'){
      concatlink = '&amp;customer='+document.forms['frmMain'].txtCustomer.value;
     }
     finallinkurl = finallinkurl + concatlink;
     newPopup(finallinkurl);
      }
   </xsl:comment>
  </script>
 </xsl:template>
</xsl:stylesheet>