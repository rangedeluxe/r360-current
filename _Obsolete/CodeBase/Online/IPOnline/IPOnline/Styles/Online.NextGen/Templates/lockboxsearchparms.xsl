<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>
  <xsl:include href="incCommon.xsl"/>
  <xsl:include href="incMessages.xsl"/>

  <xsl:output method="html"/>

  <xsl:variable name="FldTypeString">1</xsl:variable>
  <xsl:variable name="FldTypeFloat">6</xsl:variable>
  <xsl:variable name="FldTypeCurrency">7</xsl:variable>
  <xsl:variable name="FldTypeDate">11</xsl:variable>
  <xsl:variable name="WorkgroupSelection"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelection']" /></xsl:variable>
  <xsl:variable name="WorkgroupSelectionLabel"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelectionLabel']" /></xsl:variable>
  <xsl:variable name="OLWorkGroupsID"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" /></xsl:variable>

  <xsl:template name="Title">Advanced Search</xsl:template>


  <xsl:template name="PageTitle">
    <span class="contenttitle">Advanced Search</span>
  </xsl:template>


  <xsl:template match="Page">
    <div class="page-portlet-view">
      <form method="post" onSubmit="return false;" name="frmLockBoxSearch" action="./lockboxsearch.aspx">
        <xsl:choose>
          <xsl:when test="Recordset[@Name='LockboxSearch']">
            <xsl:apply-templates select="Recordset[@Name = 'LockboxSearch']" mode="LockboxSearch"/>
          </xsl:when>
          <xsl:when test="Recordset[@Name='SearchParms']">
            <xsl:call-template name="LockboxSearchParms" />
          </xsl:when>
        </xsl:choose>
      </form>
      <script language="javascript">
        
        var currentworkgroup = null;
        
        //--------------------------------------------------
        // If only one lockbox exists, no need for the check
        //-------------------------------------------------
        if(document.forms[0].txtOLLockbox &amp;&amp; document.forms[0].txtOLLockbox.selectedIndex){
        if(currentworkgroup &amp;&amp; currentworkgroup.OLWorkgroupsID != '<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" />') {
        populateAdvancedFind();
        populateDisplayFields();
        }
        }

        //-------------------------------------------------
        //
        //-------------------------------------------------
        function txtOLLockbox_OnChange() {
        populateAdvancedFind();
        populateDisplayFields();
        }


        //-------------------------------------------------
        //
        //-------------------------------------------------

        function selSaveQuery_OnChange(){
        formSubmitNegPattern(document.forms['frmLockBoxSearch'].selSaveQuery, 'txtAction');
        }

        //-------------------------------------------------
        //
        //-------------------------------------------------

        function selField_OnChange(selField, selOperator, txtField) {

        if(selField.value.length > 0) {
        var arValue = selField.value.split('~');
        switch(arValue[2]) {
        case '<xsl:value-of select="$FldTypeString" />':
        if(selOperator.options.length != 5) {
        selOperator.options.length=0;
        selOperator.options[0] = new Option("", "");
        selOperator.options[1] = new Option("Begins With", "Begins With");
        selOperator.options[2] = new Option("Contains", "Contains");
        selOperator.options[3] = new Option("Ends With", "Ends With");
        selOperator.options[4] = new Option("Equals", "Equals");
        txtField.isRequired = true;
        txtField.isNumeric = false;
        txtField.isDate = false;
        }
        break;
        case '<xsl:value-of select="$FldTypeFloat" />':
        if(selOperator.options.length != 4) {
        selOperator.options.length=0;
        selOperator.options[0] = new Option("", "");
        selOperator.options[1] = new Option("Equals", "Equals");
        selOperator.options[2] = new Option("Is Greater Than", "Is Greater Than");
        selOperator.options[3] = new Option("Is Less Than", "Is Less Than");
        txtField.isRequired = true;
        txtField.isNumeric = true;
        txtField.isDate = false;
        }
        break;
        case '<xsl:value-of select="$FldTypeCurrency" />':
        if(selOperator.options.length != 4) {
        selOperator.options.length=0;
        selOperator.options[0] = new Option("", "");
        selOperator.options[1] = new Option("Equals", "Equals");
        selOperator.options[2] = new Option("Is Greater Than", "Is Greater Than");
        selOperator.options[3] = new Option("Is Less Than", "Is Less Than");
        txtField.isRequired = true;
        txtField.isNumeric = false;
        txtField.isDate = false;
        }
        break;
        case '<xsl:value-of select="$FldTypeDate" />':
        if(selOperator.options.length != 4) {
        selOperator.options.length=0;
        selOperator.options[0] = new Option("", "");
        selOperator.options[1] = new Option("Equals", "Equals");
        selOperator.options[2] = new Option("Is Greater Than", "Is Greater Than");
        selOperator.options[3] = new Option("Is Less Than", "Is Less Than");
        txtField.isRequired = true;
        txtField.isNumeric = false;
        txtField.pattern = "([^\\d\\.]+)|(\\d*\\.){2}|((\\s+|^)\\.(\\s+|$))";
        txtField.isDate = true;
        }
        break;
        default:
        selOperator.options[0] = new Option("", "");
        txtField.isRequired = false;
        txtField.isNumeric = false;
        txtField.isDate = false;
        break;
        }

        }
        else {
          selOperator.options.length=0;
          txtField.isRequired = false;
          txtField.isNumeric = false;
          txtField.isDate = false;
        }

        txtField.value = '';
        }

        //-------------------------------------------------
        //
        //-------------------------------------------------
        function populateAdvancedFind() {

        <xsl:for-each select="/Page/Recordset[@Name='AdvancedSearchParms']/Record">
          document.forms[0].selField<xsl:value-of select="position()" />.options.length=0;
          document.forms[0].selField<xsl:value-of select="position()" />.options[0] = new Option("", "");
          document.forms[0].selOperator<xsl:value-of select="position()" />.options.length=0;
          document.forms[0].searchFieldValue<xsl:value-of select="position()" />.value='';
          document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isRequired = false;
          document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isNumeric = false;
          document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isDate = false;
        </xsl:for-each>

        if(currentworkgroup.DataEntryFields.length > 0) {
        for(var j=0;j&lt;currentworkgroup.DataEntryFields.length;++j) {

        <xsl:for-each select="/Page/Recordset[@Name='AdvancedSearchParms']/Record">
          document.forms[0].selField<xsl:value-of select="position()" />.options[j+1] = new Option(currentworkgroup.DataEntryFields[j][3]
          , currentworkgroup.DataEntryFields[j][0] + '~'
          + currentworkgroup.DataEntryFields[j][1] + '~'
          + currentworkgroup.DataEntryFields[j][2] + '~'
          + currentworkgroup.DataEntryFields[j][3] + '~'
          + currentworkgroup.DataEntryFields[j][5]
          );
        </xsl:for-each>
        }
        }
        }


        //-------------------------------------------------
        //
        //-------------------------------------------------
        function populateDisplayFields() 
        {
          document.forms[0].selAvailable.options.length=0;
          document.forms[0].selSelected.options.length=0;

          if(currentworkgroup.DataEntryFields.length > 0) 
          {
            for(var j=0;j&lt;currentworkgroup.DataEntryFields.length;++j) 
            {
              // Index of '4' is the Order.  Index of '5' is the BatchSourceKey.
              // Who needs objects when you can just arrayarray?
              document.forms[0].selAvailable.options[j] = new Option(currentworkgroup.DataEntryFields[j][3]
              , currentworkgroup.DataEntryFields[j][0] + '~'
              + currentworkgroup.DataEntryFields[j][1] + '~'
              + currentworkgroup.DataEntryFields[j][2] + '~'
              + currentworkgroup.DataEntryFields[j][3] + '~'
              + currentworkgroup.DataEntryFields[j][5] + '~'
              + '{TERMINATOR}');
            }
          }
          
          var vbatchidrow = document.getElementById("BatchIDRow");
                                        
          if(currentworkgroup.DisplayBatchID===true){
            vbatchidrow.style.display="";
          }
          else {
            vbatchidrow.style.display="none";
          }
      }

      </script>
    </div>
  </xsl:template>
  <xsl:template name="LockboxSearchParms">
    <input type="hidden" name="txtAction" value="" />
    <input type="hidden" name="txtParam" value="" />
    <input type="hidden"  name="txtStart" value="" />
    <input type="hidden"  name="txtNextStart" value="" />
    <br />
    <div id="divCalendar" style="position: absolute; background-color: white; visibility: hidden;"></div>
    <table width="1200" align="center" border="0" cellpadding="0" cellspacing="0" class="formmain">
      <tr>
        <td align="left" valign="top">
          <table width="100%" cellpadding="0" cellspacing="1" border="0">
            <tr>
              <td align="left" valign="top">
                <table width="100%" cellpadding="5" cellspacing="0" border="0" class="formsub">
                  <tr>
                    <td align="center" valign="middle"  colspan="4">
                      <table width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                        <tr>
                          <td align="center" valign="top" colspan="2">
                            <input type="button" name="cmdSearch" value="Search" class="btn btn-primary" onClick="SelectAll(this.form.elements['selAvailable'], this.form.elements['selSelected']);formSubmitNegPattern(this, 'txtAction');" />
                            &nbsp;&nbsp;
                            <input type="button" name="cmdClear"  value="Clear Search" class="btn btn-primary" onClick="ClearQuery()" />
                            <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='maintainquery.aspx']">
                              &nbsp;&nbsp;
                              <input type="button" name="cmdManageQueries" value="Manage Queries" class="btn btn-primary" onClick="location.href='maintainquery.aspx';" />
                            </xsl:if>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='maintainquery.aspx']">
                    <tr>
                      <td colspan="4" align="left" valign="top">
                        <table width="100%" cellpadding="4" cellspacing="0" border="0" class="formsub">
                          <tr>
                            <td class="hubLabel" valign="top" align="right" width="25%">Search Query Name:</td>
                            <td align="left" valign="top" class="formfield">
                              <div class="btn-group entitySelect" id="SaveQuerySelect" style="margin-top: -5px;"></div>
                              <select name="selSaveQuery" class="formfield" onChange="selSaveQuery_OnChange();" style="display: none;">
                                <option value="Ad Hoc Query">Ad Hoc Query</option>
                                <xsl:for-each select="/Page/Recordset[@Name='GetSavedQueries']/Record">
                                  <option>
                                    <xsl:attribute name="value"><xsl:value-of select="@PredefSearchID" /></xsl:attribute>
                                    <xsl:if test="@PredefSearchID=/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PredefSearchID']">
                                      <xsl:attribute name="selected">1</xsl:attribute>
                                    </xsl:if>
                                    <xsl:apply-templates select="@Name" mode="escape"/>
                                  </option>
                                </xsl:for-each>
                              </select>
                            </td>
                            <xsl:if test = "/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PredefSearchID']!='00000000-0000-0000-0000-000000000000'">
                              <td class="hubLabel" valign="top" align="left">
                                <a title="EditName" href="javascript:edit_name();SubmitSavedQuery()">Edit Name</a>
                              </td>
                            </xsl:if>
                            <td class="hubLabel" valign="top" align="left">
                              <a title="NewName" href="javascript:new_name();SubmitSavedQuery()">New Query Name</a>
                            </td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </xsl:if>
                  <tr>
                    <td align="right" class="hubLabel" width="25%">Workgroup:</td>
                    <td align="left" class="formfield" width="75%">
                      <div style="padding-top: 6px;">
                        <div id="treeBase" style="width: 375px;">
                          <input id="txtWorkgroupSelection" type="hidden" name="txtWorkgroupSelection" value="">
                            <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelection']" /></xsl:attribute>
                          </input>
                          <input id="txtWorkgroupSelectionLabel" type="hidden" name="txtWorkgroupSelectionLabel" value="">
                            <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelectionLabel']" /></xsl:attribute>
                          </input>
                        </div>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top" class="hubLabel" nowrap="nowrap">
                      Deposit Date(s):<br /><small>(mm/dd/yyyy)</small>
                    </td>
                    <td align="left" valign="top" class="formfield" nowrap="nowrap">
                      <div>
                        <div id="StartDatePicker"></div>
                        <input type="text" name="txtDateFrom" id="txtDateFrom" style="display:none">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DateFrom']" /></xsl:attribute>
                        </input>
                        <div style="margin: 10px 2px 5px 5px; display: inline-block; vertical-align: top;">
                          <i class="fa fa-minus"></i>
                        </div>
                        <div id="EndDatePicker"></div>
                        <input type="text" name="txtDateTo" id="txtDateTo" style="display:none">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DateTo']" /></xsl:attribute>
                        </input>
                      </div>
                    </td>
                  </tr>
                  <tr id="BatchIDRow">
                    <td align="right" valign="top" class="hubLabel" nowrap="nowrap">
                      <xsl:call-template name="batchidlabel" />:<br /><small>Start - End&nbsp;</small>
                    </td>
                    <td align="left" valign="top" width="90%" class="formfield" nowrap="nowrap">
                      <div style="display: inline-block;">
                        <input type="text" name="txtBatchIDFrom" maxlength="10" size="10" class="form-control">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchIDFrom']" /></xsl:attribute>
                        </input>
                      </div>
                      <div style="margin: 10px 5px; display: inline-block; vertical-align: top;">
                        <i class="fa fa-minus"></i>
                      </div>
                      <div style="display: inline-block;">
                        <input type="text" name="txtBatchIDTo" maxlength="10" size="10" class="form-control">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchIDTo']" /></xsl:attribute>
                        </input>
                      </div>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top" class="hubLabel" nowrap="nowrap">
                      <xsl:call-template name="batchnumberlabel" />:<br /><small>Start - End&nbsp;</small>
                    </td>
                    <td align="left" valign="top" width="90%" class="formfield" nowrap="nowrap">
                      <div style="width: 150px;display: inline-block;">
                        <input type="text" name="txtBatchNumberFrom" maxlength="10" size="10" class="form-control">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchNumberFrom']" /></xsl:attribute>
                        </input>
                      </div>
                      <div style="margin: 10px 5px; display: inline-block; vertical-align: top;">
                        <i class="fa fa-minus"></i>
                      </div>
                      <div style="width: 150px;display: inline-block;">
                        <input type="text" name="txtBatchNumberTo" maxlength="10" size="10" class="form-control">
                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchNumberTo']" /></xsl:attribute>
                        </input>
                      </div>
                    </td>
                  </tr>

                  <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='COTSOnly']!='True'">
                    <tr>
                      <td align="right" valign="top" class="hubLabel">Check/Trace/Ref Number:</td>
                      <td align="left" valign="top" class="formfield">
                        <div style="width: 300px;display: inline-block;">
                          <input type="text" name="txtSerial" size="10" class="form-control">
                            <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='Serial']" /></xsl:attribute>
                          </input>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td align="right" valign="top" class="hubLabel">
                        Payment Amount:<br /><small>Start - End&nbsp;</small>
                      </td>
                      <td align="left" valign="top" class="formfield">
                        <div style="width: 150px;display: inline-block;">
                          <input type="text" name="txtAmountFrom" size="10" class="form-control">
                            <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='AmountFrom']" /></xsl:attribute>
                          </input>
                        </div>
                        <div style="margin: 10px 5px; display: inline-block; vertical-align: top;">
                          <i class="fa fa-minus"></i>
                        </div>
                        <div style="width: 150px;display: inline-block;">
                          <input type="text" name="txtAmountTo" size="10" class="form-control">
                            <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='AmountTo']" /></xsl:attribute>
                          </input>
                        </div>
                      </td>
                    </tr>
                  </xsl:if>
                  <tr>
                    <td align="right" class="hubLabel" width="25%">Payment Source:</td>
                    <td align="left" class="formfield" width="75%">

                      <select name="txtPaymentSource" id ="txtPaymentSource" class="formfield"  style="display:none;" >
                        <option value="-1">-- All --</option>
                        <xsl:for-each select="/Page/Recordset/PaymentSource">
                          <option>
                            <xsl:attribute name="value"><xsl:value-of select="@PaymentSourceID" /></xsl:attribute>
                            <xsl:if test="@PaymentSourceID=/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchSourceKey']">
                              <xsl:attribute name="selected">1</xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="@PaymentSource" />
                          </option>
                        </xsl:for-each>
                      </select>
                      <div class="btn-group entitySelect" id="PaymentSourceSelect" ></div>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" class="hubLabel" width="25%">Payment Type:</td>
                    <td align="left" class="formfield" width="75%">
                      <select name="txtPaymentType" id="txtPaymentType" class="formfield" style="display:none;" >
                        <option value="-1">-- All --</option>
                        <xsl:for-each select="/Page/Recordset/PaymentType">
                          <option>
                            <xsl:attribute name="value"><xsl:value-of select="@PaymentTypeID" /></xsl:attribute>
                            <xsl:if test="@PaymentTypeID=/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchPaymentTypeKey']">
                              <xsl:attribute name="selected">1</xsl:attribute>
                            </xsl:if>
                            <xsl:value-of select="@PaymentType" />
                          </option>
                        </xsl:for-each>
                      </select>
                      <div class="btn-group entitySelect" id="PaymentTypeSelect" ></div>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" class="hubLabel">
                      Display Correspondence Only Transactions:&nbsp;
                      <input type="checkbox" name="chkCOTSOnly" onClick="chkCOTSOnly_onClick();">
                        <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='COTSOnly']='True'">
                          <xsl:attribute name="checked">1</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                  </tr>
                  <tr>
                    <td colspan="2" align="left" valign="top" class="hubLabel">
                      Display Mark Sense Transactions Only:&nbsp;
                      <input type="checkbox" name="chkMarkSenseOnly">
                        <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='MarkSenseOnly']='True'">
                          <xsl:attribute name="checked">1</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                  </tr>
                </table>

              </td>
            </tr>
            <tr>
              <td align="left" valign="top">

                <table width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                  <tr>
                    <td class="hubLabel" valign="top" cellpadding="2" align="right">
                      Advanced<br/>Find:
                    </td>
                    <td align="center">

                      <table width="95%" cellpadding="0" cellspacing="0" border="0" class="formmain">
                        <tr>
                          <td align="left" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0">
                              <tr>
                                <td align="left" valign="top">
                                  <table width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                                    <tr>
                                      <td align="center" class="hubLabel" valign="bottom">Search Field</td>
                                      <td align="center" class="hubLabel" valign="bottom">Comparison</td>
                                      <td align="center" class="hubLabel" valign="bottom">Value</td>

                                    </tr>
                                    <xsl:for-each select="/Page/Recordset[@Name='AdvancedSearchParms']/Record">
                                      <xsl:variable name="curParm"><xsl:value-of select="position()" /></xsl:variable>
                                      <xsl:variable name="curParmValue"><xsl:value-of select="@TableName" />~<xsl:value-of select="@FieldName" />~<xsl:value-of select="@DataTypeEnum" />~<xsl:value-of select="@ReportTitle" /></xsl:variable>
                                      <tr>
                                        <td class="formfield">
                                          <!--Build Field List-->
                                          <select class="form-control" >
                                            <xsl:attribute name="onchange">SelectAll(this.form.elements['selAvailable'], this.form.elements['selSelected']);selField_OnChange(this, selOperator<xsl:value-of select="position()" />, searchFieldValue<xsl:value-of select="position()" />);</xsl:attribute>
                                            <xsl:attribute name="name">selField<xsl:value-of select="position()" /></xsl:attribute>
                                            <option></option>
                                            <xsl:for-each select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/DEFields/DEField">
                                              <xsl:variable name="curFldValue"><xsl:value-of select="@TableName" />~<xsl:value-of select="@FldName" />~<xsl:value-of select="@FldDataTypeEnum" />~<xsl:value-of select="@ReportTitle" /></xsl:variable>
                                              <option>
                                                <xsl:attribute name="value">
                                                  <xsl:value-of select="$curFldValue" />
                                                </xsl:attribute>
                                                <xsl:if test="$curParmValue=$curFldValue">
                                                  <xsl:attribute name="selected">1</xsl:attribute>
                                                </xsl:if>
                                                <xsl:value-of select="@ReportTitle" />
                                              </option>
                                            </xsl:for-each>
                                          </select>

                                        </td>
                                        <td align="center" class="formfield">
                                          <select class="form-control" style="width:150px;">
                                            <xsl:attribute name="name">selOperator<xsl:value-of select="position()" /></xsl:attribute>
                                            <xsl:choose>
                                              <xsl:when test="@DataTypeEnum=$FldTypeString">
                                                <option value=""></option>
                                                <option value="Begins With">
                                                  <xsl:if test="@Operator='Begins With'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Begins With
                                                </option>
                                                <option value="Contains">
                                                  <xsl:if test="@Operator='Contains'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Contains
                                                </option>
                                                <option value="Ends With">
                                                  <xsl:if test="@Operator='Ends With'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Ends With
                                                </option>
                                                <option value="Equals">
                                                  <xsl:if test="@Operator='Equals'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Equals
                                                </option>
                                              </xsl:when>
                                              <xsl:when test="@DataTypeEnum=$FldTypeFloat or @DataTypeEnum=$FldTypeCurrency or @DataTypeEnum=$FldTypeDate">
                                                <option value=""></option>
                                                <option value="Equals">
                                                  <xsl:if test="@Operator='Equals'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Equals
                                                </option>
                                                <option value="Is Greater Than">
                                                  <xsl:if test="@Operator='Is Greater Than'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Is Greater Than
                                                </option>
                                                <option value="Is Less Than">
                                                  <xsl:if test="@Operator='Is Less Than'">
                                                    <xsl:attribute name="selected">1</xsl:attribute>
                                                  </xsl:if>
                                                  Is Less Than
                                                </option>
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <option value=""></option>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </select>
                                        </td>
                                        <td align="center" class="formfield">
                                          <input type="text" size="15" class="form-control">
                                            <xsl:attribute name="name">searchFieldValue<xsl:value-of select="position()" /></xsl:attribute>
                                            <xsl:attribute name="fieldname">Optional Parameter <xsl:value-of select="position()" /></xsl:attribute>
                                            <xsl:attribute name="value"><xsl:value-of select="@Value" /></xsl:attribute>
                                          </input>
                                          <xsl:if test="@FieldName != ''">
                                            <script language="javascript">
                                              <xsl:choose>
                                                <xsl:when test="@DataTypeEnum=$FldTypeFloat">
                                                  document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isNumeric=true;
                                                </xsl:when>
                                                <xsl:when test="@DataTypeEnum=$FldTypeDate">
                                                  document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isDate=true;
                                                </xsl:when>
			                                          <xsl:when test="@DataTypeEnum=$FldTypeCurrency">
			                                            document.forms[0].searchFieldValue<xsl:value-of select="position()" />.pattern = "([^\\d\\.]+)|(\\d*\\.){2}|((\\s+|^)\\.(\\s+|$))";
   		                                          </xsl:when>
		                                          </xsl:choose>
                                              document.forms[0].searchFieldValue<xsl:value-of select="position()" />.isRequired=true;
                                            </script>
                                          </xsl:if>
                                        </td>
                                      </tr>
                                    </xsl:for-each>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td align="right" valign="top" class="hubLabel"></td>
                    <td colspan="2" align="center">
                      <table width="95%" cellpadding="0" cellspacing="0" border="0" class="formmain">
                        <tr>
                          <td align="left" valign="top">

                            <table align="center" width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                              <tr>
                                <td align="right" valign="top" class="hubLabel" nowrap="nowrap" >Sort By:</td>
                                <td align="left" valign="top" class="formfield">

                                  <select name="selSortBy" id="selSortBy" class="formfield" style="display: none;">
                                    <xsl:variable name="SortByText"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortBy']"/>~<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDir']"/></xsl:variable>
                                    <xsl:choose>
                                      <xsl:when test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='COTSOnly']='True'">
                                        <option>
                                          <xsl:attribute name="value">Batch.BatchID~BatchID~ASCENDING</xsl:attribute>
                                          <xsl:call-template name="batchidlabel" />
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.BatchNumber~BatchNumber~ASCENDING</xsl:attribute>
                                          <xsl:call-template name="batchnumberlabel" />
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.DepositDate~DepositDate~ASCENDING</xsl:attribute>Deposit Date
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Transactions.TxnSequence~TxnSequence~ASCENDING</xsl:attribute>Transaction Number
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentSource~PaymentSource~ASCENDING</xsl:attribute>Payment Source / Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentSource~PaymentSource~DESCENDING</xsl:attribute>Payment Source / Descending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentType~PaymentType~ASCENDING</xsl:attribute>Payment Type / Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentType~PaymentType~DESCENDING</xsl:attribute>Payment Type / Descending
                                        </option>
                                      </xsl:when>
                                      <xsl:otherwise>
                                        <option>
                                          <xsl:attribute name="value">Checks.Account~Account~ASCENDING</xsl:attribute>Account Number
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.BatchID~BatchID~ASCENDING</xsl:attribute>
                                          <xsl:call-template name="batchidlabel" />
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.BatchNumber~BatchNumber~ASCENDING</xsl:attribute>
                                          <xsl:call-template name="batchnumberlabel" />
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Checks.Amount~Amount~ASCENDING</xsl:attribute>Payment Amount/Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Checks.Amount~Amount~DESCENDING</xsl:attribute>Payment Amount/Descending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.DepositDate~DepositDate~ASCENDING</xsl:attribute>Deposit Date
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Checks.RT~RT~ASCENDING</xsl:attribute>R/T
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Checks.Serial~Serial~ASCENDING</xsl:attribute>Check/Trace/Ref Number
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Transactions.TxnSequence~TxnSequence~ASCENDING</xsl:attribute>Transaction Number
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentSource~PaymentSource~ASCENDING</xsl:attribute>Payment Source / Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentSource~PaymentSource~DESCENDING</xsl:attribute>Payment Source / Descending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentType~PaymentType~ASCENDING</xsl:attribute>Payment Type / Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">Batch.PaymentType~PaymentType~DESCENDING</xsl:attribute>Payment Type / Descending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">DDA~DDA~ASCENDING</xsl:attribute>DDA / Ascending
                                        </option>
                                        <option>
                                          <xsl:attribute name="value">DDA~DDA~DESCENDING</xsl:attribute>DDA / Descending
                                        </option>
                                      </xsl:otherwise>
                                    </xsl:choose>
                                  </select>
                                  <div class="btn-group entitySelect" id="SortBySelect"></div>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <tr>
                    <td class="hubLabel" valign="top" align="right">
                      Display<br/>Fields:
                    </td>
                    <td colspan="2" align="center">
                      <table width="95%" cellpadding="0" cellspacing="0" border="0" class="formmain">
                        <tr>
                          <td align="left" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0">
                              <tr>
                                <td align="left" valign="top">
                                  <table align="center" width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                                    <tr>
                                      <td>&nbsp;</td>
                                      <td align="center" class="hubLabel">Available</td>
                                      <td>&nbsp;</td>
                                      <td align="center" class="hubLabel">Selected</td>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td width="5%">&nbsp;</td>
                                      <td width="40%" align="center" valign="top">
                                        <select name="selAvailable" size="10" multiple="1" class="form-control" style="padding-left: 0px;">
                                          <xsl:for-each select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']]/DEFields/DEField">
                                            <xsl:variable name="curTableName"><xsl:value-of select="@TableName" /></xsl:variable>
                                            <xsl:variable name="curFieldName"><xsl:value-of select="@FldName" /></xsl:variable>
                                            <xsl:variable name="curReportTitle"><xsl:value-of select="@ReportTitle" /></xsl:variable>
                                            <xsl:choose>
                                              <xsl:when test="/Page/Recordset[@Name='Columns']/Record[@TableName=$curTableName and @FieldName=$curFieldName and @DisplayName=$curReportTitle]">
                                              </xsl:when>
                                              <xsl:otherwise>
                                                <option>
                                                  <xsl:attribute name="value"><xsl:value-of select="@TableName" />~<xsl:value-of select="@FldName" />~<xsl:value-of select="@FldDataTypeEnum" />~<xsl:value-of select="@ReportTitle" />~<xsl:value-of select="@BatchSourceKey" />~{TERMINATOR}</xsl:attribute>
                                                  <xsl:value-of select="@ReportTitle" />
                                                </option>
                                              </xsl:otherwise>
                                            </xsl:choose>
                                          </xsl:for-each>
                                        </select>
                                      </td>
                                      <td width="10%" align="center" valign="middle">
                                        <input type="button" name="cmdMoveAllAvailable" value="&gt;&gt;" class="btn btn-inverse" style="width: 32px;" onclick="MoveAll(this.form.elements['selAvailable'], this.form.elements['selSelected']);" />
                                        <p />
                                        <input type="button" name="cmdMoveSingleAvailable" value=" &gt; " class="btn btn-inverse" style="width: 32px;" onclick="MoveSelected(this.form.elements['selAvailable'], this.form.elements['selSelected']);" />
                                        <p />
                                        <input type="button" name="cmdMoveSingleSelected" value=" &lt; " class="btn btn-inverse" style="width: 32px;" onclick="MoveSelected(this.form.elements['selSelected'], this.form.elements['selAvailable']);" />
                                        <p />
                                        <input type="button" name="cmdMoveAllSelected" value="&lt;&lt;" class="btn btn-inverse" style="width: 32px;" onclick="MoveAll(this.form.elements['selSelected'], this.form.elements['selAvailable']);" />
                                      </td>
                                      <td width="40%" align="center" valign="top">
                                        <select name="selSelected" size="10" multiple="1" class="form-control">
                                          <xsl:for-each select="/Page/Recordset[@Name='Columns']/Record">
                                            <xsl:variable name="curTableName"><xsl:value-of select="@TableName" /></xsl:variable>
                                            <xsl:variable name="curFieldName"><xsl:value-of select="@FieldName" /></xsl:variable>
                                            <xsl:if test="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/DEFields/DEField[@TableName=$curTableName]">
                                              <xsl:if test="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/DEFields/DEField[@FldName=$curFieldName]">
                                                <option>
                                                  <xsl:attribute name="value"><xsl:value-of select="@TableName" />~<xsl:value-of select="@FieldName" />~<xsl:value-of select="@DataTypeEnum" />~<xsl:value-of select="@DisplayName" />~<xsl:value-of select="@BatchSourceKey" />~{TERMINATOR}</xsl:attribute>
                                                  <xsl:value-of select="@DisplayName" />
                                                </option>
                                              </xsl:if>
                                            </xsl:if>
                                          </xsl:for-each>
                                        </select>
                                      </td>
                                      <td width="5%" align="left" valign="middle">
                                        <input type="button" name="cmdMoveUp" value=" + " class="btn btn-inverse" style="width: 32px;" onclick="MoveUp(this.form.elements['selSelected']);" />
                                        <p />
                                        <input type="button" name="cmdMoveDown" value=" - " class="btn btn-inverse" style="width: 32px;" onclick="MoveDown(this.form.elements['selSelected']);" />
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='maintainquery.aspx']">
                  <tr>
                    <td align="right" valign="top" class="hubLabel">
                      Predefined<br/>Search Query:
                    </td>
                    <td align="center">
                      <table width="95%" cellpadding="0" cellspacing="0" border="0" class="formmain">
                        <tr>
                          <td align="left" valign="top">
                            <table width="100%" cellpadding="0" cellspacing="1" border="0">
                              <tr>
                                <td align="left" valign="top">
                                  <table align="center" width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                                    <tr>
                                      <td align="left" valign="top" class="formfield">
                                        <input type="hidden" name="txtSavedQueryname" maxlength="256" size="30" class="formfield">
                                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SavedQueryName']" /></xsl:attribute>
                                        </input>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td align="right" valign="top" class="hubLabel">Description: </td>
                                      <td colspan="4" align="left" class="formfield" valign="top">
                                        <input type="text" name="txtSavedQueryDescription" maxlength="1024" size="60" class="form-control">
                                          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SavedQueryDescription']" /></xsl:attribute>
                                        </input>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td/>
                                      <td colspan="4" align="left" valign="top" class="hubLabel">
                                        Default query when search page is loaded:  <input type="checkbox" name="chkDefaultQuery">
                                          <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DefaultQuery']='True'">
                                            <xsl:attribute name="checked">1</xsl:attribute>
                                          </xsl:if>
                                        </input>
                                      </td>
                                    </tr>
                                    <tr>
                                      <td/>
                                      <td colspan="3" align="left" valign="top" class="hubLabel" nowrap="nowrap">
                                        <a title="DeleteQuery" href="javascript:DeleteSavedQuery()">
                                          <i class="fa fa-times icon-large" /> Delete Query
                                        </a>
                                      </td>
                                      <td align="left" valign="top" class="hubLabel" nowrap="nowrap">
                                        <a title="Save Query" href="javascript:SubmitSavedQuery()" >
                                          <i class="fa fa-save icon-large" /> Save Query
                                        </a>
                                      </td>
                                    </tr>
                                  </table>
                                </td>
                              </tr>
                            </table>
                          </td>
                        </tr>
                      </table>
                    </td>
                  </tr>
                  </xsl:if>
                </table>
                <table width="100%" cellpadding="2" cellspacing="0" border="0" class="formsub">
                  <tr>
                    <td align="center" valign="top" colspan="2">
                      <input type="button" name="cmdSearch" value="Search" class="btn btn-primary" onClick="SelectAll(this.form.elements['selAvailable'], this.form.elements['selSelected']);formSubmitNegPattern(this, 'txtAction');" />
                      &nbsp;&nbsp;

                      <input type="button" name="cmdClear"  value="Clear Search" class="btn btn-primary" onClick="ClearQuery()" />
                      <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='maintainquery.aspx']">
                      &nbsp;&nbsp;
                      <input type="button" name="cmdManageQueries" value="Manage Queries" class="btn btn-primary" onClick="location.href='maintainquery.aspx';" />
                      </xsl:if>
                    </td>
                  </tr>
                  <tr>
                    <td style="height: 3px;">&nbsp;</td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
    </table>

    <script language="javascript">
      <xsl:comment>
        //Hide script from older browsers
        //Set form field validation requirements
        with (document.forms['frmLockBoxSearch']){
        if (elements["txtDateFrom"]){
        txtDateFrom.isDate = true;
        txtDateFrom.fieldname = 'Deposit Date From';
        txtDateFrom.isRequired = true;
        }
        if (elements["txtDateTo"]){
        txtDateTo.isDate = true;
        txtDateTo.fieldname = 'Deposit Date To';
        }
        if (elements["txtBatchIDFrom"]){
        txtBatchIDFrom.isNumeric = true;
        txtBatchIDFrom.min = 0;
        txtBatchIDFrom.max = 999999999999999;
        txtBatchIDFrom.fieldname = 'Batch ID';
        }
        if (elements["txtBatchIDTo"]){
        txtBatchIDTo.isNumeric = true;
        txtBatchIDTo.min = 0;
        txtBatchIDTo.max = 999999999999999;
        txtBatchIDTo.fieldname = 'Batch ID';
        }
        if (elements["txtBatchNumberFrom"]){
        txtBatchNumberFrom.isNumeric = true;
        txtBatchNumberFrom.min = 0;
        txtBatchNumberFrom.max = 999999999999999;
        txtBatchNumberFrom.fieldname = 'Batch Number';
        }
        if (elements["txtBatchNumberTo"]){
        txtBatchNumberTo.isNumeric = true;
        txtBatchNumberTo.min = 0;
        txtBatchNumberTo.max = 999999999999999;
        txtBatchNumberTo.fieldname = 'Batch Number';
        }
        if (elements["txtAmountFrom"]){
        txtAmountFrom.pattern = '([^\\d\\.]+)|(\\d*\\.){2}|((\\s+|^)\\.(\\s+|$))';
        txtAmountFrom.fieldname = 'Payment Amount From field value is ';
        }
        if (elements["txtAmountTo"]){
        txtAmountTo.pattern = '([^\\d\\.]+)|(\\d*\\.){2}|((\\s+|^)\\.(\\s+|$))';
        txtAmountTo.fieldname = 'Payment Amount To field value is ';
        }
        if (elements["txtSavedQueryname"]){
        txtSavedQueryname.pattern = '(\\s{2,}|[^-a-zA-Z0-9._ /\'/]+)';
        txtSavedQueryname.fieldname = 'Name';
        }
        if (elements["txtSavedQueryDescription"]){
        txtSavedQueryDescription.pattern = '(\\s{2,}|[^-a-zA-Z0-9._ /\'/]+)';
        txtSavedQueryDescription.fieldname = 'Description';
        }
        //End hiding-->
        }

        function DeleteSavedQuery()
        {
        formSubmitWithoutValidation2(
        document.forms['frmLockBoxSearch'],
        document.forms['frmLockBoxSearch'].txtAction,
        'DeleteQuery');
        }

        function SubmitSavedQuery()
        {
        var bshowalert=true;
        var name=document.forms['frmLockBoxSearch'].txtSavedQueryname.value;
        if ((name==null) || (name=="") || (name=="Ad Hoc Query")) {
        name=prompt("Please enter New Query Name","");
        }
        if (name==null || name==""){
        return;
        }
        if (name!=null){
        if (name!=""){
        if (name!="Ad Hoc Query"){
        if (validatetextresult(name)){
        bshowalert=false;
        if (name!=document.forms['frmLockBoxSearch'].txtSavedQueryname.value) {
        document.forms['frmLockBoxSearch'].txtSavedQueryname.value=name;
        }
        if (document.forms['frmLockBoxSearch'].txtAction.value!='EditName'){
        document.forms['frmLockBoxSearch'].txtAction.value='SaveQuery';
        }
        SelectAll(document.forms[0].elements['selAvailable'], document.forms[0].elements['selSelected']);
        if (formValidateNegPattern(document.forms[0])) {
        formSubmitWithoutValidation2(
        document.forms['frmLockBoxSearch'],
        document.forms['frmLockBoxSearch'].txtAction,
        document.forms['frmLockBoxSearch'].txtAction.value);
        }
        }
        }
        }
        }
        if (bshowalert){
        alert("Invalid Name!  Changes have not been saved! ");
        }
        }

        function validatetextresult(strResult){
        var reg = new RegExp ('(\\s{2,}|[^-a-zA-Z0-9._ /\'/]+)',"i");
        return (!(reg.test(strResult)));
        }

        function formSubmitNegPattern (frmobj,field) {
          formSubmitWithoutValidation(frmobj,field);
        }

        function ClearQuery()
        {
        formSubmitWithoutValidation2(document.forms['frmLockBoxSearch'], document.forms['frmLockBoxSearch'].txtAction, 'clearquery');
        }

        function chkCOTSOnly_onClick()
        {
        SelectAll(document.forms[0].elements['selAvailable'], document.forms[0].elements['selSelected']);
        formSubmitNegPattern(document.forms['frmLockBoxSearch'].chkCOTSOnly, 'txtAction');
        }

        function edit_name()
        {
        var defaultname=document.forms['frmLockBoxSearch'].txtSavedQueryname.value;
        var name=prompt("Please edit query name",defaultname);
        var bnewname = false;
        if (name!=null){
        if (name!=""){
        if (defaultname==name){
        alert("Name has not changed!");
        }
        else {
        if (validatetextresult(name)){
        document.forms['frmLockBoxSearch'].txtSavedQueryname.value = name;
        add_name_to_query_list(name,bnewname);
        document.forms['frmLockBoxSearch'].txtAction.value='EditName'
        }
        else {
        document.forms['frmLockBoxSearch'].txtSavedQueryname.value = defaultname;
        alert("Invalid Name!");
        }
        }
        }
        else {
        alert("Invalid Name, No changes have been made.");
        }
        }
        }

        function new_name()
        {
        var bnewname = true;
        var name = prompt("Please enter name of new query","");
        if (name != null){
        if (name != ""){
        if (validatetextresult(name)){
        document.forms['frmLockBoxSearch'].txtSavedQueryname.value = name;
        add_name_to_query_list(name,bnewname);
        }
        else {
        alert("Invalid Name, New name has not been created.");
        }
        }
        else {
        alert("Name is blank!  New name has not been created.");
        }
        }
        }

        function add_name_to_query_list(name,bnewname)
        {
        var elSel=document.forms['frmLockBoxSearch'].selSaveQuery; //document.getElementById('selSaveQuery');

        var elOptNew=document.createElement('option');
        elOptNew.text=name;
        if (!bnewname){
        elOptNew.value=elSel.options[elSel.selectedIndex].value;
        }
        var elOptOld=elSel.options[0];
        try {
        elSel.add(elOptNew, elOptOld);
        }
        catch(ex) {
        elSel.add(elOptNew, 0);
        }
        elSel.options[0].selected = 1;
        }

        <xsl:variable name="PageAction"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PageAction']" /></xsl:variable>
        // PageAction = <xsl:value-of select="$PageAction" />
        var isFirstTime = <xsl:choose>
            <xsl:when test="$PageAction = 'refine'">true;</xsl:when>
            <xsl:when test="$PageAction = 'search'">true;</xsl:when>
            <xsl:otherwise>false;</xsl:otherwise>
            </xsl:choose>



        var selecteddataentrycolumns = [];
        var loadeddecolumns = false;

        <xsl:for-each select="/Page/Recordset[@Name='Columns']/Record">
            selecteddataentrycolumns.push('<xsl:value-of select="@TableName" />~<xsl:value-of select="@FieldName" />~<xsl:value-of select="@DataTypeEnum" />~<xsl:value-of select="@DisplayName" />~<xsl:value-of select="@BatchSourceKey" />~{TERMINATOR}');
        </xsl:for-each>
 
 	      // Create an Array of Selected Items from Saved Query (or refine)
        var selectedparmcolumns = [];
        <xsl:for-each select="/Page/Recordset[@Name='AdvancedSearchParms']/Record">
            selectedparmcolumns.push({id:'<xsl:value-of select="@TableName" />~<xsl:value-of select="@FieldName" />~<xsl:value-of select="@DataTypeEnum" />~<xsl:value-of select="@ReportTitle" />~<xsl:value-of select="@BatchSourceKey" />',operator:'<xsl:value-of select="@Operator" />',value:'<xsl:value-of select="@Value" />'});    
        </xsl:for-each>

        $(document).ready(function() {

        var loadDataEntryFieldsForWorkgroup = function(selectedworkgroup, callbackfn)
        {
            // Javascript to call the ASMX for ajax data-entry search.
            var id = selectedworkgroup.id.split('|');
            var bankid = id[0];
            var workgroupid = id[1];
            var data = { workgroupid: workgroupid, bankid: bankid };
            $.ajax({ 
                url: "/IPOnline/services/lockboxsearch.asmx/GetDataEntryFieldsByWorkgroup", 
                type: "POST",
                contentType: "application/json",  
                data: JSON.stringify( data ), 
                dataType: "json",  
                success: function(result) {   
                  // And here is where we use them.
                  var dataentryary = [];
                  
                  $.each(result.d, function(ind, val)
                  {
                    // Convert the object to an array - just to make it work with the current version of the page.
                    // "7" means it's sorted after the default DE fields.
                    var ary = [
                      val.TableName,
                      val.FldName,
                      val.DataType,
                      val.DisplayName,
                      7,
                      val.BatchSourceKey
                    ];
                    
                    dataentryary.push(ary);
                  });
                  
                  
                  currentworkgroup.DataEntryFields = dataentryary;
                  if (callbackfn)
                    callbackfn();
                  
                },
                error: function(xhr, status, e) {
                  framework.errorToast($('#tabbase'), 'Error Loading Data Entry Fields.');
                }
            });
        
        }

        var loadWorkgroup = function(selectedworkgroup, callbackfn)
        {
            // Javascript to call the ASMX for ajax workgroup search.
            var id = selectedworkgroup.id.split('|');
            var bankid = id[0];
            var workgroupid = id[1];
            var data = { workgroupid: workgroupid, bankid: bankid };
            $.ajax({ 
                url: "/IPOnline/services/lockboxsearch.asmx/GetWorkgroup", 
                type: "POST",
                contentType: "application/json",  
                data: JSON.stringify( data ), 
                dataType: "json",  
                success: function(result) {   
                  // And here is where we use them.
                  
                  currentworkgroup = result.d;
                  if (callbackfn)
                    callbackfn();
                },
                error: function(xhr, status, e) {
                  framework.errorToast($('#tabbase'), 'Error Loading Selected Workgroup.');
                }
            });
        
        };



        var isPageLoad = true;
        framework.showSpinner("Loading...");

        //**************************************************************
        //  Workgroup Selector
        //**************************************************************

        var _vm = new wGViewModel();

        var dataCallback = function (resp, widget)
        {
          var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
          var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
          var ecount = entities.length;
          var wcount = workgroups.length;

          if (ecount === 1 &#38;&#38; wcount === 1)
          {
            var el = $('&lt;input id="txtWorkgroupSelection" name="txtWorkgroupSelection" value="" type="hidden"&gt;&lt;div&gt;&lt;label class="control-label"&gt;&lt;/label&gt;&lt;input value="'+ workgroups[0].label+ '" class="form-control input-md" type="text" readonly="" style="display:inline;" /&gt;&lt;/div&gt;');
            $('#treeBase').html(el);
            _vm['selectedWorkgroup'] = workgroups[0];
            $('#txtWorkgroupSelection').val(workgroups[0].id);
            title = 'Selected: ' + workgroups[0].label;
            
            selectedTreeItem(workgroups[0], false);
          }
          else
          {
            _vm.workGroupSelector.wfsTreeSelector('initialize');
            if($('#txtWorkgroupSelection').val())
            {
              widget.setSelection($('#txtWorkgroupSelection').val());
            }
          }
        };

        var selectedTreeItem = function (selected, initial) 
        {
          var title = null;

          if (selected &#38;&#38; !initial) 
          {
            if (selected.isNode)
            {
              // Loads selected workgroup into the currentworkgroup variable.
              loadWorkgroup(selected, function()
              {
                $('#txtWorkgroupSelection').val(selected.id);
                $('#txtWorkgroupSelectionLabel').val(selected['label']);
                title = 'Selected: ' + selected['label'];
                _vm.workGroupSelector.wfsTreeSelector('setTitle', title);
                
                // Loads in data entry for the selected workgroup.
                loadDataEntryFieldsForWorkgroup(selected, function()
                {
                  if (isFirstTime)
                  {
                    isFirstTime = false;
                  }
                  txtOLLockbox_OnChange();
                
                  if (!loadeddecolumns)
                  {
                     $('select[name="selAvailable"]').val(selecteddataentrycolumns);
                     var src = $('select[name="selAvailable"]');
                     var dest = $('select[name="selSelected"]');
                     $.each(selecteddataentrycolumns, function(i, element)
                     {
                        el = src.find("option[value='"+ element + "']");
                        dest.append(el);
                     });
                     
                     $.each(selectedparmcolumns, function(ind, ele)
                     {
                       var selfield = $('select[name="selField' + (ind + 1) + '"]');
                       var selopfield = $('select[name="selOperator' + (ind + 1) + '"]');
                       var txtfield = $('input[name="searchFieldValue' + (ind + 1) + '"]');
                     
                       selfield.val(ele.id);
                       selField_OnChange(selfield[0], selopfield[0], txtfield[0]);
                       selopfield.val(ele.operator);
                       txtfield.val(ele.value);
                    });
                 
                    loadeddecolumns = true;
                    framework.hideSpinner();
                }

              }); // Load DE Fields Callback
            }); // LoadWorkgroup Callback
            
              

          }
          else
          {
            framework.errorToast($('#tabbase'), 'Please select a single workgroup.');
          }
        }
        else if (initial)
        {
          framework.hideSpinner();
        }
               
        

        };

        _vm.workGroupSelector = $('#treeBase').wfsTreeSelector({
        useExpander: true,
        entityURL: '/RecHubRaamProxy/api/entity',
        callback: selectedTreeItem,
        dataCallback: dataCallback,
        expanderTitle: "Select Workgroup",
        entitiesOnly: false,
        closeOnSelectNonNode: false
        });

        function wGViewModel(model) {
        var self = this;
        self.model = model;

        <!--self.labels = model.Labels;
        self.selectedWorkgroup = null;-->
        };

        //**************************************************************

        var startDate = new Date($('#txtDateFrom').val());
        var endDate = new Date($('#txtDateTo').val());
        //**************************************************************
        //  Start Date DatePicker
        //**************************************************************
        $('#StartDatePicker').wfsDatePicker({dateIn: startDate, callback: startDateChange});
        //**************************************************************
        //**************************************************************
        //  End Date DatePicker
        //**************************************************************
        $('#EndDatePicker').wfsDatePicker({dateIn: endDate, callback: endDateChange});
        //**************************************************************

        //**************************************************************
        //  Save Query Select
        //**************************************************************

        var saveQueryArray = [{ Id: 'Ad Hoc Query', Name: 'Ad Hoc Query' },<xsl:for-each select="/Page/Recordset[@Name='GetSavedQueries']/Record">
          {Id: '<xsl:value-of select="@PredefSearchID"/>', Name: '<xsl:apply-templates select="@Name" mode="escape"/>' },
        </xsl:for-each> {Id: 'remove', Name: 'remove'} ];

        findAndRemove(saveQueryArray, 'Id', 'remove');

        <xsl:variable name="OLPredefSearchID"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PredefSearchID']"/></xsl:variable>

        var vm = new SaveQueryViewModel(saveQueryArray);
        $('#SaveQuerySelect').wfsSelectbox({ 'items': vm.items, 'callback': vm.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
        var selectedItem = '<xsl:for-each select="/Page/Recordset[@Name='GetSavedQueries']/Record">
          <xsl:choose>
            <xsl:when test="@PredefSearchID=$OLPredefSearchID">
              <xsl:value-of select="@PredefSearchID"/>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>';
        var selObj = ($.grep(vm.items, function(e){ return e.id == selectedItem; }))[0];
        if (selObj)
        $('#SaveQuerySelect').wfsSelectbox('setValueByData', selObj);

        function SaveQueryViewModel(items) {
        var self = this;
        self.items = items;
        self.selectedItem = null;
        self.setItemValue = function (data) {
        self.selectedItem = data;
        if (data) {
        $('select[name=selSaveQuery] option[value="' + data.Id + '"]').attr('selected', true);
        }
        else
        {
        $('select[name=selSaveQuery] option[value="Ad Hoc Query"]').attr('selected', true);
        }
        if(isPageLoad == false)
        selSaveQuery_OnChange();
        }
        }
        //**************************************************************

        //**************************************************************
        //  Sort By Select
        //**************************************************************

        vm = new SelectViewModel($('#selSortBy'));
        $('#SortBySelect').wfsSelectbox({ 'items': vm.items, 'callback': vm.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
        var selectedItem = '<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortBy']" />~<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDisplayName']" />~<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDir']" />ENDING';
        var selObj = ($.grep(vm.items, function(e){ return e.id == selectedItem; }))[0];
        if (selObj)
        $('#SortBySelect').wfsSelectbox('setValueByData', selObj);

        //**************************************************************
        //  Payment Source Select
        //**************************************************************

        vm = new SelectViewModel($('#txtPaymentSource'));
        $('#PaymentSourceSelect').wfsSelectbox({ 'items': vm.items, 'callback': vm.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
        var selectedItem = '<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchSourceKey']"/>';
        var selObj = ($.grep(vm.items, function(e){ return e.id == selectedItem; }))[0];
        if (selObj)
        $('#PaymentSourceSelect').wfsSelectbox('setValueByData', selObj);


        //**************************************************************
        //  Payment Type Select
        //**************************************************************
        vm = new SelectViewModel($('#txtPaymentType'));
        $('#PaymentTypeSelect').wfsSelectbox({ 'items': vm.items, 'callback': vm.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
        var selectedItem = '<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchPaymentTypeKey']"/>';
        var selObj = ($.grep(vm.items, function(e){ return e.id == selectedItem; }))[0];
        if (selObj)
        $('#PaymentTypeSelect').wfsSelectbox('setValueByData', selObj);

        function SelectViewModel(selector) {
        var self = this;
        var $target = selector;

        $target.hide();
        self.items = [];
        $('option', $target).each(function(i) {
        self.items.push({Id: $(this).val(), Name: $(this).text()});
        });

        self.selectedItem = null;
        self.setItemValue = function (data) {
        self.selectedItem = data;
        $('option', $target).removeAttr('selected');
        if (data) {
        var t =  $('option[value="' + data.Id + '"]');
        $target.val(data.Id);
        }
        }
        }

        isPageLoad = false;

        });

        
        function findAndRemove(array, property, value) {
        $.each(array, function(index, result) {
        if(result[property] == value) {
        //Remove from array
        array.splice(index, 1);
        }
        });
        }
        
        function findIndex(array, property, value) {
        var newIndex = 0;
        $.each(array, function(index, result) {
        if(result[property] == value) {
        newIndex = index;
        }
        });
        
        return newIndex;

        }

        function findIndex1(array, property, value) {
        var newIndex = 0;
        $.each(array, function(index, result) {
        if(result[property].startsWith(value)) {
        newIndex = index;
        }
        });

        return newIndex;

        }

        if(!String.prototype.startsWith){
        String.prototype.startsWith = function (str) {
        return !this.indexOf(str);
        }
        }

        function startDateChange(val) {
        document.getElementById('txtDateFrom').value = val;
        }

        function endDateChange(val) {
        document.getElementById('txtDateTo').value = val;
        }


      </xsl:comment>
    </script>
  </xsl:template>


</xsl:stylesheet>
