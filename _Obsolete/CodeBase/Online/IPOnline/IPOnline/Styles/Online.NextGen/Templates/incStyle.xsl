<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "<xsl:text disable-output-escaping='yes'>&amp;copy;</xsl:text>">
  <!ENTITY middot "<xsl:text disable-output-escaping='yes'>&amp;middot;</xsl:text>">
  <!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:include href="incMessages.xsl"/>
  <xsl:include href="incCustomBranding.xsl"/>

  <xsl:output method="html"/>

  <xsl:template match="/">
    <xsl:text disable-output-escaping='yes'>&lt;!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"&gt;</xsl:text>

    <html>
      <head>        
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>
          <xsl:call-template name="Title"/>
        </title>

        <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />

        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/scriptlib.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/dynamicmenu.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/openwindow.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/menu.js" />
        <script type="text/javascript" language="JavaScript" src="{$brandtemplatepath}/js/adrotator.js" />

        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/AnchorPosition.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/Date.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/PopupWindow.js" />
        <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/CalendarPopup.js" />
        <script type="text/javascript" language="JavaScript" src="{$brandtemplatepath}/js/browserdetect.js" />

        <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/calendar.css" />

        <!-- *********************************************************************************** -->
        <!-- OLTA Re-theming -->
        <!-- *********************************************************************************** -->
        <!-- *************** -->
        <!--   Stylesheets   -->
        <!-- *************** -->
        <link rel="stylesheet" type="text/css" href="/Assets/jquery/css/jquery-ui.min.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/bootstrap/css/bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/jasny-bootstrap/css/jasny-bootstrap.min.css" />
		    <link rel="stylesheet" type="text/css" href="/Assets/bootstrap-modal/css/bs-modal.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/fontawesome/css/font-awesome.min.css" />        
        <link rel="stylesheet" type="text/css" href="/Assets/slickgrid/controls/slick.pager.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/select2/select2.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/jqwidgets/styles/jqx.base.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/jquery-toastmessage/resources/css/jquery.toastmessage.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/framework/CSS/framework.css" />
        <link rel="stylesheet" type="text/css" href="/Assets/framework/CSS/wfs-bootstrap.css" />
        <link rel="stylesheet" type="text/css" href="/Framework/Framework/CSSOverride" />

        <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/Overrides.css" />

        <!-- *************** -->
        <!--    Javascript   -->
        <!-- *************** -->

		  <script type="text/javascript" src="/Assets/jquery/js/jquery.min.js"></script>
		  <script type="text/javascript" src="/Assets/jquery/js/jquery-ui.min.js"></script>
		  <script type="text/javascript" src="/Assets/jquery/js/jquery-toDictionary.js"></script>
		  <script type="text/javascript" src="/Assets/jquery-validation/dist/jquery.validate.min.js"></script>
		  <script type="text/javascript" src="/Assets/knockout/knockout.js"></script>
		  <script type="text/javascript" src="/Assets/bootstrap/js/bootstrap.min.js"></script>
		  <script type="text/javascript" src="/Assets/jquery/js/jquery-toDictionary.js"></script>
      <script type="text/javascript" src="/Assets/jquery-toastmessage/js/jquery.toastmessage.js"></script>
      <script type="text/javascript" src="/Assets/etc/spin.min.js"></script>
		  <script type="text/javascript" src="/Assets/select2/select2.js"></script>

		  <script type="text/javascript" src="/Assets/framework/JS/frameworkUtils.js"></script>
		  <script type="text/javascript" src="/Assets/bootstrap-modal/js/bs-modal.js"></script>

    
      <script type="text/javascript" src="/Assets/jqwidgets/jqx-all.js"></script>
      <script type="text/javascript" src="/Assets/moment/moment.js"></script>
		  <!--<script type="text/javascript" src="/Assets/wfs/wfs-datatypes.js"></script>-->
		  <script type="text/javascript" src="/Assets/wfs/wfs-datepicker.js"></script>
		  <script type="text/javascript" src="/Assets/wfs/wfs-dateutil.js"></script>
		  <script type="text/javascript" src="/Assets/wfs/wfs-select.js"></script>
      <script type="text/javascript" src="/Assets/wfs/wfs-treeselector.js"></script>
      <script type="text/javascript" language="JavaScript" src="{$brandtemplatepath}/js/IdleTimeout.js"></script>

      <script>
        $(document).ready(function() {
          var framework = new FrameworkUtils();
          framework.loadByContainer("<xsl:value-of select="normalize-space($headerURL)"/>", $('#loadHeader'));
          framework.loadByContainer("<xsl:value-of select="normalize-space($menuURL)"/>", $('#loadMenu'));
          framework.loadByContainer("<xsl:value-of select="normalize-space($footerURL)"/>", $('#loadFooter'));
          registerKeepAlive(<xsl:value-of select="$idleTimeoutMs"/>);
        });
      </script>
        <!-- *********************************************************************************** -->
        <!-- *********************************************************************************** -->

      </head>
      <body onLoad="NN4autoHide();">
        <a name="top"></a>

        <input type="hidden" id="refreshed" value="no">
          <script type="text/javascript">
            onload=function() {

            var bolDoSubmit;

            var e=document.getElementById("refreshed");
            if(e.value=="no") {
            e.value="yes";
            } else {
            e.value="no";

            if(document.forms['frmPageState'] != undefined) {
            if(document.forms['frmPageState'].txtResubmitAllowed != undefined) {
            if(document.forms['frmPageState'].txtResubmitAllowed.value='true') {
            bolDoSubmit = true;
            } else {
            bolDoSubmit = false;
            }
            } else {
            bolDoSubmit = false;
            }
            } else {
            bolDoSubmit = false;
            }

            if(bolDoSubmit == true) {
            InsertSecurityToken(document.forms['frmPageState'].txtResubmitAllowed);
            document.forms['frmPageState'].submit();
            } else {
            document.location.reload(true);
            }
            }
            }
          </script>
        </input>

        <input type="hidden" name="PageReqID" id="Security_Token">
          <xsl:attribute name="value">
            <xsl:value-of select="/Page/@Security_Token"/>
          </xsl:attribute>
        </input>

        <table width="100%" border="0" cellpadding="0" cellspacing="0" id="MainPageTable">
          <tr>
            <td align="left" valign="top">
              <div id="loadHeader" class="frameworkContainer"></div>
              <div id="loadMenu" class="frameworkContainer"></div>

              <div class="col-xs-12" style="padding-top: 15px;">
                <div id="tabbase" class="panel panel-default">
                  <div class="panel-heading">
                    <span class="portlet-text">
                      <xsl:call-template name="PageTitle"/>
                    </span>
                  </div>
                  <div class="panel-body landing-content" style="font-size: 12px !important;">



                    <xsl:if test="/Page/SystemInfo[IsEmulationSession = 'True']">
                      <h5>*** User Emulation Mode ***</h5>
                    </xsl:if>

                    <xsl:apply-templates select="/Page/Messages"/>

                    <xsl:apply-templates select="/Page"/>

                    <p/>
                  </div>
                </div>
              </div>
            </td>
          </tr>
        </table>
        <div id="loadFooter" class="container"></div>

        <script language="javascript" type="text/javascript">buildSub();</script>
        <script language="JavaScript" type="text/javascript">
          initFocusHandler();
          initFormFocus();
        </script>

        <script type="text/javascript">

        </script>
      </body>
    </html>
  </xsl:template>


  <xsl:template name="PageTitle">
    <span class="contenttitle">
      <xsl:call-template name="appname" />
    </span>
    <!-- br/>
    <span class="contentsubtitle">&nbsp;</span -->
  </xsl:template>


  <xsl:template match="Page">
    <h3>You should not be seeing this template.</h3>
    TODO: Modify this template to display generic error message.
    <p/>
  </xsl:template>


  <xsl:template name="BreadCrumbs">

    <xsl:if test="/Page/UserInfo/SessionBreadCrumbs/SessionBreadCrumb/@ScriptName='lockboxsearch.aspx'">
      <a>
        <xsl:attribute name="href">
          lockboxsearch.aspx?id=bc&amp;Action=search
        </xsl:attribute>Advanced Search
      </a>&nbsp;&gt;&nbsp;
    </xsl:if>
    <xsl:if test="/Page/UserInfo/SessionBreadCrumbs/SessionBreadCrumb/@ScriptName='remitsearch.aspx'">
      <a>
        <xsl:attribute name="href">
          remitsearch.aspx?id=bc
        </xsl:attribute>Payment Search
      </a>&nbsp;&gt;&nbsp;
    </xsl:if>
    <xsl:if test="/Page/UserInfo/SessionBreadCrumbs/SessionBreadCrumb/@ScriptName='batchsummary.aspx'">
      <a>
        <xsl:attribute name="href">
          <xsl:value-of select="/Page/@BatchSummaryURL"/>
        </xsl:attribute>Batch Summary
      </a>&nbsp;&gt;&nbsp;
    </xsl:if>
    <xsl:if test="/Page/UserInfo/SessionBreadCrumbs/SessionBreadCrumb/@ScriptName='remitdisplay.aspx'">
      <a class="batch-detail-breadcrumb">
        <xsl:attribute name="href">
          /Framework/Framework?tab=2&amp;redir=batchdetail
        </xsl:attribute>Batch Detail
      </a>&nbsp;&gt;&nbsp;
    </xsl:if>
  </xsl:template>

  <xsl:template match="@* | node()" mode="escape">
    <!-- Escape the apostrophes second -->
    <xsl:call-template name="replace">
      <xsl:with-param name="pTarget" select='"&apos;"' />
      <xsl:with-param name="pReplacement" select='"\&apos;"'/>
      <xsl:with-param name="pText">
        <!-- Escape the backslashes first, and then pass that result directly into the next template -->
        <xsl:call-template name="replace">
          <xsl:with-param name="pTarget" select="'\'" />
          <xsl:with-param name="pReplacement" select="'\\'" />
          <xsl:with-param name="pText" select="." />
        </xsl:call-template>
      </xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template name="replace">
    <xsl:param name="pText"/>
    <xsl:param name="pTarget" select='"&apos;"'/>
    <xsl:param name="pReplacement" select="'\&quot;'"/>

    <xsl:if test="$pText">
      <xsl:value-of select='substring-before(concat($pText,$pTarget),$pTarget)'/>
      <xsl:if test='contains($pText, $pTarget)'>
        <xsl:value-of select='$pReplacement'/>
      </xsl:if>

      <xsl:call-template name="replace">
        <xsl:with-param name="pText" select='substring-after($pText, $pTarget)'/>
        <xsl:with-param name="pTarget" select="$pTarget"/>
        <xsl:with-param name="pReplacement" select="$pReplacement"/>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>

</xsl:stylesheet>
