<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>

  <xsl:output method="html"/>

  <xsl:variable name="FldTypeString">1</xsl:variable>
  <xsl:variable name="FldTypeFloat">6</xsl:variable>
  <xsl:variable name="FldTypeCurrency">7</xsl:variable>
  <xsl:variable name="FldTypeDate">11</xsl:variable>


  <xsl:variable name="MaxPrintableRows"><xsl:value-of select="/Page/PageInfo/@MaxPrintableRows"/></xsl:variable>
  <xsl:variable name="WorkgroupSelection"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelection']" /></xsl:variable>
  <xsl:variable name="WorkgroupSelectionLabel"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelectionLabel']" /></xsl:variable>
  <xsl:variable name="OLWorkGroupsID"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" /></xsl:variable>
  <xsl:variable name="ClientAccountID"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/@ClientAccountID" /></xsl:variable>
  <xsl:variable name="LockboxLongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/@LongName" /></xsl:variable>
  <xsl:variable name="IsRetailLockbox"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/@IsRetailLockbox" /></xsl:variable>
  <xsl:variable name="CheckImageDisplayMode"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/@CheckImageDisplayMode" /></xsl:variable>
  <xsl:variable name="DocumentImageDisplayMode"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Lockbox[@OLWorkGroupsID=$OLWorkGroupsID]/@DocumentImageDisplayMode" /></xsl:variable>
  <xsl:variable name="DisplayBatchID"><xsl:value-of select="/Page/WorkgroupData/DisplayBatchID" /></xsl:variable>
  <xsl:template name="Title">Search</xsl:template>
  <xsl:variable name="IsPrintable"><xsl:choose><xsl:when test="$MaxPrintableRows=0">1</xsl:when><xsl:otherwise><xsl:choose><xsl:when test="$MaxPrintableRows>=/Page/Recordset/Record[@DisplayTitle='Payment Count']/@DisplayText">1</xsl:when><xsl:when test="''=string(/Page/Recordset/Record[@DisplayTitle='Payment Count']/@DisplayText)">1</xsl:when><xsl:otherwise>0</xsl:otherwise></xsl:choose></xsl:otherwise></xsl:choose></xsl:variable>

  <xsl:template name="PageTitle">
    <span class="contenttitle">Advanced Search</span>
    <br/>
  </xsl:template>


  <xsl:template match="Page">
    <div class="page-portlet-view">
      <span class="account-heading">Workgroup: </span>
      <form method="post" onSubmit="return false;">
        <xsl:attribute name="name">frmLockBoxSearch</xsl:attribute>

        <xsl:if test="Recordset[@Name='Results']">
          <xsl:apply-templates select="Recordset[@Name='Results']" mode="LockboxSearch"/>
        </xsl:if>
      </form>
    </div>
  </xsl:template>


  <xsl:template match="Recordset" mode="LockboxSearch">
    <xsl:variable name="startrecord"><xsl:value-of select="../PageInfo/@StartRecord"/></xsl:variable>

    <input type="hidden" name="txtAction" value="" />
    <input type="hidden" name="txtStart">
      <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
    </input>
    <input type="hidden" name="PreDefSearchID">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PredefSearchID']" /></xsl:attribute>
    </input>
    <input type="hidden" name="selSaveQuery">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='PredefSearchID']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtWorkgroupSelectionLabel" id="txtWorkgroupSelectionLabel">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelectionLabel']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtWorkgroupSelection" id="txtWorkgroupSelection">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='WorkgroupSelection']" /></xsl:attribute>
    </input>
    
    <input type="hidden" name="txtOLLockbox">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtDateFrom">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DateFrom']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtDateTo">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DateTo']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtBatchIDFrom">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchIDFrom']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtBatchIDTo">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchIDTo']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtBatchNumberFrom">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchNumberFrom']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtBatchNumberTo">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchNumberTo']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtAmountFrom">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='AmountFrom']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtAmountTo">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='AmountTo']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtSerial">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='Serial']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtPaymentSource">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchSourceKey']" /></xsl:attribute>
    </input>

    <input type="hidden" name="txtPaymentType">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='BatchPaymentTypeKey']" /></xsl:attribute>
    </input>


    <input type="hidden" name="chkCOTSOnly">
      <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='COTSOnly']='True'">
        <xsl:attribute name="value">on</xsl:attribute>
      </xsl:if>
    </input>

    <input type="hidden" name="chkMarkSenseOnly">
      <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='MarkSenseOnly']='True'">
        <xsl:attribute name="value">on</xsl:attribute>
      </xsl:if>
    </input>

    <xsl:for-each select="/Page/Recordset[@Name='SearchSavedCriteria']/field"> 
      <input type="hidden" name="selSelected">
        <xsl:attribute name="value"><xsl:value-of select="@tablename" />~<xsl:value-of select="@fieldname" />~<xsl:value-of select="@datatype" />~<xsl:value-of select="@reporttitle" />~<xsl:value-of select="@batchsourcekey" />~{TERMINATOR}</xsl:attribute>
      </input>
    </xsl:for-each>

    <xsl:for-each select="/Page/Recordset[@Name='AdvancedSearchParms']/Record">

      <input type="hidden">
        <xsl:attribute name="name">selField<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@TableName" />~<xsl:value-of select="@FieldName" />~<xsl:value-of select="@DataTypeEnum" />~<xsl:value-of select="@ReportTitle" />~<xsl:value-of select="@BatchSourceKey" /></xsl:attribute>
      </input>
      <input type="hidden">
        <xsl:attribute name="name">selOperator<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@Operator" /></xsl:attribute>
      </input>
      <input type="hidden">
        <xsl:attribute name="name">searchFieldValue<xsl:value-of select="position()"/></xsl:attribute>
        <xsl:attribute name="value"><xsl:value-of select="@Value" /></xsl:attribute>
      </input>
    </xsl:for-each>

    <input type="hidden" name="selSortBy">
      <xsl:choose>
        <xsl:when test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDir'] = 'DESC'">
          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortBy']" />~<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDisplayName']" />~DESCENDING</xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortBy']" />~<xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SortByDisplayName']" />~ASCENDING</xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
    </input>

    <input type="hidden" name="selSelected">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DisplayFields']" /></xsl:attribute>
    </input>

    <input type="hidden" name="selAvailable">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='AvailableFields']" /></xsl:attribute>
    </input>

    <input type="hidden" name="searchcriteria">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SearchCriteria']" /></xsl:attribute>
    </input>

    <input type="hidden" name="CheckCount">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='CheckCount']" /></xsl:attribute>
    </input>

    <input type="hidden" name="CheckTotal">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='CheckTotal']" /></xsl:attribute>
    </input>
    <input type="hidden" name="txtSavedQueryDescription">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SavedQueryDescription']" /></xsl:attribute>
    </input>
    <input type="hidden" name="chkDefaultQuery">
      <xsl:attribute name="value"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='DefaultQuery']" /></xsl:attribute>
    </input>
    <input type="hidden" name="CheckImageDisplayMode">
      <xsl:attribute name="value"><xsl:value-of select="$CheckImageDisplayMode" /></xsl:attribute>
    </input>
    <input type="hidden" name="DocumentImageDisplayMode">
      <xsl:attribute name="value"><xsl:value-of select="$DocumentImageDisplayMode" /></xsl:attribute>
    </input>
    <br />
    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
      <tr>
        <td align="left" valign="top" colspan="2">
          Results
        </td>
      </tr>
      <tr>
        <td>
          &nbsp;
        </td>
        <td width="50%" class="formlabel" align="right">
          <a href="javascript:document.forms['frmLockBoxSearch'].txtAction.value='REFINE';InsertSecurityToken(document.forms['frmLockBoxSearch'].txtAction);document.forms['frmLockBoxSearch'].submit()">
            <strong>Refine Search</strong>
          </a>
        </td>
      </tr>
      <tr>
        <td align="left" valign="top" colspan="2">
          <table cellpadding="0" cellspacing="0" width="100%" border="0" class="grid">
            <tr>
              <td align="left" valign="top">
                <table cellpadding="5" cellspacing="0" width="100%" border="0">
                  <tr>
                    <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintMode']='True'">
                      <td class="grid-header grid-header-left" nowrap="nowrap">
                        <input type="checkbox" name="chkSelectAll" onclick="javascript:toggleAllSelectedItems(this);" />
                      </td>
                    </xsl:if>
                    <td class="grid-header grid-header-left" nowrap="nowrap">
                      <a href="#" onClick="ColumnSort('Batch.DepositDate', 'DepositDate');return false;">Deposit Date</a>
                    </td>
                    <xsl:if test="$DisplayBatchID='True'">
                      <td class="grid-header" nowrap="nowrap">
                        <a href="#" onClick="ColumnSort('Batch.BatchID', 'BatchID');return false;">
                          <xsl:call-template name="batchidlabel" />
                        </a>
                      </td>
                    </xsl:if>
                    <td class="grid-header" nowrap="nowrap">
                      <a href="#" onClick="ColumnSort('Batch.BatchNumber', 'BatchNumber');return false;">
                        <xsl:call-template name="batchnumberlabel" />
                      </a>
                    </td>
                    <td class="grid-header" nowrap="nowrap">
                      <a href="#" onClick="ColumnSort('Batch.PaymentSource', 'PaymentSource');return false;">Payment Source</a>
                    </td>
                    <td class="grid-header" nowrap="nowrap">
                      <a href="#" onClick="ColumnSort('Batch.PaymentType', 'PaymentType');return false;">Payment Type</a>
                    </td>
                    <td class="grid-header" nowrap="nowrap">
                      <a href="#" onClick="ColumnSort('Transactions.TxnSequence', 'TxnSequence');return false;">Transaction</a>
                    </td>

                    <td class="grid-header cell-center-align" >&nbsp;</td>

                    <xsl:choose>
                      <xsl:when test="/Page/Recordset[@Name='Columns']">
                        <xsl:for-each select="/Page/Recordset[@Name='Columns']/Record">
                          <td class="grid-header">
                            <xsl:choose>
                              <xsl:when test="concat(@TableName, @FieldName) != 'ChecksCheckSequence' and (@DataTypeEnum=$FldTypeFloat or @DataTypeEnum=$FldTypeCurrency)">
                                <xsl:attribute name="align">right</xsl:attribute>
                              </xsl:when>
                              <xsl:otherwise>
                                <xsl:attribute name="align">left</xsl:attribute>
                              </xsl:otherwise>
                            </xsl:choose>
                            <xsl:choose>
                              <xsl:when test="string-length(@DisplayName) &gt; 0">
                                <a href="#">
                                  <xsl:attribute name="onclick">ColumnSort('<xsl:value-of select="@TableName" />.<xsl:value-of select="@FieldName" />', '<xsl:value-of select="@DisplayName" />');return false;</xsl:attribute>
                                  <xsl:value-of select="@DisplayName" />
                                </a>
                              </xsl:when>
                              <xsl:otherwise>
                                &nbsp;
                              </xsl:otherwise>
                            </xsl:choose>
                          </td>
                        </xsl:for-each>
                      </xsl:when>
                      <xsl:otherwise>
                        <xsl:if test="/Page/Recordset[@Name='FormFields']/@COTSOnly!='True'">
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.BatchSequence', 'Payment Batch Sequence');return false;">Payment Batch Sequence</a>
                          </td>
                          <td class="grid-header cell-right-align" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.Amount', 'Amount');return false;">Payment Amount</a>
                          </td>
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.RT', 'RT');return false;">R/T</a>
                          </td>
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.Account', 'Account');return false;">Account Number</a>
                          </td>
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.Serial', 'Serial');return false;">Check/Trace/Ref Number</a>
                          </td>
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('DDA', 'DDA');return false;">DDA</a>
                          </td>
                          <td class="grid-header" nowrap="nowrap">
                            <a href="#" onClick="ColumnSort('Checks.Payer', 'Payer');return false;">Payer</a>
                          </td>
                        </xsl:if>
                      </xsl:otherwise>
                    </xsl:choose>
                  </tr>

                  <xsl:for-each select="/Page/Recordset[@Name='Results']/Record">
                    <xsl:variable name="curr"><xsl:value-of select="position()"/></xsl:variable>
                    <xsl:variable name="PDFLink">imagepdf.aspx?bank=<xsl:value-of select="Fld[@ID='BankID']" />&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']" />&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']" />&amp;batchnumber=<xsl:value-of select="Fld[@ID='BatchNumber']" />&amp;date=<xsl:value-of select="Fld[@ID='Deposit_Date']" />&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']" />&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']" />&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']" />&amp;type=c&amp;page=-1</xsl:variable>
                    <xsl:variable name="TransactionDetailLink">transactiondisplay.aspx?bank=<xsl:value-of select="Fld[@ID='BankID']" />&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']" />&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']" />&amp;date=<xsl:value-of select="Fld[@ID='Deposit_Date']" />&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']" />&amp;return=LockBoxSearch</xsl:variable>

                    <tr>
                      <xsl:if test='position() mod 2 = 0'>
                        <xsl:attribute name="class">evenrow</xsl:attribute>
                      </xsl:if>
                      <xsl:if test='position() mod 2 != 0'>
                        <xsl:attribute name="class">oddrow</xsl:attribute>
                      </xsl:if>
                      <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintMode']='True'">
                        <td class="grid-cell grid-cell-left" nowrap="nowrap">
                          <xsl:choose>
                            <xsl:when test="(sum(./Fld[@ID='ImageSize']) > 0) or (sum(./Fld[@ID='DocumentCount']) > 0)">
                              <input type="checkbox">
                                <xsl:attribute name="name">chkSelectivePrint~<xsl:value-of select="Fld[@ID='BankID']" />~<xsl:value-of select="Fld[@ID='ClientAccountID']" />~<xsl:value-of select="Fld[@ID='BatchID']" />~<xsl:value-of select="Fld[@ID='Deposit_Date']" />~<xsl:value-of select="Fld[@ID='TransactionID']" />~<xsl:value-of select="Fld[@ID='BatchNumber']" /></xsl:attribute>
                              </input>
                            </xsl:when>
                            <xsl:otherwise>
                              &nbsp;&nbsp;<img src="{$brandtemplatepath}/images/cbx_off_disabled.gif" />
                            </xsl:otherwise>
                          </xsl:choose>
                        </td>
                      </xsl:if>

                      <td class="grid-cell grid-cell-left">
                        <xsl:value-of select="Fld[@ID='Deposit_Date']" />
                      </td>
                      <xsl:if test="$DisplayBatchID='True'">
                        <td align="left" valign="top" class="grid-cell">
                          <xsl:value-of select="Fld[@ID='SourceBatchID']" />
                        </td>
                      </xsl:if>
                      <td class="grid-cell">
                        <xsl:variable name="bank">
                          <xsl:value-of select="Fld[@ID='BankID']"/>
                        </xsl:variable>
                        <xsl:variable name="lbx">
                          <xsl:value-of select="Fld[@ID='ClientAccountID']"/>
                        </xsl:variable>
                        <xsl:variable name="batch">
                          <xsl:value-of select="Fld[@ID='BatchID']"/>
                        </xsl:variable>
                        <xsl:variable name="date">
                          "<xsl:value-of select="Fld[@ID='Deposit_Date']"/>"
                        </xsl:variable>
                        <a class="showTip" title="Batch Detail" onclick="storeBatchDetailParams({$bank}, {$lbx}, {$batch}, {$date})">
                          <xsl:attribute name="href">remitdisplay.aspx?bank=<xsl:value-of select="Fld[@ID='BankID']" />&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']" />&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']" />&amp;date=<xsl:value-of select="Fld[@ID='Deposit_Date']" /></xsl:attribute>
                          <xsl:value-of select="Fld[@ID='BatchNumber']" />
                        </a>
                      </td>
                      <td align="left" valign="top" class="grid-cell">
                        <xsl:value-of select="Fld[@ID='PaymentSource']" />
                      </td>
                      <td align="left" valign="top" class="grid-cell">
                        <xsl:value-of select="Fld[@ID='PaymentType']" />
                      </td>

                      <td class="grid-cell">
                        <xsl:variable name="bank">
                          <xsl:value-of select="Fld[@ID='BankID']"/>
                        </xsl:variable>
                        <xsl:variable name="lbx">
                          <xsl:value-of select="Fld[@ID='ClientAccountID']"/>
                        </xsl:variable>
                        <xsl:variable name="batch">
                          <xsl:value-of select="Fld[@ID='BatchID']"/>
                        </xsl:variable>
                        <xsl:variable name="date">
                          "<xsl:value-of select="Fld[@ID='Deposit_Date']"/>"
                        </xsl:variable>
                        <xsl:variable name="transactionid">
                          "<xsl:value-of select="Fld[@ID='TransactionID']"/>"
                        </xsl:variable>
                        <xsl:variable name="transactionsequence">
                          "<xsl:value-of select="Fld[@ID='TxnSequence']"/>"
                        </xsl:variable>
                        <a class="showTip" title="Transaction Detail" onclick="storeTransactionDetailParams({$bank}, {$lbx}, {$batch}, {$date}, {$transactionid}, {$transactionsequence})">
                          <xsl:attribute name="href"><xsl:value-of select="$TransactionDetailLink" /></xsl:attribute>
                          <xsl:value-of select="Fld[@ID='TxnSequence']" />
                        </a>
                      </td>

                      <td class="grid-cell">
                        <xsl:variable name="bank">
                          <xsl:value-of select="Fld[@ID='BankID']"/>
                        </xsl:variable>
                        <xsl:variable name="lbx">
                          <xsl:value-of select="Fld[@ID='ClientAccountID']"/>
                        </xsl:variable>
                        <xsl:variable name="batch">
                          <xsl:value-of select="Fld[@ID='BatchID']"/>
                        </xsl:variable>
                        <xsl:variable name="date">
                          "<xsl:value-of select="Fld[@ID='Deposit_Date']"/>"
                        </xsl:variable>
                        <xsl:variable name="transactionid">
                          "<xsl:value-of select="Fld[@ID='TransactionID']"/>"
                        </xsl:variable>
                        <xsl:variable name="transactionsequence">
                          "<xsl:value-of select="Fld[@ID='TxnSequence']"/>"
                        </xsl:variable>
                        
                        <img src="{$brandtemplatepath}/images/shim.gif" style="width: 5px; height: 10px;"/>
                        <xsl:choose>
                          <xsl:when test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='COTSOnly']='True'">
                          </xsl:when>
                          <xsl:when test="Fld[@ID='ImageSize'] &gt; 0">
                            <a class="showTip" title="Show Payment" style="font-size: 14px;padding: 5px;">
                              <xsl:attribute name="href"><xsl:value-of select="$PDFLink" /></xsl:attribute>
                              <xsl:attribute name="onClick">docView('<xsl:value-of select="$PDFLink" />');return false;</xsl:attribute>
                              <i class="fa fa-money fa-lg"></i>
                            </a>

                            <!--<img src="{$brandtemplatepath}/images/shim.gif" style="width: 5px; height: 10px;"/>-->
                          </xsl:when>
                        </xsl:choose>


                        <xsl:choose>
                          <xsl:when test="number(Fld[@ID='DocumentCount']) > 0">
                            <xsl:variable name="imageurl">bank=<xsl:value-of select="Fld[@ID='BankID']" />&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']" />&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']" />&amp;batchnumber=<xsl:value-of select="Fld[@ID='BatchNumber']" />&amp;date=<xsl:value-of select="Fld[@ID='Deposit_Date']" />&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']" />&amp;page=-1&amp;seq=<xsl:value-of select="./Fld[@ID='TxnSequence']"/>&amp;imagefilteroption=DocumentsOnly</xsl:variable>
                            <a class="showTip" style="font-size: 14px;padding: 5px;">
                              <xsl:attribute name="href">#</xsl:attribute>
                              <xsl:attribute name="title">View <xsl:value-of select="Fld[@ID='DocumentCount']" /> Transaction documents</xsl:attribute>
                              <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
                              <i class="fa fa-file-o fa-lg"></i>
                            </a>
                            <!--<img src="{$brandtemplatepath}/images/shim.gif" style="width: 5px; height: 10px;"/>-->
                          </xsl:when>
                        </xsl:choose>


                        <xsl:choose>
                          <xsl:when test="number(Fld[@ID='DocumentCount']) > 0 and number(Fld[@ID='ImageSize']) > 0">
                            <xsl:variable name="imageurl">bank=<xsl:value-of select="Fld[@ID='BankID']" />&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']" />&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']" />&amp;batchnumber=<xsl:value-of select="Fld[@ID='BatchNumber']" />&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']" />&amp;date=<xsl:value-of select="Fld[@ID='Deposit_Date']" />&amp;seq=<xsl:value-of select="./Fld[@ID='TxnSequence']"/></xsl:variable>
                            <a class="showTip" style="font-size: 14px;padding: 5px;">
                              <xsl:attribute name="href">#</xsl:attribute>
                              <xsl:attribute name="title">View All Images for Transaction</xsl:attribute>
                              <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
                              <i class="fa fa-picture-o fa-lg"></i>
                            </a>
                            <!--<img src="{$brandtemplatepath}/images/shim.gif" style="width: 5px; height: 10px;"/>-->
                          </xsl:when>
                        </xsl:choose>

                        <xsl:comment>Display OMR icon</xsl:comment>
                        <xsl:choose>
                          <xsl:when test="Fld[@ID='MarkSenseDocumentCount']>0">
                            <a title="Transaction contains mark sense data" onclick="storeTransactionDetailParams({$bank}, {$lbx}, {$batch}, {$date}, {$transactionid}, {$transactionsequence})">
                              <xsl:attribute name="href"><xsl:value-of select="$TransactionDetailLink" /></xsl:attribute>
                              <img border="0">
                                <xsl:attribute name="src"><xsl:value-of select="$brandtemplatepath"/>/Images/cbo_icon_mark_sense.gif</xsl:attribute>
                              </img>
                            </a>
                            <img src="{$brandtemplatepath}/images/shim.gif" style="width: 10px; height: 16px;"/>
                          </xsl:when>
                        </xsl:choose>

                      </td>

                      <xsl:choose>
                        <xsl:when test="count(/Page/Recordset[@Name='Columns']/Record) &gt; 0">
                          <xsl:for-each select="//Recordset[@Name = 'Columns']/Record">
                            <xsl:variable name="TableName"><xsl:value-of select="@TableName"/></xsl:variable>
                            <xsl:variable name="FieldName"><xsl:value-of select="@FieldName"/></xsl:variable>
                            <xsl:variable name="ReportTitle"><xsl:value-of select="@DisplayName"/></xsl:variable>
                            <xsl:variable name="fld"><xsl:value-of select="@ColumnName"/></xsl:variable>
                            <td class="grid-cell">
                              <xsl:if test="string-length(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]) &gt; 0">
                                <xsl:choose>
                                  <xsl:when test="number(@DataTypeEnum) = $FldTypeString or number(@DataTypeEnum) = $FldTypeDate or $fld = 'ChecksCheckSequence'">
                                    <xsl:attribute name="align">left</xsl:attribute><xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
                                  </xsl:when>
                                  <xsl:when test="number(@DataTypeEnum) = $FldTypeFloat">
                                    <xsl:attribute name="align">right</xsl:attribute><xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
                                  </xsl:when>
                                  <xsl:when test="number(@DataTypeEnum) = $FldTypeCurrency">
                                    <xsl:attribute name="align">right</xsl:attribute>$<xsl:value-of select="format-number(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld], '#,##0.00')"/>
                                  </xsl:when>
                                  <xsl:otherwise>
                                    <xsl:attribute name="align">left</xsl:attribute><xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
                                  </xsl:otherwise>
                                </xsl:choose>
                              </xsl:if>
                              <xsl:if test="string-length(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]) &lt;= 0">
                                &nbsp;
                              </xsl:if>
                            </td>
                          </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                          <xsl:if test="/Page/Recordset[@Name='FormFields']/@COTSOnly!='True'">
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='ChecksBatchSequence']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='ChecksBatchSequence']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <xsl:choose>
                              <xsl:when test="string-length(Fld[@ID='Amount']) &gt; 0">
                                <td class="grid-cell cell-right-align">
                                  $<xsl:value-of select="format-number(Fld[@ID='Amount'], '#,##0.00')" />
                                </td>
                              </xsl:when>
                              <xsl:otherwise>
                                <td class="grid-cell cell-right-align">&nbsp;</td>
                              </xsl:otherwise>
                            </xsl:choose>
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='RT']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='RT']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='AccountNumber']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='AccountNumber']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='SerialNumber']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='SerialNumber']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='DDA']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='DDA']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                            <td class="grid-cell">
                              <xsl:choose>
                                <xsl:when test="string-length(Fld[@ID='Payer']) &gt; 0">
                                  <xsl:value-of select="Fld[@ID='Payer']" />
                                </xsl:when>
                                <xsl:otherwise>
                                  &nbsp;
                                </xsl:otherwise>
                              </xsl:choose>
                            </td>
                          </xsl:if>
                        </xsl:otherwise>
                      </xsl:choose>
                    </tr>
                  </xsl:for-each>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <xsl:call-template name="PaginationLinks">
          <xsl:with-param name="controlname">document.forms['frmLockBoxSearch'].txtAction</xsl:with-param>
          <xsl:with-param name="startrecord">
            <xsl:value-of select="$startrecord" />
          </xsl:with-param>
          <xsl:with-param name="confirmationmsg">Items Selected while in Selective Print Mode will not be Retained when Changing Pages.\n\nDo you wish to continue?</xsl:with-param>
          <xsl:with-param name="confirmationscript">itemsAreSelected(document.forms['frmLockBoxSearch'])</xsl:with-param>
          <xsl:with-param name="colSpan">8</xsl:with-param>
        </xsl:call-template>
      </tr>
      <tr>
        <td colspan="8">&nbsp;</td>
      </tr>
      <tr>
        <td align="left" colspan="2">
          <input type="checkbox" name="chkSelectivePrintMode">
            <xsl:attribute name="onclick">document.forms['frmLockBoxSearch'].txtAction.value='changepagemode'; document.forms['frmLockBoxSearch'].txtStart.value=<xsl:value-of select="/Page/PageInfo/@StartRecord" />; InsertSecurityToken(document.forms['frmLockBoxSearch'].txtAction);document.forms['frmLockBoxSearch'].submit();</xsl:attribute>
            <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintMode']='True'">
              <xsl:attribute name="checked">1</xsl:attribute>
            </xsl:if>
          </input>
          - Selective Print Mode
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintMode']='True'">
            <xsl:choose>
              <xsl:when test="(sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='ImageSize']) > 0) and (sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='DocumentCount']) = 0)">
                <input type="hidden" name="rdoSelectivePrintImageOption" value="ChecksOnly" />
              </xsl:when>
              <xsl:when test="(sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='ImageSize']) = 0) and (sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='DocumentCount']) > 0)">
                <input type="hidden" name="rdoSelectivePrintImageOption" value="DocumentsOnly" />
              </xsl:when>
              <xsl:when test="(sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='ImageSize']) > 0) and (sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='DocumentCount']) > 0)">
                <input type="radio" name="rdoSelectivePrintImageOption" value="ChecksOnly">
                  <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintImageOption']='PrintChecks'">
                    <xsl:attribute name="checked">1</xsl:attribute>
                  </xsl:if>
                  &nbsp;Print Payments&nbsp;
                </input>
                <input type="radio" name="rdoSelectivePrintImageOption" value="DocumentsOnly">
                  <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintImageOption']='PrintDocuments'">
                    <xsl:attribute name="checked">1</xsl:attribute>
                  </xsl:if>
                  &nbsp;Print Documents&nbsp;
                </input>
                <input type="radio" name="rdoSelectivePrintImageOption" value="All">
                  <xsl:if test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintImageOption']='PrintAll'">
                    <xsl:attribute name="checked">1</xsl:attribute>
                  </xsl:if>
                  &nbsp;Print All&nbsp;
                </input>
              </xsl:when>
              <xsl:otherwise>
                <span class="errordetail">Search results contain 0 payments and 0 documents.</span>
              </xsl:otherwise>
            </xsl:choose>
          </xsl:if>
        </td>
      </tr>
      <xsl:if test="$IsPrintable=0">
        <tr>
          <td class="copyright">
            Print View unavailable. Narrow your search to enable.
          </td>
        </tr>
      </xsl:if>
      <tr>
        <td align="left" valign="bottom">
          <xsl:choose>
            <xsl:when test="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='SelectivePrintMode']='True'">
              <xsl:if test="(sum(/Page/Recordset[@Name='Results']/Record//Fld[@ID='ImageSize']) > 0) or (sum(/Page/Recordset[@Name='Results']/Record/Fld[@ID='DocumentCount']) > 0)">
                <input type="button" name="cmdViewSelected" value="View Selected" onclick="AlternateDisplay(this, 'IPO_IMAGE_JOB_TYPE_VIEW_SELECTED');" />
              </xsl:if>
            </xsl:when>

            <xsl:otherwise>
              <nobr>
                <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='AdvanceSearchDownload']">
                  <input type="button" class="btn btn-primary" name="cmdPrintHtml" value="Print View" onclick="AlternateDisplay(this, 'IPO_IMAGE_JOB_TYPE_PRINT_HTML');">
                    <xsl:if test ="$IsPrintable=0">
                      <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                  </input>&nbsp;
                  <input type="button" class="btn btn-primary" name="cmdPrintPdf" value="PDF View" onclick="AlternateDisplay(this, 'PDF');" >
                    <xsl:if test ="$IsPrintable=0">
                      <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                  </input>&nbsp;
                  <input type="button" class="btn btn-primary" name="cmdDownload" value="Download As Text" onclick="AlternateDisplay(this, 'IPO_IMAGE_JOB_TYPE_CSV');" >
                    <xsl:if test ="$IsPrintable=0">
                      <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                  </input>&nbsp;
                  <input type="button" class="btn btn-primary" name="cmdDownloadImages" value="Download Images" onclick="AlternateDisplay(this, 'ZIP');">
                    <xsl:if test ="$IsPrintable=0">
                      <xsl:attribute name="disabled">disabled</xsl:attribute>
                    </xsl:if>
                  </input>&nbsp;
                </xsl:if>
              </nobr>
            </xsl:otherwise>

          </xsl:choose>
        </td>
        <td align="right" valign="bottom" class="pagelinks">
          <br />

        </td>
      </tr>
      <tr>
        <td align="left" valign="top" colspan="2">
          <table cellpadding="0" cellspacing="0" border="0">
            <xsl:apply-templates select="/Page/Recordset[@Name = 'SearchCriteria']/Record" mode="SearchCriteria"/>
          </table>
        </td>
      </tr>
    </table>
        
    <script language="javascript">
      <xsl:comment>

        function storeBatchDetailParams(bank, lbx, batch, date) {

          var parameters = {
            BankId: bank,
            WorkgroupId: lbx,
            BatchId: batch,
            DepositDateString: date,
            Source: "Advanced Search"
          };

          window.localStorage.setItem("batchdetailparams", JSON.stringify(parameters));
          window.localStorage.setItem("breadcrumb_local_storage", JSON.stringify([{Name: "Advanced Search", URL: "/IPOnline/Scripts/lockboxsearch.aspx?id=bc&amp;Action=search", Data: null }]));
          return true;
        }

        function storeTransactionDetailParams(bank, lbx, batch, date, tid, tseq) {

          var parameters = {
            BankId: bank,
            WorkgroupId: lbx,
            BatchId: batch,
            DepositDateString: date,
            TransactionID: tid,
            TransactionSequence: tseq
          };

          window.localStorage.setItem("transactiondetailparams", JSON.stringify(parameters));
          window.localStorage.setItem("breadcrumb_local_storage", JSON.stringify([{Name: "Advanced Search", URL: "/IPOnline/Scripts/lockboxsearch.aspx?id=bc&amp;Action=search", Data: null }]));
          return true;
        }


        //Hide script from older browsers
        //Set form field validation requirements
        function ColumnSort(FieldName, DisplayName){
        var SortField = '';
        var SortDisplayName = '';
        var SortDir = '';

        var CurrSortField = '';
        var CurrSortDisplayName = '';

        var iPos;
        var strTemp;

        CurrSortField = document.forms['frmLockBoxSearch'].selSortBy.value;

        iPos = CurrSortField.indexOf('~');
        SortField = CurrSortField.substring(0, iPos);
        strTemp = CurrSortField.substring(iPos+1);
        SortDisplayName = strTemp.substring(0, strTemp.indexOf('~'));
        SortDir = strTemp.substring(strTemp.indexOf('~') + 1);

        if(FieldName.toLowerCase() == SortField.toLowerCase() &amp;&amp; SortDisplayName.toLowerCase() == DisplayName.toLowerCase()){
        switch(SortDir.toLowerCase()){
        case 'ascending':
        SortDir = 'DESCENDING';
        break;
        case 'descending':
        SortDir = 'ASCENDING';
        break;
        default:
        SortDir = 'ASCENDING';
        }
        }else{
        SortField = FieldName;
        SortDisplayName = DisplayName;
        SortDir = 'ASCENDING';
        }

        CurrSortField = SortField + '~' + SortDisplayName + '~' + SortDir

        document.forms['frmLockBoxSearch'].selSortBy.value = CurrSortField;
        document.forms['frmLockBoxSearch'].txtAction.value = 'search';
        InsertSecurityToken(document.forms['frmLockBoxSearch'].txtAction);
        document.forms['frmLockBoxSearch'].submit();
        }

        function AlternateDisplay(obj, jobType){
        var frm;

        if(obj == null){
        return;
        }else{
        frm = obj.form;
        }


        // Pdf
        if(obj.name.toLowerCase() == 'cmdprintpdf'){
        frm.txtAction.value = 'searchResultsJobRequest';
        // Html
        }else if(obj.name.toLowerCase() == 'cmdprinthtml'){
        frm.txtAction.value = 'searchResultsJobRequest';
        // Text
        }else if(obj.name.toLowerCase() == 'cmddownload'){
        frm.txtAction.value = 'searchResultsJobRequest';
        frm.target = ""
        // Zip
        }else if(obj.name.toLowerCase() == 'cmddownloadimages'){
        frm.txtAction.value = 'searchResultsJobRequest';
        // View Selected
        }else if(obj.name.toLowerCase() == 'cmdviewselected'){
        if(itemsAreSelected(frm) == true) {
        frm.txtAction.value = 'searchResultsJobRequest';
        } else {
        frm.target = "";
        frm.action = "";
        alert('No items have been selected.');
        return;
        }
        }else{
        frm.target = "";
        frm.action = "";
        return;
        }

        var option = 'toolbar=0,location=0,directories=0,status=0,menubar=0,scrollbars=1,resizable=1,width=' + screen.availWidth * .75 + ',height=' + screen.availHeight * .75 + ',top=0,left=0';
        if(obj.name.toLowerCase() != 'cmddownload'){
        window.open('', 'processor_window', option);
        frm.target ='processor_window'
        }

        frm.action = 'lockboxsearch_alternate.aspx?JOB_TYPE=' + jobType;
        InsertSecurityToken(frm.txtAction);
        frm.submit()
        frm.target = "";
        frm.action = "";

        return;
        }

        function itemsAreSelected(frm) {

        var bolRetVal = false;

        for(var i=0;i&lt;frm.elements.length;++i)
        {
        if (frm.elements[i].name.substr(0,18) == 'chkSelectivePrint~')
        {
        if (frm.elements[i].checked)
        {
        bolRetVal = true;
        break;
        }
        }
        }

        return bolRetVal;
        }

        function toggleAllSelectedItems(ctl) {
        for(var i=0;i&lt;ctl.form.elements.length;++i)
        {
        if (ctl.form.elements[i].name.substr(0,18) == 'chkSelectivePrint~')
        {
        ctl.form.elements[i].checked = ctl.checked
        }
        }
        }
        //End Hiding

        $(document).ready(function() {
        $('.account-heading').html('Workgroup: ' + $('#txtWorkgroupSelectionLabel').val());
        $('.showTip').tooltip();
        });
      </xsl:comment>
    </script>

  </xsl:template>

  <xsl:template match="Record" mode="SearchCriteria">
    <tr>
      <td align="left" valign="top"  nowrap="nowrap">
        <xsl:value-of select="@DisplayTitle"/>:&nbsp;
      </td>
      <td align="left" valign="top" >
        <xsl:value-of select="@DisplayText"/>
      </td>
    </tr>
  </xsl:template>

</xsl:stylesheet>
