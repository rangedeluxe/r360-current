<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "&#169;">
	<!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incCustomBranding.xsl"/>

<xsl:output method="html"/>
<xsl:decimal-format name="currency" NaN="" />

<xsl:template name="Title">
	Transaction Detail
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>

<xsl:template match="Page">
	<xsl:if test="Recordset[@Name='TransactionDetail']">
		<xsl:apply-templates select="Recordset[@Name='TransactionDetail']/Record" mode="TransactionDetail"/>
	</xsl:if>
</xsl:template>

<xsl:template match="Record" mode="TransactionDetail">
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
		</head>
		<body>
   <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/DisplayBatchID/@DisplayBatchID"/></xsl:variable>
			<div id="container">
				<div id="header">
					<h1>
						<p/>
						Transaction Detail
					</h1>
				</div>
				<div id="subheader">
					<h1>
	  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">
	   BankID:&nbsp;<xsl:value-of select="./Fld[@ID='BankID']"/><br/>
	  </xsl:if>
            Workgroup:&nbsp;<xsl:value-of select="/Page/WorkgroupDisplayName"/><br/>
						Deposit Date:&nbsp;<xsl:value-of select="./Fld[@ID='DepositDate']"/><br/>
	  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
	   Account Site Code:&nbsp;<xsl:value-of select="./Fld[@ID='SiteCodeID']"/><br/>
	  </xsl:if>
	  <xsl:if test="$displaybatchid = 'True'">
			 <xsl:call-template name="batchidlabel" />:&nbsp;<xsl:value-of select="./Fld[@ID='SourceBatchID']"/><br/>
	  </xsl:if>
	  <xsl:call-template name="batchnumberlabel" />:&nbsp;<xsl:value-of select="./Fld[@ID='BatchNumber']"/><br/>
	  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
	   Batch Site Code:&nbsp;<xsl:value-of select="./Fld[@ID='BatchSiteCode']"/><br/>
	  </xsl:if>
	  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
			<xsl:call-template name="batchcueidlabel" />:&nbsp;
		<xsl:choose>
			<xsl:when test="./Fld[@ID='BatchCueID']='-1'">&nbsp;</xsl:when>
			<xsl:otherwise><xsl:value-of select="./Fld[@ID='BatchCueID']"/></xsl:otherwise>
		</xsl:choose>
			<br/>
		</xsl:if>
	  Transaction:&nbsp;<xsl:value-of select="./Fld[@ID='TxnSequence']"/>
					</h1>
				</div>
				<div id="content">
					<xsl:call-template name="ChecksSection" />
					<br/>
					<xsl:if test="Recordset[@Name='Stubs']">
						<xsl:apply-templates select="Recordset[@Name='Stubs']" mode="Stubs"/>
					</xsl:if>
					<br/>
					<xsl:if test="Recordset[@Name='Documents']">
						<xsl:apply-templates select="Recordset[@Name='Documents']" mode="Documents"/>
					</xsl:if>
				</div>
			</div>
		</body>
	</html>

</xsl:template>

<xsl:template name="ChecksSection">

	<xsl:variable name="TransactionID"><xsl:value-of select="Fld[@ID='TransactionID']"/></xsl:variable>

	<h1>Payments</h1>
	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" class="reportdata">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
	  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
	   <td align="left" class="reportheading">Payment Sequence</td>
	  </xsl:if>
	  <td align="left" class="reportheading">R/T</td>
						<td align="left" class="reportheading">Account Number</td>
						<td align="left" class="reportheading">Check/Trace/Ref Number</td>
						<td align="left" class="reportheading">Payer</td>
						<td align="left" class="reportheading">DDA</td>
						<td align="right" class="reportheading">Payment Amount</td>
						<td align="left" class="reportheading">&nbsp;</td>
					</tr>
					<xsl:if test="Recordset[@Name='Checks']">
						<xsl:apply-templates select="Recordset[@Name='Checks']" mode="Checks"/>
					</xsl:if>
				</table>
			</td>
		</tr>
	</table>

</xsl:template>

<xsl:template match="Recordset" mode="Checks">
	<xsl:variable name="TransactionID"><xsl:value-of select="../Fld[@ID='TransactionID']"/></xsl:variable>
	<xsl:choose>
		<xsl:when test="count(Record) &gt; 0">

			<xsl:for-each select="Record">
				<xsl:variable name="imageurl">bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;page=-1</xsl:variable>
				<xsl:variable name="addremitterurl">lckbx=<xsl:value-of select="../../Fld[@ID='OLLockboxID']"/>&amp;bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;rt=<xsl:value-of select="Fld[@ID='RT']"/>&amp;acct=<xsl:value-of select="Fld[@ID='Account']"/></xsl:variable>

				<tr>
					<xsl:if test='position() mod 2 = 0'>
						<xsl:attribute name="class">evenrow</xsl:attribute>
					</xsl:if>
					<xsl:if test='position() mod 2 != 0'>
						<xsl:attribute name="class">oddrow</xsl:attribute>
					</xsl:if>
	 <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
	  <td align="left" valign="top" class="reportdata">
	   <xsl:choose>
		<xsl:when test="string-length(./Fld[@ID='CheckSequence']) &gt; 0">
		 <xsl:value-of select="./Fld[@ID='CheckSequence']"/>
		</xsl:when>
		<xsl:otherwise>
		 &nbsp;
		</xsl:otherwise>
	   </xsl:choose>
	  </td>
	 </xsl:if>
	 <td align="left" valign="top" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='RT']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='RT']"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="left" valign="top" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Account']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='Account']"/>
							</xsl:when>
								<xsl:otherwise>
									&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="left" valign="top" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Serial']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='Serial']"/>
							</xsl:when>
								<xsl:otherwise>
									&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="left" valign="top" class="reportdata">
						<xsl:value-of select="./Fld[@ID='RemitterName']"/>
					</td>
					<td align="left" valign="top" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='DDA']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='DDA']"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="right" valign="top" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Amount']) &gt; 0">
								<xsl:value-of select="format-number(./Fld[@ID='Amount'], '$#,##0.00', 'currency')"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td align="left" valign="top" class="reportdata">&nbsp;</td>
				</tr>

				<xsl:if test="Recordset[@Name='ChecksDataEntry']">
					<tr>
						<xsl:if test='position() mod 2 = 0'>
							<xsl:attribute name="class">evenrow</xsl:attribute>
						</xsl:if>
						<xsl:if test='position() mod 2 != 0'>
							<xsl:attribute name="class">oddrow</xsl:attribute>
						</xsl:if>

						<td align="left" colspan="7" class="reportdata">
							<xsl:apply-templates select="Recordset[@Name='ChecksDataEntry']" mode="ChecksDataEntry"/>
						</td>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<tr>
				<td align="left" colspan="6" class="reportdata">
					<img src="{$brandtemplatepath}/images/sm_information.gif" border="0" alt="Information"/>&nbsp;There are no Checks for this transaction.
				</td>
			</tr>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

  <xsl:template match="Recordset" mode="ChecksDataEntry">
	<xsl:for-each select="Record">
		<ul>
			<xsl:for-each select="./Fld">
				<li>
					<b><xsl:value-of select="@Title"/>:</b>&nbsp;
					<xsl:choose>
						<xsl:when test="number(@Type) = 7 and string-length(.) &gt; 0">
							<xsl:value-of select="format-number(., '$#,##0.00', 'currency')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="Stubs">
	<h1>Data Entry</h1>
	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" class="reportdata">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<xsl:choose>
						<xsl:when test="count(/Page/Recordset[@Name='StubsColumns']/Record) &gt; 0">
							<tr>
								<xsl:for-each select="/Page/Recordset[@Name='StubsColumns']/Record">
									<xsl:choose>
										<xsl:when test="number(@Type) = 6 or number(@Type) = 7">
											<td align="right" class="reportheading">
												<xsl:value-of select="@Title"/>
											</td>
										</xsl:when>
										<xsl:otherwise>
											<td align="left" class="reportheading">
												<xsl:value-of select="@Title"/>
											</td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<td align="left" class="reportheading"></td>
								<xsl:if test="./Record/Fld[@ID='SystemType']='2'">
									<td align="center" class="reportheading">
										OMR
									</td>
								</xsl:if>
							</tr>

							<xsl:choose>
								<xsl:when test="count(./Record) &gt; 0">
									<xsl:for-each select="./Record">
										<tr>
											<xsl:if test='position() mod 2 = 0'>
												<xsl:attribute name="class">evenrow</xsl:attribute>
											</xsl:if>
											<xsl:if test='position() mod 2 != 0'>
												<xsl:attribute name="class">oddrow</xsl:attribute>
											</xsl:if>
											<xsl:if test="Recordset[@Name='StubsDataEntry']">
												<xsl:apply-templates select="Recordset[@Name='StubsDataEntry']" mode="StubsDataEntry"/>
											</xsl:if>
											<td align="left">
												<xsl:apply-templates select="Recordset[@Name='StubSource']" mode="StubSource"/>
											</td>
											<xsl:if test="Fld[@ID='SystemType']='2'">
												<td align="center">
													<xsl:if test="Fld[@ID='IsOMRDetected']='True'">
														<img src="{$brandtemplatepath}/Images/cbo_icon_mark_sense.gif" border="0" alt="Mark Sense was detected"/>
													</xsl:if>
												</td>
											</xsl:if>
										</tr>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<tr>
										<td align="left" class="reportdata">
											<xsl:attribute name="colspan"><xsl:value-of select="count(/Page/Recordset[@Name='StubsColumns']/Record)"/></xsl:attribute>
											<img src="{$brandtemplatepath}/images/sm_information.gif" border="0" alt="Information"/>&nbsp;There is no Data Entry for this transaction.
										</td>
									</tr>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td class="reportheading">&nbsp;</td>
							</tr>
							<tr>
								<td align="left" class="reportdata">
									<img src="{$brandtemplatepath}/images/sm_information.gif" border="0" alt="Information"/>&nbsp;Data Entry has not been configured for this transaction.
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
			</td>
		</tr>
	</table>
</xsl:template>

<xsl:template match="Recordset" mode="StubsDataEntry">
	<xsl:for-each select="Record">
		<xsl:for-each select="Fld">
			<xsl:choose>
				<xsl:when test="number(@Type) = 1">
					<td align="left" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:when test="number(@Type) = 6">
					<td align="right" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:when test="number(@Type) = 7">
					<td align="right" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="format-number(., '$#,##0.00', 'currency')"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:otherwise>
					<td align="left" class="reportdata">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="StubSource">
	<xsl:for-each select="Record">
		&nbsp;
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="Documents">
	<h1>Documents</h1>
	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" class="reportdata">
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td align="left" class="reportheading">Document</td>
						<td align="left" class="reportheading">Description</td>
						<td width="66%" align="left" class="reportheading">&nbsp;</td>
					</tr>
					<xsl:choose>
						<xsl:when test="count(Record) &gt; 0">
							<xsl:for-each select="Record">
								<tr>
									<xsl:if test='position() mod 2 = 0'>
										<xsl:attribute name="class">evenrow</xsl:attribute>
									</xsl:if>
									<xsl:if test='position() mod 2 != 0'>
										<xsl:attribute name="class">oddrow</xsl:attribute>
									</xsl:if>
									<td align="left" class="reportdata"><xsl:value-of select="number(../@StartRecord) + position() - 1"/></td>
									<td align="left" class="reportdata"><xsl:value-of select="Fld[@ID='Description']"/></td>
									<td align="left" class="reportdata">&nbsp;</td>
								</tr>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td align="left" colspan="3" class="reportdata">
									<img src="{$brandtemplatepath}/images/sm_information.gif" border="0" alt="Information"/>&nbsp;There are no Documents for this transaction.
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
			</td>
		</tr>
	</table>
</xsl:template>



</xsl:stylesheet>
