<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "&#169;">
	<!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:import href="incCommon.xsl"/>

<xsl:output method="html"/>
<xsl:decimal-format name="currency" NaN="" />

<xsl:template name="Title">
	Transaction Detail
</xsl:template>

<xsl:template name="PageTitle">

	<span class="contenttitle">
		<xsl:call-template name="BreadCrumbs" />
	</span>

	<span class="contenttitle">
		Transaction Detail
	</span>

</xsl:template>

<xsl:template match="Page">
	<link rel="stylesheet" href="/Assets/iviewer/jquery.iviewer.css" />
	<script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/assignPayer.js" />
	<script type="text/javascript" src="/Assets/iviewer/jquery.iviewer.js"></script>
	<script type="text/javascript" src="/Assets/wfs/wfs-imageviewer.js"></script>
	<div class="page-portlet-view">
		<xsl:if test="Recordset[@Name='TransactionDetail']">
			<form name="frmTransactionDetail" onSubmit="return false;" method="post">
				<input type="hidden" name="txtAction" value="" />
				<xsl:apply-templates select="Recordset[@Name='TransactionDetail']/Record" mode="TransactionDetail"/>

				<script language="javascript">
					<xsl:comment>
						//Hide script from older browsers
						//Set form field validation requirements
						function validate_form(ctl, thisform) {

						var TxnIDArray = new Array(<xsl:value-of select="count(/Page/Recordset[@Name='Transactions']/Record)"/>);
						<xsl:for-each select="/Page/Recordset[@Name='Transactions']/Record">
							TxnIDArray[<xsl:value-of select="position() -1"/>] = ['<xsl:value-of select="@TxnSequence"/>', '<xsl:value-of select="@TransactionID"/>'];
						</xsl:for-each>

						var bolRetVal = false;

						with(thisform) {
						if(elements["txtTxnJump"]) {
						for(var i=0;i&lt;<xsl:value-of select="count(/Page/Recordset[@Name='Transactions']/Record)"/>;++i) {
						if(elements["txtTxnJump"].value == TxnIDArray[i][0]) {
						elements["selTxnJump"].value = TxnIDArray[i][1];
						bolRetVal = true;
						}
						}
						}

						if(bolRetVal) {
						formSubmit(ctl, 'txtAction')
						} else {
						alert('Please enter a transaction number between ' + TxnIDArray[0][0] + ' and ' + TxnIDArray[<xsl:value-of select="count(/Page/Recordset[@Name='Transactions']/Record) - 1"/>][0]);
						}
						}
						}
						<xsl:text>
						$(document).ready(function() {
						$('.showTip').tooltip();
						payerPopup = new assignPayer('</xsl:text><xsl:value-of select="normalize-space($PayerFrameworkURL)"/><xsl:text>');


            //This removes the tooltip whenever a click occurs, also
            //it prevents the tooltip from returning whenever the main window
            //gets focus back (this was actually an issue).
            $('.showTip').on('click', function(){
              $(this).tooltip('hide');
		          $(this).one('focus',function(){
                  this.blur();
                });
            });




						});
						</xsl:text>
					</xsl:comment>
				</script>

			</form>
		</xsl:if>
	</div>
</xsl:template>


  <xsl:variable name="displayreporticon">
	<xsl:choose>
	  <xsl:when test="/Page/UserInfo/Permissions/Permission[@Name='ImageRPSAuditReport']">
		<xsl:value-of select = '1'/>
	  </xsl:when>
	  <xsl:otherwise>
		<xsl:value-of select = '0'/>
	  </xsl:otherwise>
	</xsl:choose>
  </xsl:variable>
  <xsl:variable name="importtypekey">
	<xsl:value-of select = "/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='ImportTypeKey']"/>
  </xsl:variable>

<xsl:template match="Record" mode="TransactionDetail">

 <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/DisplayBatchID/@DisplayBatchID"/></xsl:variable>
	<input type="hidden" name="hidBankID"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='BankID']"/></xsl:attribute></input>
	<input type="hidden" name="hidLockboxID"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='LockboxID']"/></xsl:attribute></input>
	<input type="hidden" name="hidDepositDate"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='DepositDate']"/></xsl:attribute></input>
	<input type="hidden" name="hidBatchID"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='BatchID']"/></xsl:attribute></input>
	<input type="hidden" name="hidTransactionID"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='TransactionID']"/></xsl:attribute></input>
	<input type="hidden" name="hidTxnSequence"><xsl:attribute name="value"><xsl:value-of select="./Fld[@ID='TxnSequence']"/></xsl:attribute></input>

	<table width="98%" align="center" cellpadding="5" cellspacing="0" border="0" onMouseover="popMenu();">
		<tr>
			<td align="left">
				<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">
						BankID:&nbsp;<xsl:value-of select="./Fld[@ID='BankID']"/><br />
				</xsl:if>
		Workgroup:&nbsp;<xsl:value-of select="/Page/WorkgroupDisplayName"/><br />
					Deposit Date:&nbsp;<xsl:value-of select="./Fld[@ID='DepositDate']"/><br />
				<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
						Account Site Code:&nbsp;<xsl:value-of select="./Fld[@ID='SiteCodeID']"/><br />
				</xsl:if>
				<xsl:if test="$displaybatchid = 'True'">
						<xsl:call-template name="batchidlabel" />:&nbsp;<xsl:value-of select="./Fld[@ID='SourceBatchID']"/><br />
				</xsl:if>
				<xsl:call-template name="batchnumberlabel" />:&nbsp;<xsl:value-of select="./Fld[@ID='BatchNumber']"/><br />
				<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
						Batch Site Code:&nbsp;<xsl:value-of select="./Fld[@ID='BatchSiteCode']"/><br />
				</xsl:if>
				<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
						<xsl:call-template name="batchcueidlabel" />:&nbsp;
						<xsl:choose>
							<xsl:when test="./Fld[@ID='BatchCueID']='-1'">&nbsp;</xsl:when>
							<xsl:otherwise>
								<xsl:value-of select="./Fld[@ID='BatchCueID']"/>
							</xsl:otherwise>
						</xsl:choose>
					<br />
				</xsl:if>
					Transaction:&nbsp;<xsl:value-of select="./Fld[@ID='TxnSequence']"/>
			</td>
		</tr>
	</table>

	<xsl:call-template name="ChecksSection" />

	<xsl:if test="Recordset[@Name='Stubs']">
		<xsl:apply-templates select="Recordset[@Name='Stubs']" mode="Stubs"/>
	</xsl:if>

	<xsl:if test="Recordset[@Name='Documents']">
		<xsl:apply-templates select="Recordset[@Name='Documents']" mode="Documents"/>
	</xsl:if>

	<p/>

	<xsl:variable name="ChecksSize"><xsl:value-of select="sum(Recordset[@Name='Checks']/Record/Fld[@ID='ImageSize'])"/></xsl:variable>
	<xsl:variable name="DocsSize"><xsl:value-of select="sum(Recordset[@Name='Documents']/Record/Fld[@ID='ImageSize'])"/></xsl:variable>

	<table align="center" width="98%" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<xsl:variable name="printviewurl">bank=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/PageInfo/@BankID"/>&amp;lbx=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/PageInfo/@LockboxID"/>&amp;batch=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/PageInfo/@BatchID"/>&amp;date=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/PageInfo/@DepositDate"/>&amp;txn=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/PageInfo/@TransactionID"/></xsl:variable>
				<a>
					<xsl:attribute name="href">JavaScript:newPopup('transactiondisplay.aspx?<xsl:value-of select="$printviewurl"/>&amp;printview=1');</xsl:attribute>
					<i class="fa fa-print" />&nbsp;Printer-Friendly Version
				</a>
			</td>
			<td>
				<xsl:if test="($ChecksSize > 0) or ($DocsSize > 0)">
					<div align="right" class="reportfooter">
						<xsl:apply-templates select="." mode="ViewAll"/>
					</div>
				</xsl:if>
			</td>
		</tr>
		<tr>
			<td style="height: 3px;">&nbsp;</td>
		</tr>
	</table>

</xsl:template>

<xsl:template name="ChecksSection">

	<xsl:variable name="TransactionID"><xsl:value-of select="Fld[@ID='TransactionID']"/></xsl:variable>

	<p/>
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td align="left" valign="bottom" class="hubLabel">Payment Items

						</td>

						<td align="right" valign="bottom">

							<xsl:variable name="ChecksSize"><xsl:value-of select="sum(Recordset[@Name='Checks']/Record/Fld[@ID='ImageSize'])"/></xsl:variable>
							<xsl:variable name="DocsSize"><xsl:value-of select="sum(Recordset[@Name='Documents']/Record/Fld[@ID='ImageSize'])"/></xsl:variable>

								<div align="right">
									<span style="display: inline;">Transaction:</span>&nbsp;
									<!-- Uncomment to use dropdown instead of textbox
									<select name="selTxnJump" onchange="form.submit();">
										<xsl:for-each select="/Page/Recordset[@Name='Transactions']/Record">
											<option>
												<xsl:attribute name="value"><xsl:value-of select="@TransactionID"/></xsl:attribute>
												<xsl:if test="@TransactionID = $TransactionID">
													<xsl:attribute name="selected">selected</xsl:attribute>
												</xsl:if>
												<xsl:value-of select="@TxnSequence"/>
											</option>
										</xsl:for-each>
									</select>
									<End Jump to Txn -->

									<!-- Comment to use dropdown instead of textbox -->
									<input type="text" name="txtTxnJump" size="3" maxlength="4" style="margin-top: 10px;">
										<xsl:attribute name="value"><xsl:value-of select="@TxnSequence"/></xsl:attribute>
									</input>
									&nbsp;
									<input type="button" class="btn btn-primary" value="Go" onClick="validate_form(this, this.form);" />
									<input type="hidden" name="selTxnJump" />
									<!-- End Jump to Txn -->

									&nbsp;&nbsp;&nbsp;&nbsp;

									<xsl:if test="($ChecksSize > 0) or ($DocsSize > 0)">
										<xsl:apply-templates select="." mode="ViewAll"/>
									</xsl:if>
								</div>
							<br />
						</td>
					</tr>
				</table>
			</td>
		</tr>


		<tr>
			<td align="left">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid">
					<tr>
						<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
							<td class="grid-header">Payment Sequence</td>
						</xsl:if>
						<td class="grid-header grid-header-left">R/T</td>
						<td class="grid-header">Account Number</td>
						<td class="grid-header">Check/Trace/Ref Number</td>
						<td class="grid-header">Payer</td>
						<td class="grid-header">DDA</td>
						<td class="grid-header cell-right-align">Payment Amount</td>
						<td class="grid-header">&nbsp;</td>
			<xsl:if test="$displayreporticon='1' and $importtypekey=2">
			  <td class="grid-header">
			  </td>
			</xsl:if>
					</tr>
					<xsl:if test="Recordset[@Name='Checks']">
						<xsl:apply-templates select="Recordset[@Name='Checks']" mode="Checks"/>
					</xsl:if>
				</table>
			</td>
		</tr>
	</table>

	<xsl:apply-templates select="Recordset[@Name='Checks']" mode="Navigation"/>

</xsl:template>

<xsl:template match="Recordset" mode="Checks">

	<xsl:variable name="TransactionID"><xsl:value-of select="../Fld[@ID='TransactionID']"/></xsl:variable>


	<xsl:choose>
		<xsl:when test="count(Record) &gt; 0">

			<xsl:for-each select="Record">
				<xsl:variable name="imageurl">bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchNumber']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;page=-1</xsl:variable>
				<xsl:variable name="addremitterurl">bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;rt=<xsl:value-of select="Fld[@ID='RT']"/>&amp;acct=<xsl:value-of select="Fld[@ID='Account']"/>&amp;srcBatchID=<xsl:value-of select="Fld[@ID='SourceBatchID']" />&amp;batchSourceShortName=<xsl:value-of select="Fld[@ID='BatchSourceShortName']"/>&amp;importTypeShortName=<xsl:value-of select="Fld[@ID='ImportTypeShortName']"/></xsl:variable>
		<xsl:variable name="reporturl">ReportName=ItemReport&amp;bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='LockboxID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;processingdate=<xsl:value-of select="Fld[@ID='ProcessingDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchNumber']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;page=-1</xsl:variable>
				<tr>
					<xsl:if test='position() mod 2 = 0'>
						<xsl:attribute name="class">evenrow</xsl:attribute>
					</xsl:if>
					<xsl:if test='position() mod 2 != 0'>
						<xsl:attribute name="class">oddrow</xsl:attribute>
					</xsl:if>
					<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
						<td class="grid-cell grid-cell-left">
							<xsl:choose>
								<xsl:when test="string-length(./Fld[@ID='CheckSequence']) &gt; 0">
									 <xsl:value-of select="./Fld[@ID='CheckSequence']"/>
								</xsl:when>
								<xsl:otherwise>
									&nbsp;
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</xsl:if>
					<td class="grid-cell grid-cell-left">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='RT']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='RT']"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Account']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='Account']"/>
							</xsl:when>
								<xsl:otherwise>
									&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Serial']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='Serial']"/>
							</xsl:when>
								<xsl:otherwise>
									&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell">
						<xsl:variable name="PaymentType">
							<xsl:call-template name="to-lower">
								<xsl:with-param name="Value">
									<xsl:value-of select="./Fld[@ID='PaymentType']"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>
						<xsl:variable name="PaymentSource">
							<xsl:call-template name="to-lower">
								<xsl:with-param name="Value">
									<xsl:value-of select="./Fld[@ID='PaymentSource']"/>
								</xsl:with-param>
							</xsl:call-template>
						</xsl:variable>
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='RemitterName']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='RemitterName']"/>
							</xsl:when>
							<xsl:when test="string-length(./Fld[@ID='RemitterName']) = 0 and (/Page/UserInfo/Permissions/Permission[@Name='AssignPayer' and @Mode='Manage'])">
								<a title="Add Payer to Payment"  class="openPayer">
									<xsl:attribute name="href">
										<xsl:value-of select="normalize-space($PayerFrameworkURL)"/>GetSuggestedPayer?<xsl:value-of select="$addremitterurl"/>
									</xsl:attribute>
									<i class="fa fa-plus-circle" />&nbsp;Payer
								</a>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='DDA']) &gt; 0">
								<xsl:value-of select="./Fld[@ID='DDA']"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell cell-right-align">
						<xsl:choose>
							<xsl:when test="string-length(./Fld[@ID='Amount']) &gt; 0">
								<xsl:value-of select="format-number(./Fld[@ID='Amount'], '$#,##0.00', 'currency')"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
					<td class="grid-cell cell-center-align">
						<xsl:choose>
							<xsl:when test="number(./Fld[@ID='ImageSize']) &gt; 0">
								<a title="View Payment Image">
									<xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
									<xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
									<i class="fa fa-money fa-lg" />
								</a>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
		  <xsl:if test="$displayreporticon='1' and $importtypekey=2">
			<td class="grid-cell cell-center-align">
			  <a class="showTip" title="Item Report" style="font-size: 14px;">
				<xsl:attribute name="href">
				  report.aspx?<xsl:value-of select="$reporturl"/>
				</xsl:attribute>
				<xsl:attribute name="onClick">
				  docView('report.aspx?<xsl:value-of select="$reporturl"/>');return false;
				</xsl:attribute>
          <span style="display: block;"><i class="fa fa-print"></i></span>
			  </a>
			</td>
		  </xsl:if>
		</tr>

				<xsl:if test="Recordset[@Name='ChecksDataEntry']">
					<tr>
						<xsl:choose>
							<xsl:when test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
								<td class="grid-cell">
									<xsl:apply-templates select="Recordset[@Name='ChecksDataEntry']" mode="ChecksDataEntry"/>
								</td>
							</xsl:when>
							<xsl:otherwise>
								<td class="grid-cell" colspan="8">
									<xsl:apply-templates select="Recordset[@Name='ChecksDataEntry']" mode="ChecksDataEntry"/>
								</td>
							</xsl:otherwise>
						</xsl:choose>
					</tr>
				</xsl:if>
			</xsl:for-each>
		</xsl:when>
		<xsl:otherwise>
			<tr>
				<xsl:choose>
					<xsl:when test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowCheckSequenceOnline' and @AppType='0' and @DefaultSettings='Y']">
						<td colspan="7" class="grid-cell grid-cell-left">
							<i class="icon-info-sign" />&nbsp;There are no Checks for this transaction.
						</td>
					</xsl:when>
					<xsl:otherwise>
						<td colspan="6" class="grid-cell grid-cell-left">
							<i class="icon-info-sign" />&nbsp;There are no Checks for this transaction.
						</td>
					</xsl:otherwise>
					</xsl:choose>
			</tr>
		</xsl:otherwise>
	</xsl:choose>

</xsl:template>

<xsl:template match="Recordset" mode="ChecksDataEntry">
	<xsl:for-each select="Record">
		<ul>
			<xsl:for-each select="./Fld">
				<li>
					<b><xsl:value-of select="@Title"/>:</b>&nbsp;
					<xsl:choose>
						<xsl:when test="number(@Type) = 7 and string-length(.) &gt; 0">
							<xsl:value-of select="format-number(., '$#,##0.00', 'currency')"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="."/>
						</xsl:otherwise>
					</xsl:choose>
				</li>
			</xsl:for-each>
		</ul>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="Stubs">

	<br />
	<br />

	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td align="left">Related Items
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid">
		  <xsl:variable name="BankID">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BankID']"/>
		  </xsl:variable>
		  <xsl:variable name="LockboxID">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='LockboxID']"/>
		  </xsl:variable>
		  <xsl:variable name="DepositDate">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='DepositDate']"/>
		  </xsl:variable>
		  <xsl:variable name="PICSDate">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='PICSDate']"/>
		  </xsl:variable>
		  <xsl:variable name="TransactionID">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='TransactionID']"/>
		  </xsl:variable>
		  <xsl:variable name="BatchID">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchID']"/>
		  </xsl:variable>
		  <xsl:variable name="ProcessingDate">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='ProcessingDate']"/>
		  </xsl:variable>
		  <xsl:variable name="BatchNumber">
			<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchNumber']"/>
		  </xsl:variable>
		  <xsl:variable name="reporturl">
			ReportName=ItemReport&amp;bank=<xsl:value-of select="$BankID" />&amp;lbx=<xsl:value-of select="$LockboxID" />&amp;date=<xsl:value-of select="$DepositDate" />&amp;picsdate=<xsl:value-of select="$PICSDate"/>&amp;processingdate=<xsl:value-of select="$ProcessingDate"/>&amp;txn=<xsl:value-of select="$TransactionID" />&amp;batch=<xsl:value-of select="$BatchID" />&amp;batchnumber=<xsl:value-of select="$BatchNumber" />&amp;type=i&amp;page=-1</xsl:variable>

		  <xsl:choose>
						<xsl:when test="count(/Page/Recordset[@Name='StubsColumns']/Record) &gt; 0">
							<tr>
								<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowTransactionStubSeqOnline' and @AppType='0' and @DefaultSettings='Y']">
									<td class="grid-header grid-header-left">Stub Sequence</td>
								</xsl:if>
								<xsl:for-each select="/Page/Recordset[@Name='StubsColumns']/Record">
									<xsl:choose>
										<xsl:when test="number(@Type) = 6 or number(@Type) = 7">
											<td  class="grid-header cell-right-align">
												<xsl:value-of select="@Title"/>
											</td>
										</xsl:when>
										<xsl:otherwise>
											<td class="grid-header grid-header-left">
												<xsl:value-of select="@Title"/>
											</td>
										</xsl:otherwise>
									</xsl:choose>
								</xsl:for-each>
								<td class="grid-header grid-header-left"></td>
								<xsl:if test="./Record/Fld[@ID='SystemType']='2'">
									<td class="grid-header cell-center-align">
										OMR
									</td>
								</xsl:if>
				<xsl:if test="$displayreporticon='1' and $importtypekey=2">
				  <td class="grid-header">
				  </td>
				</xsl:if>
							</tr>

							<xsl:choose>
								<xsl:when test="count(./Record) &gt; 0">
									<xsl:for-each select="./Record">
										<tr>
											<xsl:if test='position() mod 2 = 0'>
												<xsl:attribute name="class">evenrow grid-cell-left</xsl:attribute>
											</xsl:if>
											<xsl:if test='position() mod 2 != 0'>
												<xsl:attribute name="class">oddrow grid-cell-left</xsl:attribute>
											</xsl:if>
											<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowTransactionStubSeqOnline' and @AppType='0' and @DefaultSettings='Y']">
												<td class="grid-cell"><xsl:value-of select="Fld[@ID='StubSequence']"/></td>
											</xsl:if>

											<xsl:if test="Recordset[@Name='StubsDataEntry']">
												<xsl:apply-templates select="Recordset[@Name='StubsDataEntry']" mode="StubsDataEntry"/>
											</xsl:if>

											<td class="grid-cell">
												<xsl:apply-templates select="Recordset[@Name='StubSource']" mode="StubSource"/>
											</td>
											<xsl:if test="Fld[@ID='SystemType']='2'">
												<td class="grid-cell grid-cell-left">
													<xsl:if test="Fld[@ID='IsOMRDetected']='True'">
														<img src="{$brandtemplatepath}/Images/cbo_icon_mark_sense.gif" border="0" alt="Mark Sense was detected"/>
													</xsl:if>
												</td>
											</xsl:if>
					   <xsl:if test="$displayreporticon='1' and $importtypekey=2">

						  <td class="grid-cell cell-center-align">
							<a class="showTip" title="Item Report" style="font-size: 14px;">
							  <xsl:attribute name="href">
								report.aspx?<xsl:value-of select="$reporturl"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>
							  </xsl:attribute>
							  <xsl:attribute name="onClick">
                  <xsl:text>docView('report.aspx?</xsl:text>
                  <xsl:value-of select="normalize-space($reporturl)"/>
                  <xsl:text>&amp;item=</xsl:text>
                  <xsl:value-of select="normalize-space(Fld[@ID='BatchSequence'])"/>')
                  <xsl:text>;return false;</xsl:text>
                </xsl:attribute>
                <span style="display: block;"><i class="fa fa-print"></i></span>
							</a>
						  </td>
						</xsl:if>
										</tr>
									</xsl:for-each>
								</xsl:when>
								<xsl:otherwise>
									<tr>
										<td class="grid-cell grid-cell-left">
											<xsl:attribute name="colspan"><xsl:value-of select="count(/Page/Recordset[@Name='StubsColumns']/Record)"/></xsl:attribute>
											<img src="{$brandtemplatepath}/images/sm_information.gif" border="0" alt="Information"/>&nbsp;There is no Data Entry for this transaction.
										</td>
									</tr>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td class="grid-header grid-header-left">&nbsp;</td>
							</tr>
							<tr>
								<td  class="grid-cell grid-cell-left">
									<i class="icon-info-sign" />&nbsp;Data Entry has not been configured for this transaction.
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
			</td>
		</tr>
	</table>

	<xsl:apply-templates select="." mode="Navigation"/>
</xsl:template>

<xsl:template match="Recordset" mode="StubsDataEntry">
	<xsl:for-each select="Record">
		<xsl:for-each select="Fld">
			<xsl:choose>
				<xsl:when test="number(@Type) = 1">
					<td class="grid-cell">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:when test="number(@Type) = 6">
					<td class="grid-cell cell-right-align">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:when test="number(@Type) = 7">
					<td class="grid-cell cell-right-align">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="format-number(., '$#,##0.00', 'currency')"/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:when>
				<xsl:otherwise>
					<td class="grid-cell">
						<xsl:choose>
							<xsl:when test="string-length(.) &gt; 0">
								<xsl:value-of select="."/>
							</xsl:when>
							<xsl:otherwise>
								&nbsp;
							</xsl:otherwise>
						</xsl:choose>
					</td>
				</xsl:otherwise>
			</xsl:choose>
		</xsl:for-each>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="StubSource">
	<xsl:for-each select="Record">
 <xsl:variable name="imageurl">bank=<xsl:value-of select="./Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="./Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="./Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchNumber']"/>&amp;date=<xsl:value-of select="./Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="./Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="./Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="./Fld[@ID='BatchSequence']"/>&amp;page=-1</xsl:variable>

		<a>
			<xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
			<xsl:attribute name="title">View <xsl:value-of select="Fld[@ID='Description']"/></xsl:attribute>
			<xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
			<img src="{$brandtemplatepath}/Images/cbo_icon_document.gif" border="0"/>
		</a>&nbsp;
	</xsl:for-each>
</xsl:template>

<xsl:template match="Recordset" mode="Documents">
	<br />
	<br />
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0">
					<tr>
						<td align="left">Documents
						</td>
					</tr>
				</table>
			</td>
		</tr>

		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid">
					<tr>
						<td class="grid-header grid-header-left">Document</td>
						<td class="grid-header">Description</td>
						<td width="66%" class="grid-header">&nbsp;</td>
					</tr>
					<xsl:choose>
						<xsl:when test="count(Record) &gt; 0">
							<xsl:for-each select="Record">
								<xsl:variable name="imageurl">bank=<xsl:value-of select="./Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="./Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="./Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="/Page/Recordset[@Name='TransactionDetail']/Record/Fld[@ID='BatchNumber']"/>&amp;date=<xsl:value-of select="./Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="./Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="./Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="./Fld[@ID='BatchSequence']"/>&amp;page=-1</xsl:variable>
								<tr>
									<xsl:if test='position() mod 2 = 0'>
										<xsl:attribute name="class">evenrow</xsl:attribute>
									</xsl:if>
									<xsl:if test='position() mod 2 != 0'>
										<xsl:attribute name="class">oddrow</xsl:attribute>
									</xsl:if>
									<td class="grid-cell grid-cell-left"><xsl:value-of select="number(../@StartRecord) + position() - 1"/></td>
									<td class="grid-cell"><xsl:value-of select="Fld[@ID='Description']"/></td>
									<td class="grid-cell">
										<xsl:choose>
											<xsl:when test="number(Fld[@ID='ImageSize']) &gt; 0">
												<a>
													<xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
													<xsl:attribute name="title">View <xsl:value-of select="Fld[@ID='Description']"/></xsl:attribute>
													<xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
													<i class="fa fa-file-o fa-lg" />
												</a>
											</xsl:when>
											<xsl:otherwise>
												&nbsp;
											</xsl:otherwise>
										</xsl:choose>
									</td>
								</tr>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<tr>
								<td class="grid-cell grid-cell-left" align="left" colspan="3">
									<i class="icon-info-sign" />&nbsp;There are no Documents for this transaction.
								</td>
							</tr>
						</xsl:otherwise>
					</xsl:choose>
				</table>
			</td>
		</tr>
	</table>

	<xsl:apply-templates select="." mode="Navigation"/>
</xsl:template>

<xsl:template match="Recordset" mode="RecordCount">
	<xsl:if test="@StartRecord and @EndRecord and @TotalRecords">
		<div align="left" class="recordcount">
			Showing Results&nbsp;
			<xsl:value-of select="@StartRecord"/>&nbsp;-&nbsp;
			<xsl:value-of select="@EndRecord"/>&nbsp;of&nbsp;
			<xsl:value-of select="@TotalRecords"/>
		</div>
	</xsl:if>
</xsl:template>

<xsl:template match="Recordset" mode="Navigation">
	<xsl:variable name="checkstart"><xsl:value-of select="/Page/QueryString/CheckStartRecord"/></xsl:variable>
	<xsl:variable name="stubstart"><xsl:value-of select="/Page/QueryString/StubStartRecord"/></xsl:variable>
	<xsl:variable name="documentstart"><xsl:value-of select="/Page/QueryString/DocumentStartRecord"/></xsl:variable>
	<xsl:variable name="linkurl">transactiondisplay.aspx?bank=<xsl:value-of select="../Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="../Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="../Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="../Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="./Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="../Fld[@ID='TransactionID']"/></xsl:variable>

	<p/>

	<xsl:if test="@StartRecord and @DisplayRecords and @PreviousStartRecord and @NextStartRecord and @LastStartRecord and @TotalRecords">
		<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" style="margin-top: -10px;">
			<tr class="grid-pager" style="height: 10px;">
				<td>
					<span style="margin-left: 3px; dispay: inline-block;">
						<xsl:choose>
							<xsl:when test="number(@StartRecord) - number(@DisplayRecords) &gt;= 1">
								<xsl:choose>
									<xsl:when test="@Name='Checks'">
										<a title="View First Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;checkstart=1&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Previous Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;checkstart=<xsl:value-of select="@PreviousStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
									</xsl:when>

									<xsl:when test="@Name='Stubs'">
										<a title="View First Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;stubstart=1&amp;checkstart=<xsl:value-of select="$checkstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Previous Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;stubstart=<xsl:value-of select="@PreviousStartRecord"/>&amp;checkstart<xsl:value-of select="$checkstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
									</xsl:when>

									<xsl:when test="@Name='Documents'">
										<a title="View First Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;documentstart=1&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Previous Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;documentstart=<xsl:value-of select="@PreviousStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
									</xsl:when>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<i class="ui-icon ui-icon-seek-first ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
								<i class="ui-icon ui-icon-seek-prev ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
							</xsl:otherwise>
						</xsl:choose>

						<xsl:choose>
							<xsl:when test="number(@StartRecord) + (number(@DisplayRecords) - 1) &lt; number(@TotalRecords)">
								<xsl:choose>
									<xsl:when test="@Name='Checks'">
										<a title="View Next Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;checkstart=<xsl:value-of select="@NextStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Last Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;checkstart=<xsl:value-of select="@LastStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
									</xsl:when>

									<xsl:when test="@Name='Stubs'">
										<a title="View Next Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;stubstart=<xsl:value-of select="@NextStartRecord"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Last Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;stubstart=<xsl:value-of select="@LastStartRecord"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>&amp;documentstart=<xsl:value-of select="$documentstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										</xsl:when>

									<xsl:when test="@Name='Documents'">
										<a title="View Next Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;documentstart=<xsl:value-of select="@NextStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										<a title="View Last Page">
											<xsl:attribute name="href">
												<xsl:value-of select="$linkurl"/>&amp;documentstart=<xsl:value-of select="@LastStartRecord"/>&amp;stubstart=<xsl:value-of select="$stubstart"/>&amp;checkstart=<xsl:value-of select="$checkstart"/>
											</xsl:attribute>
											<i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
										</a>
										</xsl:when>
								</xsl:choose>
							</xsl:when>
							<xsl:otherwise>
								<i class="ui-icon ui-icon-seek-next ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
								<i class="ui-icon ui-icon-seek-end ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
							</xsl:otherwise>
						</xsl:choose>
					</span>

					<span style="dispay: inline-block; vertical-align: middle; font-size: 0.7em; margin-left: 5px;">
						<xsl:if test="../../PageInfo">
							Showing Results&nbsp;
							<xsl:value-of select="@StartRecord"/>&nbsp;-&nbsp;
							<xsl:value-of select="@EndRecord"/>&nbsp;of&nbsp;
							<xsl:value-of select="@TotalRecords"/>
						</xsl:if>
					</span>
					<!--<div style="display: inline-block; float: right; vertical-align: middle; margin-right: 5px; margin-top: 3px;">
						<i class="ui-icon ui-icon-lightbulb ui-state-disabled" style="border: 1px solid gray" ></i>
					</div>-->
				</td>
			</tr>
		</table>
	</xsl:if>
</xsl:template>

<xsl:template match="Record" mode="ViewAll">

  <xsl:variable name="imageurl">bank=<xsl:value-of select="./Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="./Fld[@ID='LockboxID']"/>&amp;batch=<xsl:value-of select="./Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="./Fld[@ID='BatchNumber']"/>&amp;date=<xsl:value-of select="./Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="./Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="./Fld[@ID='TransactionID']"/>&amp;page=-1</xsl:variable>

	<a>
		<xsl:attribute name="href">viewallimages.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
		<xsl:attribute name="title">View All Images for Transaction <xsl:value-of select="./Fld[@ID='TxnSequence']"/></xsl:attribute>
		<xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
		<i class="fa fa-picture-o fa-lg" />&nbsp;&nbsp;View All Images
	</a>

</xsl:template>

</xsl:stylesheet>