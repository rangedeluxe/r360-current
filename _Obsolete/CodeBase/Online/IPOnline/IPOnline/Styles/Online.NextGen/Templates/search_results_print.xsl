<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incCustomBranding.xsl"/>

<xsl:output method="html"/>

<xsl:variable name="OLWorkGroupsID"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" /></xsl:variable>
<xsl:variable name="DisplayBatchID"><xsl:value-of select="/Page/WorkgroupData/DisplayBatchID" /></xsl:variable>

<xsl:template match="/">

	<html>
	<head>
		<title>Search Results</title>
		<link rel="stylesheet" text="text/css" href="{$brandtemplatepath}/stylesheet.css"/>
	</head>
	<body>

		<a name="top"></a>
		<p/>
		<xsl:apply-templates select="/Page/Messages"/>
		<p/>

		<table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
			<tr>
				<td align="left" class="contenttitle">
					Search Results
				</td>
			</tr>
			<tr>
				<td align="left" class="contentsubtitle">
					<xsl:if test="/Page/Recordset[@Name = 'Results']">
						<xsl:apply-templates select="/Page/Recordset[@Name = 'Results']" mode="ContentTitle"/>
					</xsl:if>
				</td>
			</tr>
		</table>

		<p/>

		<xsl:apply-templates select="/Page"/>

		<p/>

		<xsl:if test="/Page/Recordset[@Name = 'SearchCriteria']">
   <table align="center" width="98%">
    <xsl:apply-templates select="/Page/Recordset[@Name = 'SearchCriteria']" mode="SearchCriteria"/>
   </table>
		</xsl:if>

		<table align="center" width="98%">

			<tr>
				<td align="left" valign="top">
					<a href="#top"><img src="{$brandtemplatepath}/Images/top.gif" border="0" alt="Back to Top"/></a>
				</td>
			</tr>
			<tr>
				<td class="copyright">

				</td>
			</tr>
		</table>

	</body>
	</html>

</xsl:template>


<xsl:template match="Messages">

 <xsl:if test="Message[@Type='Error']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Error</xsl:with-param>
   <xsl:with-param name="type">Error</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/critical.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <br/>

 <xsl:if test="Message[@Type='Information']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Information</xsl:with-param>
   <xsl:with-param name="type">Information</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/information.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>
 <br/>

 <xsl:if test="Message[@Type='Warning']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Warning</xsl:with-param>
   <xsl:with-param name="type">Warning</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/exclamation.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

</xsl:template>


<xsl:template name="DisplayMessages">

 <xsl:param name="type"/>
 <xsl:param name="caption">Messages</xsl:param>
 <xsl:param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/critical.gif</xsl:param>

 <table border="0" cellpadding="0" cellspacing="1" width="400" align="center" class="errormain">
  <tr>
   <td>
   <table border="0" cellpadding="5" cellspacing="0" width="400" align="center" class="errorsub">
    <tr>
     <td align="center" valign="middle" class="errortitle" colspan="2"><xsl:value-of select="$caption"/></td>
    </tr>
    <tr align="center" valign="middle">
     <td>
      <img src="{$brandtemplatepath}/Images/critical.gif">
       <xsl:attribute name="src"><xsl:value-of select="$icon"/></xsl:attribute>
      </img>
     </td>
     <td align="left" valign="top" width="320" class="errordetail">
      <menu>
       <xsl:for-each select="Message[@Type=$type]">
        <li/><xsl:value-of select="@Text"/>
       </xsl:for-each>
      </menu>
     </td>
    </tr>
   </table>
   </td>
  </tr>
 </table>

</xsl:template>


<xsl:template match="Page">

	<table align="center" width="98%" cellpadding="0" cellspacing="0" border="0" class="reportmain">
		<tr>
			<td>
				<table width="100%" cellpadding="5" cellspacing="1" border="0">
					<tr>
						<td align="left" class="reportheading">
							Deposit Date
						</td>
            <xsl:if test="$DisplayBatchID='True'">
       <td align="left" class="reportheading">
        <xsl:call-template name="batchidlabel" />
       </td>
            </xsl:if>
      <td align="left" class="reportheading">
       <xsl:call-template name="batchnumberlabel" />
      </td>
            <td align="left" class="reportheading">
              Payment Source
            </td>
            <td align="left" class="reportheading">
              Payment Type
            </td>
						<td align="left" class="reportheading">
							Transaction
						</td>

						<xsl:choose>
							<xsl:when test="Recordset[@Name='Columns']">
								<xsl:apply-templates select="Recordset[@Name = 'Columns']" mode="Columns"/>
							</xsl:when>
							<xsl:otherwise>
								<xsl:if test="Recordset[@Name='FormFields']/@COTSOnly = 'False'">
									<td align="left" class="reportheading">
                    Payment Batch Sequence
                  </td>
                  <td align="right" class="reportheading">
										Amount
									</td>
									<td align="left" class="reportheading">
										R/T
									</td>
									<td align="left" class="reportheading">
										Account Number
									</td>
									<td align="left" class="reportheading">
										Check/Trace/Ref Number
									</td>
                  <td align="left" class="reportheading">
                    DDA
                  </td>
                  <td align="left" class="reportheading">
                    Payer
                  </td>
								</xsl:if>
							</xsl:otherwise>
						</xsl:choose>

					</tr>

					<xsl:if test="Recordset[@Name='Results']">
						<xsl:apply-templates select="Recordset[@Name = 'Results']" mode="Results"/>
					</xsl:if>

				</table>
			</td>
		</tr>
	</table>

</xsl:template>


<xsl:template match="Recordset" mode="ContentTitle">

	Records <xsl:value-of select="@StartRecord"/> - <xsl:value-of select="@EndRecord"/> of <xsl:value-of select="@TotalRecords"/>

</xsl:template>


<xsl:template match="Recordset" mode="Columns">

	<xsl:for-each select="Record">
		<td class="reportheading">
			<xsl:choose>
				<xsl:when test="number(@DataTypeEnum) = 1 or number(@DataTypeEnum) = 11 or concat(@TableName, @FieldName) = 'ChecksCheckSequence'">
					<xsl:attribute name="align">left</xsl:attribute>
				</xsl:when>
				<xsl:when test="number(@DataTypeEnum) = 6 or number(@DataTypeEnum) = 7">
					<xsl:attribute name="align">right</xsl:attribute>
				</xsl:when>
				<xsl:otherwise>
					<xsl:attribute name="align">left</xsl:attribute>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:if test="string-length(@DisplayName) &gt; 0">
				<xsl:value-of select="@DisplayName"/>
			</xsl:if>

			<xsl:if test="string-length(@DisplayName) &lt;= 0">
				&nbsp;
			</xsl:if>
		</td>
	</xsl:for-each>

</xsl:template>


<xsl:template match="Recordset" mode="Results">

	<xsl:for-each select="Record">
		<xsl:variable name="curr"><xsl:value-of select="position()"/></xsl:variable>

		<tr>
			<xsl:if test='position() mod 2 = 0'>
				<xsl:attribute name="class">evenrow</xsl:attribute>
			</xsl:if>
			<xsl:if test='position() mod 2 != 0'>
				<xsl:attribute name="class">oddrow</xsl:attribute>
			</xsl:if>

			<td align="left" class="reportdata">
				<xsl:value-of select="Fld[@ID='Deposit_Date']"/>
			</td>
   <xsl:if test="$DisplayBatchID='True'">
			 <td align="left" class="reportdata">
				 <xsl:value-of select="Fld[@ID='SourceBatchID']"/>
			 </td>
   </xsl:if>
   <td align="left" class="reportdata">
				<xsl:value-of select="Fld[@ID='BatchNumber']"/>
			</td>
   <td align="left" class="reportdata">
				<xsl:value-of select="Fld[@ID='PaymentSource']"/>
			</td>
   <td align="left" class="reportdata">
				<xsl:value-of select="Fld[@ID='PaymentType']"/>
			</td>

      <td align="left" class="reportdata">
				<xsl:value-of select="Fld[@ID='TxnSequence']"/>
			</td>

			<xsl:choose>
				<xsl:when test="//Recordset[@Name = 'Columns']">
					<xsl:for-each select="//Recordset[@Name = 'Columns']/Record">

						<xsl:variable name="TableName"><xsl:value-of select="@TableName"/></xsl:variable>
						<xsl:variable name="FieldName"><xsl:value-of select="@FieldName"/></xsl:variable>
						<xsl:variable name="ReportTitle"><xsl:value-of select="@DisplayName"/></xsl:variable>
						<xsl:variable name="fld"><xsl:value-of select="@ColumnName"/></xsl:variable>
						<td class="reportdata">
							<xsl:if test="string-length(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]) &gt; 0">
								<xsl:choose>
									<xsl:when test="number(@DataTypeEnum) = 1 or number(@DataTypeEnum) = 11 or $fld = 'ChecksCheckSequence'">
										<xsl:attribute name="align">left</xsl:attribute>
										<xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
									</xsl:when>
									<xsl:when test="number(@DataTypeEnum) = 6">
										<xsl:attribute name="align">right</xsl:attribute>
										<xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
									</xsl:when>
									<xsl:when test="number(@DataTypeEnum) = 7">
										<xsl:attribute name="align">right</xsl:attribute>
										$<xsl:value-of select="format-number(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld], '#,##0.00')"/>
									</xsl:when>
									<xsl:otherwise>
										<xsl:attribute name="align">left</xsl:attribute>
										<xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:if>
							<xsl:if test="string-length(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
					</xsl:for-each>
				</xsl:when>
				<xsl:otherwise>
					<xsl:if test="//Recordset[@Name = 'FormFields']/@COTSOnly = 'False'">
						<td align="left" class="reportdata">
              <xsl:if test="string-length(Fld[@ID='ChecksBatchSequence']) &gt; 0">
                <xsl:value-of select="Fld[@ID='ChecksBatchSequence']"/>
              </xsl:if>
              <xsl:if test="string-length(Fld[@ID='ChecksBatchSequence']) &lt;= 0">
                &nbsp;
              </xsl:if>
            </td>
            <td align="right" class="reportdata">
							<xsl:if test="string-length(Fld[@ID='Amount']) &gt; 0">
								<xsl:value-of select="format-number(Fld[@ID='Amount'], '#,##0.00')"/>
							</xsl:if>
							<xsl:if test="string-length(Fld[@ID='Amount']) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
						<td align="left" class="reportdata">
							<xsl:if test="string-length(Fld[@ID='RT']) &gt; 0">
								<xsl:value-of select="Fld[@ID='RT']"/>
							</xsl:if>
							<xsl:if test="string-length(Fld[@ID='RT']) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
						<td align="left" class="reportdata">
							<xsl:if test="string-length(Fld[@ID='AccountNumber']) &gt; 0">
								<xsl:value-of select="Fld[@ID='AccountNumber']"/>
							</xsl:if>
							<xsl:if test="string-length(Fld[@ID='AccountNumber']) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
						<td align="left" class="reportdata">
							<xsl:if test="string-length(Fld[@ID='SerialNumber']) &gt; 0">
								<xsl:value-of select="Fld[@ID='SerialNumber']"/>
							</xsl:if>
							<xsl:if test="string-length(Fld[@ID='SerialNumber']) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
            <td align="left" class="reportdata">
							<xsl:if test="string-length(Fld[@ID='DDA']) &gt; 0">
								<xsl:value-of select="Fld[@ID='DDA']"/>
							</xsl:if>
							<xsl:if test="string-length(Fld[@ID='DDA']) &lt;= 0">
								&nbsp;
							</xsl:if>
						</td>
            <td align="left" class="reportdata">
              <xsl:if test="string-length(Fld[@ID='Payer']) &gt; 0">
                <xsl:value-of select="Fld[@ID='Payer']"/>
              </xsl:if>
              <xsl:if test="string-length(Fld[@ID='Payer']) &lt;= 0">
                &nbsp;
              </xsl:if>
            </td>
					</xsl:if>
				</xsl:otherwise>
			</xsl:choose>
		</tr>
	</xsl:for-each>

</xsl:template>

<xsl:template match="Record" mode="SearchCriteria">
	<tr>
		<td align="left" valign="top" class="reportfooter" nowrap="1"><xsl:value-of select="@DisplayTitle"/>:&nbsp;</td> 
		<td align="left" valign="top" class="reportfooter"><xsl:value-of select="@DisplayText"/></td>
	</tr>
</xsl:template>


</xsl:stylesheet>
