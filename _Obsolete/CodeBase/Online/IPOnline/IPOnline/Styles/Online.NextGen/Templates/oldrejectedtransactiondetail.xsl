<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
    Rejected Decisioning Transaction Detail
  </xsl:template>

  <xsl:template name="PageTitle">
    <span class="contenttitle">Rejected Decisioning Transaction Detail</span>
    <br/>
    <span class="contentsubtitle">&nbsp;</span>
  </xsl:template>


  <xsl:template match="Page">

    <xsl:variable name="BankID">
      <xsl:value-of select="RespRejectedTransactionDetail/Batch/BankID" />
    </xsl:variable>
    <xsl:variable name="LockboxID">
      <xsl:value-of select="RespRejectedTransactionDetail/Batch/LockboxID" />
    </xsl:variable>
    <xsl:variable name="OLLockboxID">
      <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@OLLockboxID" />
    </xsl:variable>
    <xsl:variable name="RejectedBatchID">
      <xsl:value-of select="/Page/FormFields/RejectedBatchID" />
    </xsl:variable>
    <xsl:variable name="TransactionID">
      <xsl:value-of select="/Page/FormFields/TransactionID" />
    </xsl:variable>

    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td>
          <table width="100%" cellpadding="5" cellspacing="0" border="0">
            <tr>
              <td class="reportcaption">
                Workgroup: <xsl:value-of select="$LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@LongName" /><br />
                Deposit Date: <xsl:value-of select="RespRejectedTransactionDetail/Batch[RejectedBatchID=$RejectedBatchID]/DepositDate" /><br />
                Batch: <xsl:value-of select="RespRejectedTransactionDetail/Batch[RejectedBatchID=$RejectedBatchID]/BatchID" /><br />
                Transaction: <xsl:value-of select="$TransactionID" />
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="left" valign="top">
          <table cellSpacing="0" cellPadding="5" width="100%" border="0">
            <tr>
              <td class="reportcaption" align="left">Checks</td>
            </tr>
          </table>
        </td>
        <td colspan="2">
          <table width="100%" cellpadding="5" cellspacing="0" border="0">
            <tr>
              <td align="right" class="pagelinks">
                <a>
                  <xsl:attribute name="href">
                    oldrejectedtransactions.aspx?OLLockboxID=<xsl:value-of select="$OLLockboxID" />
                  </xsl:attribute>
                  OLD Rejected Transactions
                </a>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table cellSpacing="0" cellPadding="5" width="100%" align="center" border="0">
            <tbody>
              <tr>
                <td class="reportheading">R/T</td>
                <td class="reportheading">Account</td>
                <td class="reportheading">Serial</td>
                <td class="reportheading">Payment Amount</td>
                <td class="reportheading">Payer</td>
              </tr>

              <xsl:for-each select="RespRejectedTransactionDetail/Batch[RejectedBatchID=$RejectedBatchID]/Transactions[TransactionID=$TransactionID]/Checks">
                <tr>

                  <xsl:if test='position() mod 2 = 0'>
                    <xsl:attribute name="class">evenrow</xsl:attribute>
                  </xsl:if>
                  <xsl:if test='position() mod 2 != 0'>
                    <xsl:attribute name="class">oddrow</xsl:attribute>
                  </xsl:if>

                  <td class="reportdata">
                    <xsl:value-of select="RT" />
                  </td>
                  <td class="reportdata">
                    <xsl:value-of select="Account" />
                  </td>
                  <td class="reportdata">
                    <xsl:value-of select="Serial" />
                  </td>
                  <td class="reportdata">
                    <xsl:value-of select="format-number(Amount, '$ ###,###.00')" />
                  </td>
                  <td class="reportdata">
                    <xsl:value-of select="RemitterName" />
                  </td>
                </tr>
              </xsl:for-each>

            </tbody>
          </table>
        </td>
      </tr>
    </table>
  </xsl:template>

</xsl:stylesheet>
