<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="text"/>

<xsl:include href="incCustomBranding.xsl"/>

  <xsl:variable name="OLWorkGroupsID"><xsl:value-of select="/Page/Recordset[@Name='SearchParms']/Record[@ParmName='OLWorkGroupsID']" /></xsl:variable>
  <xsl:variable name="DisplayBatchID"><xsl:value-of select="/Page/WorkgroupData/DisplayBatchID" /></xsl:variable>

  <xsl:template match="/">

		<xsl:if test="Page/Messages">
			<xsl:apply-templates select="Page/Messages"/>
			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:if>

		<xsl:choose>
			<xsl:when test="Page/Recordset[@Name = 'Columns']">
				<xsl:text>Deposit Date,</xsl:text>
    <xsl:if test="$DisplayBatchID='True'"><xsl:call-template name="batchidlabel" /><xsl:text>,</xsl:text></xsl:if>
    <xsl:call-template name="batchnumberlabel" /><xsl:text>,Payment Source,Payment Type,Transaction</xsl:text>
				<xsl:for-each select="Page/Recordset[@Name = 'Columns']/Record">
					<xsl:text>,</xsl:text><xsl:value-of select="@DisplayName"/>
				</xsl:for-each>
			</xsl:when>
			<xsl:otherwise>
				<xsl:text>Deposit Date,</xsl:text>
    <xsl:if test="$DisplayBatchID='True'"><xsl:call-template name="batchidlabel" /><xsl:text>,</xsl:text></xsl:if>
    <xsl:call-template name="batchnumberlabel" /><xsl:text>,Payment Source,Payment Type,Transaction</xsl:text>
				<xsl:if test="Page/Recordset[@Name = 'FormFields']/@COTSOnly = 'False'">
					<xsl:text>,Payment Batch Sequence,Payment Amount,R/T,Account Number,Check/Trace/Ref Number,DDA,Payer</xsl:text>
				</xsl:if>
			</xsl:otherwise>
		</xsl:choose>

		<xsl:text>&#13;&#10;</xsl:text>

		<xsl:if test="Page/Recordset[@Name = 'Results']">
			<xsl:for-each select="Page/Recordset[@Name = 'Results']/Record">
				<xsl:variable name="curr"><xsl:value-of select="position()"/></xsl:variable>

				<xsl:value-of select="Fld[@ID='Deposit_Date']"/><xsl:text>,</xsl:text>
    <xsl:if test="$DisplayBatchID='True'"><xsl:value-of select="Fld[@ID='SourceBatchID']"/><xsl:text>,</xsl:text></xsl:if>
				<xsl:value-of select="Fld[@ID='BatchNumber']"/><xsl:text>,</xsl:text>
        <xsl:value-of select="Fld[@ID='PaymentSource']"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="Fld[@ID='PaymentType']"/>
        <xsl:text>,</xsl:text>
        <xsl:value-of select="Fld[@ID='TransactionID']"/>

				<xsl:choose>
					<xsl:when test="/Page/Recordset[@Name = 'Columns']/Record">
						<xsl:for-each select="/Page/Recordset[@Name = 'Columns']/Record">

							<xsl:variable name="TableName"><xsl:value-of select="@TableName"/></xsl:variable>
							<xsl:variable name="FieldName"><xsl:value-of select="@FieldName"/></xsl:variable>
							<xsl:variable name="ReportTitle"><xsl:value-of select="@DisplayName"/></xsl:variable>
							<xsl:variable name="fld"><xsl:value-of select="@ColumnName"/></xsl:variable>

							<xsl:choose>
								<xsl:when test="number(@DataTypeEnum) = 1 or number(@DataTypeEnum) = 11 or number(@DataTyepEnum) = 6">
									<xsl:text>,</xsl:text><xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
								</xsl:when>
								<xsl:when test="number(@DataTypeEnum) = 7 and string-length(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]) &gt; 0">
									<xsl:text>,</xsl:text>$<xsl:value-of select="format-number(//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld], '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:text>,</xsl:text><xsl:value-of select="//Recordset[@Name = 'Results']/Record[position() = number($curr)]/Fld[@ID=$fld]"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="/Page/Recordset[@Name = 'FormFields']/@COTSOnly = 'False'">
              <xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='ChecksBatchSequence']"/>
              <xsl:text>,</xsl:text>
							<xsl:choose>
								<xsl:when test="string-length(Fld[@ID='Amount']) &gt; 0">
									<xsl:text>$</xsl:text><xsl:value-of select="format-number(Fld[@ID='Amount'], '0.00')"/>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="Fld[@ID='Amount']"/>
								</xsl:otherwise>
							</xsl:choose>
							<xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='RT']"/>
							<xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='AccountNumber']"/>
							<xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='SerialNumber']"/>
              <xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='DDA']"/>
              <xsl:text>,</xsl:text>
              <xsl:value-of select="Fld[@ID='Payer']"/>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
				<xsl:text>&#13;&#10;</xsl:text>
			</xsl:for-each>
		</xsl:if>

</xsl:template>


<xsl:template match="Messages">

 <xsl:if test="Message[@Type='Error']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Error</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <xsl:if test="Message[@Type='Information']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Information</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <xsl:if test="Message[@Type='Warning']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Warning</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

</xsl:template>


<xsl:template name="DisplayMessages">
 <xsl:param name="type"/>

	<xsl:for-each select="Message[@Type=$type]">
 	<xsl:value-of select="$type"/>: <xsl:value-of select="@Text"/><xsl:text>&#13;&#10;</xsl:text>
 </xsl:for-each>

</xsl:template>

</xsl:stylesheet>
