<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
  <xsl:value-of select="$_PageTitle" />
</xsl:template>


<xsl:template name="PageTitle">
	<span class="contenttitle"><xsl:value-of select="$_PageTitle" /></span>
	<br/>
	<span class="contentsubtitle"></span>
</xsl:template>

<xsl:variable name="_DisplayMode">
  <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='DisplayMode']/@Value" />
</xsl:variable>

<xsl:variable name="_LockboxRemitterID">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxRemitterID']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxRemitterID']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@LockboxRemitterID"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<!--
<xsl:variable name="_OLLockboxIDValue">
</xsl:variable>
-->

<xsl:variable name="_BankID">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='BankID']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='BankID']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@BankID"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_LockboxID">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxID']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxID']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@LockboxID"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_RoutingNumber">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='RoutingNumber']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='RoutingNumber']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@RoutingNumber"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_Account">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='Account']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='Account']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@Account"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_LockboxRemitterName">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxRemitterName']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='LockboxRemitterName']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@LockboxRemitterName"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<!--

<xsl:variable name="_GlobalBatchIDValue">
</xsl:variable>

<xsl:variable name="_GlobalCheckIDValue">
</xsl:variable>

<xsl:variable name="_ImageExistsValue">
</xsl:variable>

<xsl:variable name="_ReturnPageValue">
</xsl:variable>

<xsl:variable name="_BatchIDValue">
</xsl:variable>

<xsl:variable name="_PageValue">
</xsl:variable>
-->

<xsl:variable name="_RecStartValue">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='RecStartValue']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='RecStartValue']/@Value" />
 </xsl:when>
 <xsl:otherwise>1</xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<!--
<xsl:variable name="_SequenceValue">
</xsl:variable>

<xsl:variable name="_DESetupIDValue">
</xsl:variable>

<xsl:variable name="_ItemValue">
</xsl:variable>

<xsl:variable name="_ProcDateValue">
</xsl:variable>

<xsl:variable name="_AmountFromValue">
</xsl:variable>

<xsl:variable name="_AmountToValue">
</xsl:variable>

<xsl:variable name="_DateFromValue">
</xsl:variable>

<xsl:variable name="_DateToValue">
</xsl:variable>

<xsl:variable name="_LongNameValue">
</xsl:variable>

<xsl:variable name="_TypeValue">
</xsl:variable>

<xsl:variable name="_CheckNumberValue">
</xsl:variable>

<xsl:variable name="_SortValue">
</xsl:variable>

<xsl:variable name="_Warning">
</xsl:variable>

<xsl:variable name="_LockboxSelectList">
</xsl:variable>

<xsl:variable name="_RemitterName">
</xsl:variable>

<xsl:variable name="_PICS">
</xsl:variable>

<xsl:variable name="_AccountNumber">
  <xsl:choose>
 <xsl:when test="/Page/Form[@Name='frmRemitter']/Field[@Name='Account']/@Value">
   <xsl:value-of select="/Page/Form[@Name='frmRemitter']/Field[@Name='Account']/@Value" />
 </xsl:when>
 <xsl:otherwise>
   <xsl:value-of select="/Page/Remitter/@Account"/>
 </xsl:otherwise>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_ExistsMessage">
</xsl:variable>

<xsl:variable name="_TransactionIDValue">
</xsl:variable>

<xsl:variable name="_ProcessingDateValue">
</xsl:variable>

-->

<xsl:variable name="_PageTitle">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add'">Add New Payer Information</xsl:when>
 <xsl:when test="$_DisplayMode = 'edit'">Add Payer To Payment</xsl:when>
 <xsl:when test="$_DisplayMode = 'modify'">Modify Payer Information</xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">New Payer Added</xsl:when>
  </xsl:choose>
</xsl:variable>

<xsl:variable name="_FormTitle">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add'">Add New Payer Information</xsl:when>
 <xsl:when test="$_DisplayMode = 'edit'">Add Payer To Payment</xsl:when>
 <xsl:when test="$_DisplayMode = 'modify'">Modify Payer Information</xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">New Payer Added</xsl:when>
  </xsl:choose>
</xsl:variable>

<xsl:template match="Page">

  <form name="frmMain" method="POST">

 <input type="hidden" name="txtAction" value="" />

 <input type="hidden" name="txtDisplayMode">
   <xsl:attribute name="value"><xsl:value-of select="$_DisplayMode"/></xsl:attribute>
 </input>

  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'edit'">
   <!--
   <input type="hidden" name="txtGlobalBatchID"><xsl:attribute name="value"><xsl:value-of select="$_GlobalBatchIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtGlobalCheckID"><xsl:attribute name="value"><xsl:value-of select="$_GlobalCheckIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtTransactionID"><xsl:attribute name="value"><xsl:value-of select="$_TransactionIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtSequence"><xsl:attribute name="value"><xsl:value-of select="$_SequenceValue"/></xsl:attribute></input>
   <input type="hidden" name="txtOLLockboxID"><xsl:attribute name="value"><xsl:value-of select="$_OLLockboxIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtLongName"><xsl:attribute name="value"><xsl:value-of select="$_LongNameValue"/></xsl:attribute></input>
   <input type="hidden" name="txtImageExists"><xsl:attribute name="value"><xsl:value-of select="$_ImageExistsValue"/></xsl:attribute></input>
   <input type="hidden" name="txtDESetupID"><xsl:attribute name="value"><xsl:value-of select="$_DESetupIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtReturnPage"><xsl:attribute name="value"><xsl:value-of select="$_ReturnPageValue"/></xsl:attribute></input>
   <input type="hidden" name="txtProcessingDate"><xsl:attribute name="value"><xsl:value-of select="$_ProcessingDateValue"/></xsl:attribute></input>
   <input type="hidden" name="txtBatchID"><xsl:attribute name="value"><xsl:value-of select="$_BatchIDValue"/></xsl:attribute></input>
   <input type="hidden" name="txtItem"><xsl:attribute name="value"><xsl:value-of select="$_ItemValue"/></xsl:attribute></input>
   <input type="hidden" name="txtType"><xsl:attribute name="value"><xsl:value-of select="$_TypeValue"/></xsl:attribute></input>
   <input type="hidden" name="txtPage"><xsl:attribute name="value"><xsl:value-of select="$_PageValue"/></xsl:attribute></input>
   -->
   <input type="hidden" name="txtRecstart"><xsl:attribute name="value"><xsl:value-of select="$_RecStartValue"/></xsl:attribute></input>
   <!--
   <input type="hidden" name="txtProcdate"><xsl:attribute name="value"><xsl:value-of select="$_ProcDateValue"/></xsl:attribute></input>

  <xsl:if test="$_ReturnPageValue = 'search'">
			 <input type="hidden" name="txtAmountFrom"><xsl:attribute name="value"><xsl:value-of select="$_AmountFromValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtAmountTo"><xsl:attribute name="value"><xsl:value-of select="$_AmountToValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtCheckSerial"><xsl:attribute name="value"><xsl:value-of select="$_CheckNumberValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtOLLockbox"><xsl:attribute name="value"><xsl:value-of select="$_OLLockboxIDValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtSort"><xsl:attribute name="value"><xsl:value-of select="$_SortValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtDateFrom"><xsl:attribute name="value"><xsl:value-of select="$_DateFromValue" /></xsl:attribute></input>
			 <input type="hidden" name="txtDateTo"><xsl:attribute name="value"><xsl:value-of select="$_DateToValue" /></xsl:attribute></input>
  </xsl:if>
  -->

   <input type="hidden" name="txtOLLockboxRemitterID"><xsl:attribute name="value"><xsl:value-of select="$_LockboxRemitterID" /></xsl:attribute></input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'modify'">
				<input type="hidden" name="txtOLLockboxRemitterID"><xsl:attribute name="value"><xsl:value-of select="$_LockboxRemitterID" /></xsl:attribute></input>
				<input type="hidden" name="txtRecstart"><xsl:attribute name="value"><xsl:value-of select="$_RecStartValue"/></xsl:attribute></input>
 </xsl:when>
  </xsl:choose>

  <!--
  With Response
 .Write Page.GetField("LockboxRemitterID")
 .Write Page.GetField("GlobalBatchID")
 .Write Page.GetField("GlobalCheckID")
 .Write Page.GetField("TransactionID")
 .Write Page.GetField("Sequence")
 .Write Page.GetField("LockboxID")
 .Write Page.GetField("LongName")
 .Write Page.GetField("ImageExists")
 .Write Page.GetField("DESetupID")
 .Write Page.GetField("RemitterExists")
 .Write Page.GetField("ReturnPage")
 .Write Page.GetField("ProcessingDate")
 .Write Page.GetField("BatchID")
 .Write Page.GetField("Item")
 .Write Page.GetField("Type")
 .Write Page.GetField("Page")
 .Write Page.GetField("Recstart")
 .Write Page.GetField("Procdate")
 .Write Page.GetField("AmountFrom")
 .Write Page.GetField("AmountTo")
 .Write Page.GetField("CheckNumber")
 .Write Page.GetField("Lockbox")
 .Write Page.GetField("Sort")
 .Write Page.GetField("DateFrom")
 .Write Page.GetField("DateTo")
  End With

  if(Len(Page.GetField("ImageExistsValue")) <= 0) { 
   SetField("ImageExistsValue", Empty
  }

  if(IsNumeric(Page.GetField("ImageExistsValue")) And Page.GetField("ImageExistsValue") <> 0) {
 SetField("PICS", "imagepdf.aspx?date=" & Page.GetField("ProcessingDateValue")
			   & "&bank=" & User.BankID
			   & "&box=" & Page.GetField("OLLockboxIDValue")
			   & "&batch=" & Page.GetField("BatchIDValue")
			   & "&item=" & Page.GetField("ItemValue")
			   & "&type=" & Page.GetField("TypeValue")
			   & "&page=" & Page.GetField("PageValue")
  }
  -->

 <br />

 <table align="center" width="400" border="1" cellpadding="2" cellspacing="0">
   <tr>
  <td align="center" class="errortitle"><xsl:value-of select="$_FormTitle" /> <!--xsl:value-of select="$_Warning" /--></td>
   </tr>
   <tr>
  <td>
    <table align="center" width="100%" border="0" cellpadding="2" cellspacing="0">
   <!--
   <xsl:value-of select="$_ExistsMessage" />
   -->
   <tr>
	     <td align="right" class="formlabel">Workgroup:&nbsp;</td>
	     <td align="left" class="formfield"><xsl:call-template name="LockboxSelectList" /></td>
   </tr>
   <tr>
     <td align="right" class="formlabel">R/T:&nbsp;</td>
     <td align="left" class="formfield"><xsl:call-template name="RoutingNumber" /></td>
   </tr>
   <tr>
     <td align="right" class="formlabel">Account Number:&nbsp;</td>
     <td align="left" class="formfield"><xsl:call-template name="AccountNumber" /></td>
   </tr>
   <tr>
     <td align="right" class="formlabel">Payer:&nbsp;</td>
     <td align="left" class="formfield"><xsl:call-template name="RemitterName" /></td>
   </tr>
   <!--
   <xsl:if test="$_PICS != ''">
     <tr>
    <td align="right" colspan="2">
      <a href="#">
     <xsl:attribute name="onClick">docView('<xsl:value-of select="$_PICS" />');gWindow.focus();return false;</xsl:attribute>
     Display Check
      </a>
    </td>
     </tr>
   </xsl:if>
   -->
   <tr>
	   <td align="center" colspan="2">
    <xsl:choose>
      <xsl:when test="$_DisplayMode = 'add'">
	      <input type="button" name="cmdAdd" class="formfield" value="Add" onClick="formSubmit(this, 'txtAction')" />&nbsp;
     &nbsp;
	      <input type="reset" name="cmdReset" class="formfield" value="Reset" />
      </xsl:when>
      <xsl:when test="$_DisplayMode = 'edit'">
				   <input type="button" name="cmdUpdate" class="formfield" value="Update" onClick="formSubmit(this, 'txtAction')" />&nbsp;
     &nbsp;
				   <input type="button" name="cmdCancel" class="formfield" value="Cancel" onClick="JavaScript:history.back()" />					
      </xsl:when>
      <xsl:when test="$_DisplayMode = 'modify'">
				   <input type="button" name="cmdUpdate" class="formfield" value="Update" onClick="formSubmit(this, 'txtAction')" />&nbsp;
     &nbsp;
				   <input type="button" name="cmdCancel" class="formfield" value="Cancel">
				     <xsl:attribute name="onClick">location.href='viewremitter.aspx?start=<xsl:value-of select="$_RecStartValue" />';</xsl:attribute>
				   </input>
      </xsl:when>
      <xsl:when test="$_DisplayMode = 'confirm'">
 		   <input type="button" name="cmdAddNew" class="formfield" value="Add Another Payer" onClick="JavaScript:window.location.href='remittermaint.aspx?mode=add'" />
     &nbsp;
      </xsl:when>
    </xsl:choose>
	   </td>
   </tr>
    </table>
  </td>
   </tr>
 </table>

  </form>

  <script language="JavaScript">
 <xsl:comment>Hide script from older browsers
   //Set form field validation requirements
   with (document.forms["frmMain"]){
  if(elements["txtRoutingNumber"]){
    txtRoutingNumber.isRequired = true;
    txtRoutingNumber.fieldname = 'R/T';
  }
  if(elements["txtAccount"]){
    txtAccount.isRequired = true;
    txtAccount.fieldname = 'Account Number';
  }
  if(elements["txtRemitterName"]){
    txtRemitterName.isRequired = true;
    txtRemitterName.fieldname = 'Payer';
  }

   }
   //End hiding
 </xsl:comment>
  </script>	

  <!--
  <xsl:if test="$_PICS != ''">
 <script language="JavaScript">
   <xsl:comment>Hide script from older browsers
  docView('<xsl:value-of select="$_PICS" />');gWindow.focus();
  //End hiding
   </xsl:comment>
 </script>	
  </xsl:if>
  -->

</xsl:template>

<xsl:template name="LockboxSelectList">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add'">
	 <select name="cboOLLockboxes" class="formfield">
  <xsl:for-each select="/Page/UserLockboxes/Lockbox">
		   <option>
		  <xsl:attribute name="value"><xsl:value-of select="@OLLockboxID" /></xsl:attribute>
   <xsl:if test="$_BankID = @BankID and _LockboxID = @LockboxID">
     <xsl:attribute name="selected" />
   </xsl:if>
		  <xsl:value-of select="@LockboxID" /> - <xsl:value-of select="@LongName" />
		   </option>
  </xsl:for-each>
	 </select>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'edit' or $_DisplayMode = 'modify'">
   <xsl:value-of select="$_LockboxID" />
			<input type="hidden" name="cboOLLockboxes">
  <xsl:attribute name="value">
    <xsl:value-of select="/Page/UserLockboxes/Lockbox[@BankID = $_BankID and @LockboxID = $_LockboxID]/@OLLockboxID"/>
  </xsl:attribute>
			</input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">
   <xsl:value-of select="$_LockboxID" />
 </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="RoutingNumber">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add'">
			<input type="text" name="txtRoutingNumber" size="30" maxlength="30" class="formfield">
  <xsl:attribute name="value">
    <xsl:value-of select="$_RoutingNumber" />
  </xsl:attribute>
			</input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'edit' or $_DisplayMode = 'modify'">
			<xsl:value-of select="$_RoutingNumber" />
			<input type="hidden" name="txtRoutingNumber">
  <xsl:attribute name="value">
    <xsl:value-of select="$_RoutingNumber" />
  </xsl:attribute>
			</input> 
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">
			<xsl:value-of select="$_RoutingNumber" />
 </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="AccountNumber">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add'">
			<input type="text" name="txtAccount" size="30" maxlength="30" class="formfield">
  <xsl:attribute name="value">
    <xsl:value-of select="$_Account" />
  </xsl:attribute>
			</input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'edit' or $_DisplayMode = 'modify'">
			<xsl:value-of select="$_Account" />
			<input type="hidden" name="txtAccount">
  <xsl:attribute name="value">
    <xsl:value-of select="$_Account" />
  </xsl:attribute>
			</input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">
			<xsl:value-of select="$_Account" />
 </xsl:when>
  </xsl:choose>
</xsl:template>

<xsl:template name="RemitterName">
  <xsl:choose>
 <xsl:when test="$_DisplayMode = 'add' or $_DisplayMode = 'edit' or $_DisplayMode = 'modify'">
   <input type="text" name="txtRemitterName" size="30" maxlength="60" class="formfield">
  <xsl:attribute name="value">
    <xsl:value-of select="$_LockboxRemitterName" />
  </xsl:attribute>
   </input>
 </xsl:when>
 <xsl:when test="$_DisplayMode = 'confirm'">
			<xsl:value-of select="$_LockboxRemitterName" />
 </xsl:when>
  </xsl:choose>
</xsl:template>

</xsl:stylesheet>
