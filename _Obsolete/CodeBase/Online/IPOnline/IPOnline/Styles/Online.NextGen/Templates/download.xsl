<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:output method="text"/>

<xsl:template match="/">

		<xsl:if test="Page/Messages">
			<xsl:apply-templates select="Page/Messages"/>
			<xsl:text>&#13;&#10;</xsl:text>
		</xsl:if>

		<xsl:if test="Page/Recordset[@Name='Remitters']">
			<xsl:apply-templates select="Page/Recordset[@Name='Remitters']" mode="Remitters"/>
		</xsl:if>
</xsl:template>

<xsl:template match="Recordset" mode="Remitters">
	<xsl:text>Workgroup|R/T|Account Number|Payer</xsl:text>	
	<xsl:text>&#13;&#10;</xsl:text>
	
	<xsl:for-each select="Record">
		<xsl:value-of select="Fld[@ID='LockboxID']"/><xsl:text>|</xsl:text>
		<xsl:value-of select="Fld[@ID='RoutingNumber']"/><xsl:text>|</xsl:text>
		<xsl:value-of select="Fld[@ID='Account']"/><xsl:text>|</xsl:text>
		<xsl:value-of select="Fld[@ID='RemitterName']"/>		
		<xsl:text>&#13;&#10;</xsl:text>
	</xsl:for-each>
</xsl:template>

<xsl:template match="Messages">

 <xsl:if test="Message[@Type='Error']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Error</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <xsl:if test="Message[@Type='Information']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Information</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <xsl:if test="Message[@Type='Warning']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="type">Warning</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

</xsl:template>


<xsl:template name="DisplayMessages">
 <xsl:param name="type"/>

	<xsl:for-each select="Message[@Type=$type]">
 	<xsl:value-of select="$type"/>: <xsl:value-of select="@Text"/><xsl:text>&#13;&#10;</xsl:text>
 </xsl:for-each>

</xsl:template>

</xsl:stylesheet>
