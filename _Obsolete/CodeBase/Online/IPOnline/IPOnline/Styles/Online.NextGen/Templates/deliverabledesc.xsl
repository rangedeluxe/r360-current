<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
 <!ENTITY copy "&#169;">
 <!ENTITY middot "&#183;">
 <!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="html"/>

<xsl:include href="incCustomBranding.xsl"/>

<xsl:template match="/">
	<html>
	<head>
		<title>Deliverable Description</title>
		<link rel="stylesheet" text="text/css" href="{$brandtemplatepath}/stylesheet.css"/>
	</head>
	<body>

	<table width="100%" border="0" cellpadding="0" cellspacing="0">
		<tr valign="middle" class="brandheaderstyle">
			<td align="left" colspan="2"><img src="{$brandtemplatepath}/Images/Mast-Header.jpg" border="0"><xsl:attribute name="alt"><xsl:call-template name="appname" /></xsl:attribute></img></td>
		</tr>
		<tr>
			<td align="left" bgcolor="#000000" colspan="2"><img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="1"/></td>
		</tr>
		<tr>
			<td align="left" class="contenttitle" colspan="2"><img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="10"/></td>
		</tr>
		<tr>
			<td align="left" class="contenttitle"><img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="10"/></td>
			<td align="left" valign="top" class="contenttitle">Deliverable Description</td>
		</tr>
		<tr>
			<td align="left" class="contenttitle" colspan="2"><img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="10"/></td>
		</tr>
		<tr>
			<td align="left" bgcolor="#000000" colspan="2"><img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="1"/></td>
		</tr>
	</table>

	<br/>

	<a name="top"></a>
	<p/>
	<xsl:apply-templates select="/Page/Messages"/>
	<p/>


	<xsl:if test="/Page/Recordset[@Name='InternetDeliverable']">
		<xsl:apply-templates select="/Page/Recordset[@Name = 'InternetDeliverable']" mode="InternetDeliverable"/>
	</xsl:if>
	
	<br/>

	<table border="0" cellpadding="2" cellspacing="0">
		<tr>
			<td align="left" class="copyright"><xsl:call-template name="copyright" /></td>
		</tr>
	</table>

	</body>
	</html>
</xsl:template>

<xsl:template match="Recordset" mode="InternetDeliverable">
	<table border="0" cellpadding="3" cellspacing="0">
		<tr>
			<td align="left" valign="top" class="formlabel">Deliverable:</td>
			<td align="left" valign="top" class="formfield"><xsl:value-of select="Record/Fld[@ID='Deliverable']"/></td>
		</tr>
		<tr>
			<td align="left" valign="top" class="formlabel">Description:</td>
			<td align="left" valign="top" class="formfield"><xsl:value-of select="Record/Fld[@ID='Description']"/></td>
		</tr>
	</table>
</xsl:template>



<xsl:template match="Messages">
 <xsl:if test="Message[@Type='Error']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Error</xsl:with-param>
   <xsl:with-param name="type">Error</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/critical.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>

 <br/>

 <xsl:if test="Message[@Type='Information']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Information</xsl:with-param>
   <xsl:with-param name="type">Information</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/information.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>
 <br/>

 <xsl:if test="Message[@Type='Warning']">
  <xsl:call-template name="DisplayMessages">
   <xsl:with-param name="caption">Warning</xsl:with-param>
   <xsl:with-param name="type">Warning</xsl:with-param>
   <xsl:with-param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/exclamation.gif</xsl:with-param>
  </xsl:call-template>
 </xsl:if>
</xsl:template>


<xsl:template name="DisplayMessages">
 <xsl:param name="type"/>
 <xsl:param name="caption">Messages</xsl:param>
 <xsl:param name="icon"><xsl:value-of select="$brandtemplatepath"/>/Images/critical.gif</xsl:param>

 <table border="0" cellpadding="0" cellspacing="1" width="400" align="center" class="errormain">
  <tr>
   <td>
   <table border="0" cellpadding="5" cellspacing="0" width="400" align="center" class="errorsub">
    <tr>
     <td align="center" valign="middle" class="errortitle" colspan="2"><xsl:value-of select="$caption"/></td>
    </tr>
    <tr align="center" valign="middle">
     <td>
      <img src="{$brandtemplatepath}/Images/critical.gif">
       <xsl:attribute name="src"><xsl:value-of select="$icon"/></xsl:attribute>
      </img>
     </td>
     <td align="left" valign="top" width="320" class="errordetail">
      <menu>
       <xsl:for-each select="Message[@Type=$type]">
        <li/><xsl:value-of select="@Text"/>
       </xsl:for-each>
      </menu>
     </td>
    </tr>
   </table>
   </td>
  </tr>
 </table>
</xsl:template>

</xsl:stylesheet>
