<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
 <!ENTITY nbsp "&#160;">
 <!ENTITY copy "&#169;">
 <!ENTITY middot "&#183;">
 <!ENTITY laquo "&#171;">
 <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:include href="incCustomBranding.xsl"/>
	
	<xsl:output method="html"/>

 <xsl:template name="Title">
  Property Management Account Summary
 </xsl:template>

 <xsl:template name="PageTitle">
  <span class="contenttitle">Property Management Account Summary</span>
  <br/>
  <span class="contentsubtitle">&nbsp;(All Management Companies)</span>
 </xsl:template>

 <xsl:template match="Page">
  <html>
   <head>
    <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
    <link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
    <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/scriptlib.js" />
   </head>
   <body>
    <div id="container">
     <div id="header">
      <h2><p/>Property Management Account Summary</h2>
      <h2><xsl:value-of select="Recordset[@Name = 'HOAPMAReport']/Lockbox/@CustomerName"/></h2>
      <h3>(All Management Companies)</h3>
     </div>
     <div id="subheader">
     </div>
     <div id="content">
       <xsl:if test="Recordset[@Name='HOAPMAReport']">
        <xsl:apply-templates select="Recordset[@Name = 'HOAPMAReport']" mode="HOAPMAReport"/>
       </xsl:if>
     </div>
     <div id="footer">
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				  <xsl:variable name="printviewurl">haveparameters=yes&amp;reportname=HOAAllPMASummary&amp;customer=<xsl:value-of select="Inputs/@CustomerID"/>&amp;date=<xsl:value-of select="Inputs/@DepositDate"/></xsl:variable>
				  <a>
					  <xsl:attribute name="href">JavaScript:newPopup('HOAReports.aspx?<xsl:value-of select="$printviewurl"/>&amp;printview=1');</xsl:attribute>
					  <nobr>Printer-Friendly Version</nobr>
				  </a>
     </div>
    </div>
   </body>
  </html>
 </xsl:template>

 <xsl:template match="Recordset" mode="HOAPMAReport">
  
   <xsl:for-each select="Lockbox">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
     <tr>
      <td align="left"><h1>Property Manager:  <xsl:value-of select="@LockboxName"/></h1></td>
      <td align="right"><h1>Deposit Date: <xsl:value-of select="@DepositDate"/></h1></td>
     </tr>
     <tr>
      <td align="left"><h1>Workgroup Number: <xsl:value-of select="@LockboxNumber"/></h1></td>
     </tr>
     <xsl:if test="Recordset[@Name='HOAPMAAllSummaryData']">
      <xsl:apply-templates select="Recordset[@Name = 'HOAPMAAllSummaryData']" mode="HOAPMAAllSummaryData"/>
     </xsl:if>
    </table>   
   </xsl:for-each>

   <xsl:if test="Lockbox">
    <table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
     <tr><td>&nbsp;</td></tr>
     <tr>
      <td class="hoagrandtotalfooter" align="right" valign="top" width="30%">&nbsp;</td>
      <td class="hoagrandtotalfooter" align="right" valign="top" width="20%">Grand Total:</td>
      <td class="hoagrandtotalfooter" align="right" valign="top" width="20%"><xsl:value-of select="@GrandTotalCount"/></td>
      <td class="hoagrandtotalfooter" align="right" valign="top" width="27%"><xsl:value-of select="format-number(@GrandTotalAmount,'#.00')"/></td>
      <td class="hoagrandtotalfooter" align="right" valign="top" width="3%">&nbsp;</td>
     </tr>
     </table>
   </xsl:if>
    
 </xsl:template>

 <xsl:template match="Recordset" mode="HOAPMAAllSummaryData">
  <tr>
   <td align="left" valign="top" colspan="2">
    <table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
     <tr>
      <td class="hoareportheading" align="left" valign="top">HOA Number</td>
      <td class="hoareportheading" align="left" valign="top">HOA Name</td>
      <td class="hoareportheading" align="right" valign="top">Bank Account Number</td>
      <td class="hoareportheading" align="right" valign="top">Transaction Count</td>
      <td class="hoareportheading" align="right" valign="top">Total Transaction Amount</td>
      <td class="hoareportheading">&nbsp;</td>
     </tr>
     <xsl:for-each select="Record">
      <tr>
       <xsl:if test='position() mod 2 = 0'>
        <xsl:attribute name="class">evenrow</xsl:attribute>
       </xsl:if>
       <xsl:if test='position() mod 2 != 0'>
        <xsl:attribute name="class">oddrow</xsl:attribute>
       </xsl:if>

       <td class="reportdata" align="left" valign="top"><xsl:value-of select="@AccountNumber"/></td>
       <td class="reportdata" align="left" valign="top"><xsl:value-of select="@HOA_NAME"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="@HOA_DDANO"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="@StubCount"/></td>
       <td class="reportdata" align="right" valign="top"><xsl:value-of select="format-number(@StubAmount,'#.00')"/></td>
       <td class="reportdata">&nbsp;</td>

      </tr>
     </xsl:for-each>
     <tr>
      <td class="hoareportfooter" align="right" valign="top" colspan="3">Total:</td>
      <td class="hoareportfooter" align="right" valign="top"><xsl:value-of select="../@TotalTransactionCount"/></td>
      <td class="hoareportfooter" align="right" valign="top"><xsl:value-of select="format-number(../@TotalTransactionAmount,'#.00')"/></td>
      <td class="hoareportfooter">&nbsp;</td>
     </tr>
    </table>
   </td>
  </tr>
 </xsl:template>
</xsl:stylesheet>
