<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
 	<!ENTITY copy "&#169;">
 	<!ENTITY middot "&#183;">
 	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	Alerts
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Alerts</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">
	<xsl:if test="Recordset[@Name='Alerts']">
		<xsl:apply-templates select="Recordset[@Name='Alerts']" mode="Alerts"/>
		<xsl:apply-templates select="Recordset[@Name='Alerts']" mode="PageNavigation"/>
	</xsl:if>
</xsl:template>


<xsl:template match="Recordset" mode="Alerts">
	<p/>
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
		<caption class="reportcaption">Alerts</caption>
		<tr>
			<td align="left" valign="top" class="recordcount">
				Showing Results <xsl:value-of select="@StartRecord"/> - <xsl:value-of select="@EndRecord"/>&nbsp;of&nbsp;<xsl:value-of select="@TotalRecords"/>
			</td>
		</tr>
		<tr>
			<td align="left" valign="top">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
					<tr>
						<td class="reportheading" align="left" valign="top">Workgroup</td>
						<td class="reportheading" align="left" valign="top">Description</td>
						<td class="reportheading" align="left" valign="top">Rule</td>
						<td class="reportheading" align="left" valign="top">Action</td>
						<td class="reportheading" align="right" valign="top" width="16px">&nbsp;</td>
					</tr>
					<xsl:for-each select="Record">
						<tr>
							<xsl:variable name="linkurl">alertmaint.aspx?id=<xsl:value-of select="Fld[@ID='EventRuleID']"/></xsl:variable>
							
							<xsl:if test="position() mod 2 = 0">
								<xsl:attribute name="class">evenrow</xsl:attribute>
							</xsl:if>
							<xsl:if test="position() mod 2 != 0">
								<xsl:attribute name="class">oddrow</xsl:attribute>
							</xsl:if>
							
								<td align="left" valign="top" class="reportdata">
								<xsl:choose>
									<xsl:when test="number(Fld[@ID='LockboxID']) &lt; 0">
										<xsl:value-of select="Fld[@ID='CustomerName']"/>&nbsp;(<xsl:value-of select="Fld[@ID='CustomerID']"/>)	
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="Fld[@ID='LockboxID']"/>&nbsp;-&nbsp;<xsl:value-of select="Fld[@ID='LockboxName']"/>
									</xsl:otherwise>
								</xsl:choose>
								</td>
								<td align="left" valign="top" class="reportdata"><xsl:value-of select="Fld[@ID='EventRuleDescription']"/></td>
								<td align="left" valign="top" class="reportdata">
									<xsl:value-of select="Fld[@ID='EventName']"/>
									
									<xsl:choose>
										<xsl:when test="string(Fld[@ID='EventOperator']) = string('=')">&nbsp;Equal To</xsl:when>
										<xsl:when test="string(Fld[@ID='EventOperator']) = string('!=')">&nbsp;Not Equal To</xsl:when>
										<xsl:when test="string(Fld[@ID='EventOperator']) = string('&gt;')">&nbsp;Greater Than</xsl:when>
										<xsl:when test="string(Fld[@ID='EventOperator']) = string('&lt;')">&nbsp;Less Than</xsl:when>
									</xsl:choose>
									
									<xsl:choose>
										<xsl:when test="string(Fld[@ID='EventArgumentDescription']) = string('Value')">&nbsp;<xsl:value-of select="Fld[@ID='EventRuleValue']"/></xsl:when>
										<xsl:otherwise>&nbsp;<xsl:value-of select="Fld[@ID='EventArgumentDescription']"/></xsl:otherwise>
									</xsl:choose>
								</td>								
								<td align="left" valign="top" class="reportdata">
									<xsl:choose>
										<xsl:when test="contains(Fld[@ID='EventAction'], '&lt;Message&gt;')">Send Message:&nbsp;</xsl:when>
										<xsl:when test="contains(Fld[@ID='EventAction'], '&lt;Report&gt;')">
											<xsl:choose>
												<xsl:when test="contains(Fld[@ID='EventAction'], ',')">Send Report Group:&nbsp;</xsl:when>
												<xsl:otherwise>Send Report:&nbsp;</xsl:otherwise>
											</xsl:choose>
										</xsl:when>
										<xsl:when test="contains(Fld[@ID='EventAction'], '&lt;Extract&gt;')">Send Extract:&nbsp;</xsl:when>
										<xsl:otherwise>Send:&nbsp;</xsl:otherwise>
									</xsl:choose>
									
									<xsl:value-of select="substring-after(Fld[@ID='EventAction'], '&gt;')"/>
								</td>
								<td align="right" valign="top" width="16" class="reportdata">
									<!--xsl:choose>
										<xsl:when test="Fld[@ID='OnlineModifiable'] = '1' and /Page/UserInfo/Permissions/Permission[@Script='alertmaint.aspx'][@Mode='modify']">
											<a title="Edit Alert">
												<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>&amp;mode=modify</xsl:attribute>
												<img src="{$brandtemplatepath}/Images/cbo_icon_view.gif" border="0" alt="Edit Alert"/>
											</a>						
										</xsl:when>
										<xsl:otherwise-->
											<a title="View Alert Detail">
												<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>&amp;mode=view</xsl:attribute>
												<img src="{$brandtemplatepath}/Images/cbo_icon_view.gif" border="0" alt="View Alert Detail"/>
											</a>									
										<!--/xsl:otherwise>
									</xsl:choose-->
								</td>
						</tr>					
					</xsl:for-each>
				</table>
			</td>
		</tr>
	</table>	
</xsl:template>


<xsl:template match="Recordset" mode="PageNavigation">
	<xsl:variable name="linkurl">viewalerts.aspx</xsl:variable>

	<p/>
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td class="pagelinks" align="right" valign="top">
			<xsl:choose>
				<xsl:when test="number(@StartRecord) - number(@DisplayRecords) &gt;= 1">
					<span class="pagelinkssymbolson">&laquo;</span>&nbsp;
					<a title="View First Page">
						<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>?start=1</xsl:attribute>First
					</a>
					&nbsp;&nbsp;|&nbsp;&nbsp;
					<span class="pagelinkssymbolson">&laquo;</span>&nbsp;
					<a title="View Previous Page">
						<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="@PreviousStartRecord"/></xsl:attribute>Previous
					</a>
				</xsl:when>
				<xsl:otherwise>
					<span class="pagelinkssymbolsoff">&laquo;</span>&nbsp;First&nbsp;&nbsp;|&nbsp;&nbsp;
					<span class="pagelinkssymbolsoff">&laquo;</span>&nbsp;Previous
				</xsl:otherwise>
			</xsl:choose>

			&nbsp;&nbsp;|&nbsp;&nbsp;

			<xsl:choose>
				<xsl:when test="number(@StartRecord) + (number(@DisplayRecords) - 1) &lt; number(@TotalRecords)">
					<a title="View Next Page">
						<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="@NextStartRecord"/></xsl:attribute>Next
					</a>&nbsp;
					<span class="pagelinkssymbolson">&raquo;</span>&nbsp;&nbsp;|&nbsp;&nbsp;
					<a title="View Last Page">
						<xsl:attribute name="href"><xsl:value-of select="$linkurl"/>?start=<xsl:value-of select="@LastStartRecord"/></xsl:attribute>Last
					</a>&nbsp;
					<span class="pagelinkssymbolson">&raquo;</span>
				</xsl:when>
				<xsl:otherwise>
					Next&nbsp;<span class="pagelinkssymbolsoff">&raquo;</span>&nbsp;&nbsp;|&nbsp;&nbsp;
					Last&nbsp;<span class="pagelinkssymbolsoff">&raquo;</span>
				</xsl:otherwise>
			</xsl:choose>
			</td>
		</tr>
	</table>
</xsl:template>

</xsl:stylesheet>
