<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:include href="incPagination.xsl"/>
  <xsl:include href="incCommon.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
	Payment Search
  </xsl:template>

  <xsl:template name="PageTitle">
	<span class="contenttitle">Payment Search</span>
	<br/>
  </xsl:template>

  <xsl:template match="Page">
	<link rel="stylesheet" href="/Assets/iviewer/jquery.iviewer.css" />
	<script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/assignPayer.js" />
	<script type="text/javascript" src="/Assets/iviewer/jquery.iviewer.js"></script>
	<script type="text/javascript" src="/Assets/wfs/wfs-imageviewer.js"></script>
	<div class="page-portlet-view" id="parentContainer">
	  <form method="post">
		<xsl:attribute name="name">frmRemitSearch</xsl:attribute>
		<xsl:if test="Form[@Name='frmRemittanceSearch'] and not(Recordset[@Name='RemittanceSearch'])">
		  <xsl:apply-templates select="Form[@Name = 'frmRemittanceSearch']" mode="frmRemittanceSearch"/>
		</xsl:if>

		<xsl:if test="Form[@Name='frmRemittanceSearch'] and Recordset[@Name='RemittanceSearch']">
		  <xsl:apply-templates select="Form[@Name='frmRemittanceSearch']" mode="hidden"/>
		  <xsl:apply-templates select="Recordset[@Name = 'RemittanceSearch']" mode="RemittanceSearch"/>
		</xsl:if>
	  </form>
	</div>
  </xsl:template>

  <xsl:template match="Form" mode="frmRemittanceSearch">
	<br />
	<input type="hidden" name="txtAction" value="" />
  <input type="hidden" id="txtSelectedWorkgroupCount" name="txtSelectedWorkgroupCount" value="0" />
	<div id="divCalendar" style="position: absolute; background-color: white; visibility: hidden;"></div>
	<table width="600" cellpadding="0" cellspacing="0" border="0" align="center" class="formmain">
	  <tr>
		<td align="left" valign="top">
		  <table width="100%" cellpadding="0" cellspacing="1" border="0">
			<tr>
			  <td align="left" valign="top">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="formsub">
				  <tr>
					<td align="right">
            Entity/Workgroup:
          </td>
					<td>
					  <div id="treeBase" style="width: 375px;">
						<input type="hidden" id="txtWorkgroupSelection" name="txtWorkgroupSelection">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right">
					  Payer:
					</td>
					<td align="left" class="formfield">
					  <div style="width: 300px;">
						<input type="text" name="txtRemitterName" maxlength="60" class="form-control">
						  <xsl:attribute name="value">
							<xsl:value-of select="Field[@Name='txtRemitterName']/@Value"/>
						  </xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right"  style="width: 170px;">
					  <nowrap>DDA:</nowrap>
					</td>
					<td align="left" class="formfield">
					  <div style="width: 300px;display: inline-block;">
						<input type="text" name="txtDDA" maxlength="30" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDDA']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right">
					  Deposit Date:<br /><small>(mm/dd/yyyy)</small>
					</td>
					<td align="left" class="formfield">
					  <div>
						<div id="StartDatePicker"></div>
						<input type="text" name="txtDateFrom" id="txtDateFrom" style="display:none">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDateFrom']/@Value"/></xsl:attribute>
						</input>
						<div style="margin: 10px 2px 5px 5px; display: inline-block; vertical-align: top;">
						  <i class="fa fa-minus"></i>
						</div>
						<div id="EndDatePicker"></div>
						<input type="text" name="txtDateTo" id="txtDateTo" style="display:none">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDateTo']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right" >Payment Amount:</td>
					<td align="left" class="formfield">
					  <div style="width: 140px;display: inline-block;">
						<input type="text" name="txtAmountFrom" maxlength="11" size="11" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAmountFrom']/@Value"/></xsl:attribute>
						</input>
					  </div>
					  <div style="margin: 8px 5px 5px 5px; display: inline-block; vertical-align: top;">
						<i class="fa fa-minus"></i>
					  </div>
					  <div style="width: 140px;display: inline-block;">
						<input type="text" name="txtAmountTo" maxlength="11" size="11" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAmountTo']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right"  style="width: 170px;">
					  <nowrap>Account Number:</nowrap>
					</td>
					<td align="left" class="formfield">
					  <div style="width: 300px;display: inline-block;">
						<input type="text" name="txtAccount" size="10" maxlength="30" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAccount']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right"  style="width: 170px;">
					  <nowrap>Check/Trace/Ref Number:</nowrap>
					</td>
					<td align="left" class="formfield">
					  <div style="width: 300px;display: inline-block;">
						<input type="text" name="txtCheckSerial" size="10" maxlength="30" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtCheckSerial']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right"  style="width: 170px;">
					  <nowrap>R/T:</nowrap>
					</td>
					<td align="left" class="formfield">
					  <div style="width: 300px;display: inline-block;">
						<input type="text" name="txtRT" size="10" maxlength="30" class="form-control">
						  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtRT']/@Value"/></xsl:attribute>
						</input>
					  </div>
					</td>
				  </tr>
				  <tr>
					<td align="right">
					  Payment Source:
					</td>
					<td align="left" class="formfield">
            <div style="width: 300px;display: inline-block;">
              <xsl:variable name="PaymentSourceID">
                <xsl:value-of select="Field[@Name='txtPaymentSourceID']/@Value"/>
              </xsl:variable>
              <select name="cboPaymentSource" id ="cboPaymentSource" class="formfield">
                <option value="-1">-- All --</option>
                <xsl:for-each select="Field[@Name='cboPaymentSources']">
                  <option>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@Value"/>
                    </xsl:attribute>
                    <xsl:if test="@Value=$PaymentSourceID">
                      <xsl:attribute name="SELECTED"></xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="@PaymentSource"/>
                  </option>
                </xsl:for-each>
              </select>
              <div class="btn-group entitySelect" id="PaymentSourceSelect"></div>
            </div>
					</td>
				  </tr>
				  <tr>
					<td align="right">
					  Payment Type:
					</td>
					<td align="left"  class="formfield">
            <div style="width: 300px;display: inline-block;">
              <xsl:variable name="PaymentTypeID">
                <xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/>
              </xsl:variable>
              <select name="cboPaymentType" id="cboPaymentType" class="formfield">
                <option value="-1">-- All --</option>
                <xsl:for-each select="Field[@Name='cboPaymentTypes']">
                  <option>
                    <xsl:attribute name="value">
                      <xsl:value-of select="@Value"/>
                    </xsl:attribute>
                    <xsl:if test="@Value=$PaymentTypeID">
                      <xsl:attribute name="SELECTED"></xsl:attribute>
                    </xsl:if>
                    <xsl:value-of select="@PaymentType"/>
                  </option>
                </xsl:for-each>
              </select>
              <div class="btn-group entitySelect" id="PaymentTypeSelect" ></div>
            </div>
					</td>
				  </tr>

				  <tr>
					<td align="right">Sort By:</td>
					<td align="left" class="formfield">
            <div style="width: 300px;display: inline-block;">
              <xsl:variable name="SortByText">
                <xsl:value-of select="Field[@Name='cboSortBy']/@Value"/>
              </xsl:variable>
              <select id="cboSortBy" name="cboSortBy" class="formfield" style="display: none;">
                <option>
                  <xsl:attribute name="value">ClientAccountID~ASCENDING</xsl:attribute>Workgroup / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">ClientAccountID~DESCENDING</xsl:attribute>Workgroup / Descending
                </option>
                <option>
                  <xsl:attribute name="value">DepositDate~ASCENDING</xsl:attribute>Deposit Date / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">DepositDate~DESCENDING</xsl:attribute>Deposit Date / Descending
                </option>
                <option>
                  <xsl:attribute name="value">SourceBatchID~ASCENDING</xsl:attribute><xsl:call-template name="batchidlabel" /> / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">SourceBatchID~DESCENDING</xsl:attribute><xsl:call-template name="batchidlabel" /> / Descending
                </option>
                <option>
                  <xsl:attribute name="value">BatchNumber~ASCENDING</xsl:attribute><xsl:call-template name="batchnumberlabel" /> / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">BatchNumber~DESCENDING</xsl:attribute><xsl:call-template name="batchnumberlabel" /> / Descending
                </option>
                <option>
                  <xsl:attribute name="value">TxnSequence~ASCENDING</xsl:attribute>Transaction / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">TxnSequence~DESCENDING</xsl:attribute>Transaction / Descending
                </option>
                <option>
                  <xsl:attribute name="value">Amount~ASCENDING</xsl:attribute>Payment Amount / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">Amount~DESCENDING</xsl:attribute>Payment Amount / Descending
                </option>
                <option>
                  <xsl:attribute name="value">RT~ASCENDING</xsl:attribute>R/T / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">RT~DESCENDING</xsl:attribute>R/T / Descending
                </option>
                <option>
                  <xsl:attribute name="value">DDA~ASCENDING</xsl:attribute>DDA / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">DDA~DESCENDING</xsl:attribute>DDA / Descending
                </option>
                <option>
                  <xsl:attribute name="value">Account~ASCENDING</xsl:attribute>Account Number / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">Account~DESCENDING</xsl:attribute>Account Number / Descending
                </option>
                <option>
                  <xsl:attribute name="value">Serial~ASCENDING</xsl:attribute>Check/Trace/Ref Number / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">Serial~DESCENDING</xsl:attribute>Check/Trace/Ref Number / Descending
                </option>
                <option>
                  <xsl:attribute name="value">Remitter~ASCENDING</xsl:attribute>Payer / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">Remitter~DESCENDING</xsl:attribute>Payer / Descending
                </option>
                <option>
                  <xsl:attribute name="value">PaymentSource~ASCENDING</xsl:attribute>Payment Source / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">PaymentSource~DESCENDING</xsl:attribute>Payment Source / Descending
                </option>
                <option>
                  <xsl:attribute name="value">PaymentType~ASCENDING</xsl:attribute>Payment Type / Ascending
                </option>
                <option>
                  <xsl:attribute name="value">PaymentType~DESCENDING</xsl:attribute>Payment Type / Descending
                </option>
              </select>
              <div class="btn-group entitySelect" id="SortBySelect" ></div>
            </div>
					</td>
				  </tr>
				  <tr>
					<td align="center" colspan="2">
					  <input type="button" name="cmdSearch" value="Search" class="btn btn-primary" onClick="formSubmit(this, 'txtAction');" />&nbsp;
					  <input type="button" name="cmdClear" value="Clear Search" class="btn btn-primary" onClick="location.href='remitsearch.aspx';" />
					</td>
				  </tr>
				  <tr>
					<td colspan="2" style="height: 3px;">&nbsp;</td>
				  </tr>
				</table>
			  </td>
			</tr>
		  </table>
		</td>
	  </tr>
	</table>

	<script language="JavaScript">
	  <xsl:comment>
		//Hide script from older browsers
		//Set form field validation requirements
		with (document.forms["frmRemitSearch"]){
		if (elements["txtDateFrom"]){
		txtDateFrom.isDate = true;
		txtDateFrom.isRequired = true;
		txtDateFrom.fieldname = 'Deposit Date From';
		}

		if (elements["txtDateTo"]){
		txtDateTo.isDate = true;
		txtDateTo.isRequired = false;
		txtDateTo.fieldname = 'Deposit Date To';
		}
		}
		//End hiding



		$(document).ready(function() {

    var maxworkgroupssetting = <xsl:value-of select="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='MaximumWorkgroups']/@DefaultSettings"/>;


		//**************************************************************
		//  Workgroup Selector
		//**************************************************************

		var _vm = new ViewModel();

		var dataCallback = function (resp, widget) {
		  var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
		  var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
		  var ecount = entities.length;
		  var wcount = workgroups.length;

		  if (ecount === 1 &#38;&#38; wcount === 1)
		  {
			var el = $('&lt;input id="txtWorkgroupSelection" name="txtWorkgroupSelection" value="" type="hidden"&gt;&lt;div&gt;&lt;label class="control-label"&gt;&lt;/label&gt;&lt;input value="'+ workgroups[0].label+ '" class="form-control input-md" type="text" readonly="" style="display:inline;" /&gt;&lt;/div&gt;');
      $('#treeBase').html(el);
      _vm['selectedWorkgroup'] = workgroups[0];
      $('#txtWorkgroupSelection').val(workgroups[0].id);
      }
      else
      {
      _vm.workGroupSelector.wfsTreeSelector('initialize');
      if($('#txtWorkgroupSelection').val())
      {
      widget.setSelection($('#txtWorkgroupSelection').val());
      }
      }

      };


      var selectedTreeItem = function (selected, initial) {
        var title = null;
        if (selected &#38;&#38; !initial) {
          
          var count = getWorkgroupCount(selected);
          // If we're over the max count, then we have to select another.
          if (count &gt; maxworkgroupssetting)
          {
            var errormsg = 'The number of workgroups for the selected Entity hierarchy is ' + count + '. &lt;br/&gt; The system will only allow ' + maxworkgroupssetting + ' workgroups in a search. &lt;br/&gt; Please select a lower level entity or contact your administrator.';
            framework.errorToast($('#tabbase'), errormsg);
            return;
          }
          
          setWorkgroupCount(selected);
          title = 'Selected: ' + selected['label'];
          _vm.workGroupSelector.wfsTreeSelector('setTitle', title);
          $('#txtWorkgroupSelection').val(selected.id);
        }
      };

      var getWorkgroupCount = function(entity)
      {
        var count = 0;
        if (entity.isNode)
          return 1;
        $.each(entity.items, function(ind,el)
        {
          if (el.isNode)
            count++;
          else
            count+=getWorkgroupCount(el);
        });
        return count;
      }

      var setWorkgroupCount = function(entity) {
        var count = 0;
        if (entity.isNode) {
          count = 1;
        } else {
          for (var i = 0; i &lt; entity.items.length;  i++) {
      var child = entity.items[i];
      if (child.isNode) {
      count++;
      } else {
      count = getItemCount(child, count);
      }
      }
      }
      
      document.getElementById('txtSelectedWorkgroupCount').value = count;
      return count;
      };

      var getItemCount = function(entity, count) {

      if (entity &#38;&#38; entity.items) {
          for (var i = 0; i &lt; entity.items.length;  i++) {
               var child = entity.items[i];
               if (child.isNode) {
                  count++;
               } else {
                 if (child.items.length &gt; 0)
      {
      return getItemCount(child, count);
      }
      }
      }
      }

      return count;
      };


      _vm.workGroupSelector = $('#treeBase').wfsTreeSelector({
      useExpander: true,
      entityURL: '/RecHubRaamProxy/api/entity',
      callback: selectedTreeItem,
      dataCallback: dataCallback,
      expanderTitle: "Select Entity/Workgroup",
      entitiesOnly: false
      });

      function ViewModel(model) {
      var self = this;
      self.model = model;

      self.labels = model.Labels;
      self.selectedWorkgroup = null;
      };

      //**************************************************************

      $(".showTip").tooltip();

      var startDate = new Date($('#txtDateFrom').val());
      var endDate = new Date($('#txtDateTo').val());
      //**************************************************************
      //  Start Date DatePicker
      //**************************************************************

      $('#StartDatePicker').wfsDatePicker({dateIn: startDate, callback: startDateChange});
      //**************************************************************



      //**************************************************************
      //  End Date DatePicker
      //**************************************************************

      $('#EndDatePicker').wfsDatePicker({dateIn: endDate, callback: endDateChange});
      //**************************************************************


      //**************************************************************
      //  Lockbox Select
      //**************************************************************
      var fromXML = [{ Id: 'all', Name: '-- All Workgroups --' },<xsl:for-each select="Field[@Name='cboOLLockbox']">
        {Id: '<xsl:value-of select="@Value"/>', Name: '<xsl:apply-templates select="@LongName" mode="escape"/>' },
      </xsl:for-each> {Id: 'remove', Name: 'remove'} ];

      findAndRemove(fromXML, 'Id', 'remove');

      function ViewModel(items) {
      var self = this;
      self.items = items;
      self.setItemValue = function (data) {
      $('select[name=cboOLLockbox] option').removeAttr('selected');
      if (data) {
      $('select[name=cboOLLockbox] option[value="' + data.Id + '"]').attr('selected', true);
      }
      }
      }

      //**************************************************************

      //**************************************************************
      //  Sort By Select
      //**************************************************************

      var origElement = $('#cboSortBy');
      var selectedSortItem = '<xsl:value-of select="Field[@Name='cboSortBy']/@Value"/>';
		var sortVM = new SelectViewModel(origElement, selectedSortItem);

		$('#SortBySelect').wfsSelectbox({ 'items': sortVM.items, 'callback': sortVM.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
		var selSortObj = ($.grep(sortVM.items, function(e){ return e.id == selectedSortItem; }))[0];
		if (selSortObj)
		$('#SortBySelect').wfsSelectbox('setValueByData', selSortObj);


		//**************************************************************
		//  Payment Source Select
		//**************************************************************
		origElement = $('#cboPaymentSource');
		var selectedPSItem = '<xsl:value-of select="Field[@Name='txtPaymentSourceID']/@Value"/>';
		var psVM = new SelectViewModel(origElement, selectedPSItem);

		$('#PaymentSourceSelect').wfsSelectbox({ 'items': psVM.items, 'callback': psVM.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
		var selPSObj = ($.grep(psVM.items, function(e){ return e.id == selectedPSItem; }))[0];
		if (selPSObj)
		$('#PaymentSourceSelect').wfsSelectbox('setValueByData', selPSObj);


		//**************************************************************
		//  Payment Type Select
		//**************************************************************
		origElement = $('#cboPaymentType');
		var selectedPTItem = '<xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/>';
      var ptVM = new SelectViewModel(origElement, selectedPTItem);

      $('#PaymentTypeSelect').wfsSelectbox({ 'items': ptVM.items, 'callback': ptVM.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
      var selPTObj = ($.grep(ptVM.items, function(e){ return e.id == selectedPTItem; }))[0];
      if (selPTObj)
      $('#PaymentTypeSelect').wfsSelectbox('setValueByData', selPTObj);

      //**************************************************************
      // VM
      //**************************************************************
      function SelectViewModel(selector, selSortObj) {
      var self = this;
      var $target = selector;

      $target.hide();
      self.items = [];
      $('option', selector).each(function(i) {self.items.push({Id: $(this).val(), Name: $(this).text()}); });

      self.selectedItem = selSortObj;
      self.setItemValue = function (data) {
      self.selectedItem = data;
      $('option', $target).removeAttr('selected');
      if (data){
      $('option[value="' + data.Id + '"]', $target).attr('selected', true);
      }
      }
      }


      });
      
      function findAndRemove(array, property, value) {
      $.each(array, function(index, result) {
      if(result[property] == value) {
      //Remove from array
      array.splice(index, 1);
      }
      });
      }

      function findIndex(array, property, value) {
      var newIndex = 0;
      $.each(array, function(index, result) {
      if(result[property] == value) {
      newIndex = index;
      }
      });

      return newIndex;

      }

      function startDateChange(val) {
      document.getElementById('txtDateFrom').value = val;
      }

      function endDateChange(val) {
      document.getElementById('txtDateTo').value = val;
      }

    </xsl:comment>
	</script>

  </xsl:template>

  <xsl:template match="Form" mode="hidden">

	<input type="hidden" name="txtAction" value="" />
  <input type="hidden" name="txtSelectedWorkgroupCount" >
    <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtSelectedWorkgroupCount']/@Value"/></xsl:attribute>
  </input>
	<input type="hidden" name="txtWorkgroupSelection">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtRemitterName">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtRemitterName']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="cboOLLockbox">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtOLLockboxID']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtDateFrom">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDateFrom']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtDateTo">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDateTo']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtAmountFrom">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAmountFrom']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtAmountTo">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAmountTo']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtCheckSerial">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtCheckSerial']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtDDA">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtDDA']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtRT">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtRT']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="txtAccount">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtAccount']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="cboPaymentSource">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtPaymentSourceID']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="cboPaymentType">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/></xsl:attribute>
	</input>
	<input type="hidden" name="cboSortBy">
	  <xsl:attribute name="value"><xsl:value-of select="Field[@Name='cboSortBy']/@Value"/></xsl:attribute>
	</input>

  </xsl:template>

  <xsl:template match="Recordset" mode="RemittanceSearch">
  <xsl:variable name="WorkgroupSelectionCount"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtSelectedWorkgroupCount']/@Value"/></xsl:variable>
	<xsl:variable name="WorkgroupSelection"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:variable>
	<xsl:variable name="remittername"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtRemitterName']/@Value"/></xsl:variable>
	<xsl:variable name="olClientAccountID"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtOLLockboxID']/@Value"/></xsl:variable>
	<xsl:variable name="datefrom"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtDateFrom']/@Value"/></xsl:variable>
	<xsl:variable name="dateto"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtDateTo']/@Value"/></xsl:variable>
	<xsl:variable name="amountfrom"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtAmountFrom']/@Value"/></xsl:variable>
	<xsl:variable name="amountto"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtAmountTo']/@Value"/></xsl:variable>
	<xsl:variable name="checkserial"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtCheckSerial']/@Value"/></xsl:variable>
	<xsl:variable name="dda"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtDDA']/@Value"/></xsl:variable>
	<xsl:variable name="rt"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtRT']/@Value"/></xsl:variable>
	<xsl:variable name="account"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtAccount']/@Value"/></xsl:variable>
	<xsl:variable name="sortby"><xsl:value-of select="../Form[@Name='frmRemittanceSearch']/Field[@Name='txtSortBy']/@Value"/></xsl:variable>
	<xsl:variable name="startrecord"><xsl:value-of select="../PageInfo/@StartRecord"/></xsl:variable>
	<xsl:variable name="addremitterpermission"><xsl:choose><xsl:when test="../UserInfo/Permissions/Permission[@Name='AssignPayer' and @Mode='Manage']">true</xsl:when><xsl:otherwise>false</xsl:otherwise></xsl:choose></xsl:variable>

	<input type="hidden" name="txtStart">
	  <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
	</input>

	<br />


    <script type="text/javascript">
        function storeBatchDetailParams(bank, lbx, batch, date) {

          var parameters = {
            BankId: bank,
            WorkgroupId: lbx,
            BatchId: batch,
            DepositDateString: date,
            Source: "Payment Search"
          };

          window.localStorage.setItem("batchdetailparams", JSON.stringify(parameters));
          window.localStorage.setItem("breadcrumb_local_storage", JSON.stringify([{Name: "Payment Search", URL: "/IPOnline/Scripts/remitsearch.aspx?id=bc", Data: null }]));
          return true;
        }
        
        
        function storeTransactionDetailParams(bank, lbx, batch, date, tid, tseq) {

          var parameters = {
            BankId: bank,
            WorkgroupId: lbx,
            BatchId: batch,
            DepositDateString: date,
            TransactionID: tid,
            TransactionSequence: tseq
          };

          window.localStorage.setItem("transactiondetailparams", JSON.stringify(parameters));
          window.localStorage.setItem("breadcrumb_local_storage", JSON.stringify([{Name: "Payment Search", URL: "/IPOnline/Scripts/remitsearch.aspx?id=bc", Data: null }]));
          return true;
        }

    </script>

    <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0" style="background-color: #ffffff;">
	  <tr>
		<td colspan="2">Results</td>
	  </tr>
	  <tr>
		<td align="left">
		  &nbsp;
		</td>
		<td align="right" class="formlabel">
		  <a href="javascript:document.forms['frmRemitSearch'].txtAction.value='REFINE';InsertSecurityToken(document.forms['frmRemitSearch'].txtAction);document.forms['frmRemitSearch'].submit();">
			Refine Search
		  </a>
		</td>
	  </tr>
	  <tr>
		<td colspan="2">

		  <table cellpadding="0" cellspacing="0" width="100%" border="0" class="grid" style="background-color: #ffffff;">
			<tr>
			  <td align="left" valign="top">

				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid" style="background-color: #ffffff;">
				  <tr>
					<td class="grid-header grid-header-left">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('ClientAccountID');</xsl:attribute>
						Workgroup
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('DepositDate');</xsl:attribute>
						Deposit Date
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('SourceBatchID');</xsl:attribute>
						<xsl:call-template name="batchidlabel" />
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('BatchNumber');</xsl:attribute>
						<xsl:call-template name="batchnumberlabel" />
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('PaymentSource');</xsl:attribute>
						Payment Source
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('PaymentType');</xsl:attribute>
						Payment Type
					  </a>
					</td>

					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('TxnSequence');</xsl:attribute>
						Transaction
					  </a>
					</td>
					<td width="16px" class="grid-header">&nbsp;</td>
					<td class="grid-header cell-right-align">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('Amount');</xsl:attribute>
						Payment Amount
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('RT');</xsl:attribute>
						R/T
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('Account');</xsl:attribute>
						Account Number
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('Serial');</xsl:attribute>
						Check/Trace/Ref Number
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('Remitter');</xsl:attribute>
						Payer
					  </a>
					</td>
					<td class="grid-header">
					  <a href="#">
						<xsl:attribute name="onClick">ColumnSort('DDA');</xsl:attribute>
						DDA
					  </a>
					</td>
				  </tr>

				  <xsl:for-each select="Record">

					<xsl:variable name="addremitterparms">lckbx=<xsl:value-of select="Fld[@ID='OLClientAccountID']"/>&amp;bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;page=0&amp;rt=<xsl:value-of select="Fld[@ID='RT']"/>&amp;acct=<xsl:value-of select="Fld[@ID='Account']"/>&amp;gbatch=<xsl:value-of select="Fld[@ID='GlobalBatchID']"/>&amp;gcheck=<xsl:value-of select="Fld[@ID='GlobalCheckID']"/>&amp;tran=<xsl:value-of select="Fld[@ID='TransactionID']"/></xsl:variable>
					<xsl:variable name="viewdetailparms">lckbx=<xsl:value-of select="Fld[@ID='OLClientAccountID']"/>&amp;bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;return=remitsearch&amp;remitter=<xsl:value-of select="Fld[@ID='RemitterName']" />&amp;box=<xsl:value-of select="Fld[@ID='OLClientAccountID']"/>&amp;datefrom=<xsl:value-of select="$datefrom"/>&amp;dateto=<xsl:value-of select="$dateto"/>&amp;amountfrom=<xsl:value-of select="$amountfrom"/>&amp;amountto=<xsl:value-of select="$amountto"/>&amp;serial=<xsl:value-of select="$checkserial"/>&amp;sort=<xsl:value-of select="$sortby"/>&amp;</xsl:variable>
					<xsl:variable name="viewcheckparms">lckbx=<xsl:value-of select="Fld[@ID='OLClientAccountID']"/>&amp;bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;batchnumber=<xsl:value-of select="Fld[@ID='BatchNumber']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;page=-1</xsl:variable>
					<xsl:variable name="addremitterurl">bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;client=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/>&amp;picsdate=<xsl:value-of select="Fld[@ID='PICSDate']"/>&amp;txn=<xsl:value-of select="Fld[@ID='TransactionID']"/>&amp;item=<xsl:value-of select="Fld[@ID='BatchSequence']"/>&amp;type=c&amp;rt=<xsl:value-of select="Fld[@ID='RT']"/>&amp;acct=<xsl:value-of select="Fld[@ID='Account']"/>&amp;srcBatchID=<xsl:value-of select="Fld[@ID='SourceBatchID']" />&amp;batchSourceShortName=<xsl:value-of select="Fld[@ID='BatchSourceShortName']"/>&amp;importTypeShortName=<xsl:value-of select="Fld[@ID='ImportTypeShortName']"/></xsl:variable>
					<tr>
					  <xsl:if test='position() mod 2 = 0'>
						<xsl:attribute name="class">evenrow</xsl:attribute>
					  </xsl:if>
					  <xsl:if test='position() mod 2 != 0'>
						<xsl:attribute name="class">oddrow</xsl:attribute>
					  </xsl:if>

					  <td class="grid-cell grid-cell-left">
						<xsl:value-of select="Fld[@ID='ClientAccountID']" />
					  </td>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='Deposit_Date']" />
					  </td>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='SourceBatchID']" />
					  </td>
					  <td class="grid-cell">
              <xsl:variable name="bank">
                <xsl:value-of select="Fld[@ID='BankID']"/>
              </xsl:variable>
              <xsl:variable name="lbx">
                <xsl:value-of select="Fld[@ID='ClientAccountID']"/>
              </xsl:variable>
              <xsl:variable name="batch">
                <xsl:value-of select="Fld[@ID='BatchID']"/>
              </xsl:variable>
              <xsl:variable name="date">"<xsl:value-of select="Fld[@ID='DepositDate']"/>"</xsl:variable>
              
              <a class="showTip" title="Batch Detail" onclick="storeBatchDetailParams({$bank}, {$lbx}, {$batch}, {$date})">
						  <xsl:attribute name="href">remitdisplay.aspx?bank=<xsl:value-of select="Fld[@ID='BankID']"/>&amp;lbx=<xsl:value-of select="Fld[@ID='ClientAccountID']"/>&amp;batch=<xsl:value-of select="Fld[@ID='BatchID']"/>&amp;date=<xsl:value-of select="Fld[@ID='DepositDate']"/></xsl:attribute>
						  <xsl:value-of select="Fld[@ID='BatchNumber']" />
						</a>
					  </td>
					  <td align="left" valign="top" class="grid-cell">
						<xsl:value-of select="Fld[@ID='PaymentSource']" />
					  </td>
					  <td align="left" valign="top" class="grid-cell">
						<xsl:value-of select="Fld[@ID='PaymentType']" />
					  </td>

					  <td class="grid-cell">
              <xsl:variable name="bank">
                <xsl:value-of select="Fld[@ID='BankID']"/>
              </xsl:variable>
              <xsl:variable name="lbx">
                <xsl:value-of select="Fld[@ID='ClientAccountID']"/>
              </xsl:variable>
              <xsl:variable name="batch">
                <xsl:value-of select="Fld[@ID='BatchID']"/>
              </xsl:variable>
              <xsl:variable name="date">
                "<xsl:value-of select="Fld[@ID='Deposit_Date']"/>"
              </xsl:variable>
              <xsl:variable name="transactionid">
                "<xsl:value-of select="Fld[@ID='TransactionID']"/>"
              </xsl:variable>
              <xsl:variable name="transactionsequence">
                "<xsl:value-of select="Fld[@ID='TxnSequence']"/>"
              </xsl:variable>
						<a class="showTip" title="Transaction Detail" onclick="storeTransactionDetailParams({$bank}, {$lbx}, {$batch}, {$date}, {$transactionid}, {$transactionsequence})">
						  <xsl:attribute name="href">transactiondisplay.aspx?<xsl:value-of select="$viewdetailparms" /></xsl:attribute>
						  <xsl:value-of select="Fld[@ID='TxnSequence']" />
						</a>
					  </td>
					  <td width="16px" class="grid-cell">
						<xsl:if test="Fld[@ID='ImageSize']>0">
						  <a class="showTip">
							<xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$viewcheckparms" /></xsl:attribute>
							<xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$viewcheckparms" />');return false;</xsl:attribute>
							<i class="fa fa-money fa-lg" title="View Check" />
						  </a>
						</xsl:if>
					  </td>
					  <td class="grid-cell cell-right-align">
						$<xsl:value-of select="format-number(Fld[@ID='Amount'],'#,##0.00')" />
					  </td>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='RT']" />
					  </td>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='Account']" />
					  </td>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='Serial']" />
					  </td>

					  <xsl:variable name="PaymentType"><xsl:call-template name="to-lower"><xsl:with-param name="Value"><xsl:value-of select="Fld[@ID='PaymentType']"/></xsl:with-param></xsl:call-template></xsl:variable>
					  <xsl:variable name="PaymentSource"><xsl:call-template name="to-lower"><xsl:with-param name="Value"><xsl:value-of select="Fld[@ID='PaymentSource']"/></xsl:with-param></xsl:call-template></xsl:variable>

					  <xsl:choose>
						  <xsl:when test="Fld[@ID='RemitterName']='' and boolean($addremitterpermission='true')">
							  <td class="grid-cell">
								  <a title="Add Payer to Payment" class="openPayer">
									  <xsl:attribute name="href">
										  <xsl:value-of select="normalize-space($PayerFrameworkURL)"/>GetSuggestedPayer?<xsl:value-of select="$addremitterurl"/>
									  </xsl:attribute>
									  <span class="btn-small">
										  <i class="fa fa-plus-circle"></i>&nbsp;Payer
									  </span>
								  </a>
							  </td>
						  </xsl:when>
						<xsl:otherwise>
						  <td class="grid-cell">
							<xsl:value-of select="Fld[@ID='RemitterName']" />
						  </td>
						</xsl:otherwise>
					  </xsl:choose>
					  <td class="grid-cell">
						<xsl:value-of select="Fld[@ID='DDA']" />
					  </td>
					</tr>
				  </xsl:for-each>
				</table>

			  </td>
			</tr>
			<tr>
			  <td colspan="10">
				<xsl:call-template name="PaginationLinks">
				  <xsl:with-param name="controlname">document.forms['frmRemitSearch'].txtAction</xsl:with-param>
				  <xsl:with-param name="startrecord"><xsl:value-of select="$startrecord" /></xsl:with-param>
				</xsl:call-template>
			  </td>
			</tr>
		  </table>

		</td>
	  </tr>
	  <tr>
		<td colspan="10" style="height: 3px;">&nbsp;</td>
	  </tr>

	</table>

	<script language="javascript">
	  <xsl:comment>
		//Hide script from older browsers
		//Set form field validation requirements
		function ColumnSort(Column){
		var SortDir = '';
		var SortField = '';
		var CurrSortField = '';

		CurrSortField = document.forms['frmRemitSearch'].cboSortBy.value;
		SortDir = CurrSortField.substring(CurrSortField.indexOf('~') + 1, CurrSortField.length);
		SortField = CurrSortField.substring(0, CurrSortField.indexOf('~'));

		if(Column.toLowerCase() == SortField.toLowerCase()){
		switch(SortDir.toLowerCase()){
		case 'ascending':
		SortDir = 'DESCENDING';
		break;
		case 'descending':
		SortDir = 'ASCENDING';
		break;
		default:
		SortDir = 'ASCENDING';
		}
		}
		else {
		SortField = Column;
		SortDir = 'ASCENDING';
		}

		CurrSortField = SortField + '~' + SortDir

		document.forms['frmRemitSearch'].cboSortBy.value = CurrSortField;
		document.forms['frmRemitSearch'].txtAction.value = 'search';
		InsertSecurityToken(document.forms['frmRemitSearch'].txtAction);
		document.forms['frmRemitSearch'].submit();
		}
		//End Hiding

		$(document).ready(function() {

		$(".showTip").tooltip();

		<xsl:text>payerPopup = new assignPayer('</xsl:text><xsl:value-of select="normalize-space($PayerFrameworkURL)"/><xsl:text>');</xsl:text>

		});

	  </xsl:comment>
	</script>

  </xsl:template>

</xsl:stylesheet>
