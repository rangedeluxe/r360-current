<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:variable name="LOGON_METHOD_STD">0</xsl:variable>
<xsl:variable name="LOGON_METHOD_CORBA">1</xsl:variable>
<xsl:variable name="LOGON_METHOD_EXTID1">2</xsl:variable>
<xsl:variable name="LOGON_METHOD_WEBACCESS">3</xsl:variable>
<xsl:variable name="LOGON_METHOD_MFA">4</xsl:variable>
<xsl:variable name="LOGON_METHOD_SSO">5</xsl:variable>

<xsl:variable name="_LogonMethod"><xsl:value-of select="/Page/SystemInfo/LogonMethod/@Value"/></xsl:variable>

</xsl:stylesheet>

  
