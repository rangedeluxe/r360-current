<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
    Batch Summary
  </xsl:template>

  <xsl:variable name="MaxPrintableRows"><xsl:value-of select="/Page/Recordset[@Name='BatchSummary']/PageInfo/@MaxPrintableRows"/></xsl:variable>
  <xsl:variable name="RowCount"><xsl:value-of select ="/Page/Recordset[@Name='BatchSummary']/PageInfo/@TotalRecords"/></xsl:variable>

  <xsl:template name="PageTitle">
    <span class="contenttitle">
      Batch Summary
    </span>
  </xsl:template>

  <xsl:template match="Page">
    <div class="page-portlet-view">
      <xsl:if test="Form[@Name='frmMain']">
        <xsl:apply-templates select="Form[@Name = 'frmMain']" mode="frmMain"/>
      </xsl:if>

      <xsl:if test="Recordset[@Name='BatchSummary']">
        <xsl:apply-templates select="Recordset[@Name = 'BatchSummary']" mode="BatchSummary"/>
      </xsl:if>
    </div>
  </xsl:template>


  <xsl:template match="Recordset" mode="BatchSummary">

    <xsl:if test="Record">
      <xsl:apply-templates select="Record" mode="BatchSummary"/>
    </xsl:if>

  </xsl:template>


  <xsl:template match="Record" mode="BatchSummary">

    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td>
          <table width="100%" cellpadding="5" cellspacing="0" border="0">
            <tr>
              <td>
                <span class="hubLabel">
                  <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">BankID:&nbsp;<xsl:value-of select="@BankID"/><br/></xsl:if>
                  Workgroup:&nbsp;<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']"><xsl:value-of select="@SiteCode"/>&nbsp;-&nbsp;</xsl:if><xsl:value-of select="@LongName"/>
                </span>
              </td>
            </tr>
          </table>
        </td>
      </tr>

      <xsl:if test="Recordset[@Name='BatchDetails']">
        <xsl:apply-templates select="Recordset[@Name = 'BatchDetails']" mode="BatchDetails"/>
      </xsl:if>

    </table>

    <xsl:if test="../PageInfo">
      <xsl:apply-templates select="../PageInfo" mode="PageNavigation"/>
    </xsl:if>

  </xsl:template>


  <xsl:template match="Recordset" mode="BatchDetails">
    <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/Recordset[@Name = 'BatchSummary']/Record/@DisplayBatchID"/></xsl:variable>
    <tr>
      <td align="left" valign="top">
        <table width="100%" cellpadding="5" cellspacing="0" border="0" class="grid">
          <tr>
            <td class="grid-header grid-header-left">&nbsp;</td>
            <xsl:if test="$displaybatchid = 'True'">
              <td class="grid-header">
                <xsl:call-template name="batchidlabel" />
              </td>
            </xsl:if>
            <td class="grid-header">
              <xsl:call-template name="batchnumberlabel" />
            </td>
            <td class="grid-header cell-right-align">
              Deposit Date
            </td>
            <td class="grid-header">
              Payment Source
            </td>
            <td class="grid-header">
              Payment Type
            </td>
            <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
              <td class="grid-header">
                Batch Site Code
              </td>
            </xsl:if>
            <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
              <td class="grid-header">
                <xsl:call-template name="batchcueidlabel" />
              </td>
            </xsl:if>
            <td class="grid-header cell-right-align">Transaction Count</td>
            <td class="grid-header cell-right-align">Payment Count</td>
            <td class="grid-header cell-right-align">Document Count</td>
            <td class="grid-header cell-right-align">Batch Total</td>

          </tr>

          <xsl:for-each select="Record">
            <tr>
              <xsl:if test='position() mod 2 = 0'>
                <xsl:attribute name="class">evenrow</xsl:attribute>
              </xsl:if>
              <xsl:if test='position() mod 2 != 0'>
                <xsl:attribute name="class">oddrow</xsl:attribute>
              </xsl:if>
              <xsl:variable name="linkurl">bank=<xsl:value-of select="../../@BankID"/>&amp;lbx=<xsl:value-of select="../../@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>
              <td class="grid-cell grid-cell-left cell-center-align">
                <a class="batchDetail" title="Batch Details" style="font-size: 14px;">
                  <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/></xsl:attribute>
                  <span style="display:block">
                    <i class="fa fa-edit"></i>
                  </span>
                </a>
              </td>
              <xsl:if test="$displaybatchid = 'True'">
                <td class="grid-cell">
                  <xsl:value-of select="@SourceBatchID"/>
                </td>
              </xsl:if>
              <td class="grid-cell">
                <a title="View Batch Detail">
                  <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/></xsl:attribute>
                  <xsl:value-of select="@BatchNumber"/>
                </a>
              </td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="@DepositDate"/>
              </td>
              <td class="grid-cell">
                <xsl:value-of select="@PaymentSource"/>
              </td>
              <td class="grid-cell">
                <xsl:value-of select="@PaymentType"/>
              </td>
              <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
                <td class="grid-cell">
                  <xsl:value-of select="@BatchSiteCode"/>
                </td>
              </xsl:if>
              <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
                <td class="grid-cell">
                  <xsl:choose>
                    <xsl:when test="@BatchCueID = -1">
                      &nbsp;
                    </xsl:when>
                    <xsl:otherwise>
                      <xsl:value-of select="@BatchCueID"/>
                    </xsl:otherwise>
                  </xsl:choose>
                </td>
              </xsl:if>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="@TransactionCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="@CheckCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="@DocumentCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                $<xsl:value-of select="format-number(@CheckAmount, '#,##0.00')"/>
              </td>
            </tr>
          </xsl:for-each>

          <xsl:if test="../@DisplayPageTotals = 'Y'">
            <tr class="totals-row">
              <xsl:if test="$displaybatchid = 'True'">
                <td class="grid-cell grid-cell-left">&nbsp;</td>
              </xsl:if>
              <td class="grid-cell grid-cell-left">Totals:</td>
              <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
                <td class="grid-cell">&nbsp;</td>
              </xsl:if>
              <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
                <td class="grid-cell">&nbsp;</td>
              </xsl:if>
              <td class="grid-cell">&nbsp;</td>
              <td class="grid-cell">&nbsp;</td>
              <td class="grid-cell">&nbsp;</td>
              <td class="grid-cell">&nbsp;</td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="../@TransactionCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="../@CheckCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                <xsl:value-of select="../@DocumentCount"/>
              </td>
              <td class="grid-cell cell-right-align">
                $<xsl:value-of select="format-number(../@BatchTotal, '#,##0.00')"/>
              </td>
            </tr>
          </xsl:if>

          <tr class="grid-pager" style="height: 10px;">
            <td colspan="12">
              <xsl:variable name="linkurl">lckbx=<xsl:value-of select="../@OLLockboxID"/>&amp;startdate=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtStartDate']/@Value"/>&amp;enddate=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtEndDate']/@Value"/>&amp;paymentTypeId=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtPaymentTypeID']/@Value"/>&amp;workgroupSelection=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:variable>
              <span style="margin-left: 3px; dispay: inline-block;">
                <xsl:choose>
                  <xsl:when test="number(@StartRecord) - number(@DisplayRecords) &gt;= 1">
                    <a title="View First Page">
                      <xsl:attribute name="href">batchsummary.aspx?<xsl:value-of select="$linkurl"/>&amp;start=1</xsl:attribute>
                      <i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    </a>
                    <a title="View Previous Page">
                      <xsl:attribute name="href">batchsummary.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@PreviousStartRecord"/></xsl:attribute>
                      <i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <i class="ui-icon ui-icon-seek-first ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    <i class="ui-icon ui-icon-seek-prev ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                  </xsl:otherwise>
                </xsl:choose>

                <xsl:choose>
                  <xsl:when test="number(@StartRecord) + (number(@DisplayRecords) - 1) &lt; number(@TotalRecords)">
                    <a title="View Next Page">
                      <xsl:attribute name="href">batchsummary.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@NextStartRecord"/></xsl:attribute>
                      <i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    </a>
                    <a title="View Last Page">
                      <xsl:attribute name="href">batchsummary.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@LastStartRecord"/></xsl:attribute>
                      <i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    </a>
                  </xsl:when>
                  <xsl:otherwise>
                    <i class="ui-icon ui-icon-seek-next ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                    <i class="ui-icon ui-icon-seek-end ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
                  </xsl:otherwise>
                </xsl:choose>
              </span>

              <span style="dispay: inline-block; vertical-align: middle; font-size: 0.7em; margin-left: 5px;">
                <xsl:if test="../../PageInfo">
                  Showing Results&nbsp;
                  <xsl:value-of select="../../PageInfo/@StartRecord"/>&nbsp;-&nbsp;
                  <xsl:value-of select="../../PageInfo/@EndRecord"/>&nbsp;of&nbsp;
                  <xsl:value-of select="../../PageInfo/@TotalRecords"/>
                </xsl:if>
              </span>

              <!--<div style="display: inline-block; float: right; vertical-align: middle; margin-right: 5px; margin-top: 3px;">
                <i class="ui-icon ui-icon-lightbulb ui-state-disabled" style="border: 1px solid gray" ></i>
              </div>-->

            </td>
          </tr>

        </table>
      </td>
    </tr>
  </xsl:template>


  <xsl:template match="PageInfo" mode="PageNavigation">

    <xsl:variable name="linkurl">lckbx=<xsl:value-of select="../Record/@OLLockboxID"/>&amp;startdate=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtStartDate']/@Value"/>&amp;enddate=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtEndDate']/@Value"/>&amp;paymentTypeId=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtPaymentTypeID']/@Value"/>&amp;paymentSourceId=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtPaymentSourceID']/@Value"/>&amp;workgroupSelection=<xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:variable>
    <p/>
    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="left" valign="top">
          <xsl:choose>
            <xsl:when test="$MaxPrintableRows &gt;= $RowCount or $MaxPrintableRows = 0">
              <a>
                <xsl:attribute name="href">JavaScript:newPopup('batchsummary.aspx?<xsl:value-of select="$linkurl"/>&amp;printview=1');</xsl:attribute>
                <i class="fa fa-print"></i>
                &nbsp;Printer-Friendly Version
              </a>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name="class">copyright</xsl:attribute>
              Print View unavailable. Narrow your selection to enable.
            </xsl:otherwise>
          </xsl:choose>
        </td>
      </tr>
      <tr>
        <td style="height: 3px;">&nbsp;</td>
      </tr>
    </table>

  </xsl:template>


  <xsl:template match="Form" mode="frmMain">
    <form id="frmMain" method="post" onSubmit="return false;">
      <xsl:attribute name="name"><xsl:value-of select="@Name"/></xsl:attribute>

      <input type="hidden" name="txtAction" value=""/>
      <input type="hidden" name="hdnPaymentType">
        <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/></xsl:attribute>
      </input>
      <input  type="hidden" name="hdnPaymentSource">
        <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtPaymentSourceID']/@Value"/></xsl:attribute>
      </input>
      <div id="divCalendar" style="position: absolute; background-color: white; visibility: hidden;"></div>
      <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
            <table cellpadding="0" cellspacing="0" border="0">
              <tr>
                <td style="height: 20px;">&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <span class="hubLabel" style="margin-left: 5px;">Date Range:</span>
                </td>
                <td>
                  <div style="margin-left: 20px;">
                    <span class="hubLabel">Workgroup:</span>
                  </div>
                </td>
                <td>
                  <div style="margin-left: 20px;">Payment Type:</div>
                </td>
                <td>&nbsp;</td>
              </tr>
              <tr>
                <td>
                  <div style="margin-left: 5px; padding-top: 4px;">
                    <div id="StartDatePicker"></div>
                    <input name="txtStartDate" type="text" id="txtStartDate" style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtStartDate']/@Value"/></xsl:attribute>
                    </input>
                    <div style="margin: 10px 2px 5px 5px; display: inline-block; vertical-align: top;">
                      <i class="fa fa-minus"></i>
                    </div>
                    <div id="EndDatePicker"></div>
                    <input name="txtEndDate" type="text" id="txtEndDate" style="display:none">
                      <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtEndDate']/@Value"/></xsl:attribute>
                    </input>
                  </div>
                </td>
                <td>

                  <!--<xsl:variable name="OLLockboxID"><xsl:value-of select="Field[@Name='txtOLLockboxID']/@Value"/></xsl:variable>-->
                  <div style="margin-left: 20px;padding-top: 6px;">
                    <div id="treeBase" style="width: 375px;">
                      <input id="txtWorkgroupSelection" type="hidden" name="txtWorkgroupSelection" value="">
                        <xsl:attribute name="value"><xsl:value-of select="Field[@Name='txtWorkgroupSelection']/@Value"/></xsl:attribute>
                      </input>
                    </div>
                  </div>
                </td>
                <td>
                  <div style="margin-left: 20px;padding-top: 11px;">
                    <xsl:variable name="PaymentTypeID"><xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/></xsl:variable>
                    <select name="cboPaymentType" id="cboPaymentType" size="1" style="display: none;">
                      <option value="-1">-- All --</option>
                      <xsl:for-each select="Field[@Name='cboPaymentTypes']">
                        <option>
                          <xsl:attribute name="value"><xsl:value-of select="@Value"/></xsl:attribute>
                          <xsl:if test="@Value=$PaymentTypeID">
                            <xsl:attribute name="SELECTED"></xsl:attribute>
                          </xsl:if>
                          <xsl:value-of select="@PaymentType"/>
                        </option>
                      </xsl:for-each>
                    </select>
                    <div class="btn-group entitySelect" id="PaymentTypeSelect" style="margin-top: -10px;"></div>
                  </div>
                </td>
                <td>
                  <input type="button" class="btn btn-primary" id="cmdSubmit" name="cmdSubmit" value="Go" style="margin-left: 15px; margin-top: -4px; height: 28px;"/>
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="1"/>
          </td>
        </tr>
      </table>
      <input type="hidden" name="IsPostBackHiddenField" />
    </form>


    <script language="JavaScript">
      <xsl:comment>
        //Hide script from older browsers
        //Set form field validation requirements
        with (document.forms["frmMain"]){
        if (elements["txtStartDate"]){
        txtStartDate.isDate = true;
        txtStartDate.isRequired = true;
        txtStartDate.fieldname = 'Start Date';
        }

        if (elements["txtEndDate"]){
        txtEndDate.isDate = true;
        txtEndDate.isRequired = true;
        txtEndDate.fieldname = 'End Date';
        }

        if (elements["txtWorkgroupSelection"]){
        txtWorkgroupSelection.isRequired = true;
        txtWorkgroupSelection.fieldname = 'Workgroup';
        }
        //End hiding
        }



        $(document).ready(function() {

        $('#cmdSubmit').click(function()
        {
        if(!$('#txtWorkgroupSelection').val())
        {
           var container = $('#tabbase');
           framework.errorToast(container, 'Please select a single workgroup.');
           return false;
        } else {
           formSubmit(this, 'txtAction')
        }

        });

        //**************************************************************
        //  Workgroup Selector
        //**************************************************************

        var _vm = new wGViewModel();

        var dataCallback = function (resp, widget) {
        var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        if (ecount === 1 &#38;&#38; wcount === 1)
        {
          var el = $('&lt;input id="txtWorkgroupSelection" name="txtWorkgroupSelection" value="" type="hidden"&gt;&lt;div&gt;&lt;label class="control-label"&gt;&lt;/label&gt;&lt;input value="'+ workgroups[0].label+ '" class="form-control input-md" type="text" readonly="" style="margin-top: -5px;display:inline;" /&gt;&lt;/div&gt;');
          $('#treeBase').html(el);
          _vm['selectedWorkgroup'] = workgroups[0];
          $('#txtWorkgroupSelection').val(workgroups[0].id);
        }
        else
        {
        _vm.workGroupSelector.wfsTreeSelector('initialize');
          if($('#txtWorkgroupSelection').val())
          {
            widget.setSelection($('#txtWorkgroupSelection').val());
          }
        }

        };

        var selectedTreeItem = function (selected, initial) {
          var title = null;

          if (selected &#38;&#38; !initial) {
            if (selected.isNode)
            {
              $('#txtWorkgroupSelection').val(selected.id);
              title = 'Selected: ' + selected['label'];
              _vm.workGroupSelector.wfsTreeSelector('setTitle', title);
            }
            else 
            {
              var container = $('#tabbase');
              framework.errorToast(container, 'Please select a single workgroup.');
              return false;
            }
          }
        };

        _vm.workGroupSelector = $('#treeBase').wfsTreeSelector({
          useExpander: true,
          entityURL: '/RecHubRaamProxy/api/entity',
          callback: selectedTreeItem,
          dataCallback: dataCallback,
          expanderTitle: "Select Workgroup",
          entitiesOnly: false,
          closeOnSelectNonNode: false
        });

        function wGViewModel(model) {
        var self = this;
        self.model = model;

        <!--self.labels = model.Labels;
        self.selectedWorkgroup = null;-->
        };

        //**************************************************************
        var startDate = new Date($('#txtStartDate').val());
        var endDate = new Date($('#txtEndDate').val());

        $('#StartDatePicker').wfsDatePicker({dateIn: startDate, callback: startDateChange});

        $('#EndDatePicker').wfsDatePicker({dateIn: endDate, callback: endDateChange});



        /******************************  PT ***********************************/
        var cboPaymentTypeVM = new SelectViewModel('#cboPaymentType');
        var cboPaymentTypeItems = cboPaymentTypeVM.items;

        $('#PaymentTypeSelect').wfsSelectbox({ 'items': cboPaymentTypeItems, 'callback': cboPaymentTypeVM.setItemValue, 'displayField': 'Name', 'idField': 'Id' });

        var selectedPTId = '<xsl:value-of select="Field[@Name='txtPaymentTypeID']/@Value"/>';
        var selPT = ($.grep(cboPaymentTypeItems, function(e){ return e.id == selectedPTId; }))[0];
        if (selPT)
        $('#PaymentTypeSelect').wfsSelectbox('setValueByData', selPT);

        function SelectViewModel(selector) {
        var self = this;
        var $target = $(selector);

        $target.hide();
        self.items = [];
        $('option', $target).each(function(i) {
        self.items.push({Id: $(this).val(), Name: $(this).text()});
        });

        self.setItemValue = function (value) {
        $('option', $target).removeAttr('selected');
        $('option[value="' + value.Id + '"]', $target).attr('selected', true);
        }
        }

        $('.batchDetail').tooltip();
        });

        function startDateChange(val) {
        document.getElementById('txtStartDate').value = val;
        }

        function endDateChange(val) {
        document.getElementById('txtEndDate').value = val;
        }

        function findAndRemove(array, property, value) {
        $.each(array, function(index, result) {
        if(result[property] == value) {
        //Remove from array
        array.splice(index, 1);
        }
        });
        }

        function findIndex(array, property, value) {
        var newIndex = 0;
        $.each(array, function(index, result) {
        if(result[property] == value) {
        newIndex = index;
        }
        });

        return newIndex;

        }

      </xsl:comment>
    </script>
  </xsl:template>
</xsl:stylesheet>

