<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
 	<!ENTITY copy "&#169;">
 	<!ENTITY middot "&#183;">
 	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	All Payers
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">All Payers</span><br/>
</xsl:template>


<xsl:template match="Page">
	<xsl:if test="Recordset[@Name='Remitters']">
		<xsl:apply-templates select="Recordset[@Name='Remitters']" mode="Remitters"/>
	</xsl:if>
</xsl:template>


<xsl:template match="Recordset" mode="Remitters">
	<p/>
	<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
		<caption class="reportcaption">All Payers</caption>
		<tr>
			<td align="left" valign="top" class="recordcount">
				<xsl:call-template name="ShowingResultsLabel">
					<xsl:with-param name="startrecord"><xsl:value-of select="/Page/PageInfo/@StartRecord"/></xsl:with-param>
				</xsl:call-template>
			</td>
		</tr>
		<tr>
			<td align="left" valign="top">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
					<tr>
						<td class="reportheading" align="left" valign="top">Workgroup</td>
						<td class="reportheading" align="left" valign="top">R/T</td>
						<td class="reportheading" align="left" valign="top">Account Number</td>
						<td class="reportheading" align="left" valign="top">Payer</td>
						<td class="reportheading" align="left" valign="top">Date Last Updated</td>
						<td class="reportheading" align="left" valign="top" width="16px">&nbsp;</td>
					</tr>
					<xsl:for-each select="Record">
						<tr>
							<xsl:variable name="pos"><xsl:value-of select="position()"/></xsl:variable>
							<xsl:variable name="linkurl">
								id=<xsl:value-of select="Fld[@ID='LockboxRemitterID']"/>&amp;start=<xsl:value-of select="/Page/PageInfo/@StartRecord"/>
							</xsl:variable>
							
							<xsl:if test="position() mod 2 = 0">
								<xsl:attribute name="class">evenrow</xsl:attribute>
							</xsl:if>
							<xsl:if test="position() mod 2 != 0">
								<xsl:attribute name="class">oddrow</xsl:attribute>
							</xsl:if>

							<td class="reportdata" align="left" valign="top">
								<xsl:choose>
									<xsl:when test="position() = 1 or Fld[@ID='LockboxID'] != ../Record[number($pos) - 1]/Fld[@ID='LockboxID']">
										<xsl:value-of select="Fld[@ID='LockboxID']"/>
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
									
								</xsl:choose>
							</td>
							
							<td class="reportdata" align="left" valign="top">
								<xsl:choose>
									<xsl:when test="string-length(Fld[@ID='RoutingNumber']) &gt; 0">
										<xsl:value-of select="Fld[@ID='RoutingNumber']"/>	
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="reportdata" align="left" valign="top">
								<xsl:choose>
									<xsl:when test="string-length(Fld[@ID='Account']) &gt; 0">
										<xsl:value-of select="Fld[@ID='Account']"/>
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="reportdata" align="left" valign="top">
								<xsl:choose>
									<xsl:when test="string-length(Fld[@ID='RemitterName']) &gt; 0">
										<xsl:value-of select="Fld[@ID='RemitterName']"/>
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="reportdata" align="left" valign="top">
								<xsl:choose>
									<xsl:when test="string-length(Fld[@ID='ModificationDate']) &gt; 0">
										<xsl:value-of select="Fld[@ID='ModificationDate']"/>
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
							<td class="reportdata" align="right" valign="top" width="16px">
								<xsl:choose>
									<xsl:when test="/Page/UserInfo/Permissions/Permission[@Script = 'remittermaint.aspx' and @Mode = 'modify']">
										<a title="Modify Remitter">
											<xsl:attribute name="href">remittermaint.aspx?mode=modify&amp;<xsl:value-of select="$linkurl"/></xsl:attribute>
											<img src="{$brandtemplatepath}/Images/cbo_icon_view.gif" width="16" height="16" border="0" alt="Modify"/>
										</a>
									</xsl:when>
									<xsl:otherwise>
										&nbsp;
									</xsl:otherwise>
								</xsl:choose>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<br/>
				<xsl:call-template name="PaginationLinks">
					<xsl:with-param name="startrecord"><xsl:value-of select="/Page/PageInfo/@StartRecord"/></xsl:with-param>
					<xsl:with-param name="linkurl">viewremitter.aspx</xsl:with-param>
				</xsl:call-template>				
			</td>
		</tr>
		<xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='download.aspx' and @Mode='download']">
			<tr>
				<td align="right" valign="top" class="reportdata">
					<br/>
					<a href="download.aspx"><img src="{$brandtemplatepath}/images/icodownl.gif" height="16" width="16" border="0"/></a>&nbsp;
					<a href="download.aspx">Download Payer File</a>
				</td>
			</tr>
		</xsl:if>
	</table>	
</xsl:template>

</xsl:stylesheet>
