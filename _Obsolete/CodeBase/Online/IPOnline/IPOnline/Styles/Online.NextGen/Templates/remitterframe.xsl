<?xml version="1.0" encoding="iso-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "&#169;">
	<!ENTITY middot "&#183;">
	<!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incCustomBranding.xsl"/>
<xsl:include href="incMessages.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	Add Payer To Payment
</xsl:template>

<xsl:template match="/">

 <html>
 <head>
		<title><xsl:call-template name="Title"/></title>
	 <link rel="stylesheet" text="text/css" href="{$brandtemplatepath}/stylesheet.css" />
 </head>

 <xsl:apply-templates select="/Page/Messages"/>

 <xsl:apply-templates select="/Page"/>

 </html>

</xsl:template>

<xsl:template match="Page">
 <frameset rows="65%,*">
	 <frame name="image">
   <xsl:attribute name="src">
				imagepdf.aspx?bank=<xsl:value-of select="./QueryString/BankID"/>&amp;lbx=<xsl:value-of select="./QueryString/LockboxID"/>&amp;batch=<xsl:value-of select="./QueryString/BatchID"/>&amp;date=<xsl:value-of select="./QueryString/DepositDate"/>&amp;picsdate=<xsl:value-of select="./QueryString/PICSDate"/>&amp;txn=<xsl:value-of select="./QueryString/TransactionID"/>&amp;item=<xsl:value-of select="./QueryString/BatchSequence"/>&amp;type=c&amp;page=-1
   </xsl:attribute>
  </frame>
	 <frame name="remitter">
   <xsl:attribute name="src">
				addremitter.aspx?lckbx=<xsl:value-of select="./QueryString/OLLockboxID"/>&amp;bank=<xsl:value-of select="./QueryString/BankID"/>&amp;lbx=<xsl:value-of select="./QueryString/LockboxID"/>&amp;batch=<xsl:value-of select="./QueryString/BatchID"/>&amp;date=<xsl:value-of select="./QueryString/DepositDate"/>&amp;picsdate=<xsl:value-of select="./QueryString/PICSDate"/>&amp;txn=<xsl:value-of select="./QueryString/TransactionID"/>&amp;item=<xsl:value-of select="./QueryString/BatchSequence"/>&amp;rt=<xsl:value-of select="./QueryString/RoutingNumber" />&amp;acct=<xsl:value-of select="./QueryString/Account" />&amp;type=c&amp;page=-1
   </xsl:attribute>
  </frame>
 </frameset>
</xsl:template>

</xsl:stylesheet>