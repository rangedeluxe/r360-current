<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
 Access Denied
  </xsl:template>

  <xsl:template name="PageTitle">
 <span class="contenttitle">Access Denied</span>
 <span class="contentsubtitle">&nbsp;</span>
  </xsl:template>

  <xsl:template match="Page">
 <br/>
 <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0">
   <tr>
  <td valign="top">
    <h5>You do not have access to this page.</h5>
  </td>
   </tr>
 </table>
 
   </xsl:template>

</xsl:stylesheet>
