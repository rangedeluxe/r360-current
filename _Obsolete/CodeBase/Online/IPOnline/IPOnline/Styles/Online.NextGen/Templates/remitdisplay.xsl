<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:import href="incCommon.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">Batch Detail</xsl:template>

  <xsl:variable name="MaxPrintableRows"><xsl:value-of select ="/Page/Recordset[@Name='BatchDetail']/PageInfo/@MaxPrintableRows"/></xsl:variable>
  <xsl:variable name="RowCount"><xsl:value-of select ="/Page/Recordset[@Name='BatchDetail']/PageInfo/@TotalRecords"/></xsl:variable>

  <xsl:template name="PageTitle">

    <span class="contenttitle"><xsl:call-template name="BreadCrumbs" /></span>

    <span class="contenttitle">Batch Detail</span>
    <!--br/>
	<span class="contentsubtitle">&nbsp;</span-->
  </xsl:template>

  <xsl:template match="Page">
    <link rel="stylesheet" href="/Assets/iviewer/jquery.iviewer.css" />
    <script type="text/javascript" language="javascript" src="{$brandtemplatepath}/js/assignPayer.js" />
    <script type="text/javascript" src="/Assets/iviewer/jquery.iviewer.js"></script>
    <script type="text/javascript" src="/Assets/wfs/wfs-imageviewer.js"></script>
    <div class="page-portlet-view" id="parentContainer">
      <xsl:if test="Recordset[@Name='BatchDetail']">
        <xsl:apply-templates select="Recordset[@Name = 'BatchDetail']" mode="BatchDetail"/>
      </xsl:if>
    </div>
  </xsl:template>


  <xsl:template match="Recordset" mode="BatchDetail">

    <xsl:if test="Record">
      <xsl:apply-templates select="Record" mode="BatchDetail"/>
    </xsl:if>

  </xsl:template>


  <xsl:template match="Record" mode="BatchDetail">

    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td>
          <table width="100%" cellpadding="5" cellspacing="0" border="0">
            <tr>
              <td>
                <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">
                  Bank ID:&nbsp;
                  <xsl:value-of select="/Page/Recordset[@Name='BatchDetail']/PageInfo/@BankID"/>
                  <br/>
                </xsl:if>
                Workgroup:&nbsp;&nbsp;<xsl:value-of select="@LongName"/><br />
                Deposit Date:&nbsp;<xsl:value-of select="@DepositDate"/><br />
                <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
                  Account Site Code:&nbsp;<xsl:value-of select="@SiteCode"/><br />
                </xsl:if>
                <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
                  <!--<td class="reportheading" align="right" valign="top"><xsl:call-template name="batchcueidlabel" /></td>-->
                </xsl:if>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="left" valign="top">
          <table width="99%" cellpadding="5" cellspacing="0" border="0" class="grid">
            <xsl:for-each select="Recordset[@Name='BatchDetails']">
              <xsl:apply-templates select="." mode="BatchDetails"/>
            </xsl:for-each>

            <xsl:if test="../PageInfo[@DisplayPageTotals='Y']">
              <xsl:apply-templates select="../PageInfo" mode="PageTotals"/>
            </xsl:if>
          </table>
        </td>
      </tr>
    </table>

    <xsl:if test="../PageInfo">
      <xsl:apply-templates select="../PageInfo" mode="PageNavigation"/>
    </xsl:if>
    <style type="text/css">
      <xsl:text>
      .tooltip-inner{
        max-width: 400px;
      }     
	</xsl:text>
    </style>
    <script language="JavaScript">
      <xsl:text>
			$(document).ready(function() {

				$('.showTip').tooltip({tooltipClass: "my-tooltip-styling" });
        
        //This removes the tooltip whenever a click occurs, also
        //it prevents the tooltip from returning whenever the main window 
        //gets focus back (this was actually an issue). 
        $('.showTip').on('click', function(){
          $(this).tooltip('hide');
		      $(this).one('focus',function(){ 
              this.blur();
            });
        });
        

				$.support.cors = true;

				payerPopup = new assignPayer('</xsl:text>
      <xsl:value-of select="normalize-space($PayerFrameworkURL)"/>
      <xsl:text>', 'parentContainer');

			});
		</xsl:text>
    </script>




    <!--<div class="assignPayerModal container">
	<div class="well-small">
		<div class="panel panel-default">
			<div class="panel-heading">
				<span class="portlet-text" data-bind="text: 'Assign Payer'"></span>
			</div>
			<div class="panel-body" style="font-size: 12px !important;">
				<div class="row">
					<div class="col-md-12">
						<img src="./empty.jpg" height="220" width="650" title="This is a Check" />
					</div>
					<div class="row">
						<form class="form-horizontal" role="form" id="payerForm">

							<table style="border-spacing: 5px; border-collapse: separate;">
								<tr>
									<td>
										<label for="inputWorkgroup" class="control-label">Workgroup: </label>
									</td>
									<td>
										<p class="form-control-static" data-bind="text: WorkgroupId">afdafdadsf</p>
									</td>
								</tr>
								<tr>
									<td>
										<label for="inputRoutingNumber" class="control-label">R/T: </label>
									</td>
									<td>
										<p class="form-control-static" data-bind="text: RoutingNumber" />
									</td>
								</tr>
								<tr>
									<td>
										<label for="inputAccountNumber" class="control-label">Account Number: </label>
									</td>
									<td>
										<p class="form-control-static" data-bind="text: AccountNumber" />
									</td>
								</tr>
								<tr>
									<td>
										<label for="inputName" class="control-label">Payer: </label>
									</td>
									<td>
										<input type="text" class="form-control" id="inputName" name="inputName" placeholder="Payer" data-bind="value: Name" maxlength="60" required="" style="min-width: 200px; max-width: 300px;" />
									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<button type="submit" class="btn btn-primary" id="formSubmit">Submit</button>&nbsp;
										<button type="button" class="btn btn-primary" id="formCancel">Cancel</button>
									</td>
								</tr>
							</table>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>   
</div>-->




  </xsl:template>


  <xsl:template match="Recordset" mode="BatchDetails">
    <xsl:variable name="viewallurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;batchnumber=<xsl:value-of select="@BatchNumber"/></xsl:variable>
    <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/Recordset[@Name = 'BatchDetail']/Record/@DisplayBatchID"/></xsl:variable>
    <xsl:variable name="importtypekey"><xsl:value-of select = "/Page/Recordset[@Name = 'BatchDetail']/Record/@ImportTypeKey"/></xsl:variable>
    <xsl:variable name="displayreporticon">
      <xsl:choose>
        <xsl:when test="/Page/UserInfo/Permissions/Permission[@Name='ImageRPSAuditReport']">
          <xsl:value-of select = '1'/>
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select = '0'/>
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <tr>
      <td class="grid-header grid-header-left">&nbsp;</td>
      <xsl:if test="$displaybatchid = 'True'">
        <td class="grid-header">
          <xsl:call-template name="batchidlabel" />
        </td>
      </xsl:if>

      <td class="grid-header">
        <xsl:call-template name="batchnumberlabel" />
      </td>

      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
        <td class="grid-header">
          Batch Site Code
        </td>
      </xsl:if>

      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
        <td class="grid-header">
          <xsl:call-template name="batchcueidlabel" />
        </td>
      </xsl:if>

      <td class="grid-header">Transaction</td>
      <td class="grid-header cell-right-align">Payment Amount</td>
      <td class="grid-header">&nbsp;</td>
      <td class="grid-header">
        <xsl:choose>
          <xsl:when test="number(@ImageCount) &gt; 0">
            <a class="showTip">
              <xsl:attribute name="href">viewallimages.aspx?<xsl:value-of select="$viewallurl"/></xsl:attribute>
              <xsl:attribute name="title">View All Images for Batch <xsl:value-of select="@SourceBatchID"/></xsl:attribute>
              <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$viewallurl"/>');return false;</xsl:attribute>
              <span style="display:block">
                <i class="fa fa-picture fa-lg"></i>
              </span>
            </a>
          </xsl:when>
          <xsl:otherwise>
            &nbsp;
          </xsl:otherwise>
        </xsl:choose>
      </td>
      <td class="grid-header">R/T</td>
      <td class="grid-header">Account Number</td>
      <td class="grid-header">Check/Trace/Ref Number</td>
      <td class="grid-header">Payer</td>
      <td class="grid-header">DDA</td>
      <xsl:if test="$displayreporticon='1' and $importtypekey=2">
        <td class="grid-header">
        </td>
      </xsl:if>

    </tr>

    <xsl:for-each select="Record">
      <tr>
        <xsl:if test='position() mod 2 = 0'>
          <xsl:attribute name="class">evenrow</xsl:attribute>
        </xsl:if>
        <xsl:if test='position() mod 2 != 0'>
          <xsl:attribute name="class">oddrow</xsl:attribute>
        </xsl:if>
        <xsl:variable name="transactiondetailurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/></xsl:variable>
        <td class="grid-cell grid-cell-left cell-center-align">
          <a class="showTip" title="Transaction Detail" style="font-size: 14px;">
            <xsl:attribute name="href">transactiondisplay.aspx?<xsl:value-of select="$transactiondetailurl"/></xsl:attribute>
            <span style="display:block">
              <i class="fa fa-edit"></i>
            </span>
          </a>
        </td>
        <xsl:if test="$displaybatchid = 'True'">
          <td class="grid-cell">
            <xsl:choose>
              <xsl:when test="position() = 1">
                <xsl:value-of select="@SourceBatchID"/>
              </xsl:when>
              <xsl:otherwise>
                &nbsp;
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </xsl:if>

        <td class="grid-cell">
          <xsl:choose>
            <xsl:when test="position() = 1">
              <xsl:value-of select="@BatchNumber"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>

        <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
          <td class="grid-cell">
            <xsl:choose>
              <xsl:when test="position() = 1">
                <xsl:value-of select="@BatchSiteCode"/>
              </xsl:when>
              <xsl:otherwise>
                &nbsp;
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </xsl:if>

        <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
          <td class="grid-cell">
            <xsl:choose>
              <xsl:when test="position() = 1">
                <xsl:choose>
                  <xsl:when test="@BatchCueID = -1">
                    &nbsp;
                  </xsl:when>
                  <xsl:otherwise>
                    <xsl:value-of select="@BatchCueID"/>
                  </xsl:otherwise>
                </xsl:choose>
              </xsl:when>
              <xsl:otherwise>
                &nbsp;
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </xsl:if>

        <xsl:variable name="imageurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;batchnumber=<xsl:value-of select="@BatchNumber"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;item=<xsl:value-of select="@BatchSequence"/>&amp;type=c</xsl:variable>
        <xsl:variable name="addremitterurl">bank=<xsl:value-of select="@BankID"/>&amp;client=<xsl:value-of select="@ClientAccountID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;item=<xsl:value-of select="@BatchSequence"/>&amp;type=c&amp;rt=<xsl:value-of select="@RT"/>&amp;acct=<xsl:value-of select="@Account"/>&amp;srcBatchID=<xsl:value-of select="@SourceBatchID"/>&amp;batchSourceShortName=<xsl:value-of select="@BatchSourceShortName"/>&amp;importTypeShortName=<xsl:value-of select="@ImportTypeShortName"/></xsl:variable>
        <xsl:variable name="reporturl">ReportName=TransactionReport&amp;bank=<xsl:value-of select="@BankID" />&amp;lbx=<xsl:value-of select="@LockboxID" />&amp;date=<xsl:value-of select="@DepositDate" />&amp;picsdate=<xsl:value-of select="@PICSDate"/>&amp;processingdate=<xsl:value-of select="@ProcessingDate"/>&amp;txn=<xsl:value-of select="@TransactionID" />&amp;batch=<xsl:value-of select="@BatchID" />&amp;batchnumber=<xsl:value-of select="@BatchNumber" />&amp;item=&amp;type=i&amp;page=-1</xsl:variable>

        <td class="grid-cell">
          <a class="showTip" title="Transaction Detail" style="font-size: 14px;">
            <xsl:attribute name="href">transactiondisplay.aspx?<xsl:value-of select="$transactiondetailurl"/></xsl:attribute>
            <xsl:value-of select="@TxnSequence"/>
          </a>
        </td>

        <td class="grid-cell cell-right-align">
          <xsl:choose>
            <xsl:when test="string-length(@Amount) &gt; 0">
              $<xsl:value-of select="format-number(@Amount, '#,##0.00')"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>

        <td class="grid-cell">&nbsp;</td>
        <td class="grid-cell" width="80">
          <span style="display: block;">
            <xsl:choose>
              <xsl:when test="number(@ImageSize) &gt; 0">
                <a class="showTip" title="View Payment Image" style="font-size: 14px;padding: 5px;">
                  <xsl:attribute name="href">imagepdf.aspx?<xsl:value-of select="$imageurl"/></xsl:attribute>
                  <xsl:attribute name="onClick">docView('imagepdf.aspx?<xsl:value-of select="$imageurl"/>');return false;</xsl:attribute>
                  <i class="fa fa-money fa-lg" />
                </a>
              </xsl:when>
              <xsl:otherwise>
                <img src="{$brandtemplatepath}/images/shim.gif" style="width: 16px; height: 16px;"/>
              </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
              <xsl:when test="number(@DocumentCount) &gt; 0">
                <xsl:variable name="viewalldocsurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;seq=<xsl:value-of select="@TxnSequence"/>&amp;page=-1&amp;imagefilteroption=DocumentsOnly</xsl:variable>
                <!-- <img src="{$brandtemplatepath}/images/shim.gif" style="width: 10px; height: 10px;"/> -->
                <a class="showTip" style="font-size: 14px;padding: 5px;">
                  <xsl:attribute name="href">#</xsl:attribute>
                  <xsl:attribute name="title">View <xsl:value-of select="@DocumentCount" /> Transaction documents</xsl:attribute>
                  <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$viewalldocsurl"/>');return false;</xsl:attribute>
                  <i class="fa fa-file-o fa-lg"></i>
                </a>
              </xsl:when>
              <xsl:otherwise>
                <img src="{$brandtemplatepath}/images/shim.gif" style="width: 16px; height: 16px;"/>
              </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
              <xsl:when test="number(@DocumentCount) > 0 and number(@ImageSize) > 0">
                <xsl:variable name="viewalltxnurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/>&amp;txn=<xsl:value-of select="@TransactionID"/>&amp;seq=<xsl:value-of select="@TxnSequence"/>&amp;page=-1</xsl:variable>
                <!-- <img src="{$brandtemplatepath}/images/shim.gif" style="width: 10px; height: 10px;"/> -->
                <a class="showTip" style="font-size: 14px;padding: 5px;">
                  <xsl:attribute name="href">#</xsl:attribute>
                  <xsl:attribute name="title">View All Images for Transaction</xsl:attribute>
                  <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$viewalltxnurl"/>');return false;</xsl:attribute>
                  <i class="fa fa-picture-o fa-lg"></i>
                </a>
              </xsl:when>
              <xsl:otherwise>
                <img src="{$brandtemplatepath}/images/shim.gif" style="width: 16px; height: 16px;"/>
              </xsl:otherwise>
            </xsl:choose>

            <xsl:choose>
              <xsl:when test="number(@MarkSenseDocumentCount) &gt; 0">
                <!--<img src="{$brandtemplatepath}/images/shim.gif" style="width: 10px; height: 10px;"/>-->
                <a title="Transaction Contains Mark Sense Data" style="font-size: 14px;padding: 5px;">
                  <xsl:attribute name="href">transactiondisplay.aspx?<xsl:value-of select="$transactiondetailurl"/></xsl:attribute>
                  <img src="{$brandtemplatepath}/Images/cbo_icon_mark_sense.gif" border="0"/>
                </a>
              </xsl:when>
              <xsl:otherwise>
                <img src="{$brandtemplatepath}/images/shim.gif" style="width: 16px; height: 16px;"/>
              </xsl:otherwise>
            </xsl:choose>
          </span>
        </td>

        <td class="grid-cell">
          <xsl:choose>
            <xsl:when test="string-length(@RT) &gt; 0">
              <xsl:value-of select="@RT"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>

        <td class="grid-cell">
          <xsl:choose>
            <xsl:when test="string-length(@Account) &gt; 0">
              <xsl:value-of select="@Account"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>

        <td class="grid-cell">
          <xsl:choose>
            <xsl:when test="string-length(@Serial) &gt; 0">
              <xsl:value-of select="@Serial"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="grid-cell">
          <xsl:variable name="PaymentType">
            <xsl:call-template name="to-lower">
              <xsl:with-param name="Value">
                <xsl:value-of select="@PaymentType"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <xsl:variable name="PaymentSource">
            <xsl:call-template name="to-lower">
              <xsl:with-param name="Value">
                <xsl:value-of select="@PaymentSource"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:variable>
          <xsl:choose>
            <xsl:when test="string-length(@RemitterName) &gt; 0">
              <xsl:value-of select="@RemitterName"/>
            </xsl:when>
            <xsl:when test="string-length(@RemitterName) = 0 and @CheckCount &gt; 0 and /Page/UserInfo/Permissions/Permission[@Name='AssignPayer' and @Mode='Manage']">
              <a title="Add Payer to Payment" class="openPayer">
                <xsl:attribute name="href"><xsl:value-of select="normalize-space($PayerFrameworkURL)"/>GetSuggestedPayer?<xsl:value-of select="$addremitterurl"/></xsl:attribute>
                <span class="btn-small" style="display:block">
                  <i class="fa fa-plus-circle"></i>&nbsp;Payer
                </span>
              </a>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <td class="grid-cell">
          <xsl:choose>
            <xsl:when test="string-length(@DDA) &gt; 0">
              <xsl:value-of select="@DDA"/>
            </xsl:when>
            <xsl:otherwise>
              &nbsp;
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <xsl:if test="$displayreporticon='1' and $importtypekey=2">
          <td class="grid-cell cell-center-align">
            <a class="showTip" title="Transaction Report" style="font-size: 14px;">
              <xsl:attribute name="href">report.aspx?<xsl:value-of select="$reporturl"/></xsl:attribute>
              <xsl:attribute name="onClick">docView('report.aspx?<xsl:value-of select="$reporturl"/>');return false;</xsl:attribute>
              <span style="display: block;">
                <i class="fa fa-print"></i>
              </span>
            </a>
          </td>
        </xsl:if>
      </tr>
    </xsl:for-each>

    <xsl:if test="@DisplayBatchTotals = 'Y'">
      <tr class="totals-row">
        <td class="grid-cell grid-cell-left"></td>
        <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
          <td class="grid-cell"></td>
        </xsl:if>

        <xsl:if test="$displaybatchid = 'True'">
          <td class="grid-cell"></td>
        </xsl:if>
        <td class="grid-cell">&nbsp;</td>
        <td class="grid-cell cell-right-align">
          <xsl:call-template name="batchnumberlabel" />&nbsp;<xsl:value-of select="@BatchNumber"/>&nbsp;Total:
        </td>
        <td class="grid-cell cell-right-align">
          $<xsl:value-of select="format-number(@BatchTotal, '#,##0.00')"/>
        </td>
        <td class="grid-cell">&nbsp;</td>
        <td class="grid-cell" colspan="8">
          <xsl:if test="number(@ImageCount) &gt; 0">
            <a class="showTip" >
              <xsl:attribute name="href">viewallimages.aspx?<xsl:value-of select="$viewallurl"/></xsl:attribute>
              <xsl:attribute name="title">View All Images for Batch <xsl:value-of select="@BatchNumber"/></xsl:attribute>
              <xsl:attribute name="onClick">docView('viewallimages.aspx?<xsl:value-of select="$viewallurl"/>');return false;</xsl:attribute>
              <nobr style="font-weight: normal;">
                <i class="fa fa-picture-o fa-lg"></i>&nbsp;View-All
              </nobr>
            </a>
          </xsl:if>
        </td>
      </tr>

    </xsl:if>

    <tr class="grid-pager" style="height: 10px;">
      <td colspan="15">

        <xsl:variable name="linkurl">bank=<xsl:value-of select="@BankID"/>&amp;lbx=<xsl:value-of select="@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>

        <span style="margin-left: 3px; dispay: inline-block;">
          <xsl:choose>
            <xsl:when test="number(../../PageInfo/@StartRecord) - number(../../PageInfo/@DisplayRecords) &gt;= 1">
              <a title="View First Page">
                <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/>&amp;start=1</xsl:attribute>
                <i class="ui-icon ui-icon-seek-first ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              </a>
              <a title="View Previous Page">
                <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@PreviousStartRecord"/></xsl:attribute>
                <i class="ui-icon ui-icon-seek-prev ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              </a>
            </xsl:when>
            <xsl:otherwise>
              <i class="ui-icon ui-icon-seek-first ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              <i class="ui-icon ui-icon-seek-prev ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
            </xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="number(../../PageInfo/@StartRecord) + (number(../../PageInfo/@DisplayRecords) - 1) &lt; number(../../PageInfo/@TotalRecords)">
              <a title="View Next Page">
                <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@NextStartRecord"/></xsl:attribute>
                <i class="ui-icon ui-icon-seek-next ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              </a>
              <a title="View Last Page">
                <xsl:attribute name="href">remitdisplay.aspx?<xsl:value-of select="$linkurl"/>&amp;start=<xsl:value-of select="//@LastStartRecord"/></xsl:attribute>
                <i class="ui-icon ui-icon-seek-end ui-state-default" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              </a>
            </xsl:when>
            <xsl:otherwise>
              <i class="ui-icon ui-icon-seek-next ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
              <i class="ui-icon ui-icon-seek-end ui-state-disabled" style="border: 1px solid gray; margin: 1px; display: inline-block; vertical-align: middle;"></i>
            </xsl:otherwise>
          </xsl:choose>
        </span>

        <span style="dispay: inline-block; vertical-align: middle; font-size: 0.7em; margin-left: 5px;">
          <xsl:if test="../../PageInfo">
            Showing Results&nbsp;
            <xsl:value-of select="../../PageInfo/@StartRecord"/>&nbsp;-&nbsp;
            <xsl:value-of select="../../PageInfo/@EndRecord"/>&nbsp;of&nbsp;
            <xsl:value-of select="../../PageInfo/@TotalRecords"/>
          </xsl:if>
        </span>
        <!--<div style="display: inline-block; float: right; vertical-align: middle; margin-right: 5px; margin-top: 3px;">
				<i class="ui-icon ui-icon-lightbulb ui-state-disabled" style="border: 1px solid gray" ></i>
			</div>-->
      </td>
    </tr>

  </xsl:template>

  <xsl:template match="PageInfo" mode="PageNavigation">
    <p/>
    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td align="left" valign="top">
          <xsl:choose>
            <xsl:when test ="$MaxPrintableRows &gt;= $RowCount or $MaxPrintableRows = 0">
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
              <xsl:variable name="printviewurl">bank=<xsl:value-of select="/Page/Recordset[@Name='BatchDetail']/PageInfo/@BankID"/>&amp;lbx=<xsl:value-of select="/Page/Recordset[@Name='BatchDetail']/PageInfo/@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="/Page/Recordset[@Name='BatchDetail']/PageInfo/@DepositDate"/></xsl:variable>
              <a>
                <xsl:attribute name="href">JavaScript:newPopup('remitdisplay.aspx?<xsl:value-of select="$printviewurl"/>&amp;printview=1');</xsl:attribute>
                <nobr>
                  <i class="fa fa-print"></i>&nbsp;Printer-Friendly Version
                </nobr>
              </a>
            </xsl:when>
            <xsl:otherwise>
              <xsl:attribute name ="class">copyright</xsl:attribute>Print View unavailable. Narrow your selection to enable.
            </xsl:otherwise>
          </xsl:choose>
        </td>
        <tr>
          <td style="height: 3px;">&nbsp;</td>
        </tr>
      </tr>
    </table>

  </xsl:template>


</xsl:stylesheet>
