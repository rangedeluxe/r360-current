<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
	<!ENTITY copy "&#169;">
	<!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="incStyle.xsl"/>
	<xsl:import href="incPagination.xsl"/>

	<xsl:output method="html"/>

	<xsl:template name="Title">
		Notification Detail
	</xsl:template>


	<xsl:template name="PageTitle">
		<span class="contenttitle">
      <a href="#" onclick="javascript:document.forms.namedItem('formrefinesearch').submit();">Notifications</a> &gt; 
			Notification Detail
		</span>
		<br/>
		<span class="contentsubtitle">&nbsp;</span>

    <xsl:variable name="FormEntitySelection">
      <xsl:value-of select ="/Page/FormFields/entitySelection"/>
    </xsl:variable>
    <xsl:variable name="FormFileName">
      <xsl:value-of select ="/Page/FormFields/filename"/>
    </xsl:variable>
    <xsl:variable name="FormNotificationSelection">
      <xsl:value-of select ="/Page/FormFields/notificationSelection"/>
    </xsl:variable>
    <xsl:variable name="FormStartDate">
      <xsl:value-of select ="/Page/FormFields/startdate"/>
    </xsl:variable>
    <xsl:variable name="FormEndDate">
      <xsl:value-of select ="/Page/FormFields/enddate"/>
    </xsl:variable>
    
    <form action="viewnotifications.aspx" method="post" id="formrefinesearch">
      <input type="hidden" name="txtWorkgroupSelection">
        <xsl:attribute name="value">
          <xsl:value-of select="$FormEntitySelection"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtFileName" >
        <xsl:attribute name="value">
          <xsl:value-of select="$FormFileName"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="cboNotificationFileType" >
        <xsl:attribute name="value">
          <xsl:value-of select="$FormNotificationSelection"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtStartDate" >
        <xsl:attribute name="value">
          <xsl:value-of select="$FormStartDate"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtEndDate" >
        <xsl:attribute name="value">
          <xsl:value-of select="$FormEndDate"/>
        </xsl:attribute>
      </input>
    </form>
    
	</xsl:template>

	<xsl:template match="Page">
		<xsl:if test="Recordset[@Name='Notification']">
			<p/>
			<xsl:apply-templates select="Recordset[@Name='Notification']" mode="Notification"/>
		</xsl:if>
		<xsl:if test="Recordset[@Name='NotificationFiles']">
			<xsl:apply-templates select="Recordset[@Name='NotificationFiles']" mode="NotificationFiles"/>
		</xsl:if>
	</xsl:template>
  
  <xsl:variable name="TimeZoneLbl">
    <xsl:value-of select ="/Page/Form[@Name='FormMain']/Field[@Name='TimeZoneLabel']/@Value"/>
  </xsl:variable>

	<xsl:template match="Recordset" mode="Notification">
		<div class="container">
			<div class="containerrow">
				<span class="containercell containerhead right" style="width:70px; height:25px; vertical-align:top">Date: </span>
				<div class="containercell reportdata" style="padding-left:10px">
					<xsl:value-of select="Record/Fld[@ID='NotificationDate']" />&nbsp;
          <small>
            (<xsl:value-of select="$TimeZoneLbl"/>)
          </small>
				</div>
			</div>
			<div class="containerrow">
				<span class="containercell containerhead right" style="width:70px; height:25px; vertical-align:top">ID: </span>
				<div class="containercell reportdata" style="padding-left:10px; margin-top:0px">
					<xsl:choose>
						<xsl:when test="string-length(Record/Fld[@ID='LockboxID']) = 0 or number(Record/Fld[@ID='LockboxID']&lt;1)">
							<xsl:value-of select="Record/Fld[@ID='CustomerName']"/>&nbsp;(<xsl:value-of select="Record/Fld[@ID='CustomerID']"/>)
						</xsl:when>
						<xsl:otherwise>
							<xsl:value-of select="Record/Fld[@ID='LockboxID']"/><xsl:if test="string-length(Record/Fld[@ID='LockboxName'])>0">&nbsp;-&nbsp;<xsl:value-of select="Record/Fld[@ID='LockboxName']"/></xsl:if>
						</xsl:otherwise>
					</xsl:choose>
				</div>
			</div>
			<div class="containerrow">
				<span class="containercell containerhead right" style="width:70px; vertical-align:top; padding-top:12px">Message: </span>
				<div class="containercell reportdata" style="padding-left:10px;">
					<pre><xsl:value-of select="Record/Fld[@ID='MessageText']" /></pre>
				</div>
			</div>
			<div class="containerrow">
				<p/>
				<hr/>
				<p/>
			</div>
		</div>
	</xsl:template>
	
	<xsl:template match="Recordset" mode="NotificationFiles">

		<xsl:variable name="startrecord"><xsl:value-of select="./@StartRecord"/></xsl:variable>

		<p/>
		<xsl:choose>
			<xsl:when test="count(Record) > 0">	
				<table width="98%" align="center" cellpadding="0" cellspacing="0" border="0" onMouseover="popMenu();">
					<tr>
						<td class="reportcaption">Attachments</td>
					</tr>
					<tr>
						<td class="recordcount" align="left" valign="top">
							&nbsp;
						</td>
					</tr>
				</table>
	
				<table width="98%" align="center" cellpadding="5" cellspacing="0" border="0" class="grid">
					<tr>
						<td class="grid-header">File</td>
						<td class="grid-header"><nobr>File Type</nobr></td>
						<td class="grid-header" width="16">&nbsp;</td>
					</tr>
		
					<xsl:for-each select="Record">
						<tr>
							<td class="grid-cell"><xsl:value-of select="Fld[@ID='UserFileName']"/></td>
							<td class="grid-cell"><xsl:value-of select="Fld[@ID='FileTypeDescription']"/></td>
							<td class="grid-cell" width="16">
                <xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='AlertDownload']">
								  <xsl:choose>
								  	<xsl:when test="number(Fld[@ID='FileSize']) &gt; 0">
								  	  <a class="showTip" title="View/Download File">
									  		<xsl:attribute name="href">downloadfile.aspx?filepath=<xsl:value-of select="Fld[@ID='FilePath']"/>&amp;name=<xsl:value-of select="Fld[@ID='FileDescription']"/>&amp;type=<xsl:value-of select="Fld[@ID='FileType']"/></xsl:attribute>
                        <img src="{$brandtemplatepath}/Images/cbo_icon_view.gif" border="0" alt="View Detail"/>
									  	</a>
									  </xsl:when>
								  	<xsl:otherwise><small>File Not Available</small></xsl:otherwise>
							  	</xsl:choose>
                </xsl:if>
							</td>
						</tr>
					</xsl:for-each>
					<xsl:call-template name="PaginationLinks">
						<xsl:with-param name="controlname">document.forms['frmMain'].txtAction</xsl:with-param>
						<xsl:with-param name="startrecord">
							<xsl:value-of select="$startrecord" />
						</xsl:with-param>
						<xsl:with-param name="colSpan">3</xsl:with-param>
					</xsl:call-template>		
				</table>	
		
				<form method="post" onSubmit="return false;" name="frmMain">
	 
					<input type="hidden" name="txtAction" value=""/>
					<input type="hidden" name="txtStart"><xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute></input>

				</form>

			</xsl:when>
			<xsl:otherwise>
				<div class="container">
					<div class="containerrow">
						<span class="reportdata">No attachments exist for this Notification.</span>
					</div>
				</div>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>

</xsl:stylesheet>