<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
 <!ENTITY copy "&#169;">
 <!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:include href="incCustomBranding.xsl"/>

<xsl:output method="html"/>

	<xsl:template name="Title">
	Batch Summary
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Batch Summary</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>

<xsl:template match="Page">
	<html>
	<head>
		<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
		<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
	</head>
	<body>
			<div id="container">
				<div id="header">
					<h1>
						<p/>
						Batch Summary
					</h1>
				</div>
				<div id="subheader">
					<h1>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBankIDOnline' and @AppType='0' and @DefaultSettings='Y']">
       BankID:&nbsp;<xsl:value-of select="Recordset[@Name = 'BatchSummary']/Record/@BankID"/><br/>
      </xsl:if>
      Workgroup:&nbsp;<xsl:value-of select="Recordset[@Name = 'BatchSummary']/Record/@LongName"/><br/>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowLockboxSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
       Site Code:&nbsp;<xsl:value-of select="Recordset[@Name = 'BatchSummary']/Record/@SiteCode"/>
      </xsl:if>
					</h1>
				</div>
				<div id="content">
					<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
						<xsl:if test="Recordset[@Name='BatchSummary']">
							<xsl:apply-templates select="Recordset[@Name = 'BatchSummary']" mode="BatchSummary"/>
						</xsl:if>
					</table>
				</div>
			</div>
	</body>
	</html>
	
</xsl:template>


<xsl:template match="Recordset" mode="BatchSummary">

	<xsl:if test="Record">
		<xsl:apply-templates select="Record" mode="BatchSummary"/>
	</xsl:if>

</xsl:template>


<xsl:template match="Record" mode="BatchSummary">
		<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
			<xsl:if test="Recordset[@Name='BatchDetails']">
				<xsl:apply-templates select="Recordset[@Name = 'BatchDetails']" mode="BatchDetails"/>
			</xsl:if>
		</table>
</xsl:template>


<xsl:template match="Recordset" mode="BatchDetails">
 <xsl:variable name="displaybatchid"><xsl:value-of select = "/Page/Recordset[@Name = 'BatchSummary']/Record/@DisplayBatchID"/></xsl:variable>
	<tr>
		<td align="left" valign="top">
			<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
				<tr>
					<xsl:if test="$displaybatchid = 'True'">
						<td class="reportheading" align="left" valign="top">
							<xsl:call-template name="batchidlabel" />
						</td>
					</xsl:if>
					<td class="reportheading" align="left" valign="top">
						<xsl:call-template name="batchnumberlabel" />
					</td>
					<td class="reportheading" align="right" valign="top">
						Deposit Date
					</td>
					<td class="reportheading" align="left" valign="top">Payment Source</td>
					<td class="reportheading" align="left" valign="top">Payment Type</td>

					<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
						<td class="reportheading" align="left" valign="top">
							Batch Site Code
						</td>
					</xsl:if>
					<xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
						<td class="reportheading" align="left" valign="top">
							<xsl:call-template name="batchcueidlabel" />
						</td>
					</xsl:if>
					
					<td class="reportheading" align="right" valign="top">Transaction Count</td>
					<td class="reportheading" align="right" valign="top">Payment Count</td>
					<td class="reportheading" align="right" valign="top">Document Count</td>
					<td class="reportheading" align="right" valign="top">Batch Total</td>
					<td class="reportheading">&nbsp;</td>
				</tr>

				<xsl:for-each select="Record">
					<tr>
						<xsl:if test='position() mod 2 = 0'>
							<xsl:attribute name="class">evenrow</xsl:attribute>
						</xsl:if>
						<xsl:if test='position() mod 2 != 0'>
							<xsl:attribute name="class">oddrow</xsl:attribute>
						</xsl:if>

						<xsl:variable name="linkurl">bank=<xsl:value-of select="../../@BankID"/>&amp;lbx=<xsl:value-of select="../../@LockboxID"/>&amp;batch=<xsl:value-of select="@BatchID"/>&amp;date=<xsl:value-of select="@DepositDate"/></xsl:variable>
      <xsl:if test="$displaybatchid = 'True'">
						<td class="reportdata" align="left" valign="top"><xsl:value-of select="@SourceBatchID"/></td>
      </xsl:if>
						<td class="reportdata" align="left" valign="top"><xsl:value-of select="@BatchNumber"/></td>
						<td class="reportdata" align="right" valign="top"><xsl:value-of select="@DepositDate"/></td>
						<td class="reportdata" align="left" valign="top">
							<xsl:value-of select="@PaymentSource"/>
						</td>
						<td class="reportdata" align="left" valign="top">
							<xsl:value-of select="@PaymentType"/>
						</td>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
       <td class="reportdata" align="left" valign="top"><xsl:value-of select="@BatchSiteCode"/></td>
      </xsl:if>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
       <td class="reportdata" align="left" valign="top">
       <xsl:choose>
        <xsl:when test="@BatchCueID = -1">
         &nbsp;
        </xsl:when>
        <xsl:otherwise>
         <xsl:value-of select="@BatchCueID"/>
        </xsl:otherwise>
       </xsl:choose>
       </td>
      </xsl:if>
						
      <td class="reportdata" align="right" valign="top"><xsl:value-of select="@TransactionCount"/></td>
						<td class="reportdata" align="right" valign="top"><xsl:value-of select="@CheckCount"/></td>
						<td class="reportdata" align="right" valign="top"><xsl:value-of select="@DocumentCount"/></td>
						<td class="reportdata" align="right" valign="top">$<xsl:value-of select="format-number(@CheckAmount, '#,##0.00')"/></td>
						<td class="reportdata" align="right" valign="top">
						</td>
					</tr>
				</xsl:for-each>

				<xsl:if test="../@DisplayPageTotals = 'Y'">
					<tr>
      <xsl:if test="$displaybatchid = 'True'">
       <td class="reportfooter" align="right" valign="top">&nbsp;</td>
      </xsl:if>
						<td class="reportfooter" align="right" valign="top" colspan="2">Totals:</td>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='ShowBatchSiteCodeOnline' and @AppType='0' and @DefaultSettings='Y']">
       <td class="reportfooter" align="right" valign="top"></td>
      </xsl:if>
      <xsl:if test="/Page/UserInfo/OLPreferences/OLPreference[@PreferenceName='DisplayBatchCueIDOnline' and @AppType='0' and @DefaultSettings='Y']">
       <td class="reportfooter" align="right" valign="top"></td>
      </xsl:if>
						<td class="reportfooter" align="left" valign="top">&nbsp;</td>
						<td class="reportfooter" align="left" valign="top">&nbsp;</td>
      <td class="reportfooter" align="right" valign="top"><xsl:value-of select="../@TransactionCount"/></td>
						<td class="reportfooter" align="right" valign="top"><xsl:value-of select="../@CheckCount"/></td>
						<td class="reportfooter" align="right" valign="top"><xsl:value-of select="../@DocumentCount"/></td>
						<td class="reportfooter" align="right" valign="top">$<xsl:value-of select="format-number(../@BatchTotal, '#,##0.00')"/></td>
						<td class="reportfooter" align="right" valign="top">&nbsp;</td>
					</tr>
				</xsl:if>

			</table>
		</td>
	</tr>
</xsl:template>

</xsl:stylesheet>
