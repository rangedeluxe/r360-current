<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>
<xsl:include href="incPagination.xsl"/>

<xsl:output method="html"/>


<xsl:template name="Title">
	Decisioning Batch Detail
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Decisioning Batch Summary</span><br/>
	<span class="contentsubtitle">Workgroup: <xsl:value-of select="/Page/Batch/LockboxID" /> - <xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=/Page/Batch/BankID and @LockboxID=/Page/Batch/LockboxID]/@LongName" /></span>
</xsl:template>

<xsl:variable name="FldTypeString">1</xsl:variable>
<xsl:variable name="FldTypeFloat">6</xsl:variable>
<xsl:variable name="FldTypeCurrency">7</xsl:variable>
<xsl:variable name="FldTypeDate">11</xsl:variable>

<xsl:variable name="FieldDecisionStatusNoneRequired">0</xsl:variable>
<xsl:variable name="FieldDecisionStatusPending">1</xsl:variable>
<xsl:variable name="FieldDecisionStatusAccepted">2</xsl:variable>
<xsl:variable name="FieldDecisionStatusRejected">3</xsl:variable>

<xsl:variable name="DecisionSourceStatusNotRequired">0</xsl:variable>
<xsl:variable name="DecisionSourceStatusPendingDecBatLogon">1</xsl:variable>
<xsl:variable name="DecisionSourceStatusPendingDecTDE">2</xsl:variable>
<xsl:variable name="DecisionSourceStatusAccepted">3</xsl:variable>
<xsl:variable name="DecisionSourceStatusRejected">4</xsl:variable>

<xsl:variable name="GlobalBatchID"><xsl:value-of select="/Page/FormFields/GlobalBatchID" /></xsl:variable>
<xsl:variable name="DepositStatus"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/DepositStatus" /></xsl:variable>
<xsl:variable name="LogonName"><xsl:value-of select="/Page/Batch[GlobalBatchID=$GlobalBatchID]/LogonName" /></xsl:variable>

<xsl:variable name="pagemode">
	<xsl:choose>
		<xsl:when test="$DepositStatus='241' and /Page/UserInfo/User/@ID=$LogonName and /Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='modify']">edit</xsl:when>
		<xsl:otherwise>view</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="batchisready">
	<xsl:choose>
		<xsl:when test="count(//Field[FieldDecisionStatus = $FieldDecisionStatusPending]) > 0">0</xsl:when>
		<xsl:when test="/Page/Batch/Transactions/DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon">0</xsl:when>
		<xsl:when test="/Page/Batch/Transactions/DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE">0</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="decisionsinbalance">
	<xsl:choose>
		<xsl:when test="/Page/Batch/Transactions[DecisionSourceStatus=$DecisionSourceStatusAccepted and Stubs/Field/FieldDecisionStatus=$FieldDecisionStatusRejected]">0</xsl:when>
		<xsl:otherwise>1</xsl:otherwise>
	</xsl:choose>
</xsl:variable>

<xsl:variable name="BankID"><xsl:value-of select="/Page/Batch/BankID" /></xsl:variable>
<xsl:variable name="CustomerID"><xsl:value-of select="/Page/Batch/CustomerID" /></xsl:variable>
<xsl:variable name="LockboxID"><xsl:value-of select="/Page/Batch/LockboxID" /></xsl:variable>

<xsl:template match="Page">

	<form method="post" name="frmOLD">

		<input type="hidden" name="txtAction" />

		<input type="hidden" name="txtBankID">
			<xsl:attribute name="value"><xsl:value-of select="/Page/Batch/BankID" /></xsl:attribute>
		</input>

		<input type="hidden" name="txtCustomerID">
			<xsl:attribute name="value"><xsl:value-of select="/Page/Batch/CustomerID" /></xsl:attribute>
		</input>

		<input type="hidden" name="txtLockboxID">
			<xsl:attribute name="value"><xsl:value-of select="/Page/Batch/LockboxID" /></xsl:attribute>
		</input>

		<table width="60%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td>
					<table width="100%" cellpadding="2" cellspacing="0" border="0">
						<tr>
							<td class="reportcaption">
                Workgroup:&nbsp;<xsl:value-of select="$LockboxID" />&nbsp;-&nbsp;&nbsp;<xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@LongName" /><br/>
								Deposit Date:&nbsp;<xsl:value-of select="/Page/Batch/DepositDate"/><br />
								Batch:&nbsp;<xsl:value-of select="/Page/Batch/BatchID" /><br />
								Deposit Status:&nbsp;
								<xsl:choose>
									<xsl:when test="/Page/Batch/DepositStatus = '240'">Pending Decision</xsl:when>
									<xsl:when test="/Page/Batch/DepositStatus = '241'">In-Decision</xsl:when>

									<xsl:when test="/Page/Batch/DepositStatus = '250'">Pending CAR</xsl:when>
									<xsl:when test="/Page/Batch/DepositStatus = '350'">Pending KFI</xsl:when>
									<xsl:when test="/Page/Batch/DepositStatus = '427'">Pending Decision Split</xsl:when>
									<xsl:when test="/Page/Batch/DepositStatus = '550'">Pending Encode</xsl:when>
									<xsl:when test="/Page/Batch/DepositStatus = '750'">Pending Print</xsl:when>

									<xsl:otherwise><xsl:value-of select="/Page/Batch/DepositStatus" /></xsl:otherwise>
								</xsl:choose>
								<br />
								Decisioning Deadline: <xsl:value-of select="/Page/Batch/AdjustedDecisioningDeadline" /><br />
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>

		<br />

		<xsl:call-template name="TxnCommands" />

		<table width="60%" align="center" cellpadding="0" cellspacing="0" border="0">
			<tr>
				<td align="left" valign="top" colspan="2">
					<xsl:for-each select="Batch">
						<xsl:for-each select="Transactions">

							<xsl:variable name="DestLoxkboxIdentifier"><xsl:value-of select="DestLoxkboxIdentifier" /></xsl:variable>
							<xsl:variable name="DestinationLockboxKey"><xsl:value-of select="DestinationLockboxKey" /></xsl:variable>
							<xsl:variable name="DestinationBatchTypeID"><xsl:value-of select="DestinationBatchTypeID" /></xsl:variable>

							<table width="100%" cellspacing="0" border="1">
								<tr>
									<td>
										<table width="100%" cellpadding="2" cellspacing="0" border="0">
											<tr>
												<td colspan="3" align="left">
													<table cellpadding="0" cellspacing="0">
														<tr>
															<td class="reportcaption">Destination Workgroup:&nbsp;</td>
															<td class="reportdata" align="left">
																<xsl:choose>
																	<xsl:when test="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record/@SiteLockboxKey = $DestinationLockboxKey">
																		<xsl:variable name="DestLockboxID"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@LockboxID"/></xsl:variable>
																		<xsl:variable name="DestLockboxIdentifierLabel"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@DestLockboxIdentifierLabel"/></xsl:variable>
																		<xsl:variable name="DestLockboxLongName"><xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/Recordset[@Name='DestLockboxes']/Record[@SiteLockboxKey = $DestinationLockboxKey]/@LongName"/></xsl:variable>
																		<xsl:value-of select="$DestLockboxID"/> - <xsl:value-of select="$DestLockboxLongName"/> - <xsl:value-of select="$DestLockboxIdentifierLabel"/>
																	</xsl:when>
																	<xsl:otherwise>Undefined</xsl:otherwise>
																</xsl:choose>
															</td>
														</tr>
													</table>
												</td>
											</tr>
											<tr>
												<td class="reportheading" vAlign="top" align="left">Transaction</td>
												<td class="reportheading" vAlign="top" align="Left">Decision</td>
												<td class="reportheading">&nbsp;</td>
											</tr>
											<tr>
												<td class="reportdata" vAlign="top" align="left"><xsl:value-of select="TransactionID" /></td>
												<td class="reportdata" vAlign="top" align="Left">
													<xsl:choose>
														<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusNotRequired">Decision not required</xsl:when>
														<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusPendingDecBatLogon">Pending Decision - Batch Logon</xsl:when>
														<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusPendingDecTDE">Pending Decision - TDE</xsl:when>
														<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusAccepted">Accepted</xsl:when>
														<xsl:when test="DecisionSourceStatus=$DecisionSourceStatusRejected">Rejected</xsl:when>
													</xsl:choose>
												</td>
												<td class="reportdata" align="right" valign="top">
													<xsl:if test="$DepositStatus='240' or $DepositStatus=241">
														<a title="Edit Transaction">
															<xsl:attribute name="href">oldtransactiondetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="../GlobalBatchID" />&amp;transaction=<xsl:value-of select="TransactionID" /></xsl:attribute>
															<small>Edit</small>
														</a>
													</xsl:if>
													&nbsp;&nbsp;&nbsp;
												</td>
											</tr>
										</table>
										<xsl:call-template name="Checks" />
										<xsl:call-template name="Stubs" />
									</td>
								</tr>
							</table>
							<br />
						</xsl:for-each>
					</xsl:for-each>
				</td>
			</tr>
		</table>

		<xsl:call-template name="TxnCommands" />

	</form>

</xsl:template>

<xsl:template name="Checks">
	<table width="100%">
		<tr>
			<td class="reportcaption">
				Checks:
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<table width="100%" cellpadding="2" cellspacing="0" border="0">
					<tr>
						<td class="reportheading" align="left">Payment Amount</td>
						<td class="reportheading" align="left">R/T</td>
						<td class="reportheading" align="left">Account</td>
						<td class="reportheading" align="left">Serial</td>
						<td class="reportheading" align="left">Payee</td>
						<td class="reportheading" align="left">Payer</td>
						<xsl:for-each select="../DESetupField">
							<xsl:if test="TableName='checksdataentry'">
								<td class="reportheading"><xsl:value-of select="FldName" /></td>
							</xsl:if>
						</xsl:for-each>
						<td class="reportheading">Decisioning Reason</td>
					</tr>
					<xsl:for-each select="Checks">

						<xsl:variable name="GlobalCheckID">
							<xsl:choose>
								<xsl:when test="@GlobalCheckID"><xsl:value-of select="@GlobalCheckID" /></xsl:when>
								<xsl:otherwise><xsl:value-of select="GlobalCheckID" /></xsl:otherwise>
							</xsl:choose>
						</xsl:variable>
						
						<xsl:variable name="TransactionID"><xsl:value-of select="../../TransactionID" /></xsl:variable>
						
						<xsl:variable name="checklabel">gbid<xsl:value-of select="../../GlobalBatchID" />_txnid<xsl:value-of select="$TransactionID" />_checkid<xsl:value-of select="$GlobalCheckID" />_</xsl:variable>

						<tr>
							<td class="reportdata" align="left"><xsl:value-of select="Amount" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="RT" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="Account" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="Serial" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="PayeeName" /></td>
							<td class="reportdata" align="left"><xsl:value-of select="RemitterName" /></td>


							<xsl:for-each select="../../DESetupField">

								<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
								<xsl:variable name="FldName"><xsl:value-of select="FldName" /></xsl:variable>
								<xsl:variable name="FldDataTypeEnum"><xsl:value-of select="FldDataTypeEnum" /></xsl:variable>
								<xsl:variable name="FldLength"><xsl:value-of select="FldLength" /></xsl:variable>
								<xsl:variable name="DESetupFieldID"><xsl:value-of select="@DESetupFieldID" /></xsl:variable>
								<xsl:variable name="FldData"><xsl:value-of select="../Transactions[TransactionID=$TransactionID]/Checks[GlobalCheckID=$GlobalCheckID]/Field[DESetupFieldID=$DESetupFieldID]/FldData" /></xsl:variable>

								<xsl:if test="$TableName='checksdataentry'">
									<td class="reportdata">
 										<xsl:choose>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeString">
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:when>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeDate">
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:when>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeFloat">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test="string(number($FldData)) != 'NaN'">
													<xsl:value-of select="format-number($FldData, '#,##0.00')"/>
												</xsl:if>
											</xsl:when>
											<xsl:when test="number($FldDataTypeEnum) = $FldTypeCurrency">
												<xsl:attribute name="align">right</xsl:attribute>
												<xsl:if test="string(number($FldData)) != 'NaN'">
													$<xsl:value-of select="format-number($FldData, '#,##0.00')"/>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:attribute name="align">left</xsl:attribute>
												<xsl:value-of select="$FldData"/>
											</xsl:otherwise>
										</xsl:choose>
									</td>
								</xsl:if>
							</xsl:for-each>

							<td class="reportdata"><xsl:value-of select="DecisioningReasonDesc" /></td>
						</tr>
					</xsl:for-each>
				</table>
			</td>
		</tr>		 			
	</table>
</xsl:template>

<xsl:template name="Stubs">

	<table width="100%">
		<tr>
			<td class="reportcaption">
				<br />
				Data Entry:
			</td>
		</tr>
		<tr>
			<td>
				<xsl:choose>
					<xsl:when test="string-length(/Page/FormFields/DEItemRowDataID) > 0">
						<table width="100%" cellpadding="2" cellspacing="0" border="0">
							<tr>
								<td colspan="3">
									<xsl:for-each select="Stubs">
										<xsl:variable name="DEItemRowDataID">
											<xsl:choose>
												<xsl:when test="@DEItemRowDataID"><xsl:value-of select="@DEItemRowDataID" /></xsl:when>
												<xsl:otherwise><xsl:value-of select="DEItemRowDataID" /></xsl:otherwise>
											</xsl:choose>
										</xsl:variable>
									</xsl:for-each>
								</td>
							</tr>
						</table>
					</xsl:when>
					<xsl:otherwise>
						<table width="100%" cellpadding="2" cellspacing="0" border="0">
							<tr>
								<td class="reportheading">Row No</td>
								<xsl:for-each select="../DESetupField">
									<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
									<xsl:if test="$TableName='stubs' or $TableName='stubsdataentry'">
										<td class="reportheading">
											<xsl:value-of select="Title" />
										</td>
									</xsl:if>
								</xsl:for-each>
								<td class="reportheading" vAlign="top" align="left">Pending</td>
								<td class="reportheading" vAlign="top" align="left">Accepted</td>
								<td class="reportheading" vAlign="top" align="left">Rejected</td>
							</tr>
							<xsl:for-each select="Stubs">
								<xsl:variable name="DEItemRowDataID">
									<xsl:choose>
										<xsl:when test="@DEItemRowDataID"><xsl:value-of select="@DEItemRowDataID" /></xsl:when>
										<xsl:otherwise><xsl:value-of select="DEItemRowDataID" /></xsl:otherwise>
									</xsl:choose>
								</xsl:variable>

								<xsl:variable name="stublabel">gbid<xsl:value-of select="../../GlobalBatchID" />_txnid<xsl:value-of select="../TransactionID" />_deitemrowdataid<xsl:value-of select="$DEItemRowDataID" />_</xsl:variable>

								<tr>
									<td class="reportdata"><xsl:value-of select="RowNo" /></td>

									<xsl:for-each select="../../DESetupField">
										<xsl:variable name="TableName"><xsl:value-of select="TableName" /></xsl:variable>
										<xsl:variable name="FldDataTypeEnum"><xsl:value-of select="FldDataTypeEnum" /></xsl:variable>
										<xsl:variable name="DESetupFieldID"><xsl:value-of select="@DESetupFieldID" /></xsl:variable>
										<xsl:variable name="FldData"><xsl:value-of select="../Transactions[TransactionID=../Transactions/TransactionID]/Stubs[DEItemRowDataID=$DEItemRowDataID]/Field[DESetupFieldID=$DESetupFieldID]/FldData" /></xsl:variable>
										<xsl:if test="$TableName='stubs' or $TableName='stubsdataentry'">
											<td class="reportdata">
 												<xsl:choose>
													<xsl:when test="number($FldDataTypeEnum) = $FldTypeCurrency">
														<xsl:if test="string(number($FldData)) != 'NaN'">
															<xsl:value-of select="format-number($FldData, '#,##0.00')"/>
														</xsl:if>
													</xsl:when>
													<xsl:otherwise>
														<xsl:value-of select="$FldData"/>
													</xsl:otherwise>
												</xsl:choose>
											</td>
										</xsl:if>
									</xsl:for-each>
									<td class="reportdata" vAlign="top" align="left">
										<xsl:choose>
											<xsl:when test="count(Field[FieldDecisionStatus = $FieldDecisionStatusPending]) &gt; 0">
												<span class="pagelinkssymbolson"><xsl:value-of select="count(Field[FieldDecisionStatus = $FieldDecisionStatusPending])" /></span>
											</xsl:when>
											<xsl:otherwise>
												<xsl:value-of select="count(Field[FieldDecisionStatus = $FieldDecisionStatusPending])" />
											</xsl:otherwise>
										</xsl:choose>
									</td>
									<td class="reportdata" vAlign="top" align="left"><xsl:value-of select="count(Field[FieldDecisionStatus = $FieldDecisionStatusAccepted])" /></td>
									<td class="reportdata" vAlign="top" align="left"><xsl:value-of select="count(Field[FieldDecisionStatus = $FieldDecisionStatusRejected])" /></td>
								</tr>

								<xsl:variable name="stubisready">
									<xsl:choose>
										<xsl:when test="count(Field[FieldDecisionStatus = $FieldDecisionStatusPending]) > 0">0</xsl:when>
										<xsl:otherwise>1</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>

								<xsl:variable name="stubinbalance">
									<xsl:choose>
										<xsl:when test="../DecisionSourceStatus=$DecisionSourceStatusAccepted and ./Field/FieldDecisionStatus=$FieldDecisionStatusRejected">0</xsl:when>
										<xsl:otherwise>1</xsl:otherwise>
									</xsl:choose>
								</xsl:variable>

								<xsl:choose>
									<xsl:when test="$stubisready!=1">
										<tr>
											<td>
												<xsl:attribute name="colspan"><xsl:value-of select="1 + count(../../DESetupField)" /></xsl:attribute>
												<span class="pagelinkssymbolson">All pending Transactions and DE Items require decisioning before the Batch can be completed.</span>
											</td>
										</tr>
									</xsl:when>
									<xsl:when test="$stubinbalance!=1">
										<tr>
											<td>
												<xsl:attribute name="colspan"><xsl:value-of select="1 + count(../../DESetupField)" /></xsl:attribute>
												<span class="pagelinkssymbolson">A Transaction cannot be accepted if any DE Items are being rejected.</span>
											</td>
										</tr>
									</xsl:when>
								</xsl:choose>
								
							</xsl:for-each>
						</table>
					</xsl:otherwise>
				</xsl:choose>
			</td>
		</tr>
	</table>
</xsl:template>


<xsl:template name="TxnCommands">
	<table width="60%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" valign="bottom" class="pagelinks">
				<xsl:choose>
					<xsl:when test="$pagemode='edit'">
						<xsl:if test="/Page/UserInfo/Permissions/Permission[@Script='oldbatchdetail.aspx' and @Mode='reset']">
							<input type="submit" value="Reset" name="btnReset" />&nbsp;
						</xsl:if>
						<xsl:if test="$batchisready=1 and $decisionsinbalance=1">
							<input type="submit" value="Complete" name="btnComplete" />
						</xsl:if>
					</xsl:when>
					<xsl:otherwise>
						<xsl:if test="$DepositStatus!='240' and $DepositStatus!=241">
							<span class="pagelinkssymbolson">Decisioning has been completed for this batch.</span>
						</xsl:if>
					</xsl:otherwise>
				</xsl:choose>
			</td>
			<td align="right" class="pagelinks" valign="bottom">
				<a><xsl:attribute name="href">oldbatches.aspx?OLLockboxID=<xsl:value-of select="/Page/Recordset[@Name='UserLockboxes']/Record[@BankID=$BankID and @LockboxID=$LockboxID]/@OLLockboxID" /></xsl:attribute>Select Batch</a>
				<xsl:if test="$DepositStatus='240' or $DepositStatus=241">
					&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
					<a><xsl:attribute name="href">oldbatchdetail.aspx?bank=<xsl:value-of select="$BankID"/>&amp;customer=<xsl:value-of select="$CustomerID"/>&amp;lbx=<xsl:value-of select="$LockboxID"/>&amp;gbatch=<xsl:value-of select="/Page/FormFields/GlobalBatchID" /></xsl:attribute>View Batch</a>
				</xsl:if>
			</td>
		</tr>
	</table>
</xsl:template>


</xsl:stylesheet>
