<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY copy "<xsl:text disable-output-escaping='yes'>&amp;copy;</xsl:text>">
	<!ENTITY reg "&#174;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
		<xsl:template name="companyname">Private Label Bank</xsl:template>
		<xsl:template name="appname">Workgroup Online</xsl:template>
		<xsl:template name="copyright">Copyright &copy; 2013, Private Label Inc.</xsl:template>
		<xsl:template name="supportdescription">Support Services</xsl:template>
		<xsl:template name="supportphone">(123) 456-7890</xsl:template>
		<xsl:template name="supportfax">(123) 456-7890</xsl:template>
		<xsl:template name="supportemail">support@privatelabel.com</xsl:template>
  <xsl:template name="batchcueidlabel">Batch Cue ID</xsl:template>
  <xsl:template name="batchnumberlabel">Batch</xsl:template>
  <xsl:template name="batchidlabel">Batch ID</xsl:template>
		<xsl:variable name="brandfolder">
			<xsl:choose>
				<xsl:when test="string-length(/Page/UserInfo/BrandingScheme/@BrandingSchemeFolder) > 0"><xsl:value-of select="/Page/UserInfo/BrandingScheme/@BrandingSchemeFolder"/></xsl:when>
				<xsl:otherwise>../Styles/Online.NextGen</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>

		<xsl:variable name="brandtemplatepath">../Styles/<xsl:value-of select="$brandfolder" /></xsl:variable>

  <!-- Default IPOnline idle timeout = 1140000 ms (19 minutes) -->
  <xsl:variable name="idleTimeoutMs">1140000</xsl:variable>

	<!-- *********************************************************************************** -->
	<!-- Header Footer Menu Controller Paths -->
	<!-- *********************************************************************************** -->
	<xsl:variable name="headerURL">
		<xsl:text disable-output-escaping="yes">
			/Framework/Framework/FrameworkHeader/
		</xsl:text>
	</xsl:variable>

	<xsl:variable name="footerURL">
		<xsl:text disable-output-escaping="yes">
			/Framework/Framework/FrameworkFooter/
		</xsl:text>
	</xsl:variable>

	<xsl:variable name="menuURL">
		<xsl:text disable-output-escaping="yes">
			/IPOnline/RecHubPage/Menu/
		</xsl:text>
	</xsl:variable>

	<xsl:variable name="PayerFrameworkURL">
		<xsl:text disable-output-escaping="yes">
			<!-- http://localhost:1072/HubViews/Payer/ -->
			/RecHubConfigViews/Payer/
		</xsl:text>
	</xsl:variable>

	<!-- *********************************************************************************** -->
	<!-- *********************************************************************************** -->
		
	<xsl:template name="changepasswordinstructions">
 <tr>
   <td class="reportdata">
  The following minimum standards need to be met for your password:
   </td>
 </tr>
 <tr>
   <td class="reportdata">
  <li> Your password must be at least <xsl:value-of select="/Page/FormFields/PasswordLength"/> characters long </li>
   </td>
 </tr>
 <tr>
   <td class="reportdata">
  <li>It must contain at least one capitalized and one non-capitalized letter</li>
   </td>
 </tr>
 <tr>
   <td class="reportdata">
  <li>  1 number </li>
   </td>
 </tr>
 <tr>
   <td class="reportdata">
  <li> 1 special character for example (~,|,?,=,@,\,^,_,$,#,!,*,/) </li>
   </td>
 </tr>
 <tr>
   <td class="reportdata">
  <li>Must not contain more than two repeating characters</li>
   </td>
 </tr>
 <tr>
 </tr>
  </xsl:template>
		<xsl:template name="supportemaillink"><a><xsl:attribute name="href">mailto://<xsl:call-template name="supportemail" /></xsl:attribute><xsl:call-template name="supportemail" /></a></xsl:template>
</xsl:stylesheet>
