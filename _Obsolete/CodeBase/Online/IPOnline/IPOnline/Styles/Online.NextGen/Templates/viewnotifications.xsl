<?xml version="1.0" encoding="utf-8"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
  <!ENTITY laquo "&#171;">
  <!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:import href="incStyle.xsl"/>
  <xsl:import href="incPagination.xsl"/>

  <xsl:output method="html"/>

  <xsl:template name="Title">
    Notifications
  </xsl:template>

  <xsl:variable name="FormWGSelection">
    <xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtWorkgroupSelection']/@Value" />
  </xsl:variable>
  <xsl:variable name="FormName">
    <xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtFileName']/@Value" />
  </xsl:variable>
  <xsl:variable name="FormNotificationType">
    <xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtNotificationFileType']/@Value" />
  </xsl:variable>
  <xsl:variable name="FormStartDate">
    <xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtStartDate']/@Value" />
  </xsl:variable>
  <xsl:variable name="FormEndDate">
    <xsl:value-of select="/Page/Form[@Name='frmMain']/Field[@Name='txtEndDate']/@Value" />
  </xsl:variable>


  <xsl:template name="PageTitle">
    <span class="contenttitle">Notifications</span>
    <!-- br/>
    <span class="contentsubtitle">&nbsp;</span -->
  </xsl:template>

  <xsl:template match="Page">
    <div class="page-portlet-view" style="margin-top: 0px;">
      <xsl:if test="Form[@Name='frmMain']">
        <xsl:apply-templates select="Form[@Name='frmMain']" mode="frmMain"/>
      </xsl:if>

      <xsl:if test="Recordset[@Name='Notifications']">
        <xsl:apply-templates select="Recordset[@Name='Notifications']" mode="Notifications"/>
      </xsl:if>
    </div>
  </xsl:template>

  <xsl:variable name="TimeZoneLbl">
    <xsl:value-of select ="/Page/Form[@Name='frmMain']/Field[@Name='TimeZoneLabel']/@Value"/>
  </xsl:variable>

  <xsl:template match="Recordset" mode="Notifications">

    <xsl:variable name="startrecord">
      <xsl:value-of select="./@StartRecord"/>
    </xsl:variable>

    <table width="98%" align="center" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td>Notifications</td>
      </tr>
      <tr>
        <td class="recordcount" align="left" valign="top">
          &nbsp;
        </td>
      </tr>
    </table>

    <table width="98%" align="center" cellpadding="5" cellspacing="0" border="0" class="grid">
      <tr>
        <td class="grid-header grid-header-left">
          <a href="#" title="Sort by Date">
            <xsl:attribute name="onClick">ColumnSort('date');</xsl:attribute>
            Date
          </a>
        </td>
        <td class="grid-header">
          <a href="#" title="Sort by ID">
            <xsl:attribute name="onClick">ColumnSort('notificationsource');</xsl:attribute>
            ID
          </a>
        </td>
        <td class="grid-header">
          <a href="#" title="Sort by File Availability">
            <xsl:attribute name="onClick">ColumnSort('message');</xsl:attribute>
            Message
          </a>
        </td>
        <td class="grid-header">
          <nobr>
            <a href="#" title="Sort by File Availability">
              <xsl:attribute name="onClick">ColumnSort('file');</xsl:attribute>
              Attachments
            </a>
          </nobr>
        </td>
        <td class="grid-header">&nbsp;</td>
      </tr>

      <xsl:for-each select="Record">
        <tr>
          <td class="grid-cell grid-cell-left">
            <xsl:value-of select="Fld[@ID='NotificationDate']"/>&nbsp;
            <small>
              (<xsl:value-of select="$TimeZoneLbl"/>)
            </small>
          </td>
          <td class="grid-cell">
            <xsl:choose>
              <xsl:when test="string-length(Fld[@ID='LockboxID']) = 0 or number(Fld[@ID='LockboxID']&lt;1)">
                <xsl:value-of select="Fld[@ID='OrganizationName']"/>&nbsp;
                <xsl:if test="string-length(Fld[@ID='CustomerID']) = 0 or number(Fld[@ID='CustomerID']&gt;0)">(<xsl:value-of select="Fld[@ID='CustomerID']"/>)</xsl:if>
              </xsl:when>
              <xsl:otherwise>
                <xsl:value-of select="Fld[@ID='LockboxID']"/>
                <xsl:if test="string-length(Fld[@ID='LockboxName'])>0">
                  &nbsp;-&nbsp;<xsl:value-of select="Fld[@ID='LockboxName']"/>
                </xsl:if>
              </xsl:otherwise>
            </xsl:choose>
          </td>

          <td class="grid-cell">
            <pre>
              <xsl:value-of select="Fld[@ID='MessageText']"/>
            </pre>
            <xsl:if test="Fld[@ID='AdditionalMessageParts'] > 0">
              <a>
                <xsl:attribute name="href">
                  notificationdetail.aspx?id=<xsl:value-of select="Fld[@ID='NotificationMessageGroup']"/>&#38;workgroup=<xsl:value-of select="$FormWGSelection" />&#38;filename=<xsl:value-of select="$FormName"/>&#38;notificationtype=<xsl:value-of select="$FormNotificationType"/>&#38;startdate=<xsl:value-of select="$FormStartDate"/>&#38;enddate=<xsl:value-of select="$FormEndDate"/>
                </xsl:attribute>
                See rest of message...
              </a>
            </xsl:if>
          </td>
          <td class="grid-cell cell-right-align">
            <xsl:value-of select="Fld[@ID='NotificationFileCount']"/>
          </td>
          <td width="16" class="grid-cell cell-right-align">
            <a title="View Notification Detail">
              <xsl:attribute name="href">
                notificationdetail.aspx?id=<xsl:value-of select="Fld[@ID='NotificationMessageGroup']"/>&#38;workgroup=<xsl:value-of select="$FormWGSelection" />&#38;filename=<xsl:value-of select="$FormName"/>&#38;notificationtype=<xsl:value-of select="$FormNotificationType"/>&#38;startdate=<xsl:value-of select="$FormStartDate"/>&#38;enddate=<xsl:value-of select="$FormEndDate"/>
              </xsl:attribute>
              <img src="{$brandtemplatepath}/Images/cbo_icon_view.gif" border="0" alt="View Notification Detail"/>
            </a>
          </td>
        </tr>
      </xsl:for-each>
      <xsl:call-template name="PaginationLinks">
        <xsl:with-param name="controlname">document.forms['frmMain'].txtAction</xsl:with-param>
        <xsl:with-param name="startrecord">
          <xsl:value-of select="$startrecord" />
        </xsl:with-param>
        <xsl:with-param name="colSpan">5</xsl:with-param>
      </xsl:call-template>
      <tr>
        <td colspan="5" style="height: 3px;">&nbsp;</td>
      </tr>
    </table>



  </xsl:template>

  <xsl:template match="Form" mode="frmMain">

    <xsl:variable name="NotificationFileType">
      <xsl:value-of select="Field[@Name='txtNotificationFileType']/@Value"/>
    </xsl:variable>
    <xsl:variable name="startrecord">
      <xsl:value-of select="/Page/Recordset[@Name='Notifications']/@StartRecord"/>
    </xsl:variable>
    <xsl:variable name="WorkgroupSelection">
      <xsl:value-of select="Field[@Name='txtWorkgroupSelection']/@Value"/>
    </xsl:variable>
    <xsl:variable name="SelectedWorkgroups">
      <xsl:value-of select="Field[@Name='txtSelectedWorkgroups']/@Value"/>
    </xsl:variable>
    <xsl:variable name="FileName">
      <xsl:value-of select="Field[@Name='txtFileName']/@Value"/>
    </xsl:variable>


    <form method="post" onSubmit="return false;" name="frmPageState">
      <input type="hidden" name="txtResubmitAllowed" value="true"/>
      <input type="hidden" name="txtStart">
        <xsl:attribute name="value">
          <xsl:value-of select="$startrecord" />
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtFileName">
        <xsl:attribute name="value">
          <xsl:value-of select="$FileName"/>
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtWorkgroupSelection">
        <xsl:attribute name="value">
          <xsl:value-of select="$WorkgroupSelection"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtSelectedWorkgroups">
        <xsl:attribute name="value">
          <xsl:value-of select="$SelectedWorkgroups"/>
        </xsl:attribute>
      </input>

      <input type="hidden" name="cboNotificationFileType">
        <xsl:attribute name="value">
          <xsl:value-of select="$NotificationFileType" />
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtStartDate">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtStartDate']/@Value"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtEndDate">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtEndDate']/@Value"/>
        </xsl:attribute>
      </input>

      <input type="hidden" name="txtOrderBy">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtOrderBy']/@Value"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtOrderDir">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtOrderDir']/@Value"/>
        </xsl:attribute>
      </input>
    </form>

    <form method="post" onSubmit="return false;" name="frmMain">
      <div id="divCalendar" style="position: absolute; background-color: white; visibility: hidden;"></div>

      <input type="hidden" name="txtAction" value=""/>

      <input type="hidden" name="txtStart">
        <xsl:attribute name="value">
          <xsl:value-of select="$startrecord" />
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtOrderBy">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtOrderBy']/@Value"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="txtOrderDir">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtOrderDir']/@Value"/>
        </xsl:attribute>
      </input>
      <input type="hidden" name="hdnNotificationFileType">
        <xsl:attribute name="value">
          <xsl:value-of select="Field[@Name='txtNotificationFileTypeID']/@Value"/>
        </xsl:attribute>
      </input>
      <br />
      <table align="center" width="98%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td>
            <table cellpadding="5" cellspacing="0" border="0">
              <tr>
                <td>Entity/Workgroup:</td>
                <td>
                  <div id="treeBase" style="width: 375px;">
                    <input id="txtWorkgroupSelection" type="hidden" name="txtWorkgroupSelection">
                      <xsl:attribute name="value">
                        <xsl:value-of select="Field[@Name='txtWorkgroupSelection']/@Value"/>
                      </xsl:attribute>
                    </input>
                    <input id="txtSelectedWorkgroups" type="hidden" name="txtSelectedWorkgroups">
                      <xsl:attribute name="value">
                        <xsl:value-of select="Field[@Name='txtSelectedWorkgroups']/@Value"/>
                      </xsl:attribute>
                    </input>
                  </div>
                </td>
              </tr>
              <tr>
                <td>Name:</td>
                <td>
                  <input name="txtFileName" type="" id="txtFileName" maxlength="50" style="width: 425px;">
                    <xsl:attribute name="value">
                      <xsl:value-of select="$FileName"/>
                    </xsl:attribute>
                  </input>
                </td>
              </tr>
              <tr>
                <td>Type:</td>
                <td>
                  <select name="cboNotificationFileType" id="cboNotificationFileType" class="formfield" style="display: none;">
                    <option value="-1">-- All Types --</option>
                    <xsl:for-each select="Field[@Name='cboNotificationFileType']">
                      <option>
                        <xsl:attribute name="value">
                          <xsl:value-of select="@Value"/>
                        </xsl:attribute>
                        <xsl:if test="@Value=$NotificationFileType">
                          <xsl:attribute name="SELECTED"></xsl:attribute>
                        </xsl:if>
                        <xsl:value-of select="@FileTypeDescription"/>
                      </option>
                    </xsl:for-each>
                  </select>
                  <div class="btn-group entitySelect" id="FileTypeSelect" style=""></div>
                </td>
                <td align="left">&nbsp;</td>
              </tr>
              <tr>
                <td>Date Range:</td>
                <td>
                  <div class="input-append" style="margin-left: 5px; margin-top: 10px;">
                    <div id="StartDatePicker"></div>
                    <input name="txtStartDate" type="text" id="txtStartDate" style="display:none">
                      <xsl:attribute name="value">
                        <xsl:value-of select="Field[@Name='txtStartDate']/@Value"/>
                      </xsl:attribute>
                    </input>
                    <div style="margin: 10px 5px; display: inline-block; vertical-align: top;">
                      <i class="fa fa-minus"></i>
                    </div>
                    <div id="EndDatePicker"></div>
                    <input name="txtEndDate" type="text" id="txtEndDate" style="display:none">
                      <xsl:attribute name="value">
                        <xsl:value-of select="Field[@Name='txtEndDate']/@Value"/>
                      </xsl:attribute>
                    </input>
                  </div>
                </td>
                <td align="left">
                  <input type="button" name="cmdSubmit" value="Apply" class="btn btn-primary" onClick="formSubmit(this, 'txtAction');" style="margin: 5px 0px 0px 25px;height: 28px;"/>&nbsp;
                  <input type="reset" name="cmdReset" value="Reset" class="btn btn-primary" style="margin: 5px 0px 0px 0px;height: 28px;" onClick="ResetForm();"  />
                </td>
              </tr>
            </table>
          </td>
        </tr>
      </table>
      <table align="center" border="0" width="98%" cellpadding="0" cellspacing="0">
        <tr>
          <td>
            <img src="{$brandtemplatepath}/Images/shim.gif" border="0" width="1" height="10"/>
          </td>
        </tr>
      </table>
    </form>

    <script language="JavaScript">
      <xsl:comment>
        //Hide script from older browsers
        //Set form field validation requirements
        with (document.forms['frmMain']){
        if (elements['txtStartDate']){
        txtStartDate.isRequired = true;
        txtStartDate.isDate = true;
        txtStartDate.fieldname = 'Start Date';
        }
        if (elements['txtEndDate']){
        txtEndDate.isDate = true;
        txtEndDate.fieldname = 'End Date';
        }
        }

        //Change sort order
        function ColumnSort(vCol){
        var col;
        var dir;

        col = document.forms['frmMain'].elements['txtOrderBy'].value;
        dir = document.forms['frmMain'].elements['txtOrderDir'].value;

        if(vCol.toLowerCase() == col.toLowerCase()){
        switch(dir.toLowerCase()){
        case 'asc':
        dir = 'DESC';
        break;
        case 'desc':
        dir = 'ASC';
        break;
        default:
        dir = 'ASC';
        }
        }
        else	{
        col = vCol;
        dir = 'ASC';
        }

        var linkurl;
        var vForm;

        vForm = document.forms['frmMain']

        document.forms['frmMain'].elements['txtOrderBy'].value = col;
        document.forms['frmMain'].elements['txtOrderDir'].value = dir;
        InsertSecurityToken(document.forms['frmMain'].elements['txtOrderBy']);
        document.forms['frmMain'].submit();

        return true;
        }

        //reset form fields
        function ResetForm(){
        document.forms['frmMain'].elements['txtAction'].value='Apply';
        document.forms['frmMain'].elements['txtOrderBy'].value='date';
        document.forms['frmMain'].elements['txtOrderDir'].value='ASC';
        document.forms['frmMain'].elements['cboNotificationFileType'].selectedIndex = 0;

        document.forms['frmMain'].elements['txtWorkgroupSelection'].value='';
        document.forms['frmMain'].elements['txtSelectedWorkgroups'].value='';
        document.forms['frmMain'].elements['txtFileName'].value='';
        document.forms['frmMain'].elements['txtStartDate'].value='';
        document.forms['frmMain'].elements['txtEndDate'].value='';
        InsertSecurityToken(document.forms['frmMain'].elements['txtEndDate']);
        document.forms['frmMain'].submit();
        }

        //End Hiding


        $(document).ready(function() {

        //**************************************************************
        //  Workgroup Selector
        //**************************************************************

        var _vm = new ViewModel();

        var dataCallback = function (resp, widget) {
        var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
        var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroups');
        var ecount = entities.length;
        var wcount = workgroups.length;

        if (ecount === 1 &#38;&#38; wcount === 1)
        {
          var el = $('&lt;input id="txtWorkgroupSelection" name="txtWorkgroupSelection" value="" type="hidden"&gt;&lt;div&gt;&lt;label class="control-label"&gt;&lt;/label&gt;&lt;input value="'+ workgroups[0].label+ '" class="form-control input-md" type="text" readonly="" style="margin-top: -5px;display:inline;" /&gt;&lt;/div&gt;');
          $('#treeBase').html(el);
          _vm['selectedWorkgroup'] = workgroups[0];
          $('#txtWorkgroupSelection').val(workgroups[0].id);
        }
        else {
          var selection = $('#txtWorkgroupSelection').val();            
          _vm.workGroupSelector.wfsTreeSelector('initialize');
          if (selection != '')
            widget.setSelection(selection);
        }

        

        };

        var selectedTreeItem = function(selected, initial) {
            var title = null;

            if ($('#txtWorkgroupSelection').val().length == 0) {
                var entities = _vm.workGroupSelector.wfsTreeSelector('getEntities');
                $('#txtWorkgroupSelection').val(entities[0].id);
            }

            if (selected &#38;&#38; !initial) {

                title = 'Selected: ' + selected['label'];
                _vm.workGroupSelector.wfsTreeSelector('setTitle', title);

                $('#txtWorkgroupSelection').val(selected.id);

                if (selected.isNode) {
                    $('#txtSelectedWorkgroups').val(selected.id);
                } else {
                    var workgroups = _vm.workGroupSelector.wfsTreeSelector('getWorkgroupsForEntity', selected);
                    var workgroupIDs = $.map(workgroups, function(workgroup) {
                        return workgroup.id;
                    });

                    var concatenatedWorkgroupIDs = '';

                    $.each(workgroupIDs, function(key, value) {
                        concatenatedWorkgroupIDs += value + ',';
                    });

                    $('#txtSelectedWorkgroups').val(concatenatedWorkgroupIDs);

                }
            }
        };

        _vm.workGroupSelector = $('#treeBase').wfsTreeSelector({
          useExpander: true,
          entityURL: '/RecHubRaamProxy/api/entity',
          callback: selectedTreeItem,
          dataCallback: dataCallback,
          expanderTitle: "Select an Entity or Workgroup",
          entitiesOnly: false
        });

        function ViewModel(model) {
          var self = this;
          self.model = model;

          self.labels = model.Labels;
          self.selectedWorkgroup = null;
        };

        //**************************************************************

        /****************************  file type  ***************************/
        var fromXML = [{ Id: '-1', Name: '-- All Types --' },<xsl:for-each select="Field[@Name='cboNotificationFileType']">
          {Id: '<xsl:value-of select="@Value"/>', Name: '<xsl:apply-templates select="@FileTypeDescription" mode="escape"/>' },
        </xsl:for-each> {Id: 'remove', Name: 'remove'} ];

        findAndRemove(fromXML, 'Id', 'remove');


        function ViewModel(items) {
        var self = this;
        self.items = items;
        self.setItemValue = function (data) {
        if (data){
        $('select[name=cboNotificationFileType] option[value="' + data.id + '"]').attr('selected', true);
        }
        else{
          //this handles new browsers clear X that gets added to dropdowns and selects the first option 
        $('#FileTypeSelect').wfsSelectbox('setValue', $('#cboNotificationFileType option:first').val());
        }
        }
        }

        var ftSelectVM = new ViewModel(fromXML);
        var ftSelectItems = ftSelectVM.items;
        $('#FileTypeSelect').wfsSelectbox({ 'items': ftSelectItems, 'callback': ftSelectVM.setItemValue, 'displayField': 'Name', 'idField': 'Id' });
        var selectedFTId = '<xsl:for-each select="Field[@Name='cboNotificationFileType']">
          <xsl:choose>
            <xsl:when test="@Value=$NotificationFileType">
              <xsl:value-of select="@Value"/>
            </xsl:when>
          </xsl:choose>
        </xsl:for-each>';
        var selFT = ($.grep(ftSelectItems, function(e){ return e.id == selectedFTId; }))[0];
        if (selFT)
        $('#FileTypeSelect').wfsSelectbox('setValueByData', selFT);

        /****************************  file type  ***************************/

        var startDate = new Date($('#txtStartDate').val());
        var endDate = new Date($('#txtEndDate').val());
        $('#StartDatePicker').wfsDatePicker({dateIn: startDate, callback: startDateChange});

        $('#EndDatePicker').wfsDatePicker({dateIn: endDate, callback: endDateChange});

        });

        function startDateChange(val) {
        document.getElementById('txtStartDate').value = val;
        }

        function endDateChange(val) {
        document.getElementById('txtEndDate').value = val;
        }

        function findAndRemove(array, property, value) {
        $.each(array, function(index, result) {
        if(result[property] == value) {
        //Remove from array
        array.splice(index, 1);
        }
        });
        }

        function findIndex(array, property, value) {
        var newIndex = 0;
        $.each(array, function(index, result) {
        if(result[property] == value) {
        newIndex = index;
        }
        });

        return newIndex;

        }

      </xsl:comment>
    </script>
  </xsl:template>

</xsl:stylesheet>
