<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "<xsl:text disable-output-escaping='yes'>&amp;nbsp;</xsl:text>">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
	<!ENTITY laquo "&#171;">
	<!ENTITY raquo "&#187;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:import href="incStyle.xsl"/>

<xsl:output method="html"/>

<xsl:template name="Title">
	User Preferences
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">User Preferences</span>
</xsl:template>

<xsl:template match="Page">
	<div class="page-portlet-view">
		<xsl:if test="Recordset[@Name='OLUserPreferences']">
			<xsl:apply-templates select="Recordset[@Name = 'OLUserPreferences']" mode="OLUserPreferences"/>
		</xsl:if>
	</div>
</xsl:template>

<xsl:template match="Recordset" mode="OLUserPreferences">
	<br />
	<form method="post">
	<xsl:attribute name="name">frmMain</xsl:attribute>

	<input type="hidden" name="txtAction" value=""/>
	<table align="center" width="98%" cellpadding="0" cellspacing="0" border="0"> 
		<tr><td>
			<table cellpadding="5" cellspacing="0" border="0">
	
				<xsl:for-each select="Record">
					<xsl:variable name="fld"><xsl:value-of select="@PreferenceName"/></xsl:variable>
					<tr>
						<td>
							<xsl:choose>
								<xsl:when test="normalize-space(substring-before(@Description, '~')) = ''">
									<label style="display: block;">
										<xsl:value-of select="@Description"/>
									</label>
								</xsl:when>
								<xsl:otherwise>
									<label style="display: block;">
										<xsl:value-of select="normalize-space(substring-before(@Description, '~'))"/>:
									</label>
									<label style="display: block; font-size: 0.6em; position: relative; top: -6px;">
										(<xsl:value-of select="normalize-space(substring-after(@Description, '~'))"/>)
									</label>
								</xsl:otherwise>
							</xsl:choose>							
							</td>
						<td style="width: 20px;"></td>
						<td  class="reportdata" style="vertical-align: top;">
							<xsl:choose>
								<xsl:when test="@PreferenceName = 'UseCutoff' or @PreferenceName = 'DisplayScannedCheckOnline'">
									<input type="checkbox">
										<xsl:attribute name="name">fld<xsl:value-of select="@PreferenceName"/></xsl:attribute>
										<xsl:choose>
											<xsl:when test="/Page/Form[@Name = 'frmMain']/Field[@Name = $fld]">
												<xsl:if test="/Page/Form[@Name = 'frmMain']/Field[@Name = $fld]/@Value = 'Y'">
													<xsl:attribute name="checked">checked</xsl:attribute>
												</xsl:if>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if test="string-length(@UserSetting) > 0 and @UserSetting = 'Y'">
													<xsl:attribute name="checked">checked</xsl:attribute>
												</xsl:if>
												<xsl:if test="string-length(@UserSetting) = 0 and @DefaultSetting = 'Y'">
													<xsl:attribute name="checked">checked</xsl:attribute>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</input>
								</xsl:when>
								<xsl:when test="@PreferenceName = 'DisplayRemitterNameInPDF'">
									<input type="checkbox" class="formfield">
										<xsl:attribute name="name">fld<xsl:value-of select="@PreferenceName"/></xsl:attribute>
										<xsl:if test ="@UserSetting = 'Y'">
											<xsl:attribute name="checked">1</xsl:attribute>
										</xsl:if>

									</input>
								</xsl:when>
								<xsl:otherwise>
									<input type="text" size="15" class="form-control">
										<xsl:attribute name="name">fld<xsl:value-of select="@PreferenceName"/></xsl:attribute>
										<xsl:choose>
											<xsl:when test="/Page/Form[@Name = 'frmMain']/Field[@Name = $fld]">
												<xsl:attribute name="value">
													<xsl:value-of select="/Page/Form[@Name = 'frmMain']/Field[@Name = $fld]/@Value"/>
												</xsl:attribute>
											</xsl:when>
											<xsl:otherwise>
												<xsl:if test='string-length(@UserSetting) > 0'>
													<xsl:attribute name="value">
														<xsl:value-of select="@UserSetting"/>
													</xsl:attribute>
												</xsl:if>
												<xsl:if test='string-length(@UserSetting) = 0'>
													<xsl:attribute name="value">
														<xsl:value-of select="@DefaultSetting"/>
													</xsl:attribute>
												</xsl:if>
											</xsl:otherwise>
										</xsl:choose>
									</input>
								</xsl:otherwise>
							</xsl:choose>
						</td>
					</tr>	
				</xsl:for-each>
			
				<tr>
					<td align="right" colspan="2" class="formlabel">
						<input type="button" name="cmdSubmit" value="Submit" class="btn btn-primary" onClick="formSubmit(this, 'txtAction');"/>&nbsp;
						<input type="button" name="cmdRestore" value="Restore Defaults" class="btn btn-inverse" onClick="formSubmit(this, 'txtAction');"/>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="height: 3px;">&nbsp;</td>
				</tr>
			</table>
		</td></tr>
	</table>
	</form>	
					
</xsl:template>


</xsl:stylesheet>
