<?xml version="1.0" encoding="ISO-8859-1"?>
<!--DMP CBXOnline XSLT template-->
<!--Copyright 1996-2001 DMPI All rights reserved -->

<!DOCTYPE xsl:stylesheet [
  <!ENTITY nbsp "&#160;">
  <!ENTITY copy "&#169;">
  <!ENTITY middot "&#183;">
]>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:import href="incCustomBranding.xsl"/>
	
	<xsl:output method="html"/>

<xsl:template name="Title">
	Account Summary
</xsl:template>

<xsl:template name="PageTitle">
	<span class="contenttitle">Account Summary</span><br/>
	<span class="contentsubtitle">&nbsp;</span>
</xsl:template>


<xsl:template match="Page">
	<html>
		<head>
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/stylesheet.css" />
			<link rel="stylesheet" type="text/css" href="{$brandtemplatepath}/printview.css" />
		</head>
		<body>
			<div id="container">
				<div id="header">
					<h1>
						<p/>
						Account Deposit Summary
					</h1>
				</div>
				<div id="subheader">
					<h1>
						Deposit Date:&nbsp;<xsl:value-of select="Form[@Name='frmDeposits']/Field[@Name='txtDepositDate']/@Value"/>
					</h1>
				</div>
				<div id="content">
					<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
						<xsl:if test="Recordset[@Name='DailyDeposits']">
							<xsl:apply-templates select="Recordset[@Name = 'DailyDeposits']" mode="DailyDeposits"/>
						</xsl:if>
					</table>
				</div>
			</div>
		</body>
	</html>

</xsl:template>


<xsl:template match="Recordset" mode="DailyDeposits">

  <xsl:variable name="startrecord"><xsl:value-of select="../PageInfo/@StartRecord"/></xsl:variable>

  <input type="hidden" name="txtStart">
 <xsl:attribute name="value"><xsl:value-of select="$startrecord" /></xsl:attribute>
  </input>

	<table width="100%" align="center" cellpadding="0" cellspacing="0" border="0">
		<tr>
			<td align="left" valign="top">
				<table width="100%" cellpadding="5" cellspacing="0" border="0" class="reportmain">
				  <tr>
						<td class="reportheading" align="left" valign="top">Account</td>
						<td class="reportheading" align="right" valign="top">Batch Count</td>
						<td class="reportheading" align="right" valign="top">Transaction Count</td>
						<td class="reportheading" align="right" valign="top">Deposit Total</td>
						<td class="reportheading">&nbsp;</td>
				  </tr>

				  <xsl:for-each select="Record">
				  <tr>
						<xsl:if test='position() mod 2 = 0'>
							 <xsl:attribute name="class">evenrow</xsl:attribute>
						</xsl:if>
						<xsl:if test='position() mod 2 != 0'>
							 <xsl:attribute name="class">oddrow</xsl:attribute>
						</xsl:if>

						<xsl:variable name="totals"><xsl:value-of select="@DepositTotal"/></xsl:variable>

						<xsl:if test="@BatchCount = 0 or not(/Page/UserInfo/Permissions/Permission[@Script='remitdisplay.aspx'])">
							<td class="reportdata" align="left" valign="top"><xsl:value-of select="@LockboxID"/>&nbsp;&middot;&nbsp;<xsl:value-of select="@LockboxName"/></td>
						  <xsl:choose>
								<xsl:when test="string(@ExceedsViewingDays) = 'True' or string(@Released) != 'True'">
							 <td class="reportdata" align="right" valign="top">N/A</td>
							 <td class="reportdata" align="right" valign="top">N/A</td>
						   <td class="reportdata" align="right" valign="top">N/A</td>
						 </xsl:when>
							  <xsl:otherwise>
								  <td class="reportdata" align="right" valign="top"><xsl:value-of select="@BatchCount"/></td>
								  <td class="reportdata" align="right" valign="top"><xsl:value-of select="@TransactionCount"/></td>
							 <td class="reportdata" align="right" valign="top">$<xsl:value-of select="format-number(@DepositTotal,'#,##0.00')"/></td>
						 </xsl:otherwise>
					 </xsl:choose>
						  <td class="reportdata" align="center" valign="top">&nbsp;</td>
						</xsl:if>
						<xsl:if test="@BatchCount > 0 and /Page/UserInfo/Permissions/Permission[@Script='remitdisplay.aspx']">
						 <td class="reportdata" align="left" valign="top">
							<xsl:value-of select="@LockboxID"/>&nbsp;&middot;&nbsp;<xsl:value-of select="@LockboxName"/>
						 </td>
						 <td class="reportdata" align="right" valign="top"><xsl:value-of select="@BatchCount"/></td>
						 <td class="reportdata" align="right" valign="top"><xsl:value-of select="@TransactionCount"/></td>
						 <td class="reportdata" align="right" valign="top">$ <xsl:value-of select="format-number(@DepositTotal,'#,##0.00')"/></td>
						 <td class="reportdata" align="right" valign="top"></td>
						</xsl:if>
				  </tr>
				  </xsl:for-each>
    <tr class="evenrow">
   <td class="reportdata" colspan="3" align="right"><b>Total: </b></td>
   <td class="reportdata" align="right" valign="top">$ <xsl:value-of select="format-number(sum(Record/@DepositTotal), '#,###.00')" /></td>
   <td class="reportdata"></td>
    </tr>
  </table>
			</td>
		</tr>
	</table>

</xsl:template>

</xsl:stylesheet>
