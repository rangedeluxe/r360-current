/*********************************************************
**********************************************************
Purpose:
	Load Version 5.x Stylesheet based off of the
	existing link for the NN4 Stylesheet.
Author:
	Eric Goetzinger.
Inputs: 
	vSheet - String name of the stylesheet to load.
Returns: 
	None.
Revisions:
CR 7235 JMC 11/21/2005
    -Added getDateFor method to launch the calendar.
CR 46055 JMC 09/22/2011
    -Added newPopup() function.
CR 49367 JMC 01/16/2012
    -Modified docView() to popup a new window if browser 
     is IE.  All other browsers will popup new windows in 
     a single window.  Functions docViewSingleWindow() and
     docViewNewWindow() were added for this purpose.
* WI 161064 BLR 08/26/2014
*   - Removed default 'Processing Request' from the Pop-up.
**********************************************************
*********************************************************/
function StyleLoader(vSheet) { 
	var vHref='';
	var vDoc='';
	var vOpen='<link rel="stylesheet" type="text/css" href=';
	var vEnd='">';

	if(document.getElementsByTagName){
		var vCurrent=document.getElementsByTagName('link');
		if(vCurrent) {
			for(var k=0;k<vCurrent.length;k++){
				if(vCurrent[k].rel.toLowerCase()=='stylesheet'){
					var h=vCurrent[k].href
					var x=h.lastIndexOf("/");
					if(x>0){vHref=h.substring(0,x+1);}
					vCurrent[k].disabled=true;
					vDoc=vOpen+'"'+ vHref + vSheet + vEnd;
					document.write(vDoc);
					break;
				}
			}
		}
	}
}		

/*********************************************************
**********************************************************
Purpose:
	Find the DOM for the current browser and set
	the appropriate page variables at global scope.
Author:
	Eric Goetzinger.
Inputs:
	None.
Returns:
	None.
Revisions:
**********************************************************
*********************************************************/
var isDHTML = false;
var isLayers = false;
var isAll = false;
var isID = false;

if(document.getElementById){isID=true; isDHTML=true;}
else{
	if(document.all){isAll=true; isDHTML=true;}
	else{
		browserVer=parseInt(navigator.appVersion);
		if((navigator.appName.indexOf('Netscape')!=-1)&&(browserVer==4)){isLayers=true; isDHTML=true;}
}}

/*********************************************************
**********************************************************
Purpose:
	Find an object passed.
Author:
	Eric Goetzinger.
Inputs:
	vObject - Object to find.
	vStyle -  Boolean to return style or not.
Returns:
	A reference to the object w or w/o style.
Revisions:
**********************************************************
*********************************************************/
function getObject(vObject, vStyle){
	var theObj;

	if(!vStyle){vStyle=false;}


	//MSIE
	if(isAll){theObj=document.all[vObject];}
		
	//NETSCAPE 4.x
	if(isLayers){theObj=document.layers[vObject];}
		
	//OPERA AND NETSCAPE 6.x AND IE 5.x
	if(isID){theObj=document.getElementById(vObject);}
		
	if(!theObj){return;}
	if(vStyle){theObj=(document.layers)?theObj:theObj.style;}
	return theObj;
}

function getWidth(){
	if(window.innerWidth!=null){return window.innerWidth;}
	if(document.body.clientWidth!=null){return document.body.clientWidth;}
}

function getHeight(){
	if(window.innerHeight!=null){return window.innerHeight;}
	if(document.body.clientHeight!=null){return document.body.clientHeight;}
}

function getObjWidth(vObject){
	var theObj = getObject(vObject);
	
	if(theObj.offsetWidth){return theObj.offsetWidth;}
	if(theObj.clip.width){return theObj.clip.width;}
}

function getObjHeight(vObject){
	var theObj = getObject(vObject);

	if(theObj.offsetHeight){return theObj.offsetHeight;}
	if(theObj.clip.height){return theObj.clip.height;}
}

function getObjLeft(vObject){
	var theObj = getObject(vObject);
	var theObjS = getObject(vObject, true);
	var vLeft;

	//NETSCAPE 4.x
	if(theObjS.left){vLeft = theObjS.left;}	

	//MSIE 4.x, 5.x AND OPERA
	if(theObjS.pixelLeft){vLeft = theObjS.pixelLeft;}	

	//NETSCAPE 6.x
	if(theObj.offsetLeft){vLeft =  theObj.offsetLeft;}

	if(!vLeft){return 0;}
	else{return vLeft;}
}	


function getObjTop(vObject){
	var theObj = getObject(vObject);
	var theObjS = getObject(vObject, true);
	var vTop;

	//NETSCAPE 4.x
	if(theObjS.top){vTop = theObjS.top;}
	
	//MSIE 4.x, 5.x AND OPERA
	if(theObjS.pixelTop){vTop = theObjS.pixelTop;}
	
	//NETSCAPE 6.x
	if(theObj.offsetTop){vTop = theObj.offsetTop;}

	if(!vTop){return 0;}
	else{return vTop;}
}	

/*********************************************************
**********************************************************
Purpose:

Author:
	Eric Goetzinger.
Inputs:
	None.
Returns:
	None.
Revisions:
**********************************************************
*********************************************************/
if(document.layers){
	pgW = innerWidth;
	pgH = innerHeight;
}

function NN4reload(){
	if(innerWidth != pgW || innerHeight != pgH){location.reload();}
}
if(document.layers){onresize=NN4reload;}


var browserIsNS;
var browserIsIE;

// Determine browser type
//if (navigator.appName == "Netscape" && document.layers != null) {browserIsNS = true;}
if (navigator.appName == "Netscape"){browserIsNS = true;}
else if (navigator.appName == "Microsoft Internet Explorer" && document.all != null){browserIsIE = true;}


/***********************************************************
* Purpose:		Moves the hidden field PageReqID from the stash form to the form being submitted
* Input(s):		Submitting form button, hidden form action field.
* Output:		None.
***********************************************************/
function InsertSecurityToken(frmobj) {
    // Add Page Request ID to form
    var input = document.getElementById("ScrTkn");
    var bNewElement = false;

    if (input == null) {
        bNewElement = true;
        input = document.createElement("input");
        input.setAttribute("id", "ScrTkn");
        input.setAttribute("type", "hidden");
        input.setAttribute("name", "hdtSecurity_Token");
    }
    input.setAttribute("value", document.getElementById("Security_Token").value);

    /* The following commented out code will need to be added back in someday
     *   to prevent clickjacking attacks but as these pages will be displayed in an
     *   I-Frame this protection can not be implemented at this time.
     */
    //if (self == top) {
        if (bNewElement) {
            frmobj.form.appendChild(input);
        }
    //}
    //else {
    //    var errorType = document.createElement("input");

    //    errorType.setAttribute("id", "ErrTyp");
    //    errorType.setAttribute("type", "hidden");
    //    errorType.setAttribute("name", "hdtErrTyp");
    //    errorType.setAttribute("value", "ClickJacking attempt");
    //    frmobj.form.appendChild(errorType);
    //}
}

/***********************************************************
* Purpose:		Handles form submit from form button
* Input(s):		Submitting form button, hidden form action field.
* Output:		None.
***********************************************************/
function formSubmitWithoutValidation2(form, field, fieldValue) {
    InsertSecurityToken(field);
    form.elements[field.name].value = fieldValue;
    form.submit();
}

/***********************************************************
* Purpose:		Handles form submit from form button
* Input(s):		Submitting form button, hidden form action field.
* Output:		None.
***********************************************************/
function formSubmitWithoutValidation(frmobj, field) {
    var frm = frmobj.form;

    InsertSecurityToken(frmobj);
    if (frmobj.type == "button") {
        // Save button label in hidden action field.
        frm.elements[field].value = frmobj.value;
    } else {
        // if (frmobj.type == "checkbox" || frmobj.type == "radio") {
        frm.elements[field].value = frmobj.name;
    }
    frm.submit();
}

/***********************************************************
* Purpose:		Handles form submit and validation from form button
* Input(s):		Submitting form button, hidden form action field.
* Output:		None.
***********************************************************/
function formSubmit (frmobj,field) {
	var frm = frmobj.form;

	InsertSecurityToken(frmobj);
	if (frmobj.type == "button") {
		// Save button label in hidden action field.
		frm.elements[field].value = frmobj.value;

	    // Validate form and submit.
	    if (formValidate(frmobj.form)) {
	    	frm.submit();
	    }

	} else {
	// if (frmobj.type == "checkbox" || frmobj.type == "radio") {
		frm.elements[field].value = frmobj.name;
    	frm.submit();
	}

}


/***********************************************************
* Purpose:		Validate input form.
* Input(s):		Form object.
* Output:		Returns true if validation successful; false otherwise.
*
* Custom validation:
*   isRequired		: Input required. true/false.
*   isNumeric		: Simple numeric field. true/false.
*   isDate			: Date field. true/false
*   max				: Maximum numeric value.
*   min				: Minimum numeric value.
*   fieldname		: Custom field name for display.
*   errortext		: Custom error message for display.
*   custompattern	: Custom Regular Expression pattern.
***********************************************************/
function formValidate (frm) {
	var required = new Array();
	var errors = new Array();
	var msg = "";

	for (var i = 0; i < frm.elements.length; i++) {
		var e = frm.elements[i];

		if (!e.fieldname) {
			e.fieldname = e.name;
		}

		if (!e.errortext) {
			e.errortext = "";
		}

		// Create default numeric error message.
		if (e.isNumeric) {
			var errmsg = e.fieldname + ' must be a number';

			if (e.min != null)
				errmsg += ' greater than ' + e.min;
			if (e.max != null && e.min != null)
				errmsg += ' and less than ' + e.max;
			else if (e.max != null)
				errmsg += ' less than ' + e.max;
			errmsg += ".";
			//e.errortext = errmsg + '  ' + e.errortext;
			e.errortext = errmsg;
		}

		if (e.type=="text" || e.type == "textarea" || e.type == "password") {

			// Check for required fields.
			if (e.isRequired) {
				if ((e.value == null) || (e.value == "") || isBlank(e.value)) {
					if (e.isNumeric) {
						required[required.length] = e.errortext;
					} else {
						required[required.length] = e.fieldname + '.  ' + e.errortext;
					}
					continue;
				}
			}

			// Check numeric fields.
			if (e.isNumeric && !isBlank(e.value)) {
				if (!isNumeric(e.value) ||
					((e.min != null) && (e.min != "") && (e.value < e.min)) ||
					((e.max != null) && (e.max != "") && (e.value > e.max))) {
					//var errmsg = e.fieldname + ' must be a number';
					errors[errors.length] = e.errortext;
					continue;
				}
			}

			// Check date fields.
			if (e.isDate && !isBlank(e.value)) {
				if (!isDate(e.value)) {
					errors[errors.length] = e.fieldname + ' must be a valid date in mm/dd/yyyy format.  ' + e.errortext;
					continue;
				}
			}

			// Check custom fields.
			if (e.pattern && !isBlank(e.value)) {
				if (!PatternMatch(e.value,e.pattern)) {
					errors[errors.length] = e.fieldname + ' invalid.  ' + e.errortext;
					continue;
				}
			}
		}

		if (e.type=="select-one") {
			// Check for required fields.
			if ((e.isRequired) && e.selectedIndex == 0) {
				required[required.length] = e.fieldname + '.  ' + e.errortext;
				continue;
			}
		}
	}

	msg = "____________________________________________________________\n\n";
	msg += "The form was not submitted because of the following error(s).\n";
	msg += "Please correct these error(s) and re-submit.\n";
	msg += "____________________________________________________________\n\n";

	if (required.length > 0) {
		msg += "- The following field(s) are required entry:\n"
		for (var i=0; i < required.length; i++) {
			msg += "\t" + required[i] + "\n";
		}
	}

	if (errors.length > 0) {
		msg += "\n"
		for (var i = 0; i < errors.length; i++) {
			msg += "- " + errors[i] + '\n';
		}
	}

	if (required.length > 0 || errors.length > 0) {
		window.alert (msg);
		return false;
	}
	return true;
}


/***********************************************************
* Purpose:		Validates string as blank.
* Input(s):		String to validate.
* Output:		Returns false if string blank; true otherwise.
***********************************************************/
function isBlank (s) {
	for (var i = 0; i < s.length; i++) {
		var c = s.charAt(i);
		if ((c != ' ') && (c != '\n') && (c != '\t')) return false;
	}
	return true;
}


/***********************************************************
* Purpose:		Validates string as numeric.
* Input(s):		String to validate.
* Output:		Returns true if numeric; false otherwise.
***********************************************************/
function isNumeric (s) {
	var value = parseFloat(s);

	if (isNaN(value)) {

		return false;
	}
	return true;
}


/***********************************************************
* Purpose:		Validates string as date.
* Input(s):		String to validate.
* Output:		Returns true if valid date; false otherwise.
***********************************************************/
function isDate (s) {
	var strSeparatorArray = new Array("-"," ","/",".");
	var strDateArray;
	var strDay;
	var blnFound = false;	// Found date separator
	var strMonth;
	var strDay;
	var strYear;
	var intMonth;
	var intDay;
	var intYear;

	if (s.length < 1)
		return false;

	for (var iSeparator = 0; iSeparator < strSeparatorArray.length; iSeparator++) {
		if (s.indexOf(strSeparatorArray[iSeparator]) != -1) {
			// found separator
			strDateArray = s.split(strSeparatorArray[iSeparator]);	// Split date into substrings
			if (strDateArray.length != 3) {
				iSeparator = strSeparatorArray.length;
				return false;
			}
			blnFound = true;
		}
	}

	if (!blnFound)
		return false;

	strMonth = strDateArray[0];
	strDay = strDateArray[1];
	strYear = strDateArray[2];

	if (strYear.length == 2)
		strYear = '20' + strYear;

	intMonth = parseInt(strMonth, 10);
	intDay = parseInt(strDay, 10);
	intYear = parseInt(strYear, 10);

	if (isNaN(intMonth) || isNaN(intDay) || isNaN(intYear))
		return false;

	if (strYear.length > 4)
		return false;

	if (intMonth < 1 || intMonth > 12)
		return false;

	if ((intMonth == 1 || intMonth == 3 || intMonth == 5 || intMonth == 7 || intMonth == 8 || intMonth == 10 || intMonth == 12) && (intDay < 1 || intDay > 31))
		return false;

	if ((intMonth == 4 || intMonth == 6 || intMonth == 9 || intMonth == 11) && (intDay < 1 || intDay > 30))
		return false;

	if (intMonth == 2) {
		if (intDay < 1) {
			return false;
		}
		if (LeapYear(intYear) == true) {
			if (intDay > 29) {
				return false;
			}
		} else {
			if (intDay > 28) {
				return false;
			}
		}
	}

	return true;
}


/***********************************************************
* Purpose:		Determines if a year is a leap year.
* Input(s):		Integer year value.
* Output:		Returns true if leap year; false otherwise.
***********************************************************/
function LeapYear(intYear) {
	if (intYear % 100 == 0) {
		if (intYear % 400 == 0) {
			return true;
		}
	} else {
		if ((intYear % 4) == 0) { return true; }
	}
	return false;
}


/***********************************************************
* Purpose:		Validates pattern in string.
* Input(s):		String to validate, Pattern
* Output:		Returns true if pattern found; false otherwise.
***********************************************************/
function PatternMatch(s,p) {
	var pattern = new RegExp (p,"i");

	if (pattern.test (s)) {
		return true;
	}
	return false;
}


/***********************************************************
* Purpose:
* Input(s):
* Output:
***********************************************************/
//function DateRange (from,to) {
//	if (Date.parse(from.value) <= Date.parse(to.value)) {
//		alert("The dates are valid.");
//	} else {
//		if (from.value == "" || to.value == "")
//			alert("Both dates must be entered.");
//		else
//			alert("To date must occur after the from date.");
//	}
//}


/***********************************************************
* Purpose:		Event handler to select text in a form element
* Input(s):		none.
* Output:		none.
***********************************************************/
function setFocus() {
	this.select();
}


/***********************************************************
* Purpose:		Create event handlers for document forms.
* Input(s):		none.
* Output:		none.
***********************************************************/
function initFocusHandler () {
	// Loop through all forms in document.
	for (var i = 0; i < document.forms.length; i++) {
		// Loop through all elements of form.
		for (var j = 0; j < document.forms[i].elements.length; j++) {
			var e = document.forms[i].elements[j];

			// Create event handler to automatically select text.
			if (e.type=="text" || e.type=="textarea" || e.type=="password" || e.type=="fileupload") {
				e.onfocus = setFocus;
			}
		}

	}
}



/***********************************************************
* Purpose:		Set focus to first field in form
* Input(s):		none.
* Output:		none.
***********************************************************/
function initFormFocus() {
   if (document.forms.length > 0) {
      var field = document.forms[0];
      for (i = 0; i < field.length; i++) {
          if ((field.elements[i].type == "text") || (field.elements[i].type == "textarea") || (field.elements[i].type.toString().charAt(0) == "s")) {
              if (document.forms[0].elements[i].style.visibility != "hidden" && document.forms[0].elements[i].style.display != "none" && document.forms[0].elements[i].disabled != true) {
                  document.forms[0].elements[i].focus();
              }
            break;
         }
      }
   }
}


/***********************************************************
* Purpose:		Open window for image viewing
* Input(s):		Page to open.
* Output:		none.
***********************************************************/
function docView(vPage) {
    if (browserIsIE) {
        docViewNewWindow(vPage);
    } else {
        docViewSingleWindow(vPage);
    }
}

var gWindow;
function docViewSingleWindow(vPage) {
    var xratio;
    var yratio;

    xratio = .75;
    yratio = .75;

    if(gWindow && !gWindow.closed){
        gWindow.close();
    }			
	
    if(document.all){
        gWindow = window.open('', 'Viewer', 'resizable,left=0,top=0');
        gWindow.window.resizeTo(screen.availWidth * xratio, screen.availHeight * yratio);		
    }else if(document.layers){
        gWindow = window.open('', 'Viewer', 'resizable,left=0,top=0');
        gWindow.window.outerHeight = screen.availHeight * yratio;
        gWindow.window.outerWidth = screen.availWidth * xratio;
    }else if(document.getElementById){
        gWindow = window.open('', '', 'resizable,left=0,top=0');
        gWindow.window.outerHeight = screen.availHeight * yratio;
        gWindow.window.outerWidth = screen.availWidth * xratio;
    }

    gWindow.document.open();
    gWindow.document.clear();
    //gWindow.document.write('<h3>Processing Request...</h3>');
    gWindow.document.close();
    gWindow.location = vPage;
    gWindow.focus();	
}

function docViewNewWindow(vPage) {

    var xratio;
    var yratio;

    xratio = .75;
    yratio = .75;

    var objWindow = window.open('', '', 'width=' + screen.availWidth * xratio + ',height=' + screen.availHeight * yratio + ',menubar=yes,toolbar=yes,location=yes,status=yes,scrollbars=auto,resizable=yes');
    objWindow.location.href = vPage;
    objWindow.focus();
}

function MoveSelected(vSource, vDestination){
	if(vSource == null || vDestination == null){
		return;
	}

	//move the selected source options to the destination
	for(var i = 0; i < vSource.options.length; i++){
		if(vSource.options[i].selected){
			vDestination.options[vDestination.options.length] = new Option(vSource.options[i].text, vSource.options[i].value, false, false);
		}
	}

	//remove the moved source options from the list
	for(var i = vSource.options.length - 1; i >= 0; i--){
		if(vSource.options[i].selected){
			vSource.options[i] = null;
		}
	}

	//clear any selected items in both lists
	vSource.selectedIndex = -1;
	vDestination.selectedIndex = -1;

	return;
}


function MoveAll(vSource, vDestination){
	if(vSource == null || vDestination == null){
		return;
	}

	//move all source options to destintation
	for(var i = 0; i < vSource.options.length; i++){
		vDestination.options[vDestination.options.length] = new Option(vSource.options[i].text, vSource.options[i].value, false, false);
	}

	vSource.options.length = 0;

	//clear any selected items in both lists
	vSource.selectedIndex = -1;
	vDestination.selectedIndex = -1;

	return;
}


function MoveUp(obj){
	if(obj == null){
		return;
	}

	//calculate next index for each selected option
	for(var i = 0; i < obj.options.length; i++){
		if(obj.options[i].selected){
			if(i != 0 && !obj.options[i - 1].selected){
				SwapOptions(obj, i, i - 1);
			}
		}
	}

	return;
}


function MoveDown(obj){
	if(obj == null){
		return;
	}

	//calculate next index for each selected option
	for(var i = obj.options.length - 1; i >= 0; i--){
		if(obj.options[i].selected){
			if(i != obj.options.length - 1 && ! obj.options[i + 1].selected){
				SwapOptions(obj, i, i + 1);
			}
		}
	}

	return;
}


function SwapOptions(obj, vSourceIndex, vDestinationIndex){
	if(obj == null){
		return;
	}

	//temp option to be swaped
	var temp = new Option(obj[vSourceIndex].text, obj[vSourceIndex].value, false, false);

	//temp option to be swaped with
	var temp2 = new Option(obj[vDestinationIndex].text, obj[vDestinationIndex].value, false, false);

	//swap options
	obj[vSourceIndex] = temp2;
	obj[vDestinationIndex] = temp;

	obj[vDestinationIndex].selected = true

	return;
}


function SelectAll(vAvailable, vSelected){
	if(vAvailable == null || vSelected == null){
		return;
	}

	//select all available options before submitting
	for(var i = 0; i < vAvailable.options.length; i++){
		vAvailable.options[i].selected = true;
	}

	//select all selected options before submitting
	for(var i = 0; i < vSelected.options.length; i++){
					vSelected.options[i].selected = true;
	}

	return;
}


/*********************************************************
**********************************************************
Purpose:	Generate advertisement for site
Author: 	Eric Goetzinger.
**********************************************************
*********************************************************/
var arrAdBanner = new Array();

function objAdBanner(url, img, alt){
	this.URL = url;
	this.IMG = img;
	this.ALT = alt;
}

function buildAdBanner(){
	var strHTML;
	var strALT;
	var i;
	var objAdBanner;

	strALT = '';
	strHTML = '';

	if(arrAdBanner.length == 0){
		document.write('&nbsp;');
		return;
	}else{
		//randomly select an adbanner to display;
		i = Math.floor((Math.random() * arrAdBanner.length));		
		
		objAdBanner = arrAdBanner[i];

		//check if img provided
		if(objAdBanner.IMG.length == 0){
			strHTML = '&nbsp;';
		}else{		
			//check if alt provided
			if(objAdBanner.ALT.length > 0){
				strALT = ' alt=\"' + objAdBanner.ALT + '\"';
			}

			//check if url provided
			if(objAdBanner.URL.length > 0){
				strHTML = '<a href=\"' + objAdBanner.URL + '\">';
				strHTML += '<img src=\"' + objAdBanner.IMG + '\" border=\"0\"';
				strHTML += strALT + '></a>';
			}else{
				strHTML = '<img src=\"' + objAdBanner.IMG + '\" border=\"0\"' + strALT + '>';
			}
		}

		document.write(strHTML);
		return;
	}
}

function getDateFor(ctl, anchorID, divName, includeDropDowns) {

    var bolUseDiv = false;

    /*
    if (is_gecko) {
        //alert('browser: Gecko');
        bolUseDiv = true;
    }
    else if (is_nav4) {
        //alert('browser: Navigator4');
        bolUseDiv = false;
    }
    else if (is_ie5up) {
        //alert('browser: ie5+');
        bolUseDiv = true;
    }
    else {
        bolUseDiv = false;
    }
    */

    if(bolUseDiv) {
        var calendar = new CalendarPopup(divName);
        if(includeDropDowns) {
            calendar.showNavigationDropdowns();
        }
        calendar.select(ctl, anchorID, 'MM/dd/yyyy');
    } else {
        var calendar = new CalendarPopup();
        calendar.select(ctl, anchorID, 'MM/dd/yyyy'); 
    }
}

function formValidateNegPattern(frm) {
    var required = new Array();
    var errors = new Array();
    var msg = "";

    for (var i = 0; i < frm.elements.length; i++) {
        var e = frm.elements[i];

        if (!e.fieldname) {
            e.fieldname = e.name;
        }

        if (!e.errortext) {
            e.errortext = "";
        }

        // Create default numeric error message.
        if (e.isNumeric) {
            var errmsg = e.fieldname + ' must be a number';

            if (e.min != null)
                errmsg += ' greater than ' + e.min;
            if (e.max != null && e.min != null)
                errmsg += ' and less than ' + e.max;
            else if (e.max != null)
                errmsg += ' less than ' + e.max;
            errmsg += ".";
            //e.errortext = errmsg + '  ' + e.errortext;
            e.errortext = errmsg;
        }

        if (e.type == "text" || e.type == "textarea" || e.type == "password") {

            // Check for required fields.
            if (e.isRequired) {
                if ((e.value == null) || (e.value == "") || isBlank(e.value)) {
                    if (e.isNumeric) {
                        required[required.length] = e.errortext;
                    } else {
                        required[required.length] = e.fieldname + '.  ' + e.errortext;
                    }
                    continue;
                }
            }

            // Check numeric fields.
            if (e.isNumeric && !isBlank(e.value)) {
                if (!isNumeric(e.value) ||
					((e.min != null) && (e.min != "") && (e.value < e.min)) ||
					((e.max != null) && (e.max != "") && (e.value > e.max))) {
                    //var errmsg = e.fieldname + ' must be a number';
                    errors[errors.length] = e.errortext;
                    continue;
                }
            }

            // Check date fields.
            if (e.isDate && !isBlank(e.value)) {
                if (!isDate(e.value)) {
                    errors[errors.length] = e.fieldname + ' must be a valid date in mm/dd/yyyy format.  ' + e.errortext;
                    continue;
                }
            }

            // Check custom fields.
            if (e.pattern && !isBlank(e.value)) {
                if (PatternMatch(e.value, e.pattern)) {
                    errors[errors.length] = e.fieldname + ' invalid.  ' + e.errortext;
                    continue;
                }
            }
        }

        if (e.type == "select-one") {
            // Check for required fields.
            if ((e.isRequired) && e.selectedIndex == 0) {
                required[required.length] = e.fieldname + '.  ' + e.errortext;
                continue;
            }
        }
    }

    msg = "____________________________________________________________\n\n";
    msg += "The form was not submitted because of the following error(s).\n";
    msg += "Please correct these error(s) and re-submit.\n";
    msg += "____________________________________________________________\n\n";

    if (required.length > 0) {
        msg += "- The following field(s) are required entry:\n"
        for (var i = 0; i < required.length; i++) {
            msg += "\t" + required[i] + "\n";
        }
    }

    if (errors.length > 0) {
        msg += "\n"
        for (var i = 0; i < errors.length; i++) {
            msg += "- " + errors[i] + '\n';
        }
    }

    if (required.length > 0 || errors.length > 0) {
        window.alert(msg);
        return false;
    }
    return true;
}

// Popup window code
function newPopup(url) {
    popupWindow = window.open(
				url, 'popUpWindow', 'height=700,width=800,left=10,top=10,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no,status=yes')
}
