var gCalendar;
var gReturn;
var gDir;
var gMonth;
var gYear;
var gFormat;
var gMonthSelect;
var gYearSelect;
var gMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
var gStdDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var gLeapDays = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
var gNow = new Date();
var isIE = (navigator.appName.indexOf("Microsoft") != -1) ? true : false;
var isNS = (navigator.appName.indexOf("Netscape") != -1) ? true : false;

function getDateFor(pReturn, pDir, pFormat) {
  /**
  pReturn	:  Item date to be returned to
  pDir		:  Select box months (+12 Increment, -12 Decrement)
  pMonth	:  0 to 11 for January through December
  pYear		:  4 digit year
  pFormat	:  Date format (mm/dd/yyyy, mm/dd/yy, mm-dd-yyyy, mm-dd-yy)
  **/

  if (pDir == null || pDir == "")
    pDir = "+12";
  pMonth = new String(gNow.getMonth());
  pYear = new String(gNow.getFullYear().toString());
  if (pFormat == null || pFormat == "")
    pFormat = "mm/dd/yyyy";

  gCalendar = window.open("", "Calendar", "width=250,height=250,status=no,resizable=no,top=200,left=200");
  gReturn = pReturn;
  gDir = pDir;
  gMonth = pMonth;
  gYear = pYear;
  gFormat = pFormat.toUpperCase();

  buildCalendar(gMonth + gYear);
}

function buildCalendar(pSelected) {
  /**
  Build the calendar for the month selected
  **/

  //SET MONTH AND YEAR OF THE VALUE SELECTED
  gMonthSelect = parseInt(pSelected.substring(0, (pSelected.length-4)));
  gYearSelect = parseInt(pSelected.substr((pSelected.length-4), 4));

  with(gCalendar.document){
    open();
    writeln("<html>");
    writeln("<head>");
    writeln("<link rel='stylesheet' text='text/css' href='../Styles/Online/stylesheet.css'>");
    writeln("<title>Date Selector</title>");
    writeln("</head>");
    writeln("<body>");
     writeln("<table width='95%' height='95%' border='0' cellspadding='2' cellspacing='0' align='center'>");
     writeln("<tr>");
     writeln("\t<td align='center' valign='top'>");
      writeln("\t<table width='100%' border='1' cellspadding='2' cellspacing='0' align='center'>");
          writeln("\t\t<tr class='reportheading'>");
          writeln("\t\t\t<form name='frmMonths'>");
          writeln("\t\t\t<td width='100%' height='30' align='center' valign='middle' colspan='7'>");          
            writeln("\t\t\t<select name='cboMonths' onChange='opener.chgMonth(this.form)'>");
            //BUILD MONTHS AVAILABLE FOR SELECT BOX 	
            calOptions(pSelected);                      		
            writeln("\t\t\t</select>");
	  writeln("\t\t\t</td>");
          writeln("\t\t\t</form>");
        writeln("\t\t</tr>");
        writeln("\t\t<tr class='oddrow' height='30' valign='middle'>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Sun</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Mon</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Tue</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Wed</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Thur</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Fri</td>");
          writeln("\t\t\t<td width='14%' align='center' class='formlabel'>Sat</td>");
        writeln("\t\t</tr>");
        //BUILD DAYS FOR MONTH SELECTED
	calData(pSelected);
      writeln("\t</table>");
    writeln("\t</td>");
    writeln("</tr>");
    writeln("<tr>");
      writeln("\t<td align='center' valign='bottom'><form><input type='button' name='cmdClose' value='Close Window' class='formfield' onClick='self.close();'></form></td>");
    writeln("</tr>");
    writeln("</table>");
    writeln("</body>");
    writeln("</html>");
    close();}
}

function calOptions(pSelected) {
  /**
  Build the months either +/- 11 months from current month
  **/

  var iStart = gNow.getMonth();
  var iNewYear;

  //INCREMENT MONTHS
  if (gDir == "+12"){
    for (i = iStart; i < 12; i++){
      if (pSelected == i + gYear)
        gCalendar.document.writeln("\t\t\t\t<option value='" + i + gYear + "' SELECTED>" + gYear + " &middot; " + gMonths[i] + "</option>");
      else
        gCalendar.document.writeln("\t\t\t\t<option value='" + i + gYear + "'>" + gYear + " &middot; " + gMonths[i] + "</option>");
    }
    if (iStart > 0){
      iNewYear = new String(gNow.getFullYear() + 1);
      for (i = 0; i < iStart; i++){
        if (pSelected == i + iNewYear)
          gCalendar.document.writeln("\t\t\t\t<option value='" + i + iNewYear + "' SELECTED>" + iNewYear + " &middot; " + gMonths[i] + "</option>");
        else
          gCalendar.document.writeln("\t\t\t\t<option value='" + i + iNewYear + "'>" + iNewYear + " &middot; " + gMonths[i] + "</option>");
      }
    }
  }
  //DECRMENT MONTHS
  else{
    if (iStart < 11){
      iNewYear = new String(gNow.getFullYear() -1);
      for (i = iStart + 1; i < 12; i++){
        if (pSelected == i + iNewYear)
          gCalendar.document.writeln("\t\t\t\t<option value='" + i + iNewYear + "' SELECTED>" + iNewYear + " &middot; " + gMonths[i] + "</option>");
        else
          gCalendar.document.writeln("\t\t\t\t<option value='" + i + iNewYear + "'>" + iNewYear + " &middot; " + gMonths[i] + "</option>");
      }
    }
    for (i=0; i <= iStart; i++){
      if (pSelected == i + gYear)
        gCalendar.document.writeln("\t\t\t\t<option value='" + i + gYear + "' SELECTED>" + gYear + " &middot; " + gMonths[i] + "</option>");
      else
        gCalendar.document.writeln("\t\t\t\t<option value='" + i + gYear + "'>" + gYear + " &middot; " + gMonths[i] + "</option>");
    }
  }
}

function chgMonth(pForm) {
  /**
  Determine the month selected and build calendar for that month/year
  **/
	
  var vSelected = pForm.cboMonths.options[pForm.cboMonths.selectedIndex].value;
  buildCalendar(vSelected);
}

function calData(pSelected) {
  /**
  Build the calendar data for the month / year selected
  **/

  var vDate = new Date();
  var vDay = 1;
  var vFirstDay;
  var vLastDate;
  var vDone = 0;
  vDate.setDate(1);
  vDate.setMonth(gMonthSelect);
  vDate.setFullYear(gYearSelect);
  vFirstDay = vDate.getDay();

  //DETERMINE LEAP YEAR AND SET LAST DATE FOR MONTH
  if (calcLeapYear(gYearSelect))
    vLastDate = gLeapDays[gMonthSelect];
  else
    vLastDate = gStdDays[gMonthSelect];

  //PLACE BLANK CELLS BEFORE FIRST DAY OF MONTH IF NEEDED
  gCalendar.document.writeln("\t\t<tr align='center' valign='middle'>");
  for (i = 0; i < vFirstDay; i++)
    gCalendar.document.writeln("\t\t\t<td width='14%' bgcolor='#D3D3D3'>&nbsp;</td>");
  
  //BUILD REMAINDER OF FIRST WEEK OF MONTH
  for (i = vFirstDay; i < 7; i++){
    gCalendar.document.writeln("\t\t\t<td width='14%' class='reportdata'><a href='#' onClick=\"JavaScript:self.opener.document." + gReturn + ".value='" + returnDate(vDay) + "';self.close();\">" + vDay + "</a></td>");
    vDay += 1;
  }
  gCalendar.document.writeln("\t\t</tr>");

  //BUILD REMAINDER OF CALENDAR BEGINNING WEEK 2
  for (i = 0; i < 5; i++){
    gCalendar.document.writeln("\t\t<tr align='center' valign='middle'>");
      for (j = 0; j < 7; j++){
        gCalendar.document.writeln("\t\t\t<td width='14%' class='reportdata'><a href='#' onClick=\"JavaScript:self.opener.document." + gReturn + ".value='" + returnDate(vDay) + "';self.close();\">" + vDay + "</a></td>");
        if (vDay == vLastDate){
          vDone = 1;
          break;
        }
        else
          vDay += 1;
      }
    if (j == 6)
      gCalendar.document.writeln("\t\t</tr>");
    if (vDone == 1)
      break;
  }

  //PLACE BLANK CELLS AFTER LAST DAY OF MONTH IF NEEDED
  for (k = 1; k < (7 - j); k++)
    gCalendar.document.writeln("\t\t\t\<td width='14%' bgcolor='#D3D3D3' class='reportdata'><font color='#A9A9A9'>" + k + "</font></td>");
  gCalendar.document.writeln("\t\t</tr>");
}

function calcLeapYear(pYear) {
  /**
  Calculate if year passed is a leap year.
  Leap years are:
    1.) for years divisible by 4
    2.) unless they are divisible by 100
    3.) except for years divisible by 400
  **/

  if ((pYear % 4) == 0){
    if((pYear % 100) == 0 && (pYear % 400) != 0)
      return false;
    return true;
  }
  else
    return false;
}

function returnDate(pDay){
  /**
  Use the date selected to format for return based on
    1. Return Format Passed : gFormat
    2. Return to Field Passed : gReturn  
  **/
  
  var rMonth = (gMonthSelect + 1).toString();
  var rDay = pDay.toString();
  var rYear = gYearSelect.toString();
  var rDate = "";

  switch (this.gFormat){
    case "MM/DD/YYYY":
      rDate = rMonth + "/" + rDay + "/" + rYear;
      break;
    case "MM/DD/YY":
      rDate = rMonth + "/" + rDay + "/" + rYear.substr((rYear.length-2), 2);
      break;
    case "MM-DD-YYYY":
       rDate = rMonth + "-" + rDay + "-" + rYear;
       break;
    case "MM-DD-YY":
      rDate = rMonth + "-" + rDay + "-" + rYear.substr((rYear.length-2), 2); 
      break;
  }
  
  return rDate;
}