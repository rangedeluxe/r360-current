var aMain = new Array();		//array to store the main menus
var aSub = new Array();		//Array to store the sub menus
var iPageMargin = 8;		//Page margin as defined in v5stylesheet
var iSubWidth = 200;		//Width of the sub menu

//menuItem(<has sub menus>, <display text or image>, <url to link to - null if none>, <onclick event - null if none>);

aMain[0] = new menuItem(false, 'Lockbox Summary', 'dailydeposits.aspx');
aMain[1] = new menuItem(false, 'Batch Summary', 'batchsummary.aspx');
aMain[2] = new menuItem(true, 'Search Options', 'lockboxsearch.aspx');
aMain[3] = new menuItem(true, 'Notifications', 'viewnotifications.aspx');
aMain[4] = new menuItem(true, 'Remitters', 'viewremitter.aspx');
aMain[5] = new menuItem(true, 'Online Decisioning', 'oldbatches.aspx?batchmode=0');
aMain[6] = new menuItem(true, 'Users', 'viewusers.aspx');

for(var i=0; i<aMain.length; i++){aSub[i] = new Array();}

//Search Menu
aSub[2][0] = new menuItem(false, 'Remittance Search', 'remitsearch.aspx');
aSub[2][1] = new menuItem(false, 'Lockbox Search', 'LockBoxSearch.aspx');
aSub[2][2] = new menuItem(false, 'Manage Queries', 'maintainquery.aspx');

//Remitter Maintenance
aSub[4][0] = new menuItem(false, 'Add Remitter', 'remittermaint.aspx?mode=add');
aSub[4][1] = new menuItem(false, 'View Remitter File', 'viewremitter.aspx');

//Online Decisioning
aSub[5][0] = new menuItem(false, 'Batches Pending Decisioning', 'oldbatches.aspx?batchmode=0');
aSub[5][1] = new menuItem(false, 'Batches In Decisioning', 'oldbatches.aspx?batchmode=1');
aSub[5][2] = new menuItem(false, 'Rejected Transactions', 'oldrejectedtransactions.aspx');

//User Permissions
aSub[6][0] = new menuItem(false, 'Add User', 'usermaint.aspx?mode=add');
aSub[6][1] = new menuItem(false, 'View Users', 'viewusers.aspx');
aSub[6][2] = new menuItem(false, 'Change Password', 'changepwd.aspx');
aSub[6][3] = new menuItem(false, 'User Preferences', 'userpreferences.aspx');
aSub[6][4] = new menuItem(false, 'Security Questions', 'secretquestion.aspx');


//CENDS
aSub[3][0] = new menuItem(false, 'View Notifications', 'viewnotifications.aspx');
aSub[3][1] = new menuItem(false, 'CENDS Alert Setup Wizard', 'alertwizard.aspx');
aSub[3][2] = new menuItem(false, 'View CENDS Alerts', 'viewalerts.aspx');
aSub[3][3] = new menuItem(false, 'Add CENDS Contact', 'contactmaint.aspx');
aSub[3][4] = new menuItem(false, 'View CENDS Contacts', 'viewcontacts.aspx');
