/***********************************************************
* Purpose:		-Navigates a pagingated recordset to the
*                   specified record number.
* Input(s):		string -Name of calling form
                string -first, next, previous, or last
                string -numeric record number to navigate to.
* Output:		none.
***********************************************************/
function navigateRS(controlName, direction, recordNumber)
{
    controlName.value=direction;
    controlName.form.txtStart.value = recordNumber;
    InsertSecurityToken(controlName);
    controlName.form.submit();
}
    
function navigateRS2(controlName, startControlName, direction, recordNumber)
{
    controlName.value=direction;
    startControlName.value = recordNumber;
    InsertSecurityToken(startControlName);
    controlName.form.submit();
}
