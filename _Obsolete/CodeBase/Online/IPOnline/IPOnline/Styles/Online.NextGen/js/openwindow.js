/************************************************************************************************************************************************
Function to open a new window and load a specific page and page functionality.
************************************************************************************************************************************************/

function openAWindow(pageToLoad, winName, width, height, center, location, menubar, resizable, scrollbars, status, toolbar, hotkeys) {
  var win;
/************************************************************************************************************************************************
Arguments:
	pageToLoad - 	The URL of a page to load in the browser window.
                 	This can be relative or fully qualified.
    	winName - 	Name of the new window.
    		<--Pixel Values-->
	width - 	Window width.
    	height - 	Window height.
		<--1 = True / 0 = False-->
	center -     	Center window.
	location -	URL location field.
	menubar -	Window menubar.
    	resizable -  	Resize window.
	scrollbars - 	Window scrollbars.
	status -	Window status bar.
	toolbar -	Window toolbar.
	hotkeys -	Window shortcut keys.
************************************************************************************************************************************************/
					 
    xposition=0; yposition=0;
    if ((parseInt(navigator.appVersion) >= 4 ) && (center)) {
        xposition = (screen.width - width) / 2;
        yposition = (screen.height - height) / 2;
    }
    var args = "width=" + width + ","; 
    args+= "height=" + height + "," ;
    args+= "location=" + location + ","; 
    args+= "menubar=" + menubar + ",";
    args+= "resizable=" + resizable + ",";
    args+= "scrollbars=" + scrollbars + ",";
    args+= "status=" + status + ","; 
    args+= "toolbar=" + toolbar + ",";
    args+= "hotkeys=" + hotkeys + ",";
    args+= "screenx=" + xposition + ","  //NN Only
    args+= "screeny=" + yposition + ","  //NN Only
    args+= "left=" + xposition + ","     //IE Only
    args+= "top=" + yposition;           //IE Only
    
    if(!win || win.colsed){
      win = window.open( pageToLoad,winName,args );
      return win;
    }else{
      win.focus();
    }
}