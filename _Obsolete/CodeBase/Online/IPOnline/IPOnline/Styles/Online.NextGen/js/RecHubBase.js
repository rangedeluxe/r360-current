﻿var frameworkBase = (function () {

    function applyViewModel(mydata) {
        ko.applyBindings(mydata, $('.frameworkRoot').get(0));
    }

    function activateColumns() {
        $(".tabColumn").sortable({
            connectWith: ".tabColumn",
            stop: function (event, ui) {
                //alert('i changed location');
            }
        });
        $(".tabColumn").disableSelection();
    }

    function tabClick() {
        var selectedTab = this;
        var selectedTabId = 'tabPane' + selectedTab.Id;

        // close all divs
        $('.tab-pane').css("display", "none");

        // if last tab should be cleared... 
        var lastTab = $.data(document.body, 'currentTabId');
        var reloadContent = $.data(document.body, 'currentTabReloadContent') === true;

        // if reloadContent is true for previous tab... clear that tab now
        if (reloadContent) {
            // need to clear out the previous tab
            var cols = $("div[id='" + lastTab + "']").find('.tabColumn');
            $.each(cols, function (index) {
                $(this).empty();
            });
        }

        // load tab if it hasn't yet or if it reloads content
        if (!selectedTab.Loaded || selectedTab.ReloadContent) {
            loadTab(selectedTab);
            // make sure the tab is indicates it loaded
            selectedTab.Loaded = true;
        }

        // set current tab to this selected tab
        $.data(document.body, 'currentTabId', selectedTabId);
        $.data(document.body, 'currentTabReloadContent', selectedTab.ReloadContent);

        // open selected tab
        $("div[id='" + selectedTabId + "']").css("display", "block");
    }

    function loadTab(selectedTab) {
        var tabId = selectedTab.Id;
        // foreach row
        $.each(selectedTab.Rows, function (iRow) {
            var rowId = $(this).attr('Id');
            // foreach column
            $.each(selectedTab.Rows[iRow].Columns, function (iCol) {
                // load views View
                var columnId = $(this).attr('Id');
                $.each(selectedTab.Rows[iRow].Columns[iCol].Views, function (iView) {
                    var viewId = selectedTab.Rows[iRow].Columns[iCol].Views[iView].Id;
                    var viewUrl = selectedTab.Rows[iRow].Columns[iCol].Views[iView].URL;
                    var viewSequence = selectedTab.Rows[iRow].Columns[iCol].Views[iView].Sequence;

                    var pId = tabId + '_' + rowId + '_' + columnId + '_' + viewId + '_' + viewSequence

                    var viewDiv = jQuery('<div/>', {
                        id: pId
                    });
                    viewDiv.addClass("viewContainer");
                    $('#Column' + tabId + '_' + columnId).append(viewDiv);
                    framework.loadById(viewUrl, pId);
                });
            });
        });
    }

    function triggerTab() {
        var currDiv = $.data(document.body, 'currentTabId');
        if (currDiv) {
            $("a[id='" + currDiv + "']").first().trigger('click');
        }
        else {
            $("a[id^='tabPane']").first().trigger('click');
        }
    }

    function unloadPortal() {
        var callbacks = $(window).data("closingCallbacks");
        if (callbacks) {
            callbacks.fire();
            $(window).data("closingCallbacks", null);
        }
    }

    function refreshMenu() {
        framework.doJSON("/Framework/Framework/GetTabs", {}, refreshMenuCallback);
    }

    function refreshMenuCallback(jsonResult, data) {
        if (!jsonResult.HasErrors) {
            $('.frameworkRoot').data("frameworkViewModel").menuItems(jsonResult.Data);
            var tabId = $.data(document.body, 'currentTabId');
            $('#' + tabId).click();
        }
    }

    function init(menu) {

        framework.loadByContainer("/Framework/Framework/FrameworkHeader?showHome=false", $('#loadHeader'));
        framework.loadByContainer("/Framework/Framework/FrameworkFooter", $('#loadFooter'));

        // clear data if there
        $.data(document.body, 'currentTabId', '');
        $.data(document.body, 'currentTabReloadContent', '');

        var frameworkObservableMenu = ko.observableArray(menu);
        var frameworkViewModel = {
            menuItems: frameworkObservableMenu
        }

        $('.frameworkRoot').data("frameworkViewModel", frameworkViewModel);
        // apply menu item data to dom
        applyViewModel(frameworkViewModel);

        // if only one tab hide tab nav
        if (frameworkObservableMenu().length < 2) {
            $('#recTabs').hide();
        }
        //apply style to link columns for view movement
        //activateColumns();

        // on page load select the first tab
        triggerTab();

        $('#frameworkLogout').bind('click', function () {
            unloadPortal();
        });

        $(window).bind('beforeunload', function () {
            unloadPortal();
        });

    }

    return {
        tabClick: tabClick,
        refreshMenu: refreshMenu,
        init: init
    }
}());

(function ($) {
    jQuery.event.special.destroyed = {
        remove: function (o) {
            if (o.handler) {
                o.handler()
            }
        }
    }
})(jQuery)