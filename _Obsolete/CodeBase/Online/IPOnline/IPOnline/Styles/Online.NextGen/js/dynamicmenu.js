
function menuItem(vSubs, vDisplay, vURL, vOnClick){
	this.Subs = vSubs;
	this.Display = vDisplay;
	this.URL = vURL;
	if (vOnClick) { this.onclick = vOnClick; }
	else { this.onclick = null; }
}

function buildMenu(){
	var iTop;
	var iLeft;
	var sHTML;

	sHTML = '';
	sHTML = '<div id=\"mainmenu\" class=\"menustyle\">';
	for(var i=0; i<aMain.length; i++){
		sHTML += '<span id=\"main' + i + '\" class=\"mainmenustyle\">';
		if(aMain[i].URL==null){sHTML += '<a href=\"#\" onMouseOver=\"popMenu(' + i + ');\">' + aMain[i].Display + '</a>';}
		else{sHTML += '<a href=\"' + aMain[i].URL + '\" onMouseOver=\"popMenu(' + i + ');\">' + aMain[i].Display + '</a>';}
		sHTML += '&nbsp;|&nbsp;';
		sHTML +='</span>';
	}
	sHTML += '</div>';
	document.writeln(sHTML);	
}

function buildSub(){
	var sHTML;

	sHTML = '';
	for(var i=0; i<aMain.length; i++){
		if(aMain[i].Subs && aSub[i].length>0){
			sHTML += '<div id=\"sub' + i + '\" class=\"submenu\">';
			sHTML += '<table width=\"' + iSubWidth + '\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"submenuparent\">';
			sHTML += '<tr><td>';
			sHTML += '<table width=\"' + iSubWidth + '\" cellpadding=\"2\" cellspacing=\"1\" border=\"0\">';
			for(var j=0; j<aSub[i].length; j++){
				sHTML += '<tr><td align=\"left\" valign=\"middle\" class=\"submenuchild\">';
				if (aSub[i][j].URL == null) {
				    if (aSub[i][j].onclick == null) { sHTML += '<a href=\"#\">' + aSub[i][j].Display + '</a>'; }
                    else {sHTML += '<a href=\"#\" onclick=\"' + aSub[i][j].onclick + '\">' + aSub[i][j].Display + '</a>';}
                } else { sHTML += '<a href=\"' + aSub[i][j].URL + '\">' + aSub[i][j].Display + '</a>'; }

				sHTML += '</td></tr>';
			}
			sHTML += '</table>';
			sHTML += '</td></tr>';
			sHTML += '</table>';
			sHTML += '</div>';
		}
	}

	document.writeln(sHTML);
}

function popMenu(vNum){
	var vHead = getObject('mainmenu');
	var vCurrent = getObject('main' + vNum);
	var vLeft;
	var vTop;
	var obj;
	var added;

	//BUILD ARRAY TO STORE SUB MENUS FIRST TIME FUNCTION CALLED
	if(!document.bool){
		aItems = new Array();
		document.bool = true;			
	}
	//HIDE ANY SUB MENUS ADDED TO THE ARRAY
	for(var i=0; i<aItems.length; i++){
		if((obj=getObject(aItems[i]))!=null){
			obj=(document.layers)?obj:obj.style;
			obj.visibility = 'hidden';
		}
	}

	//SHOW THE SUB MENU REQUESTED
	if(vNum!=null){
		if((obj=getObject('sub' + vNum))!=null){
			obj=(document.layers)?obj:obj.style;
			if(isID || isAll){
				vLeft = (getObjLeft('main' + vNum));
				vTop = (getObjHeight('brandheader')) + (getObjHeight('mainmenu')) + iPageMargin;
			}
			if(isLayers){
				vLeft = document.layers['main' + vNum].pageX;
				vTop = document.layers['main' + vNum].pageY + document.layers['main' + vNum].clip.height;
			}
			obj.left = vLeft;
			obj.top = vTop;
			obj.visibility = 'visible';
			added = false;
			//ADD THE SUB MENU TO THE ARRAY IF NOT ALREADY ADDED
			for(var i=0; i<aItems.length; i++){if('sub' + vNum == aItems[i]){added=true;}}
			if(!added){aItems[aItems.length++] = 'sub' + vNum;}
		}
	}
}

function NN4hideIt(e){
	var obj=e.target;

	if(obj.NN4hide){obj.visibility='hidden';}
}

function NN4autoHide(){
	var obj;

	if(!isLayers){return;}

	for(var i=0; i<aMain.length; i++){
		if(aMain[i].Subs && aSub[i].length>0){
			if((obj=getObject('sub' + i))!=null){
				obj.captureEvents(Event.MOUSEOUT);
				obj.onmouseout = NN4hideIt;
				obj.NN4hide = true;
			}
		}
	}
}