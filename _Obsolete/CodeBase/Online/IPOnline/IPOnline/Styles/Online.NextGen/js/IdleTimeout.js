﻿var registerKeepAlive = (function () {
    var pingIntervalMs = 60000;
    var idleCheckIntervalMs = 60000;
    var idleTimeoutMs = 604800000; // Initial default = 1 week

    function log(message) {
        //console.log(message);
    }

    function keepAlive(data) {
        log("keepAlive: " + data);
        if (new Date() - framework.getLastWebRequest() > pingIntervalMs) {
            log("Sending keepalive ping: " + data);
            framework.doJSON("/Framework/Framework/Ping", data);
        }
    }
    function checkIdle() {
        var idleTime = new Date() - framework.getLastWebRequest();
        if (idleTime > idleTimeoutMs) {
            log("Idle too long, logging out");
            window.location.href = "/Framework/Framework/Logout";
        } else {
            log("Idle for " + idleTime + " ms");
        }
    }
    function registerKeepAlive(idleTimeoutMilliseconds) {
        idleTimeoutMs = idleTimeoutMilliseconds;
        $(document).on("mousemove.iponline", function (event) {
            keepAlive("mouse");
        });
        $(document).on("keydown.iponline", function (event) {
            keepAlive("keydown");
        });
        window.setInterval(checkIdle, idleCheckIntervalMs);
    }

    return registerKeepAlive;
})();