﻿var assignPayer = function (serviceURL, parentContainerId) {

    //******************************************************
    //***    Module Variables                            ***
    //******************************************************
    var _vm = {};
    var framework = new FrameworkUtils();
    parentContainerId = '#' + parentContainerId;
    var $getE = function (b) { return $(parentContainerId).find(b); };
    var viewer;

    //******************************************************
    //***    View Models                                 ***
    //******************************************************
    var ViewModel = function (model) {
        var self = this;
        self.payer = model;
        self['imageIsBack'] = false;
    }

    //******************************************************
    //***    Register Events                             ***
    //******************************************************
    $('.openPayer').click(function (evt) {
        evt.preventDefault();
        showModal($(this).attr('href'));
    });

    var registerPopup = function () {
        $(document).on('click', "#flipCheck", function () {
            flipCheck();
            return true;
        });

        $(document).on('click', "#formSubmit", function () {
            if ($("#payerForm").valid()) {
                savePayer();
            }
        });
    }

    //******************************************************
    //***    Init Functions                              ***
    //******************************************************
    var setFormValidation = function () {
        $("#payerForm").validate({
            rules: {
                inputName: { required: true }
            },
            messages: { 
                inputName: "A Payer Name must be entered."
            }
        });
    }

    var initializeImageViewer = function () {
        viewer = new Framework.General.ImageViewer();
        viewer.init({ 'viewerDiv': $("#viewer"), 'initImageUrl': getImageUrl() });
    }

    //******************************************************
    //***    Module Functions                            ***
    //******************************************************

    var savePayer = function () {
        var url = serviceURL + 'AssignPayer';
        var data = {
            workgroupID: _vm.payer.WorkgroupId,
            bankID: _vm.payer.BankId,
            clientAcctID: _vm.payer.LockboxId,
            depositDate: new Date(parseInt(_vm.payer.CheckDate.substr(6))),
            batchID: _vm.payer.BatchId,
            txnID: _vm.payer.Txn,
            batchSeq: _vm.payer.Item,
            payerName: _vm.payer.Name,
            lockboxPayerId: _vm.payer.LockboxPayerId,
            globalPayerId: _vm.payer.GlobalPayerId,
            routingNumber: _vm.payer.RoutingNumber,
            accountNumber: _vm.payer.AccountNumber,
            importType: _vm.payer.ImportType
        };

        var savePayerCallback = function (serverResponse) {
            if (serverResponse.HasErrors) {
               
            }
            else {
                closeModal();

                var a = location.host;
                var b = location.pathname;

                if (b.split('/')[b.split('/').length - 1] == 'remitsearch.aspx')
                    window.location = location.protocol + '//' + location.host + location.pathname + "?id=bc"
                else
                    location.reload(true);
                //location.protocol + '//' + location.host + location.pathname
            }
        };

        framework.doJSON(url, $.toDictionary(data), savePayerCallback);
    }

    var flipCheck = function () {
        if (_vm['imageIsBack'] == false)
            _vm['imageIsBack'] = true;
        else
            _vm['imageIsBack'] = false;

        viewer.setImage(getImageUrl()); return false;
    }

    var getImageUrl = function () {
       
        var params = "?bank=" + _vm.payer.BankId +
          "&lckbx=" + _vm.payer.LockboxId +
          "&picDate=" + _vm.payer.PicDate +
          "&batch=" + _vm.payer.SourceBatchID +
          "&seq=" + _vm.payer.Item +
          "&isBack=" + _vm['imageIsBack'] +
          "&srcBatchID=" + _vm.payer.SourceBatchID +
          "&batchSourceShortName=" + _vm.payer.BatchSourceShortName +
          "&importTypeShortName=" + _vm.payer.ImportType;

        return serviceURL + 'GetImage' + params;
    }

    var showModal = function (href) {
        var ww = $(window).width() / 2 - 350;
        var modalOptions = {
            title: 'Assign Payer', width: '800', height: '550', position: [ww, 75],
            closeCallback: closeModal
        };
        framework.showSpinner('Loading...');
        var data = null;
        $.ajax({
            type: "POST",
            url: href,
            success: function (data) {
                _vm = new ViewModel(data);
                ko.applyBindings(_vm, $(".assignPayerModal")[0]);
                initializeImageViewer();
                framework.hideSpinner();
            },
            error: function (xhr, error) {
                if (xhr.status === 401) {
                    location.reload(true);
                }
                else {
                    framework.hideSpinner();
                    alert('Error retrieving data.');
                }
            }
        });

        framework.getModalContent().append('<legend>Assign Payer</legend><div class="assignPayerModal container"><div class="row"><div class="col-md-12"><div id="viewer" class="viewer" style="height: 220px; width: 600px;"></div><button type="button" title="View Reverse Side of Check" id="flipCheck" class="ceImg wfs btn flip" style="float: right; margin-top: -20px; margin-right: 50px;"><i class="ui-icon wfs-icon-flip"></i></button></div><div class="row"><div class="col-md-12" style="border-bottom: solid 2px gray; width: 93%; height: 10px; margin-left: 25px;"></div></div><div class="row" id="formDiv"><form class="form-horizontal" role="form" id="payerForm"><table style="border-spacing: 5px; border-collapse: separate; margin-left: 20px;"><tr><td><label for="inputWorkgroup" class="control-label">Workgroup: </label></td><td><p class="form-control-static" data-bind="text: payer.DisplayLabel"></p></td></tr><tr><td><label for="inputRoutingNumber" class="control-label">Routing Number: </label></td><td><p class="form-control-static" data-bind="text: payer.RoutingNumber" /></td></tr><tr><td><label for="inputAccountNumber" class="control-label">Account Number: </label></td><td><p class="form-control-static" data-bind="text: payer.AccountNumber" /></td></tr><tr><td><label for="inputName" class="control-label">Payer Name: </label></td><td><input type="text" class="form-control" id="inputName" name="inputName" placeholder="Payer Name" data-bind="value: payer.Name" maxlength="60" required="" style="min-width: 200px; max-width: 300px;" /></td></tr><tr><td></td><td><button type="button" class="btn btn-primary" id="formSubmit">Submit</button>&nbsp;<button type="button" class="btn btn-primary" id="formCancel" data-dismiss="modal">Cancel</button></td></tr></table></form></div></div></div>');
        framework.openModal(modalOptions);

        setFormValidation();
        registerPopup();
    }

    var closeModal = function () {
        ko.cleanNode($(".assignPayerModal")[0]);
        $(document).off('click', "#flipCheck");
        $(document).off('click', "#formSubmit");
    }

};