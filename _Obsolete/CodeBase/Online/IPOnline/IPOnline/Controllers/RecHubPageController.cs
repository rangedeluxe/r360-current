﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Web.Mvc;
using Wfs.Raam.Core;
using Wfs.Raam.Core.Helper;


namespace WWFS.RecHub.Online.IPOnline.Controllers
{
    [NoCache]
    public class RecHubPageController : Controller
    {
        public ActionResult Menu()
        {
            var claims = ClaimsPrincipal.Current.Claims;
            var name = claims.First(o => o.Type == WfsClaimTypes.Name).Value;

            int entityId = Convert.ToInt32(claims.First(o => o.Type == WfsClaimTypes.EntityId).Value);
            var brandingmanager = new FrameworkClient.BrandingManager();
            var branding = brandingmanager.GetBranding(entityId);
            var showtitle = branding.ShowTitle && !branding.ShowHeader;
            ViewData["UserName"] = name;
            ViewData["MenuTitle"] = branding.Title;
            ViewData["ShowTitle"] = showtitle;

            return View();
        }

        public ActionResult Header()
        {
            return View();
        }

        public ActionResult Footer()
        {
            return View();
        }        

    }
}