﻿using System;
using System.Xml;

using WFS.RecHub.Common;

using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright © 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Jason Efken
* Date: 01/12/2012    
*
* Purpose:  Calling program for HOA Reports  
*
* Modification History
* CR 53188 JNE 06/08/2012
*    -Added printview check & "_printview" to name of printview version of xsl.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/
namespace WFS.RecHub.Online {
    public partial class HOAReports : _Page {
        private void Page_Load(object sender, EventArgs e) {
            string strReportName = "";
            string strLockboxID = "";
            string strCustomerID = "";
            string strHaveParameters = "no";
            string strHOA_Number = "";
            string strTemp = "";
            int intTemp = 0;
            bool bolPrintView = false;
            DateTime dteTemp;
            DateTime dteDepositDate = DateTime.MinValue;
            XmlDocument xmlDoc;
            XmlDocument xmlParms;
            XmlDocument nodeData = null;
            XmlNode nodeParms;

            // Check Inputs
            if (CleanRequest.TryGetQueryStringValue("haveparameters", out strTemp) || (CleanRequest.TryGetQueryStringValue(" haveparameters", out strTemp))){
                strHaveParameters = strTemp;
            }
            if(dteDepositDate == DateTime.MinValue && Request.QueryString[Constants.QS_PARM_DEPOSITDATE] != null && DateTime.TryParse(CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE], out dteTemp)) {
		            dteDepositDate = dteTemp;
            }else {
                dteDepositDate = DateTime.Now.Date;
            }
            if(CleanRequest.TryGetQueryStringValue("reportname", out strTemp)){
                strReportName = strTemp;
            } else {
                AddMessage(EventType.Error, "The report name is not valid.");
            }
            if(CleanRequest.QueryString[Constants.QS_PARM_CUSTOMERID] != null && CleanRequest.TryGetQueryStringValue(Constants.QS_PARM_CUSTOMERID,out strTemp)){
                strCustomerID = strTemp;
            }
            if(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID] != null && CleanRequest.TryGetQueryStringValue(Constants.QS_PARM_LOCKBOXID, out strTemp)) {
                strLockboxID = strTemp;
            } 

            if(CleanRequest.QueryString["HOA_Number"] != null && CleanRequest.TryGetQueryStringValue("HOA_Number", out strTemp)) {
                strHOA_Number = strTemp;
            } 

           if(CleanRequest.QueryString["printview"] != null && CleanRequest.TryGetQueryStringValue("printview", out intTemp)) {
                bolPrintView = (intTemp == 1);
            } else {
                bolPrintView = false;
            }

            //Convert inputs to input xml
            xmlParms=GetXmlParmsDoc(strReportName, dteDepositDate, strCustomerID, strLockboxID, strHOA_Number);

            //Determine Report Called
            if (strHaveParameters == "no")
            {
                try
                {
                    XmlDocumentResponse respdoc = R360ReportServiceClient.GetHOAParameters(xmlParms);
                    if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                    {
                        nodeData = respdoc.Data;
                    }
                }
                catch (Exception ex)
                {
                    AddMessage(EventType.Error, ex.Message);
                    EventLog.logError(ex);
                    nodeData = null;
                }

                xmlDoc = new XmlDocument();

                if (nodeData != null)
                {
                    xmlDoc = nodeData;
                }

                nodeParms = ipoXmlLib.addElement(xmlDoc.DocumentElement, "Parameters", string.Empty);
                ipoXmlLib.addAttribute(nodeParms, "ReportName", strReportName);
                ipoXmlLib.addAttribute(nodeParms, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
                ipoXmlLib.addAttribute(nodeParms, "CustomerID", strCustomerID);
                ipoXmlLib.addAttribute(nodeParms, "LockboxID", strLockboxID);


                DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_parms.aspx"));
            }
            else
            {
                switch (strReportName)
                {
                    case "HOAAllPMASummary":
                        try
                        {
                            XmlDocumentResponse respdoc = R360ReportServiceClient.GetHOAPMASummary(xmlParms);
                            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                            {
                                nodeData = respdoc.Data;
                            }
                        }
                        catch (Exception ex)
                        {
                            AddMessage(EventType.Error, ex.Message);
                            EventLog.logError(ex);
                            nodeData = null;
                        }
                        if (nodeData == null)
                        {
                            xmlDoc = ipoXmlLib.GetXMLDoc("Page");
                        }
                        else
                        {
                            xmlDoc = new XmlDocument();
                            xmlDoc = nodeData;
                        }
                        if (bolPrintView)
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_allpmasummary_printview.aspx"));
                        }
                        else
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_allpmasummary.aspx"));
                        }
                        break;
                    case "HOAPMASummary":
                        try
                        {
                            XmlDocumentResponse respdoc = R360ReportServiceClient.GetHOAPMASummary(xmlParms);
                            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                            {
                                nodeData = respdoc.Data;
                            }
                        }
                        catch (Exception ex)
                        {
                            AddMessage(EventType.Error, ex.Message);
                            EventLog.logError(ex);
                            nodeData = null;
                        }
                        if (nodeData == null)
                        {
                            xmlDoc = ipoXmlLib.GetXMLDoc("Page");
                        }
                        else
                        {
                            xmlDoc = new XmlDocument();
                            xmlDoc = nodeData;
                        }
                        if (bolPrintView)
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_pmasummary_printview.aspx"));
                        }
                        else
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_pmasummary.aspx"));
                        }
                        break;
                    case "HOAPMADetail":
                        try
                        {
                            XmlDocumentResponse respdoc = R360ReportServiceClient.GetHOAPMADetail(xmlParms);
                            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                            {
                                nodeData = respdoc.Data;
                            }
                        }
                        catch (Exception ex)
                        {
                            AddMessage(EventType.Error, ex.Message);
                            EventLog.logError(ex);
                            nodeData = null;
                        }
                        if (nodeData == null)
                        {
                            xmlDoc = ipoXmlLib.GetXMLDoc("Page");
                        }
                        else
                        {
                            xmlDoc = new XmlDocument();
                            xmlDoc = nodeData;
                        }
                        if (bolPrintView)
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_pmadetail_printview.aspx"));
                        }
                        else
                        {
                            DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_pmadetail.aspx"));
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        private XmlDocument GetXmlParmsDoc(string strReportName, DateTime dteDepositDate, string strCustomerID, string strLockboxID, string strHOA_Number){
            XmlDocument xmlParms;
            XmlNode node;

            // Create parameters xml document
            xmlParms = GetRequestDoc();
            AddPageToXmlDoc(xmlParms);

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ReportName", strReportName);
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDate", dteDepositDate.ToShortDateString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "CustomerID", strCustomerID);
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "LockboxID", strLockboxID);
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "HOA_Number", strHOA_Number);

            return(xmlParms);
        }
    }
}