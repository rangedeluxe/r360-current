using System;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
* 
* Author:   
* Date:     
* 
* Purpose:  Displays a list of Rejected Transactions Meeting
*           parameters specified by the User.
* 
* Modification History
* CR 12522 JMC 06/29/2006
*     -Created File
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldrejectedtransactions : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    string strOLLockboxID;
        //    string strStartDate;
        //    string strEndDate;
        //    int lngStartRecord;
        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    XmlNode nodeRespXml;           
        //    int intTemp;

        //    if(Request.Form["cboOLLockbox"] != null && Request.Form["cboOLLockbox"].Length > 0) {
        //        strOLLockboxID = Request.Form["cboOLLockbox"];
        //    } else if(Request.QueryString["OLLockboxID"] != null && Request.QueryString["OLLockboxID"].Length > 0) {
        //        strOLLockboxID = Request.QueryString["OLLockboxID"];
        //    } else {
        //        strOLLockboxID = "";
        //    }

        //    if(Request.Form["txtStartDate"] != null && Request.Form["txtStartDate"].Length > 0) {
        //        try {
        //            strStartDate = DateTime.Parse(Request.Form["txtStartDate"]).ToShortDateString();
        //        } catch(Exception) {
        //            strStartDate = DateTime.Now.Date.AddDays(-1).ToShortDateString();
        //        }
        //    } else {
        //        strStartDate = DateTime.Now.Date.AddDays(-1).ToShortDateString();
        //    }

        //    if(Request.Form["txtEndDate"] != null && Request.Form["txtEndDate"].Length > 0) {
        //        try {
        //            strEndDate = DateTime.Parse(Request.Form["txtEndDate"]).ToShortDateString();
        //        } catch(Exception) {
        //            strEndDate = DateTime.Now.Date.ToShortDateString();
        //        }
        //    } else {
        //        strEndDate = DateTime.Now.Date.ToShortDateString();
        //    }

        //    // Start Record
        //    if((Request.Form["txtStart"] != null) && (Request.Form["txtStart"].Length > 0) && (int.TryParse(Request.Form["txtStart"], out intTemp)) && (Request.Form["txtAction"] != null) && (Request.Form["txtAction"].Length > 0)) {
        //        lngStartRecord = intTemp;
        //    } else {
        //        lngStartRecord = 1;
        //    }

        //    strReqXml = "<ReqRejectedTransactions>"
        //              + "    <Lockbox OLLockboxID=\"" + strOLLockboxID + "\" />"
        //              + "    <BeginProcessingDate>" + strStartDate + "</BeginProcessingDate>"
        //              + "    <EndProcessingDate>" + strEndDate + "</EndProcessingDate>"
        //              + "    <StartRecord>" + lngStartRecord.ToString() + "</StartRecord>"
        //              + "</ReqRejectedTransactions>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetRejectedTransactions(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    // Add FormFields node to root of document.
        //    nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    ipoXmlLib.addElement(nodeFormFields, "OLLockboxID", strOLLockboxID);
        //    ipoXmlLib.addElement(nodeFormFields, "StartDate", strStartDate);
        //    ipoXmlLib.addElement(nodeFormFields, "EndDate", strEndDate);

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion
    }
}
