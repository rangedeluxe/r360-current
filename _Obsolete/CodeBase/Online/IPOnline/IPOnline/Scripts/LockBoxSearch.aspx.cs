using System;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/19/2010
*
* Purpose:  Creates a request to retrieve all search results as XML
*           and applies the appropriate XSL stylesheet for print view
*           or download as comma seperated.
*
* Modification History
*        JMC 1/20/2003	- Created Page
*VI 5078 JMC 3/4/2003   - Modified functions to sort by alpha : Search
*                         fields, Comparison fields, Sort By fields.
*                       - Default Sort by field is "DepositDate"
*VI 5175 JMC 3/19/2003  - Removed NULL Lockbox from dropdown.
*VI 5242 JMC 3/19/2003  - Modified DE Field drop downs to sort by MICR
*                         field alpha, and) { DE Field by alpha.
*VI 5272 JMC 3/20/2003  - Removed warning when clicking the 'COTS Only' checkbox
*VI 5244 EJG 3/21/2003  - Removed LCase() when building search field list. Placed
*                         LCase() around the strField comparison in DisplayParameterForm().
*VI 5330 JMC 3/28/2003  - Added 'Transaction Number' to sort list.
*VI 5328 JMC 3/28/2003  - Modified Verbage of Date Error warning boxes.
*CR 3812 JMC 4/17/2003  - CR 3812 - Modified search to sort RT and Serial columns as numerics.
*        JMC 5/13/2003  - Removed constants from page.  Added reference to constants.asp.
*CR 3999 JMC 5/13/2003  - Added "Mark Sense" control to criteria form. Added "Mark Sense" icon to results form.
*CR 3999 JMC 6/2/2003   - Removed code that placed "Correspondence Only" checkbox to the right when "Mark Sense"
*                         checkbox is visible. This was originally a M Korner request that had to be changed.
*CR 4179 EJG 7/10/2003  - Added column click sorting capablities to DisplaySearchResults().
*CR 4189 EJG 7/29/2003  - Added link in results to batch detail on the batchid field.
*                       - Corrected issue in NS 4.x with missing </td> in results detail.
*CR 4190 EJG 8/8/2003   - Added support for 5th/3rd enhancements.
*                       - Parameter Form allows user to select fields for display in results.
*                       - Changed COTS Only to allow user to define search criteria, only checks
*   	                  related fields are removed from the select lists.
*                       - Added dynamically selected fields to the search results with column click sort
*                         capabilities added in CR 4179.
*                       - In Search Results, added the ability to generate a print view or comma seperated
*                         download of all search results.
*                       - Added constant SELECTFIELDS to dynamically build the number of search criteria options.
*CR 4190 EJG 8/15/2003  - Modified RetrieveParmValue() to clear the Value field if(both the Field and Operator
*                         have not been selected for the appropriate search criteria definition.
*CR 5041 EJG 9/2/2003   - Added Check Amount Range and Serial Number to the search criteria. The data is
*                         added to the WHERE clause node if(specified.
*CR 5040 EJG 9/8/2003   - Added call to GetLockboxsearchTotals() to retrieve the check count and total
*                         for a given search. Also added section to the end of the search results to display the
*                         the search criteria as well as the aggregate totals for check count and amount.
*CR 5166 EJG 9/9/2003   - Modified GetDataEntryFields() to filter the standard Checks fields RT, Account, Serial, and Amount
*                         from the recordest returned from SearchSrv.GetLockboxDataEntryFields() which will return all fields
*                         for valid DESetupID's. This is to allow RemitterName, TransactionCode, etc. from the Checks table
*                         to be selected and search on.
*CR 5532 EJG 10/22/2003 - Modifed DisplaySearchResults() to bulid the displayed search criteria as a variable and store in
*						  a hidden form field to be passed to the print/download views.
*CR 5687 JMC 10/24/2003 - Added Mouseover text to Transaction and Batch fields on Search results.
*CR 5469 JMC 11/04/2003 - Added Batch range to replace single Batch lookup.
*CR 5543 EJG 12/22/2003 - Added support for new Error, Messages, and Warnings dictionary used in Emulation Mode.
*CR 7129 EJG 01/27/2004 - Modified text download to not open in a new window.
*CR 7274 JMC 03/01/2004 - Changed verbage on Pdf View and Print View buttons.
*CR 7323 JMC 03/04/2004 - Changed the order of the Pdf View and Print View buttons.
*CR 7017 JMC 03/17/2004 - Added "CheckCount" and "CheckTotal" as hidden form fields for use by
*                         LockboxSearch_Alternate.asp.
*CR 7074 03/22/2004 JMC - Now appending "page=-1" to Xml "View Check" request.
*CR 7728 JMC 06/01/2004
*   -Modified file to pass only "GlobalCheckID", and "Page"
*    to imagePdf.asp.
*CR 9345 JMC 09/07/2004 - Re-labeled "Serial Number" --> "Check Number"
*CR 9557 JMC 09/28/2004
*   -Replaced LockboxID with OLLOckboxId.
*CR10407 JMC 01/12/2005
*   -Added a condition when calling the Search function to account for records=0.
*   -Modified Select field Listbox population function to by default deselect all DE fields.
*CR 11337 JMC 01/18/2005
*   -Added SelectivePrintMode and SelectivePrintImageOption page options.
*   -Modified page display to display differently in Selective Print Mode.
*CR 10537 JMC 03/03/2005
*   -Modified page to call SystemSrv.UpdateActivityLockbox function.
*CR 12345 JMC 04/26/2005
*   -Added javascript to ensure that Deposit Date From is a required parameter.
*CR 12215 JMC 05/23/2005
*   -Added lockboxsearchUtil.asp include file.
*   -Added cLockbox.asp include file.
*   -Removed All common functions to new lockboxsearchUtil include file.  Page now include
*    Main(), Validate(), DisplayParameterForm(), and DisplaySearchResults().
* 05/20/2010 JMC CR 29405
*   -Now passing in jobType to SetXMLNodes to prevent pagination when requesting
*    print-views, pdf-views, and zip-files.
* 07/21/2011 JNE CR 31753
*   -Added PreDefSearchID, new actions, using request queries,
*    overloaded DisplayParameterForm, SetLockboxFields, SetLockboxDependent Fields
*    to replace SetFields
* 09/06/2011 JNE CR 46222
*   -Added new parameters when calling GetSavedQueries.
* 09/21/2011 JNE CR 46230
*   -SaveLockboSearchQuery, clear SavedQueryName if error when saving.
* 12/01/2011 JNE CR 48435
*   -Cleaned up Refine case statement and set DateFrom/DateTo to null in SaveLockboxSearchQuery()
* 08/08/2012 JNE CR 54169
*     - Added BatchIDFrom, BatchIDTo, BatchNumberFrom, BatchNumberTo
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
*   -Added BatchIDFrom, BatchIDTo, BatchNumberFrom, BatchNumberTo
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013
*   -Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 90076 DRP 06/18/2013
*   -Add PaymentSource and PaymentType to Online lockbox/account search forms.
* WI 117291 JMC 10/15/2013
*   -Added date values to Xml document after saving Lockbox Search query
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 151989 TWE 07/07/2014
*    Fix IPOnline to work with new Batch ID and RAAM
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 155810 TWE 07/30/2014
*    add check for permission to access this page
* WI 132957 BDH 09/17/2014
*   Workgroup Selector implementation (single workgroup select)
* WI 169423 CEJ 10/02/2014
*   Change lockboxsearchparms.xsl so the Work Group Selector can cross refernce the page data
* WI 185770 JSF 1/27/2015
* :R360 Advanced Search - error message with no text when saving queries after using refine search
******************************************************************************/
namespace WFS.RecHub.Online
{

    public partial class lockboxsearch : _LockboxSearchBase
    {

        private void Page_Load(object sender, EventArgs e)
        {

            int lngSearchResults;
	        string predefinedSearchID = string.Empty;
	        string pageAction = string.Empty;
            bool bClearFields = false;

            SetLockboxFields(bClearFields);
            SetLockboxDependentFields(bClearFields);

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_AdvanceSearch, R360Permissions.ActionType.View))
            {
                Response.Redirect("accessdenied.aspx");
            }

            pageAction = GetPageAction();

            if (isSearchAction(pageAction) && (Errors.Count == 0) && (Messages.Count == 0))
            {
                predefinedSearchID = GetPredefinedSearchID();

                switch (pageAction)
                {
                    case "clearquery":
                        ClearLockboxSearchQuery();
                        break;

                    case "selsavequery":
                        SelectLockboxSearchQuery(predefinedSearchID, pageAction);
                        break;

                    case "savequery":
                    case "editname":
                        ValidateForm();
                        SaveLockboxSearchQuery();
                        break;

		            case "deletequery":
                        DeleteLockboxSearchQuery(predefinedSearchID);
                        break;

                     default:
                       // for txtAction:
                       //  search, back, next, previous, last, first, changepagemode, searchresultsjobrequest
                       if (Request.QueryString["id"] == null || Request.QueryString["id"] == String.Empty)
                            ValidateForm();

                        if (Errors.Count == 0)
                        {
                    		lngSearchResults = DisplaySearchResults(pageAction);

                            // if errors occur in search results then display parameter form.
                            if (lngSearchResults == -1)
                            {
                                DisplayParameterForm(pageAction);
                      	    }
                	    }
                        else
                        {
                            DisplayParameterForm(pageAction);
                	    }
			            break;
		        }
	        }
            else
            {
                DefaultActionLockboxSearchQuery(pageAction);
 	        }
        }

        private string GetPredefinedSearchID()
        {
            if (!(string.IsNullOrEmpty(GetField("PreDefSearchID"))))
                return GetField("PreDefSearchID");
            else if (CleanRequest.QueryString["PreDefSearchID"] != null)
                return CleanRequest.QueryString["PreDefSearchID"].ToString();
            else
                return String.Empty;
        }

        private string GetPageAction()
        {
            if (CleanRequest.Form["txtAction"] != null)
                return CleanRequest.Form["txtAction"].ToLower();
            else if (CleanRequest.QueryString["Action"] != null)
                return CleanRequest.QueryString["Action"].ToLower();
            else
                return String.Empty;
        }

        private void ClearLockboxSearchQuery()
        {
            // Clear form and display...
            bool bClearFields = true;
            SetLockboxFields(bClearFields);
            SetLockboxDependentFields(bClearFields);
            DisplayParameterForm("ClearLockboxSearchQuery");

        }

        private void SelectLockboxSearchQuery(string sPreDefSearchID, string pageAction)
        {
            bool bClearFields = false;
            Guid gidSelectPreDefSearchID;
            XmlDocument objXML = null;

            try
            {
                if (sPreDefSearchID.Equals(string.Empty))
                {
                    AddMessage(EventType.Error, "Please select a Predefined Search Query from the list.");
                    DisplayParameterForm("SelectLockboxSearchQuery");
                }
                else
                {
                    if (sPreDefSearchID.Equals(LTAConstants.PREDEFSEARCH_ADHOC))
                    {
                        // Clear form and display...
                        bClearFields = true;
                        SetLockboxFields(bClearFields);
                        SetLockboxDependentFields(bClearFields);
                        DisplayParameterForm("SelectLockboxSearchQuery");
                    }
                    else
                    {
                        if (ipoLib.TryParse(sPreDefSearchID, out gidSelectPreDefSearchID))
                        {
                            RetrieveSavedQuery(gidSelectPreDefSearchID, out objXML);
                            DisplayParameterForm(objXML);
                        }
                        else
                        {
		                    AddMessage(EventType.Error, "Invalid Query Selection");
                            DisplayParameterForm("SelectLockboxSearchQuery");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                AddMessage(EventType.Error, "Unable to load selected query");
                ClearLockboxSearchQuery();
            }
        }

        private void SaveLockboxSearchQuery()
        {
            XmlDocument objXML = null;
            string sErrorString = string.Empty;
            Guid gidPreDefSearchID = Guid.Empty;
            DateTime dteTemp;

            if (Errors.Count == 0)
            {
                try
                {
                    objXML = GetRequestDoc();
                    AddPageToXmlDoc(objXML);
                    SetXMLNodes(objXML.DocumentElement, string.Empty, true, true);
                    objXML.DocumentElement.SelectSingleNode("DateFrom").InnerText = ""; //CR 48435
                    objXML.DocumentElement.SelectSingleNode("DateTo").InnerText = "";   //CR 48435

                    UserSavedQueryResponse respdoc = R360SearchServiceClient.SaveUserSavedQuery(objXML);
                    if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                    //if (SearchSrv.SaveUserSavedQuery(objXML, out gidPreDefSearchID, out sErrorString))
                    {
                        gidPreDefSearchID = respdoc.Data.PreDefSearchID;
                        sErrorString = respdoc.Data.Error;
                        if ((objXML.DocumentElement.SelectSingleNode("Action") != null) && (gidPreDefSearchID != Guid.Empty))
                        {
                            objXML.DocumentElement.SelectSingleNode("PreDefSearchID").InnerText = gidPreDefSearchID.ToString();
                        }
                        AddMessage(EventType.Information, "Query Saved");
                    }
                    else
                    {
                        sErrorString = respdoc.Data.Error;
                        AddMessage(EventType.Error, sErrorString);
                        if (objXML.DocumentElement.SelectSingleNode("SavedQueryName") != null)
                        {
                            objXML.DocumentElement.SelectSingleNode("SavedQueryName").InnerText = string.Empty;
                        }
                    }
                    if (objXML.DocumentElement.SelectSingleNode("Action") != null)
                    {
                        objXML.DocumentElement.SelectSingleNode("Action").InnerText = "Refine";
                    }

                    if (GetField("DepositDateFrom").Length > 0 && DateTime.TryParse(GetField("DepositDateFrom"), out dteTemp))
                    {
                        objXML.DocumentElement.SelectSingleNode("DateFrom").InnerText = dteTemp.ToShortDateString();
                    }
                    else
                    {
                        objXML.DocumentElement.SelectSingleNode("DateFrom").InnerText = DateTime.Now.ToShortDateString();
                    }

                    if (GetField("DepositDateTo").Length > 0 && DateTime.TryParse(GetField("DepositDateTo"), out dteTemp))
                    {
                        objXML.DocumentElement.SelectSingleNode("DateTo").InnerText = dteTemp.ToShortDateString();
                    }
                    else
                    {
                        objXML.DocumentElement.SelectSingleNode("DateTo").InnerText = DateTime.Now.ToShortDateString();
                    }

                    DisplayParameterForm(objXML);
                }
                catch (Exception ex)
                {
                    EventLog.logError(ex);
                    AddMessage(EventType.Error, "Unable to save query");
                    ClearLockboxSearchQuery();
                }
            }
            else
            {
                DisplayParameterForm("SaveLockboxSearchQuery");
            }
        }

        private void DeleteLockboxSearchQuery(string sPreDefSearchID)
        {
            Guid gidDeletePreDefSearchID;
            bool bClearFields = false;

            try
            {
                if (sPreDefSearchID.Equals("") || (sPreDefSearchID.Equals(Guid.Empty.ToString())) || (sPreDefSearchID.Equals(LTAConstants.PREDEFSEARCH_ADHOC)))
                {
                    AddMessage(EventType.Error, "Please select a Predefined Search Query from the list.");
                    DisplayParameterForm("DeleteLockboxSearchQuery");
                }
                else
                {
                    if (ipoLib.TryParse(sPreDefSearchID, out gidDeletePreDefSearchID))
                    {
                        BaseResponse respdoc = R360SearchServiceClient.DeleteUserSavedQuery(gidDeletePreDefSearchID);
                        if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                        {
                            bClearFields = true;
                            SetLockboxFields(bClearFields);
                            SetLockboxDependentFields(bClearFields);
                            AddMessage(EventType.Information, "Query Deleted");
                        }
                        else
                        {
                            AddMessage(EventType.Error, "Error deleting query");
                        }

                        DisplayParameterForm("DeleteLockboxSearchQuery");
                    }
                    else
                    {
                        AddMessage(EventType.Error, "Invalid Query Selection");
                        DisplayParameterForm("DeleteLockboxSearchQuery");
                    }
                }
            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                AddMessage(EventType.Error, "Unable to delete query");
                ClearLockboxSearchQuery();
            }
        }

        private void DefaultActionLockboxSearchQuery(string sTxtAction)
        {
            XmlDocument objXML = null;
            XmlDocument objNode = null;
            XmlNode objRecordNode = null;
            XmlNode objPermissionNode = null;
            Guid gidDefaultPreDefSearchID;
            string sPreDefSearchID = string.Empty;

            try
            {
                // Call default saved query
                XmlDocumentResponse respdoc = R360SearchServiceClient.GetSavedQueries(AppRaamUser.UserID, 1, false);
                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    objNode = respdoc.Data;
                }
                //objNode = SearchSrv.GetSavedQueries(AppRaamUser.UserID,1,false);
                if ((objNode != null) && (objNode.SelectSingleNode("/Page/Recordset") != null))
                {
                    objRecordNode = objNode.SelectSingleNode("/Page/Recordset/Record[@IsDefault='True']");
                  if (objRecordNode != null)
                  {
                      sPreDefSearchID = objRecordNode.Attributes["PredefSearchID"].Value;
                  }
                  objPermissionNode = objNode.SelectSingleNode("/Page/UserInfo/Permissions/Permission[@Script='maintainquery.aspx']");
                }
                switch (sTxtAction.ToLower())
                {
                    case "chkcotsonly":
                        DisplayParameterForm(sTxtAction);
                        break;
                    case "refine":
                        DisplayParameterForm(sTxtAction);
                        break;
                    default:
                        if ((objPermissionNode != null) && (sPreDefSearchID != string.Empty))
                        {
                            if (ipoLib.TryParse(sPreDefSearchID, out gidDefaultPreDefSearchID))
                            {
                                RetrieveSavedQuery(gidDefaultPreDefSearchID, out objXML);
                                DisplayParameterForm(objXML);
                            }
                            else
                            {
                                AddMessage(EventType.Error, "Default Query is invalid");
                                DisplayParameterForm(sTxtAction);
                            }
                        }
                        else
                        {
                            DisplayParameterForm(sTxtAction);
                        }
                        break;
                }

            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                AddMessage(EventType.Error, "Unable to load search page");
                ClearLockboxSearchQuery();
            }
        }

        private void ValidateForm()
        {
            cSearchParm objParm;
            string strField;
            DateTime dteTemp;
            int intTemp;

            decimal decTemp;
            decimal decAmountFrom;
            decimal decAmountTo;

            //Validate LockboxID
            if (GetField("WorkgroupSelection").Length <= 0 || GetField("WorkgroupSelection").ToLower() == "none")
            {
                AddMessage(EventType.Error, "Workgroup is required. Please select a Workgroup from the list.");
            }

            //Validate DateFrom
            if (GetField("DepositDateFrom").Length > 0)
            {
                if (!DateTime.TryParse(GetField("DepositDateFrom"), out dteTemp))
                {
                    AddMessage(EventType.Error, "Deposit Date From must be a valid date in mm/dd/yyyy format.");
                }
                else if (DateTime.Parse(GetField("DepositDateFrom")) < DateTime.Parse("01/01/1900"))
                {
                    AddMessage(EventType.Error, "Deposit Date From must be greater than '01/01/1900'");
                }
            }

            //Validate DateTo
            if (GetField("DepositDateTo").Length > 0)
            {
                if (!DateTime.TryParse(GetField("DepositDateTo"), out dteTemp))
                {
                    AddMessage(EventType.Error, "Deposit Date To must be a valid date in mm/dd/yyyy format.");
                }
                else if (DateTime.Parse(GetField("DepositDateTo")) < DateTime.Parse("01/01/1900"))
                {
                    AddMessage(EventType.Error, "Deposit Date To must be greater than '01/01/1900'");
                }
            }

            //Validate Date Range
            if (GetField("DepositDateFrom").Length > 0 && GetField("DepositDateTo").Length > 0)
            {
                if (DateTime.TryParse(GetField("DepositDateFrom"), out dteTemp) && DateTime.TryParse(GetField("DepositDateTo"), out dteTemp))
                {
                    if (DateTime.Parse(GetField("DepositDateFrom")) > DateTime.Parse(GetField("DepositDateTo")))
                    {
                        AddMessage(EventType.Error, "When specifying a Deposit Date range, " +
                            "the Date From cannot be greater than the Date To.");
                    }
                }
            }

            //Batch number must be numeric
            if (GetField("BatchIDFrom").Length > 0)
            {
                if (!int.TryParse(GetField("BatchIDFrom"), out intTemp))
                {
                    AddMessage(EventType.Error, "Batch ID is invalid: [" + GetField("BatchIDFrom") + "]");
                }
            }

            //Batch number must be numeric
            if (GetField("BatchIDTo").Length > 0)
            {
                if (!(int.TryParse(GetField("BatchIDTo"), out intTemp)))
                {
                    AddMessage(EventType.Error, "Batch ID is invalid: [" + GetField("BatchIDTo") + "]");
                }
            }

	        //Batch number range validation
            if (GetField("BatchIDFrom").Length > 0 && GetField("BatchIDTo").Length > 0)
            {
                if (int.TryParse(GetField("BatchIDFrom"), out intTemp) && int.TryParse(GetField("BatchIDTo"), out intTemp))
                {
                    if (double.Parse(GetField("BatchIDFrom")) > double.Parse(GetField("BatchIDTo")))
                    {
				        AddMessage(EventType.Error, "When specifiying a Batch ID range, the Batch ID From cannot be greater than the Batch ID To.");
			        }
		        }
	        }

            //Batch number must be numeric
            if (GetField("BatchNumberFrom").Length > 0)
            {
                if (!int.TryParse(GetField("BatchNumberFrom"), out intTemp))
                {
                    AddMessage(EventType.Error, "Batch Number is invalid: [" + GetField("BatchNumberFrom") + "]");
                }
            }

            //Batch number must be numeric
            if (GetField("BatchNumberTo").Length > 0)
            {
                if (!(int.TryParse(GetField("BatchNumberTo"), out intTemp)))
                {
                    AddMessage(EventType.Error, "Batch Number is invalid: [" + GetField("BatchNumberTo") + "]");
                }
            }

	        //Batch number range validation
            if (GetField("BatchNumberFrom").Length > 0 && GetField("BatchNumberTo").Length > 0)
            {
                if (int.TryParse(GetField("BatchNumberFrom"), out intTemp) && int.TryParse(GetField("BatchNumberTo"), out intTemp))
                {
                    if (double.Parse(GetField("BatchNumberFrom")) > double.Parse(GetField("BatchNumberTo")))
                    {
				        AddMessage(EventType.Error, "When specifiying a Batch Number range, the Batch Number From cannot be greater than the Batch Number To.");
			        }
		        }
	        }

	        //Check amount range validation
            if (GetField("AmountFrom").Length > 0)
            {
                if (!decimal.TryParse(GetField("AmountFrom"), out decTemp))
                {
			        AddMessage(EventType.Error, "Payment Amount From must be numeric.");
		        }
	        }

            if (GetField("AmountTo").Length > 0)
            {
                if (!decimal.TryParse(GetField("AmountTo"), out decTemp))
                {
			        AddMessage(EventType.Error, "Payment Amount To must be numeric.");
		        }
	        }

            if (GetField("AmountFrom").Length > 0 && GetField("AmountTo").Length > 0)
            {
                if (decimal.TryParse(GetField("AmountFrom"), out decTemp))
                {
		            decAmountFrom = decTemp;
                    if (decimal.TryParse(GetField("AmountTo"), out decTemp))
                    {
		                decAmountTo = decTemp;
                        if (decAmountFrom > decAmountTo)
                        {
				            AddMessage(EventType.Error, "When specifiying a Check Amount range, the Amount From cannot be greater than the Amount To.");
			            }
                    }
		        }
	        }
            for (int i = 1; i <= SELECTFIELDS; ++i)
            {

	            strField = RetrieveParmValue("ALL", (i).ToString());

                objParm = new cSearchParm();
                ParseParameterData(objParm, strField);

                ValidateNewParm(objParm);
            }


        }

        private void ValidateNewParm(cSearchParm vParm)
        {

            int intTemp;
            float floatTemp;
            decimal decimalTemp;
            DateTime dteTemp;

            // If we didn't select anything, nothing is wrong.
            if (string.IsNullOrWhiteSpace(vParm.FldName)
                && string.IsNullOrWhiteSpace(vParm.Operator)
                && string.IsNullOrWhiteSpace(vParm.Value))
            {
                return;
            }

            if (vParm == null)
            {
                 AddMessage(EventType.Error, "An error occurred when adding the parameter. " +
                    "Check the information and try again.");
                 return;
            }

            //Check if(field selected
            if (vParm == null)
            {
                AddMessage(EventType.Error, "You must select a Field to " +
                "add to the search criteria.");
            }

            //Check if(operator selected
            if (vParm.Operator == "")
            {
                AddMessage(EventType.Error, "You must select a Comparison to " +
                "compare against the Value in your search criteria.");
            }

            if (vParm.Operator.ToLower() == "like")
            {
                switch (vParm.FldDataTypeEnum)
                {
                    case DMPFieldTypes.FLOAT:
                        AddMessage(EventType.Error, "You cannot use the 'Like' Comparison with numeric or date " +
                            "fields. As an alternative" +
                            ", try specifying a range using 'Greater Than' and 'Less Than' Comparisons.");
                        break;
                    case DMPFieldTypes.CURRENCY:
                        AddMessage(EventType.Error, "You cannot use the 'Like' Comparison with numeric or date " +
                            "fields. As an alternative" +
                            ", try specifying a range using 'Greater Than' and 'Less Than' Comparisons.");
                        break;
                    case DMPFieldTypes.DATE:
                        AddMessage(EventType.Error, "You cannot use the 'Like' Comparison with numeric or date " +
                            "fields. As an alternative" +
                            ", try specifying a range using 'Greater Than' and 'Less Than' Comparisons.");
                        break;
                }
            }

            //Check if(value entered and validate the data based on the field type
            if (vParm.Value.Length <= 0)
            {
                AddMessage(EventType.Error, "You must specify a Value in your search criteria.");
            }
            else if (vParm.FldDataTypeEnum != DMPFieldTypes.UNKNOWN)
            {
                switch (vParm.FldDataTypeEnum)
                {
                    case DMPFieldTypes.FLOAT:
                        if (!float.TryParse(vParm.Value, out floatTemp))
                        {
                            AddMessage(EventType.Error, "The Value for " +
                            "the field you selected must be numeric.");
                        }
                        break;
                    case DMPFieldTypes.CURRENCY:
                        if (!decimal.TryParse(vParm.Value, out decimalTemp))
                        {
                            AddMessage(EventType.Error, "The Value for " +
                            "the field you selected must be numeric.");
                        }
                        break;
                    case DMPFieldTypes.DATE:      //Date
                        if (!DateTime.TryParse(vParm.Value, out dteTemp))
                        {
                            AddMessage(EventType.Error, "The Value for the field you selected must be a valid " +
                                "date in mm/dd/yy format.");
                        }
                        else if (DateTime.Parse(vParm.Value) < DateTime.Parse("01/01/1900"))
                        {
                            AddMessage(EventType.Error, "The date entered must be greater than '01/01/1900'");
                        }
                        break;
                }
            }
        }

        private void ParseParameterData(cSearchParm vParam, string vField)
        {
            var tokens = vField
                .Split('~');
            int tmp;

            if (tokens.Length > 0)
                vParam.Join = tokens[0];
            if (tokens.Length > 1)
                vParam.TableName = tokens[1];
            if (tokens.Length > 2)
                vParam.FldName = tokens[2];
            if (tokens.Length > 3 && int.TryParse(tokens[3], out tmp))
                vParam.FldDataTypeEnum = (DMPFieldTypes)int.Parse(tokens[3]);
            if (tokens.Length > 4)
                vParam.ReportTitle = tokens[4];
            if (tokens.Length > 5 && int.TryParse(tokens[5], out tmp))
                vParam.BatchSourceKey = tmp;
            if (tokens.Length > 6)
                vParam.Operator = tokens[6];
            if (tokens.Length > 7)
                vParam.Value = tokens[7];
        }


        //***************************************************************************
        //*    Function : RetrieveParmValue
        //* Description :
        //*
        //* Revision History
        //* Date        By  Change
        //* ---------- --- ---------------------------------------------------------
        //* 01.21.2003 JMC Created Function
        //* 08.15.2003 EJG Modified to clear the "Value" field when there is no Field
        //*                or Operator selected.
        //**************************************************************************
        private string RetrieveParmValue(string parmName, string index)
        {

            string strField;
            //string strParmValue;

            switch (parmName.ToUpper())
            {

                case "FIELD":
                    strField = GetField("Parm_Field" + index.ToString());
			        break;
                case "OPERATOR":
                    strField = GetField("Parm_Operator" + index.ToString());
			        break;
                case "VALUE":
                    if (GetField("Parm_Field" + index.ToString()).Length > 0 && GetField("Parm_Operator" + index.ToString()).Length > 0)
                    {
				        strField = GetField("Parm_Value" + index.ToString());
                    }
                    else
                    {
				        strField = "";
			        }
			        break;
                case "ALL":
                    strField = "~"
                        + GetField("Parm_Field" + index.ToString()) + "~"
                        + GetField("Parm_Operator" + index.ToString()) + "~"
                        + GetField("Parm_Value" + index.ToString());

			        break;
                default:
                    strField = string.Empty;
			        break;
            }

            return (strField);
        }

        private int DisplaySearchResults(string pageAction)
        {

            XmlDocument objXML;						//XML document reference for form fields
            XmlDocument xmlDoc;						//XML docuemnt of results
            XmlDocument docResults = null;
            XmlNode nodeResults = null;
            int lngRetVal = -1;
            
            //script timeout to 15 minutes
            Server.ScriptTimeout = 900;

            if (Request.QueryString["id"] != null && Request.QueryString["id"] == "bc" && GetPageAction() == "search")
            {
                objXML = new XmlDocument();
                objXML.XmlResolver = null;
                objXML.LoadXml(Session["LockBoxSearchVars"].ToString());
            }
            else
            {

                //Create a base XML document
                objXML = GetRequestDoc();

                AddPageToXmlDoc(objXML);

                //Add the form data as nodes to the XML document
                SetXMLNodes(objXML.DocumentElement, string.Empty, true, true);
                Session["LockBoxSearchVars"] = objXML.OuterXml;
            }

            try
            {
                //Submit the request
                EventLog.logEvent(objXML.InnerXml, MessageImportance.Debug);
                XmlElement userIdElement = objXML.CreateElement("UserId");
                userIdElement.InnerText = AppRaamUser.UserID.ToString();
                objXML.DocumentElement.AppendChild(userIdElement);
                XmlDocumentResponse respdoc = R360SearchServiceClient.LockBoxSearchASXML(objXML);
                docResults = respdoc.Data;

                xmlDoc = docResults;

                AppendWorkgroupData(xmlDoc, GetField("WorkgroupSelection"));
                nodeResults = xmlDoc.DocumentElement.SelectSingleNode("Recordset[@Name='Results']");

                if (respdoc.Status != R360Shared.StatusCode.SUCCESS)
                {
                    lngRetVal = 0;
                }
                else
                {
                    if (nodeResults != null && nodeResults.ChildNodes.Count > 0)
                        lngRetVal = nodeResults.ChildNodes.Count;
                    else
                        lngRetVal = 0;
                }

	            Response.ContentType = "text/html";

                if (lngRetVal > 0)
	                DisplayXMLDoc(xmlDoc, "lockboxsearchresults.aspx");
                else
                    DisplayXMLDoc(xmlDoc, "lockboxsearchparms.aspx");

            }
            catch (Exception ex)
            {
                EventLog.logError(ex);
                AddMessage(EventType.Error, "Unable to complete request at this time.  Please narrow search criteria or try again later.");
                lngRetVal = -1;
            }

            return (lngRetVal);
        }

        private void DisplayParameterForm(string pageAction)
        {

            XmlDocument objXML;						//XML document reference for form fields
            XmlDocument xmlDoc;						//XML docuemnt of results
            XmlNode nodeSearchParms;
            XmlDocument nodeResults = null;

            //script timeout to 15 minutes
            Server.ScriptTimeout = 900;

            //Create a base XML document
            objXML = GetRequestDoc();

            AddPageToXmlDoc(objXML);

            SetField("Action", "Refine");

            //Add the form data as nodes to the XML document
            SetXMLNodes(objXML.DocumentElement, string.Empty, true, true);

            objXML.DocumentElement.SelectSingleNode("PageAction").InnerText = pageAction;

            //Submit the request
            XmlDocumentResponse respdoc = R360SearchServiceClient.LockBoxSearchASXML(objXML);
            //if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            //{
                nodeResults = respdoc.Data;
            //}
            xmlDoc = new XmlDocument();
            xmlDoc = nodeResults;

            nodeSearchParms = ipoXmlLib.addElement(xmlDoc.DocumentElement, "SearchParms", "");

	        Response.ContentType = "text/html";

	        DisplayXMLDoc(xmlDoc, "lockboxsearchparms.aspx");
        }

        private void RetrieveSavedQuery(Guid PreDefSearchID, out XmlDocument objXML)
        {
            objXML = null;
            XmlDocumentResponse respdoc = R360SearchServiceClient.RetrieveSavedQuery(PreDefSearchID);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                objXML = respdoc.Data;
            }
               //objXML = SearchSrv.RetrieveSavedQuery(PreDefSearchID);
        }

        private void DisplayParameterForm(XmlDocument objXML)
        {
            XmlNode nodeSearchParms;
            XmlDocument nodeResults = null;

            Server.ScriptTimeout = 900; //script timeout to 15 minutes
            XmlDocumentResponse respdoc = R360SearchServiceClient.LockBoxSearchASXML(objXML);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeResults = respdoc.Data;
            }
            //nodeResults = SearchSrv.LockBoxSearchASXML(objXML); //Submit the request
            XmlDocument xmlDoc = nodeResults;
            AbortIfNoXml(xmlDoc, "Lockbox search");

            nodeSearchParms = ipoXmlLib.addElement(xmlDoc.DocumentElement, "SearchParms", "");

	        Response.ContentType = "text/html";
	        DisplayXMLDoc(xmlDoc, "lockboxsearchparms.aspx");
         }

    }
}
