using System;
using System.Web;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     05/03/2010
*
* Purpose:
*   Retrieves information detail for a transaction selected. Dynamically
*   writes query to display data entry fields that are batch specific.
*	Also displays links to view all images captured in the transaction.
*
* Modification History
* ejg 9/13/02
*    -Converted page to use seperation logic as outlined in VI 3573
* ejg 11/15/2002
*	 -Corrected issue with error message being displayed for no stubs
*	  instead of an informational icon/message. VI 4459
*	  Corrected issue with using a '-' instead of 'n/a' for fields with
*	  no data. VI 4412
*	  Changed the documents section to use 'View Image' instead of
*	  'View [document type]'. VI 4412
* ejg 11/21/2002
*    -Added the Document ID column to the Documents section. VI 4543
* ejg 11/22/2002
*    -Made change to use multiple windows for image viewing. VI 4544
* ejg 1/24/2003
*    -Made changes to page the documents and stubs sections. VI 4806
*    -Made chages to call the View All Images for trasnacion. VI 4551
*    -Made chagnes to display for Harris. VI 4752
* JMC VI 4939 02/05/2003
*    -Transaction Detail - now creating a line
*     item in the return recordset to display transaction
*     detais regardless of whether it has associated
*     documents.
* ejg 2/13/2003
*	 -Changed Account #/Serial # to Account Number/Serial Number. VI 5006
* ejg 2/14/2002
*	 -Added Lockbox.LongName to the ContentSubtitle. VI 4990
* ejg 3/18/2003
*	 -Updated NULL and 0 Length values to display '&nbsp;' instead of 'n/a'. VI 5173
* JMC 05.13.1003
*    -Added Error to page.  was "On error resume next".
* CR 3999 JMC 05.13.2003
*    -Added "Mark Sense" icon to display.
* CR 3621 EJG 06/17/2003
*    -Added variables to store BatchID and TransactionSequence to pass
*     in QueryString for ViewAllImages.asp. These variables are used
*     in the file name property.
* CR 5610 EJG 11/19/2003 - Converted page to XSLT for Remitter support
* CR 10537 JMC 03/03/2005
*    -Added call to AddPageToXmlDoc
* CR 28441 JMC 01/07/2010
*    -Added 'Jump-To Transaction' functionality
* CR 46058 JMC 09/22/2011
*    -Modified page to look for a 'printview' querystring paramerter
*    -Removed depracated code.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 151989 TWE 07/07/2014
*    Fix IPOnline to work with new Batch ID and RAAM
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************/
namespace WFS.RecHub.Online {

    public partial class transactiondisplay : _Page {

        private void Page_Load(object sender, EventArgs e) {
            // Grab the URL for the new transaction detail.
            var url = Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/../Framework/Framework?tab=2&redir=transactiondetail");

            // Redirect away. Note - we already stored the parameters into local javascript storage.
            Response.SetCookie(new HttpCookie("r360-redir-url", url));
            Response.Redirect(url, true);
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}
		#endregion
    }
}