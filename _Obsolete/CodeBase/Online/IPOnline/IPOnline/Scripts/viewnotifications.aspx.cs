using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date: 09/04/2012
*
* Purpose:  
*
* Modification History
* CR 54181 JMC 09/06/2012
*   -Initial Version
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 70371 TWE 02/07/2013 
*     Add TimeZoneBias for notification display
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 90134 WJS 03/04/2013 
*     FP:Add TimeZoneBias for notification display
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 118784 CEJ 10/25/2013
*   Set the From and To Dates to the current date when not containing valid date values on the View Notifications Page
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/
namespace WFS.RecHub.Online
{

    public partial class viewnotifications : _Page
    {

        private void Page_Load(object sender, EventArgs e)
        {
            XmlDocument nodeResults = null;
            XmlDocument xmlDoc;
            XmlDocument xmlInput;
            XmlNode node;

            int intNotificationFileTypeKey;
            string strOrderBy;
            string strOrderDir;
            int lngStartRecord;

            DateTime dteStartDate;
            DateTime dteEndDate;

            int intTemp;
            DateTime dteTemp;

            string fileName = "";
            string workgroupSelection = "";
            string selectedWorkgroups = "";
            List<string> selectedWorkgroupsList = new List<string>();
            List<WorkgroupParams> selectedWorkgroupsParamsList = new List<WorkgroupParams>();
            int bankID = -1;
            int clientAccountID = -1;
            string[] selectedWorkgroupsParms;

            string timeZoneLabel = this.AppSystem.TimeZoneLabel;
            if (this.AppSystem.AdjustDaylightSavings)
            {
                if (ipoDateTimeLib.GetDaylightSavingsFlag())
                {
                    timeZoneLabel = timeZoneLabel + "-dst";
                }
            }

            if (Request.Form.Count > 0)
            {
                // Get values from the Form submitted by the User
                if (Request.Form["txtStartDate"] == null)
                {
                    dteStartDate = DateTime.Now;
                }
                else
                {
                    if (DateTime.TryParse(Request.Form["txtStartDate"], out dteTemp))
                    {
                        dteStartDate = dteTemp;
                    }
                    else
                    {
                        dteStartDate = DateTime.Now;
                    }
                }

                if (Request.Form["txtEndDate"] == null)
                {
                    dteEndDate = DateTime.Now;
                }
                else
                {
                    if (DateTime.TryParse(Request.Form["txtEndDate"], out dteTemp))
                    {
                        dteEndDate = dteTemp;
                    }
                    else
                    {
                        dteEndDate = DateTime.Now;
                    }
                }

                if (Request.Form["cboNotificationFileType"] == null)
                {
                    intNotificationFileTypeKey = -1;
                }
                else
                {
                    if (int.TryParse(Request.Form["cboNotificationFileType"], out intTemp))
                    {
                        intNotificationFileTypeKey = intTemp;
                    }
                    else
                    {
                        intNotificationFileTypeKey = -1;
                    }
                }

                if (!string.IsNullOrEmpty(Request.Form["txtWorkgroupSelection"]))
                {
                    workgroupSelection = CleanRequest.Form["txtWorkgroupSelection"];            
                }

                if (!string.IsNullOrEmpty(Request.Form["txtSelectedWorkgroups"]))
                {
                    selectedWorkgroups = CleanRequest.Form["txtSelectedWorkgroups"];

                    if (selectedWorkgroups.Contains(','))
                    {
                        selectedWorkgroups = selectedWorkgroups.Remove(selectedWorkgroups.LastIndexOf(","), 1);
                    }

                    selectedWorkgroupsList = selectedWorkgroups.Split(',').Select(x => x).ToList();
                }

                if (!string.IsNullOrEmpty(Request.Form["txtFileName"]))
                {
                    fileName = Request.Form["txtFileName"];
                }

                if (Request.Form["txtAction"] != null && Request.Form["txtAction"].ToLower() == "apply")
                {
                    strOrderBy = string.Empty;
                    strOrderDir = "ASC";
                    lngStartRecord = 0;
                }
                else
                {

                    strOrderBy = CleanRequest.GetFormValue("txtOrderBy", "date");

                    if (CleanRequest.HasFormValue("txtOrderDir"))
                    {
                        if (CleanRequest.GetFormValue("txtOrderDir", "").Trim().ToLower().StartsWith("desc"))
                        {
                            strOrderDir = "DESC";
                        }
                        else
                        {
                            strOrderDir = "ASC";
                        }
                    }
                    else
                    {
                        strOrderDir = "ASC";
                    }

                    lngStartRecord = CleanRequest.GetFormValue("txtStart", 0);
                }

            }
            else if (Request.QueryString.Count > 0)
            {

                // Get values from the QueryString

                if (Request.QueryString["startdate"] == null)
                {
                    dteStartDate = DateTime.Now;
                    AddMessage(EventType.Error, "The Start Date is not valid.");
                }
                else
                {
                    if (DateTime.TryParse(Request.QueryString["startdate"], out dteTemp))
                    {
                        dteStartDate = dteTemp;
                    }
                    else
                    {
                        dteStartDate = DateTime.Now;
                        AddMessage(EventType.Error, "The Start Date is not valid.");
                    }
                }

                if (Request.QueryString["enddate"] == null)
                {
                    dteEndDate = DateTime.Now;
                    AddMessage(EventType.Error, "The End Date is not valid.");
                }
                else
                {
                    if (DateTime.TryParse(Request.QueryString["enddate"], out dteTemp))
                    {
                        dteEndDate = dteTemp;
                    }
                    else
                    {
                        dteEndDate = DateTime.Now;
                        AddMessage(EventType.Error, "The End Date is not valid.");
                    }
                }


                if (Request.QueryString["filetype"] == null)
                {
                    intNotificationFileTypeKey = -1;
                }
                else
                {
                    if (int.TryParse(Request.QueryString["filetype"], out intTemp))
                    {
                        intNotificationFileTypeKey = intTemp;
                    }
                    else
                    {
                        intNotificationFileTypeKey = -1;
                    }
                }

                if (!string.IsNullOrEmpty(Request.Form["txtFileName"]))
                {
                    fileName = Request.Form["txtFileName"];
                }

                if (Request.QueryString["orderby"] == null)
                {
                    strOrderBy = "date";
                }
                else
                {
                    strOrderBy = Request.QueryString["orderby"];
                }

                if (Request.QueryString["orderdir"] == null)
                {
                    strOrderDir = "ASC";
                }
                else
                {
                    if (Request.QueryString["orderdir"].Trim().ToLower().StartsWith("desc"))
                    {
                        strOrderDir = "DESC";
                    }
                    else
                    {
                        strOrderDir = "ASC";
                    }
                }

                if (Request.QueryString["start"] != null && int.TryParse(CleanRequest.QueryString["start"], out intTemp))
                {
                    lngStartRecord = intTemp;
                }
                else
                {
                    lngStartRecord = 0;
                }
            }
            else
            {
                dteStartDate = DateTime.Now.Date;
                dteEndDate = DateTime.Now.Date;
                intNotificationFileTypeKey = -1;
                strOrderBy = "date";
                strOrderDir = "ASC";
                lngStartRecord = 0;
            }

            //Validate date range
            if (dteStartDate > DateTime.MinValue && dteEndDate > DateTime.MinValue)
            {
                if (dteStartDate > dteEndDate)
                {
                    AddMessage(EventType.Error, "When specifying a date range, the Date From cannot be greater than the Date To.");
                    dteStartDate = DateTime.MinValue;
                    dteEndDate = DateTime.MinValue;
                }
            }


            // Create parameters xml document
            xmlInput = GetRequestDoc();

            if (dteStartDate > DateTime.MinValue)
            {
                node = ipoXmlLib.addElement(xmlInput.DocumentElement, "StartDate", dteStartDate.ToShortDateString());
            }

            if (dteEndDate > DateTime.MinValue)
            {
                node = ipoXmlLib.addElement(xmlInput.DocumentElement, "EndDate", dteEndDate.ToShortDateString());
            }

            node = ipoXmlLib.addElement(xmlInput.DocumentElement, "StartRecord", lngStartRecord.ToString());

            if (intNotificationFileTypeKey > 0)
            {
                node = ipoXmlLib.addElement(xmlInput.DocumentElement, "NotificationFileType", intNotificationFileTypeKey.ToString());
            }


            if (!string.IsNullOrEmpty(fileName))
            {
                node = ipoXmlLib.addElement(xmlInput.DocumentElement, "FileName", fileName);
            }

            node = ipoXmlLib.addElement(xmlInput.DocumentElement, "OrderBy", strOrderBy);
            node = ipoXmlLib.addElement(xmlInput.DocumentElement, "OrderDir", strOrderDir);

            if (selectedWorkgroupsList.Count > 0)
            {
                node = ipoXmlLib.addElement(xmlInput.DocumentElement, "ClientAccounts");

                foreach (string workgroup in selectedWorkgroupsList)
                {
                    WorkgroupParams workgroupParams = new WorkgroupParams();

                    selectedWorkgroupsParms = workgroup.Split('|');

                    if (int.TryParse(selectedWorkgroupsParms[0], out bankID) && int.TryParse(selectedWorkgroupsParms[1], out clientAccountID))
                    {
                        XmlNode tempNode = ipoXmlLib.addElement(node, "ClientAccount");

                        ipoXmlLib.addAttribute(tempNode, "BankID", bankID.ToString());
                        ipoXmlLib.addAttribute(tempNode, "ClientAccountID", clientAccountID.ToString());
                    }
                }
            }

            //Get the xml document
            try
            {
                XmlDocumentResponse respdoc = R360AlertServiceClient.GetAllOnlineNotifications(xmlInput);

                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    nodeResults = respdoc.Data;
                }
                    //nodeResults = AlertSrv.GetAllOnlineNotifications(xmlInput);
            }
            catch (Exception ex)
            {

                AddMessage(EventType.Error, ex.Message);

                EventLog.logError(ex);
                nodeResults = null;
            }

            if (nodeResults == null)
            {
                xmlDoc = ipoXmlLib.GetXMLDoc("Page");
            }
            else
            {
                xmlDoc = new XmlDocument();
                xmlDoc = nodeResults;
            }

            // Add form fields to xml document
            if (dteStartDate > DateTime.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtStartDate", dteStartDate.ToShortDateString(), "INPUT");
            }
            else
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtStartDate", DateTime.Now.ToShortDateString());
            }

            if (dteEndDate > DateTime.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtEndDate", dteEndDate.ToShortDateString(), "INPUT");
            }
            else
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtEndDate", DateTime.Now.ToShortDateString());
            }

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtWorkgroupSelection", workgroupSelection, "INPUT");

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtSelectedWorkgroups", selectedWorkgroups, "INPUT");

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtFileName", fileName, "INPUT");

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtNotificationFileType", intNotificationFileTypeKey.ToString(), "INPUT");

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtOrderBy", strOrderBy, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtOrderDir", strOrderDir, "INPUT");

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtStartRecord", lngStartRecord.ToString(), "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "TimeZoneLabel", timeZoneLabel);

            ////Add any errors to the xml document
            //AddErrorsToXMLDoc xmlDoc

            //Transform the xml document using the appropriate stylesheet
            DisplayXMLDoc(xmlDoc, GetScriptFile());
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            base.OnInit(e);
        }

        #endregion
    }
}
