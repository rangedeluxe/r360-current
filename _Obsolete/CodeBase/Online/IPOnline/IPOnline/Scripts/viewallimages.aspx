<%@ Page Language="C#" Inherits="WFS.RecHub.Online._JobRequestBase" %>
<script runat="server">
/*****************************************************************
' Filename:        viewallimages.aspx
'
' Author:           Eric Goetzinger
'
' Description:
'	Page will retrieve all images for a batch or transactions based
'	on the parameters passed via SOAP and convert them from individual
'	TIFFfiles to one PDF. Once the PDF is created, it is streamed to the 
'	client browser.
'
' Revisions:
'	8/5/02 - ejg
'	When displaying totals in remitdisplay.asp, there is a link to view all images
'	associated with the batch. if(no images exist for the batch,) { an error occurs.
'	The page no will generate a text only pdf informing the user that no images for 
'	the batch or transaction were found. VI 3809
'	CR 3621 6/4/2003 EJG
'	- Modified the page to submit a view all image request and poll the 
'	  database for the request status. Once done) { redirect to get the
'	  the requested file. if(error, redirect to the user-defined no image
'     page located at /styles/default/noimage.asp.
'	CR 3621 6/17/2003 EJG
'	- Modified call to getimagerequest.asp to redirect in the opening window and
'	  close the pop-up since all requests will be displayed outside of the browser.
'	CR 3621 6/24/2003 EJG
'	- Modified call to getimagerequest.asp to back to original by redirecting in the
'	  pop-up due to the use of the ViewAllLayout.asp.
'	CR 5521 10/20/2003 EJG
'		- Added error logging to the application for debugging.
'   CR 7074 03/22/2004 JMC
'       -Now appending "Page" to Xml "View All" request.
'   CR 7529 04/05/2004 JMC
'       -Modified logic to pause 3 seconds between refreshes for the first
'        9 iterations, and 15 seconds for each subsequent iteration.
'CR 7728 JMC 06/01/2004
'   -Rebuilt page to utilize ipoJobRequestUtil.asp include file to interact
'    with ipo Image Services.  All image requests are now routed through 
'    ipo Image Services.
'CR 15162 JMC 03/21/2006
'   -Added ImageFilterOption parameter to request Xml.
'CR 15161 JMC 03/01/2006
'   -Added include file "ViewAllLayout.aspx"
'   -Added include file "downloadUtil.aspx"
'   -Changed call to "ProcessNewRequest" to "ProcessRequest".
'CR 54169 JNE 08/08/2012
'   -Added BatchNumber 
'WI 119134 MLH 10/29/2013
'   - Moved Page_Load call to image service into try/catch.  Modified 'NoImage' redirect 
'     to a path that exists
'WI 121455 Fixed incorrectly merged code from WI 119372 (Merge branch 2.00_Dev into main)
'*****************************************************************/
private void Page_Load(object sender, System.EventArgs e) {

    System.Xml.XmlDocument objXML;
    System.Data.DataSet ds;

    if(CleanRequest.HasQueryStringValue("ID")) {
        CheckRequestStatus(OLFDeliveryMethod.DetermineBySize);
    } else {

        // Build Request Xml
        objXML = GetRequestDoc();

        if (CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_BANKID))
        {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "BankID", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_BANKID]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_LOCKBOXID)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "LockboxID", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_LOCKBOXID]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_BATCHID)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "BatchID", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_BATCHID]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_DEPOSITDATE)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "DepositDate", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_DEPOSITDATE]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_TXNID)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "TransactionID", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_TXNID]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_BATCHNUMBER)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "BatchNumber", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_BATCHNUMBER]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_TXNSEQUENCE)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "TxnSequence", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_TXNSEQUENCE]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_PAGE)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "Page", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_PAGE]);
        }

        if(CleanRequest.HasQueryStringValue(WFS.RecHub.Common.Constants.QS_PARM_IMAGEFILTEROPTION)) {
            WFS.RecHub.Common.ipoXmlLib.addElement(objXML.DocumentElement, "ImageFilterOption", CleanRequest.QueryString[WFS.RecHub.Common.Constants.QS_PARM_IMAGEFILTEROPTION]);
        }
		
        
	
        try {
            WFS.RecHub.R360Services.Common.BaseGenericResponse<System.Data.DataSet> respdoc = R360ImageServiceClient.CreateViewAllJobRequest(objXML);
            ds = respdoc.Data;  
            base.ProcessJobRequest(ds.Tables[0], OLFDeliveryMethod.DetermineBySize);
        } catch(Exception ex) {
            EventLog.logError(ex, this.GetType().ToString(), "Page_Load");
			
            Response.Redirect(base.NoImagePagePath);
            
        }
    }
}

</script>