using System;
using System.Collections.Generic;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Module:          Online Decisioning Batch List.
* 
* Filename:        oldbatches.aspx
* 
* Author:          Joel Caples
* 
* Description:     Displays a list of all Decisioning Batches meeting the User's
*                  input criteria.
* 
* Revisions:
* CR 12522 JMC 06/29/2006
*     -Created File
* CR 28738 JMC 03/23/2010
*   -Modified page to use the Redirect function.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
**********************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldbatches : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    string strOLLockboxID;
        //    int lngStartRecord;
        //    int intBatchMode;
        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    XmlNode nodeRespXml;
        //    int intTemp;

        //    int intGlobalBatchID = -1;
        //    int intBankID = -1;
        //    int intCustomerID = -1;
        //    int intLockboxID = -1;

        //    if(Request.Form["cboOLLockbox"] != null && Request.Form["cboOLLockbox"].Length > 0) {
        //        if(Request.Form["cboOLLockbox"] == "[All]") {
        //            strOLLockboxID = "";
        //        } else {
        //            strOLLockboxID = Request.Form["cboOLLockbox"];
        //        }
        //    } else if(Request.QueryString["OLLockboxID"] != null && Request.QueryString["OLLockboxID"].Length > 0) {
        //        strOLLockboxID = Request.QueryString["OLLockboxID"];
        //    } else {
        //        strOLLockboxID = "";
        //    }

        //    // Start Record
        //    if((Request.Form["txtStart"] != null) && (int.TryParse(Request.Form["txtStart"], out intTemp)) && (Request.Form["txtAction"] != null) && (Request.Form["txtAction"].Length > 0)) {
        //        lngStartRecord = intTemp;
        //    } else {
        //        lngStartRecord = 1;
        //    }
            
        //    if(Request.Form["selBatchMode"] != null && Request.Form["selBatchMode"].Length > 0 && int.TryParse(Request.Form["selBatchMode"], out intTemp)) {
        //        intBatchMode = intTemp;
        //    } else if((Request.QueryString["BatchMode"] != null) && (int.TryParse(Request.QueryString["BatchMode"], out intTemp))) {
        //        intBatchMode = intTemp;
        //    } else {
        //        intBatchMode = 2;
        //    }

        //    if(Request.Form["txtGlobalBatchID"] != null && int.TryParse(Request.Form["txtGlobalBatchID"], out intTemp)) {
        //        intGlobalBatchID = intTemp;
        //    }

        //    if(Request.Form["txtBankID"] != null && int.TryParse(Request.Form["txtBankID"], out intTemp)) {
        //        intBankID = intTemp;
        //    }

        //    if(Request.Form["txtCustomerID"] != null && int.TryParse(Request.Form["txtCustomerID"], out intTemp)) {
        //        intCustomerID = intTemp;
        //    }

        //    if(Request.Form["txtLockboxID"] != null && int.TryParse(Request.Form["txtLockboxID"], out intTemp)) {
        //        intLockboxID = intTemp;
        //    }

        //    if(Request.Form["txtAction"] == "checkout") {
        //        if(CheckOutBatch(intGlobalBatchID)) {
        //            //Response.Redirect("oldtransactiondetail.aspx?gbatch=" + Request.Form["txtGlobalBatchID"]);
        //            Dictionary<string ,string> QueryStringParms = new Dictionary<string,string>();
        //            QueryStringParms.Add(Constants.QS_PARM_BANKID, intBankID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_CUSTOMERID, intCustomerID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_LOCKBOXID, intLockboxID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_GLOBALBATCHID, intGlobalBatchID.ToString());
        //            Redirect("oldtransactiondetail.aspx", QueryStringParms);
        //        }
        //    }

        //    if(Request.Form["txtAction"] == "reset") {
        //        ResetBatch(intBankID
        //                 , intCustomerID
        //                 , intLockboxID
        //                 , intGlobalBatchID);
        //    }

        //    strReqXml = "<ReqGetDecisioningBatches>"
        //              + "    <Lockbox OLLockboxID=\"" + strOLLockboxID + "\" />"
        //              + "    <StartRecord>" + lngStartRecord.ToString() + "</StartRecord>"
        //              + "    <Mode>" + intBatchMode.ToString() + "</Mode>"
        //              + "</ReqGetDecisioningBatches>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetDecisioningBatches(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    // Add FormFields node to root of document.
        //    nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    ipoXmlLib.addElement(nodeFormFields, "OLLockboxID", strOLLockboxID);
        //    ipoXmlLib.addElement(nodeFormFields, "BatchMode", intBatchMode.ToString());

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion

    }
}
