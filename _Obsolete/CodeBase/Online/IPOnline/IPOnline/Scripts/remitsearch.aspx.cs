using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;


/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Goetzinger
* Date:     
*
* Purpose:  
*
* Modification History
* 9/13/02 - ejg
*    -Converted page to use seperation logic as outlined in VI 3573
* 10/28/2002 - ejg
*    -Updated field validation for dates and amounts with the Javascript
*     validation code. VI 4253
* 11/15/2002 - ejg
*    -Made change to use 'n/a' instead of '-' for empty/null data. VI 4412
* 11/18/2002 - ejg
*    -Made change to always display a select box for lockboxes. VI 4450
* 11/22/2002 - ejg
*    -Made change to use multiple windows for image viewing. VI 4544
* 12/11/02 JCS - VI 4247, Modified to support receiving of only requested
*    -page of records in recordset.
* 2/3/2003 - ejg
*    -Made change to date validation. VI 4171/4254
* 3/18/2003 - ejg
*    -Updated NULL and 0 Length values to display '&nbsp;' instead of 'n/a'. VI 5173
* CR 7074 03/22/2004 JMC
*    -Now appending "page=-1" to Xml "View Check" request.
* CR 7728 JMC 06/01/2004
*    -Modified file to pass only "GlobalCheckID", and "Page"
*     to imagePdf.asp.
* CR 7292 JMC 10/21/2004 - Modified page to utilize Xslt.
* CR 10682 JMC 11/18/2004
*    -Added an error handler around the Xml request. �When an error occurrs 
*     (connection or otherwise) in attempting the request, a user-friendly error 
*     message is now being displayed, and the actual Error message is logged to the 
*     logfile
* CR 10537 JMC 03/03/2005
*    -Added call to AddPageToXmlDoc
* CR 12492 EJG 05/09/2005
*    -Added current date as default to from and to date fields.
* CR 15165 EJG 03/06/2006
*    -Modify GetXmlParmsDoc() to determine if(action was search or refine.
* CR 28866 JMC 07/26/2011
*    -Modified page to include BankID/LockboxID in output Xml.
* WI 70440 CEJ 11/30/2012 
*  - No records result after the next button clicked when searching slightly less 365 days.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 93314 DRP 06/18/2013
*    - Add PaymentSource and PaymentType to Online remittance search forms.
* WI 116216 CEJ 10/14/2013
*   Add data validation check to ensure the from amount range value is less than the to OLServicesAPI.cSearchSrv
* WI 118783 CEJ 10/25/2013
*   Set the To Date to the current date if left blank on the Remittance Search PagetxtWorkgroupSelection
* WI 133287 TWE 03/31/2014
*   Add DDA to Remittance Search
* WI 133288 TWE 03/31/2014
*   Add RT and Account to Remittance Search
* WI 133573 CMC 04/04/2014
*   Handle null form data - Bug 132376
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 155808 TWE 07/30/2014
*    add check for permission to access this page
* WI 159616 TWE 08/19/2014
*    add ClientAccountID to remittance search if only 1 selected
* WI 132957 BDH 09/30/2014    
*    implement workgroup selector on advanced search page    
* WI 186733 JSF 01/27/2015
* Payment Search-Refine Search from 'Transaction Detail' errors
*    
'******************************************************************/
namespace WFS.RecHub.Online
{
    public partial class remitsearch : _Page
    {
        private string workgroupSelection = "";
        private string _RemitterName;
        private DateTime _DateFrom = DateTime.MinValue;
        private DateTime _DateTo = DateTime.MinValue;
        private Decimal _AmountFrom = Decimal.MinValue;
        private Decimal _AmountTo = Decimal.MinValue;
        private string _DDA;
        private string _RT;
        private string _Account;
        private string _CheckSerial;
        private string _SortBy;
        private int _StartRecord;
        private string _PaymentSource;
        private string _PaymentType;

        private string MAX_WORKGROUPS_ALLOWED_KEY = "MaximumWorkgroups";

        private void Page_Load(object sender, EventArgs e)
        {

            XmlDocument xmlDoc = new XmlDocument(); ;
            XmlDocument xmlParms;
            XmlDocument node;

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_PaymentSearch, R360Permissions.ActionType.View))
            {
                Response.Redirect("accessdenied.aspx");
            }

            try
            {
                EventLog.logEvent("peforming remitSearch.aspx.cs Validation" , MessageImportance.Debug);
                if (Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0)
                ValidateForm(xmlDoc);

                xmlParms = GetXmlParmsDoc();
                if (Errors.Count == 0)
                {
                  EventLog.logEvent("query:" + xmlParms.InnerXml, MessageImportance.Debug);
                  XmlDocumentResponse respdoc = R360SearchServiceClient.RemittanceSearch(xmlParms);
                  if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                    {
                      node = respdoc.Data;
                      xmlDoc = new XmlDocument();
                      xmlDoc = node;
                    }
                }
                else
                {
                  xmlDoc = DisplayParameterForm();
                }
                
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, this.GetType().Name, "RemittanceSearch");
                XmlDocumentResponse respdoc = R360SearchServiceClient.RemittanceSearch(GetRequestDoc());
                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    node = respdoc.Data;
                    xmlDoc = new XmlDocument();
                    xmlDoc = node;
                    ipoXmlLib.AddMessageToDoc(xmlDoc, xmlMessageType.xmlMsgError, "An error occurred attempting to execute the search.  Please verify the search criteria and try again.");
                }
            }

            // Add form fields to xml document
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtRemitterName", _RemitterName, "INPUT");
            if (_DateFrom > DateTime.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtDateFrom", _DateFrom.ToString("M/d/yyyy"), "INPUT");
            }

            if (_DateTo > DateTime.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtDateTo", _DateTo.ToString("M/d/yyyy"), "INPUT");
            }

            if (_AmountFrom > Decimal.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtAmountFrom", _AmountFrom.ToString(), "INPUT");
            }

            if (_AmountTo > Decimal.MinValue)
            {
                ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtAmountTo", _AmountTo.ToString(), "INPUT");
            }

            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtWorkgroupSelection", workgroupSelection, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtDDA", _DDA, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtRT", _RT, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtAccount", _Account, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtCheckSerial", _CheckSerial, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "cboSortBy", _SortBy, "SELECT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtStartRecord", _StartRecord.ToString(), "SELECT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtPaymentSourceID", _PaymentSource, "SELECT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtPaymentTypeID", _PaymentType, "SELECT");
            //persists the workgroup count for any time the workgroup selector tree is not in the page
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmRemittanceSearch", "txtSelectedWorkgroupCount", CleanRequest.Form["txtSelectedWorkgroupCount"], "HIDDEN");

            //AddErrorsToXMLDoc xmlDoc

            DisplayXMLDoc(xmlDoc, GetScriptFile());
        }
        private XmlDocument GetXmlParmsDoc()
        {
            XmlDocument xmlParms;
            XmlNode node;
            XmlNode tempNode;
            Decimal decTemp;
            DateTime dteTemp;
            int intTemp;
            int bankID;
            int clientAccountID;
            string[] workgroupSelectionParms;
            List<string> workgroupList;

            xmlParms = GetRequestDoc();

            AddPageToXmlDoc(xmlParms);

            // Retrieve input parameters
            if (Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0)
            {
                #region Form Values
                node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Action", "");

                switch (Request.Form["txtAction"].ToLower())
                {
                    case "refine":
                        node.InnerText = "refine";
                        break;
                    default:
                        node.InnerText = "search";
                        break;
                }

                RemitSearchVars searchVars = new RemitSearchVars();

                _StartRecord = CleanRequest.GetFormValue("txtStart", 1);
                searchVars.StartRecord = _StartRecord;

                if (!string.IsNullOrEmpty(Request.Form["txtWorkgroupSelection"]))
                {
                    workgroupSelection = CleanRequest.Form["txtWorkgroupSelection"];

                    if (workgroupSelection.Contains(','))
                    {
                        workgroupSelection = workgroupSelection.Remove(workgroupSelection.LastIndexOf(","), 1);
                    }

                    workgroupList = workgroupSelection.Split(',').Select(x => x).ToList();

                    if (workgroupList.Count > 0)
                    {
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ClientAccounts");
                    }

                    if (!workgroupSelection.Contains('|'))
                    {
                        ipoXmlLib.addElement(node, "EntityID", workgroupSelection.ToString());
                        searchVars.EntityId = workgroupSelection.ToString();
                    }
                    else
                    {
                        searchVars.EntityId = workgroupSelection.ToString();
                        // Grab the Single Workgroup
                        foreach (string workgroup in workgroupList)
                        {
                            WorkgroupParams workgroupParams = new WorkgroupParams();

                            workgroupSelectionParms = workgroup.Split('|');

                            if (int.TryParse(workgroupSelectionParms[0], out bankID) && int.TryParse(workgroupSelectionParms[1], out clientAccountID))
                            {
                                tempNode = ipoXmlLib.addElement(node, "ClientAccount");

                                ipoXmlLib.addAttribute(tempNode, "BankID", bankID.ToString());
                                ipoXmlLib.addAttribute(tempNode, "ClientAccountID", clientAccountID.ToString());

                                workgroupParams.BankID = bankID;
                                workgroupParams.ClientAccountID = clientAccountID;
                            }

                            if (workgroupParams.BankID != 0 && workgroupParams.ClientAccountID != 0)
                            {
                                searchVars.WorkgroupParamsList.Add(workgroupParams);
                            }
                        }
                    }
                }

                if (Request.Form["txtRemitterName"].Length > 0)
                {
                    _RemitterName = CleanRequest.Form["txtRemitterName"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "RemitterName", _RemitterName);
                    searchVars.RemitterName = _RemitterName;
                }

                if (CleanRequest.Form["txtDateFrom"] != null && CleanRequest.Form["txtDateFrom"].Length > 0 && DateTime.TryParse(CleanRequest.Form["txtDateFrom"], out dteTemp))
                {
                    _DateFrom = dteTemp;
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateFrom", _DateFrom.ToShortDateString());
                    searchVars.DepositDateFrom = _DateFrom;
                }

                if (CleanRequest.Form["txtDateTo"] != null && CleanRequest.Form["txtDateTo"].Length > 0 && DateTime.TryParse(CleanRequest.Form["txtDateTo"], out dteTemp))
                {
                    _DateTo = dteTemp;
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateTo", _DateTo.ToShortDateString());
                    searchVars.DepositDateTo = _DateTo;
                }
                else
                {
                    _DateTo = DateTime.Now;
                    searchVars.DepositDateTo = DateTime.Now;
                }

                if (CleanRequest.Form["txtAmountFrom"] != null && CleanRequest.Form["txtAmountFrom"].Length > 0)
                {
                    if (Decimal.TryParse(CleanRequest.Form["txtAmountFrom"].Replace("$", ""), out decTemp))
                    {
                        _AmountFrom = decTemp;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountFrom", _AmountFrom.ToString());
                    }
                    else
                    {
                        
                        _AmountFrom = decTemp;
                        Decimal.TryParse(CleanRequest.Form["txtAmountFrom"], out decTemp);
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountFrom", _AmountFrom.ToString());
                    }

                    searchVars.AmountFrom = _AmountFrom;
                }
                else
                {
                    searchVars.AmountFrom = Decimal.MinValue;
                }

                if (CleanRequest.Form["txtAmountTo"] != null && CleanRequest.Form["txtAmountTo"].Length > 0)
                {
                    if (Decimal.TryParse(CleanRequest.Form["txtAmountTo"].Replace("$", ""), out decTemp))
                    {
                        _AmountTo = decTemp;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountTo", _AmountTo.ToString());
                    }
                    else
                    {
                        Decimal.TryParse(CleanRequest.Form["txtAmountTo"], out decTemp);
                        _AmountTo = decTemp;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountTo", _AmountTo.ToString());
                    }

                    searchVars.AmountTo = _AmountTo;
                }
                else
                {
                    searchVars.AmountTo = Decimal.MinValue;
                }

                if (CleanRequest.Form["txtDDA"].Length > 0)
                {
                    _DDA = CleanRequest.Form["txtDDA"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DDA", _DDA);
                    searchVars.DDA = _DDA;
                }

                if (CleanRequest.Form["txtRT"].Length > 0)
                {
                    _RT = CleanRequest.Form["txtRT"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "RT", _RT);
                    searchVars.RoutingNumber = _RT;
                }

                if (CleanRequest.Form["txtAccount"].Length > 0)
                {
                    _Account = CleanRequest.Form["txtAccount"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Account", _Account);
                    searchVars.AccountNumber = _Account;
                }

                if (CleanRequest.Form["txtCheckSerial"].Length > 0)
                {
                    _CheckSerial = CleanRequest.Form["txtCheckSerial"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Serial", _CheckSerial);
                    searchVars.CheckSerialNumber = _CheckSerial;
                }

                if (FormDataIsValid("cboPaymentSource"))
                {
                    _PaymentSource = CleanRequest.Form["cboPaymentSource"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchSourceKey", _PaymentSource);
                    if (int.TryParse(_PaymentSource, out intTemp))
                        searchVars.PaymentSource = intTemp;
                }
                else
                {
                    searchVars.PaymentSource = int.MinValue;
                }

                if (FormDataIsValid("cboPaymentType"))
                {
                    _PaymentType = CleanRequest.Form["cboPaymentType"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchPaymentTypeKey", _PaymentType);
                    if (int.TryParse(_PaymentType, out intTemp))
                        searchVars.PaymentType = intTemp;
                }
                else
                {
                    searchVars.PaymentType = int.MinValue;
                }

                if (FormDataIsValid("cboSortBy"))
                {
                    _SortBy = CleanRequest.Form["cboSortBy"];
                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "SortBy", _SortBy);
                    searchVars.SortBy = _SortBy;
                }

                Session["RemitSearchVars"] = searchVars;

                #endregion
            }
            else if (CleanRequest.QueryString.Count > 0)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["id"] == "bc")
                {
                    RemitSearchVars searchVars = (RemitSearchVars)Session["RemitSearchVars"];

                    node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Action", "search");

                    _StartRecord = searchVars.StartRecord;

                    if (!String.IsNullOrWhiteSpace(searchVars.RemitterName))
                    {
                        _RemitterName = searchVars.RemitterName;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "RemitterName", _RemitterName);
                    }

                    if (!searchVars.IsAllLockboxes && searchVars.WorkgroupParamsList.Count > 0)
                    {
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ClientAccounts");

                        foreach (var workgroupParams in searchVars.WorkgroupParamsList)
                    {
                            tempNode = ipoXmlLib.addElement(node, "ClientAccount");
                            workgroupSelection = searchVars.EntityId;
                            ipoXmlLib.addAttribute(tempNode, "BankID", workgroupParams.BankID.ToString());
                            ipoXmlLib.addAttribute(tempNode, "ClientAccountID", workgroupParams.ClientAccountID.ToString());
                        }
                    }

                    // Add the entity element into the post back. The | indicates BankId|Workgroup do not add
                    if (!string.IsNullOrWhiteSpace(searchVars.EntityId) && !searchVars.EntityId.Contains("|"))
                    {
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ClientAccounts");
                        ipoXmlLib.addElement(node, "EntityID", searchVars.EntityId);
                        workgroupSelection = searchVars.EntityId;
                    }

                    if (searchVars.DepositDateFrom != DateTime.MinValue)
                    {
                        _DateFrom = searchVars.DepositDateFrom;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateFrom", _DateFrom.ToShortDateString());
                    }
                    else
                    {
                        _DateFrom = DateTime.Now;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateFrom", _DateFrom.ToShortDateString());
                    }

                    if (searchVars.DepositDateTo != DateTime.MinValue)
                    {
                        _DateTo = searchVars.DepositDateTo;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateTo", _DateTo.ToShortDateString());
                    }
                    else
                    {
                        _DateTo = DateTime.Now;
                    }

                    if (searchVars.AmountFrom != Decimal.MinValue)
                    {
                        _AmountFrom = searchVars.AmountFrom;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountFrom", _AmountFrom.ToString());
                    }


                    if (searchVars.AmountTo != Decimal.MinValue)
                    {
                        _AmountTo = searchVars.AmountTo;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "AmountTo", _AmountTo.ToString());
                    }

                    if (!String.IsNullOrWhiteSpace(searchVars.DDA))
                    {
                        _DDA = searchVars.DDA;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DDA", _DDA);
                    }

                    if (!String.IsNullOrWhiteSpace(searchVars.RoutingNumber))
                    {
                        _RT = searchVars.RoutingNumber;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "RT", _RT);
                    }

                    if (!string.IsNullOrWhiteSpace(searchVars.AccountNumber))
                    {
                        _Account = searchVars.AccountNumber;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Account", _Account);
                    }

                    if (!String.IsNullOrWhiteSpace(searchVars.CheckSerialNumber))
                    {
                        _CheckSerial = searchVars.CheckSerialNumber;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "Serial", _CheckSerial);
                    }

                    if (searchVars.PaymentSource != int.MinValue)
                    {
                        _PaymentSource = searchVars.PaymentSource.ToString();
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchSourceKey", _PaymentSource);
                    }

                    if (searchVars.PaymentType != int.MinValue)
                    {
                        _PaymentType = searchVars.PaymentType.ToString();
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchPaymentTypeKey", _PaymentType);
                    }

                    if (!String.IsNullOrWhiteSpace(searchVars.SortBy))
                    {
                        _SortBy = searchVars.SortBy;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "SortBy", _SortBy);
                    }
                }
                else
                {
                    #region Querystring Values

                    if (CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE] != null && CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE].Length > 0 && DateTime.TryParse(CleanRequest.QueryString["date"], out dteTemp))
                    {
                        _DateFrom = dteTemp;
                        _DateTo = dteTemp;
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateFrom", _DateFrom.ToShortDateString());
                        node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDateTo", _DateTo.ToShortDateString());
                    }
                    else
                    {
                        _DateFrom = DateTime.Today;
                        _DateTo = DateTime.Today;
                    }

                    #endregion
                }
            } // End of Querystring
            else
            {
                _DateFrom = DateTime.Today;
                _DateTo = DateTime.Today;
            }

            //Get the start record for paging
            if (_StartRecord <= 0)
            {
                _StartRecord = 1;
            }

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "StartRecord", _StartRecord.ToString());

            return (xmlParms);
        }
        private bool FormDataIsValid(string key)
        {
            if (CleanRequest.Form[key] == null)
                return false;

            if (CleanRequest.Form[key].Length == 0)
                return false;

            return true;

        }

        private void ValidateForm(XmlDocument xmlDoc)
        {

            DateTime dteTemp;
            decimal decTemp;
            decimal decAmountFrom;
            decimal decAmountTo;
            decimal numberOfWorkGroups;
            //// Validate Max Workgroups Allowed
            var maxAllowed = NumberOfWorkgroupsAllowed();
            bool isValidCount = decimal.TryParse(CleanRequest.Form["txtSelectedWorkgroupCount"].ToString(), out numberOfWorkGroups);
            if (isValidCount && numberOfWorkGroups > maxAllowed)
            {
                AddMessage(EventType.Error, String.Format("Number of Workgroups selected exceeds the configured system maximum allowed. Maximum allowed is {0}", maxAllowed));
            }

            ////Validate LockboxID
            if (CleanRequest.Form["txtWorkgroupSelection"].Length <= 0 || CleanRequest.Form["txtWorkgroupSelection"].ToLower() == "none")
             {
                AddMessage(EventType.Error, "Entity/Workgroup is required. Please select an Entity/Workgroup from the list");
             }

            //Validate DateFrom
            if (CleanRequest.Form["txtDateFrom"].Length > 0)
            {
                if (!DateTime.TryParse(CleanRequest.Form["txtDateFrom"], out dteTemp))
                {
                    AddMessage(EventType.Error, "Deposit Date From must be a valid date in mm/dd/yyyy format.");
                }
                else if (DateTime.Parse(CleanRequest.Form["txtDateFrom"]) < DateTime.Parse("01/01/1900"))
                {
                    AddMessage(EventType.Error, "Deposit Date From must be greater than '01/01/1900'");
                }
            }

            //Validate DateTo
            if (CleanRequest.Form["txtDateTo"].Length > 0)
            {
                if (!DateTime.TryParse(CleanRequest.Form["txtDateTo"], out dteTemp))
                {
                    AddMessage(EventType.Error, "Deposit Date To must be a valid date in mm/dd/yyyy format.");
                }
                else if (DateTime.Parse(CleanRequest.Form["txtDateTo"]) < DateTime.Parse("01/01/1900"))
                {
                    AddMessage(EventType.Error, "Deposit Date To must be greater than '01/01/1900'");
                }
            }

            //Validate Date Range
            if (CleanRequest.Form["txtDateFrom"].Length > 0 && CleanRequest.Form["txtDateTo"].Length > 0)
            {
                if (DateTime.TryParse(CleanRequest.Form["txtDateFrom"], out dteTemp) && DateTime.TryParse(CleanRequest.Form["txtDateTo"], out dteTemp))
                {
                    if (DateTime.Parse(CleanRequest.Form["txtDateFrom"]) > DateTime.Parse(CleanRequest.Form["txtDateTo"]))
                    {
                        AddMessage(EventType.Error, "When specifying a Deposit Date range, " +
                            "the Date From cannot be greater than the Date To.");
                    }
                }
            }


            //Check amount range validation    

            if (CleanRequest.Form["txtAmountFrom"].Length > 0)
            {
                if (!decimal.TryParse(CleanRequest.Form["txtAmountFrom"].ToString(), out decTemp))
                {
                    AddMessage(EventType.Error, "Payment Amount From must be numeric.");
                }
                else if (decTemp < 0) // Do not except negative Values. 
                {
                    AddMessage(EventType.Error, "Check Amount From must not be negative");
                }

            }

            if (CleanRequest.Form["txtAmountTo"].ToString().Length > 0)
            {
                if (!decimal.TryParse(CleanRequest.Form["txtAmountTo"].ToString(), out decTemp))
                {
                    AddMessage(EventType.Error, "Payment Amount To must be numeric.");
                }
                else if (decTemp <  0) // Do not except negative Values. 
                {
                    AddMessage(EventType.Error, "Check Amount To must not be negative");
                }
            }


            if (CleanRequest.Form["txtAmountFrom"].ToString().Length > 0 && CleanRequest.Form["txtAmountTo"].ToString().Length > 0)
            {
                if (decimal.TryParse(CleanRequest.Form["txtAmountFrom"].ToString(), out decTemp))
                {
                    decAmountFrom = decTemp;
                    if (decimal.TryParse(CleanRequest.Form["txtAmountTo"].ToString(), out decTemp))
                    {
                        decAmountTo = decTemp;
                        if (decAmountFrom > decAmountTo)
                        {
                           AddMessage(EventType.Error, "When specifiying a Check Amount range, the Amount From cannot be greater than the Amount To.");
                        }
                    }
                }
            }
        }

        private int NumberOfWorkgroupsAllowed()
        {
            var preferences = HubConfigServicesClient.GetPreferences();
            var max = 0;
            Int32.TryParse(preferences.Data.FirstOrDefault(p => p.Name == MAX_WORKGROUPS_ALLOWED_KEY).DefaultSetting, out max);
            return max;
        }

        private XmlDocument DisplayParameterForm()
        {

            XmlDocument objXML;						
            XmlDocument xmlDoc = new XmlDocument(); 
            XmlDocument nodeResults = null;
            //Create a base XML document
            objXML = GetRequestDoc();

            AddPageToXmlDoc(objXML);

            SetField("Action", "Refine");

            XmlDocumentResponse respdoc = R360SearchServiceClient.RemittanceSearch(GetRequestDoc());
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeResults = respdoc.Data;
                xmlDoc = nodeResults;
            }
            return (xmlDoc);
        }
    }
}
