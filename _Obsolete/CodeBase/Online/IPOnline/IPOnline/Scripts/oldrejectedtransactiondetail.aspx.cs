using System;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Module:          Online Decisioning Rejected Transaction Detail.
* 
* Filename:        oldrejectedtransactiondetail.aspx
* 
* Author:          Joel Caples
* 
* Description:     Displays a list of all Checks and Documents belonging to
*                  a rejected Transaction.
* 
* Revisions:
* CR 12522 JMC 06/29/2006
*     -Created File
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
'**********************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldrejectedtransactiondetail : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    string strRejectBatchID;
        //    string strTransactionID;

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    XmlNode nodeRespXml;
            
        //    if(Request.QueryString["rejectbatchid"] != null && Request.QueryString["rejectbatchid"].Length > 0) {
        //        strRejectBatchID = Request.QueryString["rejectbatchid"];
        //    } else {
        //        strRejectBatchID = "";
        //    }

        //    if(Request.QueryString["transactionid"] != null && Request.QueryString["transactionid"].Length > 0) {
        //        strTransactionID = Request.QueryString["transactionid"];
        //    } else {
        //        strTransactionID = "";
        //    }

        //    strReqXml = "<ReqRejectedTransactionDetail>"
        //              + "  <Batch RejectedBatchID=\"" + strRejectBatchID + "\">"
        //              + "    <Transaction TransactionID=\"" + strTransactionID + "\" />"
        //              + "  </Batch>"
        //              + "</ReqRejectedTransactionDetail>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetRejectedTransactionDetails(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    // Add FormFields node to root of document.
        //    nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    ipoXmlLib.addElement(nodeFormFields, "RejectedBatchID", strRejectBatchID);
        //    ipoXmlLib.addElement(nodeFormFields, "TransactionID", strTransactionID);

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        /*private void AddMessagesToPage(XmlNode nodeRespXml) {

            foreach(XmlNode nodeMsg in nodeRespXml.SelectNodes("//Messages/Message")) {
                switch(nodeMsg.Attributes.GetNamedItem("Type").InnerText) {   // .Attributes("Type").Value.ToLower())
                    case "warning":
                        AddMessage(EventType.Warning, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                    case "error":
                        AddMessage(EventType.Error, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                    default:    // "information"
                        AddMessage(EventType.Information, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                }
            }
        }*/

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}

		#endregion
    }
}
