using System;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date: 09/04/2012
*
* Purpose:  
*
* Modification History
* CR 54181 JMC 09/04/2012
*   -Initial Version
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 88731 TWE 02/21/2013 
*     Add TimeZoneBias for notification detail display   
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard..
* WI 90136 WJS 3/4/2013
*   -FP:Add TimeZoneBias for notification detail display   
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class notificationdetail : _Page {

        private void Page_Load(object sender, EventArgs e) {
        
            int intNotificationMessageGroup;
            int lngStartRecord;
            string entityselection = String.Empty;
            string filename = string.Empty;
            string startdate = string.Empty;
            string enddate = string.Empty;
            int notificationselection = -1;
            XmlDocument nodeRespXml = null;
            XmlDocument docRespXml;
            XmlNode nodeFormFields;
            
            string timeZoneLabel = this.AppSystem.TimeZoneLabel;
            if (this.AppSystem.AdjustDaylightSavings)
            {
                if (ipoDateTimeLib.GetDaylightSavingsFlag())
                {
                    timeZoneLabel = timeZoneLabel + "-dst";
                }
            }
            int intTemp;

            //if(!SystemSrv.CheckUserPageModePermissions("notificationfiles.aspx", "download")) {
            //    Response.Redirect("accessdenied.aspx");
            //}

            if (Request.QueryString["id"] != null && int.TryParse(Request.QueryString["id"], out intTemp))
            {
                intNotificationMessageGroup = intTemp;
            }
            else
            {
                intNotificationMessageGroup = -1;
                AddMessage(EventType.Error, "The notification selected is not valid or was missing. Please select a notification to view/download the files.");
            }

            if (Request.QueryString["notificationtype"] != null && int.TryParse(Request.QueryString["notificationtype"], out intTemp))
            {
                notificationselection = intTemp;
            }

            entityselection = Request.QueryString["workgroup"];
            filename = Request.QueryString["filename"];
            startdate = Request.QueryString["startdate"];
            enddate = Request.QueryString["enddate"];
            lngStartRecord = CleanRequest.GetFormValue("txtStart", 1);

            // Submit request to the Decisioning API
            XmlDocumentResponse respdoc = R360AlertServiceClient.GetNotificationFiles(intNotificationMessageGroup, lngStartRecord);
            //todo:  fix if error found play nice.
            //if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            //{
                nodeRespXml = respdoc.Data;
           // }
            //nodeRespXml = AlertSrv.GetNotificationFiles(intNotificationMessageGroup, lngStartRecord);
            docRespXml = new XmlDocument();
            docRespXml.PreserveWhitespace = true;
            docRespXml = nodeRespXml;

            // Add FormFields node to root of document.
            nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
            ipoXmlLib.addElement(nodeFormFields, "txtID", intNotificationMessageGroup.ToString());
            ipoXmlLib.addElement(nodeFormFields, "entitySelection", entityselection.ToString());
            ipoXmlLib.addElement(nodeFormFields, "filename", filename.ToString());
            ipoXmlLib.addElement(nodeFormFields, "notificationSelection", notificationselection.ToString());
            ipoXmlLib.addElement(nodeFormFields, "startdate", startdate.ToString());
            ipoXmlLib.addElement(nodeFormFields, "enddate", enddate.ToString());

            ipoXmlLib.AddFormFieldAsNode(docRespXml.DocumentElement, "FormMain", "TimeZoneLabel", timeZoneLabel);

	        Response.ContentType = "text/html";
	        DisplayXMLDoc(docRespXml, GetScriptFile());
       }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}

		#endregion
    }
}