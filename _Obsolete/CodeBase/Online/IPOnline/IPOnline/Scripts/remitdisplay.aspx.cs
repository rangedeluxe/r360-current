using System;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Helpers;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using System.Linq;
using System.Globalization;
using System.Collections.Generic;
using FrameworkDTO.Model;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Goetzinger
* Date:
*
* Purpose:
*    Displays all transactions grouped by batch for a particular lockbox
*    and processing date selected by the user.  The user has the ability
*    to add a remitter, view the check image, and see the detail of a
*    particular transaction.
*
* Modification History
* ejg 09/13/02
*    -Converted page to use seperation logic as outlined in VI 3573
* ejg 11/15/2002
*    -Made change to use 'n/a' instead of '-' for empty/null data. VI 4412
* ejg 11/22/2002
*    -Made change to use multiple windows for image viewing. VI 4544
* VI 4247 JCS 12/11/02
*    -Modified to support receiving of only requested page of records in recordset.
* ejg 01/24/2003
*    -Made changes for Harris to Batch Detail. Added icons as links, display
*     icons for add'l docs and cots transactions. Change layout of column headings
*     and column titles. Removed the Remitter Name column from dispaly. VI 4752
*     Add link to view all images. VI 4461/4551
* ejg 02/11/2003
*    -Added check for COTS defined as CheckCount = 0 And DocumentCount > 0 or
*     StubCount > 0.
* ejg 02/13/2003
*    -Changed Account #/Serial # to Account Number/Serial Number. VI 5006
* ejg 02/14/2003
*    -Corrected issue with displaying transactions with multiple checks. VI 5014
*     Added Lockbox.LongName to the ContentSubtitle. VI 4990
* ejg 03/18/2003
*    -Updated NULL and 0 Length values to display '&nbsp;' instead of 'n/a'. VI 5173
* JMC 05/13/1003
*    -Added Error to page.  was "On error resume next".
* CR 3999 JMC 05.13.2003
*    -Added "Mark Sense" icon to display.
* CR 3621 EJG 06/17/2003
*    -Added BatchID to ViewAllImages.asp QueryString to use in the file name
*     property.
* CR 4815 EJG 09/10/2003
*    -Modified GlobalBatchID, LockboxID, and DepositDate variables to be nothing
*     if variables are not valid to aide in error handling.
* CR 9557 JMC 09/28/2003
*    -Changed LockboxID to OLLockboxID.
* CR 10537 JMC 03/03/2005
*    -Added call to AddPageToXmlDoc
* CR 46057 JMC 09/14/2011
*    -Modified page to look for a 'printview' querystring paramerter
*    -Refactored code to seperate functions.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 171188 SAS  10/28/2014
*    Changes done to write data in log file.
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class remitdisplay : _Page {

        private void Page_Load(object sender, EventArgs e)
        {
            DateTime DepositDate = DateTime.MinValue;
            bool bolPrintView = false;

            XmlDocument xmlParms;
            XmlDocument nodeRemitDisplay = null;

            bolPrintView = GetIsPrintView();

            // Grab the URL for the new batch detail.
            var url = Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/../Framework/Framework?tab=2&redir=batchdetail");

            // Redirect away. Note - we already stored the parameters into local javascript storage.
            Response.SetCookie(new HttpCookie("r360-redir-url", url));
            Response.Redirect(url, true);
            return;



            xmlParms = GetXmlParmsDoc(bolPrintView);

            // Get the xml doc
            XmlDocumentResponse respdoc = R360CBXServiceClient.GetBatchTransactions(xmlParms);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeRemitDisplay = respdoc.Data;
            }
            //nodeRemitDisplay = CBXSrv.GetBatchTransactions(xmlParms);
            XmlDocument xmlDoc = nodeRemitDisplay;
            AbortIfNoXml(xmlDoc, "GetBatchTransactions");

            //xmlDoc.AppendChild(xmlDoc.ImportNode(nodeRemitDisplay, true));

            if (xmlDoc.InnerXml != null && this.GetType() != null && this.GetType().Name != null)
            {
                EventLog.logEvent(xmlDoc.InnerXml, this.GetType().Name.ToString(), MessageType.Information, MessageImportance.Debug);
            }

            // Grab the tabs, we need to be able to get back to the BatchSummary (portlet) page.
            PopulateBreadCrumbLinks(xmlDoc);

            // Transform the xml document using the appropriate stylesheet
            if(bolPrintView) {
                DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_printview.aspx"));
            } else {
                DisplayXMLDoc(xmlDoc, GetScriptFile());
            }
        }

        private XmlDocument GetXmlParmsDoc(bool PrintView) {

            XmlDocument xmlParms;

            Guid gidOLLockboxID;
            int intStartRecord;
            Guid gidTemp;

            XmlNode node;
            DateTime dteTemp;
            int intTemp;

            // Add the parameters as nodes to an xml doc
            xmlParms = GetRequestDoc();

            AddPageToXmlDoc(xmlParms);

            // Get the start record for paging
            if(PrintView) {
                intStartRecord = 1;
            } else if(int.TryParse(CleanRequest.QueryString["start"], out intTemp)) {
                intStartRecord = intTemp;
            } else {
                intStartRecord = 1;
            }

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "StartRecord", intStartRecord.ToString());

            if (Request.QueryString["id"] != null && Request.QueryString["id"].ToLower() == "bc")
            {
                BatchDetailVars detailVars = (BatchDetailVars)Session["BatchDetailVars"];

                if (detailVars.OLLockboxID == Guid.Empty)
                {
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "LockboxID", detailVars.LockboxID.ToString());
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "BankID", detailVars.BankID.ToString());
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchID", detailVars.BatchID.ToString());
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDate", detailVars.DepositDate.ToString("MM/dd/yyyy"));
                    node.InnerText = detailVars.StartRecord.ToString();
                }
                else
                {
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "OLLockboxID", detailVars.OLLockboxID.ToString());
                }
            }
            else
            {
                BatchDetailVars detailVars = new BatchDetailVars();

                if (CleanRequest.HasQueryStringValue(Constants.QS_PARM_OLLOCKBOXID) && ipoLib.TryParse(CleanRequest.QueryString[Constants.QS_PARM_OLLOCKBOXID], out gidTemp))
                {
                    gidOLLockboxID = gidTemp;
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "OLLockboxID", gidOLLockboxID.ToString());
                    detailVars.OLLockboxID = gidTemp;
                }
                else
                {
                    if (CleanRequest.HasQueryStringValue(Constants.QS_PARM_BANKID) && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BANKID], out intTemp))
                    {
                        ipoXmlLib.addElement(xmlParms.DocumentElement, "BankID", intTemp.ToString());
                        detailVars.BankID = intTemp;
                    }
                    else
                    {
                        AddMessage(EventType.Error, "The Batch is not valid. Please use your browser's back button and try again.");
                        EventLog.logEvent("Invalid BankID parameter [bank] specified.", this.GetType().Name.ToString(), MessageType.Error, MessageImportance.Essential);
                    }

                    if (CleanRequest.HasQueryStringValue(Constants.QS_PARM_LOCKBOXID) && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp))
                    {
                        ipoXmlLib.addElement(xmlParms.DocumentElement, "LockboxID", intTemp.ToString());
                        detailVars.LockboxID = intTemp;
                    }
                    else
                    {
                        AddMessage(EventType.Error, "The Batch is not valid. Please use your browser's back button and try again.");
                        EventLog.logEvent("Invalid Lockbox parameter [lbx] specified.", this.GetType().Name.ToString(), MessageType.Error, MessageImportance.Essential);
                    }
                }

                if (CleanRequest.HasQueryStringValue("batch") && int.TryParse(CleanRequest.QueryString["batch"], out intTemp))
                {
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "BatchID", intTemp.ToString());
                    detailVars.BatchID = intTemp;
                }

                if (CleanRequest.HasQueryStringValue("date") && DateTime.TryParse(CleanRequest.QueryString["date"], out dteTemp))
                {
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDate", dteTemp.ToString("MM/dd/yyyy"));
                    detailVars.DepositDate = dteTemp;
                }

                if (CleanRequest.HasQueryStringValue("start") && int.TryParse(CleanRequest.QueryString["start"], out intTemp))
                {
                    ipoXmlLib.addElement(xmlParms.DocumentElement, "StartRecord", intTemp.ToString());
                    detailVars.StartRecord = intTemp;
                }

                Session["BatchDetailVars"] = detailVars;
            }

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "PaginateRS", (PrintView ? "0" : "1"));

            return(xmlParms);
        }

        private bool GetIsPrintView()
        {
            int intTemp;

            if (CleanRequest.TryGetQueryStringValue("printview", out intTemp))
                return (intTemp == 1);
            else
                return false;
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            base.OnInit(e);
        }
        #endregion
    }

}