﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WFS.RecHub.Common.Log;
using WFS.RecHub.OLFServicesClient;
using WFS.RecHub.Online;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Brandon Resheske
* Date:     05/02/2014
*
* Purpose:  Page replaces the 'public url' functionality of the PDF's.
*
* Modification History
* WI 139003 BLR 05/02/2014
*   - Initial Version
* WI 141094 BLR 05/12/2014
*   - Added additional logging for the input validation. 
* WI 141094 BLR 05/19/2014
*   - Added more logging, switched Debug priority to Essential.
* WI 144694 BLR 05/29/2014
*   - Altered the stream reading from the File System to OLFServices. 
* WI 145504 BLR 06/04/2014
*   - Added the FolderKey to the OLFServices calls. 
* WI 152429 BLR 08/07/2014
*   - Altered GetPDF to support ZIP files as well. 
* WI 161064 BLR 08/26/2014
*   - Redesigned the downloading of PDFs and Zip files.  All PDFs now shown
*     in-browser, and all zips now have a download link.   
******************************************************************************/


namespace WFS.RecHub.Online
{
    public partial class getpdf : _Page
    {
        private const int BUFFER_READ_SIZE = 1024;
        private const string CONTENT_TYPE_PDF = "application/pdf";
        private const string CONTENT_TYPE_ZIP = "application/zip";
        // WI 145504 : OLFServices needs the FolderKey sent.
        // Passing this string triggers OLFServices to read files from the 'OnlineImagePath', rather
        // than the 'ImagePath' in the INI SiteKey section. See OLFOnlineService.cs:GetBasePath()
        // in the OLFServices solution.
        private const string ONLINE_ARCHIVE_KEY = "OnlineArchive";

        /// <summary>
        /// Downloads a PDF file.  That's about it.
        /// The authorization is handled by the _Page parent.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            EventLog.logEvent("GetPDF:Has Started.", "GetPDF",
                    Common.MessageImportance.Debug);

            // Clean out the response stream first.
            Response.Clear();

            // PDF GUID is passed in through the query string 'id'.
            // Kick out early if we can't validate the input.
            var id = Request.QueryString["id"];
            if (!IsValidId(id))
            {
                EventLog.logWarning("GetPDF:Input ID is not a valid GUID.",
                    Common.MessageImportance.Essential);
                return;
            }

            // Kick out early if we cannot validate the content type.
            var type = Request.QueryString["type"];
            if (!IsValidType(type))
            {
                EventLog.logWarning("GetPDF:Input Type is not a valid.  Supported formats {zip,pdf}.",
                    Common.MessageImportance.Essential);
                return;
            }

            var contenttype = type == "pdf"
                ? CONTENT_TYPE_PDF
                : CONTENT_TYPE_ZIP;

            var filename = string.Format("{0}.{1}", id, type);

            // WI 144694 : Using OLFServices as opposed to the File system for the PDF Stream.
            OLFOnlineClient client;
            Int64 length;
            try
            {
                client = new OLFServicesClient.OLFOnlineClient(base.SiteKey);
                length = client.GetFileSize(filename, ONLINE_ARCHIVE_KEY);
            }
            catch (Exception ex)
            {
                EventLog.logError(ex, "GetPDF");
                return;
            }

            // Set our headers before appending the body.
            var attachmenttype = contenttype == CONTENT_TYPE_ZIP
                ? "attachment"
                : "inline";
            Response.ContentType = contenttype;
            Response.AddHeader("Content-Length", length.ToString());
            Response.AddHeader("Content-Disposition", string.Format("{0}; filename=\"{1}\"", attachmenttype, filename));
            Response.Buffer = true;
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            byte[] buffer;
            Stream readstream = null;

            try
            {
                EventLog.logEvent(string.Format("GetPDF:Starting Read on File: '{0}', Length: {1}.", filename, length),
                    "GetPDF",
                    Common.MessageImportance.Debug);

                // Getting ready to build up the response.
                buffer = new byte[BUFFER_READ_SIZE];
                readstream = client.GetFile(filename, ONLINE_ARCHIVE_KEY);

                // ReadFile/WriteStream
                var numread = readstream.Read(buffer, 0, buffer.Length);
                while (numread > 0)
                {
                    Response.OutputStream.Write(buffer, 0, numread);
                    numread = readstream.Read(buffer, 0, buffer.Length);
                }

                EventLog.logEvent(string.Format("GetPDF:Finished Write of File: '{0}'.", filename),
                    "GetPDF",
                    Common.MessageImportance.Debug);
            }
            catch (Exception ex)
            {
                // Log the error and kick out.
                EventLog.logError(ex, "GetPDF");
                return;
            }
            finally
            {
                // Clean up a bit.
                if (readstream != null)
                {
                    readstream.Close();
                    //readstream.Dispose();
                }
                buffer = null;
                Response.Flush();
            }

            EventLog.logEvent(string.Format("GetPDF:Completed Response. >>" +  filename + "<<"),
                "GetPDF",
                Common.MessageImportance.Debug);
        }

        /// <summary>
        /// Just checks a string against a GUID format.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        private bool IsValidId(string id)
        {
            Guid output;
            return !string.IsNullOrWhiteSpace(id)
                && Guid.TryParse(id, out output);
        }

        public bool IsValidType(string type)
        {
            return type == "pdf" || type == "zip";
        }
    }
}