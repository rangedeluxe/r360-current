using System;
using System.IO;
using System.Configuration;


/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     
*
* Purpose:
*
* Modification History
* CR 18949 JMC 11/07/2006
*   -Initial release version.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

	/// <summary>
	/// Summary description for WebForm1.
	/// </summary>
	public partial class Logoff : global::System.Web.UI.Page {

		private void Page_Load(object sender, EventArgs e) {
        
            Response.Redirect(Path.Combine(ConfigurationManager.AppSettings["LogonServicePath"], "Scripts/Logoff.aspx"));

            /*if(Request.Cookies["SessionID"] != null) {
                EndSession();
                Response.Cookies["SessionID"].Expires = DateTime.Now.AddDays(-10);
            }
            
            if(Request.Cookies["LogonError"] != null) {
                Response.Cookies["LogonError"].Expires = DateTime.Now.AddDays(-10);
            }

            Response.Redirect("./default.aspx");*/
		}
		
		/*private void EndSession() {
            
            XmlDocument docReqXml;
            string strSessionID;
            XmlNode nodeRespXml;

            if(Request.Cookies["SessionID"] != null && ipoLib.IsGuid(Request.Cookies["SessionID"].Value)) {

                strSessionID = Request.Cookies["SessionID"].Value;

                docReqXml = GetRequestDoc("ReqEndSession");
                ipoXmlLib.addElement(docReqXml.DocumentElement, "SessionID", (new Guid(strSessionID)).ToString());

                nodeRespXml = SessionSrv.EndSession(docReqXml);
                AddMessagesFromXml(nodeRespXml);
            }
		}*/

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			InitializeComponent();
			base.OnInit(e);
		}
		
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
			this.Load += new EventHandler(this.Page_Load);
		}
		#endregion
	}
}
