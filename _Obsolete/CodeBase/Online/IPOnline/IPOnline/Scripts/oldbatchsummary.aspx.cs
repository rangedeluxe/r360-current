using System;
using System.Collections.Generic;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     
* 
* Purpose: Displays a list of all Transactions belonging to a Decisioning 
*          Batch.
* 
* Modification History
* CR 12522 JMC 06/29/2006
*     -Created File
* CR 28738 JMC 03/23/2010
*   -Added TryCatch logic and modified page to use the Redirect function.
* CR 45181 JCS 09/09/2011
*   -Added InvoiceBalancingOption to Online Decisioning request in CompleteBatch.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldbatchsummary : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {
  
        //    int intGlobalBatchID = -1;
        //    int intBankID;
        //    int intCustomerID;
        //    int intLockboxID;
        //    int intStartRecord;

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    XmlNode nodeRespXml;
            
        //    int intTemp;

        //    if(Request.QueryString["gbatch"] != null && int.TryParse(Request.QueryString["gbatch"], out intTemp)) {

        //        intGlobalBatchID = intTemp;

        //        if(Request.Form.Count > 0) {

        //            if(Request.Form["txtBankID"] != null && int.TryParse(Request.Form["txtBankID"], out intTemp)) {
        //                intBankID = intTemp;
        //            } else {
        //                AddMessage(EventType.Error, "Cannot determine Bank ID.");
        //                intBankID = -1;
        //            }

        //            if(Request.Form["txtCustomerID"] != null && int.TryParse(Request.Form["txtCustomerID"], out intTemp)) {
        //                intCustomerID = intTemp;
        //            } else {
        //                AddMessage(EventType.Error, "Cannot determine Customer ID.");
        //                intCustomerID = -1;
        //            }

        //            if(Request.Form["txtLockboxID"] != null && int.TryParse(Request.Form["txtLockboxID"], out intTemp)) {
        //                intLockboxID = intTemp;
        //            } else {
        //                AddMessage(EventType.Error, "Cannot determine Lockbox ID.");
        //                intLockboxID = -1;
        //            }

        //            if(Request.Form["btnCheckOut"] != null && Request.Form["btnCheckOut"].Length > 0) {
        //                if(CheckOutBatch(intGlobalBatchID)) {
        //                    //Response.Redirect("oldtransactiondetail.aspx?gbatch=" + intGlobalBatchID.ToString());
        //                    Dictionary<string ,string> QueryStringParms = new Dictionary<string,string>();
        //                    QueryStringParms.Add(Constants.QS_PARM_BANKID, intBankID.ToString());
        //                    QueryStringParms.Add(Constants.QS_PARM_CUSTOMERID, intCustomerID.ToString());
        //                    QueryStringParms.Add(Constants.QS_PARM_LOCKBOXID, intLockboxID.ToString());
        //                    QueryStringParms.Add(Constants.QS_PARM_GLOBALBATCHID, intGlobalBatchID.ToString());
        //                    Redirect("oldtransactiondetail.aspx", QueryStringParms);
        //                }
        //            } else if(Request.Form["btnComplete"] != null && Request.Form["btnComplete"].Length > 0) {
        //                CompleteBatch(intGlobalBatchID);
        //            } else if(Request.Form["btnReset"] != null && Request.Form["btnReset"].Length > 0) {
        //                if((intBankID > -1) && (intCustomerID > -1) && (intLockboxID > -1)) {
        //                    if(ResetBatch(intBankID, intCustomerID, intLockboxID, intGlobalBatchID)) {
        //                        //Response.Redirect("oldbatchdetail.aspx?gbatch=" + intGlobalBatchID.ToString());
        //                        Dictionary<string ,string> QueryStringParms = new Dictionary<string,string>();
        //                        QueryStringParms.Add(Constants.QS_PARM_BANKID, intBankID.ToString());
        //                        QueryStringParms.Add(Constants.QS_PARM_CUSTOMERID, intCustomerID.ToString());
        //                        QueryStringParms.Add(Constants.QS_PARM_LOCKBOXID, intLockboxID.ToString());
        //                        QueryStringParms.Add(Constants.QS_PARM_GLOBALBATCHID, intGlobalBatchID.ToString());
        //                        Redirect("oldbatchdetail.aspx", QueryStringParms);
        //                    }
        //                }
        //            }
        //        }

        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine GlobalBatchID.");
        //        intGlobalBatchID = -1;
        //    }

        //    // Start Record
        //    if(Request.Form["txtStart"] != null && 
        //       Request.Form["txtStart"].Length > 0 && 
        //       int.TryParse(Request.Form["txtStart"], out intTemp) && 
        //       Request.Form["txtAction"] != null && 
        //       Request.Form["txtAction"].Length > 0) {

        //        intStartRecord = intTemp;
        //    } else {
        //        intStartRecord = 1;
        //    }

        //    strReqXml = "<ReqGetBatch>"
        //              + "    <Batch GlobalBatchID=\"" + ipoLib.IIf(intGlobalBatchID > -1, intGlobalBatchID.ToString(), string.Empty) + "\" />"
        //              + "    <StartRecord>" + intStartRecord.ToString() + "</StartRecord>"
        //              + "    <Paginate>0</Paginate>"
        //              + "</ReqGetBatch>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetBatch(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    // Add FormFields node to root of document.
        //    nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    ipoXmlLib.addElement(nodeFormFields, "GlobalBatchID", intGlobalBatchID.ToString());

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        //private bool CompleteBatch(long GlobalBatchID) {

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeRespXml;
        //    int intTemp = -1;
        //    string strBankID = string.Empty;
        //    string strLockboxID = string.Empty;

        //    // modified as was able to complete transaction when balancing forced if you deleted the balancing row, then completed
        //    if (Request.Form["hidBankID"] != null && int.TryParse(Request.Form["hidBankID"], out intTemp)) {
        //        strBankID = "BankID=\"" + intTemp.ToString() + "\"";
        //    } else if (Request.Form["txtBankID"] != null && int.TryParse(Request.Form["txtBankID"], out intTemp)) {
        //        strBankID = "BankID=\"" + intTemp.ToString() + "\"";
        //    }

        //    if (Request.Form["hidLockboxID"] != null && int.TryParse(Request.Form["hidLockboxID"], out intTemp)) {
        //        strLockboxID = "LockboxID=\"" + intTemp.ToString() + "\"";
        //    } else if (Request.Form["txtLockboxID"] != null && int.TryParse(Request.Form["txtLockboxID"], out intTemp)) {
        //        strLockboxID = "LockboxID=\"" + intTemp.ToString() + "\"";
        //    }

        //    strReqXml = "<ReqBatchUpdate>"
        //              + "    <Batch GlobalBatchID=\"" + GlobalBatchID.ToString() + "\" " + strBankID + " " + strLockboxID + " />"
        //              + "</ReqBatchUpdate>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.CompleteBatch(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    AddMessagesToPage(docRespXml);

        //    return(!DocumentContainsErrors(docRespXml));
        //}

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion
    }
}
