using System;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose: 
*
* Modification History
* CR 45984 JCS 02/13/2012
*    -Added dynamic branding folder information.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.

* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class remitterframe : _Page {

        private void Page_Load(object sender, EventArgs e) {

            XmlDocument xmlDoc;
            XmlNode nodeFormFields;

            Guid gidOLLockboxID;
            int intBankID;
            int intLockboxID;
            int intBatchID;
            DateTime dteDepositDate;
            string strPICSDate;
            int intTransactionID;
            int intBatchSequence;
            string strRoutingNumber;
            string strAccount;

            int intTemp;
            Guid gidTemp;
            DateTime dteTemp;

            // Get the QueryString Variables
            if(CleanRequest.QueryString[Constants.QS_PARM_OLLOCKBOXID] != null && ipoLib.TryParse(CleanRequest.QueryString[Constants.QS_PARM_OLLOCKBOXID], out gidTemp)) {
                gidOLLockboxID = gidTemp;
            } else {
                EventLog.logEvent("Parameter [lckbx] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                gidOLLockboxID = Guid.Empty;
                //throw(new Exception("Invalid parameters specified"));
            }

            // Get input values from querystring
            if(CleanRequest.QueryString[Constants.QS_PARM_BANKID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BANKID], out intTemp)) {
                intBankID = intTemp;
            } else {
                EventLog.logEvent("Parameter [bank] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                intBankID = -1;
                //throw(new Exception("Invalid parameters specified"));
            }

            if(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp)) {
                intLockboxID = intTemp;
            } else {
                EventLog.logEvent("Parameter [box] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                intLockboxID = -1;
                //throw(new Exception("Invalid parameters specified"));
            }

            if(CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE] != null && DateTime.TryParse(CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE], out dteTemp)) {
                dteDepositDate = dteTemp;
            } else {
                EventLog.logEvent("Parameter [date] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                dteDepositDate = DateTime.MinValue;
                //throw(new Exception("Invalid parameters specified"));
            }

            if(CleanRequest.HasQueryStringValue(Constants.QS_PARM_PICSDATE)) {
                strPICSDate = CleanRequest.QueryString[Constants.QS_PARM_PICSDATE];
            } else {
                strPICSDate = string.Empty;
            }

            if(CleanRequest.QueryString[Constants.QS_PARM_BATCHID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHID], out intTemp)) {
                intBatchID = intTemp;
            } else {
                EventLog.logEvent("Parameter [batch] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                intBatchID = -1;
                //throw(new Exception("Invalid parameters specified"));
            }

            if(CleanRequest.QueryString[Constants.QS_PARM_TXNID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_TXNID], out intTemp)) {
                intTransactionID = intTemp;
            } else {
                EventLog.logEvent("Parameter [txn] was either not specified or was invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                AddMessage(EventType.Error, "Invalid parameters specified");
                intTransactionID = -1;
                //throw(new Exception("Invalid parameters specified"));
            }

            if(CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE], out intTemp)) {
                intBatchSequence = intTemp;
            } else {
                intBatchSequence = -1;
            }

            strRoutingNumber = CleanRequest.GetQueryStringValue(Constants.QS_PARM_RT, string.Empty);
            strAccount = CleanRequest.GetQueryStringValue(Constants.QS_PARM_ACCOUNT, string.Empty);

            xmlDoc = ipoXmlLib.GetXMLDoc("Page");
            
            // Add Branding Scheme folder name to Xml
            AddBrandingInfoToXml(xmlDoc.DocumentElement, BrandingSchemeFolder);

            nodeFormFields = ipoXmlLib.addElement(xmlDoc.DocumentElement, "FormFields", string.Empty);

            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "OLLockboxID", gidOLLockboxID.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "BankID", intBankID.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "LockboxID", intLockboxID.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "BatchID", intBatchID.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "DepositDate", dteDepositDate.ToString("MM/dd/yyyy"));
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "PICSDate", strPICSDate);
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "TransactionID", intTransactionID.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "BatchSequence", intBatchSequence.ToString());
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "RoutingNumber", strRoutingNumber);
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "Account", strAccount);
            ipoXmlLib.AddQueryStringValueAsNode(xmlDoc.DocumentElement, "Type", "C");

            DisplayXMLDoc(xmlDoc, GetScriptFile());
        }


		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}
		
		/*/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.Page_Load);
		}*/
		#endregion
    }
}