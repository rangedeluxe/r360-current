using System.Collections;
using System.Xml;
using System;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Goetzinger
* Date:     
*
* Purpose:  
*
* Modification History
* CR 46627 WJS 09/16/2011 
*    -Change to support checkboxs. Saving for checkboxes
* CR 25174 JCS 09/13/2010
*    -Ensure CSR users are not able to update preferences using Online
*     Emulation.
* CR 51411 JMC 04/26/2012
*    -Moved hashtable object containing existing User Preferences to be local
*     to Page_Load().  This corrected an issue where User Preferences would
*     not be saved after restoring default values.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 116275 CEJ 10/11/2013
*   Add value validation for receivables summary records per page
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 155812 TWE 07/30/2014
*    add check for permission to access this page
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class userpreferences : _Page {

        private void Page_Load(object sender, EventArgs e) {

            XmlDocument xmlDoc;
            XmlDocument xmlStream;
            XmlDocument xmlInput;
            XmlDocument docResults = null;
            XmlNode node = null;
            string strValue = "N" ;
            Hashtable htUserPrefTable;

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_UserPreferences, R360Permissions.ActionType.View))
            {
                Response.Redirect("accessdenied.aspx");
            }

            if (Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0 && !AppSession.IsEmulationSession)
            {
                // determine which action requested
                switch (Request.Form["txtAction"].ToLower())
                {
                    case "submit":				// update user preferences
                        XmlDocumentResponse respdoc = R360UserServiceClient.GetOLUserPreferences();
                        docResults = respdoc.Data;

                        getRecordSetNodes(docResults, out htUserPrefTable);

                        foreach (string preference in htUserPrefTable.Keys)
                        {
                            string strKey = "fld" + preference;
                            if (CleanRequest.HasFormValue(strKey))
                            {
                                CleanRequest.TryGetFormValue(strKey, out strValue);
                                if ((strValue == "on") || (strValue == "off"))
                                {
                                    switch (strValue.ToLower())
                                    {
                                        case "on": strValue = "Y"; break;
                                        case "off": strValue = "N"; break;
                                        default: strValue = "N"; break;
                                    }
                                }
                            }
                            else
                            {

                                strValue = "N";
                            }
                            SetField(preference, strValue);
                        }

                        //  Create parameters xml document
                        xmlInput = GetRequestDoc();

                        // validate form fields
                        if (ValidateFormFields())
                        {
                            // only add fields to xmlInput when no error
                            foreach (string key in PageFields.Keys)
                            {
                                node = ipoXmlLib.addElement(xmlInput.DocumentElement, key, PageFields[key]);
                            }
                        }
                        XmlDocumentResponse respdoc2 = R360UserServiceClient.SetOLUserPreferences(xmlInput);
                       
                        xmlStream = new XmlDocument();
                        xmlStream = respdoc2.Data;  

                        break;
                    case "restore defaults":	// restore default settings
                        XmlDocumentResponse respRestoredflt = R360UserServiceClient.RestoreDefaultOLPreferences();
                        xmlStream = new XmlDocument();
                        xmlStream = respRestoredflt.Data;   
                        break;
                    default:					// action not recognized selected not recognized by Online
                        AddMessage(EventType.Error, "The action requested is not valid.");
                        XmlDocumentResponse respdocdflt = R360UserServiceClient.GetOLUserPreferences();
                        xmlStream = new XmlDocument();
                        xmlStream = respdocdflt.Data;

                        break;
                }
            }
            else
            {
                // get existing oluserpreferences
                XmlDocumentResponse respdocdflt = R360UserServiceClient.GetOLUserPreferences();
                xmlStream = new XmlDocument();
                xmlStream = respdocdflt.Data;

                getRecordSetNodes(xmlStream, out htUserPrefTable);

            }

            // Display message indicating updates not allowed during User Emulation.
            if (AppSession.IsEmulationSession) {
                AddMessage(EventType.Information, "You are not able to modify user preferences in emulation mode.");
            }

            // convert xml stream to xml dom document
            xmlDoc = xmlStream;

            // only add user submitted form fields if submit
            if(Request.Form["txtAction"] != null && Request.Form["txtAction"].ToLower() == "submit") {
	            foreach(string fld in PageFields.Values) {
		            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", fld, GetField(fld), "INPUT");
	            }
            }


            // transform the xml document using the appropriate stylesheet
            DisplayXMLDoc(xmlDoc, GetScriptFile());

            xmlStream = null;
            xmlDoc = null;
            xmlInput = null;
            node = null;
        }


        private bool getRecordSetNodes(XmlDocument node, out Hashtable userPrefTable)
        {
            bool bInSuccess = true;
            XmlAttribute attribute;
            string prefName = string.Empty;
            string userSetting = string.Empty;
            XmlNodeList recordSetList = node.SelectNodes("//Recordset[@Name = 'OLUserPreferences']/Record");
            userPrefTable = new Hashtable();

            foreach (XmlNode recordSetNode in recordSetList)
            {
                attribute = recordSetNode.Attributes["PreferenceName"];
                if (attribute != null)
                {
                    prefName = attribute.Value;
                }
                attribute = recordSetNode.Attributes["UserSetting"];
                if (attribute != null)
                {
                    userSetting = attribute.Value;
                }
                userPrefTable.Add(prefName, userSetting);

            }
            return bInSuccess;
        }

        //TO DO:
        //	- Add FldDataTypeEnum to OLPreferences for dynamic display and validation
        private bool ValidateFormFields() {

            bool blnSuccess = true;
            int intTemp;
        	
            // TODO:
            // 	Validation done dynamically based on field type

          IEnumerator myEnumerator = PageFields.GetEnumerator();
          DictionaryEntry de;

          while(myEnumerator.MoveNext()) {
             de = (DictionaryEntry)myEnumerator.Current;


	            switch(de.Key.ToString()) {
		            case "recordsperpage":
			            if(de.Value.ToString().Length > 0) {
				            if(!int.TryParse(de.Value.ToString(), out intTemp)) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Records Displayed per Page must be numeric.");
				            } else if(intTemp < 1 || intTemp > 1000){ 
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Records Displayed per Page must be within range (1-1000).");
				            }
			            } else {
				            blnSuccess = false;
				            AddMessage(EventType.Error, "Records Displayed per Page is required.");
			            }
			            break;
		            case "maxlogonattempts":
			            if(de.Value.ToString().Length > 0) {
				            if(!int.TryParse(de.Value.ToString(), out intTemp)) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Maximum Number of Logon Attempts must be numeric.");
				            }
			            } else {
				            blnSuccess = false;
				            AddMessage(EventType.Error, "Maximum number of logon attempts is required.");
			            }					
			            break;
		            case "sessionexpiration":
			            if(de.Value.ToString().Length > 0) {
				            if(!int.TryParse(de.Value.ToString(), out intTemp)) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Idle Session Timeout (minutes) must be numeric.");
				            }
			            } else {
				            blnSuccess = false;
				            AddMessage(EventType.Error, "Idle Session Timeout (minutes) is required.");
			            }					
			            break;
		            case "passwordexpiration":
			            if(de.Value.ToString().Length > 0) {
				            if(!int.TryParse(de.Value.ToString(), out intTemp)) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Password Expiration (days) must be numeric.");
				            }
			            } else {
				            blnSuccess = false;
				            AddMessage(EventType.Error, "Password Expiration (days) is required.");
			            }		
			            break;
		            case "receivablessummaryrecordsperpage":
			            if(de.Value.ToString().Length > 0) {
				            if(!int.TryParse(de.Value.ToString(), out intTemp)) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Receivables Summary Records Displayed per Page must be numeric.");
				            } else if(intTemp < 1 || intTemp > 1000) {
					            blnSuccess = false;
					            AddMessage(EventType.Error, "Receivables Summary Records Displayed per Page must be within range (1-1000).");
                            }
			            } else {
				            blnSuccess = false;
				            AddMessage(EventType.Error, "Receivables Summary Records Displayed per Page is required.");
			            }
			            break;
	            }
            }
        	
            return(blnSuccess);
        }
    }
}
