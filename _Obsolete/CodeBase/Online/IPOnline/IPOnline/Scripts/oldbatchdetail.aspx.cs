using System;
using System.Collections.Generic;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Module:          Online Decisioning Batch Detail.
* 
* Filename:        oldbatchdetail.aspx
* 
* Author:          Joel Caples
* 
* Description:     Displays a list of all Transactions belonging to a Decisioning 
*                  Batch.
* 
* Revisions:
* CR 12522 JMC 06/29/2006
*     -Created File
* CR 28738 JMC 03/23/2010
*   -Added TryCatch logic and modified page to use the Redirect function.
* CR 45181 JCS 09/09/2011
*   -Added InvoiceBalancingOption to Online Decisioning request
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP: Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
**********************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldbatchdetail : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    int intGlobalBatchID = -1;
        //    int intBankID;
        //    int intCustomerID;
        //    int intLockboxID;
        //    int intStartRecord;

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    XmlNode nodeRespXml;
            
        //    int intTemp;

        //    if(Request.Form["txtBankID"] != null && int.TryParse(Request.Form["txtBankID"], out intTemp)) {
        //        intBankID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_BANKID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_BANKID], out intTemp)) {
        //        intBankID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Bank ID.");
        //        intBankID = -1;
        //    }

        //    if(Request.Form["txtCustomerID"] != null && int.TryParse(Request.Form["txtCustomerID"], out intTemp)) {
        //        intCustomerID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_CUSTOMERID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_CUSTOMERID], out intTemp)) {
        //        intCustomerID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Customer ID.");
        //        intCustomerID = -1;
        //    }

        //    if(Request.Form["txtLockboxID"] != null && int.TryParse(Request.Form["txtLockboxID"], out intTemp)) {
        //        intLockboxID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_LOCKBOXID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp)) {
        //        intLockboxID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Lockbox ID.");
        //        intLockboxID = -1;
        //    }
            
        //    if(Request.QueryString[Constants.QS_PARM_GLOBALBATCHID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_GLOBALBATCHID], out intTemp)) {

        //        intGlobalBatchID = intTemp;

        //        if(Request.Form["btnCheckOut"] != null && Request.Form["btnCheckOut"].Length > 0) {
        //            if(CheckOutBatch(intGlobalBatchID)) {
        //                Dictionary<string ,string> QueryStringParms = new Dictionary<string,string>();
        //                QueryStringParms.Add(Constants.QS_PARM_BANKID, intBankID.ToString());
        //                QueryStringParms.Add(Constants.QS_PARM_LOCKBOXID, intLockboxID.ToString());
        //                QueryStringParms.Add(Constants.QS_PARM_CUSTOMERID, intCustomerID.ToString());
        //                QueryStringParms.Add(Constants.QS_PARM_GLOBALBATCHID, intGlobalBatchID.ToString());
        //                Redirect("oldtransactiondetail.aspx", QueryStringParms);
        //            }
        //        } else if(Request.Form["btnComplete"] != null && Request.Form["btnComplete"].Length > 0) {
        //            strReqXml = "<ReqBatchUpdate>"
        //                      + "    <Batch GlobalBatchID=\"" + intGlobalBatchID.ToString() + "\" BankID=\"" + intBankID + "\" LockboxID=\"" + intLockboxID + "\" />"
        //                      + "</ReqBatchUpdate>";
                
        //            docReqXml = new XmlDocument();
        //            docReqXml.LoadXml(strReqXml);
        //            CompleteBatch(intGlobalBatchID, docReqXml);

        //        } else if(Request.Form["btnReset"] != null && Request.Form["btnReset"].Length > 0) {

        //            if((intBankID > -1) && (intCustomerID > -1) && (intLockboxID > -1)) {
        //                ResetBatch(intBankID, intCustomerID, intLockboxID, intGlobalBatchID);
        //            }
        //        }
        //    }

        //    // Start Record
        //    if((Request.Form["txtStart"] != null) && (Request.Form["txtStart"].Length > 0) && (int.TryParse(Request.Form["txtStart"], out intTemp)) && (Request.Form["txtAction"] != null) && (Request.Form["txtAction"].Length > 0)) {
        //        intStartRecord = intTemp;
        //    } else {
        //        intStartRecord = 1;
        //    }

        //    strReqXml = "<ReqGetBatch>"
        //              + "    <Batch GlobalBatchID=\"" + ipoLib.IIf(intGlobalBatchID > -1, intGlobalBatchID.ToString(), string.Empty) + "\" />"
        //              + "    <StartRecord>" + intStartRecord.ToString() + "</StartRecord>"
        //              + "</ReqGetBatch>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetBatch(docReqXml);
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    // Add FormFields node to root of document.
        //    nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    ipoXmlLib.addElement(nodeFormFields, "GlobalBatchID", intGlobalBatchID.ToString());

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion
    }
}
