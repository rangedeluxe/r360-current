using System;
using System.Collections.Generic;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     
* 
* Purpose:  Displays a list of all Transactions belonging to a Decisioning 
*           Batch.
* 
* Modification History
* CR 12522 JMC 06/29/2006
*     -Created File
* CR 28738 JMC 03/23/2010
*   -Added Destination Lockbox logic.
* CR 31651 JMC 10/26/2010
*    -Allow Decisioning for fields where fieldname contains an underscore ('_') 
*     character
* CR 45250 JMC 06/15/2011
*    -Fixed bug that caused an error that occurred when deleting a stub and no
*     decision (accept/reject) has been chosen.
* CR 45181 JCS 09/08/2011
*   -Added InvoiceBalancingOption to form for client-side scripting of invoice balancing.
*   -Added supporting fields to allow for totaling transaction check amount totals.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class oldtransactiondetail : _OLDeBase {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    int intBankID;
        //    int intCustomerID;
        //    int intLockboxID;
        //    int intGlobalBatchID;
        //    int intTransactionID;
        //    int intNextTransactionID;
        //    int intCheckStartRecord;
        //    int intStubStartRecord;
        //    int intDocumentStartRecord;
        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeFormFields;
        //    int intTemp;
        //    XmlNode nodeRespXml;

        //    if(Request.Form["hidBankID"] != null && int.TryParse(Request.Form["hidBankID"], out intTemp)) {
        //        intBankID = intTemp;
        //    } else if(Request.Form["txtBankID"] != null && int.TryParse(Request.Form["txtBankID"], out intTemp)) {
        //        intBankID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_BANKID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_BANKID], out intTemp)) {
        //        intBankID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Bank ID.");
        //        intBankID = -1;
        //    }

        //    if(Request.Form["hidCustomerID"] != null && int.TryParse(Request.Form["hidCustomerID"], out intTemp)) {
        //        intCustomerID = intTemp;
        //    } else if(Request.Form["txtCustomerID"] != null && int.TryParse(Request.Form["txtCustomerID"], out intTemp)) {
        //        intCustomerID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_CUSTOMERID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_CUSTOMERID], out intTemp)) {
        //        intCustomerID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Customer ID.");
        //        intCustomerID = -1;
        //    }

        //    if(Request.Form["hidLockboxID"] != null && int.TryParse(Request.Form["hidLockboxID"], out intTemp)) {
        //        intLockboxID = intTemp;
        //    } else if(Request.Form["txtLockboxID"] != null && int.TryParse(Request.Form["txtLockboxID"], out intTemp)) {
        //        intLockboxID = intTemp;
        //    } else if(Request.QueryString[Constants.QS_PARM_LOCKBOXID] != null && int.TryParse(Request.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp)) {
        //        intLockboxID = intTemp;
        //    } else {
        //        AddMessage(EventType.Error, "Cannot determine Lockbox ID.");
        //        intLockboxID = -1;
        //    }

        //    if(Request.QueryString["gbatch"] != null && int.TryParse(Request.QueryString["gbatch"], out intTemp)) {
        //        intGlobalBatchID = intTemp;
        //    } else {
        //        intGlobalBatchID = -1;
        //    }

        //    if(Request.Form["hidTransactionID"] != null && int.TryParse(Request.Form["hidTransactionID"], out intTemp)) {
        //        intTransactionID = intTemp;
        //    } else if(Request.QueryString["transaction"] != null && int.TryParse(Request.QueryString["transaction"], out intTemp)) {
        //        intTransactionID = intTemp;
        //    } else {
        //        intTransactionID = -1;
        //    }

        //    if(Request.Form["txtNextTxnID"] != null && int.TryParse(Request.Form["txtNextTxnID"], out intTemp)) {
        //        intNextTransactionID = intTemp;
        //    } else {
        //        intNextTransactionID = intTransactionID;
        //    }

        //    if((Request.Form["txtCheckStartRecord"] != null) && (Request.Form["txtCheckStartRecord"].Length > 0) && (int.TryParse(Request.Form["txtCheckStartRecord"], out intTemp))) {
        //        intCheckStartRecord = intTemp;
        //    } else {
        //        intCheckStartRecord = 1;
        //    }

        //    if((Request.Form["txtStubStartRecord"] != null) && (Request.Form["txtStubStartRecord"].Length > 0) && (int.TryParse(Request.Form["txtStubStartRecord"], out intTemp))) {
        //        intStubStartRecord = intTemp;
        //    } else {
        //        intStubStartRecord = 1;
        //    }

        //    if((Request.Form["txtDocumentStartRecord"] != null) && (Request.Form["txtDocumentStartRecord"].Length > 0) && (int.TryParse(Request.Form["txtDocumentStartRecord"], out intTemp))) {
        //        intDocumentStartRecord = intTemp;
        //    } else {
        //        intDocumentStartRecord = 1;
        //    }

        //    if(Request.Form["btnCheckOut"] != null && Request.Form["btnCheckOut"].Length > 0) {
        //        if(CheckOutBatch(intGlobalBatchID)) {;
        //            Dictionary<string ,string> QueryStringParms = new Dictionary<string,string>();
        //            QueryStringParms.Add(Constants.QS_PARM_BANKID, intBankID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_LOCKBOXID, intLockboxID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_CUSTOMERID, intCustomerID.ToString());
        //            QueryStringParms.Add(Constants.QS_PARM_GLOBALBATCHID, intGlobalBatchID.ToString());
        //            Redirect("oldtransactiondetail.aspx", QueryStringParms);
        //        }
        //    } else if(Request.Form["txtAction"] != null) {
        //        switch (Request.Form["txtAction"]) {
        //            case "updatetxn":
        //                if (UpdateTransaction(intGlobalBatchID, intTransactionID)) {
        //                    if (intTransactionID != intNextTransactionID) {
        //                intCheckStartRecord = 1;
        //                intStubStartRecord = 1;
        //                intDocumentStartRecord = 1;
        //            }
        //            intTransactionID = intNextTransactionID;
        //        }
        //                break;
                    
        //            case "complete":
        //                if (CompleteBatch(intGlobalBatchID, buildUpdateBatchXml())) {
        //                    Response.Redirect("oldbatches.aspx");
        //                }
        //                break;
        //        }
        //    } else if (Request.Form["btnReset"] != null && Request.Form["btnReset"].Length > 0) {
        //        if((intBankID > -1) && (intCustomerID > -1) && (intLockboxID > -1)) {
        //            ResetBatch(intBankID, intCustomerID, intLockboxID, intGlobalBatchID);
        //        }
        //    }
            
        //    strReqXml = "<ReqGetTransaction>"
        //              + "    <Bank BankID=\"" + intBankID.ToString() + "\" />"
        //              + "    <Lockbox LockboxID=\"" + intLockboxID.ToString() + "\" />"
        //              + "    <Batch GlobalBatchID=\"" + ipoLib.IIf(intGlobalBatchID > -1, intGlobalBatchID.ToString(), string.Empty) + "\" />"
        //              + "    <Transaction TransactionID=\"" + ipoLib.IIf(intTransactionID > -1, intTransactionID.ToString(), string.Empty) + "\" />"
        //              + "    <CheckStartRecord>" + intCheckStartRecord.ToString() + "</CheckStartRecord>"
        //              + "    <StubStartRecord>" + intStubStartRecord.ToString() + "</StubStartRecord>"
        //              + "    <DocumentStartRecord>" + intDocumentStartRecord.ToString() + "</DocumentStartRecord>"
        //              //+ "    <PaginateStubs>1</PaginateStubs>"
        //              + "</ReqGetTransaction>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    // Submit Request to the Decisioning API
        //    nodeRespXml = DecisioningSrv.GetTransaction(docReqXml);

        //    // Add FormFields node to root of document.
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    nodeFormFields = docRespXml.DocumentElement.SelectSingleNode("FormFields");
        //    if(nodeFormFields == null) {
        //        nodeFormFields = ipoXmlLib.addElement(docRespXml.DocumentElement, "FormFields", "");
        //    }

        //    Response.ContentType = "text/html";
        //    DisplayXMLDoc(docRespXml, GetScriptFile());
        //}

        //private bool UpdateTransaction(long GlobalBatchID, int TransactionID) {

        //    XmlDocument docReqXml;
        //    XmlDocument docRespXml;
        //    XmlNode nodeRespXml;

        //    docReqXml = buildUpdateBatchXml();
        //    nodeRespXml = DecisioningSrv.UpdateBatch(docReqXml);

        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    AddMessagesToPage(docRespXml);
            
        //    return(!DocumentContainsErrors(docRespXml));
        //}

        //private XmlDocument buildUpdateBatchXml() {

        //    string strGlobalBatchID;
        //    string strTransactionID;
        //    XmlNode nodeBatch;
        //    XmlNode nodeTransaction;
        //    XmlNode nodeStub;
        //    XmlNode nodeField;
        //    string[] arFormField;
        //    Guid gidDEItemRowDataID;
        //    XmlDocument docRetVal;
        //    XmlAttribute attRejectResolution;
        //    XmlAttribute attDestLockbox;
        //    XmlAttribute attBalancingAction;

        //    docRetVal = ipoXmlLib.GetXMLDoc("ReqBatchUpdate");

        //    foreach(string var in Request.Form) {

        //        arFormField = var.Split('_');

        //        switch(arFormField.Length - 1) {
        //            case 0:
        //                break;
        //            case 1:
        //                if(arFormField[0].StartsWith("gbid")) {

        //                    strGlobalBatchID = arFormField[0].Substring(4);
        //                    nodeBatch = docRetVal.DocumentElement.SelectSingleNode("Batch[GlobalBatchID='" + strGlobalBatchID + "']");
        //                    if(nodeBatch == null) {
        //                        nodeBatch = ipoXmlLib.addElement(docRetVal.DocumentElement, "Batch", "");
        //                        ipoXmlLib.addElement(nodeBatch, "GlobalBatchID", strGlobalBatchID);
        //                    }

        //                    switch(arFormField[1]) {
        //                        case "txtDepositDate":
        //                            ipoXmlLib.addElement(nodeBatch, "DepositDate", Request.Form[var]);
        //                            break;
        //                        case "txtBatchMode":
        //                            ipoXmlLib.addElement(nodeBatch, "BatchMode", Request.Form[var]);
        //                            break;
        //                        case "txtPriority":
        //                            ipoXmlLib.addElement(nodeBatch, "Priority", Request.Form[var]);
        //                            break;
        //                    }
        //                } 

        //                break;
                        
        //            case 2:
        //                if(arFormField[0].ToString().StartsWith("rdoTxnDecisionStatus") || arFormField[0].ToString().StartsWith("selDestLockbox")
        //                    || arFormField[0].ToString().StartsWith("txtBalancing")) {

        //                    if(arFormField[1].ToString().StartsWith("gbid")) {

        //                        strGlobalBatchID = arFormField[1].Substring(4);
        //                        nodeBatch = docRetVal.DocumentElement.SelectSingleNode("Batch[GlobalBatchID='" + strGlobalBatchID + "']");
        //                        if(nodeBatch == null) {
        //                            nodeBatch = ipoXmlLib.addElement(docRetVal.DocumentElement, "Batch", "");
        //                            ipoXmlLib.addElement(nodeBatch, "GlobalBatchID", strGlobalBatchID);
        //                        }

        //                        if (nodeBatch != null) {
        //                            // add BankID and LockboxID for Invoice Balancing to determine correct balancing option
        //                            if (Request.Form["hidBankID"] != null)
        //                                ipoXmlLib.addElement(nodeBatch, "BankID", Request.Form["hidBankID"]);
        //                            if (Request.Form["hidLockboxID"] != null)
        //                                ipoXmlLib.addElement(nodeBatch, "LockboxID", Request.Form["hidLockboxID"]);
        //                        }

        //                        if(arFormField[2].ToString().StartsWith("txnid")) {
        //                            strTransactionID = arFormField[2].Substring(5);
        //                            nodeTransaction = nodeBatch.SelectSingleNode("Transactions[@TransactionID='" + strTransactionID + "']");
        //                            if(nodeTransaction == null) {
        //                                nodeTransaction = ipoXmlLib.addElement(nodeBatch, "Transactions", "");
        //                                ipoXmlLib.addAttribute(nodeTransaction, "TransactionID", strTransactionID);
        //                            }   // if

        //                            if(arFormField[0].ToString().StartsWith("rdoTxnDecisionStatus")) {
        //                                attRejectResolution = nodeTransaction.Attributes["RejectResolution"];
        //                                if(attRejectResolution == null) {
        //                                    ipoXmlLib.addAttribute(nodeTransaction, "RejectResolution", Request.Form[var]);
        //                                } else { 
        //                                    attRejectResolution.Value = Request.Form[var].ToString();
        //                                }

        //                            // add invoice balancing action chosen by user
        //                            } else if (arFormField[0].ToString().StartsWith("txtBalancingAction")) {
        //                                attBalancingAction = nodeTransaction.Attributes["BalancingAction"];
        //                                if (attBalancingAction == null) {
        //                                    ipoXmlLib.addAttribute(nodeTransaction, "BalancingAction", Request.Form[var]);
        //                                } else {
        //                                    attBalancingAction.Value = Request.Form[var].ToString();
        //                                }

        //                            } else {
        //                                attDestLockbox = nodeTransaction.Attributes["selDestLockbox"];
        //                                if(attDestLockbox == null) {
        //                                    ipoXmlLib.addAttribute(nodeTransaction, "DestinationLockboxKey", Request.Form[var]);
        //                                } else { 
        //                                    attDestLockbox.Value = Request.Form[var].ToString();
        //                                }
        //                            }
        //                        }   // if
        //                    }
        //                }
                        
        //                break;

        //            case 3:
        //                if(arFormField[0].StartsWith("gbid")) {

        //                    strGlobalBatchID = arFormField[0].Substring(4);
        //                    nodeBatch = docRetVal.DocumentElement.SelectSingleNode("Batch[GlobalBatchID='" + strGlobalBatchID + "']");
        //                    if(nodeBatch == null) {
        //                        nodeBatch = ipoXmlLib.addElement(docRetVal.DocumentElement, "Batch", "");
        //                        ipoXmlLib.addElement(nodeBatch, "GlobalBatchID", strGlobalBatchID);
        //                    }

        //                    if(arFormField[1].StartsWith("txnid")) {
        //                        strTransactionID = arFormField[1].Substring(5);
        //                        nodeTransaction = nodeBatch.SelectSingleNode("Transactions[@TransactionID='" + strTransactionID + "']");
        //                        if(nodeTransaction == null) {
        //                            nodeTransaction = ipoXmlLib.addElement(nodeBatch, "Transactions", "");
        //                            ipoXmlLib.addAttribute(nodeTransaction, "TransactionID", strTransactionID);
        //                        }   // if

        //                        if(arFormField[2].StartsWith("deitemrowdataid")) {
        //                            gidDEItemRowDataID = new Guid(arFormField[2].Substring(15));

        //                            if(gidDEItemRowDataID != Guid.Empty) {
        //                                nodeStub = nodeTransaction.SelectSingleNode("Stubs[@DEItemRowDataID='" + gidDEItemRowDataID.ToString() + "']");
        //                                if(nodeStub == null) {
        //                                    nodeStub = ipoXmlLib.addElement(nodeTransaction, "Stubs", "");
        //                                    ipoXmlLib.addAttribute(nodeStub, "DEItemRowDataID", gidDEItemRowDataID.ToString());
        //                                }   // if
        //                                if(arFormField[3].ToLower() == "chkdeletestub") {
        //                                    ipoXmlLib.addAttribute(nodeStub, "Action", "Delete");
        //                                }
        //                            }   // if
        //                        } 
        //                    }   // if
        //                }   // if
                        
        //                break;

        //            default:
        //                if(arFormField[0].StartsWith("gbid")) {

        //                    string strFieldName = string.Empty;
        //                    for(int i=4;i<arFormField.Length;++i) {
        //                        if (strFieldName.Length > 0) {
        //                            strFieldName += "_";
        //                        }
        //                        strFieldName = strFieldName + arFormField[i];
        //                    }

        //                    strGlobalBatchID = arFormField[0].Substring(4);
        //                    nodeBatch = docRetVal.DocumentElement.SelectSingleNode("Batch[GlobalBatchID='" + strGlobalBatchID + "']");
        //                    if(nodeBatch == null) {
        //                        nodeBatch = ipoXmlLib.addElement(docRetVal.DocumentElement, "Batch", "");
        //                        ipoXmlLib.addElement(nodeBatch, "GlobalBatchID", strGlobalBatchID);
        //                    }

        //                    if(arFormField[1].StartsWith("txnid")) {
        //                        strTransactionID = arFormField[1].Substring(5);
        //                        nodeTransaction = nodeBatch.SelectSingleNode("Transactions[@TransactionID='" + strTransactionID + "']");
        //                        if(nodeTransaction == null) {
        //                            nodeTransaction = ipoXmlLib.addElement(nodeBatch, "Transactions", "");
        //                            ipoXmlLib.addAttribute(nodeTransaction, "TransactionID", strTransactionID);
        //                        }   // if

        //                        if(arFormField[2].StartsWith("deitemrowdataid")) {
        //                            gidDEItemRowDataID = new Guid(arFormField[2].Substring(15));
        //                            if(gidDEItemRowDataID != Guid.Empty) {
        //                                nodeStub = nodeTransaction.SelectSingleNode("Stubs[@DEItemRowDataID='" + gidDEItemRowDataID.ToString() + "']");
        //                                if(nodeStub == null) {
        //                                    nodeStub = ipoXmlLib.addElement(nodeTransaction, "Stubs", "");
        //                                    ipoXmlLib.addAttribute(nodeStub, "DEItemRowDataID", gidDEItemRowDataID.ToString());
        //                                }   // if
        //                                nodeField = nodeStub.SelectSingleNode("Field[@TableName='" + arFormField[3] + "' and @FldName='" + strFieldName + "']");
        //                                if(nodeField == null) {
        //                                    nodeField = ipoXmlLib.addElement(nodeStub, "Field", "");
        //                                    ipoXmlLib.addAttribute(nodeField, "TableName", arFormField[3]);
        //                                    ipoXmlLib.addAttribute(nodeField, "FldName", strFieldName);
        //                                }
        //                                ipoXmlLib.addAttribute(nodeField, "FldData", Request.Form[var]);
        //                                ipoXmlLib.addAttribute(nodeField, "Action", "Update");
                                        
        //                                switch(Request.Form["txtStubFieldDecisionStatus_gbid" + strGlobalBatchID
        //                                              + "_txnid" + strTransactionID
        //                                              + "_deitemrowdataid" + gidDEItemRowDataID
        //                                              + "_" + arFormField[3]
        //                                              + "_" + strFieldName])
        //                                {
        //                                    case "_TXN_":
        //                                        switch(Request.Form["rdoTxnDecisionStatus_" + "gbid" + strGlobalBatchID + "_txnid" + strTransactionID]) {
        //                                            case "Accept":
        //                                                ipoXmlLib.addAttribute(nodeField, "RejectResolution", "Accept");
        //                                                break;
        //                                            case "Reject":
        //                                                ipoXmlLib.addAttribute(nodeField, "RejectResolution", "Reject");
        //                                                break;
        //                                        }
        //                                        break;
        //                                    case "1":
        //                                        ipoXmlLib.addAttribute(nodeField, "RejectResolution", "PendingDecision");
        //                                        break;
        //                                    case "2":
        //                                        ipoXmlLib.addAttribute(nodeField, "RejectResolution", "Accept");
        //                                        break;
        //                                    case "3":
        //                                        ipoXmlLib.addAttribute(nodeField, "RejectResolution", "Reject");
        //                                        break;
        //                                }
                                        
        //                            }   // if
        //                        } 
        //                    }   // if
        //                }   // if                
        //                break;
        //        }   // Select Case
        //    }   // For Each

        //    return(docRetVal);

        //}   // buildUpdateBatchXml

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion
    }
}
