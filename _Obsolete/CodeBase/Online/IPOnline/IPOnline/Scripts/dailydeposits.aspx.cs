using System;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Goetzinger
* Date:     
*
* Purpose:  
*   Displays the appropriate lockbox totals for each lockbox a user
*    has access to view. The totals determine if the processor is using
*    the cutoff feature of Checkbox and pulls the appropriate totals based
*    on the ProcessingDate field in the Systems table. The user also has
*    the ability to retrieve totals for previous processing dates in history
*    by use of the calendar function.
*
* Modification History
* ejg 09/13/02
*    -Converted page to use seperation logic as outlined in VI 3573
* ejg 11/14/2002
*    -Made changes to ValidateForm as a function to provide server-side
*     form validation. VI 4174
*    -Added the JavaScript client-side form validation script. VI 4264
* ejg 11/15/2002
*    -Made change to use 'n/a' instead of '-' for empty/null data. VI 4412
* ejg 01/07/2003
*    -<tr> tag was missing in the lockbox total output.
* ejg 01/24/2003
*    -Made changes to disply Batch Count and Transaction Count. VI 4752
*    -Made changes to title from Daily Deposits to Lockbox Summary. 
*    -Removed the Welcome message and Current Processing Date headings. VI 4600
*    -Removed date validation for dates > Current Date or Current Processing Date.
*    -Added date validation to prompt user if date < 1/1/1900. VI 4254
* ejg 01/31/2003
*    -Removed the warning message when not using cutoff, lockbox is not cutoff, and
*     Deposit Date requested = System Current Processing Date. VI 4893
* ejg 02/7/2003
*    -Added an = sign to the Page.MessageBox to properly assign the property
*     when a user does not have any lockboxes. VI 4953
* CR 4530 jcs 7/10/2003 - Updated to use XSLT to display page data.
* CR 12341 JMC 04/27/2005
*    -Changed job parameter from DepositDate Date/Time to an Xml string
* CR 22114 JMC 11/06/2007
*    -Ported application to .Net from asp.
* CR 46055 JMC 09/13/2011
*    -Added querystring option for 'printable view'.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class dailydeposits : _Page {

        private void Page_Load(object sender, EventArgs e) {

            XmlDocument xmlDoc;
            XmlDocument xmlParms;

            DateTime dteTemp;
            int intTemp;

            DateTime dteDepositDate = DateTime.MinValue;
            bool bolPrintView = false;
            XmlDocument nodeDepositTotals = null;

            // Retrieve input values from form/querystring
            if(CleanRequest.HasFormValue("txtAction")) {
                if(CleanRequest.TryGetFormValue("txtDepositDate", out dteTemp)) {
                    dteDepositDate = dteTemp.Date;
                }
            } else if(CleanRequest.TryGetQueryStringValue("date", out dteTemp)) {
                dteDepositDate = dteTemp.Date;
            } else {
                dteDepositDate = DateTime.Now.Date;
            }

            if(dteDepositDate == DateTime.MinValue) {
                dteDepositDate = DateTime.Now.Date;
                AddMessage(EventType.Error, "The Deposit Date entered is not a valid date. Please enter a Deposit Date in mm/dd/yyyy format.");
            }

            if(CleanRequest.TryGetQueryStringValue("printview", out intTemp)) {
                bolPrintView = (intTemp == 1);
            } else {
                bolPrintView = false;
            }
    
            xmlParms=GetXmlParmsDoc(dteDepositDate.Date, bolPrintView);

            try
            {
                XmlDocumentResponse respdoc = R360CBXServiceClient.GetDepositTotals(xmlParms);
                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    nodeDepositTotals = respdoc.Data;
                }
                //nodeDepositTotals = CBXSrv.GetDepositTotals(xmlParms);
            }
            catch (Exception ex)
            {

                AddMessage(EventType.Error, ex.Message);

                EventLog.logError(ex);
                nodeDepositTotals = null;
            }


            if(nodeDepositTotals == null) {
                xmlDoc = ipoXmlLib.GetXMLDoc("Page");
            } else {
                xmlDoc  = new XmlDocument();
                xmlDoc  = nodeDepositTotals;
            }

            // Add form fields to xml document
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmDeposits", "txtDepositDate", dteDepositDate.ToShortDateString(), "INPUT");

            if(bolPrintView) {
                DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_printview.aspx"));
            } else {
                DisplayXMLDoc(xmlDoc, GetScriptFile());
            }
        }

        private XmlDocument GetXmlParmsDoc(DateTime DepositDate, bool PrintView) {

            XmlDocument xmlParms;
            XmlNode node;
            long lngStartRecord;
            long lngTemp;

            xmlParms = GetRequestDoc("Page");
            ipoXmlLib.addAttribute(xmlParms.DocumentElement, "ActivityLogID", this.ActivityLogID.ToString()); 

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "DepositDate", DepositDate.ToShortDateString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "PaginateRS", (PrintView ? "0" : "1"));

            // Start Record
            if(PrintView) {
                lngStartRecord = 1;
            } else if((Request.Form["txtStart"] != null) && long.TryParse(Request.Form["txtStart"], out lngTemp) && (Request.Form["txtAction"].ToLower() != "search")) {
                lngStartRecord = lngTemp;
            } else {
                lngStartRecord = 1;
            }
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "StartRecord", lngStartRecord.ToString());

            return(xmlParms);
        }


        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            base.OnInit(e);
        }
        #endregion
    }
}