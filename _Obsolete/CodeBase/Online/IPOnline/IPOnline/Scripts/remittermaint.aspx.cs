using System;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
' Module:           Maintainence of remitter information file
'
' Filename:         remittermaint.aspx
'
' Author:           Eric Goetzinger
'
' Description:
'
'   Allows user to add and maintain remitter information based off
'   of which mode the user is requesting the page in.  Page will also
'	display a check image if(exists for the particular record being updated.
'
' Revisions:
'   3/28/01 - ejg
'   Added querystring of search criteria to return to remitsearch.asp
'	with the criteria the user has selected.
'	4/13/01 - ejg
'	Added querystring to return back to remitdisplay processcing date
'	and recstart for remitdisplay, remitsearch, and viewremiter.
'	5/7/01 - ejg
'	Removed JavaScript  isnumeric check on routing and account number.
'	Added server side check to validate routing and account numbers to 
'	a regular expression and routing numbers to pass check digit validation
'	 5/21/01 - ejg
'	Added Netscape plug-in of Pegasus tool for image viewing.
'	5/29/01 - ejg
'	Moved </form> and button collection before image viewing functionality
'	to prevent the form from being unintentionally submitted in IE.
'	6/26/01 - ejg
'	Added permission check for mode level access to page functions.
'	09/07/01 - jcs
'	Corrected account number validation to allow spaces, mutiple dashes.
'	12/04/01 - ejg
'	Correced issue with not letting user return after entering remitter info  if
'	the information already exists in the global remitter file. Also corrected issue
'	with getting the image file in this situation if(exists.
'	6/26/02 - ejg
'	Rebuilt page to take advantage of Page object for variables and use of 
'	stored procedures for VI 3392.
'	7/24/02 - ejg
'	Changed the image viewer link to imagepdf.asp. VI 3722
'   03.10.2003 - Joel Caples
'   Updated remitter functionality for CBO 04.06
'CR 5453 EJG 12/17/2003 - Added support for user emulation to prevent changes
'                         from being made when in this mode.
'                       - Changed the Cancel button in 'modify' mode to call the
'                         viewallremitters.asp page with the start record instead
'                         of using javascript history. This was necessary for emulation
'                         mode to work correctly.
'CR 8478 EJG 08/26/2004 - Added missing calls to CleanVariable() on hidden form fields to prevent
'					 against cross-site scripting attacks. if(an invalid mode is provided, a warning is dispalyed.
'CR 9625 JMC 10/01/2004
'    -Modified Lockbox dropdown to key on OLLockboxID
'    -Modified call to CheckLockboxPermission to send OLLockboxID
'CR 10537 JMC 03/03/2005
'    -Modified page to call SystemSrv.UpdateActivityLockbox function.
'CR 15918 EJG 03/03/2006 - Added Page Mode check for access to page in specified mode.
'CR 16007 EJG 04/17/2006 - Modified page to support standard paging values for XSLT when in modify mode.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.

' WI 90121 WJS 3/4/2013
'   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 149727 TWE 06/23/2014
*    change key for clockbox object to olworkgroupsid
'*****************************************************************/
namespace WFS.RecHub.Online {

    //public partial class remittermaint : _Page {

    //    private string _DisplayMode = string.Empty;

    //    private Guid _OLLockboxIDValue = Guid.Empty;

    //    private int _BatchIDValue = -1;
    //    private DateTime _DepositDateValue = DateTime.MinValue;        
    //    private DateTime _ProcDateValue = DateTime.MinValue;
    //    private int _TransactionIDValue = -1;
    //    private int _BatchSequenceValue = -1;

    //    private int _RecStartValue = 1;
    //    private string _ReturnpageValue = string.Empty;

    //    private string _ReturnPageValue = string.Empty;
    //    private string _LongNameValue = string.Empty;

    //    private void Page_Load(object sender, System.EventArgs e) {

    //        bool blnLoadError = false;
    //        cRemitter objRemitter;
    //        cRemitter objTemp = null;
    //        string strErr;

    //        //LoadRemitter(out objRemitter);
    //        GetPageVariables();

    //        //**BEGIN PAGE OUTPUT AND CHECK TO SEE if(FORM SUBMIT
    //        if(CleanRequest.HasFormValue("txtAction") && CleanRequest.HasFormValue("txtDisplayMode")) {
    //            switch(CleanRequest.Form["txtDisplayMode"].ToLower()) {
    //                case "add":
    //                    ValidateForm(objRemitter);
    //                    //Do not allow insert if(in emulation mode
    //                    if(Errors.Count == 0 && !AppSession.IsEmulationSession) {
    //                        //Check if(remitter exists
    //                        if(RemitterSrv.LockboxRemitterExists(objRemitter.RoutingNumber,
    //                                                        objRemitter.Account,
    //                                                        _OLLockboxIDValue)) {
    //                            objTemp = RemitterSrv.GetLockboxRemitter(objRemitter.RoutingNumber,
    //                                                                objRemitter.Account,
    //                                                                _OLLockboxIDValue);
    //                            if(objTemp.LockboxRemitterName.Length > 0) {
    //                                //Prompt user that remitter exists and stop execution
    //                                strErr =  "A payer record already exists for Routing Number " +
    //                                          objRemitter.RoutingNumber +
    //                                          ", Account Number " +
    //                                          objRemitter.Account + ".";
    //                                //if(SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", "modify")) 
    //                                //{ 
    //                                //    strErr += " <a href=\"remittermaint.aspx?mode=modify&id=" +
    //                                //              Server.UrlEncode(objTemp.LockboxRemitterID.ToString()) +
    //                                //              "\">Click Here</a> to modify the existing record.";
    //                                //}
    //                                AddMessage(EventType.Error, strErr);
    //                            } else {
    //                                // Insert the remitter record
    //                                CreateRemitter(objRemitter.RoutingNumber
    //                                              ,objRemitter.Account
    //                                              ,_OLLockboxIDValue
    //                                              ,objRemitter.LockboxRemitterName);
    //                            }
    //                        } else {
    //                            //Insert the remitter record
    //                            CreateRemitter(objRemitter.RoutingNumber,
    //                                           objRemitter.Account,
    //                                           _OLLockboxIDValue,
    //                                           objRemitter.LockboxRemitterName);
    //                        }
    //                    }
    //                    break;
    //                case "edit":
    //                    ValidateForm(objRemitter);
    //                    // Do not allow edit if(in emulation mode
    //                    if(Errors.Count == 0 && !AppSession.IsEmulationSession) {
    //                        // Update Checks.RemitterName for the given item
    //                        if(RemitterSrv.UpdateChecksRemitter(objRemitter.LockboxRemitterName,
    //                                                            objRemitter.BankID,
    //                                                            objRemitter.LockboxID,
    //                                                            _BatchIDValue,
    //                                                            _DepositDateValue,
    //                                                            _BatchSequenceValue,
    //                                                            _TransactionIDValue)) {

    //                            if(objRemitter.LockboxRemitterID != Guid.Empty) { 
    //                                RemitterSrv.UpdateLockboxRemitter(objRemitter.LockboxRemitterID, 
    //                                                             objRemitter.LockboxRemitterName);
    //                            }

    //                            switch(_ReturnPageValue.ToLower()) {
    //                            // Return to the previous page and record count
    //                                case "display":
    //                                    Response.Redirect("remitdisplay.aspx" +
    //                                                      "?lckbx=" + _OLLockboxIDValue +
    //                                                      "&desc=" + Server.UrlEncode(_LongNameValue) +
    //                                                      "&procdate=" + _ProcDateValue +
    //                                                      "&recstart=" + _RecStartValue);
    //                                    break;
    //                            }
    //                        } else {
    //                            AddMessage(EventType.Error, "The item was not updated. Please verify the information and try again.");
    //                        }
    //                    }
    //                    break;
    //                case "modify":
    //                    ValidateForm(objRemitter);
    //                    // Do not allow edit if(in emulation mode
    //                    if(Errors.Count == 0 && !AppSession.IsEmulationSession ) {
    //                        // Update the LockboxRemitter record
    //                        if(RemitterSrv.UpdateLockboxRemitter(objRemitter.LockboxRemitterID,
    //                                                             objRemitter.LockboxRemitterName)) {
    //                            Response.Redirect("viewremitter.aspx?start=" + _RecStartValue);
    //                        } else {
    //                            AddMessage(EventType.Error, "The payer record was not updated. Please verify the information and try again.");
    //                        }
    //                   }
    //                    break;
    //                case "cancel":
    //                    Response.Redirect("viewremitter.aspx?start=" + _RecStartValue);
    //                    break;
    //            }
    //        }

    //        // Build message that you are not able to add and modify remitters in emulation mode
    //        if(AppSession.IsEmulationSession) {
    //           AddMessage(EventType.Information, "You are not able to add or modify remitters in emulation mode.");
    //        }

    //        blnLoadError = !InitializeForm(objRemitter);

    //        XmlNode nodePage;
            
    //        if(objRemitter == null) {
    //            RemitterSrv.GetRemitterMaintenancePage(Guid.Empty, out nodePage);
    //        } else {
    //            RemitterSrv.GetRemitterMaintenancePage(objRemitter.LockboxRemitterID, out nodePage);
    //        }
            
    //        XmlDocument docXml = new XmlDocument();
    //        docXml.AppendChild(docXml.ImportNode(nodePage, true));

    //        if(objRemitter != null) {
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "DisplayMode", _DisplayMode, "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "LockboxRemitterID", objRemitter.LockboxRemitterID.ToString(), "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "BankID", objRemitter.BankID.ToString(), "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "LockboxID", objRemitter.LockboxID.ToString(), "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "RoutingNumber", objRemitter.RoutingNumber, "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "Account", objRemitter.Account, "INPUT");
    //            ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "LockboxRemitterName", objRemitter.LockboxRemitterName, "INPUT");
    //        }
    //        ipoXmlLib.AddFormFieldAsNode(docXml.DocumentElement, "frmRemitter", "RecStartValue", _RecStartValue.ToString(), "INPUT");

    //        DisplayXMLDoc(docXml, GetScriptFile());
    //    }

    //    private bool InitializeForm(cRemitter objRemitter) {
	    
    //        bool blnLoadError = false;
	    
    //        if(_DisplayMode.Length == 0) {
    //            if(CleanRequest.HasFormValue("txtDisplayMode")) {
    //                _DisplayMode = CleanRequest.Form["txtDisplayMode"].ToLower();
    //            } else if(CleanRequest.HasQueryStringValue("mode")) {
    //                _DisplayMode = CleanRequest.QueryString["mode"].ToLower();
    //            }
    //        }
    			
    //        if(_DisplayMode.Length == 0) { 
    //            _DisplayMode = "add";
    //        }

    //        // CR 15918 EJG 03/03/2006 - Added Page Mode check for access to page in specified mode.		
    //        // validate permissions to page mode
    //        switch(_DisplayMode.ToLower()) {
    //            case "add":
    //                //if(!SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", "add")) {
    //                //    Response.Redirect("accessdenied.aspx");
    //                //}
    //                break;
    //            case "confirm":		//add and confirm mode are the same permission, add
    //                //if(!SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", "add")) {
    //                //    Response.Redirect("accessdenied.aspx");
    //                //}			
    //                break;
    //            case "modify":
    //                //if(!SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", _DisplayMode)) {
    //                //    Response.Redirect("accessdenied.aspx");
    //                //}
    //                break;
    //            case "edit":
    //                //if(!SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", _DisplayMode)) {
    //                //    Response.Redirect("accessdenied.aspx");
    //                //}
    //                break;
    //            default:					//invalid mode passed
    //                AddMessage(EventType.Warning, "The request could not be completed. Please verify the information and try again.");
    //                blnLoadError = true;
    //                break;
    //        }
    		
    //        return(!blnLoadError);
    //    }
    	
    //    /// <summary>
    //    /// Validate the form fields based off of page mode.
    //    /// </summary>
    //    /// <param name="objRemitter"></param>
    //    private void ValidateForm(cRemitter objRemitter) {

    //        string strFailureReason;
    		
    //        if(CleanRequest.HasFormValue("txtDisplayMode") && CleanRequest.Form["txtDisplayMode"].ToLower() == "add") {

    //            // Verify Lockbox Selected
    //            if(objRemitter.LockboxID <= 0) {
    //                AddMessage(EventType.Error, "Lockbox is required.");
    //            }
			    
    //            // Validate Routing Number
    //            if(!ValidationLib.ValidateRT(objRemitter.RoutingNumber, out strFailureReason)) {
    //                AddMessage(EventType.Error, strFailureReason);
    //            }

    //            // Validate Account Number
    //            if(!ValidationLib.ValidateAccount(objRemitter.Account, out strFailureReason)) {
    //                AddMessage(EventType.Error, strFailureReason);
    //            }
    //        }		
    		
    //        // Validate Remitter Name
    //        if(objRemitter.LockboxRemitterName.Length == 0) {
    //            AddMessage(EventType.Error, "Payer is a required field.");
    //        }
    //    }

    //    /// <summary>
    //    /// Load a reference to a cRemitter object.
    //    /// </summary>
    //    /// <returns>Reference to a cRemitter object.</returns>
    //    private bool LoadRemitter(out cRemitter Remitter) {

    //        cRemitter objRemitter = null;
    //        XmlNode nodeLockboxes;
    //        cOLLockboxes objLockboxes;
    //        bool blnLoadError = false;
    //        Guid gidLockboxRemitterID;
    //        Guid gidOLLockboxID;
    //        int intOLWorkGroupsID;

    //        if(CleanRequest.HasFormValue("txtAction")) {

    //            if(CleanRequest.HasFormValue("cboOLLockboxes")) {

    //                objRemitter = new cRemitter();
    //                nodeLockboxes = SystemSrv.GetUserLockboxes(AppRaamUser.UserID);
    //                objLockboxes = new cOLLockboxes();
    //                objLockboxes.FromXMLNode(nodeLockboxes);

    //                gidOLLockboxID = CleanRequest.GetFormValue("cboOLLockboxes", Guid.Empty);
    //                intOLWorkGroupsID = CleanRequest.GetFormValue("cboOLWorkGroups",-1);

    //                objRemitter.BankID = objLockboxes[intOLWorkGroupsID].BankID;
    //                objRemitter.LockboxID = objLockboxes[intOLWorkGroupsID].LockboxID;

    //                objLockboxes = null;
    //            } else {
                
    //                gidLockboxRemitterID = CleanRequest.GetQueryStringValue("id", Guid.Empty);
                
    //                if(gidLockboxRemitterID == Guid.Empty || !RemitterSrv.GetLockboxRemitterByID(gidLockboxRemitterID, out objRemitter)) {
    //                    AddMessage(EventType.Error, "The payer record could not be loaded. Please <a href=\"#\" onClick=\"history.back();\">select</a> another payer and try again.");
    //                    objRemitter = new cRemitter();
    //                    blnLoadError = true;
    //                }
    //            }

    //            objRemitter.LockboxRemitterID = CleanRequest.GetFormValue("txtOLLockboxRemitterID", Guid.Empty);
    //            objRemitter.RoutingNumber = CleanRequest.GetFormValue("txtRoutingNumber", string.Empty);
    //            objRemitter.Account = CleanRequest.GetFormValue("txtAccount", string.Empty);
    //            objRemitter.LockboxRemitterName = CleanRequest.GetFormValue("txtRemitterName", string.Empty);

    //        } else if(CleanRequest.HasQueryStringValue("id") && (CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "modify" || CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "confirm")) {
    //            //Modify/Confirm Mode and ID value in QueryString
    //            try {
			    
    //                gidLockboxRemitterID = CleanRequest.GetQueryStringValue("id", Guid.Empty);
			    
    //                if(gidLockboxRemitterID == Guid.Empty || !RemitterSrv.GetLockboxRemitterByID(gidLockboxRemitterID, out objRemitter)) {
    //                    AddMessage(EventType.Error, "The payer record could not be loaded. Please <a href=\"#\" onClick=\"history.back();\">select</a> another payer and try again.");
    //                    objRemitter = new cRemitter();
    //                    blnLoadError = true;
    //                }
    //            } catch(Exception ex) {
    //                AddMessage(EventType.Error, ex.Message);
    //            }
    //        } else if(CleanRequest.GetQueryStringValue("id", string.Empty).Length <= 0 && (CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "modify" || CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "confirm")) {
    //            //Modify/Confirm Mode and NO ID value in QueryString
    //            AddMessage(EventType.Error, "No payer record was selected. Please <a href=\"#\" onClick=\"history.back();\">select</a> a payer and try again.");
    //            objRemitter = new cRemitter();
    //            blnLoadError = true;
    //        } else if((CleanRequest.HasQueryStringValue(Constants.QS_PARM_RT) || CleanRequest.HasQueryStringValue(Constants.QS_PARM_ACCOUNT) || CleanRequest.HasQueryStringValue(Constants.QS_PARM_OLLOCKBOXID)) && CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "edit") {
    //            //Edit Mode and RoutingNumber, Account, LockboxID values in QueryString
    //            objRemitter = RemitterSrv.GetLockboxRemitter(CleanRequest.GetQueryStringValue(Constants.QS_PARM_RT, string.Empty),
    //                                                    CleanRequest.GetQueryStringValue(Constants.QS_PARM_ACCOUNT, string.Empty),
    //                                                    CleanRequest.GetQueryStringValue(Constants.QS_PARM_OLLOCKBOXID, Guid.Empty));
    //            if(objRemitter == null) {
    //                objRemitter = new cRemitter();
    //                objRemitter.RoutingNumber = CleanRequest.GetQueryStringValue(Constants.QS_PARM_RT, string.Empty);
    //                objRemitter.Account = CleanRequest.GetQueryStringValue(Constants.QS_PARM_ACCOUNT, string.Empty);
    //                objRemitter.LockboxID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_OLLOCKBOXID, -1);
    //            } else {
    //                if(objRemitter.LockboxRemitterName.Length <= 0 && objRemitter.GlobalRemitterName.Length > 0) { 
    //                    objRemitter.LockboxRemitterName = objRemitter.GlobalRemitterName;
    //                }
    //            }
    //        } else if((CleanRequest.GetQueryStringValue(Constants.QS_PARM_RT, string.Empty).Length <= 0 || CleanRequest.GetQueryStringValue(Constants.QS_PARM_ACCOUNT, string.Empty).Length <= 0 || CleanRequest.GetQueryStringValue(Constants.QS_PARM_OLLOCKBOXID, string.Empty).Length <= 0) && CleanRequest.GetQueryStringValue("mode", string.Empty).ToLower() == "edit") {			
    //            AddMessage(EventType.Error, "Unable to add a payer to the item selected. Information was missing or invalid. Please <a href=\"#\" onClick=\"history.back();\">select</a> another item and try again.");
    //            objRemitter = new cRemitter();
    //            blnLoadError = true;
    //        } else {
    //            objRemitter = new cRemitter();
    //        }

    //        //If: No Error occurred AND either the BankID or the LockboxID or both have 
    //        //    been set from their default values(0).
    //        if((objRemitter != null) && ((!blnLoadError) && ((objRemitter.BankID != 0) || (objRemitter.LockboxID != 0)))) {
    //            SystemSrv.UpdateActivityLockbox(this.ActivityLogID, 
    //                                            objRemitter.BankID, 
    //                                            objRemitter.LockboxID);
    //        }
    		
    //        Remitter = objRemitter;
  		    
    //        return(!blnLoadError);
    //    }
    	
    //    /// <summary>
    //    /// Set various page variables for processing and page return logic
    //    /// </summary>
    //    private void GetPageVariables() {
    //        if(CleanRequest.HasFormValue("txtAction")) {    // Request.Form["txtAction"].Length > 0) {
    //            _RecStartValue = CleanRequest.GetFormValue("txtRecstart", 1);
    //            _ReturnpageValue = CleanRequest.GetFormValue("txtReturnPage", string.Empty);
    //            _OLLockboxIDValue = CleanRequest.GetFormValue("cboOLLockboxes", Guid.Empty);
    //        } else {
    //            _RecStartValue = CleanRequest.GetQueryStringValue("start", 1);
    //            _ReturnpageValue = CleanRequest.GetQueryStringValue("return", string.Empty);
    //            _OLLockboxIDValue = CleanRequest.GetQueryStringValue(Constants.QS_PARM_OLLOCKBOXID, Guid.Empty);
    //        }
    //    }

    //    private void CreateRemitter(string RoutingNumber,
    //                                string AccountNumber,
    //                                Guid OLLockboxID,
    //                                string LockboxRemitterName) {

    //        Guid gidLockBoxRemitterID;

    //        try {

    //            gidLockBoxRemitterID = RemitterSrv.InsertLockboxRemitter(RoutingNumber,
    //                                                                     AccountNumber,
    //                                                                     OLLockboxID,
    //                                                                     LockboxRemitterName);

    //            if(gidLockBoxRemitterID != Guid.Empty) {
    //                Response.Redirect("remittermaint.aspx?mode=confirm&id=" + gidLockBoxRemitterID.ToString());
    //            } else {
    //                AddMessage(EventType.Error, "Payer not created.");
    //            }

    //        } catch(Exception ex) {
    //            AddMessage(EventType.Error, ex.Message);
    //        }					
    //    }

    //    #region Web Form Designer generated code
    //    override protected void OnInit(EventArgs e)
    //    {
    //        //
    //        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
    //        //
    //        //InitializeComponent();
    //        base.OnInit(e);
    //    }

    //    #endregion
    //}
}