using System;
using System.Data;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/19/2010
*
* Purpose:  Creates a request to retrieve all search results as XML
*           and applies the appropriate XSL stylesheet for print view
*           or download as comma seperated.
*
* Modification History
* CR 5041 EJG 9/2/2003
*     - Added Check Amount Range and Serial Number to the search criteria.
* CR 5531 EJG 10/7/2003
*     - Modified GetParmFields() to use the current iteration for the dictionary
*       index instead of the TablName, FldName, ReportTitle to allow for the
*       same field to be used more than once for criteria.
* CR 5532 EJG 10/22/2003
*     - Added the search criteria to the returned xml document to be displayed.
* CR 5469 JMC 11/04/2003
*     - Added Batch range to replace single Batch lookup.
* CR 4819 JMC 11/26/2003
*     - Modified page to utilize service for "Print View as Pdf" and
*       "Download Images".
* CR 7017 JMC 03/17/2004
*     - Added Check Count and Check Total fields to Xml requests.  Consolidated
*       this functionality into a common function (AppendSearchCriteriaNodes)
* CR 9557 JMC 09/28/2004
*     - Replaced LockboxID with OLLOckboxId.
* CR11245 JMC 01/12/2005
*     - Changed Content Header to text/plain when outputing a text file.
* CR 11337 JMC 01/18/2005
*     - Added cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED case
*     - Added requestViewSelectedJob function.
*     - Added StartsWith helper function.
* CR 10537 JMC 03/03/2005
*     - Added reference to cLockbox.asp include file in order to retreive
*       the current Lockbox to properly log "Print View" and "Download Text"
*       Activities.
*     - Modified downloadFile function to call SystemSrv.SetWebDeliveredFlag
*       function.
* CR 11825 JMC 03/24/2005
*     - Modified call to CreateSearchResultsJobRequest to pass both the search parameters
*       and the job request parameters as seperate Xml documents.
* CR 12289 JMC 04/25/2005
*     - Modified requestJob function to call AppendSearchCriteriaNodes to the Search
*       Parameter document.
* CR 12500 JMC 05/09/2005
*     - Modified call to CreateViewSelectedJobRequest to not pass in Xml Search Results.
* CR 12491 JMC 05/09/2005
*     - Added javascript to supress status message to downloadFile and BuildHTML functions.
* CR 12215 JMC 05/23/2005
*     - Added a reference to LockboxSearchUtil.asp include file.
*     - Removed SelectFields and ParmFields collections. (moved to LockboxSearchUtil.asp).
*     - Modified call to SetXmlNodes to pass in the parent node instead of the document.
*     - Added new parameters to call to SetXmlNodes (Paginate=false, AppendImageSizes=false)
*     - Removed AppendSearchCriteriaNodes function (moved to server)
*     - Removed getXMLSearchResults function.
*     - Removed SetXMLNodes, GetSelectFields, GetParmFields, ParseParameterField functions
*       (moved to LockboxSearchUtil.asp)
* CR 15161 JMC 03/01/2006
*     - Added include file "ipoJobRequestUtil.aspx"
*     - Modified calls to "CheckRequestStatus" to pass 0 parameters.
*     - Modified calls to "requestJob" and "requestViewSelectedJob" to accept a recordset as a
*       return type instead of a string.
*     - Removed "downloadFile", "CheckRequestStatus" and "BuildHTML" functions.
*     - Modified call to CreateSearchResultsJobRequest to accept a Recordset instead of a
*       string.
* 05/20/2010 JMC CR 29405
*     - Now passing in jobType to SetXMLNodes to prevent pagination when requesting
*       print-views, pdf-views, and zip-files.
* 07/21/2011 JNE CR 31753
*     - Added SetLockboxFields, SetLockboxDependent Fields
*      to replace SetFields (optimization)
* CR 30342 JCS 07/11/2011
*     - Set return content type for 'Download as Text' to "text/csv"
* CR 53947 JNE 07/18/2012
*     - Add checkImageDisplayMode & checkImageDisplayMode to JobRequestOptions xml in requestJob fn.
*     - Added GetImageDisplayMode that returns enumvalue of values found in searchparms xml.
* CR 54169 JNE 08/08/2012
*     - requestJob - Added displayBatchID to xml
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 90170 WJS 3/4/2013
*  - FP: Need to pass in batch number. Add log message to ensure correct number of items were passed in
* WI 96311 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 90076 DRP 06/18/2013
*    - Add PaymentSource and PaymentType to Online lockbox/account search forms.
* WI 152307 TWE 07/08/2014
*    fix advance search because of changes to BatchID and RAAM
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
*******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class lockboxsearch_alternate : _LockboxSearchBase {

        //private const string NOIMAGE = "../Styles/Online/noimage.aspx";

        private const int OLQ_STATUS_PENDING = 1;
        private const int OLQ_STATUS_PROCESSING = 2;
        private const int OLQ_STATUS_COMPLETED = 3;
        private const int OLQ_STATUS_ERROR = 4;

        private void Page_Load(object sender, EventArgs e) {

            string strJobType;
            DataTable dt;
            bool bClearFields = false;

            if(Request.QueryString["ID"] != null && Request.QueryString["ID"].Length > 0) {
                CheckRequestStatus(OLFDeliveryMethod.Stream);
            } else {

                SetLockboxFields(bClearFields);
                SetLockboxDependentFields(bClearFields);

                if(CleanRequest.QueryString["JOB_TYPE"] == null) {
                    strJobType = string.Empty;
                } else {
                    strJobType = CleanRequest.QueryString["JOB_TYPE"];
                }

                if(strJobType.Length > 0) {
                    switch(strJobType) {
                        case Constants.cIPO_IMAGE_JOB_TYPE_ZIP: // .cIPO_IMAGE_JOB_TYPE_ZIP:
                            dt = requestJob(strJobType);
                            ProcessJobRequest(dt, OLFDeliveryMethod.Download);
                            break;
                        case Constants.cIPO_IMAGE_JOB_TYPE_PRINT_PDF:
                            dt = requestJob(strJobType);
                            ProcessJobRequest(dt, OLFDeliveryMethod.Stream);
                            break;
                        case Constants.cIPO_IMAGE_JOB_TYPE_CSV:
                            TransformSearchResults(strJobType);
                            break;
                        case Constants.cIPO_IMAGE_JOB_TYPE_PRINT_HTML:
                            TransformSearchResults(strJobType);
                            break;
                        case Constants.cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED:
                            dt = requestViewSelectedJob();
                            ProcessJobRequest(dt, OLFDeliveryMethod.Stream);
                            break;
                        default:
                            AddMessage(EventType.Error, "Invalid Job Type : " + strJobType);
                            break;
                    }
                } else {
                    AddMessage(EventType.Error, "No Job Type specified.");
                }
            }
        }

        private void TransformSearchResults(string jobType) {

            XmlDocument objXML;						//XML document reference for form fields
            cOLLockbox objLockbox = null;
            int lngItemCount;
            XmlNode nodeSearchParms;
            XmlDocument nodeResults = null;
            Guid gidTemp;
            int intTemp;
            int bankId;
            int lockBoxId;
            //script timeout to 15 minutes
            Server.ScriptTimeout = 900;

            //Create a base XML document
            objXML = GetRequestDoc();

            //Add the form data as nodes to the XML document
            SetXMLNodes(objXML.DocumentElement, jobType, false, false);

            //Submit the request
            XmlDocumentResponse respdoc = R360SearchServiceClient.LockBoxSearchASXML(objXML);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeResults = respdoc.Data;
            }
            //nodeResults = SearchSrv.LockBoxSearchASXML(objXML);
            XmlDocument xmlDoc = nodeResults;
            AbortIfNoXml(xmlDoc, "Lockbox search");

            nodeSearchParms=ipoXmlLib.addElement(xmlDoc.DocumentElement, "SearchParms", "");

            //Add the form data as nodes to the XML document
            SetXMLNodes(nodeSearchParms, jobType, false, false);
            //we dont need to call the service to get the lockboxId and bankID
            int.TryParse(xmlDoc.DocumentElement.SelectSingleNode(".//node()[@ParmName='ClientAccountID']").InnerText, out lockBoxId);
            int.TryParse(xmlDoc.DocumentElement.SelectSingleNode(".//node()[@ParmName='BankID']").InnerText, out bankId);
            lngItemCount = xmlDoc.DocumentElement.SelectSingleNode("Recordset[@Name='Results']").ChildNodes.Count;
            AppendWorkgroupData(xmlDoc, GetField("WorkgroupSelection"));

            switch(jobType) {
                case Constants.cIPO_IMAGE_JOB_TYPE_PRINT_HTML:

                    R360SystemServiceClient.LogActivity(ActivityCodes.acSearchResultsPrintView,
                        Guid.Empty,
                        lngItemCount, bankId, lockBoxId);

                    //SystemSrv.LogActivity(ActivityCodes.acSearchResultsPrintView, Guid.Empty, lngItemCount, objLockbox.BankID, objLockbox.LockboxID);

                    Response.ContentType = "text/html";
                    DisplayXMLDoc(xmlDoc, "search_results_print.aspx");
                    break;
                case Constants.cIPO_IMAGE_JOB_TYPE_CSV:
                    R360SystemServiceClient.LogActivity(ActivityCodes.acSearchResultsCsv,
                        Guid.Empty,
                        lngItemCount, bankId, lockBoxId);
                    //SystemSrv.LogActivity(ActivityCodes.acSearchResultsCsv, Guid.Empty, lngItemCount, objLockbox.BankID, objLockbox.LockboxID);

                    Response.ContentType = "text/csv";
                    Response.AddHeader("content-disposition", "attachment; filename=search_results.csv");
                    DisplayXMLDoc(xmlDoc, "search_results_download.aspx");
                    break;
                default:
                    Response.ContentType = "text/plain";
                    break;
            }
        }

        private DataTable requestJob(string jobType) {

            XmlDocument objXmlSearchParms;
            XmlDocument objXmlJobParms;
            DataTable RST;
            XmlNode nodeJobRequestOptions;
            XmlNode nodeOutputs;
            XmlNode nodeOutput;
            XmlNode nodeOutputOptions;
            //Guid gidOLLockboxID;
            int intOLWorkGroupsID;
            Guid gidTemp;
            int intTemp;
            cOLLockbox objLockbox = null;
            int bankID;
            int accountID;
            //****************************************************
            //Create a base XML document
            objXmlSearchParms = GetRequestDoc();

            //Add the form data as nodes to the XML document
            SetXMLNodes(objXmlSearchParms.DocumentElement, jobType, false, false);


            //****************************************************

            //****************************************************
            //Create a base XML document
            objXmlJobParms = GetRequestDoc();
            //****************************************************
           // Retrieve lockbox and add the displaybatchid to the xml.
            int.TryParse(objXmlSearchParms.DocumentElement.SelectSingleNode("BankID").InnerText, out bankID);
            int.TryParse(objXmlSearchParms.DocumentElement.SelectSingleNode("ClientAccountID").InnerText, out accountID);
            BaseGenericResponse<cOLLockbox> resp = R360SystemServiceClient.GetLockBoxByAccountIDBankID(bankID,accountID);
            if (resp.Status == R360Shared.StatusCode.SUCCESS)
            {
                objLockbox = resp.Data;
            }

            //objLockbox = SystemSrv.GetLockboxByID(intOLWorkGroupsID);
            if (objLockbox != null)
            {
                ipoXmlLib.addElement(objXmlSearchParms.DocumentElement, Constants.XML_ELE_DISPLAYBATCHID, objLockbox.DisplayBatchID.ToString());
            }

            switch(jobType) {

                case Constants.cIPO_IMAGE_JOB_TYPE_ZIP:

                    nodeJobRequestOptions=ipoXmlLib.addElement(objXmlJobParms.DocumentElement, Constants.XML_ELE_JOB_REQ_OPTIONS, "");
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, Constants.XML_ATTR_RETURN_TYPE, Constants.cIPO_IMAGE_JOB_TYPE_ZIP);
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, Constants.XML_ATTR_CHECK_DISPLAY_MODE, ((int)GetImageDisplayMode(objXmlSearchParms, "check")).ToString());
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, Constants.XML_ATTR_DOCUMENT_DISPLAY_MODE, ((int)GetImageDisplayMode(objXmlSearchParms, "document")).ToString());

                    nodeOutputs=ipoXmlLib.addElement(nodeJobRequestOptions, Constants.XML_ELE_OUTPUTS, "");

                    //****** CSV ************
                    nodeOutput=ipoXmlLib.addElement(nodeOutputs, Constants.XML_ELE_OUTPUT, "");
                    ipoXmlLib.addAttribute(nodeOutput, Constants.XML_ATTR_OUTPUT_FORMAT, "TEXT");
                    nodeOutputOptions=ipoXmlLib.addElement(nodeOutput, Constants.XML_ELE_TEXT_OPTIONS, "");
                    ipoXmlLib.addAttribute(nodeOutputOptions, Constants.XML_ATTR_TEXT_FILE_EXT, Constants.FILE_EXT_CSV);

                    //****** Html ************
                    nodeOutput=ipoXmlLib.addElement(nodeOutputs, Constants.XML_ELE_OUTPUT, "");
                    ipoXmlLib.addAttribute(nodeOutput, Constants.XML_ATTR_OUTPUT_FORMAT, "HTML");

                    //****** Xml ************
                    nodeOutput=ipoXmlLib.addElement(nodeOutputs, Constants.XML_ELE_OUTPUT, "");
                    ipoXmlLib.addAttribute(nodeOutput, Constants.XML_ATTR_OUTPUT_FORMAT, "XML");

                    break;
                case Constants.cIPO_IMAGE_JOB_TYPE_PRINT_PDF:

                    nodeJobRequestOptions=ipoXmlLib.addElement(objXmlJobParms.DocumentElement, Constants.XML_ELE_JOB_REQ_OPTIONS, "");
                    ipoXmlLib.addAttribute(nodeJobRequestOptions, Constants.XML_ATTR_RETURN_TYPE, Constants.cIPO_IMAGE_JOB_TYPE_PRINT_PDF);

                    break;
                default:

                    AddMessage(EventType.Error, "Invalid Job Type : " + jobType);

                    return null;
            }

            if(!(objXmlJobParms == null || objXmlSearchParms == null))
            {
                RST = null;
                BaseGenericResponse<DataSet> respdoc = R360ImageServiceClient.CreateSearchResultsJobRequest(objXmlSearchParms, objXmlJobParms);
                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    RST = respdoc.Data.Tables[0];
                }
                //RST = ImageSrv.CreateSearchResultsJobRequest(objXmlSearchParms, objXmlJobParms).Tables[0];
                objXmlJobParms = null;
            }
            else
            {
                RST = null;
                AddMessage(EventType.Error, "Xml not available.");
            }

            objXmlJobParms = null;
            objXmlSearchParms = null;

            return(RST);
        }

        private OLFImageDisplayMode GetImageDisplayMode(XmlDocument xmlSearchParms, string strImageType){
            OLFImageDisplayMode enmRetVal;
            int intTemp;

            try{
                enmRetVal = OLFImageDisplayMode.Undefined;
                switch (strImageType.ToLower()){
                    case "check":
                        if (int.TryParse(xmlSearchParms.DocumentElement.SelectSingleNode("CheckImageDisplayMode").InnerText,out intTemp)){
                            enmRetVal = ((OLFImageDisplayMode)(byte)intTemp);
                        }
                        break;
                    case "document":
                        if (int.TryParse(xmlSearchParms.DocumentElement.SelectSingleNode("DocumentImageDisplayMode").InnerText,out intTemp)){
                            enmRetVal = ((OLFImageDisplayMode)(byte)intTemp);
                        }
                        break;
                }
            }catch (Exception){
                enmRetVal = OLFImageDisplayMode.Undefined;
            }
            return enmRetVal;
        }

        private DataTable requestViewSelectedJob() {

            XmlDocument objXml;
            DataTable RST;
            XmlNode nodeJobRequestOptions;

            XmlNode nodeSelectedItems;
            XmlNode nodeSelectedItem;
            string[] arItemParts;

            objXml = GetRequestDoc();

            nodeSelectedItems = ipoXmlLib.addElement(objXml.DocumentElement, "SelectedItems", "");
            ipoXmlLib.addAttribute(nodeSelectedItems, "ImageFilterOption", Request.Form["rdoSelectivePrintImageOption"]);
            foreach(string fld in Request.Form) {
                if(fld.StartsWith("chkSelectivePrint~")) {
                    nodeSelectedItem = ipoXmlLib.addElement(nodeSelectedItems, "SelectedItem", "");

                    arItemParts = fld.Split('~');
                    if (arItemParts.Length == 7)
                    {
                    ipoXmlLib.addAttribute(nodeSelectedItem, "BankID", arItemParts[1]);
                        ipoXmlLib.addAttribute(nodeSelectedItem, "ClientAccountID", arItemParts[2]);
                    ipoXmlLib.addAttribute(nodeSelectedItem, "BatchID", arItemParts[3]);
                    ipoXmlLib.addAttribute(nodeSelectedItem, "DepositDate", arItemParts[4]);
                    ipoXmlLib.addAttribute(nodeSelectedItem, "TransactionID", arItemParts[5]);
                        ipoXmlLib.addAttribute(nodeSelectedItem, "BatchNumber", arItemParts[6]);
                    }
                    else
                    {
                        AddMessage(EventType.Error, "Wrong number of items are passed in view selected job. It should be seven.");
                    }
                    nodeSelectedItem = null;
                }
            }

            nodeSelectedItems = null;

            nodeJobRequestOptions=ipoXmlLib.addElement(objXml.DocumentElement, Constants.XML_ELE_JOB_REQ_OPTIONS, "");
            ipoXmlLib.addAttribute(nodeJobRequestOptions, Constants.XML_ATTR_RETURN_TYPE, Constants.cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED);

            if(objXml != null)
            {
                RST = null;
                BaseGenericResponse<DataSet> respdoc = R360ImageServiceClient.CreateViewSelectedJobRequest(objXml);
                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    RST = respdoc.Data.Tables[0];
                }
                //RST = ImageSrv.CreateViewSelectedJobRequest(objXml).Tables[0];
                objXml = null;
            }
            else
            {
                RST = null;
                AddMessage(EventType.Error, "Xml not available.");
            }

            return(RST);
        }
    }
}
