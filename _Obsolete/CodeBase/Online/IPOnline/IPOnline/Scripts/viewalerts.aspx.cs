using System;
using System.Xml;


/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     
*
* Purpose:  
*
* Modification History
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class viewalerts : _Page {

        //private void Page_Load(object sender, System.EventArgs e) {

        //    XmlNode nodeResults;
        //    XmlDocument xmlDoc;
        //    int intStartRecord;
        //    int intTemp;

        //    //if(!SystemSrv.CheckUserPageModePermissions("alertmaint.aspx", "view")) { 
        //    //    Response.Redirect("accessdenied.aspx");
        //    //}

        //    // Retrieve input values from form/querystring
        //    if(CleanRequest.Form["txtAction"] != null && CleanRequest.Form["txtAction"].Length > 0 && int.TryParse(CleanRequest.Form["txtStartRecord"], out intTemp)) {
        //        intStartRecord = intTemp;
        //    } else if(CleanRequest.QueryString["start"] != null && int.TryParse(CleanRequest.QueryString["start"], out intTemp)) {
        //        intStartRecord = intTemp;
        //    } else {
        //        intStartRecord = 1;
        //    }

        //    nodeResults = AlertSrv.GetAllOnlineAlerts(intStartRecord);
        //    xmlDoc = new XmlDocument();
        //    xmlDoc.AppendChild(xmlDoc.ImportNode(nodeResults, true));

        //    DisplayXMLDoc(xmlDoc, GetScriptFile());
        //}

        //#region Web Form Designer generated code
        //override protected void OnInit(EventArgs e)
        //{
        //    //
        //    // CODEGEN: This call is required by the ASP.NET Web Form Designer.
        //    //
        //    //InitializeComponent();
        //    base.OnInit(e);
        //}

        //#endregion
    }
}
