using System;
using System.Xml;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
' Module:           Search Database For Remittance Information
'
' Filename:         viewremitter.asp
'
' Author:           Eric Goetzinger
'
' Description:
'	Allow users to view all remitter in the global remitter table
'	and enable the ability to modify the remitter information based
'	on permissions.
'	
' Revisions:
'	4/13/01 - ejg
'	Added URLEncode for Netscape.
'	6/26/01 - ejg
'	Added permission check for modify and download.
'	6/26/02 - ejg
'	Changed the display to show remitters by lockbox.
'	7/24/02 - ejg
'	Corrected NextStart issue for paging records.
'	7/24/02 - ejg
'	Corrected NextStart issue for paging records. VI 3721
'	7/29/02 - ejg
'	Made change to work with netscape regarding page links by placing URL in Next link.
'   03.10.2003 - Joel Caples
'   Updated remitter functionality for CBO 04.06
'   03/17/2003 - Joel Caples
'   VI 4550 - Fixed bug in ViewAllRemitters.  When user has no lockbox permissions, an error would occur.
'CR 5453 EJG 12/17/2003 - Added support for new Error, Message, and Warning objects for
'                         emulation mode.
'                       - Modified to use QueryString instead of Form for paging because posting the form
'                         will !work in emulation mode
'                       - Added statements to Close and Destroy the reference to the ADO Record(rs)
'CR 8478 EJG 08/26/2004 - Added the CleanVariable() function to the Start Record parameter.
' CR 9626 JMC 10/01/2004
'   -Modified header to omit the Customer designation.
'CR 16007 EJG 04/14/2006 - Convert page to use XSLT
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
'*****************************************************************/
namespace WFS.RecHub.Online {

    public partial class viewremitter : _Page {

        private void Page_Load(object sender, EventArgs e) {

            XmlDocument nodeResults = null;
            XmlDocument xmlDoc;
            int intStartRecord;
            int intTemp;

            // Retrieve input values from form/querystring
            if(Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0 && int.TryParse(CleanRequest.Form["txtStartRecord"], out intTemp)) {
                intStartRecord = intTemp;
            } else if(Request.QueryString["start"] != null && int.TryParse(CleanRequest.QueryString["start"], out intTemp)) {
                intStartRecord = intTemp;
            } else {
                intStartRecord = 1;
            }

            XmlDocumentResponse respdoc = R360RemitterServiceClient.GetAllRemitters(intStartRecord, false);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeResults = respdoc.Data;
            }

            //nodeResults = RemitterSrv.GetAllRemitters(intStartRecord, false);
            xmlDoc = new XmlDocument();
            xmlDoc = nodeResults;

            //AddErrorsToXMLDoc(xmlDoc);

            DisplayXMLDoc(xmlDoc, GetScriptFile());
        }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}

		#endregion
    }
}
