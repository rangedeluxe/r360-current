<%@ Page Language="C#" Inherits="WFS.RecHub.Online._Page" %>
<script runat="server">
/*****************************************************************
' Module:           Internet Deliverable Description Window
'
' Filename:         deliverabledesc.asp
'
' Author:           Eric Goetzinger
'
' Description:
'	
'	Displays the description of the internet deliverable item.
'
' Revisions:
'	CR 12575 EJG 01/04/2006 - Modified the page to use 4.06 architecture and XSLT
*****************************************************************/

private void Page_Load(object sender, System.EventArgs e) {

    System.Xml.XmlDocument xmlDoc;
    System.Xml.XmlNode nodeDeliverable;
    Guid gidTemp;
    Guid gidDeliverableID;

    // Retrieve the deliverableid from the querystring
    if(Request.QueryString["id"] != null && WFS.RecHub.Common.ipoLib.TryParse(Request.QueryString["id"], out gidTemp)) {

        gidDeliverableID = gidTemp;

        // Make the request to the app server
        nodeDeliverable = AlertSrv.GetDeliverableDescription(gidDeliverableID);

        xmlDoc = new System.Xml.XmlDocument();

        xmlDoc.AppendChild(xmlDoc.ImportNode(nodeDeliverable, true));

    } else {
        xmlDoc = WFS.RecHub.Common.ipoXmlLib.GetXMLDoc("Page");
    }

    // Apply XSL Template
    DisplayXMLDoc(xmlDoc, GetScriptFile());
}
</script>
