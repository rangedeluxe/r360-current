using System.Xml;
using System;


/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author: 
* Date: 
*
* Purpose:  help.aspx.cs
*
* Modification History
* CR 33002 WJS 09/07/2011
*    -Call Page base class to prevent logon
* CR 45894 JCS 02/13/2011
*    -Added branding folder to Xml
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class help : _Page {

        public help():base(true) {
        }
        private void Page_Load(object sender, EventArgs e) {

            XmlDocument objXml = WFS.RecHub.Common.ipoXmlLib.GetXMLDoc("Page");

            // Add Branding Scheme folder name to Xml
            AddBrandingInfoToXml(objXml.DocumentElement, BrandingSchemeFolder);

            DisplayXMLDoc(objXml, GetScriptFile());
        }
    }
}
