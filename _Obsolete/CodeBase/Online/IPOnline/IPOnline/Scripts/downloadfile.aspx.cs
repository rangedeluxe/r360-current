using System;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1998-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date: 09/04/2012
*
* Purpose:  
*
* Modification History
* CR 54181 JMC 09/06/2012
*   -Changed input to look for 'filepath' instead of 'ID' & 'JOB_TYPE'
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.

* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public partial class downloadfile : _JobRequestBase {

        private void Page_Load(object sender, EventArgs e) {
 
            if (CleanRequest.QueryString["filepath"] != null && CleanRequest.QueryString["filepath"].Length > 0) {
                DownloadFile(CleanRequest.QueryString["filepath"].ToString());
	        } else {
	            AddMessage(EventType.Error, "No Job Type specified.");
            }
       }

		#region Web Form Designer generated code
		override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}
		
		/*/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.Page_Load);
		}*/
		#endregion
    }
}