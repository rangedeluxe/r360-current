﻿using System;
using System.Data;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Santosh Sav
* Date:     08/26/2014
*
* Purpose:  Allow user to view a Report image in either a IE or NS browser
*           by converting it to a PDF.
*
* Modification History
* WI 172562 SAS 10/17/2014
*   Changes done for passing report parameters
******************************************************************************/
namespace WFS.RecHub.Online
{
    public partial class report : _JobRequestBase
    {

        private void Page_Load(object sender, EventArgs e)
        {
            DateTime dteTemp;
            int intTemp;
            string sReportName = ""; 
            if (Request.QueryString["ID"] != null && Request.QueryString["ID"].Length > 0)
            {
                CheckRequestStatus(OLFDeliveryMethod.Stream);
            }
            else
            {
                XmlDocument objXml;
                objXml = new XmlDocument();

                XmlNode nodeRoot = objXml.CreateElement("Root");
                XmlNode nodeReports = objXml.CreateElement("Reports");
                XmlNode nodeReport = objXml.CreateElement("Report");

                if (CleanRequest.QueryString["ReportName"] != null)
                {
                    sReportName = CleanRequest.QueryString["ReportName"];
                    ipoXmlLib.addElement(nodeReport, "ReportName", sReportName);
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'Report Name' was missing.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }


                if (CleanRequest.QueryString[Constants.QS_PARM_BANKID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BANKID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "BankID", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'bank' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "LockboxID", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'box' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_BATCHID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "BatchID", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'batch' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_BATCHNUMBER] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHNUMBER], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "BatchNumber", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'batchnumber' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }


                if (CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE] != null && DateTime.TryParse(CleanRequest.QueryString[Constants.QS_PARM_DEPOSITDATE], out dteTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "DepositDate", dteTemp.ToString("MM/dd/yyyy"));
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'date' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_PICSDATE] != null)
                {
                    ipoXmlLib.addAttribute(nodeReport, "PICSDate", CleanRequest.QueryString[Constants.QS_PARM_PICSDATE]);
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'picsdate' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_PROCESSINGDATE] != null && DateTime.TryParse(CleanRequest.QueryString["processingdate"], out dteTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "ProcessingDate", dteTemp.ToString("MM/dd/yyyy"));
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'processingdate' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_TXNID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_TXNID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "TransactionID", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'txn' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeReport, "BatchSequence", intTemp.ToString());
                }
                else
                {
                    if (sReportName == "TransactionReport")
                    { // as defined in Batchview.xsl
                        ipoXmlLib.addAttribute(nodeReport, "BatchSequence", ""); // Not required for transaction report.
                    }
                    else
                    {
                        EventLog.logEvent("Invalid Query String.  'item' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                        Response.Redirect(Constants.NOREPORT);
                    }
                }

                nodeReports.AppendChild(nodeReport);
                nodeRoot.AppendChild(nodeReports);
                objXml.AppendChild(nodeRoot);

                try
                {	
					
                    BaseGenericResponse<DataSet> respdoc = R360ReportServiceClient.CreateReportJobRequest(objXml);
                    DataSet dsRetVal = null;
                    if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                    {
                        dsRetVal = respdoc.Data;
                    }
                    if (dsRetVal != null && dsRetVal.Tables.Count > 0)
                    {
                        ProcessJobRequest(dsRetVal.Tables[0], OLFDeliveryMethod.Stream);
                    }
                    else
                    {   
                        Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                    }

                }
                catch (Exception ex)
                {

                    // Log Error
                    EventLog.logEvent(ex.Message, this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);

                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOREPORT);
                }
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            base.OnInit(e);
        }
        #endregion

    }
}