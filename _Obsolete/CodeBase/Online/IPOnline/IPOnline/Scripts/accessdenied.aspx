<%@ Page Language="C#" Inherits="WFS.RecHub.Online._Page" %>
<script runat="server">
private void Page_Load(object sender, System.EventArgs e) {
    System.Xml.XmlDocument xmlDoc;
    WFS.RecHub.R360Services.Common.XmlDocumentResponse respdoc = R360SystemServiceClient.GetBaseDocument();
    xmlDoc = new System.Xml.XmlDocument();
    xmlDoc = respdoc.Data;
    DisplayXMLDoc(xmlDoc, GetScriptFile());
}
</script>
