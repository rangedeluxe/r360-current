using System;
using System.Xml;

using WFS.RecHub.Common;

/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2013 WAUSAU Financial Systems, Inc. All rights reserved. All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information
* of WFS and contain WFS trade secrets. These materials may not be used,
* copied, modified or disclosed except as expressly permitted in writing by
* WFS (see the WFS license agreement for details). All copies, modifications
* and derivative works of these materials are property of WFS.
*
* Author:   Eric Goetzinger
* Date: 
*
* Purpose:  Allows user to add  remitter information to an item in 
*           the Checks table and to the remiiter file if not exists.
*
* Modification History
* VI 5159 JMC 03/24/2003 
*    -Add 'Add Remitter' functionality.  Used on Remit Search page.
* CR 5453 EJG 12/17/2003
*    -Added support for user emulation to prevent changes
*     from being made when in this mode.
* CR 5410 EJG 01/19/2004
*    -Corrected Regular Expression used to validate Account Numbers
* CR 9625 JMC 10/01/2004
*    -Replaced LockboxID with OLLOckboxId.
* CR 10537 JMC 03/03/2005
*    -Modified page to call SystemSrv.UpdateActivityLockbox function.
* CR 45984 JCS 02/13/2012
*    -Added dynamic branding folder information.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 86371 CRG 01/25/2013
*   -Error occurred in InsertLockboxRemitter.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 90144 WJS 3/4/2013
*   -FP:Error occurred in InsertLockboxRemitter.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    //public partial class addremitter : _Page {

    //    private cRemitter _Remitter;
    //    ////private bool _PageRefreshRequired = false;

    //    private string _DisplayMode = string.Empty;

    //    private Guid _OLLockboxID = Guid.Empty;

    //    private int _BankID;
    //    private int _LockboxID;
    //    private int _BatchID;
    //    private DateTime _DepositDate;
    //    private int _TransactionID;
    //    private int _BatchSequence;

    //    ////private string _LongNameValue;
    //    ////private DateTime _ProcessingDateValue;
    //    ////private int _RecstartValue;
    //    ////private string _ReturnPageValue;
    //    ////private double _AmountFromValue;
    //    ////private double _AmountToValue;
    //    ////private DateTime _DateFromValue;
    //    ////private DateTime _DateToValue;
    //    ////private string _SerialValue;
    //    ////private int _LockboxSelectedValue;
    //    ////private string _SortValue;
    //    ////private int _BatchIDValue;
    //    ////private int _DESetupIDValue;
    //    ////private string _PICSDateValue;
    //    ////private int _SequenceValue;
        
    //    private bool _FinishedNowCloseMe = false;

    //    private void Page_Load(object sender, System.EventArgs e) {

    //        bool blnError = false;
            
    //        Guid gidOLLockboxID = Guid.Empty;

    //        if(CleanRequest.Form["txtAction"] != null && CleanRequest.Form["txtAction"].Length > 0) {
    //            _OLLockboxID = CleanRequest.GetFormValue("txtOLLockboxID", Guid.Empty);
    //            _BankID = CleanRequest.GetFormValue("txtBankID", -1);
    //            _LockboxID = CleanRequest.GetFormValue("txtLockboxID", -1);
    //            _BatchID = CleanRequest.GetFormValue("txtBatchID", -1);
    //            _DepositDate = CleanRequest.GetFormValue("txtDepositDate", DateTime.MinValue);
    //            _TransactionID = CleanRequest.GetFormValue("txtTransactionID", -1);
    //            _BatchSequence = CleanRequest.GetFormValue("txtBatchSequence", -1);
    //        } else {
    //            _OLLockboxID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_OLLOCKBOXID, Guid.Empty);
    //            _BankID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_BANKID, -1);
    //            _LockboxID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_LOCKBOXID, -1);
    //            _BatchID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_BATCHID, -1);
    //            _DepositDate = CleanRequest.GetQueryStringValue(Constants.QS_PARM_DEPOSITDATE, DateTime.MinValue);
    //            _TransactionID = CleanRequest.GetQueryStringValue(Constants.QS_PARM_TXNID, -1);
    //            _BatchSequence = CleanRequest.GetQueryStringValue(Constants.QS_PARM_BATCHSEQUENCE, -1);
    //        }

    //        if(_BankID == -1) {
    //            AddMessage(EventType.Error, "Invalid BankID defined.");
    //        }

    //        if(_LockboxID == -1) {
    //            AddMessage(EventType.Error, "Invalid LockboxID defined.");
    //        }

    //        if(_OLLockboxID == Guid.Empty) {
    //            AddMessage(EventType.Error, "Invalid OLLockboxID defined.");
    //        }

    //        if(_BatchID == -1) {
    //            AddMessage(EventType.Error, "Invalid BatchID defined.");
    //        }

    //        if(_DepositDate <= DateTime.MinValue) {
    //            AddMessage(EventType.Error, "Invalid Deposit Date defined.");
    //        }

    //        if(_TransactionID == -1) {
    //            AddMessage(EventType.Error, "Invalid TransactionID defined.");
    //        }

    //        if(_BatchSequence == -1) {
    //            AddMessage(EventType.Error, "Invalid Batch Sequence defined.");
    //        }

    //        _Remitter = LoadRemitter();

    //        // **BEGIN PAGE OUTPUT AND CHECK TO SEE IF FORM SUBMIT
    //        if(Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0 && Request.Form["txtDisplayMode"] != null) {

    //            ////switch(Request.Form["txtDisplayMode"].ToLower()) {
    //            ////    case "edit":
    //                    ValidateForm();
    //                    // Do not allow remitter to be added if emulation mode
    //                    if(Errors.Count == 0 && !AppSession.IsEmulationSession) {
    //                        // Update Checks.RemitterName for the given item
    //                        if(!RemitterSrv.UpdateChecksRemitter(_Remitter.LockboxRemitterName,
    //                                                             _BankID, 
    //                                                             _LockboxID, 
    //                                                             _BatchID, 
    //                                                             _DepositDate, 
    //                                                             _TransactionID, 
    //                                                             _BatchSequence)) {

    //                            AddMessage(EventType.Error, 
    //                                       "The item was not updated. Please verify the information and try again.");
    //                        } else {
    //                            if(_Remitter.LockboxRemitterID == Guid.Empty) { 
    //                                // Insert the LockboxRemitter since it doesn't exist
    //                                RemitterSrv.InsertLockboxRemitter(_Remitter.RoutingNumber, 
    //                                                                  _Remitter.Account, 
    //                                                                  _OLLockboxID, 
    //                                                                  _Remitter.LockboxRemitterName);
    //                            } else {
    //                                // Update the LockboxRemitter since it does exist
    //                                RemitterSrv.UpdateLockboxRemitter(_Remitter.LockboxRemitterID, 
    //                                                                  _Remitter.LockboxRemitterName);
    //                            }

    //                            _FinishedNowCloseMe = true;
    //                        }
    //                    }
    //            ////        break;
    //            ////}
    //        }

    //        // Build message that you are not able to add and modify remitters in emulation mode
    //        if(AppSession.IsEmulationSession) {
    //           AddMessage(EventType.Information, "You are not able to add or modify remitters in emulation mode.");
    //        }

    //        if(!blnError) { 
    //            InitializeForm();
    //        }

    //        /*TODO: WHY?
    //        With Response
    //            .Write GetField("LockboxRemitterID") + vbcrlf
    //            .Write GetField("GlobalBatchID") + vbcrlf
    //            .Write GetField("GlobalCheckID") + vbcrlf
    //            .Write GetField("TransactionID") + vbcrlf
    //            .Write GetField("LongName") + vbcrlf
    //            .Write GetField("ProcessingDate") + vbcrlf
    //            .Write GetField("Recstart") + vbcrlf
    //            .Write GetField("ReturnPage") + vbcrlf
    //            .Write GetField("AmountFrom") + vbcrlf
    //            .Write GetField("AmountTo") + vbcrlf
    //            .Write GetField("DateFrom") + vbcrlf
    //            .Write GetField("DateTo") + vbcrlf
    //            .Write GetField("Serial") + vbcrlf
    //            .Write GetField("LockboxSelected") + vbcrlf
    //            .Write GetField("Sort") + vbcrlf
    //            .Write GetField("BatchID") + vbcrlf
    //            .Write GetField("DESetupID") + vbcrlf
    //            .Write GetField("PICSDate") + vbcrlf
    //            .Write GetField("Sequence") + vbcrlf
    //        End With
    //        */

    //        XmlDocument xmlDoc;
    //        XmlNode nodeFormFields;

    //        xmlDoc = GetRequestDoc("Page");

    //        // Add Branding Scheme folder name to Xml
    //        AddBrandingInfoToXml(xmlDoc.DocumentElement, BrandingSchemeFolder);

    //        nodeFormFields = ipoXmlLib.addElement(xmlDoc.DocumentElement, "FormFields", string.Empty);

    //        ipoXmlLib.addElement(nodeFormFields, "OLLockboxID", _OLLockboxID.ToString());           
    //        ipoXmlLib.addElement(nodeFormFields, "BankID", _BankID.ToString());
    //        ipoXmlLib.addElement(nodeFormFields, "LockboxID", _LockboxID.ToString());
    //        ipoXmlLib.addElement(nodeFormFields, "BatchID", _BatchID.ToString());
    //        ipoXmlLib.addElement(nodeFormFields, "DepositDate", _DepositDate.ToString("MM/dd/yyyy"));
    //        ipoXmlLib.addElement(nodeFormFields, "TransactionID", _TransactionID.ToString());
    //        ipoXmlLib.addElement(nodeFormFields, "BatchSequence", _BatchSequence.ToString());

    //        ////ipoXmlLib.addElement(nodeFormFields, "LongNameValue", _LongNameValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "ProcessingDateValue", _ProcessingDateValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "RecstartValue", _RecstartValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "ReturnPageValue", _ReturnPageValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "AmountFromValue", _AmountFromValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "AmountToValue", _AmountToValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "DateFromValue", _DateFromValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "DateToValue", _DateToValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "SerialValue", _SerialValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "LockboxSelectedValue", _LockboxSelectedValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "SortValue", _SortValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "BatchIDValue", _BatchIDValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "DESetupIDValue", _DESetupIDValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "PICSDateValue", _PICSDateValue);
    //        ////ipoXmlLib.addElement(nodeFormFields, "SequenceValue", _SequenceValue);
            
    //        ipoXmlLib.addElement(nodeFormFields, "LockboxRemitterID", _Remitter.LockboxRemitterID.ToString());           
    //        ipoXmlLib.addElement(nodeFormFields, "LockboxID", _Remitter.LockboxID.ToString());
    //        ipoXmlLib.addElement(nodeFormFields, "RoutingNumber", _Remitter.RoutingNumber);
    //        ipoXmlLib.addElement(nodeFormFields, "AccountNumber", _Remitter.Account);
    //        ipoXmlLib.addElement(nodeFormFields, "RemitterName", _Remitter.LockboxRemitterName);
            

    //        ////ipoXmlLib.addElement(nodeFormFields, "PageRefreshRequired", ipoLib.IIf(_PageRefreshRequired, "1", "0"));

    //        ipoXmlLib.addElement(nodeFormFields, "FinishedNowCloseMe", ipoLib.IIf(_FinishedNowCloseMe, "1", "0"));

    //        DisplayXMLDoc(xmlDoc, GetScriptFile());
    //    }

    //    private void InitializeForm() {

    //        ////Guid gidTemp;

    //        _DisplayMode = "edit";

    //        if(!SystemSrv.CheckUserPageModePermissions("remittermaint.aspx", "add")) { 
    //            Response.Redirect("accessdenied.aspx");
    //        }

    //        ////// Mode definitions:
    //        ////// Edit - Adding a remitter to both an item in Checks and to GlobalRemitter/LockboxRemitter
    //        ////switch(_DisplayMode) {
    //        ////    case "edit":

    //                if(_OLLockboxID == Guid.Empty || !CBXSrv.CheckLockboxPermission(_OLLockboxID)) { 
    //                    Response.Redirect("accessdenied.aspx");
    //                }

    //                //SetField("PageTitle", "Add Remitter To Item");
    //                //SetField("FormTitle", "Add Remitter To Item");
    //                //SetField("GlobalBatchID", "<input type=\"hidden\" name=\"txtGlobalBatchID\" value=\"" + GetField("GlobalBatchIDValue") + "\">");
    //                //SetField("GlobalCheckID", "<input type=\"hidden\" name=\"txtGlobalCheckID\" value=\"" + GetField("GlobalCheckIDValue") + "\">");
    //                //SetField("TransactionID", "<input type=\"hidden\" name=\"txtTransactionID\" value=\"" + GetField("TransactionIDValue") + "\">");
    //                //SetField("LockboxRemitterID", "<input type=\"hidden\" name=\"txtLockboxRemitterID\" value=\"" + objRemitter.LockboxRemitterID + "\">");
    //                //SetField("LongName", "<input type=\"hidden\" name=\"txtLongName\" value=\"" + GetField("LongNameValue") + "\">");
    //                //SetField("ProcessingDate", "<input type=\"hidden\" name=\"txtProcessingDate\" value=\"" + GetField("ProcessingDateValue") + "\">");
    //                //SetField("Recstart", "<input type=\"hidden\" name=\"txtRecstart\" value=\"" + GetField("RecstartValue") + "\">");
    //                //SetField("ReturnPage", "<input type=\"hidden\" name=\"txtReturnPage\" value=\"" + GetField("ReturnPageValue") + "\">");
    //                //SetField("AmountFrom", "<input type=\"hidden\" name=\"txtAmountFrom\" value=\"" + GetField("AmountFromValue") + "\">");
    //                //SetField("AmountTo", "<input type=\"hidden\" name=\"txtAmountTo\" value=\"" + GetField("AmountToValue") + "\">");
    //                //SetField("DateFrom", "<input type=\"hidden\" name=\"txtDateFrom\" value=\"" + GetField("DateFromValue") + "\">");
    //                //SetField("DateTo", "<input type=\"hidden\" name=\"txtDateTo\" value=\"" + GetField("DateToValue") + "\">");
    //                //SetField("Serial", "<input type=\"hidden\" name=\"txtSerial\" value=\"" + GetField("SerialValue") + "\">");
    //                //SetField("LockboxSelected", "<input type=\"hidden\" name=\"txtLockboxSelected\" value=\"" + GetField("LockboxSelectedValue") + "\">");
    //                //SetField("Sort", "<input type=\"hidden\" name=\"txtSort\" value=\"" + GetField("SortValue") + "\">");
    //                //SetField("BatchID", "<input type=\"hidden\" name=\"txtBatchID\" value=\"" + GetField("BatchIDValue") + "\">");
    //                //SetField("DESetupID", "<input type=\"hidden\" name=\"txtDESetupID\" value=\"" + GetField("DESetupIDValue") + "\">");
    //                //SetField("PICSDate", "<input type=\"hidden\" name=\"txtPICSDate\" value=\"" + GetField("PICSDateValue") + "\">");
    //                //SetField("Sequence", "<input type=\"hidden\" name=\"txtSequence\" value=\"" + GetField("SequenceValue") + "\">");
    //                //SetField("OLLockboxID", objRemitter.LockboxID + "<input type=\"hidden\" name=\"txtOLLockboxID\" value=\"" + GetField("OLLockboxIDValue") + "\">");
    //                //SetField("RoutingNumber", objRemitter.RoutingNumber + "<input type=\"hidden\" name=\"txtRoutingNumber\" value=\"" + objRemitter.RoutingNumber + "\">");
    //                //SetField("AccountNumber", objRemitter.Account + "<input type=\"hidden\" name=\"txtAccount\" value=\"" + objRemitter.Account + "\">");
    //                //SetField("RemitterName", "<input type=\"text\" name=\"txtRemitterName\" size=\"20\" maxlength=\"60\" value=\"" + objRemitter.LockboxRemitterName + "\" class=\"formfield\">");
    //                //SetField("Button1", "<input type=\"button\" name=\"cmdUpdate\" class=\"formfield\" value=\"Submit\" onClick=\"formSubmit(this, 'txtAction')\">&nbsp;");
    //                //SetField("Button2", "<input type=\"button\" name=\"cmdCancel\" class=\"formfield\" value=\"Cancel\" onClick=\"parent.window.opener.window.focus();parent.window.close();\">");

    //        ////        break;
    //        ////}
    //    }
	
	
    //    /***********************************************************************
    //     Purpose:	Validate the form fields based off of page mode.
    //     Input(s):		None.
    //     Output:		None.
    //    ***********************************************************************/
    //    private void ValidateForm() {
    		
    //        string strFailureReason;
    		
    //        if(Request.Form["txtDisplayMode"].ToLower() == "add") {
    //            // Verify Lockbox Selected
    //            if(_Remitter.LockboxID < 0) {
    //                AddMessage(EventType.Error, "Lockbox is required.");
    //            }

    //            // Validate Routing Number
    //            if(!ValidationLib.ValidateRT(_Remitter.RoutingNumber, out strFailureReason)) {
    //                AddMessage(EventType.Error, strFailureReason);
    //            }

    //            // Validate Account Number
    //            if(!ValidationLib.ValidateAccount(_Remitter.Account, out strFailureReason)) {
    //                AddMessage(EventType.Error, strFailureReason);
    //            }
    //        }		
    		
    //        // Validate Remitter Name
    //        if(_Remitter.LockboxRemitterName.Length == 0) {
    //            AddMessage(EventType.Error, "Payer Name is a required field.");
    //        }	
    //    }
    	
    //    /// <summary>
    //    /// Load a reference to a cRemitter object.
    //    /// </summary>
    //    /// <returns></returns>
    //    private cRemitter LoadRemitter() {
    		
    //        cRemitter objTmp;
    //        cOLLockbox objLockbox;
    //        bool blnError = false;
    //        Guid gidTemp;
    //        ////Guid gidOLLockboxID;
    //        int intOLWorkGropsID;
    //        Guid gidLockboxRemitterID;

    //        if(CleanRequest.HasFormValue("txtAction") && CleanRequest.Form["txtAction"].Length > 0) {
            
    //            if(CleanRequest.TryGetFormValue("txtLockboxRemitterID", out gidTemp)) {
    //                gidLockboxRemitterID = gidTemp;
    //            } else {
    //                gidLockboxRemitterID = Guid.Empty;
    //                AddMessage(EventType.Error, "Invalid LockboxRemitterID defined.");
    //            }

    //            objTmp = new cRemitter();
    //            objTmp.LockboxRemitterID = gidLockboxRemitterID;
    //            objTmp.RoutingNumber = CleanRequest.Form["txtRoutingNumber"];
    //            objTmp.Account = CleanRequest.Form["txtAccount"];
    //            objTmp.LockboxRemitterName = CleanRequest.Form["txtRemitterName"];

    //            if(_OLLockboxID != Guid.Empty) {

    //                objLockbox=SystemSrv.GetLockboxByID(_OLLockboxID);
    //                objTmp.BankID = objLockbox.BankID;
    //                objTmp.LockboxID = objLockbox.LockboxID;
    //            }

    //        } else if(CleanRequest.HasQueryStringValue(Constants.QS_PARM_RT) && 
    //                  CleanRequest.HasQueryStringValue(Constants.QS_PARM_ACCOUNT) && 
    //                  CleanRequest.HasQueryStringValue(Constants.QS_PARM_OLLOCKBOXID) && 
    //                  _BankID > -1 && 
    //                  _LockboxID > -1 && 
    //                  _BatchID > -1 && 
    //                  _DepositDate > DateTime.MinValue && 
    //                  _TransactionID > -1 && 
    //                  _BatchSequence > -1 && 
    //                  _OLLockboxID != Guid.Empty) {

    //            ////gidOLLockboxID = gidTemp;

    //            // Edit Mode and RoutingNumber, Account, LockboxID, GlobalBatchID, GlobalCheckID, TransactionID values in QueryString
    //            if(RemitterSrv.LockboxRemitterExists(CleanRequest.QueryString[Constants.QS_PARM_RT]
    //                                    ,CleanRequest.QueryString[Constants.QS_PARM_ACCOUNT]
    //                                    ,_OLLockboxID)) {

    //                objTmp = RemitterSrv.GetLockboxRemitter(CleanRequest.QueryString[Constants.QS_PARM_RT]
    //                                            ,CleanRequest.QueryString[Constants.QS_PARM_ACCOUNT]
    //                                            ,_OLLockboxID);

    //                if(objTmp.LockboxRemitterName.Length <= 0 && objTmp.GlobalRemitterName.Length > 0) { 
    //                    objTmp.LockboxRemitterName = objTmp.GlobalRemitterName;
    //                }

    //            } else {
    //                objTmp = new cRemitter();
    //                objTmp.RoutingNumber = CleanRequest.QueryString[Constants.QS_PARM_RT];
    //                objTmp.Account = CleanRequest.QueryString[Constants.QS_PARM_ACCOUNT];
    //                objLockbox=SystemSrv.GetLockboxByID(_OLLockboxID);
    //                objTmp.BankID = objLockbox.BankID;
    //                objTmp.LockboxID = objLockbox.LockboxID;
    //                objLockbox=null;
    //            }
    //        } else {
    //            blnError = true;
    //            AddMessage(EventType.Error, "Unable to add a payer to the item selected. Information was missing or invalid. Please select another item and try again.");
    //            objTmp = new cRemitter();
    //        }

    //        if(!blnError) {
    //            SystemSrv.UpdateActivityLockbox(this.ActivityLogID
    //                                          , objTmp.BankID
    //                                          , objTmp.LockboxID);
    //        }

    //        return(objTmp);
    //    }

    //    #region Web Form Designer generated code
    //    override protected void OnInit(EventArgs e)
    //    {
    //        //
    //        // CODEGEN: This call is required by the ASP.NET Web Form Designer.
    //        //
    //        //InitializeComponent();
    //        base.OnInit(e);
    //    }
		
    //    /*/// <summary>
    //    /// Required method for Designer support - do not modify
    //    /// the contents of this method with the code editor.
    //    /// </summary>
    //    private void InitializeComponent()
    //    {    
    //        this.Load += new System.EventHandler(this.Page_Load);
    //    }*/
    //    #endregion
    //}
    
}