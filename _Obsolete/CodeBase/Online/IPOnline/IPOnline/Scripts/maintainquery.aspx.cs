using System;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;

using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Jason Efken
* Date:    7/21/2011
*
* Purpose: Display/Maintain saved queries for a given user.
*
* Modification History
* 7/21/2011 JNE 31754 - Created
* 9/06/2011 JNE 46222 - Added pagination
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 155806 TWE 07/30/2014
*    add check for permission to access this page
******************************************************************************/

namespace WFS.RecHub.Online {

    public partial class maintainquery :  _Page {
        private void Page_Load(object sender, EventArgs e) {
            XmlDocument nodeSavedQueries = null;
            int UserID = AppRaamUser.UserID;
            Guid gidTemp;
            Guid PredefSearchID = Guid.Empty;
            Guid PredefSearchID_Uncheck = Guid.Empty;
            int intStartRecord = 1;
            int intTemp = 0;

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_AdvanceSearchQueries, R360Permissions.ActionType.View))
            {
                Response.Redirect("accessdenied.aspx");
            }

            // Retrieve input parameters
            if(CleanRequest.Form["txtAction"] != null && CleanRequest.Form["txtAction"].Length > 0) {
                if(CleanRequest.Form["txtPredefSearchID"] != null && ipoLib.TryParse(CleanRequest.Form["txtPredefSearchID"],out gidTemp)){
                   PredefSearchID = gidTemp;
                }
                if(CleanRequest.Form["txtPredefSearchID_Uncheck"] != null && ipoLib.TryParse(CleanRequest.Form["txtPredefSearchID_Uncheck"], out gidTemp)){
                    PredefSearchID_Uncheck = gidTemp;
                }
                switch(CleanRequest.Form["txtAction"]){
                    case "delete":
                        BaseResponse resp = R360SearchServiceClient.DeleteSavedQuery(UserID, PredefSearchID);
                        if (resp.Status == R360Shared.StatusCode.SUCCESS)
                        {
                            AddMessage(EventType.Information, "Query deleted");
                        }
                        else
                        {
                            AddMessage(EventType.Error, "Error deleting query");
                        }
                        break;
                    case "update":
                        BaseResponse resp2 = R360SearchServiceClient.UpdateSavedQuery(UserID,PredefSearchID,1);
                        if (resp2.Status == R360Shared.StatusCode.SUCCESS)
                        { //check IsDefault
                            if (PredefSearchID_Uncheck != Guid.Empty)
                            {
                                BaseResponse resp3 = R360SearchServiceClient.UpdateSavedQuery(UserID, PredefSearchID_Uncheck, 0);
                                if (resp3.Status == R360Shared.StatusCode.FAIL)
                                { //uncheck IsDefault
                                    //Failed to uncheck old default record, need to revert back
                                    R360SearchServiceClient.UpdateSavedQuery(UserID, PredefSearchID, 0);
                                    AddMessage(EventType.Error, "Error changing default");
                                }
                            }
                        }
                    else
                        {
                            AddMessage(EventType.Error, "Error changing default");
                        }
                        break;
                    default:
                        break;
                }
            }

            if ((Request.Form["txtStart"] != null) && int.TryParse(Request.Form["txtStart"], out intTemp))
            {
                intStartRecord = intTemp;
            }

            XmlDocumentResponse respdoc = R360SearchServiceClient.GetSavedQueries(UserID, intStartRecord, true);
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                nodeSavedQueries = respdoc.Data;
            }
            //nodeSavedQueries = SearchSrv.GetSavedQueries(UserID, intStartRecord, true);
            XmlDocument objXml = nodeSavedQueries;
            AbortIfNoXml(objXml, "GetSavedQueries");

            // Add form fields to xml document
            if (PredefSearchID != null )
            {
                ipoXmlLib.AddFormFieldAsNode(objXml.DocumentElement, "frmMaintainQueries", "txtPredefSearchID", PredefSearchID.ToString(), "INPUT");
            }
            if(PredefSearchID_Uncheck != null)
            {
                ipoXmlLib.AddFormFieldAsNode(objXml.DocumentElement, "frmMaintainQueries", "txtPredefSearchIDUncheck", PredefSearchID_Uncheck.ToString(), "INPUT");
            }
            DisplayXMLDoc(objXml, GetScriptFile());
        }
    }
}
