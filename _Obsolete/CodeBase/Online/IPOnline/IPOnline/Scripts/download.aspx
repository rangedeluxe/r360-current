<%@ Page Language="C#" Inherits="WFS.RecHub.Online._Page" %>
<script runat="server">
/*****************************************************************
' Module:           Download Remitter File.'
' Filename:         download.asp'
' Author:           Eric Goetzinger'
' Description:
'	Allow users to download all remitters for the lockboxes they
'   have access to.
'	
' Revisions:
'	CR 16007 EJG 04/17/2006 - Convert page to use XSLT
'****************************************************************/
private void Page_Load(object sender, System.EventArgs e) {

    System.Xml.XmlDocument xmlDoc;
    System.Xml.XmlNode nodeRespXml;

    // get all remitter records
    nodeRespXml = RemitterSrv.GetAllRemitters(1, true);
    xmlDoc = new System.Xml.XmlDocument();
    xmlDoc.AppendChild(xmlDoc.ImportNode(nodeRespXml, true));

    Response.ContentType = "text/plain";
    Response.AddHeader("Content-Disposition", "attachment;filename=\"remitters.txt\"");

    // transform the xml using xsl template
    DisplayXMLDoc(xmlDoc, GetScriptFile());
}
</script>
