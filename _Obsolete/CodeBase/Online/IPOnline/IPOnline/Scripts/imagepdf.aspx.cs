using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Xml;
using Wfs.Logging;
using WFS.RecHub.Common;
using WFS.RecHub.HubConfig;
using WFS.RecHub.OLFServices;
using WFS.RecHub.OLFServices.DataTransferObjects;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Goetzinger
* Date:     08/05/2002
*
* Purpose:  Allow user to view a check/document image in either a IE or NS browser
*           by converting it to a PDF.
*
* Modification History
* 08/05/2002 EJG
*     - Made change to display a text message that the image
*       could not be retrieved if no image exists. This is in relation
*       to using this page to display checks when adding a remitter
*       and no image exists. VI 3808
* 11/11/2002
*     - Retrieve image PDF as SOAP attachment.
* 01/31/2003 EJG
*     - Added RTrim() function to all string values in QueryString.
* 02/7/2003 EJG
*     - Updated issue when no image is received to redirect to the 
*       customer defined noimage.asp page.
* 06/01/2004 CR 7728 JMC 
*     - Rebuilt page to utilize ipoJobRequestUtil.asp include file to interact
*       with ipo Image Services.  All image requests are now routed through 
*       ipo Image Services.
*  10/21/2005 CR 12671 JMC
*     - Added "On Error Resume Next" before call to CreateViewSpecifiedJobRequest.
* 03/01/2006 CR 15161 JMC
*     - Added include file "ViewAllLayout.asp"
*     - Added include file "downloadUtil.asp"
*     - Changed call to "ProcessNewRequest" to "ProcessRequest".
* 02/13/2012 CR 45984 JCS
*     - Updated noimage URL paths to account for dynamic branding.
* 08/08/2012 CR 54169 JNE
*     - Added batchnumber 
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 83716  WJS 1/4/2013
 *  - Added BatchNumber to use constant correctly
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 83716  WJS 1/4/2013
*  - Added BatchNumber to use constant correctly
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 111833 CEJ 08/15/2013
*   Change the imagepdf page to pull in the Batch Number and include it in the image request
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
******************************************************************************/

namespace WFS.RecHub.Online {

    public partial class imagepdf : _JobRequestBase {

        private void Page_Load(object sender, EventArgs e)
        {

            var _configService = new HubConfigServicesManager();
            DateTime dteTemp;
            int intTemp;

            if (Request.QueryString["ID"] != null && Request.QueryString["ID"].Length > 0)
            {
		        CheckRequestStatus(OLFDeliveryMethod.Stream);
            }
            else
            {


                XmlDocument objXml;
                objXml = new XmlDocument();           

                XmlNode nodeRoot = objXml.CreateElement("Root");
                XmlNode nodeImages = objXml.CreateElement("Images");
                XmlNode nodeImage;

                int bankId = 0;
                int lockboxId = 0;
                int batchId = 0;
                DateTime date = DateTime.MinValue;
                int picsDate = 0;
                int transactionId = 0;
                int batchSequence = 0;
                bool isCheck = true;

                if (CleanRequest.QueryString["type"] == null || CleanRequest.QueryString["type"].ToLower() != "c")
                {
                    nodeImage = objXml.CreateElement("Document");
                    isCheck = false;
                }
                else
                {
                    nodeImage = objXml.CreateElement("Check");
                    isCheck = true;
                }
                
                if (CleanRequest.QueryString[Constants.QS_PARM_BANKID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BANKID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "BankID", intTemp.ToString());
                    bankId = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'bank' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_LOCKBOXID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "LockboxID", intTemp.ToString());
                    lockboxId = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'box' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString["batch"] != null && int.TryParse(CleanRequest.QueryString["batch"], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "BatchID", intTemp.ToString());
                    batchId = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'batch' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_BATCHNUMBER] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHNUMBER], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "BatchNumber", intTemp.ToString());
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'batchnumber' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString["date"] != null && DateTime.TryParse(CleanRequest.QueryString["date"], out dteTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "DepositDate", dteTemp.ToString("MM/dd/yyyy"));
                    date = dteTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'date' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_PICSDATE] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_PICSDATE], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "PICSDate", CleanRequest.QueryString[Constants.QS_PARM_PICSDATE]);
                    picsDate = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'picsdate' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_TXNID] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_TXNID], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "TransactionID", intTemp.ToString());
                    transactionId = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'txn' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                if (CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE] != null && int.TryParse(CleanRequest.QueryString[Constants.QS_PARM_BATCHSEQUENCE], out intTemp))
                {
                    ipoXmlLib.addAttribute(nodeImage, "BatchSequence", intTemp.ToString());
                    batchSequence = intTemp;
                }
                else
                {
                    EventLog.logEvent("Invalid Query String.  'item' was missing or invalid.", this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }

                nodeImages.AppendChild(nodeImage);
                nodeRoot.AppendChild(nodeImages);
                objXml.AppendChild(nodeRoot);

                try
                {

                    EventLog.logEvent(string.Format("Starting image retrieval at {0}", DateTime.Now.ToString()), this.GetType().Name, MessageType.Information, MessageImportance.Essential);

                    // Get the System Preference so we can decide Headers to show
                    var preferences = GetSystemPreferences(_configService);
                    var displayPayer = GetDisplayPreference(preferences, "DisplayRemitterNameInPDF");
                    var userPrefs = GetUserPreferences();
                    if (userPrefs != null && userPrefs.displayRemitterNameinPdf.HasValue)
                    {
                        displayPayer = userPrefs.displayRemitterNameinPdf.Value;
                    }

                    var request = new ImageRequestDto
                    {
                        BankId = bankId,
                        WorkgroupId = lockboxId,
                        DepositDate = date,
                        BatchId = batchId,
                        BatchSequence = batchSequence,
                        TransactionId = transactionId,
                        IsCheck = isCheck,
                        SiteKey = SiteKey,
                        PicsDate = picsDate,
                        DisplayRemitterName = displayPayer,
                        DisplayBatchId = ShowBatchId(lockboxId, bankId, _configService)
                    };

                    var configSetting = ConfigurationManager.AppSettings.Get("SubmitJobForSingleImage");
                    bool submitJobForSingleImage;
                    bool.TryParse(configSetting, out submitJobForSingleImage);
                    if (submitJobForSingleImage)
                        SubmitJob(objXml);
                    else
                        DownloadImage(request);

                }
                catch (Exception ex)
                {

                    // Log Error
                    EventLog.logEvent(ex.Message, this.GetType().ToString(), MessageType.Error, MessageImportance.Essential);

                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                }
            }
        }

        private void DownloadImage(ImageRequestDto request)
        {
            EventLog.logEvent("Performing new Download Image Function", this.GetType().Name, MessageType.Information, MessageImportance.Essential);
            var client = new OLFServicesClient.OLFOnlineClient(SiteKey);
            var metaData = new FileMetaData()
            {
                SiteKey = SiteKey,
                SessionID = AppSession.SessionID.ToString(),
                EmulationID = AppSession.EmulationID.ToString()
            };

            var response = client.DownloadImage(metaData, request);
            // Instead of creating the request, let's just build the HTML 

            // Set some headers.
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.BinaryWrite(response.ImageData);
            Response.Flush();
            EventLog.logEvent(string.Format("Finished image retrieval via Download Image at {0}", DateTime.Now.ToString()), this.GetType().Name, MessageType.Information, MessageImportance.Essential);
        }
        private void SubmitJob(XmlDocument xml)
        {
            EventLog.logEvent("Performing old Submit Job Image Function", this.GetType().Name, MessageType.Information, MessageImportance.Essential);
            BaseGenericResponse<DataSet> respdoc = R360ImageServiceClient.CreateViewSpecifiedJobRequest(xml);
            DataSet dsRetVal = null;
            if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
            {
                dsRetVal = respdoc.Data;
            }

            if (dsRetVal != null && dsRetVal.Tables.Count > 0)
            {
                ProcessJobRequest(dsRetVal.Tables[0], OLFDeliveryMethod.Stream);
            }
            else
            {
                //EventLog.logEvent(e
                // TODO: Log Error?
                Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
            }
        }

        protected List<PreferenceDto> GetSystemPreferences(HubConfigServicesManager configservice)
        {
            try
            {
                var response = configservice.GetPreferences();

                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in GetPreferences").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    EventLog.logEvent(sb.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    return null;
                }

                var preferences = response.Data;
                return preferences;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Exception occurred in GetPreferences\r\n" + ex, this.GetType().Name, MessageType.Error, MessageImportance.Essential);

                return null;
            }
        }

        protected bool ShowBatchId(int siteClientAccountId, int siteBankId, HubConfigServicesManager configService)
        {
            try
            {
                var response = configService.GetWorkgroupByWorkgroupAndBank(siteClientAccountId, siteBankId, true);
                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in ShowBatchId").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    EventLog.logEvent(sb.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    return false;
                }

                var wg = response.Data;
                if (wg.DisplayBatchID.HasValue)
                {
                    return wg.DisplayBatchID.Value;
                }
                else
                {
                    return wg.EntityID != null && ShowBatchIdEntity(wg.EntityID.Value, configService);
                }
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Exception occurred in ShowBatchId\r\n" + ex, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
            }
            return false;
        }

        protected bool ShowBatchIdEntity(int entityId, HubConfigServicesManager configService)
        {
            try
            {
                var response = configService.GetEntity(entityId);
                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in ShowBatchIdEntity").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    EventLog.logEvent(sb.ToString(), this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    return false;
                }

                var entity = response.Data;
                return entity.DisplayBatchID;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Exception occurred in ShowBatchIdEntity\r\n" + ex, this.GetType().Name, MessageType.Error, MessageImportance.Essential);

            }
            return false;
        }

        protected bool GetDisplayPreference(List<PreferenceDto> preferences, string name)
        {
            var pref = preferences.Find(s => s.Name == name);
            if (pref != null)
            {
                return pref.DefaultSetting == "Y";
            }

            return false;
        }
        protected UserPreferencesDTO GetUserPreferences()
        {
            try
            {
                var userService = new UserServiceManager();
                var response = userService.GetUserPreferences();

                if (response.Status != StatusCode.SUCCESS)
                {
                    var sb = new StringBuilder();
                    sb.Append("Exception occurred in GetUserPreferences").Append(Environment.NewLine);
                    foreach (var error in response.Errors)
                    {
                        sb.Append(error).Append(Environment.NewLine);
                    }
                    EventLog.logEvent(sb.ToString(),this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                    return null;
                }

                var preferences = response.Data;
                return preferences;
            }
            catch (Exception ex)
            {
                EventLog.logEvent("Exception occurred in GetUserPreferences\r\n" + ex, this.GetType().Name, MessageType.Error, MessageImportance.Essential);
                return null;
            }
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
		{
			//
			// CODEGEN: This call is required by the ASP.NET Web Form Designer.
			//
			//InitializeComponent();
			base.OnInit(e);
		}
		
		/*/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{    
            this.Load += new System.EventHandler(this.Page_Load);
		}*/
		#endregion

    }
}