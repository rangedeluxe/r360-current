using System;
using System.Xml;

using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Shared;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     
*
* Purpose:  
*
* Modification History
* CR 46056 JMC 10/04/2011
*    -Added querystring option for 'printable view'.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 150507 TWE 06/26/2014
*    Fix Batch Summary and initial Payment Search page display
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 155807 TWE 07/30/2014
*    add check for permission to access this page
******************************************************************************/
namespace WFS.RecHub.Online
{
    public partial class batchsummary : _Page
    {
        private DateTime dteStartDate = DateTime.MinValue;
        private DateTime dteEndDate = DateTime.MinValue;
        private int EntityID = -1;
        private string workgroupSelection = "";
        private int intPaymentType = -1;  // Wow, you gotta love this. Can't use the default 0.
        private int intPaymentSource = -1; 
        private int intStartRecord = 1;
        private int bankID = -1;
        private int clientAccountID = -1;
        private int intOLWorkgroupID = -1;

        private void Page_Load(object sender, global::System.EventArgs e)
        {
            bool bolPrintView = false;

            XmlDocument xmlParms;
            XmlDocument nodeBatchSummary = null;
            XmlDocument xmlDoc;

            if (!R360Permissions.Current.Allowed(R360Permissions.Perm_BatchSummary, R360Permissions.ActionType.View))
            {
                Response.Redirect("accessdenied.aspx");
            }

            bolPrintView = GetIsPrintView();

            // Retrieve input parameters
            if (Request.Form["txtAction"] != null && Request.Form["txtAction"].Length > 0)
            {
                GetVarsFromForm();
            }
            else if (Request.QueryString.Count > 0)
            {
                if (Request.QueryString["id"] != null && Request.QueryString["id"].ToLower() == "bc")
                {
                    GetVarsFromSession();
                }
                else
                {
                    GetVarsFromQuerystring();
                }
            }
            else
            {
                dteStartDate = DateTime.Now.Date;
                dteEndDate = DateTime.Now.Date;
            }

            
            SetDateFields();
            GetStartRecordForPaging();
            xmlParms = GetXmlParmsDoc(dteStartDate, dteEndDate, intStartRecord, intPaymentType, intPaymentSource, bolPrintView);

            try
            {
                XmlDocumentResponse respdoc = R360CBXServiceClient.GetBatchSummary(xmlParms);

                if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                {
                    nodeBatchSummary = respdoc.Data;
                }
            }
            catch (Exception ex)
            {
                AddMessage(EventType.Error, ex.Message);
                EventLog.logError(ex);
                nodeBatchSummary = null;
            }

            if (nodeBatchSummary == null)
            {
                xmlDoc = ipoXmlLib.GetXMLDoc("Page");
            }
            else
            {
                xmlDoc = new XmlDocument();
                xmlDoc = nodeBatchSummary;
            }

            // Add form fields to xml document
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtStartDate", this.GetField("StartDate"), "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtEndDate", this.GetField("EndDate"), "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtWorkgroupSelection", workgroupSelection, "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtPaymentTypeID", intPaymentType.ToString(), "INPUT");
            ipoXmlLib.AddFormFieldAsNode(xmlDoc.DocumentElement, "frmMain", "txtPaymentSourceID", intPaymentSource.ToString(), "INPUT");
            Session["BatchSummaryVars"] = GetBatchSummaryVars();

            // Transform the xml document using the appropriate stylesheet
            if (bolPrintView)
            {
                DisplayXMLDoc(xmlDoc, GetScriptFile().Replace(".aspx", "_printview.aspx"));
            }
            else
            {
                DisplayXMLDoc(xmlDoc, GetScriptFile());
            }
        }

        private void GetVarsFromSession()
        {
            BatchSummaryVars summaryVars = (BatchSummaryVars)Session["BatchSummaryVars"];
            dteStartDate = summaryVars.StartDate;
            dteEndDate = summaryVars.EndDate;
            bankID = summaryVars.BankID;
            clientAccountID = summaryVars.ClientAccountID;
            intPaymentType = summaryVars.PaymentTypeID;
            intPaymentSource = summaryVars.PaymentSourceID;
            intStartRecord = summaryVars.StartRecord;

            if (bankID > 0)
            {
                workgroupSelection = bankID + "|" + clientAccountID;
            }

        }

        private BatchSummaryVars GetBatchSummaryVars()
        {
            return new BatchSummaryVars()
            {
                StartDate = dteStartDate,
                EndDate = dteEndDate,
                BankID = bankID,
                ClientAccountID = clientAccountID,
                PaymentTypeID = intPaymentType,
                PaymentSourceID = intPaymentSource,
                StartRecord = intStartRecord
            };
        }

        private void GetVarsFromQuerystring()
        {
            int intTemp;
            DateTime dteTemp;

            if (Request.QueryString["date"] != null)
            {
                if (DateTime.TryParse(CleanRequest.QueryString["date"], out dteTemp))
                {
                    dteStartDate = dteTemp;
                    dteEndDate = dteTemp;
                }
                else
                {
                    dteStartDate = DateTime.MinValue;
                    dteEndDate = DateTime.MinValue;
                }
            }

            if (dteStartDate == DateTime.MinValue && Request.QueryString["startdate"] != null && DateTime.TryParse(CleanRequest.QueryString["startdate"], out dteTemp))
            {
                dteStartDate = dteTemp;
            }

            if (dteEndDate == DateTime.MinValue && Request.QueryString["enddate"] != null && DateTime.TryParse(CleanRequest.QueryString["enddate"], out dteTemp))
            {
                dteEndDate = dteTemp;
            }
            else
            {
                dteEndDate = dteStartDate;
            }

            if (Request.QueryString["workgroupSelection"] != null )
            {
                workgroupSelection = Request.QueryString["workgroupSelection"];
            }

            if (Request.QueryString["start"] != null && int.TryParse(CleanRequest.QueryString["start"], out intTemp))
            {
                intStartRecord = intTemp;
            }


            if (Request.QueryString["paymentTypeId"] != null && int.TryParse(CleanRequest.QueryString["paymentTypeId"], out intTemp))
            {
                intPaymentType = intTemp;
            }

            if (Request.QueryString["paymentSourceId"] != null && int.TryParse(CleanRequest.QueryString["paymentSourceId"], out intTemp))
            {
                intPaymentSource = intTemp;
            }
        }

        //private void GetVarsFromQString()
        //{
        //    int intTemp;
        //    DateTime dteTemp;

        //    intStartRecord = 1;

        //    if (Request.Params["txtStartDate"] != null && DateTime.TryParse(CleanRequest.QueryString["txtStartDate"], out dteTemp))
        //    {
        //        dteStartDate = dteTemp.Date;
        //    }
        //    else
        //    {
        //        dteStartDate = DateTime.Now.Date;
        //    }

        //    if (Request.Params["txtEndDate"] != null && DateTime.TryParse(CleanRequest.QueryString["txtEndDate"], out dteTemp))
        //    {
        //        dteEndDate = dteTemp.Date;
        //    }
        //    else
        //    {
        //        dteEndDate = dteStartDate;
        //    }

        //    if (string.IsNullOrEmpty(Request.Params["txtWorkgroupSelection"]))
        //    {
        //        workgroupSelection = "";
        //    }
        //    else
        //    {
        //        workgroupSelection = CleanRequest.QueryString["txtWorkgroupSelection"];
        //    }

        //    if (Request.Form["cboPaymentType"] != null && int.TryParse(CleanRequest.QueryString["cboPaymentType"], out intTemp))
        //    {
        //        intPaymentType = intTemp;
        //    }
        //}

        private void GetVarsFromForm()
        {
            int intTemp;
            DateTime dteTemp;

            intStartRecord = 1;

            if (Request.Params["txtStartDate"] != null && DateTime.TryParse(CleanRequest.Form["txtStartDate"], out dteTemp))
            {
                dteStartDate = dteTemp.Date;
            }
            else
            {
                dteStartDate = DateTime.Now.Date;
            }

            if (Request.Params["txtEndDate"] != null && DateTime.TryParse(CleanRequest.Form["txtEndDate"], out dteTemp))
            {
                dteEndDate = dteTemp.Date;
            }
            else
            {
                dteEndDate = dteStartDate;
            }

            if (string.IsNullOrEmpty(Request.Params["txtWorkgroupSelection"]))
            {
                workgroupSelection = "";
            }
            else
            {
                workgroupSelection = CleanRequest.Form["txtWorkgroupSelection"];
            }

            if (Request.Form["cboPaymentType"] != null && int.TryParse(CleanRequest.Form["cboPaymentType"], out intTemp))
            {
                intPaymentType = intTemp;
            }
        }

        private bool GetIsPrintView()
        {
            int intTemp;

            if (CleanRequest.TryGetQueryStringValue("printview", out intTemp))
            {
                return (intTemp == 1);
            }
            else
            {
                return false;
            }
        }

        private void SetDateFields()
        {
            this.SetField("StartDate", dteStartDate.ToShortDateString());
            this.SetField("EndDate", dteEndDate.ToShortDateString());
        }

        private void GetStartRecordForPaging()
        {
            // Get the start record for paging
            if (intStartRecord < 1)
            {
                intStartRecord = 1;
            }
        }

        private XmlDocument GetXmlParmsDoc(DateTime StartDate, DateTime EndDate, int StartRecord, int? PaymentTypeID, int? PaymentSourceID, bool PrintView)
        {
            XmlDocument xmlParms;
            XmlNode node;
            string[] workgroupSelectionParms;
            int intTemp;
            bool excludeValidation = false;

            // Create parameters xml document
            xmlParms = GetRequestDoc();
            AddPageToXmlDoc(xmlParms);

            if (!string.IsNullOrEmpty(workgroupSelection) && (bankID == -1 || clientAccountID == -1))
            {
                workgroupSelectionParms = workgroupSelection.Split('|');

                if (int.TryParse(workgroupSelectionParms[0], out intTemp))
                {
                    bankID = intTemp;
                }

                if (int.TryParse(workgroupSelectionParms[1], out intTemp))
                {
                    clientAccountID = intTemp;
                }
            }


            if (bankID == -1 || clientAccountID == -1)
            {
                excludeValidation = true;
            }

            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ExcludeValidation", (excludeValidation ? "1" : "0"));
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "BankID", bankID.ToString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "ClientAccountID", clientAccountID.ToString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "StartDate", StartDate.ToShortDateString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "EndDate", EndDate.ToShortDateString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "PaginateRS", (PrintView ? "0" : "1"));
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "StartRecord", (PrintView ? "1" : StartRecord.ToString()));
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "PaymentTypeID", PaymentTypeID.ToString());
            node = ipoXmlLib.addElement(xmlParms.DocumentElement, "PaymentSourceID", PaymentSourceID.ToString());

            return (xmlParms);
        }

        #region Web Form Designer generated code
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            base.OnInit(e);
        }

        #endregion
    }
}