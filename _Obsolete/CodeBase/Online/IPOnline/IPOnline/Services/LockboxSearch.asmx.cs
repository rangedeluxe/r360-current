﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using WFS.RecHub.Common;
using WFS.RecHub.R360Services.Common.DTO;
using WFS.RecHub.R360Services.R360ServicesServicesClient;

namespace WFS.RecHub.Online.Services
{
    /// <summary>
    /// Summary description for LockboxSearch
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    [ScriptService]
    public class LockboxSearch : WebService
    {

        [WebMethod]
        public object GetDataEntryFieldsByWorkgroup(int bankid, int workgroupid)
        {
            try
            {
                var client = new R360ServiceManager();
                var datafields = client.GetDataEntryFieldsForWorkgroup(new R360Services.Common.DTO.DataEntrySearchDTO()
                {
                    SiteBankID = bankid,
                    SiteClientAccountID = workgroupid
                });

                // Grouping to prevent "duplicates".  It doesn't matter which 
                // batch source key the user selects, all will be searched in 
                // the database anyway.
                datafields.DataEntryFields = datafields.DataEntryFields
                    .GroupBy(x => new { x.DisplayName, x.DataType, x.TableName })
                    .Select(x => x.First())
                    .ToList();
                return datafields
                    .DataEntryFields
                    .AppendDefaults();
            }
            catch (Exception ex)
            {
                return new { HasError = true, ErrorMessage = ex.Message };
            }
        }

        [WebMethod]
        public object GetWorkgroup(int bankid, int workgroupid)
        {
            try
            {
                var client = new SearchServiceManager();
                var workgroup = client.GetUserWorkgroup(new R360Services.Common.DTO.WorkgroupRequestDTO
                {
                    BankID = bankid,
                    ClientAccountID = workgroupid
                });
                return workgroup
                    .Data;
            }
            catch (Exception ex)
            {
                return new { HasError = true, ErrorMessage = ex.Message };
            }
        }

    }

    public static class ServiceExtensions
    {
        public static List<DataEntryResponse> AppendDefaults(this List<DataEntryResponse> delist)
        {
            delist.InsertRange(0, new List<DataEntryResponse>()
            {
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "CheckSequence",
                    DataType = (int)DMPFieldTypes.FLOAT,
                    DisplayName = "Payment Sequence",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "Account",
                    DataType = (int)DMPFieldTypes.STRING,
                    DisplayName = "Account Number",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "Amount",
                    DataType = (int)DMPFieldTypes.CURRENCY,
                    DisplayName = "Payment Amount",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "RT",
                    DataType = (int)DMPFieldTypes.STRING,
                    DisplayName = "R/T",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "Serial",
                    DataType = (int)DMPFieldTypes.STRING,
                    DisplayName = "Check/Trace/Ref Number",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "",
                    FldName = "DDA",
                    DataType = (int)DMPFieldTypes.STRING,
                    DisplayName = "DDA",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "Payer",
                    DataType = (int)DMPFieldTypes.STRING,
                    DisplayName = "Payer",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Checks",
                    FldName = "BatchSequence",
                    DataType = (int)DMPFieldTypes.FLOAT,
                    DisplayName = "Payment Batch Sequence",
                    BatchSourceKey = 255
                },
                new DataEntryResponse()
                {
                    TableName = "Stubs",
                    FldName = "BatchSequence",
                    DataType = (int)DMPFieldTypes.FLOAT,
                    DisplayName = "Invoice Batch Sequence",
                    BatchSourceKey = 255
                },
            });
            return delist;
        }
    }
}
