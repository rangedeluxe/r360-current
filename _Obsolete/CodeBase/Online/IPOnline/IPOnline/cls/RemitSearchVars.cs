﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2014.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Smasal
* Date:     04/08/2014
*
* Purpose:  Hold Remit Search Variables for Breadcrumbs.
*
* Modification History
* WI 155808 BDH 08/20/2014
*    Changed OLLockboxId to WorkgroupId
* WI 162590 BDH 09/01/2014
*    change Payment search to add list of accounts
******************************************************************************/

namespace WFS.RecHub.Online
{
    [Serializable]
    public class RemitSearchVars
    {
        public RemitSearchVars()
        {
            WorkgroupParamsList = new List<WorkgroupParams>();
        }

        public string RemitterName { get; set; }
        public bool IsAllLockboxes { get; set; }
        public List<WorkgroupParams> WorkgroupParamsList { get; set; }
        public DateTime DepositDateFrom { get; set; }
        public DateTime DepositDateTo { get; set; }
        public Decimal AmountFrom { get; set; }
        public decimal AmountTo { get; set; }
        public string DDA { get; set; }
        public string RoutingNumber { get; set; }
        public string AccountNumber { get; set; }
        public string CheckSerialNumber { get; set; }
        public int PaymentSource { get; set; }
        public int PaymentType { get; set; }
        public string SortBy { get; set; }
        public int StartRecord { get; set; }
        public string EntityId { get; set; }
    }

    public class WorkgroupParams
    {
        public int ClientAccountID { get; set; }
        public int BankID { get; set; }
    }
}