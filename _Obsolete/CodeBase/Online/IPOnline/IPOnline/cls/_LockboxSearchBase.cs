using System;
using System.Text.RegularExpressions;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web;
using System.Xml;
using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     4/19/2010
*
* Purpose:  Creates a request to retrieve all search results as XML
*           and applies the appropriate XSL stylesheet for print view
*           or download as comma seperated.
*
* Modification History
* CR 12215 JMC 05/23/2005
*    -Created file to maintain functionality that is shared between the different 
*     Lockbox Search pages.  Functions include:
*        -SetFields()
*        -SetXMLNodes(ByRef nodeParms, ByVal PaginateRS, ByVal AppendImageSizes)
*        -GetSelectFields()
*        -ParseParameterField(ByRef vParam, ByVal vField)
*        -GetParmFields()
*        -ParseFieldData(ByRef vParam, ByVal vField)
*        -isSearchAction(ByVal action)
* CR 12864 JMC 06/16/2005
*    -Modified Xml parameters in SetXmlNodes to use appropriate datatypes.  This
*     was done to prevent an error when run using MSXML 4.0 SP1.
* CR 22114 JMC 11/06/2007
*    -Ported application to .Net from asp.
* 05/20/2010 JMC CR 29405
*     - Added jobType parameter to SetXMLNodes to prevent pagination when 
*       requesting print-views, pdf-views, and zip-files.
* CR 28865 JNE 8/10/2011
*     - Modified SetFields() to avoid duplicate code.
* CR 47801 JNE 10/28/2011
*     - GetParmFields check to make sure where parameters exist before adding to dictionary.
* CR 53947 JNE 07/18/2012
*     - Added CheckImageDisplayMode & DocumentImageDisplayMode to Setfields, SetXMLNodes.
* CR 54169 JNE 08/08/2012
*     - Added BatchIDFrom, BatchIDTo, BatchNumberFrom, BatchNumberTo 
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 85171 JMC 01/10/2013
*   -Since the Lbx Search stored procedure now requires Display Name in the 
*    Search Parameters, modified logic that parses Sort By Field and Sort By
*    Dir to also retrieve Sort By Display Name.
*      format: <TableName>.<FieldName>~<DisplayField>~<SortByDir>
*          ex: StubsDataEntry.Account_2~SDE Alpha~DESCENDING
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 85171 JMC 01/10/2013
*   -Since the Lbx Search stored procedure now requires Display Name in the 
*    Search Parameters, modified logic that parses Sort By Field and Sort By
*    Dir to also retrieve Sort By Display Name.
*      format: <TableName>.<FieldName>~<DisplayField>~<SortByDir>
*          ex: StubsDataEntry.Account_2~SDE Alpha~DESCENDING
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 90076 DRP 06/18/2013
*    - Add PaymentSource and PaymentType to Online lockbox/account search forms.
* WI 151989 TWE 07/07/2014
*    Fix IPOnline to work with new Batch ID and RAAM
* WI 152307 TWE 07/08/2014
*    fix advance search because of changes to BatchID and RAAM
* WI 159677 CEJ 08/19/2014
*   Change the DDA field's table name to empty string
* WI 132957 BDH 09/17/2014
*   Workgroup Selector implementation (single workgroup select)
* WI 221784 MR  07/13/2015
*   Use Java Script encoding to help protect against Reflected XSS attacks.
******************************************************************************/
namespace WFS.RecHub.Online {

	/// <summary>
	/// Summary description for cError.
	/// </summary>
	public class _LockboxSearchBase : _JobRequestBase {

	protected List<string> lSearchAction = new List<string>();

        //Constant value that defines the number of search criteria fields available (zero based)
        protected const int SELECTFIELDS = 5;

        public _LockboxSearchBase(){
            lSearchAction.Add("search");
            lSearchAction.Add("back");
            lSearchAction.Add("next");
            lSearchAction.Add("previous");
            lSearchAction.Add("last");
            lSearchAction.Add("first");
            lSearchAction.Add("changepagemode");
            lSearchAction.Add("searchresultsjobrequest");
            lSearchAction.Add("selsavequery");
            lSearchAction.Add("savequery");
            lSearchAction.Add("deletequery");
            lSearchAction.Add("clearquery");
            lSearchAction.Add("editname");
            lSearchAction.Sort();
        }
		
        protected void SetLockboxFields(bool bClear){

            bool bClearFields = false;

            if((bClear) || (Request.Form["txtAction"] !=null && (Request.Form["txtAction"].ToLower() == Constants.cACTION_CLEAR || Request.Form["txtAction"] == string.Empty))){
                bClearFields = true;
            }

            SetField("Action", Request.Form["txtAction"]);

            if(Request.Form["chkSelectivePrintMode"] == "on") {
                SetField("SelectivePrintMode", "1");
            } else {
                SetField("SelectivePrintMode", "0");
            }
            
            switch(Request.Form["rdoSelectivePrintImageOption"]) {
                case "PrintChecks":
                    SetField("SelectivePrintImageOption", "PrintChecks");
                    break;
                case "PrintDocuments":
                    SetField("SelectivePrintImageOption", "PrintDocuments");
                    break;
                default:
                    SetField("SelectivePrintImageOption", "PrintAll");
                    break;
            }

            SetField("WorkgroupSelection", bClearFields ? string.Empty : CleanRequest.Form["txtWorkgroupSelection"]);
            SetField("WorkgroupSelectionLabel", bClearFields ? string.Empty : CleanRequest.Form["txtWorkgroupSelectionLabel"]);

            SetField("PreDefSearchID", bClearFields ? string.Empty : CleanRequest.Form["selSaveQuery"]);
            SetField("PreDefSearchID", bClearFields?string.Empty:CleanRequest.Form["selSaveQuery"]);
            SetField("OLWorkGroupsID", bClearFields ? string.Empty : CleanRequest.Form["txtOLLockbox"]);
           // SetField("OLClientAccountID", bClearFields ? string.Empty : CleanRequest.Form["txtOLLockbox"]);
	        SetField("DepositDateFrom", bClearFields?DateTime.Today.ToShortDateString():CleanRequest.Form["txtDateFrom"]);
            SetField("DepositDateTo", bClearFields?DateTime.Today.ToShortDateString():CleanRequest.Form["txtDateTo"]);
	        SetField("BatchIDFrom", bClearFields?string.Empty:CleanRequest.Form["txtBatchIDFrom"]);
	        SetField("BatchIDTo", bClearFields?string.Empty:CleanRequest.Form["txtBatchIDTo"]);
	        SetField("BatchNumberFrom", bClearFields?string.Empty:CleanRequest.Form["txtBatchNumberFrom"]);
	        SetField("BatchNumberTo", bClearFields?string.Empty:CleanRequest.Form["txtBatchNumberTo"]);
	        SetField("AmountFrom", bClearFields?string.Empty:CleanRequest.Form["txtAmountFrom"]);
	        SetField("AmountTo", bClearFields?string.Empty:CleanRequest.Form["txtAmountTo"]);
	        SetField("Serial", bClearFields?string.Empty:CleanRequest.Form["txtSerial"]);	
            SetField("BatchSourceKey", bClearFields ? string.Empty : CleanRequest.Form["txtPaymentSource"]);
            SetField("BatchPaymentTypeKey", bClearFields ? string.Empty : CleanRequest.Form["txtPaymentType"]);
            SetField("COTSOnly", bClearFields?string.Empty:CleanRequest.Form["chkCOTSOnly"]);
            SetField("MarkSenseOnly", bClearFields?string.Empty:CleanRequest.Form["chkMarkSenseOnly"]);
            SetField("SelectivePrintMode", bClearFields?string.Empty:CleanRequest.Form["chkSelectivePrintMode"]);
            SetField("SelectivePrintImageOption", bClearFields?string.Empty:CleanRequest.Form["rdoSelectivePrintImageOption"]);
            SetField("SortBy", bClearFields?string.Empty:CleanRequest.Form["selSortBy"]);
            SetField("SavedQueryName", bClearFields?string.Empty:CleanRequest.Form["txtSavedQueryname"]);
            SetField("SavedQueryDescription",bClearFields?string.Empty:CleanRequest.Form["txtSavedQueryDescription"]);
            SetField("DefaultQuery",bClearFields?string.Empty:CleanRequest.Form["chkDefaultQuery"]);
            SetField("CheckImageDisplayMode",bClearFields?string.Empty:CleanRequest.Form["CheckImageDisplayMode"]);
            SetField("DocumentImageDisplayMode",bClearFields?string.Empty:CleanRequest.Form["DocumentImageDisplayMode"]);

        }

        protected void SetLockboxDependentFields(bool bClear){
            bool bClearFields = false;

            if((bClear) || ((Request.Form["txtAction"] !=null && (Request.Form["txtAction"].ToLower() == Constants.cACTION_CLEAR||Request.Form["txtAction"].ToLower() == "lockbox_change")))){
                bClearFields = true;
            }
            
            SetField("Join", bClearFields?string.Empty:CleanRequest.Form["selJoin"]);
            SetField("Field", bClearFields?string.Empty:CleanRequest.Form["selField"]);
            SetField("Operator", bClearFields?string.Empty:CleanRequest.Form["selOperator"]);
            SetField("Value", bClearFields?string.Empty:CleanRequest.Form["searchFieldValue"]);

	        SetField("DisplayFields", bClearFields?string.Empty:Request.Form["selSelected"]);

	        //retrieves the values in the parameter fields
	        for(int i = 1;i<= SELECTFIELDS;++i) 
            {
                SetField("Parm_Field" + (i).ToString(), bClearFields?string.Empty:Request.Form["selField" + i.ToString()]);
                SetField("Parm_Operator" + (i).ToString(), bClearFields?string.Empty:Request.Form["selOperator" + i.ToString()]);
                SetField("Parm_Value" + (i).ToString(), bClearFields?string.Empty:Request.Form["searchFieldValue" + i.ToString()]);
            }

        }

        //***************************************************************************
        //* Function		: SetXMLNodes()
        //* Author		: Eric Goetzinger
        //* Description  : Adds form data to the XML Document
        //***************************************************************************
        protected void SetXMLNodes(XmlNode nodeParms, string JobType, bool PaginateRS, bool AppendImageSizes) {

            XmlNode node;
            XmlNode nodeSelect;
            XmlNode nodeWhere;
            string strNodeValue;
            List<cSearchParm> objSelectFields;     //Collection of display fields
            Dictionary<int, cSearchParm> objParmFields;       //Collection of parameter fields   
            bool bolCOTSOnly;
            DateTime dteTemp;
            long lngStartRecord;
            long lngTemp;
            int bankID = -1;
            int clientAccountID = -1;
            int intTemp;
            string workgroupSelection;
            string[] workgroupSelectionParms;

            //Action
            node =ipoXmlLib.addElement(nodeParms, "Action", "");
            if(isSearchAction(GetField("Action"))) {
                if (GetField("Action")=="EditName"){
                    node.InnerText = "EditName";
                }else{
                    node.InnerText = "Search";
                }
            } else {
                node.InnerText = "Refine";
            }

            //PageAction
            node = ipoXmlLib.addElement(nodeParms, "PageAction", "search");

            //Bank ID and Client Account ID
            workgroupSelection = GetField("WorkgroupSelection");

            if (!string.IsNullOrEmpty(workgroupSelection))
            {
                workgroupSelectionParms = workgroupSelection.Split('|');

                if (int.TryParse(workgroupSelectionParms[0], out intTemp))
                {
                    bankID = intTemp;
                    node = ipoXmlLib.addElement(nodeParms, "BankID", bankID.ToString());
                }

                if (int.TryParse(workgroupSelectionParms[1], out intTemp))
                {
                    clientAccountID = intTemp;
                    node = ipoXmlLib.addElement(nodeParms, "ClientAccountID", clientAccountID.ToString());
                }
            }

            //WorkgroupSelection
            node = ipoXmlLib.addElement(nodeParms, "WorkgroupSelection", GetField("WorkgroupSelection"));

            //WorkgroupSelectionLabel
            node = ipoXmlLib.addElement(nodeParms, "WorkgroupSelectionLabel", GetField("WorkgroupSelectionLabel"));

            //PreDefSearchID
            node=ipoXmlLib.addElement(nodeParms, "PreDefSearchID", GetField("PreDefSearchID"));
	    
            //OLLockboxID
            //node = ipoXmlLib.addElement(nodeParms, "OLClientAccountID", GetField("OLClientAccountID"));

            //OLWorkGroupsID
            node = ipoXmlLib.addElement(nodeParms, "OLWorkGroupsID", GetField("OLWorkGroupsID"));

            //SessionID
            node = ipoXmlLib.addElement(nodeParms, "SessionID", AppSession.SessionID.ToString());

            //Date From
            node =ipoXmlLib.addElement(nodeParms, "DateFrom", "");
            if(DateTime.TryParse(GetField("DepositDateFrom"), out dteTemp)) {
                node.InnerText = DateTime.Parse(GetField("DepositDateFrom")).ToShortDateString();
            } else {
                node.InnerText = DateTime.Today.ToShortDateString();
            }
            
            //Date To
            node =ipoXmlLib.addElement(nodeParms, "DateTo", "");
            if(DateTime.TryParse(GetField("DepositDateTo"), out dteTemp)) {
                node.InnerText = DateTime.Parse(GetField("DepositDateTo")).ToShortDateString();
            } else {
                node.InnerText = DateTime.Today.ToShortDateString();
            }
            
            //Batch From
            node=ipoXmlLib.addElement(nodeParms, "BatchIDFrom", GetField("BatchIDFrom"));

            //Batch To
            node=ipoXmlLib.addElement(nodeParms, "BatchIDTo", GetField("BatchIDTo"));

            //Batch From
            node=ipoXmlLib.addElement(nodeParms, "BatchNumberFrom", GetField("BatchNumberFrom"));

            //Batch To
            node=ipoXmlLib.addElement(nodeParms, "BatchNumberTo", GetField("BatchNumberTo"));

            //Amount From
	        node =ipoXmlLib.addElement(nodeParms, "AmountFrom", GetField("AmountFrom"));

            //Amount To
	        node =ipoXmlLib.addElement(nodeParms, "AmountTo", GetField("AmountTo"));

            //Serial (Check Number)
	        node =ipoXmlLib.addElement(nodeParms, "Serial", GetField("Serial"));

            //BatchSourceKey
            node = ipoXmlLib.addElement(nodeParms, "BatchSourceKey", GetField("BatchSourceKey"));

            //BatchPaymentTypeKey
            node = ipoXmlLib.addElement(nodeParms, "BatchPaymentTypeKey", GetField("BatchPaymentTypeKey"));

            //COTS Only
            node=ipoXmlLib.addElement(nodeParms, "COTSOnly", "");
            if(GetField("COTSOnly").ToLower() == "on") {
		        node.InnerText = "True";
                bolCOTSOnly = true;
            } else {
		        node.InnerText = "False";
                bolCOTSOnly = false;
            }
           
            //MarkSenseOnly
            node=ipoXmlLib.addElement(nodeParms, "MarkSenseOnly", "");
            if(GetField("MarkSenseOnly").ToLower() == "on") {
		        node.InnerText = "True";
            } else {
		        node.InnerText = "False";
            }
                
            //SelectivePrintMode
            node=ipoXmlLib.addElement(nodeParms, "SelectivePrintMode", "");
            if(GetField("SelectivePrintMode").ToLower() == "on") {
		        node.InnerText = "True";
            } else {
		        node.InnerText = "False";
            }

            //SelectivePrintImageOption
            node=ipoXmlLib.addElement(nodeParms, "SelectivePrintImageOption", "");
            switch(GetField("SelectivePrintImageOption")) {
                case "ChecksOnly":
                    node.InnerText = "PrintChecks";
                    break;
                case "DocumentsOnly":
                    node.InnerText = "PrintDocuments";
                    break;
                default:
                    node.InnerText = "PrintAll";
                    break;
            }
            
            string[] arSortBy;
            string strSortByField;
            string strSortByDisplayName;
            string strSortByDir;

            if(GetField("SortBy").Length > 0) {
                arSortBy = GetField("SortBy").Split('~');
                if(arSortBy.Length == 3) {
                    strSortByField = arSortBy[0];
                    strSortByDisplayName = arSortBy[1];
                    strSortByDir = arSortBy[2];
                } else {
                    strSortByField = "Batch.DepositDate";
                    strSortByDisplayName = "DepositDate";
                    strSortByDir = "ASC";
                }
            } else {
                strSortByField = "Batch.DepositDate";
                strSortByDisplayName = "DepositDate";
                strSortByDir = "ASC";
            }

            //Sort By Field
            node = ipoXmlLib.addElement(nodeParms, "SortBy", "");
            node.InnerText = strSortByField;

            //Sort By Display Name
            node = ipoXmlLib.addElement(nodeParms, "SortByDisplayName", "");
            node.InnerText = strSortByDisplayName;

            //Sort By Dir
            node = ipoXmlLib.addElement(nodeParms, "SortByDir", "");
            if(strSortByDir.EndsWith(Constants.cDESCENDING)) {
                node.InnerText = "DESC";
            } else {
                node.InnerText = "ASC";
            }

            node =ipoXmlLib.addElement(nodeParms, "SavedQueryName", GetField("SavedQueryname").Trim());
            node =ipoXmlLib.addElement(nodeParms, "SavedQueryDescription", GetField("SavedQueryDescription").Trim());
             //PreDefQuery Default
            node=ipoXmlLib.addElement(nodeParms, "DefaultQuery", "");
            if(GetField("DefaultQuery").ToLower() == "on") {
		        node.InnerText = "True";
            } else {
		        node.InnerText = "False";
            }
	    

            if(JobType.ToLower() == Constants.cIPO_IMAGE_JOB_TYPE_ZIP.ToLower() ||
               JobType.ToLower() == Constants.cIPO_IMAGE_JOB_TYPE_PRINT_PDF.ToLower() ||
               JobType.ToLower() == Constants.cIPO_IMAGE_JOB_TYPE_CSV.ToLower() ||
               JobType.ToLower() == Constants.cIPO_IMAGE_JOB_TYPE_PRINT_HTML.ToLower()) {

                lngStartRecord = 1;

                //PaginateRS
                ipoXmlLib.addElement(nodeParms, "PaginateRS", "0");

            } else {

                //Start Record
                if((Request.Form["txtStart"] != null) && long.TryParse(Request.Form["txtStart"], out lngTemp) && (Request.Form["txtAction"].ToLower() != "search")) {
                    lngStartRecord = lngTemp;
                } else {
                    lngStartRecord = 1;
                }
                ipoXmlLib.addElement(nodeParms, "StartRecord", lngStartRecord.ToString());

                //PaginateRS
                ipoXmlLib.addElement(nodeParms, "PaginateRS", (PaginateRS ? "1" : "0"));
            }

            //AppendImageSizes
            ipoXmlLib.addElement(nodeParms, "AppendImageSizes", AppendImageSizes.ToString());

            //Append DisplayImageModes
            if (GetField("CheckImageDisplayMode").Length > 0){
                ipoXmlLib.addElement(nodeParms, "CheckImageDisplayMode", GetField("CheckImageDisplayMode"));
            }
            if (GetField("DocumentImageDisplayMode").Length > 0){
                ipoXmlLib.addElement(nodeParms, "DocumentImageDisplayMode", GetField("DocumentImageDisplayMode"));
            }
               
	        //Add the selected display fields
            objSelectFields = GetSelectFields();
            nodeSelect =ipoXmlLib.addElement(nodeParms, "SelectFields", "");
	        foreach(cSearchParm parm in objSelectFields) {
                if(!(bolCOTSOnly && 
                    (parm.TableName.ToLower().StartsWith("checks") ||
                     parm.TableName.ToLower().StartsWith("dim"))))
                {

		            node = ipoXmlLib.addElement(nodeSelect, "field", "");

		            ipoXmlLib.addAttribute(node, "tablename", parm.TableName);
		            ipoXmlLib.addAttribute(node, "fieldname", parm.FldName);
		            ipoXmlLib.addAttribute(node, "datatype", ((int)parm.FldDataTypeEnum).ToString());
		            ipoXmlLib.addAttribute(node, "reporttitle", parm.ReportTitle);
                    ipoXmlLib.addAttribute(node, "batchsourcekey", parm.BatchSourceKey.ToString());

		            node = null;
                }
	        }
            nodeSelect = null;
            objSelectFields = null;
        	
	        //Add the dynamic select fields last		
            objParmFields = GetParmFields();
            nodeWhere =ipoXmlLib.addElement(nodeParms, "WhereClause", "");
	        foreach(cSearchParm parm in objParmFields.Values) {
	        
		        node = ipoXmlLib.addElement(nodeWhere, "field", "");

		        ipoXmlLib.addAttribute(node, "tablename", parm.TableName);
		        ipoXmlLib.addAttribute(node, "fieldname", parm.FldName);
		        ipoXmlLib.addAttribute(node, "datatype", ((int)parm.FldDataTypeEnum).ToString());
		        ipoXmlLib.addAttribute(node, "reporttitle",  parm.ReportTitle);
		        ipoXmlLib.addAttribute(node, "operator", parm.Operator);
                ipoXmlLib.addAttribute(node, "batchsourcekey", parm.BatchSourceKey.ToString());
        		
		        //format values accordingly
		        strNodeValue = parm.Value;
		        if(strNodeValue.Trim().Length > 0) {
			        switch(parm.FldDataTypeEnum) {
				        case DMPFieldTypes.FLOAT:
					        strNodeValue = strNodeValue.Replace(",", string.Empty);
					        break;
				        case DMPFieldTypes.CURRENCY:
					        strNodeValue = strNodeValue.Replace(",", string.Empty);
					        break;
				        case DMPFieldTypes.DATE:
					        strNodeValue = DateTime.Parse(strNodeValue).ToShortDateString();
					        break;
			        }
		        }
                strNodeValue = HttpUtility.JavaScriptStringEncode(strNodeValue);
		        ipoXmlLib.addAttribute(node, "value", strNodeValue);
	        }

            nodeWhere = null;
            objParmFields = null;
            node = null;
        }


        /// <summary>
        /// Creates a dictionary object of display fields
        /// </summary>
        /// <returns></returns>
        protected List<cSearchParm> GetSelectFields() {

	        string[] arr;
	        Hashtable htSelectFieldKeys;
	        List<cSearchParm> dict;
	        cSearchParm fld;
        	
	        dict = new List<cSearchParm>();
	        htSelectFieldKeys = new Hashtable();
        	
	        if(GetField("DisplayFields").Length > 0) {

		        //arr = GetField("DisplayFields").Split(("~[TERMINATOR],").ToCharArray());
        		arr = Regex.Split(GetField("DisplayFields"), "~{TERMINATOR},");
        			
		        for(int i=0;i<arr.Length;++i) {

                    // For the last element in the array, there will be no comma in the delimiter, but
                    // the ~{TERMINATOR} will still be part of the string so it needs to be parsed out.
                    if(i==arr.Length-1) {
                        arr[i] = arr[i].Replace("~{TERMINATOR}", string.Empty);
                    }

                    if(arr[i].Trim().Length > 0) {
			            fld = new cSearchParm();
            			
			            ParseParameterField(fld, arr[i]);
            			
			            if(!htSelectFieldKeys.ContainsKey(arr[i].Trim())) {
				            htSelectFieldKeys.Add(arr[i].Trim(), null);
				            dict.Add(fld);
			            }
            			
			            fld = null;
                    }
		        }
	        }

	        return(dict);
        }

        /// <summary>
        /// Populates a field object by parsing the form field
        ///	Format - TableName~FldName~FldDataTypeEnum~ReportTitle~BatchSourceKey
        /// </summary>
        /// <param name="vParam"></param>
        /// <param name="vField"></param>
        protected void ParseParameterField(cSearchParm vParam, string vField) {

            var fields = vField.Split('~');
            int intTemp;

            if (fields.Length > 0)
                vParam.TableName = fields[0];
            if (fields.Length > 1)
                vParam.FldName = fields[1];
            if (fields.Length > 2 && int.TryParse(fields[2], out intTemp))
                vParam.FldDataTypeEnum = (DMPFieldTypes)int.Parse(fields[2]);
            if (fields.Length > 3)
                vParam.ReportTitle = fields[3];
            if (fields.Length > 4 && int.TryParse(fields[4], out intTemp))
                vParam.BatchSourceKey = intTemp;
        }


        /// <summary>
        /// Creates a dictionary object of parameter fields
        /// </summary>
        /// <returns></returns>
        protected Dictionary<int, cSearchParm> GetParmFields() {

	        Dictionary<int, cSearchParm> dict;
	        cSearchParm parm;
        	
	        dict = new Dictionary<int, cSearchParm>();

	        for(int i = 1; i <= SELECTFIELDS; ++i) {
		        parm = new cSearchParm();

			    ParseParameterField(parm, GetField("Parm_Field" + (i).ToString()));
				parm.Operator = GetField("Parm_Operator" + (i).ToString());
                parm.Value = GetField("Parm_Value" + (i).ToString());

				if(!dict.ContainsKey(i))
                { 
				    dict.Add(i, parm);
				}
        			
		        parm = null;
	        }

	        return(dict);
        }

        /// <summary>
        /// Ported function from desearch.asp
        /// </summary>
        /// <param name="vParam"></param>
        /// <param name="vField"></param>
        protected void ParseFieldData(cSearchParm vParam, string vField) {

            int x;
            int y;
            int z;
            
            x = vField.IndexOf('~', 0);
            vParam.TableName = vField.Substring(0, x - 1);
            
            y = vField.IndexOf('~', x + 1);
            vParam.FldName = vField.Substring(x + 1, y - (x + 1));
            
            z = vField.IndexOf('~', y + 1);
            vParam.FldDataTypeEnum = (DMPFieldTypes)int.Parse(vField.Substring(y + 1, z - (y  + 1)));
            
            vParam.ReportTitle = vField.Substring(vField.Length - z);
        }

        /// <summary>
        /// Determines if the action is a Pagination action.
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        protected bool isSearchAction(string action) {

            bool bolRetVal = false;
            
            if(action != null) {
		    bolRetVal = (lSearchAction.BinarySearch(action.ToLower())>=0?true:false);
            }

            return(bolRetVal);
        }
    
        protected void AppendWorkgroupData(XmlDocument doc, string workgroupKey) {
            string[] workgroupParts = GetField("WorkgroupSelection").Split(new[] { '|' });
            int bankID, workgroupID;

            if (workgroupParts.Length == 2 && int.TryParse(workgroupParts[0], out bankID) && int.TryParse(workgroupParts[1], out workgroupID)) {
                var workgroupData = R360SearchServiceClient.GetUserWorkgroup(new R360Services.Common.DTO.WorkgroupRequestDTO {
                    BankID = bankID,
                    ClientAccountID = workgroupID
                });

                var workgroupNode = doc.CreateElement("WorkgroupData");
                var displayBatchIDNode = doc.CreateElement("DisplayBatchID");
                displayBatchIDNode.InnerXml = workgroupData.Data.DisplayBatchID.ToString();
                workgroupNode.AppendChild(displayBatchIDNode);
                doc.DocumentElement.AppendChild(workgroupNode);
            }
            else {
                EventLog.logWarning(string.Format("Invalid Selected Workgroup Key \"{0}\"", workgroupKey), MessageImportance.Essential);
            }
        }

        protected class cSearchParm {

	        private string _FldName = string.Empty;
	        private string _TableName = string.Empty;
	        private DMPFieldTypes _FldDataTypeEnum = DMPFieldTypes.UNKNOWN;
	        private string _ReportTitle = string.Empty;
	        private string _Operator = string.Empty;
	        private string _Value = string.Empty;
	        private string _Join = string.Empty;

            public int BatchSourceKey { get; set; }

    	    public string FldName {
    	        get {
    	            return(_FldName);
    	        }
    	        set {
    	            _FldName = value;
    	        }
    	    }

    	    public string TableName {
    	        get {
    	            return(_TableName);
    	        }
    	        set {
    	            _TableName = value;
    	        }
    	    }

    	    public DMPFieldTypes FldDataTypeEnum {
    	        get {
    	            return(_FldDataTypeEnum);
    	        }
    	        set {
    	            _FldDataTypeEnum = value;
    	        }
    	    }

    	    public string ReportTitle {
    	        get {
    	            return(_ReportTitle);
    	        }
    	        set {
    	            _ReportTitle = value;
    	        }
    	    }

    	    public string Operator {
    	        get {
    	            return(_Operator);
    	        }
    	        set {
    	            _Operator = value;
    	        }
    	    }

    	    public string Value {
    	        get {
    	            return(_Value);
    	        }
    	        set {
    	            _Value = value;
    	        }
    	    }

    	    public string Join {
    	        get {
    	            return(_Join);
    	        }
    	        set {
    	            _Join = value;
    	        }
    	    }
        }
    }
}
