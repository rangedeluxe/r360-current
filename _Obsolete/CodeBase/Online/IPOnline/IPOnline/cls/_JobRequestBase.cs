using System;
using System.Data;
using System.IO;
using System.Web;
using System.Xml;
using WFS.RecHub.Common;
using WFS.RecHub.Common;
using WFS.RecHub.OLFServices;
using WFS.RecHub.R360Services.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     1/14/2009
*
* Purpose:  Includes utility functions required by pages that are required to submit
*           and retrieve jobs via the ipo Image Service.
*
* Modification History
* 06/01/2004 CR 7728 JMC
*   - NEW FILE: used to interact with ipo Image services.
* 03/01/2006 CR 15161 JMC
*   - Changed name of "ProcessNewRequest" to "ProcessRequest".
*   - Modified ProcessRequest function to deliver the file to download either by streaming or
*     by providing a link as necessary.
*   - Modified CheckRequestStatus to call ProcessRequest function.  Function now perfoms all
*     functions inline, and no longer requires the getimagerequest.asp file.
*   - Added StreamRequest function originally in getimagerequest.asp file.
*   - Added DownloadRequest function originally in getimagerequest.asp file.
* 08/23/2006 CR 17806 EJG
*   - Removed the StreamRequest option and modified DownloadRequest to accept new parameters
*     for file extension and auto redirect the user to the file. All files will be saved to a 
*     temp folder instead of just the larger files.
*   - Modified Processrequest to not call StreamRequest for stream requests or files where the
* 	  size is less than the threshold defined for the site.
* CR 16082 EJG 08/23/2006
*   - Changed the file name for all saved files to be the OnlineImageQueueID 
*     to guarantee unique file names.
* CR 45984 JCS 02/13/2012
*   - Updated noimage URL to account for dynamic branding.
*   - Modified DownloadRequest to add branding info to Xml for View All Images.
* WI 70429 JMC 12/17/2012
*   - Changed namespaces to use RecHub standard.
* WI 9121 WJS 3/4/2013
*   - FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*   - Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 119134 MLH 10/29/2013
*   - Created read-only property for "No Image" redirect page for use in viewallimages.aspx  
*     viewallimages.aspx was pointing to a page that didn't exist.
* WI 139003 BLR 05/05/2014    
*   - Edited to redirect to the new getpdf aspx file.
* WI 154795 TWE 07/21/2014
*   - Remove OLservices and start using the new WCF version
* WI 152429 BLR 08/07/2014   
*   - Removed old CENDS code from DownloadRequest(), which probably should have been 
*     removed as part of WI 139003. 
* WI 161064 BLR 08/26/2014
*   - Redesigned the downloading of PDFs and Zip files.  All PDFs now shown
*     in-browser, and all zips now have a download link.   
* WI 200339 JSF 04/27/2015
*   - Advanced Search - IE - Unable To Download Images For Search Results
* WI 221784 MR  07/13/2015
*   - Use a white-list on report type, to help protect against Reflected XSS attacks.
******************************************************************************/
namespace WFS.RecHub.Online {

	/// <summary>
	/// Summary description for cError.
	/// </summary>
	public class _JobRequestBase : _Page {

        protected enum OLFDeliveryMethod {
            DetermineBySize,
            Download,
            Stream
        }

        protected string NoImagePagePath
        {
            get { return (Constants.BRANDING_BASE_PATH + base.BrandingSchemeFolder + Constants.NOIMAGE); }
        }

        protected void ProcessJobRequest(DataTable RST, OLFDeliveryMethod DeliveryMethod) {

	        int lngNumTries;
	        string strJobType;
	        string strFileExtension;
	        string strUserFileName;
	        DataRow dr;

            lngNumTries = CleanRequest.GetQueryStringValue("numTries", 0);

            strJobType = CleanRequest.GetQueryStringValue("JOB_TYPE", string.Empty);

            switch(strJobType) {
                case Constants.cIPO_IMAGE_JOB_TYPE_ZIP:
                    strFileExtension = "zip";
                    DeliveryMethod = OLFDeliveryMethod.Download;
                    break;
                case Constants.cIPO_IMAGE_JOB_TYPE_PRINT_PDF:
                    strFileExtension = "pdf";
                    break;
                case Constants.cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED:
                    strFileExtension = "pdf";
                    break;
                default:
                    strFileExtension = "pdf";
                    break;
            }
            
            if(RST == null) {
                
	            // Log Error
	            EventLog.logEvent(GetField("RequestDescriptor") + " request returned nothing.", "ProcessRequest", MessageType.Warning);

                Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
            } else if(RST.Rows.Count == 0) {
                
	            // Log Error
	            EventLog.logEvent("No " + GetField("RequestDescriptor") + " request recordset returned.", "ProcessRequest", MessageType.Warning);

                Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
            } else {
            
                dr = RST.Rows[0];

                strUserFileName = dr["UserFileName"].ToString() + "." + strFileExtension;
                var status = (int)dr["ProcessStatusID"];
	            switch(status) {
		            case 1:		// Pending, Processing
                        BuildHTML((Guid)dr["OnlineImageQueueID"], strJobType, lngNumTries, status);
                        break;
		            case 2:		// Pending, Processing
                        BuildHTML((Guid)dr["OnlineImageQueueID"], strJobType, lngNumTries, status);
                        break;
		            case 3:			// Done

                        BuildHTML((Guid)dr["OnlineImageQueueID"], strFileExtension, lngNumTries, status);
                        break;
		            case 4:			// Error
			            // Log Error
			            EventLog.logEvent(dr["ProcessStatusText"].ToString().TrimEnd(), "ProcessRequest", MessageType.Information);

                        Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                        break;
		            default:
			            // Log Error
			            EventLog.logEvent("Status " + ((int)dr["ProcessStatusID"]).ToString() + " is not defined.", "ProcessRequest", MessageType.Warning);

                        Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
                        break;
	            }
            }		
        }

        protected void CheckRequestStatus(OLFDeliveryMethod DeliveryMethod) {

	        Guid gidID;
	        Guid gidTemp;
	        DataTable RST = null;
        	
        	if(ipoLib.TryParse(CleanRequest.QueryString["ID"], out gidTemp)) {
        	    
        	    gidID = gidTemp;
        	
		        try
                {
                    BaseGenericResponse<DataSet> respdoc = R360ImageServiceClient.CheckImageJobStatus(gidID);
                    if (respdoc.Status == R360Shared.StatusCode.SUCCESS)
                    {
                        RST = respdoc.Data.Tables[0];
                    }
		            //RST = ImageSrv.CheckImageJobStatus(gidID).Tables[0];
                    ProcessJobRequest(RST, DeliveryMethod);
        		} catch(Exception e) {
			        // Log Error
			        EventLog.logError(e, this.GetType().Name, "CheckRequestStatus");
                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
		        }
	        } else {
		        // Log Error
		        EventLog.logEvent("OnlineImageQueueID was missing.", "CheckRequestStatus", MessageType.Error);
                Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);
	        }
        }

        private void BuildHTML(Guid OnlineImageQueueID, string JobType, int NumTries, int status) {
            JobType = ValidateJobType(JobType);

            int lngNumTries;

		    Response.Write("<html>" + ipoLib.CRLF);
		    Response.Write("<head>" + ipoLib.CRLF);
            Response.Write("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>");
            if (status != 3 || (status == 3 && JobType.ToUpper().Equals("ZIP")))
             {
                // Need this for loading graphic images and the IE9 download link, instead of defaulting to quirk mode in IE8,IE9.
                Response.Write("<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"/>");
             }
            Response.Write("<link type=\"text/css\" href=\"/Assets/bootstrap/css/bootstrap.min.css\" rel=\"stylesheet\" />" + ipoLib.CRLF);
            Response.Write("<link type=\"text/css\" href=\"/Assets/fontawesome/css/font-awesome.min.css\" rel=\"stylesheet\" />" + ipoLib.CRLF);
            Response.Write("<link type=\"text/css\" href=\"/Assets/framework/CSS/framework.css\" rel=\"stylesheet\" />" + ipoLib.CRLF);
            Response.Write("<link type=\"text/css\" href=\"/Assets/framework/CSS/wfs-bootstrap.css\" rel=\"stylesheet\" />" + ipoLib.CRLF);
            Response.Write("<script type=\"text/javascript\" src=\"/Assets/jquery/js/jquery.min.js\"></script>" + ipoLib.CRLF);
            Response.Write("<script type=\"text/javascript\" src=\"/Assets/bootstrap/js/bootstrap.js\"></script>" + ipoLib.CRLF);
            Response.Write("<script type=\"text/javascript\" src=\"/Assets/bootstrap-modal/js/bs-modal.js\"></script>" + ipoLib.CRLF);
            Response.Write("<script type=\"text/javascript\" src=\"/Assets/etc/spin.min.js\"></script>" + ipoLib.CRLF);
            Response.Write("<script type=\"text/javascript\" src=\"/Assets/framework/JS/frameworkUtils.js\"></script>" + ipoLib.CRLF);

            if (status == 3)
            {
                if (JobType == "pdf")
                {
                    // Completed the process.  We want to dispay PDF in browser.
                    Response.Write("<title>PDF Display</title>" + ipoLib.CRLF);
                    Response.Write("</head>");
                    Response.Write("<body>" + ipoLib.CRLF);
                    Response.Write(string.Format("<iframe type=\"application/pdf\" src=\"/iponline/scripts/getpdf.aspx?type=pdf&id={0}\" width=\"100%\" height=\"100%\" />", OnlineImageQueueID.ToString()) + ipoLib.CRLF);
                    Response.Write("</body>" + ipoLib.CRLF);
                }
                else
                {
                    
                    // ZIP file download, we need to simply close the spinner.
                    Response.Write("<title>Download Ready</title>" + ipoLib.CRLF);
                    Response.Write("</head>");
                    Response.Write("<body>" + ipoLib.CRLF);
                    Response.Write("<br />");
                    Response.Write("<div class=\"container\">");
                    Response.Write("<div class=\"row\">");
                    Response.Write("<div class=\"col-xs-1\">");
                    Response.Write("<br />");
                    Response.Write(string.Format("<a href=\"/iponline/scripts/getpdf.aspx?id={0}&type={1}\"><i class=\"fa fa-download fa-5x\"></i></a>", OnlineImageQueueID.ToString(), JobType) + ipoLib.CRLF);
                    Response.Write("</div>");
                    Response.Write("<div class=\"col-xs-11\">");
                    Response.Write("<h4>Your download is Ready!</h4>");
                    Response.Write("<p>Click the icon to begin downloading.</p>");
                    Response.Write("</div>");
                    Response.Write("</div>");
                    Response.Write("</div>");
                    Response.Write("</body>" + ipoLib.CRLF);
                }
            }
            else
            {
                // Not yet completed, continue processing request like normal.
                Response.Write("<title>Processing Request...</title>" + ipoLib.CRLF);
            lngNumTries = NumTries + 1;

                if (lngNumTries < 9)
                {
		        Response.Write("<meta http-equiv=\"refresh\" content=\"3;URL=" + GetField("RequestPage") + "?id=" + Server.UrlEncode(OnlineImageQueueID.ToString()) + "&JOB_TYPE=" + JobType + "&numTries=" + lngNumTries.ToString() + "\">" + ipoLib.CRLF);
                }
                else
                {
		        Response.Write("<meta http-equiv=\"refresh\" content=\"15;URL=" + GetField("RequestPage") + "?id=" + Server.UrlEncode(OnlineImageQueueID.ToString()) + "&JOB_TYPE=" + JobType + "&numTries=" + lngNumTries.ToString() + "\">" + ipoLib.CRLF);
	        }

                // Show the Spinner
                Response.Write("<script type=\"text/javascript\"> $(function(){ var framework = new FrameworkUtils(); framework.showSpinner('Loading...'); }); </script>");
	        Response.Write("</head>" + ipoLib.CRLF);
	        Response.Write("<body>" + ipoLib.CRLF);
                Response.Write("<div class=\"frameworkContainer\">");
                Response.Write("<div class=\"row\">");
                Response.Write("<div class=\"col-xs-12\">");
                Response.Write("</div>");
                Response.Write("</div>");
                Response.Write("</div>");
	        Response.Write("</body>" + ipoLib.CRLF);
            }

            
	        Response.Write("</html>");
        }

        private void DownloadRequest(Guid OnlineImageQueueID, string UserFileName, int FileSize, string FileExtension, bool AutoRedirect) {

	        string strFileName;
	        string strFileUrl;
            // WI 139003 : Added a string to persist the GUID, we need it for the new redirect link.
            string strGuidFile;
        	
	            // set the file name to be the OnlineImageQueueID w/o { and }
                strFileName = OnlineImageQueueID.ToString().Replace("}", string.Empty);
                strFileName  = strFileName.Replace("{", string.Empty);
            strGuidFile = strFileName;

            strFileUrl = string.Format("getpdf.aspx?id={0}&type={1}", strGuidFile, FileExtension);
			            Response.Redirect(strFileUrl, false);
        }

        protected void DownloadFile(string relativeFilePath) {

	        Stream ReadStream;
        	
            try {

                WFS.RecHub.OLFServicesClient.OLFOnlineClient objClient = new WFS.RecHub.OLFServicesClient.OLFOnlineClient(this.SiteKey);

                ReadStream = objClient.GetCENDSFileAsAttachment(relativeFilePath);
                var metadata = new FileMetaData()
                {
                    SiteKey = this.SiteKey
                };
                var length = objClient.GetCENDSFileSize(metadata, relativeFilePath);
                var name = Path.GetFileName(relativeFilePath);
                var contentType = MimeMapping.GetMimeMapping(relativeFilePath);

                if(ReadStream == null) {
		            // Log Error
                    EventLog.logEvent("The requested file not found.", "DownloadCENDSFile", MessageType.Warning);

                    Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);	

	            } else {
            			
                    // Set our headers before appending the body.
                    Context.Response.Clear();
                    Response.ContentType = contentType; 
                    Response.AddHeader("Content-Length", length.ToString());
                    Response.AddHeader("Content-Disposition", string.Format("attachment; filename=\"{0}\"", name));
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetNoStore();

                    int bufferlen;
                    byte[] buffer;
                    int count;

                    bufferlen = 4096;
                    buffer = new byte[bufferlen];
                    count = 0;

                    while((count = ReadStream.Read(buffer, 0, bufferlen)) > 0) {
                        Response.OutputStream.Write(buffer, 0, count);
                    }
                    Response.Flush();
                    ReadStream.Close();
                }
        
            } catch(Exception e) {
		        // Log Error
		        EventLog.logError(e, this.GetType().Name, "DownloadRequest");
                Response.Redirect(Constants.BRANDING_BASE_PATH + BrandingSchemeFolder + Constants.NOIMAGE);	
            }

        }

        private static class DownloadUtil {

            private const int UPPER_BOUND = 2147483647;
            private const int LOWER_BOUND = 1;

            public static string GenerateFolder() {

	            string strFolderID;
	            int intFileID;
	            int i;
	            int intRnd;
            	
	            Random rnd = new Random();
	            rnd.Next();
            	
	            strFolderID = "0000000000";
            	
            	intRnd = rnd.Next();
            	
	            intFileID = Math.Abs(((UPPER_BOUND - LOWER_BOUND + 1) * intRnd + LOWER_BOUND));
            	
	            if(intFileID.ToString().Length < 10) {
		            strFolderID = intFileID.ToString();
		            i = 10 - intFileID.ToString().Length;
		            while(i > 0) {
			            strFolderID = "0" + strFolderID;
			            i -= 1;
		            }
	            } else {
		            strFolderID = intFileID.ToString();
	            }
            	
	            return(strFolderID);
            }
        }

        private string ValidateJobType(string jobType)
        {
            if(String.IsNullOrEmpty(jobType) || (!jobType.ToLower().Equals("pdf") && !jobType.ToLower().Equals("zip")))
            {
                jobType = "pdf";  // default to PDF
            }

            return jobType;
        }
    }
}