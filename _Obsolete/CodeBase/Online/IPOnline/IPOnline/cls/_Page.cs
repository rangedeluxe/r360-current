using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Linq;
using System.Web.Services.Protocols;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;

using WFS.RecHub.Common;
using WFS.RecHub.Common.Log;
using WFS.RecHub.SessionMaintenance.ServiceClient;
using WFS.RecHub.SessionMaintenance.Common;
using WFS.RecHub.R360Services.Common;
using WFS.RecHub.R360Services.R360ServicesServicesClient;
using Wfs.Raam.Core.Services;
using Wfs.Raam.Core;
using WFS.RecHub.HubConfig;
using WFS.RecHub.R360WebShared.Services;
using WFS.RecHub.R360WebShared.Helpers;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     11/08/2007
*
* Purpose:  Online Page base class.
*
* Modification History
* CR 22114 JMC 11/08/2007
*   -Ported application to .Net from asp.
* CR 28738 JMC 03/23/2010
*   -Added Redirect function.
* CR 28866 JMC 07/26/2011
*   -Eliminated FlattenCollection() function by using Server.UrlDecode()
*    instead
* CR 47058 WJS 10/5/2011
*   -Added security question support
* CR 45984 JCS 01/19/2012
*   -Modified DisplayXMLDoc() to retrieve branding folder and dynamically use
*    assigned branding scheme.
* CR 51697 JNE 04/09/2012
*   -In DisplayXMLDoc, created PathDocument from Document for speedier transformations
* CR 51575 CEJ 4/18/2012
*   - Modified DisplayXMLDoc to use the default branding path if the Logon template
*     file is not found in the given branding path and to only store the
*     branding path in the cookie if the template is found.
* CR 51480 CEJ 4/23/2012
*   - Modified DisplayXMLDoc to set the nodeBrandingFolder.InnerText =
*     DEFAULT_BRANDING_SCHEME_FOLDER when the branding folder is not found.
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard..
* WI 96311 CRG 04/30/2013
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
* WI 108109 TWE 07/08/2013
*    Change primary key of ActivityLog from Guid to Long (bigint)
* WI 116267 CEJ 10/18/2013
*   Place and check the client side security token on each page
* WI 111463 MR 04/03/2014
 *   Added X-FRAME-OPTIONS to header to help prevent cross frame scripting
 *   attacks from succceeding.
 * WI 111467 MR  04/15/2014
*   -Added header settings to disable browser caching.
* WI 143302 TWE 06/19/2014
*    Remove OLLogon Services
*    Use RAAM instead to authorize
*    Add call to SessionMaintenance Service to register session
* WI 149481 TWE 06/23/2014
*    Remove OLOrganizationID from user object
* WI 150507 TWE 06/26/2014
*    Fix Batch Summary and initial Payment Search page display
* WI 152526 JSF 06/19/2014
*   -Changed XmlReader to not allow DTD processing.
* WI 154795 TWE 07/21/2014
*    Remove OLservices and start using the new WCF version
* WI 156537 TWE 07/31/2014
*    Only call sessionMaintenance if session doesn't already exist
* WI 155811 TWE 08/06/2014
*    Add permissions node to base document - fix stored procedure call
******************************************************************************/
namespace WFS.RecHub.Online
{

    /// <summary>
    /// Summary description for _Page.
    /// </summary>
    public class _Page : global::System.Web.UI.Page
    {

        private const int AUDIT_MAX_LEN = 100;

        private SessionMaintenanceManager _SessionMaintenanceService = null;
        private AlertServiceManager _R360AlertService = null;
        private CBXServiceManager _R360CBXService = null;
        private ImageServiceManager _R360ImageService = null;
        private RemitterServiceManager _R360RemitterService = null;
        private ReportServiceManager _R360ReportService = null;
        private SearchServiceManager _R360SearchService = null;
        private SystemServiceManager _R360SystemService = null;
        private UserServiceManager _R360UserService = null;
        private HubConfigServicesManager _HubConfigService = null;


        private string _SiteKey = string.Empty;
        private cEventLog _EventLog = null;
        private cSiteOptions _SiteOptions = null;
        private string _BrandingSchemeFolder = "Online.NextGen";

        private List<cMsg> _Errors = null;
        private List<cMsg> _Messages = null;
        private List<cMsg> _Warnings = null;

        private long _ActivityLogID = 0;

        private StringDictionary _PageFields;

        private cSystem _AppSystem = null;
        private cSession _AppSession = null;
        private cUserRAAM _AppRaamUser = null;

        private cCleanRequest _CleanRequest = null;

        private bool _SupressInitialize = false;

        private const string DEFAULT_BRANDING_SCHEME_FOLDER = "Online.NextGen";

        public _Page()
        {
        }

        public _Page(bool SupressInitialize)
        {
            _SupressInitialize = SupressInitialize;
        }

        //~_Page()
        //{ //Destructor
        //}

        private void Page_Init(object sender, EventArgs e)
        {
            LocalInit();
        }

        protected long ActivityLogID
        {
            get
            {
                return (_ActivityLogID);
            }
            set
            {
                _ActivityLogID = value;
            }
        }

        /// <summary>
        /// Uses SiteKey to retrieve site options from the local .ini file.
        /// </summary>
        protected cSiteOptions SiteOptions
        {
            get
            {
                if (_SiteOptions == null)
                {
                    _SiteOptions = new cSiteOptions(this.SiteKey);
                }

                return _SiteOptions;
            }
        }

        /// <summary>
        /// Logging component using settings from the local siteOptions object.
        /// </summary>
        protected cEventLog EventLog
        {
            get
            {
                if (_EventLog == null)
                {
                    _EventLog = new cEventLog(SiteOptions.logFilePath,
                                              SiteOptions.logFileMaxSize,
                                              (MessageImportance)SiteOptions.loggingDepth);
                }

                return _EventLog;
            }
        }

        protected void AddMessagesFromXml(XmlNode nodeRoot)
        {

            XmlNode nodeMessages;
            XmlNode nodeType;
            XmlNode nodeText;
            cMsg objMsg;

            if (nodeRoot != null)
            {
                nodeMessages = nodeRoot.SelectSingleNode("Messages");

                if (nodeMessages != null)
                {
                    foreach (XmlNode nodeMsg in nodeMessages.ChildNodes)
                    {

                        objMsg = new cMsg();

                        nodeType = nodeMsg.Attributes.GetNamedItem("Type");
                        if (nodeType != null)
                        {
                            switch (nodeType.InnerText)
                            {
                                case "Information":
                                    objMsg.Code = EventType.Information;
                                    break;
                                case "Warning":
                                    objMsg.Code = EventType.Warning;
                                    break;
                                case "Error":
                                    objMsg.Code = EventType.Error;
                                    break;
                                default:
                                    objMsg.Code = EventType.Information;
                                    break;
                            }
                        }
                        else
                        {
                            objMsg.Code = EventType.Information;
                        }

                        nodeText = nodeMsg.Attributes.GetNamedItem("Text");
                        if (nodeText != null)
                        {
                            objMsg.Message = nodeText.InnerText;
                        }

                        if (objMsg.Message.Trim().Length > 0)
                        {
                            AddMessage(objMsg);
                        }
                    }
                }
            }
        }

        protected void AddMessage(EventType vCode, string vMsg)
        {

            cMsg objMsg;

            objMsg = new cMsg();
            objMsg.Code = vCode;
            objMsg.Message = vMsg;

            AddMessage(objMsg);
        }

        protected void AddMessage(cMsg msg)
        {

            if (msg == null) { return; }

            switch (msg.Code)
            {
                case EventType.Information:
                    if (_Messages == null)
                    {
                        _Messages = new List<cMsg>();
                    }
                    _Messages.Add(msg);
                    break;
                case EventType.Warning:
                    if (_Warnings == null)
                    {
                        _Warnings = new List<cMsg>();
                    }
                    _Warnings.Add(msg);
                    break;
                case EventType.Error:
                    if (_Errors == null)
                    {
                        _Errors = new List<cMsg>();
                    }
                    _Errors.Add(msg);
                    break;
            }
        }

        protected List<cMsg> Errors
        {
            get
            {
                if (_Errors == null)
                {
                    _Errors = new List<cMsg>();
                }
                return (_Errors);
            }
        }

        protected List<cMsg> Messages
        {
            get
            {
                if (_Messages == null)
                {
                    _Messages = new List<cMsg>();
                }
                return (_Messages);
            }
        }

        protected List<cMsg> Warnings
        {
            get
            {
                if (_Warnings == null)
                {
                    _Warnings = new List<cMsg>();
                }
                return (_Warnings);
            }
        }

        /// <summary>
        /// Determines the section of the local .ini file to be used for local
        /// options.
        /// </summary>
        protected string SiteKey
        {
            get
            {
                StringCollection arrSites;
                string[] arrEntry;

                if (_SiteKey == string.Empty)
                {

                    // Retrieve null separated list of key/site value pairs and separate into array
                    arrSites = ipoINILib.GetINISection("Sites");

                    foreach (string strSite in arrSites)
                    {
                        if (strSite.Length > 0)
                        {
                            arrEntry = strSite.Split('=');	// Split site key/value pair into array
                            if (Request.ServerVariables["SERVER_NAME"] == arrEntry[arrEntry.GetUpperBound(0)])
                            {
                                _SiteKey = arrEntry[arrEntry.GetLowerBound(0)];
                                break;
                            }
                        }
                    }
                }
                return (_SiteKey);
            }
        }

        protected string BrandingSchemeFolder
        {
            get { return (_BrandingSchemeFolder); }
            set { _BrandingSchemeFolder = value; }
        }

        protected SessionMaintenanceManager SessionMaintenanceServiceClient
        {
            get
            {
                if (_SessionMaintenanceService == null)
                {
                    _SessionMaintenanceService = new SessionMaintenanceManager();
                }
                return _SessionMaintenanceService;
            }
        }

        protected AlertServiceManager R360AlertServiceClient
        {
            get
            {
                if (_R360AlertService == null)
                {
                    _R360AlertService = new AlertServiceManager();
                }
                return _R360AlertService;
            }
        }

        protected CBXServiceManager R360CBXServiceClient
        {
            get
            {
                if (_R360CBXService == null)
                {
                    _R360CBXService = new CBXServiceManager();
                }
                return _R360CBXService;
            }
        }

        protected ImageServiceManager R360ImageServiceClient
        {
            get
            {
                if (_R360ImageService == null)
                {
                    _R360ImageService = new ImageServiceManager();
                }
                return _R360ImageService;
            }
        }

        protected RemitterServiceManager R360RemitterServiceClient
        {
            get
            {
                if (_R360RemitterService == null)
                {
                    _R360RemitterService = new RemitterServiceManager();
                }
                return _R360RemitterService;
            }
        }

        protected ReportServiceManager R360ReportServiceClient
        {
            get
            {
                if (_R360ReportService == null)
                {
                    _R360ReportService = new ReportServiceManager();
                }
                return _R360ReportService;
            }
        }

        protected SearchServiceManager R360SearchServiceClient
        {
            get
            {
                if (_R360SearchService == null)
                {
                    _R360SearchService = new SearchServiceManager();
                }
                return _R360SearchService;
            }
        }

        protected SystemServiceManager R360SystemServiceClient
        {
            get
            {
                if (_R360SystemService == null)
                {
                    _R360SystemService = new SystemServiceManager();
                }
                return _R360SystemService;
            }
        }

        protected UserServiceManager R360UserServiceClient
        {
            get
            {
                if (_R360UserService == null)
                {
                    _R360UserService = new UserServiceManager();
                }
                return _R360UserService;
            }
        }

        protected HubConfigServicesManager HubConfigServicesClient
        {
            get
            {
                if (_HubConfigService == null)
                {
                    _HubConfigService = new HubConfigServicesManager();
                }
                return _HubConfigService;
            }
        }

        //------------------------------------------------------------------------

        protected void AbortIfNoXml(XmlDocument xmlDocument, string origin)
        {
            if (xmlDocument == null || xmlDocument.DocumentElement == null)
            {
                AbortWithBadRequest(origin + " failed to return data");
            }
        }

        private void AbortWithBadRequest(string logMessage)
        {
            EventLog.logEvent(logMessage, GetType().Name, MessageType.Error, MessageImportance.Essential);
            Page.Response.Clear();
            Page.Response.StatusCode = 400;
            Page.Response.End();
        }

        protected string GetTemplateFileName(string sFullBrandingPath, string sTemplateName)
        {
            return Path.Combine(sFullBrandingPath, "templates", sTemplateName + (sTemplateName.EndsWith(".xsl") ? "" : ".xsl"));
        }

        /// <summary>
        /// This is to populate the links for the breadcrumbs.  Since we're slowly converting these over
        /// to portlets, we need to call the framework to figure out where we need to go.
        /// </summary>
        /// <param name="docXml"></param>
        protected void PopulateBreadCrumbLinks(XmlDocument docXml)
        {
            var client = new FrameworkClient.TabManager();
            var tabs = client.GetTabs();
            var id = tabs.First(x => x.Name == "Batch Summary").Id;
            var url = Request.Url.GetLeftPart(UriPartial.Authority) + ResolveUrl("~/../Framework/Framework?tab=") + id;

            ipoXmlLib.addAttribute(docXml.DocumentElement, "BatchSummaryURL", url.ToString());
        }


        protected bool DisplayXMLDoc(XmlDocument docXml, string scriptName)
        {

            XmlReaderSettings settings;
            XmlReader reader = null;
            XslCompiledTransform transform = null;
            XPathNavigator navXml;
            //XmlNode nodeBrandingFolder = null;
            string strTemplate = string.Empty;
            string strTemplateFile = string.Empty;

            settings = new XmlReaderSettings();
            settings.DtdProcessing = DtdProcessing.Parse;
            // Our Input is varies and is larger than 1024 so we stepped up to a much larger size;
            settings.MaxCharactersFromEntities = 12288;
            settings.XmlResolver = null;
            // Remove script extension
            if (scriptName.IndexOf('.') > -1)
            {
                strTemplate = scriptName.Substring(0, scriptName.LastIndexOf('.'));
            }
            else
            {
                strTemplate = scriptName;
            }

            LinkSecurityToken(ref docXml);


            strTemplateFile = cBrandingMethods.GetTemplateFileName(this, DEFAULT_BRANDING_SCHEME_FOLDER, strTemplate);
            try
            {
                // load template file from branding path
                reader = XmlReader.Create(strTemplateFile, settings);
                transform = new XslCompiledTransform(false);
                transform.Load(reader);
            }
            catch (XsltException ex)
            {
                string scriptPageName = this.GetType().Name.ToString();
                string userSessionId = AppSession.SessionID.ToString();
                EventLog.logEvent("XML File " + strTemplateFile.ToString() + " Size exceeded or Potential XML Injection detected on Page" + scriptPageName + " by Session Id " + userSessionId, "_Page", MessageType.Warning, MessageImportance.Essential);
            }

            //This describes the issue, and has a solution we could use if we were using MVC:
            //http://stackoverflow.com/questions/13119340/ie6-8-unable-to-download-file-from-https-site

            //Here�s an article that describes caching in general:
            //http://www.mnot.net/cache_docs/

            Response.AddHeader("Cache-Control", "no-store");
            Response.AddHeader("Cache-Control", "no-cache");
            Response.AddHeader("Cache-Control", "must-revalidate");
            Response.AddHeader("Expires", "-1");

            // Add any page messages to the xml document
            AddMessagesToXMLDoc(docXml);

            if (SiteOptions.OnlineAPIXmlSavePath.Length > 0 &&
               Directory.Exists(SiteOptions.OnlineAPIXmlSavePath) &&
               docXml.HasChildNodes)
            {
                docXml.Save(Path.Combine(SiteOptions.OnlineAPIXmlSavePath, DateTime.Now.ToString("yyyyMMdd-HHmmss") + "_" + strTemplate + "_" + Guid.NewGuid().ToString() + ".xml"));
            }

            //CR 51697 Created PathDocument from Document for speedier transformations.
            XPathDocument pathDocXml = new XPathDocument(new XmlNodeReader(docXml));
            navXml = pathDocXml.CreateNavigator();
            reader.Close();

            Response.ContentEncoding = Encoding.GetEncoding("ISO-8859-1");

            transform.Transform(navXml, null, Response.OutputStream);
            Response.AddHeader("X-FRAME-OPTIONS", "SAMEORIGIN");

            return (true);
        }

        private void LinkSecurityToken(ref XmlDocument docXml)
        {
            if (Session["Security_Token"] == null)
            {
                Session.Add("Security_Token", Guid.NewGuid().ToString());
            }

            if (docXml.DocumentElement.HasAttribute("Security_Token"))
            {
                docXml.DocumentElement.SetAttribute("Security_Token", Session["Security_Token"].ToString());
            }
            else
            {
                ipoXmlLib.addAttribute(docXml.DocumentElement, "Security_Token", Session["Security_Token"].ToString());
            }
        }

        protected static XmlDocument GetRequestDoc()
        {
            return (GetRequestDoc("Root"));
        }

        protected static XmlDocument GetRequestDoc(string RootNodeName)
        {

            XmlDocument docReqXml;
            XmlNode nodeRoot;

            docReqXml = new XmlDocument();
            nodeRoot = docReqXml.CreateNode(XmlNodeType.Element, RootNodeName, docReqXml.NamespaceURI);
            docReqXml.AppendChild(nodeRoot);

            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqID", Guid.NewGuid().ToString());
            ipoXmlLib.addAttribute(docReqXml.DocumentElement, "ReqDateTime", DateTime.Now.ToString());

            return (docReqXml);
        }

        protected static XmlDocument GetPageDoc(string title)
        {

            XmlDocument docPageXml;
            XmlNode nodeRoot;

            docPageXml = new XmlDocument();
            nodeRoot = docPageXml.CreateNode(XmlNodeType.Element, "Page", docPageXml.NamespaceURI);
            docPageXml.AppendChild(nodeRoot);

            ipoXmlLib.addElement(docPageXml.DocumentElement, "Title", title);

            return (docPageXml);
        }

        protected StringDictionary PageFields
        {
            get
            {
                if (_PageFields == null)
                {
                    _PageFields = new StringDictionary();
                }

                return (_PageFields);
            }
        }

        protected string GetField(string FieldName)
        {

            if (PageFields.ContainsKey(FieldName) && PageFields[FieldName] != null)
            {
                return (PageFields[FieldName].ToString());
            }
            else
            {
                return (string.Empty);
            }
        }

        protected bool SetField(string FieldName, string FieldValue)
        {

            if (PageFields.ContainsKey(FieldName))
            {
                PageFields[FieldName] = FieldValue;
            }
            else
            {
                PageFields.Add(FieldName, FieldValue);
            }

            return (true);
        }

        protected string GetScriptFile()
        {

            Regex objExp = new Regex("(.*)(/)(.*$)", RegexOptions.IgnoreCase);
            string strRetVal;

            strRetVal = objExp.Replace(Request.ServerVariables["SCRIPT_NAME"], "$3");

            return (strRetVal);
        }

        protected void AddPageToXmlDoc(XmlDocument xmlDoc)
        {

            if (xmlDoc == null)
            {
                xmlDoc = ipoXmlLib.GetXMLDoc("Page");
            }
            else if (xmlDoc.DocumentElement == null)
            {
                ipoXmlLib.addElement(xmlDoc, "Page");
            }

            ipoXmlLib.addAttribute(xmlDoc.DocumentElement, "ActivityLogID", this.ActivityLogID.ToString());
        }

        private void Page_Load(object sender, EventArgs e)
        {

        }

        private static XmlNode AddMessageToDoc(XmlDocument xmlDoc, EventType errorType, string errorText)
        {

            XmlNode nodeMessages;
            XmlNode nodeMessage;

            nodeMessages = xmlDoc.DocumentElement.SelectSingleNode("Messages");

            if (nodeMessages == null)
            {
                nodeMessages = ipoXmlLib.addElement(xmlDoc.DocumentElement, "Messages");
            }

            nodeMessage = ipoXmlLib.addElement(nodeMessages, "Message");
            switch (errorType)
            {
                case EventType.Error:
                    ipoXmlLib.addAttribute(nodeMessage, "Type", "Error");
                    break;
                case EventType.Information:
                    ipoXmlLib.addAttribute(nodeMessage, "Type", "Information");
                    break;
                case EventType.Warning:
                    ipoXmlLib.addAttribute(nodeMessage, "Type", "Warning");
                    break;
                default:
                    ipoXmlLib.addAttribute(nodeMessage, "Type", "Message");
                    break;
            }

            ipoXmlLib.addAttribute(nodeMessage, "Text", errorText.Trim());

            return (nodeMessage);
        }

        private void AddMessagesToXMLDoc(XmlDocument xmlDoc)
        {

            if (this.Errors != null)
            {
                foreach (cMsg msg in this.Errors)
                {
                    AddMessageToDoc(xmlDoc, EventType.Error, msg.Message);
                }
            }
            if (this.Warnings != null)
            {
                foreach (cMsg msg in this.Warnings)
                {
                    AddMessageToDoc(xmlDoc, EventType.Warning, msg.Message);
                }
            }
            if (this.Messages != null)
            {
                foreach (cMsg msg in this.Messages)
                {
                    AddMessageToDoc(xmlDoc, EventType.Information, msg.Message);
                }
            }
        }

        protected bool CheckForSecurityToken()
        {
            bool bResult = true;

            if (Request.HttpMethod == "POST")
            {
                if (Session["Security_Token"] != null)
                {
                    if (Request.Params["hdtSecurity_Token"] == null)
                    {
                        if (Request.Params["hdtErrTyp"] == null)
                        {
                            EventLog.logEvent("Potential CSRF attack detected and handled", "_Page", MessageType.Warning, MessageImportance.Essential);
                        }
                        else
                        {
                            EventLog.logEvent(string.Format("Potential {0} detected and handled", Request.Params["hdtErrTyp"]), "_Page", MessageType.Warning, MessageImportance.Essential);
                        }
                        bResult = false;
                    }
                    else
                    {
                        if ((Session["Security_Token"] == null) ||
                                (Request.Params["hdtSecurity_Token"].ToString() != (string)Session["Security_Token"]))
                        {
                            EventLog.logEvent("Potential CSRF attack detected and handled", "_Page", MessageType.Warning, MessageImportance.Essential);
                            bResult = false;
                        }
                    }
                }
            }
            return bResult;
        }


        protected void LocalInit()
        {

            EventLog.logEvent("LocalInit:  starting...for '" + Request.ServerVariables["SERVER_NAME"] + "'.",
                Request.ServerVariables["SERVER_SOFTWARE"],
                MessageType.Information);

            bool blnLogonSuccessful = false;
            bool bolPageInit;

            long lActivityLogID = 0;

            if (_SupressInitialize)
            {
                return;
            }

            // only go out and establish session if it doesn't currently exist.
            string sessionIDstr = WSFederatedAuthentication.ClaimsAccess.GetClaimValue(WfsClaimTypes.SessionId);
            BaseGenericResponse<cUserRAAM> respUsr = R360SystemServiceClient.GetSessionUser();
            if (respUsr.Status == R360Shared.StatusCode.SUCCESS)
            {
                AppSession.SessionID = new Guid(sessionIDstr);
                blnLogonSuccessful = true;
            }
            else
            {
                EstablishSessionResponse resp = SessionMaintenanceServiceClient.EstablishSession();
                if (resp.Status == R360Shared.StatusCode.SUCCESS)
                {
                    AppSession.SessionID = resp.Data.Session;
                    blnLogonSuccessful = true;
                }
            }

            //======================================================================================
            // VerifySSLStatus();

            if (blnLogonSuccessful)
            {

                try
                {
                    bolPageInit = false;
                    var pairs = AuditHelper.GetFieldsFromQueryString(Request.QueryString.ToString());
                    var filter = new WfsAuditFilter();
                    pairs = filter.ApplyFilter(pairs);
                    // Replacement for old script names.
                    var desc = this.ScriptFile.Split('.')[0]
                        .Replace("remitdisplay", "batchdetail")
                        .Replace("remitsearch", "paymentsearch")
                        .Replace("lockboxsearch", "advancedsearch");

                    if (pairs.Any())
                        desc += " QueryString: ";
                    desc += AuditHelper.BuildAuditMessage(pairs);

                    var formCollection = Request.Form;
                    // Determine the type of audit event, based on the input values and result type
                    string auditEvent = "Web Request";	// Default value
                    string auditType = "Page View";	// Default value
                    if (formCollection.Count > 0)
                        auditEvent = "Submitted / Retrieved Data";
                    else
                        auditEvent = "Viewed Data";

                    // Deal with Form Fields
                    if (formCollection.Count > 0)
                    {
                        var formpairs = formCollection
                            .Cast<string>()
                            .Select(key => new KeyValuePair<string, string>(key, formCollection[key]))
                            .ToList();

                        formpairs = filter.ApplyFilter(formpairs);
                        if (formpairs.Count > 0)
                        {
                            desc += " Fields: ";
                            var formmessage = AuditHelper.BuildAuditMessage(formpairs);
                            desc += formmessage;
                        }
                    }

                    var manager = new R360ServiceManager();
                    var response = manager.WriteAuditEvent(auditEvent, auditType, this.ScriptFile, desc);

                    // Also startup the page activity - this is required for the breadcrumbs.
                    PageActivityResponse pageActResp = R360SystemServiceClient.InitPageActivity(this.ScriptFile,
                                                             Server.UrlDecode(Request.QueryString.ToString()));
                    if (response.Status == R360Shared.StatusCode.SUCCESS && pageActResp.Status == R360Shared.StatusCode.SUCCESS)
                    {
                        bolPageInit = true;
                    }
                }
                catch (SoapException ex)
                {
                    EventLog.logError(ex);
                    lActivityLogID = 0;
                    bolPageInit = false;
                }

                if (CheckForSecurityToken())
                {

                    if (bolPageInit)
                    {
                        // Log page request information
                        this.ActivityLogID = lActivityLogID;

                        //TODO:  add page security check????
                        //// Check that user has basic access to page.
                        //if (!bolUserHasPagePermission)
                        //{
                        //    Response.Redirect("accessdenied.aspx");
                        //}
                    }
                }
            }
        }

        protected cSession AppSession
        {
            get
            {
                if (_AppSession == null)
                {
                    _AppSession = new cSession();
                }

                return (_AppSession);
            }
        }

        protected cSystem AppSystem
        {
            get
            {
                if (_AppSystem == null)
                {
                    _AppSystem = new cSystem(this.SiteKey);
                }
                return (_AppSystem);
            }
        }

        protected cUserRAAM AppRaamUser
        {
            get
            {
                if (_AppRaamUser == null)
                {
                    BaseGenericResponse<cUserRAAM> resp = R360SystemServiceClient.GetSessionUser();
                    _AppRaamUser = resp.Data;
                }
                return (_AppRaamUser);
            }
        }

        protected string ScriptFile
        {
            get
            {
                Regex regex = new Regex("(.*)(/)(.*$)");

                return (regex.Replace(Request.ServerVariables["SCRIPT_NAME"].ToString(), "$3"));
            }
        }

        private void VerifySSLStatus()
        {

            string strSSLURL;
            int intTemp;

            if (AppSystem.RequireSSL)
            {

                if (Request.ServerVariables["SERVER_PORT_SECURE"] != null &&
                   Request.ServerVariables["SERVER_PORT_SECURE"] == "0" &&
                   Request.ServerVariables["SERVER_NAME"] != null &&
                   Request.ServerVariables["URL"] != null)
                {

                    // Redirect user to SSL site.
                    if (AppSystem.SSLPort == 443)
                    {
                        strSSLURL = "https://" + Request.ServerVariables["SERVER_NAME"] + Request.ServerVariables["URL"];
                    }
                    else
                    {
                        strSSLURL = "https://" + Request.ServerVariables["SERVER_NAME"] + ":" + AppSystem.SSLPort + Request.ServerVariables["URL"];
                    }

                    if (Request.ServerVariables["QUERY_STRING"] != null && Request.ServerVariables["QUERY_STRING"].Length > 0)
                    {
                        strSSLURL += "?" + Request.ServerVariables["QUERY_STRING"];
                    }

                    Response.Redirect(strSSLURL);
                }

                // Check if browser encryption is strong enough
                if (Request.ServerVariables["HTTPS_KEYSIZE"] != null &&
                   int.TryParse(Request.ServerVariables["HTTPS_KEYSIZE"], out intTemp) &&
                   intTemp < AppSystem.SSLKeysize)
                {

                    Response.Redirect("sslrequirements.aspx");
                }
            }
        }

        protected void ProcessSoapException(SoapException ex)
        {
            if (ex != null && ex.Detail != null && ex.Detail is XmlNode)
            {
                if (ex.Detail.SelectNodes("Msg[@MsgText != '']").Count > 0)
                {
                    foreach (XmlNode msgNode in ex.Detail.SelectNodes("Msg[@MsgText != '']"))
                    {
                        AddMessage(EventType.Error, Server.HtmlDecode(msgNode.Attributes["MsgText"].InnerText));
                    }
                }
            }
            else
            {
                AddMessage(EventType.Error, ex.Message);
            }
        }

        protected cCleanRequest CleanRequest
        {
            get
            {

                if (_CleanRequest == null)
                {
                    _CleanRequest = new cCleanRequest(this.SiteKey, Request);
                }

                return (_CleanRequest);
            }
        }

        protected void DestroyResponseCookie(string Key)
        {
            Response.Cookies[Key].Value = string.Empty;
            Response.Cookies[Key].Expires = DateTime.Now.AddDays(-1);
        }

        protected void AddMessagesToPage(XmlNode nodeRespXml)
        {

            foreach (XmlNode nodeMsg in nodeRespXml.SelectNodes("//Messages/Message"))
            {
                switch (nodeMsg.Attributes.GetNamedItem("Type").InnerText.ToLower())
                {   //.Attributes("Type").Value.ToLower())
                    case "warning":
                        AddMessage(EventType.Warning, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                    case "error":
                        AddMessage(EventType.Error, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                    default:    //"information"
                        AddMessage(EventType.Information, nodeMsg.Attributes.GetNamedItem("Text").InnerText);
                        break;
                }
            }
        }

        protected bool DocumentContainsErrors(XmlNode nodeRespXml)
        {

            return (nodeRespXml.SelectNodes("//Messages/Message[@Type='error' or @Type='Error']").Count > 0);
        }

        public void Redirect(string Url, Dictionary<string, string> QueryStringParms)
        {
            string strUrl;
            bool bolIsFirst;

            strUrl = Url;

            bolIsFirst = true;
            foreach (KeyValuePair<string, string> parm in QueryStringParms)
            {
                if (bolIsFirst)
                {
                    strUrl += "?";
                    bolIsFirst = false;
                }
                else
                {
                    strUrl += "&";
                }
                strUrl += Server.UrlEncode(parm.Key);
                strUrl += "=";
                strUrl += Server.UrlEncode(parm.Value);
            }

            Response.Redirect(strUrl);
        }

        protected void AddBrandingInfoToXml(XmlNode nodeParent, string BrandingSchemeFolder)
        {
            XmlNode nodeUserInfo = nodeParent.SelectSingleNode("UserInfo");
            if (nodeUserInfo == null)
            {
                nodeUserInfo = ipoXmlLib.addElement(nodeParent, "UserInfo");
            }
            XmlNode nodeBranding = ipoXmlLib.addElement(nodeUserInfo, "BrandingScheme");
            ipoXmlLib.addAttribute(nodeBranding, "BrandingSchemeFolder", BrandingSchemeFolder);
        }
    }
}
