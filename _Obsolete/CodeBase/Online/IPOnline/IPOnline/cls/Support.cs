using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
////using System.Web.UI;
////using System.Web.UI.WebControls;
////using System.Web.UI.WebControls.WebParts;
////using System.Web.UI.HtmlControls;

/***************************************************************************
*
*      Module: constants.asp
*    Filename: _Page.cs
*      Author: Joel Caples
* Description: Contains references for all publicly used constants.
*
* Revisions:
*
* ----------------------
* 05.13.2003    JMC
*    -Created module to contain definitions for publicly used constants.
* 12.19.2003    JMC
*    -Added constants for IPO Service Xml functions.
* CR 11337 JMC 01/18/2005
*    -Added cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED constant.
* CR 10537 JMC 03/03/2005
*    -Added activityType constants
* CR 22114 JMC 11/06/2007
*    -Ported application to .Net from asp.
**************************************************************************/
namespace WFS.integraPAY.Online.IPOnline {

    public class Constants {

        public const string cCHECKED = "on";

        public const string cACTION_CLEAR = "clear";

        public const string cICON_CHECK = "../Styles/Online/images/cbo_icon_check.gif";
        public const string cICON_DOCUMENT = "../Styles/Online/images/cbo_icon_document.gif";
        public const string cICON_BATCH_DETAIL = "../Styles/Online/images/cbo_icon_batchdetail.gif";
        public const string cICON_COTS = "../Styles/Online/images/cbo_icon_cots.gif";
        public const string cICON_MARK_SENSE = "../Styles/Online/images/cbo_icon_mark_sense.gif";

        public const string cASCENDING = "ASCENDING";
        public const string cDESCENDING = "DESCENDING";


        //*************************************************************************
        //* IPO Image Service Constants
        //*************************************************************************
        public const string cIPO_IMAGE_JOB_TYPE_ZIP = "ZIP";
        public const string cIPO_IMAGE_JOB_TYPE_PRINT_PDF = "PDF";
        public const string cIPO_IMAGE_JOB_TYPE_PRINT_HTML = "IPO_IMAGE_JOB_TYPE_PRINT_HTML";
        public const string cIPO_IMAGE_JOB_TYPE_CSV = "IPO_IMAGE_JOB_TYPE_CSV";
        public const string cIPO_IMAGE_JOB_TYPE_VIEW_SELECTED = "IPO_IMAGE_JOB_TYPE_VIEW_SELECTED";

        public const string XML_ELE_JOB_REQ_OPTIONS = "jobRequestOptions";
        public const string XML_ATTR_RETURN_TYPE = "returnType";

        //***** Outputs Node
        public const string XML_ELE_OUTPUTS = "outputs";

        //***** Output Options
        public const string XML_ELE_OUTPUT = "output";
        public const string XML_ATTR_OUTPUT_FORMAT = "outputFormat";

        //***** Text Options
        public const string XML_ELE_TEXT_OPTIONS = "textOptions";
        public const string XML_ATTR_TEXT_FILE_EXT = "fileExtension";

        public const string FILE_EXT_PDF = "pdf";
        public const string FILE_EXT_ZIP = "zip";
        public const string FILE_EXT_JPG = "jpg";
        public const string FILE_EXT_XML = "xml";
        public const string FILE_EXT_HTML = "html";
        public const string FILE_EXT_TIF = "tif";
        public const string FILE_EXT_CSV = "csv";

        public const string NOIMAGE = "../Styles/Online/noimage.aspx";

        public const string QS_PARM_OLLOCKBOXID = "lckbx";
        public const string QS_PARM_BANKID = "bank";
        public const string QS_PARM_LOCKBOXID = "lbx";
        public const string QS_PARM_BATCHID = "batch";
        public const string QS_PARM_DEPOSITDATE = "date";
        public const string QS_PARM_PICSDATE = "picsdate";
        public const string QS_PARM_TXNID = "txn";
        public const string QS_PARM_BATCHSEQUENCE = "item";
        public const string QS_PARM_RT = "rt";
        public const string QS_PARM_ACCOUNT = "acct";
    }
}
