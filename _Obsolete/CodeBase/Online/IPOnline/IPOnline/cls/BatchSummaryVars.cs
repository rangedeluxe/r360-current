﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2014.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Eric Smasal
* Date:     04/08/2014
*
* Purpose:  Hold Batch Summary Variables for Breadcrumbs.
*
* Modification History
* WI 150507 TWE 06/26/2014
*    Fix Batch Summary and initial Payment Search page display
******************************************************************************/

namespace WFS.RecHub.Online
{
   [Serializable]
    public class BatchSummaryVars
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int WorkgroupID { get; set; }
        public int PaymentTypeID { get; set; }
        public int PaymentSourceID { get; set; }    
        public int BankID { get; set; }
        public int ClientAccountID { get; set; }
        public int StartRecord { get; set; }
    }
}