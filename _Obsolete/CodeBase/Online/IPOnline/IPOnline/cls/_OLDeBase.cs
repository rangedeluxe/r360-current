using System.Xml;


/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Filename:        OLDUtil.asp
*
* Purpose: Includes generic and shared functionality used for online 
*          Decisioning.
*
* Modification History
* CR 12522 JMC 06/29/2006
*     -Created File
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard.
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

    public class _OLDeBase : _Page {

        //protected bool CheckOutBatch(long GlobalBatchID) {

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    //Dim objAttach
        //    //Dim objRespXml
        //    XmlDocument docRespXml;
        //    XmlNode nodeRespXml;

        //    strReqXml = "<ReqBatchToDecision>"
        //              + "    <Batch GlobalBatchID=\"" + GlobalBatchID.ToString() + "\" />"
        //              + "</ReqBatchToDecision>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);
        //    //objAttach = ObjectAsAttachment(docReqXml)
        //    nodeRespXml = DecisioningSrv.CheckOutBatch(docReqXml);

        //    // Add FormFields node to root of document.
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    AddMessagesToPage(docRespXml);
            
        //    return(!DocumentContainsErrors(docRespXml));

        //}

        //protected bool ResetBatch(int BankID, int CustomerID, int LockboxID, long GlobalBatchID) {

        //    string strReqXml;
        //    XmlDocument docReqXml;
        //    //Dim objAttach
        //    //Dim objRespXml
        //    XmlDocument docRespXml;
        //    XmlNode nodeRespXml;

        //    strReqXml = "<ReqBatchReset>"
        //              + "    <Bank BankID=\"" + BankID.ToString() + "\">"
        //              + "        <Customer CustomerID=\"" + CustomerID.ToString() + "\">"
        //              + "            <Lockbox LockboxID=\"" + LockboxID.ToString() + "\">"
        //              + "                <Batch GlobalBatchID=\"" + GlobalBatchID.ToString() + "\" />"
        //              + "            </Lockbox>"
        //              + "        </Customer>"
        //              + "    </Bank>"
        //              + "</ReqBatchReset>";

        //    docReqXml = new XmlDocument();
        //    docReqXml.LoadXml(strReqXml);

        //    //objAttach = ObjectAsAttachment(docReqXml);
        //    nodeRespXml = DecisioningSrv.ResetBatch(docReqXml);

        //    // Add FormFields node to root of document.
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    AddMessagesToPage(docRespXml);

        //    return(!DocumentContainsErrors(docRespXml));
        //}

        //protected bool CompleteBatch(long GlobalBatchID, XmlDocument docBatchXml) {

        //    //Dim objAttach
        //    //Dim objRespXml
        //    XmlDocument docRespXml;
        //    XmlNode nodeRespXml;

        //    //objAttach = ObjectAsAttachment(docBatchXml);

        //    nodeRespXml = DecisioningSrv.UpdateBatch(docBatchXml);

        //    // Add FormFields node to root of document.
        //    docRespXml = new XmlDocument();
        //    docRespXml.AppendChild(docRespXml.ImportNode(nodeRespXml, true));

        //    AddMessagesToPage(docRespXml);

        //    if(DocumentContainsErrors(docRespXml)) {
        //        return(false);
        //    } else {
        //        Response.Redirect("oldbatchsummary.aspx?gbatch=" + GlobalBatchID.ToString());
        //        return(true);
        //    }
        //}
    }
}
