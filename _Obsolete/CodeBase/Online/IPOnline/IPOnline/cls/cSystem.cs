using System;

using WFS.RecHub.Common;

/******************************************************************************
** Wausau
** Copyright � 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
* 
* Author:   
* Date:     
* 
* Purpose: This is the System Class Library.  
* 
* Modification History
* 	5/10/01 - jcs
* 	Added UseCutoff and CurrentProcessingDate properties
* 	5/24/01 - jcs
* 	Added properties to retrieve values stored in INI file.
*    12/11/02 JCS - VI 4247, Added RecordPerPage option to support resultpaging.
*    VI 4364 & 5298 - Exposed SessionTimeout from OnlineSetup DB table.
* 	CR 4530 7/10/2003 jcs - Added ApplicationName property for XSLT conversion.
* 	CR 13591 EJG 09/12/2005 - Modified RecordsPerPage to call SystemSrv.RecordsPerPage.
* CR 15161 JMC 03/01/2006
*     -Added FileDownloadThreshold property which reads from the local CBXOnline.ini file.
* CR 22114 JMC 11/06/2007
*    -Ported application to .Net from asp.
* WI 70429 JMC 12/17/2012
*   -Changed namespaces to use RecHub standard.
* WI 70371 TWE 02/07/2013 
*     Add TimeZoneBias for notification display
* WI 90121 WJS 3/4/2013
*   -FP:Changed namespaces to use RecHub standard..
* WI 90136 WJS 03/04/2013 
*     FP:Add TimeZoneBias for notification display
* WI 96311 CRG 04/30/2013 
*     Change the NameSpace, Framework, Using's and Flower Boxes for IPOnline
******************************************************************************/
namespace WFS.RecHub.Online {

	/// <summary>
	/// Summary description for cSystem.
	/// </summary>
	public class cSystem {

        //local variables to hold property values
        private DateTime _CurrentProcessingDate = DateTime.MinValue;
        private string _CSRSubnetMask = string.Empty;
        private bool _RequireSSL = true;
        private int _SSLPort = 443;
        private int _SSLKeysize = 0;
        private string _LogFile = string.Empty;
        private bool _StreamCENDSData = false;
        private string _CENDSURL = string.Empty;
        private string _CENDSSessionPath = string.Empty;
        private string _ApplicationName = string.Empty;
        private int _FileDownloadThreshold = -1;
        private int _TimeZoneBias = -300;   //In minutes
        private string _TimeZoneLabel = "Eastern";
        private bool _AdjustDaylightSavings = true;


        public cSystem(string SiteKey) {
            LoadIniValues(SiteKey);
        }
    	    	
    	public string CSRSubnetMask {
    	    get {
    		    return(_CSRSubnetMask);
            }
    	}

        public bool RequireSSL {
            get {
    		    return(_RequireSSL);
            }
    	}

    	public int SSLPort {
            get {
    		    return(_SSLPort);
            }
    	}
    	
    	public int SSLKeysize {
            get {
    		    return(_SSLKeysize);
            }
    	}
    	
    	public string LogFile {
            get {
    		    return(_LogFile);
            }
    	}
    	
    	public bool StreamCENDSData {
    	    get {
    		    return(_StreamCENDSData);
            }
    	}
    	
    	public string CENDSURL {
            get {
    		    return(_CENDSURL);
            }
    	}
    	
    	public string CENDSSessionPath {
            get {
    		    return(_CENDSSessionPath);
    		}
    	}

        public string ApplicationName {
            get {
                return(_ApplicationName);
            }
            set {
                _ApplicationName = value;
            }
        }
    	
    	public int FileDownloadThreshold {
    	    get {
    		    return(_FileDownloadThreshold);
            }
    	}

        public int TimeZoneBias
        {
            get
            {
                return (_TimeZoneBias);
            }
        }

        public string TimeZoneLabel
        {
            get
            {
                return (_TimeZoneLabel);
            } 
        }

        public bool AdjustDaylightSavings
        {
            get
            {
                return (_AdjustDaylightSavings);
            }
        }

        private void LoadIniValues(string vSiteKey) {

	        string strFileDownloadThreshold;
            string strTimeZoneBias;
            string strAdjustDaylightSavings;
            int intTemp;
            bool bolTemp;

            _CSRSubnetMask = ipoINILib.IniReadValue(vSiteKey, "CSRSubnetMask");
            _RequireSSL = (ipoINILib.IniReadValue(vSiteKey, "RequireSSL").Trim().ToUpper() == "Y");

            if(int.TryParse(ipoINILib.IniReadValue(vSiteKey, "SSLPort"), out intTemp)) {
                _SSLPort = intTemp;
            } else {
                _SSLPort = 443;
            }

            if(int.TryParse(ipoINILib.IniReadValue(vSiteKey, "SSLKeysize"), out intTemp)) {
                _SSLKeysize = intTemp;
            } else {
                _SSLKeysize = 443;
            }

            _LogFile = ipoINILib.IniReadValue(vSiteKey, "LogFile").TrimEnd();

			_StreamCENDSData = (ipoINILib.IniReadValue(vSiteKey, "StreamCENDSData").Trim() == "1");

			_CENDSURL =  ipoINILib.IniReadValue(vSiteKey,"CENDSURL").TrimEnd();
            if(!_CENDSURL.EndsWith("/")) {
			    _CENDSURL += "/";
			}

			_CENDSSessionPath =  ipoINILib.IniReadValue(vSiteKey, "CENDSSessionPath").TrimEnd();

            strFileDownloadThreshold = ipoINILib.IniReadValue(vSiteKey, "FileDownloadThreshold");

            if((strFileDownloadThreshold.Length > 0) && (int.TryParse(strFileDownloadThreshold, out intTemp))) {
		        _FileDownloadThreshold = intTemp;
		    } else {
		        _FileDownloadThreshold = 0;
            }

            // pick up the Time Zone Offset to use
            strTimeZoneBias = ipoINILib.IniReadValue(vSiteKey, "TimeZoneBias");
            if ((strTimeZoneBias.Length > 0) && (int.TryParse(strTimeZoneBias, out intTemp)))
            {
                _TimeZoneBias = intTemp;
            }
            else
            {
                _TimeZoneBias = -300;
            }
            //Pick up the Time Zone Label to use
            _TimeZoneLabel = ipoINILib.IniReadValue(vSiteKey, "TimeZoneLabel");
            if (_TimeZoneLabel.Length == 0)
            {
                _TimeZoneLabel = "Eastern";
            }

            strAdjustDaylightSavings = ipoINILib.IniReadValue(vSiteKey, "AdjustDaylightSavings");
            if ((strAdjustDaylightSavings.Length > 0) && (bool.TryParse(strAdjustDaylightSavings, out bolTemp)))
            {
                _AdjustDaylightSavings = bolTemp;
            }
            else
            {
                _AdjustDaylightSavings = true;
            }
        }
    }
}
