﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   EAS
* Date:     06/03/2013     
*
* Purpose: This is the UserPermission model Class Library.  
*
* Modification History
* 	
******************************************************************************/

namespace WFS.RecHub.Online
{
    public class cUserPermission
    {
        #region Constructors

        public cUserPermission() { }

        public cUserPermission(string name, string scriptFile, string mode)
        {
            this.Name = name;
            this.ScriptFile = scriptFile;
            this.Mode = mode;
        }

        #endregion

        #region Properties

        public string Name { get; set; }
        public string ScriptFile { get; set; }
        public string Mode { get; set; }

        #endregion
    }
}