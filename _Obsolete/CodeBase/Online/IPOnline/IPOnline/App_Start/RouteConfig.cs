﻿using System.Web.Mvc;
using System.Web.Routing;


namespace WFS.RecHub.Online.IPOnline
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{*x}", new { x = @".*\.asmx(/.*)?" });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: "~/batchsummary.aspx"
            );

            routes.MapRoute(
                name: "RECHUB",
                url: "RecHubPage/{action}",
                defaults: new { controller = "RecHubPage", action = "{action}" }
            );

            

        }
    }
}