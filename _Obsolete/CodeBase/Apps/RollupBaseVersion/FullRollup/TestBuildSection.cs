﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     11/05/2013
*
* Purpose:  
*
* Modification History
* WI 121402 JMC 11/05/2013
*    -Initial release version.
******************************************************************************/
namespace WFS.RecHub.FullRollup {

    public class TestBuildSection : ConfigurationSection {

        [ConfigurationProperty("", IsDefaultCollection = true)]
        public TestBuildCollection TestBuilds {
            get {
                try {
                    TestBuildCollection buildCollection = (TestBuildCollection)base[""];
                    return buildCollection;
                } catch(Exception e) {
                    string msg = e.Message;
                    return (null);
                }
            }
        }
    }


    public class TestBuildCollection : ConfigurationElementCollection {
        public TestBuildCollection() {
            try {
            TestBuildConfigElement details = (TestBuildConfigElement)CreateNewElement();
            if(details.TestBuildName != "") {
                Add(details);
            }
            } catch(Exception e) {
                string msg = e.Message;
            }
        }

        public override ConfigurationElementCollectionType CollectionType {
            get {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement() {
            return new TestBuildConfigElement();
        }

        protected override Object GetElementKey(ConfigurationElement element) {
            return ((TestBuildConfigElement)element).TestBuildName;
        }

        public TestBuildConfigElement this[int index] {
            get {
                return (TestBuildConfigElement)BaseGet(index);
            }
            set {
                if(BaseGet(index) != null) {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public TestBuildConfigElement this[string name] {
            get {
                return (TestBuildConfigElement)BaseGet(name);
            }
        }

        public int IndexOf(TestBuildConfigElement details) {
            return BaseIndexOf(details);
        }

        public void Add(TestBuildConfigElement details) {
            BaseAdd(details);
        }

        protected override void BaseAdd(ConfigurationElement element) {
            BaseAdd(element, false);
        }

        public void Remove(TestBuildConfigElement details) {
            if(BaseIndexOf(details) >= 0)
                BaseRemove(details.TestBuildName);
        }

        public void RemoveAt(int index) {
            BaseRemoveAt(index);
        }

        public void Remove(string name) {
            BaseRemove(name);
        }

        public void Clear() {
            BaseClear();
        }

        protected override string ElementName {
            get {
                return "TestBuild";
            }
        }
    }


    public class TestBuildConfigElement : ConfigurationElement {
        [ConfigurationProperty("TestBuildName", IsRequired = true, IsKey = true)]
        [StringValidator(InvalidCharacters = "")]
        public string TestBuildName {
            get {
                return (string)this["TestBuildName"];
            }
            set {
                this["TestBuildName"] = value;
            }
        }

        [ConfigurationProperty("Solutions", IsDefaultCollection = false)]
        public SolutionsCollection Solutions {
            get {
                return (SolutionsCollection)base["Solutions"];
            }

        }

    }

    public class SolutionsCollection : ConfigurationElementCollection {

        public new SolutionConfigElement this[string name] {
            get {
                if(IndexOf(name) < 0)
                    return null;
                return (SolutionConfigElement)BaseGet(name);
            }
        }

        public SolutionConfigElement this[int index] {
            get {
                return (SolutionConfigElement)BaseGet(index);
            }
        }

        public int IndexOf(string name) {

            name = name.ToLower();

            for(int idx = 0;idx < base.Count;idx++) {
                if(this[idx].SolutionFile.ToLower() == name) {
                    return idx;
                }
            }
            return -1;
        }

        public override ConfigurationElementCollectionType CollectionType {
            get {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement() {
            return new SolutionConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((SolutionConfigElement)element).SolutionFile;
        }

        protected override string ElementName {
            get {
                return "Solution";
            }
        }
    }

    public class SolutionConfigElement : ConfigurationElement {

        public SolutionConfigElement() {
            try {
                } catch(Exception e) {
                    string msg = e.Message;
                }
        }

        public SolutionConfigElement(string solutionFileName) {
            this.SolutionFile = solutionFileName;
        }

        [ConfigurationProperty("SolutionFile", IsKey = true, IsRequired = true)]
        [StringValidator(InvalidCharacters = "")]
        public string SolutionFile {
            get {
                return (string)this["SolutionFile"];
            }
            set {
                this["SolutionFile"] = value;
            }
        }
    }
}
