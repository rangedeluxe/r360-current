﻿namespace WFS.RecHub.FullRollup {
    partial class frmParms {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if(disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.btnCancel = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.colInclude = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colRollup = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.colBuildName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colBuildNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cboTestBuild = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdCleanWorkspace = new System.Windows.Forms.Button();
            this.cmdSharedDep = new System.Windows.Forms.Button();
            this.chkUseRecHubCurrent = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.btnOk = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(81, 0);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(0);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(80, 28);
            this.btnCancel.TabIndex = 2;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.colInclude,
            this.colRollup,
            this.colBuildName,
            this.colBuildNumber});
            this.dataGridView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridView1.Location = new System.Drawing.Point(4, 4);
            this.dataGridView1.Margin = new System.Windows.Forms.Padding(4);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(667, 322);
            this.dataGridView1.TabIndex = 4;
            // 
            // colInclude
            // 
            this.colInclude.HeaderText = "Build?";
            this.colInclude.Name = "colInclude";
            this.colInclude.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.colInclude.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.colInclude.Width = 52;
            // 
            // colRollup
            // 
            this.colRollup.HeaderText = "Rollup?";
            this.colRollup.Name = "colRollup";
            this.colRollup.Width = 58;
            // 
            // colBuildName
            // 
            this.colBuildName.HeaderText = "Build";
            this.colBuildName.Name = "colBuildName";
            this.colBuildName.ReadOnly = true;
            // 
            // colBuildNumber
            // 
            this.colBuildNumber.HeaderText = "Number";
            this.colBuildNumber.Name = "colBuildNumber";
            this.colBuildNumber.Width = 200;
            // 
            // cboTestBuild
            // 
            this.cboTestBuild.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cboTestBuild.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTestBuild.FormattingEnabled = true;
            this.cboTestBuild.Items.AddRange(new object[] {
            "<None>"});
            this.cboTestBuild.Location = new System.Drawing.Point(102, 0);
            this.cboTestBuild.Margin = new System.Windows.Forms.Padding(4);
            this.cboTestBuild.Name = "cboTestBuild";
            this.cboTestBuild.Size = new System.Drawing.Size(572, 24);
            this.cboTestBuild.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(0, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(105, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Run Test Build:";
            // 
            // cmdCleanWorkspace
            // 
            this.cmdCleanWorkspace.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.cmdCleanWorkspace.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanWorkspace.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCleanWorkspace.ForeColor = System.Drawing.Color.White;
            this.cmdCleanWorkspace.Location = new System.Drawing.Point(3, 1);
            this.cmdCleanWorkspace.Margin = new System.Windows.Forms.Padding(4);
            this.cmdCleanWorkspace.Name = "cmdCleanWorkspace";
            this.cmdCleanWorkspace.Size = new System.Drawing.Size(138, 28);
            this.cmdCleanWorkspace.TabIndex = 7;
            this.cmdCleanWorkspace.Text = "Clean";
            this.cmdCleanWorkspace.UseVisualStyleBackColor = false;
            this.cmdCleanWorkspace.Click += new System.EventHandler(this.cmdCleanWorkspace_Click);
            // 
            // cmdSharedDep
            // 
            this.cmdSharedDep.Location = new System.Drawing.Point(149, 1);
            this.cmdSharedDep.Margin = new System.Windows.Forms.Padding(4);
            this.cmdSharedDep.Name = "cmdSharedDep";
            this.cmdSharedDep.Size = new System.Drawing.Size(127, 28);
            this.cmdSharedDep.TabIndex = 8;
            this.cmdSharedDep.Text = "New Shared Dep.";
            this.cmdSharedDep.UseVisualStyleBackColor = true;
            this.cmdSharedDep.Click += new System.EventHandler(this.cmdSharedDep_Click);
            // 
            // chkUseRecHubCurrent
            // 
            this.chkUseRecHubCurrent.AutoSize = true;
            this.chkUseRecHubCurrent.Checked = true;
            this.chkUseRecHubCurrent.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUseRecHubCurrent.Location = new System.Drawing.Point(283, 6);
            this.chkUseRecHubCurrent.Name = "chkUseRecHubCurrent";
            this.chkUseRecHubCurrent.Size = new System.Drawing.Size(161, 21);
            this.chkUseRecHubCurrent.TabIndex = 9;
            this.chkUseRecHubCurrent.Text = "Use RecHub\\Current";
            this.chkUseRecHubCurrent.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.IsSplitterFixed = true;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.dataGridView1);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(4, 4, 4, 0);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(675, 392);
            this.splitContainer1.SplitterDistance = 326;
            this.splitContainer1.TabIndex = 10;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer2.IsSplitterFixed = true;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.cboTestBuild);
            this.splitContainer2.Panel1.Controls.Add(this.label1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer2.Panel2.Controls.Add(this.cmdCleanWorkspace);
            this.splitContainer2.Panel2.Controls.Add(this.chkUseRecHubCurrent);
            this.splitContainer2.Panel2.Controls.Add(this.cmdSharedDep);
            this.splitContainer2.Panel2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 4);
            this.splitContainer2.Size = new System.Drawing.Size(675, 62);
            this.splitContainer2.SplitterDistance = 26;
            this.splitContainer2.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.btnOk, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.btnCancel, 1, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(509, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0, 0, 4, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(162, 30);
            this.tableLayoutPanel1.TabIndex = 11;
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(0, 0);
            this.btnOk.Margin = new System.Windows.Forms.Padding(0);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(80, 28);
            this.btnOk.TabIndex = 2;
            this.btnOk.Text = "Ok";
            this.btnOk.UseVisualStyleBackColor = true;
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // frmParms
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(675, 392);
            this.Controls.Add(this.splitContainer1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MinimumSize = new System.Drawing.Size(629, 188);
            this.Name = "frmParms";
            this.Text = "Enter Parameters";
            this.Load += new System.EventHandler(this.frmParms_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel1.PerformLayout();
            this.splitContainer2.Panel2.ResumeLayout(false);
            this.splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox cboTestBuild;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button cmdCleanWorkspace;
        private System.Windows.Forms.Button cmdSharedDep;
        private System.Windows.Forms.CheckBox chkUseRecHubCurrent;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colInclude;
        private System.Windows.Forms.DataGridViewCheckBoxColumn colRollup;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBuildName;
        private System.Windows.Forms.DataGridViewTextBoxColumn colBuildNumber;
    }
}