﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     11/05/2013
*
* Purpose:  
*
* Modification History
* WI 121402 JMC 11/05/2013
*    -Initial release version.
******************************************************************************/
namespace WFS.RecHub.FullRollup {

    public class RollupSection : ConfigurationSection {

        [ConfigurationProperty("", IsDefaultCollection = true)]  
        public BuildCollection Builds {
            get {
                BuildCollection buildCollection = (BuildCollection)base[""];
                return buildCollection;                
            }
        }
    }


    public class BuildCollection : ConfigurationElementCollection {
        public BuildCollection() {
            BuildConfigElement details = (BuildConfigElement)CreateNewElement();
            if(details.BuildName != "") {
                Add(details);
            }
        }

        public override ConfigurationElementCollectionType CollectionType {
            get {
                return ConfigurationElementCollectionType.BasicMap;
            }
        }

        protected override ConfigurationElement CreateNewElement() {
            return new BuildConfigElement();
        }

        protected override Object GetElementKey(ConfigurationElement element) {
            return ((BuildConfigElement)element).BuildName;
        }

        public BuildConfigElement this[int index] {
            get {
                return (BuildConfigElement)BaseGet(index);
            }
            set {
                if (BaseGet(index) != null) {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public BuildConfigElement this[string name] {
            get {
                return (BuildConfigElement)BaseGet(name);
            }
        }

        public int IndexOf(BuildConfigElement details) {
            return BaseIndexOf(details);
        }

        public void Add(BuildConfigElement details) {
            BaseAdd(details);
        }
    
        protected override void BaseAdd(ConfigurationElement element) {
            BaseAdd(element, false);
        }

        public void Remove(BuildConfigElement details)
        {
            if (BaseIndexOf(details) >= 0)
                BaseRemove(details.BuildName);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }

        protected override string ElementName
        {
            get {
                return "Build";
            }
        }
    }


    public class BuildConfigElement:ConfigurationElement
    {
        [ConfigurationProperty("BuildName", IsRequired = true, IsKey = true)]
        [StringValidator(InvalidCharacters = "")]
        public string BuildName {
            get {
                return (string)this["BuildName"];
            }
            set {
                this["BuildName"] = value;
            }
        }

        [ConfigurationProperty("SolutionFile", IsRequired = true, IsKey = true)]
        [StringValidator(InvalidCharacters = "")]
        public string SolutionFile {
            get {
                return (string)this["SolutionFile"];
            }
            set {
                this["SolutionFile"] = value;
            }
        }

        [ConfigurationProperty("IsDefault", IsRequired = true, IsKey = true)]
        public byte IsDefault {
            get {
                return (byte)this["IsDefault"];
            }
            set {
                this["IsDefault"] = value;
            }
        }

        [ConfigurationProperty("Binaries", IsDefaultCollection = false)]
        public BinariesCollection Binaries
        {
            get {
                return (BinariesCollection)base["Binaries"];
            }

        }

    }

    public class BinariesCollection : ConfigurationElementCollection {

        public new BinaryConfigElement this[string name] {
            get {
                if (IndexOf(name) < 0) return null;
                return (BinaryConfigElement)BaseGet(name);
            }
        }

        public BinaryConfigElement this[int index] {
            get {
                return (BinaryConfigElement)BaseGet(index);
            }
        }

        public int IndexOf(string name) {
            
            name = name.ToLower();

            for (int idx = 0; idx < base.Count; idx++) {
                if(this[idx].BinaryFileName.ToLower() == name) {
                    return idx;
                }
            }
            return -1;
        }

        public override ConfigurationElementCollectionType CollectionType {
            get { return ConfigurationElementCollectionType.BasicMap; }
        }

        protected override ConfigurationElement CreateNewElement() {
            return new BinaryConfigElement();
        }

        protected override object GetElementKey(ConfigurationElement element) {
            return ((BinaryConfigElement)element).BinaryFileName;
        }

        protected override string ElementName {
            get {
                return "Binary";
            }
        }
    }

    public class BinaryConfigElement : ConfigurationElement
    {        

        public BinaryConfigElement()
        {
        }

        public BinaryConfigElement(string projectFolderPath, string binaryFileName)
        {
            this.BinaryFileName = binaryFileName;
            this.ProjectFolderPath = projectFolderPath;
        }

        [ConfigurationProperty("BinaryFileName", IsKey = true, IsRequired = true)]
        [StringValidator(InvalidCharacters = "")]
        public string BinaryFileName {
            get {
                return (string)this["BinaryFileName"];
            }
            set {
                this["BinaryFileName"] = value;
            }
        }

        [ConfigurationProperty("ProjectFolderPath", IsRequired = true, DefaultValue = "")]
        public string ProjectFolderPath
        {
            get {
                return (string)this["ProjectFolderPath"];
            }
            set {
                this["ProjectFolderPath"] = value;
            }
        }
 
    }
}
