﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using WFS.LTA.Common;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     11/05/2013
*
* Purpose:  
*
* Modification History
* WI 121402 JMC 11/05/2013
*    -Initial release version.
* WI 119063 CEJ 11/18/2013
*    -Added Version Stamp
******************************************************************************/
namespace WFS.RecHub.FullRollup {
    
    class Program {

        private static ltaLog _ErrorLog;
        private static ltaLog _EventLog;
        private const string RELEASE_MODE = "Release";

        private static string _ReleaseVersion = "Main";

        static void Main(string[] args) {
            string strBuildNumber = ConfigurationManager.AppSettings["buildNumber"];
            string strCodeBasePath = ConfigurationManager.AppSettings["codebasePath"];

            if(ConfigurationManager.AppSettings.AllKeys.Contains("ReleaseVersion")) {
                if(ConfigurationManager.AppSettings["ReleaseVersion"] != "") {
                    _ReleaseVersion = ConfigurationManager.AppSettings["ReleaseVersion"];
                }
            }
            List<string> arDLLsToCopy;
            List<RollupProcess> arBuilds;
            string strTestBuildGroup = string.Empty;
            bool bolDoBuild = false;
            bool bolDoRollup = false;
            bool bolRunSilent = false;
            bool bolUseCurrentDir = false;
            RollupProcess objRollupProcess;

            foreach(string arg in args) {
                switch(arg.ToLower()) {
                    case "-dobuild":
                    case "/dobuild":
                        bolDoBuild = true;
                        break;
                    case "-dorollup":
                    case "/dorollup":
                        bolDoRollup = true;
                        break;
                    case "-silent":
                    case "/silent":
                        bolRunSilent = true;
                        break;
                    case "-?":
                    case "/?":
                        Usage();
                        return;
                    default:
                        if(arg.StartsWith("-buildnumber:") ||
                           arg.StartsWith("/buildnumber:") ||
                           arg.StartsWith("-buildnumber=") ||
                           arg.StartsWith("/buildnumber=")) {
                               strBuildNumber = arg.Substring(("-buildnumber:").Length);
                        } else if(arg.StartsWith("-testbuild:") ||
                           arg.StartsWith("/testbuild:") ||
                           arg.StartsWith("-testbuild=") ||
                           arg.StartsWith("/testbuild=") ||
                           arg.StartsWith("-testbuild:")) {
                               strTestBuildGroup = arg.Substring(("-testbuild:").Length);
                        }
                        break;
                }
            }

            if(bolRunSilent) {
                arBuilds = new List<RollupProcess>();
                RollupSection rollupSection = (RollupSection)ConfigurationManager.GetSection("RollupSection");
                foreach(BuildConfigElement build in rollupSection.Builds) {
                    if(args.Contains("-" + build.BuildName, new StringComparer())) {
                        objRollupProcess = new RollupProcess();
                        objRollupProcess.DoBuild = bolDoBuild;
                        objRollupProcess.DoRollup = bolDoRollup;
                        objRollupProcess.BuildName = build.BuildName;
                        objRollupProcess.BuildNumber = strBuildNumber;
                        arBuilds.Add(objRollupProcess);
                    }
                }
            } else {
                string sTodaysBuildNumber = string.Format("Bld_{0:yy.MM.dd}.01", DateTime.Now);
                frmParms objParms = new frmParms(strBuildNumber == "" ? sTodaysBuildNumber : strBuildNumber, strTestBuildGroup);
                if(objParms.ShowDialog() == System.Windows.Forms.DialogResult.OK) {
                    arBuilds = objParms.GetSelectedBuilds();
                    strTestBuildGroup = objParms.SelectedTestBuild;
                    bolUseCurrentDir = objParms.UseCurrentDir;
                    objParms.Dispose();
                } else {
                    strTestBuildGroup = string.Empty;
                    objParms.Dispose();
                    return;
                }
            }

            RollupSection rollupSection2 = (RollupSection)ConfigurationManager.GetSection("RollupSection");
            foreach(RollupProcess build in arBuilds) {
                string strDLLFolder = bolUseCurrentDir ? Path.Combine(strCodeBasePath, @"..\SharedDependencies\RecHub\Current\") : Path.Combine(strCodeBasePath, @"..\SharedDependencies\", _ReleaseVersion, build.BuildNumber);
                if(!Directory.Exists(strDLLFolder)) {
                    Directory.CreateDirectory(strDLLFolder);
                }
                arDLLsToCopy = new List<string>();
                foreach(BinaryConfigElement binary in rollupSection2.Builds[build.BuildName].Binaries) {
                    arDLLsToCopy.Add(Path.Combine(strCodeBasePath, binary.ProjectFolderPath, "bin", RELEASE_MODE, binary.BinaryFileName));
                }
                if(!DoBuild(
                    Path.Combine(strCodeBasePath, rollupSection2.Builds[build.BuildName].SolutionFile),
                    arDLLsToCopy,
                    strCodeBasePath,
                    build.BuildNumber,
                    strDLLFolder,
                    build.DoBuild,
                    build.DoRollup)) {
                    return;
                }
            }

            List<string> arSolutions = new List<string>();
            TestBuildSection testBuildSection = (TestBuildSection)ConfigurationManager.GetSection("TestBuildSection");
            foreach(TestBuildConfigElement testBuildGroup in testBuildSection.TestBuilds) {
                if(testBuildGroup.TestBuildName == strTestBuildGroup) {
                    foreach(SolutionConfigElement sol in testBuildGroup.Solutions) {
                        arSolutions.Add(sol.SolutionFile);
                    }
                }
            }

            if(arSolutions.Count > 0) {
                DoTestBuilds(strCodeBasePath, arSolutions);
            }

            Console.WriteLine("Test Builds Completed.");
            Console.ReadKey();
            return;
        }

        private static bool DoBuild(
            string solutionFilePath,
            List<string> dllsToCopy,
            string codeBasePath, 
            string buildVersion, 
            string dllFolder,
            bool doBuild,
            bool doRollup) {

            bool bolSuccess;
            List<string> arBaseDLLToUpdate;
            string sharedDepDir = dllFolder.Substring(dllFolder.IndexOf(@"\SharedDependencies\") + @"\SharedDependencies\".Length);

            if(doBuild) {
                StampVersion(buildVersion.Replace("Bld_", ""), string.Format("\"{0}\"", Path.GetFullPath(solutionFilePath)));
                //foreach(string sProjectFile in GetProjectsFromSolution(solutionFilePath)) {
                //    StampVersion(buildVersion.Replace("Bld_", ""), string.Format("\"{0}\"", Path.GetFullPath(sProjectFile)));
                //}
                bolSuccess = BuildMSBuild(solutionFilePath);

                if(!bolSuccess) {
                    Console.WriteLine(Path.GetFileName(solutionFilePath) + " Build Failed.");
                    Console.ReadKey();
                    return (false);
                }

                CopyFiles(dllFolder, dllsToCopy);
            } else {
                bolSuccess = true;
            }

            if(doRollup) {
                // Roll up LTA Log to to all projects.
                arBaseDLLToUpdate = new List<string>();
                foreach(string file in dllsToCopy) {
                    arBaseDLLToUpdate.Add(Path.GetFileName(file));
                }

                UpdateFiles(codeBasePath, arBaseDLLToUpdate, sharedDepDir, buildVersion);
            } else {
                bolSuccess = true;
            }

            return (bolSuccess);
        }

        private static string[] GetProjectsFromSolution(string solution) {
            Regex rgxFilter = new Regex("Project\\s*\\(\\s*\"\\{[\\dA-F]{8}(\\-[\\dA-F]{4}){3}\\-[\\dA-F]{12}\\}\\\"\\s*\\)\\s*\\=\\s*\\\"[^\"]+\\\"\\s*,\\s*\\\"(?<ProjectFile>[^\"]+)\\\"\\s*,\\s*\"\\{[\\dA-F]{8}(\\-[\\dA-F]{4}){3}\\-[\\dA-F]{12}\\}\\\"");
            List<string> lstProjects = new List<string>();

            using(StreamReader reader = File.OpenText(solution)) {
                while(reader.Peek() >= 0) {
                    string fileLine = reader.ReadLine();
                    Match mtcFound = rgxFilter.Match(fileLine);

                    if(mtcFound.Value !="") {
                        string sCorrectedProjFile = mtcFound.Groups["ProjectFile"].Value;
                        if(!Directory.Exists(sCorrectedProjFile)) {
                            sCorrectedProjFile = Path.Combine(Path.GetDirectoryName(solution), sCorrectedProjFile);
                        }
                        lstProjects.Add(sCorrectedProjFile);
                    }
                }
                reader.Close();
            }
            return lstProjects.ToArray();
        }

        private static ltaLog ErrorLog {
            get {
                string strLogPath;

                if(_ErrorLog == null) {
                    if(ConfigurationManager.AppSettings["errorLogPath"] != null && ConfigurationManager.AppSettings["errorLogPath"].Length > 0) {
                        strLogPath = ConfigurationManager.AppSettings["errorLogPath"];
                    } else {
                        strLogPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "ErrLog.txt");
                    }
                    _ErrorLog = new ltaLog(strLogPath, 1024, LTAMessageImportance.Debug);
                    _ErrorLog.InsertBlankLine();
                }
                return (_ErrorLog);
            }
        }

        private static ltaLog EventLog {
            get {
                string strLogPath;

                if(_EventLog == null) {
                    if(ConfigurationManager.AppSettings["eventLogPath"] != null && ConfigurationManager.AppSettings["eventLogPath"].Length > 0) {
                        strLogPath = ConfigurationManager.AppSettings["eventLogPath"];
                    } else {
                        strLogPath = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "EventLog.txt");
                    }
                    _EventLog = new ltaLog(strLogPath, 1024, LTAMessageImportance.Debug);
                    _EventLog.InsertBlankLine();
                }
                return (_EventLog);
            }
        }

        private static bool BuildMSBuild(string solutionFile) {

            string strMsg;
            bool bErrorExists = false;

            try {

                string sysDir = Environment.GetEnvironmentVariable("windir");
                string msBuild = sysDir + "\\Microsoft.NET\\Framework\\v4.0.30319\\MSBuild.exe";

                strMsg = "Building: " + solutionFile;
                LTAConsole.ConsoleWriteLine(strMsg);
                EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);

                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = false;
                myProcess.StartInfo.FileName = msBuild;
                myProcess.StartInfo.Arguments = "\"" + solutionFile + "\" /t:rebuild  /p:Configuration=Release /v:minimal";
                myProcess.StartInfo.RedirectStandardOutput = true;
                myProcess.StartInfo.CreateNoWindow = true;
                myProcess.Start();
                string output = myProcess.StandardOutput.ReadToEnd();
                myProcess.WaitForExit();
                if(output.Contains("error")) {
                    bErrorExists = true;

                    strMsg = solutionFile + " failed to build";
                    LTAConsole.ConsoleWriteLine(strMsg, LTAConsoleMessageType.Error);
                    EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Error);
                    ErrorLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Error);
                } else {
                    strMsg = solutionFile + " Build was successful.";
                    LTAConsole.ConsoleWriteLine(strMsg, LTAConsoleMessageType.Information);
                    EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);
                }
                return !bErrorExists;

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                return (false);
            }

        }

        private static void UpdateFiles(string codeBasePath, List<string> baseDLLToUpdate, string sharedDepDir, string buildVersion) {

            string strMsg;

            try {

                foreach(string dllToSearchFor in baseDLLToUpdate) {
                    strMsg = "Updating project files for dll:[" + dllToSearchFor + "]";
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "BuildMSBuild", LTAMessageType.Information);
                    UpdateCSProjFiles(codeBasePath, dllToSearchFor, sharedDepDir, buildVersion);
                    UpdateBatchFiles(codeBasePath, dllToSearchFor, sharedDepDir, buildVersion);
                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        static void UpdateCSProjFiles(string directory, string dllToSearchFor, string sharedDepDir, string buildVersion) {

            string strMsg;
            bool bolFileWasUpdated;

            try {

                string[] files = Directory.GetFiles(directory, "*.csproj", SearchOption.AllDirectories);

                int year = DateTime.Now.Year;
                foreach(string setupFileName in files) {

                    if(!Path.GetFullPath(setupFileName).Contains(Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\")))) {

                        strMsg = "Setup FileName is " + setupFileName;
                        LTAConsole.ConsoleWriteLine(strMsg);
                        EventLog.logEvent(strMsg, "UpdateCSProjFiles", LTAMessageType.Information);

                        string shortDllName = dllToSearchFor.Remove(dllToSearchFor.Length - 4);
                        shortDllName += ",";
                        StringWriter sw = new StringWriter();
                        using(StreamReader reader = File.OpenText(setupFileName)) {


                            try {
                                bolFileWasUpdated = false;
                                while(reader.Peek() >= 0) {
                                    string fileLine = reader.ReadLine();

                                    if(fileLine.Contains(dllToSearchFor) && fileLine.Contains("SharedDependencies")) {
                                        int endPosition = fileLine.IndexOf("SharedDependencies");
                                        string newString = fileLine.Substring(0, endPosition);
                                        newString += Path.Combine("SharedDependencies", sharedDepDir, dllToSearchFor);
                                        newString += "</HintPath>";
                                        //if(!_FileToCheckIn.Contains(setupFileName)) {
                                        //    _FileToCheckIn.Add(setupFileName);
                                        //}
                                        sw.WriteLine(newString);
                                        bolFileWasUpdated = true;
                                    } else if(fileLine.Contains(shortDllName)) {
                                        int endPosition = fileLine.IndexOf(shortDllName);
                                        string newString = fileLine.Substring(0, endPosition);
                                        newString += shortDllName.Remove(shortDllName.Length - 1);
                                        newString += "\">";
                                        sw.WriteLine(newString);
                                        bolFileWasUpdated = true;
                                    } else {
                                        sw.WriteLine(fileLine);
                                    }
                                }
                            } finally {
                                // Close the file otherwise the compile may not work 
                                reader.Close();
                            }
                        }
                        if(bolFileWasUpdated) {
                            MakeFileWriteable(setupFileName, sw);
                        }
                } else {
                    continue;
                    }

                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        static void UpdateBatchFiles(string directory, string dllToSearchFor, string sharedDepDir, string buildVersion) {

            string strMsg;
            bool bolFileWasUpdated;

            try {

                string[] files = Directory.GetFiles(directory, "*.bat", SearchOption.AllDirectories);

                int year = DateTime.Now.Year;
                foreach(string setupFileName in files) {

                    strMsg = "Setup FileName is " + setupFileName;
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "UpdateBatchFiles", LTAMessageType.Information);

                    string shortDllName = dllToSearchFor.Remove(dllToSearchFor.Length - 4);
                    shortDllName += ",";
                    StringWriter sw = new StringWriter();
                    using(StreamReader reader = File.OpenText(setupFileName)) {


                        try {
                            bolFileWasUpdated = false;
                            while(reader.Peek() >= 0) {
                                string fileLine = reader.ReadLine();

                                if(fileLine.Contains(dllToSearchFor) && fileLine.Contains("SharedDependencies")) {
                                    int endPosition = fileLine.IndexOf("SharedDependencies");
                                    string newString = fileLine.Substring(0, endPosition);

                                    int intSubstrPos = fileLine.IndexOf('\\', endPosition);
                                    intSubstrPos = fileLine.IndexOf('\\', intSubstrPos + 1);
                                    intSubstrPos = fileLine.IndexOf('\\', intSubstrPos + 1);
                                    newString += Path.Combine("SharedDependencies", sharedDepDir, fileLine.Substring(intSubstrPos).Trim(new char[] {'\\'}));

                                    
                                    //if(!_FileToCheckIn.Contains(setupFileName)) {
                                    //    _FileToCheckIn.Add(setupFileName);
                                    //}
                                    sw.WriteLine(newString);
                                    bolFileWasUpdated = true;
                                } else if(fileLine.Contains(shortDllName)) {
                                    int endPosition = fileLine.IndexOf(shortDllName);
                                    string newString = fileLine.Substring(0, endPosition);
                                    newString += shortDllName.Remove(shortDllName.Length - 1);
                                    newString += "\">";
                                    sw.WriteLine(newString);
                                    bolFileWasUpdated = true;
                                } else {
                                    sw.WriteLine(fileLine);
                                }
                            }
                        } finally {
                            // Close the file otherwise the compile may not work 
                            reader.Close();
                        }
                    }
                    if(bolFileWasUpdated) {
                        MakeFileWriteable(setupFileName, sw);
                    }

                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }

        private static void MakeFileWriteable(string setupFileName, StringWriter sw) {

            int intRetries = 0;
            bool bolContinue = true;

            while(bolContinue) {
                try {

                    FileAttributes attributes = File.GetAttributes(setupFileName);
                    if((attributes & FileAttributes.ReadOnly) == FileAttributes.ReadOnly) {
                        // Make the file RW
                        attributes = RemoveAttribute(attributes, FileAttributes.ReadOnly);
                        File.SetAttributes(setupFileName, attributes);
                    }
                    TextWriter tw = new StreamWriter(setupFileName);
                    try {
                        tw.Write(sw.ToString());
                    } finally {
                        // close the stream 
                        tw.Close();
                    }

                    bolContinue = false;

                } catch(Exception e) {

                    ++intRetries;

                    if(intRetries > 3) {
                        ErrorLog.logError(e);
                        LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                        bolContinue = false;
                    } else {
                        LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Warning);
                        LTAConsole.ConsoleWriteLine("Retrying (" + intRetries.ToString() + ")...", LTAConsoleMessageType.Warning);
                        System.Threading.Thread.Sleep(500);
                    }
                }
            }
        }

        private static FileAttributes RemoveAttribute(FileAttributes attributes, FileAttributes attributesToRemove) {

            try {
                return attributes & ~attributesToRemove;
            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
                return (FileAttributes.Normal);
            }
        }

        private static bool CopyFiles(string sharedDependenciesFolder, List<string> dllsToCopy) {

            bool bolRetVal = false;
            string strMsg;

            try {

                if(!Directory.Exists(sharedDependenciesFolder)) {
                    Directory.CreateDirectory(sharedDependenciesFolder);
                }

                foreach(string dllOutput in dllsToCopy) {
                    string dest = sharedDependenciesFolder + "\\" + Path.GetFileName(dllOutput);
                    strMsg = "Copying " + dllOutput + " --> " + sharedDependenciesFolder;
                    LTAConsole.ConsoleWriteLine(strMsg);
                    EventLog.logEvent(strMsg, "CopyFiles", LTAMessageType.Information);
                    File.Copy(dllOutput, dest, true);
                    bolRetVal = true;
                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
            return (bolRetVal);
        }

        private static void DoTestBuilds(string codeBaseDirectory, List<string> solutionFiles) {

            bool bSuccess;

            try {

                foreach(string solutionFile in solutionFiles) {
                    bSuccess = BuildMSBuild(Path.Combine(codeBaseDirectory, solutionFile));
                }

            } catch(Exception e) {
                ErrorLog.logError(e);
                LTAConsole.ConsoleWriteLine(e.Message, LTAConsoleMessageType.Error);
            }
        }
        
        private static void Usage() {
            Console.WriteLine("Builds and/or rolls up dlls for inclusion in application solutions");
            Console.WriteLine("");
            Console.WriteLine(" Usage:   FullRollup ^");
            Console.WriteLine("            [BuildName] ^");
            Console.WriteLine("            [-dobuild] ^");
            Console.WriteLine("            [-dorollup] ^");
            Console.WriteLine("            [-silent] ^");
            Console.WriteLine("            [-buildnumber:[BuildNumber]]");
            Console.WriteLine("            [-testbuild:[TestBuildName]]");
            Console.WriteLine("");
            Console.WriteLine(" Example: FullRollup ^");
            Console.WriteLine("            -Tier2 ^");
            Console.WriteLine("            -dobuild ^");
            Console.WriteLine("            -dorollup ^");
            Console.WriteLine("            -silent ^");
            Console.WriteLine("            -buildnumber:Bld_13.02.22.2");
            Console.WriteLine("");
            Console.WriteLine(" -dobuild                      -Will execute builds using the specified");
            Console.WriteLine("                                build number");
            Console.WriteLine(" -dorollup                     -Will rollup new and existing apps to");
            Console.WriteLine("                                the specified build number");
            Console.WriteLine(" -silent                       -Bypasses user configuration screen");
            Console.WriteLine(" -buildnumber:[Build Number]   -Specifies build number for use by");
            Console.WriteLine("                                builds and rollups");
            Console.WriteLine(" -testbuild:[TestBuildName]    -Executes test builds for corresponding");
            Console.WriteLine("                                app.config section");
            Console.WriteLine(" -?                            -Shows this help screen");
            Console.WriteLine("");
            Console.WriteLine(" When no parameters are specified, the User-input form is displayed");
            Console.WriteLine("");
            Console.WriteLine("Press any key to continue . . .");
            Console.Read();
        }
    
        private static void StampVersion(string updatedVersion, string path) {

            // Start the child process.
            Process p = new Process();
            // Redirect the output stream of the child process.
            p.StartInfo.UseShellExecute = false;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.FileName = ConfigurationManager.AppSettings["stampVersionPath"];
            p.StartInfo.Arguments = updatedVersion + " " + path;
            p.Start();
            // Do not wait for the child process to exit before
            // reading to the end of its redirected stream.
            // p.WaitForExit();
            // Read the output stream first and then wait.
            string output = p.StandardOutput.ReadToEnd();
            p.WaitForExit();
        }
    }

    class StringComparer : IEqualityComparer<string> {
        // Products are equal if their names and product numbers are equal. 
        public bool Equals(string x, string y) {
            return (x.ToLower() == y.ToLower());
        }

        // If Equals() returns true for a pair of objects  
        // then GetHashCode() must return the same value for these objects. 

        public int GetHashCode(string value) {
            //Check whether the object is null 
            if(Object.ReferenceEquals(value, null))
                return 0;

            //Get hash code for the Name field if it is not null. 
            int hashProductName = value == null ? 0 : value.GetHashCode();

            //Get hash code for the Code field. 
            int hashProductCode = value.GetHashCode();

            //Calculate the hash code for the product. 
            return hashProductName ^ hashProductCode;
        }
    }
    
    public class RollupProcess {
        public string BuildName;
        public string BuildNumber;
        public bool DoBuild;
        public bool DoRollup;
    }
}
