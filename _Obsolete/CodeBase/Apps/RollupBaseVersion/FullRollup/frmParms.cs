﻿using Microsoft.TeamFoundation.Client;
using Microsoft.TeamFoundation.Framework.Client;
using Microsoft.TeamFoundation.Framework.Common;
using Microsoft.TeamFoundation.VersionControl.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

/******************************************************************************
** Wausau
** Copyright © 1997-2013 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2013.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   Joel Caples
* Date:     11/18/2013
*
* Purpose:  
*
* Modification History
* WI 121402 JMC 11/18/2013
*    -Initial release version.
******************************************************************************/
namespace WFS.RecHub.FullRollup {
    public partial class frmParms : Form {

        private string _DefaultBuildNumber = string.Empty;
        private string _DefaultTestBuildSection = string.Empty;
        private Dictionary<int, BinariesCollection> _RowToChanges = new Dictionary<int, BinariesCollection>();
        private Workspace _Workspace = null;

        public frmParms(string buildNumber, string testBuildGroup) {
            InitializeComponent();

            _DefaultBuildNumber = buildNumber;
            _DefaultTestBuildSection = testBuildGroup;
        }

        private void frmParms_Load(object sender, EventArgs e) {

            int intSelectedIndex;
            int i;

            RollupSection rollupSection = (RollupSection)ConfigurationManager.GetSection("RollupSection");
            _RowToChanges.Clear();
            foreach(BuildConfigElement build in rollupSection.Builds) {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(this.dataGridView1, new object[] { build.IsDefault > 0, build.IsDefault > 0, build.BuildName, _DefaultBuildNumber });
                this.dataGridView1.Rows.Add(row);
                _RowToChanges.Add(row.Index, build.Binaries);
            }
            FormatCell(cmdCleanWorkspace, HasPendingChanges().Count() == 0);
            TestBuildSection testBuildSection = (TestBuildSection)ConfigurationManager.GetSection("TestBuildSection");
            intSelectedIndex = 0;
            i=0;
            foreach(TestBuildConfigElement testBuildGroup in testBuildSection.TestBuilds) {
                i++;
                this.cboTestBuild.Items.Add(testBuildGroup.TestBuildName);
                if(testBuildGroup.TestBuildName.ToLower() == _DefaultTestBuildSection) {
                    intSelectedIndex = i;
                }
            }
            this.cboTestBuild.SelectedIndex = intSelectedIndex;
        }

        private void FormatCell(Button cmd, bool clean) {
            cmd.Text = clean? "Clean": "Pending Changes";
            cmd.BackColor = clean? Color.FromArgb(0,192,0): Color.Red;
            cmd.ForeColor = Color.White;
        }

        public Workspace WS {
            get {
                if(_Workspace == null) {
                    Uri tfsUri = new Uri("http://mosittfs02:8080/tfs");
                    TfsTeamProjectCollection tfs = new TfsTeamProjectCollection(tfsUri);
                    VersionControlServer versionControl = (VersionControlServer)tfs.GetService(typeof(VersionControlServer));
                    _Workspace = versionControl.GetWorkspace(Path.GetFullPath("."));
                }
                return _Workspace;
            }
        }

        public string SharedDepDir {
            get {
                return WS.GetLocalItemForServerItem(ServerCodebasePath.Replace("/Codebase/", "/SharedDependencies/"));
                //return WS.GetLocalItemForServerItem("$/Receivables_Hub/2.00_Dev/SharedDependencies");
            }
        }

        public string ServerCodebasePath {
            get {
                Regex rgx = new Regex(@"(?<Root>\$\/([^\/]+\/)+Codebase/).*");
                return rgx.Replace(WS.GetServerItemForLocalItem(Path.GetFullPath(".")), "${Root}");
            }
        }
        
        private PendingChange[] HasPendingChanges() {
            List<PendingChange> lstChanges = new List<PendingChange>();

            lstChanges.AddRange(WS.GetPendingChanges(ServerCodebasePath, RecursionType.Full));
            
            return lstChanges.ToArray();
        }

        private void btnCancel_Click(object sender, EventArgs e) {
            this.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close();
        }

        private void btnOk_Click(object sender, EventArgs e) {
            RollupSection rollupSection = (RollupSection)ConfigurationManager.GetSection("RollupSection");
            foreach(DataGridViewRow row in this.dataGridView1.Rows) {
                if(row.Cells[0].Value != null && (bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value) {

                    if(row.Cells[3].Value == null) {
                        MessageBox.Show("Build Numbers must be entered for selected builds.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    //string sPattern = "Bld_??.??.??.??";
                    string sPattern = @"bld_\d{2}(\.\d{2}){3}";
                    RegexStringValidator regex = new RegexStringValidator(sPattern);

                    try
                    {
                    // Attempt validation.
                        //regex.Validate(row.Cells[3].Value.ToString().ToLower());
                        row.Cells[3].Value = row.Cells[3].Value.ToString().ToLower().Replace("bld", "Bld");
                    } catch(ArgumentException ex) {
                        // Validation failed.
                        MessageBox.Show(ex.Message.ToString().Replace(sPattern, "Bld_##.##.##.##"), "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                }
            }
            
            this.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.Close();
        }

        public List<RollupProcess> GetSelectedBuilds() {

            List<RollupProcess> arRetVal = new List<RollupProcess>();
            RollupProcess objRollupProcess;

            foreach(DataGridViewRow row in this.dataGridView1.Rows) {
                objRollupProcess = new RollupProcess();
                if(row.Cells[0].Value != null && (bool)((DataGridViewCheckBoxCell)row.Cells[0]).Value) {
                    objRollupProcess.DoBuild = true;
                } else {
                    objRollupProcess.DoBuild = false;
                }
                if(row.Cells[1].Value != null && (bool)((DataGridViewCheckBoxCell)row.Cells[1]).Value) {
                    objRollupProcess.DoRollup = true;
                } else {
                    objRollupProcess.DoRollup = false;
                }

                if(objRollupProcess.DoBuild || objRollupProcess.DoRollup) {
                    objRollupProcess.BuildName = row.Cells[2].Value.ToString();
                    objRollupProcess.BuildNumber = row.Cells[3].Value.ToString();
                    arRetVal.Add(objRollupProcess);
                }
            }

            return (arRetVal);
        }

        public bool UseCurrentDir {
            get {
                return chkUseRecHubCurrent.Checked;
            }
        }

        public string SelectedTestBuild {
            get {
                if(this.cboTestBuild.SelectedIndex > 0) {
                    return (this.cboTestBuild.SelectedItem.ToString());
                } else {
                    return (string.Empty);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e) {
            //if(e.ColumnIndex == 4) {
            //    if(_RowToChanges.ContainsKey(e.RowIndex)) {
            //        WS.Undo(HasPendingChanges(_RowToChanges[e.RowIndex]));
            //    }
            //}
            //FormatCell(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex], HasPendingChanges(_RowToChanges[e.RowIndex]).Count() == 0);
        }

        private void cmdCleanWorkspace_Click(object sender, EventArgs e) {
            PendingChange[] changes = HasPendingChanges();
            
            if (changes.Count() > 0) {
                if (MessageBox.Show("Are you sure you want to Undo all the changes from this Workspace?", "Undo Changes?", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes) {
                    Cursor.Current = Cursors.WaitCursor;
                    WS.Undo(changes);
                    FormatCell(cmdCleanWorkspace, HasPendingChanges().Count() == 0);
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void cmdSharedDep_Click(object sender, EventArgs e) {
            dlgImportDependancy frmImpDep = new dlgImportDependancy(SharedDepDir);

            if(frmImpDep.ShowDialog() == DialogResult.OK) {
                if(!Directory.Exists(frmImpDep.NewPath)) {
                    Directory.CreateDirectory(frmImpDep.NewPath);
                }
                foreach(string sCurFile in frmImpDep.BinFiles) {
                    File.Copy(sCurFile, Path.Combine(frmImpDep.NewPath, Path.GetFileName(sCurFile)), true);
                }
                WS.PendAdd(frmImpDep.NewPath, true);
                if(MessageBox.Show("Do you want to checkin new Shared Dependancies?", "Shared Dependancies Checkin?", MessageBoxButtons.YesNo) == DialogResult.Yes) {
                    //PendingChange[] changes = WS.
                    //WS.CheckIn()
                }
            }
        }
    }
}
