using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Data.SqlClient;
using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.DAL;
using WFS.integraPAY.Online.PartitionManager;

/******************************************************************************
** Wausau
** Copyright © 1997-2008 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     1/14/2009
*
* Purpose:  
*
* Modification History
* CR 28522 JMC 01/06/2010
*    -Changed name of options class to be specific to this application
* CR 28875 JCS 06/09/2010
*    -Use Base64 encoding for user passwords after encryption.
* CR 45288 JCS 06/28/2011
*    -Upgraded Update Password to use SHA hashing
* CR 32263 WJS 08/15/2011
*    -Support to change SetPassword to make is so centralized place
* CR 46314 WJS 09/26/2011
*    -Support to forgot password with additional fields
******************************************************************************/
namespace WFS.integraPAY.Online.PartitionManager {
    public partial class DatabasePartitioning:Form {
        private PartitionManager objPartitionManager;
        public DataTable dtPartitionManager;
        public DataTable dtPartitionDisks;
        public int iRecordNo;
        public string strLogonName;
        public string strMode; 

        public DatabasePartitioning() {
            InitializeComponent();
            objPartitionManager = new PartitionManager();
            
        }
    //////////////
    // On Enter //
    //////////////
    
        private void tabPartitionManager_Enter(object sender,EventArgs e) {
            dtPartitionManager = new DataTable();
            ArrayList RangeSettings = new ArrayList();
            // Setup Listbox
            RangeSettings.Add(new RangeSetting("0","No Partitioning Used"));
            RangeSettings.Add(new RangeSetting("1","Weekly Partitioning"));
            RangeSettings.Add(new RangeSetting("2","Monthly Partitioning"));
            lstRangeSettings.DataSource = RangeSettings;
            lstRangeSettings.ValueMember = "RangeSettingValue";
            lstRangeSettings.DisplayMember = "RangeSettingDescription";
           
            // Retrieve Data
            LoadPartitionManagerTable();
            // Fill in fields on form.
            iRecordNo = 0; // first record.
            if (strMode != "PartitionManagerAdd"){
                this.btnDatabasePartitionCancel.Text = "Exit";
                FillInData(iRecordNo);
            }else{
                this.btnDatabasePartitionCancel.Text = "Cancel";
            }
            // Enable/Disable navigation buttons
            btnPreviousRecord.Enabled = false;
            if ((dtPartitionManager.Rows.Count>1) && (strMode != "PartitionManagerAdd")){
                btnNextRecord.Enabled = true;
            }else{
                btnNextRecord.Enabled = false;
            }
            this.btnDatabasePartitionOK.Text = "Save";                      
        }
        private void tabPartitionDisks_Enter(object sender,EventArgs e) {
            cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
            dtPartitionDisks = new DataTable();
            if (objPartitionManagerDAL.GetPartitionDisks(out dtPartitionDisks)){
                datagridPartitionDisks.DataSource = dtPartitionDisks;
                this.btnDatabasePartitionOK.Visible = true;
                this.btnDatabasePartitionOK.Text = "Save";
            }else{
                MessageBox.Show("Error in retrieving data from PartitionDisks table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            this.btnDatabasePartitionCancel.Text = "Exit";
            strMode = "PartitionDisks";
            datagridPartitionDisks.Refresh();
        }
        private void tabPartitionEventTracking_Enter(object sender,EventArgs e) {
            cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
            DataTable dtPartitionEventTracking = new DataTable();
            if (objPartitionManagerDAL.GetPartitionEventTracking(out dtPartitionEventTracking)){
                datagridEventTracking.DataSource = dtPartitionEventTracking;
            }else{
                MessageBox.Show("Error in retrieving data from PartitionEventTracking table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
            datagridEventTracking.Refresh();
            this.btnDatabasePartitionOK.Visible = false;
            this.btnDatabasePartitionCancel.Text = "Exit";
            
        }
        private void tabChangePassword_Enter(object sender,EventArgs e) {
            this.btnDatabasePartitionOK.Visible = false;
            this.btnDatabasePartitionCancel.Text = "Exit";
        }       
        private void lblPasswordChange_Enter(object sender,EventArgs e) {
            lblPasswordChange.Visible = false;
            txtConfirmPassword.Text = "";
            txtNewPassword.Text = "";
            txtOldPassword.Text = "";
        }
    /////////////////////////////////////
    // Main Form OK and Cancel Buttons //
    /////////////////////////////////////

        private void btnDatabasePartitionCancel_Click(object sender,EventArgs e) {
            if (strMode == "PartitionManagerAdd"){
                this.btnAddPartitionManager.Enabled = true;
                this.btnDeletePartitionManager.Enabled = true;
                this.btnDatabasePartitionCancel.Text = "Exit";
                this.btnDatabasePartitionOK.Text = "OK";
                strMode = "";
                this.tabPartitionManager_Enter(sender,e);
            }else{
                this.Close();
            }
        }
        private void btnDatabasePartitionOK_Click(object sender,EventArgs e) {
            // check if first tab is in add mode.
            if (strMode == "PartitionManagerAdd"){
                AddNewPartitionManager();
                this.btnAddPartitionManager.Enabled = true;
                this.btnDeletePartitionManager.Enabled = true;
                this.btnDatabasePartitionCancel.Text = "Exit";
                this.btnDatabasePartitionOK.Visible = false;
                strMode = "";
                this.tabPartitionManager_Enter(sender,e);
            }
            if (strMode == "PartitionDisks"){
                // Compare database data with datatable and make additions/updates/deletes accordingly
                // Done manually since we are using stored procedures and our stored procedures use NO COUNT.
                DataTable dtOrigPartitionDisks = new DataTable();
                cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
                objPartitionManagerDAL.GetPartitionDisks(out dtOrigPartitionDisks);
                foreach(DataRow dr in dtPartitionDisks.Rows){
                    if (dr.RowState == DataRowState.Deleted){
                        // Delete row in original
                       if (!(objPartitionManagerDAL.DeletePartitionDisks(dr["PartitionManagerID",DataRowVersion.Original].ToString(),dr["DiskOrder",DataRowVersion.Original].ToString()))){
                            MessageBox.Show("Error in deleting data from PartitionDisks table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                            break;
                        }
                    }else{
                        String strSelectionCriteria = "PartitionManagerID = "+dr["PartitionManagerID"].ToString()+" AND DiskOrder = "+dr["DiskOrder"].ToString();
                        DataRow[] drFound = dtOrigPartitionDisks.Select(strSelectionCriteria);        
                        if (drFound.Length>0){
                            //found now see if need any updates.
                            if (drFound.GetHashCode() != dr.GetHashCode()){ //Check if any differences.
                                if (!(objPartitionManagerDAL.UpdatePartitionDisks(dr["PartitionManagerID"].ToString(),dr["DiskOrder"].ToString(),dr["DiskPath"].ToString()))){
                                    MessageBox.Show("Error in updating data to PartitionDisks table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                                    break;
                                }
                            }
                        }else{
                            //Not found so insert.
                            // Check for referential integrity.
                            DataRow[] drTest = dtPartitionManager.Select("PartitionManagerID = "+dr["PartitionManagerID"].ToString());
                            if (drTest.Length>0){
                               if (!(objPartitionManagerDAL.AddPartitionDisks(dr["PartitionManagerID"].ToString(),dr["DiskOrder"].ToString(),dr["DiskPath"].ToString()))){
                                    MessageBox.Show("Error in adding data to PartitionDisks table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                                    break;
                                }
                            }else{
                                // PartitionManagerID doesn't exist in PartitionManager Table.
                                MessageBox.Show("Partition Manager ID entered does not exist in PartitionManager Table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                            }
                        }
                    }
                }
                tabPartitionDisks_Enter(sender,e);
            }
        }
    //////////////////////
    // Tab Form Buttons //
    //////////////////////
        private void btnUpdatePassword_Click(object sender,EventArgs e) {
            string strEncryptedNewPassword = "";
            string strNewPassword = "";
            string strErrorMessage = string.Empty;
            DataTable dtPartitionEventTracking = new DataTable();
            DataTable dtUser = null;
            try
            {
                using (cUserDAL objUserDAL = new cUserDAL(objPartitionManager.PartitionMgrOptions.siteKey))
                {
                    DataRow dr = null;
                    int iUserID;
                    errorDatabasePartitioning.SetError(this.txtNewPassword, "");
                    errorDatabasePartitioning.SetError(this.txtOldPassword, "");
                    errorDatabasePartitioning.SetError(this.txtConfirmPassword, "");
                    if (this.txtConfirmPassword.Text.Equals(this.txtNewPassword.Text) == true)
                    {
                        if (this.txtNewPassword.Text.Equals(this.txtOldPassword.Text) == true)
                        {
                            // old and new password the same
                            errorDatabasePartitioning.SetError(this.txtNewPassword, "New and Old Password are the same, please select new password");
                        }
                        else
                        {
                            if ((this.txtNewPassword.Text.Length >= 6) && (this.txtNewPassword.Text.Length <= 16))
                            {
                                if (objPartitionManager.AuthenticateUser(this.strLogonName.ToString(), this.txtOldPassword.Text))
                                {
                                    strNewPassword = this.txtNewPassword.Text;

                                    // Hash and Base64 encode input password
                                    cHashSHA256 hshSHA256 = new cHashSHA256();
                                    strEncryptedNewPassword = hshSHA256.GetHashBase64(strNewPassword);
                                    strNewPassword = "";

                                    if (objUserDAL.GetPartitionManagerUser(this.strLogonName.ToString(), out dtUser))
                                    {
                                        if (dtUser.Rows.Count > 0)
                                        {
                                            dr = dtUser.Rows[0];
                                            if (dr["UserID"].ToString() != null)
                                            {
                                                iUserID = Convert.ToInt32(dr["UserID"].ToString());
                                                if (objUserDAL.UpdateUserPassword(iUserID, strEncryptedNewPassword, 0,
                                                        FFIECPasswordConstraints.NumberOfDaysPasswordHistory,0,0, out strErrorMessage))
                                                {
                                                    lblPasswordChange.Visible = true;
                                                    lblPasswordChange.Refresh();
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Password did not save.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                }
                                            }
                                        }
                                    }

                                }
                                else
                                {
                                    // old password incorrect
                                    errorDatabasePartitioning.SetError(this.txtOldPassword, "Old Password is incorrect");
                                }
                            }
                            else
                            {
                                // New password need length need to be greater than 6 and less than 16 characters.
                                errorDatabasePartitioning.SetError(this.txtNewPassword, "New Password must be between 6 and 16 characters");
                            }
                        }
                    }
                    else
                    {
                        // new password and confirm password don't match
                        errorDatabasePartitioning.SetError(this.txtConfirmPassword, "New Password does not match Confirm Password");
                    }
                }
            }
            finally
            {
                if (dtPartitionEventTracking != null)
                {
                    dtPartitionEventTracking.Dispose();
                    dtPartitionEventTracking = null;
                }
                if (dtUser != null)
                {
                    dtUser.Dispose();
                    dtUser = null;
                }
            }
        }
        private void btnNextRecord_Click(object sender,EventArgs e) {
            if ((iRecordNo+1)<dtPartitionManager.Rows.Count){
                iRecordNo++;
            }
            FillInData(iRecordNo);
            btnPreviousRecord.Enabled = true;
            if ((iRecordNo+1) == dtPartitionManager.Rows.Count){
                btnNextRecord.Enabled = false;
            }
            this.Refresh();
        }
        private void btnPreviousRecord_Click(object sender,EventArgs e) {
            if (iRecordNo != 0){
                iRecordNo--;
            }
            FillInData(iRecordNo);
            if ((iRecordNo+1) == dtPartitionManager.Rows.Count){
                btnNextRecord.Enabled = false;
            }else{
                btnNextRecord.Enabled = true;
            }
            if (iRecordNo == 0){
                btnPreviousRecord.Enabled = false;
            }
            this.Refresh();
        }

        private void btnAddPartitionManager_Click(object sender,EventArgs e) {
            strMode = "PartitionManagerAdd";
            txtPartitionManagerID.Text = "";
            txtPartitionIdentifier.Text = "";
            txtPartitionFunctionName.Text = "";
            txtPartitionSchemaName.Text = "";
            txtPartitionSize.Text = "";
            lstRangeSettings.SelectedValue = "0";
            txtLastDiskID.Text = "";
            this.btnAddPartitionManager.Enabled = false;
            this.btnDeletePartitionManager.Enabled = false;
            this.btnDatabasePartitionOK.Visible = true;
            this.btnDatabasePartitionCancel.Text = "Cancel";
            this.btnDatabasePartitionOK.Text = "Save";
            this.Refresh();
        }

        private void btnDeletePartitionManager_Click(object sender,EventArgs e) {
            cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
            int iPartitionManagerID = Convert.ToInt32(dtPartitionManager.Rows[iRecordNo]["PartitionManagerID"].ToString());
            if (MessageBox.Show("Do you really want to delete this record?", "Confirm Delete", MessageBoxButtons.YesNo)== DialogResult.Yes){
               if (!(objPartitionManagerDAL.DeletePartitionManager(iPartitionManagerID))){
                   MessageBox.Show("Error:  Record in Partition Manager was NOT deleted!","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
               }           
            }
            LoadPartitionManagerTable();
            this.btnPreviousRecord_Click(sender, e);  
        }
    ///////////
    // Other //
    ///////////
        private void FillInData(int iRecordNo){
            txtPartitionManagerID.Text = dtPartitionManager.Rows[iRecordNo]["PartitionManagerID"].ToString();
            txtPartitionIdentifier.Text = dtPartitionManager.Rows[iRecordNo]["PartitionIdentifier"].ToString();
            txtPartitionFunctionName.Text = dtPartitionManager.Rows[iRecordNo]["PartitionFunctionName"].ToString();
            txtPartitionSchemaName.Text = dtPartitionManager.Rows[iRecordNo]["PartitionSchemeName"].ToString();
            txtPartitionSize.Text = dtPartitionManager.Rows[iRecordNo]["SizeMB"].ToString();
            lstRangeSettings.SelectedValue = dtPartitionManager.Rows[iRecordNo]["RangeSetting"].ToString();
            txtLastDiskID.Text = dtPartitionManager.Rows[iRecordNo]["LastDiskID"].ToString();
        }
        private void LoadPartitionManagerTable(){
            cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
            if (!(objPartitionManagerDAL.GetPartitionManager(out dtPartitionManager))){
                 MessageBox.Show("Error in retrieving data from PartitionManager table.","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
        private void AddNewPartitionManager(){
           cPartitionManagerDAL objPartitionManagerDAL = new cPartitionManagerDAL(objPartitionManager.PartitionMgrOptions.siteKey);
            if (CheckPartitionManagerInputs()){
                if (objPartitionManagerDAL.AddPartitionManager(txtPartitionIdentifier.Text.ToString(),txtPartitionFunctionName.Text.ToString(),  txtPartitionSchemaName.Text.ToString(), txtPartitionSize.Text.ToString(), lstRangeSettings.SelectedValue.ToString(), txtLastDiskID.Text.ToString())){
                     strMode = "";
                }else{
                    MessageBox.Show("Error: Data was NOT added to Partition Manager","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
                }
            }else{
                MessageBox.Show("Invalid Input","Error",MessageBoxButtons.OK,MessageBoxIcon.Error);
            }
        }
        private bool CheckPartitionManagerInputs(){
            bool bReturn = true;
            Regex objAlphNumPattern = new Regex("[^a-zA-Z0-9]");
            Regex objNumPattern = new Regex("[^0-9]");
            if ((objAlphNumPattern.IsMatch(this.txtPartitionIdentifier.Text))|| (this.txtPartitionIdentifier.Text.Length>20)){
                bReturn = false;
            }
            if ((objAlphNumPattern.IsMatch(this.txtPartitionFunctionName.Text))|| (this.txtPartitionFunctionName.Text.Length>256)){
                bReturn = false;
            }
            if ((objAlphNumPattern.IsMatch(this.txtPartitionSchemaName.Text))|| (this.txtPartitionSchemaName.Text.Length>256)){
                bReturn = false;
            }
            if ((objNumPattern.IsMatch(this.txtPartitionSize.Text))||(Convert.ToInt64(this.txtPartitionSize.Text) > Convert.ToInt64(2147483646)) || (Convert.ToInt64(this.txtPartitionSize.Text)<0)){
                bReturn = false;
            }
            if ((objNumPattern.IsMatch(this.txtLastDiskID.Text))||(Convert.ToInt64(this.txtLastDiskID.Text) > Convert.ToInt64(2147483646)) || (Convert.ToInt64(this.txtLastDiskID.Text)<0)){
                bReturn = false;
            }            
            return bReturn;
        }

        private void tabPartitionDisks_Click(object sender,EventArgs e) {

        }

         
    }
    public class RangeSetting
    {
        private string strValue;
        private string strDescription;
        public RangeSetting(string strValue, string strDescription){
            this.strValue = strValue;
            this.strDescription = strDescription;
        }
        public string RangeSettingValue{
            get{return strValue;}
        }
        public string RangeSettingDescription{
            get{return strDescription;}
        }
    }
}
