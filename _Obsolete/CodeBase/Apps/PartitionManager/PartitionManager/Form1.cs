﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.PartitionManager;

namespace WFS.integraPAY.Online.PartitionManager {
    public partial class SplashForm:Form {
        

        public SplashForm() {
            InitializeComponent();
        }
        private void LogonButton_Click(object sender,EventArgs e) {
            this.LogonButton.Enabled = false;
            this.Refresh();
            LogonForm objLogonForm = new LogonForm();
            objLogonForm.Show();
            objLogonForm.Focus();
        }

        private void btnExit_Click(object sender,EventArgs e) {
            this.Close();
        }
       
    }
}
