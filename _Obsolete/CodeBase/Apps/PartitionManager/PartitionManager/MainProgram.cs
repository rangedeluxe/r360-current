﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.PartitionManager;

namespace WFS.integraPAY.Online.PartitionManager {
    public static class MainProgram {
       [STAThread]
        static void Main() {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new SplashForm());
        }
       
    }
}
