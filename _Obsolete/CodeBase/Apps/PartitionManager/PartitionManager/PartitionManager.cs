﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Data;
using System.Net;
using WFS.integraPAY.Online.Common;
using WFS.integraPAY.Online.DAL;
using WFS.integraPAY.Online.PartitionManager;


/******************************************************************************
** Wausau
** Copyright © 1997-2008 Wausau All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright Wausau 2008.  All Rights Reserved.
* These materials are unpublished confidential and proprietary
* information of Wausau and contain Wausau trade secrets.  These materials
* may not be used, copied, modified or disclosed except as expressly
* permitted in writing by Wausau (see the Wausau license agreement for
* details).  All copies, modifications and derivative works of these
* materials are property of Wausau.
*
* Author:   
* Date:     1/14/2009
*
* Purpose:  
*
* Modification History
* CR 28522 JMC 01/06/2010
*    - Changed name of options class to be specific to this application
* CR 28875 JCS 06/09/2010
*    - Use Base64 encoding for user passwords after encrypting and handle if encoding has not yet occurred.
* CR 23161 JCS 05/17/2011
*    - Modify AuthenticateUser() to replace CBOCrypto password hashing.
* CR 45288 JCS 06/28/2011
*    - Upgraded password hash to SHA-2
******************************************************************************/
namespace WFS.integraPAY.Online.PartitionManager {
    class PartitionManager {
        private cPartitionMgrOptions _PartitionMgrOptions = null;
        private cEventLog _EventLog = null;
        private string _SiteKey = string.Empty;
        
        public bool CheckInputs(string strLogon, string strPassword){
            bool bReturn = false;
            // LogonName has to be alphanumeric and 16 character or less.
            Regex objLogonPattern = new Regex("[^a-zA-Z0-9]");
            if (!(objLogonPattern.IsMatch(strLogon))&& (strLogon.Length<=16)){
                // Password is encrypted and must be 16 characters or less.
                if ((strPassword.Length>0) && (strPassword.Length<=16)){
                    bReturn = true;
                }
            }
            return bReturn;
        }
        protected string SiteKey {
            get {
    		    StringCollection arrSites;
                string[] arrEntry;
        		
    		    if(_SiteKey == string.Empty) {
        		
				    // Retrieve null separated list of key/site value pairs and separate into array
                    IPHostEntry ipEntry = Dns.GetHostEntry (Dns.GetHostName());
                    IPAddress [] addr = ipEntry.AddressList;
    			    arrSites = ipoINILib.GetINISection("Sites");
        			
                    foreach(string strSite in arrSites) {                    
    				    if(strSite.Length > 0) {
	    				    arrEntry = strSite.Split('=');	// Split site key/value pair into array
	    				    if(addr[0].ToString() == arrEntry[arrEntry.GetUpperBound(0)]) {
	    					    _SiteKey = arrEntry[arrEntry.GetLowerBound(0)];
	    				    }
	    			    }
    			    }
    		    }
    		    return(_SiteKey);
            }
        }
        public bool AuthenticateUser(string strLogon, string strPassword){
            bool bReturn = false;
            string strEncodedPassword = "";
            cUserDAL objUserDAL = new cUserDAL(this.SiteKey);
            DataTable dtUser = null;
            DataRow dr;
            cHashSHA256 hshSHA = new cHashSHA256();
            
            // Hash and Base64 encode input password
            strEncodedPassword = hshSHA.GetHashBase64(strPassword);

            if (objUserDAL.GetPartitionManagerUser(strLogon, out dtUser)) {
                if ((dtUser != null) && (dtUser.Rows.Count > 0)) {
                    dr = dtUser.Rows[0];
                    if (string.Compare(dr["Password"].ToString(), strEncodedPassword) == 0) {
                       bReturn = true;
                    } else {

                        // Handle MD5 hashed password not yet upgraded to SHA
                        cHashMD5 hshMD5 = new cHashMD5();

                        // Handle MD5 hash stored passwords (including Base64 endoded hashed values)
                        if (ipoLib.TryBase64Decode(dr["Password"].ToString())) {
                            if (string.Compare(dr["Password"].ToString(), hshMD5.GetHashBase64(strPassword)) == 0)
                                bReturn = true;
                        } else {
                            if (string.Compare(dr["Password"].ToString(), hshMD5.GetHash(strPassword, System.Text.ASCIIEncoding.Default)) == 0)
                                bReturn = true;
                        }
                    }
                }
            }

            return bReturn;
        }
              
         public cPartitionMgrOptions PartitionMgrOptions{
            get{
                try{
                    if(_PartitionMgrOptions == null){
                        _PartitionMgrOptions=new cPartitionMgrOptions("IPOnline");
                    }
                }catch{}
                return _PartitionMgrOptions;
            }
        }
        public cEventLog eventLog{
            get{
                if(_EventLog == null){
                    _EventLog = new cEventLog(PartitionMgrOptions.logFilePath
                                                ,PartitionMgrOptions.logFileMaxSize
                                                ,(MessageImportance)PartitionMgrOptions.loggingDepth);
                }
                return _EventLog;
            }
        }
    }
}
