﻿namespace WFS.integraPAY.Online.PartitionManager {
    partial class SplashForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
        if(disposing && (components != null)) {
        components.Dispose();
        }
        base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SplashForm));
        this.SplashText = new System.Windows.Forms.RichTextBox();
        this.LogonButton = new System.Windows.Forms.Button();
        this.btnExit = new System.Windows.Forms.Button();
        this.SuspendLayout();
        // 
        // SplashText
        // 
        this.SplashText.AccessibleRole = System.Windows.Forms.AccessibleRole.None;
        this.SplashText.BackColor = System.Drawing.SystemColors.Window;
        this.SplashText.Font = new System.Drawing.Font("Arial",14.25F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.SplashText.Location = new System.Drawing.Point(31,30);
        this.SplashText.Name = "SplashText";
        this.SplashText.ReadOnly = true;
        this.SplashText.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.None;
        this.SplashText.ShortcutsEnabled = false;
        this.SplashText.Size = new System.Drawing.Size(453,149);
        this.SplashText.TabIndex = 0;
        this.SplashText.TabStop = false;
        this.SplashText.Text = resources.GetString("SplashText.Text");
        // 
        // LogonButton
        // 
        this.LogonButton.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.LogonButton.ForeColor = System.Drawing.Color.Black;
        this.LogonButton.Location = new System.Drawing.Point(133,186);
        this.LogonButton.Name = "LogonButton";
        this.LogonButton.Size = new System.Drawing.Size(111,37);
        this.LogonButton.TabIndex = 1;
        this.LogonButton.Text = "Logon";
        this.LogonButton.UseVisualStyleBackColor = true;
        this.LogonButton.Click += new System.EventHandler(this.LogonButton_Click);
        // 
        // btnExit
        // 
        this.btnExit.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnExit.ForeColor = System.Drawing.Color.Black;
        this.btnExit.Location = new System.Drawing.Point(263,186);
        this.btnExit.Name = "btnExit";
        this.btnExit.Size = new System.Drawing.Size(116,36);
        this.btnExit.TabIndex = 2;
        this.btnExit.Text = "Exit";
        this.btnExit.UseVisualStyleBackColor = true;
        this.btnExit.Click += new System.EventHandler(this.btnExit_Click);
        // 
        // SplashForm
        // 
        this.AllowDrop = true;
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F,13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(518,235);
        this.Controls.Add(this.btnExit);
        this.Controls.Add(this.LogonButton);
        this.Controls.Add(this.SplashText);
        this.ForeColor = System.Drawing.Color.White;
        this.MaximizeBox = false;
        this.MinimizeBox = false;
        this.Name = "SplashForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "IntegraPAY Online";
        this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.RichTextBox SplashText;
        private System.Windows.Forms.Button LogonButton;
        private System.Windows.Forms.Button btnExit;
    }
}

