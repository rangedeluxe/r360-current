﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WFS.integraPAY.Online.PartitionManager {
    public partial class LogonForm:Form {
        public LogonForm() {
            InitializeComponent();
        }

        private void btnCancel_Click(object sender,EventArgs e) {
          Close();
        }

        private void btnOK_Click(object sender,EventArgs e) {
            PartitionManager objPartitionManager = new PartitionManager();
            if (!(objPartitionManager.CheckInputs(this.txtLogon.Text,this.txtPassword.Text))){
                LogonErrorProvider.SetError(this.txtLogon,"Logon/Password Incorrect"); 
            }
            if (objPartitionManager.AuthenticateUser(this.txtLogon.Text,this.txtPassword.Text)){
                LogonErrorProvider.SetError(this.txtLogon,"");
                DatabasePartitioning frmDatabasePartition = new DatabasePartitioning();
                frmDatabasePartition.strLogonName = this.txtLogon.Text;
                frmDatabasePartition.Show();
                Close();
            }else{
                LogonErrorProvider.SetError(this.txtLogon,"Logon/Password Incorrect");
            }
         }

    }
}
