﻿namespace WFS.integraPAY.Online.PartitionManager {
    partial class LogonForm {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
        if(disposing && (components != null)) {
        components.Dispose();
        }
        base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        this.components = new System.ComponentModel.Container();
        this.btnOK = new System.Windows.Forms.Button();
        this.btnCancel = new System.Windows.Forms.Button();
        this.txtLogon = new System.Windows.Forms.TextBox();
        this.txtPassword = new System.Windows.Forms.TextBox();
        this.lblLogon = new System.Windows.Forms.Label();
        this.lblPassword = new System.Windows.Forms.Label();
        this.LogonErrorProvider = new System.Windows.Forms.ErrorProvider(this.components);
        ((System.ComponentModel.ISupportInitialize)(this.LogonErrorProvider)).BeginInit();
        this.SuspendLayout();
        // 
        // btnOK
        // 
        this.btnOK.Font = new System.Drawing.Font("Arial Black",11.25F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnOK.Location = new System.Drawing.Point(67,123);
        this.btnOK.Name = "btnOK";
        this.btnOK.Size = new System.Drawing.Size(75,31);
        this.btnOK.TabIndex = 3;
        this.btnOK.Text = "OK";
        this.btnOK.UseVisualStyleBackColor = true;
        this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
        // 
        // btnCancel
        // 
        this.btnCancel.Font = new System.Drawing.Font("Arial Black",11.25F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnCancel.Location = new System.Drawing.Point(173,123);
        this.btnCancel.Name = "btnCancel";
        this.btnCancel.Size = new System.Drawing.Size(75,31);
        this.btnCancel.TabIndex = 4;
        this.btnCancel.Text = "Cancel";
        this.btnCancel.UseVisualStyleBackColor = true;
        this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
        // 
        // txtLogon
        // 
        this.txtLogon.Location = new System.Drawing.Point(114,25);
        this.txtLogon.Name = "txtLogon";
        this.txtLogon.Size = new System.Drawing.Size(164,20);
        this.txtLogon.TabIndex = 1;
        this.txtLogon.WordWrap = false;
        // 
        // txtPassword
        // 
        this.txtPassword.Location = new System.Drawing.Point(114,66);
        this.txtPassword.Name = "txtPassword";
        this.txtPassword.PasswordChar = '*';
        this.txtPassword.Size = new System.Drawing.Size(164,20);
        this.txtPassword.TabIndex = 2;
        this.txtPassword.UseSystemPasswordChar = true;
        // 
        // lblLogon
        // 
        this.lblLogon.AutoSize = true;
        this.lblLogon.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblLogon.Location = new System.Drawing.Point(43,24);
        this.lblLogon.Name = "lblLogon";
        this.lblLogon.Size = new System.Drawing.Size(65,19);
        this.lblLogon.TabIndex = 5;
        this.lblLogon.Text = "Logon:";
        // 
        // lblPassword
        // 
        this.lblPassword.AutoSize = true;
        this.lblPassword.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPassword.Location = new System.Drawing.Point(16,65);
        this.lblPassword.Name = "lblPassword";
        this.lblPassword.Size = new System.Drawing.Size(92,19);
        this.lblPassword.TabIndex = 6;
        this.lblPassword.Text = "Password:";
        // 
        // LogonErrorProvider
        // 
        this.LogonErrorProvider.ContainerControl = this;
        // 
        // LogonForm
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F,13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(318,166);
        this.Controls.Add(this.lblPassword);
        this.Controls.Add(this.lblLogon);
        this.Controls.Add(this.txtPassword);
        this.Controls.Add(this.txtLogon);
        this.Controls.Add(this.btnCancel);
        this.Controls.Add(this.btnOK);
        this.MaximizeBox = false;
        this.MinimizeBox = false;
        this.Name = "LogonForm";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "IntegraPAY Online Database Logon";
        this.ResumeLayout(false);
        this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.TextBox txtLogon;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.Label lblLogon;
        private System.Windows.Forms.Label lblPassword;
        private System.Windows.Forms.ErrorProvider LogonErrorProvider;
    }
}