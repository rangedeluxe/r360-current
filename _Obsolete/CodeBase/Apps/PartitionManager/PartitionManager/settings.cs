﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Text;
using System.Runtime.InteropServices;

using WFS.integraPAY.Online.Common;
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author:   Jason Efken
* Date:     05/08/2009
*
* Purpose:  IntegraPAY Database Partition settings
*
* Modification History
* CR 28522 JMC 01/06/2010
*     - Settings class now inherits from cSiteOptions.
******************************************************************************/
namespace WFS.integraPAY.Online.PartitionManager{

    public class cPartitionMgrOptions : cSiteOptions {

        private string _ExcludedPWDCharacters;
        private int _PasswordMinLength;
        private string _PasswordRequiredFormat;
        
        public cPartitionMgrOptions(string SiteKey) : base(SiteKey) {

            StringCollection colSiteOptions = ipoINILib.GetINISection(SiteKey);
            string strKey;
            string strValue;
            
            const string INIKEY_PWD_MIN_LEN = "PasswordMinLength";
            const string INIKEY_PWD_REQ_FORMAT = "PasswordRequiredFormat";
            const string INIKEY_EXCLUDED_PWD_CHARS = "ExcludedPWDCharacters";
            
            int intTemp;

            System.Collections.IEnumerator myEnumerator;
            _PasswordMinLength = 0;
            _PasswordRequiredFormat = string.Empty;
            try {
                myEnumerator = ((IEnumerable)colSiteOptions).GetEnumerator();
                while ( myEnumerator.MoveNext() ) {

                    strKey = myEnumerator.Current.ToString().Substring(0, myEnumerator.Current.ToString().IndexOf('='));
                    strValue = myEnumerator.Current.ToString().Substring(strKey.Length+1);

                    if(strKey.ToLower() == INIKEY_PWD_MIN_LEN.ToLower()) {
                        if(int.TryParse(strValue, out intTemp)) {
                            _PasswordMinLength = intTemp;
                        } else {
                            _PasswordMinLength = 0;
                        }
                    }
                    else if(strKey.ToLower() == INIKEY_PWD_REQ_FORMAT.ToLower()) {
                        _PasswordRequiredFormat = strValue;
                    }
                    else if(strKey.ToLower() == INIKEY_EXCLUDED_PWD_CHARS.ToLower()) {
                        _ExcludedPWDCharacters = strValue;
                    }
                }
            } catch(Exception ex) {
                throw(new Exception("Unable to load site definition for site key " + SiteKey, ex));
            }
        }
    }
}
