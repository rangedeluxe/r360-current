﻿namespace WFS.integraPAY.Online.PartitionManager {
    partial class DatabasePartitioning {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
        if(disposing && (components != null)) {
        components.Dispose();
        }
        base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
        this.components = new System.ComponentModel.Container();
        this.tabDatabasePartition = new System.Windows.Forms.TabControl();
        this.tabPartitionManager = new System.Windows.Forms.TabPage();
        this.btnDeletePartitionManager = new System.Windows.Forms.Button();
        this.btnAddPartitionManager = new System.Windows.Forms.Button();
        this.lstRangeSettings = new System.Windows.Forms.ListBox();
        this.btnPreviousRecord = new System.Windows.Forms.Button();
        this.btnNextRecord = new System.Windows.Forms.Button();
        this.lblInMB = new System.Windows.Forms.Label();
        this.txtPartitionSize = new System.Windows.Forms.TextBox();
        this.txtLastDiskID = new System.Windows.Forms.TextBox();
        this.txtPartitionSchemaName = new System.Windows.Forms.TextBox();
        this.txtPartitionFunctionName = new System.Windows.Forms.TextBox();
        this.txtPartitionIdentifier = new System.Windows.Forms.TextBox();
        this.txtPartitionManagerID = new System.Windows.Forms.TextBox();
        this.lblLastDiskID = new System.Windows.Forms.Label();
        this.lblRangeSetting = new System.Windows.Forms.Label();
        this.lblPartitionSize = new System.Windows.Forms.Label();
        this.lblPartitionSchemaName = new System.Windows.Forms.Label();
        this.lblPartitionFunctionName = new System.Windows.Forms.Label();
        this.lblPartitionIdentifier = new System.Windows.Forms.Label();
        this.lblPartitionManagerID = new System.Windows.Forms.Label();
        this.tabPartitionDisks = new System.Windows.Forms.TabPage();
        this.datagridPartitionDisks = new System.Windows.Forms.DataGridView();
        this.tabPartitionEventTracking = new System.Windows.Forms.TabPage();
        this.datagridEventTracking = new System.Windows.Forms.DataGridView();
        this.tabChangePassword = new System.Windows.Forms.TabPage();
        this.lblPasswordChange = new System.Windows.Forms.Label();
        this.txtConfirmPassword = new System.Windows.Forms.TextBox();
        this.lblConfirmPassword = new System.Windows.Forms.Label();
        this.lblNewPassword = new System.Windows.Forms.Label();
        this.lblOldPassword = new System.Windows.Forms.Label();
        this.btnUpdatePassword = new System.Windows.Forms.Button();
        this.txtNewPassword = new System.Windows.Forms.TextBox();
        this.txtOldPassword = new System.Windows.Forms.TextBox();
        this.btnDatabasePartitionOK = new System.Windows.Forms.Button();
        this.btnDatabasePartitionCancel = new System.Windows.Forms.Button();
        this.errorDatabasePartitioning = new System.Windows.Forms.ErrorProvider(this.components);
        this.label1 = new System.Windows.Forms.Label();
        this.tabDatabasePartition.SuspendLayout();
        this.tabPartitionManager.SuspendLayout();
        this.tabPartitionDisks.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.datagridPartitionDisks)).BeginInit();
        this.tabPartitionEventTracking.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.datagridEventTracking)).BeginInit();
        this.tabChangePassword.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)(this.errorDatabasePartitioning)).BeginInit();
        this.SuspendLayout();
        // 
        // tabDatabasePartition
        // 
        this.tabDatabasePartition.Controls.Add(this.tabPartitionManager);
        this.tabDatabasePartition.Controls.Add(this.tabPartitionDisks);
        this.tabDatabasePartition.Controls.Add(this.tabPartitionEventTracking);
        this.tabDatabasePartition.Controls.Add(this.tabChangePassword);
        this.tabDatabasePartition.Location = new System.Drawing.Point(0,1);
        this.tabDatabasePartition.Name = "tabDatabasePartition";
        this.tabDatabasePartition.SelectedIndex = 0;
        this.tabDatabasePartition.Size = new System.Drawing.Size(702,428);
        this.tabDatabasePartition.TabIndex = 0;
        // 
        // tabPartitionManager
        // 
        this.tabPartitionManager.Controls.Add(this.btnDeletePartitionManager);
        this.tabPartitionManager.Controls.Add(this.btnAddPartitionManager);
        this.tabPartitionManager.Controls.Add(this.lstRangeSettings);
        this.tabPartitionManager.Controls.Add(this.btnPreviousRecord);
        this.tabPartitionManager.Controls.Add(this.btnNextRecord);
        this.tabPartitionManager.Controls.Add(this.lblInMB);
        this.tabPartitionManager.Controls.Add(this.txtPartitionSize);
        this.tabPartitionManager.Controls.Add(this.txtLastDiskID);
        this.tabPartitionManager.Controls.Add(this.txtPartitionSchemaName);
        this.tabPartitionManager.Controls.Add(this.txtPartitionFunctionName);
        this.tabPartitionManager.Controls.Add(this.txtPartitionIdentifier);
        this.tabPartitionManager.Controls.Add(this.txtPartitionManagerID);
        this.tabPartitionManager.Controls.Add(this.lblLastDiskID);
        this.tabPartitionManager.Controls.Add(this.lblRangeSetting);
        this.tabPartitionManager.Controls.Add(this.lblPartitionSize);
        this.tabPartitionManager.Controls.Add(this.lblPartitionSchemaName);
        this.tabPartitionManager.Controls.Add(this.lblPartitionFunctionName);
        this.tabPartitionManager.Controls.Add(this.lblPartitionIdentifier);
        this.tabPartitionManager.Controls.Add(this.lblPartitionManagerID);
        this.tabPartitionManager.Location = new System.Drawing.Point(4,22);
        this.tabPartitionManager.Name = "tabPartitionManager";
        this.tabPartitionManager.Padding = new System.Windows.Forms.Padding(3);
        this.tabPartitionManager.Size = new System.Drawing.Size(694,402);
        this.tabPartitionManager.TabIndex = 0;
        this.tabPartitionManager.Text = "Partition Manager";
        this.tabPartitionManager.UseVisualStyleBackColor = true;
        this.tabPartitionManager.Enter += new System.EventHandler(this.tabPartitionManager_Enter);
        // 
        // btnDeletePartitionManager
        // 
        this.btnDeletePartitionManager.Font = new System.Drawing.Font("Arial",8.25F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnDeletePartitionManager.Location = new System.Drawing.Point(8,316);
        this.btnDeletePartitionManager.Name = "btnDeletePartitionManager";
        this.btnDeletePartitionManager.Size = new System.Drawing.Size(75,55);
        this.btnDeletePartitionManager.TabIndex = 19;
        this.btnDeletePartitionManager.Text = "Delete Partition Manager";
        this.btnDeletePartitionManager.UseVisualStyleBackColor = true;
        this.btnDeletePartitionManager.Click += new System.EventHandler(this.btnDeletePartitionManager_Click);
        // 
        // btnAddPartitionManager
        // 
        this.btnAddPartitionManager.Font = new System.Drawing.Font("Arial",8.25F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnAddPartitionManager.Location = new System.Drawing.Point(8,253);
        this.btnAddPartitionManager.Name = "btnAddPartitionManager";
        this.btnAddPartitionManager.Size = new System.Drawing.Size(75,52);
        this.btnAddPartitionManager.TabIndex = 18;
        this.btnAddPartitionManager.Text = "Add New Partition Manager";
        this.btnAddPartitionManager.UseVisualStyleBackColor = true;
        this.btnAddPartitionManager.Click += new System.EventHandler(this.btnAddPartitionManager_Click);
        // 
        // lstRangeSettings
        // 
        this.lstRangeSettings.DisplayMember = "0";
        this.lstRangeSettings.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lstRangeSettings.FormattingEnabled = true;
        this.lstRangeSettings.ItemHeight = 16;
        this.lstRangeSettings.Location = new System.Drawing.Point(227,242);
        this.lstRangeSettings.Name = "lstRangeSettings";
        this.lstRangeSettings.Size = new System.Drawing.Size(144,20);
        this.lstRangeSettings.TabIndex = 17;
        // 
        // btnPreviousRecord
        // 
        this.btnPreviousRecord.Font = new System.Drawing.Font("Arial",8.25F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnPreviousRecord.Location = new System.Drawing.Point(196,337);
        this.btnPreviousRecord.Name = "btnPreviousRecord";
        this.btnPreviousRecord.Size = new System.Drawing.Size(139,33);
        this.btnPreviousRecord.TabIndex = 16;
        this.btnPreviousRecord.Text = "Go to Previous Record";
        this.btnPreviousRecord.UseVisualStyleBackColor = true;
        this.btnPreviousRecord.Click += new System.EventHandler(this.btnPreviousRecord_Click);
        // 
        // btnNextRecord
        // 
        this.btnNextRecord.Font = new System.Drawing.Font("Arial",8.25F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnNextRecord.Location = new System.Drawing.Point(359,337);
        this.btnNextRecord.Name = "btnNextRecord";
        this.btnNextRecord.Size = new System.Drawing.Size(152,34);
        this.btnNextRecord.TabIndex = 15;
        this.btnNextRecord.Text = "Go to Next Record";
        this.btnNextRecord.UseVisualStyleBackColor = true;
        this.btnNextRecord.Click += new System.EventHandler(this.btnNextRecord_Click);
        // 
        // lblInMB
        // 
        this.lblInMB.AutoSize = true;
        this.lblInMB.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblInMB.Location = new System.Drawing.Point(356,208);
        this.lblInMB.Name = "lblInMB";
        this.lblInMB.Size = new System.Drawing.Size(44,16);
        this.lblInMB.TabIndex = 14;
        this.lblInMB.Text = "in MB";
        // 
        // txtPartitionSize
        // 
        this.txtPartitionSize.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.txtPartitionSize.Location = new System.Drawing.Point(226,207);
        this.txtPartitionSize.Name = "txtPartitionSize";
        this.txtPartitionSize.Size = new System.Drawing.Size(119,22);
        this.txtPartitionSize.TabIndex = 4;
        // 
        // txtLastDiskID
        // 
        this.txtLastDiskID.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.txtLastDiskID.Location = new System.Drawing.Point(226,283);
        this.txtLastDiskID.Name = "txtLastDiskID";
        this.txtLastDiskID.Size = new System.Drawing.Size(52,22);
        this.txtLastDiskID.TabIndex = 6;
        // 
        // txtPartitionSchemaName
        // 
        this.txtPartitionSchemaName.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.txtPartitionSchemaName.Location = new System.Drawing.Point(226,169);
        this.txtPartitionSchemaName.Name = "txtPartitionSchemaName";
        this.txtPartitionSchemaName.Size = new System.Drawing.Size(401,22);
        this.txtPartitionSchemaName.TabIndex = 3;
        // 
        // txtPartitionFunctionName
        // 
        this.txtPartitionFunctionName.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.txtPartitionFunctionName.Location = new System.Drawing.Point(226,131);
        this.txtPartitionFunctionName.Name = "txtPartitionFunctionName";
        this.txtPartitionFunctionName.Size = new System.Drawing.Size(401,22);
        this.txtPartitionFunctionName.TabIndex = 2;
        // 
        // txtPartitionIdentifier
        // 
        this.txtPartitionIdentifier.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.txtPartitionIdentifier.Location = new System.Drawing.Point(226,96);
        this.txtPartitionIdentifier.Name = "txtPartitionIdentifier";
        this.txtPartitionIdentifier.Size = new System.Drawing.Size(130,22);
        this.txtPartitionIdentifier.TabIndex = 1;
        // 
        // txtPartitionManagerID
        // 
        this.txtPartitionManagerID.Enabled = false;
        this.txtPartitionManagerID.Location = new System.Drawing.Point(228,62);
        this.txtPartitionManagerID.Name = "txtPartitionManagerID";
        this.txtPartitionManagerID.Size = new System.Drawing.Size(52,20);
        this.txtPartitionManagerID.TabIndex = 0;
        // 
        // lblLastDiskID
        // 
        this.lblLastDiskID.AutoSize = true;
        this.lblLastDiskID.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblLastDiskID.Location = new System.Drawing.Point(135,286);
        this.lblLastDiskID.Name = "lblLastDiskID";
        this.lblLastDiskID.Size = new System.Drawing.Size(83,16);
        this.lblLastDiskID.TabIndex = 6;
        this.lblLastDiskID.Text = "Last Disk ID:";
        // 
        // lblRangeSetting
        // 
        this.lblRangeSetting.AutoSize = true;
        this.lblRangeSetting.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblRangeSetting.Location = new System.Drawing.Point(124,245);
        this.lblRangeSetting.Name = "lblRangeSetting";
        this.lblRangeSetting.Size = new System.Drawing.Size(94,16);
        this.lblRangeSetting.TabIndex = 5;
        this.lblRangeSetting.Text = "Range Setting:";
        // 
        // lblPartitionSize
        // 
        this.lblPartitionSize.AutoSize = true;
        this.lblPartitionSize.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPartitionSize.Location = new System.Drawing.Point(130,208);
        this.lblPartitionSize.Name = "lblPartitionSize";
        this.lblPartitionSize.Size = new System.Drawing.Size(90,16);
        this.lblPartitionSize.TabIndex = 4;
        this.lblPartitionSize.Text = "Partition Size:";
        // 
        // lblPartitionSchemaName
        // 
        this.lblPartitionSchemaName.AutoSize = true;
        this.lblPartitionSchemaName.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPartitionSchemaName.Location = new System.Drawing.Point(71,171);
        this.lblPartitionSchemaName.Name = "lblPartitionSchemaName";
        this.lblPartitionSchemaName.Size = new System.Drawing.Size(150,16);
        this.lblPartitionSchemaName.TabIndex = 3;
        this.lblPartitionSchemaName.Text = "Partition Schema Name:";
        // 
        // lblPartitionFunctionName
        // 
        this.lblPartitionFunctionName.AutoSize = true;
        this.lblPartitionFunctionName.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPartitionFunctionName.Location = new System.Drawing.Point(70,133);
        this.lblPartitionFunctionName.Name = "lblPartitionFunctionName";
        this.lblPartitionFunctionName.Size = new System.Drawing.Size(152,16);
        this.lblPartitionFunctionName.TabIndex = 2;
        this.lblPartitionFunctionName.Text = "Partition Function Name:";
        // 
        // lblPartitionIdentifier
        // 
        this.lblPartitionIdentifier.AutoSize = true;
        this.lblPartitionIdentifier.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPartitionIdentifier.Location = new System.Drawing.Point(109,98);
        this.lblPartitionIdentifier.Name = "lblPartitionIdentifier";
        this.lblPartitionIdentifier.Size = new System.Drawing.Size(112,16);
        this.lblPartitionIdentifier.TabIndex = 1;
        this.lblPartitionIdentifier.Text = "Partition Identifier:";
        // 
        // lblPartitionManagerID
        // 
        this.lblPartitionManagerID.AutoSize = true;
        this.lblPartitionManagerID.Font = new System.Drawing.Font("Arial",9.75F,System.Drawing.FontStyle.Regular,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPartitionManagerID.Location = new System.Drawing.Point(92,63);
        this.lblPartitionManagerID.Name = "lblPartitionManagerID";
        this.lblPartitionManagerID.Size = new System.Drawing.Size(130,16);
        this.lblPartitionManagerID.TabIndex = 0;
        this.lblPartitionManagerID.Text = "Partition Manager ID:";
        // 
        // tabPartitionDisks
        // 
        this.tabPartitionDisks.Controls.Add(this.label1);
        this.tabPartitionDisks.Controls.Add(this.datagridPartitionDisks);
        this.tabPartitionDisks.Location = new System.Drawing.Point(4,22);
        this.tabPartitionDisks.Name = "tabPartitionDisks";
        this.tabPartitionDisks.Padding = new System.Windows.Forms.Padding(3);
        this.tabPartitionDisks.Size = new System.Drawing.Size(694,402);
        this.tabPartitionDisks.TabIndex = 1;
        this.tabPartitionDisks.Text = "Partition Disks";
        this.tabPartitionDisks.UseVisualStyleBackColor = true;
        this.tabPartitionDisks.Click += new System.EventHandler(this.tabPartitionDisks_Click);
        this.tabPartitionDisks.Enter += new System.EventHandler(this.tabPartitionDisks_Enter);
        // 
        // datagridPartitionDisks
        // 
        this.datagridPartitionDisks.AllowDrop = true;
        this.datagridPartitionDisks.AllowUserToOrderColumns = true;
        this.datagridPartitionDisks.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
        this.datagridPartitionDisks.BackgroundColor = System.Drawing.SystemColors.InactiveBorder;
        this.datagridPartitionDisks.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.datagridPartitionDisks.Location = new System.Drawing.Point(0,0);
        this.datagridPartitionDisks.Name = "datagridPartitionDisks";
        this.datagridPartitionDisks.Size = new System.Drawing.Size(688,373);
        this.datagridPartitionDisks.TabIndex = 0;
        // 
        // tabPartitionEventTracking
        // 
        this.tabPartitionEventTracking.Controls.Add(this.datagridEventTracking);
        this.tabPartitionEventTracking.Location = new System.Drawing.Point(4,22);
        this.tabPartitionEventTracking.Name = "tabPartitionEventTracking";
        this.tabPartitionEventTracking.Size = new System.Drawing.Size(694,402);
        this.tabPartitionEventTracking.TabIndex = 3;
        this.tabPartitionEventTracking.Text = "Partition Event Tracking";
        this.tabPartitionEventTracking.UseVisualStyleBackColor = true;
        this.tabPartitionEventTracking.Enter += new System.EventHandler(this.tabPartitionEventTracking_Enter);
        // 
        // datagridEventTracking
        // 
        this.datagridEventTracking.AllowUserToAddRows = false;
        this.datagridEventTracking.AllowUserToDeleteRows = false;
        this.datagridEventTracking.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
        this.datagridEventTracking.BackgroundColor = System.Drawing.SystemColors.Window;
        this.datagridEventTracking.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
        this.datagridEventTracking.Location = new System.Drawing.Point(4,4);
        this.datagridEventTracking.Name = "datagridEventTracking";
        this.datagridEventTracking.ReadOnly = true;
        this.datagridEventTracking.Size = new System.Drawing.Size(689,397);
        this.datagridEventTracking.TabIndex = 0;
        // 
        // tabChangePassword
        // 
        this.tabChangePassword.Controls.Add(this.lblPasswordChange);
        this.tabChangePassword.Controls.Add(this.txtConfirmPassword);
        this.tabChangePassword.Controls.Add(this.lblConfirmPassword);
        this.tabChangePassword.Controls.Add(this.lblNewPassword);
        this.tabChangePassword.Controls.Add(this.lblOldPassword);
        this.tabChangePassword.Controls.Add(this.btnUpdatePassword);
        this.tabChangePassword.Controls.Add(this.txtNewPassword);
        this.tabChangePassword.Controls.Add(this.txtOldPassword);
        this.tabChangePassword.Location = new System.Drawing.Point(4,22);
        this.tabChangePassword.Name = "tabChangePassword";
        this.tabChangePassword.Size = new System.Drawing.Size(694,402);
        this.tabChangePassword.TabIndex = 2;
        this.tabChangePassword.Text = "Change Password";
        this.tabChangePassword.UseVisualStyleBackColor = true;
        this.tabChangePassword.Enter += new System.EventHandler(this.tabChangePassword_Enter);
        // 
        // lblPasswordChange
        // 
        this.lblPasswordChange.AutoSize = true;
        this.lblPasswordChange.Font = new System.Drawing.Font("Arial",15.75F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblPasswordChange.ForeColor = System.Drawing.Color.Red;
        this.lblPasswordChange.Location = new System.Drawing.Point(193,62);
        this.lblPasswordChange.Name = "lblPasswordChange";
        this.lblPasswordChange.Size = new System.Drawing.Size(302,24);
        this.lblPasswordChange.TabIndex = 100;
        this.lblPasswordChange.Text = "Password has been changed";
        this.lblPasswordChange.Visible = false;
        this.lblPasswordChange.Enter += new System.EventHandler(this.lblPasswordChange_Enter);
        // 
        // txtConfirmPassword
        // 
        this.txtConfirmPassword.Location = new System.Drawing.Point(342,194);
        this.txtConfirmPassword.Name = "txtConfirmPassword";
        this.txtConfirmPassword.Size = new System.Drawing.Size(153,20);
        this.txtConfirmPassword.TabIndex = 3;
        this.txtConfirmPassword.UseSystemPasswordChar = true;
        // 
        // lblConfirmPassword
        // 
        this.lblConfirmPassword.AutoSize = true;
        this.lblConfirmPassword.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblConfirmPassword.Location = new System.Drawing.Point(162,193);
        this.lblConfirmPassword.Name = "lblConfirmPassword";
        this.lblConfirmPassword.Size = new System.Drawing.Size(157,19);
        this.lblConfirmPassword.TabIndex = 99;
        this.lblConfirmPassword.Text = "Confirm Password:";
        // 
        // lblNewPassword
        // 
        this.lblNewPassword.AutoSize = true;
        this.lblNewPassword.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblNewPassword.Location = new System.Drawing.Point(189,154);
        this.lblNewPassword.Name = "lblNewPassword";
        this.lblNewPassword.Size = new System.Drawing.Size(130,19);
        this.lblNewPassword.TabIndex = 98;
        this.lblNewPassword.Text = "New Password:";
        // 
        // lblOldPassword
        // 
        this.lblOldPassword.AutoSize = true;
        this.lblOldPassword.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.lblOldPassword.Location = new System.Drawing.Point(197,117);
        this.lblOldPassword.Name = "lblOldPassword";
        this.lblOldPassword.Size = new System.Drawing.Size(122,19);
        this.lblOldPassword.TabIndex = 97;
        this.lblOldPassword.Text = "Old Password:";
        // 
        // btnUpdatePassword
        // 
        this.btnUpdatePassword.Font = new System.Drawing.Font("Arial",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnUpdatePassword.Location = new System.Drawing.Point(258,245);
        this.btnUpdatePassword.Name = "btnUpdatePassword";
        this.btnUpdatePassword.Size = new System.Drawing.Size(155,26);
        this.btnUpdatePassword.TabIndex = 4;
        this.btnUpdatePassword.Text = "Update Password";
        this.btnUpdatePassword.UseVisualStyleBackColor = true;
        this.btnUpdatePassword.Click += new System.EventHandler(this.btnUpdatePassword_Click);
        // 
        // txtNewPassword
        // 
        this.txtNewPassword.Location = new System.Drawing.Point(342,153);
        this.txtNewPassword.Name = "txtNewPassword";
        this.txtNewPassword.Size = new System.Drawing.Size(153,20);
        this.txtNewPassword.TabIndex = 1;
        this.txtNewPassword.UseSystemPasswordChar = true;
        // 
        // txtOldPassword
        // 
        this.txtOldPassword.Location = new System.Drawing.Point(342,116);
        this.txtOldPassword.Name = "txtOldPassword";
        this.txtOldPassword.Size = new System.Drawing.Size(153,20);
        this.txtOldPassword.TabIndex = 0;
        this.txtOldPassword.UseSystemPasswordChar = true;
        // 
        // btnDatabasePartitionOK
        // 
        this.btnDatabasePartitionOK.Font = new System.Drawing.Font("Arial Black",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnDatabasePartitionOK.Location = new System.Drawing.Point(251,435);
        this.btnDatabasePartitionOK.Name = "btnDatabasePartitionOK";
        this.btnDatabasePartitionOK.Size = new System.Drawing.Size(88,37);
        this.btnDatabasePartitionOK.TabIndex = 1;
        this.btnDatabasePartitionOK.Text = "OK";
        this.btnDatabasePartitionOK.UseVisualStyleBackColor = true;
        this.btnDatabasePartitionOK.Click += new System.EventHandler(this.btnDatabasePartitionOK_Click);
        // 
        // btnDatabasePartitionCancel
        // 
        this.btnDatabasePartitionCancel.Font = new System.Drawing.Font("Arial Black",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.btnDatabasePartitionCancel.Location = new System.Drawing.Point(363,435);
        this.btnDatabasePartitionCancel.Name = "btnDatabasePartitionCancel";
        this.btnDatabasePartitionCancel.Size = new System.Drawing.Size(87,37);
        this.btnDatabasePartitionCancel.TabIndex = 2;
        this.btnDatabasePartitionCancel.Text = "Cancel";
        this.btnDatabasePartitionCancel.UseVisualStyleBackColor = true;
        this.btnDatabasePartitionCancel.Click += new System.EventHandler(this.btnDatabasePartitionCancel_Click);
        // 
        // errorDatabasePartitioning
        // 
        this.errorDatabasePartitioning.ContainerControl = this;
        // 
        // label1
        // 
        this.label1.AutoSize = true;
        this.label1.Font = new System.Drawing.Font("Arial Black",12F,System.Drawing.FontStyle.Bold,System.Drawing.GraphicsUnit.Point,((byte)(0)));
        this.label1.Location = new System.Drawing.Point(8,376);
        this.label1.Name = "label1";
        this.label1.Size = new System.Drawing.Size(662,23);
        this.label1.TabIndex = 3;
        this.label1.Text = "Delete, Add or Edit(Only last column) rows in grid then click SAVE to save.";
        // 
        // DatabasePartitioning
        // 
        this.AutoScaleDimensions = new System.Drawing.SizeF(6F,13F);
        this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
        this.ClientSize = new System.Drawing.Size(700,484);
        this.Controls.Add(this.btnDatabasePartitionCancel);
        this.Controls.Add(this.btnDatabasePartitionOK);
        this.Controls.Add(this.tabDatabasePartition);
        this.Name = "DatabasePartitioning";
        this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
        this.Text = "IntegraPAY Online Database Partitioning";
        this.tabDatabasePartition.ResumeLayout(false);
        this.tabPartitionManager.ResumeLayout(false);
        this.tabPartitionManager.PerformLayout();
        this.tabPartitionDisks.ResumeLayout(false);
        this.tabPartitionDisks.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.datagridPartitionDisks)).EndInit();
        this.tabPartitionEventTracking.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)(this.datagridEventTracking)).EndInit();
        this.tabChangePassword.ResumeLayout(false);
        this.tabChangePassword.PerformLayout();
        ((System.ComponentModel.ISupportInitialize)(this.errorDatabasePartitioning)).EndInit();
        this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabDatabasePartition;
        private System.Windows.Forms.TabPage tabPartitionManager;
        private System.Windows.Forms.TabPage tabPartitionDisks;
        private System.Windows.Forms.Button btnDatabasePartitionOK;
        private System.Windows.Forms.Button btnDatabasePartitionCancel;
        private System.Windows.Forms.TabPage tabChangePassword;
        private System.Windows.Forms.TabPage tabPartitionEventTracking;
        private System.Windows.Forms.TextBox txtNewPassword;
        private System.Windows.Forms.TextBox txtOldPassword;
        private System.Windows.Forms.TextBox txtConfirmPassword;
        private System.Windows.Forms.Label lblConfirmPassword;
        private System.Windows.Forms.Label lblNewPassword;
        private System.Windows.Forms.Label lblOldPassword;
        private System.Windows.Forms.Button btnUpdatePassword;
        private System.Windows.Forms.Label lblLastDiskID;
        private System.Windows.Forms.Label lblRangeSetting;
        private System.Windows.Forms.Label lblPartitionSize;
        private System.Windows.Forms.Label lblPartitionSchemaName;
        private System.Windows.Forms.Label lblPartitionFunctionName;
        private System.Windows.Forms.Label lblPartitionIdentifier;
        private System.Windows.Forms.Label lblPartitionManagerID;
        private System.Windows.Forms.TextBox txtLastDiskID;
        private System.Windows.Forms.TextBox txtPartitionSchemaName;
        private System.Windows.Forms.TextBox txtPartitionFunctionName;
        private System.Windows.Forms.TextBox txtPartitionIdentifier;
        private System.Windows.Forms.TextBox txtPartitionManagerID;
        private System.Windows.Forms.DataGridView datagridPartitionDisks;
        private System.Windows.Forms.DataGridView datagridEventTracking;
        private System.Windows.Forms.ErrorProvider errorDatabasePartitioning;
        private System.Windows.Forms.Label lblPasswordChange;
        private System.Windows.Forms.Label lblInMB;
        private System.Windows.Forms.TextBox txtPartitionSize;
        private System.Windows.Forms.Button btnPreviousRecord;
        private System.Windows.Forms.Button btnNextRecord;
        private System.Windows.Forms.ListBox lstRangeSettings;
        private System.Windows.Forms.Button btnDeletePartitionManager;
        private System.Windows.Forms.Button btnAddPartitionManager;
        private System.Windows.Forms.Label label1;
    }
}