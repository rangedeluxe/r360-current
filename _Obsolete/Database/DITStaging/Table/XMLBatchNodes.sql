--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLBatchNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLBatchNodes') IS NOT NULL
       DROP TABLE DITStaging.XMLBatchNodes;

--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/30/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/30/2012 CR 52866 JPB	Created
* 04/05/2013 WI 92151 JBS	Update to 2.0 release. Change schema name to DITStaging
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLBatchNodes
(
	Batch_Id BIGINT NOT NULL,
	xmlString VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLBatchNodes.IDX_XMLBatchNode_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLBatchNodes_Batch_Id ON DITStaging.XMLBatchNodes
(
	[Batch_Id] ASC
);