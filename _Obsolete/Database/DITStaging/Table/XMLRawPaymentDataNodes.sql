--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLRawPaymentDataNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLRawPaymentDataNodes') IS NOT NULL
       DROP TABLE DITStaging.XMLRawPaymentDataNodes;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/06/2014
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/06/2014 WI 140153 JPB	Created
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLRawPaymentDataNodes
(
	Batch_Id BIGINT NOT NULL,
	xmlString VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLRawPaymentDataNodes.IDX_XMLRawPaymentDataNodes_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLRawPaymentDataNodes_Batch_Id ON DITStaging.XMLRawPaymentDataNodes
(
	[Batch_Id] ASC
);