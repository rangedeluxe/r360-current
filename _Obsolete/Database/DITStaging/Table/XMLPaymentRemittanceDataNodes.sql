--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLPaymentRemittanceDataNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLPaymentRemittanceDataNodes') IS NOT NULL
       DROP TABLE DITStaging.XMLPaymentRemittanceDataNodes;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 04/30/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 04/30/2012 CR 52879 JPB	Created
* 04/09/2013 WI 92189 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLPaymentRemittanceDataNodes
(
	Batch_Id BIGINT NOT NULL,
	xmlString VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLPaymentNodes.IDX_XMLPaymentRemittanceDataNodes_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLPaymentRemittanceDataNodes_Batch_Id ON DITStaging.XMLPaymentRemittanceDataNodes
(
	[Batch_Id] ASC
);