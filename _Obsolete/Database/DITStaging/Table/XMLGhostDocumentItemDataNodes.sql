--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorTableName XMLGhostDocumentItemDataNodes
--WFSScriptProcessorTableDrop
IF OBJECT_ID('DITStaging.XMLGhostDocumentItemDataNodes') IS NOT NULL
       DROP TABLE DITStaging.XMLGhostDocumentItemDataNodes;
--WFSScriptProcessorTableHeaderBegin
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/25/2012
*
* Purpose: Workspace for the Data Import Toolkit SSIS package.
*		   
*
* Modification History
* 05/25/2012 CR 53124 JPB	Created
* 04/09/2013 WI 92171 JPB	Updated for 2.0.
*							Moved to DITStaging schema.
******************************************************************************/
--WFSScriptProcessorTableHeaderEnd
--WFSScriptProcessorTableCreate
CREATE TABLE DITStaging.XMLGhostDocumentItemDataNodes
(
	Batch_Id BIGINT NOT NULL,
	xmlString VARCHAR(MAX)
);
--WFSScriptProcessorTableProperties
--WFSScriptProcessorIndex DITStaging.XMLGhostDocumentItemDataNodes.IDX_XMLGhostDocumentItemDataNodes_Batch_Id
CREATE CLUSTERED INDEX IDX_XMLGhostDocumentItemDataNodes_Batch_Id ON DITStaging.XMLGhostDocumentItemDataNodes
(
	[Batch_Id] ASC
);