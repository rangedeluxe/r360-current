﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatchDataNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatchDataNodes_Get') IS NOT NULL
      DROP PROCEDURE DITStaging.usp_XMLBatchDataNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatchDataNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JBS
* Date: 10/03/2016
*
* Purpose: Extract the individual columns (attributes) from the XMLBatchDataNodes Node XML data 
*			to be used by the SSIS package.
*
* Adapted from http://dba.stackexchange.com/questions/42393/get-the-2nd-or-3rd-occurrence-of-a-value-in-a-delimited-string
*
* Modification History
* 10/03/2016 PT #127604205	JBS	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		Batch_Id,
		CAST(SUBSTRING(xmlString,FieldName_Start.Pos,FieldName_End.Pos-FieldName_Start.Pos) AS VARCHAR(32)) AS FieldName,
		CAST(SUBSTRING(xmlString,FieldValue_Start.Pos,FieldValue_End.Pos-FieldValue_Start.Pos) AS VARCHAR(256)) AS FieldValue
	FROM
		DITStaging.XMLBatchDataNodes
		CROSS APPLY(SELECT (PATINDEX('%FieldName="%',xmlString)+LEN('FieldName="'))) AS FieldName_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,FieldName_Start.Pos))) AS FieldName_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%FieldValue="%',xmlString)+LEN('FieldValue="'))) AS FieldValue_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,FieldValue_Start.Pos))) AS FieldValue_End(Pos)
	ORDER BY 
		Batch_id;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH