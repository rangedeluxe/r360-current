--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatch_Parse
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatch_Parse') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBatch_Parse
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatch_Parse
(
	@parmStartTag VARCHAR(30),
	@parmEndTag VARCHAR(30) = '>'
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/20/2016
*
* Purpose: Extract the Document Node from the XML data and insert into the 
*			DITStaging.XMLDocumentNodes table.
*
* Modification History
* 02/19/2016 WI 265013 JPB	Created
* 03/03/2016 WI 265013 JBS	Added '&quot;' replacing statement
* 03/03/2016 WI 265013 MGE  Fixed offset from endtag to resolve issue with contiguous nodes
" 08/14/2016 PT 127614445 JPB	Added '&gt;' and '&lt;', removed '&quot;'
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY

	;WITH Batch_CTE(Batch_Id,xmlString,StartingPosition,EndingPosition,occurence)
	AS
	(		
		SELECT		
			DITStaging.XMLBatches.Batch_Id,
			DITStaging.XMLBatches.xmlString,
			StartTag.Pos AS StartingPosition,
			EndTag.Pos AS EndingPosition,
			1 AS occurence
		FROM
			DITStaging.XMLBatch
			INNER JOIN DITStaging.XMLBatches ON DITStaging.XMLBatches.Batch_Id = DITStaging.XMLBatch.Batch_Id
			CROSS APPLY(SELECT (CHARINDEX(@parmStartTag,xmlString))) AS StartTag(Pos)
			CROSS APPLY(SELECT (CHARINDEX(@parmEndTag,xmlString,StartTag.Pos))+1) AS EndTag(Pos)
		WHERE 
			CHARINDEX(@parmStartTag,xmlString,1) > 0
		UNION ALL
		SELECT		
			Batch_Id,
			xmlString,
			StartTag.Pos AS StartingPosition,
			EndTag.Pos AS EndingPosition,
			occurence + 1
		FROM 
			Batch_CTE
			CROSS APPLY(SELECT (CHARINDEX(@parmStartTag,xmlString,EndingPosition))) AS StartTag(Pos)
			CROSS APPLY(SELECT (CHARINDEX(@parmEndTag,xmlString,StartTag.Pos))) AS EndTag(Pos)
		WHERE CHARINDEX(@parmStartTag, xmlString, EndingPosition + 1) <> 0
	)
	SELECT	
		Batch_Id,
		REPLACE(REPLACE(REPLACE(SUBSTRING(xmlString, StartingPosition, EndingPosition-StartingPosition),'&amp;','&'),'&gt;','>'),'&lt;','<') AS xmlString
	FROM 
		Batch_CTE
	OPTION (MaxRecursion 0);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
