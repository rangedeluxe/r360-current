--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLDocumentNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLDocumentNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLDocumentNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLDocumentNodes_Ins
(
	@parmPendingDocuments BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/01/2012
*
* Purpose: Extract the Document Node from the XML data and insert into the 
*			DITStaging.XMLDocumentNodes table.
*
* Modification History
* 05/01/2012 CR 52929 JPB	Created
* 04/05/2013 WI 92128 JBS	Update to 2.0 release.  change schema name to DITStaging
*							Rename proc from usp_XMLDocumentNodes_Insert
* 02/23/2016 WI 263811 JPB	Use helper SP.
******************************************************************************/
SET NOCOUNT ON;

BEGIN TRY

	INSERT	DITStaging.XMLDocumentNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<Document ';

	IF( @@ROWCOUNT > 0 )
		SET @parmPendingDocuments = 1
	ELSE SET @parmPendingDocuments = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
