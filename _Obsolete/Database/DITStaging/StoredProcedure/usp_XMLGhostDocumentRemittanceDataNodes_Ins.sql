--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLGhostDocumentRemittanceDataNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLGhostDocumentRemittanceDataNodes_Ins
(
	@parmPendingGhostDocumentRemittanceData BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/01/2012
*
* Purpose: Extract the GhostDocumentRemittanceData Node from the XML data 
*			and insert into the DITStaging.XMLGhostDocumentRemittanceDataNodes table.
*
* Modification History
* 05/01/2012 CR 52933 JPB	Created
* 04/19/2013 WI 92134 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_XMLGhostDocumentRemittanceDataNodes_Insert
*
* 10/03/2016 PT	#127604205 JBS	Updated to use helper SP and return pending status
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY

	INSERT DITStaging.XMLGhostDocumentRemittanceDataNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<GhostDocumentRemittanceData ';

	IF( @@ROWCOUNT > 0 )
		SET @parmPendingGhostDocumentRemittanceData = 1
	ELSE SET @parmPendingGhostDocumentRemittanceData = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
