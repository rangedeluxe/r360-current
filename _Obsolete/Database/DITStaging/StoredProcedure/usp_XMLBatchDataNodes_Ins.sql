--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatchDataNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatchDataNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBatchDataNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatchDataNodes_Ins
(
	@parmPendingBatchData BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/27/2012
*
* Purpose: Extract the Batch Data Node from the XML data and insert into the 
*			DITStaging.XMLBatchDataNodes table.
*
* Modification History
* 05/27/2012 CR 53154 JPB	Created
* 04/18/2013 WI 92119 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_BatchDataNodes_Insert
* 10/03/2016 PT 127604205 JBS	Updated to use helper SP 
* 11/03/2016 PT 127604197 JPB	Added @parmPendingBatchData
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY

	INSERT DITStaging.XMLBatchDataNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<BatchData ';

	IF( @@ROWCOUNT > 0 )
		SET @parmPendingBatchData = 1
	ELSE SET @parmPendingBatchData = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
