﻿--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLPaymentNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLPaymentNodes_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLPaymentNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLPaymentNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2016
*
* Purpose: Extract the individual columns (attributes) from the Payment Node XML data 
*			to be used by the SSIS package.
*
* Adapted from http://dba.stackexchange.com/questions/42393/get-the-2nd-or-3rd-occurrence-of-a-value-in-a-delimited-string
*
* Modification History
* 02/24/2016 WI 262055	JPB/MGE	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		Batch_Id,
		CAST(SUBSTRING(xmlString,Transaction_Id_Start.Pos,Transaction_Id_End.Pos-Transaction_Id_Start.Pos) AS BIGINT) AS Transaction_Id,
		CAST(SUBSTRING(xmlString,Payment_Id_Start.Pos,Payment_Id_End.Pos-Payment_Id_Start.Pos) AS BIGINT) AS Payment_Id,
		CAST(SUBSTRING(xmlString,BatchSequence_Start.Pos,BatchSequence_End.Pos-BatchSequence_Start.Pos) AS INT) AS BatchSequence,
		CAST(SUBSTRING(xmlString,Amount_Start.Pos,Amount_End.Pos-Amount_Start.Pos) AS MONEY) AS Amount,
		CAST(SUBSTRING(xmlString,RT_Start.Pos,RT_End.Pos-RT_Start.Pos) AS VARCHAR(30)) AS RT,
		CAST(SUBSTRING(xmlString,Account_Start.Pos,Account_End.Pos-Account_Start.Pos) AS VARCHAR(30)) AS Account,
		CAST(SUBSTRING(xmlString,Serial_Start.Pos,Serial_End.Pos-Serial_Start.Pos) AS VARCHAR(30)) AS Serial,
		CAST(SUBSTRING(xmlString,TransactionCode_Start.Pos,TransactionCode_End.Pos-TransactionCode_Start.Pos) AS VARCHAR(30)) AS TransactionCode,
		CAST(SUBSTRING(xmlString,RemitterName_Start.Pos,RemitterName_End.Pos-RemitterName_Start.Pos) AS VARCHAR(60)) AS RemitterName,
		CAST(SUBSTRING(xmlString,ABA_Start.Pos,ABA_End.Pos-ABA_Start.Pos) AS VARCHAR(10)) AS ABA,
		CAST(SUBSTRING(xmlString,DDA_Start.Pos,DDA_End.Pos-DDA_Start.Pos) AS VARCHAR(35)) AS DDA,
		CASE 
			WHEN CheckSequence_End.Pos > CheckSequence_Start.Pos THEN CAST(SUBSTRING(xmlString,CheckSequence_Start.Pos,CheckSequence_End.Pos-CheckSequence_Start.Pos) AS INT) 
			ELSE NULL 
		END AS CheckSequence
	FROM
		DITStaging.XMLPaymentNodes
		CROSS APPLY(SELECT (PATINDEX('%Transaction_Id="%',xmlString)+LEN('Transaction_Id="'))) AS Transaction_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Transaction_Id_Start.Pos))) AS Transaction_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%Payment_Id="%',xmlString)+LEN('Payment_Id="'))) AS Payment_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Payment_Id_Start.Pos))) AS Payment_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchSequence="%',xmlString)+LEN('BatchSequence="'))) AS BatchSequence_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchSequence_Start.Pos))) AS BatchSequence_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%Amount="%',xmlString)+LEN('Amount="'))) AS Amount_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Amount_Start.Pos))) AS Amount_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%RT="%',xmlString)+LEN('RT="'))) AS RT_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,RT_Start.Pos))) AS RT_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%Account="%',xmlString)+LEN('Account="'))) AS Account_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Account_Start.Pos))) AS Account_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%Serial="%',xmlString)+LEN('Serial="'))) AS Serial_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Serial_Start.Pos))) AS Serial_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%TransactionCode="%',xmlString)+LEN('TransactionCode="'))) AS TransactionCode_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,TransactionCode_Start.Pos))) AS TransactionCode_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%RemitterName="%',xmlString)+LEN('RemitterName="'))) AS RemitterName_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,RemitterName_Start.Pos))) AS RemitterName_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%ABA="%',xmlString)+LEN('ABA="'))) AS ABA_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,ABA_Start.Pos))) AS ABA_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%DDA="%',xmlString)+LEN('DDA="'))) AS DDA_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,DDA_Start.Pos))) AS DDA_End(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%CheckSequence="%',xmlString) > 0 THEN PATINDEX('%CheckSequence="%',xmlString)+LEN('CheckSequence="') ELSE 0 END)
				) AS CheckSequence_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%CheckSequence="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,CheckSequence_Start.Pos) ELSE 0 END)
				) AS CheckSequence_End(Pos)
		ORDER BY Batch_id, Transaction_Id;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH