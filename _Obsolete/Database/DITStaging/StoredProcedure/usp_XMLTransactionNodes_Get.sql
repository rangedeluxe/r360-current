﻿--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLTransactionNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLTransactionNodes_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLTransactionNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLTransactionNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright © 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/02/2016
*
* Purpose: Extract the individual columns (attributes) from the Transaction Node XML data 
*			to be used by the SSIS package.
*
* Modification History
* 02/02/2016 WI 261875	JPB	Created
* 02/11/2016 WI 261875	MGE	Added to source
* 06/02/2016 WI 284776	Performance improvements
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		Batch_Id,
		CAST(SUBSTRING(xmlString,Transaction_Id_Start.Pos,Transaction_Id_End.Pos-Transaction_Id_Start.Pos) AS BIGINT) AS Transaction_Id,
		CAST(SUBSTRING(xmlString,TransactionID_Start.Pos,TransactionID_End.Pos-TransactionID_Start.Pos) AS INT) AS TransactionID,
		CAST(SUBSTRING(xmlString,TxnSequence_Start.Pos,TxnSequence_End.Pos-TxnSequence_Start.Pos) AS INT) AS TxnSequence,
		CASE
			WHEN TransactionHash_End.Pos > TransactionHash_Start.Pos THEN CAST(SUBSTRING(xmlString,TransactionHash_Start.Pos,TransactionHash_End.Pos-TransactionHash_Start.Pos) AS VARCHAR(40))
			ELSE NULL
		END AS TransactionHash,
		CASE
			WHEN TransactionSignature_End.Pos > TransactionSignature_Start.Pos THEN CAST(SUBSTRING(xmlString,TransactionSignature_Start.Pos,TransactionSignature_End.Pos-TransactionSignature_Start.Pos) AS VARCHAR(65))
			ELSE NULL
		END AS TransactionSignature
	FROM 
		DITStaging.XMLTransactionNodes
		CROSS APPLY(SELECT (PATINDEX('%Transaction_Id="%',xmlString)+LEN('Transaction_Id="'))) AS Transaction_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Transaction_Id_Start.Pos))) AS Transaction_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%TransactionID="%',xmlString)+LEN('TransactionID="'))) AS TransactionID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,TransactionID_Start.Pos))) AS TransactionID_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%TransactionSequence="%',xmlString)+LEN('TransactionSequence="'))) AS TxnSequence_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,TxnSequence_Start.Pos))) AS TxnSequence_End(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%TransactionHash="%',xmlString) > 0 THEN PATINDEX('%TransactionHash="%',xmlString)+LEN('TransactionHash="') ELSE 0 END)
				) AS TransactionHash_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%TransactionHash="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,TransactionHash_Start.Pos) ELSE 0 END)
				) AS TransactionHash_End(Pos)

		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%TransactionSignature="%',xmlString) > 0 THEN PATINDEX('%TransactionSignature="%',xmlString)+LEN('TransactionSignature="') ELSE 0 END)
				) AS TransactionSignature_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%TransactionSignature="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,TransactionSignature_Start.Pos) ELSE 0 END)
				) AS TransactionSignature_End(Pos);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
GO