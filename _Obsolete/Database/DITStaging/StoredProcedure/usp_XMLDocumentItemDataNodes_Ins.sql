--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLDocumentItemDataNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLDocumentItemDataNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLDocumentItemDataNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLDocumentItemDataNodes_Ins
(
	@parmPendingDocumentItemData BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/27/2012
*
* Purpose: Extract the Document Item Node from the XML data and insert into the 
*			DITStaging.XMLDocumentItemDataNodes table.
*
* Modification History
* 05/27/2012 CR 53155 JPB	Created
* 04/19/2013 WI 92127 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_XMLDocumentItemDataNodes_Insert
* 09/19/2016 PT 127604195	MGE		Use helper SP; Add parameter to tell pkg if Doc Item Data exists
******************************************************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	
	INSERT DITStaging.XMLDocumentItemDataNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<DocumentItemData ';

	IF( @@ROWCOUNT > 0 )
		SET @parmPendingDocumentItemData = 1
	ELSE SET @parmPendingDocumentItemData = 0;

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
