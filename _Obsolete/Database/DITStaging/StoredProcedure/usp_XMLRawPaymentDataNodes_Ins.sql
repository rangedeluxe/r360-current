--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLRawPaymentDataNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLRawPaymentDataNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLRawPaymentDataNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLRawPaymentDataNodes_Ins
(
	@parmPendingRawPayments BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2014-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/06/2014
*
* Purpose: Extract the PaymentRawData Node from the XML data and 
*			insert into the DITStaging.XMLRawPaymentDataNodes table.
*
* Modification History
* 05/06/2014 WI 140156 JPB	Created
* 03/03/2016 WI 267815 JBS  Update to use Helper Proc
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

BEGIN TRY
		
	INSERT DITStaging.XMLRawPaymentDataNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<PaymentRawData ',@parmEndTag = '/PaymentRawData>';

	IF( @@ROWCOUNT > 0 )
		SET @parmPendingRawPayments = 1
	ELSE SET @parmPendingRawPayments = 0;
END TRY

BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
