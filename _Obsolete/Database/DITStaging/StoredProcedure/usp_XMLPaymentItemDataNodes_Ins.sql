--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLPaymentItemDataNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLPaymentItemDataNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLPaymentItemDataNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLPaymentItemDataNodes_Ins
(
	@parmPendingPaymentItemData BIT OUTPUT
)
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/27/2012
*
* Purpose: Extract the Payment Item Node from the XML data and insert into the 
*			DITStaging.XMLPaymentItemData table.
*
* Modification History
* 05/27/2012 CR 53157	JPB		Created
* 04/18/2013 WI 92139	JPB		Update to 2.0 release.  Change schema to DITStaging
*								Rename proc from usp_XMLPaymentItemDataNodes_Insert
* 03/07/2016 WI	267816	MGE		Change to use helper stored procedure (eliminates duplicate batches)
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	INSERT DITStaging.XMLPaymentItemDataNodes(Batch_Id,xmlString)
	EXEC DITStaging.usp_XMLBatch_Parse 
		@parmStartTag='<PaymentItemData ';
	
	IF( @@ROWCOUNT > 0 )
		SET @parmPendingPaymentItemData = 1
	ELSE SET @parmPendingPaymentItemData = 0;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
