--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLDocumentRemittanceDataRecordNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLDocumentRemittanceDataRecordNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLDocumentRemittanceDataRecordNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLDocumentRemittanceDataRecordNodes_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2013 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/01/2012
*
* Purpose: Extract the DocumentRemittanceDataRecord Node from the XML data and 
*			insert into the DITStaging.XMLDocumentRemittanceDataRecordNodes table.
*
* Modification History
* 05/01/2012 CR 52931 JPB	Created
* 04/19/2013 WI 92130 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_XMLDocumentRemittanceDataRecordNodes_Insert
******************************************************************************/
SET ARITHABORT ON 
SET NOCOUNT ON 

DECLARE @xmlString VARCHAR(MAX),
		@StartTag VARCHAR(30),
		@EndTag CHAR(1),
		@Batch_Id INT,
		@Loop INT,
		@RecordCount INT;

BEGIN TRY
	SELECT @RecordCount = COUNT(*) FROM DITStaging.XMLBatches;
	SELECT @StartTag = '<DocumentRemittanceDataRecord ',@EndTag = '>';
	SET @Loop = 1;
	
	WHILE( @Loop <= @RecordCount )
	BEGIN
		SELECT	@Batch_Id = Batch_Id,
				@xmlString = xmlString
		FROM	DITStaging.XMLBatches
		WHERE	RowID = @Loop;
		
		IF( CHARINDEX(@StartTag,@xmlString,0) > 0 )
		BEGIN
			;WITH Batch_CTE(Batch_Id,StartingPosition, EndingPosition, occurence)
			AS
			(		
				SELECT		@Batch_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,0) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag,@xmlString,CHARINDEX(@StartTag,@xmlString,0)+1) AS INT),
							1 AS occurence
				UNION ALL
				SELECT 		@Batch_Id,
							StartingPosition = CAST(CHARINDEX(@StartTag,@xmlString,EndingPosition + 1) AS INT),
							EndingPosition = CAST(CHARINDEX(@EndTag, @xmlString, CHARINDEX(@StartTag,@xmlString,EndingPosition + 1))AS INT),
							occurence + 1
				FROM Batch_CTE
				WHERE CHARINDEX(@StartTag, @xmlString, EndingPosition + 1) <> 0
			)
			INSERT DITStaging.XMLDocumentRemittanceDataRecordNodes(Batch_Id,xmlString)
			SELECT	@Batch_Id,SUBSTRING(@xmlString, StartingPosition, EndingPosition-StartingPosition+1) AS xmlString 
			FROM	Batch_CTE
			OPTION (MaxRecursion 0);
		END

		SET @Loop = @Loop + 1;
	END
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
