--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLPaymentRawPaymentDataNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLPaymentRawPaymentDataNodes_Get') IS NOT NULL
      DROP PROCEDURE DITStaging.usp_XMLPaymentRawPaymentDataNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLPaymentRawPaymentDataNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/16/2016
*
* Purpose: Extract the individual columns (attributes) from the RawPaymentData Node XML data 
*			to be used by the SSIS package.
*
* Adapted from http://dba.stackexchange.com/questions/42393/get-the-2nd-or-3rd-occurrence-of-a-value-in-a-delimited-string
*
* Modification History
* 03/07/2016 WI 267817	JBS	Created
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		Batch_Id,
		CAST(SUBSTRING(xmlString,Payment_Id_Start.Pos,Payment_Id_End.Pos-Payment_Id_Start.Pos) AS BIGINT) AS Payment_Id,
		CAST(SUBSTRING(xmlString,RawSequence_Start.Pos,RawSequence_End.Pos-RawSequence_Start.Pos) AS INT) AS RawSequence,
		CAST(SUBSTRING(xmlString,RawDataPart_Start.Pos,RawDataPart_End.Pos-RawDataPart_Start.Pos) AS SMALLINT) AS RawDataPart,
		CAST(SUBSTRING(xmlString,FieldValue_Start.Pos,FieldValue_End.Pos-FieldValue_Start.Pos) AS VARCHAR(128)) AS RawDataText,
		FieldValue_End.Pos-FieldValue_Start.Pos AS FieldLength
	FROM
		DITStaging.XMLRawPaymentDataNodes
		CROSS APPLY(SELECT (PATINDEX('%Payment_Id="%',xmlString)+LEN('Payment_Id="'))) AS Payment_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Payment_Id_Start.Pos))) AS Payment_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%RawSequence="%',xmlString)+LEN('RawSequence="'))) AS RawSequence_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,RawSequence_Start.Pos))) AS RawSequence_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%RawDataPart="%',xmlString)+LEN('RawDataPart="'))) AS RawDataPart_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,RawDataPart_Start.Pos))) AS RawDataPart_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%>%',xmlString)+LEN('>'))) AS FieldValue_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('<',xmlString,FieldValue_Start.Pos))) AS FieldValue_End(Pos)
	ORDER BY Batch_Id, Payment_Id;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH