--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLGhostDocumentNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLGhostDocumentNodes_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLGhostDocumentNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLGhostDocumentNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 07/08/2016
*
* Purpose: Extract the individual columns (attributes) from the Document Node XML data 
*			to be used by the SSIS package.
*
* Adapted from http://dba.stackexchange.com/questions/42393/get-the-2nd-or-3rd-occurrence-of-a-value-in-a-delimited-string
*
* Modification History
* 07/08/2016 WI 290803 JPB	Created
* 10/04/2016 PT 127613917 JPB	Added IsCorrespondence.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	SELECT
		Batch_Id,
		CAST(SUBSTRING(xmlString,Transaction_Id_Start.Pos,Transaction_Id_End.Pos-Transaction_Id_Start.Pos) AS BIGINT) AS Transaction_Id,
		CAST(SUBSTRING(xmlString,GhostDocument_Id_Start.Pos,GhostDocument_Id_End.Pos-GhostDocument_Id_Start.Pos) AS BIGINT) AS GhostDocument_Id,
		CAST(SUBSTRING(xmlString,BatchSequence_Start.Pos,BatchSequence_End.Pos-BatchSequence_Start.Pos) AS INT) AS BatchSequence,
		CAST(SUBSTRING(xmlString,IsCorrespondence_Start.Pos,IsCorrespondence_End.Pos-IsCorrespondence_Start.Pos) AS BIT) AS IsCorrespondence
	FROM
		DITStaging.XMLGhostDocumentNodes
		CROSS APPLY(SELECT (PATINDEX('%Transaction_Id="%',xmlString)+LEN('Transaction_Id="'))) AS Transaction_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,Transaction_Id_Start.Pos))) AS Transaction_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%GhostDocument_Id="%',xmlString)+LEN('GhostDocument_Id="'))) AS GhostDocument_Id_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,GhostDocument_Id_Start.Pos))) AS GhostDocument_Id_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchSequence="%',xmlString)+LEN('BatchSequence="'))) AS BatchSequence_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchSequence_Start.Pos))) AS BatchSequence_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%IsCorrespondence="%',xmlString)+LEN('IsCorrespondence="'))) AS IsCorrespondence_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,IsCorrespondence_Start.Pos))) AS IsCorrespondence_End(Pos)
	ORDER BY
		Batch_Id,
		Transaction_Id;
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH