--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatchNodes_Ins
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatchNodes_Ins') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBatchNodes_Ins
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatchNodes_Ins
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2012-2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 05/01/2012
*
* Purpose: Extract the Batch Node from the XML data and insert into the 
*			DITStaging.XMLBatchNodes table.
*
* Modification History
* 05/01/2012 CR 52928 JPB	Created
* 04/18/2013 WI 92120 JPB	Update to 2.0 release.  Change schema to DITStaging
*							Rename proc from usp_XMLBatchNodes_Insert
* 06/02/2016 WI 265601 JPB	Performance Improvements.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

DECLARE @StartTag VARCHAR(30) = '<Batch ',
		@EndTag CHAR(1) = '>';

BEGIN TRY

	;WITH Batch_CTE(Batch_Id,xmlString,StartingPosition,EndingPosition,occurence)
	AS
	(		
		SELECT		
			Batch_Id,
			xmlString,
			Document_Start.Pos AS StartingPosition,
			Document_End.Pos AS EndingPosition,
			1 AS occurence
		FROM
			DITStaging.XMLBatches
			CROSS APPLY(SELECT (CHARINDEX(@StartTag,xmlString))) AS Document_Start(Pos)
			CROSS APPLY(SELECT (CHARINDEX(@EndTag,xmlString,Document_Start.Pos))+1) AS Document_End(Pos)
		UNION ALL
		SELECT		
			Batch_Id,
			xmlString,
			Document_Start.Pos AS StartingPosition,
			Document_End.Pos AS EndingPosition,
			occurence + 1
		FROM Batch_CTE
			CROSS APPLY(SELECT (CHARINDEX(@StartTag,xmlString,EndingPosition+1))) AS Document_Start(Pos)
			CROSS APPLY(SELECT (CHARINDEX(@EndTag,xmlString,Document_Start.Pos))+1) AS Document_End(Pos)
		WHERE CHARINDEX(@StartTag, xmlString, EndingPosition + 1) <> 0
	)
	INSERT	DITStaging.XMLBatchNodes(Batch_Id,xmlString)
	SELECT 
		Batch_Id,
		SUBSTRING(xmlString, StartingPosition, EndingPosition-StartingPosition) AS xmlString 
	FROM 
		Batch_CTE
	OPTION (MaxRecursion 0);

END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH
