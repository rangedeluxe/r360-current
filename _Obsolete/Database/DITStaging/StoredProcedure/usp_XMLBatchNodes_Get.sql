--WFSScriptProcessorSystemName RecHub
--WFSScriptProcessorSchema DITStaging
--WFSScriptProcessorQuotedIdentifierOn
--WFSScriptProcessorStoredProcedureName usp_XMLBatchNodes_Get
--WFSScriptProcessorStoredProcedureDrop
IF OBJECT_ID('DITStaging.usp_XMLBatchNodes_Get') IS NOT NULL
       DROP PROCEDURE DITStaging.usp_XMLBatchNodes_Get
GO

--WFSScriptProcessorStoredProcedureCreate
CREATE PROCEDURE DITStaging.usp_XMLBatchNodes_Get
AS
/******************************************************************************
** WAUSAU Financial Systems (WFS)
** Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved
*******************************************************************************
*******************************************************************************
** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
*******************************************************************************
* Copyright � 2016 WAUSAU Financial Systems, Inc. All rights reserved.  All
* other trademarks cited herein are property of their respective owners.
* These materials are unpublished confidential and proprietary information 
* of WFS and contain WFS trade secrets.  These materials may not be used, 
* copied, modified or disclosed except as expressly permitted in writing by 
* WFS (see the WFS license agreement for details).  All copies, modifications 
* and derivative works of these materials are property of WFS.
*
* Author: JPB
* Date: 02/02/2016
*
* Purpose: Extract the individual columns (attributes) from the Batch Node XML data 
*			to be used by the SSIS package.
*
* Modification History
* 02/02/2016 WI 261869 JPB	Created
* 03/03/2016 WI 267720 JPB	Add FileHashChecker.
******************************************************************************/
SET ARITHABORT ON;
SET NOCOUNT ON;

BEGIN TRY
	;WITH FileHash_CTE AS
	(
		SELECT
			Batch_Id,
			CASE 
				WHEN FileHash_End.Pos > FileHash_Start.Pos THEN CAST(SUBSTRING(xmlString,FileHash_Start.Pos,FileHash_End.Pos-FileHash_Start.Pos) AS VARCHAR(40)) 
				ELSE NULL 
			END AS FileHash
		FROM 
			DITStaging.XMLBatchNodes
			CROSS APPLY(
						SELECT (CASE WHEN PATINDEX('%FileHash="%',xmlString) > 0 THEN PATINDEX('%FileHash="%',xmlString)+LEN('FileHash="') ELSE 0 END)
					) AS FileHash_Start(Pos)
			CROSS APPLY(
						SELECT (CASE WHEN PATINDEX('%FileHash="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,FileHash_Start.Pos) ELSE 0 END)
					) AS FileHash_End(Pos)
	)
	SELECT
		DITStaging.XMLBatchNodes.Batch_Id,
		CAST(SUBSTRING(xmlString,DepositDate_Start.Pos,DepositDate_End.Pos-DepositDate_Start.Pos) AS DATE) AS DepositDate,
		CAST(SUBSTRING(xmlString,BatchDate_Start.Pos,BatchDate_End.Pos-BatchDate_Start.Pos) AS DATE) AS BatchDate,
		CAST(SUBSTRING(xmlString,ProcessingDate_Start.Pos,ProcessingDate_End.Pos-ProcessingDate_Start.Pos) AS DATE) AS ImmutableDate,
		CAST(SUBSTRING(xmlString,BankID_Start.Pos,BankID_End.Pos-BankID_Start.Pos) AS INT) AS SiteBankID,
		CAST(SUBSTRING(xmlString,ClientID_Start.Pos,ClientID_End.Pos-ClientID_Start.Pos) AS INT) AS SiteClientAccountID,
		CAST(SUBSTRING(xmlString,BatchID_Start.Pos,BatchID_End.Pos-BatchID_Start.Pos) AS INT) AS SourceBatchID,
		CAST(SUBSTRING(xmlString,BatchNumber_Start.Pos,BatchNumber_End.Pos-BatchNumber_Start.Pos) AS INT) AS BatchNumber,
		CAST(SUBSTRING(xmlString,BatchSiteCode_Start.Pos,BatchSiteCode_End.Pos-BatchSiteCode_Start.Pos) AS INT) AS BatchSiteCode,
		CAST(SUBSTRING(xmlString,BatchSource_Start.Pos,BatchSource_End.Pos-BatchSource_Start.Pos) AS VARCHAR(30)) AS BatchSource,
		CAST(SUBSTRING(xmlString,PaymentType_Start.Pos,PaymentType_End.Pos-PaymentType_Start.Pos) AS VARCHAR(30)) AS BatchPaymentType,
		CAST(SUBSTRING(xmlString,BatchCueID_Start.Pos,BatchCueID_End.Pos-BatchCueID_Start.Pos) AS INT) AS BatchCueID,
		CAST(SUBSTRING(xmlString,BatchTrackingID_Start.Pos,BatchTrackingID_End.Pos-BatchTrackingID_Start.Pos) AS UNIQUEIDENTIFIER) AS BatchTrackingID,
		CASE 
			WHEN ABA_End.Pos > ABA_Start.Pos THEN CAST(SUBSTRING(xmlString,ABA_Start.Pos,ABA_End.Pos-ABA_Start.Pos) AS VARCHAR(10)) 
			ELSE NULL 
		END AS ABA,
		CASE 
			WHEN DDA_End.Pos > DDA_Start.Pos THEN CAST(SUBSTRING(xmlString,DDA_Start.Pos,DDA_End.Pos-DDA_Start.Pos) AS VARCHAR(35)) 
			ELSE NULL 
		END AS DDA,
		FileHash,
		CASE 
			WHEN FileSignature_End.Pos > FileSignature_Start.Pos THEN CAST(SUBSTRING(xmlString,FileSignature_Start.Pos,FileSignature_End.Pos-FileSignature_Start.Pos) AS VARCHAR(55)) 
			ELSE NULL 
		END AS FileSignature,
		CASE 
			WHEN LEN(FileHash) > 10 THEN CONVERT(BIGINT,CONVERT(VARBINARY(8),'0x' + SUBSTRING(FileHash,1,10),1))
			ELSE CAST(FileHash AS BIGINT)
		END AS FileHashChecker
	FROM 
		DITStaging.XMLBatchNodes
		INNER JOIN FileHash_CTE ON FileHash_CTE.Batch_Id = DITStaging.XMLBatchNodes.Batch_Id
		CROSS APPLY(SELECT (PATINDEX('%DepositDate="%',xmlString)+LEN('DepositDate="'))) AS DepositDate_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,DepositDate_Start.Pos))) AS DepositDate_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchDate="%',xmlString)+LEN('BatchDate="'))) AS BatchDate_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchDate_Start.Pos))) AS BatchDate_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%ProcessingDate="%',xmlString)+LEN('ProcessingDate="'))) AS ProcessingDate_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,ProcessingDate_Start.Pos))) AS ProcessingDate_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BankID="%',xmlString)+LEN('BankID="'))) AS BankID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BankID_Start.Pos))) AS BankID_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%ClientID="%',xmlString)+LEN('ClientID="'))) AS ClientID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,ClientID_Start.Pos))) AS ClientID_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchID="%',xmlString)+LEN('BatchID="'))) AS BatchID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchID_Start.Pos))) AS BatchID_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchNumber="%',xmlString)+LEN('BatchNumber="'))) AS BatchNumber_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchNumber_Start.Pos))) AS BatchNumber_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchSiteCode="%',xmlString)+LEN('BatchSiteCode="'))) AS BatchSiteCode_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchSiteCode_Start.Pos))) AS BatchSiteCode_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchSource="%',xmlString)+LEN('BatchSource="'))) AS BatchSource_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchSource_Start.Pos))) AS BatchSource_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%PaymentType="%',xmlString)+LEN('PaymentType="'))) AS PaymentType_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,PaymentType_Start.Pos))) AS PaymentType_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchCueID="%',xmlString)+LEN('BatchCueID="'))) AS BatchCueID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchCueID_Start.Pos))) AS BatchCueID_End(Pos)
		CROSS APPLY(SELECT (PATINDEX('%BatchTrackingID="%',xmlString)+LEN('BatchTrackingID="'))) AS BatchTrackingID_Start(Pos)
		CROSS APPLY(SELECT (CHARINDEX('"',xmlString,BatchTrackingID_Start.Pos))) AS BatchTrackingID_End(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%ABA="%',xmlString) > 0 THEN PATINDEX('%ABA="%',xmlString)+LEN('ABA="') ELSE 0 END)
				) AS ABA_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%ABA="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,ABA_Start.Pos) ELSE 0 END)
				) AS ABA_End(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%DDA="%',xmlString) > 0 THEN PATINDEX('%DDA="%',xmlString)+LEN('DDA="') ELSE 0 END)
				) AS DDA_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%DDA="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,DDA_Start.Pos) ELSE 0 END)
				) AS DDA_End(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%FileSignature="%',xmlString) > 0 THEN PATINDEX('%FileSignature="%',xmlString)+LEN('FileSignature="') ELSE 0 END)
				) AS FileSignature_Start(Pos)
		CROSS APPLY(
					SELECT (CASE WHEN PATINDEX('%FileSignature="%',xmlString) > 0 THEN CHARINDEX('"',xmlString,FileSignature_Start.Pos) ELSE 0 END)
				) AS FileSignature_End(Pos);
END TRY
BEGIN CATCH
	EXEC RecHubCommon.usp_WfsRethrowException;
END CATCH