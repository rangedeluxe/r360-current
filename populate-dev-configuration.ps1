﻿<#
.SYNOPSIS
    We don't generally check Configuration\Debug files into Git, because they typically contain
    per-machine edits that don't make sense to share. This script will create and fill your
    Configuration\Debug directories after a fresh checkout (or after new template files have
    been added to a Configuration\Templates directory).
#>

#Requires -Version 3
[CmdletBinding(SupportsShouldProcess = $True)]
param (
    # Point config files to RAAM on RecHubDevApp02
    [switch] $RaamOnDev02,
    # Point config files to RAAM on the local computer
    [switch] $RaamLocal,
    # Replace config files that contain @@ placeholders, even if those files already exist in Configuration\Debug
    [switch] $Replace,
    # Copy config files even if they contain @@ unrecognized placeholders
    [switch] $Force,
    # Remove any Configuration\Debug directories that have corresponding Configuration\Templates directories
    [switch] $Clean
)

invoke-expression -Command .\CreateWfsApps.ps1
invoke-expression -Command .\populate-local-computer-config-files.ps1

$ErrorActionPreference = "Stop"
Set-Location $PSScriptRoot

Add-Type -Path (Join-Path $PSScriptRoot "SharedDependencies\RecHub\Current\ipoCrypto.dll")

function Encrypt($Value) {
    $OutValue = ""
    $Crypto3Des = New-Object WFS.RecHub.Common.Crypto.cCrypto3DES
    $Crypto3Des.Encrypt($Value, [ref] $OutValue) | Out-Null
    $OutValue
}

function WriteStep {
    [CmdletBinding(SupportsShouldProcess = $True)]
    param (
        $Message,
        $ForegroundColor
    )

    if ($WhatIfPreference) {
        # Don't output anything - the WhatIf calls will do that for us
    } elseif ($ConfirmPreference -ne "High") {
        # Don't output anything - the confirmation prompt will take care of that
    } else {
        $Params = @{}
        if ($ForegroundColor) {
            $Params.ForegroundColor = $ForegroundColor
        }
        Write-Host $Message @Params
    }
}

function RunClean {
    [CmdletBinding(SupportsShouldProcess = $True)]
    param()
    $ConfigurationDirectories | Where-Object { Test-Path (Join-Path $_.FullName "Templates") } | ForEach-Object {
        $DebugDirectory = Join-Path $_.FullName "Debug"
        if (Test-Path $DebugDirectory) {
            WriteStep "Removing $DebugDirectory"
            # If the user specified -WhatIf or -Confirm, the Remove-Item call will react to those options
            Remove-Item $DebugDirectory -Recurse
        }
    }
}

function RunCopy {
    $TemplateFiles = $ConfigurationDirectories | ForEach-Object {
        Get-ChildItem (Join-Path $_ "Templates") -ErrorAction SilentlyContinue
    }

    $CountCopied = 0
    $CountNotCopiedBecauseOfPlaceholders = 0
    $TemplateFiles | ForEach-Object {
        $SourcePath = $_.FullName
        Write-Verbose "SourcePath = $SourcePath"
        $TemplateDirectory = Split-Path $SourcePath -Parent
        $ConfigurationDirectory = Split-Path $TemplateDirectory -Parent
        $ConfigurationDebugDirectory = Join-Path $ConfigurationDirectory "Debug"

        $FileName = Split-Path $SourcePath -Leaf
        $TargetPath = Join-Path $ConfigurationDebugDirectory $FileName

        if (!(Test-Path $ConfigurationDebugDirectory -PathType Container)) {
            Write-Verbose "Creating directory: $ConfigurationDebugDirectory"
            New-Item $ConfigurationDebugDirectory -ItemType Directory | Out-Null
        }

        if (($ConfirmPreference -ne "High") -and (!(Test-Path $ConfigurationDebugDirectory))) {
            # The user enabled -Confirm and said No to creating the directory.
            # So don't try to copy any files.
        } else {
            # Either the directory was created, or we're in -WhatIf mode.
            # Proceed with copying files.
            $Content = [System.IO.File]::ReadAllText($SourcePath)
            $FileExists = Test-Path $TargetPath -PathType Leaf
            $ShouldReplaceExistingFile = ($Content -match "@@") -and $Replace
            if ($FileExists -and !$ShouldReplaceExistingFile) {
                Write-Host "Exists: $TargetPath" -ForegroundColor DarkGray
            } else {
                if ($RaamLocal) {
                    $Content = $Content -replace "@@RAAM_SOURCE@@", "localhost:44302"
                } elseif ($RaamOnDev02) {
                    $Content = $Content -replace "@@RAAM_SOURCE@@", "rechubdevapp02.qalabs.nwk"
                }
                $Content = ([Regex] "@@encrypt\(([^@]*)\)@@").Replace($Content, {
                    param ($Match)
                    Encrypt $Match.Groups[1].Value
                })

                if (($Content -match "@@") -and !$Force) {
                    Write-Warning "Unresolved @@ placeholders found in $SourcePath"
                    ([Regex] "@@\w+@@").Matches($Content) | ForEach-Object {
                        Write-Host "  $_" -ForegroundColor Yellow
                    }
                    $CountNotCopiedBecauseOfPlaceholders++
                } else {
                    if ($PSCmdlet.ShouldProcess($TargetPath, "Write file")) {
                        [System.IO.File]::WriteAllText($TargetPath, $Content)
                        Write-Host "Copied: " -ForegroundColor Green -NoNewline
                        Write-Host $TargetPath
                        if (Test-Path $TargetPath) {
                            $CountCopied++
                        }
                    }
                }
            }
        }
    }
    Write-Host "$CountCopied file(s) copied"
    if ($CountNotCopiedBecauseOfPlaceholders) {
        Write-Host
        Write-Host "Warning: $CountNotCopiedBecauseOfPlaceholders file(s) were not copied because they contain unrecognized @@ placeholders." -ForegroundColor Yellow
        Write-Host "Specify additional command-line parameters to affect placeholder substitution."
        Write-Host "Run `"Get-Help $(Split-Path -Leaf $MyInvocation.PSCommandPath) -Detailed`" for help on parameters."
        Write-Host
    }
}

$ConfigurationDirectories = Get-ChildItem -Recurse -Directory Configuration
if ($Clean) {
    RunClean
} else {
    RunCopy
}