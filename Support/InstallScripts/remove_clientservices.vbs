'*******************************************************************************
'** WAUSAU Financial Systems (WFS)
'** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
'*******************************************************************************
'*******************************************************************************
'** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
'*******************************************************************************
'* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
'* other trademarks cited herein are property of their respective owners.
'* These materials are unpublished confidential and proprietary information 
'* of WFS and contain WFS trade secrets.  These materials may not be used, 
'* copied, modified or disclosed except as expressly permitted in writing by 
'* WFS (see the WFS license agreement for details).  All copies, modifications 
'* and derivative works of these materials are property of WFS.
'*
'* Author:   Wayne Schwarz
'* Date:     9/11/2012
'*
'* Purpose:  Remove client services
'*
'* Modification History
'*   09/11/2012 CR 55616 WJS
'*       - Initial version.
'*******************************************************************************
Option Explicit

Dim objShell
Dim strPath
Dim strServicePath
Dim strCommand
Dim dicOptions
Dim arrOptions
Dim objOption
Dim i

Set objShell = WScript.CreateObject("WScript.Shell")

On Error Resume Next

strPath = objShell.CurrentDirectory

If Err.Number <> 0 Then
    strPath = ""
End If

On Error Goto 0

If Len(strPath) > 0 Then

    RemoveService "DataImportClientSvc", "WFS Data Import Client Service"
    RemoveService "FileImportClientSvc", "WFS File Import Client Service"

    msgbox "Done"
Else
    msgbox "Please edit file and set Path to reflect installation directory."
End If

Set objShell = Nothing

Function RemoveService (ServiceName, DisplayName)
    Dim strServicePrompt

    strCommand = "bin\sc.exe delete " & ServiceName
    
    strServicePrompt = DisplayName & " Un-Installation"

    If msgbox (strServicePrompt & vbcrlf & String(50,"=") & vbcrlf & vbcrlf & "Command that will execute: " & vbcrlf & vbcrlf & strCommand & vbcrlf & vbcrlf & "Remove " & DisplayName & "?", vbYesNo, strServicePrompt) = vbYes Then
        objShell.Run strCommand
    'Else
    '   msgbox "Command cancelled."
    End If
    
End Function