function DeployPortletDir
{

param
(
    [string] $source, 
    [string] $destination 
)

Write-Host "deploying portlet from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host


if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse


}

$srcPortletDir = "D:\WFSStaging\Portlets\"
$destPortletDir = "D:\WFSApps\RecOnline\"
$destServiceDir = "D:\WFSApps\RecHub\wwwroot\R360Services\"

#  ************************************************   
#  ********* PORTLET VIEW DEPLOYMENT **************   
#  ************************************************   

#  ************** EXTRACT SCHEDULE VIEW **************  
Write-Host "Copying ExtractScheduleViews..."
$tmpSrc = Join-Path $srcPortletDir "ExtractScheduleViews\"
$tmpDest = Join-Path $destPortletDir "ExtractScheduleViews\"
DeployPortletDir $tmpSrc  $tmpDest 

#  ************** EXTRACT SCHEDULE WCF Service *********  
Write-Host "Copying ExtractScheduleService..."
$tmpSrc = Join-Path $srcPortletDir "ExtractScheduleService\"
$tmpDest = Join-Path $destServiceDir "ExtractScheduleService\"
DeployPortletDir $tmpSrc  $tmpDest 

#  ****************    HUB VIEW    ********************  
Write-Host "Copying HubViews..."
$tmpSrc = Join-Path $srcPortletDir "HubViews\"
$tmpDest = Join-Path $destPortletDir "HubViews\"
DeployPortletDir $tmpSrc  $tmpDest 

