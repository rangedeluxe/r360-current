#<#
#	.SYNOPSIS
#	PowerShell script to restore a database from a backup file.
#	
#	.DESCRIPTION
#	The script will restore a database from a backup file. The restored database does not have to be on
#	the same server or same database name. I.E. the PowerShell script does a WITH MOVE. The final step is
#	to cleanup the database making sure it is ready for use by integraPAY.
#	
#	The database will be restored to the default file location defined by the database server.
#	
#	.PARAMETER DBServer
#	The name of the SQL Server where the database will be restored.
#	
#	.PARAMETER DBName
#	The name of the database that will be restored.
#	
#	.PARAMETER Backupfile
#	The name of the backup file. The current version of SMO only supports files on your local drive. 
#	Please copy the backup file from the DBLibrary to a local drive before restoring the database.
#
#	.PARAMETER DefaultDataDirectory
#	By default, the data file is restord to the location defined by the Default Data Directory on the SQL
#	server the restore is ccurring on. This parameter will override the SQL Server setting.
#	
#	.PARAMETER DefaultLogDirectory
#	By default, the log file is restord to the location defined by the Default Log Directory on the SQL
#	server the restore is ccurring on. This parameter will override the SQL Server setting.
#	
#	.PARAMETER LoginName
#	By default, the database connection is made with Windows Authentication. LoginName can be used
#	to define a SQL Server
#	
#	.PARAMETER Password
#	If SQL Server Login Authentication is used, a password must be defined on the command line.
#	
#	.PARAMETER MatchLogicalName
#	When a database is restored, the logical name from the source database is retained. This 
#	parameter causes the restord logical name to match the new database name. By default this is true.
##>
param
(
	[parameter(Mandatory = $true)][string] $DBServer = "",
	[parameter(Mandatory = $true)][string] $DBName = "",
	[parameter(Mandatory = $true)][string] $BackupFile = "",
	[string] $DefaultDataDirectory = "",
	[string] $DefaultLogDirectory = "",
	[string] $LoginID = "",
	[string] $Password = "",
	[string] $PartitionMap = "",
	[bool] $MatchLogicalName = $true,
	[bool] $ScriptOnly = $false,
	[bool] $KeepCDC = $false,
	[bool] $PauseAfterComplete = $true,
	[int] $CompatibilityMode = 0,
	[bool] $UpdateProcessingDate = $false
) 


[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Management.Common") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SMO") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null;
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoEnum") | Out-Null;

$ScriptVersion = "1.7";
################################################################################
#
# 07/08/10 JPB	1.0		Created.
# 07/15/10 JPB	1.1		Added code to 'fix up' the login IDs after the restore is complete.
# 07/16/10 JPB	1.2		Added log directory
# 07/17/10 JPB	1.3		Added Default Data/Log Directory settings.
# 07/21/10 JPB  1.4		Added Service Broker fix up and fixed SQL2000 errors.
# 07/22/10 JPB	1.5		Added partition map XML to support partition database restores.
# 11/16/10 JPB 	1.6		Added ScripOnly option to produce restore script.
# 01/24/14 JPB	1.7		Added CDC support.
#						Support for remote database server.
# 02/10/14 JPB  1.8		Added support for receivables users.
#
################################################################################

###############################################################################
#                                                                             #
# Generic function to execute a SQL statement                                 #
#                                                                             #
###############################################################################

function ExecSQLStatement([string] $sSQLStatement)
{
	$SQLCommand = New-Object System.Data.SqlClient.SqlCommand;
	$SQLCommand = $SQLServerConnection.CreateCommand();
	$SQLCommand.CommandText = $sSQLStatement;
	$SQLCommand.CommandTimeout = 21600;
	$SQLCommand.ExecuteNonQuery() | Out-Null;
	if( $? -eq $false )
	{
		Write-Host "Error executing query";
	}
}

###############################################################################
#                                                                             #
# Fix up the login IDs (resync the GUIDs).                                    #
#                                                                             #
###############################################################################

function FixLoginID([string] $sLoginFix)
{
	Write-Host "Fixing $sLoginFix (if required).." -NoNewline;
	ExecSQLStatement "USE [$DBName]";
#	if( $DBSQLVersion -eq 8 )
#	{
		ExecSQLStatement "IF EXISTS(SELECT [Name] FROM dbo.sysusers WHERE [Name] = '$sLoginFix') IF EXISTS(SELECT b.name as USERName FROM [$DBName].dbo.sysmembers a JOIN [$DBName].dbo.sysusers  b on a.memberuid = b.uid WHERE b.name = '$sLoginFix') EXEC sp_change_users_login @Action = 'update_one', @UserNamePattern = '$sLoginFix', @LoginName = '$sLoginFix'";
#	}
#	else
#	{
#		ExecSQLStatement "IF EXISTS(SELECT * FROM sys.server_principals WHERE [Name] = '$sLoginFix') BEGIN ALTER USER $sLoginFix WITH NAME = $sLoginFix; EXEC sp_addrolemember @rolename = 'IPGroup', @membername = '$sLoginFix' END;";
#	}
	ExecSQLStatement "USE MASTER";
	Write-Host "complete";
}

###############################################################################
#                                                                             #
# Change the owner from the logged in ID to SA.                               #
#                                                                             #
###############################################################################

function ChangeOwner
{
	Write-Host "Setting ownership to SA.." -NoNewline;
	ExecSQLStatement "USE [$DBName]";
	ExecSQLStatement "sp_changedbowner 'sa', 'true'";
	ExecSQLStatement "USE MASTER";
	Write-Host "Complete";
}

function GetNewPartitionPath
{
	param
	(
		[string] $CurrentFileName
	)

	#set the returning path to the current path just in case we do not find a new path in the xml document
	$NewPath = $CurrentFileName;
	$Path = [System.IO.Path]::GetDirectoryName($CurrentFileName);
	$Path += "\";
	$xml = New-Object "System.Xml.XmlDocument";
	$xml.load($PartitionMap);
	##process the file line by line
	foreach( $Convert in $xml.Conversions.Convert )
	{
		if( $Convert.GetAttribute("CurrentPath") -eq $Path )
		{
			$NewPath = $Convert.GetAttribute("NewPath")
			if( !$NewPath.EndsWith("\") )
			{
				$NewPath += "\";
				break;
			}
		}
	}
	return $NewPath;
}

###############################################################################
#                                                                             #
# After a database is restored with Service Broker, Service Broker will not   #
# work until the authorization of the database belongs to SA and the master   #
# key is regenerated. The enable commands are here as 'better safe then       #
# sorry' insurance.                                                           #
#                                                                             #
###############################################################################

function ResetServiceBroker
{
	Write-Host "Checking for OLTA.." -NoNewline;
	foreach( $Database in $SQLServer.Databases )
	{
		if( $Database.Name -eq $DBName )
		{
			foreach( $Schema in $Database.Schemas )
			{
Write-Host $Database.Name"."$Schema.Name;
				if( $Schema.Name -eq "OLTA" )
				{	#found the database that was just restored and it has OLTA installed
					Write-Host "found.";
PressAnyKey;
					Write-Host "Starting Service Broker Cleanup.." -NoNewline;
					ExecSQLStatement "USE MASTER";
PressAnyKey;
					ExecSQLStatement "ALTER AUTHORIZATION ON DATABASE::[$DBName] TO [sa]";
					ExecSQLStatement "ALTER DATABASE $DBName SET NEW_BROKER";
					ExecSQLStatement "ALTER DATABASE $DBName SET ENABLE_BROKER";
					ExecSQLStatement "USE $DBName";
PressAnyKey;
					ExecSQLStatement "ALTER MASTER KEY FORCE REGENERATE WITH ENCRYPTION BY PASSWORD = 'WF`$_mSr_k3y%'"
					Write-Host "Complete";
					break;
				}
			}
		}
	}
	Write-Host;
}

###############################################################################
#                                                                             #
# Error trapping function. Just abort and close any connections that are open.#
#                                                                             #
###############################################################################
Trap 
{
	Write-Host
	Write-Host "*********************************************************************" -ForegroundColor Red;
	$err = $_.Exception
	while ( $err.InnerException )
	{
		$err = $err.InnerException
		Write-Output $err.Message
	};
	if( $err.Errors.Count -gt 0 )
	{
		foreach( $errorMessage in $err.Errors )
		{
			Write-Output $errorMessage.Message;
		}
	}
	Write-Host "*********************************************************************"  -ForegroundColor Red;
	Write-Host "Aborting restore....."  -ForegroundColor Red;
	if( $SQLServerConnection.State -eq [System.Data.ConnectionState]::Open )
	{
		Write-Host "Disconnecting from "$DBServer"."$DBName"";
		$SQLServerConnection.Close();
		$SQLServerConnection.Dispose();
	}
	break;
  }

#function PressAnyKey
#{
#Write-Host "Press any key to continue ..."
#
#$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
#}


#Global Vars needed
[string] $sLogicalDataFileName | Out-Null;
[string] $sLogicalLogFileName | Out-Null;
[string] $sDataFolder | Out-Null;
[string] $sLogFolder | Out-Null;
$WindowsAuth = $true;

cls
Write-Host "RestoreDB.PS1" $ScriptVersion;

if( $DBServer.Length -eq 0 )
{
	Write-Host "Must include SQL Server Name";
	break;
}

if( $DBName.Length -eq 0 )
{
	Write-Host "Must include Datbase Name";
	HelpInfo;
	break;
}

if( $BackupFile.Length -eq 0 )
{
	Write-Host "Must include Backup File Name";
	HelpInfo;
	break;
}


if( $LoginID.Length -gt 0 -or $Password.Length -gt 0 )
{
	if( $LoginID.Length -eq 0 )
	{
		Write-Host "Password defined, must include Login ID";
		break;
	}
	if( $Password.Length -eq 0 )
	{
		Write-Host "Login ID defined, must include Password";
		break;
	}
	$WindowsAuth = $false;
}

if( $PartitionMap.Length -gt 0 -and !([System.IO.Path]::HasExtension($PartitionMap)) )
{
	$PartitionMap += ".xml";
}

Write-Host "Restoring              " $BackupFile;
Write-Host "Server                 " $DBServer;
Write-Host "Database               " $DBName;


$dbConnection = New-Object Microsoft.SqlServer.Management.Common.ServerConnection;
$dbConnection.ServerInstance=$DBServer;
$dbConnection.DatabaseName="master";
$dbConnection.ConnectTimeout = 21600;
$dbConnection.StatementTimeout = 0;
if( $LoginID.Length -eq 0 )
{
	$dbConnection.LoginSecure=$true;
}
else
{
	$dbConnection.LoginSecure=$false;
	$dbConnection.Login=$LoginID;
	$dbConnection.Password=$Password;
}
$SQLServer = new-object Microsoft.SqlServer.Management.Smo.Server($dbConnection);

#$SQLServer = New-Object Microsoft.SqlServer.Management.Smo.Server("$DBServer");

if( $SQLServer.NetName -eq $null )
{
	Write-Host "Could not connect to server" $DBServer "Aborting........." -ForegroundColor Red;
	break;
}

#we need to know what version of SQL Server is running
$DBSQLVersion = $SQLServer.VersionMajor;

if( $DBSQLVersion -eq 8 )
{
	Write-Host "Server SQL Version      2000";
}
elseif( $DBSQLVersion -eq 9 )
{
	Write-Host "Server SQL Version      2005";
}
elseif( $DBSQLVersion -eq 10 )
{
	Write-Host "Server SQL Version      2008";
}
elseif( $DBSQLVersion -eq 11 )
{
	Write-Host "Server SQL Version      2012";
}

if( $PartitionMap.Length -ne 0 )
{
	if( ![System.IO.File]::Exists($PartitionMap) )
	{
		Write-Host "Could not find Partition Map file $PartitionMap. Aborting..." -ForegroundColor Red;
		break;
	}
	else
	{
		Write-Host "Partition Map          " $PartitionMap;
	}
}

#Setup folders where the data/log files will be stored
if( $SQLServer.Settings.DefaultFile.Length -eq 0 )
{
	$sDataFolder = $SQLServer.MasterDBPath;
}
else
{
	$sDataFolder = $SQLServer.Settings.DefaultFile;
}
if( $SQLServer.Settings.DefaultLog.Length -eq 0 )
{
	$sLogFolder = $SQLServer.MasterDBLogPath;
}
else
{
	$sLogFolder = $SQLServer.Settings.DefaultLog;
}

#Override the default folders if passed in from the command line
if( $DefaultDataDirectory.Length -ne 0 )
{
	$sDataFolder =  $DefaultDataDirectory;
}

if( $DefaultLogDirectory.Length -ne 0 )
{
	$sLogFolder =  $DefaultLogDirectory;
}

#double check taht we have valid folder names
if( $sDataFolder -eq $null -or $sDataFolder.Length -eq 0 )
{
	
	Write-Host "Could not determine a location ($sDataFolder) to store the database data file. Aborting.....";
	break;
}

if( $sLogFolder -eq $null -or $sLogFolder.Length -eq 0 )
{
	Write-Host "Could not determine a location to store the database log file. Aborting.....";
	break;
}

#Cleanup the folder names
if( $sDataFolder -ne $null -and !$sDataFolder.EndsWith("\") )
{
		$sDataFolder = $sDataFolder + "\";
}

if( $sLogFolder.Length -ne $null -and !$sLogFolder.EndsWith("\") )
{
	$sLogFolder = $sLogFolder + "\";
}

Write-Host "Data Restore Location  " $sDataFolder;
Write-Host "Log Restore Location   " $sLogFolder;
Write-Host;

#create the restore SMO object and add the backup file to the devices
$Restore = New-Object "Microsoft.SqlServer.Management.Smo.Restore";
$Restore.Devices.AddDevice($BackupFile,[Microsoft.SqlServer.Management.Smo.DeviceType]::File);

$HeaderInfo = $Restore.ReadBackupHeader($SQLServer);
#$HeaderInfo = $Restore.ReadBackupHeader("$DBServer");


if( $HeaderInfo.Rows[0].SoftwareVersionMajor -gt $DBSQLVersion )
{
	Write-Host "The backup file is from " -NoNewline;
	if( $HeaderInfo.Rows[0].SoftwareVersionMajor -eq 8 )
	{
		Write-Host "Server SQL Version 2000" -NoNewline;
	}
	elseif( $HeaderInfo.Rows[0].SoftwareVersionMajor -eq 9 )
	{
		Write-Host "Server SQL Version 2005" -NoNewline;
	}
	elseif( $HeaderInfo.Rows[0].SoftwareVersionMajor -eq 10 )
	{
		Write-Host "Server SQL Version 2008" -NoNewline;
	}
	elseif( $HeaderInfo.Rows[0].SoftwareVersionMajor -eq 11 )
	{
		Write-Host "Server SQL Version 2012" -NoNewline;
	}
	Write-Host " and cannot be restored to the selected server. Aborting restore..";
	break;
}

Write-Host "Reading backup file information.." -NoNewline;
$RestoreFiles = $Restore.ReadFileList($SQLServer);
#$RestoreFiles = $Restore.ReadFileList("$DBServer");

#relocate files to new location
$RestoreDataFile = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile");
$RestoreLogFile = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile");

foreach($r in $RestoreFiles.Rows)
{
	if( $r.Type -eq "D" -and $r.FileGroupName.ToUpper() -eq "PRIMARY" )
	{
		$sLogicalDataFileName = $r.LogicalName;
		$RestoreDataFile.LogicalFileName = $r.LogicalName;
		$RestoreDataFile.PhysicalFileName = $sDataFolder + $DBName + "_data.mdf";
		$Restore.RelocateFiles.Add($RestoreDataFile) | Out-Null;
	}
	elseif( $r.Type -eq "D" )
	{
		$NewPath = GetNewPartitionPath $r.PhysicalName;
		$RestorePartitionFile = New-Object("Microsoft.SqlServer.Management.Smo.RelocateFile");
		$RestorePartitionFile.LogicalFileName = $r.LogicalName;
		$RestorePartitionFile.PhysicalFileName = $NewPath + [System.IO.Path]::GetFileName($r.PhysicalName);
#		$RestorePartitionFile.PhysicalFileName = $NewPath + $r.LogicalName + ".mdf";
		$Restore.RelocateFiles.Add($RestorePartitionFile) | Out-Null;
	}
	elseif( $r.Type -eq "L" )
	{
		$sLogicalLogFileName = $r.LogicalName
		$RestoreLogFile.LogicalFileName = $r.LogicalName;
		$RestoreLogFile.PhysicalFileName = $sLogFolder + $DBName + "_log.ldf";
		$Restore.RelocateFiles.Add($RestoreLogFile) | Out-Null;
	}
}

Write-Host

#setup the restore options
$Restore.Database = $DBName;

#determine if database already exists on the server
foreach( $Database in $SQLServer.Databases )
{
	if( $Database.Name -eq $DBName -and !$Database.IsSystemObject )
	{
		$Restore.ReplaceDatabase = $true;
		break;
	}
}

$Restore.PercentCompleteNotification = 5;
$Restore.Action = "Database";

Write-Host "Moving data file  " $RestoreDataFile.LogicalFileName "to" $RestoreDataFile.PhysicalFileName;
Write-Host "Moving log file   " $RestoreLogFile.LogicalFileName "to" $RestoreLogFile.PhysicalFileName;

#restore the database
#$SQLServer.ConnectionContext.StatementTimeout = 6000;
$SQLServer.ConnectionContext.StatementTimeout = 0;

$Restore.NoRecovery = $false;
if( $ScriptOnly -eq $true )
{
	$ScriptFileName = "C:\RestoreScripts\RestoreDB-"+$DBName+".SQL";
	Write-Host "Creating restore script" $ScriptFileName;
	$Restore.Script("$DBServer") | Out-File $ScriptFileName;
	break;
}

if( $KeepCDC )
{
	$RestoreScript = $Restore.Script($SQLServer);
	$RestoreSQL = '';
	foreach( $line in $RestoreScript )
	{
		$RestoreSQL += $line;
	}
	$RestoreSQL += ", keep_cdc";
	$SQLServerConnectionOptions = "Data Source=$DBServer; Initial Catalog=master;";
	if( $WindowsAuth )
	{
		$SQLServerConnectionOptions += "Integrated Security=SSPI";
	}
	else 
	{
		$SQLServerConnectionOptions += "User ID=$LoginID;Password=$Password";
	}	
	$SQLServerConnection = New-Object System.Data.SqlClient.SqlConnection($SQLServerConnectionOptions);
	$SQLServerConnection.Open();
	if( $Restore.ReplaceDatabase )
	{
		Write-Host "Clearing CDC Jobs";
		ExecSQLStatement "IF( SELECT 1 FROM sys.databases WHERE name = '$DBName' and is_cdc_enabled = 1 ) IS NOT NULL BEGIN USE [$DBName];exec sys.sp_cdc_drop_job 'capture';USE master;END";
		ExecSQLStatement "IF( SELECT 1 FROM sys.databases WHERE name = '$DBName' and is_cdc_enabled = 1 ) IS NOT NULL BEGIN USE [$DBName];exec sys.sp_cdc_drop_job 'cleanup';USE master;END";
	}
	Write-Host "Restoring..." -NoNewline;
	ExecSQLStatement $RestoreSQL;
	Write-Host;
	Write-Host "Starting CDC Jobs";
	ExecSQLStatement "USE [$DBName];exec sys.sp_cdc_add_job 'capture';USE master;";
	ExecSQLStatement "USE [$DBName];exec sys.sp_cdc_add_job 'cleanup';USE master;";
	$SQLServerConnection.Close()
	$SQLServerConnection = $null;
	Write-Host "complete";
}
else
{

	Write-Host "Restoring..." -NoNewline;
	#$Restore.SqlRestore("$DBServer");
	$Restore.SqlRestoreAsync($SQLServer);
	#$Restore.SqlRestoreAsync("$DBServer");
	$Restore.Wait();
	Write-Host "complete";
}

if( $PauseAfterComplete )
{
	Write-Host "Press any key to continue ..."
	$x = $host.UI.RawUI.ReadKey("NoEcho,IncludeKeyDown")
}

#We have a bunch of clean to do now that the DB is restored
$SQLServerConnectionOptions = "Data Source=$DBServer; Initial Catalog=$DBName;";
if( $WindowsAuth )
{
	$SQLServerConnectionOptions += "Integrated Security=SSPI";
}
else 
{
	$SQLServerConnectionOptions += "User ID=$LoginID;Password=$Password";
}

Write-Host;
Write-Host "Connecting to"$DBServer"."$DBName" for cleanup..." -NoNewline;
$SQLServerConnection = New-Object System.Data.SqlClient.SqlConnection($SQLServerConnectionOptions);
$SQLServerConnection.Open();
Write-Host;

ChangeOwner;
FixLoginID "IPAdmin";
FixLoginID "IPUser";
FixLoginID "IPBulkAdmin";
FixLoginID "OLTAUser";
FixLoginID "OLUser";
FixLoginID "RecHubAlert_User";
FixLoginID "RecHubAudit_User";
FixLoginID "RecHubConfig_Admin";
FixLoginID "RecHubData_User";
FixLoginID "RecHubDBO_User";
FixLoginID "RecHubException_User";
FixLoginID "RecHubExtractWizard_User";
FixLoginID "RecHubService_Account";
FixLoginID "RecHubSystem_User";
FixLoginID "RecHubUser_User";

#set the current processing date now
if( $UpdateProcessingDate )
{
	Write-Host "Update current processing date";
	$SQL = "IF( OBJECT_ID('[" +$DBName + "].dbo.Systems') IS NOT NULL ) UPDATE [" + $DBName + "].dbo.Systems SET CurrentProcessingDate = GETDATE() WHERE SystemName='CheckBOX'";
	ExecSQLStatement $SQL ;
}

if( $CompatibilityMode -gt 0 )
{
	Write-Host "Setting Compatibility Mode";
	ExecSQLStatement "USE master;ALTER DATABASE [$DBName] SET COMPATIBILITY_LEVEL = $CompatibilityMode";
}

if( $MatchLogicalName -eq $true )
{
	Write-Host "Setting Logical Name.." -NoNewline
	$SQL = "IF NOT EXISTS(SELECT 1 FROM sys.master_files WHERE name = '"+$sLogicalDataFileName+"' AND DB_NAME(database_id) = '"+$DBName+"' ) ALTER DATABASE [$DBName] MODIFY FILE (NAME=["+$sLogicalDataFileName+"], NEWNAME=["+$DBName+"_data])";
	ExecSQLStatement  $SQL;
	$SQL = "IF NOT EXISTS(SELECT 1 FROM sys.master_files WHERE name = '"+$sLogicalLogFileName+"' AND DB_NAME(database_id) = '"+$DBName+"' ) ALTER DATABASE [$DBName] MODIFY FILE (NAME=["+$sLogicalLogFileName+"], NEWNAME=["+$DBName+"_log])";
	ExecSQLStatement $SQL;
	Write-Host "Complete";
}

#Check and see if the restored database has OLTA installed
#ResetServiceBroker;

#Close the database connection so we are clean
if( $SQLServerConnection.State -eq [System.Data.ConnectionState]::Open )
{
	Write-Host "Disconnecting from "$DBServer"."$DBName"";
	$SQLServerConnection.Close();
}
exit