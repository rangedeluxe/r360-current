function DeployFrameworkDir
{


param
(
    [string] $source, 
    [string] $destination 
)

Write-Host "deploying framework from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host


if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse


}

$srcFrameworkDir = "D:\WFSStaging\Framework\"
$destFrameworkDir = "D:\WFSApps\RecOnline\"


#  ************************************************   
#  ************** PORTLET FRAMEWORK DEPLOYMENT ****   
#  ************************************************   
Write-Host "Deploying portlet framework from:"
Write-Host $srcFrameworkDir
Write-Host "To area: " 
Write-Host $destFrameworkDir
Write-Host 

#  ************** ASSETS  **************  
Write-Host "Copying Assets..."
$tmpSrc = Join-Path $srcFrameworkDir "Assets"
$tmpDest = Join-Path $destFrameworkDir "Assets"
DeployFrameworkDir $tmpSrc  $tmpDest 

#  ************** Framework  **************  
Write-Host "Copying Framework..."
$tmpSrc = Join-Path $srcFrameworkDir "Framework"
$tmpDest = Join-Path $destFrameworkDir "Framework"
DeployFrameworkDir $tmpSrc  $tmpDest 

#  ************** framework.services  **************  
Write-Host "Copying framework.services..."
$tmpSrc = Join-Path $srcFrameworkDir "FrameworkServices"
$tmpDest = Join-Path $destFrameworkDir "FrameworkServices"
DeployFrameworkDir $tmpSrc  $tmpDest 

#  ************** FrameworkViews  **************  
Write-Host "Copying FrameworkViews..."
$tmpSrc = Join-Path $srcFrameworkDir "FrameworkViews"
$tmpDest = Join-Path $destFrameworkDir "FrameworkViews" 
DeployFrameworkDir $tmpSrc  $tmpDest 

#  ************** wfs.i18n.services  **************  
Write-Host "Copying wfs.i18n.services..."
$tmpSrc = Join-Path $srcFrameworkDir "i18nservices"
$tmpDest = Join-Path $destFrameworkDir "i18nservices"
DeployFrameworkDir $tmpSrc  $tmpDest 

