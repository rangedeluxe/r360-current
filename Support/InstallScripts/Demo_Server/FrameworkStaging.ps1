function StageFrameworkDir
{
param
(
    [string] $source, 
    [string] $destination 
)


Write-Host "staging framework from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host

if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse


#copy over the configs as .oem files
get-childitem $destination *.config -Recurse | foreach ($_) {Copy-Item $_.fullname  ($_.fullname + ".oem") -Force }

#remove the *config files
get-childitem $destination *.config -Recurse | foreach ($_) {remove-item $_.fullname}

}

$srcFrameworkPath = "\\sanserver\ProductDevelopment\Legacy\QA Pending\Receivables 360\2.01\Framework\Current\"
$destFrameworkDir = "D:\WFSStaging\Framework\"

#  ************************************************   
#  ************** PORTLET FRAMEWORK DEPLOYMENT ****   
#  ************************************************   
Write-Host "Fetching portlet framework from:"
Write-Host $srcFrameworkPath
Write-Host "To copy to staging area: " 
Write-Host $destFrameworkDir
Write-Host 

#  ************** ASSETS  **************  
Write-Host "Copying Assets..."
$tmpSrc = Join-Path $srcFrameworkPath "Assets\"
$tmpDest = Join-Path $destFrameworkDir "Assets\"
StageFrameworkDir $tmpSrc  $tmpDest 

#  ************** Framework  **************  
Write-Host "Copying Framework..."
$tmpSrc = Join-Path $srcFrameworkPath "Framework\"
$tmpDest = Join-Path $destFrameworkDir "Framework\"
StageFrameworkDir $tmpSrc  $tmpDest 

#  ************** FrameworkViews  **************  
Write-Host "Copying FrameworkViews..."
$tmpSrc = Join-Path $srcFrameworkPath "FrameworkViews\"
$tmpDest = Join-Path $destFrameworkDir "FrameworkViews\" 
StageFrameworkDir $tmpSrc  $tmpDest 

#  ************** framework.services  **************  
Write-Host "Copying framework.services..."
$tmpSrc = Join-Path $srcFrameworkPath "framework.services\"
$tmpDest = Join-Path $destFrameworkDir "FrameworkServices\"
StageFrameworkDir $tmpSrc  $tmpDest 

#  ************** wfs.i18n.services  **************  
Write-Host "Copying wfs.i18n.services..."
$tmpSrc = Join-Path $srcFrameworkPath "wfs.i18n.services\"
$tmpDest = Join-Path $destFrameworkDir "i18nservices\"
StageFrameworkDir $tmpSrc  $tmpDest 
