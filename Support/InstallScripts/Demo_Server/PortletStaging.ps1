function StagePortletDir
{

param
(
    [string] $source, 
    [string] $destination 
)


Write-Host "staging portlet from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host

if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse

#copy over the configs as .oem files
get-childitem $destination *.config -Recurse | foreach ($_) {Copy-Item $_.fullname  ($_.fullname + ".oem") -Force }

#remove the *config files
get-childitem $destination *.config -Recurse | foreach ($_) {remove-item $_.fullname}

}

$srcPortletDir = "\\sanserver\ProductDevelopment\Legacy\QA Pending\Receivables 360\2.01\HubUI\Current\"
$srcWcfDir = "\\sanserver\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\2014\2.01\Current_Build\RecHub.R360_Service.ExtractSchedule\wwwroot\R360Services\"
$destPortletDir = "D:\WFSStaging\Portlets\"



#  ************************************************   
#  ********* PORTLET VIEW DEPLOYMENT **************   
#  ************************************************   
Write-Host 
Write-Host "Fetching portlet views from:"
Write-Host $srcPortletDir
Write-Host "To copy to staging area: " 
Write-Host $destPorletDir
Write-Host 

#  ************** EXTRACT SCHEDULE VIEW **************  
Write-Host "Copying ExtractScheduleViews..."
$tmpSrc = Join-Path $srcPortletDir "ExtractScheduleViews\"
$tmpDest = Join-Path $destPortletDir "ExtractScheduleViews\"
StagePortletDir $tmpSrc  $tmpDest 

#  ************** EXTRACT SCHEDULE WCF Service *********  
Write-Host "Copying ExtractSchedule WCF Service..."
$tmpSrc = Join-Path $srcWcfDir "ExtractSchedule\"
$tmpDest = Join-Path $destPortletDir "ExtractScheduleService\"
StagePortletDir $tmpSrc  $tmpDest 

#  ****************    HUB VIEW    ********************  
Write-Host "Copying HubViews..."
$tmpSrc = Join-Path $srcPortletDir "HubViews\"
$tmpDest = Join-Path $destPortletDir "HubViews\"
StagePortletDir $tmpSrc  $tmpDest 
