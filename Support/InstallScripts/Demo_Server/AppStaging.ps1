﻿function StageAppDir
{

param
(
    [string] $source, 
    [string] $destination 
)


Write-Host "staging framework from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host

if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse


#copy over the configs as .oem files
get-childitem $destination *.config -Recurse | foreach ($_) {Copy-Item $_.fullname  ($_.fullname + ".oem") -Force }

#remove the *config files
get-childitem $destination *.config -Recurse | foreach ($_) {remove-item $_.fullname}

}

$srcQaPath = "\\sanserver\ProductDevelopment\Legacy\QA Pending\RecHub_Build_Location\2014\2.01\Current_Build\"
$destAppDir = "D:\WFSStaging\Apps\"

Write-Host "Fetching applications from QA:"
Write-Host $srcQaPath
Write-Host "To copy to staging area:"
Write-Host $destAppDir
Write-Host 

#  ************** EXTRACT WIZARD **************   
Write-Host "Copying Extract Wizard..."
$tmpSrc = Join-Path $srcQaPath "RecHub.DataOutput.ExtractWizard\bin2\"
$tmpDest = Join-Path $destAppDir "ExtractWizard\"
StageAppDir $tmpSrc  $tmpDest 


#  ************** EXTRACT PROCESS SERVICE **************   
Write-Host "Copying Extract Process Service..."
$tmpSrc = Join-Path $srcQaPath "RecHub.DataOutput.ExtractProcessSvc\bin2\"
$tmpDest = Join-Path $destAppDir "ExtractProcessSvc\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** OLF IMAGE SERVICE (Windows Service) **************    
Write-Host "Copying OLF Image Service..."
$tmpSrc = Join-Path $srcQaPath "RecHub.OLF.Services\bin2\"
$tmpDest = Join-Path $destAppDir "OLFImageSvc\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** OLF SERVICES (WCF) **************
Write-Host "Copying OLF Services..."
$tmpSrc = Join-Path $srcQaPath "RecHub.OLF.Services\wwwroot\OLFServices"
$tmpDest = Join-Path $destAppDir "OLFServices\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** OLF SERVICES (Windows Service version of WCF) **************
Write-Host "Copying OLF Services Windows Service..."
$tmpSrc = Join-Path $srcQaPath "RecHub.OLF.Services\bin2\"
$tmpDest = Join-Path $destAppDir "OLFServiceSvc\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** HUB SERVICES (WCF) **************
Write-Host "Copying Hub Services..."
$tmpSrc = Join-Path $srcQaPath "RecHub.R360_Service.Hub\wwwroot\R360Services\Hub"
$tmpDest = Join-Path $destAppDir "HubServices\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** PAYER SERVICES (WCF) **************
Write-Host "Copying Payer Services..."
$tmpSrc = Join-Path $srcQaPath "RecHub.R360_Service.Payer\wwwroot\R360Services\Payer"
$tmpDest = Join-Path $destAppDir "PayerServices\"
StageAppDir $tmpSrc  $tmpDest 

#  ************** IPOnline APPLICATION **************
Write-Host "Copying IPOnline..."
$tmpSrc = Join-Path $srcQaPath "RecHub.Online\wwwroot\IPOnline"
$tmpDest = Join-Path $destAppDir "IPOnline\"
StageAppDir $tmpSrc  $tmpDest

#  ************** ONLINE SERVICES (WCF) **************
Write-Host "Copying Online Services..."
$tmpSrc = Join-Path $srcQaPath "RecHub.Online\wwwroot\OLServices"
$tmpDest = Join-Path $destAppDir "OLServices\"
StageAppDir $tmpSrc  $tmpDest 