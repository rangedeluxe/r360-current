﻿function DeployAppkDir
{


param
(
    [string] $source, 
    [string] $destination 
)

Write-Host "deploying framework from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host


if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse


}


$srcDir = "D:\WFSStaging\Apps\"
$destDir = "D:\WFSApps\RecHub\"
$destBin2Dir = "D:\WFSApps\RecHub\bin2\"
$destR360ServciesDir = "D:\WFSApps\RecHub\wwwroot\R360Services\"

Write-Host "Fetching applications from Staging:"
Write-Host $srcDir
Write-Host "To copy to deployment:"
Write-Host $destBin2Dir
Write-Host 

#  ************** EXTRACT WIZARD **************   
Write-Host "Copying ExtractWizard..."
$tmpSrc = Join-Path $srcDir "ExtractWizard\"
$tmpDest = $destBin2Dir
DeployAppkDir $tmpSrc  $tmpDest 

#  ************** EXTRACT PROCESS SERVICE **************  
# Install/Uninstall: 
# ExtractProcessSvc.exe /install
# ExtractProcessSvc.exe /uninstall

$service = Get-WmiObject -Class Win32_Service -Filter "Name='WFSExtractProcessSvc'"
If ($service)
{
    Write-Host "Stopping ExtractProcessorSvc..."
    Stop-Service -InputObject WFSExtractProcessSvc
}

Write-Host "Copying ExtractProcessSvc..."
$tmpSrc = Join-Path $srcDir "ExtractProcessSvc\"
$tmpDest = $destBin2Dir
DeployAppkDir $tmpSrc  $tmpDest 

Write-Host "Uninstalling ExtractProcessorSvc..."
$svcPath = Join-Path $tmpDest "ExtractProcessSvc.exe"
Start-Process -FilePath $svcPath -ArgumentList /uninstall -Wait

Write-Host "Installing ExtractProcessorSvc..."
Start-Process -FilePath $svcPath -ArgumentList /install -Wait

Write-Host "Starting ExtractProcessSvc"
Start-Service "WFSExtractProcessSvc"
Write-Host "ExtractProcessSvc Started"

#  ************** OLF IMAGE SERVICE (Windows Service) **************   
# Needs Administrative permissions to call New-Service

$service = Get-WmiObject -Class Win32_Service -Filter "Name='OLFImageSvc'"
If ($service)
{
    Write-Host "Stopping OLF Image Service..."
    Stop-Service -InputObject OLFImageSvc
    Write-Host "Removing OLFImageSvc..."
    $service.delete()
}

Write-Host "Copying OLF Image Service..."
$tmpSrc = Join-Path $srcDir "OLFImageSvc\"
$tmpDest = $destBin2Dir
DeployAppkDir $tmpSrc  $tmpDest 

Write-Host "Installing OLF Image Service..."

$exePath = Join-Path $destBin2Dir "OLFImageSvc.exe"
New-Service -BinaryPathName $exePath -Name OLFImageSvc -DisplayName "OLF IPOnline Image Service"

Write-Host "Starting OLFImageSvc..."
Start-Service -InputObject OLFImageSvc
Write-Host "OLFImageSvc Started"

#  ************** OLF SERVICES (WCF) **************
Write-Host "Copying OLFServices..."
$tmpSrc = Join-Path $srcDir "OLFServices\"
$tmpDest = $destBin2Dir
DeployAppkDir $tmpSrc  $tmpDest 

#  ************  HUB SERVICES (WCF)  ****************  
Write-Host "Copying Hub Services..."
$tmpSrc = Join-Path $srcDir "HubServices\"
$tmpDest = Join-Path $destR360ServciesDir "Hub\"
DeployPortletDir $tmpSrc  $tmpDest 

#  ***********  PAYER SERVICES (WCF)  ***************  
Write-Host "Copying Payer Services..."
$tmpSrc = Join-Path $srcDir "PayerServices\"
$tmpDest = Join-Path $destR360ServciesDir "Payer\"
DeployPortletDir $tmpSrc  $tmpDest 

#  ***********  IPONLINE APPLICATION  ***************  
Write-Host "Copying IPOnline Application..."
$tmpSrc = Join-Path $srcDir "IPOnline\"
$tmpDest = Join-Path $destDir "IPOnline\"
DeployPortletDir $tmpSrc  $tmpDest 

#  ***********  OL SERVICES (WCF)  ***************  
Write-Host "Copying OL Services..."
$tmpSrc = Join-Path $srcDir "OLServices\"
$tmpDest = Join-Path $destDir "OLServices\"
DeployPortletDir $tmpSrc  $tmpDest 