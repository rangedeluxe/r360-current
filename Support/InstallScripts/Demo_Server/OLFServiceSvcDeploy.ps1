﻿function DeployAppkDir
{

param
(
    [string] $source, 
    [string] $destination 
)

Write-Host "deploying framework from" 
Write-Host $source
Write-Host "to"
Write-Host $destination
Write-Host


if (!(Test-Path -path $destination)) { New-Item $destination -itemtype directory -force } 

$source = $source.TrimEnd("\")
if( !$source.EndsWith("\*") ) { $source = $source + "\*" }

copy-item $source $destination -Force -Recurse

}

$srcDir = "D:\WFSStaging\Apps\"
$destDir = "D:\WFSApps\RecHub\"
$destBin2Dir = "D:\WFSApps\RecHub\bin2\"
$destR360ServciesDir = "D:\WFSApps\RecHub\wwwroot\R360Services\"

Write-Host "Fetching applications from Staging:"
Write-Host $srcDir
Write-Host "To copy to deployment:"
Write-Host $destBin2Dir
Write-Host 

#  ************** OLF SERVICES (Windows Service version of WCF) **************   
# Needs Administrative permissions to call New-Service

$service = Get-WmiObject -Class Win32_Service -Filter "Name='OLFServiceSvc'"
If ($service)
{
    Write-Host "Stopping OLF Windows Service..."
    Stop-Service -InputObject OLFServiceSvc
    Write-Host "Removing OLFServiceSvc..."
    $service.delete()
}

Write-Host "Copying OLF Windows Service..."
$tmpSrc = Join-Path $srcDir "OLFServiceSvc\"
$tmpDest = $destBin2Dir
DeployAppkDir $tmpSrc  $tmpDest 

Write-Host "Installing OLF Windows Service..."

$exePath = Join-Path $destBin2Dir "OLFServiceSvc.exe"
New-Service -BinaryPathName $exePath -Name OLFServiceSvc -DisplayName "OLF Services Windows Service"

Write-Host "Starting OLFServiceSvc..."
Start-Service -InputObject OLFServiceSvc
Write-Host "OLFServiceSvc Started"
