'*******************************************************************************
'** WAUSAU Financial Systems (WFS)
'** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
'*******************************************************************************
'*******************************************************************************
'** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
'*******************************************************************************
'* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
'* other trademarks cited herein are property of their respective owners.
'* These materials are unpublished confidential and proprietary information 
'* of WFS and contain WFS trade secrets.  These materials may not be used, 
'* copied, modified or disclosed except as expressly permitted in writing by 
'* WFS (see the WFS license agreement for details).  All copies, modifications 
'* and derivative works of these materials are property of WFS.
'*
'* Author:   Joy Sucha
'* Date:     4/16/2009
'*
'* Purpose:  Remove integraPAY Online services.
'*
'* Modification History
'*   04/16/2009 CR 25816 JCS
'*       - Initial version.
'*   0/12/2010 CR 28640 MEH
'*       - added ICON service
'*   06/11/2013 WI 105131 CRG
'*	-  Add Purge Service to install and remove service vbscript.
'*  06/27/2013 WI 107460 DRP
'*  - Added ExtractProcessSvc to install and remove service script.
'*******************************************************************************
Option Explicit

Dim objShell
Dim strPath
Dim strServicePath
Dim strCommand
Dim dicOptions
Dim arrOptions
Dim objOption
Dim i

Set objShell = WScript.CreateObject("WScript.Shell")

On Error Resume Next

strPath = objShell.CurrentDirectory
'strPath = "C:\IPOnline"

If Err.Number <> 0 Then
    strPath = ""
End If

On Error Goto 0

If Len(strPath) > 0 Then

    RemoveService "OLFImageSvc", "WFS IPOnline Image Service"
    RemoveService "FileCleanupSvc", "WFS IPOnline Cleanup Service"
    RemoveService "OLFImageImportSvc", "WFS IPOnline Image Import Service"
    RemoveService "FilePurgeSvc", "WFS File Purge Service"
    RemoveService "WFSExtractProcessSvc", "WFS Extract Process Service"
    RemoveService "WFSR360BillingExtractsSvc", "WFS R360 Billing Extracts Service"
    RemoveService "DataImportSvc", "WFS Data Import Service"
    RemoveService "FileImportSvc", "WFS File Import Service"

    msgbox "Done"
Else
    msgbox "Please edit file and set Path to reflect installation directory."
End If

Set objShell = Nothing

Function RemoveService (ServiceName, DisplayName)
    Dim strServicePrompt

    strCommand = "bin\sc.exe delete " & ServiceName
    
    strServicePrompt = DisplayName & " Un-Installation"

    If msgbox (strServicePrompt & vbcrlf & String(50,"=") & vbcrlf & vbcrlf & "Command that will execute: " & vbcrlf & vbcrlf & strCommand & vbcrlf & vbcrlf & "Remove " & DisplayName & "?", vbYesNo, strServicePrompt) = vbYes Then
        objShell.Run strCommand
    'Else
    '   msgbox "Command cancelled."
    End If
    
End Function