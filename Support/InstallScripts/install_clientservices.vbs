'*******************************************************************************
'** WAUSAU Financial Systems (WFS)
'** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
'*******************************************************************************
'*******************************************************************************
'** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
'*******************************************************************************
'* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
'* other trademarks cited herein are property of their respective owners.
'* These materials are unpublished confidential and proprietary information 
'* of WFS and contain WFS trade secrets.  These materials may not be used, 
'* copied, modified or disclosed except as expressly permitted in writing by 
'* WFS (see the WFS license agreement for details).  All copies, modifications 
'* and derivative works of these materials are property of WFS.
'*
'* Author:   Wayne Schwarz
'* Date:     9/11/2012
'*
'* Purpose:  Install client services
'*
'* Modification History
'*   09/11/2012 CR 55616 WJS
'*       - Initial version.

'*******************************************************************************
Option Explicit

Dim objShell
Dim strPath
Dim strServicePath
Dim strCommand
Dim dicOptions
Dim arrOptions
Dim objOption
Dim i

Set objShell = WScript.CreateObject("WScript.Shell")

On Error Resume Next

strPath = objShell.CurrentDirectory

If Err.Number <> 0 Then
    strPath = ""
End If

On Error Goto 0

If Len(strPath) > 0 Then
    
    AddService "DataImportClientSvc", "\bin\DataImportClientSvc.exe", "WFS Data Import Client Service", "", ""
    AddService "FileImportClientSvc", "\bin\FileImportClientSvc.exe", "WFS File Import Client Service", "", ""
    
    msgbox "Done"
Else
    msgbox "Please edit file and set Path to reflect installation directory."
End If

Set objShell = Nothing


Function AddService (ServiceName, ServicePath, DisplayName, UserName, Password)
    Dim strServicePrompt

    Set dicOptions = WScript.CreateObject ("Scripting.Dictionary")

    strServicePath = strPath & ServicePath
    
    ' Add sc.exe options to collection
    dicOptions.Add "binPath", strServicePath
    dicOptions.Add "start", "demand"
    dicOptions.Add "DisplayName", DisplayName
    
    If Len(UserName) > 0 Then
        dicOptions.Add "obj", UserName
        dicOptions.Add "password", Password
    End If

    Redim arrOptions(dicOptions.Count - 1)
    i = 0
    
    ' Construct string of options
    for each objOption in dicOptions
        If Instr(1,dicOptions.Item(objOption)," ",1) Then
            arrOptions(i) = objOption & "= " & chr(34) & dicOptions.Item(objOption) & chr(34)
        Else
            arrOptions(i) = objOption & "= " & dicOptions.Item(objOption)
        End If
        i = i + 1
    next

    strCommand = "bin\sc.exe create " & ServiceName & " " & Join(arrOptions," ")
    
    strServicePrompt = DisplayName & " Installation"
    
    If msgbox (strServicePrompt & vbcrlf & String(50,"=") & vbcrlf & vbcrlf & "Command that will execute: " & vbcrlf & vbcrlf & strCommand & vbcrlf & vbcrlf & "Install " & DisplayName & "?", vbYesNo, strServicePrompt) = vbYes Then
        objShell.Run strCommand
    'Else
    '   msgbox "Command cancelled."
    End If
    
    Set dicOptions = Nothing
End Function