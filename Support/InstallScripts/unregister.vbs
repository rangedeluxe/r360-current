'*******************************************************************************
'** WAUSAU Financial Systems (WFS)
'** Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved
'*******************************************************************************
'*******************************************************************************
'** DO NOT DISCLOSE THESE MATERIALS TO ANY THIRD PARTY.
'*******************************************************************************
'* Copyright � 2009 WAUSAU Financial Systems, Inc. All rights reserved.  All
'* other trademarks cited herein are property of their respective owners.
'* These materials are unpublished confidential and proprietary information 
'* of WFS and contain WFS trade secrets.  These materials may not be used, 
'* copied, modified or disclosed except as expressly permitted in writing by 
'* WFS (see the WFS license agreement for details).  All copies, modifications 
'* and derivative works of these materials are property of WFS.
'*
'* Author:   Joy Sucha
'* Date:     4/16/2009
'*
'* Purpose:  Unregister integraPAY Online COM components.
'*
'* Modification History
'*   04/16/2009 CR 25816 JCS
'*       - Initial version.
'*******************************************************************************

Set WshShell = WScript.CreateObject("WScript.Shell")
'WshShell.Run "%windir%\notepad " & WScript.ScriptFullName
'wshShell.run "cmd /K CD C:\ & Dir"

WshShell.Run "%windir%\System32\Regsvr32.exe /s /u bin\CBOCrypto.dll"

msgbox "Done"
