param (
    [string] $drive = "d:"
)

Write-Host "Setting up paths"
$destRecHubDir = Join-Path $drive "\WFSApps\RecHub\"
$destRecHubBin2Dir = Join-Path $destRecHubDir "bin2"
$destRecHubBinDir = Join-Path $destRecHubDir "bin"

# ================== Define Services =============
$serviceList = @(,
    @("DataImportClientSvc",    (Join-Path $destRecHubBinDir "DataImportClientSvc.exe"),  "WFS Data Import Client Service", "", "", "")
)

function StartServices
{
    param
    (
        $serviceList
    )

    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName -ErrorAction SilentlyContinue
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)
}

# ================== Start Services if applicable =============
Write-Host "Starting Services"
StartServices $serviceList
