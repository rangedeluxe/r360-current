﻿param (
    [switch] $NestedCall,
    [switch] $Silent,
    # Don't perform any Windows service configuration. Intended for testing only.
    [switch] $NoService,
    $StagingPath = ".",
	# Don't backup before copying files
    [Switch] $SkipBackup
)

# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
[string] $Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)c"

$ErrorActionPreference = "Stop"

if (!$NestedCall) {
    # Start a new instance of PowerShell, so the DLL gets unloaded at the end
    $ExtraArgs = @()
    if ($Silent) {
        $ExtraArgs += "-Silent"
    }
    if ($NoService) {
        $ExtraArgs += "-NoService"
    }
    if ($SkipBackup) {
        $ExtraArgs += "-SkipBackup"
    }    powershell -file $MyInvocation.MyCommand.Path -NestedCall @ExtraArgs
    if (!$Silent) {
        Write-Host "Press ENTER to exit: " -NoNewline
        Read-Host
    }
    return
}

if( $SkipBackup -and $Silent )
{
	Write-Host "Skipping backup, remember to backup files manually" -ForegroundColor Red -BackgroundColor White;
}
elseif( $SkipBackup )
{
	Write-Host "Skipping backup, remember to backup files manually. [Q] to quit, any other key to continue." -ForegroundColor Red -BackgroundColor White;
	$UserInput = [Console]::ReadKey($true);
	if( $UserInput.Key.ToString().ToUpper() -eq "Q" )
	{
		return
	}
}

function CopyFolder
{
    param
    (
        [string] $source,
        [string] $destination,
        [string] $backup,
        [string[]] $Exclude
    )

    Write-Host "Copying files from:"
    Write-Host "  $source"
    Write-Host "to:"
    Write-Host "  $destination"
    Write-Host "excluding:"
    Write-Host "  $Exclude"
	if( !$SkipBackup )
	{
	    Write-Host "with backups in:"
	    Write-Host "  $backup"
	    Write-Host

	    if (!(Test-Path -path $destination)) { New-Item $destination -ItemType Directory -Force | Out-Null }
	    if (!(Test-Path -path $backup)) { New-Item $backup -ItemType Directory -Force | Out-Null }

	    $source = $source.TrimEnd("\")

	    $backupSource = $destination.TrimEnd("\")
	    if( !$backupSource.EndsWith("\*") ) { $backupSource = $backupSource + "\*" }

	    copy-item $backupSource $backup -Force -Recurse
	}
	else
	{
		Write-Host
		Write-Host "Backup skipped" -ForegroundColor Red;
	}

    # Copy all source files except config files, which will be handled later.
    # Copy-Item -Recurse -Exclude doesn't behave as expected (-Exclude only applies
    # to top-level files, not files in subfolders); work around this by using
    # Get-ChildItem, which does work correctly with -Recurse and -Exclude.
    Get-ChildItem "$source\*" -Recurse -File -Exclude $Exclude | Copy-Item -Destination {
        $DestinationPath = Join-Path $destination $_.FullName.Substring($source.Length)
        $DestinationDirectory = Split-Path -Parent $DestinationPath
        if (!(Test-Path $DestinationDirectory)) {
            New-Item $DestinationDirectory -ItemType Directory -Force | Out-Null
        }
        $DestinationPath
    }
}

function ProcessConfigFile
{
    param
    (
        [string] $OemFolder,
        [string] $TargetFolder,
        [string] $BackupFolder,
        [string] $FileName
    )

    $OemExtension = ".oem"
    $OemPath = Join-Path $OemFolder "$FileName$OemExtension"
    $TargetPath = Join-Path $TargetFolder $FileName

	if (!(test-path $OemPath)) {
		write-host "Configuration File '$OemPath' does not exist. Moving on..."
		return
	}
	
    Write-Host "Processing File: $TargetPath"
    $configMgr.ProcessConfigVariables($OemPath, $TargetPath)

    # Compare to backup
    Write-Host -NoNewline "Checking for changes:  "
    $rslt = $configMgr.DetectConfigChanges($TargetFolder, $BackupFolder, $FileName)
    Write-Host $rslt
}

function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        if ($service.CanStop)
        {
            Write-Host "Stopping" $svcName "..."
            Stop-Service $service
        }
    }
}

function InstallServiceIfNotExists
{
    param
    (
        [string] $svcName,
        [string] $svcPath,
        [string] $displayName,
        [string] $user = "",
        [string] $password = ""
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -le 0)
    {
        Write-Host "Creating Service:" $svcName "..."

        # if password is empty, create a dummy one to allow having credentials for system accounts:
        #NT AUTHORITY\LOCAL SERVICE
        #NT AUTHORITY\NETWORK SERVICE
        if ($user -ne "")
        {
            $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
            $creds = New-Object System.Management.Automation.PSCredential ($user, $secpasswd)

            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -Credential $creds -StartupType Automatic
        }
        else
        {
            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -StartupType Automatic
        }
        return $true
    }
    else
    {
        Write-Host "Already Installed: " $svcName "-" $displayName
        return $true
    }
}


#  copy the mdtemplate used by configurator
function CopyMdTemplate($Source, $Target)
{
    write-host "getting ready to copy $Source" 
    #check to make sure something is actually there to copy.
    if (!(Test-Path $Source))
    {
        write-host "$Source does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $Source to $Target"
    Copy-Item -Path $Source -Destination $Target
}

function CopyScript($Source, $Target)
{
     $SourceWebTemplatePath = $Source
    write-host "getting ready to copy" $Source 
	#check to make sure the target folder exists if not create it
	if (!(Test-Path $Target))
	{
	    Write-Host "$Target does not exist need to create it"
        New-Item -ItemType Directory -Force -Path $Target | Out-Null
	}
    #check to make sure something is actually there to copy.
    if (!(Test-Path $SourceWebTemplatePath))
    {
        write-host "$SourceWebTemplatePath does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $SourceWebTemplatePath to $Target"
    Copy-Item -Path $SourceWebTemplatePath -Destination $Target
}



#  **********************************************************
#  ************** RECHUB CLIENT-FIT DEPLOYMENT **************
#  **********************************************************
Write-Host "Getting Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
$configMgr.GetParams(!$Silent, "FIT");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Deployment Cancelled"
}


# ================== Setup Paths ==================
Write-Host "Setting up paths"
$drive = $configMgr.GetParamValue("InstallDrive") + "\"

Write-Host "Initiating Rollup"
# Get full path to the script:
$ScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RecHub Rollup.ps1"))
# Execute script at location:
&"$ScriptRoute" -Drive $drive -Tier CLIENT-FIT
Write-Host "Resume setting up paths"

if (!(test-path ".\CLIENT-FIT\$Version\")) {
	write-host "Root directory '.\CLIENT-FIT\$Version\' does not exist. Nothing to install."
	return
}

$srcRecHubDir = Resolve-Path (Join-Path $StagingPath "CLIENT-FIT\$Version\")
$srcRecHubBinDir = Join-Path $srcRecHubDir "bin"
$srcRecHubBin2Dir = Join-Path $srcRecHubDir "bin2"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destRecHubBinDir = Join-Path $destRecHubDir "bin"
$destRecHubBin2Dir = Join-Path $destRecHubDir "bin2"
$backupTimestamp = get-date -f yyyy-MM-dd_HH_mm_ss
$backupPath = Join-Path (Join-Path (Join-Path (Resolve-Path ".") "Backups") $backupTimestamp) "RecHub"
$backupPathBin = Join-Path $backupPath "bin"
$backupPathBin2 = Join-Path $backupPath "bin2"

Write-Host "Deploying RecHub from:" $srcRecHubDir
Write-Host "To area:" $destRecHubDir
Write-Host

if( !$SkipBackup )
{
	Write-Host "Backup of current files will be placed under:" $backupPath
	Write-Host
}

write-host "Ensuring all files are marked non read-only..."

write-host "Checking destRecHubBinDir $destRecHubBinDir folder"
if ((Test-Path $destRecHubBinDir))
	{
	    Write-Host "altering Read only flag for $destRecHubBinDir"
        get-childitem $destRecHubBinDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }
write-host "Checking destRecHubBin2Dir $destRecHubBin2Dir folder"
if ((Test-Path $destRecHubBin2Dir))
	{
	    Write-Host "altering Read only flag for $destRecHubBin2Dir"
        get-childitem $destRecHubBin2Dir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }

# ================== Clean up obsolete staging files =============
$ObsoleteStagingDirs = @(
    Join-Path $srcRecHubBinDir "..\ICONXClient"
)
$ObsoleteStagingDirs | ForEach-Object {
    if (Test-Path $_) {
        # Collapse any ".."s in the path for display.
        # The path has to exist, otherwise Resolve-Path will fail.
        $Path = Resolve-Path $_
        Write-Host "Cleaning up obsolete files: $Path"
        [System.IO.Directory]::Delete($Path, $True)
    }
}
#=================== template to allow configurator to update config files ==============================
CopyMdTemplate (Join-Path $srcRecHubDir "R360FIT.mdtemplate") $destRecHubDir
CopyMdTemplate (Join-Path $srcRecHubDir "R360FITConsole.mdtemplate") $destRecHubDir

$ServiceScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "FitClientRestartServices.ps1"))
CopyScript $ServiceScriptRoute $destRecHubDir

# ================== Define Services =============
$serviceList = @(,
    @("FileImportClientSvc",    (Join-Path $destRecHubBinDir "FileImportClientSvc.exe"),  "WFS File Import Client Service", "", "", "")
)

# ================== Stop Service if applicable =============
if (!$NoService) {
    Write-Host "Stopping Services..."
    $serviceList | ForEach-Object { StopServiceIfExists $_[0] }
}

#  ***************** X-Copy Files **************
CopyFolder $srcRecHubBinDir $destRecHubBinDir $backupPathBin -Exclude *.config,*.config.oem,*.ini
CopyFolder $srcRecHubBin2Dir $destRecHubBin2Dir $backupPathBin2 -Exclude *.config,*.ini

#  ================= Create Log Folder ==============
$drive = $configMgr.GetParamValue("LogDrive") + "\"
$destLogDir = Join-Path $drive "WFSApps\Logs\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

$destLogDir = Join-Path $drive "WFSApps\RecHub\Data\Logfiles\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#  ***************** Update Configs *************
if (!$Silent) {
    Write-Host "Updating Configuration Files..."
    ProcessConfigFile -OemFolder $srcRecHubBin2Dir -TargetFolder $destRecHubBin2Dir -BackupFolder $backupPathBin2 -FileName "FileImportClient.ini"
}

# ================== Install / Start Services if applicable =============
if (!$NoService) {
    Write-Host "Installing / Starting Services..."
    $serviceList | ForEach-Object {
        if ($_[1] -ne "")
        {
            InstallServiceIfNotExists $_[0] $_[1] $_[2] $_[3] $_[4]
        }
    }
}

Write-Host
Write-Host "-> Deployment Complete <-"
if( $SkipBackup -and $Silent )
{
	Write-Host "*** Backup skipped, remember to backup files manually ***" -ForegroundColor Red -BackgroundColor White;
}
