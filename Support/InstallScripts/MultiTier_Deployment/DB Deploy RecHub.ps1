param(
	#Version of SQL the install is going against
	#Valid values: 2012, 2016
	$SQLVersion="2016",
	$NestedCall
)

$ErrorActionPreference = "Stop"

# To leave powershell window open, restart PowerShell with -noexit, the same script, and 1
if (!$NestedCall) {
    powershell -noexit -file $MyInvocation.MyCommand.Path 1 -SQLVersion $SQLVersion
    return
}

function Process-Parameters
{
    param
    (
        [string] $sourceFileName,
        [string] $targetFileName,
		[string] $aliases   
    )
    
    Write-Host "Processing File: " $sourceFileName "-> into: " $targetFileName
    $configMgr.ProcessDBVariables($sourceFileName, $targetFileName, $aliases)
}

function Prompt-Install
{
    $title = "Confirm Install:"   
    $message = "Install " + $CurrentCommandFile + "?"
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Install it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Skip it."
    $skip = New-Object System.Management.Automation.Host.ChoiceDescription "&Skip", "Skip it."
    $abort = New-Object System.Management.Automation.Host.ChoiceDescription "&Abort", "Abort all further processing."
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no, $skip, $abort)
    $result = $host.ui.PromptForChoice($title, $message, $options, 0)
    
    switch ($result)
    {
        0 { return $true }
        1 { return $false }
        2 { return $false }
        3 {
            $ErrorActionPreference = "Stop"
            Write-Host "Processing Aborted!"
            Throw "Processing Aborted"
        }
    }
}

function Install-WithPartitioning
{
    if (Prompt-Install)
    {
        $path = Join-Path $InstallFolder "InstallPatch.ps1"
        & "$path" -DBServer "$DBServer" -DBName "$DBName" -LogFolder "$LogFolder" -InstallationFolder "$CurrentInstallFolder" -CommandFile "$CurrentCommandFile" -Partitioned $Partitioned -PartitionStartDate "$PartitionStartDate" -PartitionYears $PartitionYears -PartitionType $PartitionType
    }
}

function Install-Normal
{
    if (Prompt-Install)
    {
        $path = Join-Path $InstallFolder "InstallPatch.ps1"
        & "$path" -DBServer "$DBServer" -DBName "$DBName" -LogFolder "$LogFolder" -InstallationFolder "$CurrentInstallFolder" -CommandFile "$CurrentCommandFile" -Partitioned 0
    }
}

function GetInstallApp
{
	$InstallDrive = [System.IO.Path]::GetPathRoot($InstallFolder)
	$InstallAppFolder = Join-Path -Path $InstallDrive -ChildPath "WFSApps\Utilities\WFSDatabaseInstall"
	# Find our installer location
	if ( $SQLVersion -eq "2016" )
	{
		$Installer = "$InstallAppFolder\WFSDatabaseInstallConsole2016"
	}
	else {
		$Installer = "$InstallAppFolder\WFSDatabaseInstallConsole"
	}
	return $Installer;
}

function InstallApp-WithPartitioning
{
    if (Prompt-Install)
    {
        $path = GetInstallApp;
        & "$path" -DBServer "$DBServer" -DBName "$DBName" -LogFolder "$LogFolder" -InstallationFolder "$CurrentInstallFolder" -CommandFile "$CurrentCommandFile" -Partitioned $Partitioned -PartitionStartDate "$PartitionStartDate" -PartitionYears $PartitionYears -PartitionType $PartitionType
    }
}

function InstallApp-Normal
{
    if (Prompt-Install)
    {
        $path = GetInstallApp;
        & "$path" -DBServer "$DBServer" -DBName "$DBName" -LogFolder "$LogFolder" -InstallationFolder "$CurrentInstallFolder" -CommandFile "$CurrentCommandFile" -Partitioned 0
    }
}


function GetAlias
{
    param ([string]$aliasName, [string] $aliases)
    
    [string]$pattern = ($aliasName + "\s*=\s*(\w*)")
    if($aliases -match $pattern)
    {    
        return $matches[1]
    }
    else
    {
        return $null
    }
}


function BuildFailedFolderPrompt
{
    param ([string]$dbName, [string[]]$failedFolders)
    
    [string]$failedFolderPrompt = "The folder"
    if($failedFolders.Count -gt 1)
    {
        $failedFolderPrompt += "s"
    }
    
    for($i = 0; $i -lt $failedFolders.Count; $i++)
    {
        if($i -eq 0)
        {
            $failedFolderPrompt += " "
        }
        else
        {
            if( $i -eq $failedFolders.Count - 1)
            {
                $failedFolderPrompt += " and "
            }
            else
            {
                $failedFolderPrompt += ", "
            }
        }
        $failedFolderPrompt += ("""" + $failedFolders[$i] + """")
    }
    
    $failedFolderPrompt += (" could not be created.  This can cause the creation of database """ + $DBName + """ to fail.")
    
    return $failedFolderPrompt
}


function Prompt-ContinueDbInstall
{
    param( [string]$title )
    
    $message = "Would you like to continue with the installation of this database?"
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Install it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Skip it."
    $abort = New-Object System.Management.Automation.Host.ChoiceDescription "&Abort", "Abort all further processing."
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no, $abort)
    $result = $host.ui.PromptForChoice($title, $message, $options, 0)
    
    switch ($result)
    {
        0 { return $true }
        1 { return $false }
        2 {
            $ErrorActionPreference = "Stop"
            Write-Host "Processing Aborted!"
            Throw "Processing Aborted"
        }
    }
}


function CreatePartitionFolders
{
	param ( $filePath )

    [xml] $partitionConfigurationXml = Get-Content $filePath
    $partitionPaths = $partitionConfigurationXml.StaticData.Data.DiskPath

    [string[]]$failedFolders = @()

    foreach( $partitionPath in $partitionPaths)
    {
        if(-not (Test-Path -path $partitionPath))
        {
            New-Item $partitionPath -itemType directory -force -errorAction SilentlyContinue | Out-Null
            if(-not (Test-Path -path $partitionPath))
            {
				$failedFolders += $partitionPath
            }
        }
    }

    return ($failedFolders)
}


function CreateDatabase
{
	param ( $aliases )

	$srcScript = Join-Path $installRoot "CreateDatabase-Generic.sql"
	$local:ScriptName = "CreateDatabase-$DBName"
	$destScript = Join-Path $InstallFolder ($local:ScriptName + ".sql")
	$local:ResultPath = Join-Path $LogFolder ("Results_" + (Get-Date -f yyyyMMddHHmmss));

	$CommandLine = New-Object System.Text.StringBuilder; 
	[Void]$CommandLine.Append("SQLCMD ");
	[Void]$CommandLine.Append("-S $DBServer ");
	[Void]$CommandLine.Append("-d master ");
	[Void]$CommandLine.Append("-E "); # Windows Authentication
	[Void]$CommandLine.Append("-i ");
	[Void]$CommandLine.Append("""$destScript""");
	[Void]$CommandLine.Append(" ");
	[Void]$CommandLine.Append("-r 1 -V 10 ");

	$local:LogFileName = Join-Path $local:ResultPath ($local:ScriptName + "_Results.txt");
	[Void]$CommandLine.Append("-o ");
	[void]$CommandLine.Append("""$local:LogFileName""");
	
	$CurrentCommandFile = "-> Create Database: " + $DBName
	if (Prompt-Install)
	{
		Process-Parameters $srcScript $destScript $aliases

		Write-Host "Creating / Verifying Log folder: " $local:ResultPath
		if (!(Test-Path -path $local:ResultPath)) { New-Item $local:ResultPath -itemtype directory -force | Out-Null } 

		[string[]]$failedFolders = @()

        $failedFolders = CreatePartitionFolders $PartitionFilePath

        [string]$sqlPathAlias = GetAlias "SQL_DATA_PATH" $aliases
        [string]$sqlPath = $configMgr.GetDBParamValue($sqlPathAlias)

		# Note, this will fail if the installation script is run from a remote system instead of the DB server
        if(-not (Test-Path -path $sqlPath))
        {
            New-Item $sqlPath -itemType directory -force -errorAction SilentlyContinue | Out-Null
            if(-not (Test-Path -path $sqlPath))
            {
				$failedFolders += $sqlPath
            }
        }

        $sqlPathAlias = GetAlias "SQL_LOG_PATH" $aliases
        $sqlPath = $configMgr.GetDBParamValue($sqlPathAlias)
        if(-not (Test-Path -path $sqlPath))
        {
            New-Item $sqlPath -itemType directory -force -errorAction SilentlyContinue | Out-Null
            if(-not (Test-Path -path $sqlPath) -and (-not ($failedFolders -Contains $sqlPath)))
            {
				$failedFolders += $sqlPath
            }
        }

        [bool]$continueDbInstall = $true
		if($failedFolders.Count -gt 0)
		{
			$failedFolderPrompt = BuildFailedFolderPrompt -dbName $DBName -failedFolders $failedFolders
            Write-Host $failedFolderPrompt -ForegroundColor yellow
            $continueDbInstall = Prompt-ContinueDbInstall
		}

		if($continueDbInstall)
        {
    		Write-Host $CommandLine;
	    	Write-Host "";

    		cmd /c $CommandLine;
	    	#return $?;
        }
	}
}

function WritePartitionConfiguration
{
    param($filePath)

	#Create Backup file, if applicable
	$backupPath = $filePath + ".bak"
	if (!(Test-Path $backupPath))
	{
		Copy-Item $filePath $backupPath
	}

	#Have config manager create the appropriate configuration
	$configMgr.CreatePartitionDisksXml($filePath)
}

#  ************************************************   
#  ************** RECHUB DB DEPLOYMENT ***************   
#  ************************************************   
Write-Host "Prompting for Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
Write-Host "Verifying Parameters..."
$configMgr.GetDBParams($True);
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host
    
    #Stop All Further processing
    Throw "Deployment Cancelled"
}

# ================== Setup Paths / Parameters ==================
$installRoot = $configMgr.GetDBParamValue("INSTALL_ROOT")
#$InstallFolder =  Join-Path $installRoot "RecHub2.03.04\"
$InstallFolder =  Join-Path $installRoot "RecHub2.1\"
$DBServer = $configMgr.GetDBParamValue("DB_SERVER")
$Partitioned = [System.Convert]::ToInt32($configMgr.GetDBParamValue("PARTITIONED"))
$PartitionStartDate = $configMgr.GetDBParamValue("PARTITION_START_DATE")
$PartitionYears = $configMgr.GetDBParamValue("PARTITION_YEARS")
$PartitionType = $configMgr.GetDBParamValue("PARTITION_TYPE")

#  ================= Create Log Folder ==============
$LogFolder = $configMgr.GetDBParamValue("LOG_FOLDER")
Write-Host "Creating / Verifying Log folder: " $LogFolder
if (!(Test-Path -path $LogFolder)) { New-Item $LogFolder -itemtype directory -force } 

# ================== RecHub Database ==================
Write-Host
Write-Host -ForegroundColor Magenta "RecHub Database"

$DBName = $configMgr.GetDBParamValue("RECHUB_DB_NAME")
$CurrentInstallFolder = Join-Path $InstallFolder "2.00.00.00"
$CurrentCommandFile = "RecHub2.00.00.00.xml"
$PartitionFilePath = Join-Path $CurrentInstallFolder "RecHubSystem\PartitionDisks.xml"
WritePartitionConfiguration $PartitionFilePath

CreateDatabase "DB_NAME=RECHUB_DB_NAME;SQL_DATA_PATH=RECHUB_SQL_DATA_PATH;SQL_LOG_PATH=RECHUB_SQL_LOG_PATH"

Install-WithPartitioning

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.01.00"
$CurrentCommandFile = "RecHub2.00.01.00Patch.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.02.00"
$CurrentCommandFile = "RecHub2.00.02.00Patch.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.03.00"
$CurrentCommandFile = "RecHub2.00.03.00Patch.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.04.00"
$CurrentCommandFile = "RecHub2.00.04.00Patch.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "RecHub2.01.00.00Patch.xml"
InstallApp-WithPartitioning

# ================== SSIS Packages ================================
Write-Host
Write-Host -ForegroundColor Magenta "RecHub SSIS"

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "RecHub2.01.00.00SSISPatch.xml"
InstallApp-WithPartitioning

# ================== SSIS Configuration Database ==================
Write-Host
Write-Host -ForegroundColor Magenta "RecHub SSIS Configuration Database"
$DBName = $configMgr.GetDBParamValue("SSIS_CONFIG_DB_NAME")
CreateDatabase "DB_NAME=SSIS_CONFIG_DB_NAME;SQL_DATA_PATH=SSIS_CONFIG_SQL_DATA_PATH;SQL_LOG_PATH=SSIS_CONFIG_SQL_LOG_PATH"

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.00.00\SSIS_Configuration"
$CurrentCommandFile = "SSISConfigurationDatabase2.00.00.00.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.01.00"
$CurrentCommandFile = "SSISConfigurationDatabase2.00.01.00Patch.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "SSISConfigurationDatabase2.01.00.00Patch.xml"
InstallApp-WithPartitioning

# ================== SSIS Logging Database ==================
Write-Host
Write-Host -ForegroundColor Magenta "RecHub SSIS Logging Database"
$DBName = $configMgr.GetDBParamValue("SSIS_LOG_DB_NAME")
CreateDatabase "DB_NAME=SSIS_LOG_DB_NAME;SQL_DATA_PATH=SSIS_LOG_SQL_DATA_PATH;SQL_LOG_PATH=SSIS_LOG_SQL_LOG_PATH"

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.00.00"
$CurrentCommandFile = "SSISLoggingDatabase2.00.00.00.xml"
Install-Normal

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "SSISLoggingDatabase2.01.00.00Patch.xml"
InstallApp-Normal

# ================== RAAM Static Data ==================
Write-Host
Write-Host -ForegroundColor Magenta "RAAM Database (R360 Static Data)"
$DBName = $configMgr.GetDBParamValue("RAAM_DB_NAME")

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "RAAMRecHub2.01.00.00Patch.xml"
InstallApp-Normal

Write-Host
Write-Host "-> Deployment Complete <-"
Write-Host
