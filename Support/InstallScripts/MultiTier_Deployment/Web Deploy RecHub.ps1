﻿param (
    [switch] $NestedCall,
    [switch] $Silent,
    # Don't perform any IIS configuration. Intended for testing only.
    [switch] $NoIis,
    # Don't perform any Windows service configuration. Intended for testing only.
    [switch] $NoService,
	# Don't backup before copying files
    [Switch] $SkipBackup
)

# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
[string] $Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)c"

$ErrorActionPreference = "Stop"

if (!$NestedCall) {
    # Start a new instance of PowerShell, so the DLL gets unloaded at the end
    $ExtraArgs = @()
    if ($Silent) {
        $ExtraArgs += "-Silent"
    }
    if ($NoIis) {
        $ExtraArgs += "-NoIis"
    }
    if ($NoService) {
        $ExtraArgs += "-NoService"
    }
    if ($SkipBackup) {
        $ExtraArgs += "-SkipBackup"
    }
    powershell -file $MyInvocation.MyCommand.Path -NestedCall @ExtraArgs
    if (!$Silent) {
        Write-Host "Press ENTER to exit: " -NoNewline
        Read-Host
    }
    return
}

if( $SkipBackup -and $Silent )
{
	Write-Host "Skipping backup, remember to backup files manually" -ForegroundColor Red -BackgroundColor White;
}
elseif( $SkipBackup )
{
	Write-Host "Skipping backup, remember to backup files manually. [Q] to quit, any other key to continue." -ForegroundColor Red -BackgroundColor White;
	$UserInput = [Console]::ReadKey($true);
	if( $UserInput.Key.ToString().ToUpper() -eq "Q" )
	{
		return
	}
}

function Get-FilesWithRelativePaths {
    param (
        $Directory,
        $Include = "*"
    )

	if (!(test-path $Directory)) {
		write-host "Directory '$Directory' does not exist. Moving on..."
		return
	}
	
    Get-ChildItem $Directory -Recurse -File -Include $Include | ForEach-Object {
        # Add the RelativePath property
        $RelativePath = $_.FullName.Substring($Directory.Length).TrimStart("\")
        Add-Member -InputObject $_ -MemberType NoteProperty -Name RelativePath -Value $RelativePath
        $_
    }
}

function CopyFolder
{
    param
    (
        [string] $source,
        [string] $destination,
        [string] $backup,
        # Predicate to decide whether each file should be included. Receives a FileInfo with an
        # extra RelativePath property; returns truthy or falsy.
        [ScriptBlock] $IncludeIf = { $True },
        # Subdirectories to delete in the destination, after backing up but before copying new files.
        [string[]] $SubdirectoriesToClean = @()
    )

    Write-Host "Copying files from:"
    Write-Host "  $source"
    Write-Host "to:"
    Write-Host "  $destination"
    if ($SubdirectoriesToClean) {
        Write-Host "cleaning: $SubdirectoriesToClean"
    }
	if( !$SkipBackup )
	{
	    Write-Host "with backups in:"
	    Write-Host "  $backup"
	    Write-Host

	    # Ensure the destination and backup directories exist
	    if (!(Test-Path -path $destination)) { New-Item $destination -ItemType Directory -Force | Out-Null }
	    if (!(Test-Path -path $backup)) { New-Item $backup -ItemType Directory -Force | Out-Null }

	    # Back up
	    $backupSource = $destination.TrimEnd("\")
	    if (!$backupSource.EndsWith("\*")) { $backupSource = $backupSource + "\*" }
	    Copy-Item $backupSource $backup -Force -Recurse -Exclude "Logs"
	}
	else
	{
		Write-Host
		Write-Host "Backup skipped" -ForegroundColor Red;
	}
	
    # Clean subdirectories
    $SubdirectoriesToClean | ForEach-Object {
        $CleanDirectory = Join-Path $destination $_
        if (Test-Path $CleanDirectory -PathType Container) {
            [System.IO.Directory]::Delete($CleanDirectory, $True)
        }
    }

    # Copy files
    $source = $source.TrimEnd("\")
    Get-FilesWithRelativePaths $source | Where-Object {
        # Filter out files we never want to copy
        $_.Name -ne "Thumbs.db"
    } | Where-Object {
        # Call IncludeIf to decide whether to copy the file or not
        & $IncludeIf $_
    } | Copy-Item -Destination {
        $DestinationPath = Join-Path $destination $_.RelativePath
        $DestinationDirectory = Split-Path -Parent $DestinationPath
        if (!(Test-Path $DestinationDirectory)) {
            New-Item $DestinationDirectory -ItemType Directory -Force | Out-Null
        }
        $DestinationPath
    }
}

function CopyWebApp($WebAppName, $SourceWebRoot, $TargetWebRoot, $BackupWebRoot)
{
    $SourceWebAppPath = Join-Path $SourceWebRoot $WebAppName
    $TargetWebAppPath = Join-Path $TargetWebRoot $WebAppName
    $BackupWebAppPath = Join-Path $BackupWebRoot $WebAppName

    if (!(test-path $SourceWebAppPath)) {
        write-host "SourceWebRoot '$SourceWebAppPath' does not exist. Moving on..."
        return
    }

    CopyFolder $SourceWebAppPath $TargetWebAppPath $BackupWebAppPath -IncludeIf {
        param ([System.IO.FileInfo] $File)
        $File.Extension -notin @(".oem", ".config") -or $File.RelativePath -eq "Views\Web.config"
    } -SubdirectoriesToClean "bin"
}

function CopyMdTemplate($Source, $Target)
{
     $SourceWebTemplatePath = Join-Path $Source "RecHub.mdtemplate"
    write-host "getting ready to copy RecHub.Mdtemplate" 
    #check to make sure the target folder exists if not create it
	if (!(Test-Path $Target))
	{
	    Write-Host "$Target does not exist need to create it"
        New-Item -ItemType Directory -Force -Path $Target | Out-Null
	}
    #check to make sure something is actually there to copy.
    if (!(Test-Path $SourceWebTemplatePath))
    {
        write-host "$SourceWebTemplatePath does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $SourceWebTemplatePath to $Target"
    Copy-Item -Path $SourceWebTemplatePath -Destination $Target
}

function BackUpAndRemoveObsoleteFolder
{
    param
    (
        [string] $local:source,
        [string] $local:dest,
        [string] $local:backup
    )

    $local:source = $local:source.TrimEnd("\");
    if( !$local:source.EndsWith("\*") ) { $local:source = $local:source.TrimEnd("\*") }
    $local:dest = $local:dest.TrimEnd("\");
    if( !$local:dest.EndsWith("\*") ) { $local:dest = $local:dest.TrimEnd("\*") }

	if( !$SkipBackup )
	{
	    if (!(Test-Path -path $local:backup)) { New-Item $local:backup -ItemType Directory -Force | Out-Null }
	    copy-item "$local:dest\*" $local:backup -Force -Recurse
	}
	else
	{
		Write-Host "Backup skipped" -ForegroundColor Red;
	}
    RemoveFolders $local:source, $local:dest;
}

function RemoveFolders
{
    param
    (
        [string[]] $local:folders
    )
    $local:folders|% {
        RemoveFolder $_;
    };
}

function RemoveFolder
{
    param
    (
        [string] $local:folder
    )
    if (Test-Path $local:folder)
    {
        Get-ChildItem (Join-Path $local:folder *)|? {
                $_.PSIsContainer
            }|% {
                RemoveFolder $_.FullName;
        };
        Get-ChildItem (Join-Path $local:folder *)|? {
                ! $_.PSIsContainer
            }|% {
                try
                {
                    $_.Delete();
                }
                catch
                {
                    Write-Host "Error - Unable to delete: $($_.Exception.Message)";
                }
        };
        try
        {
            Get-Item -Path $local:folder|%{$_.Delete()};
        }
        catch
        {
            Write-Host "Error - Unable to delete: $local:folder";
        }
    }
}

function CreateWebApp
{
    param
    (
        [string] $iisSite,
        [string] $iisAppPoolName,
        [string] $sourcePath,
        [string] $basePath,
        [string] $appFolder
    )

    Write-Host "Web Application: " $iisSite " - " $appFolder
    cd 'IIS:\Sites'
    cd $iisSite
    
    $siteName = $appFolder

    if ($siteName -eq 'R360UI') { ## special handling of the R360UI site
        $siteName = "UI"
        
        if (Test-Path 'R360UI' -pathType container) { ## see if the site is there, and if it is, rename it
            Rename-Item 'R360UI' $siteName
            Write-Host "Renamed 'R360UI' site to '$siteName'"
        }
    }
    
    if (!(Test-Path $siteName -pathType container))
    {
        Write-Host "Creating Web Application: " $siteName
        $fullSourcePath = Join-Path $sourcePath $appFolder
        $webConfig = Join-Path $fullSourcePath "web.config.oem"

        $fullPath = Join-Path $basePath $appFolder

        $app = New-Item $siteName -physicalPath $fullPath -type Application
        Set-ItemProperty $siteName -Name "applicationPool" -Value $iisAppPoolName
    }
}

function RemoveItemFromIISForRecHub
{
    param
    (
        [string[]] $appNames
    )

    $appNames|% {
        $local:appName = $_;
        if(Get-WebApplication -Site R360 -Name $local:appName)
        {
            Remove-WebApplication -Site R360 -Name $local:appName;
        }
    }
}

function ConfigureIISForRecHub
{
    param
    (
        [string] $sourceDir,
        [string] $destRecHubDir,
        $appFolders
    )

    #  ***************** Setup IIS *******************
    $iisAppPoolName = "WFS-RecHub"
    $iisAppPoolDotNetVersion = "v4.0"
    $iisSiteName = "R360"

    Write-Host "Beginning IIS Configuration..."

    #navigate to the app pools root
    Push-Location
    cd IIS:\AppPools\

    #check if the app pool exists
    if (!(Test-Path $iisAppPoolName -pathType container))
    {
        #create the app pool
        Write-Host "Creating App Pool: " $iisAppPoolName
        $appPool = New-Item $iisAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion
        $appPool | Set-ItemProperty -Name "processModel.identityType" -value 2 # 2 = NETWORK SERVICE
    }

    #navigate to the sites root
    cd IIS:\Sites\

    #check if the site exists
    if (!(Test-Path $iisSiteName -pathType container))
    {
        Write-Host "Error - Site was not found: " $iisSiteName

        #Stop deployment - we can't find the website...
        #Stop All Further processing
        Throw "Deployment Cancelled"
    }

    #Create Web Applications
    Write-Host "Creating Web Applications..."
    $appFolders | ?{ CreateWebApp $iisSiteName $iisAppPoolName $sourceDir $destRecHubDir $_ }

    # Default .NET Error Pages
    Write-Host "Configuring custom errors..."
    
    Set-Location IIS:\Sites\$iisSiteName
    Set-WebConfigurationProperty -Filter "/system.web/customErrors" -Name defaultredirect -Value "~/UI/Error.html"
    Set-WebConfigurationProperty -Filter "/system.web/customErrors" -Name redirectmode -Value "ResponseRedirect"
    Set-WebConfigurationProperty -Filter "/system.web/customErrors" -Name mode -Value "On"

    #Restore current directory
    Pop-Location
}

function ProcessConfigFile
{
    param
    (
        [string] $OemFolder,
        [string] $TargetFolder,
        [string] $FileName
    )
	
    $OemExtension = if ($FileName -like "*RecHub.ini") { ".web" } else { ".oem" }
    $OemPath = Join-Path $OemFolder "$FileName$OemExtension"
    $TargetPath = Join-Path $TargetFolder $FileName

	if (!(test-path $OemPath)) {
		write-host "Configuration File '$OemPath' does not exist. Moving on..."
		return
	}
	
    Write-Host "Processing File: $TargetPath"
    $configMgr.ProcessConfigVariables($OemPath, $TargetPath)
}

function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        if ($service.CanStop)
        {
            Write-Host "Stopping" $svcName "..."
            Stop-Service $service
        }
    }
}

function InstallServiceIfNotExists
{
    param
    (
        [string] $svcName,
        [string] $svcPath,
        [string] $displayName,
        [string] $user = "",
        [string] $password = ""
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -le 0)
    {
        Write-Host "Creating Service:" $svcName "..."

        # if password is empty, create a dummy one to allow having credentials for system accounts:
        #NT AUTHORITY\LOCAL SERVICE
        #NT AUTHORITY\NETWORK SERVICE
        if ($user -ne "")
        {
            $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
            $creds = New-Object System.Management.Automation.PSCredential ($user, $secpasswd)

            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -Credential $creds -StartupType Automatic
        }
        else
        {
            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -StartupType Automatic
        }
        return $true
    }
    else
    {
        Write-Host "Already Installed: " $svcName "-" $displayName
        return $true
    }
}

function StartServices
{
    param
    (
        $serviceList
    )

    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName -ErrorAction SilentlyContinue
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)
}

function EnableSSLForPort
{
    param
    (
        [string] $port,
        [string] $sslCertThumbprint
    )

    if ($sslCertThumbprint.Length -gt 0)
    {
        # First, check to see if SSL is already configured
        $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
        if ($result.IndexOf($sslCertThumbprint, [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
        {
            Write-Host "SSL Binding already Configured"
        }
        else
        {
            & netsh http add sslcert ipport=0.0.0.0:$port certhash=$sslCertThumbprint "appid={00112233-0042-0042-0042-AABBCCDDEEFF}"
        }
    }
}

function Remove-Bundles([string] $local:Path)
{
	$local:DeleteBundles = @("data-table.","fontawesome-webfont.","glyphicons-halflings-regular.","inline.","main.","map-hue.","map-saturation-overlay.","polyfills.","scripts.","styles.","vendor.");
	
	foreach( $local:DeleteBundle in $local:DeleteBundles )
	{
		$local:Item = Join-Path -Path $local:Path -ChildPath $local:DeleteBundle;
		Write-Host "Removing bundle files: " $local:Item;
		Remove-Item ($local:Item + "*") -Force;
	}
}

#  ***************************************************
#  ************** RECHUB WEB DEPLOYMENT **************
#  ***************************************************
if (!$NoIis) {
    Import-Module WebAdministration
}

Write-Host "Getting Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
$configMgr.GetParams(!$Silent, "WEB");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Deployment Cancelled"
}

# ================== Setup Paths ==================
Write-Host "Setting up paths"
$drive = $configMgr.GetParamValue("InstallDrive") + "\"

Write-Host "Initiating Rollup"
#& "RecHub Rollup.ps1" -Drive $drive -Tier Web
# Get full path to the script:
$ScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RecHub Rollup.ps1"))

# Execute script at location:
&"$ScriptRoute" -Drive $drive -Tier Web

$srcRecHubDir = Resolve-Path ".\WEB\$Version\"
$srcRecHubIISDir = Join-Path $srcRecHubDir "wwwroot"
$srcRecHubBinDir = Join-Path $srcRecHubDir "bin2"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destRecHubIISDir = Join-Path $destRecHubDir "wwwroot"
$destRecHubBinDir = Join-Path $destRecHubDir "bin2"
$destFrameworkDir = Join-Path $drive "WFSApps\Framework\"
$destFrameworkWebDir = Join-Path $drive "WFSApps\Framework\Framework.Views\"

$backupTimestamp = get-date -f yyyy-MM-dd_HH_mm_ss
$backupPath = Join-Path (Join-Path (Join-Path (Resolve-Path ".") "Backups") $backupTimestamp) "RecHub"
$backupPathIIS = Join-Path $backupPath "wwwroot"
$backupPathBin = Join-Path $backupPath "bin2"

write-host "Ensuring all files are marked non read-only..."
write-host "Checking destRecHubIISDir $destRecHubIISDir folder"
if ((Test-Path $destRecHubIISDir))
	{
	    Write-Host "altering Read only flag for $destRecHubIISDir"
        get-childitem $destRecHubIISDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }
write-host "Checking destRecHubBinDir $destRecHubBinDir folder"
if ((Test-Path $destRecHubBinDir))
	{
	    Write-Host "altering Read only flag for $destRecHubBinDir"
        get-childitem $destRecHubBinDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }

Write-Host "Deploying RecHub from:" $srcRecHubDir
Write-Host "To area:" $destRecHubDir
Write-Host

Write-Host "Backup of current files will be placed under:" $backupPath
Write-Host

#=================== template to allow configurator to update config files ==============================

CopyMdTemplate $srcRecHubDir $destRecHubDir

# ================== Define Services =============
$serviceList = @(,
    @("FileCleanupSvc",            (Join-Path $destRecHubBinDir "FileCleanupSvc.exe"),         "WFS IPOnline Cleanup Service", "", "", "")
)

#$webAppFolders = get-ChildItem -Path $destRecHubIISDir | ?{ $_.PSIsContainer } | Select-Object -Expand Name
$webAppFolders = "RecHubConfigViews", "RecHubExtractScheduleViews", "RecHubRaamProxy", "RecHubReportingViews", "RecHubViews", "R360UI"
$webAppRemoveList = "IPOnline", "RecHubExceptionsViews"

# ================== Stop Service if applicable =============
if (!$NoService) {
    Write-Host "Stopping Services..."
    ($serviceList+$webAppRemoveList) | ?{ StopServiceIfExists $_[0] }
}

#  **************** Remove Bundles *************
#Before we x-copy the files, remove any old bundle files
if (Test-Path (Join-Path -Path $destRecHubIISDir -ChildPath "R360UI"))
{
    Write-Host "Removing old R360UI bundles"
    Remove-Bundles (Join-Path -Path $destRecHubIISDir -ChildPath "R360UI");
}

#  ***************** X-Copy Files **************
$webAppFolders | ForEach-Object {
    CopyWebApp -WebAppName $_ -SourceWebRoot $srcRecHubIISDir -TargetWebRoot $destRecHubIISDir -BackupWebRoot $backupPathIIS
}
CopyFolder $srcRecHubBinDir $destRecHubBinDir $backupPathBin -Exclude *.config

#There should not be any Web.config.oem files when everything is done. Remove any that are hanging around.
Write-Host "Search folder $destRecHubIISDir for any '.oem'" 
$webAppFolders | ForEach-Object {
    Get-ChildItem (Join-Path -Path (Join-Path $destRecHubIISDir $_) -ChildPath "Web.config.oem") -Recurse | Remove-Item
}

Write-Host "Search folder $destRecHubBinDir for any '.oem'" 

Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "FileCleanupSvc.exe.config.oem") -Recurse | Remove-Item
Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "WCFClients.web.config.oem") -Recurse | Remove-Item


#  ================= Remove Eliminated Items ========
$webAppRemoveList|?{ Test-Path (Join-Path $destRecHubIISDir $_)}|%{
    Write-Host "Removing folder $_";
    BackUpAndRemoveObsoleteFolder (Join-Path $srcRecHubIISDir $_) (Join-Path $destRecHubIISDir $_) (Join-Path $backupPathIIS $_);
}

#  ================= Create Log Folder ==============
$drive = $configMgr.GetParamValue("LogDrive") + "\"
$destLogDir = Join-Path $drive "WFSApps\Logs\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

$destLogDir = Join-Path $drive "WFSApps\RecHub\Data\Logfiles\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#  ***************** Setup IIS **************
if (!$NoIis) {
    ConfigureIISForRecHub $srcRecHubIISDir $destRecHubIISDir $webAppFolders
    RemoveItemFromIISForRecHub $webAppRemoveList
}

#  ***************** Update Configs *************
#  ************************************************************************
#  currently the .ini file is the only thing that may pop up a win merge
#  this needs to be kept in place till all the code to remove the ini is finished.
#  ************************************************************************

if (!$Silent) {
    Write-Host "Updating Configuration Files..."
    $configs = "RecHub.ini"
    $configs | ForEach-Object {
        $config = $_
		
        ProcessConfigFile -OemFolder $srcRecHubBinDir -TargetFolder $destRecHubBinDir -FileName $config
        
        Write-Host -NoNewline "Checking for changes:  "
        $rslt = $configMgr.DetectConfigChanges($destRecHubBinDir, $backupPathBin, $config)
        Write-Host $rslt
      
    }
}

# ================== Install / Start Services if applicable =============
if (!$NoService) {
    Write-Host "Installing / Starting Services, and registering SSL when applicable..."
    $sslCert = $configMgr.GetParamValue("SSL_CERT")
    $serviceList | ForEach-Object {
        if ($_[1] -ne "")
        {
            if ((InstallServiceIfNotExists $_[0] $_[1] $_[2] $_[3] $_[4]) -eq $true)
            {
                if ($_[5] -ne "")
                {
                    Write-Host "(Service requires SSL Port Registration)"
                    EnableSSLForPort $_[5] $sslCert
                }
            }
        }
    }

    StartServices $serviceList
}

Write-Host
Write-Host "-> Deployment Complete <-"
if( $SkipBackup -and $Silent )
{
	Write-Host "*** Backup skipped, remember to backup files manually ***" -ForegroundColor Red -BackgroundColor White;
}
