param($NestedCall)
$ErrorActionPreference = "Stop"

# To leave powershell window open, restart PowerShell with -noexit, the same script, and 1
if (!$NestedCall) {
    powershell -noexit -file $MyInvocation.MyCommand.Path 1
    return
}

function Prompt-Install
{
    $title = "Confirm Install:"   
    $message = "Install " + $CurrentCommandFile + "?"
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Install it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Skip it."
    $skip = New-Object System.Management.Automation.Host.ChoiceDescription "&Skip", "Skip it."
    $abort = New-Object System.Management.Automation.Host.ChoiceDescription "&Abort", "Abort all further processing."
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no, $skip, $abort)
    $result = $host.ui.PromptForChoice($title, $message, $options, 0)
    
    switch ($result)
    {
        0 { return $true }
        1 { return $false }
        2 { return $false }
        3 {
            $ErrorActionPreference = "Stop"
            Write-Host "Processing Aborted!"
            Throw "Processing Aborted"
        }
    }
}

function Install-SSRS
{
    if (Prompt-Install)
    {
        $path = Join-Path $InstallFolder "InstallSSRS.ps1"
        & "$path" -DBServer "$DBServer" -DBName "$DBName" -LogFolder "$LogFolder" -InstallationFolder "$CurrentInstallFolder" -CommandFile "$CurrentCommandFile" -URL "$URL" -SSRSFolder "$SSRSFolder"
    }
}

#  ************************************************   
#  ************** RAAM DB DEPLOYMENT ***************   
#  ************************************************   
Write-Host "Prompting for Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
Write-Host "Verifying Parameters..."
$configMgr.GetDBParams($True);
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host
    
    #Stop All Further processing
    Throw "Deployment Cancelled"
}

# ================== Setup Paths / Parameters ==================
$installRoot = $configMgr.GetDBParamValue("INSTALL_ROOT")
$InstallFolder =  Join-Path $installRoot "RecHub2.03.04\"
$DBServer = $configMgr.GetDBParamValue("DB_SERVER")
$URL = $configMgr.GetDBParamValue("SSRS_URL")

#  ================= Create Log Folder ==============
$LogFolder = $configMgr.GetDBParamValue("LOG_FOLDER")
Write-Host "Creating / Verifying Log folder: " $LogFolder
if (!(Test-Path -path $LogFolder)) { New-Item $LogFolder -itemtype directory -force } 

# ================== Raam Database ==================
Write-Host
Write-Host -ForegroundColor Magenta "RecHub SSRS Reports"
$DBName = $configMgr.GetDBParamValue("RECHUB_DB_NAME")
$SSRSFolder = $configMgr.GetDBParamValue("SSRS_REPORT_FOLDER")

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.00.00"
$CurrentCommandFile = "RecHub2.00.00.00SSRS.xml"
Install-SSRS

$CurrentInstallFolder = Join-Path $InstallFolder "2.00.01.00"
$CurrentCommandFile = "RecHub2.00.01.00SSRSPatch.xml"
Install-SSRS

$CurrentInstallFolder = Join-Path $InstallFolder "2.01.00.00"
$CurrentCommandFile = "RecHub2.01.00.00SSRSPatch.xml"
Install-SSRS

Write-Host
Write-Host "-> Deployment Complete <-"
Write-Host