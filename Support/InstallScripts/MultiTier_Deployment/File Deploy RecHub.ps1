﻿
param (
    [switch] $NestedCall,
    [switch] $Silent,
    # Don't perform any IIS configuration. Intended for testing only.
    [switch] $NoIis,
    # Don't perform any Windows service configuration. Intended for testing only.
    [switch] $NoService,
	# Don't backup before copying files
    [Switch] $SkipBackup
)

# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
[string] $Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)c"

$ErrorActionPreference = "Stop"

if (!$NestedCall) {
    # Start a new instance of PowerShell, so the DLL gets unloaded at the end
    $ExtraArgs = @()
    if ($Silent) {
        $ExtraArgs += "-Silent"
    }
    if ($NoIis) {
        $ExtraArgs += "-NoIis"
    }
    if ($NoService) {
        $ExtraArgs += "-NoService"
    }
    if ($SkipBackup) {
        $ExtraArgs += "-SkipBackup"
    }
    powershell -file $MyInvocation.MyCommand.Path -NestedCall @ExtraArgs
    if (!$Silent) {
        Write-Host "Press ENTER to exit: " -NoNewline
        Read-Host
    }
    return
}

if( $SkipBackup -and $Silent )
{
	Write-Host "Skipping backup, remember to backup files manually" -ForegroundColor Red -BackgroundColor White;
}
elseif( $SkipBackup )
{
	Write-Host "Skipping backup, remember to backup files manually. [Q] to quit, any other key to continue." -ForegroundColor Red -BackgroundColor White;
	$UserInput = [Console]::ReadKey($true);
	if( $UserInput.Key.ToString().ToUpper() -eq "Q" )
	{
		return
	}
}

function RemoveAppFiles
{
    param
    (
        [string] $local:app,
        [string] $local:sourceFolder,
        [string] $local:destFolder
    )

    ("*.exe", "*.exe.config", "*.exe.config.oem")|% {
        $local:file=$_ -replace "\*", $local:app;
        ($local:sourceFolder, $local:destFolder)|% {
            if(Test-Path -path (Join-Path $_ $local:file))
            {
                Remove-Item -Path (Join-Path $_ $local:file);
            }
        }
    }
}

function RemoveItemFromIISForRecHub
{
    param
    (
        $appNames
    )

    $appNames|%
    {
        $local:appName = $_;
        if(Get-WebApplication -Site R360 -Name $local:appName)
        {
            Remove-WebApplication -Site R360 -Name $local:appName;
        }
    }
}

function RemoveServiceIfExists
{
    param
    (
        [string] $local:svcName
    )
    $local:service = Get-WmiObject -Class Win32_Service -Filter "Name='$local:svcName'";
    if ($local:service)
    {
        Write-Host "Removing Service:" $local:svcName "...";
        $local:service.Delete();
    }
}

function CopyDitContainer ($source)
{
    Write-Host "source:  $source"
    Write-Host "destDitContainerDir:  $destDitContainerDir "
    
    if (!(test-path $source)) {
    		write-host "Folder '$source' does not exist. Moving on..."
    		return
    	}
    # Ensure the destination exist
	if (!(Test-Path -path $destDitContainerDir)) 
    { 
        Write-Host "$destDitContainerDir does not exist, creating it"
        New-Item $destDitContainerDir -ItemType Directory -Force | Out-Null 
    }
    Write-Host "Copying files from $source to $destDitContainerDir"
    #Copy-Item -Path (Join-Path $theSourceFolder "*") -Destination (Join-Path $theTarget "InstallDitContainer.exe") -Recurse -Force 
    Copy-Item -Path (Join-Path $source "*") -Destination $destDitContainerDir -Force -Recurse
}

function CopyFolder
{
    param
    (
        [string] $source,
        [string] $destination,
        [string] $backup,
        [string[]] $Exclude,
        # Subdirectories to delete in the destination, after backing up but before copying new files.
        [string[]] $SubdirectoriesToClean = @()
    )

	if (!(test-path $source)) {
		write-host "Folder '$source' does not exist. Moving on..."
		return
	}
	
    Write-Host "Copying files from:"
    Write-Host "  $source"
    Write-Host "to:"
    Write-Host "  $destination"
    Write-Host "excluding:"
    Write-Host "  $Exclude"
    if ($SubdirectoriesToClean) {
        Write-Host "cleaning: $SubdirectoriesToClean"
    }
	if( !$SkipBackup )
	{
		Write-Host "with backups in:"
		Write-Host "  $backup"
		Write-Host
	
	    # Ensure the destination and backup directories exist
	    if (!(Test-Path -path $destination)) { New-Item $destination -ItemType Directory -Force | Out-Null }
	    if (!(Test-Path -path $backup)) { New-Item $backup -ItemType Directory -Force | Out-Null }

	    # Back up
	    $backupSource = $destination.TrimEnd("\")
	    if (!$backupSource.EndsWith("\*")) { $backupSource = $backupSource + "\*" }
	    Copy-Item $backupSource $backup -Force -Recurse
	}
	else
	{
	    Write-Host
		Write-Host "Backup skipped" -ForegroundColor Red;
	}

    # Clean subdirectories
    $SubdirectoriesToClean | ForEach-Object {
        $CleanDirectory = Join-Path $destination $_
        if (Test-Path $CleanDirectory -PathType Container) {
            [System.IO.Directory]::Delete($CleanDirectory, $True)
        }
    }

    # Copy all source files except config files, which will be handled later.
    # Copy-Item -Recurse -Exclude doesn't behave as expected (-Exclude only applies
    # to top-level files, not files in subfolders); work around this by using
    # Get-ChildItem, which does work correctly with -Recurse and -Exclude.
    $source = $source.TrimEnd("\")
    Get-ChildItem "$source\*" -Recurse -File -Exclude $Exclude | Copy-Item -Destination {
        $DestinationPath = Join-Path $destination $_.FullName.Substring($source.Length)
        $DestinationDirectory = Split-Path -Parent $DestinationPath
        if (!(Test-Path $DestinationDirectory)) {
            New-Item $DestinationDirectory -ItemType Directory -Force | Out-Null
        }
        $DestinationPath
    }
}

function CopyMdTemplate($Source, $Target)
{
     $SourceWebTemplatePath = Join-Path $Source "RecHub.mdtemplate"
    write-host "getting ready to copy RecHub.Mdtemplate" 
	#check to make sure the target folder exists if not create it
	if (!(Test-Path $Target))
	{
	    Write-Host "$Target does not exist need to create it"
        New-Item -ItemType Directory -Force -Path $Target | Out-Null
	}
    #check to make sure something is actually there to copy.
    if (!(Test-Path $SourceWebTemplatePath))
    {
        write-host "$SourceWebTemplatePath does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $SourceWebTemplatePath to $Target"
    Copy-Item -Path $SourceWebTemplatePath -Destination $Target
}

function CopyScript($Source, $Target)
{
     $SourceWebTemplatePath = $Source
    write-host "getting ready to copy" $Source 
	#check to make sure the target folder exists if not create it
	if (!(Test-Path $Target))
	{
	    Write-Host "$Target does not exist need to create it"
        New-Item -ItemType Directory -Force -Path $Target | Out-Null
	}
    #check to make sure something is actually there to copy.
    if (!(Test-Path $SourceWebTemplatePath))
    {
        write-host "$SourceWebTemplatePath does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $SourceWebTemplatePath to $Target"
    Copy-Item -Path $SourceWebTemplatePath -Destination $Target
}

function CopyWebApp($WebAppName, $SourceWebRoot, $TargetWebRoot, $BackupWebRoot)
{
    $SourceWebAppPath = Join-Path $SourceWebRoot $WebAppName
    $TargetWebAppPath = Join-Path $TargetWebRoot $WebAppName
    $BackupWebAppPath = Join-Path $BackupWebRoot $WebAppName

    if (!(test-path $SourceWebAppPath)) {
        write-host "SourceWebRoot '$SourceWebAppPath' does not exist. Moving on..."
        return
    }

    CopyFolder $SourceWebAppPath $TargetWebAppPath $BackupWebAppPath -Exclude *.config,*.oem -SubdirectoriesToClean bin
}

function CreateWebApp
{
    param
    (
        [string] $iisSite,
        [string] $iisAppPoolName,
        [string] $sourcePath,
        [string] $basePath,
        [string] $appFolder
    )

    Write-Host "Web Application: " $iisSite " - " $appFolder
    cd 'IIS:\Sites'
    cd $iisSite

    if (!(Test-Path $appFolder -pathType container))
    {
        Write-Host "Creating Web Application: " $appFolder
        $fullSourcePath = Join-Path $sourcePath $appFolder
        $webConfig = Join-Path $fullSourcePath "web.config.template"
        if (!(Test-Path $webConfig))
        {
            Write-Host "<Skipping: Not a Web Application Folder>"
        }
        else
        {
            $fullPath = Join-Path $basePath $appFolder
            $app = New-Item $appFolder -physicalPath $fullPath -type Application
            Set-ItemProperty $appFolder -Name "applicationPool" -Value $iisAppPoolName
        }
    }
}

function ConfigureIISForRecHub
{
    param
    (
        [string] $sourceDir,
        [string] $destRecHubDir,
        [string] $sslCertThumbprint,
        $appFolders
    )

    #  ***************** Setup IIS *******************
    $iisAppPoolName = "WFS-RecHub"
    $iisAppPoolDotNetVersion = "v4.0"
    $iisSiteName = "R360"

    Write-Host "Beginning IIS Configuration..."

    #navigate to the app pools root
    Push-Location
    cd IIS:\AppPools\

    #check if the app pool exists
    if (!(Test-Path $iisAppPoolName -pathType container))
    {
        #create the app pool
        Write-Host "Creating App Pool: " $iisAppPoolName
        $appPool = New-Item $iisAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion
        $appPool | Set-ItemProperty -Name "processModel.identityType" -value 2 # 2 = NETWORK SERVICE
    }

    #navigate to the sites root
    cd IIS:\Sites\

    #stop the default site, if applicable
    Write-Host "Stopping Default Web Site..."
    Stop-WebItem 'Default Web Site'

    #check if the site exists
    if (!(Test-Path $iisSiteName -pathType container))
    {
        #create the site
        Write-Host "Creating Site: " $iisSiteName
        $sitePath = Join-Path $destRecHubDir "wwwroot"
            if (!(Test-Path -path $sitePath)) { New-Item $sitePath -ItemType Directory -Force | Out-Null }

        $iisSite = New-Item $iisSiteName -bindings @{protocol="https";bindingInformation="*:443:"} -physicalPath $sitePath
        $iisSite | Set-ItemProperty -Name "applicationPool" -Value $iisAppPoolName
    }

    if ($sslCertThumbprint.Length -gt 0)
    {
        if (Test-Path "IIS:\SslBindings\0.0.0.0!443")
        {
            Remove-Item IIS:\SslBindings\0.0.0.0!443
        }

        Get-ChildItem cert:LocalMachine\My\$sslCertThumbprint | New-Item IIS:\SslBindings\0.0.0.0!443
    }

    #verify the site is started
    Write-Host "Starting Site: " $iisSiteName
    Start-WebItem $iisSiteName

    #Create Web Applications
    Write-Host "Creating Web Applications..."
    $appFolders | ?{ CreateWebApp $iisSiteName $iisAppPoolName $sourceDir $destRecHubDir $_ }

    #Restore current directory
    Pop-Location
}


function ProcessConfigFile
{
    param (
        [string] $OemFolder,
        [string] $TargetFolder,
        [string] $FileName
    )

    $OemExtension = if ($FileName -like "*RecHub.ini") { ".file" } else { ".oem" }
    $OemPath = Join-Path $OemFolder "$FileName$OemExtension"
    $TargetPath = Join-Path $TargetFolder $FileName

	if (!(test-path $OemPath)) {
		write-host "Configuration File '$OemPath' does not exist. Moving on..."
		return
	}
	
    Write-Host "Processing File: $TargetPath"
    $configMgr.ProcessConfigVariables($OemPath, $TargetPath)
}


function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        if ($service.CanStop)
        {
            Write-Host "Stopping" $svcName "..."
            Stop-Service $service
        }
    }
}

function UninstallServiceIfExists {
    param (
        [string] $svcName
    )
    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0) {
        sc.exe delete $svcName
    }
}

function InstallServiceIfNotExists
{
    param
    (
        [string] $svcName,
        [string] $svcPath,
        [string] $displayName,
        [string] $user = "",
        [string] $password = ""
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -le 0)
    {
        Write-Host "Creating Service:" $svcName "..."

        # if password is empty, create a dummy one to allow having credentials for system accounts:
        #NT AUTHORITY\LOCAL SERVICE
        #NT AUTHORITY\NETWORK SERVICE
        if ($user -ne "")
        {
            $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
            $creds = New-Object System.Management.Automation.PSCredential ($user, $secpasswd)

            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -Credential $creds -StartupType Automatic
        }
        else
        {
            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -StartupType Automatic
        }
        return $true
    }
    else
    {
        Write-Host "Already Installed: " $svcName "-" $displayName
        return $true
    }
}

 function GenerateWCFHostedCommandLine {
    param (
        [string]$appFolder,
        [string]$svcName,
        [string]$className,
        [string]$serviceName,
        [string]$displayName
    )
    return GenerateWCFHostedCommandLine2 $appFolder $appFolder $svcName $className $serviceName $displayName
}

function GenerateWCFHostedCommandLine2 {
    param (
        [string]$appFolder,
        [string]$dllName,
        [string]$svcName,
        [string]$className,
        [string]$serviceName,
        [string]$displayName
    )
    $exePath = Join-Path $destRecHubBin2Dir "WCFServiceHost.exe"
    $baseSite = $configMgr.GetParamValue("FILE_SERVER")
    #displayname and servicename must be the first two arguments in the command line
    return "`"$exePath`" -displayname `"$displayName`" -servicename `"$serviceName`" -d:`"$(Join-Path $destRecHubIISDir "$appFolder\bin\$dllName.dll")`" -u:https://$baseSite/$appFolder/$svcName -c:$className"
}

function StartServices
{
    param
    (
        $serviceList
    )

    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName -ErrorAction SilentlyContinue
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)
}

function EnableSSLForPort
{
    param
    (
        [string] $port,
        [string] $sslCertThumbprint
    )

    if ($sslCertThumbprint.Length -gt 0)
    {
        # First, check to see if SSL is already configured
        $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
        if ($result.IndexOf($sslCertThumbprint, [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
        {
            Write-Host "SSL Binding already Configured"
        }
        else
        {
            & netsh http add sslcert ipport=0.0.0.0:$port certhash=$sslCertThumbprint "appid={00112233-0042-0042-0042-AABBCCDDEEFF}"
        }
    }
}

function TestCreateFolder
{
    param
    (
        [string] $folderTitle,
        [string] $folderPath
    )

    Write-Host "Creating / Verifying " $folderTitle " folder: " $folderPath
    if (!(Test-Path -path $folderPath)) { New-Item $folderPath -ItemType Directory -Force | Out-Null }
}


function InstallACHService
{
	$RecHubDeployPath = Join-Path $drive "WfsApps\RecHub\DitContainer\deploy\res\";
	$RecHubNSSM = Join-Path $RecHubDeployPath "Dependencies\nssm.exe";
	$RecHubACHServicePath = Join-Path $RecHubDeployPath "ACHService";

	Write-Host "Installing DitContainer located at" $RecHubACHServicePath;
	$cmd = [string]::Format("{0} install achservice {1}", $RecHubNSSM, (Join-Path $RecHubACHServicePath "ACHService.exe"));
	#write-host $cmd
	Invoke-Expression $cmd;
}

#  ****************************************************
#  ************** RECHUB FILE DEPLOYMENT **************
#  ****************************************************
Write-Host "Getting Install Parameters..."
$utilsPath = Join-Path $PSScriptRoot "Wfs.R360.DeploymentConfigurationUtilities.dll"
Add-Type -Path $utilsPath
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
$configMgr.GetParams(!$Silent, "FILE");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Deployment Cancelled"
}

if (($configMgr.GetParamValue("FILE_HOST_SERVICES_WITH") -eq "IIS") -and !$NoIis) {
    Import-Module WebAdministration
}

# ================== Setup Paths ==================
Write-Host "Setting up paths"
$drive = $configMgr.GetParamValue("InstallDrive") + "\"

Write-Host "Initiating Rollup"
# Get full path to the script:
$ScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RecHub Rollup.ps1"))
# Execute script at location:
&"$ScriptRoute" -Drive $drive -Tier File
Write-Host "Resume setting up paths"

$srcRecHubDir = Resolve-Path ".\FILE\$Version\"
$srcRecHubIISDir = Join-Path $srcRecHubDir "wwwroot"
$srcRecHubBinDir = Join-Path $srcRecHubDir "bin"
$srcRecHubBin2Dir = Join-Path $srcRecHubDir "bin2"
#$drive = $configMgr.GetParamValue("InstallDrive") + "\"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$srcDitContainer = Join-Path $drive "WFSStaging\DitContainer\$Version\"
$destDitContainerDir = Join-Path $drive "WFSApps\RecHub\DitContainer\"
$destRecHubIISDir = Join-Path $destRecHubDir "wwwroot"
$destRecHubBinDir = Join-Path $destRecHubDir "bin"
$destRecHubBin2Dir = Join-Path $destRecHubDir "bin2"
$backupTimestamp = get-date -f yyyy-MM-dd_HH_mm_ss
$backupPath = Join-Path (Join-Path (Join-Path (Resolve-Path ".") "Backups") $backupTimestamp) "RecHub"
$backupPathIIS = Join-Path $backupPath "wwwroot"
$backupPathBin = Join-Path $backupPath "bin"
$backupPathBin2 = Join-Path $backupPath "bin2"

Write-Host " "
Write-host "srcRecHubDir: $srcRecHubDir"
Write-Host "srcDitContainer: $srcDitContainer"
Write-Host "destDitContainerDir:  $destDitContainerDir"
Write-Host " "

write-host "Ensuring all files are marked non read-only..."
write-host "Checking destRecHubIISDir $destRecHubIISDir folder"
if ((Test-Path $destRecHubIISDir))
	{
	    Write-Host "altering Read only flag for $destRecHubIISDir"
        get-childitem $destRecHubIISDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }
write-host "Checking destRecHubBinDir $destRecHubBinDir folder"
if ((Test-Path $destRecHubBinDir))
	{
	    Write-Host "altering Read only flag for $destRecHubBinDir"
        get-childitem $destRecHubBinDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }
write-host "Checking destRecHubBin2Dir $destRecHubBin2Dir folder"
if ((Test-Path $destRecHubBin2Dir))
	{
	    Write-Host "altering Read only flag for $destRecHubBin2Dir"
        get-childitem $destRecHubBin2Dir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }

Write-Host "Deploying RecHub from:" $srcRecHubDir
Write-Host "To area:" $destRecHubDir
Write-Host

Write-Host "Backup of current files will be placed under:" $backupPath
Write-Host

#=================== template to allow configurator to update config files ==============================

CopyMdTemplate $srcRecHubDir $destRecHubDir
$ServiceScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RechubFileRestartServices.ps1"))

CopyScript $ServiceScriptRoute $destRecHubDir

$IdentityConfigTemplate = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($srcRecHubBin2Dir, "Identity.app.config.template"))

$webAppFolders = "DataImportServices", "FileImportServices", "ICONServices", "OLFServices", "OLFServicesExternal", "RecHubAuditingServices"
$webAppFolders | ForEach-Object {
	CopyScript $IdentityConfigTemplate (Join-Path $destRecHubIISDir $_)
}
# ================== Define Services =============
$serviceList = @(
    @("FileCleanupSvc",            (Join-Path $destRecHubBin2Dir "FileCleanupSvc.exe"),         "WFS IPOnline Cleanup Service", "", "", ""),
    @("OLFImageSvc",               (Join-Path $destRecHubBin2Dir "OLFImageSvc.exe"),            "WFS IPOnline Image Service", "", "", ""),
    @("OLFImageImportSvc",         "",                                                          "", "", "", ""),
    @("FilePurgeSvc",              (Join-Path $destRecHubBin2Dir "FilePurgeSvc.exe"),           "WFS File Purge Service", "", "", ""),
    @("DataImportSvc",             "",                                                          "WFS Data Import Service", "", "", ""),
    @("FileImportSvc",             "",                                                          "WFS File Import Service", "", "", ""),
    @("FileImportUploadSvc",       "",                                                          "WFS File Import Upload Service", "", "", ""),
    @("ExtractProcessService",     (Join-Path $destRecHubBin2Dir "ExtractProcessSvc.exe"),      "WFS Extract Processor Service", "", "", ""),
    @("ICONServices",              "",                                                          "WFS ICON Service", "", "", ""),
    @("OLFServices",               "",                                                          "WFS OLF Services", "", "", ""),
    @("OLFOnlineServices",         "",                                                          "WFS OLF Online Services", "", "", ""),
    @("OLFServicesExternal",       "",                                                          "WFS OLF Services External", "", "", ""),
    @("RecHubAuditingServices",    "",                                                          "WFS Auditing Service", "", "", "")
)

#$webAppFolders = get-ChildItem -Path $destRecHubIISDir | ?{ $_.PSIsContainer } | Select-Object -Expand Name
$appsToRemoveFromBin2 = "DataImportSvc", "FileImportSvc"

if($configMgr.GetParamValue("FILE_HOST_SERVICES_WITH") -eq "Stand Alone Service" ) {
    #  Setup DIT Service as a Windows WCF Hosted Service
    $serviceList.Item(4).Item(1) = GenerateWCFHostedCommandLine "DataImportServices" "DataImportService.svc" "DataImportService" $serviceList.Item(4).Item(0) $serviceList.Item(4).Item(2);
    $serviceList.Item(5).Item(1) = GenerateWCFHostedCommandLine "FileImportServices" "FileDataService.svc" "FileDataService" $serviceList.Item(5).Item(0) $serviceList.Item(5).Item(2);
    $serviceList.Item(6).Item(1) = GenerateWCFHostedCommandLine "FileImportServices" "FileUploadService.svc" "FileUploadService" $serviceList.Item(6).Item(0) $serviceList.Item(6).Item(2);
    $serviceList.Item(8).Item(1) = (Join-Path $destRecHubBin2Dir "ICONWebServiceBin\ICONWebServiceSvc.exe");
    $serviceList.Item(9).Item(1) = GenerateWCFHostedCommandLine "OLFServices" "OLFService.svc" "OLFService" $serviceList.Item(9).Item(0) $serviceList.Item(9).Item(2);
    $serviceList.Item(10).Item(1) = GenerateWCFHostedCommandLine "OLFServices" "OLFOnlineService.svc" "OLFOnlineService" $serviceList.Item(10).Item(0) $serviceList.Item(10).Item(2);
    $serviceList.Item(11).Item(1) = GenerateWCFHostedCommandLine "OLFServicesExternal" "OLFServiceExternal.svc" "OLFServiceExternal" $serviceList.Item(11).Item(0) $serviceList.Item(11).Item(2);
    $serviceList.Item(12).Item(1) = GenerateWCFHostedCommandLine2 "RecHubAuditingServices" "AuditingServices" "auditingDIT.svc" "AuditingDitSvc" $serviceList.Item(12).Item(0) $serviceList.Item(12).Item(2);
}
else {
    # Uninstall the WCFHosted services if we are using IIS
    $serviceList | Where-Object { $_[1] -eq "" } | % { UninstallServiceIfExists $_[0] };
}

# ================== Stop Service if applicable =============
if (!$NoService) {
    Write-Host "Stopping Services..."
    $serviceList | ?{ StopServiceIfExists $_[0] }
}

#  ***************** X-Copy Files **************
$webAppFolders | ForEach-Object {
    CopyWebApp -WebAppName $_ -SourceWebRoot $srcRecHubIISDir -TargetWebRoot $destRecHubIISDir -BackupWebRoot $backupPathIIS
}
CopyFolder $srcRecHubBinDir $destRecHubBinDir $backupPathBin -Exclude *.config
CopyFolder $srcRecHubBin2Dir $destRecHubBin2Dir $backupPathBin2 -Exclude *.config

# ==================== Remove Eliminated Items ===============
$appsToRemoveFromBin2|%{
    Write-Host "Removing $_ from $destRecHubBin2Dir";
    RemoveAppFiles $_ $srcRecHubBin2Dir $destRecHubBin2Dir;
}
Write-Host "  "
#=================== move DitContainer ==============
Write-Host "Moving DitContainer to fixed location"
CopyDitContainer $srcDitContainer
Write-Host "  "
#  ================= Create Log Folder ==============
$drive = $configMgr.GetParamValue("LogDrive") + "\"
$destLogDir = Join-Path $drive "WFSApps\Logs\"
TestCreateFolder "Log" $destLogDir

$destLogDir = Join-Path $drive "WFSApps\RecHub\Data\Logfiles\"
TestCreateFolder "Log" $destLogDir

#  ================= Create Img Folder ==============
$destImgDir = Join-Path $drive "Wfsapps\RecHub\Img\"
TestCreateFolder "Img" $destImgDir

#  ================= Create CENDS Folder ==============
$destCENDSDir = Join-Path $drive "\Wfsapps\Rechub\CENDS\"
TestCreateFolder "CENDS" $destCENDSDir

#  ================= Create Online Archive Folder ==============
$destArchiveDir = Join-Path $drive "Wfsapps\RecHub\OnlineArchive\"
TestCreateFolder "Online Archive" $destArchiveDir

if (($configMgr.GetParamValue("FILE_HOST_SERVICES_WITH") -eq "IIS") -and !$NoIis) {
    #  ***************** Setup IIS **************
    $sslCert = $configMgr.GetParamValue("SSL_CERT")
    ConfigureIISForRecHub $srcRecHubIISDir $destRecHubIISDir $sslCert $webAppFolders
}

#  ***************** Update Configs *************
if (!$Silent) {
    Write-Host "Updating Configuration Files..."
    $configs =  "RecHub.ini", "WCFServiceHost.exe.config"
    $configs | ForEach-Object {
        $config = $_
        ProcessConfigFile -OemFolder $srcRecHubBin2Dir -TargetFolder $destRecHubBin2Dir -FileName $config

        # Compare to backup, if not an encrypted file
        if ($config -ne "webMachineKey.config")
        {
            Write-Host -NoNewline "Checking for changes:  "
            $rslt = $configMgr.DetectConfigChanges($destRecHubBin2Dir, $backupPathBin2, $config)
            Write-Host $rslt
        }      
 
    }

}

# ========================= remove oem files ===================================
Write-Host "Removing any leftover .oem files"
Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "FileCleanupSvc.exe.config.oem") -Recurse | Remove-Item
Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "WCFClients.app.config.oem") -Recurse | Remove-Item


# ================== Install / Start Services if applicable =============
if (!$NoService) {
    Write-Host "Installing / Starting Services, and registering SSL when applicable..."
    $sslCert = $configMgr.GetParamValue("SSL_CERT")
    $serviceList | ?{
        if ($_[1] -ne "")
        {
            if ((InstallServiceIfNotExists $_[0] $_[1] $_[2] $_[3] $_[4]) -eq $true)
            {
                if ($_[5] -ne "")
                {
                    Write-Host "(Service requires SSL Port Registration)"
                    EnableSSLForPort $_[5] $sslCert
                }
            }
        }
    }

   # StartServices $serviceList
}

#Start DitContainer Installer
Write-Host "Starting DitContainer installer";
Write-Host "---Please wait for installer to complete ---";
Start-Process -FilePath (Join-Path $destDitContainerDir "InstallDitContainer") -ArgumentList "--silentMode" -NoNewWindow -Wait;
#Install ACHService 
InstallACHService
Write-Host "*********************************";
Write-Host "     Deploy of code complete     ";
Write-Host "*********************************";
Write-Host "   Now manually";
Write-Host "     Run Configurator";
Write-Host "     Start achservice";
Write-Host "     Start other Services";

Write-Host
Write-Host "-> Deployment Complete <-"
if( $SkipBackup -and $Silent )
{
	Write-Host "*** Backup skipped, remember to backup files manually ***" -ForegroundColor Red -BackgroundColor White;
}
