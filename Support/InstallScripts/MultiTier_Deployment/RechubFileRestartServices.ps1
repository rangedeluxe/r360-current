param (
    [string] $drive = "d:"
)

Write-Host "Setting up paths"
$destRecHubDir = Join-Path $drive "\WFSApps\RecHub\"
$destRecHubBin2Dir = Join-Path $destRecHubDir "bin2"

# ================== Define Services =============
$serviceList = @(
    @("FileCleanupSvc",            (Join-Path $destRecHubBin2Dir "FileCleanupSvc.exe"),         "WFS IPOnline Cleanup Service", "", "", ""),
    @("OLFImageSvc",               (Join-Path $destRecHubBin2Dir "OLFImageSvc.exe"),            "WFS IPOnline Image Service", "", "", ""),
    @("OLFImageImportSvc",         "",                                                          "", "", "", ""),
    @("FilePurgeSvc",              (Join-Path $destRecHubBin2Dir "FilePurgeSvc.exe"),           "WFS File Purge Service", "", "", ""),
    @("DataImportSvc",             "",                                                          "WFS Data Import Service", "", "", ""),
    @("FileImportSvc",             "",                                                          "WFS File Import Service", "", "", ""),
    @("FileImportUploadSvc",       "",                                                          "WFS File Import Upload Service", "", "", ""),
    @("ExtractProcessService",     (Join-Path $destRecHubBin2Dir "ExtractProcessSvc.exe"),      "WFS Extract Processor Service", "", "", ""),
    @("ICONServices",              "",                                                          "WFS ICON Service", "", "", ""),
    @("OLFServices",               "",                                                          "WFS OLF Services", "", "", ""),
    @("OLFOnlineServices",         "",                                                          "WFS OLF Online Services", "", "", ""),
    @("OLFServicesExternal",       "",                                                          "WFS OLF Services External", "", "", ""),
    @("RecHubAuditingServices",    "",                                                          "WFS Auditing Service", "", "", "")
)

function StartServices
{
    param
    (
        $serviceList
    )

    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName -ErrorAction SilentlyContinue
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)
}

# ================== Start Services if applicable =============
Write-Host "Starting Services"
StartServices $serviceList