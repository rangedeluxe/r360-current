﻿param (
    [switch] $NestedCall,
    [switch] $Silent,
    # Don't perform any IIS configuration. Intended for testing only.
    [switch] $NoIis,
    # Don't perform any Windows service configuration. Intended for testing only.
    [switch] $NoService,
	# Don't backup before copying files
    [Switch] $SkipBackup
)

# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
[string] $Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)c"

$ErrorActionPreference = "Stop"

if (!$NestedCall) {
    # Start a new instance of PowerShell, so the DLL gets unloaded at the end
    $ExtraArgs = @()
    if ($Silent) {
        $ExtraArgs += "-Silent"
    }
    if ($NoIis) {
        $ExtraArgs += "-NoIis"
    }
    if ($NoService) {
        $ExtraArgs += "-NoService"
    }
    if ($SkipBackup) {
        $ExtraArgs += "-SkipBackup"
    }
    powershell -file $MyInvocation.MyCommand.Path -NestedCall @ExtraArgs
    if (!$Silent) {
        Write-Host "Press ENTER to exit: " -NoNewline
        Read-Host
    }
    return
}

if( $SkipBackup -and $Silent )
{
	Write-Host "Skipping backup, remember to backup files manually" -ForegroundColor Red -BackgroundColor White;
}
elseif( $SkipBackup )
{
	Write-Host "Skipping backup, remember to backup files manually. [Q] to quit, any other key to continue." -ForegroundColor Red -BackgroundColor White;
	$UserInput = [Console]::ReadKey($true);
	if( $UserInput.Key.ToString().ToUpper() -eq "Q" )
	{
		return
	}
}

function BackUpAndRemoveObsoleteFolder
{
    param
    (
        [string] $local:source,
        [string] $local:dest,
        [string] $local:backup
    )

    $local:source = $local:source.TrimEnd("\");
    if( !$local:source.EndsWith("\*") ) { $local:source = $local:source.TrimEnd("\*") }
    $local:dest = $local:dest.TrimEnd("\");
    if( !$local:dest.EndsWith("\*") ) { $local:dest = $local:dest.TrimEnd("\*") }

	if( !$SkipBackup )
	{
    	if (!(Test-Path -path $local:backup)) { New-Item $local:backup -ItemType Directory -Force | Out-Null }
		copy-item "$local:dest\*" $local:backup -Force -Recurse
	}
	else
	{
		Write-Host "Backup skipped" -ForegroundColor Red;
	}
	
    Remove-Item -Path $local:source -Recurse;
    Remove-Item -Path $local:dest -Recurse;
}

function CopyFolder
{
    param
    (
        [string] $source,
        [string] $destination,
        [string] $backup,
        # Subdirectories to delete in the destination, after backing up but before copying new files.
        [string[]] $SubdirectoriesToClean = @()
    )
	
	if (!(test-path $source)) {
		write-host "Folder '$source' does not exist. Moving on..."
		return
	}

    Write-Host "Copying files from:"
    Write-Host "  $source"
    Write-Host "to:"
    Write-Host "  $destination"
    if ($SubdirectoriesToClean) {
        Write-Host "cleaning: $SubdirectoriesToClean"
    }
	if( !$SkipBackup )
	{
		Write-Host "with backups in:"
		Write-Host "  $backup"
		Write-Host
	
	    # Ensure the destination and backup directories exist
	    if (!(Test-Path -path $destination)) { New-Item $destination -ItemType Directory -Force | Out-Null }
	    if (!(Test-Path -path $backup)) { New-Item $backup -ItemType Directory -Force | Out-Null }

	    # Back up
		$backupSource = $destination.TrimEnd("\")
		if (!$backupSource.EndsWith("\*")) { $backupSource = $backupSource + "\*" }
		Copy-Item $backupSource $backup -Force -Recurse
	}
	else
	{
		Write-Host
		Write-Host "Backup skipped" -ForegroundColor Red;
	}
	
    # Clean subdirectories
    $SubdirectoriesToClean | ForEach-Object {
        $CleanDirectory = Join-Path $destination $_
        if (Test-Path $CleanDirectory -PathType Container) {
            [System.IO.Directory]::Delete($CleanDirectory, $True)
        }
    }

    # Copy all source files including .template files
    # Copy-Item -Recurse -Exclude doesn't behave as expected (-Exclude only applies
    # to top-level files, not files in subfolders); work around this by using
    # Get-ChildItem, which does work correctly with -Recurse and -Exclude.
    
    $source = $source.TrimEnd("\")
    Get-ChildItem "$source\*" -Recurse -File -Exclude *.app,*.oem | Copy-Item -Destination {
        $DestinationPath = Join-Path $destination $_.FullName.Substring($source.Length)
        $DestinationDirectory = Split-Path -Parent $DestinationPath
        if (!(Test-Path $DestinationDirectory)) {
            New-Item $DestinationDirectory -ItemType Directory -Force | Out-Null
        }
        $DestinationPath
    } 
}

function CopyWebApp($WebAppName, $SourceWebRoot, $TargetWebRoot, $BackupWebRoot)
{
    $SourceWebAppPath = Join-Path $SourceWebRoot $WebAppName
    $TargetWebAppPath = Join-Path $TargetWebRoot $WebAppName
    $BackupWebAppPath = Join-Path $BackupWebRoot $WebAppName

    if (!(test-path $SourceWebAppPath)) {
        write-host "SourceWebRoot '$SourceWebAppPath' does not exist. Moving on..."
        return
    }

    CopyFolder $SourceWebAppPath $TargetWebAppPath $BackupWebAppPath -SubdirectoriesToClean bin
}

function CopyMdTemplate($Source, $Target)
{
     $SourceWebTemplatePath = Join-Path $Source "RecHub.mdtemplate"
    write-host "getting ready to copy RecHub.Mdtemplate" 
	#check to make sure the target folder exists if not create it
	if (!(Test-Path $Target))
	{
	    Write-Host "$Target does not exist need to create it"
        New-Item -ItemType Directory -Force -Path $Target | Out-Null
	}
    #check to make sure something is actually there to copy.
    if (!(Test-Path $SourceWebTemplatePath))
    {
        write-host "$SourceWebTemplatePath does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $SourceWebTemplatePath to $Target"
    Copy-Item -Path $SourceWebTemplatePath -Destination $Target
}

function CreateWebApp
{
    param
    (
        [string] $iisSite,
        [string] $iisAppPoolName,
        [string] $sourcePath,
        [string] $basePath,
        [string] $appFolder
    )

    Write-Host "Web Application: " $iisSite " - " $appFolder
    cd 'IIS:\Sites'
    cd $iisSite

    if (!(Test-Path $appFolder -pathType container))
    {
        Write-Host "Creating Web Application: " $appFolder
        $fullSourcePath = Join-Path $sourcePath $appFolder
        $webConfig = Join-Path $fullSourcePath "web.config.template"
		$jsonConfig = Join-Path $fullSourcePath "appsettings.json.template"
        if ( (!(Test-Path $webConfig)) -and (!(Test-Path $jsonConfig)) )
        {
            Write-Host "<Skipping: Not a Web Application Folder>"
        }
        else
        {
            $fullPath = Join-Path $basePath $appFolder
            $app = New-Item $appFolder -physicalPath $fullPath -type Application
            Set-ItemProperty $appFolder -Name "applicationPool" -Value $iisAppPoolName
        }
    }
}

function RemoveItemFromIISForRecHub
{
    param
    (
        $appNames
    )

    $appNames|%{
        $local:appName = $_;
        if(Get-WebApplication -Site R360 -Name $local:appName)
        {
            Remove-WebApplication -Site R360 -Name $local:appName;
        }
    }
}

function ConfigureIISForRecHub
{
    param
    (
        [string] $sourceDir,
        [string] $destRecHubDir,
        $appFolders,
		$coreAppFolders
    )

    #  ***************** Setup IIS *******************
    $iisAppPoolName = "WFS-RecHub"
    $iisAppPoolDotNetVersion = "v4.0"
	$iisCoreAppPoolName = "WFS-RecHubCore"
	$iisCoreAppPoolDotNetVersion = ""
    $iisSiteName = "R360"

    Write-Host "Beginning IIS Configuration..."

    #navigate to the app pools root
    Push-Location
    cd IIS:\AppPools\
	
	#check if the app pools exists; if not, create them 
    if (!(Test-Path $iisAppPoolName -pathType container))
    {
        #create the app pool
        Write-Host "Creating App Pool: " $iisAppPoolName
        $appPool = New-Item $iisAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion
        $appPool | Set-ItemProperty -Name "processModel.identityType" -value 2 # 2 = NETWORK SERVICE
    }
	if (!(Test-Path $iisCoreAppPoolName -pathType container))
    {
        #create the app pool
        Write-Host "Creating App Pool: " $iisCoreAppPoolName
        $appPool = New-Item $iisCoreAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisCoreAppPoolDotNetVersion
        $appPool | Set-ItemProperty -Name "processModel.identityType" -value 2 # 2 = NETWORK SERVICE
    }
	
    #navigate to the sites root
    cd IIS:\Sites\

    #check if the site exists
    if (!(Test-Path $iisSiteName -pathType container))
    {
        Write-Host "Error - Site was not found: " $iisSiteName

        #Stop deployment - we can't find the website...
        #Stop All Further processing
        Throw "Deployment Cancelled"
    }

    #Create Web Applications
    Write-Host "Creating Web Applications..."
    $appFolders | ?{ CreateWebApp $iisSiteName $iisAppPoolName $sourceDir $destRecHubDir $_ }
	$coreAppFolders | ?{ CreateWebApp $iisSiteName $iisCoreAppPoolName $sourceDir $destRecHubDir $_ }

    #Restore current directory
    Pop-Location
}

function ProcessConfigFile
{
    param (
        [string] $OemFolder,
        [string] $TargetFolder,
        [string] $FileName
    )

    $OemExtension = if ($FileName -like "*RecHub.ini") { ".app" } else { ".oem" }
    $OemPath = Join-Path $OemFolder "$FileName$OemExtension"
    $TargetPath = Join-Path $TargetFolder $FileName
	
	if (!(test-path $OemPath)) {
		write-host "Configuration File '$OemPath' does not exist. Moving on..."
		return
	}

    Write-Host "Processing File: $TargetPath"
    $configMgr.ProcessConfigVariables($OemPath, $TargetPath)
}

function CopyOemConfigs {
    param (
        $SourceDirectory,
        $TargetDirectory
    )

	if (!(test-path $SourceDirectory)) {
		write-host "OemConfig directory '$SourceDirectory' does not exist. Moving on..."
		return
	}
	
    Write-Host "Copying OEM files"
    Write-Host "  from $SourceDirectory"
    Write-Host "  into $TargetDirectory"
    Copy-Item "$SourceDirectory\*" $TargetDirectory -Include *.oem,*.app
}

function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        if ($service.CanStop)
        {
            Write-Host "Stopping" $svcName "..."
            Stop-Service $service
        }
    }
}

function RemoveServiceIfExists
{
    param
    (
        [string] $local:svcName
    )
    $local:service = Get-WmiObject -Class Win32_Service -Filter "Name='$local:svcName'";
    if ($local:service)
    {
        Write-Host "Removing Service:" $local:svcName "...";
        $local:service.Delete();
    }
}

function InstallServiceIfNotExists
{
    param
    (
        [string] $svcName,
        [string] $svcPath,
        [string] $displayName,
        [string] $user = "",
        [string] $password = ""
    )

    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -le 0)
    {
        Write-Host "Creating Service:" $svcName "..."

        # if password is empty, create a dummy one to allow having credentials for system accounts:
        #NT AUTHORITY\LOCAL SERVICE
        #NT AUTHORITY\NETWORK SERVICE
        if ($user -ne "")
        {
            $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
            $creds = New-Object System.Management.Automation.PSCredential ($user, $secpasswd)

            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -Credential $creds -StartupType Automatic
        }
        else
        {
            New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -StartupType Automatic
        }
        return $true
    }
    else
    {
        Write-Host "Already Installed: " $svcName "-" $displayName
        return $true
    }
}

function StartServices
{
    param
    (
        $serviceList
    )

    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_.serviceName
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName -ErrorAction SilentlyContinue
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)
}

function EnableSSLForPort
{
    param
    (
        [string] $port,
        [string] $sslCertThumbprint
    )

    if ($sslCertThumbprint.Length -gt 0)
    {
        # First, check to see if SSL is already configured
        $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
        if ($result.IndexOf($sslCertThumbprint, [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
        {
            Write-Host "SSL Binding already Configured"
        }
        else
        {
            & netsh http add sslcert ipport=0.0.0.0:$port certhash=$sslCertThumbprint "appid={00112233-0042-0042-0042-AABBCCDDEEFF}"
        }
    }
}

function DisableSSLForPort
{
    param
    (
        [string] $port
    )

    # First, check to see if SSL is already configured
    $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
    if ($result.IndexOf("0.0.0.0:$port", [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
    {
        & netsh http delete sslcert ipport=0.0.0.0:$port
    }
    else
    {
        Write-Host "SSL Binding already removed"
    }
}

function TestCreateFolder
{
    param
    (
        [string] $folderTitle,
        [string] $folderPath
    )

    Write-Host "Creating / Verifying $folderTitle folder: $folderPath"
    if (!(Test-Path -path $folderPath)) { New-Item $folderPath -ItemType Directory -Force | Out-Null }
}

function RemoveAppFiles
{
    param
    (
        [string] $local:app,
        [string] $local:sourceFolder,
        [string] $local:destFolder
    )

    ("*.exe", "*.exe.config", "*.exe.config.oem")|% {
        $local:file=$_ -replace "\*", $local:app;
        ($local:sourceFolder, $local:destFolder)|% {
            if(Test-Path -path (Join-Path $_ $local:file))
            {
                Remove-Item -Path (Join-Path $_ $local:file);
            }
        }
    }
}


#  ***************************************************
#  ************** RECHUB APP DEPLOYMENT **************
#  ***************************************************
if (!$NoIis) {
    Import-Module WebAdministration
}

Write-Host "Getting Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
$configMgr.GetParams(!$Silent, "APP");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Deployment Cancelled"
}

# ================== Setup Paths ==================
Write-Host "Setting up paths"
$drive = $configMgr.GetParamValue("InstallDrive") + "\"

Write-Host "Initiating Rollup"
# Get full path to the script:
$ScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RecHub Rollup.ps1"))
# Execute script at location:
&"$ScriptRoute" -Drive $drive -Tier App
Write-Host "Resume setting up paths"

$srcRecHubDir = Resolve-Path ".\APP\$Version\"
$srcRecHubIISDir = Join-Path $srcRecHubDir "wwwroot"
$srcRecHubBinDir = Join-Path $srcRecHubDir "bin2"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destRecHubIISDir = Join-Path $destRecHubDir "wwwroot"
$destRecHubBinDir = Join-Path $destRecHubDir "bin2"
$destFrameworkDir = Join-Path $drive "WFSApps\Framework\"
$destFrameworkServicesDir = Join-Path $drive "WFSApps\Framework\Framework.Services\"
$backupTimestamp = get-date -f yyyy-MM-dd_HH_mm_ss
$backupPath = Join-Path (Join-Path (Join-Path (Resolve-Path ".") "Backups") $backupTimestamp) "RecHub"
$backupPathIIS = Join-Path $backupPath "wwwroot"
$backupPathBin = Join-Path $backupPath "bin2"

write-host "Ensuring all files are marked non read-only..."
#write-host "Checking destRecHubDir $destRecHubDir folder"
#if ((Test-Path $destRecHubDir))
#	{
#	    Write-Host "altering Read only flag for $destRecHubDir"
#        get-childitem $destRecHubDir -recurse | where-object { !$_.PSIsContainer } | % {
#	       set-itemproperty $_.fullname -name isreadonly -value $false
#        }
#	}
write-host "Checking destRecHubIISDir $destRecHubIISDir folder"
if ((Test-Path $destRecHubIISDir))
	{
	    Write-Host "altering Read only flag for $destRecHubIISDir"
        get-childitem $destRecHubIISDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }
write-host "Checking destRecHubBinDir $destRecHubBinDir folder"
if ((Test-Path $destRecHubBinDir))
	{
	    Write-Host "altering Read only flag for $destRecHubBinDir"
        get-childitem $destRecHubBinDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }

Write-Host "Deploying RecHub from:" $srcRecHubDir
Write-Host "To area:" $destRecHubDir
Write-Host

Write-Host "Backup of current files will be placed under:" $backupPath
Write-Host

#=================== template to allow configurator to update config files ==============================

CopyMdTemplate $srcRecHubDir $destRecHubDir

# ================== Define Services =============
$serviceList = @(
    ([Management.Automation.PSObject]@{
            serviceName="FileCleanupSvc";
            servicePath=(Join-Path $destRecHubBinDir "FileCleanupSvc.exe");
            displayName="WFS IPOnline Cleanup Service";
            appName="FileCleanupSvc";
            userId="";
            password="";
            sslPort="";
        }
    )
)

$deprecatedServiceList = @(
    ([Management.Automation.PSObject] @{
            serviceName="WFSR360BillingExtractsSvc";
            servicePath=(Join-Path $destRecHubBinDir "BillingExtractsService.exe");
            displayName="WFS R360 Billing Extracts Service";
            appName="BillingExtractsService";
            userId="";
            password="";
            sslPort="9500"
        }
    )
)

#$webAppFolders = get-ChildItem -Path $destRecHubIISDir | ?{ $_.PSIsContainer } | Select-Object -Expand Name
$webAppFolders = "OLDecisioningServices", "OLFServices", "RecHubConfigService", "RecHubExternalLogonService", "RecHubExtractScheduleService",
                 "RecHubReportingService", "RecHubServices", "RecHubSessionMaintenanceService"
$webCoreAppFolders = "AdminAPI"
$webAppRemoveList = "RecHubPayerService", "RecHubExceptionsService", "RecHubAuditingServices", "PayerAPI"

# ================== Stop Service if applicable =============
if (!$NoService) {
    Write-Host "Stopping Services..."
    $serviceList | ?{ StopServiceIfExists $_.serviceName }
}

#  ================= Remove Eliminated Items =========
$webAppRemoveList|?{ Test-Path(Join-Path $destRecHubIISDir $_)}|%{
    Write-Host "Removing folder $_";
    BackUpAndRemoveObsoleteFolder (Join-Path $srcRecHubIISDir $_) (Join-Path $destRecHubIISDir $_) (Join-Path $backupPathIIS $_);
    RemoveServiceIfExists $_;
}

#remove the deprecated services
if( !$NoService ){
    Write-Host "Stopping Deprecated Services..."
    $deprecatedServiceList | ?{ StopServiceIfExists $_.serviceName }

    Write-Host "Removing Deprecated Services..."
    $deprecatedServiceList | ?{RemoveServiceIfExists $_.serviceName } | Out-Null;

    Write-Host "Removing Deprecated SSL when applicable..."
    $deprecatedServiceList | ?{
        if ($_.servicePath -ne "")
        {
            if ($_.sslPort -ne "")
            {
                Write-Host "(Service had SSL Port Registration)"
                DisableSSLForPort $_.sslPort
            }
        }
    }

    Write-Host "Removing Deprecated Services executables, configs, etc..."
    $deprecatedServiceList |?{
        Write-Host ([string]::Format("Removing {0} from {1}",$_.appName,$destRecHubBinDir));
        RemoveAppFiles $_.appName $srcRecHubBinDir $destRecHubBinDir;
    }
}

#  ***************** X-Copy Files **************
$webAppFolders | ForEach-Object {
    CopyWebApp -WebAppName $_ -SourceWebRoot $srcRecHubIISDir -TargetWebRoot $destRecHubIISDir -BackupWebRoot $backupPathIIS
}
$webCoreAppFolders | ForEach-Object {
    CopyWebApp -WebAppName $_ -SourceWebRoot $srcRecHubIISDir -TargetWebRoot $destRecHubIISDir -BackupWebRoot $backupPathIIS
}
CopyFolder $srcRecHubBinDir $destRecHubBinDir $backupPathBin

#  ================= Create Log Folders ==============
$drive = $configMgr.GetParamValue("LogDrive") + "\"
$destLogDir = Join-Path $drive "WFSApps\Logs\"
TestCreateFolder "Log" $destLogDir

$destLogDir = Join-Path $drive "WFSApps\RecHub\Data\Logfiles\"
TestCreateFolder "Log" $destLogDir

#  ================= Create Reports Folder ==============
$destReportsDir = Join-Path $drive "Wfsapps\Reports\"
TestCreateFolder "Reports" $destReportsDir

#  ================= Create Online Archive Folder ==============
$destArchiveDir = Join-Path $drive "Wfsapps\RecHub\OnlineArchive\"
TestCreateFolder "Online Archive" $destArchiveDir

#  ***************** Setup IIS **************
if (!$NoIis) {
    ConfigureIISForRecHub $srcRecHubIISDir $destRecHubIISDir $webAppFolders $webCoreAppFolders
    RemoveItemFromIISForRecHub $webAppRemoveList
}

#  ***************** Update Configs *************
if (!$Silent) {
Write-Host "Updating Configuration Files..."
    $configs = "RecHub.ini"
    $configs | ?{
        $config = $_
        ProcessConfigFile -OemFolder $srcRecHubBinDir -TargetFolder $destRecHubBinDir -FileName $config
       
        Write-Host -NoNewline "Checking for changes:  "
        $rslt = $configMgr.DetectConfigChanges($destRecHubBinDir, $backupPathBin, $config)
        Write-Host $rslt       
    }
}

# ========================= remove oem files ===================================
Write-Host "Removing any leftover .oem files"
Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "FileCleanupSvc.exe.config.oem") -Recurse | Remove-Item
Get-ChildItem (Join-Path -Path $destRecHubBinDir -ChildPath "WCFClients.app.config.oem") -Recurse | Remove-Item

# ================== Install / Start Services if applicable =============
if (!$NoService) {
    Write-Host "Installing / Starting Services, and registering SSL when applicable..."
    $sslCert = $configMgr.GetParamValue("SSL_CERT")
    $serviceList | ?{
        if ($_.servicePath -ne "")
        {
            if ((InstallServiceIfNotExists $_.serviceName $_.servicePath $_.displayName $_.userId $_.password) -eq $true)
            {
                if ($_.sslPort -ne "")
                {
                    Write-Host "(Service requires SSL Port Registration)"
                    EnableSSLForPort $_.sslPort $sslCert
                }
            }
        }
    }

    StartServices $serviceList
}

Write-Host
Write-Host "-> Deployment Complete <-"
if( $SkipBackup -and $Silent )
{
	Write-Host "*** Backup skipped, remember to backup files manually ***" -ForegroundColor Red -BackgroundColor White;
}
