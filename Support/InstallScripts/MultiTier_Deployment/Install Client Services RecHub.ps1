param($NestedCall, $backupTimestamp = "", $promptParams = $True)

$ErrorActionPreference = "Stop"

# restart PowerShell with -noexit, the same script, and 1
if (!$NestedCall) {
    powershell -noexit -file $MyInvocation.MyCommand.Path 1
    return
}


function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )
    
    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        Write-Host "Stopping" $svcName "..."
        Stop-Service $service
    }
}

function Confirm
{
    param
    (
        [string] $title1,
        [string] $message
    )
    
    $title2 = $title1 + ': '
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Go for it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Not right now."
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
    $result = $host.ui.PromptForChoice($title2, $message, $options, 0)
    
    switch ($result)
    {
        0 { return $true }
        1 { return $false }
    }
}

function InstallServiceIfNotExists
{
    param
    (
        [string] $svcName,
        [string] $svcPath,
        [string] $displayName,
        [string] $user = "",
        [string] $password = ""
    )
    
    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -le 0)
    {
        if ((Confirm $displayName "Do you want to install the service ($svcName)?") -eq $true)
        {
            Write-Host "Creating Service:" $svcName "..."
            
            # if password is empty, create a dummy one to allow having credentials for system accounts: 
            #NT AUTHORITY\LOCAL SERVICE
            #NT AUTHORITY\NETWORK SERVICE
            if ($user -ne "") 
            {
                $secpasswd = ConvertTo-SecureString $password -AsPlainText -Force
                $creds = New-Object System.Management.Automation.PSCredential ($user, $secpasswd)

                New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -Credential $creds -StartupType Automatic
            }
            else
            {
                New-Service -BinaryPathName $svcPath -Name $svcName -DisplayName $displayName -StartupType Automatic
            }
        }
        else
        {
            Write-Host "Skipping:" $svcName
        }
    }
    else
    {
        Write-Host "Already Installed: " $svcName "-" $displayName
    }
}

function StartServices
{
    param
    (
        $serviceList
    )
    
    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {    
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service 
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)    
}

function EnableSSLForPort
{
    param
    (
        [string] $port,
        [string] $sslCertThumbprint
    )
    
    if ($sslCertThumbprint.Length -gt 0)
    {
        # First, check to see if SSL is already configured
        $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
        if ($result.IndexOf($sslCertThumbprint, [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
        {
            Write-Host "SSL Binding already Configured"
        }
        else
        {    
            & netsh http add sslcert ipport=0.0.0.0:$port certhash=$sslCertThumbprint "appid={00112233-0042-0042-0042-AABBCCDDEEFF}"
        }
    }
}

#  ************************************************   
#  ************** RECHUB SERVICES *****************
#  ************************************************   
Write-Host "Prompting for Service Installation..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
Write-Host "Verifying Parameters..."
$configMgr.GetParams($promptParams, "DRIVE");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Installation Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Installation Cancelled"
}


# ================== Setup Paths ==================
$drive = $configMgr.GetParamValue("InstallDrive") + "\"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destRecHubBinDir = Join-Path $destRecHubDir "bin"

write-host "Ensuring all files are marked non read-only..."
get-childitem $destRecHubDir -recurse | where-object { !$_.PSIsContainer } | % {
	set-itemproperty $_.fullname -name isreadonly -value $false
}
get-childitem $destRecHubBinDir -recurse | where-object { !$_.PSIsContainer } | % {
	set-itemproperty $_.fullname -name isreadonly -value $false
}

# ================== Define Services =============
$serviceList = @(
    @("DataImportClientSvc",    (Join-Path $destRecHubBinDir "DataImportClientSvc.exe"),  "WFS Data Import Client Service", "", "", ""),
    @("FileImportClientSvc",    (Join-Path $destRecHubBinDir "FileImportClientSvc.exe"),  "WFS File Import Client Service", "", "", "")
)

# ================== Stop Service if applicable =============
Write-Host "Stopping Services..."
$serviceList | ?{ StopServiceIfExists $_[0] }

# ================== Install / Start Services if applicable =============
Write-Host "Installing / Starting Services, and registering SSL when applicable..."
$serviceList | ?{ 
    if ($_[1] -ne "") 
    { 
        if ((InstallServiceIfNotExists $_[0] $_[1] $_[2] $_[3] $_[4]) -eq $true)
        {
            if ($_[5] -ne "") 
            { 
                EnableSSLForPort $_[5] $sslCert 
            }
        }
    } 
}

StartServices $serviceList

Write-Host
Write-Host "-> Installation Complete <-"