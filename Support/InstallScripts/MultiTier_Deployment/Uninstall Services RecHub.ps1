param($NestedCall, $backupTimestamp = "", $promptParams = $True)

$ErrorActionPreference = "Stop"

# restart PowerShell with -noexit, the same script, and 1
if (!$NestedCall) {
    powershell -noexit -file $MyInvocation.MyCommand.Path 1
    return
}


function StopServiceIfExists
{
    param
    (
        [string] $svcName
    )
    
    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        Write-Host "Stopping" $svcName "..."
        Stop-Service $service
    }
}

function UninstallServiceIfExists
{
    param
    (
        [string] $svcName
    )
    
    $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
    if ($service.Length -gt 0)
    {
        $wmiService = Get-WmiObject -Class Win32_Service -Filter "Name='$svcName'"
        if ((Confirm $svcName "Do you want to uninstall the service ($svcName)?") -eq $true)
        {
            Write-Host "Uninstalling" $svcName "..."
            $wmiService.delete()
            Write-Host "<Service Removed>"
        }
    }
}

function Confirm
{
    param
    (
        [string] $title1,
        [string] $message
    )
    
    $title2 = $title1 + ': '
    $yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", "Go for it."
    $no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", "Not right now."
    $options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
    $result = $host.ui.PromptForChoice($title2, $message, $options, 0)
    
    switch ($result)
    {
        0 { return $true }
        1 { return $false }
    }
}

function StartServices
{
    param
    (
        $serviceList
    )
    
    Write-Host "Starting Services..."
    $serviceStatus = @()
    $serviceList | ?{
        $svcName = $_[0]
        $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
        if ($service.Length -gt 0)
        {    
            Write-Host "Starting" $svcName "..."
            Start-Service $svcName
            $service = Get-Service -Name $svcName -ErrorAction SilentlyContinue
            $serviceStatus += $service 
        }
    }
    Write-Host ($serviceStatus | Format-Table | Out-String)    
}

function EnableSSLForPort
{
    param
    (
        [string] $port,
        [string] $sslCertThumbprint
    )
    
    if ($sslCertThumbprint.Length -gt 0)
    {
        # First, check to see if SSL is already configured
        $result = & netsh http show sslcert ipport=0.0.0.0:$port | Out-String
        if ($result.IndexOf($sslCertThumbprint, [StringComparison]::InvariantCultureIgnoreCase) -ge 0)
        {
            Write-Host "SSL Binding already Configured"
        }
        else
        {    
            & netsh http add sslcert ipport=0.0.0.0:$port certhash=$sslCertThumbprint "appid={00112233-0042-0042-0042-AABBCCDDEEFF}"
        }
    }
}

#  ************************************************   
#  ************** RECHUB SERVICES *****************
#  ************************************************   
Write-Host "Prompting for Service Installation..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
Write-Host "Verifying Parameters..."
$configMgr.GetParams($promptParams, "DRIVE");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Installation Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Installation Cancelled"
}


# ================== Setup Paths ==================
$drive = $configMgr.GetParamValue("InstallDrive") + "\"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destRecHubBin2Dir = Join-Path $destRecHubDir "bin2"

# ================== Define Services =============
$serviceList = @(
    @("FileCleanupSvc",            (Join-Path $destRecHubBin2Dir "FileCleanupSvc.exe"),         "WFS IPOnline Cleanup Service", "", "", ""),
    @("WFSExtractProcessSvc",      (Join-Path $destRecHubBin2Dir "ExtractProcessSvc.exe"),      "WFS Extract Process Service", "", "", ""),
    @("OLFImageSvc",               (Join-Path $destRecHubBin2Dir "OLFImageSvc.exe"),            "WFS IPOnline Image Service", "", "", ""),
    @("OLFImageImportSvc",         "",                                                          "", "", "", ""),
    @("FilePurgeSvc",              (Join-Path $destRecHubBin2Dir "FilePurgeSvc.exe"),           "WFS File Purge Service", "", "", ""),
    @("DataImportSvc",             (Join-Path $destRecHubBin2Dir "DataImportSvc.exe"),          "WFS Data Import Service", "", "", ""),
    @("FileImportSvc",             (Join-Path $destRecHubBin2Dir "FileImportSvc.exe"),          "WFS File Import Service", "", "", ""),
    @("WFSR360BillingExtractsSvc", (Join-Path $destRecHubBin2Dir "BillingExtractsService.exe"), "WFS R360 Billing Extracts Service", "", "", "9500")
)

# ================== Stop Service if applicable =============
Write-Host "Stopping Services..."
$serviceList | ?{ StopServiceIfExists $_[0] }

# ================== Uninstall / Start Services if applicable =============
Write-Host "Uninstalling Services..."
ForEach ($svc IN $serviceList)
{
    UninstallServiceIfExists $svc[0]
}

Write-Host "Restarting remaining services..."
StartServices $serviceList

Write-Host
Write-Host "-> Un-Installation Complete <-"