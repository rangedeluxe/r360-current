SET QUOTED_IDENTIFIER OFF
GO

RAISERROR('Creating Database with name: @@DB_NAME@@',10,1) WITH NOWAIT
USE [master]
GO

/****** Object:  Database [@@DB_NAME@@]    Script Date: 12/8/2014 8:48:50 AM ******/
if exists (select name from master.dbo.sysdatabases where name = N'@@DB_NAME@@')
BEGIN
	RAISERROR('@@DB_NAME@@ already exists.',10,1) WITH NOWAIT
END
ELSE
BEGIN
	CREATE DATABASE [@@DB_NAME@@]
	 CONTAINMENT = NONE
	 ON  PRIMARY 
	( NAME = N'@@DB_NAME@@', FILENAME = N'@@SQL_DATA_PATH@@\@@DB_NAME@@.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
	 LOG ON 
	( NAME = N'@@DB_NAME@@_log', FILENAME = N'@@SQL_LOG_PATH@@\@@DB_NAME@@_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
END
GO

ALTER DATABASE [@@DB_NAME@@] SET COMPATIBILITY_LEVEL = 110
GO

IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [@@DB_NAME@@].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO

ALTER DATABASE [@@DB_NAME@@] SET ANSI_NULL_DEFAULT OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET ANSI_NULLS OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET ANSI_PADDING OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET ANSI_WARNINGS OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET ARITHABORT OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET AUTO_CLOSE OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET AUTO_CREATE_STATISTICS ON 
GO

ALTER DATABASE [@@DB_NAME@@] SET AUTO_SHRINK OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET AUTO_UPDATE_STATISTICS ON 
GO

ALTER DATABASE [@@DB_NAME@@] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET CURSOR_DEFAULT  GLOBAL 
GO

ALTER DATABASE [@@DB_NAME@@] SET CONCAT_NULL_YIELDS_NULL OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET NUMERIC_ROUNDABORT OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET QUOTED_IDENTIFIER OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET RECURSIVE_TRIGGERS OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET TRUSTWORTHY OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET PARAMETERIZATION SIMPLE 
GO

ALTER DATABASE [@@DB_NAME@@] SET READ_COMMITTED_SNAPSHOT OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET HONOR_BROKER_PRIORITY OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET RECOVERY FULL 
GO

ALTER DATABASE [@@DB_NAME@@] SET  MULTI_USER 
GO

ALTER DATABASE [@@DB_NAME@@] SET PAGE_VERIFY CHECKSUM  
GO

ALTER DATABASE [@@DB_NAME@@] SET DB_CHAINING OFF 
GO

ALTER DATABASE [@@DB_NAME@@] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO

ALTER DATABASE [@@DB_NAME@@] SET  READ_WRITE 
GO

RAISERROR('Database creation complete',10,1) WITH NOWAIT
GO
