﻿param ( 
    [string] $Drive,
    [string] $Tier
)

# this version of rollup works for the following release
# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
$Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)"


#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
#VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
#----------------------------------------------------------------------------
#  For each release you must add the copy from folder for the current release
#----------------------------------------------------------------------------
#^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
#||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

#now go remove the folder from the target before copying new folder contents
function RemoveFolder($remove)
{
    Write-Host "Executing function RemoveFolder"
    $theFolderToRemove = Join-Path $theTarget ("wwwroot\" + $remove)
    Write-Host "    Removing the following folder '$theFolderToRemove'  from target "
    if (!(Test-Path $theFolderToRemove))
    {
        Write-Host "    $theFolderToRemove does not exist....skipping"
        return
    }
    Write-Host "    Deleting $theFolderToRemove"
    Remove-Item $theFolderToRemove -Force -Recurse
}

#remove any folders under wwwroot that we are getting ready to copy in
function CheckForFoldersToRemove()
{
    Write-Host "Executing function CheckForFoldersToRemove"
    Write-Host "    checking $theSourceFolder for anything to delete"
    $checkRoot = Join-Path $theSourceFolder "wwwroot"
    Write-Host "    checking for children under $checkRoot"
    if (!(Test-Path $checkRoot))
    {
        write-host "    $checkRoot does not exist. Moving on..."
        write-host "  "
        return
    } 
   $thechildren = Get-ChildItem -Path $checkRoot -Directory
   Write-Host "the children $thechildren"
   $thechildren | ForEach-Object -Process{RemoveFolder $_}
  
}

function CopyOnlyFolder ($theSourceFolder)
{
   Write-Host "Executing function CopyFolder"
   Write-Host "    Copy from $theSourceFolder to $theTarget"
   # make sure the source exists.
   if (!(Test-Path $theSourceFolder))
    {
        write-host "$theSourceFolder does not exist. Moving on..."
        write-host "  "
        return
    } 

   # if target does not exist create it
   if (!(Test-Path $theTarget))
    {
        write-host "    $theTarget does not exist. Creating it"
        New-Item $theTarget -ItemType directory 
    }
    else
    {
        CheckForFoldersToRemove
    }

    #copy only what is in the folder, not the folder
   Copy-Item -Path (Join-Path $theSourceFolder "*") -Destination $theTarget -Recurse -Force 
}

#Dit folder only contains .exe
#  but currently it has the rel version as part of the name
#  copy but rename the target (excluding rel version)
function CopyDitFolder ($theSourceFolder)
{    
   Write-Host "Ditcontainer found - executing CopyDitFolder"
   # make sure the source exists.
   if (!(Test-Path $theSourceFolder))
    {
        write-host "    $theSourceFolder does not exist. Moving on..."
        write-host "  "
        return
    } 

   # if target does not exist create it
   if (!(Test-Path $theTarget))
    {
        write-host "    $theTarget does not exist. Creating it"
        New-Item $theTarget -ItemType directory 
    }

    #copy only what is in the folder, not the folder
    #                                                             renameing target
   Copy-Item -Path (Join-Path $theSourceFolder "*") -Destination (Join-Path $theTarget "InstallDitContainer.exe") -Recurse -Force 
}

#copy the folders to the target folder
function CopyFolder ($theSourceFolder)
{
    Write-Host "Copy from $theSourceFolder to $theTarget"
    if ($Target -eq "DitContainer")
    {
       CopyDitFolder $theSourceFolder
    }
    else
    {
        CopyOnlyFolder $theSourceFolder
    }
   
}

# pick up the previous releases and copy them to a common folder.
#  the common folder is current version + "c"
function CopyRelease($Source, $Target)
{
    Write-Host "Executing function CopyRelease"
   Write-Host "  "
   # create source
   $theSource = Join-Path $Source $Target
   $theTarget = Join-Path $theSource $targetVersion

   #first remove the target if it exists to start with fresh target
   if (Test-Path $theTarget)
   {
       Write-Host "Removing the directory '$theTarget' from the target"
       Remove-Item $theTarget -Force -Recurse

   }
   
    #loop through to copy all folders from array
	for ($i=0; $i -lt $versionData.AllVersions.length; $i++) { 
		$filePath = Join-Path $theSource $versionData.AllVersions[$i];
		Write-Host "Copying file $filePath "
		CopyFolder($filePath) 
	}
}

function ProcessTier()
{
    switch ($Tier)
    {
        "App" {CopyRelease $srcFolder "App"; break;}
        "Client-Dit" {CopyRelease $srcFolder "Client-Dit" break;}
        "Client-Extract" {CopyRelease $srcFolder "Client-Extract" break;}
        "Client-Fit" {CopyRelease $srcFolder "Client-Fit" break;}
        "File" {CopyRelease $srcFolder "File"
                CopyRelease $srcFolder "DitContainer"
                break;}
        "Web" {CopyRelease $srcFolder "Web" break;}
        default {Write-Host "Tier not found to roll up:  $Tier"}
    }
}


# ================== Setup Paths ==================
$srcFolder = $Drive + "WFSStaging"
$targetVersion = $Version + "c"

Write-Host "Starting the rollup"
Write-Host "SourceFolder = $srcFolder"
Write-Host "Tier = $Tier"
Write-Host "The Combined folder $targetVersion"

IF (!(Test-Path -Path $Drive))
{
    Write-Host "Drive not found: $Drive"
    exit
}

IF (!(Test-Path -Path $srcFolder))
{
    Write-Host "Folder not found: $srcFolder"
    exit
}

#Now go copy the files for the tier specified
ProcessTier


Write-Host

Write-Host "-> rollup Complete <-"

