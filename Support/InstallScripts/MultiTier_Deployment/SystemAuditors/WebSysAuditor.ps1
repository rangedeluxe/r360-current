param($inputFile)
function Get-Windows-Info($minOsVersion) {
    $currentHostOs = (Get-WmiObject -Class Win32_OperatingSystem)
    $currentUpdates = Get-WmiObject -class win32_quickfixengineering | 
        Select-Object "Description", "HotFixID", "InstalledBy", "InstalledOn"
    $osversion = $currentHostOs.Version 
    $statusMessage = $null
    if ([Version]$minOsVersion -gt [Version]$osversion) {
        $statusMessage = "Min OS Version expected: $minOsVersion Found: $osversion"
    }
    $winInfo = [pscustomobject]@{ OSName = $currentHostOs.Caption; UpdateList = $currentUpdates;
        Status = $statusMessage 
    }
    return $winInfo
}

function Get-Url-RewriteModule {
    $path = "Registry::HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\IIS Extensions\URL Rewrite\"
    $val = "Install"
    $retVal = Test-RegistryKeyValue $path $val
    
    if ($retVal -eq $true) { return "IIS Rewrite Module is installed "}
    else { return "IIS Rewrite Module is Not Installed"}

}
function Get-NET-Version($minVersion) {

    $retVal = (Get-ChildItem 'HKLM:\SOFTWARE\Microsoft\NET Framework Setup\NDP' -recurse |
            Get-ItemProperty -name Version, Release -EA 0 |  
            Where-Object { ($_.PSChildName -match 'Full') -and ([Version]$minVersion -lt [Version]$_.Version )} | 
            Select-Object @{Name = "Product"; Expression = {$_.PSChildName}}, Version, Release)
    if ($retVal.Length -eq 0) {
        return [PSCustomObject]@{
            Status = "No .net version found greater than $minVersion"
        }
    }
    return $retVal 
}

function Get-NETCore-Version($minVersion) {
    $res = (Get-ChildItem (Get-Command dotnet).Path.Replace('dotnet.exe', 'shared\Microsoft.NETCore.App'))
    $ret = $res | Where-Object { [Version]$minVersion -lt [Version]$_.Name } | 
        Select-Object @{Name = "Install Time"; Expression = {$_.LastWriteTime}}, 
    @{Name = "Version #"; Expression = {$_.Name}}

    if ($ret.Length -eq 0) {
        return [PSCustomObject]@{
            Status = "No .net core found greater than $minVersion"
        }
    }
    return $ret 
}

function Get-IIS-Version ($minVersion) {
    $iis = "HKLM:\SOFTWARE\Microsoft\InetStp\"
    if (Get-ChildItem $iis -ErrorAction SilentlyContinue) {

        return get-itemproperty $iis |
            Where-Object {[Version]"$($_.MajorVersion).$($_.MinorVersion)" -gt [Version]$minVersion} |
            Select-Object @{Name = "Product"; Expression = {$_.ProductString}},
        MajorVersion, MinorVersion, VersionString
    }
    else { 

        return [PSCustomObject]@{
            Status = "Internet Information Services(IIS) $minVersion or greater not found." 
        }
        
    }  
}

function Get-Tls-Versions($minVersion) {
    $protocols = "HKLM:\SYSTEM\CurrentControlSet\Control\SecurityProviders\SCHANNEL\Protocols\"
    $res = @(Get-ChildItem $protocols -Recurse |
            Get-ItemProperty | 
            Where-Object { ($_.PSParentPath -match 'TLS') -and ($_.PSParentPath -match $minVersion) } |
            Select-Object @{ Name = "Path"; Expression = { $_.PSPath -split '\\' | Select-Object -last 2} }, 
        @{Name = "Enabled"; Expression = {if ($_.Enabled -gt 0) {"Enabled"} else {"Disabled"}}}, 
        @{Name = "DisabledByDefault"; Expression = {if ($_.DisabledByDefault -gt 0) { "Yes"}else {"No"} }})
    if ($res.Length -eq 0) {return [PSCustomObject]@{Status = "No TLS Version $minVersion found"} 
    }
    return $res
            
}

function Test-StrongCrypto {
    $message = "Strong cryptography not enforced for all .net apps. SchUseStrongCrypto flag not found"
    $res = $false
    if ((Test-RegistryKeyValue -Path 'HKLM:\SOFTWARE\Wow6432Node\Microsoft\.NetFramework\v4.0.30319' -Name 'SchUseStrongCrypto') -and 
        (Test-RegistryKeyValue -Path 'HKLM:\SOFTWARE\Microsoft\.NetFramework\v4.0.30319' -name 'SchUseStrongCrypto')) {
        $message = "Strong cryptography enforced for all .net apps"
        $res = $true
    }
    return [PSCustomObject]@{
        Status = $message;
        Result = $res
    }
}

function Test-RegistryKeyValue {
    <#
    .SYNOPSIS
    Tests if a registry value exists.

    .DESCRIPTION
    The usual ways for checking if a registry value exists don't handle when a value simply has an empty or null value.  This function actually checks if a key has a value with a given name.

    .EXAMPLE
    Test-RegistryKeyValue -Path 'hklm:\Software\Carbon\Test' -Name 'Title'

    Returns `True` if `hklm:\Software\Carbon\Test` contains a value named 'Title'.  `False` otherwise.
    #>
    [CmdletBinding()]
    param(
        [Parameter(Mandatory = $true)]
        [string]
        # The path to the registry key where the value should be set.  Will be created if it doesn't exist.
        $Path,

        [Parameter(Mandatory = $true)]
        [string]
        # The name of the value being set.
        $Name
    )

    if ( -not (Test-Path -Path $Path -PathType Container) ) {
        return $false
    }

    $properties = Get-ItemProperty -Path $Path 
    if ( -not $properties ) {
        return $false
    }

    $member = Get-Member -InputObject $properties -Name $Name
    if ( $member ) {
        return $true
    }
    else {
        return $false
    }

}

function Write-ColorOutput {
    [CmdletBinding()]
    Param(
        [Parameter(Mandatory = $True, Position = 1, ValueFromPipeline = $True, ValueFromPipelinebyPropertyName = $True)][Object] $Object,
        [Parameter(Mandatory = $False, Position = 2, ValueFromPipeline = $True, ValueFromPipelinebyPropertyName = $True)][ConsoleColor] $ForegroundColor,
        [Parameter(Mandatory = $False, Position = 3, ValueFromPipeline = $True, ValueFromPipelinebyPropertyName = $True)][ConsoleColor] $BackgroundColor,
        [Switch]$NoNewline
    )    

    # Save previous colors
    $previousForegroundColor = $host.UI.RawUI.ForegroundColor
    $previousBackgroundColor = $host.UI.RawUI.BackgroundColor

    # Set BackgroundColor if available
    if ($BackgroundColor -ne $null) { 
        $host.UI.RawUI.BackgroundColor = $BackgroundColor
    }

    # Set $ForegroundColor if available
    if ($ForegroundColor -ne $null) {
        $host.UI.RawUI.ForegroundColor = $ForegroundColor
    }

    # Always write (if we want just a NewLine)
    if ($null -eq $Object) {
        $Object = ""
    }

    if ($NoNewline) {
        [Console]::Write($Object)
    }
    else {
        Write-Output $Object
    }

    # Restore previous colors
    $host.UI.RawUI.ForegroundColor = $previousForegroundColor
    $host.UI.RawUI.BackgroundColor = $previousBackgroundColor
}

if ($null -eq $inputFile) { 
    $inputFile = Join-Path (Get-Item -Path ".\").FullName "r360reqs.json"
}
if ((Test-Path -path $inputFile -PathType Leaf) -ne $true) {
    Write-Output "Requirements file not found." 
    exit
}
$reqs = Get-Content -Raw -Path $inputFile| ConvertFrom-Json

Write-Output "File defined r360 requirements"
$reqs | Write-Output 
Write-Output $env:COMPUTERNAME
#get some OS Info
$winInfo = Get-Windows-Info $reqs.os 
Write-Output $winInfo.OSName
if ($null -ne $winInfo.Status ) {Write-ColorOutput $winInfo.Status Red}
Write-Output "Updates"
$winInfo.UpdateList | Format-Table | Write-Output
#get TLS protocols
Write-Output "TLS Protocols"
$tls = Get-Tls-Versions $reqs.tls 
if ( $tls.Status -eq $null ) {Write-Output $tls} 
else { Write-ColorOutput $tls}


Write-Output "`n"
#get .net version
$net = Get-NET-Version $reqs.net
Write-Output ".Net Version"
if ($null -ne $net.Status) {Write-ColorOutput $net.Status Red}
else {$net | Format-Table | Write-Output}
#get .net core 
Write-Output ".Net core" 
$core = Get-NETCore-Version $reqs.netcore
if ($null -ne $core.Status) { Write-ColorOutput $core.Status Red} 
if ($null -ne $core) {
    ($core | Format-Table | Out-String).Trim()
}

Write-Output "`n"
#get IIS info
Write-Output "IIS"
$iis = Get-IIS-Version $reqs.iis
if ($null -ne $iis.Status) { 

    Write-ColorOutput $iis.Status Red
}
$iis | Where-Object {$null -eq $_.Status } | Format-Table | Write-Output
#check if URl rewrite module is installed for IIS
if ($reqs.rewritemodule -eq $true) {
    $rewriteModule = Get-Url-RewriteModule
    if ($null -ne $rewriteModule.Status) {
        Write-ColorOutput $rewriteModule.Status Red
    }
    $rewriteModule | Where-Object {$null -ne $_}| Write-Output
}

#check enforce strong crypto for .net apps
$scrypto = Test-StrongCrypto
if ($scrypto.Result -eq $false) {
    Write-ColorOutput $scrypto.Status Red
}
else { Write-Output $scrypto.Status }