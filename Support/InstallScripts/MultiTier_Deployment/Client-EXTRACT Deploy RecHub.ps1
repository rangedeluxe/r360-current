﻿param (
    [switch] $NestedCall,
    [switch] $Silent,
	# Don't backup before copying files
    [Switch] $SkipBackup
)

# Get version data from Version.json
$versionData = (Get-Content ".\Version.json" | ConvertFrom-Json)
[string] $Version = "$($versionData.ProductName)$($versionData.Major).$($versionData.Minor).$($versionData.HotFix).$($versionData.Patch)c"

$ErrorActionPreference = "Stop"

if (!$NestedCall) {
    # Start a new instance of PowerShell, so the DLL gets unloaded at the end
    $ExtraArgs = @()
    if ($Silent) {
        $ExtraArgs += "-Silent"
    }
    if ($SkipBackup) {
        $ExtraArgs += "-SkipBackup"
    }
    powershell -file $MyInvocation.MyCommand.Path -NestedCall @ExtraArgs
    if (!$Silent) {
        Write-Host "Press ENTER to exit: " -NoNewline
        Read-Host
    }
    return
}

if( $SkipBackup -and $Silent )
{
	Write-Host "Skipping backup, remember to backup files manually" -ForegroundColor Red -BackgroundColor White;
}
elseif( $SkipBackup )
{
	Write-Host "Skipping backup, remember to backup files manually. [Q] to quit, any other key to continue." -ForegroundColor Red -BackgroundColor White;
	$UserInput = [Console]::ReadKey($true);
	if( $UserInput.Key.ToString().ToUpper() -eq "Q" )
	{
		return
	}
}

function CopyFolder
{
    param
    (
        [string] $source,
        [string] $destination,
        [string] $backup,
        [string[]] $Exclude
    )

    Write-Host "Copying files from:"
    Write-Host "  $source"
    Write-Host "to:"
    Write-Host "  $destination"
    Write-Host "excluding:"
    Write-Host "  $Exclude"
	if( !$SkipBackup )
	{
	    Write-Host "with backups in:"
	    Write-Host "  $backup"
	    Write-Host

	    if (!(Test-Path -path $destination)) { New-Item $destination -ItemType Directory -Force | Out-Null }
	    if (!(Test-Path -path $backup)) { New-Item $backup -ItemType Directory -Force | Out-Null }

	    $source = $source.TrimEnd("\")

	    $backupSource = $destination.TrimEnd("\")
	    if( !$backupSource.EndsWith("\*") ) { $backupSource = $backupSource + "\*" }

    	copy-item $backupSource $backup -Force -Recurse
	}
	else
	{
		Write-Host "Backup skipped" -ForegroundColor Red;
		Write-Host
	}

    # Copy all source files except config files, which will be handled later.
    # Copy-Item -Recurse -Exclude doesn't behave as expected (-Exclude only applies
    # to top-level files, not files in subfolders); work around this by using
    # Get-ChildItem, which does work correctly with -Recurse and -Exclude.
    Get-ChildItem "$source\*" -Recurse -File -Exclude $Exclude | Copy-Item -Destination {
        $DestinationPath = Join-Path $destination $_.FullName.Substring($source.Length)
        $DestinationDirectory = Split-Path -Parent $DestinationPath
        if (!(Test-Path $DestinationDirectory)) {
            New-Item $DestinationDirectory -ItemType Directory -Force | Out-Null
        }
        $DestinationPath
    }
}

function ProcessConfigFile
{
    param
    (
        [string] $OemFolder,
        [string] $TargetFolder,
        [string] $FileName
    )

    $OemExtension = if ($FileName -like "*RecHub.ini") { ".file" } else { ".oem" }
    $OemPath = Join-Path $OemFolder "$FileName$OemExtension"
    $TargetPath = Join-Path $TargetFolder $FileName

	if (!(test-path $OemPath)) {
		write-host "Configuration File '$OemPath' does not exist. Moving on..."
		return
	}
	
    Write-Host "Processing File: $TargetPath"
    $configMgr.ProcessConfigVariables($OemPath, $TargetPath)
}

#  copy the mdtemplate used by configurator
function CopyMdTemplate($Source, $Target)
{
    write-host "getting ready to copy $Source" 
    #check to make sure something is actually there to copy.
    if (!(Test-Path $Source))
    {
        write-host "$Source does not exist. Moving on..."
        write-host "  "
        return
    }
    Write-Host "copying $Source to $Target"
    Copy-Item -Path $Source -Destination $Target
}


#  **************************************************************
#  ************** RECHUB CLIENT-EXTRACT DEPLOYMENT **************
#  **************************************************************
Write-Host "Getting Install Parameters..."
Add-Type -Path "Wfs.R360.DeploymentConfigurationUtilities.dll"
$configMgr = New-Object -TypeName Wfs.R360.DeploymentConfigurationUtilities.ConfigManager
$configMgr.GetParams(!$Silent, "EXTRACT");
if ($configMgr.ParamFileName -eq "")
{
    Write-Host
    Write-Host "-> Deployment Cancelled <- ";
    Write-Host

    #Stop All Further processing
    Throw "Deployment Cancelled"
}


# ================== Setup Paths ==================
Write-Host "Setting up paths"
$drive = $configMgr.GetParamValue("InstallDrive") + "\"

Write-Host "Initiating Rollup"
# Get full path to the script:
$ScriptRoute = [System.IO.Path]::GetFullPath([System.IO.Path]::Combine($PSScriptRoot, "RecHub Rollup.ps1"))
# Execute script at location:
&"$ScriptRoute" -Drive $drive -Tier CLIENT-EXTRACT
Write-Host "Resume setting up paths"

if (!(test-path ".\CLIENT-EXTRACT\$Version\")) {
	write-host "Root directory '.\CLIENT-EXTRACT\$Version\' does not exist. Nothing to install."
	return
}

$srcRecHubDir = Resolve-Path ".\CLIENT-EXTRACT\$Version\"
$srcRecHubBin2Dir = Join-Path $srcRecHubDir "bin2"
#$drive = $configMgr.GetParamValue("InstallDrive") + "\"
$destRecHubDir = Join-Path $drive "WFSApps\RecHub\"
$destExtractDesignManagerDir = Join-Path $drive "WFSApps\RecHub\ExtractDesignManager\"
$destRecHubBin2Dir = Join-Path $destExtractDesignManagerDir "bin2"
$backupTimestamp = get-date -f yyyy-MM-dd_HH_mm_ss
$backupPath = Join-Path (Join-Path (Join-Path (Resolve-Path ".") "Backups") $backupTimestamp) "RecHub"
$backupPathBin2 = Join-Path $backupPath "bin2"

Write-Host "Deploying RecHub from:" $srcRecHubDir
Write-Host "To area:" $destExtractDesignManagerDir
Write-Host

if( !$SkipBackup )
{
	Write-Host "Backup of current files will be placed under:" $backupPath
	Write-Host
}

write-host "Ensuring all files are marked non read-only..."
write-host "Checking destExtractDesignManagerDir $destExtractDesignManagerDir folder"
if ((Test-Path $destExtractDesignManagerDir))
	{
	    Write-Host "altering Read only flag for $destExtractDesignManagerDir"
        get-childitem $destExtractDesignManagerDir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
	}
write-host "Checking destRecHubBin2Dir $destRecHubBin2Dir folder"
if ((Test-Path $destRecHubBin2Dir))
	{
	    Write-Host "altering Read only flag for $destRecHubBin2Dir"
        get-childitem $destRecHubBin2Dir -recurse | where-object { !$_.PSIsContainer } | % {
	       set-itemproperty $_.fullname -name isreadonly -value $false
        }
    }

#  ***************** X-Copy Files **************
CopyFolder $srcRecHubBin2Dir $destRecHubBin2Dir $backupPathBin2 -Exclude *.config

#  ================= Create Log Folder ==============
$drive = $configMgr.GetParamValue("LogDrive") + "\"
$destLogDir = Join-Path $drive "WFSApps\RecHub\Logs\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

$destLogDir = Join-Path $drive "WFSApps\RecHub\ExtractDesignManager\Logs\"
Write-Host "Creating / Verifying Log folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#  ================= Create ExtractDefinitions Folder ==============
$destLogDir = Join-Path $drive "WFSApps\RecHub\ExtractDesignManager\ExtractDefinitions\"
Write-Host "Creating / Verifying ExtractDefinitions folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#  ================= Create Image Folder ==============
$destLogDir = Join-Path $drive "WFSApps\RecHub\ExtractDesignManager\Image\"
Write-Host "Creating / Verifying Image folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#  ================= Create Output Folder ==============
$destLogDir = Join-Path $drive "WFSApps\RecHub\ExtractDesignManager\Output\"
Write-Host "Creating / Verifying Output folder: " $destLogDir
if (!(Test-Path -path $destLogDir)) { New-Item $destLogDir -ItemType Directory -Force | Out-Null }

#=================== template to allow configurator to update config files ==============================
CopyMdTemplate (Join-Path $srcRecHubDir "R360EDM.mdtemplate") $destRecHubDir

Write-Host
Write-Host "-> Deployment Complete <-"
if( $SkipBackup -and $Silent )
{
	Write-Host "*** Backup skipped, remember to backup files manually ***" -ForegroundColor Red -BackgroundColor White;
}
